var viewport;
var myMask;
var authString;
var loginClient;
var loginId;
var httpStatus;
var campusCode='TTU';
//var clientId='001';
var flag='';
var pageSize;
var selectRecord;
var serviceReq='N';
var status='SUCCESS';
var refreshItem='TABLE';
var selectIndex;
var userAddFlag=true;
var source='AdminConsole';
var label=null;
var task=null;
var selectAll=false;
//var webserver='https://ttumobileapp.tntech.edu:8443/enroll/json/'
var webserver='http://prod2.n2nservices.com:8080/enrollttu/json/'
//var webserver='/enroll/json/'

Ext.Loader.setConfig({
    enabled:true,
    disableCaching: true
});
Ext.application({
    //    requires: ['Ext.container.Viewport'],
    name:'login',
    appFolder:'app',

    views:[
    'attdLoginView',
    'courseListView'
    ],

    controllers:[
    'attendanceController'  
    ],


    launch:function () {
        myApp=this;
        // Create the vieport and store the reference in global variable
        viewport = Ext.create('Ext.Viewport', {
            alias:'viewPort',
            layout:'card'
        });

        // Add loginView (first one) to viewport
        var login=Ext.widget('attdLoginView');
        viewport.add(login);
        Ext.Ajax.timeout = 60000;

        // invokes before each ajax request
        Ext.Ajax.on('beforerequest', function () {
            if(task!=null){
                task.cancel();
                task.delay(1200000);
            }
            // showing the loading mask
            myMask = new Ext.LoadMask(viewport, {
                msg:"Please wait..."
            });
            myMask.show();
        });
        Ext.Ajax.on('requestcomplete', function () {
            httpStatus='200';
            // hidding the loading mask
            myMask.hide();
        });

        // invokes if exception occured
        Ext.Ajax.on('requestexception', function (conn, response, options, eOpts) {

            if (response.status == 401) {
                httpStatus='401';
                Ext.Msg.alert(' ', 'Invalid username or password');
                myMask.hide();
            }
            else {
                Ext.Msg.alert(' ', '<center>Server error has occured.Please try again</center>');
                myMask.hide();
            }

            myMask.unmask();
        });
    }

});



