Ext.define('login.view.classRosterView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.classRosterView',
    title: '<table height=10><tr><td width=10%><img src="images/n2n_logo.png" height="70" width=550></td><td><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
    autoScroll: true,

    config: {
        padding: '20',
        items: [{
            xtype: 'datefield',
            format: 'm-d-Y',
            width: 500,
            cls: 'logo',
            emptyText: 'Select Date',
            maxValue: new Date(),
            //            value:new Date(),
            fieldLabel: 'Date',
            name: 'classRosterDate',
            id: 'classRosterDate',
            editable: false,
            allowBlank: false,
            required: true,
            listeners: {
                change: function(picker, newdate, olddate) {
                    if (newdate != null) {
                        var dateValue = newdate;
                        var newdateValue;
                        if (dateValue == null) {
                            newdateValue = '';
                        } else {
                            newdateValue = (dateValue.getDate() + '-' + [dateValue.getMonth() + 1] + '-' + dateValue.getFullYear());
                        }
                        //get the store info
                        var store = Ext.getStore('attCourseList');
                        var sData = store.data.getRange();
                        var options = Ext.getStore('attClassList');
                        options.getProxy().clear();
                        options.removeAll();
                        options.sync();
                        options.add({
                            name: 'Select Class',
                            value: 'select Class'
                        });
                        options.sync();
                        for (var c = 0; c < sData.length; c++) {
                            var crn = sData[c].data.crn;
                            var meetings = sData[c].data.meeting;
                            for (var i = 0; i < meetings.length; i++) {
                                var storeDate = meetings[i].startDate;
                                var storeEndDate = meetings[i].endDate;
                                var storeDateValue = Ext.Date.parse(storeDate, "m-d-Y");
                                var storeNewDateValue = (storeDateValue.getDate() + '-' + [storeDateValue.getMonth() + 1] + '-' + storeDateValue.getFullYear());
                                var endDate = Ext.Date.parse(storeEndDate, "m-d-Y");
                                var newEndDate = (endDate.getDate() + '-' + [endDate.getMonth() + 1] + '-' + endDate.getFullYear());
                                //get the month and day
                                var dateValueMonth = dateValue.getMonth() + 1;
                                var dateValueDay = dateValue.getDate();
                                var storeSDateMonth = storeDateValue.getMonth() + 1;
                                var storeSDateDay = storeDateValue.getDate();
                                var storeEDateMonth = endDate.getMonth() + 1;
                                var storeEDateDay = endDate.getDate();

                                if ((dateValueMonth > storeSDateMonth || (dateValueMonth == storeSDateMonth && dateValueDay >= storeSDateDay)) && (dateValueMonth < storeEDateMonth || (dateValueMonth == storeEDateMonth && dateValueDay <= storeEDateDay))) {
                                    var period = meetings[i].period;
                                    //Split the periods from list
                                    var periodList = new Array();
                                    periodList = period.split(':');
                                    var day = '';
                                    switch (dateValue.getDay()) {
                                        case 0:
                                            day = "SUN";
                                            break;
                                        case 1:
                                            day = "MON";
                                            break;
                                        case 2:
                                            day = "TUE";
                                            break;
                                        case 3:
                                            day = "WED";
                                            break;
                                        case 4:
                                            day = "THU";
                                            break;
                                        case 5:
                                            day = "FRI";
                                            break;
                                        case 6:
                                            day = "SAT";
                                            break;
                                    }
                                    //                            var day = this.getWeek(dateValue);
                                    for (var j = 0; j < periodList.length; j++) {
                                        if (day == periodList[j]) {
                                            var beginTime = meetings[i].beginTime;
                                            beginTime = beginTime.replace(':', '');
                                            if ((beginTime.search('AM')) != -1) {
                                                beginTime = beginTime.replace('AM', '');
                                            } else {
                                                beginTime = beginTime.replace('PM', '');
                                                if (new Number(beginTime) < 1200)
                                                    beginTime = new Number(beginTime) + 1200;
                                            }
                                            var endTime = meetings[i].endTime;
                                            endTime = endTime.replace(':', '');
                                            if ((endTime.search('AM')) != -1) {
                                                endTime = endTime.replace('AM', '');
                                            } else {
                                                endTime = endTime.replace('PM', '');
                                                if (new Number(endTime) < 1200)
                                                    endTime = new Number(endTime) + 1200;
                                            }
                                            options.add({
                                                name: sData[c].data.courseTitle + ' (' + crn + ' ' + meetings[i].beginTime + '-' + meetings[i].endTime + ')',
                                                value: crn + '-' + beginTime + '-' + endTime + '-' + sData[c].data.termCode
                                            });
                                            options.sync();
                                        }
                                    }
                                }
                            }
                        }
                        Ext.getCmp('class').setValue('select Class')
                    }
                }
            }
        }, {
            xtype: 'combobox',
            fieldLabel: 'Class',
            name: 'class',
            width: 500,
            id: 'class',
            allowBlank: false,
            displayField: 'name',
            valueField: 'value',
            emptyText: 'Select Class',
            store: 'attClassList',
            editable: false,
            listeners: {
                change: function(combo, newValue, oldValue, eOpts) {
                    var date = Ext.getCmp('classRosterDate').getValue();
                    if (newValue != null && newValue != '' && newValue != 'select class') {
                        var selectedValue = newValue.split('-');
                        var attDate;
                        if (date == null || date == '') {
                            attDate = '';
                        } else {
                            date = new Date(date);
                            attDate = ([date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1] + '/' + [date.getDate() < 10 ? '0' + date.getDate() : date.getDate()] + '/' + date.getFullYear());
                        }

                        var store = Ext.getStore('updateModuleStore');
                        store.getProxy().url = webserver + 'generateCode';
                        store.getProxy().headers = {
                            Authorization: authString
                        };
                        store.getProxy().extraParams = {
                            termCode: selectedValue[3],
                            crn: selectedValue[0],
                            instructorId: loginId,
                            attDate: Ext.Date.format(date, 'n/j/Y'),
                            startTime: selectedValue[1], //'1500',
                            endTime: selectedValue[2]
                        };
                        store.getProxy().afterRequest = function() {
                            var proxyStore = Ext.getStore('attendanceInfoStore');
                            if (proxyStore.getCount() > 0) {
                                proxyStore.removeAll();
                                proxyStore.removed = [];
                            }
                            proxyStore.getProxy().headers = {
                                Authorization: authString
                            }
                            proxyStore.getProxy().url = webserver + 'getAttendanceReport';
                            proxyStore.getProxy().extraParams = {
                                startDate: attDate,
                                endDate: attDate,
                                studentId: '',
                                id: loginId,
                                termCode: selectedValue[3],
                                crn: selectedValue[0],
                                classStartTime: selectedValue[1],
                                classEndTime: selectedValue[2]
                            };
                            proxyStore.load();
                        }
                        store.load();
                    } else {
                        var store = Ext.getStore('attendanceInfoStore');
                        store.removeAll();
                    }
                }
            }
        }, {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'right',
                pack: 'end'
            },
            border: false,
            items: [{
                xtype: 'button',
                frame: true,
                margin: 5,
                minWidth: 75,
                id: 'selectAll',
                text: 'Select All',
                action: 'selectAll',
                name: 'selectAll'
            }, {
                xtype: 'button',
                frame: true,
                margin: 5,
                minWidth: 75,
                id: 'reset',
                text: 'Reset',
                action: 'reset',
                name: 'reset'
            }, {
                xtype: 'button',
                frame: true,
                margin: 5,
                minWidth: 75,
                id: 'updateAttendance',
                text: 'Update',
                action: 'update',
                name: 'update'
            }]
        }, {
            xtype: 'gridpanel',
            columnLines: true,
            id: 'moduleGrid',
            store: 'attendanceInfoStore',
            maxHeight: 340,
            minHeight: 340,
            //            margin:'20',
            style: {
                borderStyle: 'solid',
                borderWidth: '1px'
            },
            columns: [{
                header: 'Student Name',
                dataIndex: 'name',
                flex: 2
            }, {
                xtype: 'gridcolumn',
                header: 'Attended',
                dataIndex: 'attIndicator',
                renderer: function(val) {
                    if (selectAll == true || val.toLowerCase() == 'y') return '<input type="checkbox" checked=true>';
                    else return '<input type="checkbox" >';
                }
            }],
            features: [{
                ftype: 'grouping',
                groupHeaderTpl: '{[values.groupValue ? values.groupValue : ""]}'
            }],
        }]

    }
});