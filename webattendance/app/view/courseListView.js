Ext.define('login.view.courseListView', {
    extend:'Ext.form.FieldSet',
    alias:'widget.courseListView',
    autoScroll : true,
    overflowX:'hidden',
    overflowY:'auto',
    layout:{
        align:'center',
        type:'hbox'
    },
    bodyStyle:{
        padding : '10 0 0 20'
    },
    items: [{
        xtype: 'gridpanel',
        columnLines: true,
        store: 'attCourseList',
        maxHeight:300,
        minHeight:300,
        //            margin:'20',
        style: {            
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        columns: [           
        {
            header: 'Course Title',
            dataIndex: 'courseTitle',    
            hideHeaders:true,
            flex: 1
        }],
        listeners:{
            itemclick:function(viewRef, record, item, index, e, eOpts ){
                var doc='';
                doc='<table><tr style="line-height: 1.5"><td width="40%"><h2>Course Title</h2></td><td><h2>:</h2></td><td align="left">'+record.data.courseTitle+' ('+record.data.crn+')</td></tr>'+
                '<tr style="line-height: 1.5"><td  width="50%"><h2>Course Id</h2></td><td><h2>:</h2></td><td align="left">'+record.data.subject+' '+ record.data.courseNumber + '</td></tr>'+
                '<tr style="line-height: 1.5"><td  width="50%"><h2>Subject</h2></td><td><h2>:</h2></td><td align="left">'+record.data.description+'</td></tr>' +
                '<tr style="line-height: 1.5"><td  width="50%"><h2>Instructor</h2></td><td><h2>:</h2></td><td align="left">'+record.data.faculty+'</td></tr>' +
                '<tr style="line-height: 1.5"><td  width="50%"><h2>Term</h2></td><td><h2>:</h2></td><td align="left">'+record.data.termDescription+'</td></tr>' +
                '<tr style="line-height: 1.5"><td  width="50%"><h2>Department</h2></td><td><h2>:</h2></td><td align="left">'+record.data.department+'</td></tr>' +
                '<tr style="line-height: 1.5"><td  width="50%"><h2>College</h2></td><td><h2>:</h2></td><td align="left">'+record.data.college+'</td></tr>' +
                '<tr style="line-height: 1.5"><td  valign="top" width="50%"><h2>Level</h2></td><td valign="top"><h2>:</h2></td><td align="left"><p>'+record.data.level+'</p></td></tr>' +
                '<tr style="line-height: 1.5"><td  width="50%"><h2>Campus</h2></td><td><h2>:</h2></td><td align="left">'+record.data.campus+'</td></tr>' +
                '<tr style="line-height: 1.5"><td valign="top"  width="50%"><h2>Schedule</h2></td><td valign="top"><h2>:</h2></td><td align="left">';
                var meeting=record.data.meeting;
                var doc=doc+'<table>';
                for(var i=0;i<meeting.length;i++){
                    doc=doc+
                    '<tr><td>'+ meeting[i].startDate+ ' - ' + meeting[i].endDate+'<br />' + meeting[i].beginTime +' - ' +meeting[i].endTime+'<br />' + meeting[i].period+'<br />' + 'Building: '+meeting[i].bldgCode+'<br />' +' Room: '+meeting[i].roomCode+'<br />Type: '+meeting[i].scheduleDesc+'<br /><hr>' + '</td></tr>';
                }
                doc=doc+'</table>'
                var viewRef=Ext.getCmp('courseDoc');
                viewRef.remove(label);
                label=viewRef.add({
                    padding:'0 0 0 5',
                    html:doc
                });
            }
        },
        flex:1
    },{
        xtype:'panel',
        id:'courseDoc',
        height:'100px',
        padding:'0 0 0 20',
        flex:2,
        layout:{
            align:'left',
            type:'vbox'
        },
        minHeight:'300',
        maxHeight:'300',
        defaultType: 'label',
        autoScroll : true,
        overflowX:'hidden',
        overflowY:'auto',
        items:[
        {
            
        }
        ]
    }]

});