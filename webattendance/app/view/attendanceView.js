Ext.define('login.view.attendanceView',{
    extend:'Ext.tab.Panel',
    alias: 'widget.attendanceView',
    bodyStyle: "background-image: url('images/app_bg_460_320.jpg');background-repeat:no-repeat;background-position:center;background-size:100%;",
    title:'<img src="images/Header.png" height="70"/>',
    header:true,
    titleAlign:'center',
    //    cls:'logo',
    minTabWidth:'130',
    layout: 'fit',
    id:'attendanceView',    
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'tbfill' // fills left side of tbar and pushes following items to be aligned to right
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Terms',
                    name: 'termCC',
                    id: 'termCC',
                    store: 'termListStore',
                    displayField: 'description',
                    valueField: 'code',
                    triggerAction: 'all',
                    listeners:{
                        change:function(field, newValue, oldValue, eOpts) {                            
                            var controllerRef  =   myApp.getController('login.controller.attendanceController');
                            controllerRef.loadCourses(newValue);
                        }
                    }
                },
                {
                    xtype: 'button',
                    action:'logout',
                    margin: 4,
                    minWidth: 50,           
                    text: 'Logout'
                },
                {
                    xtype:'tbspacer',
                    width:'45'
                }]
            //    },{
            //	   dock: 'bottom',
            //	   html:'<img src="images/Footer.png" width="100%" />'
        }],

    items:[
        {
            id:'viewClasses',
            title: 'View Classes',
            xtype:'courseListView'
        },
        {
            id:'classRoster',
            title: 'Roster By Class',
            xtype:'classRosterView'
        }
    ]
    
        
});
