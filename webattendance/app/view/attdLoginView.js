Ext.define('login.view.attdLoginView',{
    extend: 'Ext.form.Panel',
    alias: 'widget.attdLoginView',
    //    bodyStyle: "background-image:url('images/background.png')",
    activeItem: 'attdLoginView',
    layout: {
        type: 'table',
        columns: 1,
        tableAttrs:{
            style: {
                width: '100%',
                height:'100%'
            },
            align: 'center',
            valign: 'middle'
        },
        trAttrs:{
            align: 'center'
        }
    },   
    cls:'logo',
    items:[{
        formId: 'loginForm',
        
        xtype: 'form',
        border:false,
        items:[
        {
            html: '<img height="200" src="images/TTU.png">',
            align: 'center',
            height:250,
            border:false
        },
        {
            xtype:'form',
            border:false,
            //            bodyStyle: "background-image:url('images/background.png')",
            items:[
            {                
                //                bodyStyle: "background-image:url('images/background.png')",
                //                width:'300',
                formId: 'admin-loginform',
                xtype: 'form',
                border:false,
                items:[
                {
                    xtype:'textfield',
                    fieldLabel:'T#',
                    id:'username',
                    allowBlank:false,
                    enableKeyEvents :true,                    
                    listeners : {
                        'render' : function(cmp) {
                            cmp.getEl().on('keypress', function(e) {
                                var controllerRef  =   myApp.getController('login.controller.attendanceController');
                                controllerRef.onUsernameKeypress(e);   
                            });
                        }
                    }
                },
                {
                    xtype:'textfield',
                    inputType:'password',
                    fieldLabel:'Pin',
                    id:'password', 
                    name:'password',
                    allowBlank:false,
                    enableKeyEvents :true,
                    listeners : {                        
                        'render' : function(cmp) {
                            cmp.getEl().on('keypress', function(e) {
                                var controllerRef  =   myApp.getController('login.controller.attendanceController');
                                controllerRef.onPasswordKeypress(e);   
                            });
                        }
                    }
                },
                {
                    xtype:'button',
                    frame:true,
                    cls:'buttonClr',
                    margin: 4,
                    minWidth: 50,
                    text:'Login',
                    action:'signin',
                    name:'signin',
                    keys: [
                    {
                        key: [Ext.EventObject.ENTER], 
                        handler: function() {
                            Ext.Msg.alert("Alert","Enter Key Event !");
                        }
                    }
                    ]
                }
                                
                ]
            }
            ]
        }
        ]

    }]
});