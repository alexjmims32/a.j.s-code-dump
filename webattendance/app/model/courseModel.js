Ext.define('login.model.courseModel', {
    extend:'Ext.data.Model',

    config:{

        fields : [ {
            name : 'courseNumber',
            type : 'string'
        },{
            name : 'courseTitle',
            type : 'string'
        },{
            name : 'actionDesc',
            type : 'string'
        },{
            name : 'actionCode',
            type : 'string'
        },{
            name : 'subject',
            type : 'string'
        },{
            name : 'description',
            type : 'string'
        },{
            name : 'level',
            type : 'string'
        },{
            name : 'college',
            type : 'string'
        },{
            name : 'period',
            type : 'string'
        },{
            name : 'roomCode',
            type : 'string'
        },{
            name : 'termCode',
            type : 'string'
        },{
            name : 'termDescription',
            type : 'string'
        },{
            name : 'scheduleType',
            type : 'string'
        },{
            name : 'department',
            type : 'string'
        },{
            name : 'campus',
            type : 'string'
        },{
            name : 'crn',
            type : 'string'
        },{
            name : 'scheduleTime',
            type : 'string'
        },{
            name : 'faculty',
            type : 'string'
        },{
            name : 'lectureHours',
            type : 'string'
        },{
            name : 'credits',
            type : 'string'
        },{
            name : 'varCredit',
            type : 'string',
            convert: function(value, record) {
                if(value=='Y' || value==true){
                    return true;
                }
                else{
                    return false;
                }
            }
        },{
            name : 'varCreditHrs',
            type : 'string'
        },{
            name : 'seqNo',
            type : 'string'
        },{
            name : 'linkId',
            type : 'string'
        },{
            name : 'maxCredit',
            type : 'string'
        },{
            name : 'minCredit',
            type : 'string'
        },{
            name : 'meeting',
            type : 'array'
        },{
            name: 'statusMessage',
            type:'string'
        },{
            name: 'dropFlag',
            type:'string'
        },{
            name: 'facultyEmail',
            type:'string'
        }
        ]
    }
});