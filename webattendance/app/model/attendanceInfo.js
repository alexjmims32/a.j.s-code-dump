Ext.define('login.model.attendanceInfo', {
    extend:'Ext.data.Model',

    fields:[
    {
        name : 'termCode'
    },{
        name : 'studentId'
    },{
        name : 'name'
    },{
        name : 'crn'
    },{
        name : 'email'
    },{
        name : 'attIndicator'
    },{
        name:'attGroup',
        convert: function(value, record) {
            var att = record.get('attIndicator');
            if(att=='Y'){
                return 'Attended'
            }else{
                return 'Absent'
            }
        }
    },{
        name : 'code'
    },{
        name : 'date',
        convert: function(value, record) {
            if(value!=null && value!=''){
                value = value.split('-',3);
                var date = new Date(value[0], value[1]-1, value[2]);   
                return Ext.Date.format(date, 'F j, Y');
            }
        }
    },{
        name : 'count'
    },{
        name : 'level'
    },{
        name : 'grade'
    }
    ]
})