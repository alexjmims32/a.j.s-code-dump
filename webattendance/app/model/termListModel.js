Ext.define('login.model.termListModel', {
    extend:'Ext.data.Model',

    fields:[{
        name : 'code',
        type : 'string'
    },{
        name : 'description',
        type : 'string'
    }]
})