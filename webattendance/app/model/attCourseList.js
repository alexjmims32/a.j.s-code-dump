Ext.define('login.model.attCourseList', {
    extend:'Ext.data.Model',

    fields:[
    {
        name:'statusMessage',
        type:'string'
    },{
        name : 'courseTitle',
        type : 'string',
        mapping:'course.courseTitle'
    },{
        name : 'subject',
        type : 'string',
        mapping:'course.subject'
    },{
        name : 'courseNumber',
        type : 'string',
        mapping:'course.courseNumber'
    },{
        name : 'description',
        type : 'string',
        mapping:'course.description'
    },{
        name : 'level',
        type : 'string',
        mapping:'course.level'
    },{
        name : 'college',
        type : 'string',
        mapping:'course.college'
    },{
        name : 'termCode',
        type : 'string',
        mapping:'course.termCode'
    },{
        name : 'termDescription',
        type : 'string',
        mapping:'course.termDescription'
    },{
        name : 'department',
        type : 'string',
        mapping:'course.department'
    },{
        name : 'campus',
        type : 'string',
        mapping:'course.campus'
    },{
        name : 'crn',
        type : 'string',
        mapping:'course.crn'
    },{
        name : 'faculty',
        type : 'string',
        mapping:'course.faculty'
    },{
        name : 'meeting',
        type : 'array',
        mapping:'course.meetingList.meeting'
    }
    ]
})