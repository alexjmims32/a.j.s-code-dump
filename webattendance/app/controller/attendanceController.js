Ext.define('login.controller.attendanceController', {
    extend:'Ext.app.Controller',

    stores:[
    'attdLoginStore',
    'attCourseListStore',
    'attCourseListLocalStore',
    'attClassList',
    'attCourseList',
    'attendanceInfoStore',
    'updateModuleStore',
    'termListStore'
    ],

    models:[
    ],

    views:[
    'attdLoginView',
    'attendanceView',
    'classRosterView',
    'courseListView'
    ],

    init:function () {
        this.control({
            'attdLoginView button[action=signin]':{
                click:this.loginSubmit           
            },
            'attendanceView button[action=logout]':{
                click:this.processLogout
            },
            'classRosterView button[action=selectAll]':{
                click:this.selectAllStudents
            },
            'classRosterView button[action=update]':{
                click:this.updateAttendance
            },
            'classRosterView button[action=reset]':{
                click:this.resetStudents
            }
        });
    },   
    processLogout : function () {
        Ext.getCmp('username').reset();
        Ext.getCmp('password').reset();
        window.open('index.html','_self');
    },
            
    onUsernameKeypress:function(e){
        if (e.getKey() == e.ENTER) {
            var username = Ext.getCmp('username').getValue();
            if(username==''){
                Ext.getCmp('username').focus();
            }else{
                Ext.getCmp('password').focus();
            }
        }
    },
            
    onPasswordKeypress:function(e){
        if (e.getKey() == e.ENTER) {
            var username = Ext.getCmp('username').getValue();
            var password = Ext.getCmp('password').getValue();
            if (username == '' || password == '') {            
                Ext.Msg.alert('Login Error', 'Please provide T# and Pin.', function(btn, text){
                    if (btn == 'ok'){
                        if(username==''){
                            Ext.getCmp('username').focus();
                        }else{
                            Ext.getCmp('password').focus();
                        }

                    }
                });
            } else{
                this.loginSubmit();
            }
        }
    },
            
    loginSubmit:function () {
        var attendanceContoller=this;
        var username = Ext.getCmp('username').getValue();
        var password = Ext.getCmp('password').getValue();                  
        var encCred = username + ':' + password
        var encodePassword =Base64.encode(encCred);
        authString = 'Basic ' + encodePassword;        
        if (username == '' || password == '') {            
            Ext.Msg.alert('Login Error', 'Please provide T# and Pin.', function(btn, text){
                if (btn == 'ok'){
                    if(username==''){
                        Ext.getCmp('username').focus();
                    }else{
                        Ext.getCmp('password').focus();
                    }
                
                }
            });
        } else {
            var store = Ext.getStore('attdLoginStore');
            store.getProxy().url = webserver + 'login';
            store.getProxy().headers = {
                Authorization:authString
            }
            store.getProxy().afterRequest = function () {
                attendanceContoller.loginResponseHandler(username);
            }                 
        }
        store.load();
    },
            
    loginResponseHandler:function(username){
        var store = Ext.getStore('attdLoginStore');
        var attendanceContoller=this;
        myMask.hide();                
        if(httpStatus==401){
            var loginView=Ext.getCmp('attdLoginView');
            viewport.layout.setActiveItem(loginView);
        }else
            var status = store.getProxy().getReader().rawData;                
        if (status!=undefined && status.status == 'success') {
            var resRole=status.userInfo.roleCode;
            var roles=resRole.split(',');
            var isFaculty=false;
            for(var i=0;i<roles.length;i++){
                if(roles[i] == 'FACULTY'){
                    isFaculty=true;
                }
            }
            loginId=username.toUpperCase();

            if(isFaculty==true){
                task = Ext.create('Ext.util.DelayedTask', function() {
                    Ext.Msg.show({
                        msg: 'Session Expired... Please relogin.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox',
                        fn: function(btn) {
                            if (btn == 'ok') {
                                Ext.getCmp('username').reset();
                                Ext.getCmp('password').reset();
                                window.open('index.html','_self');
                            }
                        }
                    });
                }, this);
                task.delay(1200000);
                attendanceContoller.loadCourses();
                var options=Ext.getStore('attClassList');
                options.removeAll();
                options.sync();
                var menu=Ext.widget('attendanceView');
                viewport.add(menu);
                viewport.layout.setActiveItem(menu);                              
                console.log('USER LOGIN SUCCESS');
            }else{
                Ext.Msg.alert('  ','You don\'t have an access to Attendance Module.');
            }                                            
        }else{
            Ext.Msg.alert('  ','You are not authorized to access Attendance Module.');
        }
    },
            
    loadCourses:function(termCode){
        var attendanceContoller=this;
        var coursesStore = Ext.getStore('attCourseList');
        coursesStore.getProxy().headers={
            Authorization: authString
        };
        coursesStore.getProxy().url=webserver + 'facultyCourses';
        coursesStore.getProxy().extraParams={
            studentId: loginId,
            termCode: termCode
        }; 
        coursesStore.getProxy().afterRequest = function () {
            if(termCode==null || termCode==''){
                attendanceContoller.coursesResponseHandler();
            }
        }
        coursesStore.load();
    },
            
    coursesResponseHandler:function(){
        var coursesStore = Ext.getStore('attCourseList');
        var termsStore = Ext.getStore('termListStore');
        termsStore.getProxy().headers={
            Authorization: authString
        };
        termsStore.getProxy().url=webserver + 'getFacultyTerms';
        termsStore.getProxy().extraParams={
            facultyId: loginId,
        };            
        termsStore.load();
        var code = coursesStore.getAt(0).get('termCode');
        Ext.getCmp('termCC').setValue(code);
    },
    
    updateAttendance:function(){
        var grid = Ext.getCmp('moduleGrid');
        var controller=this;

        var gridItems = grid.items.items[0].all.elements;

        var stdAttendance = '';
        var proxyStore = Ext.getStore('attendanceInfoStore');
        for(i=0;i<gridItems.length;i++){
            var node = gridItems[i];
            var attd=  node.cells[1].firstChild.firstChild.checked;
            var rec=proxyStore.getAt(i);
            if(attd==true){
                attd='Y';
            }else{
                attd='N';
            }

            stdAttendance += rec.data.studentId +'-' + attd +','+rec.data.code+';';
        }
        var attendanceInfo=proxyStore.getAt(0);
        var selected=Ext.getCmp('class').getValue();
        var selectedValue=selected.split('-');
        var store = Ext.getStore('updateModuleStore');
        store.getProxy().url = webserver +'updateAttendance';
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().extraParams={
            values:stdAttendance,
            termCode: selected[3],
            studentId:loginId,
            crn: selected[0],
            attendanceCode:attendanceInfo.data.code,
            overWriteBy:loginId,
            overComments:null
        };
        store.getProxy().afterRequest = function () {
            var store1 = Ext.getStore('updateModuleStore');
            var status = store1.getProxy().getReader().rawData;
            if (status.status == 'SUCCESS') {
                Ext.Msg.alert('  ', 'Attendance updated successfully.',controller.resetStudents);
            }else{
                Ext.Msg.alert('   ', 'Attendance update failed.');
            }
        }
        store.load();
    },
    
    selectAllStudents:function(){
        var selected=Ext.getCmp('class').getValue();
        if(selected !=null && selected !='select Class'){
            selectAll=true;
            Ext.getStore('attendanceInfoStore').load();
        }
    },
    
    resetStudents:function(){
        var selected=Ext.getCmp('class').getValue();
        if(selected !=null && selected !='select Class'){
        selectAll=false;
        Ext.getStore('attendanceInfoStore').load();
        }
    }
});