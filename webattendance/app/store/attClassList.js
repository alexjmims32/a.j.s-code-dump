Ext.define('login.store.attClassList', {
    extend: 'Ext.data.Store',
    fields:['name', 'value'],
    alias: 'attClassList',

    proxy: {
        type: 'localstorage',
        id:'attClassList'
    }
//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json'
//        }
//    }
});