Ext.define('login.store.attendanceInfoStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.attendanceInfo',
    alias: 'attendanceInfoStore',

    autoLoad: false,
    groupField: 'attGroup',
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'roaster',
            totalProperty: 'numrows'
        }
    }

});