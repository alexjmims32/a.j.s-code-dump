Ext.define('login.store.attdLoginStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.loginModel',
    alias: 'attdLoginStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'privilegesList.privilege'
        }
    }
});