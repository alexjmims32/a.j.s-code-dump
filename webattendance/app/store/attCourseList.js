Ext.define('login.store.attCourseList', {
    extend: 'Ext.data.Store',
    model: 'login.model.attCourseList',
    alias: 'attCourseList',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'registeredCourseList.registeredCourse',
            totalProperty: 'numrows'
        }
    }

});