Ext.define('login.store.attCourseListStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.courseList',
    alias: 'attCourseListStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'registeredCourseList.registeredCourse'
        }
    }

});
