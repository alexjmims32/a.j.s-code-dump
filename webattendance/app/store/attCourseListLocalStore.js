Ext.define('login.store.attCourseListLocalStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.courseList',
    alias: 'attCourseListLocalStore',

    autoLoad: true,

    proxy: {
        type: 'localstorage',
        id:'attCourseListLocalStore'
    }

});