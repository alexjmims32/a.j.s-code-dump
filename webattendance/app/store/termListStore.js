Ext.define('login.store.termListStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.termListModel',
    alias: 'termListStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'registerTermList'
        }
    }

});