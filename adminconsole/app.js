var viewport;
var myMask;
var authString;
var loginClient;
var loginId;
var httpStatus;
var campusCode = 'TTU';
//var clientId='001';
var flag = '';
var pageSize;
var selectRecord;
var serviceReq = 'N';
var status = 'SUCCESS';
var refreshItem = 'TABLE';
var selectIndex;
var userAddFlag = true;
var source = 'AdminConsole';
var notifWebserver = 'https://ttumobileappdev.tntech.edu:8444/entservices/json/'
var webserver = 'http://qa.cloud.n2nservices.com:8080/ttuqaencore/json/'

Ext.Loader.setConfig({
	enabled: true
});
Ext.application({
	//    requires: ['Ext.container.Viewport'],
	name: 'login',
	appFolder: 'app',

	views: [
		'loginView',
		'welcomeView',
		'updateConfigView',
		'configListGridView',
		'addConfigView',
		'clientDetailsView',
		'tabView',
		'addNotificationView',
		'notificationListView',
		'mapView',
		'mapListView',
		'emergencyContactsView',
		'emergencyContactView',
		'addEmergencyContactView',
		'addMapView',
		'roleView',
		'studentConfigView',
		'moduleListView',
		//    'feedView',
		'feedTabView',
		'addFeedLinkView',
		'addFeedModuleView',
		'editMenuView',
		'editLinkView',
		'editModuleView',
		'userListView',
		'addUserView',
		'userDetailsView',
		'editUserView',
		'passwordView',
		'updateUserView',
		'contactEditView',
		'feedbackListView',
		'editFeedbackView',
		'editFeedbackMenu',
		'helpView',
		'searchPopupView',
		'notificationSearchListView',
		'notificationSearchView',
		'feedTrackerView',
		'trackerTabView',
		'updateMapView',
		'updateContactView',
		'createContactView',
		'createMapView',
		'createUserView',
		'customTitleView',
		'reportListView',
		'addReportView',
		'createReportView',
		'metaDataView',
		'createMetaView',
		'tableListView',
		'columnListView',
		'commentsEditView',
		'columnEditMenu',
		'imageUploadForm',
		'termAndConditionView'
	],

	controllers: [
		'loginController',
		'configUpdateController',
		'welcomCntroller',
		'addConfigController',
		'clientListController',
		'addClientController',
		'notificationController',
		'emergencyController',
		'mapController',
		'studentConfigController',
		'feedController', 'userController', 'feedbackController',
		'helpController',
		'reportController',
		'metaDataController',
		'fileUploadController'
	],


	launch: function() {

		// Create the vieport and store the reference in global variable
		viewport = Ext.create('Ext.Viewport', {
			alias: 'viewPort',
			layout: 'card'
		});

		// Add loginView (first one) to viewport
		viewport.add(Ext.widget('loginView'));
		Ext.Ajax.timeout = 60000;

		// invokes before each ajax request
		Ext.Ajax.on('beforerequest', function() {
			// showing the loading mask
			myMask = new Ext.LoadMask(viewport, {
				msg: "Please wait..."
			});
			myMask.show();
		});
		Ext.Ajax.on('requestcomplete', function() {
			httpStatus = '200';
			// hidding the loading mask
			myMask.hide();
		});

		// invokes if exception occured
		Ext.Ajax.on('requestexception', function(conn, response, options, eOpts) {

			if (response.status == 401) {
				httpStatus = '401';
				Ext.Msg.alert(' ', 'Invalid username or password');
				myMask.hide();
			} else {
				Ext.Msg.alert(' ', '<center>Server error has occured.Please try again</center>');
				myMask.hide();
			}

			myMask.unmask();
		});
	}

});