Ext.define('login.view.columnEditMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.columnEditMenu',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editcolumnComments',
                iconCls: 'icon-edit'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});