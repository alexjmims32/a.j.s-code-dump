Ext.define('login.view.notificationListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.notificationListView',
    store:'notificationStore',
    title:'<font size="3"><b>Notifications</b></font>',
    titleAlign:'center',
    id:'list',    
    scrollable: 'both',
    minWidth: 300,
    columnLines: true,    
  
    margin:'20',
    style: {
        //        borderColor: 'green',
        borderStyle: 'solid',
        borderWidth:'1px',
        overflowX:'hidden',
        overflowY:'auto'
        
        
    },
    columns:[ 
    {
        header:'User Name',
        dataIndex:'userName',
        flex:1,
        cls:'gridClr'
    }
    ,
    {
        header:'Message',
        dataIndex:'message',
        flex:1,
        cls:'gridClr'
    },   
    {
        header:'Delivery Method',
        dataIndex:'deliveryMethod',
        flex:1,
        cls:'gridClr'

    },  
    {
        header:'Type',
        dataIndex:'type',
        flex:1,
        cls:'gridClr'
    },
    
    {
        header:'Due Date',
        dataIndex:'dueDate',
        flex:1,
        cls:'gridClr'
    },
    

    ],
    
   
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,

        items:[
        {
            xtype:'button',
            cls:'buttonClr',
            minWidth: 50,
            margin: 4,
            text:'Add',
            action:'ADD',
            buttonAlign:'right'
        },      
        ]
    }
    ]  

})
;