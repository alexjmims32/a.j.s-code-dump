Ext.define('login.view.emergencyContactsView', {
    extend:'Ext.grid.Panel',
    columnLines: true,
    alias:'widget.emergencyContactsView',
    store:'emergencyContactStore', 
    scrollable: 'both',
    padding:'0 0 28 0',
    minWidth: 300,
    margin:'20',
    style: {
        borderStyle: 'solid',
        borderWidth:'1px',
        maxHeight:420,
        overflowX:'hidden',
        overflowY:'auto'
    },
    columns:[
    {
        header:'Name',
        dataIndex:'name',
        flex:2,
        cls:'gridClr'
    },

    {
        header:'Phone',
        dataIndex:'phone',
        flex:1,
        cls:'gridClr'
    },
    {
        header:'Address',
        dataIndex:'address',
        flex:3,
        cls:'gridClr'
    },
    {
        header:'Email',
        dataIndex:'email',
        flex:2,
        cls:'gridClr'
    },
    {
        header:'Image Url',
        dataIndex:'picture_url',
        //        flex:,
        cls:'gridClr'
    },
    {
        header:'Comments',
        dataIndex:'comments',
        flex:3,
        cls:'gridClr'
    //        hidden:true
    },
    {
        header:'Category',
        dataIndex:'category',
        flex:2,
        renderer: function(val) {
            if(val=='IMPNUM'){
                return 'Important Phone Numbers';
            }else if(val=='EMR'){
                return 'Emergency Contacts';
            }
        }
    }
    ]
    ,
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,
        items:[
        {
            xtype:'button',
            minWidth: 50,
            margin: 4,
            text:'Add',
            cls:'buttonClr',
            action:'ADD',
            buttonAlign:'right'
        },
        
        {
            xtype:'tbspacer',
            width:'970'
        }
        ]
    }
    ]
});