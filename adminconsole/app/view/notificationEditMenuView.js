Ext.define('login.view.notificationEditMenuView', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.notificationEditMenuView',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'View',
                id:'editNotif',
                iconCls: 'icon-edit'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});