Ext.define('login.view.addUserView',{
    extend:'Ext.panel.Panel',    
    alias:'widget.addUserView',
    scrollable: 'both',
    minWidth: 300,
 
    layout:{
        align:'center',
        type:'table',
        columns: 1,
        tableAttrs: {
            width:'500',
            align:'center'
        }
    },   
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Add User</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'UserName',
        name: 'addUsername',
        id: 'addUsername',
        allowBlank: false
    },
    {
        fieldLabel: 'First Name',
        name: 'userFname',
        id: 'userFname',
        allowBlank: false
    },
    {
        fieldLabel: 'Last Name',
        name: 'userLname',
        id: 'userLname',
        allowBlank: false
    },  
    {
        xtype: 'combobox',
        fieldLabel: 'Active',
        name: 'userActiveFlag',
        id: 'userActiveFlag',
        allowBlank: false,
        editable: false,
        displayField: 'description',
        valueField: 'code',
        store: 'activeFlagStore'
    },
    {
        fieldLabel: 'Password',
        name: 'userPassword',
        id: 'userPassword',
        inputType:'Password',
        allowBlank: false
    },
    {
        xtype: 'checkboxgroup',
        id:'check',
        fieldLabel: 'Role',
        // Arrange checkboxes into one column, distributed vertically
        columns: 1,
        vertical: true,
        items: [
        {
            boxLabel: 'Notification', 
            name: 'notification', 
            inputValue: '1',
            id:'notification'
        },
        {
            boxLabel: 'Feed', 
            name: 'feed', 
            inputValue: '2',
            id:'feed'        
        },
        {
            boxLabel: 'Map', 
            name: 'map', 
            inputValue: '3',
            id:'map'
        },
        {
            boxLabel: 'Emergency Contact', 
            name: 'contact', 
            inputValue: '4',
            id:'contact'
        },
        
        {
            boxLabel: 'Help About', 
            name: 'helpAbout', 
            inputValue: '5',
            id:'helpAbout'
        },
        {
            boxLabel: 'Help Faq', 
            name: 'helpFaq', 
            inputValue: '6',
            id:'helpFaq'        
        },
        {
            boxLabel: 'Feature Visibility', 
            name: 'roles', 
            inputValue: '7',
            id:'roles'
        },
        {
            boxLabel: 'Feedback', 
            name: 'feedback', 
            inputValue: '8',
            id:'feedback'
        },
        {
            boxLabel: 'Terms & Conditions', 
            name: 'aTC', 
            inputValue: '9',
            id:'aTC'
        },
        {
            boxLabel: 'Users', 
            name: 'aUsers', 
            inputValue: '10',
            id:'aUsers'
        }
        
        ]
    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            cls:'buttonClr',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});