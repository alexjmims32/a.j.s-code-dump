Ext.define('login.view.welcomeView',{
    extend:'Ext.panel.Panel',
    //    align:'center',
    //    title:'<center><font size="3"><b>MobEdu Client Configuaration Form</b></font></center>',
    alias:'widget.welcomeView',
    overflowX:'hidden',
    overflowY:'auto',

    layout:{
        align:'center',
        type:'vbox'
    },
    bodyStyle:{
        padding : '10 0 0 20'
    },

    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="4"><b>MobEdu Client Configuration</b></font></center>',
        padding:20

    },
    {
        xtype:'textfield',
        fieldLabel: 'Client',
        labelWidth:180,
        readOnly:true,
        id: 'client',
        name: 'client',
        allowBlank: false
    },{
        fieldLabel: 'DB Hostname',
        labelWidth:180,
        name: 'hostname',
        id: 'hostname',
        allowBlank: true
    },
    {
        fieldLabel: 'Port',
        labelWidth:180,
        name: 'port',
        id: 'port',
        allowBlank: true
    },{
        fieldLabel: 'SID',
        labelWidth:180,
        name: 'sid',
        id: 'sid',
        allowBlank: true
    },
    {
        fieldLabel: 'Username',
        labelWidth:180,
        name: 'username',
        id: 'wusername',
        allowBlank: true
    },{
        fieldLabel: 'Password',
        labelWidth:180,
        inputType:'password',
        name: 'wpassword',
        id: 'wpassword',
        allowBlank: true
    },
    {
        padding:'20 0 0 0',
        fieldLabel: 'LDAP Hostname',
        labelWidth:180,
        name: 'ldapHost',
        id: 'ldapHost',
        allowBlank: true
    },
    {
        fieldLabel: 'Port',
        labelWidth:180,
        name: 'ldapPort',
        id: 'ldapPort',
        allowBlank: true
    },
    {
        fieldLabel: 'Protocal',
        labelWidth:180,
        name: 'Protocal',
        id: 'Protocal',
        allowBlank: true
    },
    {
//        padding:'20 0 0 0',
        fieldLabel: 'Base DN',
        labelWidth:180,
        name: 'baseDN',
        id: 'baseDN',
        allowBlank: true
    },
    {
        fieldLabel: 'UserId',
        labelWidth:180,
        name: 'dnUserID',
        id: 'dnUserID',
        allowBlank: true
    },
    {
        fieldLabel: 'Password',
        labelWidth:180,
        inputType:'password',
        name: 'dnPassword',
        id: 'dnPassword',
        allowBlank: true

    },
    {
        xtype:'label',
        padding:'20 0 10 0',
        html:'<b>VPN</b>'
    },
    {
        fieldLabel: 'VPN Type',
        labelWidth:180,
        name: 'vpnType',
        id: 'vpnType',
        allowBlank: true
    },
    {
        fieldLabel: 'VPN URL/Host',
        labelWidth:180,
        name: 'vpnHost',
        id: 'vpnHost',
        allowBlank: true
    },
    {
        fieldLabel: 'VPN User',
        labelWidth:180,
        name: 'vpnUser',
        id: 'vpnUser',
        allowBlank: true
    },
    {
        fieldLabel: 'VPN Password',
        labelWidth:180,
        inputType:'password',
        name: 'vpnPassword',
        id: 'vpnPassword',
        allowBlank: true
    },
    {
        xtype:'label',
        padding:'20 0 10 0',
        html:'<b>Test/Staging Server</b>'
    },
    {
        fieldLabel: 'OS',
        labelWidth:180,
        name: 'testOs',
        id: 'testOs',
        allowBlank: true
    },
    {
        fieldLabel: 'Hostname',
        labelWidth:180,
        name: 'testHostname',
        id: 'testHostname',
        allowBlank: true
    },
    {
        fieldLabel: 'Port',
        labelWidth:180,
        name: 'testPort',
        id: 'testPort',
        allowBlank: true
    },
    {
        fieldLabel: 'UserId',
        labelWidth:180,
        name: 'testUserId',
        id: 'testUserId',
        allowBlank: true
    },
    {
        fieldLabel: 'Password',
        labelWidth:180,
        inputType:'password',
        name: 'testPassword',
        id: 'testPassword',
        allowBlank: true
    },
    {
        fieldLabel: 'Web Server Software',
        labelWidth:180,
        name: 'testWS',
        id: 'testWS',
        allowBlank: true
    },
    {
        fieldLabel: 'Web Server Software Version',
        labelWidth:180,
        name: 'testWSVersion',
        id: 'testWSVersion',
        allowBlank: true
    },
    {
        fieldLabel: 'Application Server Software',
        labelWidth:180,
        name: 'testAS',
        id: 'testAS',
        allowBlank: true
    },
    {
        fieldLabel: 'Application Server Software Version',
        labelWidth:180,
        name: 'testASVersion',
        id: 'testASVersion',
        allowBlank: true
    },
    {
        xtype:'label',
        padding:'20 0 10 0',
        html:'<b>Contact Information</b>'
    },
    {
        fieldLabel: 'Lead Techinician Name',
        labelWidth:180,
        name: 'leadName',
        id: 'leadName',
        allowBlank: true
    },
    {
        fieldLabel: 'Lead Techinician Phone',
        labelWidth:180,
        name: 'leadPhone',
        id: 'leadPhone',
        allowBlank: true
    },
    {
        fieldLabel: 'Lead Techinician E-mail',
        labelWidth:180,
        name: 'leadEmail',
        id: 'leadEmail',
        allowBlank: true
    },
    {
        fieldLabel: 'DBA Techinician Name',
        labelWidth:180,
        name: 'dbaName',
        id: 'dbaName',
        allowBlank: true
    },
    {
        fieldLabel: 'DBA Techinician Phone',
        labelWidth:180,
        name: 'dbaPhone',
        id: 'dbaPhone',
        allowBlank: true
    },
    {
        fieldLabel: 'DBA Techinician E-mail',
        labelWidth:180,
        name: 'dbaEmail',
        id: 'dbaEmail',
        allowBlank: true
    },
    {
        fieldLabel: 'Notes/Comments',
        labelWidth:180,
        name: 'notes',
        id: 'notes',
        allowBlank: true
    },
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'30',
        border:false,
        align:'middle',
        items:[
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Submit',
            action:'submit',
            name:'submit'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Reset',
            action:'reset',
            name:'reset'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Back',
            action:'back',
            name:'back'
        }
        ]
    }
    ]

});