Ext.define('login.view.notificationSearchByQueryView', {
    extend: 'Ext.panel.Panel',
    alias:'widget.notificationSearchByQueryView',
    maxHeight:280,
    autoScroll : true,
    
    margin:'20',        
    style: {
        //        borderColor: 'green',
        borderStyle: 'solid',
        borderWidth:'1px'            
    },   
    config: {
        items: [{
            xtype: 'gridpanel',
            id:'queryGrid',
            store: 'querySearchStore',
            border:false,
            selModel: Ext.create("Ext.selection.CheckboxModel"),
            columns: [           
            {
                header:'Id',
                dataIndex:'id',
                type:'number',
                flex:1
            } ]
                
        }]
    }
});
        