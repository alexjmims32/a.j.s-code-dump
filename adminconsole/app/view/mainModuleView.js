Ext.define('login.view.mainModuleView', {
    extend: 'Ext.panel.Panel',
    alias:'widget.mainModuleView',
    title:'<table height=10><tr><td width=10%><img src="images/n2n_logo.png" height="70" width=550></td><td><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
    autoScroll : true,
    
    config: {

        items: [{

            xtype: 'gridpanel',
            columnLines: true,
            id: 'moduleGrid',
            store: 'mainModuleStore',
            maxHeight:380,
            margin:'20',
            style: {            
                borderStyle: 'solid',
                borderWidth:'1px'            
            },
                     
           
            //            border:false,
            columns: [           
            {
                header: 'Module',
                dataIndex: 'code',               
                flex: 2
            },
            {                
                header: 'Module Description',
                dataIndex: 'description',               
                flex: 2
            },
            {
                header: 'Position',
                dataIndex: 'position',               
                flex: 2
            },
            {
                xtype: 'gridcolumn',
                header: 'Enabled',
                dataIndex: 'showByDefault',              
                renderer: function(val) {
                    if(val.toLowerCase() == 'y') return '<input type="checkbox" checked=true>';
                    else return '<input type="checkbox" >';
                }               
            },
            {
                xtype: 'gridcolumn',
                header: 'Authorization',
                dataIndex: 'authRequired',              
                renderer: function(val) {
                    if(val.toLowerCase() == 'y') return '<input type="checkbox" checked=true>';
                    else return '<input type="checkbox" >';
                }               
            }
            ]
        }, {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'center',    
                pack: 'center'
            },
            padding: '20',
            border: false,
            items: [
            {
                xtype: 'button',
                frame: true,
                margin: 4,            
                minWidth: 50,
                text: 'Submit',
                action: 'submit',
                name: 'submit'
            }
            ]
        }]

    }
});