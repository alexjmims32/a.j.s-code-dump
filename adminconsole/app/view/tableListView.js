Ext.define('login.view.tableListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.tableListView',
    store:'metaDataTableStore',    
    titleAlign:'center',
    padding:'0 0 40 0',
    bodyBorder:false,

    columns:[
    {
        header:'Table',
       dataIndex:'name',
        flex:1
    },

    {
        header:'Comments',
        dataIndex:'comments',
        flex:2
    }
    ]
    
    
});