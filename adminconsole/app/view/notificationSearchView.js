Ext.define('login.view.notificationSearchView', {
    extend:'login.view.customTitleView',
    alias:'widget.notificationSearchView',
    titleAlign:'center',
    layout:{
        type:'table',
         
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    defaults: {
        autoScroll: true
    },
    items:[
    {
        margin:'20',
        style: {
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'searchPopupView',
        autoScroll: false,
        flex:1
    },{
        xtype:'notificationSearchListView',
        
        autoScroll: true,
        flex:2
    }
    ],
    dockedItems:[
    {
        xtype:'panel',
        dock:'bottom',            
        layout:{
            type:'hbox',
            pack:'center'
        },
        padding:'5 0 5 10',
        border:false,
        items:[  
        //        {
        //            xtype:'button',
        //            margin: 4,
        //            minWidth: 50,
        //            text:'Clear',
        //            action:'clear',
        //            buttonAlign:'right',
        //            cls:'buttonClr'
        //        },             
        {
            xtype:'button',
            margin: 4,
            minWidth: 50,
            text:'Submit',
            cls:'buttonClr',
            action:'submit',
            buttonAlign:'right'
        },
        {
            xtype:'tbspacer',
            width:'25'
        },
        {
            xtype:'button',
            margin: 4,
            minWidth: 50,
            text:'Back',
            action:'back',
            cls:'buttonClr',
            buttonAlign:'right'
        }
       
        ]
    }
    ]

        
       
});