Ext.define('login.view.searchPopupView',{
    extend:'Ext.panel.Panel',
    alias:'widget.searchPopupView',
    scrollable: 'both',
    minWidth: 300,
    border:false,
    items:[
    {
        xtype:'label',
        html:'<center><font size="5"  class="headerTitle"><b>Search By Name</b></font></center>',
        padding:20
    },
    {
        border:false,
        items:[                          
        {
            xtype: 'combobox',
            fieldLabel: 'Person Type',
            name: 'notifPT',
            id: 'notifPT',
            value:'All',
            store: 'nameSearchStore',
            displayField: 'description',
            valueField: 'code',
            editable: false                  
        },       
        {
            layout:{
                type:'vbox'            
            },       
            border:false,
            items:[                   
            {
                xtype:'panel',
            
                layout: {
                    type: 'hbox',
                    align: 'center',
                    pack: 'center'
                },
                border:false,           
                items:[             
                {
                    xtype: 'textfield',
                    //                    fieldLabel: 'First Name',
                    emptyText: 'First Name',
                    name: 'notifFName',
                    id: 'notifFName',
                    allowBlank: false                        
                },
                {
                    xtype:'tbspacer',
                    width: 100
                },
                {
                    xtype: 'textfield',
                    //                    fieldLabel: 'Last Name',
                    emptyText: 'Last Name',
                    name: 'notifLName',
                    id: 'notifLName'
                //                    allowBlank: false                        
                },  
                ]             
            }             
            ]             
        }, 
        {
            xtype:'panel',
            layout: {
                type: 'hbox',
                align: 'center',    
                pack: 'center'
            },
            padding: '20',
            border: false,
            items:[     
            {
                xtype:'button',
                frame:true,
                margin: 4,
                minWidth: 50,
                text:'Search',
                cls:'buttonClr',
                action:'find',
                name:'find'
            },
            ]

        }   
        ]
    }
    ]
});
