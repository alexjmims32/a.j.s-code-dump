Ext.define('login.view.userListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.userListView',
    store:'userStore',
    titleAlign:'center',
    autoScroll : true,
    columnLines: true,
    id:'list',   
    padding:'0 0 40 0',    
    bodyBorder:false,
    margin:'20',
    style: {
        borderStyle: 'solid',
        borderWidth:'1px'            
    },
    columns:[
    {        
        header: 'User Name',
        dataIndex:'username',
        flex:1,
        cls:'gridClr'
    } ,
{
        header: 'First Name',
        dataIndex:'firstName',
        flex:1,
        cls:'gridClr'
    },
    {
        header: 'Last Name',
        dataIndex:'lastName',
        flex:1,
        cls:'gridClr'
    },
    {
        header: 'Active',
        dataIndex:'active',
        flex:1,
        cls:'gridClr'
    },
        
    ],
    
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,

        items:[
        {
            xtype:'button',
            margin: 4,            
            minWidth: 50,
            cls:'buttonClr',
            text:'Add',
            action:'ADD',
            id:'userAdd',
            buttonAlign:'right'                        
        },
        
        {
            xtype:'tbspacer',
            width:'970'
        }

        ]
    }
    ]
});