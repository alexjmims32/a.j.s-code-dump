Ext.define('login.view.editFeedbackMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.editFeedbackMenu',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editFeedback',
                iconCls: 'icon-edit'
            },
            {
                xtype: 'menuitem',
                id:'deleteFeedback',
                text: 'Delete',
                iconCls: 'icon-delete'
            },
            {
                xtype: 'menuitem',
                text: 'Move Up',
                id:'moveFeedbackUp',
                iconCls: 'icon-up'
            },
            {
                xtype: 'menuitem',
                id:'moveFeedbackDown',
                text: 'Move Down',
                iconCls: 'icon-down'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});