Ext.define('login.view.editUserView',{
    extend:'Ext.panel.Panel',
    title:'<table height=10><tr><td width=10%><img src="images/n-logo2.png" height="70" width=550></td><td  class="headerTitle"><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
    alias:'widget.editUserView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5"><b>Edit user</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'FirstName',
        name: 'editUserFname',
        id: 'editUserFname',
        allowBlank: true
    },
    {
        fieldLabel: 'LastName',
        name: 'editUserLname',
        id: 'editUserLname',
        allowBlank: true
    },
    {
        fieldLabel: 'UserName',
        name: 'editUsername',
        id: 'editUsername',
        allowBlank: true
    },
    {
        fieldLabel: 'Role',
        name: 'editUserRole',
        id: 'editUserRole',
        allowBlank: true
    },
    {
        fieldLabel: 'Active',
        name: 'editActiveFlag',
        id: 'editActiveFlag',
        allowBlank: true
    },
  {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            cls:'buttonClr',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});