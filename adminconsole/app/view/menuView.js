Ext.define('login.view.menuView', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.menuView',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                id:'addLinkMenu',
                text: 'Add Link',
                hidden:true,
                iconCls: 'icon-Link'
                
            },
            {
                xtype: 'menuitem',
                hidden:true,
                id:'addModuleMenu',
                text: 'Add Module',
                iconCls: 'icon-Module'
                
            }
            ,
            {
                xtype: 'menuitem',
                id:'editMenu',
                text: 'Edit',
                hidden:true,
                iconCls: 'icon-edit'
            },
            {
                xtype: 'menuitem',
                id:'deleteMenu',
                hidden:true,
                text: 'Delete',
                iconCls: 'icon-delete'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});