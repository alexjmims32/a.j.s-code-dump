Ext.define('login.view.addFeedModuleView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addFeedModuleView',
    scrollable: 'both',
    minWidth: 300, 
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Add Module</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'Title',
        id: 'fdTitle',
        name: 'fdTitle',
        allowBlank: false

    },   
    {
        fieldLabel: 'Icon',
        id: 'fdIcon',
        name: 'fdIcon',
        allowBlank: false

    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            cls:'buttonClr',
            action: 'submit',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});



