Ext.define('login.view.createNotificationView', {
    extend:'login.view.customTitleView',
    titleAlign:'center',
    padding:'0 0 40 0',   
    bodyBorder:false,
    overflowX:'hidden',
    overflowY:'auto',
    height:'100%',
    width:'100%',
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 150 50 150',
        style: {
//            borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'addNotificationView',
        flex:1
    }
    ]     
});