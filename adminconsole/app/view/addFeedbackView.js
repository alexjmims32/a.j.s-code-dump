Ext.define('login.view.addFeedbackView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addFeedbackView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Add Feedback</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'Alias',
        name: 'addFbTitle',
        id: 'addFbTitle',
        allowBlank: false
    },
    {
        fieldLabel: 'Email',
        name: 'addFbEmail',
        id: 'addFbEmail',
        regex:new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$"),
        regexText: 'Email must in (xyz.a@sample.com or xyz@sample.com) format only',
        allowBlank: false
    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            cls:'buttonClr',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});