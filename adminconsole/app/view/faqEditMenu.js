Ext.define('login.view.faqEditMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.faqEditMenu',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editFaq',
                iconCls: 'icon-edit'
            },
            {
                xtype: 'menuitem',
                text: 'Delete',
                id:'deleteFaq',
                iconCls: 'icon-delete'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});