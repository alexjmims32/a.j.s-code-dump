Ext.define('login.view.tabView', {
	extend: 'Ext.tab.Panel',
	alias: 'widget.tabView',
	//    bodyStyle: "background-image:url('images/background.png')",
	title: '<table height=10><tr><td width=10%><img src="images/ttu-logo.png" height="70" width=120></td><td  class="headerTitle"><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
	header: true,
	titleAlign: 'center',
	minTabWidth: '130',
	layout: 'fit',
	id: 'tabView',
	dockedItems: [

		{
			xtype: 'toolbar',
			dock: 'top',
			items: [{
					xtype: 'tbfill' // fills left side of tbar and pushes following items to be aligned to right
				},

				{
					xtype: 'button',
					action: 'logout',
					margin: 4,
					minWidth: 50,
					text: 'Logout'
				}, {
					xtype: 'tbspacer',
					width: '45'
				}
			]
		}
	]


});