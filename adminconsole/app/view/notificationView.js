Ext.define('login.view.notificationView', {
    extend:'login.view.customTitleView',
    alias:'widget.notificationView',   
    titleAlign:'center',
    padding:'0 0 40 0',   
    bodyBorder:false,
    height:'100%',
    width:'100%',
    overflowX:'hidden',
    overflowY:'auto',
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 190 50 190',
        style: {
            borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'notifDetailsView',
        flex:1
    }
    ]     
});