Ext.define('login.view.logListView', {
    extend:'Ext.grid.Panel',
    columnLines: true,
    alias:'widget.logListView',
    store:'logStore', 
    padding:'0 0 28 0',
    minWidth: 300,
    margin:'20',
    style: {
        borderStyle: 'solid',
        borderWidth:'1px',
        maxHeight:420,
        overflowX:'hidden',
        overflowY:'auto'
    },
    columns:[
    {
        header:'Name',
        dataIndex:'jobName',
        flex:2,
        cls:'gridClr'
    },

    {
        header:'Status',
        dataIndex:'jobStatus',
        flex:1,
        cls:'gridClr'
    },
    {
        header:'StartDate',
        dataIndex:'jobStartDate',
        flex:3,
        cls:'gridClr'
    },
    {
        header:'EndDate',
        dataIndex:'jobEndDate',
        flex:2,
        cls:'gridClr'
    },
    {
        header:'Execution Time',
        dataIndex:'executionTime',                
        cls:'gridClr'
    },
    {
        header:'Error Message',
        dataIndex:'errorMessage',
        flex:3,
        cls:'gridClr'
    }
    ],
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,
        items:[
        {
            xtype:'button',
            minWidth: 50,
            margin: 4,
            text:'Back',
            cls:'buttonClr',
            action:'Back',
            buttonAlign:'right'
        },
        
        {
            xtype:'tbspacer',
            width:'970'
        }
        ]
    }
    ]
    
   
    
});