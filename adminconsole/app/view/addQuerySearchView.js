Ext.define('login.view.addQuerySearchView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addQuerySearchView',
    scrollable: 'both',
    minWidth: 300,
    border:false,
    items:[
    {
        xtype:'label',
        html:'<center><font size="5"  class="headerTitle"><b>Search By Query</b></font></center>',
        padding:20
    },
    {
        border:false,
        layout:{
            align:'center',
            type:'vbox'
        },
        bodyStyle:{
            padding : '10 0 0 20'
        },

        defaultType: 'textfield',
        items:[
           
        {                                           
            xtype:'textareafield',
            id:'popQuery',
            fieldLabel:'Query',
            name:'popQuery',
            allowBlank:false
                
        },
        {
            xtype:'tbspacer',
            width:'15'
        }              
        ]             
    }, 
    {
        xtype:'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,
        items:[ 
        {
            xtype:'button',
            margin: 4,
            tooltip:'Query must contain studentid and pidm',
            text:'Search',
            cls:'buttonClr',
            action:'byQuery',
            name:'find',
            minWidth: 50
        }
       
        ]

    }   
    ]
});