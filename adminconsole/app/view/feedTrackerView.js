Ext.define('login.view.feedTrackerView', {
    extend:'Ext.grid.Panel',
    alias:'widget.feedTrackerView',
    store:'feedTrackerStore',
    //    title:'<center><font size="3"><b>eNtourage Admin Console</b></font></center>',
    //    titleAlign:'center',
    padding:'0 0 40 0',
    //    minHeight:420,
    bodyBorder:false,

    columns:[    
    {
        header:'Id',
        dataIndex:'id',
        flex:1
    },

    //    {
    //        header:'ParentId',
    //        dataIndex:'parentID',
    //        flex:1
    //    },
    {
        header:'Type',
        dataIndex:'type',
        flex:1
    },
        
//    {
//        header:'Link',
//        dataIndex:'link',
//        flex:1
//    },
//
//    {
//        header:'Format',
//        dataIndex:'format',
//        flex:1
//    },
    //    {
    //        header:'ModuleId',
    //        dataIndex:'moduleID',
    //        flex:1
    //    },
        
        
    {
        header:'Title',
        dataIndex:'name',
        flex:1
    },

    //    {
    //        header:'Image',
    //        dataIndex:'image',
    //        flex:1
    //    },
    //    {
    //        header:'Campus Code',
    //        dataIndex:'campusCode',
    //        flex:1
    //    },
        
        
    {
        header:'LastModifiedOn',
        dataIndex:'lastModifiedOn',
        flex:1
    },

    {
        header:'LastModifiedBy',
        dataIndex:'lastModifiedBy',
        flex:1
    },
    {
        header:'Version',
        dataIndex:'versionNo',
        flex:1
    }
       
    ]
});