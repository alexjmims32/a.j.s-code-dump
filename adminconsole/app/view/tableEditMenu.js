Ext.define('login.view.tableEditMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.tableEditMenu',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editTableComments',
                iconCls: 'icon-edit'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});