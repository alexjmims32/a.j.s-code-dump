Ext.define('login.view.createUserView', {
    extend:'login.view.customTitleView',
    titleAlign:'center',
    bodyBorder:false,
    height:'100%',
    width:'100%',
    overflowX:'hidden',
    overflowY:'auto',
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 180 50 180',
        style: {
            //            borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'addUserView',
        flex:1
    }
    ]     
});