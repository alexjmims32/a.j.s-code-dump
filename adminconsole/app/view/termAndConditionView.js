Ext.define('login.view.termAndConditionView',{
    extend:'Ext.panel.Panel',
    alias:'widget.termAndConditionView',
    //    title:'<table height=10><tr><td width=10%><img src="images/n2n_logo.png" height="70" width=550></td><td  class="headerTitle"><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
    overflowX:'hidden',
    overflowY:'auto',
    minWidth: 300,
    layout:{
        align:'center',
        type:'vbox'
    },    
    margin:'40 30 30 30',
    style: {
        borderStyle: 'solid',
        borderWidth:'1px'            
    },    
    items:[
    {
        
        fieldLabel: 'Id',
        name: 'helpId',
        id: 'helpId',
        hidden:true
        
    },
    {
        xtype:'textareafield',
        autoScroll:true,
        fieldLabel: 'Terms & Conditions',
        id: 'termsAdnConditionText',
        grow : true,
        name: 'termsAdnConditionText',
        width:600,
        height:150,
        margin:'40 10 30 10',
        allowBlank: false

    },    
    {
        xtype:'panel',
        margin:'4 5 5 5',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'tbspacer',
            width:'680'
        },
        {
            xtype:'button',
            frame:true,
            margin: 4,            
            minWidth: 50,
            cls:'buttonClr',
            text:'Submit',
            action:'submit',
            name:'submit'
        }
        
        ]
    }
    ]

});
