Ext.define('login.view.mapEditMenuView', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.mapEditMenuView',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editMap',
                iconCls: 'icon-edit'
            },{
                xtype: 'menuitem',
                text: 'Delete',
                id:'deleteMap',
                iconCls: 'icon-delete'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});