Ext.define('login.view.createContactView', {
    extend:'login.view.customTitleView',
    titleAlign:'center',
    padding:'0 0 40 0',
    height:'100%',
    width:'100%',
    bodyBorder:false,
    overflowX:'hidden',
    overflowY:'auto',   
    //    bodyStyle:{
    //        background:'#ffc',
    //        padding : '10 0 0 20'
    //    },
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 150 50 150',
        style: {
            //                    borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'addEmergencyContactView',
        flex:1
    }
    ]     
});