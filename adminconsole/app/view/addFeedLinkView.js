Ext.define('login.view.addFeedLinkView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addFeedLinkView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Add Link</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'Url',
        id: 'fdlUrl',
        name: 'fdlUrl',        
        allowBlank: false
    },
    {
         xtype: 'combobox',
        fieldLabel: 'Format',
        id: 'fdlFormat',
        name: 'fdlFormat',        
        allowBlank: false,
        store: 'addModuleStore',
        displayField: 'description',
        valueField: 'code',
        editable: false
    },
    {
        fieldLabel: 'Title',
        id: 'fdlTitle',
        name: 'fdlTitle',        
        allowBlank: false
    },
    {
        fieldLabel: 'Icon',
        id: 'fdlIcon',
        name: 'fdlIcon',       
        allowBlank: false
    },      
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            cls:'buttonClr',
            action: 'submit',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});