Ext.define('login.view.notificationSearchListView', {
    extend: 'Ext.panel.Panel',
    alias:'widget.notificationSearchListView',
    //    store:'notificationSearchStore', 
    //        autoScroll : true,
    maxHeight:280,
    
    
    margin:'10',        
    style: {
        //        borderColor: 'green',
        borderStyle: 'solid',
        borderWidth:'1px'            
    },
    config: {
            
        items: [{
            xtype: 'gridpanel',
            id:'notifGrid',
            autoScroll: true,
            store: 'notificationSearchStore',
            border:false,
            selModel: Ext.create("Ext.selection.CheckboxModel"),
            dockedItems: [{
                xtype: 'pagingtoolbar',
                store: 'notificationSearchStore',   // same store GridPanel is using
                dock: 'top',
                displayInfo: true,
                id:'paging'
                
            }],
            columns: [
            {
                header:'Id',
                dataIndex:'id',
                type:'number',
                flex:1
            },   
            {
                header:'First Name',
                dataIndex:'firstName',
                flex:1
            }
            ,
            {
                header:'Last Name',
                dataIndex:'lastName',
                flex:1
            },  
            {
                header:'Middle Name',
                dataIndex:'middleName',
                flex:1

            }
            ]

        }]
    }
});
        