Ext.define('login.view.addNotificationView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addNotificationView',
    scrollable: 'both',
    minWidth: 300,
     
    items:[
    {
        xtype:'label',
        html:'<center><font size="5"  class="headerTitle"><b>Create Notification</b></font></center>',
        padding:20
    }, 
    {
        layout:{
            type:'vbox'            
        },       
        border:false,
        items:[                   
        {
            xtype:'panel',
            
            layout: {
                type: 'hbox',
                align: 'center'
            //                pack: 'center'
            },
            border:false,           
            items:[
            {
                xtype:'textfield',
                //                width:278,
                fieldLabel: 'To',
                id: 'notifTo',
                name: 'notifTo',
                padding:'3 2 3 0',
                //                height:27,
                width:277,
                allowBlank: false,
                required:true               
            },          
            {
                xtype:'button',             
                text:'Name',
                tooltip:'Search users by name',
                action:'search',
                cls:'buttonClr',
                name:'search',                
                minWidth: 55,
                margin: 2
            },            
            {
                xtype:'button',            
                text:'Query',
                cls:'buttonClr',
                action:'querySearch',
                tooltip:'Search users by query',
                name:'querySearch',
                minWidth: 55,
                margin: 1
            }                
            ]             
        },
       
        {
            xtype:'textareafield',
            fieldLabel: 'Message', 
            width:277,
            id: 'notifMessage',            
            name: 'notifMessage',
            allowBlank: false

        },
        {
            xtype:'datefield',
            format:'d-m-Y',
            width:277,
            minValue:new Date(),
            fieldLabel: 'Expiry Date',
            name: 'notifExpiryDate',
            id: 'notifExpiryDate',
            editable:false,
            allowBlank: false,
            required:true
        },
        {
            xtype:'combobox',
            fieldLabel: 'Type',
            name: 'notifType',
            width:277,
            id: 'notifType',
            allowBlank: false,
            editable: false,
            store:[
            'Emergency','Sports','Academics'
            ]
        },
        {
            xtype:'textfield',
            //            width:'50%',
            fieldLabel: 'Title',
            width:277,
            name: 'notifTitle',
            id: 'notifTitle',
            //            rows:4,
            allowBlank: false
        },
        {
            xtype:'datefield',
            format:'d-m-Y',     
            width:277,
            minValue:new Date(),
            fieldLabel: 'Due Date',
            name: 'notifDueDate',
            id: 'notifDueDate',
            editable:false,
            //            rows:4,
            allowBlank: false,
            required:true
        },
        {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'center',    
                pack: 'center'
            },
            padding: '20',
            border: false,

            items: [{
                xtype: 'button',
                margin: 4,
                text: 'Submit',
                action: 'submit',
                name: 'submit',
                cls:'buttonClr',
                minWidth: 50
            }, {
                xtype: 'button',
                margin: 4,
                text: 'Reset',
                action: 'reset',
                name: 'reset',
                cls:'buttonClr',
                minWidth: 50
            }, {
                xtype: 'button',
                text: 'Back',
                action: 'back',
                name: 'back',
                minWidth: 50,
                cls:'buttonClr',
                margin: 4
                
            }
            ]
        }]
    }   
    ]
});