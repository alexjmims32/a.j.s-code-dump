Ext.define('login.view.emergencyContactView',{
    extend:'Ext.panel.Panel',
    alias:'widget.emergencyContactView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[       
    {
        xtype:'label',
        html:'<center><font size="5"  class="headerTitle"><b>Contact Details</b></font></center>',
        padding:20
    },
    {
        xtype:'combobox',
        fieldLabel: 'Campus Code',
        name: 'eContactCC',
        id: 'eContactCC',
        allowBlank: false,
        store:'addMapComboStore',
        displayField: 'description',
        valueField: 'code',
        editable:false
    },
    {
        xtype:'combobox',
        fieldLabel: 'Category',
        name: 'ECategory',
        id: 'ECategory',  
        allowBlank: false,
        editable: false,
        displayField: 'description',
        valueField: 'code',
        store:'addEmergencyStore'
    },
    {
        fieldLabel: 'Name',        
        name: 'conName',
        id: 'conName',
        allowBlank: false
    },
    {
        xtype:'textareafield',
        fieldLabel: 'Comments',        
        name: 'conComments',
        id: 'conComments',
        allowBlank: true
    },
    {
        fieldLabel: 'Phone',        
        name: 'conPhone',
        id: 'conPhone',
        regex:new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$"),
        regexText: 'Phone number must in 123-456-7894 format only',
        allowBlank: false
    },{
        fieldLabel: 'Address',
        name: 'conAdd',
        id: 'conAdd',
        allowBlank: true
    },
    {
        fieldLabel: 'Email',
        name: 'conEmail',
        id: 'conEmail',
        regex:new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$"),
        regexText: 'Email must in xyz@sample.com format only',
        allowBlank: true
    },{
        fieldLabel: 'Picture Url',
        name: 'pictureUrl',
        id: 'pictureUrl',
        allowBlank: true
    },   
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            cls:'buttonClr',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            cls:'buttonClr',
            margin: 4,
            text: 'Back',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});