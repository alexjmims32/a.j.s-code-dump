Ext.define('login.view.updateLinkView', {
    extend:'login.view.customTitleView',
    titleAlign:'center',
    height:'100%',
    width:'100%',
    overflowX:'hidden',
    overflowY:'auto',
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 150 50 150',
        style: {
            //            borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'editLinkView',
        flex:1
    }
    ]     
});