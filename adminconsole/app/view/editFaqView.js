Ext.define('login.view.editFaqView',{
    extend:'Ext.panel.Panel',
    alias:'widget.editFaqView',
    scrollable: 'both',
    minWidth: 300,
    
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Edit Faq</b></font></center>',
        padding:20
    },
    {
        xtype:'textareafield',
        fieldLabel: 'Question',
        id: 'eQuestion',
        name: 'eQuestion',
        allowBlank: false,
        border:'10 10 10 10'

    },
    {
        xtype:'textareafield',
        fieldLabel: 'Answer',
        id: 'eAnswer',
        name: 'eAnswer',
        allowBlank: false

    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            cls:'buttonClr',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});