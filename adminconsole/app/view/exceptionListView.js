Ext.define('login.view.exceptionListView', {
    extend:'Ext.grid.Panel',
    columnLines: true,
    alias:'widget.exceptionListView',
    store:'exceptionStore', 
    padding:'0 0 28 0',
    minWidth: 300,
    margin:'20',
    style: {
        borderStyle: 'solid',
        borderWidth:'1px',
        maxHeight:420,
        overflowX:'hidden',
        overflowY:'auto'
    },
    columns:[
    {
        header:'Job Id',
        dataIndex:'jobId',
        cls:'gridClr',
        hidden:true
    },
    {
        header:'Name',
        dataIndex:'jobName',
        cls:'gridClr'
    },

    {
        header:'Table Name',
        dataIndex:'tableName',
        flex:1,
        cls:'gridClr'
    },
    {
        header:'Start Time',
        dataIndex:'startTime',
//        flex:1,
        cls:'gridClr'
    },
    {
        header:'End Time',
        dataIndex:'endTime',
//        flex:1,
        cls:'gridClr'
    },
    {
        header:'Error Message',
        dataIndex:'errorMessage',
        cls:'gridClr',
         flex:4
    },
    
    ],
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,
        items:[
        {
            xtype:'button',
            minWidth: 50,
            margin: 4,
            text:'Back',
            cls:'buttonClr',
            action:'Back',
            buttonAlign:'right'
        },
        
        {
            xtype:'tbspacer',
            width:'970'
        }
        ]
    }
    ]
    
   
    
});