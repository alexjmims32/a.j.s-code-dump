Ext.define('login.view.columnListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.columnListView',
    store:'metaDataColumnsStore',    
    titleAlign:'center',
    padding:'0 0 40 0',
    bodyBorder:false,

    columns:[
    {
        header:'Column',
        dataIndex:'name',
        flex:1
    },

    {
        header:'Comments',
        dataIndex:'comments',
        flex:2
    }    
    ]
    
    
});