Ext.define('login.view.editModuleView',{
    extend:'Ext.panel.Panel',
    alias:'widget.editModuleView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Edit Module</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'Title',
        id: 'eFdTitle',
        name: 'eFdTitle',
        allowBlank: false

    },
    {
        fieldLabel: 'Icon',
        id: 'eFdIcon',
        name: 'eFdIcon',
        allowBlank: true

    },    
    {
        fieldLabel: 'Campus Code',
        id: 'eFdmCampCode',
        name: 'eFdmCampCode',
        allowBlank: false,
        readOnly:true

    },
   {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            cls:'buttonClr',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            cls:'buttonClr',
            text: 'Back',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});