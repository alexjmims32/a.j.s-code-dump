Ext.define('login.view.addConfigView',{
//    extend:'Ext.panel.Panel',
    extend:'Ext.form.FieldSet',
    //    align:'center',
    alias:'widget.addConfigView',
//    autoScroll:true,
    overflowX:'hidden',
    overflowY:'auto',
    ////            frame:true,
    layout:{
        align:'center',
        type:'vbox'
    },
    bodyStyle:{
        padding : '10 0 0 20'
    },

    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="4"><b>MobEdu Client Configuration</b></font></center>',
        ////            html:'<center><h3>MobEdu Client Configuaration</h3></center>',
        padding:20
    //
    },
    {
        xtype:'textfield',
        fieldLabel: 'Client',
        id: 'adclient',
        name: 'adclient',
        labelWidth:180,
        allowBlank: false

    },{
        fieldLabel: 'DB Hostname',
        name: 'adhostname',
        id: 'adhostname',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Port',
        name: 'adport',
        id: 'adport',
        labelWidth:180,
        allowBlank: true
    },{
        fieldLabel: 'SID',
        name: 'adsid',
        id: 'adsid',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Username',
        name: 'adusername',
        id: 'adusername',
        labelWidth:180,
        allowBlank: true
    },{
        fieldLabel: 'Password',
        inputType:'password',
        name: 'adpassword',
        id: 'adpassword',
        labelWidth:180,
        allowBlank: true
    },
    {
        padding:'20 0 0 0',
        fieldLabel: 'LDAP Hostname',
        name: 'adldapHost',
        id: 'adldapHost',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Port',
        name: 'adldapPort',
        id: 'adldapPort',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Protocal',
        name: 'adprotocal',
        id: 'adprotocal',
        labelWidth:180,
        allowBlank: true
    },
    {
        //            padding:'20 0 0 0',
        fieldLabel: 'Base DN',
        name: 'baseDN',
        id: 'adbaseDN',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'UserId',
        name: 'aduserId',
        id: 'aduserId',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Password',
        inputType:'password',
        name: 'addnPassword',
        id: 'addnPassword',
        labelWidth:180,
        allowBlank: true

    },
    {
        xtype:'label',
        padding:'20 0 10 0',
        html:'<b>VPN</b>'
    },
    {
        //        padding:'20 0 0 0',
        fieldLabel: 'VPN Type',
        name: 'vpnType',
        id: 'advpnType',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'VPN URL/Host',
        name: 'vpnHost',
        id: 'advpnHost',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'VPN User',
        name: 'vpnUser',
        id: 'advpnUser',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'VPN Password',
        inputType:'password',
        name: 'vpnPassword',
        id: 'advpnPassword',
        labelWidth:180,
        allowBlank: true
    },
    {
        xtype:'label',
        padding:'20 0 10 0',
        html:'<b>Test/Staging Server</b>'
    },
    {
        fieldLabel: 'OS',
        name: 'testOs',
        id: 'adtestOs',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Hostname',
        name: 'testHostname',
        id: 'adtestHostname',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Port',
        name: 'testPort',
        id: 'adtestPort',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'UserId',
        name: 'testUserId',
        id: 'adtestUserId',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Password',
        inputType:'password',
        name: 'testPassword',
        id: 'adtestPassword',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Web Server Software',
        name: 'testWS',
        id: 'adtestWS',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Web Server Software Version',
        name: 'testWSVersion',
        id: 'adtestWSVersion',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Application Server Software',
        name: 'testAS',
        id: 'adtestAS',
        labelWidth:180,
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Application Server Software Version',
        name: 'testASVersion',
        id: 'adtestASVersion',
        labelWidth:180,
        allowBlank: true
    },
    {
        xtype:'label',
        padding:'20 0 10 0',
        html:'<b>Contact Information</b>'
    },
    {
        fieldLabel: 'Lead Techinician Name',
        name: 'leadName',
        id: 'adleadName',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Lead Techinician Phone',
        name: 'leadPhone',
        id: 'adleadPhone',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Lead Techinician E-mail',
        name: 'leadEmail',
        id: 'adleadEmail',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'DBA Techinician Name',
        name: 'dbaName',
        id: 'addbaName',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'DBA Techinician Phone',
        name: 'dbaPhone',
        id: 'addbaPhone',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'DBA Techinician E-mail',
        name: 'dbaEmail',
        id: 'addbaEmail',
        labelWidth:180,
        allowBlank: true
    },
    {
        fieldLabel: 'Notes/Comments',
        name: 'notes',
        id: 'adnotes',
        labelWidth:180,
        allowBlank: true
    },
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'30',
        border:false,
        items:[
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Submit',
            action:'submit',
            name:'submit'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Reset',
            action:'reset',
            name:'reset'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Back',
            action:'back',
            name:'back'
        }
        ]
    }
    ]

});