Ext.define('login.view.studentConfigView', {
    extend: 'Ext.panel.Panel',
    alias:'widget.studentConfigView',
    title:'<table height=10><tr><td width=10%><img src="images/n2n_logo.png" height="70" width=550></td><td><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
    autoScroll : true,
    
    config: {

        items: [{

            xtype: 'gridpanel',
            columnLines: true,
            id: 'configGrid',
            store: 'studentConfigStore',
            maxHeight:380,
            margin:'20',
            style: {            
                borderStyle: 'solid',
                borderWidth:'1px'            
            },
                     
           
            //            border:false,
            columns: [
            {
                header: 'Campus',
                dataIndex: 'campusCode',               
                flex: 2,
                cls:'gridClr'
            },
            {
                header: 'Module',
                dataIndex: 'moduleCode',               
                flex: 2,
                cls:'gridClr'
            },
            {                
                header: 'Module Description',
                dataIndex: 'description',               
                flex: 2,
                cls:'gridClr'
            },                                             
            {
                xtype: 'gridcolumn',
                header: 'Enabled',
                dataIndex: 'accessFlag'
                ,
                cls:'gridClr',
                renderer: function(val) {
                    if(val.toLowerCase() == 'y') return '<input type="checkbox" checked=true>';
                    else return '<input type="checkbox">';
                }
            }, {
                xtype: 'gridcolumn',
                cls:'gridClr',
                header: 'Authorization',
                dataIndex: 'authRequired',              
                renderer: function(val) {
                    if(val.toLowerCase() == 'y') return '<input type="checkbox" checked=true>';
                    else return '<input type="checkbox" >';
                }               
            }]
        }, {
            xtype: 'panel',
            layout: {
                type: 'hbox',
                align: 'center',    
                pack: 'center'
            },
            padding: '20',
            border: false,
            items: [
            {
                xtype: 'button',
                frame: true,
                margin: 4,            
                minWidth: 50,
                text: 'Submit',
                cls:'buttonClr',
                action: 'submit',
                name: 'submit'
            }
            ]
        }]

    }
});