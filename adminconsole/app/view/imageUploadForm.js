Ext.define('login.view.imageUploadForm', {
    extend:'Ext.form.Panel',
    alias:'widget.imageUploadForm',
    width: 500,
    scrollable: 'both',    
    frame: true,
    title: 'File Upload Form',
    bodyPadding: '10 10 0',
    id:'form',
    layou:{
        align:'cennter'
    },
    defaults: {
        anchor: '40%',
        allowBlank: false,
        msgTarget: 'side'
    //        labelWidth: 50
    },

    items: [    
    {
        xtype: 'fileuploadfield',
        id: 'formfile',
        name:'formfile',
        emptyText: 'Select an image',
        fieldLabel: 'Image',
        allowBlank:false,        
        readOnly:false,
        buttonText: 'Browse'
    }
    ],
    buttons: [{
        text: 'Save',
        handler: function(){
            var form = this.up('form').getForm();
            if(form.isValid()){
                form.submit({
                    url: webserver+'fileUpload', 
                    enctype : 'multipart/form-data',
                    waitMsg: 'Please wait, your image is uploading...',
                    success: function(fp, o) {
                        Ext.Msg.alert('Success', 'File Successfully proccessed to server');
                    },
                    failure: function(fp, o) {
                        Ext.Msg.alert('Fail', 'File not proccessed to server');
                    }

                });            
            }else {
                Ext.Msg.alert( "Error!", "Your form is invalid!" );
            }
            
        }
    },{
        text: 'Reset',
        handler: function() {
            this.up('form').getForm().reset();
        }
    }]

});

//Ext.define('login.view.imageUploadForm', {
//    extend: 'Ext.form.Panel',
//    alias : 'widget.fileuploadform',
//    width: 500,
//    frame: true,
//    title: 'File Upload Form',
//    bodyPadding: '10 10 0',
// 
//    defaults: {
//        anchor: '100%',
//        allowBlank: false,
//        msgTarget: 'side',
//        labelWidth: 75
//    },
// 
//    items: [{
//        xtype: 'textfield',
//        name: 'filename',
//        fieldLabel: 'File Name'
//    },{
//        xtype: 'filefield',
//        id: 'myFile',
//        emptyText: 'Select a File to Upload',
//        fieldLabel: 'Select a File',
//        name: 'fileSelected',
//        buttonText: '',
//        buttonConfig: {
//            iconCls: 'upload-icon'
//        }
//    }],
// 
//    buttons: [{
//        text: 'Upload',
//        action: 'uploadFile'
//    },
//    {
//        text: 'Reset Form',
//        scope: this,
//        handler: function() {
//            this.reset();
//        }
//    }]
//});