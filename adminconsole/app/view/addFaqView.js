Ext.define('login.view.addFaqView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addFaqView',
    scrollable: 'both',
    minWidth: 300, 
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Add Faq</b></font></center>',
        padding:20
    },
    {
        xtype:'textareafield',
        fieldLabel: 'Question',
        id: 'qFaq',
        name: 'qFaq',
        allowBlank: false

    },   
    {
        xtype:'textareafield',
        fieldLabel: 'Answer',
        id: 'aFaq',
        name: 'aFaq',
        allowBlank: false

    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});



