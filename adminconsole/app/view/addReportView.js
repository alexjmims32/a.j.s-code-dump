Ext.define('login.view.addReportView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.addReportView',
    scrollable: 'both',
    minWidth: 340,
    items: [{
        xtype: 'label',
        html: '<center><font class="headerTitle"><b>Add Report</b></font></center>',
        padding: 20
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'Refresh Type',
        name: 'srefreshType',
        id: 'srefreshType',
        allowBlank: false,
        store:[
        'TABLE','MODULE'
        ],
        displayField: 'description',
        valueField: 'code',
        width:277,
        editable: false,
        listeners:{
            change: function(field,newValue,oldValue,eOpts){
                if(newValue!=null){
                    var lsStore=Ext.getStore('reportDropDownStore');
                    lsStore.getProxy().url =webserver + 'getEnumerations?enumType='+newValue;
                    lsStore.load();                  
                }
                
            }
        }
    }, 
    {
        xtype: 'textfield',
        fieldLabel: 'Job Name',
        name: 'refreshName',
        id: 'refreshName',
        width:277,
        allowBlank: false
        
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'T/M To Refresh',
        name: 'tmToRefreshType',
        id: 'tmToRefreshType',
        allowBlank: false,
        store:'reportDropDownStore',
        width:277,            
        displayField: 'description',
        valueField: 'code',
        editable: true                   
    },
    {
        xtype: 'datefield',
        format:'d-m-Y',
        width:277,
        minValue:new Date(),
        fieldLabel: 'Start Date',
        name: 'ssDate',
        id: 'ssDate',        
        allowBlank: false
    },{
        xtype: 'datefield',
        format:'d-m-Y',
        width:277,
        minValue:new Date(),
        fieldLabel: 'End Date',
        name: 'seDate',
        id: 'seDate',
        allowBlank: true
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'Refresh Frequency',
        name: 'rFrequency',
        width:277,
        id: 'rFrequency',
        allowBlank: true,
        store:[
        'Yearly','Monthly','Weekly','Daily'
        ]
    }, {
        xtype: 'combobox',
        fieldLabel: 'Refresh Interval',
        name: 'rInterval',
        width:277,
        id: 'rInterval',
        allowBlank: true
    },
    {
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'arEnable',
        width:277,
        id: 'arEnable',
        allowBlank: true
    },
    {
        xtypr:'panel',
        layout:{
            type:'vbox'            
        },       
        border:false,
        items:[                   
        {
            layout:{
                type:'hbox' ,           
                align:'center'
            }, 
            border:false,           
            items:[    
            {
                xtype: 'numberfield',
                fieldLabel: 'Schedule Time',
                id:'schdeuleTime1',
                value: 0,
                minValue: 00,
                maxValue: 23,
                width:155,
                margin:'0 5 10 10'
            },  
            {
                xtype: 'numberfield',
                id:'schdeuleTime2',
                value: 0,
                minValue: 00,
                maxValue: 59,
                width:45,
                margin:'0 5 10 10'
            },  
            {
                xtype: 'numberfield',
                id:'schdeuleTime3',
                value: 0,
                minValue: 00,
                maxValue: 59,
                width:45,
                margin:'0 5 10 10'
            }                       
            ]             
                
        }
        ]
    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [
        {
            xtype: 'button',
            margin: 4,
            align:'center',
            text: 'Schedule',
            cls:'buttonClr',
            action: 'schedule',
            name: 'schedule',
            minWidth: 50
        },
        {
            xtype: 'button',
            margin: 4,
            align:'center',
            text: 'Reset',
            cls:'buttonClr',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        },
        {
            xtype: 'button',
            margin: 4,
            align:'center',
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        } 
        ]
    }]

});