Ext.define('login.view.addClientView',{
    extend:'Ext.form.FieldSet',
    alias:'widget.addClientView',
    overflowX:'hidden',
    overflowY:'auto',
    layout:{
        align:'center',
        type:'vbox'
    },
    bodyStyle:{
        padding : '10 0 0 20'
    },

    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="4"><b>MobEdu Client</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'Client Name',
        id: 'adclientName',
        name: 'adclientName',
        width:350,
        allowBlank: false

    },
    {
        fieldLabel: 'User Name',
        name: 'clientUserId',
        id: 'clientUserId',
        width:350,
        allowBlank: false
    },{
        fieldLabel: 'Password',
        inputType:'password',
        name: 'clientPassword',
        id: 'clientPassword',
        width:350,
        allowBlank: false
    },{
        xtype:'textareafield',
        fieldLabel: 'Description',
        name: 'clientDesc',
        id: 'clientDesc',
        rows:4,
        width:350,
        allowBlank: true
    },
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'30',
        border:false,
        items:[
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Submit',
            action:'submit',
            name:'submit'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Reset',
            action:'reset',
            name:'reset'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Back',
            action:'back',
            name:'back'
        }
        ]
    }
    ]

});