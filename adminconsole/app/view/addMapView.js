Ext.define('login.view.addMapView', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.addMapView',
	scrollable: 'both',
	minWidth: 300,
	defaultType: 'textfield',
	items: [{
			xtype: 'label',
			html: '<center><font size="5"  class="headerTitle"><b>Add Map</b></font></center>',
			padding: 20
		}, {
			xtype: 'combobox',
			fieldLabel: 'Campus',
			name: 'mapCC',
			id: 'mapCC',
			allowBlank: false,
			store: 'addMapComboStore',
			displayField: 'description',
			valueField: 'code',
			editable: false
		}, {
			fieldLabel: 'Title',
			name: 'addTitle',
			id: 'addTitle',
			allowBlank: false
		}, {
			xtype: 'combobox',
			fieldLabel: 'Category',
			name: 'mapCategory',
			id: 'mapCategory',
			multiSelect: true,
			allowBlank: false,
			store: 'categoryStore',
			displayField: 'category',
			valueField: 'categoryId',
			editable: false,
			listeners: {
				change: function(categoryId, newValue, oldValue, eOpts) {
					console.log(newValue);
				}
			}
		}, {
			fieldLabel: 'Building Code',
			name: 'addBldgCode',
			id: 'addBldgCode',
			allowBlank: false
		}, {
			fieldLabel: 'Building Name',
			name: 'addBldgName',
			id: 'addBldgName',
			allowBlank: false
		}, {
			xtype: 'textareafield',
			fieldLabel: 'Building Description',
			name: 'addBldgDesc',
			id: 'addBldgDesc',
			allowBlank: true
		}, {
			xtype: 'textareafield',
			fieldLabel: 'Address',
			name: 'addAddress',
			id: 'addAddress',
			allowBlank: true
		}, {
			fieldLabel: 'Phone',
			name: 'addPhone',
			id: 'addPhone',
			regex: new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$"),
			regexText: 'Phone number must in 123-456-7894 format only',
			allowBlank: true
		}, {
			xtype: 'textfield',
			fieldLabel: 'Email',
			id: 'addEmail',
			name: 'addEmail',
			regex: new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$"),
			regexText: 'Email must in xyz@sample.com format only',
			allowBlank: true
		}, {
			fieldLabel: 'Image Url',
			name: 'addImageUrl',
			id: 'addImageUrl',
			allowBlank: true
		}, {
			fieldLabel: 'Latitude',
			name: 'addLatitude',
			id: 'addLatitude',
			allowBlank: false
		}, {
			fieldLabel: 'Longitude',
			name: 'addLongitude',
			id: 'addLongitude',
			allowBlank: false
		},

		{
			xtype: 'panel',
			layout: {
				type: 'hbox',
				align: 'center',
				pack: 'center'
			},
			padding: '20',
			border: false,

			items: [{
				xtype: 'button',
				margin: 4,
				text: 'Submit',
				action: 'submit',
				cls: 'buttonClr',
				name: 'submit',
				minWidth: 50
			}, {
				xtype: 'button',
				margin: 4,
				text: 'Reset',
				cls: 'buttonClr',
				action: 'reset',
				name: 'reset',
				minWidth: 50
			}, {
				xtype: 'button',
				text: 'Back',
				cls: 'buttonClr',
				action: 'back',
				name: 'back',
				margin: 4,
				minWidth: 50
			}]
		}
	]

});