Ext.define('login.view.feedbackListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.feedbackListView',
    store:'feedbackStore',
    //    padding:'0 0 40 0',
    columnLines: true,
    bodyBorder:false,
    margin:'20',
    //    frame:true, 
    style: {
        borderStyle: 'solid',
        borderWidth:'1px'            
    },
    columns:[    
    {
        header:'Alias',
        dataIndex:'title',
        flex:1,
        cls:'gridClr'
    },

    {
        header:'Email',
        dataIndex:'email',
        flex:1,
        cls:'gridClr'
    },
    //    {
    //        header:'Description',
    //        dataIndex:'description',
    //        flex:1
    //    }
    //       
    ]
    ,
    dockedItems:[
    {
        xtype:'panel',
        layout: {
            type: 'hbox',
            align: 'left',    
            pack: 'left'
        },
        padding:'5 0 5 10',
        border: false,
        items:[
        {
            xtype:'button',
            margin: 4,            
            minWidth: 50,
            text:'Add',
            cls:'buttonClr',
            action:'ADD',
            buttonAlign:'right'
        },
        
        {
            xtype:'tbspacer',
            width:'970'
        }
        ]
    }
    ]
     
    
});