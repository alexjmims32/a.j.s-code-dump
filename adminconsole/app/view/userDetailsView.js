Ext.define('login.view.userDetailsView',{
    extend:'Ext.panel.Panel',
    alias:'widget.userDetailsView',
    scrollable: 'both',
    minWidth: 300,
    bodyStyle:{
        padding : '10 0 0 20'
    },
    layout:{
        align:'center',
        type:'vbox'      
    },
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5"   class="headerTitle"><b>User Details View</b></font></center>',
        padding:20
    },
    {
        xtype:'textfield',
        fieldLabel: 'User Name',
        readOnly:true,
        id: 'vwUserName',
        name: 'vwUserName',        
        allowBlank: false,
        width:277
    },
    {
        fieldLabel: 'Password',
        name: 'vwPassword',
        id: 'vwPassword',
        allowBlank: true,
        inputType:'Password',
        width:277
    },
    {
        fieldLabel: 'First Name',
        name: 'vwFirstName',
        id: 'vwFirstName',   
        allowBlank: true,
        width:277
    },{
        fieldLabel: 'Last Name',
        name: 'vwLastName',
        id: 'vwLastName',
        allowBlank: true,
        width:277
    },      
    {
        xtype: 'combobox',
        fieldLabel: 'Active',
        name: 'vwActive',
        id: 'vwActive',
        allowBlank: true,
        editable: false,
        displayField: 'description',
        valueField: 'code',
        store: 'activeFlagStore',
        width:277
    },
    {
        xtype: 'checkboxgroup',
        id:'eCheck',
        width:277,
        fieldLabel: 'Role',
        // Arrange checkboxes into one column, distributed vertically
        columns: 1,
        vertical: true,
        items: [
        {
            boxLabel: 'Notification', 
            name: 'notification', 
            inputValue: '1',
            id:'eNotification'
        },
        {
            boxLabel: 'Feed', 
            name: 'eFeed', 
            inputValue: '2',
            id:'eFeed'        
        },
        {
            boxLabel: 'Map', 
            name: 'eMap', 
            inputValue: '3',
            id:'eMap'
        },
        {
            boxLabel: 'Contacts', 
            name: 'eContact', 
            inputValue: '4',
            id:'eContact'
        },
            
        {
            boxLabel: 'Help About', 
            name: 'eHelpAbout', 
            inputValue: '5',
            id:'eHelpAbout'
        },
        {
            boxLabel: 'Help Faq', 
            name: 'eHelpFaq', 
            inputValue: '6',
            id:'eHelpFaq'        
        },
        {
            boxLabel: 'Feature Visibility', 
            name: 'eRoles', 
            inputValue: '7',
            id:'eRoles'
        },
        {
            boxLabel: 'Feedback', 
            name: 'eFeedback', 
            inputValue: '8',
            id:'eFeedback'
        },
        {
            boxLabel: 'Terms & Conditions', 
            name: 'eTC', 
            inputValue: '9',
            id:'eTC'
        },
        {
            boxLabel: 'Users', 
            name: 'eUsers', 
            inputValue: '10',
            id:'eUsers'
        }
            
        ]
    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            id:'editSubmit',
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            name: 'reset',
            cls:'buttonClr',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});