Ext.define('login.view.reportEditMenu', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.reportEditMenu',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editReport',
                iconCls: 'icon-edit'
            },           
            {
                xtype: 'menuitem',
                id:'delReport',
                text: 'Delete',
                iconCls: 'icon-delete'
            },
            {
                xtype: 'menuitem',
                id:'reportLog',
                text: 'Log',
                iconCls: 'icon-log'
            },
            {
                xtype: 'menuitem',
                id:'reportEsception',
                text: 'Exceptions',
                iconCls: 'icon-error'
            },
            ]
        });
 
        me.callParent(arguments);
    }
});