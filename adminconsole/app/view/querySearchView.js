Ext.define('login.view.querySearchView', {
   extend:'login.view.customTitleView',
    alias:'widget.querySearchView',
    titleAlign:'center',
    bodyBorder:false,
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'20',
        style: {
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'addQuerySearchView',
        flex:1
    },{
        margin:'20',
        //            frame:true, 
        style: {
            borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'notificationSearchByQueryView',
        flex:2
    }
    ],
    dockedItems:[
    {
        xtype:'panel',
        dock:'bottom',            
        layout:{
            type:'hbox',
            pack:'center'
        },
        padding:'5 0 5 10',
        border:false,
        items:[         
        {
            xtype:'button',
             margin: 4,
            minWidth: 50,
            text:'Submit',
            action:'submit',
            cls:'buttonClr',
            buttonAlign:'right'
        },
        {
            xtype:'tbspacer',
            width:'15'
        },
        {
            xtype:'button',
            margin: 4,
            minWidth: 50,
            text:'Back',
            cls:'buttonClr',
            action:'back',
            buttonAlign:'right'
        }
       
        ]
    }
    ]

        
       
});