Ext.define('login.view.editReportView',{
    extend:'Ext.panel.Panel',
    alias:'widget.editReportView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[       
    {
        xtype:'label',
        html:'<center><font size="5"  class="headerTitle"><b>Schedule</b></font></center>',
        padding:20
    },
    {
        xtype: 'combobox',
        fieldLabel: 'Refresh Type',
        name: 'esrefreshType',
        id: 'esrefreshType',
        allowBlank: false,    
        store:[
        'TABLE','MODULE'
        ],
        displayField: 'description',
        valueField: 'code',
        width:277,
        editable: false,
        listeners:{
            change: function(field,newValue,oldValue,eOpts){
                if(newValue!=null){
                    var lsStore=Ext.getStore('reportDropDownStore');
                    lsStore.getProxy().url =webserver + 'getEnumerations?enumType='+newValue;
                    lsStore.load();                  
                }
                
            }
        }
    }, 
    {
        xtype: 'textfield',
        fieldLabel: 'Refresh Name',
        name: 'erefreshName',
        id: 'erefreshName',
        width:277,
        allowBlank: false
        
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'Table/Module To Refresh',
        name: 'etmToRefreshType',
        id: 'etmToRefreshType',
        allowBlank: false,
        width:277,
        store:'reportDropDownStore',
        displayField: 'description',
        valueField: 'code',
        editable: false
    },
    {
        xtype: 'datefield',
        format:'d-m-Y',
        width:277,
        //        minValue:new Date(),
        fieldLabel: 'Start Date',
        name: 'essDate',
        id: 'essDate',        
        allowBlank: false
    },{
        xtype: 'datefield',
        format:'d-m-Y',
        width:277,
        //        minValue:new Date(),
        fieldLabel: 'End Date',
        name: 'eseDate',
        id: 'eseDate',
        allowBlank: true
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'Refresh Frequency',
        name: 'erFrequency',
        width:277,
        id: 'erFrequency',
        allowBlank: true,
        store:[
        'Yearly','Monthly','Weekly','Daily'
        ]
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'Refresh Interval',
        name: 'erInterval',
        width:277,
        id: 'erInterval',
        allowBlank: true
    },
    {
        xtype: 'checkbox',
        fieldLabel: 'Enable',
        name: 'erEnable',
        width:277,
        id: 'erEnable',
        allowBlank: true
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox'            
        },       
        border:false,
        items:[                   
        {
            layout:{
                type:'hbox' ,           
                align:'center'
            }, 
            border:false,           
            items:[    
            {
                xtype: 'numberfield',
                fieldLabel: 'Schedule Time',
                id:'eschdeuleTime1',
                value: 0,
                minValue: 0,
                maxValue: 23,
                width:155,
                margin:'0 5 10 10'
            },
            
            {
                xtype: 'numberfield',
                id:'eschdeuleTime2',
                value: 0,
                minValue: 0,
                maxValue: 59,
                width:45,
                margin:'0 5 10 10'
            },  
            {
                xtype: 'numberfield',
                id:'eschdeuleTime3',
                value: 0,
                minValue: 0,
                maxValue: 59,
                width:45,
                margin:'0 5 10 10'
            }                       
            ]             
                
        }
        ]
    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [
        {
            xtype: 'button',
            margin: 4,
            align:'center',
            text: 'Schedule',
            cls:'buttonClr',
            action: 'schedule',
            name: 'schedule',
            minWidth: 50
        },
        {
            xtype: 'button',
            margin: 4,
            align:'center',
            text: 'Reset',
            cls:'buttonClr',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        },
        {
            xtype: 'button',
            margin: 4,
            align:'center',
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        } 
        ]
    }]

});