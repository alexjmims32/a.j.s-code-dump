Ext.define('login.view.passwordView',{
    extend:'Ext.panel.Panel',
    alias:'widget.passwordView',
    scrollable: 'both',
    minWidth: 300,
    bodyStyle:{
        padding : '10 0 0 20'
    },
    layout:{
        align:'center',
        type:'vbox'
    },
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5"   class="headerTitle"><b>Change Password</b></font></center>',
        padding:20
    },   
    {
        fieldLabel: 'username',     
        name: 'pwUsername',
        id: 'pwUsername',
        allowBlank: true,
        hidden:true
    },
    {
        fieldLabel: 'Password',
        inputType:'Password',
        name: 'changePassword',
        id: 'changePassword',
        allowBlank: true
    },
    {
        fieldLabel: 'Confirm Password',
        inputType:'Password',
        name: 'confirmPassword',
        id: 'confirmPassword',
        allowBlank: true
    },
    {
        xtype:'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,
        items:[
        {
            xtype:'button',
            frame:true,
            margin: 4,
            text:'Submit',
            action:'submit',
            cls:'buttonClr',
            name:'submit',
            minWidth: 50
        }                
        ]
    }
    ]

});