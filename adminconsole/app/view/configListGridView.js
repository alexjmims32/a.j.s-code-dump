Ext.define('login.view.configListGridView', {
    extend:'Ext.grid.Panel',
    alias:'widget.configListGridView',
    store:'updateConfigStore',
    title:'<font size="3"><b>Available Configurations</b></font>',
    titleAlign:'center',
    id:'list',
    padding:'0 0 40 0',
    //    minHeight:420,
    bodyBorder:false,

    columns:[
    {
        header:'Client',
        dataIndex:'client',
        flex:1
    },

    {
        header:'DB HostName',
        dataIndex:'dbHostname',
        flex:1
    },
    {
        header:'DB Port',
        dataIndex:'dbPort',
        flex:1
    }
    ,
    {
        header:'DB Sid',
        dataIndex:'dbSid',
        flex:1
    },
    {

        header:'DB UserName',
        dataIndex:'dbUsername',
        flex:1
    },
    {
        header:'DB Password',
        dataIndex:'dbPassword',
        inputfield:'password',
        flex:1

    },
    {
        header:'LDAP HostName',
        dataIndex:'ldapHostname',
        flex:1
    },
    {
        header:'LDAP port',
        dataIndex:'ldapPort',
        flex:1
    },
    {
        header:'Protocol',
        dataIndex:'ldapProtocol',
        flex:1
    },
    {
        header:'DN UserId',
        dataIndex:'dnUserID',
        flex:1
    },
    {
        header:'DN Password',
        dataIndex:'dnPassword',
        flex:1
    },
    {
        xtype:'gridcolumn',
        header:'Version',
        dataIndex:'version',
        id:'cVersion',
//        name:'version',
        renderer: function(val) {
            return '<a href="#">'+ val  +'</a>';
        },
        flex:1
    }

    ],
   
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,

        items:[
        {
            xtype:'button',
            padding:'2 50',
            text:'ADD',
            action:'ADD',
            buttonAlign:'right'
        },
        {
            xtype:'tbspacer',
            width:'20'
        },
        {
            xtype:'button',
            padding:'2 40',
            id:'clientListButton',
            text:'Client List',
            action:'clientList',
            buttonAlign:'right'
        },
        {
            xtype:'tbspacer',
            flex:1
//            width:'1000'
        },
        {
            xtype:'button',
            padding:'2 50',
            text:'Logout',
            action:'logout',
            buttonAlign:'left'
        },
         {
            xtype:'tbspacer',
            width:'20'
        },


        ]
    }
    ]


})
;