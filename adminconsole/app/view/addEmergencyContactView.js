Ext.define('login.view.addEmergencyContactView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.addEmergencyContactView',
    scrollable: 'both',
    minWidth: 300,
    items: [{
        xtype: 'label',
        html: '<center><font class="headerTitle"><b>Add Contact</b></font></center>',
        padding: 20
    }, {
        xtype: 'combobox',
        fieldLabel: 'Campus',
        name: 'contactCC',
        id: 'contactCC',
        allowBlank: false,
        store: 'addMapComboStore',
        displayField: 'description',
        valueField: 'code',
        editable: false
    }, {
        xtype: 'combobox',
        fieldLabel: 'Category',
        name: 'ccCategory',
        id: 'ccCategory',
        allowBlank: false,
        editable: false,
        displayField: 'description',
        valueField: 'code',
        store: 'addEmergencyStore'
    }, {
        xtype: 'textfield',
        fieldLabel: 'Name',
        name: 'addEName',
        id: 'addEName',
        allowBlank: false
    },{
        xtype: 'textareafield',
        fieldLabel: 'Comments',
        name: 'addEComments',
        id: 'addEComments',
        allowBlank: true
    }, 
    {
        xtype: 'textfield',
        fieldLabel: 'Phone',
        name: 'addEPhone',
        id: 'addEPhone',
        regex: new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$"),
        regexText: 'Phone number must be in 123-456-7894 format only',
        allowBlank: false
    }, {
        xtype: 'textfield',
        fieldLabel: 'Address',
        name: 'addEAddress',
        id: 'addEAddress',
        allowBlank: true
    }, {
        xtype: 'textfield',
        fieldLabel: 'Email',
        name: 'addEcEmail',
        id: 'addEcEmail',
        regex: new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$"),
        regexText: 'Email must be in xyz@sample.com format only',
        allowBlank: true
    }, {
        xtype: 'textfield',
        fieldLabel: 'Image Url',
        name: 'addEPictureUrl',
        id: 'addEPictureUrl',
        allowBlank: true
    }, {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            cls:'buttonClr',
            action: 'submit',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            cls:'buttonClr',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            cls:'buttonClr',
            action: 'back',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});