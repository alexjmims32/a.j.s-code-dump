Ext.define('login.view.userEditMenuView', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.userEditMenuView',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editUser',
                iconCls: 'icon-edit'
            },
//            {
//                xtype: 'menuitem',
//                id:'deleteContact',
//                text: 'Delete',
//                iconCls: 'icon-delete'
//            }
            ]
        });
 
        me.callParent(arguments);
    }
});