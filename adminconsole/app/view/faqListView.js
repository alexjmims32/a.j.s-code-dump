Ext.define('login.view.faqListView', {
    extend:'Ext.panel.Panel',
    alias:'widget.faqListView',
    title:'<table height=10><tr><td width=10%><img src="images/n-logo2.png" height="70" width=550></td><td  class="headerTitle"><font size="5"><b>eNtourage Admin Console</b></font></td></tr></table>',
    titleAlign:'center',
    padding:'0 0 40 0',
     height:'100%',
    width:'100%',
    overflowX:'hidden',
    overflowY:'auto',
    bodyBorder:false,
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 150 50 150',
        style: {
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'faqGridView',
        flex:1
    }
    ]     
});