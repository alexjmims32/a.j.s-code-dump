Ext.define('login.view.createModuleView', {
    extend:'login.view.customTitleView',
    titleAlign:'center',
    padding:'0 0 40 0',
    height:'100%',
    width:'100%',
    overflowX:'hidden',
    overflowY:'auto',
    bodyBorder:false,
//    bodyStyle:{
//        background:'#ffc',
//        padding : '10 0 0 20'
//    },
    layout:{
        type:'table',
        columns:1,
        align:'center',
        tableAttrs: {
            style: {                 
                width: '100%',
                align:'center'                
            }
        },
        tdAttrs:{
            width: '100%',
            align:'center' 
        }
    },   
    items:[
    {
        margin:'50 150 50 150',
        style: {
//            borderColor: 'green',
            borderStyle: 'solid',
            borderWidth:'1px'            
        },
        xtype:'addFeedModuleView',
        flex:1
    }
    ]     
});