Ext.define('login.view.editLinkView',{
    extend:'Ext.panel.Panel',
    alias:'widget.editLinkView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Edit Link</b></font></center>',
        padding:20
    },
    {
        fieldLabel: 'Url',
        id: 'eFdlUrl',
        name: 'eFdlUrl',
        allowBlank: false
    },   
    {
        xtype: 'combobox',
        fieldLabel: 'Format',
        id: 'eFdlFormat',
        name: 'eFdlFormat',        
        allowBlank: false,
        store: 'addModuleStore',
        displayField: 'description',
        valueField: 'code',
        editable: false
    },
    {
        fieldLabel: 'Title',
        id: 'eFdlTitle',
        name: 'eFdlTitle',
        allowBlank: false
    },
    {
        fieldLabel: 'Icon',
        id: 'eFdlIcon',
        name: 'eFdlIcon',
        allowBlank: false
    },    
    {
        fieldLabel: 'Type',
        id: 'eFdlType',
        name: 'eFdlType',
        allowBlank: false,
        readOnly:true
    },
    {
        fieldLabel: 'Campus Code',
        id: 'eFdlCampCode',
        name: 'eFdlCampCode',
        allowBlank: false,
        readOnly:true
    },
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            cls:'buttonClr',
            action: 'reset',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});