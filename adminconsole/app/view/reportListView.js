Ext.define('login.view.reportListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.reportListView',
    store:'reportListStore',    
    titleAlign:'center',
    id:'list',    
    scrollable: 'both',
    minWidth: 300,
    columnLines: true,    
  
    margin:'20',
    style: {
        //        borderColor: 'green',
        borderStyle: 'solid',
        borderWidth:'1px',
        overflowX:'hidden',
        overflowY:'auto'
        
        
    },
    columns:[ 
    {
        header:'Refresh Type',
        dataIndex:'refreshType',
        flex:1,
        cls:'gridClr'
    }
    ,
    {
        header:'Job Name',
        dataIndex:'refreshName',
        flex:1,
        cls:'gridClr'
    },   
    {
        header:'Table/Module To Refresh',
        dataIndex:'toRefresh',
        flex:1,
        cls:'gridClr'

    },  
    {
        header:'Start Date',
        dataIndex:'startDate',
        flex:1,
        cls:'gridClr'
    },
    
    {
        header:'End Date',
        dataIndex:'endDate',
        flex:1,
        cls:'gridClr'
    },   
    {
        header:'Refresh Frequency',
        dataIndex:'frequency',
        flex:1,
        cls:'gridClr'

    },  
    {
        header:'Refresh Interval',
        dataIndex:'interval',
        flex:1,
        cls:'gridClr'
    },
    
    {
        header:'Schedule Time',
        dataIndex:'scheduleTime',
        flex:1,
        cls:'gridClr'
    },
    {
        xtype: 'gridcolumn',
        header: 'Enable',
        dataIndex: 'enabled',
        editable:false,
        cls:'gridClr',
        renderer: function(val) {
            console.log(val);
            if(val.toLowerCase() == 'true') return '<input type="checkbox" checked=true>';
            else return '<input type="checkbox">';
        }
    }

    ],
    
   
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,

        items:[
        {
            xtype:'button',
            cls:'buttonClr',
            minWidth: 50,
            margin: 4,
            text:'Add',
            action:'ADD',
            buttonAlign:'right'
        },      
        ]
    }
    ]  

})
;