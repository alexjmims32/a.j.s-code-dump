Ext.define('login.view.roleView',{
    extend:'Ext.tab.Panel',
    alias: 'widget.roleView',
    title:'<center><font size="4"><b>eNtourage Admin Console</b></font></center>',
    minTabWidth:'145',
    items:[     
    {
        id:'facultyTab',
        title: 'Main Module',
        xtype:'mainModuleView',
        width:250
    }
    ]
        
});
