Ext.define('login.view.faqGridView', {
    extend:'Ext.grid.Panel',
    //    columnLines: true,
    alias:'widget.faqGridView',
    store:'faqStore', 
    padding:'0 0 28 0',
    margin:'20',
    style: {
        borderStyle: 'solid',
        borderWidth:'1px',
        maxHeight:420,
        overflowX:'hidden',
        overflowY:'auto'
    },
    columns:[
    {
        header:'Question',
        dataIndex:'question',
        flex:2,
        cls:'gridClr'
    },

    {
        header:'Answer',
        dataIndex:'answer',
        flex:2,
        cls:'gridClr'
    }
    ]
    ,
    dockedItems:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'5 0 5 10',
        border:false,
        items:[
        {
            xtype:'button',
            minWidth: 50,
            margin: 4,
            text:'Add',
            cls:'buttonClr',
            action:'ADD',
            buttonAlign:'right'
        }
       
        ]
    }
    ]
});