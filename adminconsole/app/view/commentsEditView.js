Ext.define('login.view.commentsEditView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.commentsEditView',
    scrollable: 'both',
    minWidth: 300,
    items: [{
        xtype: 'label',
        html: '<center><font class="headerTitle"><b>Comments</b></font></center>',
        padding: 20
    }, {
        xtype: 'textfield',
        fieldLabel: 'Table Name',
        name: 'tName',
        id: 'tName',
        width:500,        
        editable: false,
         readOnly:true
    }, {
        xtype: 'textareafield',
        fieldLabel: 'Comments',
        width:500,
        name: 'tComments',
        id: 'tComments'        
    },{
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            cls:'buttonClr',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }
    ]
});