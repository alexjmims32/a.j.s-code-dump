Ext.define('login.view.columnCommentsEditView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.columnCommentsEditView',
    scrollable: 'both',
    minWidth: 300,
    items: [{
        xtype: 'label',
        html: '<center><font class="headerTitle"><b>Column Comments</b></font></center>',
        padding: 20
    }, {
        xtype: 'textfield',
        fieldLabel: 'Column Name',
        name: 'cName',
        id: 'cName',
        width:300,        
        editable: false,
         readOnly:true
    }, {
        xtype: 'textareafield',
        fieldLabel: 'Comments',
        width:300,
        name: 'cComments',
        id: 'cComments'        
    },{
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [{
            xtype: 'button',
            margin: 4,
            text: 'Submit',
            action: 'submit',
            cls:'buttonClr',
            name: 'submit',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Reset',
            action: 'reset',
            cls:'buttonClr',
            name: 'reset',
            minWidth: 50
        }, {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }
    ]
});