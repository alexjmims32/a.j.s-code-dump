Ext.define('login.view.clientDetailsView', {
    extend:'Ext.grid.Panel',
    alias:'widget.clientDetailsView',
    store:'clientDetailStore',
    title:'<font size="4"><b>Available Configurations For the Client</b></font>',
    titleAlign:'center',
    padding:'0 0 40 0',
//    minHeight:420,
    bodyBorder:false,

    columns:[
        {
            header:'Client',
            dataIndex:'client',
            flex:1
        },

        {
            header:'DB HostName',
            dataIndex:'dbHostname',
            flex:1
        },
        {
            header:'DB Port',
            dataIndex:'dbPort',
            flex:3
        }
        ,
        {
            header:'DB Sid',
            dataIndex:'dbSid',
            flex:1
        },
        {

            header:'DB UserName',
            dataIndex:'dbUsername',
            flex:1
        },
        {
            header:'DB Password',
            dataIndex:'dbPassword',
            inputfield:'password',
            flex:1

        },
        {
            header:'LDAP HostName',
            dataIndex:'ldapHostname',
            flex:1
        },
        {
            header:'LDAP port',
            dataIndex:'ldapPort',
            flex:1
        },
        {
            header:'Protocol',
            dataIndex:'ldapProtocol',
            flex:1
        },
        {
            header:'DN UserId',
            dataIndex:'dnUserID',
            flex:1
        },
        {
            header:'DN Password',
            dataIndex:'dnPassword',
            flex:1
        },
        {
            header:'Version',
            dataIndex:'version',
            flex:1
        }

    ]
    ,
    dockedItems:[
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'5 0 5 10',
            border:false,

            items:[
                {
                    xtype:'tbspacer',
                    width:'20'
                },
                {
                    xtype:'button',
                    padding:'2 50',
                    text:'Back',
                    action:'back',
                    buttonAlign:'left'
                },
                {
                    xtype:'tbspacer',
                    flex:1
                }
            ]
        }
    ]


})
;