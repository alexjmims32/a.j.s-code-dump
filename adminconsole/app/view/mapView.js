Ext.define('login.view.mapView', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.mapView',
	store: 'mapStore',
	scrollable: 'both',
	minWidth: 300,
	titleAlign: 'center',
	columnLines: true,
	id: 'list',
	padding: '0 0 28 0',
	margin: '20',
	style: {
		borderStyle: 'solid',
		borderWidth: '1px',
		//        maxHeight:420,
		overflowX: 'hidden',
		overflowY: 'auto'
	},
	columns: [{
			header: 'Campus',
			dataIndex: 'campusCode',
			flex: 1,
			cls: 'gridClr'
		},

		{
			header: 'Building Code',
			dataIndex: 'buildingCode',
			flex: 1,
			cls: 'gridClr'
		},

		{
			header: 'Building Name',
			dataIndex: 'buildingName',
			flex: 2,
			cls: 'gridClr'
		},

		{
			header: 'Building Description',
			dataIndex: 'buildingDesc',
			flex: 2,
			cls: 'gridClr'
		},

		{
			header: 'Phone',
			dataIndex: 'phone',
			flex: 1,
			cls: 'gridClr'
		},


		{
			header: 'Email',
			dataIndex: 'email',
			flex: 1,
			cls: 'gridClr'
		}, {
			header: 'Image Url',
			dataIndex: 'imgUrl',
			flex: 1,
			cls: 'gridClr'
		}, {
			header: 'Address',
			dataIndex: 'address',
			flex: 3,
			cls: 'gridClr'
		}, {
			header: 'Longitude',
			dataIndex: 'longitude',
			flex: 1,
			cls: 'gridClr'
		}, {
			header: 'Latitude',
			dataIndex: 'latitude',
			flex: 1,
			cls: 'gridClr'
		}, {
			header: 'Category',
			dataIndex: 'category',
			renderer: function(category) {

				var categories = category.split(',');
				var name = '';
				for (var i = 0; i < categories.length; i++) {
					var store = Ext.getStore('categoryStore');
					var index = store.find('categoryId', categories[i]); // find it's parent
					if (name == '') {
						name = store.getAt(index).get('category');
					} else {
						name = name + ', ' + store.getAt(index).get('category');
					}
				}

				return name;

			},
			flex: 3,
			cls: 'gridClr'
		},


	],

	dockedItems: [{
		xtype: 'panel',
		layout: {
			type: 'hbox',
			align: 'strech'
		},
		padding: '5 0 5 10',
		border: false,
		items: [{
			xtype: 'button',
			minWidth: 50,
			margin: 4,
			text: 'Add',
			cls: 'buttonClr',
			action: 'ADD',
			buttonAlign: 'right'
		}, {
			xtype: 'tbspacer',
			width: '400'
		}, ]
	}]
});