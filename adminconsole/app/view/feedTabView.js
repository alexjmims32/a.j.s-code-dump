Ext.define('login.view.feedTabView', {
    extend:'Ext.panel.Panel',
    alias:'widget.feedTabView',
    titleAlign:'center',
    padding:'0 0 40 0',
    //    minHeight:420,
    bodyBorder:false,
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'moduleListView',
        flex:1
    },{
        xtype:'feedView',
        flex:4
    },{
        xtype:'feedLevel2View',
        id:'feedDetails',
        hidden:true,
        flex:4    
    }
    ]

        
       
});