Ext.define('login.view.moduleListView', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.moduleListView',
    store: 'treeStore',
    xtype:'treepanel',
    margin:'20 10 20 10',
    collapsed:false,
    border: 5,
    //    frame:true, 
    style: {
        //        borderColor: 'green',        
        borderStyle: 'solid',
        borderWidth:'1px'
    }
});
