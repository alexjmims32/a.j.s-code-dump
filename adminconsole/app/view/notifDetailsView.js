Ext.define('login.view.notifDetailsView',{
    extend:'Ext.panel.Panel',
    alias:'widget.notifDetailsView',
    scrollable: 'both',
    minWidth: 300,
    defaultType: 'textfield',
    items:[
    {
        xtype:'label',
        html:'<center><font size="5" class="headerTitle"><b>Notification Details</b></font></center>',
        padding:20
    },
    {
        xtype:'textfield',
        fieldLabel: 'User Name',
        readOnly:true,
        id: 'notVwUser',
        name: 'notVwUser',
        
        allowBlank: false
    },{
        xtype:'textareafield',
        fieldLabel: 'Message',
        readOnly:true,
        name: 'notVwMsg',
        id: 'notVwMsg',
        allowBlank: true
    },
    {
        fieldLabel: 'Delivery Method',
        readOnly:true,
        name: 'notVwDeliverMethod',
        id: 'notVwDeliverMethod',   
        allowBlank: true
    },{
        fieldLabel: 'Type',
        readOnly:true,
        name: 'notVwType',
        id: 'notVwType',
        allowBlank: true
    },
    {
        fieldLabel: 'Title',
        readOnly:true,
        name: 'notVwTitle',
        id: 'notVwTitle',       
        allowBlank: true
    },{
        
        fieldLabel: 'Due Date',
        readOnly:true,
        name: 'notVwDueDate',
        id: 'notVwDueDate',
        allowBlank: true
    },    
    {
        xtype: 'panel',
        layout: {
            type: 'hbox',
            align: 'center',    
            pack: 'center'
        },
        padding: '20',
        border: false,

        items: [
        {
            xtype: 'button',
            margin: 4,
            text: 'Back',
            action: 'back',
            cls:'buttonClr',
            name: 'back',
            minWidth: 50
        }
        ]
    }]

});