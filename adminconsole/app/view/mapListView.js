Ext.define('login.view.mapListView', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.mapListView',
	scrollable: 'both',
	minWidth: 300,
	defaultType: 'textfield',
	items: [{
		xtype: 'label',
		html: '<center><font size="5" class="headerTitle"><b>Map Details</b></font></center>',
		padding: 20
	}, {
		xtype: 'combobox',
		fieldLabel: 'Campus Code',
		name: 'eMapCC',
		id: 'eMapCC',
		allowBlank: true,
		store: 'addMapComboStore',
		displayField: 'description',
		valueField: 'code',
		editable: false
	}, {
		xtype: 'combobox',
		fieldLabel: 'Category',
		multiSelect: true,
		name: 'emapCategory',
		id: 'emapCategory',
		allowBlank: false,
		store: 'categoryStore',
		displayField: 'category',
		valueField: 'categoryId',
		editable: false
	}, {
		fieldLabel: 'Title',
		name: 'mapTitle',
		id: 'mapTitle',
		allowBlank: true,
		readOnly: true
	}, {
		fieldLabel: 'Building Code',
		name: 'mapBldgCode',
		id: 'mapBldgCode',
		allowBlank: true
	}, {
		fieldLabel: 'Building Name',
		name: 'mapBldgName',
		id: 'mapBldgName',
		allowBlank: true
	}, {
		xtype: 'textareafield',
		fieldLabel: 'Building Description',
		name: 'mapBldgDesc',
		id: 'mapBldgDesc',
		allowBlank: true
	}, {
		xtype: 'textareafield',
		fieldLabel: 'Address',
		name: 'editAddress',
		id: 'editAddress',
		allowBlank: true
	}, {
		fieldLabel: 'Phone',
		name: 'mapPhone',
		id: 'mapPhone',
		regex: new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$"),
		regexText: 'Phone number must in 123-456-7894 format only',
		allowBlank: true
	}, {
		xtype: 'textfield',
		fieldLabel: 'Email',
		id: 'mapEmail',
		name: 'mapEmail',
		regex: new RegExp("^[a-zA-Z][a-zA-Z0-9]+\@[a-zA-Z0-9]+[.][a-zA-Z]{2,3}$"),
		regexText: 'Email must in xyz@sample.com format only',
		allowBlank: true
	}, {
		fieldLabel: 'Image Url',
		name: 'mapImageUrl',
		id: 'mapImageUrl',
		allowBlank: true
	}, {
		fieldLabel: 'Latitude',
		name: 'mapLatitude',
		id: 'mapLatitude',
		allowBlank: true
	}, {
		fieldLabel: 'Longitude',
		name: 'mapLongitude',
		id: 'mapLongitude',
		allowBlank: true
	}, {
		xtype: 'panel',
		layout: {
			type: 'hbox',
			align: 'center',
			pack: 'center'
		},
		padding: '20',
		border: false,

		items: [{
			xtype: 'button',
			margin: 4,
			text: 'Submit',
			cls: 'buttonClr',
			action: 'submit',
			name: 'submit',
			minWidth: 50
		}, {
			xtype: 'button',
			margin: 4,
			text: 'Reset',
			cls: 'buttonClr',
			action: 'reset',
			name: 'reset',
			minWidth: 50
		}, {
			xtype: 'button',
			margin: 4,
			text: 'Back',
			cls: 'buttonClr',
			action: 'back',
			name: 'back',
			minWidth: 50
		}]
	}]

});