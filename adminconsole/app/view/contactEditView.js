Ext.define('login.view.contactEditView', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.contactEditView',
    width: 120,
 
    initComponent: function() {
        var me = this;
 
        Ext.applyIf(me, {
            items: [
            {
                xtype: 'menuitem',
                text: 'Edit',
                id:'editContact',
                iconCls: 'icon-edit'
            },
            {
                xtype: 'menuitem',
                id:'deleteContact',
                text: 'Delete',
                iconCls: 'icon-delete'
            },
            {
                xtype: 'menuitem',
                text: 'Move Up',
                id:'moveContactUp',
                iconCls: 'icon-up'
            },
            {
                xtype: 'menuitem',
                id:'moveContactDown',
                text: 'Move Down',
                iconCls: 'icon-down'
            }
            ]
        });
 
        me.callParent(arguments);
    }
});