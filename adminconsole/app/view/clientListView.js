Ext.define('login.view.clientListView', {
    extend:'Ext.grid.Panel',
    alias:'widget.clientListView',
    store:'clientListStore',
    title:'<font size="4"><b>Available Clients List</b></font>',
    titleAlign:'center',
    padding:'0 0 40 0',
//    minHeight:420,
    bodyBorder:false,

    columns:[
        {
            header:'Client',
            dataIndex:'client',
            flex:1
        },

        {
            header:'User Name',
            dataIndex:'pid',
            flex:1
        },
        {
            header:'Password',
            dataIndex:'pin',
            flex:1
        },
        {
            header:'Description',
            dataIndex:'description',
            flex:5
        }
    ]
    ,
    dockedItems:[
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'5 0 5 10',
            border:false,

            items:[
                  {
                    xtype:'button',
                    padding:'2 50',
                    text:'Back',
                    action:'back',
                    buttonAlign:'right'
                },
                {
                    xtype:'tbspacer',
                    flex:1
                },
                {
                    xtype:'button',
                    padding:'2 50',
                    text:'Add Client',
                    action:'addClient',
                    buttonAlign:'right'
                },
                {
                    xtype:'tbspacer',
                    width:20
                }
                

            ]
        }
    ]
});