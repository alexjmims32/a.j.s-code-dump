Ext.define('login.view.metaDataView', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.metaDataView',
    scrollable: 'both',
    minWidth: 300,
    overflowX:'hidden',
    overflowY:'auto',
    //    layout:{
    //        align:'center',
    //        type:'vbox'
    //    },    
    margin:'30 30 30 30',
    items: [
    {
        xtype: 'combobox',
        fieldLabel: 'Select Type',
        name: 'sType',
        id: 'sType',
        allowBlank: false,
        store: ['TABLE','COLUMN'],
        displayField: 'description',
        valueField: 'code',
        editable: false,
        listeners:{
            change: function(field,newValue,oldValue,eOpts){
                console.log(newValue);
                var lsStore=Ext.getStore('metaDataTableStore');
                    lsStore.getProxy().url =webserver + 'getTables';
                    
                if(newValue=='TABLE'){   
                    lsStore.load();
                    Ext.getCmp('columnList').setVisible(false);
                    Ext.getCmp('tableList').setVisible(true);
                    Ext.getCmp('sTable').setDisabled(true);                    
                }else{
                     Ext.getCmp('columnList').setVisible(true);
                    Ext.getCmp('tableList').setVisible(false);
                    Ext.getCmp('sTable').setDisabled(false);
                }
            }
        }
    }, 
    {
        xtype: 'combobox',
        fieldLabel: 'Select Table',
        name: 'sTable',
        id: 'sTable',
        allowBlank: false,
        editable: false,
        displayField: 'name',
        valueField: 'name',
        store: 'metaDataTableStore',
        listeners:{
            change: function(field,newValue,oldValue,eOpts){
                console.log(newValue);
                var lsStore=Ext.getStore('metaDataColumnsStore');
                lsStore.getProxy().url =webserver + 'getColumns?name='+newValue;
                lsStore.load();
                Ext.getCmp('columnList').setVisible(true);
                Ext.getCmp('tableList').setVisible(false);                    
                    
                
            }
        }
    },
    {
        xtype:'tableListView',
        id:'tableList'
    },
    {
        xtype:'columnListView',
        id:'columnList'
    }
    ]
});