Ext.define('login.model.updateUserRoleModel', {
    extend:'Ext.data.Model',
    fields:[
    {
        name:'privilegeCode',
        type:'string'
    },
    {
        name:'accessValue',
        type:'string'
    }
    ]
});