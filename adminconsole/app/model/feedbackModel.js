Ext.define('login.model.feedbackModel', {
	extend: 'Ext.data.Model',
	fields: [{
		name: 'feedbackId',
		type: 'string'
	}, {
		name: 'title',
		type: 'string'
	}, {
		name: 'email',
		type: 'string'
	}, {
		name: 'description',
		type: 'string'
	}]
});