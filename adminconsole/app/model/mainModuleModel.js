Ext.define('login.model.mainModuleModel', {
    extend:'Ext.data.Model',

    fields:[   
    {
        name:'code',
        type:'string'
    },    
    {
        name:'authRequired',
        type:'string'
    },
    {
        name:'description',
        type:'string'
    },
    {
        name:'position',
        type:'string'
    },
    {
        name:'showByDefault',
        type:'string'
    }
    ]
})