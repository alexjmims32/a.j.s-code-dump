Ext.define('login.model.clientListModel', {
    extend:'Ext.data.Model',

    fields:[
        {
            name:'client',
            type:'string'
        },
        {
            name:'pin',
            type:'string'
        },
        {
            name:'pid',
            type:'string'
        },
        {
            name:'description',
            type:'string'
        }
    ]
})