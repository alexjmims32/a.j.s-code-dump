Ext.define('login.model.feedModuleModel', {
	extend: 'Ext.data.Model',
	idProperty: 'tslid',
	fields: [{
		name: 'feedId',
		type: 'string'
	}, {
		name: 'parentID',
		type: 'string'
	}, {
		name: 'type',
		type: 'string'
	}, {
		name: 'link',
		type: 'string'
	}, {
		name: 'format',
		type: 'string'
	}, {
		name: 'moduleID',
		type: 'string'
	}, {
		name: 'text',
		mapping: 'name',
		type: 'string'

	}, {
		name: 'campusCode',
		type: 'string'
	}, {
		name: 'image',
		type: 'string'
	}, {
		name: 'leaf',
		type: 'boolean',
		defaultValue: false
	}],

	proxy: {
		type: 'ajax',
		actionMethods: {
			type: 'json',
			read: 'GET'
		},
		reader: {
			type: 'json',
			root: 'feederList'
		}
	}
});