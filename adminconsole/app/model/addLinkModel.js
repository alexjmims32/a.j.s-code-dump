Ext.define('login.model.addLinkModel', {
	extend: 'Ext.data.Model',

	fields: [{
		name: 'feedId',
		type: 'string'
	}, {
		name: 'status',
		type: 'string'
	}]
})