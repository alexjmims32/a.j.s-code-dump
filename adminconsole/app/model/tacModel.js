Ext.define('login.model.tacModel', {
    extend:'Ext.data.Model',
    fields:[
    {
        name:'id',
        type:'string'
    },
    {
        name:'campusCode',
        type:'string'
    },
    {
        name:'termAndConditions',
        type:'string'
    }
    ]
});