Ext.define('login.model.addMapCampusModel', {
    extend:'Ext.data.Model',
    fields:[
    {
        name:'code',
        type:'string'
    },
    {
        name:'description',
        type:'string'
    }
    ]
});