Ext.define('login.model.faqModel', {
	extend: 'Ext.data.Model',
	fields: [{
		name: 'question',
		type: 'string'
	}, {
		name: 'answer',
		type: 'string'
	}, {
		name: 'campusCode',
		type: 'string'
	}, {
		name: 'faqId',
		type: 'string'
	}]
});