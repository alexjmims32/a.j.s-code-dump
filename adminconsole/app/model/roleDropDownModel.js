Ext.define('login.model.roleDropDownModel', {
    extend:'Ext.data.Model',
    fields:[
    {
        name:'title',
        type:'string'
    },
    {
        name:'email',
        type:'string'
    },
    {
        name:'description',
        type:'string'
    }
    ]
});