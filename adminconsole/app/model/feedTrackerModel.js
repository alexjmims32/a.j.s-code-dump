Ext.define('login.model.feedTrackerModel', {
    extend: 'Ext.data.Model',
    //    idProperty: 'tslid',
    fields: [{
        name: 'feedId',
        type: 'string'
    }, {
        name: 'parentID',
        type: 'string'
    }, {
        name: 'type',
        type: 'string'
    }, {
        name: 'link',
        type: 'string'
    }, {
        name: 'format',
        type: 'string'
    }, {
        name: 'moduleID',
        type: 'string'
    }, {

        name: 'name',
        type: 'string'

    }, {
        name: 'image',
        type: 'string'
    }, {
        name: 'campusCode',
        type: 'string'
    }, {
        name: 'versionNo',
        type: 'string'

    }, {
        name: 'lastModifiedBy',
        type: 'string'
    }, {
        name: 'lastModifiedOn',
        type: 'string'
    }]

});