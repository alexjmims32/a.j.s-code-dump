Ext.define('login.model.loginModel', {
    extend:'Ext.data.Model',

    config:{
        fields:[
        {
            name:"accessFlag",
            type:'string'
        },
        {
            name:"privilegeCode",
            type:'string'
        },
        {
            name:"roleId",
            type:'string'
        },
        {
            name:"activeFlag",
            type:'string'
        }
        ]
    }
});