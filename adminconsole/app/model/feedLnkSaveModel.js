Ext.define('login.model.feedLnkSaveModel', {
    extend:'Ext.data.Model',

    fields:[
    {
        name:'parentID',
        type:'string'
    },
    {
        name:'moduleID',
        type:'string'
    },
    {
        name:'name',
        type:'string'
    },    
    {
        name:'leaf',
        type:'boolean'
    }
    ]
})