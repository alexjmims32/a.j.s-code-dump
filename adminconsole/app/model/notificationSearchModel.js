Ext.define('login.model.notificationSearchModel', {
    extend:'Ext.data.Model',

    fields:[
    {
        name:'id',
        type:'string'
    },
    {
        name:'firstName',
        type:'string'
    },
    {
        name:'lastName',
        type:'string'
    },
    {
        name:'middleName',
        type:'string'
    }
    ]
})