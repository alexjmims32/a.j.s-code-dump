Ext.define('login.model.categoryModel', {
	extend: 'Ext.data.Model',

	fields: [{
		name: 'categoryId',
		type: 'string'
	}, {
		name: 'category',
		type: 'string'
	}, {
		name: 'showByDefault',
		type: 'string'
	}]
})