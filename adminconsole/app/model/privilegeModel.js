Ext.define('login.model.privilegeModel', {
    extend:'Ext.data.Model',
    fields:[
    {
        name:'privilegeCode',
        type:'string'
    },
    {
        name:'accessFlag',
        type:'string'
    }]
});