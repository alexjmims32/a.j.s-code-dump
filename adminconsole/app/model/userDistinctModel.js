Ext.define('login.model.userDistinctModel', {
    extend:'Ext.data.Model',

    fields:[
    {
        name:'username',
        type:'string'
    },    
    {
        name:'firstName',
        type:'string'
    },
    {
        name:'lastName',
        type:'string'
    },
    {
        name:'password',
        type:'string'
    },
    {
        name:'active',
        type:'string'
    },{
        name:'privilegeList',
        model:'login.model.privilegeModel'
    }
    
    ]
})