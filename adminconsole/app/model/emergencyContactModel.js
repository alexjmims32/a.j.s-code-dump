Ext.define('login.model.emergencyContactModel', {
	extend: 'Ext.data.Model',

	fields: [{
		name: 'contactId',
		type: 'string'
	}, {
		name: 'name',
		type: 'string'
	}, {
		name: 'phone',
		type: 'string'
	}, {
		name: 'address',
		type: 'string'
	}, {
		name: 'email',
		type: 'string'
	}, {
		name: 'picture_url',
		type: 'string'
	}, {
		name: 'campus',
		type: 'string'
	}, {
		name: 'category',
		type: 'string'
	}, {
		name: 'comments',
		type: 'string'
	}, {
		name: 'sequenceNumber',
		type: 'string'
	}]
})