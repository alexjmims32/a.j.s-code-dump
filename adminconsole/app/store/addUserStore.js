Ext.define('login.store.addUserStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.userModel',
    alias: 'addUserStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});