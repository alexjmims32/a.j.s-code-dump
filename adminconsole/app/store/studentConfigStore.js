Ext.define('login.store.studentConfigStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.studentConfigModel',
    alias: 'studentConfigStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'roleList.role',
            totalProperty: 'numrows'
        }
    }

});