Ext.define('login.store.configListStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateModel',
    alias: 'configListStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'configList.config',
            totalProperty: 'numrows'
        }
    }

});