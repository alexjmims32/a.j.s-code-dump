Ext.define('login.store.updateLinkStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateLinkModel',
    alias: 'updateLinkStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});