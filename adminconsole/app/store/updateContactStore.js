Ext.define('login.store.updateContactStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateContactModel',
    alias: 'updateContactStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});