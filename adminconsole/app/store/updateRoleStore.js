Ext.define('login.store.updateRoleStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.studentConfigUpdateModel',
    alias: 'updateRoleStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'status',
            totalProperty: 'numrows'
        }
    }

});