Ext.define('login.store.addNotificationStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.notificationModel',
    alias: 'addLinkStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});