Ext.define('login.store.userDistinctStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.userDistinctModel',
    alias: 'userStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'admin'
        }
    }
});