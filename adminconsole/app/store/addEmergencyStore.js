Ext.define('login.store.addEmergencyStore', {
    extend: 'Ext.data.Store',    
    alias: 'addEmergencyStore',
    fields: ['code', 'description'],
    data : [
    //    {
    //        "code":"IMPNUM", 
    //        "description":"Department Phone Numbers"
    //    },
    //    {
    //        "code":"EMR", 
    //        "description":"Local Emergency Information"
    //    }

    {
        "code":"IMPNUM", 
        "description":"Important Numbers"
    },
    {
        "code":"EMR", 
        "description":"Emergency Contacts"
    }
    ]

});