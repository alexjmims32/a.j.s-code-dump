Ext.define('login.store.feedLevel2Store', {
    extend: 'Ext.data.TreeStore',
    model: 'login.model.feedViewModel',
    alias: 'feedLevel2Store',    
    proxy: {
        type: 'ajax',        
        reader: {
            root: 'feederList',
            type: 'json'
        }
    }
});