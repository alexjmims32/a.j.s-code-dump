Ext.define('login.store.feedDetailsSaveStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.feedLnkSaveModel',
    alias: 'feedDetailsSaveStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'feederList',
            totalProperty: 'numrows'
        }
    }

});