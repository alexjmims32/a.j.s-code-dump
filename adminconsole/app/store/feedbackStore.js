Ext.define('login.store.feedbackStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.feedbackModel',
    alias: 'feedbackStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'feedback',
            totalProperty: 'numrows'
        }
    }

});