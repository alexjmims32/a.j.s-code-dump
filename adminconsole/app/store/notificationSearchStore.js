Ext.define('login.store.notificationSearchStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.notificationSearchModel',
    alias: 'notificationSearchStore',

    autoLoad: false,
    'pageSize':pageSize,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            totalProperty: 'totalRecords',
            root: 'notifications'         
        }
    }

});