Ext.define('login.store.mapUpdateStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateModel',
    alias: 'mapUpdateStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'status',
            totalProperty: 'numrows'
        }
    }

});