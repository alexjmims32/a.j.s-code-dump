Ext.define('login.store.helpStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.addLinkModel',
    alias: 'helpStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});