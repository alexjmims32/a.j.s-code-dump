Ext.define('login.store.clientListStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.clientListModel',
    alias: 'clientListStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'clientList',
            totalProperty: 'numrows'
        }
    }

});