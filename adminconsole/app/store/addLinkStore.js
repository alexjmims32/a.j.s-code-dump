Ext.define('login.store.addLinkStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.addLinkModel',
    alias: 'addLinkStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});