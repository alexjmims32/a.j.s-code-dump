Ext.define('login.store.activeFlagStore', {
    extend: 'Ext.data.Store',    
    alias: 'activeFlagStore',
    fields: ['code', 'description'],
    data : [
    {
        "code":"Y", 
        "description":"Yes"
    },
    {
        "code":"N", 
        "description":"No"
    }
    ]

});