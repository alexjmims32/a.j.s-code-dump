Ext.define('login.store.userStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.userModel',
    alias: 'userStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'admin'
        }
    }
});