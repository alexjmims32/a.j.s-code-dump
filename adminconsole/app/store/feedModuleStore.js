Ext.define('login.store.feedModuleStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.feedModuleModel',
    alias: 'feedModuleStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'feederList',
            totalProperty: 'numrows'
        }
    }

});