Ext.define('login.store.loginStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.loginModel',
    alias: 'loginStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'privilegesList.privilege'
        }
    }
});