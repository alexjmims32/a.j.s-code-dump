Ext.define('login.store.updateUserRoleStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateUserRoleModel',
    alias: 'updateUserRoleStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'admin.adminRole',
            totalProperty: 'numrows'
        }
    }

});