Ext.define('login.store.welcomeStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateModel',
    alias: 'welcomeStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json'
//            root: 'status'
        }
    }

});