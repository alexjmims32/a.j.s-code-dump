//Ext.define('login.store.feedTrackerStore', {
//    extend: 'Ext.data.Store',
//    model: 'login.model.feedTrackerModel',
//    alias: 'feedTrackerStore',
//
//    autoLoad: false,
//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'feederList'
//        }
//    }
//
//});

Ext.define('login.store.feedTrackerStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.feedTrackerModel',
    alias: 'feedTrackerStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'feederList',
            totalProperty: 'numrows'
        }
    }

});