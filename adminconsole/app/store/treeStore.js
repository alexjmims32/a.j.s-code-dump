Ext.define('login.store.treeStore', {
    extend: 'Ext.data.TreeStore',
    model: 'login.model.feedModuleModel',
    alias: 'treeStore',  

    root: {
        expand: true,
        text: 'Modules',
        id: null
    },

    autoLoad: false
});