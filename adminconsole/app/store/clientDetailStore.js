Ext.define('login.store.clientDetailStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateModel',
    alias: 'clientDetailStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'configList',
            totalProperty: 'numrows'
        }
    }

});