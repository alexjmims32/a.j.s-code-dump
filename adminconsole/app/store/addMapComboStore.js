Ext.define('login.store.addMapComboStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.addMapCampusModel',
    alias: 'addMapComboStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'campuses'
        }
    }
    
});