Ext.define('login.store.updateFaqStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateContactModel',
    alias: 'updateFaqStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});