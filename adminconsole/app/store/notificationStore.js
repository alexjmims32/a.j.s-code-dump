Ext.define('login.store.notificationStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.notificationModel',
    alias: 'notificationStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'notificationList.notification',
            totalProperty: 'numrows'
        }
    }

});