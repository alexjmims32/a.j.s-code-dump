Ext.define('login.store.feedViewStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.feedViewModel',
    alias: 'feedViewStore',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'feederList'
        }
    }

});