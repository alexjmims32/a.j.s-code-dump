Ext.define('login.store.tacStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.tacModel',
    alias: 'tacStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});