Ext.define('login.store.exceptionStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.exceptionModel',
    alias: 'exceptionStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'exceptions',
            totalProperty: 'numrows'
        }
    }

});