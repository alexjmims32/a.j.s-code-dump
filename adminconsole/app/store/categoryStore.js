Ext.define('login.store.categoryStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.categoryModel',
    alias: 'categoryStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'category'
        }
    }
    
});