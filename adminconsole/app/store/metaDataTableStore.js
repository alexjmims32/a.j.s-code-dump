Ext.define('login.store.metaDataTableStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.tableModel',
    alias: 'metaDataTableStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'tables'
        }
    }
    
});