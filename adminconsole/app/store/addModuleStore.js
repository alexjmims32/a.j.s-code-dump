Ext.define('login.store.addModuleStore', {
    extend: 'Ext.data.Store',    
    alias: 'addModuleStore',
    fields: ['code', 'description'],
    data : [
    {
        "code":"xml", 
        "description":"XML"
    },   
    {
        "code":"html", 
        "description":"HTML"
    },
    {
        "code":"jsp", 
        "description":"JSP"
    },
    {
        "code":"GDATA", 
        "description":"Youtube"
    }
    ]

});