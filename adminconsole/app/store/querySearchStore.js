Ext.define('login.store.querySearchStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.notificationSearchModel',
    alias: 'querySearchStore',

    autoLoad: false,
    //    'pageSize':pageSize,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            //            totalProperty: 'totalRecords',
            root: 'notifications'         
        }
    }

});