Ext.define('login.store.helpListStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.helpListModel',
    alias: 'helpListStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});