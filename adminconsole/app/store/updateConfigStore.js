Ext.define('login.store.updateConfigStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateModel',
    alias: 'updateConfigStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'configList',
            totalProperty: 'numrows'
        }
    }

});