Ext.define('login.store.updateUserStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.userModel',
    alias: 'updateUserStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});