Ext.define('login.store.reportListStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.reportListModel',
    alias: 'reportListStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'reports',
            totalProperty: 'numrows'
        }
    }

});