Ext.define('login.store.metaDataColumnsStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.tableModel',
    alias: 'metaDataColumnsStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'tables'
        }
    }
    
});