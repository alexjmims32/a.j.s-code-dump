Ext.define('login.store.reportDropDownStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.addMapCampusModel',
    alias: 'reportDropDownStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'enumerationList'
        }
    }
//    fields: ['code', 'description'],
//    data : [
//    {
//        "code":"ACC", 
//        "description":"ACCOUNTS"
//    }
//    ]
    
});