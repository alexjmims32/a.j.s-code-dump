Ext.define('login.store.mainModuleStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.mainModuleModel',
    alias: 'mainModuleStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'modules',
            totalProperty: 'numrows'
        }
    }

});