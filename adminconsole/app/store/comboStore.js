Ext.define('login.store.comboStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.roleDropDownModel',
    alias: 'comboStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'feedback'
        }
    }
    
});
