Ext.define('login.store.faqStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.faqModel',
    alias: 'faqStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'faqList',
            totalProperty: 'numrows'
        }
    }

});