Ext.define('login.store.updateModuleStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.studentConfigUpdateModel',
    alias: 'updateModuleStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'status',
            totalProperty: 'numrows'
        }
    }

});