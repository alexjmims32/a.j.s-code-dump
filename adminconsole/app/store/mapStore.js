Ext.define('login.store.mapStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.mapModel',
    alias: 'mapStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'map',
            totalProperty: 'numrows'
        }
    }

});