Ext.define('login.store.updateFeedbackStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateLinkModel',
    alias: 'updateFeedbackStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});