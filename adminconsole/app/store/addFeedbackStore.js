Ext.define('login.store.addFeedbackStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.updateContactModel',
    alias: 'addFeedbackStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: '',
            totalProperty: 'numrows'
        }
    }

});