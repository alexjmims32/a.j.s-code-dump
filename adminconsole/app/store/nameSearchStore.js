Ext.define('login.store.nameSearchStore', {
    extend: 'Ext.data.Store',    
    alias: 'nameSearchStore',
    fields: ['code', 'description'],
    data : [   
    {
        "code":"ALL", 
        "description":"All"
    },
    {
        "code":"FACULTY", 
        "description":"Faculty"
    },
    {
        "code":"EMPLOYEE", 
        "description":"Staff"
    },
    {
        "code":"STUDENT", 
        "description":"Students"
    }
    ]

});