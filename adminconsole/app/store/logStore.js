Ext.define('login.store.logStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.logModel',
    alias: 'logStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'logs',
            totalProperty: 'numrows'
        }
    }

});