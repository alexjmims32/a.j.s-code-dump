Ext.define('login.store.emergencyContactStore', {
    extend: 'Ext.data.Store',
    model: 'login.model.emergencyContactModel',
    alias: 'emergencyContactStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'emergencyContacts',
            totalProperty: 'numrows'
        }
    }

});