Ext.define('login.controller.metaDataController', {
    extend:'Ext.app.Controller',
    stores:[
    'reportListStore',
    'reportDropDownStore',
    'metaDataTableStore',
    'metaDataColumnsStore'
    ],
    models:[
    'reportListModel'
    ],
    views:[
    'reportListView',
    'addReportView',
    'createReportView',
    'reportEditMenu',
    'editReportView',
    'updateReportView',
    'commentsEditView',
    'tableEditMenu',
    'columnEditMenu',
    'columnCommentsEditView',
    'updateCommentsOnColumn',
    'updateCommentsOnTable'
    ],
    init:function () {
        this.control({
            'tableListView':{
                beforeactivate:this.tableList,
                itemcontextmenu : this.treeRightClick
            },
            'columnListView':{               
                itemcontextmenu : this.treeRightClick
            },
            refs : [                
            {
                ref : 'tableListView',
                selector : 'tableEditMenu'
            },
            {
                ref : 'columnListView',
                selector : 'columnEditMenu'
            } 
            ], 
            'tableEditMenu menuitem[text=Edit]':{
                click:this.editView
            }, 
            'columnEditMenu menuitem[text=Edit]':{
                click:this.columnComments
            }, 
            'commentsEditView button[action=submit]':{
                click:this.commentsUpdate
            }, 
            'commentsEditView button[action=back]':{
                click:this.tabForm
            },            
            'columnCommentsEditView button[action=back]':{
                click:this.tabForm
            },
            'columnCommentsEditView button[action=submit]':{
                click:this.colCommentsUpdate
            },
            'columnCommentsEditView button[action=reset]':{
                click:this.setActualColumnData
            },
            'commentsEditView button[action=reset]':{
                click:this.setActualTableData
            }
                 
        }
        );
    },
    setActualColumnData:function () {  
        Ext.getCmp('cComments').setValue(selectRecord.data.comments);
        Ext.getCmp('cName').setValue(selectRecord.data.name);
        
    },
    setActualTableData:function () {  
        Ext.getCmp('tComments').setValue(selectRecord.data.comments);
        Ext.getCmp('tName').setValue(selectRecord.data.name);
        
    },
    tabForm:function () {  
        
        var tab = Ext.getCmp('tabView');
        viewport.add(tab);
        viewport.layout.setActiveItem(tab);
    },
    tableList:function () {
        var lsStore=Ext.getStore('metaDataTableStore');       
        lsStore.getProxy().url =webserver + 'getTables';
                  
    },
    editView:function(){
        
        if (typeof login.view.updateCommentsOnTable == 'function') {
            login.view.updateCommentsOnTable = Ext.create(login.view.updateCommentsOnTable);
        }
        var twoView = login.view.updateCommentsOnTable;   
        var viewName= Ext.getCmp('tName');
        var newViewName=viewName.setValue(selectRecord.data.name);
        
        var tableComments= Ext.getCmp('tComments');
        var comments=tableComments.setValue(selectRecord.data.comments);
        
        viewport.add(twoView);
        viewport.layout.setActiveItem(twoView);
    },
    columnComments:function(){
        
        if (typeof login.view.updateCommentsOnColumn == 'function') {
            login.view.updateCommentsOnColumn = Ext.create(login.view.updateCommentsOnColumn);
        }
        var twoView = login.view.updateCommentsOnColumn;   
        var viewName= Ext.getCmp('cName');
        var newViewName=viewName.setValue(selectRecord.data.name);
        
        var tableComments= Ext.getCmp('cComments');
        var comments=tableComments.setValue(selectRecord.data.comments);
        
        viewport.add(twoView);
        viewport.layout.setActiveItem(twoView);
    },
    commentsUpdate:function () 
    {
        var store = Ext.getStore('updateContactStore');
        
        var name= Ext.getCmp('tName').getValue();
        var tableComments= Ext.getCmp('tComments').getValue();
        store.getProxy().url = webserver + 'commentsOnTable?&name=' + name +'&comments=' + encodeURIComponent(tableComments);
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('updateContactStore');
            var status = store1.getProxy().getReader().rawData;
            if (status == 'SUCCESS') {
                Ext.Msg.alert('  ', 'Comments updated successfully.', function () {
                    var tab = Ext.getCmp('tabView');
                    viewport.add(tab);
                    viewport.layout.setActiveItem(tab); 
                    //                        viewport.layout.setActiveItem(login.view.tabView);   
                    var conStore = Ext.getStore('metaDataTableStore');
                    conStore.load();
                });
            }else{
                Ext.Msg.alert('  ', 'FAIL');  
            }
        }
        store.load();
                  
    },
    colCommentsUpdate:function () 
    {
        var store = Ext.getStore('updateContactStore');
        
        var table= Ext.getCmp('sTable').getValue();
        var column= Ext.getCmp('cName').getValue();
        var name=table+'.'+column;
        
        var tableComments= Ext.getCmp('cComments').getValue();
        store.getProxy().url = webserver + 'commentsOnColumns?&name=' + name +'&comments=' + encodeURIComponent(tableComments);
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('updateContactStore');
            var status = store1.getProxy().getReader().rawData;
            if (status == 'SUCCESS') {
                Ext.Msg.alert('  ', 'Comments updated successfully.', function () {
                    var tab = Ext.getCmp('tabView');
                    viewport.add(tab);
                    viewport.layout.setActiveItem(tab); 
                    //                        viewport.layout.setActiveItem(login.view.tabView);   
                    var conStore = Ext.getStore('metaDataColumnsStore');
                    conStore.load();
                });
            }else{
                Ext.Msg.alert('  ', 'FAIL');  
            }
        }
        store.load();
                  
    },
    treeRightClick : function(view, record, item, index, e) {
        //stop the default action
        e.stopEvent();
        selectIndex=index;
        selectRecord=record;  
        var name= Ext.getCmp('sType').getValue();
        if(name=='TABLE'){
            if (typeof login.view.tableEditMenu == 'function') {
                login.view.tableEditMenu = Ext.create(login.view.tableEditMenu);
            }
            var module = login.view.tableEditMenu;       
            Ext.getCmp('editTableComments').setVisible(true);
        }else{
            if (typeof login.view.columnEditMenu == 'function') {
                login.view.columnEditMenu = Ext.create(login.view.columnEditMenu);
            }
            var module = login.view.columnEditMenu; 
            Ext.getCmp('editcolumnComments').setVisible(true); 
        }
    
        
                
        module.showAt(e.getXY());
    }
});