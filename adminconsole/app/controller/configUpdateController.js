Ext.define('login.controller.configUpdateController', {
    extend:'Ext.app.Controller',

    stores:[
        'updateConfigStore',
        'configListStore'
    ],
    models:[
        'updateModel'
    ],

    views:[

        'updateConfigView'

    ]


});