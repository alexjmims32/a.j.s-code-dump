Ext.define('login.controller.userController', {
    extend:'Ext.app.Controller',

    stores:[  
    'userStore',
    'addUserStore',
    'updateUserStore',
    'comboStore',
    'updateUserRoleStore',
    'activeFlagStore',
    'userDistinctStore'
    ],

    models:[
    //        'loginModel'
    ],

    views:[
    'userListView',
    'addUserView',
    'editUserView',
    'userDetailsView',
    'passwordView',
    'updateUserView',
    'userEditMenuView',
    'createUserView',
    ],

    init:function () {
        this.control({
            'userListView':{
                beforeactivate:this.userList,
                //                itemdblclick:this.editUser
                itemcontextmenu : this.treeRightClick
            },
            'userListView button[action=ADD]':{
                click:this.userDetail
            },
            'addUserView button[action=submit]':{
                click:this.addUser
            },
            'addUserView button[action=reset]':{
                click:this.resetDetail
            },
            'addUserView button[action=back]':{
                click:this.mainScreen
            },
            'userDetailsView button[action=submit]':{
                click:this.userUpdate
            },
            'userDetailsView button[action=reset]':{
                click:this.resetEdit
            },
            'userDetailsView button[action=back]':{
                click:this.mainScreen
            },
            'passwordView button[action=submit]':{
                click:this.newPassword
            },
            'passwordView button[action=back]':{
                click:this.mainScreen
            },
            refs : [{
                //reference to the country window
                ref : 'updateUserView',
                selector : 'userEditMenuView'
            }
            ],
            'userEditMenuView menuitem[text=Edit]':{
                click:this.editUser
            }
           
        });
    },
    passwordChange:function () {          
        if (typeof login.view.passwordView == 'function') {
            login.view.passwordView = Ext.create(login.view.passwordView);
        }
        var addConView = login.view.passwordView;
        viewport.add(addConView);
        viewport.layout.setActiveItem(addConView); 
        Ext.getCmp('changePassword').reset();
        Ext.getCmp('confirmPassword').reset();       
    },
    newPassword:function () {        
       
        var store = Ext.getStore('updateUserStore');
        var username=Ext.getCmp('pwUsername').getValue();
        var newPassword=Ext.getCmp('changePassword').getValue();     
        var confirmPassword=Ext.getCmp('confirmPassword').getValue();
        
        if(newPassword=='' || confirmPassword==''){
            Ext.Msg.alert(' ','Password and Confirm Password are mandatory.' );
        }else{
            if(newPassword == confirmPassword){   
                var encCred = username + ':' + newPassword;
                var enCodePassword=Base64.encode(encCred);
                store.getProxy().url = webserver + 'changePassword?userId=' +username +'&password='+enCodePassword;
                store.getProxy().headers = {
                    Authorization:authString
                };
                store.getProxy().afterRequest = function () {
                    myMask.hide();
                    var store1 = Ext.getStore('updateUserStore');
                    var status = store1.getProxy().getReader().rawData;
                    if (status.status == 'SUCCESS') {
                        Ext.Msg.alert('   ', 'Password updated successfully.', function () { 
                            var tab = Ext.getCmp('tabView');
                            viewport.add(tab);
                            viewport.layout.setActiveItem(tab);
                            var listStore = Ext.getStore('userStore');
                            listStore.load();
                            
                            Ext.getCmp('changePassword').reset();
                            Ext.getCmp('confirmPassword').reset();
                        });
                    }else{
                        Ext.Msg.alert('',status.status );
                    }
                
                }
                store.load();
            }else{
                Ext.Msg.alert('','The passwords are not match, please try again.' );
                Ext.getCmp('changePassword').reset();
                Ext.getCmp('confirmPassword').reset();
            }
        }
    },
    mainScreen:function () {  
        
        var tab = Ext.getCmp('tabView');
        viewport.add(tab);
        viewport.layout.setActiveItem(tab);
    },
    userList:function () {
        if(userAddFlag==false){
            var theSameButton = Ext.getCmp('userAdd');
            theSameButton.setVisible(false);
        }
        var store = Ext.getStore('userStore');
        store.getProxy().url = webserver + 'userList';
        store.getProxy().headers = {
            Authorization:authString
        };      
        store.load();
       
    },
    userDetail:function () {      
        if (typeof login.view.createUserView == 'function') {
            login.view.createUserView = Ext.create(login.view.createUserView);
        }
        var userDetailView = login.view.createUserView;
        if(campusCode=='SUBR'|| campusCode=='CCM' || campusCode=='SPSU'){
            Ext.getCmp('aTC').setVisible(false);
        } 
        viewport.add(userDetailView);
        viewport.layout.setActiveItem(userDetailView);        
        
    },
    
    editUser:function (grid, record) {
        if (typeof login.view.updateUserView == 'function') {
            login.view.updateUserView = Ext.create(login.view.updateUserView);
        }
        var twoView = login.view.updateUserView;                       
        var userDetailView = login.view.userDetailsView;
        if(campusCode=='UT' || campusCode=='SUBR'){
            Ext.getCmp('passwordId').setVisible(false);
            Ext.getCmp('vwPassword').setVisible(false);
            Ext.getCmp('vwFirstName').setVisible(false);
            Ext.getCmp('vwLastName').setVisible(false);
            Ext.getCmp('vwActive').setVisible(false);
            
        }
        if(campusCode=='SUBR' || campusCode=='CCM' || campusCode=='SPSU'){
            Ext.getCmp('eTC').setVisible(false);
        } 
        var username = Ext.getCmp('vwUserName');
        var newUser = username.setValue(selectRecord.data.username);
        var password = Ext.getCmp('vwPassword');
        var decodePassword=Base64.decode(selectRecord.data.password);
        var passArray=decodePassword.split(':'); 
        console.log(passArray[1]);
        var newMsg = password.setValue(passArray[1]);
        var firstName = Ext.getCmp('vwFirstName');
        var newPhone = firstName.setValue(selectRecord.data.firstName);
        var lastName = Ext.getCmp('vwLastName');
        var newType = lastName.setValue(selectRecord.data.lastName);
        var roleCheck = selectRecord.data.privilegeList.privilege
        //        var check=Ext.getCmp('eCheck')
        Ext.getCmp('eNotification').setValue(false);
        Ext.getCmp('eFeed').setValue(false);
        Ext.getCmp('eHelpAbout').setValue(false);
        Ext.getCmp('eHelpFaq').setValue(false);
        Ext.getCmp('eContact').setValue(false);
        Ext.getCmp('eRoles').setValue(false);
        Ext.getCmp('eMap').setValue(false);
        Ext.getCmp('eFeedback').setValue(false);
        Ext.getCmp('eTC').setValue(false);
        Ext.getCmp('eUsers').setValue(false);
        for(i=0;i<roleCheck.length;i++){
            var access=roleCheck[i].accessFlag;
            var privCode =roleCheck[i].privilegeCode;
                       
            if (privCode=='NOTIFICATIONS'){
                if(access=='Y'){
                    Ext.getCmp('eNotification').setValue(true);
                }else{
                    Ext.getCmp('eNotification').setValue(false);
                }
                
            } 
        
            if (privCode=='FEEDS'){
                if(access=='Y'){
                    Ext.getCmp('eFeed').setValue(true);
                }else{
                    Ext.getCmp('eFeed').setValue(false);
                } 
            }
            if (privCode=='HELP-ABOUT'){
                if(access=='Y'){
                    Ext.getCmp('eHelpAbout').setValue(true);
                }else{
                    Ext.getCmp('eHelpAbout').setValue(false);
                } 
            }
        
            if (privCode=='HELP-FAQ'){
                if(access=='Y'){
                    Ext.getCmp('eHelpFaq').setValue(true);
                }else{
                    Ext.getCmp('eHelpFaq').setValue(false);
                } 
            }
            if (privCode=='EMERGENCYCONTACTS'){
                if(access=='Y'){
                    Ext.getCmp('eContact').setValue(true);
                }else{
                    Ext.getCmp('eContact').setValue(false);
                } 
            }
            if (privCode=='ROLES'){
                if(access=='Y'){
                    Ext.getCmp('eRoles').setValue(true);
                }else{
                    Ext.getCmp('eRoles').setValue(false);
                } 
            }
        
            if (privCode=='MAPS'){
                if(access=='Y'){
                    Ext.getCmp('eMap').setValue(true);
                }else{
                    Ext.getCmp('eMap').setValue(false);
                } 
            }
            if (privCode=='FEEDBACK'){
                if(access=='Y'){
                    Ext.getCmp('eFeedback').setValue(true);
                }else{
                    Ext.getCmp('eFeedback').setValue(false);
                } 
            }
            if (privCode=='TERMSANDCONDITIONS'){
                if(access=='Y'){
                    Ext.getCmp('eTC').setValue(true);
                }else{
                    Ext.getCmp('eTC').setValue(false);
                } 
            }
            if (privCode=='USERS'){
                if(access=='Y'){
                    Ext.getCmp('eUsers').setValue(true);
                }else{
                    Ext.getCmp('eUsers').setValue(false);
                } 
            }
        }
        var active = Ext.getCmp('vwActive');
        var newactive = active.setValue(selectRecord.data.active); 
                
        var passwordView = login.view.passwordView;                
        var userId=Ext.getCmp('pwUsername');
        var passUser = userId.setValue(selectRecord.data.username);                          
        
        viewport.add(twoView);
        viewport.layout.setActiveItem(twoView);
        
        
    },
    resetDetail:function () {
        Ext.getCmp('userFname').reset();
        Ext.getCmp('userLname').reset();
        Ext.getCmp('addUsername').reset();
        Ext.getCmp('check').reset();
        Ext.getCmp('userActiveFlag').reset();
        Ext.getCmp('userPassword').reset();
    },
    resetEdit:function () {
        Ext.getCmp('vwUserName').setValue(selectRecord.data.username);
        //        Ext.getCmp('vwPassword').setValue(selectRecord.data.password);
        Ext.getCmp('vwFirstName').setValue(selectRecord.data.firstName);
        Ext.getCmp('vwLastName').setValue(selectRecord.data.lastName);       
        Ext.getCmp('vwActive').setValue(selectRecord.data.active);
        Ext.getCmp('eCheck').reset();
        var roleCheck = selectRecord.data.privilegeList.privilege;
        for(i=0;i<roleCheck.length;i++){
            var access=roleCheck[i].accessFlag;
            var privCode =roleCheck[i].privilegeCode;
                       
            if (privCode=='NOTIFICATIONS'){
                if(access=='Y'){
                    Ext.getCmp('eNotification').setValue(true);
                }else{
                    Ext.getCmp('eNotification').setValue(false);
                }
                
            } 
        
            if (privCode=='FEEDS'){
                if(access=='Y'){
                    Ext.getCmp('eFeed').setValue(true);
                }else{
                    Ext.getCmp('eFeed').setValue(false);
                } 
            }
            if (privCode=='HELP-ABOUT'){
                if(access=='Y'){
                    Ext.getCmp('eHelpAbout').setValue(true);
                }else{
                    Ext.getCmp('eHelpAbout').setValue(false);
                } 
            }
        
            if (privCode=='HELP-FAQ'){
                if(access=='Y'){
                    Ext.getCmp('eHelpFaq').setValue(true);
                }else{
                    Ext.getCmp('eHelpFaq').setValue(false);
                } 
            }
            if (privCode=='EMERGENCYCONTACTS'){
                if(access=='Y'){
                    Ext.getCmp('eContact').setValue(true);
                }else{
                    Ext.getCmp('eContact').setValue(false);
                } 
            }
            if (privCode=='ROLES'){
                if(access=='Y'){
                    Ext.getCmp('eRoles').setValue(true);
                }else{
                    Ext.getCmp('eRoles').setValue(false);
                } 
            }
        
            if (privCode=='MAPS'){
                if(access=='Y'){
                    Ext.getCmp('eMap').setValue(true);
                }else{
                    Ext.getCmp('eMap').setValue(false);
                } 
            }
            if (privCode=='FEEDBACK'){
                if(access=='Y'){
                    Ext.getCmp('eFeedback').setValue(true);
                }else{
                    Ext.getCmp('eFeedback').setValue(false);
                } 
            }
            if (privCode=='TERMSANDCONDITIONS'){
                if(access=='Y'){
                    Ext.getCmp('eTC').setValue(true);
                }else{
                    Ext.getCmp('eTC').setValue(false);
                } 
            }
            if (privCode=='USERS'){
                if(access=='Y'){
                    Ext.getCmp('eUsers').setValue(true);
                }else{
                    Ext.getCmp('eUsers').setValue(false);
                } 
            }
        }
    },
    addUser:function () {
        var store = Ext.getStore('addUserStore');
        var firstName = Ext.getCmp('userFname').getValue();
        var lastName = Ext.getCmp('userLname').getValue();
        var username = Ext.getCmp('addUsername').getValue();
        username=username.toLowerCase();
        Ext.getCmp('addUsername').setValue(username);
        //        var role = Ext.getCmp('userRole').getValue();
        var active = Ext.getCmp('userActiveFlag').getValue();
        var password = Ext.getCmp('userPassword').getValue();
        
        var check=Ext.getCmp('check').getChecked();
        
        var checkedItems='';
        for(i=0;i<check.length;i++){
            var checkedItem=check[i].id;
            var activeValue=check[i].lastValue;
            
            if(activeValue==true){
                activeValue='Y';
            }else{
                activeValue='N';
            }
            if(checkedItem=='feed'){
                checkedItems+='FEEDS,2' 
            }
            if((checkedItem=='map')){
                checkedItems+='MAPS,3' 
            }
            if(checkedItem=='contact'){
                checkedItems+='EMERGENCYCONTACTS,4' 
            } 
            if(checkedItem=='notification'){
                checkedItems+='NOTIFICATIONS,1' 
            }             
            if(checkedItem=='helpAbout'){
                checkedItems+='HELP-ABOUT,6' 
            }
            if((checkedItem=='helpFaq')){
                checkedItems+='HELP-FAQ,8' 
            }
            if(checkedItem=='roles'){
                checkedItems+='ROLES,5' 
            } 
            if(checkedItem=='feedback'){
                checkedItems+='FEEDBACK,9' 
            } 
            if(checkedItem=='aTC'){
                checkedItems+='TERMSANDCONDITIONS,7' 
            } 
            if(checkedItem=='aUsers'){
                checkedItems+='USERS,10' 
            }
            checkedItems+=','+activeValue+';';  
        }
        
        var encCred = username + ':' + password
        var enCodePassword=Base64.encode(encCred);
        
        if(firstName != '' && lastName != '' && username != '' && active != '' && password != ''){                                 
            store.getProxy().url = webserver + 'addUser?firstName=' + firstName + '&lastName=' + lastName + '&username=' + username + '&role=' + checkedItems + '&active=' + active+'&password='+enCodePassword;
            store.getProxy().headers = {
                Authorization:authString
            };
            store.getProxy().afterRequest = function () {
                myMask.hide();
                var store1 = Ext.getStore('addUserStore');
                var status = store1.getProxy().getReader().rawData;
                if (status.status == 'SUCCESS') {
                    Ext.Msg.alert('   ', 'User added successfully.', function () {                        
                        var tab = Ext.getCmp('tabView');
                        viewport.add(tab);
                        viewport.layout.setActiveItem(tab);
                        var listStore = Ext.getStore('userStore');
                        listStore.load();
                        Ext.getCmp('userFname').reset();
                        Ext.getCmp('userLname').reset();
                        Ext.getCmp('addUsername').reset();
                        Ext.getCmp('check').reset();
                        Ext.getCmp('userActiveFlag').reset();
                        Ext.getCmp('userPassword').reset();                        
                    });
                }else{
                    Ext.Msg.alert('  ','User already exists' );
                    Ext.getCmp('addUsername').reset();
                    
                }
                
            }
            store.load();
        }else{
            Ext.Msg.alert('  ','User Name,First Name,Last Name,Active and Password fields are mandatory.');
        }
    },
    userUpdate:function () {
        var store = Ext.getStore('updateUserStore');
        var firstName = Ext.getCmp('vwFirstName').getValue();
        var lastName = Ext.getCmp('vwLastName').getValue();
        var username = Ext.getCmp('vwUserName').getValue();
        username=username.toLowerCase();
        Ext.getCmp('vwUserName').setValue(username);
        var active = Ext.getCmp('vwActive').getValue();   
        
        var check=Ext.getCmp('eCheck').getChecked();
        
        var checkedItems='';
        for(i=0;i<check.length;i++){
            var checkedItem=check[i].id;
            var activeValue=check[i].lastValue;
            
            if(activeValue==true){
                activeValue='Y';
            }else{
                activeValue='N';
            }
            if(checkedItem=='eFeed'){
                checkedItems+='FEEDS,2' 
            }
            if((checkedItem=='eMap')){
                checkedItems+='MAPS,3' 
            }
            if(checkedItem=='eContact'){
                checkedItems+='EMERGENCYCONTACTS,4' 
            } 
            if(checkedItem=='eNotification'){
                checkedItems+='NOTIFICATIONS,1' 
            }             
            if(checkedItem=='eHelpAbout'){
                checkedItems+='HELP-ABOUT,6' 
            }
            if((checkedItem=='eHelpFaq')){
                checkedItems+='HELP-FAQ,8' 
            }
            if(checkedItem=='eRoles'){
                checkedItems+='ROLES,5' 
            } 
            if(checkedItem=='eFeedback'){
                checkedItems+='FEEDBACK,9' 
            }
            if(checkedItem=='eTC'){
                checkedItems+='TERMSANDCONDITIONS,7' 
            } 
            if(checkedItem=='eUsers'){
                checkedItems+='USERS,10' 
            }
            checkedItems+=','+activeValue+';';  
        }
        
           
        store.getProxy().url = webserver + 'updateUser?firstName=' + firstName + '&lastName=' + lastName + '&username=' + username + '&active=' + active + '&role=' + checkedItems;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('updateUserStore');
            var status = store1.getProxy().getReader().rawData;
            if (status.status == 'SUCCESS') {
                Ext.Msg.alert('  ', 'User updated successfully.', function () {                        
                    var tab = Ext.getCmp('tabView');
                    viewport.add(tab);
                    viewport.layout.setActiveItem(tab);
                    var listStore = Ext.getStore('userStore');
                    listStore.load();                                                                   
                });
            }else{
                Ext.Msg.alert('  ','FAIL');
            }
                
        }
        store.load();
    },
    treeRightClick : function(view, record, item, index, e) {
        //stop the default action
        e.stopEvent();
        selectRecord=record;      
        if (typeof login.view.userEditMenuView == 'function') {
            login.view.userEditMenuView = Ext.create(login.view.userEditMenuView);
        }
        var module = login.view.userEditMenuView;      
        Ext.getCmp('editUser').setVisible(true);    
        
        module.showAt(e.getXY());         
    }


});
