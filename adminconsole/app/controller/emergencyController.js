Ext.define('login.controller.emergencyController', {
	extend: 'Ext.app.Controller',
	stores: [
		'emergencyContactStore',
		'updateContactStore',
		'addEmergencyStore'
	],
	models: [
		'emergencyContactModel'
	],
	views: [
		'emergencyContactsView',
		'emergencyContactView',
		'addEmergencyContactView',
		'tabView',
		'contactEditView',
		'updateContactView',
		'createContactView'
	],
	init: function() {
		this.control({
			'emergencyContactsView': {
				beforeactivate: this.contactList,
				//                select:this.contactView
				itemcontextmenu: this.treeRightClick
			},
			'emergencyContactView button[action=back]': {
				click: this.contactListView
			},
			'emergencyContactView button[action=submit]': {
				click: this.updateContact
			},
			'emergencyContactView button[action=reset]': {
				click: this.resetAll
			},
			'emergencyContactsView button[action=ADD]': {
				click: this.addContactView
			},
			'addEmergencyContactView button[action=submit]': {
				click: this.addEmergencyContact
			},
			'addEmergencyContactView button[action=reset]': {
				click: this.resetView
			},
			'addEmergencyContactView button[action=back]': {
				click: this.eContact
			},
			refs: [{
				//reference to the country window
				ref: 'emergencyContactView',
				selector: 'contactEditView'
			}],
			'contactEditView menuitem[text=Edit]': {
				click: this.contactView
			},
			'contactEditView menuitem[text=Delete]': {
				click: this.conactDelete
			},
			'contactEditView menuitem[text=Move Up]': {
				click: this.contactUp
			},
			'contactEditView menuitem[text=Move Down]': {
				click: this.contactDown
			}
		});
	},
	contactUp: function() {
		var store = Ext.getStore('updateContactStore');
		var id = selectRecord.data.contactId;
		var sequenceNumber = selectIndex - 1;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().url = webserver + 'contactUp?id=' + id + '&sequenceNumber=' + sequenceNumber;

		store.getProxy().afterRequest = function() {
			var conStore = Ext.getStore('emergencyContactStore');
			conStore.load();
		}
		store.load();
	},
	contactDown: function() {
		var store = Ext.getStore('updateContactStore');
		var id = selectRecord.data.contactId;
		var sequenceNumber = selectIndex + 1;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().url = webserver + 'contactDown?id=' + id + '&sequenceNumber=' + sequenceNumber;

		store.getProxy().afterRequest = function() {
			var conStore = Ext.getStore('emergencyContactStore');
			conStore.load();
		}
		store.load();
	},
	resetView: function() {
		Ext.getCmp('addEName').reset();
		Ext.getCmp('addEPhone').reset();
		Ext.getCmp('addEAddress').reset();
		Ext.getCmp('addEcEmail').reset();
		Ext.getCmp('addEPictureUrl').reset();
		Ext.getCmp('contactCC').reset();
		Ext.getCmp('ccCategory').reset();
	},
	resetAll: function() {
		Ext.getCmp('ECategory').setValue(selectRecord.data.category);
		Ext.getCmp('conName').setValue(selectRecord.data.name);
		Ext.getCmp('conPhone').setValue(selectRecord.data.phone);
		Ext.getCmp('conAdd').setValue(selectRecord.data.address);
		Ext.getCmp('conEmail').setValue(selectRecord.data.email);
		Ext.getCmp('pictureUrl').setValue(selectRecord.data.picture_url);
		Ext.getCmp('eContactCC').setValue(selectRecord.data.campus);
	},
	eContact: function() {

		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	addContactView: function() {
		var lsStore = Ext.getStore('addMapComboStore');
		lsStore.getProxy().headers = {
			Authorization: authString
		};
		lsStore.getProxy().url = webserver + 'getCampusDropDown?campusCode=' + campusCode;

		lsStore.getProxy().afterRequest = function() {
			if (typeof login.view.createContactView == 'function') {
				login.view.createContactView = Ext.create(login.view.createContactView);
			}
			var addConView = login.view.createContactView;
			viewport.add(addConView);
			viewport.layout.setActiveItem(addConView);
			Ext.getCmp('addEName').reset();
			Ext.getCmp('addEPhone').reset();
			Ext.getCmp('addEAddress').reset();
			Ext.getCmp('addEcEmail').reset();
			Ext.getCmp('addEPictureUrl').reset();
			Ext.getCmp('contactCC').reset();
			Ext.getCmp('ccCategory').reset();
		}
		lsStore.load();
	},
	contactList: function() {
		var store = Ext.getStore('emergencyContactStore');
		store.getProxy().url = webserver + 'universityContacts?';
		store.getProxy().headers = {
			Authorization: authString
		};
		store.load();
	},
	contactListView: function() {
		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	contactView: function() {
		var lsStore = Ext.getStore('addMapComboStore');
		lsStore.getProxy().headers = {
			Authorization: authString
		};
		lsStore.getProxy().url = webserver + 'getCampusDropDown?campusCode=' + campusCode;
		lsStore.getProxy().afterRequest = function() {
			if (typeof login.view.updateContactView == 'function') {
				login.view.updateContactView = Ext.create(login.view.updateContactView);
			}
			var emergencyContact = login.view.updateContactView;
			//        var id = Ext.getCmp('conId');
			//        var newId = selectRecord.data.contactId;
			var campus = Ext.getCmp('eContactCC');
			if (campus != '') {
				var newCampCode = campus.setValue(selectRecord.data.campus);
			} else {
				var newCampCode = campus.setValue('');
			}
			var name = Ext.getCmp('conName');
			var newName = name.setValue(selectRecord.data.name);
			var phone = Ext.getCmp('conPhone');
			var newPhone = phone.setValue(selectRecord.data.phone);
			var address = Ext.getCmp('conAdd');
			var newAdd = address.setValue(selectRecord.data.address);
			var email = Ext.getCmp('conEmail');
			var newEmail = email.setValue(selectRecord.data.email);
			var pictureUrl = Ext.getCmp('pictureUrl');
			var newPicUrl = pictureUrl.setValue(selectRecord.data.picture_url);

			var comments = Ext.getCmp('conComments');
			var newComments = comments.setValue(selectRecord.data.comments);

			var category = Ext.getCmp('ECategory');
			var newCategory = category.setValue(selectRecord.data.category);
			viewport.add(emergencyContact);
			viewport.layout.setActiveItem(emergencyContact);
		}
		lsStore.load();

	},

	updateContact: function() {
		var store = Ext.getStore('updateContactStore');
		var id = selectRecord.data.contactId;
		var campus = Ext.getCmp('eContactCC').getValue();
		var category = Ext.getCmp('ECategory').getValue();
		var name = Ext.getCmp('conName').getValue();
		var phone = Ext.getCmp('conPhone').getValue();
		var address = Ext.getCmp('conAdd').getValue();
		var email = Ext.getCmp('conEmail').getValue();
		var picUrl = Ext.getCmp('pictureUrl').getValue();
		var comments = Ext.getCmp('conComments').getValue();

		var emailReg = new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$");
		var phoneReg = new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$");
		if (name != '' && category != '' && phone != '') {
			if (!(email == '' || email == null) && !emailReg.test(email)) {
				Ext.Msg.alert('  ', 'Email is not valid format.')
			} else {
				if (!phoneReg.test(phone)) {
					Ext.Msg.alert('  ', 'Phone is not valid format.')
				} else {

					store.getProxy().url = webserver + 'editEmergencyContacts?id=' + id + '&name=' + name + '&phone=' + phone + '&address=' + address + '&email=' + email + '&pictureUrl=' + encodeURIComponent(picUrl) + '&campusCode=' + campus + '&lastModifiedBy=' + loginId + '&category=' + category + '&comments=' + comments;
					store.getProxy().headers = {
						Authorization: authString
					};
					store.getProxy().afterRequest = function() {
						myMask.hide();
						var store1 = Ext.getStore('updateContactStore');
						var status = store1.getProxy().getReader().rawData;
						if (status.status == 'SUCCESS') {
							Ext.Msg.alert('  ', 'Contact updated successfully.', function() {
								var tab = Ext.getCmp('tabView');
								viewport.add(tab);
								viewport.layout.setActiveItem(tab);
								//                        viewport.layout.setActiveItem(login.view.tabView);   
								var conStore = Ext.getStore('emergencyContactStore');
								conStore.load();
							});
						} else {
							Ext.Msg.alert('  ', 'FAIL');
						}
					}
					store.load();
				}
			}
		} else {
			Ext.Msg.alert('  ', 'Campus,Category,Name and Phone fields are mandatory.');
		}
	},
	addEmergencyContact: function() {
		var store = Ext.getStore('updateContactStore');
		var id = '';
		var name = Ext.getCmp('addEName').getValue();
		var phone = Ext.getCmp('addEPhone').getValue();
		var address = Ext.getCmp('addEAddress').getValue();
		var email = Ext.getCmp('addEcEmail').getValue();
		var picUrl = Ext.getCmp('addEPictureUrl').getValue();
		var category = Ext.getCmp('ccCategory').getValue();
		var campus = Ext.getCmp('contactCC').getValue();
		var comments = Ext.getCmp('addEComments').getValue();
		if (campus == null) {
			campus = '';
		}
		if (category == null) {
			category = '';
		}

		var emailReg = new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$");
		var phoneReg = new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$");
		if (name != '' && category != '' && phone != '' && campus != '') {
			if (!(email == '' || email == null) && !emailReg.test(email)) {
				Ext.Msg.alert('  ', 'Email is not valid format.')
			} else {
				if (!phoneReg.test(phone)) {
					Ext.Msg.alert('  ', 'Phone is not valid format.')
				} else {
					store.getProxy().url = webserver + 'editEmergencyContacts?id=' + id + '&name=' + name + '&phone=' + phone + '&address=' + address + '&email=' + email + '&pictureUrl=' + encodeURIComponent(picUrl) + '&campusCode=' + campus + '&lastModifiedBy=' + loginId + '&category=' + category + '&comments=' + comments;
					store.getProxy().headers = {
						Authorization: authString
					};
					store.getProxy().afterRequest = function() {
						myMask.hide();
						var store1 = Ext.getStore('updateContactStore');
						var status = store1.getProxy().getReader().rawData;
						if (status.status == 'SUCCESS') {
							Ext.Msg.alert('', 'Contact added successfully.', function() {
								var tab = Ext.getCmp('tabView');
								viewport.add(tab);
								viewport.layout.setActiveItem(tab);
								var listStore = Ext.getStore('emergencyContactStore');
								listStore.load();
								Ext.getCmp('addEName').reset();
								Ext.getCmp('addEPhone').reset();
								Ext.getCmp('addEAddress').reset();
								Ext.getCmp('addEcEmail').reset();
								Ext.getCmp('addEPictureUrl').reset();
								Ext.getCmp('ccCategory').reset();
								Ext.getCmp('addEComments').reset();
							});
						} else {
							Ext.Msg.alert('', status.status);
						}
					}
					store.load();
				}
			}
		} else {
			Ext.Msg.alert('  ', 'Campus,Category,Name and Phone fields are mandatory.');
		}

	},
	conactDelete: function(item, e) {
		var store = Ext.getStore('updateContactStore');

		store.getProxy().url = webserver + 'deleteContact?id=' + selectRecord.data.contactId;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			myMask.hide();
			var store1 = Ext.getStore('updateContactStore');
			var status = store1.getProxy().getReader().rawData;
			if (status.status == 'SUCCESS') {
				Ext.Msg.alert('', 'Contact deleted successfully.', function() {
					var tab = Ext.getCmp('tabView');
					viewport.add(tab);
					viewport.layout.setActiveItem(tab);
					var listStore = Ext.getStore('emergencyContactStore');
					listStore.load();
				});
			} else {
				Ext.Msg.alert('', status.status);
			}
		}
		store.load();
	},
	treeRightClick: function(view, record, item, index, e) {
		//stop the default action
		e.stopEvent();
		selectIndex = index;
		selectRecord = record;
		if (typeof login.view.contactEditView == 'function') {
			login.view.contactEditView = Ext.create(login.view.contactEditView);
		}
		var module = login.view.contactEditView;

		Ext.getCmp('editContact').setVisible(true);
		Ext.getCmp('deleteContact').setVisible(true);
		Ext.getCmp('moveContactUp').setVisible(false);
		Ext.getCmp('moveContactDown').setVisible(false);

		module.showAt(e.getXY());
	}

});