Ext.define('login.controller.feedbackController', {
	extend: 'Ext.app.Controller',
	stores: [
		'feedbackStore',
		'addFeedbackStore',
		'updateFeedbackStore'
	],
	models: [

	],
	views: [
		'feedbackListView',
		'addFeedbackView', 'editFeedbackMenu',
		'editFeedbackView', 'createFeedbackView',
		'updateFeedbackView'
	],
	init: function() {
		this.control({
			refs: [{
				//reference to the country window
				ref: 'feedbackListView',
				selector: 'editFeedbackView'
			}],
			'feedbackListView': {
				itemcontextmenu: this.treeRightClick,
				beforeactivate: this.feedbackDetail
			},
			'feedbackListView button[action=ADD]': {
				click: this.addFeedback
			},
			'addFeedbackView button[action=back]': {
				click: this.mainView
			},
			'editFeedbackView button[action=back]': {
				click: this.mainView
			},
			'editFeedbackView button[action=reset]': {
				click: this.resetEditView
			},
			'addFeedbackView button[action=reset]': {
				click: this.resetAddView
			},
			'addFeedbackView button[action=submit]': {
				click: this.addUserFeedback
			},
			'editFeedbackView button[action=submit]': {
				click: this.updateFeedback
			},
			'editFeedbackMenu menuitem[text=Edit]': {
				click: this.feedbackView
			},
			'editFeedbackMenu menuitem[text=Delete]': {
				click: this.feedbackDelete
			},
			'editFeedbackMenu menuitem[text=Move Up]': {
				click: this.feedbackUp
			},
			'editFeedbackMenu menuitem[text=Move Down]': {
				click: this.feedbackDown
			}
		});
	},
	feedbackUp: function() {
		var store = Ext.getStore('updateFeedbackStore');
		var id = selectRecord.data.feedbackId;
		var sequenceNumber = selectIndex - 1;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().url = webserver + 'feedbackUp?id=' + id + '&sequenceNumber=' + sequenceNumber;

		store.getProxy().afterRequest = function() {
			var conStore = Ext.getStore('feedbackStore');
			conStore.load();
		}
		store.load();
	},
	feedbackDown: function() {
		var store = Ext.getStore('updateFeedbackStore');
		var id = selectRecord.data.feedbackId;
		var sequenceNumber = selectIndex + 1;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().url = webserver + 'feedbackDown?id=' + id + '&sequenceNumber=' + sequenceNumber;

		store.getProxy().afterRequest = function() {
			var conStore = Ext.getStore('feedbackStore');
			conStore.load();
		}
		store.load();
	},
	resetEditView: function() {
		Ext.getCmp('editFdTitle').setValue(selectRecord.data.title);
		Ext.getCmp('editFdEmail').setValue(selectRecord.data.email);
		//        Ext.getCmp('editFbDescription').reset();
	},
	resetAddView: function() {
		Ext.getCmp('addFbTitle').reset();
		Ext.getCmp('addFbEmail').reset();
		//        Ext.getCmp('addfbDescription').reset();
	},
	mainView: function() {

		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	feedbackView: function() {
		if (typeof login.view.updateFeedbackView == 'function') {
			login.view.updateFeedbackView = Ext.create(login.view.updateFeedbackView);
		}
		var feedback = login.view.updateFeedbackView;
		var newId = selectRecord.data.feedbackId;
		var name = Ext.getCmp('editFdTitle');
		var newName = name.setValue(selectRecord.data.title);
		var email = Ext.getCmp('editFdEmail');
		var newEmail = email.setValue(selectRecord.data.email);
		viewport.add(feedback);
		viewport.layout.setActiveItem(feedback);
	},
	feedbackDetail: function() {
		var store = Ext.getStore('feedbackStore');
		store.getProxy().url = webserver + 'getFeedbackList';
		store.getProxy().headers = {
			Authorization: authString
		};
		store.load();
	},
	addFeedback: function() {
		if (typeof login.view.createFeedbackView == 'function') {
			login.view.createFeedbackView = Ext.create(login.view.createFeedbackView);
		}
		var addfeedback = login.view.createFeedbackView;
		viewport.add(addfeedback);
		viewport.layout.setActiveItem(addfeedback);
		Ext.getCmp('addFbTitle').reset();
		Ext.getCmp('addFbEmail').reset();
		//        Ext.getCmp('addfbDescription').reset();       
	},
	addUserFeedback: function() {
		var store = Ext.getStore('addFeedbackStore');
		var name = Ext.getCmp('addFbTitle').getValue();

		var email = Ext.getCmp('addFbEmail').getValue();
		var desc = '';
		var id = '';

		var emailReg = new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$");

		if (name != '' && email != '') {
			if (!emailReg.test(email)) {
				Ext.Msg.alert('  ', 'Email is not valid format.')
			} else {
				store.getProxy().url = webserver + 'addFeedback?id=' + id + '&title=' + name + '&email=' + email + '&description=' + desc;
				store.getProxy().headers = {
					Authorization: authString
				};
				store.getProxy().afterRequest = function() {
					myMask.hide();
					var store1 = Ext.getStore('addFeedbackStore');
					var status = store1.getProxy().getReader().rawData;
                    if (status.status == 'SUCCESS') {
						Ext.Msg.alert('', 'Feedback recipient created successfully.', function() {
							var tab = Ext.getCmp('tabView');
							viewport.add(tab);
							viewport.layout.setActiveItem(tab);
							var listStore = Ext.getStore('feedbackStore');
							listStore.load();
							Ext.getCmp('addFbTitle').reset();
							Ext.getCmp('addFbEmail').reset();
							//                        Ext.getCmp('addfbDescription').reset();

						});
					} else {
						Ext.Msg.alert('', status.status);
					}
				}
				store.load();
			}
		} else {
			Ext.Msg.alert('  ', 'Alias and Email fields are mandatory.');
		}
	},
	updateFeedback: function() {
		var store = Ext.getStore('updateFeedbackStore');
		var id = selectRecord.data.feedbackId;
		var name = Ext.getCmp('editFdTitle').getValue();
		var email = Ext.getCmp('editFdEmail').getValue();
		var desc = '';

		var emailReg = new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$");

		if (name != '' && email != '') {
			if (!emailReg.test(email)) {
				Ext.Msg.alert('  ', 'Email is not valid format.')
			} else {

				store.getProxy().url = webserver + 'addFeedback?id=' + id + '&title=' + name + '&email=' + email + '&description=' + desc;
				store.getProxy().headers = {
					Authorization: authString
				};
				store.getProxy().afterRequest = function() {
					myMask.hide();
					var store1 = Ext.getStore('updateFeedbackStore');
					var status = store1.getProxy().getReader().rawData;
					if (status.status == 'SUCCESS') {
						Ext.Msg.alert('  ', 'Feedback recipient updated successfully.', function() {
							var tab = Ext.getCmp('tabView');
							viewport.add(tab);
							viewport.layout.setActiveItem(tab);
							//                        viewport.layout.setActiveItem(login.view.tabView);   
							var conStore = Ext.getStore('feedbackStore');
							conStore.load();
						});
					}
				}
				store.load();
			}
		} else {
			Ext.Msg.alert('  ', 'Alias and Email fields are mandatory.');
		}
	},
	feedbackDelete: function(item, e) {
		var store = Ext.getStore('updateFeedbackStore');

		store.getProxy().url = webserver + 'deleteFeedback?id=' + selectRecord.data.feedbackId;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			myMask.hide();
			var store1 = Ext.getStore('updateFeedbackStore');
			var status = store1.getProxy().getReader().rawData;
			if (status.status == 'SUCCESS') {
				Ext.Msg.alert('', 'Feedback recipient deleted successfully.', function() {
					var tab = Ext.getCmp('tabView');
					viewport.add(tab);
					viewport.layout.setActiveItem(tab);
					var listStore = Ext.getStore('feedbackStore');
					listStore.load();
				});
			} else {
				Ext.Msg.alert('', status.status);
			}
		}
		store.load();
	},
	treeRightClick: function(view, record, item, index, e) {
		//stop the default action
		e.stopEvent();
		selectIndex = index;
		selectRecord = record;
		if (typeof login.view.editFeedbackMenu == 'function') {
			login.view.editFeedbackMenu = Ext.create(login.view.editFeedbackMenu);
		}
		var module = login.view.editFeedbackMenu;
		Ext.getCmp('editFeedback').setVisible(true);
		Ext.getCmp('deleteFeedback').setVisible(true);
		Ext.getCmp('moveFeedbackUp').setVisible(false);
		Ext.getCmp('moveFeedbackDown').setVisible(false);
		module.showAt(e.getXY());

		console.log(selectRecord);
	}
});