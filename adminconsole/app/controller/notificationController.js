Ext.define('login.controller.notificationController', {
	extend: 'Ext.app.Controller',
	stores: [
		'notificationStore',
		'addNotificationStore',
		'notificationSearchStore',
		'querySearchStore',
		'nameSearchStore',
		'addMapComboStore'
	],
	models: [
		'notificationModel'
	],
	views: [
		'addNotificationView',
		'notifDetailsView',
		'notificationEditMenuView',
		'searchPopupView',
		'notificationSearchListView',
		'notificationSearchView',
		'notificationSearchByQueryView',
		'querySearchView',
		'addQuerySearchView',
		'notificationView',
		'createNotificationView'
	],
	init: function() {
		this.control({
			'notificationListView': {
				beforeactivate: this.notificationList,
				itemcontextmenu: this.treeRightClick
			},
			'notificationListView button[action=ADD]': {
				click: this.notifAdd
			},
			'notificationListView button[action=back]': {
				click: this.tabView
			},
			'notifDetailsView button[action=back]': {
				click: this.tabView
			},

			'addNotificationView button[action=back]': {
				click: this.tabView
			},
			'addNotificationView button[action=submit]': {
				click: this.addNotification
			},
			'addNotificationView button[action=reset]': {
				click: this.resetNotification
			},
			'addNotificationView button[action=search]': {
				click: this.namePopup
			},
			'addNotificationView button[action=querySearch]': {
				click: this.queryPopup
			},
			refs: [{
				//reference to the country window
				ref: 'notificationView',
				selector: 'notificationEditMenuView'
			}],
			'notificationEditMenuView menuitem[text=View]': {
				click: this.notificationView
			},
			'searchPopupView button[action=find]': {
				click: this.getList
			},
			'notificationSearchView button[action=submit]': {
				click: this.nameSearch
			},
			'addQuerySearchView button[action=byQuery]': {
				click: this.getByQuery
			},
			'querySearchView button[action=submit]': {
				click: this.querySearch
			},
			'notificationSearchView button[action=back]': {
				click: this.notifAdd
			},
			'querySearchView button[action=back]': {
				click: this.notifAdd
			},
			'notificationSearchView button[action=export]': {
				click: this.exportPdf
			},
			'notificationSearchView button[action=clear]': {
				click: this.clearSearch
			}

		});
	},
	clearSearch: function() {
		//        Ext.getStore('notificationSearchStore').removeAll(); 
		var lsStore = Ext.getStore('notificationSearchStore');
		lsStore.removeAll();

		Ext.getCmp('paging').removeAll(true);

		Ext.getCmp('notifName').reset();
	},
	exportPdf: function(sm, selected, eOpts) {
		var lsStore = Ext.getStore('addMapComboStore');
		lsStore.getProxy().headers = {
			Authorization: authString
		};
		lsStore.getProxy().url = notifWebserver + 'pdf?method=GET' + '&name=studentId';

		lsStore.getProxy().afterRequest = function() {
			if (typeof login.view.notificationSearchView == 'function') {
				login.view.notificationSearchView = Ext.create(login.view.notificationSearchView);
			}
			var popup = login.view.notificationSearchView;
			viewport.add(popup);
			viewport.layout.setActiveItem(popup);

		}
		lsStore.load();

	},
	nameSearch: function(sm, selected, eOpts) {

		var grid = Ext.getCmp('notifGrid');
		var selModel = grid.getSelectionModel();
		var selected = selModel.getSelection();
		var userId = '';
		for (i = 0; i < selected.length; i++) {
			if (userId == '') {
				userId = selected[i].data.id;
			} else {
				userId = userId + ',' + selected[i].data.id;
			}
		}
		Ext.getCmp('notifPT').reset();
		Ext.getCmp('notifFName').reset();
		Ext.getCmp('notifLName').reset();
		var to = Ext.getCmp('notifTo');
		var toList = to.setValue(userId);
		var addNotif = login.view.createNotificationView;
		viewport.add(addNotif);
		viewport.layout.setActiveItem(addNotif);
		//        }
		//        lsStore.load();
		console.log(userId);



	},
	querySearch: function(sm, selected, eOpts) {

		var grid = Ext.getCmp('queryGrid');
		var selModel = grid.getSelectionModel();
		var selected = selModel.getSelection();
		var userId = '';
		for (i = 0; i < selected.length; i++) {
			if (userId == '') {
				userId = selected[i].data.id;
			} else {
				userId = userId + ',' + selected[i].data.id;
			}
		}
		Ext.getCmp('popQuery').reset();
		var to = Ext.getCmp('notifTo');
		var toList = to.setValue(userId);
		var addNotif = login.view.createNotificationView;
		viewport.add(addNotif);
		viewport.layout.setActiveItem(addNotif);

		console.log(userId);



	},
	setNotifPaging: function() {
		var paging = Ext.getCmp('appPaging');
		paging.bindStore('notificationSearchStore');
	},
	queryPopup: function() {
		if (typeof login.view.querySearchView == 'function') {
			login.view.querySearchView = Ext.create(login.view.querySearchView);
		}
		var popup = login.view.querySearchView;
		viewport.add(popup);
		viewport.layout.setActiveItem(popup);
	},
	namePopup: function() {
		// var lsStore = Ext.getStore('addMapComboStore');
		// lsStore.getProxy().headers = {
		// 	Authorization: authString
		// };
		// lsStore.getProxy().url = notifWebserver + 'getCampusDropDown?campusCode=' + campusCode;

		// lsStore.getProxy().afterRequest = function() {
		if (typeof login.view.notificationSearchView == 'function') {
			login.view.notificationSearchView = Ext.create(login.view.notificationSearchView);
		}
		var popup = login.view.notificationSearchView;
		viewport.add(popup);
		viewport.layout.setActiveItem(popup);

		// }
		// lsStore.load();
	},
	getByQuery: function() {
		var store = Ext.getStore('querySearchStore');
		var query = Ext.getCmp('popQuery').getValue();
		if (query != '') {
			store.getProxy().url = notifWebserver + 'searchByQuery?query=' + query;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				//                myMask.hide();
				var store1 = Ext.getStore('querySearchStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					if (typeof login.view.querySearchView == 'function') {
						login.view.querySearchView = Ext.create(login.view.querySearchView);
					}
					var tab = login.view.querySearchView;
					viewport.add(tab);
					viewport.layout.setActiveItem(tab);
				}
			}
			store.load();
		} else {
			Ext.Msg.alert('  ', 'Query field is mandatory.');
		}

	},
	getList: function() {
		var store = Ext.getStore('notificationSearchStore');
		var name = Ext.getCmp('notifPT').getValue();
		var firstName = Ext.getCmp('notifFName').getValue();
		var lastName = Ext.getCmp('notifLName').getValue();

		console.log(name);
		//        if(searchString==''){
		//            searchString='';
		//        }
		if (name == '' || name == null) {
			name = 'ALL'
		}

		if (firstName != '') {
			store.getProxy().url = notifWebserver + 'searchByAll?name=' + name + '&campusCode=' + campusCode + '&firstName=' + firstName + '&lastName=' + lastName;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				//                myMask.hide();
				var store1 = Ext.getStore('notificationSearchStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {

					if (typeof login.view.notificationSearchView == 'function') {
						login.view.notificationSearchView = Ext.create(login.view.notificationSearchView);
					}
					var tab = login.view.notificationSearchView;
					viewport.add(tab);
					viewport.layout.setActiveItem(tab);
					Ext.getCmp('notifPT').reset();

				}
			}
			store.load();
		} else {
			Ext.Msg.alert('  ', 'First Name field is mandatory.');
		}

	},
	notifAdd: function() {
		if (typeof login.view.createNotificationView == 'function') {
			login.view.createNotificationView = Ext.create(login.view.createNotificationView);
		}
		var notificationView = login.view.createNotificationView;
		viewport.add(notificationView);
		viewport.layout.setActiveItem(notificationView);
		//        Ext.getCmp('notifMessage').reset();
		//        Ext.getCmp('notifExpiryDate').reset();
		//        Ext.getCmp('notifType').reset();
		//        Ext.getCmp('notifTitle').reset();
		//        Ext.getCmp('notifDueDate').reset();
		//        Ext.getCmp('notifTo').reset();
	},
	tabView: function() {
		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	mainScreen: function() {
		if (typeof login.view.notificationListView == 'function') {
			login.view.notificationListView = Ext.create(login.view.notificationListView);
		}
		var notificationView = login.view.notificationListView;
		viewport.add(notificationView);
		viewport.layout.setActiveItem(notificationView);
	},
	addNotification: function() {
		var store = Ext.getStore('addNotificationStore');
		var newExpiryDate = '';
		var newDueDate = '';
		var message = Ext.getCmp('notifMessage').getValue();
		var expiryDate = Ext.getCmp('notifExpiryDate').getValue();
		if (expiryDate == null) {
			newExpiryDate = '';
		} else {
			var newDate = expiryDate.getDate();
			var newMonth = expiryDate.getMonth() + 1;
			if (newDate < 10) {
				newDate = '0' + newDate;
			}
			if (newMonth < 10) {
				newMonth = '0' + newMonth;
			}
			newExpiryDate = (newDate + '-' + newMonth + '-' + expiryDate.getFullYear() + ' 23:59:59');
		}
		var newNotifType = Ext.getCmp('notifType').getValue();

		var newNotifTitle = Ext.getCmp('notifTitle').getValue();
		var titleLength = newNotifTitle.length;
		var dueDate = Ext.getCmp('notifDueDate').getValue();
		if (dueDate == null) {
			newDueDate = '';
		} else {
			var newDue = dueDate.getDate();
			var newDueMonth = dueDate.getMonth() + 1;
			if (newDue < 10) {
				newDue = '0' + newDue;
			}
			if (newDueMonth < 10) {
				newDueMonth = '0' + newDueMonth;
			}
			newDueDate = (newDue + '-' + newDueMonth + '-' + dueDate.getFullYear() + ' 00:00:00');
		}
		var to = Ext.getCmp('notifTo').getValue();
		if (titleLength > 100) {
			Ext.Msg.alert('', 'Title field must be less than 100 chars.')
		} else {
			if (to != '' && newNotifTitle != '' && message != '' && newExpiryDate != '' && newDueDate != '' && newNotifType != null) {
				store.getProxy().url = notifWebserver + 'addAdminNotification?message=' + message + '&expirydate=' + newExpiryDate + '&type=' + newNotifType + '&title=' + newNotifTitle + '&dueDate=' + newDueDate + '&to=' + to;
				store.getProxy().headers = {
					Authorization: authString
				};
				store.getProxy().afterRequest = function() {
					myMask.hide();
					var store1 = Ext.getStore('addNotificationStore');
					var status = store1.getProxy().getReader().rawData;
					if (status.status = 'SUCCESS') {
						Ext.Msg.alert('', 'Notification added successfully.', function() {
							//                        if (typeof login.view.tabView == 'function') {
							//                            login.view.tabView = Ext.create(login.view.tabView);
							//                        }
							var tab = Ext.getCmp('tabView');
							viewport.add(tab);
							viewport.layout.setActiveItem(tab);
							var listStore = Ext.getStore('notificationStore');
							listStore.load();
							Ext.getCmp('notifMessage').reset();
							Ext.getCmp('notifExpiryDate').reset();
							Ext.getCmp('notifType').reset();
							Ext.getCmp('notifTitle').reset();
							Ext.getCmp('notifDueDate').reset();
							Ext.getCmp('notifTo').reset();


						});
					} else {
						Ext.Msg.alert('  ', 'FAIL');
					}

				}
				store.load();
			} else {
				Ext.Msg.alert('  ', 'All fields are mandatory.');
			}
		}
	},
	resetNotification: function() {
		Ext.getCmp('notifTo').reset();
		Ext.getCmp('notifMessage').reset();
		Ext.getCmp('notifExpiryDate').reset();
		Ext.getCmp('notifType').reset();
		Ext.getCmp('notifTitle').reset();
		Ext.getCmp('notifDueDate').reset();
	},
	notificationList: function() {
		var store = Ext.getStore('notificationStore');
		store.getProxy().url = notifWebserver + 'adminNotificationList?studentId=' + loginId;
		store.getProxy().headers = {
			Authorization: authString
		};
		if (serviceReq == 'N') {
			store.load();
			serviceReq = 'Y';
		}
		store.getProxy().afterRequest = function() {
			serviceReq = 'N';
		}
	},
	notificationView: function(grid, record) {
		if (typeof login.view.notificationView == 'function') {
			login.view.notificationView = Ext.create(login.view.notificationView);
		}
		var notificationDetails = login.view.notificationView;
		var user = Ext.getCmp('notVwUser');
		var newUser = user.setValue(selectRecord.data.userName);
		var message = Ext.getCmp('notVwMsg');
		var newMsg = message.setValue(selectRecord.data.message);
		var delMethod = Ext.getCmp('notVwDeliverMethod');
		var newPhone = delMethod.setValue(selectRecord.data.deliveryMethod);
		var type = Ext.getCmp('notVwType');
		var newType = type.setValue(selectRecord.data.type);
		var title = Ext.getCmp('notVwTitle');
		var newTitle = title.setValue(selectRecord.data.title);
		var dueDate = Ext.getCmp('notVwDueDate');
		var newDueDate = dueDate.setValue(selectRecord.data.dueDate);
		viewport.add(notificationDetails);
		viewport.layout.setActiveItem(notificationDetails);
	},
	treeRightClick: function(view, record, item, index, e) {
		//stop the default action
		e.stopEvent();
		selectRecord = record;
		if (typeof login.view.notificationEditMenuView == 'function') {
			login.view.notificationEditMenuView = Ext.create(login.view.notificationEditMenuView);
		}
		var module = login.view.notificationEditMenuView;
		Ext.getCmp('editNotif').setVisible(true);
		//        Ext.getCmp('deleteContact').setVisible(false);
		module.showAt(e.getXY());

		console.log(selectRecord);
	}
});