Ext.define('login.controller.addClientController', {
    extend:'Ext.app.Controller',
    stores:[
    'welcomeStore',
    'configListStore'
    ],
    models:[
    'updateModel'
    ],
    views:[
    'addClientView',
    'configListGridView',
    'tabView'
    ],
    init:function () {
        this.control({

            'addClientView button[action=back]':{
                click:this.mainScreen
            },
            'addClientView button[action=submit]':{
                click:this.addClient
            },
            'addClientView button[action=reset]':{
                click:this.resetClient
            }
        });
    },
    mainScreen:function () {
        if (typeof login.view.tabView == 'function') {
            login.view.tabView = Ext.create(login.view.tabView);
        }
        var clientListView = login.view.tabView;
        viewport.add(clientListView);
        viewport.layout.setActiveItem(clientListView);
    },
    addClient:function () {
        var store = Ext.getStore('configListStore');
        var client = Ext.getCmp('adclientName').getValue();
        var userName = Ext.getCmp('clientUserId').getValue();
        var password = Ext.getCmp('clientPassword').getValue();
        var desc = Ext.getCmp('clientDesc').getValue();
        
        if (client == '' || userName == '' || password== '') {
            Ext.Msg.alert('  ',' <div id="text-align">Client Name, User Name and Password are Mandatory</div>');

        } else {
            store.getProxy().url = webserver + 'addClient?client=' + client + '&pid=' + userName + '&pin=' + password + '&description=' +desc;
            store.getProxy().headers = {
                Authorization:authString
            };
            store.getProxy().afterRequest = function () {
                myMask.hide();
                var store1 = Ext.getStore('configListStore');
                var status = store1.getProxy().getReader().rawData;
                if (status.status != 'SUCCESS') {
                    Ext.Msg.alert('  ', 'Client already exist')
                } else {
                    Ext.Msg.alert('  ', 'Client Added successfully', function () {
                        var clientList = Ext.widget('clientListView');
                        viewport.add(clientList);
                        viewport.layout.setActiveItem(clientList);
                        Ext.getCmp('adclientName').reset();
                        Ext.getCmp('clientUserId').reset();
                        Ext.getCmp('clientPassword').reset();
                        Ext.getCmp('clientDesc').reset();
                        
                        var clientListStore = Ext.getStore('clientListStore');
                        clientListStore.getProxy().url = webserver + 'getClientList';
                        clientListStore.getProxy().headers = {
                            Authorization:authString
                        }
                        clientListStore.load();
                    });
                }

            }
            store.load();
        }

    },
    resetClient:function () {
        Ext.getCmp('adclientName').reset();
        Ext.getCmp('clientUserId').reset();
        Ext.getCmp('clientPassword').reset();
        Ext.getCmp('clientDesc').reset();
        
    }
});
