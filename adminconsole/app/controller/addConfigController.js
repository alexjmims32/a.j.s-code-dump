Ext.define('login.controller.addConfigController', {
    extend:'Ext.app.Controller',
    stores:[
    'welcomeStore',
    'configListStore'
    ],
    models:[
    'updateModel'
    ],
    views:[
    'addConfigView',
    'configListGridView'
    ],
    init:function () {
        this.control({

            'addConfigView button[action=back]':{
                click:this.mainScreen
            },
            'addConfigView button[action=submit]':{
                click:this.addConfig
            },
            'addConfigView button[action=reset]':{
                click:this.resetConfig
            }
        });
    },
    mainScreen:function () {
        if (typeof login.view.configListGridView == 'function') {
            login.view.configListGridView = Ext.create(login.view.configListGridView);
        }
        var configListView = login.view.configListGridView;
        viewport.add(configListView);
        viewport.layout.setActiveItem(configListView);
    },
    addConfig:function () {
        var store = Ext.getStore('configListStore');
        var client = Ext.getCmp('adclient').getValue();
        var newDbHostName = Ext.getCmp('adhostname').getValue();
        var newPort = Ext.getCmp('adport').getValue();
        var newSid = Ext.getCmp('adsid').getValue();
        var newUserName = Ext.getCmp('adusername').getValue();
        var newPassword = Ext.getCmp('adpassword').getValue();
        var newLdapHostName = Ext.getCmp('adldapHost').getValue();
        var newLdapPort = Ext.getCmp('adldapPort').getValue();
        var newProtocol = Ext.getCmp('adprotocal').getValue();
        var baseDN = Ext.getCmp('adbaseDN').getValue();
        var newUserId = Ext.getCmp('aduserId').getValue();
        var newDnPassword = Ext.getCmp('addnPassword').getValue();
        var vpnType = Ext.getCmp('advpnType').getValue();
        var vpnHost = Ext.getCmp('advpnHost').getValue();
        var vpnUser = Ext.getCmp('advpnUser').getValue();
        var vpnPassword = Ext.getCmp('advpnPassword').getValue();
        var testOs = Ext.getCmp('adtestOs').getValue();
        var testHostname = Ext.getCmp('adtestHostname').getValue();
        var testPort = Ext.getCmp('adtestPort').getValue();
        var testUserId = Ext.getCmp('adtestUserId').getValue();
        var testPassword = Ext.getCmp('adtestPassword').getValue();
        var testWS = Ext.getCmp('adtestWS').getValue();
        var testWSVersion = Ext.getCmp('adtestWSVersion').getValue();
        var testAS = Ext.getCmp('adtestAS').getValue();
        var testASVersion = Ext.getCmp('adtestASVersion').getValue();
        var leadName = Ext.getCmp('adleadName').getValue();
        var leadPhone = Ext.getCmp('adleadPhone').getValue();
        var leadEmail = Ext.getCmp('adleadEmail').getValue();
        var dbaName = Ext.getCmp('addbaName').getValue();
        var dbaPhone = Ext.getCmp('addbaPhone').getValue();
        var dbaEmail = Ext.getCmp('addbaEmail').getValue();
        var notes = Ext.getCmp('adnotes').getValue();
        
        

        if (client == '') {
            Ext.Msg.alert('  ',' <div id="text-align">Client Name is Mandatory</div>');

        } else {

            store.getProxy().url = webserver + 'addConfig?client=' + client + '&dbHostname=' + newDbHostName + '&dbPort=' + newPort + '&dbSid=' + newSid + '&dbUsername=' + newUserName + '&dbPassword=' + newPassword + '&ldapHostname=' + newLdapHostName + '&ldapPort=' + newLdapPort + '&ldapProtocol=' + newProtocol + '&baseDN=' + baseDN + '&dnUserID=' + newUserId + '&dnPassword=' + newDnPassword +'&vpnType='+vpnType+'&vpnHost='+vpnHost+'&vpnUser='+vpnUser+'&vpnPassword='+vpnPassword+'&testOs='+testOs+'&testHostname='+testHostname+'&testPort='+testPort+'&testUserId='+testUserId+'&testPassword='+testPassword+'&testWS='+testWS+'&testWSVersion='+testWSVersion+'&testAS='+testAS+'&testASVersion='+testASVersion+'&leadName='+leadName+'&leadPhone='+leadPhone+'&leadEmail='+leadEmail+'&dbaName='+dbaName+'&dbaPhone='+dbaPhone+'&dbaEmail='+dbaEmail+'&notes='+notes;
            store.getProxy().headers = {
                Authorization:authString
            };
            store.getProxy().afterRequest = function () {
                myMask.hide();
                var store1 = Ext.getStore('configListStore');
                var status = store1.getProxy().getReader().rawData;
                if (status.status != 'SUCCESS') {
                    Ext.Msg.alert('  ', 'Client already exist')
                } else {
                    Ext.Msg.alert('  ', 'Configuarations submitted successfully', function () {
                        var configlList = Ext.widget('configListGridView');
                        viewport.add(configlList);
                        viewport.layout.setActiveItem(configlList);
                        Ext.getCmp('adclient').reset();
                        Ext.getCmp('adhostname').reset();
                        Ext.getCmp('adport').reset();
                        Ext.getCmp('adsid').reset();
                        Ext.getCmp('adusername').reset();
                        Ext.getCmp('adpassword').reset();
                        Ext.getCmp('adldapHost').reset();
                        Ext.getCmp('adldapPort').reset();
                        Ext.getCmp('adprotocal').reset();
                        Ext.getCmp('adbaseDN').reset();
                        Ext.getCmp('aduserId').reset();
                        Ext.getCmp('addnPassword').reset();
                        Ext.getCmp('advpnType').reset();
                        Ext.getCmp('advpnHost').reset();
                        Ext.getCmp('advpnUser').reset();
                        Ext.getCmp('advpnPassword').reset();
                        Ext.getCmp('adtestOs').reset();
                        Ext.getCmp('adtestHostname').reset();
                        Ext.getCmp('adtestPort').reset();
                        Ext.getCmp('adtestUserId').reset();
                        Ext.getCmp('adtestPassword').reset();
                        Ext.getCmp('adtestWS').reset();
                        Ext.getCmp('adtestWSVersion').reset();
                        Ext.getCmp('adtestAS').reset();
                        Ext.getCmp('adtestASVersion').reset();
                        Ext.getCmp('adleadName').reset();
                        Ext.getCmp('adleadPhone').reset();
                        Ext.getCmp('adleadEmail').reset();
                        Ext.getCmp('addbaName').reset();
                        Ext.getCmp('addbaPhone').reset();
                        Ext.getCmp('addbaEmail').reset();
                        Ext.getCmp('adnotes').reset();
                        
                        var updateConfigStore = Ext.getStore('updateConfigStore');
                        updateConfigStore.getProxy().url = webserver + 'getConfigList?client='+client;
                        updateConfigStore.getProxy().headers = {
                            Authorization:authString
                        }
                        updateConfigStore.load();
                    });
                }

            }
            store.load();
        }

    },
    resetConfig:function () {
        Ext.getCmp('adclient').reset();
        Ext.getCmp('adhostname').reset();
        Ext.getCmp('adport').reset();
        Ext.getCmp('adsid').reset();
        Ext.getCmp('adusername').reset();
        Ext.getCmp('adpassword').reset();
        Ext.getCmp('adldapHost').reset();
        Ext.getCmp('adldapPort').reset();
        Ext.getCmp('adprotocal').reset();
        Ext.getCmp('aduserId').reset();
        Ext.getCmp('addnPassword').reset();
        Ext.getCmp('vpnType').reset();
        Ext.getCmp('vpnHost').reset();
        Ext.getCmp('vpnUser').reset();
        Ext.getCmp('vpnPassword').reset();
        Ext.getCmp('adtestOs').reset();
        Ext.getCmp('adtestHostname').reset();
        Ext.getCmp('adtestPort').reset();
        Ext.getCmp('adtestUserId').reset();
        Ext.getCmp('adtestPassword').reset();
        Ext.getCmp('adtestWS').reset();
        Ext.getCmp('adtestWSVersion').reset();
        Ext.getCmp('adtestAS').reset();
        Ext.getCmp('adtestASVersion').reset();
        Ext.getCmp('adleadName').reset();
        Ext.getCmp('adleadPhone').reset();
        Ext.getCmp('adleadEmail').reset();
        Ext.getCmp('addbaName').reset();
        Ext.getCmp('addbaPhone').reset();
        Ext.getCmp('addbaEmail').reset();
        Ext.getCmp('adnotes').reset();
        Ext.getCmp('adbaseDN').reset();
    }
});
