Ext.define('login.controller.helpController', {
	extend: 'Ext.app.Controller',

	stores: [
		'helpStore',
		'helpListStore',
		'faqStore',
		'updateFaqStore',
		'tacStore'
	],

	models: [
		//        'loginModel'
	],

	views: [
		'tabView',
		'helpView',
		'faqGridView',
		'addFaqView', 'createFaqView',
		'faqEditMenu',
		'updateFaqView',
		'editFaqView',
		'termAndConditionView'
	],

	init: function() {
		this.control({
			'helpView': {
				beforeactivate: this.helpList
			},
			'termAndConditionView': {
				beforeactivate: this.condition
			},
			'faqGridView': {
				beforeactivate: this.faqListView,
				itemcontextmenu: this.treeRightClick
			},
			'helpView button[action=submit]': {
				click: this.createHelp
			},
			'faqGridView button[action=ADD]': {
				click: this.addFaqView
			},
			'addFaqView button[action=back]': {
				click: this.tabView
			},
			'faqGridView button[action=back]': {
				click: this.tabView
			},
			'addFaqView button[action=submit]': {
				click: this.addFaq
			},
			'editFaqView button[action=back]': {
				click: this.tabView
			},
			'editFaqView button[action=reset]': {
				click: this.resetView
			},
			refs: [{
				ref: 'updateFaqView',
				selector: 'faqEditMenu'
			}],
			'faqEditMenu menuitem[text=Edit]': {
				click: this.edit
			},
			'faqEditMenu menuitem[text=Delete]': {
				click: this.faqDelete
			},
			'editFaqView button[action=submit]': {
				click: this.editFaq
			},
			'termAndConditionView button[action=submit]': {
				click: this.createTermsAndConditions
			}


		});
	},
	resetView: function() {
		Ext.getCmp('eQuestion').setValue(selectRecord.data.question);
		Ext.getCmp('eAnswer').setValue(selectRecord.data.answer);
	},
	addFaqView: function() {


		if (typeof login.view.createFaqView == 'function') {
			login.view.createFaqView = Ext.create(login.view.createFaqView);
		}
		var faq = login.view.createFaqView;
		viewport.add(faq);
		viewport.layout.setActiveItem(faq);

	},
	tabView: function() {
		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	faqListView: function() {
		var store = Ext.getStore('faqStore');
		store.getProxy().url = webserver + 'getFaqContent?campusCode=' + campusCode;
		store.getProxy().headers = {
			Authorization: authString
		};

		store.load();
	},

	condition: function() {
		var store = Ext.getStore('tacStore');
		store.getProxy().url = webserver + 'getTermsConditions?campusCode=' + campusCode;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			var record = store.getRange();
			var helpText = Ext.getCmp('termsAdnConditionText');
			var newHelpText = helpText.setValue(record[0].data.termAndConditions);

		}
		store.load();
	},
	helpList: function() {
		var store = Ext.getStore('helpListStore');
		store.getProxy().url = webserver + 'getHelpContent?campusCode=' + campusCode;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			var record = store.getRange();
			var helpText = Ext.getCmp('helpText');
			var newHelpText = helpText.setValue(record[0].data.areaText);

		}
		store.load();
	},
	createTermsAndConditions: function() {
		var store = Ext.getStore('helpStore');
		var conText = Ext.getCmp('termsAdnConditionText').getValue();

		if (conText != '') {

			store.getProxy().url = webserver + 'updateTermsConditions?termsConditions=' + encodeURIComponent(conText) + '&lastModifiedBy=' + loginId + '&campusCode=' + campusCode;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('helpStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('  ', 'Terms and Conditions content created successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
					});
				} else {
					Ext.Msg.alert('', 'Unable to process the save this information');
				}

			}
			store.load();

		} else {
			Ext.Msg.alert('  ', 'Terms & Conditions is mandatory.');
		}

	},
	createHelp: function() {
		var store = Ext.getStore('helpStore');
		var helpText = Ext.getCmp('helpText').getValue();

		if (helpText != '') {

			store.getProxy().url = webserver + 'createHelpDoc?areaText=' + encodeURIComponent(helpText) + '&lastModifiedBy=' + loginId + '&campusCode=' + campusCode;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('helpStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('  ', 'About Us content created successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
					});
				} else {
					Ext.Msg.alert('', status.status);
				}

			}
			store.load();

		} else {
			Ext.Msg.alert('  ', 'About Us Content is mandatory.');
		}

	},
	addFaq: function() {
		var store = Ext.getStore('helpStore');
		var question = Ext.getCmp('qFaq').getValue();
		var answer = Ext.getCmp('aFaq').getValue();
		var id = '';
		if (question != '' && answer != '') {

			store.getProxy().url = webserver + 'createFaq?campusCode=' + campusCode + '&question=' + question + '&answer=' + answer + '&lastModifiedBy=' + loginId + '&id=' + id;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('helpStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('  ', 'Created successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
						var listStore = Ext.getStore('faqStore');
						listStore.load();
						Ext.getCmp('qFaq').reset();
						Ext.getCmp('aFaq').reset();
					});
				} else {
					Ext.Msg.alert('', status.status);
				}

			}
			store.load();

		} else {
			Ext.Msg.alert('  ', 'Question and Answer fields are mandatory.');
		}

	},
	editFaq: function() {
		var store = Ext.getStore('updateFaqStore');
		var question = Ext.getCmp('eQuestion').getValue();
		var answer = Ext.getCmp('eAnswer').getValue();
		var id = selectRecord.data.faqId;
		if (question != '' && answer != '') {

			store.getProxy().url = webserver + 'createFaq?campusCode=' + campusCode + '&question=' + question + '&answer=' + answer + '&lastModifiedBy=' + loginId + '&id=' + id;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('updateFaqStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('  ', 'Updated successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
						var listStore = Ext.getStore('faqStore');
						listStore.load();
						Ext.getCmp('eQuestion').reset();
						Ext.getCmp('eAnswer').reset();
					});
				} else {
					Ext.Msg.alert('', status.status);
				}

			}
			store.load();

		} else {
			Ext.Msg.alert('  ', 'Question and Answer fields are mandatory.');
		}

	},
	treeRightClick: function(view, record, item, index, e) {
		//stop the default action
		e.stopEvent();
		selectRecord = record;
		if (typeof login.view.faqEditMenu == 'function') {
			login.view.faqEditMenu = Ext.create(login.view.faqEditMenu);
		}
		var module = login.view.faqEditMenu;
		Ext.getCmp('editFaq').setVisible(true);
		Ext.getCmp('deleteFaq').setVisible(true);
		module.showAt(e.getXY());
	},
	edit: function() {

		if (typeof login.view.updateFaqView == 'function') {
			login.view.updateFaqView = Ext.create(login.view.updateFaqView);
		}
		var editModule = login.view.updateFaqView;

		var faqQuestion = Ext.getCmp('eQuestion');
		var question = faqQuestion.setValue(selectRecord.data.question);

		var faqAnswer = Ext.getCmp('eAnswer');
		var answer = faqAnswer.setValue(selectRecord.data.answer);
		viewport.add(editModule);
		viewport.layout.setActiveItem(editModule);
	},
	faqDelete: function(item, e) {
		var store = Ext.getStore('updateFaqStore');

		store.getProxy().url = webserver + 'deleteFaq?id=' + selectRecord.data.faqId;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			myMask.hide();
			var store1 = Ext.getStore('updateFaqStore');
			var status = store1.getProxy().getReader().rawData;
			if (status.status == 'Success') {
				Ext.Msg.alert('', 'Deleted successfully.', function() {
					var tab = Ext.getCmp('tabView');
					viewport.add(tab);
					viewport.layout.setActiveItem(tab);
					var listStore = Ext.getStore('faqStore');
					listStore.load();
				});
			} else {
				Ext.Msg.alert('', status.status);
			}
		}
		store.load();
	}
});