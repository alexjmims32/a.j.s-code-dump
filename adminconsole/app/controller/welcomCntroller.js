Ext.define('login.controller.welcomCntroller', {
    extend:'Ext.app.Controller',
    stores:[
    'welcomeStore',
    'clientListStore'
    ],
    models:[
    'updateModel',
    'clientListModel'
    ],
    views:[
    'loginView',
    'welcomeView',
    'clientListView',
    'addConfigView',
    'addClientView',
    'configListGridView',
    'clientDetailsView',
    ],
    init:function () {
        this.control({
            'welcomeView button[action=submit]':{
                click:this.updateConfig
            },
            'welcomeView button[action=back]':{
                click:this.mainScreen
            },
            'configListGridView':{
                itemdblclick:this.configView
           
            },
            'configListGridView button[action=ADD]':{
                click:this.addConfigView
            },
            'configListGridView button[action=clientList]':{
                click:this.loadClientListView
            },
            'configListGridView button[action=logout]':{
                click:this.processLogout
            },
            'clientDetailsView button[action=back]':{
                click:this.mainScreen
            },
            'welcomeView button[action=reset]':{
                click:this.resetConfig
            },
            'configListGridView button[action=his]':{
                click:this.clientList
            },
            'clientListView button[action=back]':{
                click:this.mainScreen
            },
            'clientListView button[action=addClient]':{
                click:this.addClientView
            }
            
            
        });
    },
    mainScreen:function () {
        if (typeof login.view.tabView == 'function') {
            login.view.tabView = Ext.create(login.view.tabView);
        }
        var configListView = login.view.tabView;
        viewport.add(configListView);
        viewport.layout.setActiveItem(configListView);
    },
    addConfigView:function () {
        if (typeof login.view.addConfigView == 'function') {
            login.view.addConfigView = Ext.create(login.view.addConfigView);
        }
        var addConfig = login.view.addConfigView;
        if(loginClient != ''){
            var addClient=Ext.getCmp('adclient');
            addClient.setValue(loginClient);
            addClient.setReadOnly(true);
        }
        viewport.add(addConfig);
        viewport.layout.setActiveItem(addConfig);
    },
    
    loadClientListView: function(){
        var clientListStore = Ext.getStore('clientListStore');
        clientListStore.getProxy().url = webserver + 'getClientList';
        clientListStore.getProxy().headers = {
            Authorization:authString
        }
        clientListStore.getProxy().afterRequest = function () {
            //                        myMask.hide();
            var clientListStore1 = Ext.getStore('clientListStore');
            var status = clientListStore1.getProxy().getReader().rawData;
            if (status.status == 'SUCCESS') {
                if (typeof login.view.clientListView == 'function') {
                    login.view.clientListView = Ext.create(login.view.clientListView);
                }
                var clientListView = login.view.clientListView;
                viewport.add(clientListView);
                viewport.layout.setActiveItem(clientListView);
            }
        }
        clientListStore.load();
    },
    
    addClientView:function () {
        if (typeof login.view.addClientView == 'function') {
            login.view.addClientView = Ext.create(login.view.addClientView);
        }
        var addClientView = login.view.addClientView;
        viewport.add(addClientView);
        viewport.layout.setActiveItem(addClientView);
    },
    
    configView:function (grid, record) {
        if (typeof login.view.welcomeView == 'function') {
            login.view.welcomeView = Ext.create(login.view.welcomeView);
        }
        var welcome = login.view.welcomeView;
        var client = Ext.getCmp('client');
        var newClient = client.setValue(record.data.client);
        var hostname = Ext.getCmp('hostname');
        var newHostName = hostname.setValue(record.data.dbHostname);
        var port = Ext.getCmp('port');
        var newucport = port.setValue(record.data.dbPort);
        var sid = Ext.getCmp('sid');
        var newucsid = sid.setValue(record.data.dbSid);
        var userName = Ext.getCmp('wusername');
        var newUserName = userName.setValue(record.data.dbUsername);
        var password = Ext.getCmp('wpassword');
        var newPassword = password.setValue(record.data.dbPassword);
        var ldapHostName = Ext.getCmp('ldapHost');
        var newLdapHostName = ldapHostName.setValue(record.data.ldapHostname);
        var ldapPort = Ext.getCmp('ldapPort');
        var newLdapPort = ldapPort.setValue(record.data.ldapPort);
        var protocol = Ext.getCmp('Protocal');
        var newProtocol = protocol.setValue(record.data.ldapProtocol);
        var baseDN = Ext.getCmp('baseDN');
        var newBaseDN = baseDN.setValue(record.data.baseDN);
        var userId = Ext.getCmp('dnUserID');
        var newUserId = userId.setValue(record.data.dnUserID);
        var dnPassword = Ext.getCmp('dnPassword');
        var newDnPassword = dnPassword.setValue(record.data.dnPassword);
        Ext.getCmp('vpnType').setValue(record.data.vpnType);
        Ext.getCmp('vpnHost').setValue(record.data.vpnHost);
        Ext.getCmp('vpnUser').setValue(record.data.vpnUser);
        Ext.getCmp('vpnPassword').setValue(record.data.vpnPassword);
        Ext.getCmp('testOs').setValue(record.data.testOs);
        Ext.getCmp('testHostname').setValue(record.data.testHostname);
        Ext.getCmp('testPort').setValue(record.data.testPort);
        Ext.getCmp('testUserId').setValue(record.data.testUserId);
        Ext.getCmp('testPassword').setValue(record.data.testPassword);
        Ext.getCmp('testWS').setValue(record.data.testWS);
        Ext.getCmp('testWSVersion').setValue(record.data.testWSVersion);
        Ext.getCmp('testAS').setValue(record.data.testAS);
        Ext.getCmp('testASVersion').setValue(record.data.testASVersion);
        Ext.getCmp('leadName').setValue(record.data.leadName);
        Ext.getCmp('leadPhone').setValue(record.data.leadPhone);
        Ext.getCmp('leadEmail').setValue(record.data.leadEmail);
        Ext.getCmp('dbaName').setValue(record.data.dbaName);
        Ext.getCmp('dbaPhone').setValue(record.data.dbaPhone);
        Ext.getCmp('dbaEmail').setValue(record.data.dbaEmail);
        Ext.getCmp('notes').setValue(record.data.notes);

        viewport.add(welcome);
        viewport.layout.setActiveItem(welcome);
    },
    updateConfig:function () {
        var store = Ext.getStore('welcomeStore');
        var client = Ext.getCmp('client').getValue();
        var newDbHostName = Ext.getCmp('hostname').getValue();
        var newPort = Ext.getCmp('port').getValue();
        var newSid = Ext.getCmp('sid').getValue();
        var newUserName = Ext.getCmp('wusername').getValue();
        var newPassword = Ext.getCmp('wpassword').getValue();
        var newLdapHostName = Ext.getCmp('ldapHost').getValue();
        var newLdapPort = Ext.getCmp('ldapPort').getValue();
        var newProtocol = Ext.getCmp('Protocal').getValue();
        var baseDN = Ext.getCmp('baseDN').getValue();
        var newUserId = Ext.getCmp('dnUserID').getValue();
        var newDnPassword = Ext.getCmp('dnPassword').getValue();
        var vpnType = Ext.getCmp('vpnType').getValue();
        var vpnHost = Ext.getCmp('vpnHost').getValue();
        var vpnUser = Ext.getCmp('vpnUser').getValue();
        var vpnPassword = Ext.getCmp('vpnPassword').getValue();
        var testOs = Ext.getCmp('testOs').getValue();
        var testHostname = Ext.getCmp('testHostname').getValue();
        var testPort = Ext.getCmp('testPort').getValue();
        var testUserId = Ext.getCmp('testUserId').getValue();
        var testPassword = Ext.getCmp('testPassword').getValue();
        var testWS = Ext.getCmp('testWS').getValue();
        var testWSVersion = Ext.getCmp('testWSVersion').getValue();
        var testAS = Ext.getCmp('testAS').getValue();
        var testASVersion = Ext.getCmp('testASVersion').getValue();
        var leadName = Ext.getCmp('leadName').getValue();
        var leadPhone = Ext.getCmp('leadPhone').getValue();
        var leadEmail = Ext.getCmp('leadEmail').getValue();
        var dbaName = Ext.getCmp('dbaName').getValue();
        var dbaPhone = Ext.getCmp('dbaPhone').getValue();
        var dbaEmail = Ext.getCmp('dbaEmail').getValue();
        var notes = Ext.getCmp('notes').getValue();
        
        store.getProxy().url = webserver + 'updateDetails?client=' + client + '&dbHostname=' + newDbHostName + '&dbPort=' + newPort + '&dbSid=' + newSid + '&dbUsername=' + newUserName + '&dbPassword=' + newPassword + '&ldapHostname=' + newLdapHostName + '&ldapPort=' + newLdapPort + '&ldapProtocol=' + newProtocol + '&baseDN=' + baseDN + '&dnUserID=' + newUserId + '&dnPassword=' + newDnPassword+'&vpnType='+vpnType+'&vpnHost='+vpnHost+'&vpnUser='+vpnUser+'&vpnPassword='+vpnPassword+'&testOs='+testOs+'&testHostname='+testHostname+'&testPort='+testPort+'&testUserId='+testUserId+'&testPassword='+testPassword+'&testWS='+testWS+'&testWSVersion='+testWSVersion+'&testAS='+testAS+'&testASVersion='+testASVersion+'&leadName='+leadName+'&leadPhone='+leadPhone+'&leadEmail='+leadEmail+'&dbaName='+dbaName+'&dbaPhone='+dbaPhone+'&dbaEmail='+dbaEmail+'&notes='+notes;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('welcomeStore');
            var status = store1.getProxy().getReader().rawData;
            if (status.status = 'SUCCESS') {
                Ext.Msg.alert('', 'Configuarations updated Successfully', function () {
                    if (typeof login.view.configListGridView == 'function') {
                        login.view.configListGridView = Ext.create(login.view.configListGridView);
                    }
                    var configListView = login.view.configListGridView;
                    viewport.add(configListView);
                    viewport.layout.setActiveItem(configListView);
                    
                    Ext.getCmp('client').reset();
                    Ext.getCmp('hostname').reset();
                    Ext.getCmp('port').reset();
                    Ext.getCmp('sid').reset();
                    Ext.getCmp('wusername').reset();
                    Ext.getCmp('wpassword').reset();
                    Ext.getCmp('ldapHost').reset();
                    Ext.getCmp('ldapPort').reset();
                    Ext.getCmp('Protocal').reset();
                    Ext.getCmp('baseDN').reset();
                    Ext.getCmp('dnUserID').reset();
                    Ext.getCmp('dnPassword').reset();
                    Ext.getCmp('vpnType').reset();
                    Ext.getCmp('vpnHost').reset();
                    Ext.getCmp('vpnUser').reset();
                    Ext.getCmp('vpnPassword').reset();
                    Ext.getCmp('testOs').reset();
                    Ext.getCmp('testHostname').reset();
                    Ext.getCmp('testPort').reset();
                    Ext.getCmp('testUserId').reset();
                    Ext.getCmp('testPassword').reset();
                    Ext.getCmp('testWS').reset();
                    Ext.getCmp('testWSVersion').reset();
                    Ext.getCmp('testAS').reset();
                    Ext.getCmp('testASVersion').reset();
                    Ext.getCmp('leadName').reset();
                    Ext.getCmp('leadPhone').reset();
                    Ext.getCmp('leadEmail').reset();
                    Ext.getCmp('dbaName').reset();
                    Ext.getCmp('dbaPhone').reset();
                    Ext.getCmp('dbaEmail').reset();
                    Ext.getCmp('notes').reset();
                        
                    var updateConfigStore = Ext.getStore('updateConfigStore');
                    updateConfigStore.getProxy().url = webserver + 'getConfigList?client='+loginClient;
                    updateConfigStore.getProxy().headers = {
                        Authorization:authString
                    }
                    updateConfigStore.load();
                });
            }
        }
        store.load();
    },
    resetConfig:function () {
        Ext.getCmp('client').reset();
        Ext.getCmp('hostname').reset();
        Ext.getCmp('port').reset();
        Ext.getCmp('sid').reset();
        Ext.getCmp('wusername').reset();
        Ext.getCmp('wpassword').reset();
        Ext.getCmp('ldapHost').reset();
        Ext.getCmp('ldapPort').reset();
        Ext.getCmp('Protocal').reset();
        Ext.getCmp('baseDN').reset();
        Ext.getCmp('userId').reset();
        Ext.getCmp('dnPassword').reset();
        Ext.getCmp('vpnType').reset();
        Ext.getCmp('vpnHost').reset();
        Ext.getCmp('vpnUser').reset();
        Ext.getCmp('vpnPassword').reset();
        Ext.getCmp('testOs').reset();
        Ext.getCmp('testHostname').reset();
        Ext.getCmp('testPort').reset();
        Ext.getCmp('testUserId').reset();
        Ext.getCmp('testPassword').reset();
        Ext.getCmp('testWS').reset();
        Ext.getCmp('testWSVersion').reset();
        Ext.getCmp('testAS').reset();
        Ext.getCmp('testASVersion').reset();
        Ext.getCmp('leadName').reset();
        Ext.getCmp('leadPhone').reset();
        Ext.getCmp('leadEmail').reset();
        Ext.getCmp('dbaName').reset();
        Ext.getCmp('dbaPhone').reset();
        Ext.getCmp('dbaEmail').reset();
        Ext.getCmp('notes').reset();
    },
    processLogout : function () {
        Ext.getCmp('username').reset();
        Ext.getCmp('password').reset();
        var loginView=Ext.getCmp('loginView');
        viewport.layout.setActiveItem(loginView);
    }
    ,
    clientList:function(){
//        var detailsView = Ext.widget('clientDetailsView');
//        viewport.add(detailsView);
//        viewport.layout.setActiveItem(detailsView);

 if (typeof login.view.clientDetailsView == 'function') {
                        login.view.clientDetailsView = Ext.create(login.view.clientDetailsView);
                    }
                    var configListView = login.view.clientDetailsView;
                    viewport.add(configListView);
                    viewport.layout.setActiveItem(configListView);
    }
});
