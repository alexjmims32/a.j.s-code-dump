Ext.define('login.controller.reportController', {
    extend:'Ext.app.Controller',
    stores:[
    'reportListStore',
    'reportDropDownStore',
    'metaDataTableStore',
    'metaDataColumnsStore',
    'exceptionStore',
    'logStore'
    ],
    models:[
    'reportListModel'
    ],
    views:[
    'reportListView',
    'addReportView',
    'createReportView',
    'reportEditMenu',
    'editReportView',
    'updateReportView',
    'logListView',
    'exceptionListView'
    ],
    init:function () {
        this.control({
            'reportListView':{
                beforeactivate:this.reportList,
                itemcontextmenu : this.treeRightClick
            },
            
            'reportListView button[action=ADD]':{
                click:this.createReportView
            },
            'addReportView button[action=back]':{
                click:this.tabForm
            },
            refs : [{                
                ref : 'reportListView',
                selector : 'reportEditMenu'
            },
            
            ],
            'reportEditMenu menuitem[text=Edit]':{
                click:this.reportEdit
            },
            'reportEditMenu menuitem[text=Delete]':{
                click:this.deleteReport
            },
            'reportEditMenu menuitem[text=Log]':{
                click:this.logView
            },
            'reportEditMenu menuitem[text=Exceptions]':{
                click:this.exceptionView
            },
            'addReportView button[action=schedule]':{
                click:this.addReport
            },
            'addReportView button[action=reset]':{
                click:this.clearAddReport
            },
            'editReportView button[action=reset]':{
                click:this.setActual
            },
            'editReportView button[action=back]':{
                click:this.tabForm
            },            
            'logListView button[action=Back]':{
                click:this.tabForm
            },
            'exceptionListView button[action=Back]':{
                click:this.tabForm
            },
            'editReportView button[action=schedule]':{
                click:this.editReport
            }
           
        });
    },
    exceptionView:function () {  
        var store = Ext.getStore('exceptionStore');        
        var jobName=selectRecord.data.refreshName;                       
        store.getProxy().url =webserver + 'getException?jobName='+jobName; 
        store.getProxy().afterRequest = function () {
            
            if (typeof login.view.exceptionListView == 'function') {
                login.view.exceptionListView = Ext.create(login.view.exceptionListView);
            }
            var logList = login.view.exceptionListView;        
            viewport.add(logList);
            viewport.layout.setActiveItem(logList); 
        }
        store.load();
    },
    logView:function () {  
        var store = Ext.getStore('logStore');
        
        var jobName=selectRecord.data.refreshName;
        
        store.getProxy().headers={
            Authorization:authString
        };       
        store.getProxy().url =webserver + 'getLogs?jobName='+jobName;
        
        store.getProxy().afterRequest = function () {
            if (typeof login.view.logListView == 'function') {
                login.view.logListView = Ext.create(login.view.logListView);
            }
            var logList = login.view.logListView;        
            viewport.add(logList);
            viewport.layout.setActiveItem(logList); 
        }
        store.load();
    },
    clearAddReport:function () {          
        Ext.getCmp('srefreshType').reset();
        Ext.getCmp('refreshName').reset();
        Ext.getCmp('tmToRefreshType').reset();
        Ext.getCmp('ssDate').reset();
        Ext.getCmp('seDate').reset(); 
        Ext.getCmp('rFrequency').reset(); 
        Ext.getCmp('rInterval').reset();         
        Ext.getCmp('arEnable').setValue(false);
        Ext.getCmp('schdeuleTime1').reset(); 
        Ext.getCmp('schdeuleTime2').reset(); 
        Ext.getCmp('schdeuleTime3').reset(); 
    },
    setActual:function () {          
        Ext.getCmp('esrefreshType').setValue(selectRecord.data.refreshType);
        Ext.getCmp('erefreshName').setValue(selectRecord.data.refreshName);
        Ext.getCmp('etmToRefreshType').setValue(selectRecord.data.toRefresh);
        Ext.getCmp('essDate').setValue(selectRecord.data.startDate);
        Ext.getCmp('eseDate').setValue(selectRecord.data.endDate); 
        Ext.getCmp('erFrequency').setValue(selectRecord.data.frequency); 
        Ext.getCmp('erInterval').setValue(selectRecord.data.interval);         
        Ext.getCmp('erEnable').setValue(false);
        
        var scheduleTime = selectRecord.data.scheduleTime;
        var splitTime=scheduleTime.split(':');
        
        var t1=splitTime[0];
        var t2=splitTime[1];
        var t3=splitTime[2];   
          
        Ext.getCmp('eschdeuleTime1').setValue(t1); 
        Ext.getCmp('eschdeuleTime2').setValue(t2); 
        Ext.getCmp('eschdeuleTime3').setValue(t3); 
    },        
    tabForm:function () {  
        
        var tab = Ext.getCmp('tabView');
        viewport.add(tab);
        viewport.layout.setActiveItem(tab);
    },    
    reportEdit:function(){     
        
        if (typeof login.view.updateReportView == 'function') {
            login.view.updateReportView = Ext.create(login.view.updateReportView);
        }
        var report = login.view.updateReportView;
        //        var id = Ext.getCmp('conId');
        //        var newId = selectRecord.data.id;
               
        var type = Ext.getCmp('esrefreshType');
        var selectType = type.setValue(selectRecord.data.refreshType);
        
        var name = Ext.getCmp('erefreshName');
        var selectName = name.setValue(selectRecord.data.refreshName);
        
        var toRefreshType = Ext.getCmp('etmToRefreshType');
        var refreshType = toRefreshType.setValue(selectRecord.data.toRefresh);
        
        var startDate = Ext.getCmp('essDate');
        var newStartDate = startDate.setValue(selectRecord.data.startDate);
        
        var endDate = Ext.getCmp('eseDate');
        var newEndDate = endDate.setValue(selectRecord.data.endDate);
                    
        var frequency = Ext.getCmp('erFrequency');
        var newFrequency = frequency.setValue(selectRecord.data.frequency);
        
        var interval = Ext.getCmp('erInterval');
        var newInterval = interval.setValue(selectRecord.data.interval);
        
        var interval = Ext.getCmp('erInterval');
        
        var scheduleTime = selectRecord.data.scheduleTime;
        var splitTime=scheduleTime.split(':');
        
        var t1=splitTime[0];
        var t2=splitTime[1];
        var t3=splitTime[2];   
        
        var time1 = Ext.getCmp('eschdeuleTime1');
        var newInterval = time1.setValue(t1);
        
        var time2 = Ext.getCmp('eschdeuleTime2');
        var newInterval = time2.setValue(t2);
        
        var time3 = Ext.getCmp('eschdeuleTime3');
        var newInterval = time3.setValue(t3);
        
        var newEnable = selectRecord.data.enabled;
        if(newEnable=='TRUE'){
            Ext.getCmp('erEnable').setValue(true);
        }else{
            Ext.getCmp('erEnable').setValue(false);
        }
       
        viewport.add(report);
        viewport.layout.setActiveItem(report); 
    },
    createReportView:function () {  
        
        if (typeof login.view.createReportView == 'function') {
            login.view.createReportView = Ext.create(login.view.createReportView);
        }
        var addReport = login.view.createReportView;        
        viewport.add(addReport);
        viewport.layout.setActiveItem(addReport);   
    },
    reportList:function () {  
        
        var store = Ext.getStore('reportListStore');
        store.getProxy().url = webserver + 'getReports';
        store.getProxy().headers = {
            Authorization:authString
        };              
        store.getProxy().afterRequest=function(){
            console.log(store);
        }
        store.load(); 
    },
    addReport:function () {
        var store = Ext.getStore('updateContactStore');
        var newStartDate ='';
        var newEndDate = '';
        
        var type = Ext.getCmp('srefreshType').getValue();               
        var jobName = Ext.getCmp('refreshName').getValue();
        var refreshName = Ext.getCmp('tmToRefreshType').getValue();  
            
        var startDate = Ext.getCmp('ssDate').getValue();
        
        if(startDate == null) {
            newStartDate = '';
        } else {
            var newDate=startDate.getDate();
            var newMonth=startDate.getMonth() + 1;
            if(newDate<10){
                newDate='0'+newDate;
            }
            if(newMonth<10){
                newMonth='0'+newMonth;
            }
            newStartDate = (newDate + '-' +newMonth+'-'+ startDate.getFullYear());
        }
        var endDate = Ext.getCmp('seDate').getValue();
        if(endDate == null) {
            newEndDate = '';
        } else {
            var newDate=endDate.getDate();
            var newMonth=endDate.getMonth() + 1;
            if(newDate<10){
                newDate='0'+newDate;
            }
            if(newMonth<10){
                newMonth='0'+newMonth;
            }
            newEndDate = (newDate + '-' +newMonth+'-'+ endDate.getFullYear());
        }
        
        var frequency = Ext.getCmp('rFrequency').getValue();
        var interval = Ext.getCmp('rInterval').getValue();
        
        var enable = Ext.getCmp('arEnable').lastValue;
        if(enable==false){
            enable='FALSE';
        }else{
            enable='TRUE';
        }
        flag='N';
        
        var time1 = Ext.getCmp('schdeuleTime1').getValue();
        var time2 = Ext.getCmp('schdeuleTime2').getValue();
        var time3 = Ext.getCmp('schdeuleTime3').getValue();
        
        store.getProxy().url = webserver + 'addReport?type='+ type+ '&jobName=' + jobName +'&refreshName=' + refreshName+ '&startDate=' + newStartDate + '&endDate=' + newEndDate + '&frequency=' + frequency + '&interval=' + interval+'&hours='+time1+'&minutes='+time2+'&seconds='+time3+'&enable='+enable+'&flag='+flag;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('updateContactStore');
            var status = store1.getProxy().getReader().rawData;
            if (status == 'SUCCESS') {
                Ext.Msg.alert('', 'Report added successfully.', function () {
                    var tab = Ext.getCmp('tabView');
                    viewport.add(tab);
                    viewport.layout.setActiveItem(tab); 
                    var listStore = Ext.getStore('reportListStore');
                    listStore.load();                  
                    Ext.getCmp('srefreshType').reset();
                    Ext.getCmp('refreshName').reset();
                    Ext.getCmp('tmToRefreshType').reset();
                    Ext.getCmp('ssDate').reset();
                    Ext.getCmp('seDate').reset(); 
                    Ext.getCmp('rFrequency').reset();
                    Ext.getCmp('rInterval').reset();
                    
                    Ext.getCmp('schdeuleTime1').reset(); 
                    Ext.getCmp('schdeuleTime2').reset();
                    Ext.getCmp('schdeuleTime3').reset();
                });
            }else{
                Ext.Msg.alert('',status );
            }                
        }
        store.load();                
    },
    editReport:function () {
        var store = Ext.getStore('updateContactStore');
        var newStartDate ='';
        var newEndDate = '';
        
        var type = Ext.getCmp('esrefreshType').getValue();               
        var jobName = Ext.getCmp('erefreshName').getValue();
        var refreshName = Ext.getCmp('etmToRefreshType').getValue();              
        var startDate = Ext.getCmp('essDate').getValue();
        if(startDate == null) {
            newStartDate = '';
        } else {
            var newDate=startDate.getDate();
            var newMonth=startDate.getMonth() + 1;
            if(newDate<10){
                newDate='0'+newDate;
            }
            if(newMonth<10){
                newMonth='0'+newMonth;
            }
            newStartDate = (newDate + '-' +newMonth+'-'+ startDate.getFullYear());
        }
        
        var endDate = Ext.getCmp('eseDate').getValue();
        if(endDate == null) {
            newEndDate = '';
        } else {
            var newDate=endDate.getDate();
            var newMonth=endDate.getMonth() + 1;
            if(newDate<10){
                newDate='0'+newDate;
            }
            if(newMonth<10){
                newMonth='0'+newMonth;
            }
            newEndDate = (newDate + '-' +newMonth+'-'+ endDate.getFullYear());
        }
        
        var frequency = Ext.getCmp('erFrequency').getValue();
        var interval = Ext.getCmp('erInterval').getValue();        
        var enable = Ext.getCmp('erEnable').lastValue;
        if(enable==false){
            enable='FALSE';
        }else{
            enable='TRUE';
        }
        flag='U';        
        var time1 = Ext.getCmp('eschdeuleTime1').getValue();
        var time2 = Ext.getCmp('eschdeuleTime2').getValue();
        var time3 = Ext.getCmp('eschdeuleTime3').getValue();
        
        store.getProxy().url = webserver + 'addReport?type='+ type+ '&jobName=' + jobName +'&refreshName=' + refreshName+ '&startDate=' + newStartDate + '&endDate=' + newEndDate + '&frequency=' + frequency + '&interval=' + interval+'&hours='+time1+'&minutes='+time2+'&seconds='+time3+'&enable='+enable+'&flag='+flag;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('updateContactStore');
            var status = store1.getProxy().getReader().rawData;
            if (status == 'SUCCESS') {
                Ext.Msg.alert('', 'Report updated successfully.', function () {
                    var tab = Ext.getCmp('tabView');
                    viewport.add(tab);
                    viewport.layout.setActiveItem(tab); 
                    var listStore = Ext.getStore('reportListStore');
                    listStore.load();                  
                    Ext.getCmp('esrefreshType').reset();
                    Ext.getCmp('erefreshName').reset();
                    Ext.getCmp('etmToRefreshType').reset();
                    Ext.getCmp('essDate').reset();
                    Ext.getCmp('eseDate').reset(); 
                    Ext.getCmp('erFrequency').reset();
                    Ext.getCmp('erInterval').reset();                    
                    Ext.getCmp('eschdeuleTime1').reset(); 
                    Ext.getCmp('eschdeuleTime2').reset();
                    Ext.getCmp('eschdeuleTime3').reset();
                });
            }else{
                Ext.Msg.alert('',status );
            }                
        }
        store.load();
      
    },
    
    deleteReport:function () {
        var store = Ext.getStore('updateContactStore');
        
        var type = '';               
        var jobName = selectRecord.data.refreshName;
        var refreshName = '';  
        var startDate = '';
        var endDate = '';
        var frequency = '';
        var interval = '';
        
        var enable = 'FALSE';
        flag='D';
        var time1 = '';
        var time2 = '';
        var time3 = '';
        
        store.getProxy().url = webserver + 'addReport?type='+ type+ '&jobName=' + jobName +'&refreshName=' + refreshName+ '&startDate=' + startDate + '&endDate=' + endDate + '&frequency=' + frequency + '&interval=' + interval+'&hours='+time1+'&minutes='+time2+'&seconds='+time3+'&enable='+enable+'&flag='+flag;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            myMask.hide();
            var store1 = Ext.getStore('updateContactStore');
            var status = store1.getProxy().getReader().rawData;
            if (status == 'SUCCESS') {
                Ext.Msg.alert('', 'Report Deleted successfully.', function () {
                    var tab = Ext.getCmp('tabView');
                    viewport.add(tab);
                    viewport.layout.setActiveItem(tab);
                    var listStore = Ext.getStore('reportListStore');
                    listStore.load(); 
                });
            }else{
                Ext.Msg.alert('',status );
            }                
        }
        store.load();
      
    },
    treeRightClick : function(view, record, item, index, e) {
        //stop the default action
        e.stopEvent();
        selectIndex=index;
        selectRecord=record;  
        if (typeof login.view.reportEditMenu == 'function') {
            login.view.reportEditMenu = Ext.create(login.view.reportEditMenu);
        }
        var module = login.view.reportEditMenu;
    
        Ext.getCmp('editReport').setVisible(true);
        Ext.getCmp('reportLog').setVisible(true);
                
        module.showAt(e.getXY());
    }
    
});