Ext.define('login.controller.feedController', {
	extend: 'Ext.app.Controller',
	stores: [
		'feedModuleStore',
		'feedViewStore',
		'feedDetailsSaveStore',
		'treeStore',
		'addLinkStore',
		'updateLinkStore',
		'feedTrackerStore',
		'addModuleStore'
	],
	models: [
		'feedModuleModel'
	],
	views: [
		'moduleListView',
		'addFeedLinkView',
		'addFeedModuleView',
		'feedLevel2View',
		'feedTabView',
		'menuView',
		'editMenuView',
		'editLinkView',
		'editModuleView',
		'feedTrackerView',
		'trackerTabView',
		'createLinkView',
		'createModuleView',
		'updateLinkView',
		'updateModuleView'

	],
	init: function() {
		this.control({
			'moduleListView': {
				beforeactivate: this.moduleList,
				itemexpand: this.moduleView,
				itemcontextmenu: this.treeRightClick
			},
			'feedTrackerView': {
				beforeactivate: this.trackerFeeds
			},
			refs: [{
				//reference to the country Tree
				ref: 'myConfigTree',
				selector: 'menuView'
			}, {
				//reference to the country window
				ref: 'myConfigEdit',
				selector: 'editMenuView'
			}],
			'menuView menuitem[text=Add Link]': {
				click: this.addLinkForm
			},
			'menuView menuitem[text=Add Module]': {
				click: this.addModuleForm
			},
			'menuView menuitem[text=Delete]': {
				click: this.moduleDelete
			},
			'menuView menuitem[text=Edit]': {
				click: this.edit
			},
			'addFeedLinkView button[action=submit]': {
				click: this.addLink
			},
			'addFeedModuleView button[action=submit]': {
				click: this.addModule
			},
			'addFeedLinkView button[action=back]': {
				click: this.tabForm
			},
			'addFeedModuleView button[action=back]': {
				click: this.tabForm
			},
			'addFeedLinkView button[action=cancel]': {
				click: this.moduleList
			},
			'editMenuView menuitem[text=Edit]': {
				click: this.edit
			},

			'editLinkView button[action=submit]': {
				click: this.linkEdit
			},
			'editModuleView button[action=submit]': {
				click: this.moduleEdit
			},
			'editLinkView button[action=back]': {
				click: this.tabForm
			},
			'editModuleView button[action=back]': {
				click: this.tabForm
			},
			'editLinkView button[action=cancel]': {
				click: this.moduleList
			},
			'editModuleView button[action=reset]': {
				click: this.moduleEditReset
			},
			'editLinkView button[action=reset]': {
				click: this.linkEditReset
			}
		});
	},
	linkEditReset: function() {
		Ext.getCmp('eFdlUrl').setValue(selectRecord.data.link);
		Ext.getCmp('eFdlFormat').setValue(selectRecord.data.format);
		Ext.getCmp('eFdlTitle').setValue(selectRecord.data.text);
		Ext.getCmp('eFdlIcon').setValue(selectRecord.data.image);
		Ext.getCmp('eFdlType').setValue(selectRecord.data.type);
		Ext.getCmp('eFdlCampCode').setValue(campusCode);
	},
	moduleEditReset: function() {
		Ext.getCmp('eFdTitle').setValue(selectRecord.data.text);
		Ext.getCmp('eFdIcon').setValue(selectRecord.data.image);
		Ext.getCmp('eFdmCampCode').setValue(campusCode);
	},
	linkAddReset: function() {
		Ext.getCmp('eFdlUrl').reset();
		Ext.getCmp('eFdlFormat').reset();
		Ext.getCmp('eFdlTitle').reset();
		Ext.getCmp('eFdlIcon').reset();
		Ext.getCmp('eFdlType').reset();
		Ext.getCmp('eFdlCampCode').reset();
	},
	moduleAddReset: function() {
		Ext.getCmp('eFdTitle').reset();
		Ext.getCmp('eFdIcon').reset();
		Ext.getCmp('eFdmCampCode').reset();
	},
	edit: function() {
		if (selectRecord.data.leaf == true) {
			if (typeof login.view.updateLinkView == 'function') {
				login.view.updateLinkView = Ext.create(login.view.updateLinkView);
			}
			var store = Ext.getStore('treeStore');
			var editLink = login.view.updateLinkView;
			//            var id = Ext.getCmp('eFdlID');
			var linkId = selectRecord.data.feedId;
			var url = Ext.getCmp('eFdlUrl');
			var linkUrl = url.setValue(selectRecord.data.link);

			var format = Ext.getCmp('eFdlFormat');
			var linkFormat = format.setValue(selectRecord.data.format);

			var title = Ext.getCmp('eFdlTitle');
			var linkTitle = title.setValue(selectRecord.data.text);

			var icon = Ext.getCmp('eFdlIcon');
			var linkIcon = icon.setValue(selectRecord.data.image);

			var type = Ext.getCmp('eFdlType');
			var linkType = type.setValue(selectRecord.data.type);

			var eCampusCode = Ext.getCmp('eFdlCampCode');
			var linkCode = eCampusCode.setValue(campusCode);


			viewport.add(editLink);
			viewport.layout.setActiveItem(editLink);
		} else {
			if (typeof login.view.updateModuleView == 'function') {
				login.view.updateModuleView = Ext.create(login.view.updateModuleView);
			}
			var editModule = login.view.updateModuleView;

			//            var id = Ext.getCmp('fdID');
			var moduleId = selectRecord.data.feedId;

			var title = Ext.getCmp('eFdTitle');
			var moduleTitle = title.setValue(selectRecord.data.text);

			var icon = Ext.getCmp('eFdIcon');
			var moduleIcon = icon.setValue(selectRecord.data.image);

			var campCode = Ext.getCmp('eFdmCampCode');
			var moduleCode = campCode.setValue(campusCode);

			viewport.add(editModule);
			viewport.layout.setActiveItem(editModule);
		}

	},
	addLinkForm: function() {

		if (typeof login.view.createLinkView == 'function') {
			login.view.createLinkView = Ext.create(login.view.createLinkView);
		}
		var addLink = login.view.createLinkView;

		viewport.add(addLink);
		viewport.layout.setActiveItem(addLink);
	},
	addModuleForm: function() {

		if (typeof login.view.createModuleView == 'function') {
			login.view.createModuleView = Ext.create(login.view.createModuleView);
		}
		var addModule = login.view.createModuleView;
		viewport.add(addModule);
		viewport.layout.setActiveItem(addModule);
	},
	tabForm: function() {

		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	addModule: function() {

		var store = Ext.getStore('addLinkStore');
		var name = Ext.getCmp('fdTitle').getValue();
		var icon = Ext.getCmp('fdIcon').getValue();
		var type = "MENU";
		var link = "";
		var format = "";
		var moduleID = selectRecord.data.moduleID;
		if (moduleID == '') {
			moduleID = 0
		}
		var id = 0;
		if (name != '' && icon != '') {
			if (selectRecord.data.feedId != '' && !selectRecord.data.feedId == undefined) {
				id = selectRecord.data.feedId;
			}
			store.getProxy().url = webserver + 'addEditFeed?parentID=' + id + '&type=' + type + '&link=' + encodeURIComponent(link) + '&format=' + format + '&moduleID=' + moduleID + '&name=' + name + '&icon=' + icon + '&campusCode=' + campusCode + '&lastModifiedBy=' + loginId + '&id=' + id;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('addLinkStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('', 'Module added successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
						var listStore = Ext.getStore('treeStore');
						listStore.load();
						Ext.getCmp('fdTitle').reset();
						Ext.getCmp('fdIcon').reset();
						//                    Ext.getCmp('fdmcampus').reset();        
					});
				} else {
					Ext.Msg.alert('', status.status);
				}
			}

			store.load();
		} else {
			Ext.Msg.alert('    ', 'All are mandatory fields.')
		}

	},
	addLink: function() {

		var store = Ext.getStore('addLinkStore');
		var link = Ext.getCmp('fdlUrl').getValue();
		var format = Ext.getCmp('fdlFormat').getValue();
		var title = Ext.getCmp('fdlTitle').getValue();
		var icon = Ext.getCmp('fdlIcon').getValue();
		var type = 'LINK';
		var moduleID = selectRecord.data.moduleID;
		if (moduleID == '') {
			moduleID = 0
		}
		//        var campCode = campusCode;
		if (title != '' && link != '' && icon != '' && format != '') {
			var id = 0;
			console.log(selectRecord.data);
			if (selectRecord.data.feedId != '' || !selectRecord.data.feedId == undefined) {
				id = selectRecord.data.feedId;
			}
			store.getProxy().url = webserver + 'addEditFeed?parentID=' + id + '&type=' + type + '&link=' + encodeURIComponent(link) + '&format=' + format + '&moduleID=' + moduleID + '&name=' + title + '&icon=' + icon + '&campusCode=' + campusCode + '&lastModifiedBy=' + loginId + '&id=0';
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('addLinkStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('', 'Link added successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
						var listStore = Ext.getStore('treeStore');
						listStore.load();


						Ext.getCmp('fdlUrl').reset();
						Ext.getCmp('fdlFormat').reset();
						Ext.getCmp('fdlTitle').reset();
						Ext.getCmp('fdlIcon').reset();
						//                    Ext.getCmp('fdlCode').reset(); 
					});
				} else {
					Ext.Msg.alert('', status.status);
				}
			}
			store.load();
		} else {
			Ext.Msg.alert('    ', 'All are mandatory fields.');
		}
	},
	moduleList: function() {
		//        var feedStore = Ext.getStore('feedViewStore');
		//        feedStore.removeAll();
		var store = Ext.getStore('treeStore');
		store.getProxy().url = webserver + 'getFeedModules?campusCode=' + campusCode;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {}
		store.load();
	},
	moduleView: function(node, e) {
		if (node.data.moduleID != '' && node.data.moduleID != null) {
			var store = Ext.getStore('feedViewStore');
			store.removeAll();
			store.getProxy().url = webserver + 'getAdminConfig?moduleID=' + node.data.moduleID + '&parentID=' + node.data.feedId + '&campusCode=' + campusCode;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				node.removeAll();
				store.each(function(record) {
					console.log(record.data.leaf);
					if (record.data.leaf == false) {
						var child = node.createNode({
							'text': record.data.text,
							'feedId': record.data.feedId,
							'parentID': record.data.parentID,
							'type': record.data.type,
							'link': record.data.link,
							'format': record.data.format,
							'moduleID': record.data.moduleID,
							'title': record.data.name,
							'image': record.data.image,
							'leaf': record.data.leaf,
							'campusCode': record.data.campusCode
						});
						child.appendChild(child.createNode({
							'text': ''
						}));
					} else {
						child = node.createNode({
							'text': record.data.text,
							'feedId': record.data.feedId,
							'parentID': record.data.parentID,
							'type': record.data.type,
							'link': record.data.link,
							'format': record.data.format,
							'moduleID': record.data.moduleID,
							'title': record.data.name,
							'image': record.data.image,
							'leaf': record.data.leaf,
							'campusCode': record.data.campusCode
						});
					}
					node.appendChild(child);

				});

			}
			store.load();
		}
	},
	treeRightClick: function(view, record, item, index, e) {
		//stop the default action
		e.stopEvent();

		if (record.data.parentID == '') {
			record.data.parentID == '0';
		}

		selectRecord = record;
		if (record.data.leaf == false) {
			console.log("parentId : " + record.data.parentID);
			console.log("moduleId : " + record.data.moduleID);
			if (typeof login.view.menuView == 'function') {
				login.view.menuView = Ext.create(login.view.menuView);
			}
			var module = login.view.menuView;
			Ext.getCmp('addLinkMenu').setVisible(true);
			Ext.getCmp('addModuleMenu').setVisible(true);
			Ext.getCmp('editMenu').setVisible(true);
			Ext.getCmp('deleteMenu').setVisible(true);
			module.showAt(e.getXY());
		} else {
			if (typeof login.view.menuView == 'function') {
				login.view.menuView = Ext.create(login.view.menuView);
			}
			var module = login.view.menuView;
			Ext.getCmp('addLinkMenu').setVisible(false);
			Ext.getCmp('addModuleMenu').setVisible(false);
			Ext.getCmp('editMenu').setVisible(true);
			Ext.getCmp('deleteMenu').setVisible(true);
			module.showAt(e.getXY());
		}


		return false;
	},
	moduleEdit: function(item, e) {
		var store = Ext.getStore('updateLinkStore');
		var name = Ext.getCmp('eFdTitle').getValue();
		var id = selectRecord.data.feedId;
		var icon = Ext.getCmp('eFdIcon').getValue();
		var type = 'MENU';
		var link = '';
		var format = '';
		var parentID = selectRecord.data.parentID;
		if (parentID == '') {
			parentID = 0;
		}
		//        var campusCode=Ext.getCmp('eFdmCampCode').getValue();
		if (name != '' && icon != '') {
			store.getProxy().url = webserver + 'addEditFeed?id=' + id + '&parentID=' + parentID + '&type=' + type + '&link=' + encodeURIComponent(link) + '&format=' + format + '&moduleID=' + selectRecord.data.moduleID + '&name=' + name + '&icon=' + icon + '&campusCode=' + campusCode + '&lastModifiedBy=' + loginId;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('updateLinkStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('', 'Module updated successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
						var listStore = Ext.getStore('treeStore');
						listStore.load();
					});
				} else {
					Ext.Msg.alert('', status.status);
				}
			}

			store.load();
		} else {
			Ext.Msg.alert('', 'All are mandatory fields.');
		}
	},
	linkEdit: function(item, e) {

		var store = Ext.getStore('updateLinkStore');
		var name = Ext.getCmp('eFdlTitle').getValue();
		var id = selectRecord.data.feedId;
		var icon = Ext.getCmp('eFdlIcon').getValue();
		var type = Ext.getCmp('eFdlType').getValue();
		var parentID = selectRecord.data.parentID;
		if (parentID == '') {
			parentID = 0;
		}

		var link = Ext.getCmp('eFdlUrl').getValue();
		var format = Ext.getCmp('eFdlFormat').getValue();
		if (name != '' && link != '' && icon != '' && format != '') {
			store.getProxy().url = webserver + 'addEditFeed?id=' + id + '&parentID=' + parentID + '&type=' + type + '&link=' + encodeURIComponent(link) + '&format=' + format + '&moduleID=' + selectRecord.data.moduleID + '&name=' + name + '&icon=' + icon + '&campusCode=' + campusCode + '&lastModifiedBy=' + loginId;
			store.getProxy().headers = {
				Authorization: authString
			};
			store.getProxy().afterRequest = function() {
				myMask.hide();
				var store1 = Ext.getStore('updateLinkStore');
				var status = store1.getProxy().getReader().rawData;
				if (status.status == 'SUCCESS') {
					Ext.Msg.alert('', 'Link updated successfully.', function() {
						var tab = Ext.getCmp('tabView');
						viewport.add(tab);
						viewport.layout.setActiveItem(tab);
						var listStore = Ext.getStore('treeStore');
						listStore.load();
					});
				} else {
					Ext.Msg.alert('', status.status);
				}
			}
			store.load();
		} else {
			Ext.Msg.alert('', 'All are mandatory fields');
		}
	},
	moduleDelete: function(item, e) {
		var store = Ext.getStore('updateLinkStore');

		store.getProxy().url = webserver + 'deleteFeed?id=' + selectRecord.data.feedId;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			myMask.hide();
			Ext.Msg.show({
				id: 'deleteOk',
				title: null,
				msg: '<center> Are you sure you want to delete this item?</center>',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						var store1 = Ext.getStore('updateLinkStore');
						var status = store1.getProxy().getReader().rawData;
						if (status.status == 'SUCCESS') {
							Ext.Msg.alert('', 'Deletion completed successfully.', function() {
								var tab = Ext.getCmp('tabView');
								viewport.add(tab);
								viewport.layout.setActiveItem(tab);
								var listStore = Ext.getStore('treeStore');
								listStore.load();
							});
						} else {
							Ext.Msg.alert('', status.status);
						}
					}
				}
			});
		}
		store.load();
	}

});