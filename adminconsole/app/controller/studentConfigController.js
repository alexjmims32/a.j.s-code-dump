Ext.define('login.controller.studentConfigController', {
    extend:'Ext.app.Controller',
    stores:[   
    'studentConfigStore',
    'updateRoleStore',
    'mainModuleStore',
    'updateModuleStore'
    ],
    models:[
    'studentConfigModel',
    'studentConfigUpdateModel'
    
    ],
    views:[
    'studentConfigView',
    'roleView',
    'tabView',
    'mainModuleView'
        
    ],
    init:function () {
        this.control({
            'studentConfigView':{
                beforeactivate:this.roles      
            },
            'studentConfigView button[action=submit]':{
                click:this.selectionchange
            },
            'mainModuleView button[action=submit]':{
                click:this.moduleChange
            },
            'mainModuleView':{
                beforeactivate:this.modules
            }
        
        });
    },  
    modules:function () {
        var store = Ext.getStore('mainModuleStore');        
        
        store.getProxy().url = webserver +'getModules?campusCode=' +campusCode;
        store.getProxy().headers = {
            Authorization:authString
        };       
        
        store.load();
    },    
    roles:function () {               
        var store = Ext.getStore('studentConfigStore');        
        var roleId = 1;
        
        store.getProxy().url = webserver +'getPrivilege?roleId=' +roleId+'&campusCode=' +campusCode;
        store.getProxy().headers = {
            Authorization:authString
        };       
        
        store.load();
    },   
    // Method to consolidate the privileges for update method
    selectionchange:function(sm,selected,eOpts){

        var grid = Ext.getCmp('configGrid');

        var gridItems = grid.items.items[0].all.elements;

        var modulePrivileges = '';
        for(i=0;i<gridItems.length;i++){
            var node = gridItems[i];
            var access=  node.cells[3].firstChild.firstChild.checked;
            var auth=node.cells[4].firstChild.firstChild.checked; 
            
            if(access==true){
                access='Y';
            }else{
                access='N';
            }
            
            if(auth==true){
                auth='Y';
            }else{
                auth='N';
            }

            //            modulePrivileges += node.cells[0].lastChild.innerText + ':' + node.cells[1].firstChild.firstChild.checked + ':' + node.cells[2].firstChild.firstChild.checked + ';';
            modulePrivileges += node.cells[1].firstChild.firstChild.data + ',' + access + ',' + auth + ';';
        }
        
        var store = Ext.getStore('updateRoleStore');
        store.getProxy().url = webserver +'updatePrivilege?updateValue='+modulePrivileges+'&lastModifiedBy='+loginId;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            var store1 = Ext.getStore('updateRoleStore');
            var status = store1.getProxy().getReader().rawData;
            if (status.status == 'SUCCESS') {
                Ext.Msg.alert('  ', 'Privileges updated successfully.', function () {                   
                    var gridsAndTrees = Ext.ComponentQuery.query('configGrid, roleView');               
                   
                });
            }else{
                Ext.Msg.alert('   ', ' Update failed');
            }
        }
        store.load();
    },
    moduleChange:function(sm,selected,eOpts){

        var grid = Ext.getCmp('moduleGrid');

        var gridItems = grid.items.items[0].all.elements;

        var modulePrivileges = '';
        for(i=0;i<gridItems.length;i++){
            var node = gridItems[i];
            var show=  node.cells[3].firstChild.firstChild.checked;
            var auth=node.cells[4].firstChild.firstChild.checked; 
               
            if(show==true){
                show='Y';
            }else{
                show='N';
            }
            if(auth==true){
                auth='Y';
            }else{
                auth='N';
            }

            //            modulePrivileges += node.cells[0].lastChild.innerText + ':' + node.cells[1].firstChild.firstChild.checked + ':' + node.cells[2].firstChild.firstChild.checked + ';';
            modulePrivileges += node.cells[0].firstChild.firstChild.data +',' + show +',' + auth +';';
        }
        
        var store = Ext.getStore('updateModuleStore');
        store.getProxy().url = webserver +'updateModule?updateValue='+modulePrivileges+'&lastModifiedBy='+loginId;
        store.getProxy().headers = {
            Authorization:authString
        };
        store.getProxy().afterRequest = function () {
            var store1 = Ext.getStore('updateModuleStore');
            var status = store1.getProxy().getReader().rawData;
            if (status.status == 'SUCCESS') {
                Ext.Msg.alert('  ', 'Modules updated successfully.', function () {                   
                    var grid = Ext.ComponentQuery.query('moduleGrid, roleView');               
                   
                });
            }else{
                Ext.Msg.alert('   ', ' Update failed');
            }
        }
        store.load();
    }
});