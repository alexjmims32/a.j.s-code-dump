Ext.define('login.controller.clientListController', {
    extend:'Ext.app.Controller',
    stores:[
    'clientDetailStore'
        
    ],
    models:[
    'updateModel'
    ],
    views:[
    'configListGridView',
    'clientDetailsView',
    ],
    
    init:function () {
        this.control({
            'configListGridView':{
                afterrender:function(){
                    Ext.getCmp('cVersion').on('click', function(evt, el, rowIndex, colIndex,target,record) {
                        if (typeof login.view.clientDetailsView == 'function') {
                            login.view.clientDetailsView = Ext.create(login.view.clientDetailsView);
                        }
                        var welcome = Ext.widget('clientDetailsView');
                        viewport.add(welcome);
                        viewport.layout.setActiveItem(welcome);
        
                        var store = Ext.getStore('clientDetailStore');
                        store.getProxy().url = webserver + 'getVersions?client=' + record.data.client;
                        store.getProxy().headers = {
                            Authorization:authString
                        };
                        store.getProxy().afterRequest = function () {
//                            myMask.hide();
                            var store1 = Ext.getStore('clientDetailStore');
                            var status = store1.getProxy().getReader().rawData;
                            if (status.status == 'SUCCESS') {
                                var configList = Ext.widget('clientDetailsView');
                                viewport.add(configList);
                                viewport.layout.setActiveItem(configList);
                
                                viewport.unmask();
                                viewport.show();
                            }
        
                        }
                        store.load();

                    });
                }
            
            }
        });
    },
    detailsScreen:function () {
       
        
      
    }

        
});
    
