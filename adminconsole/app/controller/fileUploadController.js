Ext.define('login.controller.fileUploadController', {
    extend : 'Ext.app.Controller',
 
    //define the views
    views : ['imageUploadForm'],
 
    //special method that is called when your application boots
    init : function() {
                
        //control function makes it easy to listen to events on 
        //your view classes and take some action with a handler function
        this.control({
                    
            //when the viewport is rendered
            'viewport > panel' : {
                render : this.onPanelRendered
            },
            //when you click Upload file button
            'fileuploadform button[action=uploadFile]' : {
                click : this.uploadFile   
            }
        });
    },
 
    onPanelRendered : function() {
        //just a console log to show when the panel is rendered
        console.log('The panel was rendered');
    },
            
    displayMessage : function(title, format){
        var msgCt;
        if(!msgCt){
            msgCt = Ext.DomHelper.insertFirst(document.body, {
                id:'msg-div'
            }, true);
        }
        var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
        var m = Ext.DomHelper.append(msgCt, this.createBox(title, s), true);
        m.hide();
        m.slideIn('t').ghost("t", {
            delay: 1000, 
            remove: true
        });
    },
            
    createBox : function (t, s){
        return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
    },
 
    uploadFile : function(button) {
        //just a console log to show when the file Upload starts
        console.log('File Upload in progress');
                                
        var form = button.up('form').getForm();
        if(form.isValid()){
            form.submit({
                url: 'http://localhost:8080/enroll/'+'FileUpload',
                waitMsg: 'Uploading your file...',
                scope: this,
                success: function(form, action){
                    // server responded with success = true
                    //                    response = action.response.responseText;
                    //                    if(response.success){
                    this.displayMessage('File Upload', 'Succefully uploaded to the Server');
                    form.reset();
                //                    }
                },
                failure: function(form, action){
                    if (action.failureType === CONNECT_FAILURE) {
                        Ext.Msg.alert('Error', 'Status:'+action.response.status+': '+
                            action.response.statusText);
                    }
                    if (action.failureType === SERVER_INVALID){
                        // server responded with success = false
                        Ext.Msg.alert('Invalid', action.result.errormsg);
                    }
                }
            });
        }
    }   
        
});