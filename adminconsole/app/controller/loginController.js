Ext.define('login.controller.loginController', {
	extend: 'Ext.app.Controller',

	stores: [
		'loginStore',
		'treeStore',
		'feedTrackerStore'
	],

	models: [
		//        'loginModel'
	],

	views: [
		'loginView',
		'welcomeView',
		'configListGridView',
		'tabView',
		'notificationListView',
		'addNotificationView',
		'mapView',
		'emergencyContactsView',
		'emergencyContactView',
		'addEmergencyContactView',
		'roleView',
		'studentConfigView',
		'addFeedModuleView',
		'addFeedLinkView',
		'moduleListView',
		'feedbackListView',
		'helpView',
		'faqListView',
		'addReportView',
		'imageUploadForm',
		'termAndConditionView'
	],

	init: function() {
		me = this;
		this.control({
			'loginView button[action=signin]': {
				click: this.loginSubmit
			},
			'tabView button[action=logout]': {
				click: this.processLogout
			}
		});
	},
	processLogout: function() {
		Ext.getCmp('username').reset();
		Ext.getCmp('password').reset();

		window.open('index.html', '_self');
	},
	loginSubmit: function() {
		var username = Ext.getCmp('username').getValue();
		var password = Ext.getCmp('password').getValue();
		username = username.toLowerCase();
		Ext.getCmp('username').setValue(username);
		var encCred = username + ':' + password
		var encodePassword = Base64.encode(encCred);
		authString = 'Basic ' + encodePassword;
		if (username == '' || password == '') {
			Ext.Msg.alert('Login Error', 'Please provide Username and Password.', function(btn, text) {
				if (btn == 'ok') {
					if (username == '') {
						Ext.getCmp('username').focus();
					} else {
						Ext.getCmp('password').focus();
					}

				}
			});
		} else {
			var store = Ext.getStore('loginStore');
			store.getProxy().url = notifWebserver + 'login?username=' + username + '&password=' + encodePassword + '&source=' + source;
			store.getProxy().headers = {
				Authorization: authString
			}
			store.getProxy().afterRequest = function() {
				myMask.hide();

				if (httpStatus == 401) {
					var loginView = Ext.getCmp('loginView');
					viewport.layout.setActiveItem(loginView);
				} else {
					var status = store.getProxy().getReader().rawData;
					if (status.status == 'success') {
						loginId = username;
						var menu = Ext.widget('tabView');
						var privilegeObj = status.privileges;
						var count = 0;
						// for (var i = 0; i < privilegeObj.length; i++) {
						// var type = privilegeObj[i].type;
						// if (type == 'ADMIN_ACCESS') {
						// var privileges = privilegeObj[i].value.privilege;

						for (var j = 0; j < privilegeObj.length; j++) {
							if (privilegeObj[j].value == 'Y')
								count++;
						}

						for (var j = 0; j < privilegeObj.length; j++) {
							var type = privilegeObj[j].type;
							var accessFlag = privilegeObj[j].value;
							var privCode = privilegeObj[j].name;
							if (type == 'ADMIN_ACCESS') {
								if (privCode == 'NOTIFICATIONS' && accessFlag == 'Y') {
									menu.add({
										id: 'notificationsTab',
										title: 'Notifications',
										xtype: 'notificationListView'
									});
									menu.setActiveTab('notificationsTab');
									serviceReq = 'Y';
								}
								if (privCode == 'FEEDS' && accessFlag == 'Y') {
									menu.add({
										id: 'moduleTab',
										title: 'Feeds',
										xtype: 'moduleListView'
									});
								}
								if (privCode == 'EMERGENCYCONTACTS' && accessFlag == 'Y') {
									menu.add({
										id: 'emergencyContactsTab',
										title: 'Contacts',
										xtype: 'emergencyContactsView'

									});
								}
								if (privCode == 'MAPS' && accessFlag == 'Y') {
									menu.add({
										id: 'mapsTab',
										title: 'Maps',
										xtype: 'mapView'
									});
								}
								if (privCode == 'ROLES' && accessFlag == 'Y') {
									menu.add({
										id: 'rolesTab',
										title: 'Feature Visibility',
										xtype: 'roleView'

									});
								}
								if (privCode == 'HELP-ABOUT' && accessFlag == 'Y') {
									menu.add({
										id: 'helpAboutTab',
										title: 'Help-About',
										xtype: 'helpView'

									});
								}
								if (privCode == 'TERMSANDCONDITIONS' && accessFlag == 'Y') {
									menu.add({
										id: 'terms&ConditionsTab',
										title: 'Terms & Conditions',
										xtype: 'termAndConditionView'

									});
								}
								if (privCode == 'HELP-FAQ' && accessFlag == 'Y') {
									menu.add({
										id: 'helpFaqTab',
										title: 'Help-Faq',
										xtype: 'faqGridView'

									});
								}
								if (privCode == 'USERS' && accessFlag == 'Y') {
									menu.add({
										id: 'userTab',
										title: 'Users',
										xtype: 'userListView'
									});
								}
								if (privCode == 'FEEDBACK' && accessFlag == 'Y') {
									menu.add({
										id: 'feedbackTab',
										title: 'Feedback',
										xtype: 'feedbackListView'

									});
								}
							}
						}
						// }

						if (count > 0) {
							viewport.add(menu);
							viewport.layout.setActiveItem(menu);
							console.log('USER LOGIN SUCCESS');
						} else {
							Ext.Msg.alert('  ', 'You have no Privileges.Please contact adminstrator.');
						}
					} else {
						Ext.Msg.alert('  ', 'You are not authorized to access this site.');
					}
				}
			}
		}
		store.load();
	}


});