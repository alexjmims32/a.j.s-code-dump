Ext.define('login.controller.mapController', {
	extend: 'Ext.app.Controller',
	stores: [
		'mapStore',
		'mapUpdateStore',
		'addMapComboStore',
		'categoryStore'
	],
	models: [
		'mapModel'
	],
	views: [
		'mapView',
		'mapListView',
		'addMapView',
		'tabView',
		'mapEditMenuView',
		'createMapView',
		'updateMapView'
	],
	refs: [{
		//reference to the country Tree
		ref: 'myCountryTree',
		selector: 'myTree'
	}, {
		//reference to the country window
		ref: 'myCountryWindow',
		selector: 'menuView'
	}],
	init: function() {
		this.control({
			'mapView': {
				beforeactivate: this.mapList,
				//                itemdblclick:this.mapView,
				itemcontextmenu: this.treeRightClick
			},
			'mapView button[action=back]': {
				click: this.maps
			},
			//            'mapView button[action=search]':{
			//                click:this.mapFromCombo
			//            },
			'mapListView button[action=back]': {
				click: this.maps
			},
			'addMapView button[action=back]': {
				click: this.maps
			},
			'mapListView button[action=submit]': {
				click: this.updateMap
			},
			'mapListView button[action=reset]': {
				click: this.resetAll
			},
			'mapView button[action=ADD]': {
				click: this.addMapView
			},
			'addMapView button[action=submit]': {
				click: this.addMap
			},
			'addMapView button[action=reset]': {
				click: this.resetView
			},
			refs: [{
				//reference to the country window
				ref: 'mapListView',
				selector: 'mapEditMenuView'
			}],
			'mapEditMenuView menuitem[text=Edit]': {
				click: this.mapView
			},
			'mapEditMenuView menuitem[text=Delete]': {
				click: this.deleteMap
			}
		});
	},
	resetView: function() {
		//        Ext.getCmp('addMapId').reset();
		Ext.getCmp('mapCC').reset();
		Ext.getCmp('addBldgCode').reset();
		Ext.getCmp('addBldgName').reset();
		Ext.getCmp('addBldgDesc').reset();
		Ext.getCmp('addPhone').reset();
		Ext.getCmp('addEmail').reset();
		Ext.getCmp('addImageUrl').reset();
		Ext.getCmp('addLatitude').reset();
		Ext.getCmp('addLongitude').reset();
		Ext.getCmp('addTitle').reset();
		Ext.getCmp('addAddress').reset();
	},

	resetAll: function() {
		//        Ext.getCmp('mapId').reset();
		Ext.getCmp('eMapCC').setValue(campusCode);
		Ext.getCmp('mapBldgCode').setValue(selectRecord.data.buildingCode);
		Ext.getCmp('mapBldgName').setValue(selectRecord.data.buildingName);
		Ext.getCmp('mapBldgDesc').setValue(selectRecord.data.buildingDesc);
		Ext.getCmp('mapPhone').setValue(selectRecord.data.phone);
		Ext.getCmp('mapEmail').setValue(selectRecord.data.email);
		Ext.getCmp('mapImageUrl').setValue(selectRecord.data.imgUrl);
		Ext.getCmp('mapLatitude').setValue(selectRecord.data.latitude);
		Ext.getCmp('mapLongitude').setValue(selectRecord.data.longitude);
		Ext.getCmp('mapTitle').setValue(selectRecord.data.title);
		Ext.getCmp('editAddress').setValue(selectRecord.data.address);
	},
	addMapView: function() {


		var lsStore = Ext.getStore('addMapComboStore');
		lsStore.getProxy().headers = {
			Authorization: authString
		};
		lsStore.getProxy().url = webserver + 'getCampusDropDown?campusCode=' + campusCode;

		lsStore.getProxy().afterRequest = function() {
			var caStore = Ext.getStore('categoryStore');
			caStore.getProxy().headers = {
				Authorization: authString
			};
			caStore.getProxy().url = webserver + 'getCategory?';
			if (typeof login.view.createMapView == 'function') {
				login.view.createMapView = Ext.create(login.view.createMapView);
			}
			Ext.getCmp('mapCC').reset();
			Ext.getCmp('addBldgCode').reset();
			Ext.getCmp('addBldgName').reset();
			Ext.getCmp('addBldgDesc').reset();
			Ext.getCmp('addPhone').reset();
			Ext.getCmp('addEmail').reset();
			Ext.getCmp('addImageUrl').reset();
			Ext.getCmp('addLatitude').reset();
			Ext.getCmp('addLongitude').reset();
			Ext.getCmp('addTitle').reset();
			Ext.getCmp('addAddress').reset();
			Ext.getCmp('mapCategory').reset();
			var map = login.view.createMapView;
			viewport.add(map);
			viewport.layout.setActiveItem(map);
		}
		lsStore.load();
	},
	maps: function() {
		var tab = Ext.getCmp('tabView');
		viewport.add(tab);
		viewport.layout.setActiveItem(tab);
	},
	mapList: function() {
		var caStore = Ext.getStore('categoryStore');
		caStore.getProxy().headers = {
			Authorization: authString
		};
		caStore.getProxy().url = webserver + 'getCategory?';
		caStore.getProxy().afterRequest = function() {
			var store = Ext.getStore('mapStore');
			store.getProxy().url = webserver + 'maps';
			store.getProxy().headers = {
				Authorization: authString
			};

			store.load();
		}
		caStore.load();
	},
	mapView: function(grid, record) {


		var lsStore = Ext.getStore('addMapComboStore');
		lsStore.getProxy().headers = {
			Authorization: authString
		};
		lsStore.getProxy().url = webserver + 'getCampusDropDown?campusCode=' + campusCode;

		lsStore.getProxy().afterRequest = function() {
			var caStore = Ext.getStore('categoryStore');
			caStore.getProxy().headers = {
				Authorization: authString
			};
			caStore.getProxy().url = webserver + 'getCategory';
			caStore.load();
			if (typeof login.view.updateMapView == 'function') {
				login.view.updateMapView = Ext.create(login.view.updateMapView);
			}
			var mapList = login.view.updateMapView;
			//            var id = Ext.getCmp('mapId');
			var newId = selectRecord.data.mapId;
			var campCode = Ext.getCmp('eMapCC');
			var newCampCode = campCode.setValue(selectRecord.data.campusCode);
			var bldgCode = Ext.getCmp('mapBldgCode');
			var newBldgCode = bldgCode.setValue(selectRecord.data.buildingCode);
			var bldgName = Ext.getCmp('mapBldgName');
			var newBldgName = bldgName.setValue(selectRecord.data.buildingName);
			var bldgDesc = Ext.getCmp('mapBldgDesc');
			var newBldgDesc = bldgDesc.setValue(selectRecord.data.buildingDesc);
			var mapPhone = Ext.getCmp('mapPhone');
			var newMapPhone = mapPhone.setValue(selectRecord.data.phone);
			var email = Ext.getCmp('mapEmail');
			var newEmail = email.setValue(selectRecord.data.email);
			var imgUrl = Ext.getCmp('mapImageUrl');
			var newImageUrl = imgUrl.setValue(selectRecord.data.imgUrl);
			var latitude = Ext.getCmp('mapLatitude');
			var newLatitude = latitude.setValue(selectRecord.data.latitude);
			var longitude = Ext.getCmp('mapLongitude');
			var newLongitude = longitude.setValue(selectRecord.data.longitude);
			var mapTitle = Ext.getCmp('mapTitle');
			var newMapTitle = mapTitle.setValue(selectRecord.data.title);
			var address = Ext.getCmp('editAddress');
			var newAddress = address.setValue(selectRecord.data.address);
			if (campusCode != 'SPSU') {
				var values = selectRecord.get('category').split(',');
				Ext.getCmp('emapCategory').setValue(values);
			} else {
				category = Ext.getCmp('emapCategory');
				newCategory = category.setValue(selectRecord.data.category);
			}
			viewport.add(mapList);
			viewport.layout.setActiveItem(mapList);
		}
		lsStore.load();

	},
	updateMap: function() {
		var store = Ext.getStore('mapUpdateStore');
		var mapId = selectRecord.data.mapId;
		var mapCampusCode = Ext.getCmp('eMapCC').getValue();
		var mapBldgCode = Ext.getCmp('mapBldgCode').getValue();
		var mapBldgName = Ext.getCmp('mapBldgName').getValue();
		var mapBldgDesc = Ext.getCmp('mapBldgDesc').getValue();
		var mapPhone = Ext.getCmp('mapPhone').getValue();
		var mapEmail = Ext.getCmp('mapEmail').getValue();
		var mapImageUrl = Ext.getCmp('mapImageUrl').getValue();
		var mapLatitude = Ext.getCmp('mapLatitude').getValue();
		var mapLongitude = Ext.getCmp('mapLongitude').getValue();
		var mapTitle = Ext.getCmp('mapTitle').getValue();
		var mapAddress = Ext.getCmp('editAddress').getValue();
		var category = Ext.getCmp('emapCategory').getValue();
		var emailReg = new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$");
		var phoneReg = new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$");

		if (mapCampusCode != '' && mapBldgCode != '' && mapBldgName != '' && mapLatitude != '' && mapLongitude != '' && mapTitle != '' && category != '') {
			if (!(mapEmail == '' || mapEmail == null) && !emailReg.test(mapEmail)) {
				Ext.Msg.alert('  ', 'Email is not valid format.')
			} else {
				if (!(mapPhone == '' || mapPhone == null) && !phoneReg.test(mapPhone)) {
					Ext.Msg.alert('  ', 'Phone is not valid format.')
				} else {
					if (mapLatitude.indexOf(' ') >= 0) {
						Ext.Msg.alert('  ', 'Latitude is not valid format.');
					} else {
						if (mapLongitude.indexOf(' ') >= 0) {
							Ext.Msg.alert('  ', 'Longitude is not valid format.');
						} else {
							store.getProxy().url = webserver + 'updateMaps?campusCode=' + mapCampusCode + '&buildingCode=' + mapBldgCode + '&buildingName=' + mapBldgName + '&buildingDesc=' + mapBldgDesc + '&phone=' + mapPhone + '&email=' + mapEmail + '&imgUrl=' + encodeURIComponent(mapImageUrl) + '&longitude=' + mapLongitude + '&latitude=' + mapLatitude + '&id=' + mapId + '&lastModifiedBy=' + loginId + '&address=' + mapAddress + '&category=' + encodeURIComponent(category);
							store.getProxy().headers = {
								Authorization: authString
							};
							store.getProxy().afterRequest = function() {
								// myMask.hide();
								var store1 = Ext.getStore('mapUpdateStore');
								var status = store1.getProxy().getReader().rawData;
								if (status.status == 'SUCCESS') {
									Ext.Msg.alert('  ', 'Map updated successfully.', function() {
										var tab = Ext.getCmp('tabView');
										viewport.add(tab);
										viewport.layout.setActiveItem(tab);
										var mapStore = Ext.getStore('mapStore');
										mapStore.load();
									});
								} else {
									Ext.Msg.alert('', 'Error updating map details. Please check the data!');
								}
							}
							store.load();
						}
					}
				}

			}
		} else {
			Ext.Msg.alert('  ', 'Campus,Category,Building Code,Building Name,Latitude,Longitude and Title fields are mandatory.');
		}
	},
	addMap: function() {
		var store = Ext.getStore('mapUpdateStore');
		var mapCampusCode = Ext.getCmp('mapCC').getValue();

		if (mapCampusCode == null) {
			mapCampusCode = '';
		}

		var mapBldgCode = Ext.getCmp('addBldgCode').getValue();
		var mapBldgName = Ext.getCmp('addBldgName').getValue();
		var mapBldgDesc = Ext.getCmp('addBldgDesc').getValue();
		var mapPhone = Ext.getCmp('addPhone').getValue();
		var mapEmail = Ext.getCmp('addEmail').getValue();
		var mapImageUrl = Ext.getCmp('addImageUrl').getValue();
		var mapLatitude = Ext.getCmp('addLatitude').getValue();
		var mapLongitude = Ext.getCmp('addLongitude').getValue();
		var mapTitle = Ext.getCmp('addTitle').getValue();
		var mapAddress = Ext.getCmp('addAddress').getValue();
		var category = Ext.getCmp('mapCategory').getValue();
		var id = '';

		var emailReg = new RegExp("^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$");
		var phoneReg = new RegExp("^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$");
		var latitudeReg = new RegExp("/^\s+$/");
		var longitudeReg = new RegExp("/^\s+$/");

		if (mapCampusCode != '' && mapBldgCode != '' && mapBldgName != '' && mapLatitude != '' && mapLongitude != '' && mapTitle != '' && category != '') {
			if (!(mapEmail == '' || mapEmail == null) && !emailReg.test(mapEmail)) {
				Ext.Msg.alert('  ', 'Email is not valid format.')
			} else {
				if (!(mapPhone == '' || mapPhone == null) && !phoneReg.test(mapPhone)) {
					Ext.Msg.alert('  ', 'Phone is not valid format.');
				} else {
					if (mapLatitude.indexOf(' ') >= 0) {
						Ext.Msg.alert('  ', 'Latitude is not valid format.');
					} else {
						if (mapLongitude.indexOf(' ') >= 0) {
							Ext.Msg.alert('  ', 'Longitude is not valid format.');
						} else {
							store.getProxy().url = webserver + 'updateMaps?campusCode=' + mapCampusCode + '&buildingCode=' + mapBldgCode + '&buildingName=' + mapBldgName + '&buildingDesc=' + mapBldgDesc + '&phone=' + mapPhone + '&email=' + mapEmail + '&imgUrl=' + encodeURIComponent(mapImageUrl) + '&longitude=' + mapLongitude + '&latitude=' + mapLatitude + '&id=' + id + '&lastModifiedBy=' + loginId + '&address=' + mapAddress + '&category=' + encodeURIComponent(category);
							store.getProxy().headers = {
								Authorization: authString
							};
							store.getProxy().afterRequest = function() {
								//            myMask.hide();
								var store1 = Ext.getStore('mapUpdateStore');
								var status = store1.getProxy().getReader().rawData;
								if (status.status == 'SUCCESS') {
									Ext.Msg.alert('', 'Map added successfully.', function() {
										var tab = Ext.getCmp('tabView');
										viewport.add(tab);
										viewport.layout.setActiveItem(tab);
										var mapStore = Ext.getStore('mapStore');
										mapStore.load();
										Ext.getCmp('mapCC').reset();
										Ext.getCmp('addBldgCode').reset();
										Ext.getCmp('addBldgName').reset();
										Ext.getCmp('addBldgDesc').reset();
										Ext.getCmp('addPhone').reset();
										Ext.getCmp('addEmail').reset();
										Ext.getCmp('addImageUrl').reset();
										Ext.getCmp('addLatitude').reset();
										Ext.getCmp('addLongitude').reset();
										Ext.getCmp('addTitle').reset();
										Ext.getCmp('addAddress').reset();
									});
								} else {
									Ext.Msg.alert('', 'Error adding map details. Please check the data!');
								}

							}
							store.load();
						}
					}
				}
			}
		} else {
			Ext.Msg.alert('  ', 'Campus,Title,Category,Building Code,Building Name,Latitude and Longitude fields are mandatory.');
		}

	},
	deleteMap: function(item, e) {
		var store = Ext.getStore('mapUpdateStore');

		store.getProxy().url = webserver + 'deleteMap?id=' + selectRecord.data.mapId;
		store.getProxy().headers = {
			Authorization: authString
		};
		store.getProxy().afterRequest = function() {
			myMask.hide();
			var store1 = Ext.getStore('mapUpdateStore');
			var status = store1.getProxy().getReader().rawData;
			if (status.status == 'SUCCESS') {
				Ext.Msg.alert('', 'Map deleted successfully.', function() {
					var tab = Ext.getCmp('tabView');
					viewport.add(tab);
					viewport.layout.setActiveItem(tab);
					var listStore = Ext.getStore('mapStore');
					listStore.load();
				});
			} else {
				Ext.Msg.alert('', status.status);
			}
		}
		store.load();
	},
	treeRightClick: function(view, record, item, index, e) {
		//stop the default action
		e.stopEvent();
		selectRecord = record;
		if (typeof login.view.mapEditMenuView == 'function') {
			login.view.mapEditMenuView = Ext.create(login.view.mapEditMenuView);
		}
		var module = login.view.mapEditMenuView;
		Ext.getCmp('editMap').setVisible(true);
		Ext.getCmp('deleteMap').setVisible(true);
		module.showAt(e.getXY());

		console.log(selectRecord);
	}
});