Ext.define('Ext.ux.IframeComponent', {
    extend: 'Ext.Component',

    xtype: 'iframecmp',

    config: {
        /**
         * @cfg {String} url URL to load
         */
        url     : null,

        /**
         * @cfg
         * @inheritdoc
         *
         * Add your own style
         */
        baseCls : Ext.baseCSSPrefix + 'iframe'
    },

    initialize: function() {
        var me = this;
        me.callParent();

        me.iframe = this.element.createChild({
            tag   : 'iframe',
            src   : this.getUrl(),
            style : 'width: 100%; height: 100%;'
        });

        me.relayEvents(me.iframe, '*');
    }

});