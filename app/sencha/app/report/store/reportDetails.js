Ext.define('mobEdu.report.store.reportDetails', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.report.model.report'
	],

	config: {
		storeId: 'mobEdu.report.store.reportDetails',
		model: 'mobEdu.report.model.report',
		proxy: {
			type: 'localstorage',
			id: 'reportDetails',
			reader: {
				type: 'xml'
			}
		}
	}
});