Ext.define('mobEdu.report.store.reports', {
    extend: 'Ext.data.Store',

    requires: ['Ext.data.Store',
        'mobEdu.report.model.report'
    ],

    config: {
        storeId: 'mobEdu.report.store.reports',
        autoLoad: true,
        autoSync: true,
        model: 'mobEdu.report.model.report',
        data: [{
            image: mobEdu.util.getResourcePath() + 'images/report1.png',
            subject: 'Road Damage causing inconvience for the people',
            reportedDate: '16 Feb 2015',
            location: 'Street 4, near mary towers',
            status: 'Open',
            category: 'Street Services(City)',
        }],
        proxy: {
            type: 'localstorage',
            id: 'toStoreLocal'
        }
    }
});