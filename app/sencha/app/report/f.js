Ext.define('mobEdu.report.f', {
	requires: ['mobEdu.report.store.reports'],
	statics: {

		loadReportsModule: function() {

			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.report.view.report');

		},

		captureImage: function() {
			// alert("camera");
			Ext.device.Camera.capture({
				source: 'camera',
				destination: 'file',

				success: function(url) {
					//show the newly captured image in a full screen Ext.Img component:
					Ext.create('Ext.Img', {
						src: url,
						fullscreen: true
					});
				},
				failure: function() {
					Ext.Msg.show({
						message: 'Image capture failed.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			});
		},
		showReportDetails: function(view, index, item, e, record) {
			var store = mobEdu.util.getStore('mobEdu.report.store.reportDetails');
			store.removeAll();
			store.removed = [];
			store.sync();
			store.add(record.copy());
			store.sync();
			mobEdu.util.updatePrevView(mobEdu.report.f.loadReports);
			mobEdu.util.show('mobEdu.report.view.reportDetails');
		},
		loadReports: function() {
			mobEdu.util.getStore('mobEdu.report.store.reports');
			mobEdu.util.updatePrevView(mobEdu.report.f.loadReportsModule);
			mobEdu.util.show('mobEdu.report.view.viewReports');
		},
		submitReportInfo: function() {
			var category = Ext.getCmp('reportCategory').getValue();
			var type = Ext.getCmp('reportType').getValue();
			var location = Ext.getCmp('location').getValue();
			var reportInfo = Ext.getCmp('reportInfo').getValue();
			var reportStore = mobEdu.util.getStore('mobEdu.report.store.reports');
			if (mobEdu.report.f.isExist(category) && mobEdu.report.f.isExist(type) && mobEdu.report.f.isExist(location) && mobEdu.report.f.isExist(reportInfo)) {
				reportStore.add({
					image: mobEdu.util.getResourcePath() + 'images/noimage.png',

					subject: reportInfo,
					reportedDate: Ext.Date.format(new Date(), 'd M, Y'),
					location: location,
					status: 'Open',
					category: category,
				});
				reportStore.sync();
				mobEdu.report.f.showReporterDetailsView();

			} else {
				Ext.Msg.show({
					message: 'All Fields are mandatory.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		getCurrentLocation: function() {
			var geoLocationOptions = {
				maximumAge: 3000,
				timeout: 5000,
				enableHighAccuracy: true
			};
			var marker, directionsRenderer, directionsService;
			navigator.geolocation.getCurrentPosition(geoLocationSuccess, geoLocationError, geoLocationOptions);

			function geoLocationSuccess(position) {

				directionsService = new google.maps.DirectionsService();
				directionsRenderer = new google.maps.DirectionsRenderer(dirMap.mapRendererOptions);
				directionsRenderer.setMap(dirMap.getMap());

				marker = new google.maps.Marker({
					map: dirMap.getMap(),
					visible: true
				});
			}

			function geoLocationError() {
				Ext.Viewport.unmask();
				Ext.Msg.show({
					message: '<p>Error while getting geolocation.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						// if (btn == 'ok') {
						// 	mobEdu.maps.f.showLocationDetails();
						// }
					}
				});
			}

		},
		isExist: function(value) {
			if (value == null || value == undefined || value == '')
				return false;
			return true;
		},
		submitReporterData: function() {
			var annonomous = Ext.getCmp('Annonomous').getValue();
			if (annonomous == 1) {
				mobEdu.report.f.sendReporterInfo();
			} else {
				var nameValue = Ext.getCmp('fullName').getValue();
				var cEmailValue = Ext.getCmp('email').getValue();
				var phone = Ext.getCmp('phone').getValue();
				phone = phone.replace(/-|\(|\)/g, "");
				if (nameValue != null && cEmailValue != null && nameValue != '' && cEmailValue != '') {
					var firstName = Ext.getCmp('fullName');
					var regExp = new RegExp("^[a-zA-Z ]+$");
					if (!regExp.test(nameValue)) {
						Ext.Msg.show({
							id: 'name',
							name: 'name',
							cls: 'msgbox',
							title: null,
							message: '<p>Invalid full name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									firstName.focus(true);
								}
							}
						});
					} else {
						var mail = Ext.getCmp('email');
						var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
						if (regMail.test(cEmailValue) == false) {
							Ext.Msg.show({
								id: 'email',
								name: 'email',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid e-mail address.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										mail.focus(true);
									}
								}
							});
						} else {
							if (nameValue.charAt(0) == ' ') {
								Ext.Msg.show({
									id: 'name',
									name: 'comment',
									title: null,
									cls: 'msgbox',
									message: '<p>Name first letter must be alphanumeric.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											nameField.focus(true);
											nameField.setValue('');
										}
									}
								});
							} else {
								if (phone.length != 10 && phone.length > 0) {
									Ext.Msg.show({
										id: 'phone1msg',
										name: 'phone1msg',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid Phone Number.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												phone.focus(true);
											}
										}
									});
								} else {
									mobEdu.report.f.sendReporterInfo(nameValue, cEmailValue, phone);
								}
							}
						}
					}
				} else {
					Ext.Msg.show({
						message: '<p>Please provide all mandatory fields.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox',
					});
				}
			}

		},
		sendReporterInfo: function(nameValue, cEmailValue, phone) {
			Ext.Msg.show({
				message: '<p>Your report submitted successfully.</p>',
				buttons: Ext.MessageBox.OK,
				cls: 'msgbox',
				fn: function(btn) {
					if (btn == 'ok') {
						mobEdu.report.f.loadReports();
					}
				}
			});
		},
		showReporterDetailsView: function() {
			mobEdu.util.updatePrevView(mobEdu.report.f.loadReportsModule);
			mobEdu.util.show('mobEdu.report.view.reporterDetails');
		},
		loadReportType: function() {
			var options = new Array();
			var cnt = 0;
			var category = Ext.getCmp('reportCategory').getValue();
			if (category == 'Animal Services') {
				options[cnt++] = ({
					text: 'Dead Animal Removal',
					value: 'Dead Animal Removal'
				});
				options[cnt++] = ({
					text: 'Negelect',
					value: 'Negelect'
				});
				options[cnt++] = ({
					text: 'Other',
					value: 'Other'
				});
				options[cnt++] = ({
					text: 'Unleashed Pet',
					value: 'Unleashed Pet'
				});
			}
			if (category == 'Other') {
				options[cnt++] = ({
					text: 'Other',
					value: 'Other'
				});

			}

			if (category == 'Private Property Complaints') {
				options[cnt++] = ({
					text: 'Inoperable Vehicle',
					value: 'Inoperable Vehicle'
				});

				options[cnt++] = ({
					text: 'Nuisance ',
					value: 'Nuisance'
				});
				options[cnt++] = ({
					text: 'Other ',
					value: 'Other'
				});
				options[cnt++] = ({
					text: 'Overgrown Property',
					value: 'Overgrown Property'
				});
				options[cnt++] = ({
					text: 'Trash on Property',
					value: 'Trash on Property'
				});
			}
			if (category == 'Street Services(City)') {

				options[cnt++] = ({
					text: 'Other',
					value: 'Other'
				});

				options[cnt++] = ({
					text: 'Pothole',
					value: 'Pothole'
				});

				options[cnt++] = ({
					text: 'SideWalk Issues',
					value: 'SideWalk Issues'
				});

				options[cnt++] = ({
					text: 'Street Lights',
					value: 'Street Lights'
				});
				options[cnt++] = ({
					text: 'Traffic Lights/Road Signs',
					value: 'Traffic Lights/Road Signs'
				});
			}

			Ext.getCmp('reportType').setOptions(options);
		}
	}
});