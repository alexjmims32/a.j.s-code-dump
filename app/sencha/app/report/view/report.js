Ext.define('mobEdu.report.view.report', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.main.store.recipientsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'reportProblem',
            title: '<h1>Report</h1>'
        }, {
            xtype: 'panel',
            padding: '0 0 0 .7em',
            html: ['<table><tr><td><h3>' + 'Please provide the report details  below.' + '</h3></td></tr></table><br />']
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'selectfield',
                label: 'Category',
                labelWidth: '50%',
                name: 'Category',
                id: 'reportCategory',
                required: true,
                useClearIcon: true,
                displayField: 'text',
                // valueField: 'email',
                options: [{
                    text: 'Select a Category',
                    value: ''
                }, {
                    text: 'Animal Services',
                    value: 'Animal Services'
                }, {
                    text: 'Other',
                    value: 'Other'
                }, {
                    text: 'Private Property Complaints',
                    value: 'Private Property Complaints'
                }, {
                    text: 'Street Services(City)',
                    value: 'Street Services(City)'
                }],
                listeners: {
                    change: function() {
                        mobEdu.report.f.loadReportType();
                    }
                }
                // store: mobEdu.util.getStore('mobEdu.main.store.recipientsList')
            }, {
                xtype: 'selectfield',
                label: 'Type',
                labelWidth: '50%',
                name: 'reportType',
                id: 'reportType',
                required: true,
                useClearIcon: true,
                // displayField: 'title',
                // valueField: 'text',
                // store: mobEdu.util.getStore('mobEdu.main.store.recipientsList')
            }, {
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    label: 'Location',
                    labelWidth: '52.3%',
                    flex: 1,
                    width: '50%',
                    name: 'location',
                    id: 'location',
                    required: true,
                    useClearIcon: true,
                }, {
                    xtype: 'button',
                    name: 'searchIcon',
                    id: 'getmap',
                    maxWidth: '52px',
                    height: '52px',
                    flex: 3,
                    align: 'right',
                    text: '<img src="' + mobEdu.util.getResourcePath() + 'images/map.png" height="52px" width="52px">',
                    style: {
                        border: 'none',
                        background: 'none'
                    },
                    handler: function() {
                        mobEdu.report.f.getCurrentLocation();
                    }
                }]
            }, {
                layout: 'hbox',
                items: [{
                    flex: 1,
                    xtype: 'textareafield',
                    label: 'Additional Information',
                    labelAlign: 'top',
                    labelWidth: '80%',
                    maxRows: 4,
                    name: 'reportInfo',
                    id: 'reportInfo',
                    required: true,
                    useClearIcon: true
                }, {
                    xtype: 'button',
                    name: 'searchIcon',
                    id: 'camera',
                    maxWidth: '57px',
                    height: '48px',
                    flex: 3,
                    align: 'right',
                    text: '<img src="' + mobEdu.util.getResourcePath() + 'images/camera.png" height="64px" width="64px">',
                    style: {
                        border: 'none',
                        background: 'none'
                    },
                    handler: function() {
                        mobEdu.report.f.captureImage();
                    }
                }]
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Continue',
                align: 'right',
                handler: function() {
                    mobEdu.report.f.submitReportInfo();
                }
            }, {
                text: 'View Reports',
                align: 'right',
                handler: function() {
                    mobEdu.report.f.loadReports();
                }
            }]
        }]
    }
});