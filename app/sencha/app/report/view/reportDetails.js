Ext.define('mobEdu.report.view.reportDetails', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.report.store.reportDetails'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            setHtmlContent: true,
            padding: '10 10 10 10',
            xtype: 'dataview',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table> <tr><td><h3><b>{category}<b></h3></td></tr>' +
                '<tr><td><h3>{subject}</h3></td></tr>' +
                '<tr><td>Reported on {reportedDate}</h3></td></tr>' +
                '<tr><td><img src={image} height="100%" width="100%"> </img></td></tr>' +
                '<tr><td><h3><b>{location}<b></h3></td></tr>' +
                '<tr><td><h3><b>Status : {status}<b></h3></td></tr>' +
                '</table>', {
                    compiled: true,
                    getDescription: function(description) {
                        return mobEdu.util.convertRelativeToAbsoluteUrls(description);
                    },
                    getContent: function(content) {
                        return mobEdu.util.convertRelativeToAbsoluteUrls(content);
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.report.store.reportDetails'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Details</h1>'
        }],
        flex: 1
    }
});