Ext.define('mobEdu.report.view.viewReports', {
    extend: 'Ext.Panel',
    xtype: 'dirSearch',
    requires: [
        'mobEdu.report.store.reports'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'dirTitle',
            title: '<h1>Details</h1>'
        }, {
            docked: 'top',
            style: 'background: transparent;',
            items: [{

                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                            xtype: 'selectfield',
                            hidden: false,
                            id: 'dirSearchlName',
                            labelWidth: 0,
                            flex: 1,
                            displayField: 'text',
                            width: '48%',
                            options: [{
                                text: 'All',
                                value: ''
                            }, {
                                text: 'Animal Services',
                                value: 'Animal Services'
                            }, {
                                text: 'Other',
                                value: 'Other'
                            }, {
                                text: 'Private Property Complaints',
                                value: 'Private Property Complaints'
                            }, {
                                text: 'Street Services(City)',
                                value: 'Street Services(City)'
                            }],
                            placeHolder: 'Last Name',
                            listeners: {
                                keyup: function(textfield, e, eOpts) {
                                    var fNamefield = Ext.getCmp('dirSearchfName')
                                    mobEdu.directory.f.dirSearch(fNamefield, textfield, e);
                                },
                                clearicontap: function(textfield, e, eOpts) {
                                    var fNamefield = Ext.getCmp('dirSearchfName');
                                    mobEdu.directory.f.doCheckDirectoryBeforeSearch(fNamefield, textfield, e);
                                }
                            }
                        },
                        // }, {
                        //     xtype: 'spacer',
                        //     width: '4%',
                        //     //     style: ' background-color: #fff',
                        //     // },{
                        {
                            xtype: 'selectfield',
                            id: 'dirSearchfName',
                            labelWidth: 0,
                            flex: 1,
                            width: '48%',
                            displayField: 'text',
                            placeHolder: '',
                            options: [{
                                text: 'All',
                                value: 'All'
                            }, {
                                text: 'Open',
                                value: 'Open'
                            }, {
                                text: 'Acknowledged',
                                value: 'Acknowledged'
                            }, {
                                text: 'Submitted',
                                value: 'Submitted'
                            }, {
                                text: 'Deferred',
                                value: 'Deferred'
                            }, {
                                text: 'Closed',
                                value: 'Closed'
                            }],
                            listeners: {
                                keyup: function(textfield, e, eOpts) {
                                    var lNamefield = null
                                    mobEdu.directory.f.dirSearch(textfield, lNamefield, e);
                                },
                                clearicontap: function(textfield, e, eOpts) {
                                    var lNamefield = Ext.getCmp('dirSearchlName');
                                    mobEdu.directory.f.doCheckDirectoryBeforeOnlineSearch(textfield, null, e);
                                }
                            }
                        }
                    ]
                }
                //            {
                //                xtype:'textfield',
                //                name:'dirSearchItem',
                //                id:'dirSearchItem',
                //                placeHolder:'Please enter value to search',
                //                listeners:{
                //                    keyup: function( textfield, e, eOpts ) {
                //                        mobEdu.directory.f.dirSearch(textfield,e);
                //                    }
                //                }
                //            }
            ]
        }, {
            xtype: 'list',
            name: 'searchDirectoryList',

            id: 'searchDirectoryList',
            emptyText: 'No search results',
            itemTpl: '<table width="100%"><tr><td><img src={image} height="90px" width="100px"></img></td><td width="100%" align="left"><h3>{subject}</h3></td>{reportedDate}<td></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.report.store.reports'),
            singleSelect: true,
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        mobEdu.report.f.showReportDetails(view, index, item, e, record);
                    }, 500);
                }
            }
        }],
        flex: 1
    },
    // initialize: function() {
    //     var searchStore = mobEdu.util.getStore('mobEdu.directory.store.searchList');
    //     searchStore.addBeforeListener('load', mobEdu.directory.f.checkForDirectoryListEnd, this);
    // }
});