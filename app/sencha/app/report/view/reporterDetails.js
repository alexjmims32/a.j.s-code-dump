Ext.define('mobEdu.report.view.reporterDetails', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Details</h1>'
        }, {
            xtype: 'panel',
            padding: '0 0 0 .7em',
            html: ['<table><tr><td><h3>' + 'Please provide the report details  below.' + '</h3></td></tr></table><br />']
        }, {
            xtype: 'fieldset',
            style: 'top:27px;',
            // top: '27 px',
            items: [{
                xtype: 'togglefield',
                name: 'awesome',
                id: 'Annonomous',
                value: 0,
                label: 'Stay Annonomous?',
                labelWidth: '40%'
            }, {
                xtype: 'textfield',
                label: 'Full Name',
                labelWidth: '50%',
                placeHolder: 'Name',
                id: 'fullName',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'emailfield',
                placeHolder: 'Email',
                id: 'email',
                labelWidth: '50%',
                label: 'E-mail',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });
                    me.callParent(arguments);
                }
            }, {
                xtype: 'textfield',
                label: 'Phone number',
                useClearIcon: true,
                labelWidth: '50%',
                // labelWidth: Ext.os.is.Phone ? '45%' : '30%',
                labelWrap: true,
                id: 'phone',
                required: true,
                name: 'phone number',
                placeHolder: 'Phone Number'
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Submit',
                align: 'right',
                handler: function() {
                    mobEdu.report.f.submitReporterData();
                }
            }]

        }]
    }

});