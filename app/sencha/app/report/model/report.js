Ext.define('mobEdu.report.model.report', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'status',
            type: 'string'
        }, {
            name: 'reportId',
            type: 'string'
        }, {
            name: 'subject',
            type: 'string'
        }, {
            name: 'status',
            type: 'string'
        }, {
            name: 'email',
            type: 'string'
        }, {
            name: 'phone',
            type: 'string'
        }, {
            name: 'location',
            type: 'string'
        }, {
            name: 'reportedDate',
            type: 'string',
            // convert: function(value, record) {
            //     var nodes = record.raw.childNodes;
            //     var date = '';
            //     var val = '';
            //     if (nodes != null) {
            //         var l = nodes.length;
            //         for (var i = 0; i < l; i++) {
            //             var node = nodes[i];
            //             if (node.nodeName == 'datePosted') {
            //                 val = node.textContent;
            //                 if (val != null && val != '') {
            //                     date = val;
            //                 }
            //             } else if (node.nodeName == 'reportedDate') {
            //                 val = node.textContent;
            //                 if (val != null && val != '' && val != '[auto]') {
            //                     date = val;
            //                 } else {
            //                     date = '';
            //                 }
            //             }
            //         }
            //     }
            //     return date;
            // }
        }, {
            name: 'description',
            type: 'string'
        }, {
            name: 'category',
            type: 'string'
        }, {
            name: 'type',
            type: 'string'
        }, {
            name: 'type',
            type: 'string'
        }, {
            name: 'latitude',
            type: 'string'
        }, {
            name: 'longitude',
            type: 'string'
        }, {
            name: 'image',
            type: 'string'
        }, {
            name: 'user',
            type: 'string'
        }, {
            name: 'userStatus',
            type: 'string'
        }]
    }
});