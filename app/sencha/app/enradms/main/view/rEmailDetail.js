Ext.define('mobEdu.enradms.main.view.rEmailDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enradms.main.f',
        'mobEdu.enradms.leads.store.viewEmail'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
                xtype: 'list',
                cls: 'logo',
                disableSelection: true,
                itemTpl: new Ext.XTemplate('<table width="100%">' +
                    '<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                    '<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{versionDate}</h3></td></tr>' +
                    '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{messageStatus}</h3></td></tr>' +
                    '<tr><td valign="top"  align="right" width="50%"><h2>Message:</h2></td><td align="left"><h3>{body}</h3></td></tr>' +
                    //                    '</table></td></tr>' +
                    '</table>'
                ),
                store: mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail'),
                singleSelect: true,
                loadingText: ''
            }, {
                title: '<h1>Message Details</h1>',
                xtype: 'customToolbar',
                ui: 'light',
                docked: 'top',
                id: 'admseDetailTitle',
                name: 'eDetailTitle'
                // minHeight: '39px',
                // items:[
                //     {
                //     ui:'light',
                //     xtype:'button',
                //     text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                //     style:'background:none;border:none;',
                //     iconMask:true,
                //     handler:function () {
                //         mobEdu.util.showMenu();
                //     }
                // },
                // //Back Button
                // {
                //     xtype: 'img',
                //     cls: 'back',
                //     padding: '10 0 0 0',
                //     width: 32,
                //     height: 32,
                //     listeners: {
                //         tap: function() {
                //             mobEdu.enradms.main.f.reqEmailListBack();
                //         }
                //     }
                // }
                // ]
            }
            //            {
            //                xtype:'toolbar',
            //                docked:'top',
            //                title:'Email Details',
            //                style:'font-size:09pt; background:#1e90ff'
            //            },

        ],
        flex: 1
    }

});