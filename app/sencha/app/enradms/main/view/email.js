Ext.define('mobEdu.enradms.main.view.email', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.main.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		xtype: 'toolbar',
		cls: 'logo',
		items: [{
			xtype: 'toolbar',
			docked: 'top',
			title: '<h1>New Email</h1>'
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'To',
				labelWidth: '30%',
				name: 'reqto',
				id: 'reqto',
				required: true,
				readOnly: true,
				useClearIcon: true
			}, {
				xtype: 'textfield',
				label: 'Subject',
				labelWidth: '30%',
				name: 'reqsub',
				id: 'reqsub',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textareafield',
				label: 'Message',
				labelWidth: '30%',
				labelAlign: 'top',
				name: 'reqmsg',
				id: 'reqmsg',
				maxRows: 15,
				required: true
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Send',
				align: 'right',
				handler: function() {
					mobEdu.enradms.main.f.sentmail();
				}
			}]
		}]
	}
});