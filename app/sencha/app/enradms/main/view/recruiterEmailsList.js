Ext.define('mobEdu.enradms.main.view.recruiterEmailsList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enradms.main.f',
        'mobEdu.enradms.leads.store.email'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',

        items: [{
            xtype: 'list',
            id: 'admsrecruiterEmailsList',
            name: 'recruiterEmailsList',
            emptyText: '<h3 align="center">No Messages</h3>',
            //                plugins: [{
            //                    type: 'listpaging',
            //                    autoPaging: true,
            //                    loadMoreText: 'Load More...',
            //                    noMoreRecordsText: ''
            //                }],
            cls: 'logo',
            itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                '<tr>',
                '<tpl if="(mobEdu.enradms.main.f.hasReqEmailDirection(direction)===true)">',
                '<td width="5%"><img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
                '<tpl else>',
                '<td width="5%"><img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
                '</tpl>',
                '<tpl if="(mobEdu.enradms.main.f.hasReqReadFlag(status)===true)">',
                '<td><b><h3>{subject}</h3></b></td>' +
                '<td align="right"><b><h5>{versionDate}</h5></b></td>' +
                '<tpl else>',
                '<td style="color: #0066cc;"><h3>{subject}</h3></td>' +
                '<td style="color: #0066cc;" align="right"><h5>{versionDate}</h5></td>' +
                '</tpl>',
                '</tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enradms.leads.store.email'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    mobEdu.enradms.main.f.loadReqEmailDetail(record);
                }
            }
        }, {
            xtype: 'customToolbar',
            docked: 'top',
            title: '<h1>Messages</h1>',
            ui: 'light'
            // minHeight: '39px'
            // items:[
            //     {
            //     ui:'light',
            //     xtype:'button',
            //     text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
            //     style:'background:none;border:none;',
            //     iconMask:true,
            //     handler:function () {
            //         mobEdu.util.showMenu();
            //     }
            // },
            // {
            //     xtype: 'img',
            //     cls: 'back',
            //     padding: '10 0 0 0',
            //     width: 32,
            //     height: 32,
            //     listeners: {
            //         tap: function() {
            //             mobEdu.util.show('mobEdu.enradms.main.view.menu');
            //         }
            //     }
            // }
            // ]

        }],
        flex: 1
    }
});