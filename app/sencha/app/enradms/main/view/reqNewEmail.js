Ext.define('mobEdu.enradms.main.view.reqNewEmail',{
    extend: 'Ext.form.FormPanel',
    requires:[
    'mobEdu.enradms.main.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        xtype:'toolbar',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>New Email</h1>',
            // Back Button
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                height: 32,
                width: 32,
                cls:'back',
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.showRecruiterEList();
                }
            }
            ]
        },

        {
            xtype:'fieldset',
            items:[
            {
                xtype:'textfield',
                label:'Subject',
                labelWidth:'30%',
                name:'reqNewSub',
                id:'reqNewSub',
                required:true,
                useClearIcon:true
            },
            {
                xtype:'textareafield',
                label:'Message',
                labelWidth:'30%',
                labelAlign:'top',
                name:'reqNewMsg',
                id:'reqNewMsg',
                maxRows:15,
                required:true
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            //                style:'font-size:12pt',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Send',
                //                        ui:'confirm',
                align:'right',
                handler:function () {
                    mobEdu.enradms.main.f.sendNewMail();
                }
            }
            ]
        }
        ]
    }
});
