Ext.define('mobEdu.enradms.main.view.reqAppointmentsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.main.f',
		'mobEdu.enradms.leads.store.appointments'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'admsappointmentList',
			name: 'appointmentList',
			emptyText: '<h3 align="center">No Appointments</h3>',
			cls: 'logo',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>',
				'<td ><h3>{subject}</h3></td>' +
				'<td align="right"><h5>{appointmentDate}</h5></td>' +
				'</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enradms.main.f.showReqAppointmentDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>My Appointments</h1>'
		}],
		flex: 1
	}
});