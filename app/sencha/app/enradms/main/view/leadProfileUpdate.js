Ext.define('mobEdu.enradms.main.view.leadProfileUpdate',{
    extend: 'Ext.form.FormPanel',
    requires: [
    'mobEdu.enradms.main.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            name:'updateTitle',
            id:'updateTitle',
            // Back Button
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.showLeadDetail();
                }
            }
            ]
        },
        {
            xtype:'fieldset',
            items:[
            {
                xtype:'textfield',
                label:'First Name',
                labelWidth:'40%',
                name:'upFName',
                id:'upFName',
                required:true,
                useClearIcon:true
            },
            {
                xtype:'textfield',
                label:'Last Name',
                labelWidth:'40%',
                name:'upLName',
                id:'upLName',
                required:true,
                useClearIcon:true
            },
            {
                xtype:'datepickerfield',
                name:'update',
                id:'update',
                labelWidth:'40%',
                label:'Birth Date',
                required:true,
                useClearIcon:true
            //                        value:new Date()
            //                        picker:{
            //                            yearFrom:2012,
            //                            yearTo:2015,
            //                            slotOrder:['day', 'month', 'year']
            //                        }
            },
            {
                xtype:'emailfield',
                name:'upEmail',
                id:'upEmail',
                labelWidth:'40%',
                label:'Email',
                required:true,
                useClearIcon:true,
                initialize: function() {
                    var me    = this,
                    input = me.getInput().element.down('input');

                    input.set({
                        pattern : '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });

                    me.callParent(arguments);
                }
            },
            {
                xtype:'textfield',
                labelAlign:'left',
                labelWidth:'40%',
                label:'Phone',
                name:'upPhone',
                id:'upPhone',
                required:true,
                useClearIcon:true,
                initialize: function() {
                    var me    = this,
                    input = me.getInput().element.down('input');

                    input.set({
                        pattern : '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enradms.main.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength:10
            },
            {
                xtype:'selectfield',
                label:'Status',
                labelWidth:'40%',
                name:'upStatus',
                id:'upStatus',
                required:true,
                useClearIcon:true,
                //                        store:mobEdu.util.getStore('mobEdu.enradms.main.store.enumerations'),
                displayField:'displayValue',
                valueField:'enumValue'
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Save',
                align:'right',
                handler:function () {
                    mobEdu.enradms.main.f.saveLeadUpdateProfile();
                }
            }
            ]
        }
        ]
    }
});