Ext.define('mobEdu.enradms.main.view.reqNewAppointment', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.main.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		xtype: 'toolbar',
		items: [{
			xtype: 'toolbar',
			docked: 'top',
			ui: 'light',
			title: '<h1>New Appointment</h1>',
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'Subject',
				labelWidth: '40%',
				name: 'reqappsub',
				id: 'reqappsub',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'datepickerfield',
				name: 'reqappdate',
				id: 'reqappdate',
				labelAlign: 'top',
				labelWidth: '40%',
				label: 'Appointment Date',
				required: true,
				useClearIcon: true,
				value: new Date()
			}, {
				xtype: 'textfield',
				labelWidth: '100%',
				label: 'Time',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'spinnerfield',
				label: 'Hours',
				labelWidth: '40%',
				id: 'reqappTimeH',
				name: 'reqappTimeH',
				required: true,
				useClearIcon: true,
				minValue: 8,
				maxValue: 18,
				increment: 1,
				cycle: true
			}, {
				xtype: 'spinnerfield',
				label: 'Minutes',
				labelWidth: '40%',
				id: 'reqappTimeM',
				name: 'reqappTimeM',
				useClearIcon: true,
				minValue: 0,
				maxValue: 59,
				increment: 5,
				cycle: true
			}, {
				xtype: 'textfield',
				label: 'Location',
				labelWidth: '40%',
				name: 'reqapploc',
				id: 'reqapploc',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'selectfield',
				label: 'Status',
				labelWidth: '40%',
				name: 'reqappsta',
				id: 'reqappsta',
				required: true,
				useClearIcon: true,
				options: [{
					text: '',
					value: 'select'
				}, {
					text: 'Requested',
					value: 'REQUESTED'
				}, {
					text: 'Canceled',
					value: 'CANCELLED'
				}, {
					text: 'Scheduled',
					value: 'SCHEDULED'
				}, {
					text: 'Completed',
					value: 'COMPLETED'
				}]
			}, {
				xtype: 'textareafield',
				label: 'Description',
				labelWidth: '40%',
				labelAlign: 'top',
				name: 'reqappdes',
				id: 'reqappdes',
				required: true,
				useClearIcon: true,
				maxRows: 15
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Save',
				align: 'right',
				handler: function() {
					mobEdu.enradms.main.f.sendReqNewAppointment();
				}
			}]
		}]
	}
});