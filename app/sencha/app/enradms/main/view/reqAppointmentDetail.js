Ext.define('mobEdu.enradms.main.view.reqAppointmentDetail', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.main.f',
		'mobEdu.enradms.leads.store.appointmentDetail'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			cls: 'logo',
			disableSelection: true,
			itemTpl: new Ext.XTemplate('<table width="100%">' +
				'<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
				'<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{appointmentDate}</h3></td></tr>' +
				'<tr><td  align="right" width="50%"><h2>Location:</h2></td><td align="left"><h3>{location}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{appointmentStatus}</h3></td></tr>' +
				'<tr><td valign="top"  align="right" width="50%"><h2>Description:</h2></td><td align="left"><h3>{description}</h3></td></tr>' + '</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail'),
			singleSelect: true,
			loadingText: ''
		}, {
			title: '<h1>Appointment Details</h1>',
			xtype: 'customToolbar',
			docked: 'top'
		}],
		flex: 1
	}
});