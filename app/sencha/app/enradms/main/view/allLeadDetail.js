Ext.define('mobEdu.enradms.main.view.allLeadDetail', {
    extend:'Ext.form.FormPanel',
    requires: [
    'mobEdu.enradms.main.f'  
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            title:'<h1>Lead Details</h1>',
            ui:'light',
            // Back Button
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.showAllLeadList();
                }
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Update Profile',
                handler:function(){
                    mobEdu.enradms.main.f.allLeadsLeadProfileUpdate();
                }
            },
            {
                xtype:'spacer'
            },
            {
                text:'Assign Recruiter',
                //                        ui:'confirm',
                id:'assign',
                name:'assign',
                handler:function () {
                    mobEdu.enradms.main.f.loadAllLeadsReqSearch();

                }
            },
            {
                text:'Un-assign Recruiter',
                //                        ui:'confirm',
                id:'unAssign',
                name:'unAssign',
                handler:function () {
                    mobEdu.enradms.main.f.loadUnAssignLead();

                }
            }
            ]
        },
        {
            xtype:'panel',
            name:'allLeadsDetailSummary',
            id:'allLeadsDetailSummary'
        }

        ]
    }

});
