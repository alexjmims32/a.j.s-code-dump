Ext.define('mobEdu.enradms.main.view.allLeadsList', {
    extend:'Ext.Panel',
    xtype: 'allLeadsList',
    requires: [
    'mobEdu.enradms.main.f',
    'mobEdu.enradms.main.store.allLeadsList'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>All Leads</h1>',
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    //                            util.showMainView();
                    mobEdu.util.show('mobEdu.enradms.main.view.menu');
                }
            }
            ]
        },
        {
            xtype:'fieldset',
            docked:'top',
            cls:'searchfieldset',
            items:[
            {
                xtype:'textfield',
                name:'sALItem',
                id:'sALItem',
                width:'100%',
                placeHolder:'Search lead',
                listeners:{
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enradms.main.f.loadAllLeadsList();
                    }
                }
            }
            ]
        },
        {
            xtype:'list',
            id:'allLeadList',
            name:'allLeadList',
            emptyText:'<h3 align="center">No Leads</h3>',
            //                plugins: [{
            //                    type: 'listpaging',
            //                    autoPaging: true,
            //                    loadMoreText: 'Load More...',
            //                    noMoreRecordsText: ''
            //                }],
            itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr>' +
            '<tr><td width="30%" valign="top" style="color: blue;"><h5>{status}</h5></td><td width="70%" align="right"><h5>{versionDate}</h5></td></tr>'+
            '<tr><td width="40%" align="left"><h5>{phone1}</h5></td><td width="60%" align="right"><h5>{email}</h5></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList'),
            singleSelect:true,
            loadingText: '',
            listeners:{
                itemtap:function (view, index, target, record, item, e, eOpts) {
                    setTimeout(function () {
                        view.deselect(index);
                    }, 500);
                    mobEdu.enradms.main.f.showAllLeadDetailProfile(index,view,record,item);
                }
            }
        }
        ],
        flex:1
    }
});
