Ext.define('mobEdu.enradms.main.view.supLeadDetail', {
    extend:'Ext.form.FormPanel',
    requires:[
    'mobEdu.enradms.main.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            title:'<h1>Lead Details</h1>',
            ui:'light',
            // Back Button
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.showSupLeadsList();
                }
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Update Profile',
                handler:function () {
                    mobEdu.enradms.main.f.showLeadUpdateProfile();

                }
            },
            {
                xtype:'spacer'
            },
            {
                text:'Assign Recruiter',
                //                        ui:'confirm',
                handler:function () {
                    mobEdu.enradms.main.f.showReqSerach();

                }
            }

            ]
        },
        {
            xtype:'panel',
            name:'supLeadDetailSummary',
            id:'supLeadDetailSummary'
        }

        ]
    }

});
