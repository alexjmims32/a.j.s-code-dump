Ext.define('mobEdu.enradms.main.view.updateReqAppointment',{
    extend: 'Ext.form.FormPanel',
    requires:[
    'mobEdu.enradms.main.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        xtype:'toolbar',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>Appointment Update</h1>',
            // Back Button
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                height: 32,
                width: 32,
                cls:'back',
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.loadReqDetail();
                }
            }
            ]
        },
        {
            xtype:'fieldset',
            items:[
            {
                xtype:'textfield',
                label:'Subject',
                labelWidth:'40%',
                name:'requpappsub',
                id:'requpappsub',
                required:true,
                useClearIcon:true
            },
            {
                xtype:'datepickerfield',
                name:'requpappdate',
                id:'requpappdate',
                labelWidth:'40%',
                label:'Date',
                required:true,
                useClearIcon:true,
                picker:{
                    yearFrom:new Date().getFullYear(),
                    yearTo:new Date().getFullYear() + 1,
                    slotOrder:['day', 'month', 'year']
                }
            },
            {
                xtype: 'spinnerfield',
                label:'Hours',
                labelWidth:'40%',
                id:'reqUpTimeHours',
                name:'reqUpTimeHours',
                required:true,
                useClearIcon:true,
                minValue: 8,
                maxValue: 18,
                increment: 1,
                cycle: true
            },
            {
                xtype: 'spinnerfield',
                label:'Minutes',
                labelWidth:'40%',
                id:'reqUpTimeMinutes',
                name:'reqUpTimeMinutes',
                //                        required:true,
                useClearIcon:true,
                minValue: 0,
                maxValue: 59,
                increment: 5,
                cycle: true
            },
            {
                xtype:'textfield',
                label:'Location',
                labelWidth:'40%',
                name:'requpapploc',
                id:'requpapploc',
                required:true,
                useClearIcon:true
            } ,
{
                xtype:'selectfield',
                label:'Status',
                labelWidth:'40%',
                name:'requpappsta',
                id:'requpappsta',
                required:true,
                useClearIcon:true,
                //                        store:mobEdu.util.getStore('mobEdu.enradms.main.store.enumerations'),
                displayField:'displayValue',
                valueField:'enumValue'
            },
            {
                xtype:'textareafield',
                label:'Description',
                labelWidth:'40%',
                labelAlign:'top',
                name:'requpappdes',
                id:'requpappdes',
                required:true,
                useClearIcon:true,
                maxRows:10
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Save',
                align:'right',
                handler:function () {
                    mobEdu.enradms.main.f.updateReqAppointmentSend();
                }
            }
            ]
        }
        ]
    }
});
