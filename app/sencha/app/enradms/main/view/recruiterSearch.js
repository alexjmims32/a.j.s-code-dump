Ext.define('mobEdu.enradms.main.view.recruiterSearch', {
    extend:'Ext.Panel',
    xtype: 'recruiterSearchList',
    requires:[
    'mobEdu.enradms.main.f',
    'mobEdu.enradms.main.store.recruiterSearch'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>Recruiter Search</h1>',
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                height: 32,
                width: 32,
                cls:'back',
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.showLeadDetail();
                }
            }
            ]
        },
        {
            xtype:'fieldset',
            docked:'top',
            cls:'admssearchfieldset',
            //                margin: 10,
            items:[
            {
                xtype:'textfield',
                name:'searchReq',
                id:'searchReq',
                width:'100%',
                //                        placeHolder:'Please enter value to search',
                placeHolder:'Search by Recruiter',
                listeners:{
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enradms.main.f.recruiterSearchList();
                    }
                }
            }
            ]
        },
        {
            xtype:'list',
            id:'admsprofileList',
            itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
            //                    '<tr><td width="30%" valign="top" style="font-size: .7em;color: blue;">{status}</td><td width="70%" align="right" style="font-size:.6em">{createdDate}</td></tr>'+
            //                    '<tr><td width="40%" align="left" style="font-size:.7em"><b>{phone1}</b></td><td width="60%" align="right" style="font-size:.7em">{email}</td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch'),
            singleSelect:true,
            loadingText: '',
            listeners:{
                itemtap:function (view, index, item, e) {
                    //                        setTimeout(function () {
                    //                            view.deselect(index);
                    //                        }, 500);
                    mobEdu.util.deselectSelectedItem(index,view);
                    mobEdu.enradms.main.f.assignRecruiterUnAssignLeads(index,e);
                }
            }
        }
        ],
        flex:1
    }
});
