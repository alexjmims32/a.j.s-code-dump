Ext.define('mobEdu.enradms.main.view.allLeadAssignPopup', {
    extend:'Ext.Panel',
    requires: [
        'mobEdu.enradms.main.f',
        'mobEdu.enradms.main.store.recruiterSearch'       
    ],
    config:{
        id:'allLeadsPopup',
        scroll:'vertical',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'60%',
        height:'60%',
        layout:'fit',
        items:[
            {
                xtype:'textfield',
                docked:'top',
                id:'searchReq',
                name:'searchReq',
                placeHolder:'Search Recruiter',
                flex:1,
                listeners:{
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enradms.main.f.allLeadsReqSearch();
                    }
                }

            },
            {
                xtype:'list',
                docked:'bottom',
                height:300,
                name:'allSearchResultList',
                id:'allSearchResultList',
                itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch'),
                singleSelect:true,
                loadingText:'',
                listeners:{
                    itemtap:function (view, index, item, e) {
                        mobEdu.util.deselectSelectedItem(index,view);
                        mobEdu.enradms.main.f.showAllLeadsReqProfile(index, e);
                    }
                },
                flex:4
            }
        ],
        flex:1
    }

});
