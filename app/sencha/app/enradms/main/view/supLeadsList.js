Ext.define('mobEdu.enradms.main.view.supLeadsList', {
    extend:'Ext.Panel',
    requires:[
    'mobEdu.enradms.main.f',
    'mobEdu.enradms.main.store.supLeadsList'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            name:'leadsList',
            id:'leadsList',
            title:'<h1>Unassigned Leads</h1>',
            ui:'light',
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                height: 32,
                width: 32,
                cls:'back',
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.util.show('mobEdu.enradms.main.view.menu');
                }
            }
            ]

        },
        {
            xtype:'fieldset',
            docked:'top',
            cls:'searchfieldset',
            items:[
            {
                xtype:'textfield',
                name:'sUALeads',
                id:'sUALeads',
                width:'100%',
                placeHolder:'Search by name,phone or email',
                listeners:{
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enradms.main.f.searchUnassignedLeads();
                    }
                }
            }
            ]
        },
        {
            xtype:'list',
            id:'supLeadsList',
            name:'supLeadsList',
            emptyText:'<h3 align="center">No Leads</h3>',
            //                plugins: [{
            //                    type: 'listpaging',
            //                    autoPaging: true,
            //                    loadMoreText: 'Load More...',
            //                    noMoreRecordsText: ''
            //                }],
            itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left" ><h2>{firstName} {lastName}</h2></td></tr>'+
            '<tr><td width="40%" style="color: blue;"><h5>{status}</h5></td><td width="60%" align="right" ><h5>{versionDate}</h5></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadsList'),
            singleSelect:true,
            loadingText: '',
            listeners:{
                itemtap:function (view, index, target, record, item, e, eOpts) {
                    setTimeout(function () {
                        view.deselect(index);
                    }, 500);
                    mobEdu.enradms.main.f.loadSupLeadDetail(record,view,index,e);
                }
            }
        }
        ],
        flex:1
    }
});
