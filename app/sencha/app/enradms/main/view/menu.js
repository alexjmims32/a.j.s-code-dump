Ext.define('mobEdu.enradms.main.view.menu', {
	extend: 'Ext.Panel',

	requires: [
		'mobEdu.enradms.main.f',
		'mobEdu.enradms.main.store.menu',
		'Ext.Video'
	],

	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',

		listeners: {
			beforedeactivate: function() {
				mobEdu.enradms.notification.f.disablePopup();
			}

		},
		items: [{
			xtype: 'list',
			id: 'admisModules',
			cls: 'logo',
			itemTpl: '<table class="menu" width="100%"><tr><td width="5%" align="left"><img height="57px" width="57px" src="{img}"></td><td width="95%" align="left"><p>{title}</p></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enradms.main.store.menu'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, item, e) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					e.data.action();
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>Counselor (Admissions)</h1>'
		}],
		flex: 1
	},
	initialize: function() {
		mobEdu.enradms.main.f.initializeMainView();
	}
});