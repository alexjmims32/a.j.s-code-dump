Ext.define('mobEdu.enradms.main.view.respondEmail',{
    extend: 'Ext.form.FormPanel',
    requires:[
    'mobEdu.enradms.main.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        xtype:'toolbar',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>Reply Message</h1>',
            // Back Button
            items:[
                {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            {
                xtype:'button',
                height: 32,
                width: 32,
                cls:'back',
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enradms.main.f.showReqEmailDetail();
                }
            }
            ]
        },
        {
            xtype:'fieldset',
            items:[
            {
                xtype:'textfield',
                label:'Subject',
                labelWidth:'30%',
                name:'rsub',
                id:'rsub',
                required:true,
                useClearIcon:true
            },
            {
                xtype:'textareafield',
                label:'Message',
                labelWidth:'30%',
                labelAlign:'top',
                name:'rmsg',
                id:'rmsg',
                maxRows:15,
                required:true
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Send',
                //                        ui:'confirm',
                align:'right',
                handler:function () {
                    mobEdu.enradms.main.f.sentUpdateEmail();
                }
            }
            ]
        }
        ]
    }
});
