Ext.define('mobEdu.enradms.main.model.reqNewAppointment', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'appointmentID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});