Ext.define('mobEdu.enradms.main.model.respondEmail', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'emailID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});
