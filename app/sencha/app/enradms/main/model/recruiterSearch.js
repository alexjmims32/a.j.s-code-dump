Ext.define('mobEdu.enradms.main.model.recruiterSearch', {
    extend:'Ext.data.Model',

    config:{

        fields : [
            {
                name : 'recruiterID',
                type : 'string'
            },
            {
                name : 'name',
                type : 'string'
            },
            {
                name : 'status',
                type : 'string'
            },
            {
                name:'firstName',
                type:'string'
            },
            {
                name:'lastName',
                type:'string'
            },
            {
                name:'userID',
                type:'string'
            }
//            {
//                name : 'createdDate',
//                type : 'string'
//            }
        ]
    }
});
