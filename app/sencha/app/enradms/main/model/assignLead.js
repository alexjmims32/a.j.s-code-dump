Ext.define('mobEdu.enradms.main.model.assignLead', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'status',
                type:'string'
            },
            {
                name:'leadID',
                type:'string'
            }
//            {
//                name:'createdDate',
//                type:'string'
//            }
        ]
    }
});