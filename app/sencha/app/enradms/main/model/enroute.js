Ext.define('mobEdu.enradms.main.model.enroute', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'recruiterId'
            },
            {
                name:'authString'
            },
            {
                name:'role'
            }
        ]
    }
});