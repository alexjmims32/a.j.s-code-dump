Ext.define('mobEdu.enradms.main.model.appointmentsList', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'subject',
                type:'string'
            },
            {
                name:'appointmentDate',
                type:'string'
            },
            {
                name:'appointmentID',
                type:'string'
            },
            {
                name:'appointmentStatus',
                type: 'string'
            },
            {
                name:'description',
                type: 'string'
            },{
                name:'location',
                type:'string'
            },
            {
                name:'leadID',
                type:'string'
            },{
                name:'createdDate',
                type:'string'
            },
            {
                name:'readFlag',
                type:'string'
            },
            {
                name:'versionDate',
                type:'string'
            },
            {
                name:'versionNo',
                type:'string'
            },
            {
                name:'versionUser',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});
