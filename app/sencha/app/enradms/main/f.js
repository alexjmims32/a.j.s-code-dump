Ext.define('mobEdu.enradms.main.f', {
	requires: [
		'mobEdu.enradms.main.store.menu',
		'mobEdu.main.store.entourage',
		'mobEdu.enradms.main.store.supLeadsList',
		'mobEdu.enradms.main.store.supLeadDetail',
		'mobEdu.enradms.main.store.leadProfileUpdate',
		'mobEdu.enradms.main.store.allLeadDetail',
		'mobEdu.enradms.main.store.recruiterSearch',
		'mobEdu.enradms.main.store.assignLead',
		'mobEdu.enradms.main.store.allLeadsList',
		'mobEdu.enradms.main.store.unAssignLead',
		'mobEdu.enradms.main.store.appointmentsList',
		'mobEdu.enradms.main.store.appointmentDetail',
		'mobEdu.enradms.main.store.reqNewAppointment',
		'mobEdu.enradms.main.store.recruiterEmail',
		'mobEdu.enradms.main.store.emailDetail',
		'mobEdu.enradms.main.store.respondEmail',
		'mobEdu.enradms.main.store.enroute',
		'mobEdu.enradms.leads.store.appointments',
		'mobEdu.enradms.leads.store.appointmentDetail',
		'mobEdu.enradms.leads.store.email',
		'mobEdu.enradms.leads.store.viewEmail'
	],
	statics: {

		showEnrouteMainview: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enradms.main.view.menu');
			// Ext.getCmp('admisTitle').setTitle('<h1> eNroute Admissions </h1>');
		},

		initializeMainView: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			var store = mobEdu.util.getStore('mobEdu.enradms.main.store.menu');
			store.load();
			store.removeAll();
			store.removed = [];
			store.getProxy().clear();
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/menu/profile.png',
				title: '<h2>My Students</h2>',
				action: mobEdu.enradms.leads.f.loadProfileList
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/menu/student-history.png',
				title: '<h2>Appointments</h2>',
				action: mobEdu.enradms.main.f.loadAppointmentsList
			});
			store.sync();
			store.add({
				img: mobEdu.util.getResourcePath() + 'images/menu/student-mail.png',
				title: '<h2>Messages</h2>',
				action: mobEdu.enradms.main.f.loadMailsList
			});
			store.sync();
		},

		privilegesChecking: function(priStaticName) {
			//            var privileges = mobEdu.enradms.main.f.getPrivileges();
			//             for(var i = 0; i < privileges.length; i++){
			//                 var priName = privileges[i].name;
			//                 if (priStaticName == priName){
			//                    return true;
			//                }
			//             }
			return true;
		},

		getRecruiterId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var recruiterId = rec.data.recruiterId;
				return recruiterId;
			}
			return null;
		},
		getRole: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var role = rec.data.role;
				return role;
			}
			return null;
		},
		getCompanyId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var companyId = rec.data.companyId;
				return companyId;
			}
			return null;
		},

		getRoleId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var roleId = rec.data.roleId;
				return roleId;
			}
			return null;
		},

		getAuthString: function() {
			//            var enrouteStore=mobEdu.util.getStore('mobEdu.enradms.main.store.enroute');
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var authString = rec.data.authString;
				return authString;
			}
			return null;
		},

		getPrivileges: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var privileges = rec.data.privileges;
				return privileges;
			}
			return null;
		},

		loadSupLeadsList: function() {
			var searchItem = '';
			var reqId = 0;
			var leadsStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadsList');

			leadsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			leadsStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/search');
			leadsStore.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: reqId
			});

			leadsStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.supLeadsListResponseHandler();
			};
			leadsStore.load();
		},
		supLeadsListResponseHandler: function() {
			var leadsStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadsList');
			var leadStatus = leadsStore.getProxy().getReader().rawData;
			if (leadStatus.status == 'success') {
				mobEdu.enradms.main.f.showSupLeadsList();
			} else {
				Ext.Msg.show({
					message: leadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		searchUnassignedLeads: function() {
			var searchItem = Ext.getCmp('sUALeads').getValue();
			var reqId = 0;
			var leadsStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadsList');

			leadsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			leadsStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/search');
			leadsStore.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: reqId,
				maxRecords: 100
			});
			//            recruiterID:mobEdu.enradms.main.f.getRecruiterId(),
			leadsStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.UnassignedLeadsResponseHandler();
			};
			leadsStore.load();
		},
		UnassignedLeadsResponseHandler: function() {
			var leadsStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadsList');
			var leadStatus = leadsStore.getProxy().getReader().rawData;
			if (leadStatus.status != 'success') {
				Ext.Msg.show({
					message: leadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadSupLeadDetail: function(record) {
			var lId = record.data.leadID;

			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');

			leadDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			leadDetailStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/get');
			leadDetailStore.getProxy().setExtraParams({
				leadID: lId
			});

			leadDetailStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.supLeadDetailResponseHandler();
			};
			leadDetailStore.load();

		},

		supLeadDetailResponseHandler: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var leadDetailStatus = leadDetailStore.getProxy().getReader().rawData;
			if (leadDetailStatus.status == 'success') {
				mobEdu.enradms.main.f.showLeadDetail();
			} else {
				Ext.Msg.show({
					message: leadDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showLeadUpdateProfile: function() {
			mobEdu.enradms.main.f.loadEnumerations();
			var profileStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var storeData = profileStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}
			mobEdu.util.show('mobEdu.enradms.main.view.leadProfileUpdate');
			Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upFName').setValue(storeData[0].data.firstName);
			Ext.getCmp('upLName').setValue(storeData[0].data.lastName);
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('update').setValue(new Date(dobFormatted));
			}
			Ext.getCmp('upEmail').setValue(storeData[0].data.email);
			Ext.getCmp('upPhone').setValue(storeData[0].data.phone1);
			if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
				Ext.getCmp('upStatus').setValue(storeData[0].data.leadStatus);
			}
		},

		saveLeadUpdateProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFName').getValue();
			var updateLName = Ext.getCmp('upLName').getValue();
			var updateBirthDate = Ext.getCmp('update').getValue();
			var updateEmail = Ext.getCmp('upEmail').getValue();
			var updatePhone = Ext.getCmp('upPhone').getValue();
			var updateStatus = Ext.getCmp('upStatus').getValue();
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateStatus != null && updateStatus != '') {
				var firstName = Ext.getCmp('upFName');
				var regExp = new RegExp("^[a-zA-Z]+$");
				if (!regExp.test(updateFName)) {
					Ext.Msg.show({
						id: 'updateFNmae',
						name: 'updateFNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid first name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					var lastName = Ext.getCmp('upLName');
					var lastNRegExp = new RegExp("^[a-zA-Z]+$");
					if (!lastNRegExp.test(updateLName)) {
						Ext.Msg.show({
							id: 'updateLName',
							name: 'updateLName',
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid last name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						var updatePhone1 = Ext.getCmp('upPhone');
						var updatePhoneValue = updatePhone1.getValue();
						updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

						var length = updatePhoneValue.length;
						if (updatePhoneValue.length != 10) {
							Ext.Msg.show({
								id: 'phonemsg',
								name: 'phonemsg',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid phone number.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										updatePhone1.focus(true);
									}
								}
							});
						} else {
							var a = updatePhoneValue.replace(/\D/g, '');
							var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
							updatePhone1.setValue(newValue);
							var updateMail = Ext.getCmp('upEmail');
							var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
							if (updateEmail != '' && regMail.test(updateEmail) == false) {
								Ext.Msg.show({
									id: 'upMail',
									name: 'upMail',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid email address.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											updateMail.focus(true);
										}
									}
								});
							} else {
								mobEdu.enradms.main.f.loadLeadUpdate();
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},

		loadLeadUpdate: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFName').getValue();
			var updateLName = Ext.getCmp('upLName').getValue();
			var updateBirthDate = Ext.getCmp('update').getValue();
			var updateEmail = Ext.getCmp('upEmail').getValue();
			var updatePhone = Ext.getCmp('upPhone').getValue();
			var updateStatus = Ext.getCmp('upStatus').getValue();
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateStore = mobEdu.util.getStore('mobEdu.enradms.main.store.leadProfileUpdate');

			updateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			updateStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/update');
			updateStore.getProxy().setExtraParams({
				leadID: leadId,
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				status: updateStatus
			});

			updateStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.loadLeadUpdateResponseHandler();
			};
			updateStore.load();
		},

		loadLeadUpdateResponseHandler: function() {
			var updateStore = mobEdu.util.getStore('mobEdu.enradms.main.store.leadProfileUpdate');
			var updateStatus = updateStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.show('mobEdu.enradms.main.view.menu');
						}
					}
				});
			} else {
				Ext.Msg.show({
					message: updateStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
				//                Ext.Msg.alert(null,updateStatus.status);
			}
		},

		supLeadsListRefresh: function() {
			var supLeadsListStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadsList');
			supLeadsListStore.load();
			mobEdu.enradms.main.f.showSupLeadsList();
		},

		showSupLeadsList: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.supLeadsList');
		},

		showLeadDetail: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.supLeadDetail');
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var fullName;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			fullName = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			if (dateOfBirth == null) {
				dateOfBirth = '';
			}
			var emailId1 = profileData[0].data.email;
			var phoneNo1 = profileData[0].data.phone1;
			var ldStatus = profileData[0].data.leadStatus;
			if (ldStatus == null) {
				ldStatus = '';
			}
			var reqName = profileData[0].data.recruiterName;

			var finalLeadSummaryHtml = '<table class=".td,th"><tr><td align="right" width="50%"><h2>Full Name:</h2></td>' + '<td align="left"><h3>' + fullName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>BirthDate:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + reqName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr></table>';

			Ext.getCmp('supLeadDetailSummary').setHtml(finalLeadSummaryHtml);
		},

		//        showReqSerach:function(){
		//            if (Ext.os.is.Phone) {
		////                mobEdu.enradms.main.f.showReqSearchView();
		//                mobEdu.enradms.main.f.recruiterUnAssignSearchList();
		//            } else {
		//                mobEdu.enradms.main.f.assignedLeadList();
		//            }
		//        },

		allLeadsLeadProfileUpdate: function() {
			mobEdu.enradms.main.f.loadEnumerations();
			var leadDeatilStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var storeData = leadDeatilStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}
			mobEdu.util.show('mobEdu.enradms.main.view.updateLeadProfile');
			Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upFirstN').setValue(storeData[0].data.firstName);
			Ext.getCmp('upLastN').setValue(storeData[0].data.lastName);
			Ext.getCmp('updateDate').setValue(new Date(storeData[0].data.birthDate));
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('updateDate').setValue(new Date(dobFormatted));
			}
			Ext.getCmp('upEmailvalue').setValue(storeData[0].data.email);
			Ext.getCmp('upPhoneValue').setValue(storeData[0].data.phone1);
			if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
				Ext.getCmp('upStatusValue').setValue(storeData[0].data.leadStatus);
			}
		},

		loadEnumerations: function() {
			//            var enumType = 'LEAD_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			////            var enuStore =  mobEdu.util.getStore('mobEdu.enradms.main.store.enumerations');
			//
			//            enuStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			//            enuStore.getProxy().setExtraParams({
			//                roleID:roleId,
			//                enumType:enumType
			//            });
			//            enuStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.main.f.loadEnumerationsResponseHandler();
			//            };
			//            enuStore.load();
		},
		loadEnumerationsResponseHandler: function() {
			//            var enuStore =  mobEdu.util.getStore('mobEdu.enradms.main.store.enumerations');
			//            var enumStatus = enuStore.getProxy().getReader().rawData;
			//            if(enumStatus.status != 'success'){
			//                Ext.Msg.show({
			//                    id:'enum',
			//                    title:null,
			//                    message:enumStatus.status,
			//                    buttons:Ext.MessageBox.OK,
			//                    cls:'msgbox'
			//                });
			//            }
		},

		saveSupLeadProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstN').getValue();
			var updateLName = Ext.getCmp('upLastN').getValue();
			var updateBirthDate = Ext.getCmp('updateDate').getValue();
			var updateEmail = Ext.getCmp('upEmailvalue').getValue();
			var updatePhone = Ext.getCmp('upPhoneValue').getValue();
			var updateStatus = Ext.getCmp('upStatusValue').getValue();
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateStatus != null && updateStatus != '') {
				var firstName = Ext.getCmp('upFirstN');
				var regExp = new RegExp("^[a-zA-Z]+$");
				if (!regExp.test(updateFName)) {
					Ext.Msg.show({
						id: 'updateFNmae',
						name: 'updateFNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid first name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					var lastName = Ext.getCmp('upLastN');
					var lastNRegExp = new RegExp("^[a-zA-Z]+$");
					if (!lastNRegExp.test(updateLName)) {
						Ext.Msg.show({
							id: 'updateLName',
							name: 'updateLName',
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid last name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						var updatePhone1 = Ext.getCmp('upPhoneValue');
						var updatePhoneValue = updatePhone1.getValue();
						updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

						var length = updatePhoneValue.length;
						if (updatePhoneValue.length != 10) {
							Ext.Msg.show({
								id: 'phonemsg',
								name: 'phonemsg',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid phone number.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										updatePhone1.focus(true);
									}
								}
							});
						} else {
							var a = updatePhoneValue.replace(/\D/g, '');
							var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
							updatePhone1.setValue(newValue);
							var updateMail = Ext.getCmp('upEmailvalue');
							var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
							if (updateEmail != '' && regMail.test(updateEmail) == false) {
								Ext.Msg.show({
									id: 'upMail',
									name: 'upMail',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid email address.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											updateMail.focus(true);
										}
									}
								});
							} else {
								mobEdu.enradms.main.f.loadSupLeadUpdateProfile();
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadSupLeadUpdateProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstN').getValue();
			var updateLName = Ext.getCmp('upLastN').getValue();
			var updateBirthDate = Ext.getCmp('updateDate').getValue();
			var updateEmail = Ext.getCmp('upEmailvalue').getValue();
			var updatePhone = Ext.getCmp('upPhoneValue').getValue();
			var updateStatus = Ext.getCmp('upStatusValue').getValue();
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateStore = mobEdu.util.getStore('mobEdu.enradms.main.store.leadProfileUpdate');

			updateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			updateStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/update');
			updateStore.getProxy().setExtraParams({
				leadID: leadId,
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				status: updateStatus
			});
			updateStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.loadSupLeadUpdateProfileResponseHandler();
			};
			updateStore.load();
		},
		loadSupLeadUpdateProfileResponseHandler: function() {
			var updateStore = mobEdu.util.getStore('mobEdu.enradms.main.store.leadProfileUpdate');
			var updateStatus = updateStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.show('mobEdu.enradms.main.view.menu');
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: updateStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onPhone1Keyup: function(phone1Field) {
			var phoneNumber = (phone1Field.getValue()).toString() + '';
			phoneNumber = phoneNumber.replace(/\D/g, '');
			phone1Field.setValue(phoneNumber);
			var length = phoneNumber.length;
			if (length > 10) {
				phone1Field.setValue(phoneNumber.substring(0, 10));
				return false;
			}
			return true;
		},

		loadAllLeadsReqSearch: function() {
			if (Ext.os.is.Phone) {
				//                mobEdu.enradms.main.f.showAllLeadsReqSearch();
				mobEdu.enradms.main.f.allLeadsRecruiterSearchList();
			} else {
				//                mobEdu.enradms.main.f.leadAllLeadAssignPopup();
				mobEdu.enradms.main.f.allLeadsAssignedLeadList();
			}
		},

		leadPopup: function(popupView) {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.hidePopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enradms.main.view.leadAssignPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		leadAllLeadAssignPopup: function(popupView) {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.allLeadsHidePopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enradms.main.view.allLeadAssignPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		showReqSearchView: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.recruiterSearch');
		},

		showAllLeadsReqSearch: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.allLeadsRecruiterSearch');
		},

		assignedLeadList: function() {
			var searchItem = '';

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.assignedLeadListResponseHandler();
			};
			reqSearchStore.load();
		},

		assignedLeadListResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				//                mobEdu.enradms.main.f.hidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enradms.main.f.leadPopup();
			}
		},

		recruiterUnAssignSearchList: function() {
			//            var searchItem = Ext.getCmp('searchReq').getValue();
			var searchItem = '';

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.recruiterUnAssignSearchResponseHandler();
			};
			reqSearchStore.load();
		},
		recruiterUnAssignSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enradms.main.f.showReqSearchView();
			}
		},

		allLeadsAssignedLeadList: function() {
			var searchItem = '';

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.allLeadsAssignedLeadListResponseHandler();
			};
			reqSearchStore.load();
		},
		allLeadsAssignedLeadListResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				//                mobEdu.enradms.main.f.allLeadsHidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enradms.main.f.leadAllLeadAssignPopup();
			}
		},

		allLeadsRecruiterSearchList: function() {
			//            var searchItem = Ext.getCmp('searchReq').getValue();
			var searchItem = '';
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.allLeadsRecruiterSearchResponseHandler();
			};
			reqSearchStore.load();
		},
		allLeadsRecruiterSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enradms.main.f.showAllLeadsReqSearch();
			}
		},

		recruiterSearchList: function() {
			var searchItem = Ext.getCmp('searchReq').getValue();

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.recruiterSearchResponseHandler();
			};
			reqSearchStore.load();
		},
		recruiterSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				//                mobEdu.enradms.main.f.hidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});

			}
		},

		allLeadsReqSearch: function() {
			var searchItem = Ext.getCmp('searchReq').getValue();

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.allLeadsReqSearchResponseHandler();
			};
			reqSearchStore.load();
		},

		allLeadsReqSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				mobEdu.enradms.main.f.allLeadsHidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});

			}
		},

		showReqProfile: function(index, e) {
			mobEdu.enradms.main.f.hidePopup();
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enradms.main.f.hidePopup();
						mobEdu.enradms.main.f.assignLead(index, e);
					}
					if (btn == 'no') {
						mobEdu.enradms.main.f.leadPopup();
					}
				}
			});
		},

		assignRecruiter: function(index, e) {
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enradms.main.f.assignRecruiterInAllLeads(index, e);
					}

				}
			});
		},

		assignRecruiterUnAssignLeads: function(index, e) {
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enradms.main.f.selectUnAssignLeadsRecruiter(index, e);
					}

				}
			});
		},

		showAllLeadsReqProfile: function(index, e) {
			mobEdu.enradms.main.f.allLeadsHidePopup();
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enradms.main.f.allLeadsHidePopup();
						mobEdu.enradms.main.f.assignAllLeadsList(index, e);
					}
					if (btn == 'no') {
						mobEdu.enradms.main.f.leadAllLeadAssignPopup();
					}
				}
			});
		},

		assignLead: function(index, e) {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var deatilData = leadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.assignLeadResponseHandler();
			};
			assignStore.load();
		},
		assignLeadResponseHandler: function() {
			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Lead assigned successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.hidePopup();
							mobEdu.enradms.main.f.supLeadsListRefresh();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		assignRecruiterInAllLeads: function(index, e) {
			var allLeadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var deatilData = allLeadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			console.log(e.data);
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.assignRecruiterInAllLeadsResponseHandler();
			};
			assignStore.load();
		},
		assignRecruiterInAllLeadsResponseHandler: function() {
			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Lead assigned successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.allLeadsListRefresh();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		selectUnAssignLeadsRecruiter: function(index, e) {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.supLeadDetail');
			var deatilData = leadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.selectUnAssignLeadsRecruiterResponseHandler();
			};
			assignStore.load();
		},
		selectUnAssignLeadsRecruiterResponseHandler: function() {
			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Lead assigned successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.supLeadsListRefresh();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		assignAllLeadsList: function(index, e) {
			var allLeadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var deatilData = allLeadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.assignAllLeadsListResponseHandler();
			};
			assignStore.load();
		},

		assignAllLeadsListResponseHandler: function() {
			var assignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Lead assigned successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							//                            mobEdu.enradms.main.f.allLeadsHidePopup();
							mobEdu.enradms.main.f.allLeadsListRefresh();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAllLeadsList: function() {
			//            var searchLeadItem = Ext.getCmp('sALItem').getValue();
			//
			//            var allLeadStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList');
			//
			//            allLeadStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            allLeadStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/search');
			//            allLeadStore.getProxy().setExtraParams({
			//                searchText:searchLeadItem,
			//                maxRecords:100
			//            });
			//
			//            allLeadStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.main.f.loadAllLeadsListResponseHandler();
			//            };
			//            allLeadStore.load();
		},
		loadAllLeadsListResponseHandler: function() {
			var allLeadStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList');
			var allLeadStatus = allLeadStore.getProxy().getReader().rawData;
			if (allLeadStatus.status != 'success') {
				Ext.Msg.show({
					message: allLeadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAllLeads: function() {
			//            var searchLeadItem = '';
			//
			//            var allLeadStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList');
			//
			//            allLeadStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            allLeadStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/search');
			//            allLeadStore.getProxy().setExtraParams({
			//                searchText:searchLeadItem
			//            });
			//            allLeadStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.main.f.loadAllLeadsResponseHandler();
			//            };
			//            allLeadStore.load();
		},
		loadAllLeadsResponseHandler: function() {
			var allLeadStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList');
			var allLeadStatus = allLeadStore.getProxy().getReader().rawData;
			if (allLeadStatus.status != 'success') {
				Ext.Msg.show({
					message: allLeadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enradms.main.f.showAllLeadList();
			}
		},

		showAllLeadList: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.allLeadsList');
		},

		showAllLeadDetailProfile: function(index, view, record, item) {
			var lId = record.data.leadID;

			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');

			leadDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			leadDetailStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/get');
			leadDetailStore.getProxy().setExtraParams({
				leadID: lId
			});

			leadDetailStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.showAllLeadDetailResponseHandler();
			};
			leadDetailStore.load();
		},

		showAllLeadDetailResponseHandler: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var detailStatus = leadDetailStore.getProxy().getReader().rawData;
			if (detailStatus.status == 'success') {
				mobEdu.enradms.main.f.showAllLeadDetail();
				if (detailStatus.recruiterID == 0) {
					Ext.getCmp('assign').show();
					Ext.getCmp('unAssign').hide();
				} else {
					Ext.getCmp('assign').hide();
					Ext.getCmp('unAssign').show();
				}
			} else {
				Ext.Msg.show({
					message: detailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		allLeadsListRefresh: function() {
			var allLeadsListStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList');
			allLeadsListStore.load();
			mobEdu.enradms.main.f.showAllLeadList();
		},

		showAllLeadDetail: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.allLeadDetail');
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var fullName;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			fullName = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			if (dateOfBirth == null) {
				dateOfBirth = '';
			}
			var emailId1 = profileData[0].data.email;
			var phoneNo1 = profileData[0].data.phone1;
			var ldStatus = profileData[0].data.leadStatus;
			if (ldStatus == null) {
				ldStatus = '';
			}
			var reqName = profileData[0].data.recruiterName;

			var finalSummaryHtml = '<table class=".td,th"><tr><td align="right" width="50%"><h2>Full Name:</h2></td>' + '<td align="left"><h3>' + fullName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>BirthDate:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + reqName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr></table>';

			Ext.getCmp('allLeadsDetailSummary').setHtml(finalSummaryHtml);
		},

		loadUnAssignLead: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
			var leadData = leadDetailStore.data.all;
			var unAssignLeadId = leadData[0].data.leadID;
			var recruiterId = leadData[0].data.recruiterID;
			var unAssignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.unAssignLead');

			unAssignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			unAssignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/unassignlead');
			unAssignStore.getProxy().setExtraParams({
				recruiterID: recruiterId,
				leadID: unAssignLeadId
			});

			unAssignStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.UnAssignLeadResponseHandler();
			};
			unAssignStore.load();
		},
		UnAssignLeadResponseHandler: function() {
			var unAssignStore = mobEdu.util.getStore('mobEdu.enradms.main.store.unAssignLead');
			var unAssignStatus = unAssignStore.getProxy().getReader().rawData;
			if (unAssignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'unAssignSuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Successfully unassigned the lead.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.allLeadsListRefresh();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: unAssignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAppointmentsList: function() {
			var searchText = '';
			var appStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments');
			appStore.load();
			var data = appStore.data.all;
			if (data.length == 0) {
				appStore.add({
					'subject': 'Meeting Request 1',
					'appointmentDate': '2013-02-28 08:00',
					'appointmentStatus': 'Requested',
					'description': 'Request 1',
					'location': 'HR Block',
					'status': 'success',
					'leadID': 'C001000012',
					'source': ''
				});
				appStore.sync();
			}
			mobEdu.enradms.main.f.showAppointmentsList();

			//            var rId = mobEdu.enradms.main.f.getRecruiterId();
			//            var reqStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentsList');

			//            reqStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            reqStore.getProxy().setUrl(enrouteWebserver + 'enroute/appointment/search');
			//            reqStore.getProxy().setExtraParams({
			//                searchText:searchText
			//            });
			//
			//            reqStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.main.f.appointmentsListResponseHandler();
			//            };
			//            reqStore.load();
		},
		appointmentsListResponseHandler: function() {
			var reqStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentsList');
			reqStore.sort('appointmentDate', 'DESC');
			var appStatus = reqStore.getProxy().getReader().rawData;
			if (appStatus.status == 'success') {
				mobEdu.enradms.main.f.showAppointmentsList();
			} else {
				Ext.Msg.show({
					message: appStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		reqAppListBack: function() {
			//            Ext.getCmp('appointmentList').refresh();
			//            var reqAppListStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentsList');
			//            reqAppListStore.load();
			mobEdu.enradms.main.f.showAppointmentsList();
		},

		showAppointmentsList: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showEnrouteMainview);
			mobEdu.util.show('mobEdu.enradms.main.view.reqAppointmentsList');
		},

		showReqAppointmentDetail: function(record) {
			var appId = record.data.appointmentID;

			var appDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			appDetailStore.load();
			appDetailStore.removeAll();
			appDetailStore.sync();

			appDetailStore.add(record);
			appDetailStore.sync();
			mobEdu.enradms.main.f.loadReqDetail();
		},

		reqAppointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentDetail');
			var appDetailStatus = appDetailStore.getProxy().getReader().rawData;
			if (appDetailStatus.status == 'success') {
				mobEdu.enradms.main.f.loadReqDetail();
			} else {
				Ext.Msg.show({
					message: appDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showReqNewAppointment: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.reqNewAppointment');
		},

		sendReqNewAppointment: function() {
			var aSub = Ext.getCmp('reqappsub').getValue();
			var aDes = Ext.getCmp('reqappdes').getValue();
			var aDate = Ext.getCmp('reqappdate').getValue();
			var aLoc = Ext.getCmp('reqapploc').getValue();
			var aStatus = Ext.getCmp('reqappsta').getValue();
			var aTimeHours = Ext.getCmp('reqappTimeH').getValue();
			var aTimeMinutes = Ext.getCmp('reqappTimeM').getValue();
			if (aStatus == 'select') {
				aStatus = '';
			}
			if (aTimeHours == 0) {
				aTimeHours = '';
			}
			if (aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aStatus != '' && aTimeHours != '') {
				mobEdu.enradms.main.f.newReqAppointmentResponse();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory values.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},
		newReqAppointmentResponse: function() {
			var aSub = Ext.getCmp('reqappsub').getValue();
			var aDes = Ext.getCmp('reqappdes').getValue();
			var aDate = Ext.getCmp('reqappdate').getValue();
			var aLoc = Ext.getCmp('reqapploc').getValue();
			var aTimeHours = Ext.getCmp('reqappTimeH').getValue();
			var aTimeMinutes = Ext.getCmp('reqappTimeM').getValue();
			var appTime;
			if (aTimeHours == null) {
				aTimeHours = '';
			}
			if (aTimeMinutes == null) {
				aTimeMinutes = '';
			}
			appTime = aTimeHours + ':' + aTimeMinutes;
			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = (aDate.getFullYear() + '-' + [aDate.getMonth() + 1] + '-' + aDate.getDate());
			}
			var appText = 'appointment';
			var appOperationText = 'create';
			var creBy = 1;
			var leadApp = 'OUT';
			var leadStatus = 'SCHEDULED';
			var newAppStore = mobEdu.util.getStore('mobEdu.enradms.main.store.reqNewAppointment');

			newAppStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			newAppStore.getProxy().setUrl(enrouteWebserver + 'enroute/appointment/create');
			newAppStore.getProxy().setExtraParams({
				leadID: leadId,
				subject: aSub,
				description: aDes,
				appointmentDate: appDateFormatted,
				location: aLoc,
				createdBy: creBy,
				source: leadApp,
				status: leadStatus
			});

			newAppStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.newReqAppointmentResponseHandler();
			};
			newAppStore.load();
		},
		newReqAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enradms.main.store.reqNewAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Appointment created successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.showAppointmentsList();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadReqUpdateAppointment: function() {
			mobEdu.enradms.main.f.loadAppointmentEnumerations();
			var upAppReqStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentDetail');
			var upDateStore = upAppReqStore.data.all;
			mobEdu.util.show('mobEdu.enradms.main.view.updateReqAppointment');
			Ext.getCmp('requpappsub').setValue(upDateStore[0].data.subject);
			Ext.getCmp('requpappdes').setValue(upDateStore[0].data.description);
			var appDate = upDateStore[0].data.appointmentDate;
			var appDateValue = appDate.split(' ');
			var dateFormatted = appDateValue[0];
			var newStartDate = Ext.Date.parse(dateFormatted, "Y-m-d");
			var appTime = appDateValue[1];
			var appTimeFormatted = appTime.split(':');
			var appTimeHours = appTimeFormatted[0];
			var appTimeMints = appTimeFormatted[1];
			if (appTimeHours < 10) {
				appTimeHours = '0' + appTimeHours;
			}
			if (appTimeMints < 10) {
				appTimeMints = '0' + appTimeMints;
			}
			if (upDateStore[0].data.appointmentDate != '' && upDateStore[0].data.appointmentDate != undefined && upDateStore[0].data.appointmentDate != null) {
				Ext.getCmp('requpappdate').setValue(new Date(newStartDate));
			}
			Ext.getCmp('reqUpTimeHours').setValue(appTimeHours);
			Ext.getCmp('reqUpTimeMinutes').setValue(appTimeMints);
			Ext.getCmp('requpapploc').setValue(upDateStore[0].data.location);
			if (upDateStore[0].data.appointmentStatus != '' && upDateStore[0].data.appointmentStatus != undefined && upDateStore[0].data.appointmentStatus != null) {
				Ext.getCmp('requpappsta').setValue(upDateStore[0].data.appointmentStatus);
			}

		},

		loadAppointmentEnumerations: function() {
			//            var enumType = 'APPOINTMENT_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			//            var enuStore =  mobEdu.util.getStore('mobEdu.enradms.main.store.enumerations');
			//
			//            enuStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			//            enuStore.getProxy().setExtraParams({
			//                roleID:roleId,
			//                enumType:enumType
			//            });
			//            enuStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.main.f.loadEnumerationsResponseHandler();
			//            };
			//            enuStore.load();
		},

		updateReqAppointmentSend: function() {
			var upAppReqStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentDetail');
			var upDateStore = upAppReqStore.data.all;
			var appointmentId = upDateStore[0].data.appointmentID;
			var upSub = Ext.getCmp('requpappsub').getValue();
			var upDes = Ext.getCmp('requpappdes').getValue();
			var upDate = Ext.getCmp('requpappdate').getValue();
			var upLoc = Ext.getCmp('requpapploc').getValue();
			var upStatus = Ext.getCmp('requpappsta').getValue();
			var upAppTimeHours = Ext.getCmp('reqUpTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('reqUpTimeMinutes').getValue();
			var upAppTime;
			if (upAppTimeHours == null) {
				upAppTimeHours = '';
			}
			if (upAppTimeMints == null) {
				upAppTimeMints = '';
			}
			upAppTime = upAppTimeHours + ':' + upAppTimeMints;
			var appDateFormatted;
			if (upDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = (upDate.getFullYear() + '-' + [upDate.getMonth() + 1] + '-' + upDate.getDate());
			}
			var appDateTime = appDateFormatted + ' ' + upAppTime;
			var currentDate = new Date();
			var currentDateFormatted = (currentDate.getFullYear() + '-' + [currentDate.getMonth() + 1] + '-' + currentDate.getDate());

			if (appDateFormatted >= currentDateFormatted) {
				var newAppStore = mobEdu.util.getStore('mobEdu.enradms.main.store.reqNewAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getRecruiterAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(enrouteWebserver + 'enroute/appointment/update');
				newAppStore.getProxy().setExtraParams({
					appointmentID: appointmentId,
					subject: upSub,
					description: upDes,
					appointmentDate: appDateTime,
					location: upLoc,
					status: upStatus
				});

				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enradms.main.f.updateAppointmentResponseHandler();
				};
				newAppStore.load();
			} else {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enradms.main.store.reqNewAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappreqsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Appointment updated successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.reqAppListBack();
							mobEdu.enradms.main.f.resetUppDateAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetUppDateAppointment: function() {
			mobEdu.util.get('mobEdu.enradms.main.view.updateReqAppointment').reset();
		},

		hasReqAppointmentDirection: function(appSource) {
			if (appSource == "IN") {
				return true;
			} else {
				return false;
			}
		},
		hasAppReadFlag: function(readFlag) {
			if (readFlag == "Opened") {
				return true;
			} else {
				return false;
			}
		},

		hasReqEmailDirection: function(direction) {
			if (direction == "IN") {
				return true;
			} else {
				return false;
			}
		},

		hasReqReadFlag: function(estatus) {
			if (estatus == "Opened") {
				return true;
			} else {
				return false;
			}
		},
		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},
		setAppointmentScheduleInfo: function(selectedDate) {
			var scheduleInfo = '';
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			var newDateValue = (selectedDate.getFullYear() + '-' + mobEdu.enradms.main.f.dateChecking([selectedDate.getMonth() + 1]) + '-' + mobEdu.enradms.main.f.dateChecking(selectedDate.getDate()));
			var appointmentsStore = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentsList');
			appointmentsStore.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var appDateValue = appDate.split(' ');
				var dateFormatted = appDateValue[0];
				if (newDateValue == dateFormatted) {
					var appTime = appDateValue[1];
					var appSubject = rec.get('subject');
					var appDesc = rec.get('description');
					var appLocation = rec.get('location');
					scheduleInfo = this.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc);
				}
			});
			//set the event schedule info in calendar view label
			Ext.getCmp('reqAppointmentLabel').setHtml(scheduleInfo);
		},

		formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc) {
			if (appSubject == null) {
				appSubject = '';
			}
			if (appTime == null) {
				appTime = '';
			}
			if (appDesc == null) {
				appDesc = '';
			}
			if (appLocation == null) {
				appLocation = '';
			}
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + appSubject + '</h3>';
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + appLocation + '</h3>';
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Description: ' + '</b>' + appDesc + '</h3>' + '<br/>';

			return scheduleInfo;
		},

		//To highlight Schedule dates
		doHighlightAppointmentsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentsList');
			store.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var appDateValue = appDate.split(' ');
				var dateFormatted = appDateValue[0];
				var newStartDate = Ext.Date.parse(dateFormatted, "Y-m-d");
				if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
					classes.push(highlightedItemCls);
				}
			});
			return classes;
		},

		loadCalendarView: function() {
			mobEdu.main.f.calendarInstance = 'reqappt';
			mobEdu.util.get('mobEdu.enradms.main.view.recruiterAppointmentCalendar');
			var appointmentDock = Ext.getCmp('appointmentsDock');
			appointmentDock.remove(calendar);

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'crseCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			appointmentDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.enradms.main.f.setAppointmentScheduleInfo(new Date());
		},

		showCalendar: function() {
			mobEdu.enradms.main.f.loadCalendarView();
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.reqAppListBack);
			mobEdu.util.show('mobEdu.enradms.main.view.recruiterAppointmentCalendar');
		},

		loadEnrouteMainMenu: function() {
			Ext.getCmp('admisModules').refresh();
			mobEdu.util.show('mobEdu.enradms.main.view.menu');
		},


		showReqNewEmail: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.reqNewEmail');
		},

		showUpdateAppointment: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.updateReqAppointment');
		},

		loadReqDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.enradms.main.view.reqAppointmentDetail');
		},

		showEmail: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.email');
		},

		showRecruiterEList: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showEnrouteMainview);
			mobEdu.util.show('mobEdu.enradms.main.view.recruiterEmailsList');
		},

		showResopndEmail: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.respondEmail');
		},

		loadMailsList: function() {
			var searchText = '';

			var eStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.email');
			eStore.load();
			var data = eStore.data.all;
			if (data.length == 0) {

				eStore.add({
					'subject': 'Submit documents',
					'createdDate': '2013-02-23',
					'time': '',
					'body': 'Submit the documents for review',
					'leadID': 'C001000012',
					'emailID': 'shanda@gmail.com',
					'status': 'success',
					'createdBy': 'Shanda',
					'readFlag': 'Y',
					'emailDirection': 'OUT',
					'emailStatus': 'Read',
					'entityType': '',
					'messageID': '01',
					'versionDate': '2013-02-23',
					'direction': 'OUT',
					'messageStatus': 'Read',
					'fromID': 'Walter',
					'toID': 'C001000012'
				});
				eStore.sync();

				eStore.add({
					'subject': 'Re: Submit documents',
					'createdDate': '2013-02-23',
					'time': '',
					'body': 'Submited the documents for review',
					'leadID': 'C001000012',
					'emailID': 'shanda@gmail.com',
					'status': 'success',
					'createdBy': 'Shanda',
					'readFlag': 'N',
					'emailDirection': 'IN',
					'emailStatus': 'Unread',
					'entityType': '',
					'messageID': '01',
					'versionDate': '2013-02-24',
					'direction': 'IN',
					'messageStatus': 'Unread',
					'fromID': 'C001000012',
					'toID': 'Walter'
				});
				eStore.sync();
			}

			mobEdu.enradms.main.f.showRecruiterEList();
			//            var entType = 'RECRUITER';
			//            var eStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterEmail');
			//
			//            eStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            eStore.getProxy().setUrl(enrouteWebserver + 'enroute/message/search');
			//            eStore.getProxy().setExtraParams({
			//                searchText:searchText
			//            });
			//
			//            eStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.main.f.loadEmailListResponseHandler();
			//            };
			//            eStore.load()
		},
		loadEmailListResponseHandler: function() {
			var emailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterEmail');
			emailStore.sort('versionDate', 'DESC');
			var eStatus = emailStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success') {
				mobEdu.enradms.main.f.showRecruiterEList();
			} else {
				Ext.Msg.show({
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadReqEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;

			var eDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail');
			eDetailStore.load();
			eDetailStore.removeAll();
			eDetailStore.sync();

			eDetailStore.add(record);
			eDetailStore.sync();
			mobEdu.enradms.main.f.showReqEmailDetail();
		},

		loadReqEmaildetailResponseHandler: function() {
			var eDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.emailDetail');
			var eDetailStatus = eDetailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status == 'success') {
				mobEdu.enradms.main.f.showReqEmailDetail();
			} else {
				Ext.Msg.show({
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		reqEmailListBack: function() {
			var reqEListStore = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterEmail');
			reqEListStore.load();
			mobEdu.enradms.main.f.showRecruiterEList();
		},

		showEDetail: function(record) {
			var eDetailStore = mobEdu.util.getStore('mobEdu.enradms.main.store.emailDetail');
			eDetailStore.removeAll();
			eDetailStore.add(record);
			eDetailStore.sync();
			mobEdu.enradms.main.f.showReqEmailDetail();

		},
		showReqEmailDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showRecruiterEList);
			mobEdu.util.show('mobEdu.enradms.main.view.rEmailDetail');
		},

		loadNewMail: function() {
			mobEdu.util.show('mobEdu.enradms.main.view.email');
		},

		sendNewMail: function() {
			//            var msgField=Ext.getCmp('rto').getValue();
			var esub = Ext.getCmp('reqNewSub').getValue();
			var msgArea = Ext.getCmp('reqNewMsg').getValue();
			if (msgArea != '' && esub != '') {
				mobEdu.enradms.main.f.sendReqNewEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory values.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sendReqNewEmail: function() {
			//            var msgField= Ext.getCmp('rto').getValue();
			var esub = Ext.getCmp('reqNewSub').getValue();
			var msgArea = Ext.getCmp('reqNewMsg').getValue();

			var eStore = mobEdu.util.getStore('mobEdu.enradms.main.store.respondEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/sendmessage');
			eStore.getProxy().setExtraParams({
				leadID: leadId,
				subject: esub,
				body: msgArea
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.rEmailResponseHandler();
			};
			eStore.load();
		},
		rEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enradms.main.store.respondEmail');
			var nEStatus = nEStore.getProxy().getReader().rawData;
			if (nEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsendsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Message sent successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.reqEmailListBack();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sentUpdateEmail: function() {
			var upsub = Ext.getCmp('rmsg').getValue();
			var upArea = Ext.getCmp('rsub').getValue();
			if (upsub != '' && upArea != '') {
				mobEdu.enradms.main.f.updateEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory values.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateEmail: function() {
			var esub = Ext.getCmp('rmsg').getValue();
			var msgArea = Ext.getCmp('rsub').getValue();
			var store = mobEdu.util.getStore('mobEdu.enradms.main.store.emailDetail')
			var data = store.data.all;
			var lId = data[0].data.leadID;
			var fromId = data[0].data.fromID;

			var eStore = mobEdu.util.getStore('mobEdu.enradms.main.store.respondEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/sendmessage');
			eStore.getProxy().setExtraParams({
				leadID: fromId,
				subject: esub,
				body: msgArea
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enradms.main.f.updateEmailResponseHandler();
			};
			eStore.load();
		},
		updateEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enradms.main.store.respondEmail');
			var NEStatus = nEStore.getProxy().getReader().rawData;
			if (NEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsendsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Message sent successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.main.f.reqEmailListBack();
							mobEdu.enradms.main.f.resetSendReplyEmail();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: NEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetSendReplyEmail: function() {
			mobEdu.util.get('mobEdu.enradms.main.view.respondEmail').reset();
		},

		enrouteLogout: function() {
			//Removing pin store
			var pinStore = mobEdu.util.getStore('mobEdu.reg.store.pin');
			pinStore.getProxy().clear();
			pinStore.sync();

			var enrouteStore = mobEdu.util.getStore('mobEdu.enradms.main.store.enroute');
			var rec = enrouteStore.getAt(0);
			rec.set('recruiterId', null);
			rec.set('authString', null);
			rec.set('role', null);
			enrouteStore.sync();

			window.open('index.html', '_self');
		},

		senchaChangeOrientation: function() {
			if (Ext.getOrientation() == "landscape") {
				popup.setWidth("75%");
				popup.setHeight("55%");
			} else {
				popup.setWidth("80%");
				popup.setHeight("60%");
			}
		},


		hidePopup: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showLeadDetail);
			var popUp = Ext.getCmp('leadsPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		allLeadsHidePopup: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showAllLeadDetail);
			var popUp = Ext.getCmp('allLeadsPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		loadInquiry: function() {
			window.open(inquiryApp, '_self');
		},

		showEnrouteLogin: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showRecruiterMainView);
			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.enradms.login.view.phone.login');
			} else {
				mobEdu.util.show('mobEdu.enradms.login.view.tablet.login');
			}
		}

	}
});