Ext.define('mobEdu.enradms.main.store.enroute', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enradms.main.model.enroute'
    ],

    config:{
        storeId: 'mobEdu.enradms.main.store.enroute',
        autoLoad: true,
        model: 'mobEdu.enradms.main.model.enroute',
        proxy:{
            type:'localstorage',
            id:'enrouteStorage'
        }
    }
});