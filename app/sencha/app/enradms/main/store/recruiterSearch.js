Ext.define('mobEdu.enradms.main.store.recruiterSearch', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.recruiterSearch'
    ],
    config: {
        storeId: 'mobEdu.enradms.main.store.recruiterSearch',

        autoLoad: false,

        model: 'mobEdu.enradms.main.model.recruiterSearch',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'users'
            }
        }
    }
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'recruiters'
//        return proxy;
//    }
});
