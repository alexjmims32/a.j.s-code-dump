Ext.define('mobEdu.enradms.main.store.recruiterEmail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.recruiterEmail'
    ],
    config: {
        storeId: 'mobEdu.enradms.main.store.recruiterEmail',

        autoLoad: false,
//        pageSize:pageSize,

        model: 'mobEdu.enradms.main.model.recruiterEmail',

               proxy : {
            type:'localstorage',
            id:'admsrecruiterEmail'
        }
    }
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'messages'
//        return proxy;
//    }
});
