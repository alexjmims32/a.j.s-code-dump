Ext.define('mobEdu.enradms.main.store.reqNewAppointment', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.reqNewAppointment'
    ],
    config: {
        storeId: 'mobEdu.enradms.main.store.reqNewAppointment',

        autoLoad: false,

        model: 'mobEdu.enradms.main.model.reqNewAppointment',

            proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});
