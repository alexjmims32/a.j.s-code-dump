Ext.define('mobEdu.enradms.main.store.supLeadsList', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.supLeadsList'
    ],
    config: {
        storeId: 'mobEdu.enradms.main.store.supLeadsList',

        autoLoad: false,
//        pageSize:pageSize,

        model: 'mobEdu.enradms.main.model.supLeadsList',

               proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'leads'
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'leads'
//        return proxy;
//    }
});