Ext.define('mobEdu.enradms.main.store.supLeadDetail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.supLeadsList'
    ],

    config:{
        storeId: 'mobEdu.enradms.main.store.supLeadDetail',
        autoLoad: false,
        model: 'mobEdu.enradms.main.model.supLeadsList',

               proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});

