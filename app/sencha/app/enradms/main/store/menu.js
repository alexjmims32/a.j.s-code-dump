Ext.define('mobEdu.enradms.main.store.menu', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enradms.main.model.menu'
    ],

    config:{
        storeId: 'mobEdu.enradms.main.store.menu',
        autoLoad: false,
        model: 'mobEdu.enradms.main.model.menu',
        proxy:{
            type:'localstorage',
            id:'eNroutemenu'
        }
    }
});