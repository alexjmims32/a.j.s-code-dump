Ext.define('mobEdu.enradms.main.store.allLeadDetail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.allLeadsList'
    ],

    config:{
        storeId: 'mobEdu.enradms.main.store.allLeadDetail',
        autoLoad: false,
        model: 'mobEdu.enradms.main.model.allLeadsList',

               proxy : {
             type:'localstorage',
            id:'allLeadDetail'
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});


