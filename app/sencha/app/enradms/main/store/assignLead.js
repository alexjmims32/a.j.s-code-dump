Ext.define('mobEdu.enradms.main.store.assignLead', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.assignLead'
    ],

    config:{
        storeId: 'mobEdu.enradms.main.store.assignLead',
        autoLoad: false,
        model: 'mobEdu.enradms.main.model.assignLead',

               proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});