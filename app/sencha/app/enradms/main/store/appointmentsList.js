Ext.define('mobEdu.enradms.main.store.appointmentsList', {
    extend:'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
    'mobEdu.enradms.main.model.appointmentsList'
    ],
    config: {
        storeId: 'mobEdu.enradms.main.store.appointmentsList',

        autoLoad: false,
        //        pageSize:pageSize,

        model: 'mobEdu.enradms.main.model.appointmentsList' ,

        proxy : {
            type:'localstorage',
            id:'appointmentsList'
        }
    }
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'appointments'
//        return proxy;
//    }

});
