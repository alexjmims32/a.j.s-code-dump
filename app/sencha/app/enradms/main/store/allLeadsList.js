Ext.define('mobEdu.enradms.main.store.allLeadsList', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.main.model.allLeadsList'
    ],
    config: {
        storeId: 'mobEdu.enradms.main.store.allLeadsList',

        autoLoad: false,
//        pageSize:pageSize,

        model: 'mobEdu.enradms.main.model.allLeadsList' ,

        proxy : {
             type:'localstorage',
            id:'allLeadsList'
        }
    }

//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'leads'
//        return proxy;
//    }
});


