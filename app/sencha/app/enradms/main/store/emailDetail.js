Ext.define('mobEdu.enradms.main.store.emailDetail', {
    extend:'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
    'mobEdu.enradms.main.model.recruiterEmail'
    ],

    config: {
        storeId: 'mobEdu.enradms.main.store.emailDetail',

        autoLoad: false,

        model: 'mobEdu.enradms.main.model.recruiterEmail',

        proxy : {
            type:'localstorage',
            id:'emailDetail'
        }

    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});