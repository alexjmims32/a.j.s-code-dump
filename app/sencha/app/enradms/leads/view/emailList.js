Ext.define('mobEdu.enradms.leads.view.emailList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.email'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'admsemailLst',
			name: 'emailLst',
			emptyText: '<h3 align="center">No Messages</h3>',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>',
				'<tpl if="(mobEdu.enradms.leads.f.hasEmailDirection(direction)===true)">',
				'<td width="5%"><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
				'<tpl else>',
				'<td width="5%"><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
				'</tpl>',
				'<tpl if="(mobEdu.enradms.leads.f.hasReadFlag(status)===true)">',
				'<td><b><h2>{subject}</h2></b></td>' +
				'<td align="right"><b><h5>{versionDate}</h5></b></td>' +
				'<tpl else>',
				'<td style="color: #0066cc;"><h2>{subject}</h2></td>' +
				'<td style="color: #0066cc;" align="right"><h5>{versionDate}</h5></td>' +
				'</tpl>',
				'</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.email'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enradms.leads.f.loadEmailDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Messages</h1>',
			name: 'emailTitle',
			id: 'admsemailTitle',
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'New Message',
				handler: function() {
					mobEdu.enradms.leads.f.showLeadEmail();
				}
			}]
		}],
		flex: 1
	}
});