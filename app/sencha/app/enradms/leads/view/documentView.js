Ext.define('mobEdu.enradms.leads.view.documentView', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			id: 'admsimg',
			xtype: 'label',
			flex: 1
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Document Details</h1>',
			id: 'admsdocumentViewTitle',
		}],
		flex: 1
	}
});