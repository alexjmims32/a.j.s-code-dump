Ext.define('mobEdu.enradms.leads.view.newAppointment', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		xtype: 'toolbar',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>New Appointment</h1>',
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'Subject',
				labelWidth: '40%',
				name: 'appsub',
				id: 'appsub',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'datepickerfield',
				name: 'appdate',
				id: 'appdate',
				labelWidth: '40%',
				label: 'Date',
				required: true,
				useClearIcon: true,
				value: new Date(),
				picker: {
					yearFrom: new Date().getFullYear(),
					yearTo: new Date().getFullYear() + 1,
					slotOrder: ['day', 'month', 'year']
				},
				listeners: {
					change: function(datefield, e, eOpts) {
						mobEdu.enradms.leads.f.onAppDateKeyup(datefield)
					}
				}
			}, {
				xtype: 'textfield',
				name: 'appTime',
				id: 'appTime',
				labelWidth: '100%',
				label: 'Appointment Time',
				useClearIcon: true
			}, {
				xtype: 'spinnerfield',
				label: 'Hours',
				labelWidth: '40%',
				id: 'appTimeHours',
				name: 'appTimeHours',
				required: true,
				useClearIcon: true,
				minValue: 8,
				maxValue: 18,
				increment: 1,
				cycle: true
			}, {
				xtype: 'spinnerfield',
				label: 'Minutes',
				labelWidth: '40%',
				id: 'appTimeMinutes',
				name: 'appTimeMinutes',
				//                        required:true,
				useClearIcon: true,
				minValue: 0,
				maxValue: 59,
				increment: 5,
				cycle: true
			}, {
				xtype: 'textfield',
				label: 'Location',
				labelWidth: '40%',
				name: 'apploc',
				id: 'apploc',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'selectfield',
				label: 'Status',
				labelWidth: '40%',
				name: 'appsta',
				id: 'appsta',
				required: true,
				useClearIcon: true,
				options: [{
					text: 'Requested',
					value: 'Requested'
				}, {
					text: 'Scheduled',
					value: 'Scheduled'
				}, {
					text: 'Cancelled',
					value: 'Cancelled'
				}, {
					text: 'Completed',
					value: 'Completed'
				}]
			}, {
				xtype: 'textareafield',
				label: 'Description',
				labelWidth: '40%',
				labelAlign: 'top',
				name: 'appdes',
				id: 'appdes',
				required: true,
				useClearIcon: true,
				maxRows: 10
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Save',
				align: 'right',
				handler: function() {
					mobEdu.enradms.leads.f.sendAppointment();
				}
			}]
		}]
	}
});