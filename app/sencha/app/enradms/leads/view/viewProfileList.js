Ext.define('mobEdu.enradms.leads.view.viewProfileList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.viewProfileList'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			cls: 'logo',
			name: 'viewProfileList',
			itemTpl: '<table class="menu" width="100%"><tr><h7><td align="left">{title}</td></h7><td width="2%"><div align="right" class="arrow" /></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.viewProfileList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, item, e) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					e.data.action();
				}
			}
		}, {
			xtype: 'customToolbar',
			id: 'admsviewProfTool',
			name: 'viewProfTool',
			docked: 'top'
		}],
		flex: 1
	},
	initialize: function() {
		mobEdu.enradms.leads.f.initializeViewProfile();
	}

});