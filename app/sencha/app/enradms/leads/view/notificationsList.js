Ext.define('mobEdu.enradms.leads.view.notificationsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.notifications'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'notificationsList',
			itemTpl: '<table class="menu" width="100%"><tr><td width="50%" align="left"><h2>{title}</h2></td><td width="50%" align="right" ><h5>{dueDate}</h5></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.notifications'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enradms.leads.f.loadNotificationListDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			name: 'notifiListTitle',
			id: 'notifiListTitle'
		}, {
			xtype: 'toolbar',
			docked: 'top',
			title: '<h1>Notifications</h1>',
			cls: 'headerColor'
		}],
		flex: 1
	}
});