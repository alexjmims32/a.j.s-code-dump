Ext.define('mobEdu.enradms.leads.view.financialApp', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		xtype: 'toolbar',
		items: [{
				xtype: 'customToolbar',
				docked: 'top',
				ui: 'light',
				//                title:'Sample User',
				name: 'financialTitle',
				id: 'financialTitle',
				// Back Button
				items: [{
					ui: 'light',
					xtype: 'button',
					cls: 'home',
					height: 32,
					width: 32,
					style: 'background:none;border:none;',
					iconMask: true,
					handler: function() {
						mobEdu.util.showMenu();
					}
				}, {
					xtype: 'button',
					cls: 'back',
					height: 32,
					width: 32,
					style: 'background:none;border:none;',
					handler: function() {
						mobEdu.enradms.leads.f.loadViewProfile();
					}
				}]
			}, {
				xtype: 'toolbar',
				docked: 'top',
				title: '<h1>Financial Application</h1>',
				cls: 'headerColor'
			}, {
				xtype: 'fieldset',
				items: [{
					xtype: 'textfield',
					labelAlign: 'left',
					labelWidth: '50%',
					label: 'Student Id',
					name: 'studentid',
					id: 'studentid',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelAlign: 'left',
					labelWidth: '50%',
					label: 'Term',
					name: 'term',
					id: 'term',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Subject',
					name: 'sub',
					id: 'sub',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Major',
					name: 'major',
					id: 'major',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Minor',
					name: 'minor',
					id: 'minor',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Status',
					name: 'status',
					id: 'status',
					required: true,
					useClearIcon: true
				}]
			}

		]
	}
});