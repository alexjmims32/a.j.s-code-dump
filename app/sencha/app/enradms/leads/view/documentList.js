Ext.define('mobEdu.enradms.leads.view.documentList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.docList'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'admsdocList',
			emptyText: '<h3 align="center">No Documents</h3>',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>', '<td ><h2>{type}</h2></td>' +
				'<td align="right"><h5>{status}</h5></td>' + '</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.docList'),
			singleSelect: false,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enradms.leads.f.showDocView(index);
				}
			}
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Documents</h1>',
			id: 'admsdocumentTitle'
		}],
		flex: 1
	}
});