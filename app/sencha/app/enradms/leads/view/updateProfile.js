Ext.define('mobEdu.enradms.leads.view.updateProfile', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.main.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		xtype: 'toolbar',
		items: [{
			xtype: 'toolbar',
			docked: 'top',
			name: 'updateTitle',
			id: 'updateTitle'
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'First Name',
				labelWidth: '40%',
				name: 'upFirstName',
				required: true,
				useClearIcon: true,
				id: 'upFirstName'
			}, {
				xtype: 'textfield',
				label: 'Last Name',
				labelWidth: '40%',
				name: 'upLastName',
				required: true,
				useClearIcon: true,
				id: 'upLastName'
			}, {
				xtype: 'datepickerfield',
				name: 'updateDateValue',
				id: 'updateDateValue',
				labelWidth: '40%',
				required: true,
				useClearIcon: true,
				label: 'Birth Date'
			}, {
				xtype: 'emailfield',
				name: 'upEmailText',
				id: 'upEmailText',
				labelWidth: '40%',
				label: 'Email',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');
					input.set({
						pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
					});
					me.callParent(arguments);
				}
			}, {
				xtype: 'textfield',
				labelAlign: 'left',
				labelWidth: '40%',
				label: 'Phone',
				name: 'upPhoneNo',
				id: 'upPhoneNo',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');
					input.set({
						pattern: '[0-9]*'
					});
					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.enradms.main.f.onPhone1Keyup(textfield);
					}
				},
				maxLength: 10
			}, {
				xtype: 'selectfield',
				label: 'Status',
				labelWidth: '40%',
				name: 'upStatusText',
				id: 'upStatusText',
				required: true,
				useClearIcon: true,
				displayField: 'displayValue',
				valueField: 'enumValue'
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Save',
				align: 'right',
				handler: function() {
					mobEdu.enradms.leads.f.saveLeadProfile();
				}
			}]
		}]
	}
});