Ext.define('mobEdu.enradms.leads.view.updateAppointment', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		xtype: 'toolbar',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			docked: 'top',
			name: 'upAppTitle',
			id: 'upAppTitle',
		}, {
			xtype: 'toolbar',
			docked: 'top',
			title: '<h1>Appointment Update</h1>',
			cls: 'headerColor'
		}, {
			xtype: 'fieldset',
			items: [{
					xtype: 'textfield',
					label: 'Subject',
					labelWidth: '40%',
					name: 'upsub',
					id: 'upsub',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'datepickerfield',
					name: 'update',
					//                        labelAlign:'top',
					id: 'update',
					labelWidth: '40%',
					label: 'Date',
					required: true,
					useClearIcon: true,
					picker: {
						yearFrom: new Date().getFullYear(),
						yearTo: new Date().getFullYear() + 1
						//                            slotOrder:['month', 'day', 'year']
					}

				},

				{
					xtype: 'spinnerfield',
					label: 'Hours',
					labelWidth: '40%',
					id: 'upTimeHours',
					name: 'upTimeHours',
					required: true,
					useClearIcon: true,
					minValue: 8,
					maxValue: 18,
					increment: 1,
					cycle: true
				}, {
					xtype: 'spinnerfield',
					label: 'Minutes',
					labelWidth: '40%',
					id: 'upTimeMinutes',
					name: 'upTimeMinutes',
					//                        required:true,
					useClearIcon: true,
					minValue: 0,
					maxValue: 59,
					increment: 5,
					cycle: true
				}, {
					xtype: 'textfield',
					label: 'Location',
					labelWidth: '40%',
					name: 'uploc',
					id: 'uploc',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'selectfield',
					label: 'Status',
					labelWidth: '40%',
					name: 'upsta',
					id: 'upsta',
					required: true,
					useClearIcon: true,
					//                        store:mobEdu.util.getStore('mobEdu.enradms.main.store.enumerations'),
					displayField: 'displayValue',
					valueField: 'enumValue'
				}, {
					xtype: 'textareafield',
					label: 'Description',
					labelWidth: '40%',
					labelAlign: 'top',
					name: 'updes',
					id: 'updes',
					required: true,
					useClearIcon: true,
					maxRows: 10
				}
			]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Save',
				//                        ui:'confirm',
				align: 'right',
				handler: function() {
					mobEdu.enradms.leads.f.updateAppointmentSend();
				}
			}]
		}]
	}
});