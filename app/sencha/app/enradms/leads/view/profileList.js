Ext.define('mobEdu.enradms.leads.view.profileList', {
	extend: 'Ext.Panel',
	xtype: 'profileSearchList',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.profileSearch'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>My Students</h1>'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset'
		}, {
			xtype: 'list',
			id: 'admsprofileList',
			name: 'profileList',
			emptyText: '<h3 align="center">No Leads</h3>',
			//                plugins: [{
			//                    type: 'listpaging',
			//                    autoPaging: true,
			//                    loadMoreText: 'Load More...',
			//                    noMoreRecordsText: ''
			//                }],
			itemTpl: '<table class="menu" width="100%"><tr><h7><td colspan="2" align="left"><b>{firstName} {lastName}</b></td></h7></tr>' +
				'<tr><h4><td width="30%" valign="top" style="color: blue;">{status}</td></h4><h5><td width="70%" align="right" >{versionDate}</td><h5></tr>' +
				'<tr><h4><td width="40%" align="left"><b>{phone1}</b></td><td width="60%" align="right">{email}</td></h4></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enradms.leads.f.showViewProfile(index, view, record, item);
				}
			}
		}],
		flex: 1
	}
});