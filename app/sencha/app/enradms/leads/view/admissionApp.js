Ext.define('mobEdu.enradms.leads.view.admissionApp', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		xtype: 'toolbar',
		cls: 'logo',
		items: [{
				xtype: 'toolbar',
				docked: 'top',
				name: 'admissionTitle',
				id: 'admissionTitle'
			}, {
				xtype: 'customToolbar',
				docked: 'top',
				title: '<h1>Admission Application</h1>',
				cls: 'headerColor'
			}, {
				xtype: 'fieldset',
				items: [{
					xtype: 'textfield',
					labelAlign: 'left',
					labelWidth: '50%',
					label: 'Student Id',
					name: 'studentid',
					id: 'studentid',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelAlign: 'left',
					labelWidth: '50%',
					label: 'Term',
					name: 'term',
					id: 'term',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Subject',
					name: 'sub',
					id: 'sub',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Major',
					name: 'major',
					id: 'major',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Minor',
					name: 'minor',
					id: 'minor',
					required: true,
					useClearIcon: true
				}, {
					xtype: 'textfield',
					labelWidth: '50%',
					label: 'Status',
					name: 'status',
					id: 'status',
					required: true,
					useClearIcon: true
				}]
			}

		]
	}
});