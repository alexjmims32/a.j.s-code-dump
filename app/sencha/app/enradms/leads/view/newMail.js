Ext.define('mobEdu.enradms.leads.view.newMail', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		xtype: 'toolbar',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Reply Message</h1>',
			id: 'admsnewETitle',
			name: 'newETitle'
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'Subject',
				labelWidth: '30%',
				name: 'newsub',
				id: 'newsub',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textareafield',
				label: 'Message',
				labelWidth: '30%',
				labelAlign: 'top',
				name: 'newmsg',
				id: 'newmsg',
				maxRows: 15,
				required: true
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Send',
				align: 'right',
				handler: function() {
					mobEdu.enradms.leads.f.sentReplyMessage();
				}
			}]
		}]
	}
});