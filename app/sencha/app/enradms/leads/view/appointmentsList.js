Ext.define('mobEdu.enradms.leads.view.appointmentsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.appointments'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'appList',
			emptyText: '<h3 align="center">No Appointments</h3>',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>', '<td ><h2>{subject}</h2></td>' +
				'<td align="right"><h5>{appointmentDate}</h5></td>' + '</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enradms.leads.f.loadAppointDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Appointments</h1>',
			name: 'appTitle',
			id: 'admsappTitle'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'New Appointment',
				handler: function() {
					mobEdu.enradms.leads.f.showNewAppointment();
				}
			}]
		}],
		flex: 1
	}
});