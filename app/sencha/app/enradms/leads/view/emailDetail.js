Ext.define('mobEdu.enradms.leads.view.emailDetail', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enradms.leads.f',
		'mobEdu.enradms.leads.store.viewEmail'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			cls: 'logo',
			disableSelection: true,
			itemTpl: new Ext.XTemplate('<table width="100%">' +
				'<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
				'<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{versionDate}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{messageStatus}</h3></td></tr>' +
				'<tr><td valign="top"  align="right" width="50%"><h2>Message:</h2></td><td align="left"><h3>{body}</h3></td></tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail'),
			singleSelect: true,
			loadingText: ''
		}, {
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Message Details</h1>',
			name: 'emailDetailTitle',
			id: 'admseDetailTitle',
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Send Reply',
				align: 'right',
				handler: function() {
					mobEdu.enradms.leads.f.showNewMail();
				}
			}]
		}],
		flex: 1
	}
});