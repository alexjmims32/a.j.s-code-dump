Ext.define('mobEdu.enradms.leads.view.leadNewEmail', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		xtype: 'toolbar',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>New Message</h1>',
			id: 'newLeadTitle',
			name: 'newLeadTitle'
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'Subject',
				labelWidth: '25%',
				name: 'newsub',
				id: 'newsub',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textareafield',
				label: 'Message',
				labelWidth: '30%',
				labelAlign: 'top',
				name: 'newmsg',
				id: 'newmsg',
				maxRows: 15,
				required: true
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Send',
				align: 'right',
				handler: function() {
					mobEdu.enradms.leads.f.sentmail();
				}
			}]
		}]
	}
});