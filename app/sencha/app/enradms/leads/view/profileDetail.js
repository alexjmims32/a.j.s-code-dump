Ext.define('mobEdu.enradms.leads.view.profileDetail', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enradms.leads.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			docked: 'top',
			title: '<h1>Profile Summary</h1>'
		}, {
			xtype: 'panel',
			name: 'profilesummary',
			id: 'profilesummary'
		}]
	}
});