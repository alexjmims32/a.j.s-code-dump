Ext.define('mobEdu.enradms.leads.f', {
	requires: [
		'mobEdu.enradms.leads.store.profileSearch',
		'mobEdu.enradms.leads.store.viewProfileList',
		'mobEdu.enradms.leads.store.profileDetail',
		'mobEdu.enradms.leads.store.profileList',
		'mobEdu.enradms.main.store.leadProfileUpdate',
		'mobEdu.enradms.leads.store.email',
		'mobEdu.enradms.leads.store.viewEmail',
		'mobEdu.enradms.leads.store.newEmail',
		'mobEdu.enradms.leads.store.appointments',
		'mobEdu.enradms.leads.store.newAppointment',
		'mobEdu.enradms.leads.store.appointmentDetail',
		'mobEdu.enradms.leads.store.notifications',
		'mobEdu.enradms.leads.store.notificationDetail',
		'mobEdu.enradms.leads.store.docList'
	],
	statics: {
		loadProfileList: function() {
			mobEdu.enradms.leads.f.myLeadsSearch();
		},

		myLeadsSearch: function() {

			var searchItem = '';
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch');
			store.load();
			store.removeAll();
			store.sync();


			store.add({
				'firstName': 'Shanda',
				'lastName': 'McGrew',
				'status': 'Pending',
				'createdDate': '',
				'leadID': 'C001000012',
				'leadStatus': 'Pending',
				'addressID': '',
				'addressType': '',
				'address1': '',
				'address2': '',
				'address3': '',
				'gender': '',
				'birthDate': '1992-10-20',
				'city': '',
				'state': '',
				'zip': '',
				'country': '',
				'phone1': '9064427927',
				'email': 'shanda@gmail.com',
				'versionDate': '2013-02-21',
				'versionUser': 'Walter'
			});
			store.sync();

			store.add({
				'firstName': 'Robert',
				'lastName': 'Gray',
				'status': 'Pending',
				'createdDate': '',
				'leadID': '02',
				'leadStatus': 'Decision Made',
				'addressID': '',
				'addressType': '',
				'address1': '',
				'address2': '',
				'address3': '',
				'gender': 'Male',
				'birthDate': '1993-10-21',
				'city': 'Dulth',
				'state': '',
				'zip': '',
				'country': '',
				'phone1': '9068333235',
				'email': 'robert@gmail.com',
				'versionDate': '2013-02-20',
				'versionUser': 'Walter'
			});
			store.sync();

			store.add({
				'firstName': 'Matthew',
				'lastName': 'Cllm',
				'status': 'Pending',
				'createdDate': '',
				'leadID': 'C001000023',
				'leadStatus': 'Pending',
				'addressID': '',
				'addressType': '',
				'address1': '',
				'address2': '',
				'address3': '',
				'gender': 'Male',
				'birthDate': '1995-04-21',
				'city': '',
				'state': '',
				'zip': '',
				'country': '',
				'phone1': '9066167823',
				'email': 'clim@gmail.com',
				'versionDate': '2013-02-27',
				'versionUser': 'Walter'
			});
			store.sync();

			store.add({
				'firstName': 'Jeannette',
				'lastName': 'Sanger',
				'status': 'Pending',
				'createdDate': '',
				'leadID': 'C00100045',
				'leadStatus': 'Pending',
				'addressID': '',
				'addressType': '',
				'address1': '',
				'address2': '',
				'address3': '',
				'gender': '',
				'birthDate': '1993-12-20',
				'city': '',
				'state': '',
				'zip': '',
				'country': '',
				'phone1': '9064443522',
				'email': 'sanger@gmail.com',
				'versionDate': '2013-2-23',
				'versionUser': 'Walter'
			});
			store.sync();

			mobEdu.enradms.leads.f.showProfileList();
			//            var store=mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch');
			//
			//            store.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            store.getProxy().setUrl(enrouteWebserver + 'enroute/lead/search');
			//            store.getProxy().setExtraParams({
			//                searchText:searchItem,
			//                recruiterID:mobEdu.enradms.main.f.getRecruiterId()
			//            });
			//
			//            store.getProxy().afterRequest = function () {
			//                mobEdu.enradms.leads.f.myLeadsSearchResponseHandler();
			//            };
			//            store.load();
		},

		myLeadsSearchResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch');
			var storeStatus = store.getProxy().getReader().rawData;
			if (storeStatus.status != 'success') {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enradms.leads.f.showProfileList();
			}
		},

		profileSearch: function() {
			var searchItem = Ext.getCmp('searchItem').getValue();
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch');

			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(enrouteWebserver + 'enroute/lead/search');
			store.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: mobEdu.enradms.main.f.getRecruiterId(),
				maxRecords: 100,
				companyId: companyId
			});

			store.getProxy().afterRequest = function() {
				mobEdu.enradms.leads.f.profileSearchResponseHandler();
			};
			store.load();
		},

		profileSearchResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch');
			var storeStatus = store.getProxy().getReader().rawData;
			if (storeStatus.status != 'success') {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		initializeViewProfile: function() {
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewProfileList');
			//            store.load();
			store.removeAll();
			store.removed = [];
			//            store.getProxy().clear();
			store.sync();

			store.add({
				title: '<h2>Profile</h2>',
				action: mobEdu.enradms.leads.f.viewProfileDetail
			});
			store.sync();

			store.add({
				title: '<h2>Messages</h2>',
				action: mobEdu.enradms.leads.f.loadEmail
			});
			store.sync();

			store.add({
				title: '<h2>Appointments</h2>',
				action: mobEdu.enradms.leads.f.loadAppointments
			});
			store.sync();

			store.add({
				title: '<h2>Documents</h2>',
				action: mobEdu.enradms.leads.f.loadDocuments
			});
			store.sync();

			//            store.add({
			////                img:'resources/images/main-menu/StudentProfile.png',
			//                title:'Notifications'
			////                action: mobEdu.enradms.leads.f.loadNotificationsList
			//            });
			//            store.sync();


			//            store.load();
		},

		showViewProfile: function(selectedIndex, viewRef, record, item) {
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			store.removeAll();
			store.add(record);
			store.sync();

			mobEdu.enradms.leads.f.loadViewProfile();
		},
		loadViewProfile: function() {
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var data = store.data.all;
			var fName = data[0].data.firstName;
			var lName = data[0].data.lastName;
			leadId = data[0].data.leadID;
			var lId = data[0].data.leadID;
			leadTitle = fName + ' ' + lName;
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showProfileList);
			mobEdu.util.show('mobEdu.enradms.leads.view.viewProfileList');
			Ext.getCmp('admsviewProfTool').setTitle('<h1>' + leadTitle + '</h1>');
		},


		viewProfileDetail: function() {
			//            var store=mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			//            var data = store.data.all;
			//            var lId = data[0].data.leadID;
			//
			//            var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileList');
			//            detailStore.load();
			//
			//            detailStore.removeAll();
			//            detailStore.sync();
			//
			//            detailStore.add(data[0]);
			//            detailStore.sync();
			mobEdu.enradms.leads.f.loadProfileSummary();
		},
		ProfileDetailResponseHandler: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileList');
			var storeStatus = detailStore.getProxy().getReader().rawData;
			if (storeStatus.status == 'success') {
				mobEdu.enradms.leads.f.loadProfileSummary();
			} else {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadProfileSummary: function(lId) {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enradms.leads.view.profileDetail');
			// Ext.getCmp('admsprofDetail').setTitle('<h1>' + leadTitle + '</h1>');
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			//            detailStore.load();
			var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var middleValue;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			middleValue = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			if (dateOfBirth == null) {
				dateOfBirth = '';
			}
			var address1 = profileData[0].data.address1;
			var address2 = profileData[0].data.address2;
			var cityValue = profileData[0].data.city;
			var stateValue = profileData[0].data.state;
			var countryName = profileData[0].data.country;
			var emailId1 = profileData[0].data.email;
			var phoneNo1 = profileData[0].data.phone1;
			var phoneNo2 = profileData[0].data.phone2;
			var addType = profileData[0].data.addressType;
			var ldStatus = profileData[0].data.leadStatus;
			if (ldStatus == null) {
				ldStatus = '';
			}
			var recqName = profileData[0].data.recruiterName;
			var updatedDate = profileData[0].data.versionDate;
			var updatedBy = profileData[0].data.versionUser;

			var finalSummaryHtml = '<center><table class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Last updated date:</h2></td>' + '<td align="left"><h3>' + updatedDate + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Last updated by:</h2></td>' + '<td align="left"><h3>' + updatedBy + '</h3></td></tr></table></center>';

			Ext.getCmp('profilesummary').setHtml(finalSummaryHtml);

		},

		loadEnumerations: function() {
			//            var enumType = 'LEAD_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			//            var enuStore =  mobEdu.util.getStore('mobEdu.enradms.leads.store.enumerations');
			//
			//            enuStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			//            enuStore.getProxy().setExtraParams({
			//                roleID:roleId,
			//                enumType:enumType
			//            });
			//            enuStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.leads.f.loadEnumerationsResponseHandler();
			//            };
			//            enuStore.load();
		},
		loadEnumerationsResponseHandler: function() {
			//            var enuStore =  mobEdu.util.getStore('mobEdu.enradms.leads.store.enumerations');
			//            var enumStatus = enuStore.getProxy().getReader().rawData;
			//            if(enumStatus.status != 'success'){
			//                Ext.Msg.show({
			//                    id:'enum',
			//                    title:null,
			//                    message:enumStatus.status,
			//                    buttons:Ext.MessageBox.OK,
			//                    cls:'msgbox'
			//                });
			//            }
		},

		ProfileUpdate: function() {
			mobEdu.enradms.leads.f.loadEnumerations();
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileList');
			var storeData = detailStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}
			mobEdu.util.show('mobEdu.enradms.leads.view.updateProfile');
			Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upFirstName').setValue(storeData[0].data.firstName);
			Ext.getCmp('upLastName').setValue(storeData[0].data.lastName);
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('updateDateValue').setValue(new Date(dobFormatted));
			}
			Ext.getCmp('upEmailText').setValue(storeData[0].data.email);
			Ext.getCmp('upPhoneNo').setValue(storeData[0].data.phone1);
			if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
				Ext.getCmp('upStatusText').setValue(storeData[0].data.leadStatus);
			}
		},

		saveLeadProfile: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileList');
			var storeData = detailStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstName').getValue();
			var updateLName = Ext.getCmp('upLastName').getValue();
			var updateBirthDate = Ext.getCmp('updateDateValue').getValue();
			var updateEmail = Ext.getCmp('upEmailText').getValue();
			var updatePhone = Ext.getCmp('upPhoneNo').getValue();
			var updateStatus = Ext.getCmp('upStatusText').getValue();
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateStatus != null && updateStatus != '') {
				var firstName = Ext.getCmp('upFirstName');
				var regExp = new RegExp("^[a-zA-Z]+$");
				if (!regExp.test(updateFName)) {
					Ext.Msg.show({
						id: 'updateFNmae',
						name: 'updateFNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid first name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					var lastName = Ext.getCmp('upLastName');
					var lastNRegExp = new RegExp("^[a-zA-Z]+$");
					if (!lastNRegExp.test(updateLName)) {
						Ext.Msg.show({
							id: 'updateLName',
							name: 'updateLName',
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid last name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						var updatePhone1 = Ext.getCmp('upPhoneNo');
						var updatePhoneValue = updatePhone1.getValue();
						updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

						var length = updatePhoneValue.length;
						if (updatePhoneValue.length != 10) {
							Ext.Msg.show({
								id: 'phonemsg',
								name: 'phonemsg',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid phone number.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										updatePhone1.focus(true);
									}
								}
							});
						} else {
							var a = updatePhoneValue.replace(/\D/g, '');
							var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
							updatePhone1.setValue(newValue);
							var updateMail = Ext.getCmp('upEmailText');
							var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
							if (updateEmail != '' && regMail.test(updateEmail) == false) {
								Ext.Msg.show({
									id: 'upMail',
									name: 'upMail',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid email address.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											updateMail.focus(true);
										}
									}
								});
							} else {
								mobEdu.enradms.leads.f.loadLeadUpdateProfile();
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadLeadUpdateProfile: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileList');
			var storeData = detailStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstName').getValue();
			var updateLName = Ext.getCmp('upLastName').getValue();
			var updateBirthDate = Ext.getCmp('updateDateValue').getValue();
			var updateEmail = Ext.getCmp('upEmailText').getValue();
			var updatePhone = Ext.getCmp('upPhoneNo').getValue();
			var updateStatus = Ext.getCmp('upStatusText').getValue();
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateStore = mobEdu.util.getStore('mobEdu.enradms.main.store.leadProfileUpdate');

			updateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			updateStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/update');
			updateStore.getProxy().setExtraParams({
				leadID: leadId,
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				status: updateStatus,
				companyId: companyId
			});
			updateStore.getProxy().afterRequest = function() {
				mobEdu.enradms.leads.f.loadLeadUpdateProfileResponseHandler();
			};
			updateStore.load();
		},

		loadLeadUpdateProfileResponseHandler: function() {
			var updateStore = mobEdu.util.getStore('mobEdu.enradms.main.store.leadProfileUpdate');
			var updateStatus = updateStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.show('mobEdu.enradms.leads.view.viewProfileList');
						}
					}
				});
			} else {
				Ext.Msg.alert(null, updateStatus.status);
			}
		},

		loadEmail: function() {
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;
			var serviceText = 'recruiteremail';
			var operationText = 'leademails';
			var searchText = '';
			var entityType = 'LEAD';
			var eStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.email');
			eStore.removeAll();
			eStore.sync();

			eStore.add({
				'subject': 'Submit documents',
				'createdDate': '2013-02-23',
				'time': '',
				'body': 'Please submit the documents for review',
				'leadID': 'C001000012',
				'emailID': 'shanda@gmail.com',
				'status': 'success',
				'createdBy': 'Shanda',
				'readFlag': 'Y',
				'emailDirection': 'OUT',
				'emailStatus': 'Read',
				'entityType': '',
				'messageID': '01',
				'versionDate': '2013-02-23',
				'direction': 'OUT',
				'messageStatus': 'Read',
				'fromID': 'Walter',
				'toID': 'C001000012'
			});
			eStore.sync();

			eStore.add({
				'subject': 'Re: Submit documents',
				'createdDate': '2013-02-23',
				'time': '',
				'body': 'Submited the documents for review',
				'leadID': 'C001000012',
				'emailID': 'shanda@gmail.com',
				'status': 'success',
				'createdBy': 'Shanda',
				'readFlag': 'N',
				'emailDirection': 'IN',
				'emailStatus': 'Unread',
				'entityType': '',
				'messageID': '01',
				'versionDate': '2013-02-24',
				'direction': 'IN',
				'messageStatus': 'Unread',
				'fromID': 'C001000012',
				'toID': 'Walter'
			});
			eStore.sync();


			mobEdu.enradms.leads.f.showEmailList();
		},
		loadEmailResponseHandler: function() {
			var emailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.email');
			emailStore.sort('versionDate', 'DESC');
			var eStatus = emailStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success') {
				mobEdu.enradms.leads.f.showEmailList();
			} else {
				Ext.Msg.show({
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;

			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail');
			detailStore.load();
			detailStore.removeAll();
			detailStore.sync();

			detailStore.add(record);
			detailStore.sync();
			mobEdu.enradms.leads.f.showEmailDetail();
			//            detailStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            detailStore.getProxy().setUrl(enrouteWebserver + 'enroute/message/get');
			//            detailStore.getProxy().setExtraParams({
			//                messageID:eId
			//            });
			//
			//            detailStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.leads.f.loadEmailDetailResponseHandler();
			//            };
			//            detailStore.load();

		},
		loadEmailDetailResponseHandler: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail');
			var eDetailStatus = detailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status == 'success') {
				mobEdu.enradms.leads.f.showEmailDetail();
			} else {
				Ext.Msg.show({
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		hasEmailDirection: function(direction) {
			if (direction == "IN") {
				return true;
			} else {
				return false;
			}
		},

		hasReadFlag: function(status) {
			if (status == "Opened") {
				return true;
			} else {
				return false;
			}
		},

		sentmail: function() {
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();
			if (msgArea != '' && esub != '') {
				mobEdu.enradms.leads.f.sentNewEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		sentReplyMessage: function() {
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();
			if (msgArea != '' && esub != '') {
				mobEdu.enradms.leads.f.sentNewEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sentNewEmail: function() {
			var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();

			//            var eDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail');
			//            var eDetailData = eDetailStore.data.all;
			//             var fromId = eDetailData[0].data.fromID;

			var eCreater = 1;
			var eStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.email');
			eStore.add({
				'subject': esub,
				'createdDate': '2013-02-25',
				'time': '',
				'body': msgArea,
				'leadID': lId,
				'emailID': data[0].data.emailId,
				'status': 'success',
				'createdBy': 'Walter',
				'readFlag': 'N',
				'emailDirection': 'OUT',
				'emailStatus': 'Unread',
				'entityType': '',
				//                'messageID':'01',
				'versionDate': '2013-02-25',
				'direction': 'OUT',
				'messageStatus': 'Unread',
				'fromID': 'Walter',
				'toID': lId
			});
			eStore.sync();
			Ext.Msg.show({
				id: 'rsubmitsuccess',
				title: null,
				cls: 'msgbox',
				message: '<center>Message sent successfully.</center>',
				buttons: Ext.MessageBox.OK,
				fn: function(btn) {
					if (btn == 'ok') {
						mobEdu.enradms.leads.f.emailListBackButton();
						mobEdu.enradms.leads.f.resetMail();
						mobEdu.enradms.leads.f.resetSendReplyMail();
					}
				}

			});
			//            eStore.getProxy().setHeaders({
			//                Authorization:mobEdu.util.getRecruiterAuthString()
			//            });
			//            eStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/sendmessage');
			//            eStore.getProxy().setExtraParams({
			//                leadID:lId,
			//                subject:esub,
			//                body:msgArea
			//            });
			//
			//            eStore.getProxy().afterRequest = function () {
			//                mobEdu.enradms.leads.f.newEmailResponseHandler();
			//            };
			//            eStore.load();
		},

		newEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.newEmail');
			var nEStatus = nEStore.getProxy().getReader().rawData;
			if (nEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsubmitsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Message sent successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.leads.f.emailListBackButton();
							mobEdu.enradms.leads.f.resetMail();
							mobEdu.enradms.leads.f.resetSendReplyMail();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetMail: function() {
			mobEdu.util.get('mobEdu.enradms.leads.view.leadNewEmail').reset();
		},
		resetSendReplyMail: function() {
			mobEdu.util.get('mobEdu.enradms.leads.view.newMail').reset();
		},

		loadAppointments: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;

			var searchText = '';

			var appStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments');
			appStore.load();
			appStore.removeAll();
			appStore.sync();

			appStore.add({
				'subject': 'Meeting Request 1',
				'appointmentDate': '2013-02-28 08:00',
				'appointmentStatus': 'Requested',
				'description': 'Request 1',
				'location': 'HR Block',
				'status': 'success',
				'leadID': 'C001000012',
				'source': ''
			});
			appStore.sync();
			mobEdu.enradms.leads.f.showAppointmentsList();
		},
		appointmentsResponseHandler: function() {
			var appStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments');
			appStore.sort('appointmentDate', 'DESC');
			var appStatus = appStore.getProxy().getReader().rawData;
			if (appStatus.status == 'success') {
				mobEdu.enradms.leads.f.showAppointmentsList();
			} else {
				Ext.Msg.show({
					message: appStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sendAppointment: function() {
			var aSub = Ext.getCmp('appsub').getValue();
			var aDes = Ext.getCmp('appdes').getValue();
			var aDate = Ext.getCmp('appdate').getValue();
			var aLoc = Ext.getCmp('apploc').getValue();
			var aStatus = Ext.getCmp('appsta').getValue();
			//            var aTime = Ext.getCmp('appTime').getValue();
			var aTimeHours = Ext.getCmp('appTimeHours').getValue();
			var aTimeMinutes = Ext.getCmp('appTimeMinutes').getValue();
			if (aStatus == 'select') {
				aStatus = '';
			}
			if (aTimeHours == 0) {
				aTimeHours = '';
			}

			if (aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aStatus != '' && aTimeHours != '') {
				var presentDate = new Date();
				var presentDateFormatted;
				if (presentDate == null) {
					presentDateFormatted = '';
				} else {
					presentDateFormatted = (presentDate.getFullYear() + '-' + [presentDate.getMonth() + 1] + '-' + presentDate.getDate());
				}
				var selectDate = Ext.getCmp('appdate').getValue();
				var selectDateFormatted;
				if (selectDate == null) {
					selectDateFormatted = '';
				} else {
					selectDateFormatted = (selectDate.getFullYear() + '-' + [selectDate.getMonth() + 1] + '-' + selectDate.getDate());
				}
				if (presentDateFormatted > selectDateFormatted) {
					Ext.Msg.show({
						title: 'Invalid Date',
						message: '<p>Please select a future date.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					mobEdu.enradms.leads.f.appointmentResponse();
				}
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},
		appointmentResponse: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;
			var aSub = Ext.getCmp('appsub').getValue();
			var aDes = Ext.getCmp('appdes').getValue();
			var aDate = Ext.getCmp('appdate').getValue();
			var aLoc = Ext.getCmp('apploc').getValue();
			var aStatus = Ext.getCmp('appsta').getValue();
			//            var aTime = Ext.getCmp('appTime').getValue();
			var aTimeHours = Ext.getCmp('appTimeHours').getValue();
			var aTimeMinutes = Ext.getCmp('appTimeMinutes').getValue();
			var appTime;
			if (aTimeHours == null) {
				aTimeHours = '';
			}
			if (aTimeMinutes == null) {
				aTimeMinutes = '';
			}
			appTime = aTimeHours + ':' + aTimeMinutes;
			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = (aDate.getFullYear() + '-' + [aDate.getMonth() + 1] + '-' + aDate.getDate());
			}
			var appDateTime = appDateFormatted + ' ' + appTime;

			var newAppStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments');

			newAppStore.add({
				'leadID': lId,
				'subject': aSub,
				'description': aDes,
				'appointmentDate': appDateTime,
				'location': aLoc,
				'status': aStatus
			});
			newAppStore.sync();
			Ext.Msg.show({
				id: 'rappsuccess',
				title: null,
				cls: 'msgbox',
				message: '<center>Appointment created successfully.</center>',
				buttons: Ext.MessageBox.OK,
				fn: function(btn) {
					if (btn == 'ok') {
						mobEdu.enradms.leads.f.appointmentListBack();
						mobEdu.enradms.leads.f.resetAppointment();
					}
				}

			});
		},
		newAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Appointment created successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.leads.f.appointmentListBack();
							mobEdu.enradms.leads.f.resetAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadUpdateAppointment: function() {
			mobEdu.enradms.main.f.loadAppointmentEnumerations();
			var upAppStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			var upDateStore = upAppStore.data.all;
			mobEdu.util.show('mobEdu.enradms.leads.view.updateAppointment');
			Ext.getCmp('admsupAppTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upsub').setValue(upDateStore[0].data.subject);
			Ext.getCmp('updes').setValue(upDateStore[0].data.description);
			var appDate = upDateStore[0].data.appointmentDate;
			var appDateValue = appDate.split(' ');
			var dateFormatted = appDateValue[0];
			var newStartDate = Ext.Date.parse(dateFormatted, "Y-m-d");
			var appTime = appDateValue[1];
			var appTimeFormatted = appTime.split(':');
			var appTimeHours = appTimeFormatted[0];
			var appTimeMints = appTimeFormatted[1];

			if (upDateStore[0].data.appointmentDate != '' && upDateStore[0].data.appointmentDate != undefined && upDateStore[0].data.appointmentDate != null) {
				Ext.getCmp('update').setValue(new Date(newStartDate));
			}
			Ext.getCmp('upTimeHours').setValue(appTimeHours);
			Ext.getCmp('upTimeMinutes').setValue(appTimeMints);
			Ext.getCmp('uploc').setValue(upDateStore[0].data.location);
			if (upDateStore[0].data.appointmentStatus != '' && upDateStore[0].data.appointmentStatus != undefined && upDateStore[0].data.appointmentStatus != null) {
				Ext.getCmp('upsta').setValue(upDateStore[0].data.appointmentStatus);
			}
		},

		updateAppointmentSend: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;
			var appDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			var appDeatil = appDetailStore.data.all;
			var appointmentId = appDeatil[0].data.appointmentID;
			var appDetail = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			var detailData = appDetail.data.all;
			//            var appStatus = detailData[0].data.status;
			var upSub = Ext.getCmp('upsub').getValue();
			var upDes = Ext.getCmp('updes').getValue();
			var upDate = Ext.getCmp('update').getValue();
			var upLoc = Ext.getCmp('uploc').getValue();
			var upStatus = Ext.getCmp('upsta').getValue();
			//            var uppTime = Ext.getCmp('upTime').getValue();
			var upAppTimeHours = Ext.getCmp('upTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('upTimeMinutes').getValue();
			var upAppTime;
			if (upAppTimeHours == null) {
				upAppTimeHours = '';
			}
			if (upAppTimeMints == null) {
				upAppTimeMints = '';
			}
			upAppTime = upAppTimeHours + ':' + upAppTimeMints;
			var appDateFormatted;
			if (upDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = (upDate.getFullYear() + '-' + [upDate.getMonth() + 1] + '-' + upDate.getDate());
			}
			var appDateTime = appDateFormatted + ' ' + upAppTime;
			var currentDate = new Date();
			var currentDateFormatted = (currentDate.getFullYear() + '-' + [currentDate.getMonth() + 1] + '-' + currentDate.getDate());
			if (appDateFormatted >= currentDateFormatted) {
				var newAppStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.newAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getRecruiterAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(enrouteWebserver + 'enroute/appointment/update');
				newAppStore.getProxy().setExtraParams({
					appointmentID: appointmentId,
					subject: upSub,
					description: upDes,
					location: upLoc,
					status: upStatus,
					appointmentDate: appDateTime,
					companyId: companyId
				});
				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enradms.leads.f.updateAppointmentResponseHandler();
				};
				newAppStore.load();
			} else {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<center>Appointment updated successfully.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enradms.leads.f.appointmentListBack();
							mobEdu.enradms.leads.f.resetUpDateAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetAppointment: function() {
			mobEdu.util.get('mobEdu.enradms.leads.view.newAppointment').reset();
		},

		resetUpDateAppointment: function() {
			mobEdu.util.get('mobEdu.enradms.leads.view.updateAppointment').reset();
		},

		onAppTimeKeyup: function(appTimeField) {
			var timeField = Ext.getCmp('appTime');
			var reg = new RegExp('^(20|21|22|23|[01]\d|\d)(([:][0-5]\d){1,2})$');
			if (reg.test(appTimeField.getValue()).toString() == false) {
				Ext.Msg.show({
					id: 'timeMsg',
					name: 'timeMsg',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid time.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							timeField.focus(true);
						}
					}
				});
			}
			//            var phoneNumber=(appTimeField.getValue()).toString()+'';
			////            phoneNumber = phoneNumber.replace(/\D/g, '');
			//            appTimeField.setValue(phoneNumber);
			//            var length=phoneNumber.length;
			//            if(length>5){
			//                appTimeField.setValue(phoneNumber.substring(0,5));
			//                return false;
			//            }
			//            return true;
		},

		loadNotificationsList: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;
			var upappServiceText = 'notification';
			var upOperationText = 'leadnotifications';
			var notfiID = 1;
			var notfiStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.notifications');

			notfiStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			notfiStore.getProxy().setUrl(enrouteWebserver + 'enquire');
			notfiStore.getProxy().setExtraParams({
				service: upappServiceText,
				operation: upOperationText,
				leadID: lId,
				companyId: companyId
			});

			notfiStore.getProxy().afterRequest = function() {
				mobEdu.enradms.leads.f.loadNotificationsResponseHandler();
			};
			notfiStore.load();
		},
		loadNotificationsResponseHandler: function() {
			var notfiStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.notifications');
			var notfiStatus = notfiStore.getProxy().getReader().rawData;
			if (notfiStatus.id != '') {
				mobEdu.enradms.leads.f.showNotificationsList();
			} else {
				Ext.Msg.show({
					message: '<p>No notifications.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadNotificationListDetail: function(record) {
			var notfiID = record.data.id;
			var notifServiceText = 'notification';
			var notifOperationText = 'view';

			var notifiDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.notificationDetail');

			notifiDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getRecruiterAuthString(),
				companyID: companyId
			});
			notifiDetailStore.getProxy().setUrl(enrouteWebserver + 'enquire');
			notifiDetailStore.getProxy().setExtraParams({
				service: notifServiceText,
				operation: notifOperationText,
				notificationID: notfiID,
				companyId: companyId
			});

			notifiDetailStore.getProxy().afterRequest = function() {
				mobEdu.enradms.leads.f.lNotificationListDetailResponseHandler();
			};
			notifiDetailStore.load();
		},

		lNotificationListDetailResponseHandler: function() {
			var notifiDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.notificationDetail');
			var notfiDetailStatus = notifiDetailStore.getProxy().getReader().rawData;
			if (notfiDetailStatus.id != '') {
				mobEdu.enradms.leads.f.showNotificationDetail();
			} else {
				Ext.Msg.show({
					message: '<p>No notifications.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAppointDetail: function(record) {
			var appId = record.data.appointmentID;

			var appDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			appDetailStore.load();
			appDetailStore.removeAll();
			appDetailStore.sync();

			appDetailStore.add(record);
			appDetailStore.sync();
			mobEdu.enradms.leads.f.loadAppDetail();
		},
		appointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			var appDetailStatus = appDetailStore.getProxy().getReader().rawData;
			if (appDetailStatus.status == 'success') {
				mobEdu.enradms.leads.f.loadAppDetail();
			} else {
				Ext.Msg.show({
					message: appDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		appointmentListBack: function() {
			var appListStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments');
			appListStore.load();
			mobEdu.enradms.leads.f.showAppointmentsList();
		},

		hasAppointmentDirection: function(appSource) {
			if (appSource == "IN") {
				return true;
			} else {
				return false;
			}
		},

		showProfileDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showProfileList);
			mobEdu.util.show('mobEdu.enradms.leads.view.profileDetail')
		},

		showProfileList: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.main.f.showEnrouteMainview);
			mobEdu.util.show('mobEdu.enradms.leads.view.profileList');
		},



		emailListBackButton: function() {
			var eListStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.email');
			eListStore.load();
			mobEdu.enradms.leads.f.showEmailList();
		},

		showEmailList: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enradms.leads.view.emailList');
			Ext.getCmp('admsemailTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showEmailDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showEmailList);
			mobEdu.util.show('mobEdu.enradms.leads.view.emailDetail');
			Ext.getCmp('admseDetailTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},

		showNotificationsList: function() {
			mobEdu.util.show('mobEdu.enradms.leads.view.notificationsList');
			Ext.getCmp('notifiListTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showAppointmentsList: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enradms.leads.view.appointmentsList');
			Ext.getCmp('admsappTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showAdminAppList: function() {
			mobEdu.util.show('mobEdu.enradms.leads.view.adminAppList');
		},
		showNewMail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showEmailList);
			mobEdu.util.show('mobEdu.enradms.leads.view.newMail');
			Ext.getCmp('admsnewETitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showLeadEmail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showEmailList);
			mobEdu.util.show('mobEdu.enradms.leads.view.leadNewEmail');
			Ext.getCmp('newLeadTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showAdmissionApp: function() {
			mobEdu.util.show('mobEdu.enradms.leads.view.admissionApp');
			Ext.getCmp('admissionTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showAppointmentDetail: function(record) {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
			appDetailStore.removeAll();
			appDetailStore.add(record);
			appDetailStore.sync();
			mobEdu.enradms.leads.f.loadAppDetail();
		},
		loadAppDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.enradms.leads.view.appointmentDetail');
			Ext.getCmp('admsappDetailTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showNotificationDetail: function(record) {
			//            var notifiDetailStore=mobEdu.util.getStore('mobEdu.enradms.leads.store.notificationDetail');
			//            notifiDetailStore.removeAll();
			//            notifiDetailStore.add(record);
			//            notifiDetailStore.sync();
			mobEdu.enradms.leads.f.loadNotifiDetail();
		},
		loadNotifiDetail: function() {
			mobEdu.util.show('mobEdu.enradms.leads.view.notificationDetail');
			Ext.getCmp('nltfiDatilTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showFinancialApp: function() {
			mobEdu.util.show('mobEdu.enradms.leads.view.financialApp');
			Ext.getCmp('financialTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		onAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = (newDate.getFullYear() + '-' + [newDate.getMonth() + 1] + '-' + newDate.getDate());
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = (presentDate.getFullYear() + '-' + [presentDate.getMonth() + 1] + '-' + presentDate.getDate());
			}

			if (newDateFormatted == presentDateFormatted) {
				//                if(timeH < 18){
				//                    Ext.Msg.show({
				//                        title:'Invalid Time',
				//                        message:'Appointment time between 8AM to 18PM.',
				//                        buttons:Ext.MessageBox.OK,
				//                        cls:'msgbox'
				//                    });
				//                    Ext.getCmp('appTimeHours').setMinValue(8);
				//                    Ext.getCmp('appTimeMinutes').setMinValue(0);
				//                    Ext.getCmp('appTimeHours').setValue(8);
				//                    Ext.getCmp('appTimeMinutes').setValue(0);
				//                }else{

				if (Ext.getCmp('appTimeHours') != undefined) {
					Ext.getCmp('appTimeHours').setMinValue(timeH);
					Ext.getCmp('appTimeHours').setValue(timeH);
				}
				if (Ext.getCmp('appTimeMinutes') != undefined) {
					Ext.getCmp('appTimeMinutes').setMinValue(timeM);
					Ext.getCmp('appTimeMinutes').setValue(timeM);
				} //                }
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('appdate').setValue(new Date());
							Ext.getCmp('appTimeHours').setMinValue(timeH);
							Ext.getCmp('appTimeMinutes').setMinValue(timeM);
							Ext.getCmp('appTimeHours').setValue(timeH);
							Ext.getCmp('appTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('appTimeHours').setMinValue(8);
				Ext.getCmp('appTimeMinutes').setMinValue(0);
				Ext.getCmp('appTimeHours').setValue(8);
				Ext.getCmp('appTimeMinutes').setValue(0);
			}
		},

		showNewAppointment: function() {
			mobEdu.enradms.main.f.loadAppointmentEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.enradms.leads.view.newAppointment');
			// Ext.getCmp('admsnewAppTitle').setTitle('<h1>' + leadTitle + '</h1>');
			var newDate = Ext.getCmp('appdate').getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			Ext.getCmp('appTimeHours').setMinValue(timeH);
			Ext.getCmp('appTimeMinutes').setMinValue(timeM);
			Ext.getCmp('appTimeHours').setValue(timeH);
			Ext.getCmp('appTimeMinutes').setValue(timeM);
		},


		loadDocuments: function() {
			var docstore = mobEdu.util.getStore('mobEdu.enradms.leads.store.docList');
			docstore.load();
			docstore.removeAll();
			docstore.sync();
			docstore.add({
				'type': 'Admissions Application',
				'status': 'In Progress'
			});
			docstore.sync();
			docstore.add({
				'type': 'Health Insurance',
				'status': 'In Progress'
			});
			docstore.sync();
			mobEdu.enradms.leads.f.showDocList();
		},

		showDocList: function() {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enradms.leads.view.documentList');
			Ext.getCmp('admsdocumentTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},

		showDocView: function(index) {
			mobEdu.util.updatePrevView(mobEdu.enradms.leads.f.showDocList);
			mobEdu.util.show('mobEdu.enradms.leads.view.documentView');
			if (index == '0') {
				var img = Ext.getCmp('admsimg');
				img.setHtml('<img src="' + mobEdu.util.getResourcePath() + 'images/application.gif" height="100%" width="100%" >');
			} else {
				var img = Ext.getCmp('admsimg');
				img.setHtml('<img src="' + mobEdu.util.getResourcePath() + 'images/insurence.jpg" height="100%" width="100%" >');
			}
			Ext.getCmp('admsdocumentViewTitle').setTitle('<h1>' + leadTitle + '</h1>');
		}
	}
});