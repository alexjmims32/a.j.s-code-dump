Ext.define('mobEdu.enradms.leads.model.profileList', {
    extend:'Ext.data.Model',

    config:{

        fields : [
            {
                name : 'firstName',
                type : 'string'
            },{
                name : 'lastName',
                type : 'string'
            },{
                name : 'status',
                type : 'string'
            },{
                name : 'createdDate',
                type : 'string'
            },{
                name:'leadID',
                type:'string'
            },
            {
                name:'leadStatus',
                type:'string'
            },
            {
                name:'addressID',
                type:'string'
            },
            {
                name:'addressType',
                type:'string'
            },
            {
                name:'address1',
                type:'string'
            },
            {
                name:'address2',
                type:'string'
            },
            {
                name:'address3',
                type:'string'
            },
            {
                name : 'gender',
                type : 'string'
            },
            {
                name : 'birthDate',
                type : 'string'
            },
//            {
//                name : 'address',
//                type : 'string'
//            },
            {
                name : 'city',
                type : 'string'
            },{
                name : 'state',
                type : 'string'
            },{
                name : 'zip',
                type : 'string'
            },{
                name : 'country',
                type : 'string'
            },{
                name : 'phone1',
                type : 'string'
            },{
                name : 'phone2',
                type : 'string'
            },{
                name : 'email',
                type : 'string'
            },
            {
                name:'appointmentID',
                type: 'string'
            },
            {
                name:'versionDate',
                type:'string'
            },
            {
                name:'versionNo',
                type:'string'
            },
            {
                name:'versionUser',
                type:'string'
            },{
                name:'recruiterName',
                type:'string'
            }
        ]
    }
});
