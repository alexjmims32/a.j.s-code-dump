Ext.define('mobEdu.enradms.leads.model.newEmail', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'emailID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});