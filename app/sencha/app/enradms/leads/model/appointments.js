Ext.define('mobEdu.enradms.leads.model.appointments', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'subject',
                type:'string'
            },
            {
                name:'appointmentDate',
                type:'string'
            },
            {
                name:'appointmentID',
                type:'string'
            },
            {
                name:'appointmentStatus',
                type: 'string'
            },
            {
                name:'description',
                type: 'string'
            },{
                name:'location',
                type:'string'
            },{
                name:'status',
                type:'string'
            },
            {
                name:'leadID',
                type:'string'
            },
            {
                name:'source',
                type:'string'
            }
        ]
    }
});