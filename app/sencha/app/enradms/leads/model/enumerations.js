Ext.define('mobEdu.enradms.leads.model.enumerations', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'status',
                type:'string'
            },
            {
                name:'displayValue',
                type:'string'
            },
            {
                name:'enumValue',
                type:'string'
            },
            {
                name:'sortOrder',
                type:'string'
            }
        ]
    }
});