Ext.define('mobEdu.enradms.leads.model.newAppointment', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'appointmentID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});