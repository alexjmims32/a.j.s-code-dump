Ext.define('mobEdu.enradms.leads.model.docList', {
	extend: 'Ext.data.Model',
	config: {
		fields: [
			{
				name: 'type',
				type: 'string'
			},
			{
				name: 'status',
				type: 'string'
			}
		]
	}
});