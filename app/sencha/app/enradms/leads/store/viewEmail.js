Ext.define('mobEdu.enradms.leads.store.viewEmail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.email'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.viewEmail',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.email',

                   proxy : {
             type:'localstorage',
            id:'viewEmail'
        }
    }

//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});