Ext.define('mobEdu.enradms.leads.store.newEmail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.newEmail'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.newEmail',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.newEmail',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});