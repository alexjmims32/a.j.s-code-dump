Ext.define('mobEdu.enradms.leads.store.appointments', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.appointments'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.appointments',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.appointments',

             proxy : {
            type:'localstorage',
            id:'appointments'
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'appointments'
//        return proxy;
//    }

});