Ext.define('mobEdu.enradms.leads.store.localEmail', {
    extend:'Ext.data.Store',
//    alias: 'notificationLocalStore',

    requires: [
        'mobEdu.enradms.leads.model.email'
    ],
//       model : 'notificationModel',
    config: {
        storeId: 'mobEdu.enradms.leads.store.localEmail',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.email',

        proxy:{
            type:'localstorage',
            id:'emailLocal'
        }

    }
});