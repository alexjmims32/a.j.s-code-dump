Ext.define('mobEdu.enradms.leads.store.docList', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.docList'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.docList',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.docList',

             proxy : {
            type:'localstorage',
            id:'docList'
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'appointments'
//        return proxy;
//    }

});