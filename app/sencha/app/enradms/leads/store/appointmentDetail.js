Ext.define('mobEdu.enradms.leads.store.appointmentDetail', {
    extend:'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.appointments'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.appointmentDetail',
        autoLoad: false,
        model: 'mobEdu.enradms.leads.model.appointments',

        proxy : {
            type:'localstorage',
            id:'appointmentDetail'
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});
