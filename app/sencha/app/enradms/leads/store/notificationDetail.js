Ext.define('mobEdu.enradms.leads.store.notificationDetail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.notifications'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.notificationDetail',
        autoLoad: false,
        model: 'mobEdu.enradms.leads.model.notifications',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});
