Ext.define('mobEdu.enradms.leads.store.adminApp', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enradms.leads.model.appointments'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.adminApp',
        autoLoad: true,
        model: 'mobEdu.enradms.leads.model.appointments',
        data:[
            {title:'Application1', date:'Mon 10 Sep 2012',time:'11:30 AM', details:'Application'},
            {title:'Application2', date:'Sat 15 Sep 2012',time:'14:45 PM', details:'Application'}
        ]
    }
});