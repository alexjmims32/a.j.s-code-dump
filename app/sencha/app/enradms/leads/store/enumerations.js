Ext.define('mobEdu.enradms.leads.store.enumerations', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.enumerations',
        autoLoad: false,
        model: 'mobEdu.enradms.leads.model.enumerations',

        proxy : {
            type:'localstorage',
            id:'appointments'
        }
    }
});