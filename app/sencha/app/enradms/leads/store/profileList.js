Ext.define('mobEdu.enradms.leads.store.profileList', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.profileList'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.profileList',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.profileList',

                    proxy : {
             type:'localstorage',
            id:'profileList'
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});
