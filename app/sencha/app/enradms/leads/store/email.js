Ext.define('mobEdu.enradms.leads.store.email', {
    extend:'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
    'mobEdu.enradms.leads.model.email'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.email',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.email',

        proxy : {
            type:'localstorage',
            id:'email'
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'emails'
//        return proxy;
//    }

});
