Ext.define('mobEdu.enradms.leads.store.notifications', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.notifications'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.notifications',
        autoLoad: false,
        model: 'mobEdu.enradms.leads.model.notifications',

                    proxy: {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'notificationList.notification'
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'notificationList.notification'
//        return proxy;
//    }
});