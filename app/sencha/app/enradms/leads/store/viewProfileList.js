Ext.define('mobEdu.enradms.leads.store.viewProfileList', {
    extend:'Ext.data.Store',


    requires: [
        'mobEdu.enradms.leads.model.viewProfileList'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.viewProfileList',
        autoLoad: false,
        model: 'mobEdu.enradms.leads.model.viewProfileList',
        proxy:{
            type:'localstorage',
            id:'viewProfileList'
        }
    }
});
