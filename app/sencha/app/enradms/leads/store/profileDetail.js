Ext.define('mobEdu.enradms.leads.store.profileDetail', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enradms.leads.model.profileList'
    ],

    config:{
        storeId: 'mobEdu.enradms.leads.store.profileDetail',
        autoLoad: false,
        model: 'mobEdu.enradms.leads.model.profileList',
        proxy:{
            type:'localstorage',
            id:'profileDetail'
        }
    }
});