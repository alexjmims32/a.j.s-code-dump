Ext.define('mobEdu.enradms.leads.store.newAppointment', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enradms.leads.model.newAppointment'
    ],
    config: {
        storeId: 'mobEdu.enradms.leads.store.newAppointment',

        autoLoad: false,

        model: 'mobEdu.enradms.leads.model.newAppointment',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});