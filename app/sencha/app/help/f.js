Ext.define('mobEdu.help.f', {
	requires: [
		'mobEdu.help.store.aboutText',
		'mobEdu.help.store.faqs',
		'mobEdu.help.store.menu'
	],
	statics: {

		initializeHelpView: function() {
			var store = mobEdu.util.getStore('mobEdu.help.store.menu');
			store.load();
			if (store.data.length > 0) {
				store.removeAll();
				store.sync();
				store.removed = [];
			}
			if (displayfaqs == 'true') {
				store.add({
					title: 'FAQs',
					action: mobEdu.help.f.loadFAQs
				});
				store.sync();
			}
			if (campusCode == 'UT') {
				store.add({
					title: 'About MyUT Mobile',
					action: mobEdu.help.f.loadAboutAppText
				});
				store.sync();
			} else {
				store.add({
					title: mobEdu.util.getAboutTitle(),
					action: mobEdu.help.f.loadAboutText
				});
				store.sync();

				store.add({
					title: 'About eNtourage',
					action: mobEdu.help.f.loadAboutEntourage
				});
				store.sync();
			}



			if (displayvideos == 'true') {
				console.log(displayvideos);
				store.add({
					title: 'How to videos',
					action: mobEdu.help.f.loadVideos
				});
				store.sync();
			}

			if (showPreferencesInHelp != 'false') {
				store.add({
					title: 'Preferences',
					action: mobEdu.settings.f.loadPreferences
				});
				store.sync();
			}

			mobEdu.help.f.showMenu();
			mobEdu.util.setTitle('HELP', Ext.getCmp('helpTitle'));
		},

		contactInfo: function() {
			mobEdu.util.loadExternalUrl('http://www.n2nservices.com');
		},

		loadVideos: function() {
			var feedsLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
			var feedId, moduleId;
			if (feedsLocalStore.data.length > 0) {
				var rec = feedsLocalStore.findRecord('name', 'How to videos');
				if (rec != null) {
					feedId = rec.get('feedId');
					moduleId = rec.get('moduleID');
				}
			}
			mobEdu.help.f.loadFeedList(moduleId, feedId);
			mobEdu.help.f.showVideoList();
		},

		loadFeedList: function(m, p) {
			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
			var records = new Array();
			feedLocalStore.each(function(record) {
				if (record.data.moduleID == m && record.data.parentID == p) {
					records.push(record);
				}
			});
			var videosLocal = mobEdu.util.getStore('mobEdu.help.store.videos');
			if (videosLocal.data.length > 0) {
				videosLocal.removeAll();
				videosLocal.sync();
				videosLocal.removed = [];
			}
			for (var i = 0; i < records.length; i++) {
				videosLocal.add(records[i]);
				videosLocal.sync();
			}
		},

		loadFAQs: function() {
			var store = mobEdu.util.getStore('mobEdu.help.store.faqs');
			store.clearFilter();
			if (store.getCount() > 0) {
				mobEdu.help.f.showFAQs();
			}

			var code = mobEdu.util.getSchoolCode();
			if (code == null || code == '') {
				code = campusCode
			}
			store.getServerProxy().setHeaders({
				companyID: companyId
			});
			store.getServerProxy().setExtraParams({
				campusCode: code,
				clientCode: code
			});
			store.loadServer(function() {
				mobEdu.help.f.loadFAQsResponsehandler();
			});
		},
		loadFAQsResponsehandler: function() {
			var store = mobEdu.util.getStore('mobEdu.help.store.faqs');
			mobEdu.help.f.showFAQs();
			if (store.getCount() === 0) {
				Ext.getCmp('faqsList').setEmptyText('No FAQs available');
			} else {
				Ext.getCmp('faqsList').setEmptyText(null);
			}
		},

		loadAboutText: function() {
			var store = mobEdu.util.getStore('mobEdu.help.store.aboutText');
			store.clearFilter();

			if (store.getCount() > 0) {
				mobEdu.help.f.showAbout();
			}

			var code = mobEdu.util.getSchoolCode();
			if (code == null || code == '') {
				code = campusCode
			}

			store.getServerProxy().setExtraParams({
				campusCode: code,
				clientCode: code
			});
			store.loadServer(function() {
				mobEdu.help.f.loadAboutTextResponsehandler();
			});
		},

		loadAboutTextResponsehandler: function() {
			var store = mobEdu.util.getStore('mobEdu.help.store.aboutText');
			var data = store.data.all;
			var text = data[0].data.areaText;
			if (text == '') {
				text = '';
			} else {
				text = mobEdu.util.formatUrls(text);
				mobEdu.util.updateAboutText(text);
			}
			mobEdu.help.f.showAbout();
			Ext.getCmp('aboutText').setHtml('<h3><p class="wordWrap">' + text + '</p></h3>');
		},

		loadVideoView: function(record) {
			mobEdu.util.get('mobEdu.help.view.videoView');
			var link = record.get('link').replace('watch?v=', 'embed/');
			link = link.replace('&', '?');

			if (Ext.os.is.Android) {
				var videoId = link.replace("http://www.youtube.com/embed/", "");
				videoId = videoId.split("&")[0];
				videoId = videoId.split("?")[0];
				window.plugins.youtube.show({
					videoid: videoId
				}, function() {}, function() {});
			} else {
				Ext.getCmp('helpVideoPanel').setHtml('<embed id="player" width="100%" height="100%" src="' + link + '" frameborder="0" allowfullscreen></embed>');
				mobEdu.help.f.showVideoView();
			}
		},

		showAbout: function() {
			mobEdu.util.updatePrevView(mobEdu.help.f.showMenu);
			mobEdu.util.show('mobEdu.help.view.about');
			Ext.getCmp('aboutUsTitle').setTitle('<h1>' + mobEdu.util.getAboutTitle() + '</h1>');
		},

		showFAQs: function() {
			if (mobEdu.util.currentModule == 'FAQS')
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			else {
				mobEdu.util.updatePrevView(mobEdu.help.f.showMenu);
			}

			mobEdu.util.show('mobEdu.help.view.faqs');
		},

		showVideoList: function() {
			mobEdu.util.updatePrevView(mobEdu.help.f.showMenu);
			mobEdu.util.show('mobEdu.help.view.videoList');
		},

		showVideoView: function() {
			mobEdu.util.updatePrevView(mobEdu.help.f.showVideoList);
			mobEdu.util.show('mobEdu.help.view.videoView');
		},

		showMenu: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.help.view.menu');
			mobEdu.util.setTitle('HELP', Ext.getCmp('helpTitle'));
		},

		loadAboutEntourage: function() {
			mobEdu.util.updatePrevView(mobEdu.help.f.showMenu);
			mobEdu.util.show('mobEdu.help.view.aboutEntourage');
		},

		loadAboutAppText: function() {
			var store = mobEdu.util.getStore('mobEdu.help.store.aboutText');
			store.clearFilter();

			if (store.getCount() > 0) {
				mobEdu.help.f.showAboutApp();
			}

			var code = mobEdu.util.getSchoolCode();
			if (code == null || code == '') {
				code = campusCode
			}

			store.getServerProxy().setExtraParams({
				campusCode: code
			});
			store.loadServer(function() {
				var data = store.data.all;
				var text = data[0].data.areaText;
				if (text == '') {
					text = '';
				} else {
					text = decodeURIComponent(text);
					text = mobEdu.util.formatUrls(text);
					mobEdu.util.updateAboutText(text);
				}
				mobEdu.help.f.showAboutApp();
				Ext.getCmp('aboutText').setHtml('<h3><p class="wordWrap">' + text + '</p></h3>');
			});
		},

		searchFaq: function() {
			var value = Ext.getCmp('faqSearchParam').getValue();
			var faqsLocalStore = mobEdu.util.getStore('mobEdu.help.store.faqs');
			faqsLocalStore.clearFilter();
			faqsLocalStore.filter(Ext.create('Ext.util.Filter', {
				filterFn: function(item) {
					var str = item.get("question").toLowerCase().search(value.toLowerCase()) !== -1;
					if (str == false) {
						var str1 = item.get("answer").toLowerCase().search(value.toLowerCase()) !== -1;
						return str1;
					}
					return str;
				},
				root: 'data'
			}));
		},


		showAboutApp: function() {
			mobEdu.util.updatePrevView(mobEdu.help.f.showMenu);
			mobEdu.util.show('mobEdu.help.view.aboutApp');
			Ext.getCmp('aboutAppTitle').setTitle('<h1>' + mobEdu.util.getAboutTitle() + '</h1>');
		}
	}
});