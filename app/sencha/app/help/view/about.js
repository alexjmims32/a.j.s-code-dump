Ext.define('mobEdu.help.view.about', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        id: 'aboutText',
        padding:'10px',
        html: '',
        items: [{
            //            title: '<h1>About</h1>',
            xtype: 'customToolbar',
            id:'aboutUsTitle'
        }],
        flex: 1
    }
});