Ext.define('mobEdu.help.view.aboutApp', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [
        {
            xtype:'panel',
            padding:'10px',
            height:'100%',
            scrollable:true,
            html:'<p><br/><br/><br/><b>About eNtourage</b><br /><br /><b>Version 1.0</b><br/><br/>N2N eNtourage&#8482 is an integrated Enterprise Mobile Integration Framework designed specifically for Higher Education and K-12.<br/><br/>N2N eNtourage&#8482 is a suite of Mobile and Desktop applications for use by students, faculty and staff.<br/><br/>N2N eNtourage&#8482 applications help campuses with enrollment, retention, progression and graduation by leveraging better Student Engagement & communication on Mobile devices.<br/><br/>N2N eNtourage&#8482 provides Mobile applications for Inquiry Management, Enrollment Management, Student Engagement and Alumni involvement.<br/><br/>To learn more visit  <a href="javascript:mobEdu.help.f.contactInfo()">www.n2nservices.com</a><br/></p>',
            items:[
            {
                xtype:'panel',
                id:'aboutText',
                padding:'10px',
                html:''
            }]
        },
        {
            xtype: 'customToolbar',
            id:'aboutAppTitle'
        }       
        ],
        flex: 1
    }
});