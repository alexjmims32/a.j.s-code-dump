Ext.define('mobEdu.help.view.faqs', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.help.store.faqs'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>FAQs</h1>'
        }, {
            xtype: 'textfield',
            docked: 'top',
            id: 'faqSearchParam',
            labelWidth: 0,
            flex: 1,
            placeHolder: 'Enter Text to Search',
            listeners: {
                keyup: function(textfield, e, eOpts) {
                    mobEdu.help.f.searchFaq();
                },
                clearicontap: function(textfield, e, eOpts) {
                    mobEdu.help.f.searchFaq();
                }
            }
        }, {
            xtype: 'list',
            id: 'faqsList',
            emptyText: '<h3 align="center">No Faqs</h3>',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h2>{question}</h2></td></tr><tr><td width="100%" align="left"><div class="wordWrap"><h3>{[mobEdu.util.formatUrls(values.answer)]}</h3></div></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.help.store.faqs'),
            disableSelection: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                }
            }
        }],
        flex: 1
    }
});