Ext.define('mobEdu.help.view.videoView', {
	extend: 'Ext.Container',
	alias: 'helpvideoView',
	config: {
		fullscreen: 'true',
		layout: {
			type: 'vbox',
			align: 'strech'
		},
		cls: 'logo',
		defaults: {
			flex: 0
		},
		scrollable: true,
		items: [{
				xtype: 'panel',
				id: 'helpVideoPanel',
				html: ''
			}, {
				xtype: 'customToolbar',
				docked: 'top',
				title: '<h1>Video</h1>'
			}]
	}
});