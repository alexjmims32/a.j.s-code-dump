Ext.define('mobEdu.help.view.videoList', {
    extend: 'Ext.Panel',
	requires: [
		'mobEdu.help.store.videos'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'helpVideosList',
            itemTpl: '<table width="100%"><tr><td width="80%" align="left" ><h3>{name}</h3></td><td width="20%" align="right" ><h4>{date}</h4></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.help.store.videos'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.help.f.loadVideoView(record);
                }
            }
        }, {
            xtype: 'customToolbar',
            docked: 'top',
            id: 'helpVideoListTitle',
            title:'<h1>Videos</h1>'
        }],
        flex: 1
    }
});