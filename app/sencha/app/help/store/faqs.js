Ext.define('mobEdu.help.store.faqs', {
    extend: 'Ext.ux.OfflineSyncStore',

    requires: [
        'mobEdu.help.model.faqs'
    ],

    config: {
        model: 'mobEdu.help.model.faqs',

        trackLocalSync: false,
        autoServerSync: false,

        // define a LOCAL proxy for saving the store's data locally
        localProxy: {
            type: 'localstorage',
            id: 'faqs-sync-store'
        },

        // define a SERVER proxy for saving the store's data on the server
        serverProxy: {
            type: 'ajax',
            api: {
                read: encorewebserver + 'open/faqs'
            },
            reader: {
                type: 'json',
                rootProperty: 'faqList'
            }
        }
    },

    initialize: function() {
        this.loadLocal();
    }
});