Ext.define('mobEdu.help.store.videos', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.feeds.model.feeds'
    ],

    config:{
        storeId: 'mobEdu.help.store.videos',
        autoLoad: true,
        model: 'mobEdu.feeds.model.feeds',
        proxy:{
            type:'localstorage',
            id:'helpVideosLocalStorage'
        }
    }

});