Ext.define('mobEdu.help.store.menu', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.help.model.menu'
    ],

    config:{
        storeId: 'mobEdu.help.store.menu',
        autoLoad: false,
        model: 'mobEdu.help.model.menu',
        proxy:{
            type:'memory',
            id:'helpStorage'
        }
    }
});