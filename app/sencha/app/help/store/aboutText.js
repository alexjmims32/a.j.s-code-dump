Ext.define('mobEdu.help.store.aboutText', {
    extend: 'Ext.ux.OfflineSyncStore',

    requires: [
        'mobEdu.help.model.aboutText'
    ],

    config: {
        model: 'mobEdu.help.model.aboutText',

        trackLocalSync: false,
        autoServerSync: false,

        // define a LOCAL proxy for saving the store's data locally
        localProxy: {
            type: 'localstorage',
            id: 'help-sync-store'
        },

        // define a SERVER proxy for saving the store's data on the server
        serverProxy: {
            type: 'ajax',
            api: {
                read: encorewebserver + 'open/helpContent'
            },
            headers: {
                companyID: companyId
            },
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    },

    initialize: function() {
        this.loadLocal();
    }
});