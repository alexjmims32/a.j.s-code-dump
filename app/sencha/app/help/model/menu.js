Ext.define('mobEdu.help.model.menu', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'title'
            },
            {
                name:'action',
                type: 'function'
            }
        ]
    }
});
