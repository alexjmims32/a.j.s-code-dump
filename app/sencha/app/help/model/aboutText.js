Ext.define('mobEdu.help.model.aboutText', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'campusCode',
            type: 'string'
        }, {
            name: 'areaText',
            type: 'string'
        }, {
            name: 'faq',
            type: 'string'
        }, {
            name: 'termAndConditions',
            type: 'string'
        }]
    }
});