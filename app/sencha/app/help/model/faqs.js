Ext.define('mobEdu.help.model.faqs', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'question'
            },
            {
                name:'answer'
            }
        ]
    }
});
