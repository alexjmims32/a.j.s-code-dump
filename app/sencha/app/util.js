Ext.define('mobEdu.util', {
    singleton: true,
    authString: null,
    studentId: null,
    code: null,
    studentLdapId: null,
    firstName: null,
    lastName: null,
    proxyUserId: null,
    proxyLdapId: null,
    viewMap: new Ext.util.HashMap(),
    storeMap: new Ext.util.HashMap(),
    viewStack: new Array(),
    slideMenuCmp: null,
    maxDrag: 250,
    dragDuration: 300,
    draggableWidth: 100,
    analyticsPlugin: null,
    loginModuleCode: null,
    currentModule: null,
    parentModule: null,
    menuIconNumber: 0,
    menuImageName: null,
    menuGroupIconNumber: 0,
    menuGroupImageName: null,
    imageUrl: null,
    sessionCASValidated: false,
    casURL: null,
    masked: false,
    showBioInfo: false,
    isLogin: false,
    applicationId: null,
    isHomeTapped: false,

    isShowCampus: function() {
        if (isShowCampus == "YES")
            return true;
        return false;
    },
    updateFirstName: function(firstName) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                firstName: firstName
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('firstName', firstName);
        }
        entourageStore.sync();

    },
    updateImageUrl: function(imageUrl) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                imageUrl: imageUrl
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('imageUrl', imageUrl);
        }
        entourageStore.sync();

    },
    getImageUrl: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var imageUrl = rec.data.imageUrl;
            return imageUrl;
        }
        return null;
    },
    alreadySingnedUpUser: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
    },
    updateWelcomeVideoFlag: function(flag) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                welcomeVideo: flag
            });
        } else {
            rec = entourageStore.getAt(0);
            rec.set('welcomeVideo', flag);
        }
        entourageStore.sync();
    },

    doLeadLoginCheck: function(loginFlag, viewRef) {
        if (mobEdu.util.getLeadId() == null && mobEdu.util.getLeadAuthString() == null) {
            mobEdu.util.updateNextView(viewRef);
            mobEdu.enquire.f.showLeadLogin();
        } else {
            //            eval(viewRef + '()');
            mobEdu.enquire.f.showLeadMainView();
        }
    },

    updateLastRequestTime: function(lastRequestTime) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                lastRequestTime: lastRequestTime
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('lastRequestTime', lastRequestTime);
        }
        entourageStore.sync();
    },

    getLastRequestTime: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var lastRequestTime = rec.data.lastRequestTime;
            return lastRequestTime;
        }
        return null;
    },

    updateUsername: function(username) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                username: username
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('username', username);
        }
        entourageStore.sync();
    },

    getUsername: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var username = rec.data.username;
            return username;
        }
        return null;
    },

    updateLastName: function(lastName) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                lastName: lastName
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('lastName', lastName);
        }
        entourageStore.sync();
    },
    updateIsLogin: function() {
        mobEdu.util.isLogin = true;

    },
    isLoginFn: function() {
        if (mobEdu.util.getAuthString() != null)
            return true;
        return false;
    },
    getFirstName: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var firstName = rec.data.firstName;
            return firstName;
        }
        return null;
    },

    getLastName: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var lastName = rec.data.lastName;
            return lastName;
        }
        return null;
    },



    getAccTermDescription: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        var desc = (entourageStore.getAt(0)).get('accTermDescription');
        if (desc == null || desc == '')
            return 'Summary'
        return desc;
    },
    getAccTermCode: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('accTermCode');
    },
    getCartTermDescription: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        var desc = (entourageStore.getAt(0)).get('cartTermDescription');
        if (desc == null || desc == '')
            return 'Cart'
        return desc;
    },
    getCartTermCode: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('cartTermCode');
    },
    getCoursesTermDescription: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        var desc = (entourageStore.getAt(0)).get('coursesTermDescription');
        if (desc == null || desc == '')
            return 'Courses'
        return desc;
    },
    getCoursesTermCode: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('coursesTermCode');
    },
    getSummaryFlag: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('summaryFlag');
    },
    getDetailsFlag: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('detailsFlag');
    },
    getModules: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('modules');
    },

    isProxyRegEnabled: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.getAt(0) != null) {
            return (entourageStore.getAt(0)).get('proxyReg');
        } else {
            return null;
        }
    },

    updateProxyReg: function(proxyReg) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                proxyReg: proxyReg
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('proxyReg', proxyReg);
        }
        entourageStore.sync();
    },

    updateProxyAccessFlag: function(proxyAccessFlag) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                proxyAccessFlag: proxyAccessFlag
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('proxyAccessFlag', proxyAccessFlag);
        }
        entourageStore.sync();
    },

    getProxyAccessFlag: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.getAt(0) != null) {
            return (entourageStore.getAt(0)).get('proxyAccessFlag');
        } else {
            return null;
        }
    },

    getCampusCode: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.getAt(0) != null) {
            return (entourageStore.getAt(0)).get('campusCode');
        } else {
            return null;
        }
    },

    updateCampusCode: function(campusCode) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                campusCode: campusCode
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('campusCode', campusCode);
        }
        entourageStore.sync();
    },

    getDefaultCampus: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (defaultCampus == undefined || defaultCampus == 'undefined' || defaultCampus == '' || defaultCampus == '${defaultCampus}')
            defaultCampus = null;
        if (entourageStore.getAt(0) != null) {
            var defaultCampusCode = (entourageStore.getAt(0)).get('defaultCampus');
            if (defaultCampusCode == null || defaultCampusCode == undefined)
                return defaultCampus;
            return (entourageStore.getAt(0)).get('defaultCampus');
        } else {
            return defaultCampus;
        }
    },

    updateDefaultCampus: function(defaultCampus) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                defaultCampus: defaultCampus
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('defaultCampus', defaultCampus);
        }
        entourageStore.sync();
    },

    getDefaultMapView: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.getAt(0) != null) {
            return (entourageStore.getAt(0)).get('defaultMapView');
        } else {
            return null;
        }
    },

    updateDefaultMapView: function(defaultMapView) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                defaultMapView: defaultMapView
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('defaultMapView', defaultMapView);
        }
        entourageStore.sync();
    },
    updateSingupStatus: function(signUpReq) {
        var signUpLocalStore = mobEdu.util.getStore('mobEdu.main.store.signUpLocal');
        if (signUpLocalStore.data.length == 0) {
            signUpLocalStore.add({
                signUpReq: 'N',
                count: 1
            });
        } else {
            var rec = signUpLocalStore.getAt(0);
            rec.set('signUpReq', signUpReq);
            rec.set('count', rec.count + 1)
        }
        signUpLocalStore.sync();
    },
    getSingupStatus: function() {
        var signUpLocalStore = mobEdu.util.getStore('mobEdu.main.store.signUpLocal');
        if (signUpLocalStore.getAt(0) != null) {
            return (signUpLocalStore.getAt(0)).get('signUpReq');
        } else {
            return null;
        }
    },
    getProxyUserId: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.proxyUserId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.getAt(0) != null) {
                return (entourageStore.getAt(0)).get('proxyUserId');
            } else {
                return null;
            }
        }
    },

    updateProxyUserId: function(proxyUserId) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.proxyUserId = proxyUserId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    proxyUserId: proxyUserId
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('proxyUserId', proxyUserId);
            }
            entourageStore.sync();
        }
    },

    getProxyLdapId: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.proxyLdapId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.getAt(0) != null) {
                return (entourageStore.getAt(0)).get('proxyLdapId');
            } else {
                return null;
            }
        }
    },

    updateProxyLdapId: function(proxyLdapId) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.proxyLdapId = proxyLdapId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    proxyLdapId: proxyLdapId
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('proxyLdapId', proxyLdapId);
            }
            entourageStore.sync();
        }
    },

    updateAccTermDescription: function(termDescription) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                accTermDescription: termDescription
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('accTermDescription', termDescription);
        }
        entourageStore.sync();
    },

    updateAccTermCode: function(termCode) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                accTermCode: termCode
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('accTermCode', termCode);
        }
        entourageStore.sync();
    },

    updateCartTermDescription: function(termDescription) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                cartTermDescription: termDescription
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('cartTermDescription', termDescription);
        }
        entourageStore.sync();
    },
    updateCartTermCode: function(termCode) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                cartTermCode: termCode
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('cartTermCode', termCode);
        }
        entourageStore.sync();
    },

    updateCoursesTermDescription: function(termDescription) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                coursesTermDescription: termDescription
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('coursesTermDescription', termDescription);
        }
        entourageStore.sync();
    },

    updateCoursesTermCode: function(termCode) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                coursesTermCode: termCode
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('coursesTermCode', termCode);
        }
        entourageStore.sync();
    },

    updateCurrentTermCode: function(termCode) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                currentTermCode: termCode
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('currentTermCode', termCode);
        }
        entourageStore.sync();
    },
    updateCurrentTermDescription: function(termDescription) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                currentTermDescription: termDescription
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('currentTermDescription', termDescription);
        }
        entourageStore.sync();
    },

    updateSummaryFlag: function(flag) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                summaryFlag: flag
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('summaryFlag', flag);
        }
        entourageStore.sync();
    },
    updateDetailsFlag: function(flag) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                detailsFlag: flag
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('detailsFlag', flag);
        }
        entourageStore.sync();
    },

    updateModules: function(modules) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                modules: modules
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('modules', modules);
        }
        entourageStore.sync();
    },
    checkInfo: function(field) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        var rec = entourageStore.getAt(0);
        var fieldValue = rec.get(field);
        if (fieldValue == null || fieldValue == '') {
            return false;
        }
        return true;
    },
    resizeText: function(text, size) {
        // var count = 0;
        // if (text != null) {
        //  count = text.length;
        // }
        // if (count > size) {
        //  text = text.substr(0, size) + '...';
        // }
        return text;
    },
    loadCurrentTerm: function() {
        var store = mobEdu.util.getStore('mobEdu.main.store.term');
        store.getProxy().setHeaders({
            Authorization: mobEdu.util.getAuthString(),
            companyID: companyId
        });
        store.getProxy().setUrl(webserver + 'getCurrentTerm');

        store.getProxy().afterRequest = function() {
            return mobEdu.util.currentTermResponseHandler();
        }
        store.load();
    },
    currentTermResponseHandler: function() {
        var store = mobEdu.util.getStore('mobEdu.main.store.term');
        var rec = store.getAt(0);
        if (rec.data.errMsg == null || rec.data.errMsg == '') {
            mobEdu.util.updateCurrentTermCode(rec.data.termCode);
            mobEdu.util.updateCurrentTermDescription(rec.data.description);
            if (!(mobEdu.util.checkInfo('cartTermDescription'))) {
                mobEdu.util.updateCartTermDescription(rec.data.description);
            }
            if (!(mobEdu.util.checkInfo('cartTermCode'))) {
                mobEdu.util.updateCartTermCode(rec.data.termCode);
            }
            //            if (!(mobEdu.util.checkInfo('accTermDescription'))) {
            //                mobEdu.util.updateAccTermDescription(rec.data.description);
            //            }
            //            if (!(mobEdu.util.checkInfo('accTermCode'))) {
            //                mobEdu.util.updateAccTermCode(rec.data.termCode);
            //            }
            if (!(mobEdu.util.checkInfo('coursesTermDescription'))) {
                mobEdu.util.updateCoursesTermDescription(rec.data.description);
            }
            if (!(mobEdu.util.checkInfo('coursesTermCode'))) {
                mobEdu.util.updateCoursesTermCode(rec.data.termCode);
            }
        }
        return store;
    },

    loadAccCurrentTerm: function() {
        var store = mobEdu.util.getStore('mobEdu.main.store.term');
        store.getProxy().setHeaders({
            Authorization: mobEdu.util.getAuthString(),
            companyID: companyId
        });
        store.getProxy().setUrl(webserver + 'getBursarCurrentTerm');
        store.getProxy().setExtraParams({
            studentId: mobEdu.main.f.getStudentId()
        });
        store.getProxy().afterRequest = function() {
            return mobEdu.util.accCurrentTermResponseHandler();
        }
        store.load();
    },
    accCurrentTermResponseHandler: function() {
        var store = mobEdu.util.getStore('mobEdu.main.store.term');
        var rec = store.getAt(0);
        if (rec.data.errMsg == null || rec.data.errMsg == '') {
            if (!(mobEdu.util.checkInfo('accTermDescription'))) {
                mobEdu.util.updateAccTermDescription(rec.data.description);
            }
            if (!(mobEdu.util.checkInfo('accTermCode'))) {
                mobEdu.util.updateAccTermCode(rec.data.termCode);
            }
        }
        return store;
    },

    loadRegTermList: function(flag) {
        var tStore = mobEdu.util.getStore('mobEdu.main.store.termList');
        if (tStore.getCount() > 0) {
            tStore.removeAll();
            tStore.sync();
            tStore.removed = [];

        }
        tStore.getProxy().setUrl(webserver + 'getRegistrationTerms');
        tStore.getProxy().setHeaders({
            Authorization: mobEdu.util.getAuthString(),
            companyID: companyId
        });
        tStore.getProxy().afterRequest = function() {
            // flag:0-Request received from reg module
            // flag:1-Request received from courses module
            mobEdu.util.regTermListResponseHandler(flag);
        }
        tStore.load();
    },

    regTermListResponseHandler: function(flag) {
        // flag:0-Request received from reg module
        // flag:1-Request received from courses module
        if (Ext.os.is.Phone) {
            if (flag == 0)
                mobEdu.reg.f.showTermList();
            else
                mobEdu.courses.f.showTermList();
        } else {
            if (flag == 0)
                mobEdu.reg.f.showTermListPopup();
            else
                mobEdu.courses.f.showTermListPopup();
        }
    },
    doLoginCheck: function(loginFlag, viewRef, code, image, id, category, subCategory, position) {
        if (mobEdu.util.getPrevView() == null) {
            mobEdu.util.parentModule = code;
        } else {
            mobEdu.util.currentModule = code;
        }
        if (iconColorChangeOnTap == 'yes') {
            if (image != null && id != null) {
                document.getElementById(id).removeAttribute("src");
                document.getElementById(id).setAttribute('src', mobEdu.util.getResourcePath() + 'images/pressedmenu/' + image);

                if (category == null || category == '') {
                    mobEdu.util.menuGroupIconNumber = id;
                    mobEdu.util.menuGroupImageName = image;
                    mobEdu.util.menuIconNumber = null;
                    mobEdu.util.menuImageName = null;
                } else {
                    mobEdu.util.menuIconNumber = id;
                    mobEdu.util.menuImageName = image;

                }

            }
            mobEdu.util.changeColor(loginFlag, viewRef, code, category, subCategory, position);
            if (mainMenuLayout != 'SGRID') {
                mobEdu.util.doLoginAction(loginFlag, viewRef, code, category, subCategory, position);
            }
        } else {
            mobEdu.util.doLoginAction(loginFlag, viewRef, code, category, position);
        }
    },

    doLoginAction: function(loginFlag, viewRef, code, category, subCategory, position) {
        var modulesStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
        mobEdu.util.setLoginModuleCode(code);
        modulesStore.clearFilter();
        var rec = modulesStore.findRecord('code', code);
        if (category == null || category == '') {
            mobEdu.main.f.category = code;
            mobEdu.main.f.categoryDesc = rec.get('description');
            modulesStore.filter('category', code);
        } else {
            if (position === 'SUBMENU') {
                mobEdu.main.f.category = code;
                mobEdu.main.f.categoryDesc = rec.get('description');
                modulesStore.filter('subCategory', code);
            } else {
                if (subCategory == undefined)
                    mobEdu.main.f.category = category;
                else
                    mobEdu.main.f.category = subCategory;
                modulesStore.filter('category', category);
            }
        }

        if (loginFlag && mobEdu.util.getStudentId() == null && mobEdu.util.getAuthString() == null) {
            if (showBioInfo !== true) {
                mobEdu.util.updateNextView(viewRef);
            }
            if (position === 'SUBMENU') {
                mobEdu.main.f.subMenuCategory = true;
            }
            // mobEdu.main.f.category=code;
            mobEdu.main.f.showLogin();
        } else if (loginFlag && mobEdu.util.getBioInfoCheck() === true) {
            // mobEdu.main.f.category=code;
            mobEdu.profile.f.loadBioInfo();
        } else {
            // mobEdu.main.f.category=code;
            if (position === 'SUBMENU') {
                mobEdu.main.f.loadSubMenu(code, category);
            } else {
                if (studentInfoChecking == 'true') {
                    var studentDetailsStore = mobEdu.util.getStore('mobEdu.main.store.studentInformation');
                    var studentData = studentDetailsStore.data.all;
                    if (studentData.length == 0) {
                        mobEdu.main.f.showStudentDetailsView();
                    } else if (code == 'FAVS') {
                        mobEdu.main.f.loadUserFavourites();
                    } else {
                        eval(viewRef + '(\'' + code + '\')');
                    }
                } else if (code == 'FAVS') {
                    mobEdu.main.f.loadUserFavourites();
                } else {
                    eval(viewRef + '(\'' + code + '\')');
                }
            }
        }
    },

    getStudentId: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.studentId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length > 0) {
                var rec = entourageStore.getAt(0);
                var studentId = rec.data.studentId;
                return studentId;
            }
            return null;
        }
    },

    getModuleCode: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.code;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length > 0) {
                var rec = entourageStore.getAt(0);
                var code = rec.data.code;
                return code;
            }
            return null;
        }
    },

    getStudentLdapId: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.studentLdapId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length > 0) {
                var rec = entourageStore.getAt(0);
                var studentLdapId = rec.data.studentLdapId;
                return studentLdapId;
            }
            return null;
        }
    },

    getRecruiterId: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var recruiterId = rec.data.recruiterId;
            return recruiterId;
        }
        return null;
    },

    getRole: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var role = rec.data.role;
            return role;
        }
        return null;
    },

    getLeadId: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var leadId = rec.data.leadId;
            return leadId;
        }
        return null;
    },

    getRoleId: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var roleId = rec.data.roleId;
            return roleId;
        }
        return null;
    },

    getCompanyId: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var companyId = rec.data.companyId;
            return companyId;
        }
        return null;
    },

    getPrivileges: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var privileges = rec.data.privileges;
            return privileges;
        }
        return null;
    },

    getAuthString: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.authString;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length > 0) {
                var rec = entourageStore.getAt(0);
                var authString = rec.data.authString;
                return authString;
            }
            return null;
        }
    },

    getLeadAuthString: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var leadAuthString = rec.data.leadAuthString;
            return leadAuthString;
        }
        return null;
    },

    getRecruiterAuthString: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var recruiterAuthString = rec.data.recruiterAuthString;
            return recruiterAuthString;
        }
        return null;
    },

    getAboutText: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var aboutText = rec.data.aboutUs;
            return aboutText;
        }
        return null;
    },

    updateAboutText: function(aboutText) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                aboutUs: aboutText
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('aboutUs', aboutText);
        }
        entourageStore.sync();
    },

    updateStudentId: function(studentId) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.studentId = studentId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    studentId: studentId
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('studentId', studentId);
            }
            entourageStore.sync();
        }
    },

    updateModuleCode: function(code) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.code = code;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    code: code
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('code', code);
            }
            entourageStore.sync();
        }
    },

    updateStudentLdapId: function(studentLdapId) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.studentLdapId = studentLdapId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    studentLdapId: studentLdapId
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('studentLdapId', studentLdapId);
            }
            entourageStore.sync();
        }
    },

    updateRecruiterId: function(recruiterId) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                recruiterId: recruiterId
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('recruiterId', recruiterId);
        }
        entourageStore.sync();
    },

    updateLeadId: function(leadId) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                leadId: leadId
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('leadId', leadId);
        }
        entourageStore.sync();
    },

    updateRole: function(role) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                role: role
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('role', role);
        }
        entourageStore.sync();
    },

    updateRoleId: function(roleId) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                roleId: roleId
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('roleId', roleId);
        }
        entourageStore.sync();
    },
    updateCompanyId: function(companyId) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                companyId: companyId
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('companyId', companyId);
        }
        entourageStore.sync();
    },

    updatePrivileges: function(privileges) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                privileges: privileges
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('privileges', privileges);
        }
        entourageStore.sync();
    },

    updateAuthString: function(authString) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.authString = authString;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    authString: authString
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('authString', authString);
            }
            entourageStore.sync();
        }
    },
    updateLeadAuthString: function(leadAuthString) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                leadAuthString: leadAuthString
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('leadAuthString', leadAuthString);
        }
        entourageStore.sync();
    },

    updateRecruiterAuthString: function(recruiterAuthString) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                recruiterAuthString: recruiterAuthString
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('recruiterAuthString', recruiterAuthString);
        }
        entourageStore.sync();
    },
    setLoginModuleCode: function(code) {
        mobEdu.util.loginModuleCode = code;
    },
    updateNextView: function(nextView) {

        if ((typeof nextView) == "string") {
            nextView = eval(nextView);
        }

        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                nextView: nextView
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('nextView', nextView);
        }
        entourageStore.sync();
    },
    updatePrevView: function(prevView) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                prevView: prevView
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('prevView', prevView);
        }
        entourageStore.sync();
    },

    updateResourcePath: function(resourcesPath) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                resourcesPath: resourcesPath
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('resourcesPath', resourcesPath);
        }
        entourageStore.sync();
    },

    updateBioInfoCheck: function(value) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                bioInfoCheck: value
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('bioInfoCheck', value);
        }
        entourageStore.sync();
    },

    getBioInfoCheck: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var bioInfoCheck = rec.data.bioInfoCheck;
            return bioInfoCheck;
        }
        return false;
    },

    getResourcePath: function() {

        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        var resourcePath = null;
        if (entourageStore.data.length > 0) {
            resourcePath = (entourageStore.getAt(0)).get('resourcesPath');
        }

        if (resourcePath == null || resourcePath == '') {
            resourcePath = resourcesPath;
        } else {
            resourcePath = resourcePath;
        }

        return resourcePath;
    },

    getSchoolName: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var schoolName = rec.data.schoolName;
            return schoolName;
        }
        return null;
    },

    updateSchoolName: function(schoolName) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                schoolName: schoolName
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('schoolName', schoolName);
        }
        entourageStore.sync();
    },

    getSchoolCode: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var schoolCode = rec.data.schoolCode;
            return schoolCode;
        }
        return null;
    },

    updateSchoolCode: function(schoolCode) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                schoolCode: schoolCode
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('schoolCode', schoolCode);
        }
        entourageStore.sync();
    },

    getTitleInfo: function(titleName) {
        var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
        var modulesData = modulesLocalStore.data.all;
        for (var t = 0; t < modulesData.length; t++) {
            var titleCode = modulesData[t].data.code;
            if (titleCode == titleName) {
                var titleDesc = modulesData[t].data.description;
                return titleDesc;
            }
        }
    },
    sessionTimeoutHandler: function() {
        if (isSessionTimeoutEnabled === 'true') {
            if (mobEdu.util.getAuthString() !== null && mobEdu.util.getStudentId() !== null && task !== null) {
                task.cancel();
                task.delay(sessionTimeout * 60 * 1000);
            }
        }
    },
    getLoginModuleCode: function() {
        return mobEdu.util.loginModuleCode;
    },
    getNextView: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('nextView');
    },
    getPrevView: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        return (entourageStore.getAt(0)).get('prevView');
        mobEdu.util.sessionTimeoutHandler();
    },
    get: function(viewRef) {
        var ref = this.viewMap.get(viewRef);
        if (ref == undefined) {
            ref = Ext.create(viewRef);

            if (mainMenuLayout === 'BGRID' || mainMenuLayout === 'SGRID') {
                if (ref.config.floating != true) {
                    var container = Ext.create('Ext.Container', {
                        scroll: 'vertical',
                        fullscreen: true,
                        layout: 'fit',
                        cls: 'custombackground',
                        items: [{
                            xtype: 'container',
                            layout: 'fit',
                            cls: 'bevelcontainer',
                            id: 'bevelContainer'
                        }]
                    });
                    container.getAt(0).add([ref]);
                    if (viewRef != 'mobEdu.enroute.leads.view.paging') {
                        var dockeditems = ref.getDockedItems();
                        for (i = 0; i < dockeditems.length; i++) {
                            if (dockeditems[i].config.bevelEscape === undefined && (dockeditems[i].config.xtype === 'toolbar' || dockeditems[i].config.xtype === 'customToolbar')) {
                                container.add(Ext.clone(dockeditems[i]));
                            }
                        }
                    }

                    ref = container;
                }
            }
            this.viewMap.add(viewRef, ref);
        }
        return ref;
    },
    destroyView: function(viewRef) {
        var ref = this.viewMap.get(viewRef);
        if (ref != undefined) {
            Ext.Viewport.remove(ref, true);
            this.viewMap.remove(ref);
        }
    },
    getStore: function(storeRef) {
        var ref = this.storeMap.get(storeRef);
        if (ref == undefined) {
            ref = Ext.create(storeRef);
            this.storeMap.add(storeRef, ref);
        }
        return ref;
    },
    show: function(viewRef) {
        if (mainMenuLayout === "AP") {

            if (mobEdu.util.slideMenuCmp === null) {
                mobEdu.util.slideMenuCmp = Ext.create('Ext.Panel', {
                    scrollable: false,
                    width: mobEdu.util.maxDrag,
                    height: '100%',
                    style: 'position: absolute; left:0;',
                    cls: 'apmenu',
                    items: [{
                        xtype: 'searchfield',
                        placeHolder: 'Search',
                        id: 'menuSearch',
                        docked: 'top',
                        cls: 'mainmenusearch',
                        listeners: {
                            keyup: function(field) {
                                mobEdu.main.f.onSearchKeyUp(field);
                            },
                            clearicontap: function() {
                                mobEdu.main.f.onSearchClearIconTap();
                            }
                        }
                    }, {
                        xtype: 'toolbar',
                        cls: 'apMenuBar',
                        width: '100%',
                        id: 'menuTitle',
                        docked: 'top'
                    }, {
                        xtype: 'list',
                        id: 'apMenuList',
                        width: mobEdu.util.maxDrag,
                        itemTpl: new Ext.XTemplate('<div style="display: inline-block;"><img src="' + mobEdu.util.getResourcePath() + 'images/apMenu/{icon}" width=16 height=16 /></div><div class="mainModules">{description}</div><div style="display: inline-block; float:right;">' + '<tpl if="(authRequired===true)">' + '<img src="' + mobEdu.util.getResourcePath() + 'images/menu/lock_basic_blue.png" width="16" />' + '<tpl else>' + '<img width="15px" height="15px" src="' + mobEdu.util.getResourcePath() + 'images/arrow.png"/>' + '</tpl></div>'),
                        docked: 'left',
                        disableSelection: true,
                        scrollable: 'vertical',
                        margin: 0,
                        padding: 0,
                        store: mobEdu.util.getStore('mobEdu.main.store.slidingMenu'),
                        listeners: {
                            itemtap: function itemtap(me, index, target, record, e, eOpts) {
                                if (closeSlidingMenuOnItemTap != 'true') {
                                    mobEdu.util.toggleSlidingMenu();
                                }
                                mobEdu.util.doLoginCheck(record.get('authRequired'), record.get('action'), record.get('code'));
                            }
                        }
                    }],
                    listeners: {
                        painted: function() {
                            var task = new Ext.util.DelayedTask(function() {
                                Ext.getCmp('menuSearch').enable();
                            });
                            if (Ext.os.is.Phone) {
                                task.delay(500);
                            } else {
                                task.delay(300);
                            }
                        }
                    }
                });
            }

            var firstName = mobEdu.util.getFirstName();
            if (firstName !== undefined && firstName !== null) {
                Ext.getCmp('menuTitle').setTitle('<h1>' + firstName + '</h1>');
            }
            var activeViewRef = mobEdu.util.get(viewRef);

            activeViewRef.setStyle('width: 100%; height: 100%; position: absolute; opacity: 1; z-index: 1; background: rgb(238,238,238);');
            activeViewRef.setMinWidth('100%');

            activeViewRef.setDraggable({
                direction: 'horizontal',
                constraint: {
                    min: {
                        x: 0,
                        y: 0
                    },
                    max: {
                        x: mobEdu.util.maxDrag,
                        y: 0
                    }
                },
                listeners: {
                    dragstart: {
                        fn: function(ele, e, offsetX, offsetY, eOpts) {
                            if (e.x > offsetX + mobEdu.util.draggableWidth) {
                                return false;
                            }
                        },
                        order: 'before'
                    },
                    dragend: mobEdu.util.onContainerDragend
                }
            });

            var draggable = activeViewRef.draggableBehavior.draggable;
            if (viewRef != "mobEdu.main.view.dashboard") {
                draggable.setOffset(0, 0);
            }

            var tempContainer = Ext.create('Ext.Container', {
                layout: 'hbox',
                listeners: {
                    show: function() {
                        this.add([
                            mobEdu.util.slideMenuCmp,
                            activeViewRef
                        ]);
                    }
                }
            });

            Ext.Viewport.add(tempContainer);
            Ext.Viewport.setActiveItem(tempContainer);
            if (mainMenuSearchDisabled == 'true') {
                Ext.getCmp('menuSearch').hide();
                Ext.getCmp('menuTitle').show();
            } else {
                Ext.getCmp('menuSearch').show();
                Ext.getCmp('menuTitle').hide();
            }
        } else {
            if (viewRef == 'mobEdu.main.view.menu') {
                this.viewStack = [];
                this.viewStack.push(viewRef);
                Ext.Viewport.animateActiveItem(mobEdu.util.get(viewRef), {
                    type: 'slide',
                    direction: 'right'
                });
            } else {
                var firstPoppedItem = this.viewStack.pop();
                var secondPoppedItem = this.viewStack.pop();
                if (viewRef == secondPoppedItem) {
                    this.viewStack.push(secondPoppedItem);
                    Ext.Viewport.animateActiveItem(mobEdu.util.get(viewRef), {
                        type: 'slide',
                        direction: 'right'
                    });
                } else {
                    this.viewStack.push(secondPoppedItem);
                    this.viewStack.push(firstPoppedItem);
                    this.viewStack.push(viewRef);
                    Ext.Viewport.animateActiveItem(mobEdu.util.get(viewRef), {
                        type: 'slide',
                        direction: 'left'
                    });
                }
            }
        }
    },
    destroyAllViews: function() {
        var keys = mobEdu.util.viewMap.getKeys();
        for (i = 0; i < keys.length; i++) {
            if (keys[i] != 'mobEdu.main.view.preMenu') {
                mobEdu.util.destroyView(keys[i]);
            }
        }
    },

    showMainView: function() {

        if (mainMenuLayout === "AP") {
            if (mobEdu.util.slideMenuCmp != null) {
                mobEdu.util.slideMenuCmp.getComponent('menuSearch').disable();
            }
            mobEdu.util.show('mobEdu.main.view.dashboard');
        } else if (mainMenuLayout === "BGRID") {
            if (mobEdu.util.isHomeTapped == true) {
                mobEdu.util.updatePrevView(null);
                mobEdu.util.show('mobEdu.main.view.preMenu');
                mobEdu.util.isHomeTapped = false;
                setTimeout(mobEdu.util.destroyAllViews, 500);
            } else if (mobEdu.main.f.subMenuCategory == false) {
                mobEdu.util.updatePrevView(mobEdu.main.f.displayPreMenu);
                mobEdu.main.f.updateBevelGridMenu();
                mobEdu.util.show('mobEdu.main.view.bevelmenu');
            } else {
                mobEdu.util.updatePrevView(mobEdu.main.f.displayBevelGridMenu);
                mobEdu.main.f.loadSubMenu(mobEdu.main.f.category);
                // mobEdu.util.show('mobEdu.main.view.subMenu');
            }
            if (Ext.os.is.Android && Ext.os.deviceType === 'Phone' && Ext.os.version.major > 3 && Ext.os.version.minor > 3) {
                // if (showLoginName == 'true' && mobEdu.util.isLogin == true) {
                //  Ext.getCmp('bevelLogoCss').setHtml('<div class="x-center customcenter"><div class="divpositioncss"><div class="logoinnerhomelog"></div><div class="logocirclelog"></div></div></div>');
                // } else {
                //  Ext.getCmp('bevelLogoCss').setHtml('<div class="x-center customcenter"><div class="divpositioncss1"><div class="logoinnerhome"></div><div class="logocircle"></div></div></div>');
                // }

            } else {
                // if (showLoginName == 'true' && mobEdu.util.isLogin == true) {
                //  Ext.getCmp('bevelLogoCss').setHtml('<div class="x-center custcenter"><div class="divpositioncss"><div class="logoinnerhomelog"></div><div class="logocirclelog"></div></div></div>');
                // } else {
                //  Ext.getCmp('bevelLogoCss').setHtml('<div class="x-center custcenter"><div class="divpositioncss"><div class="logoinnerhome"></div><div class="logocircle"></div></div></div>');
                // }
            }

        } else if (mainMenuLayout === "SGRID") {
            // mobEdu.util.show('mobEdu.main.view.smenu');
            if (mobEdu.util.getStudentId() != null && mobEdu.util.getAuthString() != null) {
                mobEdu.util.show('mobEdu.main.view.smenu');
                var width = Ext.os.is.Phone ? 150 : 300;
                var height = Ext.os.is.Phone ? 20 : 40;
                // Ext.getCmp('mainTopTb').setTitle('<h1><img src="' + mobEdu.util.getResourcePath() + 'images/ungTitle.png" height="' + height + 'px" width="' + width + 'px"  /></h1>');
            } else {
                mobEdu.util.updatePrevView(null);
                mobEdu.util.show('mobEdu.main.view.preMenu');
            }
        } else {
            mobEdu.util.show('mobEdu.main.view.menu');
        }
        mobEdu.util.sessionTimeoutHandler();
    },
    showEnquireMainView: function() {
        mobEdu.util.updatePrevView(null);
        mobEdu.enquire.f.showLeadMainView();
        mobEdu.util.sessionTimeoutHandler();
    },
    toggleSlidingMenu: function() {
        // The active item in the view port is always our temp container
        // with 2 items
        // [0] Sliding menu
        // [1] Actual view (which we want to drag now)
        var activeViewRef = Ext.Viewport.getActiveItem().getItems().items[1];

        activeViewRef.setDraggable({
            direction: 'horizontal',
            constraint: {
                min: {
                    x: 0,
                    y: 0
                },
                max: {
                    x: mobEdu.util.maxDrag,
                    y: 0
                }
            },
            listeners: {
                dragstart: {
                    fn: function(ele, e, offsetX, offsetY, eOpts) {
                        if (e.x > offsetX + mobEdu.util.draggableWidth) {
                            return false;
                        }
                    },
                    order: 'before'
                },
                dragend: mobEdu.util.onContainerDragend
            }
        });

        var draggable = activeViewRef.draggableBehavior.draggable;

        if (draggable.offset.x == mobEdu.util.maxDrag) {
            draggable.setOffset(0, 0, {
                duration: mobEdu.util.dragDuration
            });
        } else {
            draggable.setOffset(mobEdu.util.maxDrag, 0, {
                duration: mobEdu.util.dragDuration
            });
        }
    },
    onContainerDragend: function(draggable, e, eOpts) {
        var velocity = Math.abs(e.deltaX / e.deltaTime),
            listPosition = 'left',
            direction = (e.deltaX > 0) ? "right" : "left",
            offset = Ext.clone(draggable.offset),
            threshold = parseInt(mobEdu.util.maxDrag * .70);

        switch (direction) {
            case "right":
                offset.x = (velocity > 0.75 || offset.x > threshold) ? mobEdu.util.maxDrag : 0;
                break;
            case "left":
                offset.x = (velocity > 0.75 || offset.x < threshold) ? 0 : mobEdu.util.maxDrag;
                break;
        }

        draggable.setOffset(offset.x, 0, {
            duration: mobEdu.util.dragDuration
        });
    },
    showMenu: function() {
        // If the layout type is set to AP, show the sliding menu on Home button
        // Otherwise just take it to home screen
        if (mainMenuLayout === "AP") {
            mobEdu.util.toggleSlidingMenu();
            Ext.getCmp('menuSearch').disable();
            var task = new Ext.util.DelayedTask(function() {
                Ext.getCmp('menuSearch').enable();
            });
            if (Ext.os.is.Phone) {
                task.delay(500);
            } else {
                task.delay(300);
            }
        } else {
            mobEdu.util.isHomeTapped = true;
            mobEdu.util.showMainView();
        }
    },
    showRecruiterMainView: function() {
        mobEdu.util.updatePrevView(null);
        mobEdu.util.show('mobEdu.enroute.main.view.menu');
    },
    deselectSelectedItem: function(selectedItemIndex, viewRef) {
        setTimeout(function() {
            viewRef.deselect(selectedItemIndex);
        }, 500);
    },

    sortDates: function(dates, order) {
        if (order == null || order == '')
            order = 'asc';

        var date_sort_asc = function(date1, date2) {
            if (date1 > date2)
                return 1;
            if (date1 < date2)
                return -1;
            return 0;
        };

        var date_sort_desc = function(date1, date2) {
            if (date1 > date2)
                return -1;
            if (date1 < date2)
                return 1;
            return 0;
        };

        if (order == 'asc')
            dates.sort(date_sort_asc);
        else
            dates.sort(date_sort_desc);

        return dates;
    },

    updateTheme: function() {
        // Clear existing css files

        var cssFiles = document.getElementsByTagName('link');

        for (var i = cssFiles.length - 1; i >= 0; i--) {
            if (cssFiles[i].getAttribute('rel').indexOf('stylesheet') != -1) {
                cssFiles[i].parentNode.removeChild(cssFiles[i]);
            }
        }

        // Load css files again; the folder to be loaded will be determined from resourcePath
        mobEdu.util.loadClientFiles();
    },

    loadClientFiles: function() {
        // This method should load all required css files
        if (Ext.os.is.Phone)
            mobEdu.util.loadCssFile('phone.css');
        else if (Ext.os.is.Tablet)
            mobEdu.util.loadCssFile('tablet.css');
        else
            mobEdu.util.loadCssFile('tablet.css');

        mobEdu.util.loadCssFile('Ext.ux.TouchCalendarView.css');
        mobEdu.util.loadCssFile('Ext.ux.grid.View.css');
    },

    loadCssFile: function(filename) {
        filename = mobEdu.util.getResourcePath() + 'css/' + filename;
        var fileref = document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
        document.getElementsByTagName("head")[0].appendChild(fileref);
    },

    doEnrouteAdmisLoginCheck: function(viewRef, loginFlag) {
        if (admisflag == false) {
            //        if(mobEdu.util.getRecruiterId()==null && mobEdu.util.getRecruiterAuthString()==null &&
            //            mobEdu.util.getRole()==null){
            mobEdu.util.updateNextView(viewRef);
            mobEdu.enradms.login.f.showLogin();
        } else {
            eval(viewRef + '()');
            //            mobEdu.util.showAdmissionMainView();
        }
    },

    showAdmissionMainView: function() {
        mobEdu.util.updatePrevView(mobEdu.util.showMainView);
        mobEdu.util.show('mobEdu.enradms.main.view.menu');
    },

    loadExternalLinkForModule: function(moduleName) {
        var url = '';
        var feedsLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
        if (feedsLocalStore.data.length > 0) {
            var rec = feedsLocalStore.findRecord('name', moduleName);
            if (rec != null) {
                url = rec.get('link');
            }
        }
        if (url != null && url != '') {
            mobEdu.util.loadExternalUrl(url);
        }
    },

    loadExternalUrl: function(url, showLocation) {
        // If using file protocol (used inside a phone gap container)
        // use the childbrowser plugin
        // otherwise just open in another window
        // TODO - Make this better to work with safari on ios and also this wont work when
        //  phonegap container opens a client url directly (instead of client code in the container)
        if (document.location.protocol == "file:") {
            // For SPSU, they do not want to use childbrowser on IOS. So we use a setting
            // For Android, we can use the child browser
            if (url.substr(-4, 4) == ".pdf") {
                url = "https://docs.google.com/gview?embedded=true&url=" + url;
            }
            if (Ext.os.is.Android || doNotUseChildBrowser != 'true') {
                if (showLocation == null) {
                    // Do not show the location by default for android
                    // But show it for iOS, so that we have the DONE button to close the child browser
                    if (Ext.os.is.Android) {
                        showLocation = false;
                    } else {
                        showLocation = true;
                    }
                }
                // window.open('https://docs.google.com/gview?embedded=true&url=https://smcwired.swmich.edu/smc/mysmcspace/spops/Disability_Services_at_SMC.pdf', '_blank', 'location=yes,EnableViewPortScale=yes');
                window.open(url, '_blank', 'location=yes,EnableViewPortScale=yes');
            } else {
                window.open(url, '_system');
            }
        } else {
            window.open(url);
        }
    },

    getDirSearchDefault: function() {
        return dirSearchDefault;
    },

    convertRelativeToAbsoluteUrls: function(description, onlyUrls) {
        if (description == null || description == undefined || description == '') {
            return "";
        }
        description = description.replace('iframe', 'a');
        var div = document.createElement('div');
        div.innerHTML = description;
        var hrefList = div.getElementsByTagName('a');
        for (i = 0; i < hrefList.length; i++) {
            hrefEl = hrefList[i];
            oldHref = hrefEl.getAttribute('href');
            if (oldHref === null) {
                oldHref = hrefEl.getAttribute('src');
            }
            if (oldHref.indexOf("/") == 0 || oldHref.indexOf("http") != 0) {
                hrefEl.href = relativeUrlPrefix + oldHref;
            }
            if (onlyUrls) {
                hrefEl.href = this.convertPdfUrl(hrefEl.href);
            } else {
                hrefEl.href = 'javascript:mobEdu.util.loadExternalUrl("' + this.convertPdfUrl(hrefEl.href) + '");';
            }
        }
        return div.innerHTML;
    },

    formatUrls: function(description) {
        if (description == null || description == undefined || description == '') {
            return "";
        }
        var div = document.createElement('div');
        div.innerHTML = description;
        var hrefList = div.getElementsByTagName('a');
        for (i = 0; i < hrefList.length; i++) {
            hrefEl = hrefList[i];
            oldHref = hrefEl.getAttribute('href');
            hrefEl.href = 'javascript:mobEdu.util.loadExternalUrl("' + hrefEl.href + '");';
        }
        return div.innerHTML;
    },

    convertPdfUrl: function(url) {
        if (Ext.os.is.Android && url.lastIndexOf('.pdf') == (url.length - 4)) {
            return 'http://docs.google.com/viewer?url=' + url;
        }
        return url;
    },

    getAboutTitle: function() {
        if (aboutTitle == '' || aboutTitle == null || aboutTitle == undefined) {
            return 'About';
        }
        return aboutTitle;
    },

    getPhoneIconSize: function() {
        if (phoneIconSize != null && phoneIconSize != '' && Ext.isNumber(phoneIconSize) != true) {
            return phoneIconSize;
        }
        return '57';
    },
    getIconSize: function() {
        if (iconSize != null && iconSize != '' && Ext.isNumber(iconSize) != true) {
            return iconSize;
        }
        return '57';
    },
    isShowDefaultCampus: function() {
        if (isShowDefaultCampus == 'false') {
            return false;
        } else {
            return true;
        }
    },

    isShowMapSubMenu: function() {
        if (isShowMapSubMenu == 'true') {
            return true;
        } else {
            return false;
        }
    },

    isShowDirectoryCategory: function() {
        if (isShowDirectoryCategory == 'false') {
            return false;
        } else {
            return true;
        }
    },
    isShowSignUP: function() {
        var store = mobEdu.util.getStore('mobEdu.main.store.signUpLocal');
        if (store.data.length > 0) {
            var rec = store.getAt(0);
            var signupReq = rec.data.signUpReq;
            // var signUpStatus=store.
            if (campusCode == 'N2N' && signupReq != 'N') {
                return true;
            } else {
                return false;
            }
        }
        return true;
    },
    filterSpecialCharacters: function(content) {
        var str, result = '';
        str = JSON.stringify(content);

        for (i = 1; i < str.length - 1; i++) {
            ch = str.charCodeAt(i);
            if ((ch < 128 && ch != 92) || (ch == 92 && str.charCodeAt(i + 1) != 34)) {
                result += str.charAt(i);
            }
        }
        return result;
    },

    isFeedIdExists: function(feedId) {
        var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
        var feedsArray = new Array();
        var rec = encoreStore.getAt(0);
        if (rec != null && rec != '') {
            feedsArray = rec.data.hiddenFeeds;
            if (Ext.Array.contains(feedsArray, feedId)) {
                return true;
            }
        }
        return false;

    },

    addModuleToHiddenModules: function(code) {
        var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
        var modulesArray = new Array();
        if (encoreStore.data.length == 0) {
            modulesArray.push(code);
            encoreStore.add({
                hiddenModules: modulesArray
            });
            encoreStore.sync();
        } else {
            var rec = encoreStore.getAt(0);
            if (rec != null && rec != '') {
                modulesArray = rec.data.hiddenModules;
                if (!(Ext.Array.contains(modulesArray, code))) {
                    modulesArray.push(code);
                    rec.set('hiddenModules', modulesArray);
                    rec.setDirty();
                    encoreStore.sync();
                }
            }
        }
    },

    removeModuleFromHiddenModules: function(code) {
        var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
        var modulesArray = new Array();
        var rec = encoreStore.getAt(0);
        if (rec != null && rec != '') {
            modulesArray = rec.data.hiddenModules;
            if ((Ext.Array.contains(modulesArray, code))) {
                var index = modulesArray.indexOf(code);
                if (index != -1) {
                    modulesArray.splice(index, 1);
                    rec.set('hiddenModules', modulesArray);
                    rec.setDirty();
                    encoreStore.sync();
                }
            }
        }
    },

    isModuleExists: function(code) {
        var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
        var modulesArray = new Array();
        var rec = encoreStore.getAt(0);
        if (rec != null && rec != '') {
            modulesArray = rec.data.hiddenModules;
            if ((Ext.Array.contains(modulesArray, code))) {
                return true;
            }
        }
        return false;
    },

    addFeedToHiddenFeeds: function(feedId, cmp) {
        var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
        var feedsArray = new Array();
        if (encoreStore.data.length == 0) {
            if (mobEdu.util.isLastFeed(feedId) == true) {
                Ext.Msg.show({
                    title: null,
                    cls: 'msgbox',
                    message: '<center>Need to show atleast one category.</center>',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            cmp.refresh();
                        }
                    }
                });
            } else {
                feedsArray.push(feedId);
                encoreStore.add({
                    hiddenFeeds: feedsArray
                });
                encoreStore.sync();
            }
        } else {
            var rec = encoreStore.getAt(0);
            if (rec != null && rec != '') {
                feedsArray = rec.data.hiddenFeeds;
                if (mobEdu.util.isLastFeed(feedId, feedsArray) == true) {
                    Ext.Msg.show({
                        title: null,
                        cls: 'msgbox',
                        message: '<center>Need to show atleast one category.</center>',
                        buttons: Ext.MessageBox.OK,
                        fn: function(btn) {
                            if (btn == 'ok') {
                                cmp.refresh();
                            }
                        }
                    });
                } else {
                    if (!(Ext.Array.contains(feedsArray, feedId))) {
                        feedsArray.push(feedId);
                        rec.set('hiddenFeeds', feedsArray);
                        rec.setDirty();
                        encoreStore.sync();
                    }
                }
            }
        }
        cmp.refresh();
    },

    removeFeedFromHiddenFeeds: function(feedId, cmp) {
        var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
        var feedsArray = new Array();
        var rec = encoreStore.getAt(0);
        if (rec != null && rec != '') {
            feedsArray = rec.data.hiddenFeeds;
            if ((Ext.Array.contains(feedsArray, feedId))) {
                var index = feedsArray.indexOf(feedId);
                if (index != -1) {
                    feedsArray.splice(index, 1);
                    rec.set('hiddenFeeds', feedsArray);
                    rec.setDirty();
                    encoreStore.sync();
                }
            }
        }
        cmp.refresh();
    },

    isLastFeed: function(feedId, hiddenFeeds) {
        var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal');
        var rec = null;
        rec = feedLocalStore.findRecord('feedId', feedId);
        if (rec != null) {
            var storeRecords = new Array();
            var cnt = 0;
            if (hiddenFeeds != null) {
                feedLocalStore.each(function(record) {
                    storeRecords.push(record);
                    if (Ext.Array.contains(hiddenFeeds, record.data.feedId)) {
                        cnt = cnt + 1;
                    }
                });
            }
            if (storeRecords.length == cnt + 1) {
                return true;
            }
        }
        return false;
    },

    dateFormat: function(date) {
        var jobsDateFormat = Ext.DateExtras.format(new Date(Date.parse(date)), 'D <br>m/d');
        return jobsDateFormat;
    },

    displayDateFormat: function(date) {
        if (feedsDateFormat === null || feedsDateFormat === undefined) {
            return date;
        } else {
            date = Ext.DateExtras.format(new Date(Date.parse(date)), feedsDateFormat);
            return date;
        }
    },

    symbolConverter: function(address) {
        var address1 = address.replace(/;+/g, ";");
        var result = address1.replace(/;/g, "<br>");
        return result;
    },

    isValueExists: function(value) {
        if (value != null && value != '') {
            return true;
        }
        return false;
    },

    setTitle: function(code, cmp, title, parentID) {
        if (code != null && code != '') {
            var store = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var rec = store.findRecord('code', code);
            if (rec != null) {
                title = rec.get('description');
                cmp.setTitle('<h1>' + title + '</h1>');
            }
        } else if (parentID != null && parentID != '') {
            var store = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
            var rec = store.getAt(store.findExact('feedId', parentID));
            if (rec != null) {
                title = rec.get('name');
                cmp.setTitle('<h1>' + title + '</h1>');
            }
        } else if (title != null && title != '') {
            cmp.setTitle('<h1>' + title + '</h1>');
        }
    },

    hideKeyboard: function(callback, scope) {
        if (Ext.os.is.Android) {
            var activeElement = document.activeElement;
            activeElement.setAttribute('readonly', 'readonly'); // Force keyboard to hide on input field.
            activeElement.setAttribute('disabled', 'true'); // Force keyboard to hide on textarea field.
            Ext.defer(function() {
                activeElement.blur();
                // Remove readonly attribute after keyboard is hidden.
                activeElement.removeAttribute('readonly');
                activeElement.removeAttribute('disabled');
                if (callback) {
                    callback.call(scope);
                }
            }, 100);
        }
    },

    isShowThemes: function() {
        if (themes == 'yes')
            return true;
        return false;
    },
    gaTrackEventSuccess: function() {
        // Tracking success. Nothing to do.
    },
    gaTrackEventFailure: function() {
        console.log('GA: Error tracking event');
    },
    gaTrackPageView: function(pageview) {
        if (mobEdu.util.analyticsPlugin != null) {
            mobEdu.util.analyticsPlugin.trackPageView(pageview, mobEdu.util.gaTrackEventSuccess, mobEdu.util.gaTrackEventFailure);
        }
    },
    gaTrackEvent: function(category, eventName, label, value) {
        if (mobEdu.util.analyticsPlugin != null) {
            mobEdu.util.analyticsPlugin.trackEvent(category, eventName, label, value, mobEdu.util.gaTrackEventSuccess, mobEdu.util.gaTrackEventFailure);
        }
    },
    onOrientationChange: function(orientation, popupView, classRef) {
        mobEdu.util.adjustViewportIfIOS7();
        if (orientation == 'portrait') {
            if (Ext.os.is.Phone) {
                popupView.setWidth(classRef.phone.portrait.width);
                popupView.setHeight(classRef.phone.portrait.height);
            } else if (Ext.os.is.Tablet) {
                popupView.setWidth(classRef.tablet.portrait.width);
                popupView.setHeight(classRef.tablet.portrait.height);
            }
        } else {
            if (Ext.os.is.Phone) {
                popupView.setWidth(classRef.phone.landscape.width);
                popupView.setHeight(classRef.phone.landscape.height);
            } else if (Ext.os.is.Tablet) {
                popupView.setWidth(classRef.tablet.landscape.width);
                popupView.setHeight(classRef.tablet.landscape.height);
            }
        }
    },

    isDualRole: function() {
        var resRole = mobEdu.util.getRole();
        var count = 0;
        if (resRole != null) {
            var userRole = resRole.split(',');
            for (var len = 0; len < userRole.length; len++) {
                if (userRole[len] == 'FACULTY' || userRole[len] == 'STUDENT') {
                    count++;
                }
            }
        }
        if (count > 1)
            return true;
        return false;
    },

    getCurrentRole: function() {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length > 0) {
            var rec = entourageStore.getAt(0);
            var role = rec.data.currentRole;
            return role;
        }
        return null;
    },

    updateCurrentRole: function(role) {
        var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (entourageStore.data.length == 0) {
            entourageStore.add({
                currentRole: role
            });
        } else {
            var rec = entourageStore.getAt(0);
            rec.set('currentRole', role);
        }
        entourageStore.sync();
    },

    getModuleCodeById: function(moduleId) {
        var moduleStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
        if (moduleStore.data.length > 0) {
            var rec = moduleStore.findRecord('moduleId', moduleId);
            if (rec != null) {
                return rec.get('code');
            } else {
                return null;
            }
        } else {
            return null;
        }
    },

    getCurrentModuleIcon: function() {
        var rec = mobEdu.util.getModuleByCode(mobEdu.util.currentModule);
        var image = null;
        if (rec != null) {
            image = rec.data.icon;
        }
        return image;
    },

    getParentModuleIcon: function() {
        var rec = mobEdu.util.getModuleByCode(mobEdu.util.parentModule);
        var image = null;
        if (rec != null) {
            image = rec.data.icon;
        }
        return image;
    },

    getModuleByCode: function(code) {
        var modStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
        var rec = modStore.findRecord('code', code, null, null, null, true);
        return rec;
    },

    getModuleId: function(code) {
        var modStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
        var rec = modStore.findRecord('code', code, null, null, null, true);
        var id = null;
        if (rec != null) {
            id = rec.get('moduleId');
        }
        return id;
    },

    adjustViewportIfIOS7: function() {
        if (Ext.os.is.ios && Ext.os.version.major >= 7) {
            Ext.Viewport.setSize(Ext.Viewport.getSize().width, Ext.Viewport.getSize().height - 20);
        }
    },

    loadTask: function() {
        var reqTime = new Date().getTime();
        var lastReqTime = mobEdu.util.getLastRequestTime();
        if (lastReqTime == null || lastReqTime == '') {
            lastReqTime = reqTime;
        }
        if (reqTime - lastReqTime > (sessionTimeout * 60 * 1000)) {
            Ext.Msg.show({
                message: 'Session Expired... Please relogin',
                buttons: Ext.MessageBox.OK,
                cls: 'msgbox',
                fn: function(btn) {
                    if (btn == 'ok') {
                        mobEdu.main.f.logout();
                    }
                }
            });
        } else {
            mobEdu.util.updateLastRequestTime(reqTime);
            if (task != null) {
                task.cancel();
                task.delay(sessionTimeout * 60 * 1000);
            }
        }
    },

    formatCurrency: function(number, decimals, decimal_sep, thousands_sep) {
        var s = null;
        var i = null;
        var j = null;
        decimals = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals;
        decimal_sep = decimal_sep == undefined ? "." : decimal_sep;
        thousands_sep = thousands_sep == undefined ? "," : thousands_sep;
        s = number < 0 ? "-" : "";
        i = parseInt(number = Math.abs(+number || 0).toFixed(decimals)) + "";
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + thousands_sep : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep) + (decimals ? decimal_sep + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    },
    facSplit: function(value) {
        if (value === '' || value === undefined || value === null) {
            return '';
        } else {
            var fac = value;
            var facArray = new Array();
            facArray = fac.split(':');
            return facArray[0];
        }
    },
    insSplit: function(value) {
        var ins = '';
        if (value === '' || value === undefined || value === null) {
            return '';
        } else {
            var fac = value;
            var facArray = new Array();
            facArray = fac.split(',');
            for (var i = 0; i < facArray.length; i++) {
                var local = facArray[i].split(':');
                if (ins === '') {
                    ins = ins + local[0];
                } else {
                    ins = ins + ',' + local[0];
                }
            };
            return ins;
        }
    },
    toSplit: function(toValue) {
        var array = new Array();
        array = toValue;
        var result = '';
        for (var i = 0; i < array.length; i++) {
            if (result === '') {
                result = result + array[i].toName;
            } else {
                result = result + ',' + array[i].toName;
            }
        };
        return result;
    },
    participantSplit: function(toValue) {
        var array = new Array();
        array = toValue;
        var result = '';
        for (var i = 0; i < array.length; i++) {
            if (result === '') {
                result = result + array[i].userID;
            } else {
                result = result + ',' + array[i].userID;
            }
        };
        return result;
    },
    datSplit: function(value) {
        var dat = '';
        if (value === '' || value === undefined || value === null) {
            return '';
        } else {
            var res = value;
            resArray = res.split(' ');
            return resArray[0];
        }

    },
    isRecieved: function(value) {
        var store = mobEdu.util.getStore('mobEdu.main.store.entourage');
        if (store.data.length > 0) {
            var sid = store.first().data.studentId;
        };
        value = value.toLowerCase();
        sid = sid.toLowerCase();
        if (sid == value) {
            return false;
        } else {
            return true;
        }
    },
    isVisible: function(fromID, toArry, loginUserID) {
        var acceptance;
        if (fromID == loginUserID) {
            acceptance = fromDetails.acceptance;
        }

        if (acceptance == '' || acceptance == undefined || acceptance == null) {
            for (var i = 0; i < toArry.length; i++) {
                var toID = toArry[i].toID;
                if (toID == loginUserID) {
                    acceptance = toArry[i].acceptance;
                }
            }
        }

        if (acceptance == 'Y' || acceptance == 'N') {
            return false;
        } else {
            return true;
        }
    },

    createCustomToolbar: function() {
        if (mainMenuLayout === 'BGRID' || mainMenuLayout === 'SGRID') {
            var width = Ext.os.is.Phone ? 150 : 300;
            var height = Ext.os.is.Phone ? 20 : 40;
            Ext.define('mobEdu.main.view.customToolbar', {
                extend: 'Ext.Toolbar',
                xtype: 'customToolbar',
                config: {
                    docked: 'top',
                    cls: 'beveltoptoolbar',
                    layout: {
                        type: 'hbox',
                        pack: 'center'
                    },
                    currentIcon: null,

                    // listeners: {
                    //     painted: function(me) {
                    //         //if (mobEdu.util.currentModule != 'SETTINGS') {
                    //         var modStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
                    //         modStore.clearFilter();
                    //         //}

                    //         var icon = mobEdu.util.getCurrentModuleIcon();

                    //         if (mobEdu.util.getPrevView() == mobEdu.main.f.displayPreMenu) {
                    //             icon = mobEdu.util.getParentModuleIcon();
                    //         } else {
                    //             icon = mobEdu.util.getCurrentModuleIcon();
                    //         }
                    //         /*if (mobEdu.util.currentModule != 'SETTINGS') {
                    //             if (showPremenu == 'always')
                    //                 modStore.filter('category', mobEdu.main.f.category);
                    //         }*/
                    //         if (icon != me.currentIcon) {
                    //             me.dom.getElementsByClassName('logoinnercourse')[0].setAttribute('style', 'background-image: url(resources/images/menu/' + icon + ') !important;');
                    //             currentIcon = icon;
                    //         }
                    //     }
                    // },

                    items: [{
                        xtype: 'img',
                        cls: 'back',
                        align: 'left',
                        listeners: {
                            tap: function() {
                                (mobEdu.util.getPrevView())();
                            }
                        }
                    }, {
                        xtype: 'panel',
                        pack: 'center',
                        html: '<div class="x-center"><img class="titleWidth" src="' + mobEdu.util.getResourcePath() + 'images/wentworth-full.png" height="' + height + 'px" width="' + width + 'px" /></div>',
                        flex: 1
                    }, {
                        xtype: 'img',
                        cls: 'home',
                        align: 'right',
                        listeners: {
                            tap: function() {
                                // mobEdu.util.showMenu();
                                // mobEdu.util.isHomeTapped = true;
                                setTimeout(mobEdu.util.destroyAllViews, 500);
                                mobEdu.main.f.displayPreMenu(); // mobEdu.ut();
                                // if (mainMenuLayout == 'BGRID' && Ext.os.is.Phone) {                                  
                                //  mobEdu.main.f.displayPreMenu();
                                // }
                            }
                        }
                    }]
                }
            });
        } else {
            Ext.define('mobEdu.main.view.customToolbar', {
                extend: 'Ext.Toolbar',
                xtype: 'customToolbar',
                config: {
                    docked: 'top',
                    ui: 'light',
                    minHeight: '40px',
                    items: [{
                        xtype: 'img',
                        cls: 'home',
                        width: 32,
                        height: 32,
                        listeners: {
                            tap: function() {
                                mobEdu.util.showMenu();
                            }
                        }
                    }, {
                        xtype: 'img',
                        cls: 'back',
                        padding: '10 0 0 0',
                        width: 32,
                        height: 32,
                        listeners: {
                            tap: function() {
                                (mobEdu.util.getPrevView())();
                            }
                        }
                    }]
                }
            });
        }
    },

    loadEnquire: function() {
        window.open('enquire/index.html', '_self');
    },
    changeColor: function(loginFlag, viewRef, code, subCategory, category) {
        if (mobEdu.util.menuIconNumber != null && mobEdu.util.menuIconNumber != 0) {
            setTimeout(function() {
                document.getElementById(mobEdu.util.menuIconNumber).removeAttribute("src");
                document.getElementById(mobEdu.util.menuIconNumber).setAttribute('src', mobEdu.util.getResourcePath() + 'images/menu/' + mobEdu.util.menuImageName);

            }, 3000);

            if (mainMenuLayout == 'SGRID') {
                setTimeout(function() {
                    if (mainMenuLayout == 'SGRID') {
                        mobEdu.util.doLoginAction(loginFlag, viewRef, code);
                    }
                }, 1000);
            }
        }

        if (mobEdu.util.menuGroupIconNumber != null && mobEdu.util.menuGroupIconNumber != 0) {
            setTimeout(function() {
                document.getElementById(mobEdu.util.menuGroupIconNumber).removeAttribute("src");
                document.getElementById(mobEdu.util.menuGroupIconNumber).setAttribute('src', mobEdu.util.getResourcePath() + 'images/menu/' + mobEdu.util.menuGroupImageName);
            }, 3000);
        }
    },

    openURLWithCAS: function(link) {
        var url = '';
        var casStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
        var casData = casStore.data.all;
        for (var i = 0; i < casData.length; i++) {
            var code = casData[i].data.moduleCode;
            if (code === 'CAS') {
                var record = casData[i].data.feeds[0];
                casURL = record.link;
            }
        }
        Ext.Ajax.request({
            url: casURL + 'login',
            method: 'POST',
            useDefaultXhrHeader: false,
            headers: {
                "Accept": "application/html"
            },
            success: function(response) {
                mobEdu.util.openURLWithCASResponseHandler(response, link);
            },
            failure: function(response) {
                alert('cas authentication is failed');
            }
        });
    },

    openURLWithCASResponseHandler: function(response, link) {
        if (response.statusText == 'OK') {
            var data = response.responseText;
            var ltStart = data.indexOf("name=\"lt\" value=\"");
            if (ltStart == -1) {
                mobEdu.util.sessionCASValidated = true;
                mobEdu.util.loadExternalUrl(link);
            } else {
                var start = ltStart + 17;
                var ltEnd = data.indexOf("\"", start);
                var lt = data.substring(start, ltEnd);
                var eStart = data.indexOf("name=\"execution\" value=\"") + 24;
                var eEnd = data.indexOf("\"", eStart);
                var execution = data.substring(eStart, eEnd);
                mobEdu.util.validateCASSession(lt, execution, link);
            }
        } else {
            alert('cas authentication is failed');
        }
    },

    validateCASSession: function(lt, execution, link) {
        var authString = mobEdu.util.getAuthString();
        var authStrng = authString.split(' ');
        var encodedCred = authStrng[1];
        var decodedCred = Base64.decode(encodedCred);
        var cred = decodedCred.split(':');
        var userName = cred[0];
        var password = cred[1];
        mobEdu.util.sessionCASValidated = true;
        var casLoginURL = casURL + '?lt=' + lt + '&username=' + encodeURIComponent(userName) + '&password=' + encodeURIComponent(password) + '&execution=' + execution + '&_eventId=submit';
        Ext.Ajax.request({
            url: casLoginURL,
            method: 'POST',
            useDefaultXhrHeader: false,
            headers: {
                "Accept": "application/html"
            },
            success: function(response) {
                mobEdu.util.sessionCASValidated = true;
                mobEdu.util.loadExternalUrl(link);
            },
            failure: function(response) {
                alert('cas authentication is failed');
            }
        });

    },

    casLogout: function() {
        if (mobEdu.util.sessionCASValidated === true) {
            Ext.Ajax.request({
                url: casURL + 'logout',
                method: 'POST',
                useDefaultXhrHeader: false,
                headers: {
                    "Accept": "application/html"
                },
                success: function(response) {},
                failure: function(response) {
                    alert('cas authentication is failed');
                }
            });
            mobEdu.util.sessionCASValidated = false;
        }
    },


    prepareTargetUrl: function(lt, execution, link) {
        var authString = mobEdu.util.getAuthString();
        var authStrng = authString.split(' ');
        var encodedCred = authStrng[1];
        var decodedCred = Base64.decode(encodedCred);
        var cred = decodedCred.split(':');
        var userName = cred[0];
        var password = cred[1];
        mobEdu.util.sessionCASValidated = true;
        var targetURL = casURL + '?service=' + encodeURIComponent(link) + '&lt=' + lt + '&username=' + encodeURIComponent(userName) + '&password=' + encodeURIComponent(password) + '&execution=' + execution + '&_eventId=submit';
        mobEdu.util.loadExternalUrl(targetURL);
    },

    appSplit: function(val) {
        var toName = '';
        for (var i = 0; i < val.length; i++) {
            if (toName != '' && val[i].status != 'Cancelled') {
                toName = toName + ' , ';
            }
            if (val[i].status != 'Cancelled') {
                toName = toName + val[i].toName;
            }
        }

        return toName;
    },

    fromSplit: function(val) {
        var fromName = '';
        if (val.status != 'Cancelled') {
            fromName = fromName + val.fromName;
        }
        return fromName + ' , ';
    },

    getStatus: function(from, to, id) {
        var status = '';
        if (from.fromID == id) {
            status = from.status;
            return status;
        }
        for (var i = 0; i < to.length; i++) {
            if (to[i].toID == id) {
                status = to[i].status;
                return status;
            }
        }
        return status;
    },

    getApplicationId: function() {
        if (authStorageType == 'MEMORY') {
            return mobEdu.util.applicationId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length > 0) {
                var rec = entourageStore.getAt(0);
                var applicationId = rec.data.applicationId;
                return applicationId;
            }
            return null;
        }
    },

    updateApplicationId: function(applicationId) {
        if (authStorageType == 'MEMORY') {
            mobEdu.util.applicationId = applicationId;
        } else {
            var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
            if (entourageStore.data.length == 0) {
                entourageStore.add({
                    applicationId: applicationId
                });
            } else {
                var rec = entourageStore.getAt(0);
                rec.set('applicationId', applicationId);
            }
            entourageStore.sync();
        }
    }
});