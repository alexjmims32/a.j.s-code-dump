Ext.define('mobEdu.directory.model.emerCnt', {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'id',
			mapping: 'contactId'
		}, {
			name: 'name'
		}, {
			name: 'phone'
		}, {
			name: 'address'
		}, {
			name: 'email'
		}, {
			name: 'pictureUrl'
		}, {
			name: 'comments'
		}, {
			name: 'picture_url'
		}]
	}
});