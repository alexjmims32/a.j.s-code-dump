Ext.define('mobEdu.directory.f', {
	requires: [
		'mobEdu.directory.store.eContctList',
		'mobEdu.directory.store.peopleDetail',
		'mobEdu.directory.store.peopleList',
		'mobEdu.directory.store.searchList'
	],
	statics: {


		loadDirectory: function() {
			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
			if (feedLocalStore.data.length > 0) {
				var rec = feedLocalStore.findRecord('name', 'Directory');
				if (rec != null) {
					var directorySource = rec.data.format;
					var directoryUrl = rec.data.link;
					//                    if(directorySource == 'LINK' || directorySource == 'ELINK') {
					if (directorySource == 'HTML' || directorySource == 'JSP') {
						mobEdu.util.loadExternalUrl(directoryUrl);
					} else {
						mobEdu.directory.f.loadShowDirectory();
					}
				} else {
					mobEdu.directory.f.loadShowDirectory();
				}
			}
		},
		loadShowDirectory: function() {
			mobEdu.directory.f.showDirectory();
			if (mobEdu.util.isShowDirectoryCategory() == false) {
				Ext.getCmp('dirSearchCategory').setHidden(true);
			} else {
				var options = new Array();
				var cnt = 0;
				if (mobEdu.util.getDirSearchDefault() == 'All') {
					options[cnt++] = ({
						text: 'All',
						value: 'all'
					});
				}
				if (dirSearchDefault == 'onlyFacStaff') {
					options[cnt++] = ({
						text: 'Faculty/Staff',
						value: 'facstaf'
					});
				} else {
					options[cnt++] = ({
						text: 'Student',
						value: 'student'
					});
					options[cnt++] = ({
						text: 'Faculty/Staff',
						value: 'facstaf'
					});
				}
				Ext.getCmp('dirSearchCategory').setOptions(options);
			}
		},

		dirSearch: function(fNamefield, lNamefield, e) {
			if (e.browserEvent.keyCode != 9) {
				if (customDirectorySearch == 'true') {
					mobEdu.directory.f.doCheckDirectoryBeforeOnlineSearch(fNamefield, lNamefield);
				} else {
					mobEdu.directory.f.doCheckDirectoryBeforeSearch(fNamefield, lNamefield);
				}

			}
		},
		doCheckDirectoryBeforeOnlineSearch: function(fNamefield, lNamefield) {
			var fnValue = fNamefield.getValue();
			var lnValue = '';
			var fnLength = fnValue.length;
			var lnLength = 0;
			if (fnLength > 2) {
				setTimeout(function() {
					var fnItem = Ext.getCmp('dirSearchfName').getValue();
					//var lnItem = Ext.getCmp('dirSearchlName').getValue();
					if (fnValue == fnItem) {
						if (Ext.getCmp('pagingdirectoryplugin') == null) {
							var pArray = new Array();
							pArray[0] = mobEdu.directory.view.paging.create();
							Ext.getCmp('searchDirectoryList').setPlugins(pArray);
							Ext.getCmp('searchDirectoryList').setEmptyText('No search results');
						}
						Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().show();

						mobEdu.directory.f.clearDirectorySearchStore();
						mobEdu.directory.f.directorySearch();
					}
				}, 1000);
			} else {
				if (Ext.getCmp('pagingdirectoryplugin') != null) {
					Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
				}
				Ext.getCmp('searchDirectoryList').setPlugins(null);
				Ext.getCmp('searchDirectoryList').setEmptyText(null);
				mobEdu.directory.f.clearDirectorySearchStore();
			}
		},

		doCheckDirectoryBeforeSearch: function(fNamefield, lNamefield) {
			var fnValue = fNamefield.getValue();
			var lnValue = lNamefield.getValue();
			var fnLength = fnValue.length;
			var lnLength = lnValue.length;
			if (fnLength > 2 || lnLength > 2) {
				setTimeout(function() {
					var fnItem = Ext.getCmp('dirSearchfName').getValue();
					var lnItem = Ext.getCmp('dirSearchlName').getValue();
					if (fnValue == fnItem && lnValue == lnItem) {
						if (Ext.getCmp('pagingdirectoryplugin') == null) {
							var pArray = new Array();
							pArray[0] = mobEdu.directory.view.paging.create();
							Ext.getCmp('searchDirectoryList').setPlugins(pArray);
							Ext.getCmp('searchDirectoryList').setEmptyText('No search results');
						}
						Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().show();

						mobEdu.directory.f.clearDirectorySearchStore();
						mobEdu.directory.f.directorySearch();
					}
				}, 1000);
			} else {
				if (Ext.getCmp('pagingdirectoryplugin') != null) {
					Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
				}
				Ext.getCmp('searchDirectoryList').setPlugins(null);
				Ext.getCmp('searchDirectoryList').setEmptyText(null);
				mobEdu.directory.f.clearDirectorySearchStore();
			}
		},

		directorySearch: function() {
			var searchCategory = Ext.getCmp('dirSearchCategory').getValue();
			var searchfirstName = Ext.getCmp('dirSearchfName').getValue();
			var searchlastName = Ext.getCmp('dirSearchlName').getValue();
			//            if(searchItem==searchValue){
			var store = mobEdu.util.getStore('mobEdu.directory.store.searchList');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'searchContact');
			store.getProxy().setExtraParams({
				firstName: searchfirstName,
				lastName: searchlastName,
				category: searchCategory,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.directory.f.directorySearchResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('encore', 'searchcontact');
			//            }
		},

		directorySearchResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.directory.store.searchList');
			store.sort('fullName', 'ASC');
			var status = store.getProxy().getReader().rawData.status;
			if (status != 'SUCCESS') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		checkForDirectoryListEnd: function(store, records, isSuccessful) {
			var pageSize = store.getPageSize();
			var pageIndex = store.currentPage - 1; // Page numbers start at 1

			if (isSuccessful && records.length < pageSize) {
				//Set count to disable 'loading' message
				var totalRecords = pageIndex * pageSize + records.length;
				store.setTotalCount(totalRecords);
			} else
				store.setTotalCount(null);

		},

		clearDirectorySearchStore: function() {
			var store = mobEdu.util.getStore('mobEdu.directory.store.searchList');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
			//            store.sync();
		},

		loadPeopleDetail: function(view, index, item, e, record) {
			mobEdu.util.deselectSelectedItem(index, view);
			var store = mobEdu.util.getStore('mobEdu.directory.store.peopleDetail');
			store.removeAll();
			store.add(record);
			store.sync();
			var dirTitle = record.data.fullName;
			mobEdu.directory.f.showPeopleDetails(dirTitle);

		},

		doCheckEmrCntBeforeSearch: function(textfield) {
			var value = textfield.getValue();
			var length = value.length;

			var store = mobEdu.util.getStore('mobEdu.directory.store.eContctList');

			if (length > 0) {
				setTimeout(function() {
					var item = Ext.getCmp('emrSearchParam').getValue();
					if (value == item) {
						store.filter("name", item, true, false);
					}
				}, 1000);
			} else if (length == 0) {
				store.clearFilter();
			}
		},

		loadEmergencyContacts: function() {
			var store = mobEdu.util.getStore('mobEdu.directory.store.eContctList');
			store.clearFilter();
			if (store.getCount() > 0) {
				mobEdu.directory.f.showEmergencyCntctList();
			}
			store.getServerProxy().setHeaders({
				companyID: companyId
			});

			store.getServerProxy().setExtraParams({
				category: 'EMR',
				clientCode: campusCode,
				page: 0
			});
			store.loadServer(function() {
				mobEdu.directory.f.loadEmergencyContactsResponsehandler();
			});
		},
		loadEmergencyContactsResponsehandler: function() {
			var store = mobEdu.util.getStore('mobEdu.directory.store.eContctList');
			mobEdu.directory.f.showEmergencyCntctList();
			if (store.getCount() === 0) {
				Ext.getCmp('emergency').setEmptyText('No contacts available');
			} else {
				Ext.getCmp('emergency').setEmptyText(null);
			}
		},


		doCheckImpBeforeSearch: function(textfield) {
			var value = textfield.getValue();
			var length = value.length;

			var store = mobEdu.util.getStore('mobEdu.directory.store.eContctList');

			if (length > 0) {
				setTimeout(function() {
					var item = Ext.getCmp('impSearchParam').getValue();
					if (value == item) {
						store.filter("name", item, true, false);
					}
				}, 1000);
			} else if (length == 0) {
				store.clearFilter();
			}
		},


		loadImportantNumbers: function() {
			var store = mobEdu.util.getStore('mobEdu.directory.store.eContctList');
			store.clearFilter();
			if (store.getCount() > 0) {
				mobEdu.directory.f.showImportantNumbers();
			}
			store.getServerProxy().setHeaders({
				companyID: companyId
			});
			store.getServerProxy().setExtraParams({
				category: 'IMPNUM',
				page: 0
			});
			store.loadServer(function() {
				mobEdu.directory.f.loadImportantNumbersResponsehandler();
			});
		},

		loadImportantNumbersResponsehandler: function() {
			mobEdu.directory.f.showImportantNumbers();
			if (store.getCount() === 0) {
				Ext.getCmp('impNum').setEmptyText('No contacts available');
			} else {
				Ext.getCmp('impNum').setEmptyText(null);
			}
		},

		ShowAvatar: function() {
			if (showAvatar === 'true')
				return true;
			else
				return false;
		},

		checkPhoneExists: function(contacts) {

			for (i = 0; i < contacts.length; i++) {
				if (contacts[i].grp == 'PHONE') {
					return true;
				}
			}
			return false;
		},

		checkEmailExists: function(contacts) {

			for (i = 0; i < contacts.length; i++) {
				if (contacts[i].grp == 'EMAIL') {
					return true;
				}
			}
			return false;
		},

		isTypeEmpty: function(type) {
			if (type === 'NA' || type === '')
				return false;
			else
				return true;
		},

		isPhoneGrp: function(grp) {
			if (grp == "PHONE") {
				return true;
			}
			return false;
		},
		isEmailGrp: function(grp) {
			if (grp == "EMAIL") {
				return true;
			}
			return false;
		},
		isEmergencyGrp: function(grp) {
			if (grp == "EMERGENCY CONTACT") {
				return true;
			}
			return false;
		},

		isCampusExists: function(type) {
			if (type == 'Campus') {
				return true;
			}
			return false;
		},
		isCellExists: function(type) {
			if (type == "Cell") {
				return true;
			}
			return false;
		},
		isBusinessOrWorkExists: function(type) {
			if (type == "Business or work") {
				return true;
			}
			return false;
		},
		isMailingLocalExists: function(type) {
			if (type == "Mailing/Local") {
				return true;
			}
			return false;
		},
		isPermanentExists: function(type) {
			if (type == "Permanent") {
				return true;
			}
			return false;
		},
		isBillingExists: function(type) {
			if (type == "Billing") {
				return true;
			}
			return false;
		},

		checkAddressExists: function(contacts) {

			for (i = 0; i < contacts.length; i++) {
				if (contacts[i].grp == 'ADDRESS') {
					return true;
				}
			}
			return false;
		},

		checkEmergencyContactExists: function(contacts) {

			for (i = 0; i < contacts.length; i++) {
				if (contacts[i].grp == 'EMERGENCY CONTACT') {
					return true;
				}
			}
			return false;
		},
		isAddressGrp: function(grp) {
			if (grp == "ADDRESS") {
				return true;
			}
			return false;
		},
		isFaxExists: function(address) {
			if (address !== '' || address !== null) {
				return true;
			}
			return false;
		},
		checkFaxExists: function(contacts) {

			for (i = 0; i < contacts.length; i++) {
				if (contacts[i].grp == 'FAX') {
					return true;
				}
			}
			return false;
		},

		isFaxGrp: function(grp) {
			if (grp == "FAX") {
				return true;
			}
			return false;
		},

		isExists: function(value) {
			if (value != '' && value != null) {
				return true;
			}
			return false
		},

		isEmailExists: function(email) {
			if (email == '' || email == 'NA') {
				return false;
			}
			return true;
		},

		showDirectory: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			if (customDirectorySearch == 'true') {
				mobEdu.util.show('mobEdu.directory.view.dirSearch');
			} else {
				mobEdu.util.show('mobEdu.directory.view.dirSearchList');
			}

			mobEdu.util.setTitle('DIRECTORY', Ext.getCmp('dirTitle'));
		},

		showPeopleDetails: function(dirTitle) {
			mobEdu.util.updatePrevView(mobEdu.directory.f.showDirectory);
			mobEdu.util.show('mobEdu.directory.view.peopleDetails');
		},

		showEmergencyCntctList: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.directory.view.emerCntList');
			mobEdu.util.setTitle('EMER', Ext.getCmp('EmerTitle'));
		},

		showImportantNumbers: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.directory.view.impNumCntList');
			mobEdu.util.setTitle('IMPNUM', Ext.getCmp('impTitle'));
		},

		getDirectoryImageSource: function() {
			return directoryImageSource;
		}
	}
});