Ext.define('mobEdu.directory.view.peopleDetails', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.directory.store.peopleDetail'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'dataview',
			disableSelection: true,
			itemTpl: new Ext.XTemplate(
				'<div style="width: 100%;padding-left: 10px;">' +
				'<tpl if="(mobEdu.directory.f.ShowAvatar()===true)">',
				'<div style="display: inline-block; margin-right: 10px;">',
				'<tpl if="(mobEdu.directory.f.isExists(imageUrl)===true && (mobEdu.directory.f.getDirectoryImageSource()===\'LDAP\'))">'+
				'<img height=75 width=75 src="data:image/png;base64,{imageUrl}">',
				'<tpl elseif="(mobEdu.directory.f.isExists(imageUrl)===true && (mobEdu.directory.f.getDirectoryImageSource()===\'BANNER\'))">'+
				'<img height=75 width=75 src="{imageUrl}">',
				'<tpl else><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg">',
				'</tpl>',
				'</div>' +
				'</tpl>',
				'<div style="display: inline-block; vertical-align: top; overflow: hidden; text-overflow: ellipsis;">' +
				'<h3><b>{fullName}</b></h3>' +
				'<tpl if="(mobEdu.directory.f.isExists(title)===true)">',
				'<h4>{title}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(department)===true)">',
				'<h4>{department}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(campus)===true)">',
				'<h4>{campus}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(office)===true)">',
				'<h4>{office}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(mailstop)===true)">',
				'<h4>{mailstop}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(location)===true)">',
				'<h4>{location}</h4>',
				'</tpl>',
				'</div>',
				'</div>',
				'<tpl if="(mobEdu.directory.f.checkPhoneExists(contactList)===true)">',
				'<tpl for="contactList">',
				'<tpl if="(mobEdu.directory.f.isPhoneGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/phone.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3><a href="tel:{address}">{address}</a></h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'</tpl>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.checkFaxExists(contactList)===true)">',
				'<tpl for="contactList">',
				'<tpl if="(mobEdu.directory.f.isFaxGrp(grp)===true) && (mobEdu.directory.f.isFaxExists(address)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/phone24.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3><a href="tel:{address}">{address}</a></h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'</tpl>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.checkEmailExists(contactList)===true)">',
				'<tpl for="contactList">',
				'<tpl if="(mobEdu.directory.f.isEmailGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/mailSmallIcon.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3><a href="mailto:{address}">{address}</a></h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'</tpl>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.checkAddressExists(contactList)===true)">',
				'<tpl for="contactList">',
				'<tpl if="(mobEdu.directory.f.isAddressGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block;vertical-align: top;"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/addressbook.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3>{[mobEdu.util.symbolConverter(values.address)]}</h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'</tpl>',
				'</tpl>'
			),
			store: mobEdu.util.getStore('mobEdu.directory.store.peopleDetail'),
			singleSelect: true,
			loadingText: ''
		}, {
			xtype: 'customToolbar',
			id: 'directoryTitleNew',
			title: '<h1>Contact Details</h1>'
		}],
		flex: 1
	}
});