Ext.define('mobEdu.directory.view.impNumCntList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.directory.store.eContctList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'impTitle'
        }, {
            xtype: 'textfield',
            docked: 'top',
            id: 'impSearchParam',
            labelWidth: 0,
            flex: 1,
            placeHolder: 'Enter Name to Search',
            listeners: {
                keyup: function(textfield, e, eOpts) {
                    mobEdu.directory.f.doCheckImpBeforeSearch(textfield, e);
                },
                clearicontap: function(textfield, e, eOpts) {
                    mobEdu.directory.f.doCheckImpBeforeSearch(textfield, e);
                }
            }
        }, {
            xtype: 'list',
            id: 'impNum',
            emptyText: 'No search results',
            itemTpl: '<table class="menu" width="100%">' +
                '<tr>' +
                '<tpl if="(mobEdu.util.isValueExists(picture_url)===true)">' + '<td rowspan=5><img height=75 width=75 src="{picture_url}"></td>' +
                '<tpl else>' + '<td rowspan=5><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg"></td>' +
                '</tpl>' +
                '<td width="100%" align="left"><h2>' +
                '<tpl if="(mobEdu.directory.f.isEmailExists(email) === false)">' + '{name}' + '<tpl else>' + '{name}' + '</br><a href="mailto:{email}" >{email}</a></tpl>' + '</h2></td></tr><tr><td width="100%" align="left"><h3><a href="tel:{phone}">{phone}</a><h3></td></tr>' + '<tr><td><tpl if="(mobEdu.directory.f.isExists(address) === true)"><h3>{address}</h3></tpl></td></tr>' + '<tpl if="(mobEdu.directory.f.isExists(comments) === true)">' + '<tr><td><h4>{comments}</h4></td></tr>' + '</tpl>' + '</table>',
            store: mobEdu.util.getStore('mobEdu.directory.store.eContctList'),
            loadingText: '',
            disableSelection: true
        }],
        flex: 1
    }
});