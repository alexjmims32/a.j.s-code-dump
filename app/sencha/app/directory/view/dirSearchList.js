Ext.define('mobEdu.directory.view.dirSearchList', {
    extend: 'Ext.Panel',
    xtype: 'dirSearchList',
    requires: [
        'mobEdu.directory.store.searchList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'dirTitle'
        }, {
            docked: 'top',
            style: 'background: transparent;',
            items: [{
                    xtype: 'selectfield',
                    labelWidth: '0%',
                    border: 0,
                    inputCls: 'gradientSelect',
                    name: 'dirSearchCategory',
                    required: true,
                    useClearIcon: true,
                    id: 'dirSearchCategory',
                    //                options: [{
                    //                    text: 'Student',
                    //                    value: 'student'
                    //                }, {
                    //                    text: 'Faculty/Staff',
                    //                    value: 'facstaf'
                    //                }],
                    listeners: {
                        change: function() {
                            var fNamefield = Ext.getCmp('dirSearchfName')
                            var lNamefield = Ext.getCmp('dirSearchlName')
                            mobEdu.directory.f.doCheckDirectoryBeforeOnlineSearch(fNamefield, lNamefield);
                        }
                    }
                }, {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'textfield',
                        id: 'dirSearchlName',
                        labelWidth: 0,
                        flex: 1,
                        width: '48%',
                        placeHolder: 'Last Name',
                        listeners: {
                            keyup: function(textfield, e, eOpts) {
                                var fNamefield = Ext.getCmp('dirSearchfName')
                                mobEdu.directory.f.dirSearch(fNamefield, textfield, e);
                            },
                            clearicontap: function(textfield, e, eOpts) {
                                var fNamefield = Ext.getCmp('dirSearchfName');
                                mobEdu.directory.f.doCheckDirectoryBeforeSearch(fNamefield, textfield, e);
                            }
                        }
                    }, {
                        xtype: 'spacer',
                        width: '0%',
                        style: ' background-color: #fff',
                    }, {
                        xtype: 'textfield',
                        id: 'dirSearchfName',
                        labelWidth: 0,
                        flex: 1,
                        width: '48%',
                        placeHolder: 'First Name',
                        listeners: {
                            keyup: function(textfield, e, eOpts) {
                                var lNamefield = Ext.getCmp('dirSearchlName')
                                mobEdu.directory.f.dirSearch(textfield, lNamefield, e);
                            },
                            clearicontap: function(textfield, e, eOpts) {
                                var lNamefield = Ext.getCmp('dirSearchlName');
                                mobEdu.directory.f.doCheckDirectoryBeforeSearch(textfield, lNamefield, e);
                            }
                        }
                    }]
                }
                //            {
                //                xtype:'textfield',
                //                name:'dirSearchItem',
                //                id:'dirSearchItem',
                //                placeHolder:'Please enter value to search',
                //                listeners:{
                //                    keyup: function( textfield, e, eOpts ) {
                //                        mobEdu.directory.f.dirSearch(textfield,e);
                //                    }
                //                }
                //            }
            ]
        }, {
            xtype: 'list',
            name: 'searchDirectoryList',
            id: 'searchDirectoryList',
            emptyText: 'No search results',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{fullName}</h3></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.directory.store.searchList'),
            singleSelect: true,
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        mobEdu.directory.f.loadPeopleDetail(view, index, item, e, record);
                    }, 500);
                }
            }
        }],
        flex: 1
    },
    initialize: function() {
        var searchStore = mobEdu.util.getStore('mobEdu.directory.store.searchList');
        searchStore.addBeforeListener('load', mobEdu.directory.f.checkForDirectoryListEnd, this);
    }
});