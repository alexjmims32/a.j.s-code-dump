Ext.define('mobEdu.directory.view.paging', {
    extend:'Ext.plugin.ListPaging',
    config: {
        id: 'pagingdirectoryplugin',
        type: 'listpaging',
        autoPaging: true,
        loadMoreText: 'Load Directory...',
        noMoreRecordsText: ''
    }
});
