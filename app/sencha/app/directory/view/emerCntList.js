Ext.define('mobEdu.directory.view.emerCntList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.directory.store.eContctList'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			id: 'EmerTitle'
		}, {
			xtype: 'textfield',
			docked: 'top',
			id: 'emrSearchParam',
			labelWidth: 0,
			flex: 1,
			placeHolder: 'Enter Name to Search',
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.directory.f.doCheckEmrCntBeforeSearch(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.directory.f.doCheckEmrCntBeforeSearch(textfield, e);
				}
			}
		}, {
			xtype: 'list',
			id: 'emergency',
			itemTpl: '<table class="menu" width="100%">' +
				'<tr>' +
				'<tpl if="(mobEdu.util.isValueExists(picture_url)===true)">' + '<td rowspan=5><img height=75 width=75 src="{picture_url}"></td>' +
				'<tpl else>' + '<td rowspan=5><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg"></td>' +
				'</tpl>' +
				'<td width="100%" align="left"><h2 class="subnavText">' +
				'<tpl if="(mobEdu.directory.f.isEmailExists(email) === false)">' + '{name}' +
				'<tpl else>' + '<a href="mailto:{email}" >{name}</a>' +
				'</tpl>' +
				'</h2></td></tr>' + '<tpl if="(mobEdu.directory.f.isExists(address) === true)">' + '<tr><td><h4 class="accountsAmount">{address}</h4></td></tr>' + '</tpl>' + '<tr><td width="100%" align="left"><h3 class="accountsAmount">Phone: <a href="tel:{phone}" class="listReadMore">{phone}</a><h3></td></tr>' + '<tpl if="(mobEdu.directory.f.isExists(comments) === true)">' + '<tr><td><h4 class="listDetails">{comments}</h4></td></tr>' + '</tpl>' + '</table>',
			store: mobEdu.util.getStore('mobEdu.directory.store.eContctList'),
			loadingText: '',
			disableSelection: true
		}],
		flex: 1
	}
});