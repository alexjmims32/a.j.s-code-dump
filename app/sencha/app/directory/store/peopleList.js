Ext.define('mobEdu.directory.store.peopleList', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.directory.model.people'
    ],

    config:{
        storeId: 'mobEdu.directory.store.peopleList',
        autoLoad: true,
        model: 'mobEdu.directory.model.people',
        data:[
            {fullName: 'Hallums Williams A.', phone: '16784000154', email: 'testuser1@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_1.jpg'},
            {fullName: 'Usry Rudolph L.', phone: '16784000155', email: 'testuser2@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f7.jpg'},
            {fullName: 'Anderson Bobby O.', phone: '16784000156', email: 'testuser3@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_3.jpg'},
            {fullName: 'Stansell Michael', phone: '16784000157', email: 'testuser4@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f1.jpg'},
            {fullName: 'Yates Francis S.', phone: '16784000158', email: 'testuser5@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_5.jpg'},
            {fullName: 'Ward Julie', phone: '16784000159', email: 'testuser6@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f21.jpg'},
            {fullName: 'Reading Debbie', phone: '16784000160', email: 'testuser7@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s38.jpg'},
            {fullName: 'Luhrs Pamela D.', phone: '16784000161', email: 'testuser8@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f11.jpg'},
            {fullName: 'Shinall Hugh', phone: '16784000162', email: 'testuser9@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_9.jpg'},
            {fullName: 'Griffin Steven S.', phone: '16784000163', email: 'testuser10@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_10.jpg'},
            {fullName: 'Hanna Glen', phone: '16784000164', email: 'testuser11@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_11.jpg'},
            {fullName: 'Summers Mac', phone: '16784000165', email: 'testuser12@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f3.jpg'},
            {fullName: 'English Thomas D.', phone: '16784000166', email: 'testuser13@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_13.jpg'},
            {fullName: 'Reading Robert', phone: '16784000167', email: 'testuser14@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f10.jpg'},
            {fullName: 'Humphries Bobby', phone: '16784000168', email: 'testuser15@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_15.jpg'},
            {fullName: 'Luhrs Alan A.', phone: '16784000169', email: 'testuser16@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f4.jpg'},
            {fullName: 'June Ferdinand', phone: '16784000170', email: 'testuser17@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_17.jpg'},
            {fullName: 'Teal James', phone: '16784000171', email: 'testuser18@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/f6.jpg'},
            {fullName: 'Truesdell Margaret J.', phone: '16784000172', email: 'testuser19@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s39.jpg'},
            {fullName: 'Hallums Mark', phone: '16784000173', email: 'testuser20@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/profile_20.jpg'},
            {fullName: 'Bell Ruth J.', phone: '16784000174', email: 'testuser21@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s3.jpg'},
            {fullName: 'Anderson Bobby', phone: '16784000175', email: 'testuser21@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s5.jpg'},
            {fullName: 'Richardson Gerald', phone: '16784000176', email: 'testuser22@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s8.jpg'},
            {fullName: 'Carnes Merilyn E.', phone: '16784000177', email: 'testuser23@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s2.jpg'},
            {fullName: 'Mitchell Robert T.', phone: '16784000178', email: 'testuser24@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s12.jpg'},
            {fullName: 'Knopp Beth', phone: '16784000179', email: 'testuser25@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s6.jpg'},
            {fullName: 'Blackmon Sherry D.', phone: '16784000180', email: 'testuser26@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s7.jpg'},
            {fullName: 'Porter Angela D.', phone: '16784000181', email: 'testuser27@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s14.jpg'},
            {fullName: 'Gawronski Tammie L.', phone: '16784000182', email: 'testuser28@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s15.jpg'},
            {fullName: 'Melia Carrie A.', phone: '16784000183', email: 'testuser29@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s17.jpg'},
            {fullName: 'Pierce Casey H.', phone: '16784000184', email: 'testuser30@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s13.jpg'},
            {fullName: 'Wilson Richard', phone: '16784000185', email: 'testuser31@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s16.jpg'},
            {fullName: 'Farrington Judith S.', phone: '16784000186', email: 'testuser32@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s26.jpg'},
            {fullName: 'Martin Linda M.', phone: '16784000187', email: 'testuser33@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s27.jpg'},
            {fullName: 'Hall Paula M.', phone: '16784000188', email: 'testuser34@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s28.jpg'},
            {fullName: 'Morrison Craig D.', phone: '16784000189', email: 'testuser35@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s18.jpg'},
            {fullName: 'Robinson Celeste', phone: '16784000190', email: 'testuser36@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s29.jpg'},
            {fullName: 'Michaud Kenneth A.', phone: '16784000191', email: 'testuser37@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s19.jpg'},
            {fullName: 'Duval Brian', phone: '16784000192', email: 'testuser38@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s30.jpg'},
            {fullName: 'Pairett David J.', phone: '16784000193', email: 'testuser39@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s20.jpg'},
            {fullName: 'Tibbetts James S.', phone: '16784000194', email: 'testuser40@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s21.jpg'},
            {fullName: 'Hartwick Traci L.', phone: '16784000195', email: 'testuser41@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s31.jpg'},
            {fullName: 'Bowley Dianne L.', phone: '16784000196', email: 'testuser42@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s32.jpg'},
            {fullName: 'Marsland Gary A.', phone: '16784000197', email: 'testuser43@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s22.jpg'},
            {fullName: 'Rishkofski Lynn', phone: '16784000198', email: 'testuser44@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s33.jpg'},
            {fullName: 'Hemenway Ashely A.', phone: '16784000199', email: 'testuser45@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s34.jpg'},
            {fullName: 'Jirschele Shauna L.', phone: '16784000200', email: 'testuser46@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s35.jpg'},
            {fullName: 'Berrie Ashley M.', phone: '16784000201', email: 'testuser47@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s36.jpg'},
            {fullName: 'Ames Gary B.', phone: '16784000202', email: 'testuser48@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s22.jpg'},
            {fullName: 'McWilliams Chad D.', phone: '16784000203', email: 'testuser49@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s23.jpg'},
            {fullName: 'Vachon Elaine J.', phone: '16784000204', email: 'testuser50@n2n.com', img: mobEdu.util.getResourcePath() + 'images/profiles/s37.jpg'}
            
        ]
    }
});