Ext.define('mobEdu.directory.store.searchList', {
//    extend:'Ext.data.Store',
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.directory.model.people'
    ],

    config:{
        storeId: 'mobEdu.directory.store.searchList',
        autoLoad: false,
        model: 'mobEdu.directory.model.people',
        pageSize:25
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'personDetails'
        return proxy;
    }

});