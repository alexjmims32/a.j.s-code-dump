Ext.define('mobEdu.directory.store.eContctList', {
	extend: 'Ext.ux.OfflineSyncStore',

	requires: [
		'mobEdu.directory.model.emerCnt'
	],

	config: {
		model: 'mobEdu.directory.model.emerCnt',

		trackLocalSync: false,
		autoServerSync: false,

		// define a LOCAL proxy for saving the store's data locally
		localProxy: {
			type: 'localstorage',
			id: 'emergency-contacts-sync-store'
		},

		// define a SERVER proxy for saving the store's data on the server
		serverProxy: {
			type: 'ajax',
			api: {
				read: encorewebserver + 'open/universityContacts'
			},
			reader: {
				type: 'json',
				rootProperty: 'emergencyContacts'
			}
		}
	},

	initialize: function() {
		this.loadLocal();
	}
});