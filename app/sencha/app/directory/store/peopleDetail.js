Ext.define('mobEdu.directory.store.peopleDetail', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.directory.model.people'
    ],

    config:{
        storeId: 'mobEdu.directory.store.peopleDetail',
        autoLoad: true,
        model: 'mobEdu.directory.model.people',
        proxy:{
            type:'memory',
            id:'peopleDetail'
        }
    }
});