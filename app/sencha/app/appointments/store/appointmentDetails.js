Ext.define('mobEdu.appointments.store.appointmentDetails', {
    extend: 'Ext.data.Store',

    requires: [
        'mobEdu.finaid.model.appointment'
    ],

    config: {
        storeId: 'mobEdu.appointments.store.appointmentDetails',
        autoLoad: false,
        model: 'mobEdu.finaid.model.appointment',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }

    }
});