Ext.define('mobEdu.appointments.store.appUserList', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.enroute.main.model.allLeadsList'
    ],
    config: {
        storeId: 'mobEdu.appointments.store.appUserList',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.allLeadsList'

    },

    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'rows'
        return proxy;
    }
});