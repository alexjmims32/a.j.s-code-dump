Ext.define('mobEdu.appointments.store.newAppointment', {
    extend: 'mobEdu.data.store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.advising.model.appointment'
    ],
    config: {
        storeId: 'mobEdu.appointments.store.newAppointment',

        autoLoad: false,

        model: 'mobEdu.advising.model.appointment',

        initProxy: function() {
            var proxy = this.callParent();
            // proxy.reader.rootProperty= 'roaster';      
            return proxy;
        }
    }
});