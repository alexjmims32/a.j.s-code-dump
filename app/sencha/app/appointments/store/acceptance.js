Ext.define('mobEdu.appointments.store.acceptance', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.appointments.model.appointmentsList'
    ],
    config: {
        storeId: 'mobEdu.appointments.store.acceptance',

        autoLoad: false,

        model: 'mobEdu.appointments.model.appointmentsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});