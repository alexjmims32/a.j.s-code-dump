Ext.define('mobEdu.appointments.store.appointmentsList', {
    extend: 'mobEdu.data.store',
    requires: [
        'mobEdu.finaid.model.appointment'
    ],
    config: {
        storeId: 'mobEdu.appointments.store.appointmentsList',
        autoLoad: false,
        model: 'mobEdu.finaid.model.appointment',
        sorters: {
            property: 'appointmentDate',
            direction: 'ASC'
        }
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'resultSet';
        return proxy;
    }

});