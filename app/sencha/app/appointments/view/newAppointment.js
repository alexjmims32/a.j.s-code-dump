Ext.define('mobEdu.appointments.view.newAppointment', {
    extend: 'Ext.form.Panel',
    requires: [
        'mobEdu.appointments.f',
        'mobEdu.appointments.store.appUserList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>New Appointment</h1>'
        }, {
            xtype: 'fieldset',
            items: [{
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    label: 'To',
                    labelWidth: '25%',
                    id: 'mTo',
                    name: 'mTo',
                    width: '85%',
                    required: true,
                    readOnly: true
                }, {
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    name: 'searchHelp',
                    id: 'search',
                    text: '<a href="javascript:mobEdu.appointments.f.showSearchToIdsPopup();"><img width="30px" height="30px" src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png"></a>',
                    style: {
                        border: 'none',
                        background: 'none'
                    }
                }]
            }, {
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '40%',
                name: 'mappsub',
                id: 'mappsub',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'datepickerfield',
                name: 'mappdate',
                id: 'mappdate',
                labelAlign: 'top',
                labelWidth: '40%',
                label: 'Appointment Date',
                required: true,
                useClearIcon: true,
                value: new Date(),
                picker: {
                    yearFrom: new Date().getFullYear(),
                    yearTo: new Date().getFullYear() + 1
                        //                            slotOrder:['day', 'month', 'year']
                },
                listeners: {
                    change: function(datefield, e, eOpts) {
                        mobEdu.appointments.f.onReqAppDateKeyup(datefield)
                    }
                }
            }, {
                xtype: 'textfield',
                labelWidth: '100%',
                label: 'Appointment Time',
                required: true,
                useClearIcon: true

            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'mappTimeH',
                name: 'mappTimeH',
                required: true,
                useClearIcon: true,
                minValue: 8,
                maxValue: 18,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'mappTimeM',
                name: 'mappTimeM',
                //                        required:true,
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                label: 'Location',
                labelWidth: '40%',
                name: 'mapploc',
                id: 'mapploc',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textareafield',
                label: 'Description',
                labelWidth: '40%',
                labelAlign: 'top',
                name: 'mappdes',
                id: 'mappdes',
                required: true,
                useClearIcon: true,
                maxRows: 15
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Save',
                //                        ui:'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.appointments.f.sendReqNewAppointment();
                }
            }]
        }]
    }
});