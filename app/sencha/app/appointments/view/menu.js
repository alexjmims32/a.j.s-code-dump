Ext.define('mobEdu.appointments.view.menu', {
	extend: 'Ext.Container',
	config: {
		fullscreen: true,
		layout: {
			type: 'card',
			animation: {
				type: 'slide',
				direction: 'left',
				duration: 250
			}
		},
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Appointment Search</h1>'
		}, {
			xtype: 'tabpanel',
			id: 'enrouteAppTabs',
			name: 'enrouteAppTabs',
			tabBar: {
				layout: {
					pack: 'center'
				}
			},
			items: [{
				layout: 'fit',
				title: '<h3>SYS Search</h3>',
				scrollable: false,
				width: '100',
				id: 'sysSearch',
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.appointments.f.clearSystemStore(Ext.getCmp('searchstId'));
					}
				},
				items: [{
					xtype: 'toolbar',
					docked: 'bottom',
					layout: {
						pack: 'right'
					},
					items: [{
						text: 'Add',
						handler: function() {
							mobEdu.appointments.f.selectedToNames();
						}
					}]
				}, {
					xtype: 'selectfield',
					labelWidth: '0%',
					docked: 'top',
					hidden: true,
					border: 0,
					inputCls: 'gradientSelect',
					name: 'searchCategory',
					required: true,
					useClearIcon: true,
					id: 'searchCategory',
					options: [{
						text: 'Student',
						value: 'student'
					}, {
						text: 'Faculty/Staff',
						value: 'faculty'
					}, {
						text: 'Other',
						value: 'employee'
					}],
					listeners: {
						change: function() {
							var fNamefield = Ext.getCmp('searchstId').setValue('');
							mobEdu.appointments.f.clearSystemStore();
							mobEdu.appointments.f.clearAppStore();
							if (Ext.getCmp('pagingdirectoryplugin') != null) {
								Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
							}
							Ext.getCmp('appSelectAll').setPlugins(null);
							Ext.getCmp('appSelectAll').setEmptyText(null);
						}
					}
				}, {
					xtype: 'textfield',
					id: 'searchstId',
					name: 'searchstId',
					docked: 'top',
					placeHolder: 'Search',
					listeners: {
						keyup: function(textfield, e, eOpts) {
							mobEdu.appointments.f.doCheckBeforeSearch(textfield);
						},
						clearicontap: function(textfield, e, eOpts) {
							mobEdu.appointments.f.clearSystemStore();
						}
					}
				}, {
					xtype: 'list',
					name: 'appSelectAll',
					scrollable: {
						direction: 'vertical',
						directionLock: true
					},
					id: 'appSelectAll',
					emptyText: 'No search results',
					itemTpl: '<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
					store: mobEdu.util.getStore('mobEdu.appointments.store.appUserList'),
					singleSelect: true,
					loadingText: '',
					listeners: {
						itemtap: function(view, index, target, record, item, e, eOpts) {
							//mobEdu.appointments.f.selectedAppToNames(index, view, record, item);
							mobEdu.appointments.f.onSearchToIdItemTap(view, index, target, record, item, e, eOpts);
						}
					}
				}],

			}, {
				layout: 'fit',
				title: '<h3>My Leads</h3>',
				scrollable: false,
				width: '100',
				id: 'myLeadSearch',
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.appointments.f.clearLeadsStore(Ext.getCmp('searchItem'));
					}
				},
				items: [{
					xtype: 'textfield',
					name: 'searchItem',
					id: 'searchItem',
					docked: 'top',
					width: '100%',
					placeHolder: 'Search lead',
					listeners: {
						keyup: function(textfield, e, eOpts) {
							mobEdu.appointments.f.doCheckBeforeMyLeadSearch(textfield);
						},
						clearicontap: function(textfield, e, eOpts) {
							mobEdu.appointments.f.clearLeadsStore();
						}
					}
				}, {
					xtype: 'list',
					name: 'selectMyLeads',
					scrollable: {
						direction: 'vertical',
						directionLock: true
					},
					id: 'selectMyLeads',
					emptyText: 'No search results',
					itemTpl: '<table class="menu" width="100%"><tr><td><h2>{firstName} {lastName}</h2></td></tr></table>',
					store: mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList'),
					singleSelect: true,
					loadingText: '',
					listeners: {
						itemtap: function(view, index, target, record, item, e, eOpts) {
							mobEdu.appointments.f.selectedAppToNames(view, index, target, record, item, e, eOpts);
						}
					}
				}]
			}, {
				layout: 'fit',
				title: '<h3>User Search</h3>',
				scrollable: false,
				width: '100',
				id: 'allLeadSearch',
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.appointments.f.clearLeadsStore(Ext.getCmp('sALItem'));
					}
				},
				items: [{
					xtype: 'textfield',
					id: 'sALItem',
					name: 'sALItem',
					docked: 'top',
					placeHolder: 'Search',
					listeners: {
						keyup: function(textfield, e, eOpts) {
							mobEdu.appointments.f.doCheckBeforeAllLeadSearch(textfield);
						},
						clearicontap: function(textfield, e, eOpts) {
							mobEdu.appointments.f.clearLeadsStore();
						}
					}
				}, {
					xtype: 'list',
					name: 'allleads',
					scrollable: {
						direction: 'vertical',
						directionLock: true
					},
					id: 'allleads',
					emptyText: 'No search results',
					itemTpl: '<table class="menu" width="100%"><tr><td><h2>{firstName} {lastName}</h2></td></tr></table>',
					store: mobEdu.util.getStore('mobEdu.messages.store.userList'),
					singleSelect: true,
					loadingText: '',
					listeners: {
						itemtap: function(view, index, target, record, item, e, eOpts) {
							mobEdu.appointments.f.selectedAppToNames(index, view, record, item);
						}
					}
				}],
			}],
			flex: 1
		}]
	},
	initialize: function() {
		var searchStore = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
		searchStore.addBeforeListener('load', mobEdu.directory.f.checkForDirectoryListEnd, this);
	}
});