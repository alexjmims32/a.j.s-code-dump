Ext.define('mobEdu.appointments.view.paging', {
	extend: 'Ext.plugin.ListPaging',
	config: {
		//id: 'pagingplugin',
		id: 'stMessageplugin',
		type: 'listpaging',
		autoPaging: true,
		loadMoreText: 'Load appointments..',
		noMoreRecordsText: 'No more appointments'
	}
});