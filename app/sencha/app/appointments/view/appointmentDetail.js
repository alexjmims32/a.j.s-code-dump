Ext.define('mobEdu.appointments.view.appointmentDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.appointments.f',
        'mobEdu.appointments.store.appointmentDetails'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{appointmentDate}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Location:</h2></td><td align="left"><h3>{location}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{[mobEdu.util.getStatus(values.fromDetails,values.toIDs,values.loginUserID)]}</h3></td></tr>' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Description:</h2></td><td align="left"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Participants:</h2></td><td align="left"><h3>{[mobEdu.util.fromSplit(values.fromDetails)]}{[mobEdu.util.appSplit(values.toIDs)]}</h3></td></tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.appointments.store.appointmentDetails'),
            singleSelect: true,
            loadingText: ''
        }, {
            title: '<h1>Appointment Details</h1>',
            xtype: 'customToolbar'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                xtype: 'spacer'
            }, {
                text: 'Accept',
                id: 'maccept',
                handler: function() {
                    mobEdu.appointments.f.acceptAppointment('Accept');
                }
            }, {
                text: 'Decline',
                id: 'mdecline',
                handler: function() {
                    mobEdu.appointments.f.acceptAppointment('Decline');
                }
            }, {
                text: 'Update Appointment',
                id: 'mupdateApp',
                handler: function() {
                    mobEdu.appointments.f.loadReqUpdateAppointment();
                }
            }]
        }],
        flex: 1
    }

});