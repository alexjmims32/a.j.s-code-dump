Ext.define('mobEdu.appointments.view.appointmentsList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.appointments.f',
        'mobEdu.appointments.store.appointmentsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'mainappointmentList',
            name: 'mainappointmentList',
            emptyText: '<h3 align="center">No Appointments</h3>',
            cls: 'logo',
            itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                '<tr>',
                '<td ><h3>{subject}</h3></td>' + '<td align="right"><h5>{appointmentDate}</h5></td>' + '</tr>' + '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    mobEdu.appointments.f.showReqAppointmentDetail(record);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>My Appointments</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'New Appointment',
                //                        ui:'confirm',
                handler: function() {
                    mobEdu.appointments.f.showReqNewAppointment();
                }
            }, {
                text: 'Calendar',
                id: 'reqAppCal',
                handler: function() {
                    mobEdu.appointments.f.showCalendar();
                }
            }]
        }],
        flex: 1
    },

    initialize: function() {
        this.callParent();
        mobEdu.enroute.leads.f.setPagingPlugin(Ext.getCmp('mainappointmentList'));
        var searchStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
        searchStore.addBeforeListener('load', mobEdu.enroute.leads.f.checkForSearchListEnd, this);
    }
});