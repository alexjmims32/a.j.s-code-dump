Ext.define('mobEdu.appointments.view.mainAppointmentCalendar', {
    extend: 'Ext.Container',
    alias: 'mainAppointmentCalendar',
    config: {
        fullscreen: 'true',
        scrollable: true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            id: 'mainAppointmentsDock',
            name: 'mainAppointmentsDock',
            items: [{}]
        }, {
            xtype: 'label',
            id: 'mainAppointmentLabel',
            padding: 10
        }]
    }
});