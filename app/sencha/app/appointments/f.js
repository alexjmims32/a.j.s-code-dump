Ext.define('mobEdu.appointments.f', {
	requires: [
		'mobEdu.appointments.store.appUserList',
		'mobEdu.appointments.store.appointmentDetails',
		'mobEdu.appointments.store.appointmentsList',
		'mobEdu.appointments.store.newAppointment',
		'mobEdu.appointments.store.toAppStore',
		'mobEdu.appointments.store.acceptance'
	],
	statics: {
		prevViewRef: null,
		count: 0,
		targetFlag: false,
		appLeadId: null,
		loadFromCalendarModule: function() {
			// mobEdu.appointments.f.loadAppointmentsList('CALENDAR');
			var role = mobEdu.util.getRole();
			mobEdu.appointments.f.loadEnrollAppList();

		},
		loadEnrollAppList: function() {
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
			if (store.data.length >= 0) {
				store.removeAll();
				store.removed = [];
				store.currentPage = 1
			}
			store.getProxy().setUrl(commonwebserver + 'appointment/search');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				// userID: mobEdu.util.getStudentId(),
				companyId: companyId,
				loginID: mobEdu.util.getStudentId()
			});
			store.getProxy().afterRequest = function() {
				mobEdu.appointments.f.viewAppointmentsResponseHandler();
			};
			store.load();
		},

		viewAppointmentsResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
			mobEdu.util.get('mobEdu.enroute.leads.view.paging');
			if (store.data.length > 0) {
				Ext.getCmp('leadplugin').setNoMoreRecordsText('No More Records');
			} else {
				Ext.getCmp('leadplugin').setNoMoreRecordsText('');
			}
			mobEdu.appointments.f.showViewAppts();
		},

		showAppointmentsList: function(val) {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			var reqAppListStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
			if (reqAppListStore.data.length > 0) {
				reqAppListStore.removeAll();
				reqAppListStore.removed = [];
				reqAppListStore.currentPage = 1
			}
			reqAppListStore.load();
			mobEdu.util.show('mobEdu.appointments.view.appointmentsList');

		},
		showViewAppts: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.appointments.view.appointmentsList');
		},

		getRecruiterId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var recruiterId = rec.data.recruiterId;
				return recruiterId;
			}
			return null;
		},

		showReqAppointmentDetail: function(record) {
			var appId = record.data.appointmentID;
			var acceptance;
			var fromDetails = record.data.fromDetails;
			var toArry = record.data.toIDs;
			if (fromDetails.fromID == record.data.loginUserID) {
				acceptance = fromDetails.acceptance;
			}
			if (acceptance == '' || acceptance == undefined || acceptance == null) {
				for (var i = 0; i < toArry.length; i++) {
					var toID = toArry[i].toID;
					if (toID == record.data.loginUserID) {
						acceptance = toArry[i].acceptance;
					}
				}
			}

			var versionUser = record.data.createdBy;
			var user = mobEdu.util.getStudentId();
			user = user.toLowerCase();
			if (versionUser.toLowerCase().match(user)) {
				mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
				Ext.getCmp('maccept').hide();
				Ext.getCmp('mdecline').hide();
			} else {
				mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
				Ext.getCmp('maccept').show();
				Ext.getCmp('mdecline').show();
				if (acceptance == 'Y' || acceptance == 'N') {
					mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
					Ext.getCmp('maccept').hide();
					Ext.getCmp('mdecline').hide();
				} else {
					mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
					Ext.getCmp('maccept').show();
					Ext.getCmp('mdecline').show();
				}
			}
			var role = mobEdu.util.getRole();
			if (role.match(/student/gi)) {
				if (!mobEdu.util.isDualRole()) {
					mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
					Ext.getCmp('mupdateApp').hide();
				};
			}

			var appId = record.data.appointmentID;

			var appDetailStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentDetails');

			appDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			appDetailStore.getProxy().setUrl(commonwebserver + 'appointment/get');
			appDetailStore.getProxy().setExtraParams({
				appointmentID: appId,
				companyId: companyId,
				loginID: mobEdu.util.getStudentId()
			});

			appDetailStore.getProxy().afterRequest = function() {
				mobEdu.appointments.f.reqAppointmentDetailResponseHandler();
			};
			appDetailStore.load();
		},

		reqAppointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentDetails');
			mobEdu.appointments.f.loadReqDetail();
		},

		loadReqDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.appointments.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.appointments.view.appointmentDetail');
		},

		acceptAppointment: function(value) {
			if (value == 'Accept') {
				Ext.Msg.show({
					id: 'mAccept',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Accepted.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.appointments.f.reqAppListBack();
						}
					}
				});
			}
			if (value == 'Decline') {
				Ext.Msg.show({
					id: 'mDecline',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Declined.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.appointments.f.reqAppListBack();
						}
					}
				});
			}
			var appDetail = mobEdu.util.getStore('mobEdu.appointments.store.appointmentDetails');
			mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
			Ext.getCmp('maccept').hide();
			Ext.getCmp('mdecline').hide();

			var detailData = appDetail.data.all;
			var appId = detailData[0].data.appointmentID;
			var sAcceptance = null;
			var cAcceptance = null;
			var role = mobEdu.util.getRole();

			var aStatus = '';

			if (value == 'Accept') {
				sAcceptance = 'Y';
				aStatus = 'Scheduled';
			} else {
				sAcceptance = 'N';
				aStatus = 'Cancelled';
			};

			var newAppStore = mobEdu.util.getStore('mobEdu.appointments.store.acceptance');
			newAppStore.getProxy().setUrl(commonwebserver + 'appointment/statusUpdate');
			newAppStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			newAppStore.getProxy().setExtraParams({
				appointmentID: appId,
				acceptance: sAcceptance,
				status: aStatus,
				loginID: mobEdu.util.getStudentId()
			});
			newAppStore.getProxy().afterRequest = function() {
				mobEdu.appointments.f.acceptanceResponseHandler();
			};
			newAppStore.load();
		},
		acceptanceResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.appointments.store.acceptance');
		},

		reqAppListBack: function() {
			// Ext.getCmp('mainappointmentList');
			var reqAppListStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
			reqAppListStore.currentPage = 1
			reqAppListStore.load();
			mobEdu.appointments.f.showAppointmentsList();
		},

		showReqNewAppointment: function() {
			mobEdu.util.updatePrevView(mobEdu.appointments.f.showAppointmentsList);
			mobEdu.util.get('mobEdu.appointments.view.newAppointment');
			mobEdu.util.show('mobEdu.appointments.view.newAppointment');
			var newDate = new Date();
			Ext.getCmp('mappdate').setValue(newDate);
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			Ext.getCmp('mappTimeH').setMinValue(timeH);
			Ext.getCmp('mappTimeM').setMinValue(timeM);
			Ext.getCmp('mappTimeH').setValue(timeH);
			Ext.getCmp('mappTimeM').setValue(timeM);
			Ext.getCmp('mTo').reset();
			var store = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			store.removeAll();
		},
		sendReqNewAppointment: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			store.removeAll();
			store.sync();
			var lead = Ext.getCmp('mTo').getValue();
			var aSub = Ext.getCmp('mappsub').getValue();
			var aDes = Ext.getCmp('mappdes').getValue();
			var aDate = Ext.getCmp('mappdate').getValue();
			var aLoc = Ext.getCmp('mapploc').getValue();
			var aStatus = 'SCHEDULED';
			var aTimeH = Ext.getCmp('mappTimeH').getValue();
			var aTimeM = Ext.getCmp('mappTimeM').getValue();
			if (aStatus == 'select') {
				aStatus = '';
			}
			if (aTimeH == 0) {
				aTimeH = '';
			}
			if (aSub.charAt(0) == ' ' || aDes.charAt(0) == ' ') {
				Ext.Msg.show({
					message: '<p>Subject & Description must start with alphanumeric.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (lead != '' && aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aStatus != '' && aTimeH != '') {
					var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);
					var selectDate = Ext.getCmp('mappdate').getValue();
					selectDate = Ext.Date.format(selectDate, 'm/d/Y');
					if (aTimeM < 10) {
						aTimeM = "0" + aTimeM;
					}
					var selectDateFormatted = Ext.Date.parse(selectDate + " " + aTimeH + ":" + aTimeM, 'm/d/Y G:i');
					console.log(selectDateFormatted);
					if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
						mobEdu.appointments.f.newReqAppointmentResponse();
					} else {
						Ext.Msg.show({
							title: 'Invalid Date',
							message: '<p>Please select a future date.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
					if (aTimeH >= 18 || aTimeH < 8) {
						Ext.Msg.show({
							title: 'Invalid Time',
							message: '<p>Appointment time between 8AM to 18PM.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				} else {
					Ext.Msg.show({
						title: null,
						message: '<p>Please enter all mandatory fields.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}

			}

		},
		newReqAppointmentResponse: function() {
			var role = mobEdu.util.getRole();
			//var toIds = mobEdu.appointments.f.appLeadId;
			var toIds = '';
			var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			if (localStore.data.length == 0) {
				toIds = '';
			} else {
				localStore.each(function(record) {
					if (toIds == '') {
						toIds = record.data.pidm;
					} else {
						toIds = toIds + ',' + record.data.pidm;
					}
				});
			}

			mobEdu.appointments.f.appLeadId = '';
			var aSub = Ext.getCmp('mappsub').getValue();
			var aDes = Ext.getCmp('mappdes').getValue();
			var aDate = Ext.getCmp('mappdate').getValue();
			var aLoc = Ext.getCmp('mapploc').getValue();
			var aTimeHours = Ext.getCmp('mappTimeH').getValue();
			var aTimeMinutes = Ext.getCmp('mappTimeM').getValue();
			var toNames = Ext.getCmp('mTo').getValue();
			var appTime;
			if (aTimeHours == null) {
				aTimeHours = '';
			}
			if (aTimeMinutes == null) {
				aTimeMinutes = '';
			}

			if (aTimeHours < 10) {
				aTimeHours = '0' + aTimeHours;
			}

			if (aTimeMinutes < 10) {
				aTimeMinutes = '0' + aTimeMinutes;
			}
			appTime = aTimeHours + ':' + aTimeMinutes;
			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(aDate, 'm/d/Y');
			}
			var appDateFormatted = appDateFormatted + ' ' + appTime;
			var appText = 'appointment';
			var appOperationText = 'create';
			var creBy = 1;
			var leadApp = 'OUT';
			var aStatus = '';
			var role = mobEdu.util.getRole();
			if (role.match(/STUDENT/gi)) {
				aStatus = 'Requested';
			} else {
				aStatus = 'Scheduled';
			}

			if (aTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.appointments.f.loadAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.appointments.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.appointments.view.newAppointment');
						}
					}
				});
			} else {
				var newAppStore = mobEdu.util.getStore('mobEdu.appointments.store.newAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});

				newAppStore.getProxy().setUrl(commonwebserver + 'appointment/add');
				newAppStore.getProxy().setExtraParams({
					to: toIds,
					subject: aSub,
					body: aDes,
					appointmentDate: appDateFormatted,
					location: aLoc,
					participants: mobEdu.util.getStudentId() + ',sent;' + toIds + ',sent',
					duration: appTime,
					status: aStatus,
					companyId: companyId,
					loginID: mobEdu.util.getStudentId()
				});

				newAppStore.getProxy().afterRequest = function() {
					mobEdu.appointments.f.newReqAppointmentResponseHandler();
				};
				newAppStore.load();
			}
		},

		newReqAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.appointments.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment created successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var store = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
							store.currentPage = 1
							store.load();
							mobEdu.appointments.f.showAppointmentsList();
							mobEdu.appointments.f.resetAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAppointmentEnumerations: function() {
			var enumType = 'APPOINTMENT_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.enumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(commonwebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.appointments.f.loadEnumerationsResponseHandler();
			};
			enuStore.load();
		},

		resetAppointment: function() {
			var aSub = Ext.getCmp('mappsub').reset();
			var aDes = Ext.getCmp('mappdes').reset();
			Ext.getCmp('mappdate').suspendEvents(true);
			var aDate = Ext.getCmp('mappdate').reset();
			Ext.getCmp('mappdate').resumeEvents(true);
			var aLoc = Ext.getCmp('mapploc').reset();
			var aTimeHours = Ext.getCmp('mappTimeH').reset();
			var aTimeMinutes = Ext.getCmp('mappTimeM').reset();
			var toNames = Ext.getCmp('mTo').reset();
			// mobEdu.util.get('mobEdu.appointments.view.newAppointment').reset();
		},


		loadEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.enumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadReqUpdateAppointment: function() {
			//mobEdu.appointments.f.loadAppointmentEnumerations();
			var upAppReqStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentDetails');
			var upDateStore = upAppReqStore.data.all;
			mobEdu.util.updatePrevView(mobEdu.appointments.f.loadReqDetail);
			mobEdu.util.show('mobEdu.appointments.view.updateAppointment');
			Ext.getCmp('mupappsub').setValue(upDateStore[0].data.subject);
			Ext.getCmp('mupappdes').setValue(upDateStore[0].data.body);
			var appDate = upDateStore[0].data.appointmentDate;
			var appDateValue = appDate.split(' ');
			var dateFormatted = appDateValue[0];
			var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");

			var appTime = appDateValue[1];
			var appTimeFormatted = appTime.split(':');
			var appTimeHours = appTimeFormatted[0];
			var appTimeMints = appTimeFormatted[1];
			if (appTimeHours < 10) {
				appTimeHours = '0' + appTimeHours;
			}
			if (appTimeMints < 10) {
				appTimeMints = '0' + appTimeMints;
			}
			if (upDateStore[0].data.appointmentDate != '' && upDateStore[0].data.appointmentDate != undefined && upDateStore[0].data.appointmentDate != null) {
				Ext.getCmp('mappdate').setValue(new Date(newStartDate));
			}
			Ext.getCmp('mUpTimeHours').setValue(appTimeHours);
			Ext.getCmp('mUpTimeMinutes').setValue(appTimeMints);
			Ext.getCmp('mupapploc').setValue(upDateStore[0].data.location);
		},
		updateReqAppointmentSend: function() {
			var upSub = Ext.getCmp('mupappsub').getValue();
			var upDes = Ext.getCmp('mupappdes').getValue();
			var upDate = Ext.getCmp('mappdate').getValue();
			var upLoc = Ext.getCmp('mupapploc').getValue();
			var upStatus = 'A';
			var upAppTimeHours = Ext.getCmp('mUpTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('mUpTimeMinutes').getValue();

			if (upSub != '' && upDes != '' && upDate != '' && upLoc != '' && upAppTimeHours != '') {
				var presentDate = new Date();
				var presentDateFormatted;
				if (presentDate == null) {
					presentDateFormatted = '';
				} else {
					presentDateFormatted = Ext.Date.parse(presentDate, "m/d/Y");
					// (presentDate.getFullYear() + '-' + mobEdu.appointments.f.dateChecking([presentDate.getMonth() + 1]) + '-' + mobEdu.appointments.f.dateChecking(presentDate.getDate()));
				}
				var selectDate = Ext.getCmp('mappdate').getValue();
				var selectDateFormatted;
				if (selectDate == null) {
					selectDateFormatted = '';
				} else {
					selectDateFormatted = Ext.Date.parse(selectDate, "m/d/Y");
					// (selectDate.getFullYear() + '-' + mobEdu.appointments.f.dateChecking([selectDate.getMonth() + 1]) + '-' + mobEdu.appointments.f.dateChecking(selectDate.getDate()));
				}
				if (presentDateFormatted > selectDateFormatted) {
					Ext.Msg.show({
						title: 'Invalid Date',
						message: '<p>Please select a future date.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					if (upAppTimeHours >= 18) {
						Ext.Msg.show({
							title: 'Invalid Time',
							message: '<p>Appointment time between 8:00 to 18:00.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					} else {
						mobEdu.appointments.f.updateAppointmentSentRequest();
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateAppointmentSentRequest: function() {
			var upAppReqStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentDetails');
			var upDateStore = upAppReqStore.data.all;
			var appointmentId = upDateStore[0].data.appointmentID;
			var participants = upDateStore[0].raw.participants;
			var fromDetails = upDateStore[0].raw.fromDetails;
			var from = '';
			var userID = fromDetails.fromID;
			var acceptance = fromDetails.acceptance;
			from = userID + ',' + acceptance + ';';

			var to = upDateStore[0].raw.toIDs;
			var versionUser = upDateStore[0].raw.createdBy;
			var participant = '';
			var res = '';
			for (var i = 0; i < to.length; i++) {
				var userID = to[i].toID;
				var acceptance = to[i].acceptance;
				res = res + userID + ',' + acceptance + ';';
			}
			var upSub = Ext.getCmp('mupappsub').getValue();
			var upDes = Ext.getCmp('mupappdes').getValue();
			var upDate = Ext.getCmp('mappdate').getValue();
			var upLoc = Ext.getCmp('mupapploc').getValue();
			var upStatus = 'Rescheduled';
			var upAppTimeHours = Ext.getCmp('mUpTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('mUpTimeMinutes').getValue();
			var upAppTime;
			if (upAppTimeHours == null) {
				upAppTimeHours = '';
			}
			if (upAppTimeMints == null) {
				upAppTimeMints = '';
			}

			if (upAppTimeHours < 10) {
				upAppTimeHours = '0' + upAppTimeHours;
			}
			if (upAppTimeMints < 10) {
				upAppTimeMints = '0' + upAppTimeMints;
			}
			upAppTime = upAppTimeHours + ':' + upAppTimeMints;
			var appDateFormatted;
			if (upDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(upDate, 'm/d/Y');
				// (upDate.getFullYear() + '-' + [upDate.getMonth() + 1] + '-' + upDate.getDate());
			}
			var appDateTime = appDateFormatted + ' ' + upAppTime;
			var currentDate = new Date();
			var currentDateFormatted = Ext.Date.format(currentDate, 'm/d/Y');
			// (currentDate.getFullYear() + '-' + [currentDate.getMonth() + 1] + '-' + currentDate.getDate());
			if (upAppTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.appointments.f.loadAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.appointments.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.appointments.view.updateReqAppointment');
						}
					}
				});
			} else {
				var newAppStore = mobEdu.util.getStore('mobEdu.appointments.store.newAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(commonwebserver + '/appointment/update');
				newAppStore.getProxy().setExtraParams({
					appointmentID: appointmentId,
					subject: upSub,
					body: upDes,
					appointmentDate: appDateTime,
					location: upLoc,
					status: upStatus,
					duration: upAppTime,
					participants: participant,
					versionUser: versionUser,
					companyId: companyId,
					from: from,
					to: res,
					loginID: mobEdu.util.getStudentId()
				});

				newAppStore.getProxy().afterRequest = function() {
					mobEdu.appointments.f.updateAppointmentResponseHandler();
				};
				newAppStore.load();
			}
		},

		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.appointments.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappreqsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.appointments.f.reqAppListBack();
							mobEdu.appointments.f.resetUppDateAppointment();
							mobEdu.util.get('mobEdu.appointments.view.appointmentDetail');
							Ext.getCmp('maccept').show();
							Ext.getCmp('mdecline').show();

						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		resetUppDateAppointment: function() {
			mobEdu.util.get('mobEdu.appointments.view.updateAppointment');
			Ext.getCmp('mupappsub').reset();
			Ext.getCmp('mupappdes').reset();
			Ext.getCmp('mappdate').suspendEvents(true);
			var aDate = Ext.getCmp('mappdate').reset();
			Ext.getCmp('mappdate').resumeEvents(true);
			Ext.getCmp('mupapploc').reset();
			Ext.getCmp('mUpTimeHours').reset();
			Ext.getCmp('mUpTimeMinutes').reset();
		},
		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},

		showCalendar: function() {
			mobEdu.util.updatePrevView(mobEdu.appointments.f.reqAppListBack);
			mobEdu.appointments.f.loadCalendarView();
		},
		loadCalendarView: function() {
			mobEdu.main.f.calendarInstance = 'mainAppt';
			mobEdu.util.get('mobEdu.appointments.view.mainAppointmentCalendar');
			var appointmentDock = Ext.getCmp('mainAppointmentsDock');
			appointmentDock.remove(calendar);
			mobEdu.util.show('mobEdu.appointments.view.mainAppointmentCalendar');

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'appointmentsCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			appointmentDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.appointments.f.setAppointmentScheduleInfo(new Date());
		},
		setAppointmentScheduleInfo: function(selectedDate) {
			var scheduleInfo = '';
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			var newDateValue = (mobEdu.appointments.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.appointments.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());

			var appointmentsStore = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
			appointmentsStore.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var leadAcceptance = rec.get('leadAcceptance');
				var recAcceptance = rec.get('recAcceptance');

				var appDateValue = appDate.split(' ');
				var dateFormatted = appDateValue[0];
				if (newDateValue == dateFormatted) {
					var appTime = appDateValue[1];
					var appSubject = rec.get('subject');
					var appDesc = rec.get('body');
					var appLocation = rec.get('location');
					scheduleInfo = mobEdu.appointments.f.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc, leadAcceptance, recAcceptance);
				}
			});
			//set the event schedule info in calendar view label
			Ext.getCmp('mainAppointmentLabel').setHtml(scheduleInfo);
		},

		formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc, leadAcceptance, recAcceptance) {
			if (leadAcceptance != 'N' && recAcceptance != 'N') {

				if (appSubject == null) {
					appSubject = '';
				}
				if (appTime == null) {
					appTime = '';
				}
				if (appDesc == null) {
					appDesc = '';
				}
				if (appLocation == null) {
					appLocation = '';
				}
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + decodeURIComponent(appSubject) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + decodeURIComponent(appLocation) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Description: ' + '</b>' + decodeURIComponent(appDesc) + '</h3>' + '<br/>';

				return scheduleInfo;
			} else {
				return scheduleInfo;
			}
		},
		showSearchToIdsPopup: function(popupView) {
			mobEdu.util.get('mobEdu.appointments.view.menu');
			Ext.getCmp('searchCategory').reset();
			var role = mobEdu.util.getRole();
			if (role.match(/SUPERVISOR/gi)) {
				Ext.getCmp('enrouteAppTabs').getTabBar().getComponent(0).hide();
				Ext.getCmp('enrouteAppTabs').getTabBar().getComponent(1).hide();
				Ext.getCmp('enrouteAppTabs').setActiveItem(2);
			} else if (role.match(/RECRUITER/gi)) {
				Ext.getCmp('enrouteAppTabs').getTabBar().getComponent(0).hide();
				Ext.getCmp('enrouteAppTabs').getTabBar().getComponent(2).hide();
				Ext.getCmp('enrouteAppTabs').setActiveItem(1);
			} else {
				Ext.getCmp('enrouteAppTabs').getTabBar().hide();
			}

			mobEdu.util.updatePrevView(mobEdu.appointments.f.toNameBack);
			mobEdu.util.show('mobEdu.appointments.view.menu');
			// Ext.getCmp('enrouteTabs').setActiveItem(0);
			if (Ext.getCmp('pagingdirectoryplugin') != null) {
				Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
			}
			Ext.getCmp('appSelectAll').setPlugins(null);
			Ext.getCmp('appSelectAll').setEmptyText(null);
			mobEdu.appointments.f.clearAppStore();

		},
		statusValue: function(value) {
			if (value == 'I') {
				return 'Inactive';
			} else if (value == 'A') {
				return 'Active';
			}
		},
		selectedAppToNames: function(view, index, record, target, item, e, eOpts) {
			// mobEdu.appointments.f.hideAppSearchPopup();
			var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			localStore.removeAll();

			if (record != null) {
				localStore.add({
					fullName: record.data.firstName + ' ' + record.data.lastName,
					pidm: record.data.leadID
				});
				localStore.sync();
			}

			mobEdu.appointments.f.setAppToField(record);

		},
		// setToField: function() {
		// 	var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
		// 	var selectVlaue = '';
		// 	localStore.each(function(record) {
		// 		if (selectVlaue == '') {
		// 			selectVlaue = record.data.fullName;
		// 		} else {
		// 			selectVlaue = selectVlaue + ',' + record.data.fullName;
		// 		}
		// 	});
		// 	Ext.getCmp('mToName').setValue(selectVlaue);
		// },
		setAppToField: function(record1) {
			var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			var selectVlaue = '';
			localStore.each(function(record) {
				if (selectVlaue == '') {
					selectVlaue = record.data.fullName;
				} else {
					selectVlaue = selectVlaue + ',' + record.data.fullName;
				}
			});
			mobEdu.util.get('mobEdu.appointments.view.newAppointment');
			Ext.getCmp('mTo').setValue(selectVlaue);
			mobEdu.util.show('mobEdu.appointments.view.newAppointment');
			var role = mobEdu.util.getRole();
			if (role.match(/SUPERVISOR/gi) || role.match(/RECRUITER/gi)) {
				mobEdu.appointments.f.appLeadId = record1.data.leadID;
			} else
				mobEdu.appointments.f.appLeadId = record1.data.userID;

		},

		doCheckBeforeSearch: function(textfield) {
			mobEdu.util.get('mobEdu.appointments.view.menu');
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						if (Ext.getCmp('pagingdirectoryplugin') == null) {
							var pArray = new Array();
							pArray[0] = mobEdu.directory.view.paging.create();
							Ext.getCmp('appSelectAll').setPlugins(pArray);
							Ext.getCmp('appSelectAll').setEmptyText('No search results');
						}
						Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().show();
						mobEdu.util.hideKeyboard();
						mobEdu.appointments.f.clearAppStore();
						mobEdu.appointments.f.toIdSearchList(searchString);
					}
				}, 1000);
			} else {
				if (Ext.getCmp('pagingdirectoryplugin') != null) {
					Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
				}
				Ext.getCmp('appSelectAll').setPlugins(null);
				Ext.getCmp('appSelectAll').setEmptyText(null);
				mobEdu.appointments.f.clearAppStore();
			}
		},

		clearAppStore: function() {
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
		},

		toIdSearchList: function(searchString) {
			var role = mobEdu.util.getRole();
			var searchCategory = Ext.getCmp('searchCategory').getValue();
			var searchlastName = '';
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
			store.removeAll();

			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (role.match(/SUPERVISOR/gi)) {
				store.getProxy().setUrl(commonwebserver + 'secured/admin/user/search');
				store.getProxy().setExtraParams({
					searchText: searchString,
					limit: 500
				});
			} else {
				store.getProxy().setUrl(commonwebserver + 'secured/admin/user/search');
				//store.getProxy().setUrl(commonwebserver + 'secured/admin/user/search');
				store.getProxy().setExtraParams({
					searchText: searchString,
					//category: searchCategory,
					companyId: companyId
				});
			}

			store.getProxy().afterRequest = function() {
				mobEdu.appointments.f.toIdSearchListResponseHandler();
			};
			store.load();
		},

		onSearchToIdItemTap: function(viewRef, selectedIndex, target, record, item, e, eOpts) {
			var role = mobEdu.util.getRole();
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
			if (role.match(/SUPERVISOR/gi)) {
				if (selectedIndex == 0) {
					if (item.target.id == "on") {
						Ext.getCmp('allleads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allleadNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						// Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = true;
					} else {
						Ext.getCmp('allleads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allleadNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						// Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = false;
					}
				}

				//if selectall is checked and any item is unchecked
				if (mobEdu.messages.f.targetFlag == true && item.target.id == 'off') {
					var items = Ext.DomQuery.select("img[name='allleadNames']");
					items[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
					items[0].id = 'off';
				};
				//if all items are checked and Selectall is unchecked
				var items = Ext.DomQuery.select("img[name='allleadNames']");
				var allSelected = true;
				Ext.each(items, function(item, i) {
					if (item.id == 'off' && i != 0) {
						allSelected = false;
					};
				});
				if (allSelected) {
					items[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
					items[0].id = 'on';
					mobEdu.messages.f.targetFlag = true;
				};
			} else if (role.match(/RECRUITER/gi)) {
				if (selectedIndex == 0) {
					if (item.target.id == "on") {
						Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = true;
					} else {
						Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = false;
					}
				}
			} else {
				if (selectedIndex == 0) {
					if (item.target.id == "on") {
						Ext.getCmp('appSelectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = true;
					} else {
						Ext.getCmp('appSelectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = false;
					}
				}
			}
			//if selectall is checked and any item is unchecked
			if (mobEdu.messages.f.targetFlag == true && item.target.id == 'off') {
				var items = Ext.DomQuery.select("img[name='allNames']");
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				items[0].id = 'off';
			};
			//if all items are checked and Selectall is unchecked
			var items = Ext.DomQuery.select("img[name='allNames']");
			var allSelected = true;
			Ext.each(items, function(item, i) {
				if (item.id == 'off' && i != 0) {
					allSelected = false;
				};
			});
			if (allSelected) {
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				items[0].id = 'on';
				mobEdu.messages.f.targetFlag = true;
			};

		},

		selectedToNames: function() {

			var role = mobEdu.util.getRole();
			mobEdu.util.updatePrevView(mobEdu.appointments.f.toNameBack);
			if (role.match(/SUPERVISOR/gi)) {
				var searchList = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
				var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
				//localStore.removeAll();
				//localStore.removeAll();
				var items = Ext.DomQuery.select("img[name='allleadNames']");
				Ext.each(items, function(item, i) {
					if (item.id == "on" && i != 0) {
						var rec = searchList.findRecord('userID', item.title);
						var fullName = rec.data.firstName + ' ' + rec.data.lastName;
						if (rec != null) {
							localStore.add({
								fullName: fullName,
								pidm: rec.data.userID
							});
							localStore.sync();
						}
					}
				});
			} else if (role.match(/RECRUITER/gi)) {
				var searchList = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
				var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
				//localStore.removeAll();
				//localStore.removeAll();
				var items = Ext.DomQuery.select("img[name='allNames']");
				Ext.each(items, function(item, i) {
					if (item.id == "on" && i != 0) {
						var rec = searchList.findRecord('leadID', item.title);
						var fullName = rec.data.firstName + ' ' + rec.data.lastName;
						if (rec != null) {
							localStore.add({
								fullName: fullName,
								pidm: rec.data.leadID
							});
							localStore.sync();
						}
					}
				});
			} else {
				var searchList = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
				var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
				//localStore.removeAll();
				//localStore.removeAll();
				var items = Ext.DomQuery.select("img[name='allNames']");
				Ext.each(items, function(item, i) {
					if (item.id == "on" && i != 0) {
						var rec = searchList.findRecord('userID', item.title);
						var fullName = rec.data.firstName + ' ' + rec.data.lastName;
						if (rec != null) {
							localStore.add({
								fullName: fullName,
								pidm: rec.data.userID
							});
							localStore.sync();
						}
					}
				});
			}

			if (localStore.data.length == 0) {
				Ext.Msg.show({
					message: '<p>Please select user to send an appointment.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.updatePrevView(mobEdu.messages.f.showNewMessage);
							//mobEdu.messages.f.showNewMessage();
						}
					}
				});
			} else {
				mobEdu.util.show('mobEdu.appointments.view.newAppointment');
				mobEdu.appointments.f.setToField();
			}
			// var searchList = mobEdu.util.getStore('mobEdu.messages.store.userList');
		},


		toNameBack: function() {
			mobEdu.util.updatePrevView(mobEdu.appointments.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.appointments.view.newAppointment');
		},

		setToField: function() {
			var localStore = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			mobEdu.util.get('mobEdu.appointments.view.newAppointment');
			var selectVlaue = '';
			localStore.each(function(record) {
				if (selectVlaue == '') {
					selectVlaue = record.data.fullName;
				} else {
					selectVlaue = selectVlaue + ',' + record.data.fullName;
				}
			});
			Ext.getCmp('mTo').setValue(selectVlaue);
			Ext.getCmp('searchstId').setValue('');
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
			store.removeAll();
			store.sync();
			store.removed = [];
		},


		toIdSearchListResponseHandler: function() {
			Ext.getCmp('appSelectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
			var status = store.getProxy().getReader().rawData.status;
			if (status != 'success') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (store.currentPage == 1) {
					if (store.data.length > 0) {
						store.insert(0, {
							firstName: "Select All",
							lastName: " "
						});
					}
				};
			}
		},

		clearSystemStore: function(component) {
			if (component != undefined) {
				component.setValue('');
			}
			var searchList = mobEdu.util.getStore('mobEdu.appointments.store.appUserList');
			searchList.removeAll();
		},

		clearLeadsStore: function(component) {
			if (component != undefined) {
				component.setValue('');
			}
			var searchList = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			searchList.removeAll();
		},

		doCheckBeforeMyLeadSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						mobEdu.util.hideKeyboard();
						mobEdu.enroute.leads.f.profileSearch();
					}
				}, 1000);
			}
		},
		doCheckBeforeAllLeadSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						mobEdu.util.hideKeyboard();
						mobEdu.messages.f.toIdSearchList(searchString);
					}
				}, 1000);
			}
		},

		hideAppSearchPopup: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			store.removeAll();
			var store1 = mobEdu.util.getStore('mobEdu.appointments.store.toAppStore');
			store1.removeAll();
			store1.sync();
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.appointments.view.appointmentsList');
		},

		onReqAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
				// (newDate.getFullYear() + '-' + [newDate.getMonth() + 1] + '-' + newDate.getDate());

			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
				// (presentDate.getFullYear() + '-' + [presentDate.getMonth() + 1] + '-' + presentDate.getDate());
			}
			if (newDateFormatted == presentDateFormatted) {
				if (timeH < 8) {
					if (Ext.getCmp('mappTimeH') != undefined) {
						Ext.getCmp('mappTimeH').setMinValue(8);
						Ext.getCmp('mappTimeH').setValue(8);
					}
					if (Ext.getCmp('mappTimeM') != undefined) {
						Ext.getCmp('mappTimeM').setMinValue(0);
						Ext.getCmp('mappTimeM').setValue(0);
					}
				} else {
					if (Ext.getCmp('mappTimeH') != undefined) {
						Ext.getCmp('mappTimeH').setMinValue(timeH);
						Ext.getCmp('mappTimeH').setValue(timeH);
					}
					if (Ext.getCmp('mappTimeM') != undefined) {
						Ext.getCmp('mappTimeM').setMinValue(timeM);
						Ext.getCmp('mappTimeM').setValue(timeM);
					}
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('mupappdate').setValue(new Date());
							Ext.getCmp('mappTimeH').setMinValue(timeH);
							Ext.getCmp('mappTimeM').setMinValue(timeM);
							Ext.getCmp('mappTimeH').setValue(timeH);
							Ext.getCmp('mappTimeM').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('mappTimeH').setMinValue(8);
				Ext.getCmp('mappTimeM').setMinValue(0);
				Ext.getCmp('mappTimeH').setValue(8);
				Ext.getCmp('mappTimeM').setValue(0);
			}
		},

		onAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
				(newDate.getFullYear() + '-' + [newDate.getMonth() + 1] + '-' + newDate.getDate());
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
				// (presentDate.getFullYear() + '-' + [presentDate.getMonth() + 1] + '-' + presentDate.getDate());
			}
			if (newDateFormatted == presentDateFormatted) {
				if (timeH < 8) {
					if (Ext.getCmp('mUpTimeHours') != undefined) {
						Ext.getCmp('mUpTimeHours').setMinValue(8);
						Ext.getCmp('mUpTimeHours').setValue(8);
					}
					if (Ext.getCmp('mUpTimeMinutes') != undefined) {
						Ext.getCmp('mUpTimeMinutes').setMinValue(0);
						Ext.getCmp('mUpTimeMinutes').setValue(0);
					}
				} else {
					if (Ext.getCmp('mUpTimeHours') != undefined) {
						Ext.getCmp('mUpTimeHours').setMinValue(timeH);
						Ext.getCmp('mUpTimeHours').setValue(timeH);
					}
					if (Ext.getCmp('mUpTimeMinutes') != undefined) {
						Ext.getCmp('mUpTimeMinutes').setMinValue(timeM);
						Ext.getCmp('mUpTimeMinutes').setValue(timeM);
					}
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('mappdate').setValue(new Date());
							Ext.getCmp('mUpTimeHours').setMinValue(timeH);
							Ext.getCmp('mUpTimeMinutes').setMinValue(timeM);
							Ext.getCmp('mUpTimeHours').setValue(timeH);
							Ext.getCmp('mUpTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('mUpTimeHours').setMinValue(8);
				Ext.getCmp('mUpTimeMinutes').setMinValue(0);
				Ext.getCmp('mUpTimeHours').setValue(8);
				Ext.getCmp('mUpTimeMinutes').setValue(0);
			}
		},
		doHighlightAppointmentsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.appointments.store.appointmentsList');
			if (store.data.length > 0) {
				var sData = store.data.all;
				//var user = mobEdu.util.getStore('mobEdu.enquire.login.store.login').data.first().raw.userInfo.userName;
				//get the meeting info
				for (var a = 0; a < sData.length; a++) {
					//if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
					var appDate = sData[a].data.appointmentDate;
					var appDateValue = appDate.split(' ');
					var dateFormatted = appDateValue[0];
					var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
					if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
						classes.push(highlightedItemCls);
					}
					//Incrementing startDate
					newStartDate = new Date(newStartDate.getTime() + 86400000);
				}
			}
			return classes
		},
		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.appointments.f.dateChecking([currentDate.getMonth() + 1]) + '/' + mobEdu.appointments.f.dateChecking(currentDate.getDate()) + '/' + currentDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.appointments.f.dateChecking([presentDate.getMonth() + 1]) + '/' + mobEdu.appointments.f.dateChecking(presentDate.getDate()) + '/' + presentDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.appointments.f.setAppointmentScheduleInfo(date)
			}
		}

	}
});