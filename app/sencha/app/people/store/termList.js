Ext.define('mobEdu.people.store.termList', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.people.model.termList'
    ],

    config:{
        storeId: 'mobEdu.people.store.termList',
        
        autoLoad: false,
        
        model: 'mobEdu.people.model.termList'
    },
    initProxy: function() {
        var proxy = this.callParent();
            proxy.reader.rootProperty= 'registerTermList'        
        return proxy;
    }
});
