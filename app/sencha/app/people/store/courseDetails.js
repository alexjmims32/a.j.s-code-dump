Ext.define('mobEdu.people.store.courseDetails', {
    extend: 'mobEdu.data.store',
//    alias: 'courseDetailStore',

    requires: [
        'mobEdu.people.model.courseDetails'
    ],

    config: {
        storeId: 'mobEdu.people.store.courseDetails',
        autoLoad: false,
        model: 'mobEdu.people.model.courseDetails',
        initProxy: function() {
        var proxy = this.callParent();
            proxy.reader.rootProperty= 'registeredCourseList.registeredCourse'        
            return proxy;
        }
    }
});
