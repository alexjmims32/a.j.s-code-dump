Ext.define('mobEdu.people.store.menu', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.people.model.menu'
    ],

    config:{
        storeId: 'mobEdu.people.store.menu',
        autoLoad: false,
        model: 'mobEdu.people.model.menu',
       proxy:{
            type:'localstorage',
            id:'peopleTerm'
       }
    }
});