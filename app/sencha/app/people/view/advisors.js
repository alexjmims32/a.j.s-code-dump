Ext.define('mobEdu.people.view.advisors', {
    extend: 'Ext.Panel',
    requires: [
    'mobEdu.people.store.courseDetails',
    'mobEdu.people.store.menu',
    'mobEdu.courses.store.advisors',
    'mobEdu.people.store.termList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: {
            type: 'vbox'
        },
        items: [
        {
            xtype: 'customToolbar',
            id: 'Padvisors',
            title: '<h1>My Advisors</h1>'
        }, {
            xtype: 'list',
            name: 'advisors',
            id: 'advisors',
            disableSelection: true,
            deferEmptyText: false,
            emptyText: '<center><h3 class="emptyText">No Advisors</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><h2>{name}</h2></td></tr>'+'<tr><td><h3>{email}</h3></td></tr>'+ '</table>'),
            //itemTpl: new Ext.XTemplate('test'),
            store: mobEdu.util.getStore('mobEdu.courses.store.advisors'),
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.people.f.loadAdvisorsInfo(view, index, item, e,record);
                }
            },
            flex: 2
        }
        ]
    }
});