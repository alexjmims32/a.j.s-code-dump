Ext.define('mobEdu.people.view.classmates', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.people.store.courseDetails',
        'mobEdu.people.store.menu',
        'mobEdu.people.store.termList',
        'mobEdu.courses.store.classmates'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: {
            type: 'vbox'
        },
        items: [{
                xtype: 'customToolbar',
                id: 'Pclassmates',
                title: '<h1>My Classmates</h1>'
            }, {
                xtype: 'selectfield',
                name: 'clsm',
                id: 'clsm',
                required: true,
                maxHeight: '46px',
                placeHolder: 'Select Term',
                singleSelect: true,
                listeners: {
                    change: function(selectfield, newValue, oldValue, eOpts) {
                        if (newValue != 0) {
                            mobEdu.people.f.onTermSelectionChange(selectfield, newValue, oldValue, eOpts);
                        } else {
                            mobEdu.util.updateCoursesTermCode(newValue);
                            var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
                            if (classmatesStore.data.length > 0) {
                                classmatesStore.removeAll();
                                classmatesStore.removed = [];
                                Ext.getCmp('CcourseList').reset();
                            }
                        }
                    }
                },
                flex: 1
            }, {
                xtype: 'selectfield',
                name: 'CcourseList',
                required: true,
                maxHeight: '46px',
                placeHolder: 'Select Course',
                id: 'CcourseList',
                listeners: {
                    change: function(selectfield, newValue, oldValue, eOpts) {
                        if (newValue != 0 && (mobEdu.util.getCoursesTermCode != 0)) {
                            mobEdu.people.f.loadClassmates(selectfield, newValue, oldValue, eOpts);
                        } else {
                            var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
                            if (classmatesStore.data.length > 0) {
                                classmatesStore.removeAll();
                                classmatesStore.removed = [];
                            }
                        }
                    }
                },
                flex: 2
            }, {
                xtype: 'list',
                name: 'classmates',
                id: 'classmates',
                disableSelection: true,
                deferEmptyText: false,
                emptyText: '<center><h3 class="emptyText">No Classmates</h3><center>',
                itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><h2>{name}</h2></td></tr>' + '<tr><td><h3>{email}</h3></td></tr>' + '</table>'),
                //itemTpl: new Ext.XTemplate('test'),
                store: mobEdu.util.getStore('mobEdu.courses.store.classmates'),
                loadingText: '',
                // listeners: {
                //     itemtap: function(view, index, target, record, item, e, eOpts) {
                //         mobEdu.people.f.loadClasmatesInfo(view, index, item, e,record);
                //     }
                // },
                flex: 3
            }]
            //    }],
            //flex: 1
    }
});