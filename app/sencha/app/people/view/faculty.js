Ext.define('mobEdu.people.view.faculty', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.people.store.courseDetails',
        'mobEdu.people.store.menu',
        'mobEdu.people.store.termList',
        'mobEdu.courses.store.faculty'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: {
            type: 'vbox'
        },
        items: [{
                xtype: 'customToolbar',
                id: 'Pfaculty',
                title: '<h1>My Faculty</h1>'
            }, {
                xtype: 'selectfield',
                name: 'fact',
                id: 'fact',
                required: true,
                maxHeight: '46px',
                placeHolder: 'Select Term',
                singleSelect: true,
                listeners: {
                    change: function(selectfield, newValue, oldValue, eOpts) {
                        if (newValue != 0) {
                            mobEdu.people.f.onFacultyTermSelection(selectfield, newValue, oldValue, eOpts);
                        } else {
                            mobEdu.util.updateCoursesTermCode(newValue);
                            var facultyStore = mobEdu.util.getStore('mobEdu.courses.store.faculty');
                            if (facultyStore.data.length > 0) {
                                facultyStore.removeAll();
                                facultyStore.removed = [];
                                Ext.getCmp('FcourseList').reset();
                            }
                        }
                    }
                },
                flex: 1
            }, {
                xtype: 'list',
                name: 'faculty',
                id: 'faculty',
                disableSelection: true,
                deferEmptyText: false,
                emptyText: '<center><h3 class="emptyText">No Faculty</h3><center>',
                itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><h2>{courseTitle} ({crn})</h2></td></tr>' + '<tr><td><h2>{name}</h2></td></tr>' + '<tr><td><h3><a href="mailto:{email}">{email}</a></h3></td></tr>' + '<tr><td><h3>{officeLocation}</h3></td></tr>' + '</table>'),
                //itemTpl: new Ext.XTemplate('test'),
                store: mobEdu.util.getStore('mobEdu.courses.store.faculty'),
                loadingText: '',
                // listeners: {
                //     itemtap: function(view, index, target, record, item, e, eOpts) {
                //         mobEdu.people.f.loadFacultyInfo(view, index, item, e,record);
                //     }
                // },
                flex: 3
            }]
            //    }],
            //flex: 1
    }
});