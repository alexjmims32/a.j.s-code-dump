Ext.define('mobEdu.people.view.peopleList', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.people.store.menu',
                'mobEdu.main.store.entourage',
                'mobEdu.people.f'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.people.store.menu'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'PeopleTitle',
            title:'<h1>People<h1>'
        }],
        flex: 1
    },
    initialize: function() {
        mobEdu.people.f.initializePeopleView();
    }
});