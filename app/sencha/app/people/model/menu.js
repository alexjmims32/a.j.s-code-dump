Ext.define('mobEdu.people.model.menu', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {
                name: 'fullName'
            },
            {
                name: 'title'
            },
            {
                name: 'action',
                type: 'function'
            },
            {
                name: 'name'
            },
            {
                name: 'email'
            },
            {
                name: 'subject',
                type: 'string'
            },
            {
                name: 'courseNumber',
                type: 'string'
            },
            {
                name: 'termCode',
                type: 'string'
            },
            {
                name: 'ssn',
                type: 'string'
            },
            {
                name: 'crn',
                type: 'string'
            },
            {
                name: 'phone'
            },
            {
                name: 'email'
            },
            {
                name: 'img'
            },
            {
                name: 'id',
                type: 'string'
            },
            {
                name: 'pidm',
                type: 'string'
            },
            {
                name: 'lastName',
                type: 'string'
            },
            {
                name: 'dob',
                type: 'string'
            },
            {
                name: 'prefix',
                type: 'string'
            },
            {
                name: 'category',
                type: 'string'
            },
            {
                name: 'published',
                type: 'string'
            },
            {
                name: 'updated',
                type: 'string'
            },
            {
                name: 'published',
                type: 'string'
            },
            {
                name: 'published',
                type: 'string'
            },
            {
                name: 'published',
                type: 'string'
            },
            {
                name: 'published',
                type: 'string'
            },
            {
                name: 'firstName',
                type: 'string'
            },
            {
                name: 'middleName',
                type: 'string'
            },
            {
                name: 'contactList',
                type: 'array',
                mapping: 'contactList.contact'
            },
            {
                name: 'status',
                type: 'string'
            },
            {
                name: 'title',
                type: 'string'
            },
            {
                name: 'department',
                type: 'string'
            },
            {
                name: 'campus',
                type: 'string'
            },
            {
                name: 'office',
                type: 'string'
            },
            {
                name: 'courseDet',
                type: 'string'
            }
        ]
    }
});
