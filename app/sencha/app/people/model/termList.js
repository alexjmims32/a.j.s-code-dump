Ext.define('mobEdu.people.model.termList', {
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'code',
            type : 'string'
        },{
            name : 'description',
            type : 'string'
        }]
    }
});
