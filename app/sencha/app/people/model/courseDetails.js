Ext.define('mobEdu.people.model.courseDetails', {
    extend:'Ext.data.Model',

    config:{

         fields : [ {
            name:'id'
        },{
            name : 'registeredDate',
            type : 'string'
        },{
            name : 'registeredBy',
            type : 'string'
        },{
            name : 'statusMsg',
            type : 'string',
            mapping :'statusMessage'    
        },{
            name : 'statusCode',
            type : 'string',
            mapping : 'statusCode'
        },{
            name : 'courseNumber',
            type : 'string',
            mapping:'course.courseNumber'
        },{
            name : 'courseTitle',
            type : 'string',
            mapping:'course.courseTitle'
        },{
            name : 'subject',
            type : 'string',
            mapping:'course.subject'
        },{
            name : 'description',
            type : 'string',
            mapping:'course.description'
        },{
            name : 'level',
            type : 'string',
            mapping:'course.level'
        },{
            name : 'college',
            type : 'string',
            mapping:'course.college'
        },{
            name : 'period',
            type : 'string',
            mapping:'course.period'
        },{
            name : 'roomCode',
            type : 'string',
            mapping:'course.roomCode'
        },{
            name : 'termCode',
            type : 'string',
            mapping:'course.termCode'
        },{
            name : 'termDescription',
            type : 'string',
            mapping:'course.termDescription'
        },{
            name : 'scheduleType',
            type : 'string',
            mapping:'course.scheduleType'
        },{
            name : 'department',
            type : 'string',
            mapping:'course.department'
        },{
            name : 'campus',
            type : 'string',
            mapping:'course.campus'
        },{
            name : 'crn',
            type : 'string',
            mapping:'course.crn'
        },{
            name : 'scheduleTime',
            type : 'string',
            mapping:'course.scheduleTime'
        },{
            name : 'faculty',
            type : 'string',
            mapping:'course.faculty'
        },{
            name : 'lectureHours',
            type : 'string',
            mapping:'course.lectureHours'
        },{
            name : 'credits',
            type : 'string',
            mapping:'course.credits'
        },{
            name : 'varCredit',
            type : 'string',
            mapping:'course.varCredit',
            convert: function(value, record) {
                if(value=='Y' || value==true){
                    return true;
                }
                else{
                    return false;
                }
            }
        },{
            name : 'varCreditHrs',
            type : 'string',
            mapping:'course.varCreditHrs'
        },{
            name : 'ssn',
            type : 'string',
            mapping:'course.ssn'
        },{
            name : 'maxCredit',
            type : 'string',
            mapping:'course.maxCredit'
        },{
            name : 'minCredit',
            type : 'string',
            mapping:'course.minCredit'
        },{
            name : 'meeting',
            type : 'array',
            mapping:'course.meetingList.meeting'
        },{
            name: 'statusMessage',
            type:'string'
        },{
            name: 'facultyEmail',
            mapping:'course.facultyEmail',
            type:'string'
        },{
            name:'courseId',
            convert: function(value, record) {
                return record.get('subject') + record.get('courseNumber');
            }
        },{
            name:'courseDet',
            convert: function(value, record) {
                return record.get('subject') + record.get('courseNumber')+ record.get('ssn')+ record.get('termCode');
            }
        }
        ]
    }
});