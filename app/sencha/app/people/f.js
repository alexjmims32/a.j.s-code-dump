Ext.define('mobEdu.people.f', {
    requires: [
        'mobEdu.courses.store.classmates',
        'mobEdu.courses.store.faculty',
        'mobEdu.courses.store.advisors',
        'mobEdu.people.store.menu',
        'mobEdu.people.store.termList',
        'mobEdu.people.store.courseDetails',
        'mobEdu.profile.store.myInfo',
        'mobEdu.courses.store.courseList'
    ],
    statics: {
        showPeopleView: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.people.view.peopleList');
            mobEdu.util.setTitle('PEOPLE', Ext.getCmp('PeopleTitle'));
        },
        showMyClassmates: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.people.view.classmates');
        },
        showMyFaculty: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.people.view.faculty');
        },
        showAdvisorsBackButton: function() {
            var store = mobEdu.util.getStore('mobEdu.courses.store.advisors');
            store.removeAll();
            store.removed = [];
            store.sync();
            mobEdu.util.showMainView();
        },
        showMyAdvisors: function() {
            mobEdu.util.updatePrevView(mobEdu.people.f.showAdvisorsBackButton);
            mobEdu.util.show('mobEdu.people.view.advisors');
        },
        initializePeopleView: function() {
            var store = mobEdu.util.getStore('mobEdu.people.store.menu');
            store.load();
            store.removeAll();
            store.removed = [];
            store.getProxy().clear();
            store.sync();
            store.add({
                title: 'My Classmates',
                action: mobEdu.people.f.loadTermList
            });
            store.sync();
            store.add({
                title: 'My Faculty',
                action: mobEdu.people.f.loadTermListForFaculty
            });
            store.sync();

            store.add({
                title: 'My Advisors',
                action: mobEdu.people.f.loadAdvisors
            });
            store.sync();
        },
        loadTermList: function() {
            var tStore = mobEdu.util.getStore('mobEdu.people.store.termList');
            if (tStore.getCount() > 0) {
                tStore.removeAll();
                tStore.removed = [];
            }
            mobEdu.util.get('mobEdu.people.view.classmates');
            tStore.getProxy().setUrl(webserver + 'getMyCoursesTerms');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = function() {
                mobEdu.people.f.termListResponseHandler();
            }
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'mycoursesterms');
        },
        termListResponseHandler: function() {
            var termStore = mobEdu.util.getStore('mobEdu.people.store.termList');
            if (termStore.data.length > 0) {
                var term = new Array();
                var index = 1;
                term[0] = ({
                    text: 'Select Term',
                    value: '0'
                });
                termStore.each(function(record) {
                    term[index] = ({
                        text: record.data.description,
                        value: record.data.code
                    });
                    index = index + 1;
                });
                Ext.getCmp('clsm').suspendEvents();
                Ext.getCmp('clsm').setOptions(term);
                Ext.getCmp('clsm').setValue('0');
                Ext.getCmp('clsm').resumeEvents();
                Ext.getCmp('CcourseList').setOptions([{
                    text: 'Select Course',
                    value: '0'
                }]);
                Ext.getCmp('CcourseList').select(0);
                mobEdu.people.f.showMyClassmates();
            } else {
                Ext.Msg.show({
                    message: '<p>You do not have courses registered in any term.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });

            }
        },
        loadClassmates: function(selectfield, newValue, oldValue, eOpts) {
            var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
            if (classmatesStore.data.length > 0) {
                classmatesStore.removeAll();
                classmatesStore.removed = [];
            }
            classmatesStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            classmatesStore.getProxy().setUrl(webserver + 'getClassmates');
            classmatesStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                crn: newValue,
                termCode: mobEdu.util.getCoursesTermCode(),
                companyId: companyId
            });
            classmatesStore.getProxy().afterRequest = function() {
                mobEdu.people.f.classmatesResponseHandler();
            };
            classmatesStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'classmates');
        },
        classmatesResponseHandler: function() {
            var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
            classmatesStore.sort('name', 'ASC');
        },
        showClassmatesDetails: function() {
            mobEdu.util.updatePrevView(mobEdu.people.f.showMyClassmates);
            mobEdu.util.show('mobEdu.profile.view.myInfo');
        },
        onTermSelectionChange: function(selectfield, newValue, oldValue, eOpts) {
            //mobEdu.util.deselectSelectedItem(index, view);
            mobEdu.util.updateCoursesTermCode(newValue);
            mobEdu.people.f.loadRegisteredCourses(newValue);
        },
        loadRegisteredCourses: function(termCode, flag) {
            var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
            if (coursesStore.data.length > 0) {
                coursesStore.removeAll();
                coursesStore.removed = [];
            }
            coursesStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            coursesStore.getProxy().setUrl(webserver + 'getRegisteredCourses');
            coursesStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                termCode: termCode,
                companyId: companyId
            });
            coursesStore.getProxy().afterRequest = function() {
                mobEdu.people.f.registeredCourseResponseHandler(flag);
            };
            coursesStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'registeredcourses');
        },
        registeredCourseResponseHandler: function(flag) {
            var index = 1;
            var term = new Array();
            var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
            if (coursesStore.data.length == 0) {
                mobEdu.util.showMainView();
                Ext.Msg.show({
                    message: '<p>You do not have courses registered in the selected term.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                if (coursesStore.data.length > 0) {
                    term[0] = ({
                        text: 'Select Course',
                        value: '0'
                    });
                    coursesStore.data.each(function(record) {
                        var crseRec = coursesStore.data.items[index - 1].data;
                        term[index] = ({
                            text: crseRec.courseTitle,
                            value: crseRec.crn
                        });
                        index = index + 1;
                    });
                    Ext.getCmp('CcourseList').suspendEvents();
                    Ext.getCmp('CcourseList').setOptions(term);
                    //                    Ext.getCmp('courseList').setValue('0');
                    Ext.getCmp('CcourseList').resumeEvents();
                    Ext.getCmp('CcourseList').select(0);
                } else {
                    mobEdu.util.get('mobEdu.courses.view.phone.courseList');
                    if (coursesStore.data.length > 0) {
                        var crseRec = coursesStore.data.items[0].data;
                        mobEdu.util.setTitle(null, Ext.getCmp('coursesP'), crseRec.termDescription);
                    }
                }
            }
        },
        onFacultyTermSelection: function(selectfield, newValue, oldValue, eOpts) {
            //mobEdu.util.deselectSelectedItem(index, view);
            mobEdu.util.updateCoursesTermCode(newValue);
            mobEdu.people.f.loadFaculty();
            // mobEdu.people.f.loadFacultyRegisteredCourses(newValue);
        },
        loadFacultyRegisteredCourses: function(termCode, flag) {
            var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
            if (coursesStore.data.length > 0) {
                coursesStore.removeAll();
                coursesStore.removed = [];
            }
            coursesStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            coursesStore.getProxy().setUrl(webserver + 'getRegisteredCourses');
            coursesStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                termCode: termCode,
                companyId: companyId
            });
            coursesStore.getProxy().afterRequest = function() {
                mobEdu.people.f.facultyRegisteredCourseResponseHandler(flag);
            };
            coursesStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'registeredcourses');
        },
        facultyRegisteredCourseResponseHandler: function(flag) {
            var index = 1;
            var term = new Array();
            var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
            if (coursesStore.data.length == 0) {
                mobEdu.util.showMainView();
                Ext.Msg.show({
                    message: '<p>You do not have courses registered in the selected term.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                if (coursesStore.data.length > 0) {
                    term[0] = ({
                        text: 'Select Course',
                        value: '0'
                    });
                    coursesStore.data.each(function(record) {
                        var crseRec = coursesStore.data.items[index - 1].data;
                        term[index] = ({
                            text: crseRec.courseTitle,
                            value: crseRec.crn
                        });
                        index = index + 1;
                    });
                    Ext.getCmp('FcourseList').suspendEvents();
                    Ext.getCmp('FcourseList').setOptions(term);
                    //                    Ext.getCmp('courseList').setValue('0');
                    Ext.getCmp('FcourseList').resumeEvents();
                    Ext.getCmp('FcourseList').select(0);
                } else {
                    mobEdu.util.get('mobEdu.courses.view.phone.courseList');
                    if (coursesStore.data.length > 0) {
                        var crseRec = coursesStore.data.items[0].data;
                        mobEdu.util.setTitle(null, Ext.getCmp('coursesP'), crseRec.termDescription);
                    }
                }
            }
        },
        loadTermListForFaculty: function() {
            var tStore = mobEdu.util.getStore('mobEdu.people.store.termList');
            if (tStore.getCount() > 0) {
                tStore.removeAll();
                tStore.removed = [];
            }
            mobEdu.util.get('mobEdu.people.view.faculty');
            tStore.getProxy().setUrl(webserver + 'getMyCoursesTerms');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = function() {
                mobEdu.people.f.termListResponseHandlerForFaculty();
            }
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'mycoursesterms');
        },
        termListResponseHandlerForFaculty: function() {
            var termStore = mobEdu.util.getStore('mobEdu.people.store.termList');
            if (termStore.data.length > 0) {
                var term = new Array();
                var index = 1;
                term[0] = ({
                    text: 'Select Term',
                    value: '0'
                });
                termStore.each(function(record) {
                    term[index] = ({
                        text: record.data.description,
                        value: record.data.code
                    });
                    index = index + 1;
                });
                Ext.getCmp('fact').suspendEvents();
                Ext.getCmp('fact').setOptions(term);
                Ext.getCmp('fact').setValue('0');
                Ext.getCmp('fact').resumeEvents();
                //                Ext.getCmp('clsm').select(0);
                // Ext.getCmp('FcourseList').setOptions([{
                //     text: 'Select Course',
                //     value: '0'
                // }]);
                // Ext.getCmp('FcourseList').select(0);
            }
            mobEdu.people.f.showMyFaculty();
        },
        loadFaculty: function(selectfield, newValue, oldValue, eOpts) {
            var facultyStore = mobEdu.util.getStore('mobEdu.courses.store.faculty');
            if (facultyStore.data.length > 0) {
                facultyStore.removeAll();
                facultyStore.removed = [];
            }
            facultyStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            facultyStore.getProxy().setUrl(webserver + 'getFaculty');
            facultyStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                crn: newValue,
                termCode: mobEdu.util.getCoursesTermCode(),
                companyId: companyId
            });
            facultyStore.getProxy().afterRequest = function() {
                mobEdu.people.f.facultyResponseHandler();
            };
            facultyStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'faculty');
        },
        facultyResponseHandler: function() {
            var facultyStore = mobEdu.util.getStore('mobEdu.courses.store.faculty');
            facultyStore.sort('name', 'ASC');
            mobEdu.people.f.showMyFaculty();
        },
        loadTermListForAdvisors: function() {
            var tStore = mobEdu.util.getStore('mobEdu.people.store.termList');
            if (tStore.getCount() > 0) {
                tStore.removeAll();
                tStore.removed = [];
            }
            mobEdu.util.get('mobEdu.people.view.advisors');
            tStore.getProxy().setUrl(webserver + 'getMyCoursesTerms');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = function() {
                mobEdu.people.f.termListResponseHandlerForAdvisors();
            }
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'mycoursesterms');
        },
        termListResponseHandlerForAdvisors: function() {
            var termStore = mobEdu.util.getStore('mobEdu.people.store.termList');
            if (termStore.data.length > 0) {
                var term = new Array();
                var index = 1;
                term[0] = ({
                    text: 'Select Term',
                    value: '0'
                });
                termStore.each(function(record) {
                    term[index] = ({
                        text: record.data.description,
                        value: record.data.code
                    });
                    index = index + 1;
                });
                Ext.getCmp('advrs').suspendEvents();
                Ext.getCmp('advrs').setOptions(term);
                Ext.getCmp('advrs').setValue('0');
                Ext.getCmp('advrs').resumeEvents();
                ////                Ext.getCmp('clsm').select(0);
                //                Ext.getCmp('courseList').setOptions([{
                //                        text: 'Select Course',
                //                        value: '0'
                //                    }]);
                //                Ext.getCmp('courseList').select(0);
                mobEdu.people.f.showMyAdvisors();
            } else {
                Ext.Msg.show({
                    message: '<p>You do not have courses registered in any term.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });

            }
        },
        loadAdvisors: function(selectfield, newValue, oldValue, eOpts) {
            mobEdu.util.updateCoursesTermCode(newValue);
            var advisorsStore = mobEdu.util.getStore('mobEdu.courses.store.advisors');
            if (advisorsStore.data.length > 0) {
                advisorsStore.removeAll();
                advisorsStore.removed = [];
            }
            advisorsStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            advisorsStore.getProxy().setUrl(webserver + 'getAdvisors');
            advisorsStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                // termCode: mobEdu.util.getCoursesTermCode(),
                companyId: companyId
            });
            advisorsStore.getProxy().afterRequest = function() {
                mobEdu.people.f.advisorsResponseHandler();
            };
            advisorsStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'advisors');
        },
        advisorsResponseHandler: function() {
            var advisorsStore = mobEdu.util.getStore('mobEdu.courses.store.advisors');
            advisorsStore.sort('name', 'ASC');
            mobEdu.people.f.showMyAdvisors();
        },
        showFacultyDetails: function(dirTitle) {
            mobEdu.util.updatePrevView(mobEdu.people.f.showMyFaculty);
            mobEdu.util.show('mobEdu.profile.view.myInfo');
        },
        showAdvisorsDetails: function(dirTitle) {
            mobEdu.util.updatePrevView(mobEdu.people.f.showMyAdvisors);
            mobEdu.util.show('mobEdu.profile.view.myInfo');
        },
        loadClasmatesInfo: function(view, index, item, e, record) {
            var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
            store.getProxy().setUrl(webserver + 'getContact');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: record.data.id,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.people.f.myClassmatesResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'contact');
        },
        myClassmatesResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
            if (store.data.length > 0) {
                var record = store.getAt(0);
                var title = record.get('title');
                if (title === null || title === '') {
                    title = record.get('fullName');
                }
            }
            mobEdu.util.get('mobEdu.profile.view.myInfo');
            if (title === undefined) {
                mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), 'My Info');
            } else {
                mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), title);
            }
            if (store.data.length === 0) {
                Ext.Msg.show({
                    message: 'No information available.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }

            mobEdu.people.f.showClassmatesDetails();

        },
        loadFacultyInfo: function(view, index, item, e, record) {
            var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
            store.getProxy().setUrl(webserver + 'getContact');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: record.data.id,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.people.f.myFacultyResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'contact');
        },
        myFacultyResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
            if (store.data.length > 0) {
                var record = store.getAt(0);
                var title = record.get('title');
                if (title === null || title === '') {
                    title = record.get('fullName');
                }
            }
            mobEdu.util.get('mobEdu.profile.view.myInfo');
            if (title === undefined) {
                mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), 'My Info');
            } else {
                mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), title);
            }
            if (store.data.length == 0) {
                Ext.Msg.show({
                    message: 'No information available.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
            mobEdu.people.f.showFacultyDetails();

        },
        loadAdvisorsInfo: function(view, index, item, e, record) {
            var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
            store.getProxy().setUrl(webserver + 'getContact');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: record.data.id,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.people.f.myAdvisorsResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'contact');
        },
        myAdvisorsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
            if (store.data.length > 0) {
                var record = store.getAt(0);
                var title = record.get('title');
                if (title === null || title === '') {
                    title = record.get('fullName');
                }
            }
            mobEdu.util.get('mobEdu.profile.view.myInfo');
            if (title === undefined) {
                mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), 'My Info');
            } else {
                mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), title);
            }
            if (store.data.length == 0) {
                Ext.Msg.show({
                    message: 'No information available.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
            mobEdu.people.f.showAdvisorsDetails();

        }
    }
});