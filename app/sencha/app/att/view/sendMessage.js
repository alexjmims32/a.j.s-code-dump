Ext.define('mobEdu.att.view.sendMessage', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.att.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'customToolbar',
                id: 'newETitle',
                name: 'newETitle',
                title: '<h1>Attendance</h1>'
            },
            {
                xtype: 'panel',
                id: 'studentName'
            },
            {
                xtype: 'fieldset',
                items: [
                    {
                        xtype: 'textfield',
                        label: 'Subject',
                        labelWidth: '25%',
                        name: 'newsub',
                        id: 'newsub',
                        required: true,
                        useClearIcon: true
                    },
                    {
                        xtype: 'textareafield',
                        label: 'Message',
                        labelWidth: '30%',
                        labelAlign: 'top',
                        name: 'newmsg',
                        id: 'newmsg',
                        maxRows: 15,
                        required: true
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [
                    {
                        text: 'Send',
                        align: 'right',
                        handler: function() {
                            mobEdu.att.f.sendMessage();
                        }
                    }
                ]
            }
        ]
    }
});