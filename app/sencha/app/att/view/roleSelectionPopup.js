Ext.define('mobEdu.att.view.roleSelectionPopup', {
	extend: 'Ext.form.Panel',
	config: {
		id: 'roleSelectionPopup',
		scrollable: true,
		floating: true,
		cls: 'crsPop',
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		docked: 'top',
		width: '25%',
		height: '25%',
		style: 'background-color: transparent;',
		layout: {
			type: 'vbox',
			pack: 'middle'
		},
		padding: '5 5 0 5',
		items: [{
			xtype: 'radiofield',
			name: 'Student',
			id: 'studentRF',
			label: 'Student',
			labelWidth: '75%',
			listeners: {
				check: function(radiofield, e, eOpts) {
					mobEdu.att.f.getCurrentClass('STUDENT');
				}
			}
		}, {
			xtype: 'radiofield',
			name: 'Faculty',
			id: 'facultyRF',
			label: 'Faculty',
			labelWidth: '75%',
			listeners: {
				check: function(radiofield, e, eOpts) {
					mobEdu.att.f.getCurrentClass('FACULTY');
				}
			}
		}],
		flex: 1,
		listeners: {
			hide: function() {
				mobEdu.att.f.hideRoleSelectionPopup();
			}
		}
	}
});
