Ext.define('mobEdu.att.view.studentList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.att.f',
		'mobEdu.att.store.studentAttendance',
		'mobEdu.att.store.termListLocal'
	],
	config: {
		scrollable: true,
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Attendance</h1>'
		}, {
			xtype: 'list',
			// id: 'attendanceReport',
			itemTpl: new Ext.XTemplate('<table width="100%"><tr>' +
				'<tpl if="mobEdu.att.f.isShowCheckBox()===true">',
				'<td width="42px">',
				'<tpl if="mobEdu.att.f.isAttendancePresent(attIndicator)===true">',
				'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" height=24 id="on" name="chkAttendS"  />' +
				'<tpl else>',
				'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" height=24 id="off" name="chkAttendS" />',
				'</tpl>',
				'</td>',
				'</tpl>' +
				'<td>' +
				'<tpl if="(mobEdu.att.f.isDateRoster()=== true)">',
				'<h3>{name}</h3>',
				'<tpl else><h3>{date}</h3></tpl>' +
				'</td></tr></table>'),
			cls: 'logo',
			grouped: true,
			store: mobEdu.util.getStore('mobEdu.att.store.studentAttendance'),
			singleSelect: false,
			disableSelection: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					mobEdu.att.f.onItemTap(view, target, index, item, e, record);
				}
			}
		}, {
			xtype: 'toolbar',
			id: 'updateTbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Update',
				handler: function() {
					mobEdu.att.f.updateAttendanceInfo();
				}
			}]
		}],
		flex: 1
	}
});