Ext.define('mobEdu.att.view.codeGenerator', {
    extend: 'Ext.form.Panel',
    statics: {
        tablet: {
            landscape: {
                width: '30%',
                height: '40%'
            },
            portrait: {
                width: '40%',
                height: '30%'
            }
        },
        phone: {
            landscape: {
                width: '50%',
                height: '85%'
            },
            portrait: {
                width: '80%',
                height: '50%'
            }
        }
    },   
    config: {
        id: 'codePopup',
        scrollable: false,
        floating: true,
        modal: true,
        centered: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '50%',
        height: '50%',
        style: 'background-color: transparent;',
        layout: {
            type: 'vbox',
            pack: 'middle'
        },
        padding: '5 5 0 5',
        items: [{
            id: 'codeAndTime',
            name: 'codeAndTime',
            html: '',
            padding: '0 0 10 0',
        }, {
            margin: 5,
            xtype: 'button',
            text: '<h7>Extend Time 30 Seconds</h7>',
            id: 'btnExtendTime',
            ui: 'action',
            handler: function() {
                mobEdu.att.f.ExtendendTimer();
            }
        }],
        flex: 1
    },

    initialize: function() {
        // Add a Listener. Listen for [Viewport ~ Orientation] Change.
        Ext.Viewport.on('orientationchange', 'handleOrientationChange', this);
        this.callParent(arguments);
    },

    handleOrientationChange: function(viewport, orientation, width, height) {
        // Execute the code that needs to fire on Orientation Change.
        mobEdu.util.onOrientationChange(orientation, this, mobEdu.att.view.codeGenerator);
    }
});