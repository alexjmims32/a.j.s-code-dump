Ext.define('mobEdu.att.view.sendMessageToClass', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.att.f',
        'mobEdu.att.store.classDetails'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'customToolbar',
                id: 'newETitle',
                name: 'newETitle',
                title: '<h1>Send Message To Class</h1>'
            },
            {
                xtype: 'panel',
                id: 'studentName'
            },
            {
                xtype: 'fieldset',
                defaults: {
                    labelWidth: '95%',
                    labelAlign: 'right'
                },
                items: [
                    {
                        xtype: 'radiofield',
                        name: 'subject',
                        id:'classCancel',
                        value: 'Class Cancelled',
                        label: 'Class Cancelled',
                        checked: true,
                        listeners: {
                            change: function(subject, e, eOpts) {
                                mobEdu.att.f.showSubjectTextBox(subject, e, eOpts);
                            }
                        }
                    },
                    {
                        xtype: 'radiofield',
                        name: 'subject',
                        id:'timeLocation',
                        value: 'Time or Location Change',
                        label: 'Time or Location Change',
                        listeners: {
                            change: function(subject, e, eOpts) {
                                mobEdu.att.f.showSubjectTextBox(subject, e, eOpts);
                            }
                        }
                    },
                    {
                        xtype: 'panel',
                        layout: 'hbox',
                        items: [
                            {
                                xtype: 'radiofield',
                                name: 'subject',
                                id:'manulsub',
                                value: 'newSubject',
                                listeners: {
                                    change: function(subject, e, eOpts) {
                                        mobEdu.att.f.showSubjectTextBox(subject, e, eOpts);
                                    }
                                }
                            },
                            {
                                xtype: 'textfield',
                                name: 'newsub',
                                placeHolder: '[Enter Subject Here]',
                                readOnly: true,
                                id: 'subjectText'
                            }
                        ]
                    },
                    {
                        xtype: 'textareafield',
                        label: 'Message',
                        labelWidth: '30%',
                        labelAlign: 'top',
                        name: 'newmsg',
                        id: 'newmsg',
                        maxRows: 15,
                        required: true
                    }
                ]
            },
            {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [
                    {
                        text: 'Send',
                        align: 'right',
                        handler: function() {
                            mobEdu.att.f.sendMailToClass();
                        }
                    }
                ]
            }
        ]
    }
});