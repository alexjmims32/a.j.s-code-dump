Ext.define('mobEdu.att.view.menu', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.att.store.attdMenu'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.att.store.attdMenu'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>Attendance</h1>'
        },{
            xtype: 'fieldset',
            docked: 'top',
            cls: 'searchfieldset',
            items: [{
                xtype: 'selectfield',
                name: 'attendanceRole',
                id: 'attendanceRole',
                listeners: {
                    change: function(selectbox, newValue, oldValue) {
                        mobEdu.att.f.loadMenu(selectbox, newValue, oldValue);
                    }
                }
            }]
        }],
        flex: 1
    }
});