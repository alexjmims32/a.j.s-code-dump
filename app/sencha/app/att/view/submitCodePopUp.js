Ext.define('mobEdu.att.view.submitCodePopUp', {
    extend: 'Ext.form.Panel',
    statics: {
        tablet: {
            landscape: {
                width: '30%',
                height: '20%'
            },
            portrait: {
                width: '40%',
                height: '15%'
            }
        },
        phone: {
            landscape: {
                width: '50%',
                height: '65%'
            },
            portrait: {
                width: '80%',
                height: '30%'
            }
        }
    },
    config: {
        id: 'submitCodePopUp',
        scrollable: false,
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '25%',
        height: '25%',
        style: 'background-color: transparent;',
        layout: {
            type: 'vbox',
            pack: 'middle'
        },
        padding: '5 5 0 5',
        items: [{
            xtype: 'textfield',
            name: 'submitCode',
            id: 'submitCode',
            placeHolder: 'Enter Code',
            clearIcon: true,
            autoCapitalize: false,
            required: true,
            margin: 5,
            listeners: {
                blur: function(textfield, e, eOpts) {
                    mobEdu.util.hideKeyboard();
                }
            }
        }, {
            margin: 5,
            xtype: 'button',
            text: '<h7>Submit</h7>',
            ui: 'action',
            handler: function() {
                mobEdu.att.f.submitedTodyCode();
            }
        }],
        flex: 1
    },

    initialize: function() {
        // Add a Listener. Listen for [Viewport ~ Orientation] Change.
        Ext.Viewport.on('orientationchange', 'handleOrientationChange', this);
        this.callParent(arguments);

    },

    handleOrientationChange: function(viewport, orientation, width, height) {
        // Execute the code that needs to fire on Orientation Change.
        mobEdu.util.onOrientationChange(orientation, this, mobEdu.att.view.submitCodePopUp);
    }

});