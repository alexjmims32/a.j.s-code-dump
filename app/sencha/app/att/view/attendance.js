Ext.define('mobEdu.att.view.attendance', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.att.f',
		'mobEdu.att.store.studentAttendance',
		'mobEdu.att.store.termListLocal'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Attendance</h1>'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset',
			items: [{
				xtype: 'selectfield',
				name: 'attTermList',
				id: 'attTermList',
				valueField: 'code',
				displayField: 'description',
				store: mobEdu.util.getStore('mobEdu.att.store.termListLocal'),
				listeners: {
					change: function(selectbox, newValue, oldValue) {
						mobEdu.att.f.loadAttdClassList(selectbox, newValue, oldValue);
					}
				}
			}, {
				xtype: 'datepickerfield',
				labelWidth: '50%',
				label: 'Date',
				name: 'attdDate',
				id: 'attdDate',
				required: true,
				placeHolder: 'Select',
				useClearIcon: true,
				//                value: new Date(),
				listeners: {
					change: function(datePicker, value, eOpts) {
						mobEdu.att.f.onAttdDateChange(datePicker, value, eOpts);
					}
				}
			}, {
				xtype: 'selectfield',
				name: 'attClassList',
				id: 'attClassList',
				listeners: {
					change: function(selectbox, newValue, oldValue) {
						mobEdu.att.f.loadAttdStudentList(selectbox, newValue, oldValue);
					}
				}
			}, {
				xtype: 'selectfield',
				name: 'attStdList',
				id: 'attStdList',
				listeners: {
					change: function(selectbox, newValue, oldValue) {
						mobEdu.att.f.attStoreClear();
					}
				}
			}, {
				xtype: 'datepickerfield',
				labelWidth: '50%',
				label: 'From Date',
				name: 'attFromDate',
				id: 'attFromDate',
				required: true,
				placeHolder: 'Select',
				useClearIcon: true,
				value: new Date(),
				listeners: {
					change: function(datePicker, value, eOpts) {
						mobEdu.att.f.onAttdFromDateChange(datePicker, value, eOpts);
					}
				}
			}, {
				xtype: 'datepickerfield',
				labelWidth: '50%',
				label: 'To Date',
				name: 'attToDate',
				id: 'attToDate',
				required: true,
				placeHolder: 'Select',
				useClearIcon: true,
				value: new Date(),
				listeners: {
					change: function(datePicker, value, eOpts) {
						mobEdu.att.f.onAttdToDateChange(datePicker, value, eOpts);
					}
				}
			}]
		}, {
			xtype: 'list',
			id: 'attendanceReport',
			itemTpl: new Ext.XTemplate('<table width="100%"><tr>' +
				'<tpl if="mobEdu.att.f.isShowCheckBox()===true">',
				'<td width="42px">',
				'<tpl if="mobEdu.att.f.isAttendancePresent(attIndicator)===true">',
				'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" height=24 id="on" name="chkAttend"  />' +
				'<tpl else>',
				'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" height=24 id="off" name="chkAttend" />',
				'</tpl>',
				'</td>',
				'</tpl>' +
				'<td>' +
				'<tpl if="(mobEdu.att.f.isDateRoster()=== true)">',
				'<h3>{name}</h3>',
				'<tpl else><h3>{date}</h3></tpl>' +
				'</td></tr></table>'),
			cls: 'logo',
			grouped: true,
			store: mobEdu.util.getStore('mobEdu.att.store.studentAttendance'),
			singleSelect: false,
			disableSelection: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					mobEdu.att.f.onItemTap(view, target, index, item, e, record);
				}
			}
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Search',
				id: 'attdSearch',
				handler: function() {
					mobEdu.att.f.loadReport();
				}
			}, {
				text: 'Update',
				id: 'updateAttd',
				handler: function() {
					mobEdu.att.f.updateAttendanceInfo();
				}
			}]
		}],
		flex: 1
	}
});