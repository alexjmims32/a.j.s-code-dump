Ext.define('mobEdu.att.f', {
	requires: [
		'mobEdu.main.store.term',
		'mobEdu.att.store.menu',
		'mobEdu.att.store.classDetails',
		'mobEdu.att.store.attendanceInfo',
		'mobEdu.att.store.studentDetails',
		'mobEdu.att.store.attStatus',
		'mobEdu.att.store.mettingScheduleLocal',
		'mobEdu.att.store.studentAttendance',
		'mobEdu.att.store.codeGenerate',
		'mobEdu.att.store.studentNames',
		'mobEdu.att.store.attdMenu',
		'mobEdu.courses.store.courseListLocal',
		'mobEdu.courses.store.classmates',
		'mobEdu.att.store.mettingScheduleLocal',
		'mobEdu.att.store.termList',
		'mobEdu.att.store.currentClass',
		'mobEdu.att.store.termListLocal'
	],
	statics: {
		prevView: null,
		isAttendsCountFind: 'N',
		attendancePresentCount: '0',
		attendanceAbsentCount: '0',
		selectedAttdDate: '',
		slectedDate: '',
		popupStartDate: '',
		popupEndDate: '',
		attendanceFlag: '',
		ticker: null,
		timeInSecs: null,
		attPrevView: null,
		timeExtenderCount: 0,
		defaultTerm: null,

		checkRolePriorToGetCurrentClass: function() {
			var role = mobEdu.util.getRole();
			if (role.match(/faculty/gi) && role.match(/student/gi)) {
				mobEdu.att.f.showRoleSelectionPopUp();
			} else {
				if (role.match(/student/gi)) {
					role = 'STUDENT';
				} else {
					role = 'FACULTY';
				}
				mobEdu.att.f.getCurrentClass(role);
			}
		},

		showRoleSelectionPopUp: function() {
			var popupView = mobEdu.util.get('mobEdu.att.view.roleSelectionPopup');
			Ext.getCmp('studentRF').setChecked(false);
			Ext.getCmp('facultyRF').setChecked(false);
			Ext.Viewport.add(popupView);
			popupView.show();
		},

		hideRoleSelectionPopup: function() {
			var popUp = Ext.getCmp('roleSelectionPopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.courses.view.roleSelectionPopup');
			}
		},

		getCurrentClass: function(role) {
			if (role == undefined || role == '') {
				var role = mobEdu.util.getRole();
				if (role.indexOf("FACULTY") != -1) {
					role = 'FACULTY';
				} else {
					role = 'STUDENT';
				}
			}
			mobEdu.att.f.hideRoleSelectionPopup();
			mobEdu.util.updateCurrentRole(role);
			var coursesStore = mobEdu.util.getStore('mobEdu.att.store.currentClass');
			coursesStore.getProxy().setUrl(webserver + 'getCurrentClass');
			coursesStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			coursesStore.getProxy().setExtraParams({
				userId: mobEdu.main.f.getStudentId(),
				role: role,
				companyId: companyId
			});
			coursesStore.getProxy().afterRequest = function() {
				mobEdu.att.f.currentClassResponseHandler();
			};
			coursesStore.load();
			mobEdu.util.gaTrackEvent('attendance', 'currentClass');

		},

		currentClassResponseHandler: function() {
			var coursesStore = mobEdu.util.getStore('mobEdu.att.store.currentClass');
			if (coursesStore.data.length > 0) {
				var rec = coursesStore.getAt(0);
				var currDate = new Date();
				var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
				store.removeAll();
				store.removed = [];
				var time = rec.get('time').split('-');
				var title = rec.get('courseTitle');
				var termCode = rec.get('termCode');
				var crn = rec.get('crn');
				store.add({
					'title': title,
					'startTime': time[0],
					'endTime': time[1],
					'termCode': termCode,
					'crn': crn
				});
				store.sync();
				mobEdu.att.f.attPrevView = mobEdu.util.showMainView;
				if (mobEdu.courses.f.isFacultyRole() == true) {
					Ext.Msg.show({
						message: '<p>Do you want to generate code for<br/><br/><b>' + title + '( ' + crn + ' ).' + '</b></p>',
						buttons: Ext.MessageBox.YESNO,
						cls: 'msgbox',
						fn: function(btn) {
							if (btn == 'yes') {
								mobEdu.courses.f.generateCode(title, time[0], time[1], termCode, crn, currDate);
							}
						}
					});
				} else {
					Ext.Msg.show({
						message: '<p>Do you want to submit code for<br/><br/><b>' + title + '( ' + crn + ' ).' + '</b></p>',
						buttons: Ext.MessageBox.YESNO,
						cls: 'msgbox',
						fn: function(btn) {
							if (btn == 'yes') {
								mobEdu.att.f.showSubmitCodePopup();
							}
						}
					});
				}
			} else {
				Ext.Msg.show({
					message: '<p>No classes at this time.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadClassRosterDetails: function() {
			mobEdu.att.f.attendenceFlag = 'todayRoster';
			var clsRosterStore = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			mobEdu.att.f.loadAttendanceReport(clsRosterStore.data.items[0].data.crn, clsRosterStore.data.items[0].data.termCode, mobEdu.att.f.formatTimeToSave(clsRosterStore.data.items[0].data.startTime), mobEdu.att.f.formatTimeToSave(clsRosterStore.data.items[0].data.endTime), '', '', '');
			mobEdu.util.get('mobEdu.att.view.attendance');
			Ext.getCmp('attClassList').hide();
			var term = new Array();
			term[0] = ({
				text: clsRosterStore.data.items[0].data.crn + '-' + mobEdu.att.f.formatTimeToSave(clsRosterStore.data.items[0].data.startTime) + '-' + mobEdu.att.f.formatTimeToSave(clsRosterStore.data.items[0].data.endTime) + '-' + clsRosterStore.data.items[0].data.termCode,
				value: clsRosterStore.data.items[0].data.crn + '-' + mobEdu.att.f.formatTimeToSave(clsRosterStore.data.items[0].data.startTime) + '-' + mobEdu.att.f.formatTimeToSave(clsRosterStore.data.items[0].data.endTime) + '-' + clsRosterStore.data.items[0].data.termCode
			});
			Ext.getCmp('attClassList').suspendEvents();
			Ext.getCmp('attClassList').setOptions(term);
			Ext.getCmp('attClassList').setValue(term[0]);
			Ext.getCmp('attClassList').resumeEvents();

			//Ext.getCmp('attClassList').setValue(clsRosterStore.data.all[0].data.crn + '-' + mobEdu.att.f.formatTimeToSave(clsRosterStore.data.all[0].data.startTime) + '-' + mobEdu.att.f.formatTimeToSave(clsRosterStore.data.all[0].data.endTime) + '-' + clsRosterStore.data.all[0].data.termCode);
			Ext.getCmp('attTermList').hide();
			Ext.getCmp('attStdList').hide();
			Ext.getCmp('attdDate').hide();
			Ext.getCmp('attFromDate').hide();
			Ext.getCmp('attToDate').hide();
			clsRosterStore.clearFilter();
			mobEdu.att.f.showAttendanceReport();
		},
		showViewDetails: function() {
			mobEdu.util.updatePrevView(mobEdu.att.f.showCourseViewDetails);
			mobEdu.util.show('mobEdu.att.view.viewClass');
		},
		showCourseViewDetails: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.courses.view.phone.courseList');
			} else {
				mobEdu.util.show('mobEdu.courses.view.tablet.courseList');
			}
		},
		onItemTap: function(view, target, index, item, e, record) {
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else if (item.target.id == "off") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
		},

		isAttendancePresent: function(attIndicator) {
			var presentStatus = false;
			if (attIndicator == 'Y') {
				presentStatus = true;
			} else {
				presentStatus = false;
			}
			return presentStatus;
		},
		updateAttendanceInfo: function() {
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.attStatus');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
			var selected = Ext.getCmp('attClassList').getValue();
			var selectedValues = selected.split('-');
			var attendanceListStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			var record = attendanceListStore.getAt(0);
			var items;
			if (mobEdu.att.f.attendenceFlag == 'student') {
				if (!Ext.os.is.Tablet) {
					items = Ext.DomQuery.select("img[name='chkAttendS']");
				} else {
					items = Ext.DomQuery.select("img[name='chkAttend']");
				}
			} else {
				items = Ext.DomQuery.select("img[name='chkAttend']");
			}
			var attendInfo = '';
			Ext.each(items, function(item, i) {
				if (item.id == "on" && attendanceListStore.getAt(i).data.attIndicator != 'Y') {
					attendInfo = attendInfo + attendanceListStore.getAt(i).data.studentId + '-Y,' + attendanceListStore.getAt(i).data.code + ';';
					attendanceListStore.getAt(i).data.attIndicator = 'Y';
				} else if (item.id == "off" && attendanceListStore.getAt(i).data.attIndicator != 'N') {
					attendInfo = attendInfo + attendanceListStore.getAt(i).data.studentId + '-N,' + attendanceListStore.getAt(i).data.code + ';';
					attendanceListStore.getAt(i).data.attIndicator = 'N';
				}
			});
			if (attendInfo == '') {
				Ext.Msg.show({
					message: 'Nothing to update.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				proxyStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				proxyStore.getProxy().setUrl(webserver + 'updateAttendance');
				proxyStore.getProxy().setExtraParams({
					values: attendInfo,
					studentId: mobEdu.main.f.getStudentId(),
					termCode: selectedValues[3],
					crn: selectedValues[0],
					overWriteBy: mobEdu.main.f.getStudentId(),
					overComments: null,
					companyId: companyId
				});
				proxyStore.getProxy().afterRequest = function() {
					mobEdu.att.f.updateAttendanceResponseHandler(selected[3]);
				}
				proxyStore.load();
				mobEdu.util.gaTrackEvent('attendance', 'updateAttendance');
			}
		},
		updateAttendanceResponseHandler: function(termCode) {
			mobEdu.util.updateCoursesTermCode(termCode);
			var store = mobEdu.util.getStore('mobEdu.att.store.attStatus');
			var status = store.getProxy().getReader().rawData.status;
			if (status.toString().toUpperCase() == 'SUCCESS') {
				Ext.Msg.show({
					message: 'Successfully updated.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
				mobEdu.att.f.resetAllCheckBox();
				var attendanceListStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
				attendanceListStore.removeAll();
				attendanceListStore.load();
			} else {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetAllCheckBox: function() {
			var items = Ext.DomQuery.select("img[name='absent']");
			Ext.each(items, function(item, i) {
				item.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.id = "off";
			});
			items = Ext.DomQuery.select("img[name='attended']");
			Ext.each(items, function(item, i) {
				item.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.id = "on";
			});
		},

		formatTimeToSave: function(time) {
			time = time.replace(':', '');
			if ((time.search('AM')) != -1) {
				time = time.replace('AM', '');
			} else if ((time.search('PM')) != -1) {
				time = time.replace('PM', '');
				if (new Number(time) < 1200)
					time = new Number(time) + 1200;
			} else {
				return time.toString();
			}
			return time.toString();
		},
		formatTimeShow: function(time) {
			time = time.toString();
			if (time.length < 4) {
				time = "0" + time;
			}
			var hours = time.substring(0, 2);
			var minutes = time.substring(2, 4);
			var meridian = "AM";
			if (new Number(hours) > 12) {
				meridian = "PM";
				hours = new Number(hours) - 12;
			}
			return hours + ":" + minutes + " " + meridian;
		},
		showAttDatePopup: function(tsPopupView) {
			mobEdu.att.f.hidePopup();
			if (!tsPopupView) {
				var popup = tsPopupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.att.view.attDatePopUp'));
				Ext.Viewport.add(tsPopupView);
			}
			tsPopupView.show();
		},

		hidePopup: function() {
			mobEdu.util.get('mobEdu.att.view.attDatePopUp');
			var popUp = Ext.getCmp('attPopup');
			popUp.hide();
			Ext.Viewport.unmask();
			var loginStore = mobEdu.util.getStore('mobEdu.main.store.login');
			var record = loginStore.getAt(0);
			var role = '';
			var userRole = record.get('role').split(',');
			for (var len = 0; len < userRole.length; len++) {
				if (userRole[len] == 'STUDENT') {
					role = 'STUDENT';
				} else if (userRole[len] == 'FACULTY') {
					role = 'FACULTY';
				}
			}
			if (role == 'STUDENT') {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
				mobEdu.util.show('mobEdu.att.view.classToCheckIn');
			} else if (role == 'FACULTY') {
				mobEdu.util.updatePrevView(mobEdu.att.f.showViewDetails);
				mobEdu.util.show('mobEdu.att.view.studentDetails');
			}

		},

		isAttendCountCalculated: function() {
			return mobEdu.att.f.isAttendsCountFind;
		},

		getAttendanceCount: function() {
			var presentCnt = '0';
			var absentCnt = '0';
			var filterStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			filterStore.each(function(record) {
				if (record.get('attIndicator') == 'Y') {
					presentCnt = new Number(presentCnt) + 1;
				} else if (record.get('attIndicator') == 'N') {
					absentCnt = new Number(absentCnt) + 1;
				}
			});
			mobEdu.att.f.attendancePresentCount = presentCnt;
			mobEdu.att.f.attendanceAbsentCount = absentCnt;
			mobEdu.att.f.isAttendsCountFind = 'Y';
		},

		generateCode: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.classDetails');
			var record = store.getAt(0);
			var sTime = record.get('startTime');
			var eTime = record.get('endTime');
			var codeGenerateStore = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			var date = new Date();
			if (attDate != null) {
				date = attDate;
			}
			codeGenerateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			codeGenerateStore.getProxy().setUrl(webserver + 'generateCode');
			codeGenerateStore.getProxy().setExtraParams({
				instructorId: mobEdu.main.f.getStudentId(), //'T00001617',
				termCode: record.get('termCode'), //'201380',
				crn: record.get('crn'), //'82865',
				startTime: mobEdu.att.f.formatTimeToSave(sTime), //'1500',
				endTime: mobEdu.att.f.formatTimeToSave(eTime), //'1700',
				attDate: Ext.Date.format(date, 'n/j/Y'),
				companyId: companyId
			});
			codeGenerateStore.getProxy().afterRequest = function() {
				mobEdu.att.f.generateCodeResponseHandler();
			};
			codeGenerateStore.load();
			mobEdu.util.gaTrackEvent('attendance', 'generateCode');
		},
		generateCodeResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			var status = store.data.all[0].data.status;
			if (status == "SUCCESS") {
				var code = store.data.all[0].data.code;
				mobEdu.att.f.showCodeGenerate(code);
			} else {
				//var message = status.split('\n', 1);
				//var statusmsg = message[0].split(":", 2);
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		codeGenerateBackButton: function() {
			clearInterval(mobEdu.att.f.ticker);
			document.getElementById("countdown").innerHTML = 120;
			mobEdu.att.f.showClassDetails();
		},
		showCodeGenerate: function(code) {
			var codeValue = code;
			mobEdu.util.get('mobEdu.att.view.codeGenerator');
			if (Ext.getCmp('btnExtendTime').isHidden()) {
				Ext.getCmp('btnExtendTime').show();
			}

			mobEdu.courses.f.showGenerateCodePopup();
			mobEdu.att.f.startTimer(120);
			Ext.getCmp('codeAndTime').setHtml('<table width="100%">' +
				'<tr><td style="height: 10px;"></td></tr>' +
				'<tr><td  align="center"><h3>Today\'s Code</h3></td></tr>' +
				'<tr><td style="height: 10px;"></td></tr>' +
				'<tr><td align="center"><h2 class="codeTextSize">' + code + '</h2></td></tr>' +
				'<tr><td style="height: 15px;"></td></tr>' +
				'<tr><td align="center"><h3>Time Remaining</h3></td></tr>' +
				'<tr><td style="height: 10px;"></td></tr>' +
				'<tr><td align="center" id="countdown" class="codeTextSize">120</td></tr>' +
				'<table>');
		},

		startTimer: function(secs) {
			mobEdu.att.f.timeInSecs = parseInt(secs) - 1;
			mobEdu.att.f.ticker = setInterval("mobEdu.att.f.tick()", 1000); // every second
		},
		tick: function() {
			var secs = mobEdu.att.f.timeInSecs;
			if (secs > 0) {
				document.getElementById("countdown").innerHTML = secs;
				mobEdu.att.f.timeInSecs--;
				return;
			}

			Ext.getCmp('codeAndTime').setHtml('');
			Ext.getCmp('btnExtendTime').hide();
			mobEdu.courses.f.hideGenerateCodePopup();
			//Ext.getCmp('codePopup').hide();
			clearInterval(mobEdu.att.f.ticker);
			mobEdu.att.f.loadClassRosterDetails();
		},

		ExtendendTimer: function() {
			if (mobEdu.att.f.timeExtenderCount > 0) {
				Ext.getCmp('btnExtendTime').hide();
				mobEdu.att.f.timeExtenderCount = 0;
			} else {
				mobEdu.att.f.timeExtenderCount = 1;
			}

			if (mobEdu.att.f.timeInSecs <= 0) {
				Ext.Msg.show({
					title: null,
					message: 'Time out. \nPlease try to generate code again.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
				return;
			}
			clearInterval(mobEdu.att.f.ticker);
			mobEdu.att.f.startTimer(parseInt(mobEdu.att.f.timeInSecs) + 31);
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			var record = store.data.items[0];
			var sTime = record.get('startTime');
			var eTime = record.get('endTime');
			var tcode = record.get('termCode');
			var crn = record.get('crn');
			var codeGenerateStore = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			var date = new Date();
			codeGenerateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			codeGenerateStore.getProxy().setUrl(webserver + 'extendTime');
			codeGenerateStore.getProxy().setExtraParams({
				instructorId: mobEdu.main.f.getStudentId(), //'T00001617',
				termCode: tcode, //'201380',
				crn: crn, //'82865',
				startTime: mobEdu.att.f.formatTimeToSave(sTime), //'1500',
				endTime: mobEdu.att.f.formatTimeToSave(eTime), //'1700',
				attDate: Ext.Date.format(date, 'm/d/Y'),
				extendTime: '30',
				companyId: companyId
			});
			codeGenerateStore.getProxy().afterRequest = function() {};
			codeGenerateStore.load();
			mobEdu.util.gaTrackEvent('attendance', 'extendTime');
		},

		submitedTodyCode: function() {
			var codeText = Ext.getCmp('submitCode').getValue();
			Ext.getCmp('submitCode').setValue('');
			if (codeText == '' || codeText == undefined) {
				mobEdu.att.f.hideSubmitCodePopup();
				Ext.Msg.show({
					title: null,
					message: 'Please enter today\'s code.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						mobEdu.att.f.showSubmitCodePopup();
					}
				});
			} else {
				mobEdu.att.f.sendtodaysCode(codeText);
			}
		},
		sendtodaysCode: function(code) {
			mobEdu.att.f.hideSubmitCodePopup();
			var sendCodeInstructor;
			var sendCodetermcode;
			var sendCodecrn;

			var sendCodeStore = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			sendCodeInstructor = sendCodeStore.data.items[0].data.instructorId;
			sendCodetermcode = sendCodeStore.data.items[0].data.termCode;
			sendCodecrn = sendCodeStore.data.items[0].data.crn;
			var enteredCode = code;
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			proxyStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			proxyStore.getProxy().setUrl(webserver + 'attendnceSubmitByStudent');
			proxyStore.getProxy().setExtraParams({
				instructorId: null,
				studentId: mobEdu.main.f.getStudentId(),
				termCode: sendCodetermcode,
				crn: sendCodecrn,
				attendanceCode: enteredCode,
				overWriteBy: null,
				overComments: null,
				values: mobEdu.main.f.getStudentId() + ',' + 'Y',
				companyId: companyId
			});
			proxyStore.getProxy().afterRequest = function() {
				mobEdu.att.f.sendtodaysCodeResponseHandler();
			}
			proxyStore.load();
			mobEdu.util.gaTrackEvent('attendance', 'attendnceSubmitByStudent');
		},
		sendtodaysCodeResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			var res = store.getProxy().getReader().rawData.status;
			if (res.toUpperCase() == 'SUCCESS') {
				//                Ext.getCmp('submitCodePopUp').hide();
				Ext.Msg.show({
					title: '<h1>Thank You</h1>',
					message: '<p>Today\'s code has been successfully submitted.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				//var message = res.split(":", 2);
				//var stsMessage = message[1].split(".", 1);
				Ext.Msg.show({
					title: '<h1>Sorry</h1>',
					message: res,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						mobEdu.att.f.showSubmitCodePopup();
					}
				});
			}
			mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal').clearFilter();
		},

		showSubmitCodePopup: function(popupView) {
			//            mobEdu.att.f.prevView = mobEdu.util.getPrevView();
			//            mobEdu.util.updatePrevView(mobEdu.att.f.hideSubmitCodePopup);
			var title;
			mobEdu.util.get('mobEdu.courses.view.attCrsePopup');
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			if (store.data.length > 1) {
				for (var i = 0; i < store.data.length; i++) {
					var check = Ext.getCmp('course' + i).isChecked();
					if (check) {
						title = store.data.all[i].data.title;
					};
				};
				store.filter(function(rec) {
					var title1 = rec.get('title');
					if (title == title1) {
						return true;
					}
					return false;
				});
			};
			mobEdu.courses.f.hideAttCoursePopup();
			if (!popupView) {
				var popupView = mobEdu.util.get('mobEdu.att.view.submitCodePopUp');
				Ext.Viewport.add(popupView);
			}
			mobEdu.util.onOrientationChange(Ext.Viewport.getOrientation(), popupView, mobEdu.att.view.submitCodePopUp);
			popupView.show();
		},
		hideSubmitCodePopup: function() {
			//            mobEdu.util.updatePrevView(mobEdu.att.f.prevView);
			var popUp = Ext.getCmp('submitCodePopUp');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		showClassRosterView: function() {
			mobEdu.util.updatePrevView(mobEdu.att.view.attdMenu);
			mobEdu.att.f.getTodaysClass();
			mobEdu.util.show('mobEdu.att.view.todaysRoster');
		},

		loadAttdMenu: function() {
			mobEdu.util.get('mobEdu.att.view.menu');
			if (mobEdu.util.isDualRole()) {
				var options = new Array();
				options[1] = ({
					text: 'Student Reports',
					value: 'STUDENT'
				});
				options[0] = ({
					text: 'Faculty Reports',
					value: 'FACULTY'
				});
				Ext.getCmp('attendanceRole').show();
				Ext.getCmp('attendanceRole').setOptions(options);
				mobEdu.att.f.showAttendanceMenu();
			} else if (mobEdu.courses.f.isFacultyRole()) {
				mobEdu.att.f.showAttendanceMenu();
				mobEdu.att.f.loadMenu(null, 'FACULTY', null);
				Ext.getCmp('attendanceRole').hide();
			} else {
				Ext.getCmp('attendanceRole').hide();
				mobEdu.att.f.loadStudentAttendance();
			}
		},

		loadMenu: function(selectbox, newValue, oldValue) {
			mobEdu.util.updateCurrentRole(newValue);
			var store = mobEdu.util.getStore('mobEdu.att.store.attdMenu');
			store.removeAll();
			store.sync();
			if (mobEdu.courses.f.isFacultyRole()) {
				store.add({
					title: 'Today\'s Roster',
					action: mobEdu.att.f.loadTodayAttendance
				});
				store.sync();

				store.add({
					title: 'Attendance By Date',
					action: mobEdu.att.f.loadAttendanceByDate
				});
				store.sync();

				store.add({
					title: 'Attendance By Class',
					action: mobEdu.att.f.loadClassAttendance
				});
				store.sync();

				store.add({
					title: 'Attendance By Student',
					action: mobEdu.att.f.loadStudentAttendance
				});
				store.sync();

			} else {
				store.add({
					title: 'Attendance By Student',
					action: mobEdu.att.f.loadStudentAttendance
				});
				store.sync();
			}
		},

		loadTermList: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.termList');
			if (mobEdu.courses.f.isFacultyRole()) {
				store.getProxy().setUrl(webserver + 'getFacultyTerms');
				store.getProxy().setExtraParams({
					'facultyId': mobEdu.main.f.getStudentId(),
					companyId: companyId
				});
			} else {
				store.getProxy().setUrl(webserver + 'getMyCoursesTerms');
				store.getProxy().setExtraParams({
					'studentId': mobEdu.main.f.getStudentId(),
					companyId: companyId
				});
			}
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.att.f.termListResponseHandler();
			}
			store.load();
			if (mobEdu.courses.f.isFacultyRole()) {
				mobEdu.util.gaTrackEvent('enroll', 'facultyterms');
			} else {
				mobEdu.util.gaTrackEvent('enroll', 'getMyCoursesTerms');
			}
		},

		termListResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.termList');
			var localStore = mobEdu.util.getStore('mobEdu.att.store.termListLocal');
			if (store.data.length == 0) {
				if (mobEdu.courses.f.isFacultyRole()) {
					Ext.Msg.show({
						message: '<p>No teaching courses available.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					Ext.Msg.show({
						message: '<p>No registered courses available.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			} else {
				localStore.removeAll();
				localStore.sync();
				localStore.add(store.getRange());
				localStore.sync();
				//                eval(nextView + '()');
				localStore.each(function(record) {
					if (record.raw.defaultTermInd === 'true' || record.raw.defaultTermInd === 'Y') {
						if (mobEdu.att.f.defaultTerm == null) {
							mobEdu.att.f.defaultTerm = record.raw.code;
						}
					}
				});
				mobEdu.util.get('mobEdu.att.view.attendance');
			}

		},

		attStoreClear: function() {
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
		},

		loadTodayAttendance: function() {
			mobEdu.att.f.attendenceFlag = 'todayRoster';
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
			mobEdu.util.get('mobEdu.att.view.attendance');
			Ext.getCmp('attClassList').show();
			Ext.getCmp('attendanceReport').show();
			Ext.getCmp('attdDate').hide();
			Ext.getCmp('attTermList').hide();
			Ext.getCmp('attStdList').hide();
			Ext.getCmp('attFromDate').hide();
			Ext.getCmp('attToDate').hide();
			mobEdu.att.f.loadAttdClassList();
		},

		loadAttendanceByDate: function() {
			mobEdu.att.f.attendenceFlag = 'dateRoster';
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
			mobEdu.util.get('mobEdu.att.view.attendance');
			Ext.getCmp('attdDate').show();
			Ext.getCmp('attendanceReport').show();
			Ext.getCmp('attClassList').show();
			Ext.getCmp('attTermList').hide();
			Ext.getCmp('attStdList').hide();
			Ext.getCmp('attFromDate').hide();
			Ext.getCmp('attToDate').hide();
			mobEdu.att.f.loadAttdClassList();
		},

		loadStudentAttendance: function() {
			mobEdu.att.f.attendenceFlag = 'student';
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
			mobEdu.util.get('mobEdu.att.view.attendance');
			Ext.getCmp('attClassList').show();
			if (mobEdu.courses.f.isFacultyRole()) {
				Ext.getCmp('attStdList').show();
			} else {
				Ext.getCmp('attStdList').hide();
			}
			if (!Ext.os.is.Tablet) {
				Ext.getCmp('attendanceReport').hide();
			} else {
				Ext.getCmp('attendanceReport').show();
			}
			Ext.getCmp('attTermList').show();
			Ext.getCmp('attdDate').hide();
			Ext.getCmp('attFromDate').show();
			Ext.getCmp('attToDate').show();
			mobEdu.att.f.loadTermList();
		},

		loadClassAttendance: function() {
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
			mobEdu.util.get('mobEdu.att.view.attendance');
			if (!Ext.os.is.Tablet) {
				Ext.getCmp('attendanceReport').hide();
			} else {
				Ext.getCmp('attendanceReport').show();
			}
			Ext.getCmp('attStdList').hide();
			Ext.getCmp('attClassList').show();
			Ext.getCmp('attTermList').show();
			Ext.getCmp('attdDate').hide();
			Ext.getCmp('attFromDate').show();
			Ext.getCmp('attToDate').show();
			mobEdu.att.f.attendenceFlag = 'attendanceByClass';
			mobEdu.att.f.loadTermList();
		},
		loadAttdClassList: function(selectbox, newValue, oldValue) {
			mobEdu.att.f.attStoreClear();
			Ext.getCmp('attendanceReport').setEmptyText('');
			var coursesLocal = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			coursesLocal.removeAll();
			coursesLocal.sync();
			if (newValue == undefined || newValue == null) {
				newValue = '';
			}
			mobEdu.att.f.loadRegisteredCourses(newValue);
		},

		loadRegisteredCourses: function(termCode) {
			var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
			coursesStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (mobEdu.courses.f.isFacultyRole()) {
				coursesStore.getProxy().setUrl(webserver + 'facultyCourses');
			} else {
				coursesStore.getProxy().setUrl(webserver + 'getRegisteredCourses');
			}
			coursesStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				'termCode': termCode,
				companyId: companyId
			});
			coursesStore.getProxy().afterRequest = function() {
				mobEdu.att.f.registeredCourseResponseHandler();
			};
			coursesStore.load();
			if (mobEdu.courses.f.isFacultyRole()) {
				mobEdu.util.gaTrackEvent('enroll', 'facultyCourses');
			} else {
				mobEdu.util.gaTrackEvent('enroll', 'registeredcourses');
			}
		},

		registeredCourseResponseHandler: function() {
			var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
			var coursesLocal = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			coursesLocal.removeAll();
			coursesLocal.sync();
			coursesLocal.add(coursesStore.getRange());
			coursesLocal.sync();
			if (coursesStore.data.length == 0) {
				Ext.Msg.show({
					message: '<p>You do not have courses in current term.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.att.f.showAttendance();
			}
		},

		loadAttdStudentList: function(selectfield, newValue, oldValue) {
			mobEdu.att.f.attStoreClear();
			if (mobEdu.att.f.attendenceFlag == 'todayRoster' && (newValue == '0' || newValue == null)) {
				var store = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
				if (store.getCount() > 0) {
					store.removeAll();
					store.removed = [];
				}
			} else if (mobEdu.att.f.attendenceFlag == 'todayRoster' && newValue != '0') {
				mobEdu.att.f.loadAttdByStudent();
			} else if (newValue == '0' || mobEdu.att.f.attendenceFlag != 'student' || mobEdu.courses.f.isFacultyRole() == false) {
				Ext.getCmp('attStdList').setOptions([{
					text: 'Select Student',
					value: '0'
				}]);
			} else {
				if (newValue != null || newValue != undefined) {
					var selectedValue = newValue.split('-');
					var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
					if (classmatesStore.data.length > 0) {
						classmatesStore.removeAll();
						classmatesStore.removed = [];
					}
					classmatesStore.getProxy().setHeaders({
						Authorization: mobEdu.util.getAuthString(),
						companyID: companyId
					});
					classmatesStore.getProxy().setUrl(webserver + 'getClassmates');
					classmatesStore.getProxy().setExtraParams({
						crn: selectedValue[0],
						termCode: selectedValue[3],
						companyId: companyId
					});
					classmatesStore.getProxy().afterRequest = function() {
						mobEdu.att.f.attStudentListResponseHandler();
					};
					classmatesStore.load();
					mobEdu.util.gaTrackEvent('enroll', 'classmates');
				}
			}
		},

		attStudentListResponseHandler: function() {
			var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
			if (classmatesStore.data.length > 0) {
				classmatesStore.sort('name', 'ASC');
				mobEdu.att.f.setAttdStudentList(classmatesStore);
			} else {
				Ext.getCmp('attendanceReport').setEmptyText('No Data');
			}
		},

		loadReport: function() {
			if (mobEdu.att.f.attendenceFlag == 'dateRoster') {
				mobEdu.att.f.loadDateRoster();
			} else {
				mobEdu.att.f.loadAttdByStudent();
			}
		},

		loadAttdByStudent: function() {
			var studentId = '';
			var attFromDate = new Date();
			var attToDate = new Date();
			if (mobEdu.att.f.attendenceFlag != 'attendanceByClass') {
				if (mobEdu.courses.f.isFacultyRole()) {
					studentId = Ext.getCmp('attStdList').getValue();
				} else if (mobEdu.att.f.attendenceFlag != 'todayRoster') {
					studentId = mobEdu.main.f.getStudentId();
				}
			}
			var selected = Ext.getCmp('attClassList').getValue();
			if (mobEdu.att.f.attendenceFlag != 'todayRoster') {
				attFromDate = Ext.getCmp('attFromDate').getValue();
				attToDate = Ext.getCmp('attToDate').getValue();
			}

			if (((studentId != null && selected != null && attFromDate != null && attToDate != null && studentId != '0') || mobEdu.att.f.attendenceFlag == 'todayRoster') && selected != '0') {
				var selectedValue = selected.split('-');
				mobEdu.att.f.loadAttendanceReport(selectedValue[0], selectedValue[3], selectedValue[1], selectedValue[2], studentId, attFromDate, attToDate);
			} else {
				Ext.Msg.show({
					message: '<p>All fields are mandatory.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadDateRoster: function() {
			var attdDate = Ext.getCmp('attdDate').getValue();
			var selected = Ext.getCmp('attClassList').getValue();
			var selectedValue = selected.split('-');
			var codeGenerateStore = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			codeGenerateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			codeGenerateStore.getProxy().setUrl(webserver + 'extendTime');
			codeGenerateStore.getProxy().setExtraParams({
				instructorId: mobEdu.main.f.getStudentId(), //'T00001617',
				termCode: selectedValue[3], //'201380',
				crn: selectedValue[0], //'82865',
				startTime: selectedValue[1], //'1500',
				endTime: selectedValue[2], //'1700',
				attDate: Ext.Date.format(attdDate, 'n/j/Y'),
				extendTime: '0',
				companyId: companyId
			});
			codeGenerateStore.getProxy().afterRequest = function() {
				mobEdu.att.f.generateCodeResponseHandler();
			};
			codeGenerateStore.load();
			mobEdu.util.gaTrackEvent('attendance', 'dateRoaterextendTime');
		},
		generateCodeResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			var status = store.data.all[0].data.status;
			var attdDate = Ext.getCmp('attdDate').getValue();
			var selected = Ext.getCmp('attClassList').getValue();
			if (attdDate == null || selected == 0) {
				Ext.Msg.show({
					message: '<p>All fields are mandatory.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else if (status == "SUCCESS") {
				var attdDate = Ext.getCmp('attdDate').getValue();
				var selected = Ext.getCmp('attClassList').getValue();
				var selectedValue = selected.split('-');
				mobEdu.att.f.loadAttendanceReport(selectedValue[0], selectedValue[3], selectedValue[1], selectedValue[2], '', attdDate, attdDate);
			} else {
				//var message = status.split('\n', 1);
				//var statusmsg = message[0].split(":", 2);
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		isDateRoster: function() {
			if (mobEdu.att.f.attendenceFlag == 'todayRoster' || mobEdu.att.f.attendenceFlag == 'dateRoster')
				return true;
			return false;
		},

		isShowCheckBox: function() {
			if (mobEdu.courses.f.isFacultyRole())
				return true;
			return false;
		},

		loadAttendanceReport: function(crn, termCode, startTime, endTime, studentId, fromDate, toDate) {
			if (studentId == '0') {
				studentId = '';
			}
			var attDateFrom;
			if (fromDate == null || fromDate == '') {
				fromDate = new Date();
			} else {
				fromDate = new Date(fromDate);
			}
			attDateFrom = ([fromDate.getMonth() < 9 ? '0' + (fromDate.getMonth() + 1) : fromDate.getMonth() + 1] + '/' + [fromDate.getDate() < 10 ? '0' + fromDate.getDate() : fromDate.getDate()] + '/' + fromDate.getFullYear());
			var attDateTo;
			if (toDate == null || toDate == '') {
				toDate = new Date();
			} else {
				toDate = new Date(toDate);
			}
			attDateTo = ([toDate.getMonth() < 9 ? '0' + (toDate.getMonth() + 1) : toDate.getMonth() + 1] + '/' + [toDate.getDate() < 10 ? '0' + toDate.getDate() : toDate.getDate()] + '/' + toDate.getFullYear());
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			if (proxyStore.getCount() > 0) {
				proxyStore.removeAll();
				proxyStore.removed = [];
			}
			proxyStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			proxyStore.getProxy().setUrl(webserver + 'getAttendanceReport');
			if (mobEdu.att.f.attendenceFlag == 'attendanceByClass') {
				proxyStore.setSorters({
					property: 'date',
					direction: 'DESC',
					sorterFn: function(o1, o2) {
						var v1 = new Date(o1.data.date);
						var v2 = new Date(o2.data.date);
						return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
					}
				});

				proxyStore.setGrouper({
					sortProperty: 'name',
					groupFn: function(record) {
						return record.get('name');
					}
				});

			} else {
				proxyStore.setSorters({
					property: 'name',
					direction: 'ASC'
				});
				proxyStore.setSorters({
					property: 'date',
					direction: 'DESC',
					sorterFn: function(o1, o2) {
						var v1 = new Date(o1.data.date);
						var v2 = new Date(o2.data.date);
						return v1 > v2 ? 1 : (v1 < v2 ? -1 : 0);
					}
				});
				if (mobEdu.util.getCurrentRole() === 'FACULTY') {
					proxyStore.setGrouper({
						sortProperty: 'attIndicator',
						//                    direction:'DESC',
						groupFn: function(record) {
							if (record.get('attIndicator') === 'Y') {
								return mobEdu.att.f.isChecked('attended', record.get('attIndicator')) ? '<table width="100%"><tr><td width="42px"><a href="javascript:mobEdu.att.f.onAttendedItemTap()"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" height=24 id="on" name="attended"  /></a></td><td>Attended</td></tr></table>' : '<table width="100%"><tr><td width="42px"><a href="javascript:mobEdu.att.f.onAttendedItemTap()"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" height=24 id="off" name="attended"  /></a></td><td>Attended</td></tr></table>'
							} else {
								return mobEdu.att.f.isChecked('absent', record.get('attIndicator')) ? '<table width="100%"><tr><td width="42px"><a href="javascript:mobEdu.att.f.onAbsentItemTap()"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" height=24 id="on" name="absent" /></td><td>Absent</td></tr></table>' : '<table width="100%"><tr><td width="42px"><a href="javascript:mobEdu.att.f.onAbsentItemTap()"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" height=24 id="off" name="absent" /></td><td>Absent</td></tr></table>';
							}
							//                        return record.get('attIndicator') === 'Y' ? 'Attended' : 'Absent';
						}
					});
				} else {
					proxyStore.setGrouper({
						sortProperty: 'attIndicator',
						groupFn: function(record) {
							if (record.get('attIndicator') === 'Y') {
								return '<b>Attended</b>';
							} else {
								return '<b>Absent</b>';
							}
						}
					});
				}
			}
			proxyStore.getProxy().setExtraParams({
				startDate: attDateFrom,
				endDate: attDateTo,
				studentId: mobEdu.main.f.getStudentId(),
				reportId: studentId,
				termCode: termCode,
				crn: crn,
				classStartTime: startTime,
				classEndTime: endTime,
				companyId: companyId
			});
			proxyStore.getProxy().afterRequest = function() {
				mobEdu.att.f.loadAttendanceReportResponseHandler();
			};
			proxyStore.load();
			mobEdu.util.gaTrackEvent('attendance', 'getAttendanceReport');

		},

		loadAttendanceReportResponseHandler: function() {
			var proxyStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');

			if (proxyStore.data.length === 0) {
				Ext.getCmp('attendanceReport').setEmptyText('No Data');
				if (mobEdu.att.f.attendenceFlag == 'student') {
					if (!Ext.os.is.Tablet) {
						Ext.Msg.show({
							message: 'No Data',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				}
			} else {
				if (mobEdu.att.f.attendenceFlag == 'student' || mobEdu.att.f.attendenceFlag == 'attendanceByClass') {
					if (!Ext.os.is.Tablet) {
						mobEdu.att.f.showStudentList();
					}
				}
			}
		},

		isChecked: function(name, attInd) {
			var items = Ext.DomQuery.select("img[name='" + name + "']");
			if ((items.length > 0 && items[0].id == "on") || (items.length == 0 && attInd == 'Y')) {
				return true;
			}
			return false;
		},

		attendanceByClassResponseHandler: function() {
			var dataStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			dataStore.setGroupField('name');
			dataStore.setGrouper(mobEdu.att.f.groupFunctioAttendanceByClass());
		},
		groupFunctioAttendanceByClass: function() {
			return dataStore.name;
		},
		setDayClasses: function(store, dateValue) {
			var options = new Array();
			var index = 1;
			options[0] = ({
				text: 'Select Class',
				value: '0'
			});
			//            var dateValue = new Date();
			var newdateValue;
			if (dateValue == null) {
				newdateValue = '';
			} else {
				newdateValue = (dateValue.getDate() + '-' + [dateValue.getMonth() + 1] + '-' + dateValue.getFullYear());
			}
			//get the store info
			var sData = store.data.all;
			for (var c = 0; c < sData.length; c++) {
				var crn = sData[c].data.crn;
				var meetings = sData[c].data.meeting;
				for (var i = 0; i < meetings.length; i++) {
					var storeDate = meetings[i].startDate;
					var storeEndDate = meetings[i].endDate;
					var storeDateValue = Ext.Date.parse(storeDate, "m-d-Y");
					var storeNewDateValue = (storeDateValue.getDate() + '-' + [storeDateValue.getMonth() + 1] + '-' + storeDateValue.getFullYear());
					var endDate = Ext.Date.parse(storeEndDate, "m-d-Y");
					var newEndDate = (endDate.getDate() + '-' + [endDate.getMonth() + 1] + '-' + endDate.getFullYear());
					//get the month and day
					var dateValueMonth = dateValue.getMonth() + 1;
					var dateValueDay = dateValue.getDate();
					var storeSDateMonth = storeDateValue.getMonth() + 1;
					var storeSDateDay = storeDateValue.getDate();
					var storeEDateMonth = endDate.getMonth() + 1;
					var storeEDateDay = endDate.getDate();

					if ((dateValueMonth > storeSDateMonth || (dateValueMonth == storeSDateMonth && dateValueDay >= storeSDateDay)) && (dateValueMonth < storeEDateMonth || (dateValueMonth == storeEDateMonth && dateValueDay <= storeEDateDay))) {
						var period = meetings[i].period;
						//Split the periods from list
						var periodList = new Array();
						periodList = period.split(',');
						var day = this.getWeek(dateValue);
						for (var j = 0; j < periodList.length; j++) {
							if (day == periodList[j]) {
								var scheduleTime = meetings[i].beginTime + '-' + meetings[i].endTime;
								options[index] = ({
									text: sData[c].data.courseTitle + ' (' + crn + ' ' + mobEdu.att.f.isTimeExists(scheduleTime) + ')',
									value: crn + '-' + mobEdu.att.f.formatTimeToSave(meetings[i].beginTime) + '-' + mobEdu.att.f.formatTimeToSave(meetings[i].endTime) + '-' + sData[c].data.termCode
								});
								index = index + 1;
							}
						}
					}
				}
			}
			mobEdu.util.get('mobEdu.att.view.attendance');
			Ext.getCmp('attClassList').suspendEvents();
			Ext.getCmp('attClassList').setOptions(options);
			Ext.getCmp('attClassList').setValue('0');
			Ext.getCmp('attClassList').resumeEvents(true);
			Ext.getCmp('attStdList').setOptions([{
				text: 'Select Student',
				value: '0'
			}]);

			if (options.length > 1) {
				mobEdu.att.f.showAttendanceView();
			} else {
				Ext.Msg.show({
					message: 'You do not have any classes.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		getWeek: function(date) {
			switch (date.getDay()) {
				case 0:
					return "Sun";
				case 1:
					return "Mon";
				case 2:
					return "Tue";
				case 3:
					return "Wed";
				case 4:
					return "Thu";
				case 5:
					return "Fri";
				case 6:
					return "Sat";
			}
		},

		setAttdClassList: function(store) {
			console.log('length is ' + store.data.length);
			var options = new Array();
			var index = 1;
			options[0] = ({
				text: 'Select Class',
				value: '0'
			});
			store.each(function(record) {
				var meeting = record.data.meeting;
				for (var i = 0; i < meeting.length; i++) {
					var scheduleTime = meeting[i].beginTime + '-' + meeting[i].endTime;
					options[index] = ({
						text: record.data.courseTitle + ' (' + record.data.crn + ' ' + mobEdu.att.f.isTimeExists(scheduleTime) + ')',
						value: record.data.crn + '-' + mobEdu.att.f.formatTimeToSave(meeting[i].beginTime) + '-' + mobEdu.att.f.formatTimeToSave(meeting[i].endTime) + '-' + record.data.termCode
					});
					index = index + 1;
				}
			});
			mobEdu.util.get('mobEdu.att.view.attendance');
			Ext.getCmp('attClassList').suspendEvents();
			Ext.getCmp('attClassList').setOptions(options);
			Ext.getCmp('attClassList').setValue('0');
			Ext.getCmp('attClassList').resumeEvents();
			Ext.getCmp('attStdList').setOptions([{
				text: 'Select Student',
				value: '0'
			}]);
		},

		isTimeExists: function(scheduleTime) {
			if (scheduleTime.match('NA'))
				return '';
			else
				return scheduleTime;
		},

		setAttdStudentList: function(store) {
			var options = new Array();
			var index = 1;
			options[0] = ({
				text: 'Select Student',
				value: '0'
			});
			store.each(function(record) {
				options[index] = ({
					text: record.data.name,
					value: record.data.id
				});
				index = index + 1;
			});
			Ext.getCmp('attStdList').setOptions(options);
		},

		onAttdDateChange: function(datePicker, value, eOpts) {

			if ((new Date()) < value) {
				Ext.Msg.show({
					message: 'You can not select future date.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('attdDate').setValue(new Date());
						}
					}
				});
			} else {
				var coursesLocal = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
				mobEdu.att.f.setDayClasses(coursesLocal, value);
			}
		},

		onAttdFromDateChange: function(datePicker, value, eOpts) {
			if ((new Date()) < value) {
				Ext.Msg.show({
					message: 'You can not select future date.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('attFromDate').setValue(new Date());
						}
					}
				});
			}
		},

		onAttdToDateChange: function(datePicker, value, eOpts) {
			if ((new Date()) < value) {
				Ext.Msg.show({
					message: 'You can not select future date.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('attToDate').setValue(new Date());
						}
					}
				});
			}
		},

		showAttendance: function() {
			var coursesLocal = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			if (mobEdu.courses.f.isFacultyRole()) {
				Ext.getCmp('updateAttd').show();
			} else {
				Ext.getCmp('updateAttd').hide();
			}
			if (mobEdu.att.f.attendenceFlag == 'todayRoster') {
				var today = new Date();
				mobEdu.att.f.setDayClasses(coursesLocal, today);
			} else if (mobEdu.att.f.attendenceFlag == 'dateRoster') {
				mobEdu.util.get('mobEdu.att.view.attendance');
				Ext.getCmp('attdDate').setValue(new Date());
				//                mobEdu.att.f.setDayClasses(coursesLocal, date);     
			} else {
				mobEdu.att.f.setAttdClassList(coursesLocal);
			}
			mobEdu.att.f.showAttendanceView();
		},

		showAttendanceView: function() {
			if (mobEdu.util.isDualRole() || mobEdu.courses.f.isFacultyRole()) {
				mobEdu.util.updatePrevView(mobEdu.att.f.showAttendanceMenu);
				if (mobEdu.att.f.attendenceFlag == 'todayRoster') {
					Ext.getCmp('attdSearch').setText('Refresh');
				} else {
					Ext.getCmp('attdSearch').setText('Search');
				}
			} else {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			}

			if (mobEdu.att.f.attendenceFlag == 'student' || mobEdu.att.f.attendenceFlag == 'attendanceByClass') {
				if (mobEdu.util.getCurrentRole() === 'FACULTY') {
					if (!Ext.os.is.Tablet) {
						Ext.getCmp('updateAttd').hide();
					} else {
						Ext.getCmp('updateAttd').show();
					}
				} else {
					Ext.getCmp('updateAttd').hide();
				}
			} else {
				Ext.getCmp('updateAttd').show();
			}

			mobEdu.util.show('mobEdu.att.view.attendance');

			if (mobEdu.att.f.defaultTerm != null & mobEdu.att.f.defaultTerm != '') {
				Ext.getCmp('attTermList').setValue(mobEdu.att.f.defaultTerm);
			}
		},

		showAttendanceReport: function() {
			if (mobEdu.att.f.attendenceFlag == 'student') {
				if (!Ext.os.is.Tablet) {
					Ext.getCmp('updateAttd').hide();
				} else {
					Ext.getCmp('updateAttd').show();
					Ext.getCmp('attdSearch').setText('Refresh');
				}
			} else {
				Ext.getCmp('updateAttd').show();
				Ext.getCmp('attdSearch').setText('Refresh');
			}


			if (mobEdu.att.f.attPrevView == null) {
				mobEdu.util.updatePrevView(mobEdu.att.f.showCourseViewDetails);
			} else {
				mobEdu.util.updatePrevView(mobEdu.att.f.attPrevView);
				mobEdu.att.f.attPrevView = null;
			}
			mobEdu.util.show('mobEdu.att.view.attendance');

			if (mobEdu.att.f.defaultTerm != null & mobEdu.att.f.defaultTerm != '') {
				Ext.getCmp('attTermList').setValue(mobEdu.att.f.defaultTerm);
			}
		},

		showAttendanceMenu: function() {
			var role = Ext.getCmp('attendanceRole').getValue();
			if (role == 'FACULTY') {
				mobEdu.util.updateCurrentRole(role);
			}
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.att.view.menu');
		},

		showStudentList: function() {
			mobEdu.util.updatePrevView(mobEdu.att.f.showAttendanceView);
			mobEdu.util.show('mobEdu.att.view.studentList');
			if (mobEdu.util.getCurrentRole() === 'FACULTY') {
				Ext.getCmp('updateTbar').show();
			} else {
				Ext.getCmp('updateTbar').hide();
			}
		},

		onAttendedItemTap: function() {
			var items = Ext.DomQuery.select("img[name='attended']");
			var check = 'on';
			Ext.each(items, function(item, i) {
				if (item.id == "on") {
					item.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
					item.id = "off";
					check = "off";
				} else if (item.id == "off") {
					item.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
					item.id = "on";
					check = "on";
				}
			});
			var attendanceListStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			var stditems;
			if (mobEdu.att.f.attendenceFlag == 'student') {
				if (!Ext.os.is.Tablet) {
					stditems = Ext.DomQuery.select("img[name='chkAttendS']");
				} else {
					stditems = Ext.DomQuery.select("img[name='chkAttend']");
				}
			} else {
				stditems = Ext.DomQuery.select("img[name='chkAttend']");
			}
			Ext.each(stditems, function(item, i) {
				if (attendanceListStore.getAt(i).data.attIndicator == 'Y') {
					item.src = mobEdu.util.getResourcePath() + "images/checkbox" + check + ".png";
					item.id = check;
				}
			});
		},

		onAbsentItemTap: function() {
			var items = Ext.DomQuery.select("img[name='absent']");
			var check = 'on';
			Ext.each(items, function(item, i) {
				if (item.id == "on") {
					item.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
					item.id = "off";
					check = "off";
				} else if (item.id == "off") {
					item.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
					item.id = "on";
					check = "on";
				}
			});
			var attendanceListStore = mobEdu.util.getStore('mobEdu.att.store.studentAttendance');
			var stditems;
			if (mobEdu.att.f.attendenceFlag == 'student') {
				if (!Ext.os.is.Tablet) {
					stditems = Ext.DomQuery.select("img[name='chkAttendS']");
				} else {
					stditems = Ext.DomQuery.select("img[name='chkAttend']");
				}
			} else {
				stditems = Ext.DomQuery.select("img[name='chkAttend']");
			}
			Ext.each(stditems, function(item, i) {
				if (attendanceListStore.getAt(i).data.attIndicator == 'N') {
					item.src = mobEdu.util.getResourcePath() + "images/checkbox" + check + ".png";
					item.id = check;
				}
			});
		}
	}
});