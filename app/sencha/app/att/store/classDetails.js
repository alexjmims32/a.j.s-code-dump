Ext.define('mobEdu.att.store.classDetails', {
    extend:'mobEdu.data.store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.att.model.menu'
    ],

    config:{
        storeId: 'mobEdu.att.store.classDetails',
        autoLoad: false,
        model: 'mobEdu.att.model.menu',
        proxy:{
            type:'localstorage',
            id:'classDetails'
        }
    }
});