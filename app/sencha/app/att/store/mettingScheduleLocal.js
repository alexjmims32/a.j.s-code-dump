Ext.define('mobEdu.att.store.mettingScheduleLocal', {
    extend: 'Ext.data.Store',
    requires: [
        'mobEdu.att.model.meetingScheduleLocal'
    ],
    config: {
        storeId: 'mobEdu.att.store.mettingScheduleLocal',
        autoLoad: false,
        model: 'mobEdu.att.model.meetingScheduleLocal',
        proxy: {
            type: 'localstorage',
            id: 'instructorSchedule'
        }
    }
});
