Ext.define('mobEdu.att.store.termList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.termList'
	],
	config: {
		storeId: 'mobEdu.att.store.termList',
		autoLoad: false,
		model: 'mobEdu.reg.model.termList'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'registerTermList';
		return proxy;
	}
});
