Ext.define('mobEdu.att.store.menu', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.att.model.menu'               
	],
	config: {
		storeId: 'mobEdu.att.store.menu',
		autoLoad: false,
		model: 'mobEdu.att.model.menu'		
	},
        initProxy: function() {
            var proxy = this.callParent();
            proxy.reader.rootProperty= 'registeredCourseList.registeredCourse';      
            return proxy;
        }
});