Ext.define('mobEdu.att.store.class', {
    extend:'mobEdu.data.store',
    
    requires: [
    'mobEdu.att.model.class'
    ],

    config:{
        storeId: 'mobEdu.att.store.class',
        autoLoad: false,
        model: 'mobEdu.att.model.class',
        proxy:{
            type:'localstorage',
            id:'class'
        }
    }
});