Ext.define('mobEdu.att.store.currentClass', {
    extend: 'mobEdu.data.store',

    requires: ['mobEdu.att.model.currentClass'],
    config: {
        storeId: 'mobEdu.att.store.currentClass',
        autoLoad: false,
        model: 'mobEdu.att.model.currentClass',
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'ROWSET',
                record: 'ROW'
            }
        }
    }
});