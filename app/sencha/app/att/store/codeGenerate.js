Ext.define('mobEdu.att.store.codeGenerate', {
    extend:'mobEdu.data.store',

    requires: [
        'mobEdu.att.model.codeGenerate'
    ],

    config:{
        storeId: 'mobEdu.att.store.codeGenerate',
        autoLoad: false,

        model: 'mobEdu.att.model.codeGenerate',
        initProxy: function() {
            var proxy = this.callParent();
            proxy.reader.rootProperty= ''
            return proxy;
        }
    }
});