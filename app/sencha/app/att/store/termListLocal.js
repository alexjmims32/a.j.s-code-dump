Ext.define('mobEdu.att.store.termListLocal', {
    extend:'Ext.data.Store',

    requires: [
    'mobEdu.reg.model.termList'
    ],

    config:{
        storeId: 'mobEdu.att.store.termListLocal',
        autoLoad: false,
        model: 'mobEdu.reg.model.termList',
        proxy:{
            type:'localstorage',
            id:'termListLocal'
        }
    }
});