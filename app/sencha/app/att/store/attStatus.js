Ext.define('mobEdu.att.store.attStatus',{
extend: 'mobEdu.data.store',
//    alias:'dropStore',
    
    requires:[
        'mobEdu.courses.model.drop'
    ],
    config: {
        storeId: 'mobEdu.att.store.attStatus',

        autoLoad: false,

        model: 'mobEdu.courses.model.drop'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'status'        
        return proxy;
    }
})