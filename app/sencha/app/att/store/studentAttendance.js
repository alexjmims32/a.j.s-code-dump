Ext.define('mobEdu.att.store.studentAttendance', {
    extend: 'mobEdu.data.store',
    requires: [
    'mobEdu.att.model.attendanceInfo'             
    ],
    config: {
        storeId: 'mobEdu.att.store.studentAttendance',
        autoLoad: false,
        model: 'mobEdu.att.model.attendanceInfo',			
        sorters: {
            property: 'date',
            direction: 'ASC'
        }
    },     
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'roaster';      
        return proxy;
    }
});