Ext.define('mobEdu.att.store.attdMenu', {
    extend:'Ext.data.Store',

    requires: [
    'mobEdu.att.model.attdMenu'
    ],

    config:{
        storeId: 'mobEdu.att.store.attdMenu',
        autoLoad: false,
        model: 'mobEdu.att.model.attdMenu',
        proxy:{
            type:'localstorage',
            id:'attdMenuLocal'
        }
    }
});