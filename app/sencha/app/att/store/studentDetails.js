Ext.define('mobEdu.att.store.studentDetails', {
    extend:'Ext.data.Store',
    requires: [
    'mobEdu.courses.model.classmates'
    ],

    config:{
        storeId: 'mobEdu.att.store.studentDetails',
        autoLoad: false,
        model: 'mobEdu.att.model.attendanceInfo',
        proxy:{
            type:'localstorage',
            id:'studentDetails'
        }
    }
});