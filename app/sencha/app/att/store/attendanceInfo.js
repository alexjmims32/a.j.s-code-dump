Ext.define('mobEdu.att.store.attendanceInfo', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.att.model.attendanceInfo'               
	],
	config: {
		storeId: 'mobEdu.att.store.attendanceInfo',
		autoLoad: false,
		model: 'mobEdu.att.model.attendanceInfo'		
	},
        initProxy: function() {
            var proxy = this.callParent();
            proxy.reader.rootProperty= 'roaster';      
            return proxy;
        }
});