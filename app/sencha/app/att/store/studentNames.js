Ext.define('mobEdu.att.store.studentNames', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.courses.model.classmates'            
	],
	config: {
		storeId: 'mobEdu.att.store.studentNames',
		autoLoad: false,
		model: 'mobEdu.courses.model.classmates'		
	},
        initProxy: function() {
            var proxy = this.callParent();
            proxy.reader.rootProperty= 'roaster';      
            return proxy;
        }
});