Ext.define('mobEdu.att.model.attendanceInfo', {
	extend: 'Ext.data.Model',
	config: {
		fields: [{
			name: 'termCode'
		}, {
			name: 'studentId'
		}, {
			name: 'name'
		}, {
			name: 'crn'
		}, {
			name: 'email'
		}, {
			name: 'attIndicator'
		}, {
			name: 'code'
		}, {
			name: 'date',
			convert: function(value, record) {
				if (value != null && value != '') {
					return Ext.Date.format(
						Ext.Date.parse(value, 'Y-m-d H:i:s.u'), 'F j, Y');
				}
			}
		}, {
			name: 'count'
		}, {
			name: 'level'
		}, {
			name: 'grade'
		}, ]
	}
});