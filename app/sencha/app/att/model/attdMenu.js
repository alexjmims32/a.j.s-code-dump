Ext.define('mobEdu.att.model.attdMenu', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
            name:'title'
        },{
            name:'action',
            type: 'function'
        },{
            name:'role'
        }
        ]
    }
});
