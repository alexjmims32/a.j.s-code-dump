Ext.define('mobEdu.att.model.class', {
    extend: 'Ext.data.Model',
    config: {
       fields : [ {
            name:'courseTitle'
        },{
            name:'startTime'
        },{
            name:'endTime'
        },{
            name:'startDate'
        },{
            name:'endDate'
        },{
            name:'termCode'
        },{
            name:'crn'
        },{
            name:'attIndicator'
        }
            
       ]
    }
});