Ext.define('mobEdu.att.model.currentClass', {
    extend: 'Ext.data.Model',
    config: {
       fields : [ {
            name:'studentId',
            mapping:'ID'
        },{
            name:'termCode',
            mapping:'TERM_CODE'
        },{
            name:'courseTitle',
            mapping:'COURSE_TITLE'
        },{
            name:'crn',
            mapping:'CRN'
        },{
            name:'date',
            mapping:'CLASS_DATE'
        },{
            name:'time',
            mapping:'MEETING_TIME'
        },{
            name:'bldgDesc',
            mapping:'BLDG_DESC'
        },{
            name:'roomCode',
            mapping:'ROOM_CODE'
        }            
       ]
    }
});