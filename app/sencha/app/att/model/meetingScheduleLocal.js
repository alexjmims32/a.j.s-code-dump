Ext.define('mobEdu.att.model.meetingScheduleLocal',{
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'title',
            type : 'string'
        },{
            name: 'startTime',
            type: 'string'
        },{
            name: 'endTime',
            type: 'string'
        },{
            name: 'type',
            type: 'string'
        },{
            name: 'buildgNo',
            type: 'string'
        },{
            name: 'roomNo',
            type: 'string'
        },{
            name: 'latitude',
            type: 'string'
        },{
            name: 'longitude',
            type: 'string'
        },{
            name:'crn',
            type:'string'
        },{
            name:'termCode',
            type:'string'
        }]
    }
});