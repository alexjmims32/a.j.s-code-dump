Ext.define('mobEdu.att.model.codeGenerate',{
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'code',
            type : 'string'
        },{
            name: 'status',
            type: 'string'
        }]
    }
});