Ext.define('mobEdu.data.store', {
    extend:'Ext.data.Store',
    alias: 'genericStore',

	requires:[
		'Ext.data.Store',
		'Ext.data.proxy.Ajax'
	],
//    remoteFilter: true,
//    remoteSort: true,
    
    constructor: function() {
        this.config.proxy = this.initProxy();
        this.callParent(arguments);
    },
    
    initProxy: function() {
        return {
            type: 'ajax',
            headers: '',
//            filterOnLoad: true,
            extraParams: {
//                server: SERVER,
                target: null,
                action: 'read'
            },
            actionMethods: {
                create : 'POST',
                read   : 'POST', // by default GET
                update : 'POST',
                destroy: 'POST'
            },                
            reader: {
                type: 'json'
            }
        };
    }

});