Ext.define('mobEdu.advising.store.courses', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.courses'               
	],
	config: {
		storeId: 'mobEdu.advising.store.courses',
		autoLoad: false,
		model: 'mobEdu.advising.model.courses'		,
		sorters: {
            property: 'title',
            direction: 'ASC'
        }
	},
        initProxy: function() {
            var proxy = this.callParent();
            // proxy.reader.rootProperty= 'roaster';      
            return proxy;
        }
});