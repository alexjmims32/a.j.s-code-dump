Ext.define('mobEdu.advising.store.viewAppts', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.appointment'
	],
	config: {
		storeId: 'mobEdu.advising.store.viewAppts',
		autoLoad: false,
		model: 'mobEdu.finaid.model.appointment',
		sorters: {
			property: 'appointmentDate',
			direction: 'ASC'
		}
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'resultSet';
		return proxy;
	}
});