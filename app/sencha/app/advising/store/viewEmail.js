Ext.define('mobEdu.advising.store.viewEmail', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.advising.model.emailsList'
    ],
    config: {
        storeId: 'mobEdu.advising.store.viewEmail',

        autoLoad: false,

        model: 'mobEdu.advising.model.emailsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json'
                //                rootProperty: 'emails'
            }
        }
    }
});