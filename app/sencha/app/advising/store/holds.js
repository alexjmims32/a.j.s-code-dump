Ext.define('mobEdu.advising.store.holds', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.holds'               
	],
	config: {
		storeId: 'mobEdu.advising.store.holds',
		autoLoad: false,
		model: 'mobEdu.advising.model.holds'		
	},
        initProxy: function() {
            var proxy = this.callParent();   
            return proxy;
        }
});