Ext.define('mobEdu.advising.store.details', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.students'               
	],
	config: {
		storeId: 'mobEdu.advising.store.details',
		autoLoad: false,
		model: 'mobEdu.advising.model.students'		
	},
        initProxy: function() {
            var proxy = this.callParent();  
            return proxy;
        }
});