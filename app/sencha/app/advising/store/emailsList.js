Ext.define('mobEdu.advising.store.emailsList', {
    extend: 'mobEdu.data.store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.advising.model.emailsList'
    ],
    config: {
        storeId: 'mobEdu.advising.store.emailsList',

        autoLoad: false,

        model: 'mobEdu.advising.model.emailsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'messages'
            }
        }
    }
});