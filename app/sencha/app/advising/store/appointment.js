Ext.define('mobEdu.advising.store.appointment', {
    extend: 'mobEdu.data.store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.advising.model.appointment'
    ],
    config: {
        storeId: 'mobEdu.advising.store.appointment',

        autoLoad: false,

        model: 'mobEdu.finaid.model.appointment',

        initProxy: function() {
            var proxy = this.callParent();
            // proxy.reader.rootProperty= 'roaster';      
            return proxy;
        }
    }
});