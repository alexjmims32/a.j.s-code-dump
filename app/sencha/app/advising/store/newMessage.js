Ext.define('mobEdu.advising.store.newMessage', {
    extend: 'mobEdu.data.store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.advising.model.newMessage'
    ],
    config: {
        storeId: 'mobEdu.advising.store.newMessage',

        autoLoad: false,

        model: 'mobEdu.advising.model.newMessage',

        initProxy: function() {
            var proxy = this.callParent();
            // proxy.reader.rootProperty= 'roaster';      
            return proxy;
        }
    }
});