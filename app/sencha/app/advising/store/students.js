Ext.define('mobEdu.advising.store.students', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.students'
	],
	config: {
		storeId: 'mobEdu.advising.store.students',
		autoLoad: false,
		model: 'mobEdu.advising.model.students',
		sorters: {
			property: 'name',
			direction: 'ASC'
		}
	},
	initProxy: function() {
		var proxy = this.callParent();
		// proxy.reader.rootProperty= 'roaster';      
		return proxy;
	}
});