Ext.define('mobEdu.advising.store.regCrse', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.courses'               
	],
	config: {
		storeId: 'mobEdu.advising.store.regCrse',
		autoLoad: false,
		model: 'mobEdu.advising.model.courses',
		sorters: {
            property: 'title',
            direction: 'ASC'
        }		
	},
        initProxy: function() {
            var proxy = this.callParent();     
            return proxy;
        }
});