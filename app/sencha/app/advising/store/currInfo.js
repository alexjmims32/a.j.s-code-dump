Ext.define('mobEdu.advising.store.currInfo', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.advising.model.currInfo'               
	],
	config: {
		storeId: 'mobEdu.advising.store.currInfo',
		autoLoad: false,
		model: 'mobEdu.advising.model.currInfo'		
	},
        initProxy: function() {
            var proxy = this.callParent();   
            return proxy;
        }
});