Ext.define('mobEdu.advising.view.appointmentCalendar', {
    extend: 'Ext.Container',
    alias: 'appointmentCalendar',
    config: {
        fullscreen: 'true',
        scrollable: true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            id: 'advAppointmentsDock',
            name: 'advAppointmentsDock'
        }, {
            xtype: 'label',
            id: 'advAppointmentLabel',
            padding: 10
        }]
    }
});