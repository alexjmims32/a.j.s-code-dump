Ext.define('mobEdu.advising.view.holds', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            name: 'advHoldsDetail',
            id: 'advHoldsDetail',
            disableSelection: true,
            deferEmptyText: false,
            emptyText: '<center><h3>No student selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tpl if="(mobEdu.util.isValueExists(amount) === true)">', '<tr><td width="50%"><h2>Amount</h2></td><td width="50%"><h3>: ${[mobEdu.util.formatCurrency(values.amount)]}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(reason) === true)">', '<tr><td width="50%"><h2>Reason</h2></td><td width="50%"><h3>: {reason}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(procAffected) === true)">', '<tr><td width="50%"><h2>Processes Affected</h2></td><td width="50%"><h3>: {procAffected}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(orgOffice) === true)">', '<tr><td width="50%"><h2>Originating Office</h2></td><td width="50%"><h3>: {orgOffice}</h3></td></tr>' + '</tpl>' + '<tr><td width="50%"><h2>Date</h2></td><td width="50%"><h3>: {fromDate} to {toDate}</h3></td></tr>' + '</table>'),
            store: mobEdu.util.getStore('mobEdu.advising.store.holds'),
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Holds</h1>',
        }],
        flex: 1
    }
});