Ext.define('mobEdu.advising.view.viewAppts', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            name: 'advViewAppts',
            id: 'advViewAppts',
            disableSelection: true,
            deferEmptyText: false,
            emptyText: '<center><h3>No Appointments</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tpl if="(mobEdu.util.isValueExists(subject) === true)">', '<tr><td width="20%"><h2>Subject</h2></td><td width="20%"><h3>: {subject}</h3></td>' + '</tpl>' +
                '<tpl if="((mobEdu.util.isRecieved(createdBy) === true)&&(mobEdu.util.isVisible(fromID,toIDs,loginUserID) === true))">', '<td width="60%" align="right" rowspan=4><img name="accept" width="40px" height="40px" src="' + mobEdu.util.getResourcePath() + 'images/accept.png"/> &nbsp; <img name="decline" width="40px" height="40px" src="' + mobEdu.util.getResourcePath() + 'images/reject.png"/></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(appointmentDate) === true)">', '<tr><td width="35%"><h2>Date</h2></td><td width="35%"><h3>: {appointmentDate}</h3></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(location) === true)">', '<tr><td width="50%"><h2>Location</h2></td><td width="50%"><h3>: {location}</h3></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(status) === true)">', '<tr><td width="50%"><h2>Status</h2></td><td width="50%"><h3>: {status}</h3></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(body) === true)">', '<tr><td colspan="2" width="50%"><h2>Description:</h2></td></tr>', '<tr><td colspan="2" width="50%"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' + '</tpl>' +
                '</table>'),
            store: mobEdu.util.getStore('mobEdu.advising.store.viewAppts'),
            loadingText: '',
            listeners: {
                itemtap: function(me, index, target, record, e, eOpts) {
                    mobEdu.advising.f.appTap(me, index, target, record, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>View Appointments</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',

            layout: {

                pack: 'right'
            },
            items: [{
                text: 'New Appointment',
                ui: 'confirm',
                handler: function() {
                    mobEdu.advising.f.showNewAppt();
                }
            }, {
                text: 'Calendar',
                id: 'advAppCal',
                handler: function() {
                    mobEdu.advising.f.showCalendar();
                }
            }]
        }],
        flex: 1
    }
});