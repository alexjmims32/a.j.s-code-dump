Ext.define('mobEdu.advising.view.subMenu', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            singleSelect: true,
            loadingText: '',
            data: [{
                title: 'Details',
                action: mobEdu.advising.f.loadStudentDetails
            }, {
                title: 'Holds',
                action: mobEdu.advising.f.loadHolds
            }, {
                title: 'Curriculum Info',
                action: mobEdu.advising.f.loadCurriculumInfo
            }, {
                title: 'Registered Courses',
                action: mobEdu.advising.f.loadRegisteredCourses
            },/* {
                title: 'New Appointment',
                action: mobEdu.advising.f.showNewAppt
            }, {
                title: 'View Appointments',
                action: mobEdu.advising.f.loadViewAppointments
            },*/ {
                title: 'Appointments',
                action: mobEdu.advising.f.loadAppointments
            }, {
                title: 'Messages',
                action: mobEdu.advising.f.loadViewMessages
            }],
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    if (record.data.action != undefined || record.data.action != null) {
                        record.data.action();
                    }
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'adviSunMenuTitle',
            name: 'adviSunMenuTitle'
        }],
        flex: 1
    }
});