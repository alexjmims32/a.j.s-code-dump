Ext.define('mobEdu.advising.view.courses', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            name: 'advCourseDetail',
            id: 'advCourseDetail',
            disableSelection: true,
            deferEmptyText: false,
            emptyText: '<center><h3>No student selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><h2>{title}</h2><h3>({crn})</h3></td></tr>' + '</table>'),
            store: mobEdu.util.getStore('mobEdu.advising.store.regCrse'),
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Registered Courses</h1>'
        }],
        flex: 1
    }
});