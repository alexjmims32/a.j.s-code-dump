Ext.define('mobEdu.advising.view.viewMsgs', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        scrollable: false,
        items: [{
            xtype: 'list',
            id: 'emailsList',
            cls: 'border',
            name: 'emailsList',
            emptyText: '<h3 align="center">No Messages</h3>',
            itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                '<tr>',
                '<tpl if="(mobEdu.advising.f.hasEmailDirection(label)===true)">',
                '<td align="right"><b><h2>{subject}</h2></b><br/><div class="bodyclass" style="vertical-align: top; overflow: hidden; text-overflow: ellipsis; width:200px; font-weight:normal;">{[decodeURIComponent(values.body)]}</div><br/><b><h4>{versionDate}</h4></b></td>',
                '<tpl else>',
                '<td align="left"><b><h2>{subject}</h2></b><br/><div class="bodyclass" style="vertical-align: top; overflow: hidden; text-overflow: ellipsis; width:200px; font-weight:normal">{[decodeURIComponent(values.body)]}</div><br/><b><h4>{versionDate}</h4></b></td>',
                '</tpl>',
                '</tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.advising.store.emailsList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    mobEdu.advising.f.loadEmailDetail(record);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>View Messages</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'New Message',
                id: 'newViewMsg',
                handler: function() {
                    mobEdu.advising.f.showNewMsg();
                }
            }]
        }],
        flex: 1
    }
});