Ext.define('mobEdu.advising.view.phone.students', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Advising</h1>',
            id: 'adviStudentsTitle',
            name: 'adviStudentsTitle'
        }, {
            xtype: 'selectfield',
            id: 'adviCourses',
            valueField: 'crn',
            displayField: 'name',
            name: 'adviCourses',
            docked: 'top',
            store: mobEdu.util.getStore('mobEdu.advising.store.courses'),
            listeners: {
                change: function(selectbox, newValue, oldValue) {
                    mobEdu.advising.f.onCourseChange(newValue);
                }
            }
        }, {
            xtype: 'list',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{name}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            singleSelect: true,
            loadingText: '',
            emptyText: '<h3 align="center">No students assigned to this course</h3>',
            store: mobEdu.util.getStore('mobEdu.advising.store.students'),
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts) {
                    mobEdu.advising.f.onStudentItemTap(view, index, target, record, e, eOpts);
                }
            }
        }],
        flex: 1
    },
    initialize: function() {
        Ext.Viewport.on('orientationchange', 'handleOrientationChange', this);
        this.callParent(arguments);
    },

    handleOrientationChange: function(viewport, orientation, width, height) {
        mobEdu.advising.f.changeTitle(orientation);
    }
});