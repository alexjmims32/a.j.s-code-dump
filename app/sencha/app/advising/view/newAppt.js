Ext.define('mobEdu.advising.view.newAppt', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'formpanel',
            items: [{
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '40%',
                name: 'adviSubject',
                id: 'adviSubject',
                placeHolder: 'Enter Subject',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'datepickerfield',
                name: 'advidate',
                id: 'advidate',
                labelWidth: '40%',
                label: 'Date',
                required: true,
                useClearIcon: true,
                value: new Date(),
                picker: {
                    yearFrom: new Date().getFullYear(),
                    yearTo: new Date().getFullYear() + 1
                },
                listeners: {
                    change: function(datefield, e, eOpts) {
                        mobEdu.advising.f.onAppDateKeyup(datefield)
                    }
                }
            }, {
                xtype: 'textfield',
                labelWidth: '100%',
                label: 'Appointment Time',
                useClearIcon: true
            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'appTimeHours',
                name: 'appTimeHours',
                required: true,
                useClearIcon: true,
                minValue: 8,
                maxValue: 18,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'appTimeMinutes',
                name: 'appTimeMinutes',
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                labelWidth: '100%',
                label: 'Duration',
                useClearIcon: true
            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'durTimeHours',
                name: 'durTimeHours',
                required: true,
                useClearIcon: true,
                minValue: 0,
                maxValue: 24,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'durTimeMinutes',
                name: 'durTimeMinutes',
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                label: 'Location',
                labelWidth: '40%',
                name: 'adviLocation',
                id: 'adviLocation',
                placeHolder: 'Enter Location',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'selectfield',
                label: 'Status',
                labelWidth: '40%',
                name: 'adviStatus',
                id: 'adviStatus',
                hidden: true,
                required: true,
                useClearIcon: true,
                options: [{
                    text: 'Active',
                    value: 'A'
                }, {
                    text: 'InActive',
                    value: 'I'
                }]
            }, {
                xtype: 'textareafield',
                label: 'Description',
                labelWidth: '40%',
                labelAlign: 'top',
                name: 'adviDesc',
                id: 'adviDesc',
                placeHolder: 'Enter Description',
                required: true,
                useClearIcon: true,
                maxRows: 15
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Close',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.showViewAppts();
                }
            }, {
                text: 'Save',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.createAppointment();
                }
            }]
        }, {
            xtype: 'customToolbar',
            title: '<h1>New Appointment</h1>'
        }],
        flex: 1
    }
});