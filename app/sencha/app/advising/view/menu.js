Ext.define('mobEdu.advising.view.menu', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            singleSelect: true,
            loadingText: '',
            data:[
                {title:'Students Attending My Class',value:0},
                {title:'My Advisees',value:1}
            ],
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts ) {
                    mobEdu.advising.f.onMenuItemTap(view, index, target, record, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>Advising</h1>'
        }],
        flex: 1
    }
});