Ext.define('mobEdu.advising.view.tablet.newMsgPopup', {
    extend: 'Ext.Panel',
    config: {
        id: 'newMsgPopup',
        scroll: 'vertical',
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        layout: 'fit',
        width: '80%',
        height: '70%',
        items: [{
            xtype: 'formpanel',
            id: 'msgFormPanel',
            items: [{
                xtype: 'textfield',
                label: 'Subject',
                name: 'advingMsgSubject',
                id: 'advingMsgSubject',
                placeHolder: 'Enter Subject',
                clearIcon: true,
                required: true
            }, {
                xtype: 'textareafield',
                label: 'Message',
                labelWidth: '30%',
                labelAlign: 'top',
                name: 'advisingnewmsg',
                id: 'advisingnewmsg',
                placeHolder: 'Enter Message',
                maxRows: 15,
                required: true
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Close',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.closeMsgPopup();
                }
            }, {
                text: 'Send',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.createMessage();
                }
            }]
        }],
        flex: 1
    }
});