Ext.define('mobEdu.advising.view.tablet.newApptPopup', {
    extend: 'Ext.Panel',
    config: {
        id: 'newAppPopup',
        scroll: 'vertical',
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        layout: 'fit',
        width: '80%',
        height: '70%',
        items: [{
            xtype: 'formpanel',
            id: 'advAppFormPanel',
            items: [{
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '40%',
                name: 'adviSubject',
                id: 'adviAppSubject',
                placeHolder: 'Enter Subject',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'datepickerfield',
                name: 'advidate',
                id: 'adviAppdate',
                labelWidth: '40%',
                label: 'Date',
                required: true,
                useClearIcon: true,
                value: new Date(),
                picker: {
                    yearFrom: new Date().getFullYear(),
                    yearTo: new Date().getFullYear() + 1
                },
                listeners: {
                    change: function(datefield, e, eOpts) {
                        mobEdu.advising.f.onAppDateKeyup(datefield)
                    }
                }
            }, {
                xtype: 'textfield',
                labelWidth: '100%',
                label: 'Appointment Time',
                useClearIcon: true
            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'advAppTimeHours',
                name: 'appTimeHours',
                required: true,
                useClearIcon: true,
                minValue: 8,
                maxValue: 18,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'advAppTimeMinutes',
                name: 'appTimeMinutes',
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                labelWidth: '100%',
                label: 'Duration',
                hidden: true,
                useClearIcon: true
            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'advDurTimeHours',
                name: 'durTimeHours',
                hidden: true,
                required: true,
                useClearIcon: true,
                minValue: 0,
                maxValue: 24,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'advDurTimeMinutes',
                name: 'durTimeMinutes',
                hidden: true,
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                label: 'Location',
                labelWidth: '40%',
                name: 'adviLocation',
                id: 'adviAppLocation',
                placeHolder: 'Enter Location',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'selectfield',
                label: 'Status',
                labelWidth: '40%',
                name: 'adviStatus',
                id: 'adviAppStatus',
                hidden: true,
                required: true,
                useClearIcon: true,
                options: [{
                    text: 'Active',
                    value: 'A'
                }, {
                    text: 'InActive',
                    value: 'I'
                }]
            }, {
                xtype: 'textareafield',
                label: 'Description',
                labelWidth: '40%',
                labelAlign: 'top',
                name: 'adviDesc',
                id: 'adviAppDesc',
                placeHolder: 'Enter Description',
                required: true,
                useClearIcon: true,
                maxRows: 15
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Close',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.closePopup();
                }
            }, {
                text: 'Save',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.createAppointment();
                }
            }]
        }],
        flex: 1
    }
});