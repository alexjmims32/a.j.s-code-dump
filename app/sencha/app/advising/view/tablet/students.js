Ext.define('mobEdu.advising.view.tablet.students', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Advising</h1>',
            id: 'adviStudentsTitle',
            name: 'adviStudentsTitle'
        }, {
            xtype: 'selectfield',
            id: 'adviCourses',
            valueField: 'crn',
            displayField: 'name',
            name: 'adviCourses',
            docked: 'top',
            store: mobEdu.util.getStore('mobEdu.advising.store.courses'),
            listeners: {
                change: function(selectbox, newValue, oldValue) {
                    mobEdu.advising.f.onCourseChange(newValue);
                }
            }
        }, {
            xtype: 'list',
            docked: 'left',
            id: 'studentsList',
            width: '30%',
            cls: 'advBorder',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{name}</h3></td></tr></table>',
            singleSelect: true,
            loadingText: '',
            emptyText: '<h3 align="center">No students assigned to this course.</h3>',
            store: mobEdu.util.getStore('mobEdu.advising.store.students'),
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts) {
                    mobEdu.advising.f.onStudentItemTap(view, index, target, record, e, eOpts);
                }
            }
        }, {
            xtype: 'tabpanel',
            id: 'studentDetailsTabs',
            name: 'studentDetailsTabs',
            tabBar: {
                layout: {
                    pack: 'center'
                },
                ui: 'light'
            },
            defaults: {
                style: 'border:none;'
            },
            items: [{
                layout: 'fit',
                title: '<h3>Details</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                        xtype: 'list',
                        name: 'advStudentDetail',
                        id: 'advStudentDetail',
                        baseCls: 'removeBorder',
                        disableSelection: true,
                        deferEmptyText: false,
                        itemTpl: new Ext.XTemplate('<table width="100%">' +
                            '<tr width="100%"><td width="30%"><h2>Name  </h2></td><td ><h3>: {firstName} {mi} {lastName}</h3></td>' +
                            '<tpl if="(mobEdu.util.isValueExists(image_url)===true)">' +
                            '<td rowspan=5><img height=75 width=75 src="{image_url}"></td>',
                            '<tpl else><td rowspan=3><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg"></td>',
                            '</tpl></tr>',
                            '<tpl if="(mobEdu.util.isValueExists(phone) === true)">', '<tr width="100%"><td width="30%"><h2>Phone  </h2></td><td ><h3>: {phone}</h3></td></tr>' + '</tpl>' +
                            '<tpl if="(mobEdu.util.isValueExists(email) === true)">', '<tr width="100%"><td width="30%" style="vertical-align: top"><h2>Email  </h2></td><td  style="word-wrap: break-word; word-break: break-word;display: inline-block;"><h3>:&nbsp;{email}</h3></td></tr>' + '</tpl>' +
                            '<tpl if="(mobEdu.util.isValueExists(birthDate) === true)">', '<tr width="100%"><td width="30%"><h2>DOB  </h2></td><td ><h3>: {birthDate}</h3></td></tr>' + '</tpl>' +
                            '<tpl if="(mobEdu.util.isValueExists(status) === true)">', '<tr width="100%"><td width="30%"><h2>Status  </h2></td><td ><h3>: {status}</h3></td></tr>' + '</tpl>' +
                            '</table>'),
                        store: mobEdu.util.getStore('mobEdu.advising.store.details'),
                        loadingText: '',
                        listeners: {
                            itemtap: function(view, index, target, record, item, e, eOpts) {
                                mobEdu.advising.f.loadStudentDetails();
                            }
                        }
                    }]
                }]
            }, {
                layout: 'fit',
                title: '<h3>Holds</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                        xtype: 'list',
                        name: 'advHoldsDetail',
                        id: 'advHoldsDetail',
                        disableSelection: true,
                        deferEmptyText: false,
                        emptyText: '',
                        itemTpl: new Ext.XTemplate('<table width="100%">' + '<tpl if="(mobEdu.util.isValueExists(amount) === true)">', '<tr><td width="50%"><h2>Amount</h2></td><td width="50%"><h3>: ${[mobEdu.util.formatCurrency(values.amount)]}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(reason) === true)">', '<tr><td width="50%"><h2>Reason</h2></td><td width="50%"><h3>: {reason}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(procAffected) === true)">', '<tr><td width="50%"><h2>Processes Affected</h2></td><td width="50%"><h3>: {procAffected}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(orgOffice) === true)">', '<tr><td width="50%"><h2>Originating Office</h2></td><td width="50%"><h3>: {orgOffice}</h3></td></tr>' + '</tpl>' + '<tr><td width="50%"><h2>Date</h2></td><td width="50%"><h3>: {fromDate} to {toDate}</h3></td></tr>' + '</table>'),
                        store: mobEdu.util.getStore('mobEdu.advising.store.holds'),
                        loadingText: '',
                    }]
                }],
                listeners: {
                    activate: function(newActiveItem, container, oldActiveItem, eOpts) {
                        mobEdu.advising.f.loadHolds();
                    }
                }
            }, {
                layout: 'fit',
                title: '<h3>Curriculum</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                        xtype: 'list',
                        name: 'advCurrDetail',
                        id: 'advCurrDetail',
                        baseCls: 'removeBorder',
                        disableSelection: true,
                        deferEmptyText: false,
                        itemTpl: new Ext.XTemplate('<table width="100%">' + '<tpl if="(mobEdu.util.isValueExists(major) === true)">', '<tr><td width="50%"><h2>Major</h2></td><td width="50%"><h3>: {major}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(pgmDesc) === true)">', '<tr><td width="50%"><h2>Current Program</h2></td><td width="50%"><h3>: {pgmDesc}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(level) === true)">', '<tr><td width="50%"><h2>Level</h2></td><td width="50%"><h3>: {level}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(department) === true)">', '<tr><td width="50%"><h2>Department</h2></td><td width="50%"><h3>: {department}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(college) === true)">', '<tr><td width="50%"><h2>College</h2></td><td width="50%"><h3>: {college}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(admitTerm) === true)">', '<tr><td width="50%"><h2>Admit Term</h2></td><td width="50%"><h3>: {admitTerm}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(admitType) === true)">', '<tr><td width="50%"><h2>Admit Type</h2></td><td width="50%"><h3>: {admitType}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(catalogTerm) === true)">', '<tr><td width="50%"><h2>Catlog Term</h2></td><td width="50%"><h3>: {catalogTerm}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(concentration) === true)">', '<tr><td width="50%"><h2>Major Concentration</h2></td><td width="50%"><h3>: {concentration}</h3></td></tr>' + '</tpl>' + '</table>'),
                        store: mobEdu.util.getStore('mobEdu.advising.store.currInfo'),
                        loadingText: ''
                    }]
                }],
                listeners: {
                    activate: function(newActiveItem, container, oldActiveItem, eOpts) {
                        mobEdu.advising.f.loadCurriculumInfo();
                    }
                }
            }, {
                layout: 'fit',
                title: '<h3>Courses</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                        xtype: 'list',
                        name: 'advCourseDetail',
                        id: 'advCourseDetail',
                        disableSelection: true,
                        deferEmptyText: false,
                        itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><h2>{title}</h2><h3>({crn})</h3></td></tr>' + '</table>'),
                        store: mobEdu.util.getStore('mobEdu.advising.store.regCrse'),
                        loadingText: ''
                    }]
                }],
                listeners: {
                    activate: function(newActiveItem, container, oldActiveItem, eOpts) {
                        mobEdu.advising.f.loadRegisteredCourses();
                    }
                }
            }, {
                layout: 'fit',
                title: '<h3>Appts</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                        xtype: 'list',
                        name: 'advViewAppts',
                        id: 'advViewAppts',
                        disableSelection: true,
                        deferEmptyText: false,
                        itemTpl: new Ext.XTemplate('<table width="100%">' +
                            '<tr><td width="20%"><h2>Subject</h2></td><td width="20%"><h3>: {subject}</h3></td>' +
                            '<tpl if="((mobEdu.util.isRecieved(createdBy) === true)&&(mobEdu.util.isVisible(fromID,toIDs,loginUserID) === true))">', '<td width="60%" align="right" rowspan=4><img name="accept" width="40px" height="40px" src="' + mobEdu.util.getResourcePath() + 'images/accept.png"/> &nbsp; <img name="decline" width="40px" height="40px" src="' + mobEdu.util.getResourcePath() + 'images/reject.png"/></td></tr>' + '</tpl>' +
                            '<tr><td width="35%"><h2>Date</h2></td><td width="35%"><h3>: {appointmentDate}</h3></td></tr>' +
                            '<tr><td width="50%"><h2>Location</h2></td><td width="50%"><h3>: {location}</h3></td></tr>' +
                            '<tr><td width="50%"><h2>Status</h2></td><td width="50%"><h3>: {[mobEdu.util.getStatus(values.fromDetails,values.toIDs,values.loginUserID)]}</h3></td></tr>' +
                            '<tr><td colspan="2" width="50%"><h2>Description:</h2></td></tr>', '<tr><td colspan="2" width="50%"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
                            '</table>'),
                        store: mobEdu.util.getStore('mobEdu.advising.store.viewAppts'),
                        loadingText: '',
                        listeners: {
                            itemtap: function(me, index, target, record, e, eOpts) {
                                mobEdu.advising.f.appTap(me, index, target, record, e, eOpts);
                            }
                        }
                    }]
                }],
                listeners: {
                    activate: function(newActiveItem, container, oldActiveItem, eOpts) {
                        mobEdu.advising.f.loadViewAppointments();
                    }
                }
            }, {
                layout: 'fit',
                title: '<h3>Msgs</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    layout: 'fit',
                    scrollable: false,
                    items: [{
                        xtype: 'list',
                        name: 'advViewMsgs',
                        id: 'advViewMsgs',
                        deferEmptyText: false,
                        itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                            '<tr>',
                            '<tpl if="(mobEdu.advising.f.hasEmailDirection(label)===true)">',
                            '<td align="right"><b><h2>{subject}</h2></b><br/><div class="bodyclass" style="vertical-align: top; overflow: hidden; text-overflow: ellipsis; width:200px; font-weight:normal;">{[decodeURIComponent(values.body)]}</div><br/><b><h4>{versionDate}</h4></b></td>',
                            '<tpl else>',
                            '<td align="left"><b><h2>{subject}</h2></b><br/><div class="bodyclass" style="vertical-align: top; overflow: hidden; text-overflow: ellipsis; width:200px; font-weight:normal">{[decodeURIComponent(values.body)]}</div><br/><b><h4>{versionDate}</h4></b></td>',
                            '</tpl>',
                            '</tr>' +
                            '</table>'
                        ),
                        store: mobEdu.util.getStore('mobEdu.advising.store.emailsList'),
                        loadingText: '',
                        listeners: {
                            itemtap: function(view, index, target, record, item, e, eOpts) {
                                setTimeout(function() {
                                    view.deselect(index);
                                }, 500);
                                mobEdu.advising.f.loadEmailDetail(record);
                            }
                        }
                    }]
                }],
                listeners: {
                    activate: function(newActiveItem, container, oldActiveItem, eOpts) {
                        mobEdu.advising.f.loadViewMessages();
                    }
                }
            }, {
                xtype: 'toolbar',
                id: 'studentsBTab',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [{
                    text: 'New Appointment',
                    ui: 'button',
                    handler: function() {
                        mobEdu.advising.f.showNewAppPopup();
                    }
                }, {
                    text: 'New Message',
                    ui: 'button',
                    handler: function() {
                        mobEdu.advising.f.showNewMsgPopup();
                    }
                }, {
                    text: 'Calendar',
                    id: 'advAppCal',
                    handler: function() {
                        mobEdu.advising.f.showCalendar();
                    }
                }]
            }]
        }],
        flex: 1
    }
});