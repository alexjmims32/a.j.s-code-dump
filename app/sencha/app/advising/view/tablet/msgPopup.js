Ext.define('mobEdu.advising.view.tablet.msgPopup', {
    extend: 'Ext.Panel',
    config: {
        id: 'newMsgDetailPopup',
        scroll: 'vertical',
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        layout: 'fit',
        width: '80%',
        height: '70%',
        items: [{
            xtype: 'dataview',
            name: 'msgDetails',
            id: 'msgDetails',
            emptyText: '<center><h3>No Message selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><td align="right" valign="top" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Date:</h2></td><td align="left"><h3>{versionDate}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{messageStatus}</h3></td></tr>' +
                '<tr><td align="right" valign="top" width="50%"><h2>Message:</h2></td><td align="left"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.advising.store.viewEmail'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Close',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.closeMsgDetailPopup();
                }
            }]
        }],
        flex: 1
    }
});