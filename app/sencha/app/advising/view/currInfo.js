Ext.define('mobEdu.advising.view.currInfo', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            name: 'advCurrDetail',
            id: 'advCurrDetail',
            disableSelection: true,
            deferEmptyText: false,
            emptyText: '<center><h3>No student selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tpl if="(mobEdu.util.isValueExists(major) === true)">', '<tr><td width="50%"><h2>Major</h2></td><td width="50%"><h3>: {major}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(pgmDesc) === true)">', '<tr><td width="50%"><h2>Current Program</h2></td><td width="50%"><h3>: {pgmDesc}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(level) === true)">', '<tr><td width="50%"><h2>Level</h2></td><td width="50%"><h3>: {level}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(department) === true)">', '<tr><td width="50%"><h2>Department</h2></td><td width="50%"><h3>: {department}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(college) === true)">', '<tr><td width="50%"><h2>College</h2></td><td width="50%"><h3>: {college}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(admitTerm) === true)">', '<tr><td width="50%"><h2>Admit Term</h2></td><td width="50%"><h3>: {admitTerm}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(admitType) === true)">', '<tr><td width="50%"><h2>Admit Type</h2></td><td width="50%"><h3>: {admitType}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(catalogTerm) === true)">', '<tr><td width="50%"><h2>Catlog Term</h2></td><td width="50%"><h3>: {catalogTerm}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(concentration) === true)">', '<tr><td width="50%"><h2>Major Concentration</h2></td><td width="50%"><h3>: {concentration}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isValueExists(campus) === true)">', '<tr><td width="50%"><h2>Campus </h2></td><td width="50%"><h3>: {campus}</h3></td></tr>' + '</tpl>' + '</table>'),
            store: mobEdu.util.getStore('mobEdu.advising.store.currInfo'),
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Curriculum Info</h1>'
        }],
        flex: 1
    }
});