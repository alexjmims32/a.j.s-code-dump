Ext.define('mobEdu.advising.view.details', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            name: 'advStudentDetail',
            id: 'advStudentDetail',
            disableSelection: true,
            deferEmptyText: false,
            emptyText: '<center><h3>No student selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr width="100%"><td width="30%"><h2>Name  </h2></td><td ><h3>: {firstName} {mi} {lastName}</h3></td>' +
                '<tpl if="(mobEdu.util.isValueExists(image_url)===true)">' +
                '<td rowspan=5><img height=75 width=75 src="{image_url}"></td>',
                '<tpl else><td rowspan=3><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg"></td>',
                '</tpl></tr>',
                '<tpl if="(mobEdu.util.isValueExists(phone) === true)">', '<tr width="100%"><td width="30%"><h2>Phone  </h2></td><td ><h3>: {phone}</h3></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(email) === true)">', '<tr width="100%"><td width="30%" style="vertical-align: top"><h2>Email  </h2></td><td  style="word-wrap: break-word; word-break: break-word;display: inline-block;"><h3>:&nbsp;{email}</h3></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(birthDate) === true)">', '<tr width="100%"><td width="30%"><h2>DOB  </h2></td><td ><h3>: {birthDate}</h3></td></tr>' + '</tpl>' +
                '<tpl if="(mobEdu.util.isValueExists(status) === true)">', '<tr width="100%"><td width="30%"><h2>Status  </h2></td><td ><h3>: {status}</h3></td></tr>' + '</tpl>' +
                '</table>'),
            store: mobEdu.util.getStore('mobEdu.advising.store.details'),
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Details</h1>'
        }],
        flex: 1
    }
});