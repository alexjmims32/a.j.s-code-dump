Ext.define('mobEdu.advising.view.newMsg', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'formpanel',
            items: [{
                xtype: 'textfield',
                label: 'Subject',
                name: 'adviMsgSubject',
                id: 'adviMsgSubject',
                placeHolder: 'Enter Subject',
                clearIcon: true,
                required: true
            }, {
                xtype: 'textareafield',
                label: 'Message',
                labelWidth: '30%',
                labelAlign: 'top',
                name: 'newmsg',
                id: 'newmsg',
                placeHolder: 'Enter Message',
                maxRows: 15,
                required: true
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Close',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.showViewMsgs();
                }
            }, {
                text: 'Send',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.createNewMessage();
                }
            }]
        }, {
            xtype: 'customToolbar',
            title: '<h1>New Message</h1>'
        }],
        flex: 1
    }
});