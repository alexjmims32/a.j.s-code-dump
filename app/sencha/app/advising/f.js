Ext.define('mobEdu.advising.f', {
	requires: [
		'mobEdu.advising.store.courses',
		'mobEdu.advising.store.details',
		'mobEdu.advising.store.holds',
		'mobEdu.advising.store.currInfo',
		'mobEdu.advising.store.regCrse',
		'mobEdu.advising.store.viewAppts',
		'mobEdu.advising.store.students',
		'mobEdu.advising.store.newMessage',
		'mobEdu.advising.store.appointment',
		'mobEdu.advising.store.emailsList',
		'mobEdu.advising.store.viewEmail'
	],
	statics: {
		menuValue: null,
		studentId: null,
		termCode: null,
		studentMsgId: null,

		changeTitle: function(orientation) {
			mobEdu.util.get('mobEdu.advising.view.phone.students');
			if (orientation === 'portrait') {
				Ext.getCmp('adviStudentsTitle').setTitle('<h1 class="titleOverflow">Students Attending My Class</h1>');
			} else {
				Ext.getCmp('adviStudentsTitle').setTitle('<h1>Students Attending My Class</h1>');
			}
		},

		onMenuItemTap: function(viewRef, selectedIndex, target, record, e, eOpts) {
			mobEdu.counselor.f.role = '';
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			if (Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.phone.students');
			} else {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
			}
			mobEdu.advising.f.menuValue = record.get('value');
			if (mobEdu.advising.f.menuValue == 0) {
				Ext.getCmp('adviStudentsTitle').setTitle('<h1 class="titleOverflow">Students Attending My Class</h1>');
				mobEdu.advising.f.loadCourses();
			} else {
				Ext.getCmp('adviStudentsTitle').setTitle('<h1>My Advisees</h1>');
				mobEdu.advising.f.loadRegisteredStudents();
			}
		},

		loadCourses: function() {
			if (Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.phone.students');
			} else {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
			}
			var cmp = Ext.getCmp('adviCourses');
			cmp.suspendEvents();
			var store = mobEdu.util.getStore('mobEdu.advising.store.courses');
			store.getProxy().setUrl(webserver + 'getAdvisingCourses');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.advising.f.coursesResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('advising', 'courses');
		},

		coursesResponseHandler: function() {
			var cmp = Ext.getCmp('adviCourses');
			var value = cmp.getValue();
			if (value != null && value != '') {
				mobEdu.advising.f.onCourseChange(value);
			}
			cmp.resumeEvents(true);
		},

		onCourseChange: function(value) {
			var store = mobEdu.util.getStore('mobEdu.advising.store.courses');
			var rec = store.findRecord('crn', value);
			if (rec != null) {
				mobEdu.advising.f.termCode = rec.get('termCode');
				mobEdu.advising.f.loadRegisteredStudents(rec);
			}
		},

		loadRegisteredStudents: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.advising.store.students');
			store.removeAll();
			store.removed = [];
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (mobEdu.advising.f.menuValue == 0) {
				store.getProxy().setUrl(webserver + 'getAdvisingRegisteredStudents');
				store.getProxy().setExtraParams({
					studentId: mobEdu.main.f.getStudentId(),
					termCode: rec.get('termCode'),
					crn: rec.get('crn'),
					companyId: companyId
				});
			} else {
				store.getProxy().setUrl(webserver + 'getAdvisees');
				store.getProxy().setExtraParams({
					studentId: mobEdu.main.f.getStudentId(),
					companyId: companyId
				});
			}
			store.getProxy().afterRequest = function() {
				mobEdu.advising.f.showStudents();
			};
			store.load();
			mobEdu.util.gaTrackEvent('advising', 'registeredStudents');
		},

		onStudentItemTap: function(view, index, target, record, e, eOpts) {
			mobEdu.advising.f.studentId = record.get('studentId');
			mobEdu.advising.f.termCode = record.get('termCode');
			if (Ext.os.is.phone) {
				mobEdu.util.deselectSelectedItem(index, view);
				mobEdu.advising.f.showSubMenu();
				Ext.getCmp('adviSunMenuTitle').setTitle('<h1>' + record.get('name') + '</h1>');
			} else {
				mobEdu.advising.f.loadStudentDetails();
				Ext.getCmp('studentDetailsTabs').setActiveItem(0);
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
			}
		},

		loadStudentDetails: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.details');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			if (!Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advStudentDetail').setEmptyText('');
				Ext.getCmp('advAppCal').hide();
			}
			if (mobEdu.advising.f.studentId != null && mobEdu.advising.f.studentId != '') {
				store.getProxy().setUrl(webserver + 'getAdvisingStudentDetails');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setExtraParams({
					reportId: mobEdu.advising.f.studentId,
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.advising.f.showDetails();
				};
				store.load();
				mobEdu.util.gaTrackEvent('advising', 'registeredStudents');
			} else {
				if (store.data.length > 0) {
					store.removeAll();
					store.removed = [];
				}
			}
		},

		loadHolds: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.holds');
			store.removeAll();
			store.removed = [];
			store.sync();
			if (!Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advHoldsDetail').setEmptyText('');
				Ext.getCmp('advAppCal').hide();
			}
			if (mobEdu.advising.f.studentId != null && mobEdu.advising.f.studentId != '') {
				store.getProxy().setUrl(webserver + 'getAdvisingHoldsInfo');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setExtraParams({
					reportId: mobEdu.advising.f.studentId,
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.advising.f.holdsResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('advising', 'Holds');
			} else {
				if (store.data.length > 0) {
					store.removeAll();
					store.removed = [];
				}
			}
		},

		holdsResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.holds');
			if (store.data.length > 0) {
				if (Ext.os.is.phone) {
					mobEdu.advising.f.showHolds();
				}
			} else {
				if (Ext.os.is.phone) {
					Ext.Msg.show({
						message: '<p>No holds exists.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					mobEdu.util.get('mobEdu.advising.view.tablet.students');
					Ext.getCmp('advHoldsDetail').setEmptyText('Student has no Holds.');
				}
			}
		},

		loadCurriculumInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.currInfo');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			if (!Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advCurrDetail').setEmptyText('');
				Ext.getCmp('advAppCal').hide();
			}
			if (mobEdu.advising.f.studentId != null && mobEdu.advising.f.studentId != '') {
				store.getProxy().setUrl(webserver + 'getAdvisingCurriculumInfo');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setExtraParams({
					reportId: mobEdu.advising.f.studentId,
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.advising.f.curriculumInfoResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('advising', 'curriculumInfo');
			} else {
				if (store.data.length > 0) {
					store.removeAll();
					store.removed = [];
				}
			}
		},

		curriculumInfoResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.currInfo');
			if (store.data.length > 0) {
				if (Ext.os.is.phone) {
					mobEdu.advising.f.showCurrInfo();
				}
			} else {
				if (Ext.os.is.phone) {
					Ext.Msg.show({
						message: '<p>No curriculum info exists.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					mobEdu.util.get('mobEdu.advising.view.tablet.students');
					Ext.getCmp('advCurrDetail').setEmptyText('Student has no Curriculum Info.');
				}
			}
		},

		loadRegisteredCourses: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.regCrse');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			if (!Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advCourseDetail').setEmptyText('');
				Ext.getCmp('advAppCal').hide();
			}
			if (mobEdu.advising.f.studentId != null && mobEdu.advising.f.studentId != '') {
				if (mobEdu.advising.f.termCode != null && mobEdu.advising.f.termCode != '') {
					store.getProxy().setUrl(webserver + 'getAdvisingRegisteredCourses');
					store.getProxy().setHeaders({
						Authorization: mobEdu.util.getAuthString(),
						companyID: companyId
					});
					store.getProxy().setExtraParams({
						reportId: mobEdu.advising.f.studentId,
						termCode: mobEdu.advising.f.termCode,
						companyId: companyId
					});
					store.getProxy().afterRequest = function() {
						mobEdu.advising.f.registeredCoursesResponseHandler();
					};
					store.load();
					mobEdu.util.gaTrackEvent('advising', 'registeredCourses');
				} else {
					if (store.data.length > 0) {
						store.removeAll();
						store.removed = [];
					} else {
						if (!Ext.os.is.phone) {
							mobEdu.util.get('mobEdu.advising.view.tablet.students');
							Ext.getCmp('advCourseDetail').setEmptyText('Student has no Registered Courses.');
						}
					}
				}
			} else {
				if (store.data.length > 0) {
					store.removeAll();
					store.removed = [];
				} else {
					if (!Ext.os.is.phone) {
						mobEdu.util.get('mobEdu.advising.view.tablet.students');
						Ext.getCmp('advCourseDetail').setEmptyText('Student has no Registered Courses.');
					}
				}
			}
		},

		registeredCoursesResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.regCrse');
			if (store.data.length > 0) {
				if (Ext.os.is.phone) {
					mobEdu.advising.f.showCourses();
				}
			} else {
				if (Ext.os.is.phone) {
					Ext.Msg.show({
						message: '<p>No registered courses exists.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					mobEdu.util.get('mobEdu.advising.view.tablet.students');
					Ext.getCmp('advCourseDetail').setEmptyText('Student has no Registered Courses.');
				}
			}
		},

		loadViewAppointments: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.viewAppts');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			if (!Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advViewAppts').setEmptyText('');
			}
			if (mobEdu.advising.f.studentId != null && mobEdu.advising.f.studentId != '') {
				store.getProxy().setUrl(commonwebserver + 'appointment/createdsearch');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setExtraParams({
					bannerID: mobEdu.advising.f.studentId,
					companyId: companyId,
					loginID: mobEdu.util.getStudentId()
				});
				store.getProxy().afterRequest = function() {
					mobEdu.advising.f.viewAppointmentsResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('advising', 'viewAppointments');
			} else {
				if (store.data.length > 0) {
					store.removeAll();
					store.removed = [];
				}
			}
		},
		loadAppointments: function() {
			mobEdu.advising.f.loadViewAppointments();
		},
		viewAppointmentsResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.viewAppts');
			if (Ext.os.is.phone) {
				store.sort('appointmentDate', 'ASC');
				mobEdu.advising.f.showViewAppts();
			} else {
				if (mobEdu.counselor.f.role == 'counselor') {
					mobEdu.advising.f.showViewAppts();
				} else {
					mobEdu.util.get('mobEdu.advising.view.tablet.students');
					Ext.getCmp('advViewAppts').setEmptyText('Student has no Appointments.');
					Ext.getCmp('advAppCal').show();
				}
			}
		},

		loadViewMessages: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.emailsList');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			if (!Ext.os.is.phone) {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advViewMsgs').setEmptyText('');
				Ext.getCmp('advAppCal').hide();
			}
			if (mobEdu.advising.f.studentId != null && mobEdu.advising.f.studentId != '') {
				store.getProxy().setUrl(commonwebserver + 'message/messageList');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setExtraParams({
					bannerID: mobEdu.advising.f.studentId,
					companyId: companyId,
					loginID: mobEdu.util.getStudentId()
				});
				store.getProxy().afterRequest = function() {
					mobEdu.advising.f.viewMessagessResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('advising', 'viewMessages');
			} else {
				if (store.data.length > 0) {
					store.removeAll();
					store.removed = [];
				}
			}
		},

		viewMessagessResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.emailsList');
			if (Ext.os.is.phone) {
				mobEdu.advising.f.showViewMsgs();
			} else {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advViewMsgs').setEmptyText('Student has no Messages.');
			}
		},

		hasEmailDirection: function(direction) {
			if (direction == "INBOX") {
				return true;
			} else {
				return false;
			}
		},

		loadEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;
			var detailStore = mobEdu.util.getStore('mobEdu.advising.store.viewEmail');

			detailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			detailStore.getProxy().setUrl(commonwebserver + 'message/get');
			detailStore.getProxy().setExtraParams({
				messageID: eId,
				companyId: companyId,
				loginID: mobEdu.util.getStudentId()
			});
			detailStore.getProxy().afterRequest = function() {
				mobEdu.advising.f.loadEmailDetailResponseHandler();
			};
			detailStore.load();
		},

		loadEmailDetailResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.viewEmail');
			if (store.data.length > 0) {
				if (Ext.os.is.phone) {
					mobEdu.advising.f.showDetailMsg();
				} else {
					if (mobEdu.counselor.f.role == 'counselor') {
						mobEdu.advising.f.showDetailMsg();
					} else {
						mobEdu.advising.f.showMsgDetailsPopup();
					}
				}
			} else {
				Ext.Msg.show({
					message: '<p>No messages exists.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showMsgDetailsPopup: function(popupView) {
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.advising.view.tablet.msgPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		createAppointment: function() {
			if (!(mobEdu.counselor.f.role == 'counselor')) {
				if (Ext.os.is.phone) {
					var aSub = Ext.getCmp('adviSubject').getValue();
					var aDes = Ext.getCmp('adviDesc').getValue();
					var aDate = Ext.getCmp('advidate').getValue();
					var aLoc = Ext.getCmp('adviLocation').getValue();
					var aStatus = Ext.getCmp('adviStatus').getValue();
					var aTimeHours = Ext.getCmp('appTimeHours').getValue();
					var aTimeMinutes = Ext.getCmp('appTimeMinutes').getValue();
					var dTimeHours = Ext.getCmp('durTimeHours').getValue();
					var dTimeMinutes = Ext.getCmp('durTimeMinutes').getValue();
				} else {
					var aSub = Ext.getCmp('adviAppSubject').getValue();
					var aDes = Ext.getCmp('adviAppDesc').getValue();
					var aDate = Ext.getCmp('adviAppdate').getValue();
					var aLoc = Ext.getCmp('adviAppLocation').getValue();
					var aStatus = Ext.getCmp('adviAppStatus').getValue();
					var aTimeHours = Ext.getCmp('advAppTimeHours').getValue();
					var aTimeMinutes = Ext.getCmp('advAppTimeMinutes').getValue();
					var dTimeHours = Ext.getCmp('advDurTimeHours').getValue();
					var dTimeMinutes = Ext.getCmp('advDurTimeMinutes').getValue();
				}
			} else {
				var aSub = Ext.getCmp('adviSubject').getValue();
				var aDes = Ext.getCmp('adviDesc').getValue();
				var aDate = Ext.getCmp('advidate').getValue();
				var aLoc = Ext.getCmp('adviLocation').getValue();
				var aStatus = Ext.getCmp('adviStatus').getValue();
				var aTimeHours = Ext.getCmp('appTimeHours').getValue();
				var aTimeMinutes = Ext.getCmp('appTimeMinutes').getValue();
				var dTimeHours = Ext.getCmp('durTimeHours').getValue();
				var dTimeMinutes = Ext.getCmp('durTimeMinutes').getValue();
			}

			if (aStatus == 'select') {
				aStatus = '';
			}
			if (aTimeHours == 0 || aTimeHours == null) {
				aTimeHours = '';
			}

			if (dTimeHours == 0 || dTimeHours == null) {
				dTimeHours = '1';
			}
			if (aSub.charAt(0) == ' ' || aDes.charAt(0) == ' ') {
				Ext.Msg.show({
					message: '<p>Subject & Description must start with alphanumeric.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else if (aTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: '<p>Appointment time between 8AM to 18PM.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aStatus != '' && aTimeHours != '') {
					var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);
					var selectDate = aDate;
					selectDate = Ext.Date.format(selectDate, 'm/d/Y');
					if (aTimeMinutes < 10) {
						aTimeMinutes = "0" + aTimeMinutes;
					}
					var selectDateFormatted = Ext.Date.parse(selectDate + " " + aTimeHours + ":" + aTimeMinutes, 'm/d/Y G:i');
					if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
						mobEdu.advising.f.appointmentResponse();
					} else {
						Ext.Msg.show({
							title: 'Invalid Date',
							message: '<p>Please select a future date..</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				} else {
					Ext.Msg.show({
						message: '<p>Please provide all mandatory fields.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}
		},

		appointmentResponse: function() {
			if (!(mobEdu.counselor.f.role == 'counselor')) {
				if (Ext.os.is.phone) {
					var aSub = Ext.getCmp('adviSubject').getValue();
					var aDes = Ext.getCmp('adviDesc').getValue();
					var aDate = Ext.getCmp('advidate').getValue();
					var aLoc = Ext.getCmp('adviLocation').getValue();
					var aStatus = Ext.getCmp('adviStatus').getValue();
					var aTimeH = Ext.getCmp('appTimeHours').getValue();
					var aTimeM = Ext.getCmp('appTimeMinutes').getValue();
					var dTimeH = Ext.getCmp('durTimeHours').getValue();
					var dTimeM = Ext.getCmp('durTimeMinutes').getValue();
				} else {
					var aSub = Ext.getCmp('adviAppSubject').getValue();
					var aDes = Ext.getCmp('adviAppDesc').getValue();
					var aDate = Ext.getCmp('adviAppdate').getValue();
					var aLoc = Ext.getCmp('adviAppLocation').getValue();
					var aStatus = Ext.getCmp('adviAppStatus').getValue();
					var aTimeH = Ext.getCmp('advAppTimeHours').getValue();
					var aTimeM = Ext.getCmp('advAppTimeMinutes').getValue();
					var dTimeH = Ext.getCmp('advDurTimeHours').getValue();
					var dTimeM = Ext.getCmp('advDurTimeMinutes').getValue();
				}
			} else {
				var aSub = Ext.getCmp('adviSubject').getValue();
				var aDes = Ext.getCmp('adviDesc').getValue();
				var aDate = Ext.getCmp('advidate').getValue();
				var aLoc = Ext.getCmp('adviLocation').getValue();
				var aStatus = Ext.getCmp('adviStatus').getValue();
				var aTimeH = Ext.getCmp('appTimeHours').getValue();
				var aTimeM = Ext.getCmp('appTimeMinutes').getValue();
				var dTimeH = Ext.getCmp('durTimeHours').getValue();
				var dTimeM = Ext.getCmp('durTimeMinutes').getValue();
			}

			var dt = Ext.Date.add(aDate, Ext.Date.MINUTE, 30);
			var timeHours = dt.getHours();
			var timeMinutes = dt.getMinutes();
			var appTime;
			var updateTime = timeHours + ':' + timeMinutes;

			if (dTimeH == null) {
				dTimeH = '';
			}
			if (dTimeM == null) {
				dTimeM = '';
			}

			durTime = dTimeH + ':' + dTimeM;

			var newDateFormat =
				console.log(newDateFormat);

			if (aTimeH == null) {
				aTimeH = '';
			}
			if (aTimeM == null) {
				aTimeM = '';
			}

			appTime = aTimeH + ':' + aTimeM;

			var role = mobEdu.util.getRole();
			if (role.match(/STUDENT/gi)) {
				aStatus = 'Requested';
			} else {
				aStatus = 'Scheduled';
			}

			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(aDate, 'm/d/Y');
			}
			var appDateTime = appDateFormatted + ' ' + appTime;

			var newAppStore = mobEdu.util.getStore('mobEdu.advising.store.appointment');

			newAppStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			newAppStore.getProxy().setUrl(commonwebserver + 'appointment/add');
			newAppStore.getProxy().setExtraParams({
				bannerID: mobEdu.advising.f.studentId,
				subject: aSub,
				body: aDes,
				appointmentDate: appDateTime,
				location: aLoc,
				participants: mobEdu.util.getStudentId() + ',sent;' + mobEdu.advising.f.studentId + ',sent',
				duration: durTime,
				status: aStatus,
				companyId: companyId,
				loginID: mobEdu.util.getStudentId()
			});
			newAppStore.getProxy().afterRequest = function() {
				mobEdu.advising.f.createAppointmentResponseHandler();
			};
			newAppStore.load();

		},

		createAppointmentResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.appointment');
			var res = store.getProxy().getReader().rawData.status;
			if (res.toUpperCase() == 'SUCCESS') {
				Ext.Msg.show({
					title: '',
					message: '<p> Appointment created successfully. </p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (Ext.os.is.phone) {
							mobEdu.util.destroyView('mobEdu.advising.view.viewAppts');
							mobEdu.advising.f.clearAppointment();
							mobEdu.advising.f.loadViewAppointments();
						} else {
							if (!(mobEdu.counselor.f.role == 'counselor')) {
								mobEdu.util.destroyView('mobEdu.advising.view.viewAppts');
								mobEdu.advising.f.closePopup();
								mobEdu.advising.f.loadViewAppointments();
								mobEdu.util.get('mobEdu.advising.view.tablet.students');
								Ext.getCmp('studentDetailsTabs').setActiveItem(4);
							} else {
								mobEdu.util.destroyView('mobEdu.advising.view.viewAppts');
								mobEdu.advising.f.clearAppointment();
								mobEdu.advising.f.loadViewAppointments();
							}
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: '',
					message: res,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		closePopup: function() {
			var popUp = Ext.getCmp('newAppPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		closeMsgPopup: function() {
			var popUp = Ext.getCmp('newMsgPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		closeMsgDetailPopup: function() {
			var popUp = Ext.getCmp('newMsgDetailPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		createNewMessage: function() {
			//to return appropriate student Id based on authentication.
			mobEdu.finaid.f.getStudentId(mobEdu.advising.f.studentId);
		},

		createMessage: function() {
			// mobEdu.util.get('mobEdu.advising.view.tablet.newMsgPopup');
			if (!(mobEdu.counselor.f.role == 'counselor')) {
				var aSub = Ext.getCmp('advingMsgSubject').getValue();
				aSub = aSub.trim();
				var aDes = Ext.getCmp('advisingnewmsg').getValue();
				aDes = aDes.trim();
			} else {
				var aSub = Ext.getCmp('adviMsgSubject').getValue();
				aSub = aSub.trim();
				var aDes = Ext.getCmp('newmsg').getValue();
				aDes = aDes.trim();
			}

			var store = mobEdu.util.getStore('mobEdu.advising.store.newMessage');
			store.removeAll();
			store.removed = [];
			store.sync();
			if (aSub == '' || aDes == '') {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
				});
			} else {
				var regExp = new RegExp("^[0-9a-zA-Z&. ]+$");

				if (!regExp.test(aDes)) {
					Ext.Msg.show({
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid Message Content.</p>',
						buttons: Ext.MessageBox.OK,
					});
				} else {
					if (!regExp.test(aSub)) {
						Ext.Msg.show({
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid Message Content.</p>',
							buttons: Ext.MessageBox.OK,
						});
					} else {
						store.getProxy().setUrl(commonwebserver + 'message/send');
						store.getProxy().setHeaders({
							Authorization: mobEdu.util.getAuthString(),
							companyID: companyId
						});
						store.getProxy().setExtraParams({
							bannerID: mobEdu.advising.f.studentId,
							subject: aSub,
							body: aDes,
							conversationID: '',
							companyId: companyId,
							loginID: mobEdu.util.getStudentId()
						});
						store.getProxy().afterRequest = function() {
							mobEdu.advising.f.createMessageResponseHandler();
						};
						store.load();
						mobEdu.util.gaTrackEvent('advising', 'new Message');
					}
				}
			}
		},

		createMessageResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.advising.store.newMessage');
			var res = store.getProxy().getReader().rawData.status;
			if (res.toUpperCase() == 'SUCCESS') {
				Ext.Msg.show({
					message: '<p> Message sent successfully. </p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (Ext.os.is.phone) {
							mobEdu.advising.f.loadViewMessages();
							mobEdu.advising.f.clearMessage();
						} else {
							if (!(mobEdu.counselor.f.role == 'counselor')) {
								mobEdu.advising.f.closeMsgPopup();
								mobEdu.advising.f.loadViewMessages();
								mobEdu.util.get('mobEdu.advising.view.tablet.students');
								Ext.getCmp('studentDetailsTabs').setActiveItem(5);
							} else {
								mobEdu.advising.f.loadViewMessages();
								mobEdu.advising.f.clearMessage();
								mobEdu.advising.f.showViewMsgs();
							}
						}
					}
				});
			} else {
				Ext.Msg.show({
					message: res,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			// var dt = Ext.Date.add(presentDate, Ext.Date.MINUTE, 30);
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
			}
			if (newDateFormatted == presentDateFormatted) {
				if ((mobEdu.counselor.f.role == 'counselor')) {
					if (Ext.getCmp('appTimeHours') != undefined) {
						Ext.getCmp('appTimeHours').setMinValue(timeH);
						Ext.getCmp('appTimeHours').setValue(timeH);
					}
					if (Ext.getCmp('appTimeMinutes') != undefined) {
						Ext.getCmp('appTimeMinutes').setMinValue(timeM);
						Ext.getCmp('appTimeMinutes').setValue(timeM);
					}
				} else {
					if (Ext.os.is.phone) {
						if (Ext.getCmp('appTimeHours') != undefined) {
							Ext.getCmp('appTimeHours').setMinValue(timeH);
							Ext.getCmp('appTimeHours').setValue(timeH);
						}
						if (Ext.getCmp('appTimeMinutes') != undefined) {
							Ext.getCmp('appTimeMinutes').setMinValue(timeM);
							Ext.getCmp('appTimeMinutes').setValue(timeM);
						}

					} else {
						if (Ext.getCmp('advAppTimeHours') != undefined) {
							Ext.getCmp('advAppTimeHours').setMinValue(timeH);
							Ext.getCmp('advAppTimeHours').setValue(timeH);
						}
						if (Ext.getCmp('advAppTimeMinutes') != undefined) {
							Ext.getCmp('advAppTimeMinutes').setMinValue(timeM);
							Ext.getCmp('advAppTimeMinutes').setValue(timeM);
						}
					}
				}

			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							if ((mobEdu.counselor.f.role == 'counselor')) {
								Ext.getCmp('appdate').setValue(new Date());
								Ext.getCmp('appTimeHours').setMinValue(timeH);
								Ext.getCmp('appTimeMinutes').setMinValue(timeM);
								Ext.getCmp('appTimeHours').setValue(timeH);
								Ext.getCmp('appTimeMinutes').setValue(timeM);
							} else {
								if (Ext.os.is.phone) {
									Ext.getCmp('appdate').setValue(new Date());
									Ext.getCmp('appTimeHours').setMinValue(timeH);
									Ext.getCmp('appTimeMinutes').setMinValue(timeM);
									Ext.getCmp('appTimeHours').setValue(timeH);
									Ext.getCmp('appTimeMinutes').setValue(timeM);
								} else {
									Ext.getCmp('adviAppdate').setValue(new Date());
									Ext.getCmp('advAppTimeHours').setMinValue(timeH);
									Ext.getCmp('advAppTimeMinutes').setMinValue(timeM);
									Ext.getCmp('advAppTimeMinutes').setValue(timeH);
									Ext.getCmp('advAppTimeMinutes').setValue(timeM);
								}
							}
						}
					}
				});
			} else {
				if ((mobEdu.counselor.f.role == 'counselor')) {
					Ext.getCmp('appTimeHours').setMinValue(8);
					Ext.getCmp('appTimeHours').setValue(8);
					Ext.getCmp('appTimeMinutes').setMinValue(0);
					Ext.getCmp('appTimeMinutes').setValue(0);
				} else {
					if (Ext.os.is.phone) {
						Ext.getCmp('appTimeHours').setMinValue(8);
						Ext.getCmp('appTimeHours').setValue(8);
						Ext.getCmp('appTimeMinutes').setMinValue(0);
						Ext.getCmp('appTimeMinutes').setValue(0);
					} else {
						Ext.getCmp('advAppTimeHours').setMinValue(8);
						Ext.getCmp('advAppTimeHours').setValue(8);
						Ext.getCmp('advAppTimeMinutes').setMinValue(0);
						Ext.getCmp('advAppTimeMinutes').setValue(0);
					}
				}
			}
		},

		clearAppointment: function() {
			/*Ext.getCmp('adviSubject').setValue('');
			Ext.getCmp('adviDesc').setValue('');
			Ext.getCmp('advidate').setValue(new Date());
			Ext.getCmp('adviLocation').setValue('');
			Ext.getCmp('adviStatus').setValue('');
			Ext.getCmp('appTimeHours').setMinValue(8);
			Ext.getCmp('appTimeMinutes').setMinValue(0);
			Ext.getCmp('durTimeHours').setValue(0);
			Ext.getCmp('durTimeMinutes').setValue(0);*/

			if (!(mobEdu.counselor.f.role == 'counselor')) {
				if (Ext.os.is.phone) {
					Ext.getCmp('adviSubject').setValue('');
					Ext.getCmp('adviDesc').setValue('');
					Ext.getCmp('advidate').setValue(new Date());
					Ext.getCmp('adviLocation').setValue('');
					Ext.getCmp('adviStatus').setValue('');
					Ext.getCmp('appTimeHours').setMinValue(8);
					Ext.getCmp('appTimeMinutes').setMinValue(0);
					Ext.getCmp('durTimeHours').setValue(0);
					Ext.getCmp('durTimeMinutes').setValue(0);
				} else {
					Ext.getCmp('adviAppSubject').setValue('');
					Ext.getCmp('adviAppDesc').setValue('');
					Ext.getCmp('adviAppdate').setValue(new Date());
					Ext.getCmp('adviAppLocation').setValue('');
					Ext.getCmp('adviAppStatus').setValue('');
					Ext.getCmp('advAppTimeHours').setMinValue(8);
					Ext.getCmp('advAppTimeMinutes').setMinValue(0);
					Ext.getCmp('advDurTimeHours').setValue(0);
					Ext.getCmp('advDurTimeMinutes').setValue(0);
				}
			} else {
				Ext.getCmp('adviSubject').setValue('');
				Ext.getCmp('adviDesc').setValue('');
				Ext.getCmp('advidate').setValue(new Date());
				Ext.getCmp('adviLocation').setValue('');
				Ext.getCmp('adviStatus').setValue('');
				Ext.getCmp('appTimeHours').setMinValue(8);
				Ext.getCmp('appTimeMinutes').setMinValue(0);
				Ext.getCmp('durTimeHours').setValue(0);
				Ext.getCmp('durTimeMinutes').setValue(0);
			}
		},

		clearMessage: function() {
			Ext.getCmp('adviMsgSubject').setValue('');
			Ext.getCmp('newmsg').setValue('');
		},

		showMenu: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.advising.view.menu');
			mobEdu.util.getStore('mobEdu.advising.store.details').clearData();
		},

		showSubMenu: function() {
			if (mobEdu.counselor.f.role == 'counselor') {
				mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
				mobEdu.util.show('mobEdu.counselor.view.details');
			} else {
				mobEdu.util.updatePrevView(mobEdu.advising.f.showStudents);
				mobEdu.util.show('mobEdu.advising.view.subMenu');
			}
		},

		showCourses: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showSubMenu);
			mobEdu.util.show('mobEdu.advising.view.courses');
		},

		showCurrInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showSubMenu);
			mobEdu.util.show('mobEdu.advising.view.currInfo');
		},

		showHolds: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showSubMenu);
			mobEdu.util.show('mobEdu.advising.view.holds');
		},

		showNewAppt: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.loadAppointments);
			mobEdu.util.show('mobEdu.advising.view.newAppt');
		},

		showNewMsg: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showViewMsgs);
			mobEdu.util.show('mobEdu.advising.view.newMsg');
		},

		showViewAppts: function() {
			if (mobEdu.counselor.f.role == 'counselor') {
				mobEdu.util.updatePrevView(mobEdu.counselor.f.showDetails);
				mobEdu.counselor.f.showDetails();
			} else {
				mobEdu.util.updatePrevView(mobEdu.advising.f.showSubMenu);
				mobEdu.util.show('mobEdu.advising.view.viewAppts');
			}
		},

		showViewMsgs: function() {
			if (mobEdu.counselor.f.role == 'counselor') {
				mobEdu.util.get('mobEdu.advising.view.newMsg');
				mobEdu.advising.f.clearMessage();
				mobEdu.util.updatePrevView(mobEdu.counselor.f.showDetails);
				mobEdu.util.get('mobEdu.advising.view.viewMsgs');
				Ext.getCmp('newViewMsg').hide();
				mobEdu.counselor.f.showDetails();
			} else {
				mobEdu.util.get('mobEdu.advising.view.newMsg');
				mobEdu.advising.f.clearMessage();
				mobEdu.util.updatePrevView(mobEdu.advising.f.showSubMenu);
				mobEdu.util.get('mobEdu.advising.view.viewMsgs');
				Ext.getCmp('newViewMsg').show();
				mobEdu.util.show('mobEdu.advising.view.viewMsgs');
			}
		},

		showDetailMsg: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showViewMsgs);
			mobEdu.util.show('mobEdu.advising.view.emailDetail');
		},

		showStudents: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showMenu);
			if (Ext.os.is.phone) {
				mobEdu.util.show('mobEdu.advising.view.phone.students');
				if (mobEdu.advising.f.menuValue == 0) {
					Ext.getCmp('stdntList').setEmptyText('<h3 align="center">No students are assigned to this course.</h3>');
					Ext.getCmp('advisCourses').show();
				} else {
					Ext.getCmp('stdntList').setEmptyText('<h3 align="center">No students are assigned to you.</h3>');
					Ext.getCmp('advisCourses').hide();
				}
			} else {
				var studentsData = mobEdu.util.getStore('mobEdu.advising.store.students');
				var students = studentsData.data.all;
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				if (students.length > 0) {
					Ext.getCmp("studentsList").select(0);
					var record = Ext.getCmp("studentsList").getActiveItem().getItems(0).items[0].getRecord();
					mobEdu.advising.f.studentId = record.get('studentId');
					mobEdu.advising.f.loadStudentDetails();
					Ext.getCmp('studentDetailsTabs').show();
					Ext.getCmp('studentsList').setWidth('30%');
				} else {
					mobEdu.util.getStore('mobEdu.advising.store.details').clearData();
					mobEdu.advising.f.studentId = null;
					Ext.getCmp('studentDetailsTabs').hide();
					Ext.getCmp('studentsList').setWidth('100%');
				}
				Ext.getCmp('studentDetailsTabs').setActiveItem(0);
				Ext.getCmp('advAppCal').hide();
				mobEdu.util.show('mobEdu.advising.view.tablet.students');
				if (mobEdu.advising.f.menuValue == 0) {
					Ext.getCmp('studentsList').setEmptyText('<h3 align="center">No students are assigned to this course.</h3>');
					Ext.getCmp('adviCourses').show();
				} else {
					Ext.getCmp('studentsList').setEmptyText('<h3 align="center">No students are assigned to you.</h3>');
					Ext.getCmp('adviCourses').hide();
				}
			}
		},

		showDetails: function() {
			if (Ext.os.is.phone) {
				mobEdu.util.updatePrevView(mobEdu.advising.f.showSubMenu);
				mobEdu.util.show('mobEdu.advising.view.details');
			} else {
				mobEdu.util.get('mobEdu.advising.view.tablet.students');
				Ext.getCmp('advStudentDetail').setEmptyText('No details');
			}
		},

		showConselorDetails: function() {
			if (mobEdu.counselor.f.flag == true) {
				mobEdu.util.updatePrevView(mobEdu.counselor.f.showAdvSearchList);
			} else {
				mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
			}
			mobEdu.util.show('mobEdu.counselor.view.details');
		},

		showCounNewAppt: function() {
			mobEdu.advising.f.studentId = mobEdu.counselor.f.studentId;
			mobEdu.util.updatePrevView(mobEdu.counselor.f.showDetails);
			mobEdu.util.show('mobEdu.advising.view.newAppt');
		},

		showConNewMsg: function() {
			mobEdu.advising.f.studentId = mobEdu.counselor.f.studentId;
			mobEdu.util.updatePrevView(mobEdu.counselor.f.showDetails);
			mobEdu.util.show('mobEdu.advising.view.newMsg');
		},


		statusValue: function(value) {
			if (value == 'I') {
				return 'Inactive';
			} else if (value == 'A') {
				return 'Active';
			}
		},

		showCalendar: function() {
			mobEdu.advising.f.loadCalendarView();
			if (mobEdu.counselor.f.role == 'counselor') {
				mobEdu.util.updatePrevView(mobEdu.advising.f.loadViewAppointments);
			} else {
				if (Ext.os.is.phone) {
					mobEdu.util.updatePrevView(mobEdu.advising.f.showViewAppts);
				} else {
					mobEdu.util.updatePrevView(mobEdu.advising.f.showStudentsView);
				}
			}
			mobEdu.util.show('mobEdu.advising.view.appointmentCalendar');
		},

		showStudentsView: function() {
			mobEdu.util.updatePrevView(mobEdu.advising.f.showMenu);
			mobEdu.util.show('mobEdu.advising.view.tablet.students');
		},

		loadCalendarView: function() {
			mobEdu.main.f.calendarInstance = 'advAppt';
			mobEdu.util.get('mobEdu.advising.view.appointmentCalendar');
			var appointmentDock = Ext.getCmp('advAppointmentsDock');
			appointmentDock.remove(calendar);

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'crseCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			appointmentDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.advising.f.setAppointmentScheduleInfo(new Date());
		},
		setAppointmentScheduleInfo: function(selectedDate) {
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			var newDateValue = (mobEdu.advising.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.advising.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());
			var scheduleInfo = '';
			var appointmentsStore = mobEdu.util.getStore('mobEdu.advising.store.viewAppts')
			appointmentsStore.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var appDateValue = appDate.split(' ');
				var dateFormatted = appDateValue[0];
				var appDesc = ' ';
				if (newDateValue == dateFormatted) {
					var appTime = appDateValue[1];
					var appSubject = rec.get('subject');
					var appLocation = rec.get('location');
					scheduleInfo = mobEdu.advising.f.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc);
				}
			});

			//set the event schedule info in calendar view label
			Ext.getCmp('advAppointmentLabel').setHtml(scheduleInfo);
		},
		formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc) {
			//var user = mobEdu.util.getStore('mobEdu.enquire.login.store.login').data.first().raw.userInfo.userName;
			if (appSubject == null) {
				appSubject = '';
			}
			if (appTime == null) {
				appTime = '';
			}
			if (appLocation == null) {
				appLocation = '';
			}
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + decodeURIComponent(appSubject) + '</h3>';
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
			scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + decodeURIComponent(appLocation) + '</h3>';
			scheduleInfo = scheduleInfo + '<br></br>';
			return scheduleInfo;
		},
		doHighlightAppointmentsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.advising.store.viewAppts');
			if (store.data.length > 0) {
				var sData = store.data.all;
				//var user = mobEdu.util.getStore('mobEdu.enquire.login.store.login').data.first().raw.userInfo.userName;
				//get the meeting info
				for (var a = 0; a < sData.length; a++) {
					//if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
					var appDate = sData[a].data.appointmentDate;
					var appDateValue = appDate.split(' ');
					var dateFormatted = appDateValue[0];
					var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
					if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
						classes.push(highlightedItemCls);
					}
					//Incrementing startDate
					newStartDate = new Date(newStartDate.getTime() + 86400000);
				}
			}
			return classes
		},
		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},
		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.advising.f.dateChecking([currentDate.getMonth() + 1]) + '/' + mobEdu.advising.f.dateChecking(currentDate.getDate()) + '/' + currentDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.advising.f.dateChecking([presentDate.getMonth() + 1]) + '/' + mobEdu.advising.f.dateChecking(presentDate.getDate()) + '/' + presentDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.advising.f.setAppointmentScheduleInfo(date)
			}
		},

		showNewAppPopup: function(popupView) {
			mobEdu.util.get('mobEdu.advising.view.tablet.newApptPopup');
			Ext.getCmp('advAppFormPanel').reset();
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.advising.view.tablet.newApptPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		showNewMsgPopup: function(popupView) {
			mobEdu.util.get('mobEdu.advising.view.tablet.newMsgPopup');
			Ext.getCmp('msgFormPanel').reset();
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.advising.view.tablet.newMsgPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		appTap: function(me, index, target, record, e, eOpts) {
			if (e.target.getAttribute("name") === "accept") {
				var tap = e.target.getAttribute("name");
				mobEdu.advising.f.acceptAppointment(tap, record);
			} else if (e.target.getAttribute("name") === "decline") {
				var tap = e.target.getAttribute("name");
				mobEdu.advising.f.acceptAppointment(tap, record);
			}
		},

		acceptAppointment: function(value, record) {
			var acceptance = null;
			var role = mobEdu.util.getRole();
			var status;
			if (value == 'accept') {
				acceptance = 'Y';
				status = 'Scheduled';
			} else {
				acceptance = 'N';
				status = 'Cancelled';
			};
			var appId = record.data.appointmentID;
			var store = mobEdu.util.getStore('mobEdu.finaid.store.appAcceptance');
			store.getProxy().setUrl(commonwebserver + 'appointment/statusUpdate');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				appointmentID: appId,
				acceptance: acceptance,
				companyId: companyId,
				status: status
			});
			store.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.acceptanceResponseHandler();
			};
			store.load();
			if (value == 'accept') {
				Ext.Msg.show({
					id: 'aAccept',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Accepted.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.advising.f.loadViewAppointments();
						}
					}
				});
			}
			if (value == 'decline') {
				Ext.Msg.show({
					id: 'aDecline',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Declined.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.advising.f.loadViewAppointments();
						}
					}
				});
			}
		},
	}
});