Ext.define('mobEdu.advising.model.currInfo', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
            name:'major'
        },{
            name:'pgmDesc'
        },{
            name:'level'
        },{
            name:'department'
        },{
            name:'college'
        },{
            name:'admitTerm'
        },{
            name:'admitType'
        },{
            name:'catalogTerm'
        },{
            name:'concentration'
        },{
            name:'campus'
        }
        ]
    }
});
