Ext.define('mobEdu.advising.model.holds', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'studentId'
        }, {
            name: 'hold'
        }, {
            name: 'ind'
        }, {
            name: 'amount',
            convert: function(value, record) {
                if (value == '' || value == null) {
                    return ((parseFloat(0)).toFixed(2));
                }
                if (isNaN(value)) {
                    return ((parseFloat(0)).toFixed(2));
                } else {
                    value = parseFloat(value);
                    value = value.toFixed(2);
                    return value;
                }
            }
        }, {
            name: 'reason'
        }, {
            name: 'desc'
        }, {
            name: 'fromDate',
            convert: function(value, record) {
                if (value != '' && value != null) {
                    value = new Date(value.replace(' ', 'T'));
                    value = Ext.Date.format(value, 'M-d-Y');
                }
                return value;
            }
        }, {
            name: 'toDate',
            convert: function(value, record) {
                if (value != '' && value != null) {
                    value = new Date(value.replace(' ', 'T'));
                    value = Ext.Date.format(value, 'M-d-Y');
                }
                return value;
            }
        }, {
            name: 'orgOffice'
        }, {
            name: 'orgOfficeDesc'
        }, {
            name: 'procAffected'
        }, {
            name: 'currDate',
            convert: function(value, record) {
                if (value != '' && value != null) {
                    value = new Date(value.replace(' ', 'T'));
                    value = Ext.Date.format(value, 'M-d-Y');
                }
                return value;
            }
        }]
    }
});