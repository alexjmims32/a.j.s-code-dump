Ext.define('mobEdu.advising.model.students', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'title'
        }, {
            name: 'crn'
        }, {
            name: 'termCode'
        }, {
            name: 'lastName'
        }, {
            name: 'firstName'
        }, {
            name: 'mi'
        }, {
            name: 'birthDate',
            convert: function(value, record) {
                if (value != '' && value != null) {
                    value = new Date(value.replace(' ', 'T'));
                    value = Ext.Date.format(value, 'M d, Y');
                }
                return value;
            }
        }, {
            name: 'gender'
        }, {
            name: 'area'
        }, {
            name: 'phone',
            convert: function(value, record) {
                number = null;
                if (record.data.area != '' && record.data.area != null) {
                    number = record.data.area
                }
                if (number != null && value != null && value != '') {
                    value = number + '-' + value;
                } else if (number == null && value != null && value != '') {
                    value = value;
                } else {
                    value = number;
                }

                return value;
            }
        }, {
            name: 'email'
        }, {
            name: 'studentId'
        }, {
            name: 'name'
        }, {
            name: 'status'
        }, {
            name: 'image_url'
        }]
    }
});