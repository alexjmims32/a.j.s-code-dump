Ext.define('mobEdu.advising.model.newMessage', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'emailID',
            type: 'string'
        }, {
            name: 'status',
            type: 'string'
        }]
    }
});