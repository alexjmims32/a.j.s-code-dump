Ext.define('mobEdu.advising.model.courses', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
            name:'title'
        },{
            name:'crn'
        },{
            name:'termCode'
        },{
            name:'name',
            convert: function(value, record) { 
                return(record.get('title') + '(' + record.get('crn') + ')');
            }
        }
        ]
    }
});
