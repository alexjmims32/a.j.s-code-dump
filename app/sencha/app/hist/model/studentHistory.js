Ext.define('mobEdu.hist.model.studentHistory', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'title'
            },
            {
                name:'action',
                type: 'function'
            }
        ]
    }
});
