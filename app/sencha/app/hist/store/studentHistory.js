Ext.define('mobEdu.hist.store.studentHistory', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.hist.model.studentHistory'
    ],

    config:{
        storeId: 'mobEdu.hist.store.studentHistory',
        autoLoad: false,
        model: 'mobEdu.hist.model.studentHistory',
        proxy:{
            type:'localstorage',
            id:'studenthistory'
        }
    }
});