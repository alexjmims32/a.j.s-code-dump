Ext.define('mobEdu.hist.f', {
    statics:{

        initializeHistoryView:function () {
            var store = mobEdu.util.getStore('mobEdu.hist.store.studentHistory');
            store.load();
            store.removed = [];
            store.getProxy().clear();
            store.sync();

            store.add({
                title:'Student Academic Transcript'
//                action:
            });
            store.sync();

            store.add({
                title:'Request Student Transcript'
//                action:
            });
            store.sync();

            store.add({
                title:'Request Enrollment Verification'
//                action:
            });
            store.sync();

            store.add({
                title:'Financial Aid Scholarship'
//                action:
            });
            store.sync();

            store.add({
                title:'Student Billing Statements'
//                action:
            });
            store.sync();

            store.add({
                title:'Graduation Information'
//            action:
            });
            store.sync();

            store.load();

        },
        
        showStudentHistoryView:function(){
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.hist.view.studentHistory');
            mobEdu.util.setTitle('HISTORY',Ext.getCmp('studentHTitle'));
        }
    }
});
