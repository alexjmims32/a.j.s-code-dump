Ext.define('mobEdu.hist.view.studentHistory', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.hist.store.studentHistory'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'modules',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.hist.store.studentHistory'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'studentHTitle'
        }],
        flex: 1
    },
    initialize: function() {
        mobEdu.hist.f.initializeHistoryView();
    }
});