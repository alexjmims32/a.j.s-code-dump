Ext.define('mobEdu.counselor.store.search', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.counselor.model.search'
    ],

    config: {
        storeId: 'mobEdu.counselor.store.search',
        autoLoad: false,
        model: 'mobEdu.counselor.model.search'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'searchResult'
        return proxy;
    }

});