Ext.define('mobEdu.counselor.store.advSearch', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.counselor.model.search'
    ],

    config: {
        storeId: 'mobEdu.counselor.store.advSearch',
        autoLoad: false,
        model: 'mobEdu.counselor.model.search',
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'ROWSET',
                record: 'ROW'
            }
        }
    },


});