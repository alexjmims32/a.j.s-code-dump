Ext.define('mobEdu.counselor.store.details', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.counselor.model.search'
    ],
    
    config:{
        storeId: 'mobEdu.counselor.store.details',
        autoLoad: false,
        model: 'mobEdu.counselor.model.search'
    },
    initProxy: function() {
        var proxy = this.callParent();
        // proxy.reader.rootProperty= 'details'
        return proxy;
    }
    
});