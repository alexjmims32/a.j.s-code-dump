Ext.define('mobEdu.counselor.store.cnslrDetails', {
    extend: 'mobEdu.data.store',

    requires: ['mobEdu.counselor.model.cnslrDetails'],
    config: {
        storeId: 'mobEdu.counselor.store.cnslrDetails',
        autoLoad: false,
        model: 'mobEdu.counselor.model.cnslrDetails',
        grouper: {
            groupFn: function(record) {
                return 'FA code ' + record.get('year');
            }
        },
        initProxy: function() {
            var proxy = this.callParent();
            proxy.reader.rootProperty = '';
            return proxy;
        }
    }
});