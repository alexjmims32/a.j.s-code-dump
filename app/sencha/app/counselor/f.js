Ext.define('mobEdu.counselor.f', {
    requires: [
        'mobEdu.counselor.store.search',
        'mobEdu.counselor.store.details',
        'mobEdu.counselor.store.advSearch',
        'mobEdu.counselor.store.cnslrDetails'
    ],
    statics: {
        studentId: null,
        role: null,
        desc: null,
        flag: false,
        aidYear: '',
        awardsTot: null,
        loadCodes: function() {
            mobEdu.finaid.f.loadFinaidDropdown(null, mobEdu.counselor.f.showSearch);
            mobEdu.util.get('mobEdu.counselor.view.search');
            var store = mobEdu.util.getStore('mobEdu.counselor.store.search');
            if (store.data.length > 0) {
                store.removeAll();
            }
            Ext.getCmp('counStudentId').setValue('');
            Ext.getCmp('counfirstName').setValue('');
            Ext.getCmp('counLastName').setValue('');
        },

        onSearchBtnTap: function() {
            var code = Ext.getCmp('finaidCounselorCodes').getValue();
            var studentId = Ext.getCmp('counStudentId').getValue();
            if (studentId == null) {
                studentId = '';
            }
            var firstName = Ext.getCmp('counfirstName').getValue();
            if (firstName == null) {
                firstName = '';
            }
            var lastName = Ext.getCmp('counLastName').getValue();
            if (lastName == null) {
                lastName = '';
            }
            if ((code == null || code == '') || ((studentId == null || studentId == '') && (firstName == null || firstName == '') && (lastName == null || lastName == ''))) {
                Ext.Msg.show({
                    title: null,
                    cls: 'msgbox',
                    message: '<p>Please provide info to search.</p>',
                    buttons: Ext.MessageBox.OK,
                });
            } else {
                mobEdu.finaid.f.aidyCode = code;
                mobEdu.counselor.f.loadSearch(code, studentId, firstName, lastName);
            }
        },

        loadSearch: function(code, studentId, firstName, lastName) {
            var store = mobEdu.util.getStore('mobEdu.counselor.store.search');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'finAidSearch');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                reportId: studentId,
                firstName: firstName,
                lastName: lastName,
                code: code,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.counselor.f.searchResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('counselor', 'search');
        },

        searchResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.counselor.store.search');
            if (store.getCount() === 0) {
                Ext.getCmp('counselorList').setEmptyText('No data');
            } else {
                Ext.getCmp('counselorList').setEmptyText(null);
            }
        },

        loadDetails: function(view, index, target, record, item, e, eOpts) {
            mobEdu.counselor.f.aidYear = record.get('desc');
            mobEdu.util.deselectSelectedItem(index, view);
            var code = Ext.getCmp('finaidCounselorCodes').getValue();
            var studentId = record.get('studentId');
            if (studentId == null || studentId == undefined) {
                studentId = record.get('sid');
            }
            mobEdu.finaid.f.studentId = studentId;
            var store = mobEdu.util.getStore('mobEdu.counselor.store.details');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'finAidSearchDetail');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                reportId: studentId,
                code: code,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.counselor.f.detailsResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('counselor', 'searchDetails');
        },
        loadAdvDetails: function(view, index, target, record, item, e, eOpts) {
            mobEdu.util.deselectSelectedItem(index, view);
            if (Ext.os.is.phone) {
                var code = Ext.getCmp('finaidCounselorAdvCodesP').getValue();
            } else {
                var code = Ext.getCmp('finaidCounselorAdvCodes').getValue();
            }
            var studentId = record.get('studentId');
            if (studentId == null || studentId == undefined) {
                studentId = record.get('sid');
            }
            mobEdu.finaid.f.studentId = studentId;
            var store = mobEdu.util.getStore('mobEdu.counselor.store.details');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'finAidSearchDetail');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                reportId: studentId,
                code: code,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.counselor.f.detailsResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('counselor', 'searchDetails');
        },

        detailsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.counselor.store.details');
            if (store.data.length > 0) {
                var cmp = '';

                var record = store.getAt(0);
                if (Ext.os.is.Phone) {
                    mobEdu.util.get('mobEdu.counselor.view.details');
                    cmp = Ext.getCmp('counDetailsP');
                } else {
                    mobEdu.util.get('mobEdu.finaid.view.tablet.menu');
                    cmp = Ext.getCmp('counDetailsT');
                }
                mobEdu.counselor.f.aidYear = record.get('desc');
                cmp.setHtml('<table width="100%"><tr><td><h4><b>' + record.get('studentId') + '</b></h4></td><td colspan="2"><h4><b>' + record.get('name') + '</b></h4></td></tr>' +
                    '<tr><td colspan="2"><h4 align="left">' + record.get('email') + '</h4></td><td><h4 align="right">' + record.get('phone') + '</h4></td></tr>' +
                    '<tr><td><h4><b>Level : </b></h4></td><td colspan="2"><h4>' + record.get('levelDesc') + '</h4></td></tr>' +
                    '<tr><td><h4><b>Residence : </b></h4></td><td colspan="2"><h4>' + record.get('residence') + '</h4></td></tr>' +
                    '<tr><td><h4><b>Academic Progress : </b></h4></td><td colspan="2"><h4>' + record.get('status') + '</h4></td></tr>' +
                    '<tr><td><h4><b>Tracking Info : </b></h4></td><td colspan="2"><h4>' + record.get('trackingGroup') + '</h4></td></tr></table>' +
                    '<br /><br />' +
                    '<table width="100%">' +
                    '<tr><tdcolspan="5"><h4><b>Financial Need</b></h4></td></tr>' +
                    '<tr><td><h4>Offered: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('offeredAmt')) + '</h4></td><td>&nbsp;</td><td><h4>Resource: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('otherResource')) + '</h4></td></tr>' +
                    '<tr><td><h4>Budget: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('budgetAmt')) + '</h4></td><td>&nbsp;</td><td><h4>EFC: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('efc')) + '</h4></td></tr>' +
                    '<tr><td><h4>Gross Need: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('grossNeed')) + '</h4></td><td>&nbsp;</td><td><h4>Unmet Need: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('unmetNeed')) + '</h4></td></tr>' +
                    '<tr><td><h4>Pell Budget: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('pellBudget')) + '</h4></td><td>&nbsp;</td><td><h4>Pell EFC: </h4></td><td align="right"><h4>' + '$' + mobEdu.util.formatCurrency(record.get('pellEfc')) + '</h4></td></tr></table>');

                mobEdu.counselor.f.loadList(record);
            }
        },

        loadList: function(record) {
            mobEdu.counselor.f.studentId = record.get('studentId');

            var localStore = mobEdu.util.getStore('mobEdu.finaid.store.statusLocal');
            if (localStore.data.length > 0) {
                localStore.removeAll();
                localStore.sync();
                localStore.removed = [];
            }

            value = record.get('budgetAmt')
            value = parseFloat(value);
            value = value.toFixed(2);

            localStore.add({
                description: 'Cost of Attendance <br> <h6>' + record.get('budgetGroup') + '<br><b>(Budget Group)</b></h6>',
                amount: '$' + mobEdu.util.formatCurrency(value),
                next: 1,
                action: mobEdu.counselor.f.loadCostAttendance
            });


            value = record.get('awardAmt')
            value = parseFloat(value);
            value = value.toFixed(2);
            mobEdu.counselor.f.awardsTot = '$' + mobEdu.util.formatCurrency(value);


            localStore.add({
                description: 'Award',
                amount: '$' + mobEdu.util.formatCurrency(value),
                next: 1,
                action: mobEdu.counselor.f.loadAwards
            });

            localStore.add({
                description: 'Resources',
                next: 1,
                action: mobEdu.counselor.f.loadResources
            });


            // localStore.add({
            //     description: 'Academic Progress <br> <h5>' + record.get('status') + '</h5>',
            //     amount: '',
            //     next: 0
            // });

            if (record.get('requirements') == '0/0' || record.get('requirements') == '/') {
                localStore.add({
                    description: 'Requirements',
                    next: 0
                });
            } else {
                localStore.add({
                    description: 'Requirements',
                    amount: record.get('requirements'),
                    next: 1,
                    action: mobEdu.counselor.f.loadRequirements
                });
            }

            if (mobEdu.counselor.f.role == 'counselor') {
                mobEdu.counselor.f.desc = 'Student has no holds';
            } else {
                mobEdu.counselor.f.desc = 'You Have No Holds';
            }
            var amtt = record.get('holds')
            amtt = parseFloat(amtt);
            amtt = amtt.toFixed(2);

            if (amtt > 0) {
                localStore.add({
                    description: 'You Have Holds',
                    amount: record.get('holds'),
                    next: 1,
                    action: mobEdu.counselor.f.loadHolds
                });
            } else {
                localStore.add({
                    description: mobEdu.counselor.f.desc,
                    amount: '',
                    next: 0
                });
            }

            localStore.add({
                description: 'Appointments',
                next: 1,
                action: mobEdu.counselor.f.loadAppointments
            });

            localStore.add({
                description: 'Messages',
                next: 1,
                action: mobEdu.counselor.f.loadMessages
            });
            // localStore.add({
            //     description: 'Tracking Group <br> <h5>' + record.get('trackingGroup') + '</h5>',
            //     amount: '',
            //     next: 0
            // });

            localStore.sync();
            mobEdu.counselor.f.showDetails();
            if (!Ext.os.is.Phone) {
                Ext.getCmp('finBudgetGrp').setHtml('<h4 align="center">' + record.get('budgetGroup') + '<br><b>(Budget Group)</b></h4>');
                Ext.getCmp('finAwardsTot').setHtml('<br /><h4 align="center"><b>Total - ' + mobEdu.counselor.f.awardsTot + '</b></h4><br />');
            }
        },

        loadAppointments: function() {
            mobEdu.finaid.f.loadAppointments(mobEdu.counselor.f.showDetails);
        },

        loadMessages: function() {
            mobEdu.finaid.f.loadMessages(mobEdu.counselor.f.showDetails);
        },

        loadHolds: function() {
            mobEdu.finaid.f.loadHolds(mobEdu.counselor.f.showDetails);
        },

        loadCostAttendance: function() {
            mobEdu.finaid.f.loadCostAttendance(mobEdu.counselor.f.showDetails);
        },

        loadAwards: function() {
            mobEdu.finaid.f.loadAwards(mobEdu.counselor.f.showDetails);
        },

        loadResources: function() {
            mobEdu.finaid.f.loadResources(mobEdu.counselor.f.showDetails);
        },

        loadRequirements: function() {
            mobEdu.finaid.f.loadRequirements(mobEdu.counselor.f.showDetails);
        },

        showSearch: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.counselor.f.flag = false;
            mobEdu.util.show('mobEdu.counselor.view.search');
            Ext.getCmp('finaidCounselorCodes').setValue(0);
            /*Ext.getCmp('counStudentId').setValue('');
            Ext.getCmp('counfirstName').setValue('');
            Ext.getCmp('counLastName').setValue('');*/
        },

        showDetails: function() {
            if (mobEdu.counselor.f.flag == true) {
                if ((Ext.os.is.phone)) {
                    mobEdu.util.updatePrevView(mobEdu.counselor.f.showAdvSearchList);
                } else {
                    mobEdu.util.updatePrevView(mobEdu.counselor.f.showAdvSearchList);
                }
            } else {
                mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
            }
            if (Ext.os.is.Phone) {
                mobEdu.util.show('mobEdu.counselor.view.details');
                Ext.getCmp('counselorDetailsP').setTitle('<h1>' + mobEdu.counselor.f.aidYear + '</h1>');
            } else {
                var viewRef = mobEdu.util.get('mobEdu.finaid.view.tablet.menu');
                Ext.getCmp('counToolbar').show();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(0).show();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(6).hide();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(7).hide();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(8).hide();
                Ext.getCmp('finaidTabs').setActiveItem(0);
                Ext.getCmp('finaidCodesT').setStyle('display:none;');
                Ext.getCmp('finaidTitleT').setTitle('<h1>' + mobEdu.counselor.f.aidYear + '</h1>');
                mobEdu.util.show('mobEdu.finaid.view.tablet.menu');
            }
        },

        onCodeChange: function(code) {
            mobEdu.util.get('mobEdu.counselor.view.details');
            Ext.getCmp('counfirstName').setValue('');
            Ext.getCmp('counLastName').setValue('');
            var store = mobEdu.util.getStore('mobEdu.counselor.store.search');
            store.removeAll();
            store.removed = [];
        },
        onAdvCodeChange: function() {
            if ((Ext.os.is.phone)) {
                mobEdu.util.get('mobEdu.counselor.view.phone.advSearch');
                Ext.getCmp('counAdvStudentIdP').setValue('');
                Ext.getCmp('counAdvfirstNameP').setValue('');
                Ext.getCmp('counAdvLastNameP').setValue('');
                Ext.getCmp('unmetNeedP').setChecked(false);
                Ext.getCmp('outstandingRequirementsP').setChecked(false);
                Ext.getCmp('studentsHoldsP').setChecked(false);
                var store = mobEdu.util.getStore('mobEdu.counselor.store.advSearch');
                store.removeAll();
                store.removed = [];
            } else {
                mobEdu.util.get('mobEdu.counselor.view.advSearch');
                Ext.getCmp('counAdvStudentId').setValue('');
                Ext.getCmp('counAdvfirstName').setValue('');
                Ext.getCmp('counAdvLastName').setValue('');
                Ext.getCmp('unmetNeed').setChecked(false);
                Ext.getCmp('outstandingRequirements').setChecked(false);
                Ext.getCmp('studentsHolds').setChecked(false);
                Ext.getCmp('counselorAdvList').setEmptyText('');
                Ext.getCmp('counselorAdvList').refresh();
                var store = mobEdu.util.getStore('mobEdu.counselor.store.advSearch');
                store.removeAll();
                store.removed = [];
            }
        },
        advSearch: function() {
            if (Ext.os.is.phone) {
                mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
                var store = mobEdu.util.getStore('mobEdu.counselor.store.search');
                store.removeAll()
                store.sync();
                var store1 = mobEdu.util.getStore('mobEdu.counselor.store.advSearch');
                store1.removeAll()
                store1.sync();
                Ext.getCmp('counStudentId').setValue('');
                Ext.getCmp('counfirstName').setValue('');
                Ext.getCmp('counLastName').setValue('');
                mobEdu.counselor.f.showAdvSearch();
                Ext.getCmp('finaidCounselorAdvCodesP').reset();
                Ext.getCmp('counAdvStudentIdP').setValue('');
                Ext.getCmp('counAdvStudentIdP').focus(true);
                Ext.getCmp('counAdvfirstNameP').setValue('');
                Ext.getCmp('counAdvLastNameP').setValue('');
                Ext.getCmp('unmetNeedP').uncheck();
                Ext.getCmp('outstandingRequirementsP').uncheck();
                Ext.getCmp('studentsHoldsP').uncheck();

            } else {
                mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
                var store = mobEdu.util.getStore('mobEdu.counselor.store.advSearch');
                store.removeAll()
                store.sync();
                var store1 = mobEdu.util.getStore('mobEdu.counselor.store.search');
                store1.removeAll()
                store1.sync();
                Ext.getCmp('counStudentId').setValue('');
                Ext.getCmp('counfirstName').setValue('');
                Ext.getCmp('counLastName').setValue('');
                mobEdu.counselor.f.showTabAdvSearch();
                Ext.getCmp('finaidCounselorAdvCodes').reset();
                Ext.getCmp('counform').reset();
                Ext.getCmp('unmetNeed').uncheck();
                Ext.getCmp('outstandingRequirements').uncheck();
                Ext.getCmp('studentsHolds').uncheck();
            }
        },
        showTabAdvSearch: function() {
            mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
            Ext.getCmp('finaidCounselorCodes').reset();
            mobEdu.util.get('mobEdu.counselor.view.advSearch');
            Ext.getCmp('counform').reset();
            mobEdu.util.show('mobEdu.counselor.view.advSearch');
        },
        showAdvSearch: function() {
            mobEdu.util.updatePrevView(mobEdu.counselor.f.showSearch);
            mobEdu.util.show('mobEdu.counselor.view.phone.advSearch');

        },
        showAdvSearchList: function() {
            mobEdu.util.updatePrevView(mobEdu.counselor.f.showAdvSearch);
            mobEdu.util.show('mobEdu.counselor.view.phone.advSearchList');
        },
        onAdvSearchBtnTap: function() {
            if (Ext.os.is.phone) {
                mobEdu.counselor.f.flag = true;
                var code = Ext.getCmp('finaidCounselorAdvCodesP').getValue();
                var studentId = Ext.getCmp('counAdvStudentIdP').getValue();
                if (studentId == null) {
                    studentId = '';
                }
                var firstName = Ext.getCmp('counAdvfirstNameP').getValue();
                if (firstName == null) {
                    firstName = '';
                }
                var lastName = Ext.getCmp('counAdvLastNameP').getValue();
                if (lastName == null) {
                    lastName = '';
                }
                var unmetneed = Ext.getCmp('unmetNeedP').getChecked();
                if (unmetneed == true) {
                    unmetneed = 'Y';
                } else {
                    unmetneed = '';
                }
                var outreq = Ext.getCmp('outstandingRequirementsP').getChecked();
                if (outreq == true) {
                    outreq = 'Y';
                } else {
                    outreq = '';
                }
                var stuholds = Ext.getCmp('studentsHoldsP').getChecked();
                if (stuholds == true) {
                    stuholds = 'Y';
                } else {
                    stuholds = '';
                }
            } else {
                mobEdu.counselor.f.flag = true;
                var code = Ext.getCmp('finaidCounselorAdvCodes').getValue();
                var studentId = Ext.getCmp('counAdvStudentId').getValue();
                if (studentId == null) {
                    studentId = '';
                }
                var firstName = Ext.getCmp('counAdvfirstName').getValue();
                if (firstName == null) {
                    firstName = '';
                }
                var lastName = Ext.getCmp('counAdvLastName').getValue();
                if (lastName == null) {
                    lastName = '';
                }
                var unmetneed = Ext.getCmp('unmetNeed').getChecked();
                if (unmetneed == true) {
                    unmetneed = 'Y';
                } else {
                    unmetneed = '';
                }
                var outreq = Ext.getCmp('outstandingRequirements').getChecked();
                if (outreq == true) {
                    outreq = 'Y';
                } else {
                    outreq = '';
                }
                var stuholds = Ext.getCmp('studentsHolds').getChecked();
                if (stuholds == true) {
                    stuholds = 'Y';
                } else {
                    stuholds = '';
                }
            }
            if ((code == null || code == '') || ((studentId == null || studentId == '') && (firstName == null || firstName == '') && (lastName == null || lastName == '') && (stuholds == null || stuholds == '') && (outreq == null || outreq == '') && (unmetneed == null || unmetneed == ''))) {
                Ext.Msg.show({
                    title: null,
                    cls: 'msgbox',
                    message: '<p>Please provide info to search.</p>',
                    buttons: Ext.MessageBox.OK,
                });
            } else {
                mobEdu.finaid.f.aidyCode = code;
                mobEdu.counselor.f.loadAdvSearch(code, studentId, firstName, lastName, unmetneed, outreq, stuholds);
                mobEdu.counselor.f.showAdvSearchList();
            }

        },
        loadAdvSearch: function(code, studentId, firstName, lastName, unmetneed, outreq, stuholds) {
            var store = mobEdu.util.getStore('mobEdu.counselor.store.advSearch');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'finAdvSearch');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                reportId: studentId,
                firstName: firstName,
                lastName: lastName,
                code: code,
                unmetneed: unmetneed,
                outreq: outreq,
                stuholds: stuholds,
                companyId: companyId

            });
            store.load();
            mobEdu.util.gaTrackEvent('counselor', 'advSearch');
            store.getProxy().afterRequest = function() {
                mobEdu.counselor.f.advSearchResponseHandler();
            };
        },
        advSearchResponseHandler: function() {
            if (!Ext.os.is.phone) {
                mobEdu.util.updatePrevView(mobEdu.counselor.f.showTabAdvSearch);
                var store = mobEdu.util.getStore('mobEdu.counselor.store.advSearch');
                if (store.getCount() === 0) {
                    Ext.getCmp('counselorAdvList').setEmptyText('No data');
                } else {
                    Ext.getCmp('counselorAdvList').setEmptyText(null);
                }
            }
        },

        loadCounselorDetails: function() {
            var store = mobEdu.util.getStore('mobEdu.counselor.store.cnslrDetails');
            store.getProxy().setUrl(webserver + 'getFinCounselorDetails');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId()
            });
            store.load();
            mobEdu.util.gaTrackEvent('counselor', 'getFinCounselorDetails');
            store.getProxy().afterRequest = function() {
                mobEdu.counselor.f.cnslrDetailsResponseHandler();
            };
        },

        cnslrDetailsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.counselor.store.cnslrDetails');
            if (store.data.length > 0) {
                mobEdu.counselor.f.showFACnslrDetails();
            } else {
                Ext.Msg.show({
                    title: null,
                    cls: 'msgbox',
                    message: '<p>No Information Available.</p>',
                    buttons: Ext.MessageBox.OK
                });
            }
        },

        showFACnslrDetails: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.counselor.view.cnslrDetails');
        }
    }
});