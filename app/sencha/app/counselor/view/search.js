Ext.define('mobEdu.counselor.view.search', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'counselorTitle',
            title: '<h1>Financial Aid</h1>'
        }, {
            xtype: 'selectfield',
            docked: 'top',
            name: 'finaidCounselorCodes',
            valueField: 'code',
            displayField: 'description',
            id: 'finaidCounselorCodes',
            store: mobEdu.util.getStore('mobEdu.finaid.store.menu'),
            listeners: {
                change: function(selectbox, newValue, oldValue) {
                    mobEdu.counselor.f.onCodeChange(newValue);
                }
            }
        }, {
            xtype: 'fieldset',
            docked: 'top',
            items: [{
                xtype: 'textfield',
                id: 'counStudentId',
                cls: 'searchText',
                name: 'counStudentId',
                placeHolder: 'Enter StudentId',
                clearIcon: true
            }, {
                xtype: 'textfield',
                id: 'counfirstName',
                cls: 'searchText',
                name: 'counfirstName',
                placeHolder: 'Enter First Name',
                clearIcon: true
            }, {
                xtype: 'textfield',
                id: 'counLastName',
                cls: 'searchText',
                name: 'counLastName',
                placeHolder: 'Enter Last Name',
                clearIcon: true
            }]
        }, {
            xtype: 'list',
            id: 'counselorList',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr><td><h3 class="subnavText">{studentId}</h3></td><td align="right" rowspan="2" width="10%"><div align="right" class="arrow" /></td></tr><tr><td ><h4 >{name}</h4></td></tr></table>'),
            store: mobEdu.util.getStore('mobEdu.counselor.store.search'),
            singleSelect: true,
            emptyText: 'No data found',
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.counselor.f.loadDetails(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Search',
                ui: 'button',
                handler: function() {
                    mobEdu.counselor.f.onSearchBtnTap();
                }
            }, {
                text: 'AdvSearch',
                ui: 'button',
                handler: function() {
                    mobEdu.counselor.f.advSearch();
                }
            }]
        }],
        flex: 1
    }
});