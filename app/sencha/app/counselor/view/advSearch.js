Ext.define('mobEdu.counselor.view.advSearch', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'advcounselorTitle',
            title: '<h1>Financial Aid</h1>'
        }, {
            xtype: 'selectfield',
            docked: 'top',
            name: 'finaidCounselorAdvCodes',
            valueField: 'code',
            displayField: 'description',
            id: 'finaidCounselorAdvCodes',
            store: mobEdu.util.getStore('mobEdu.finaid.store.menu'),
            listeners: {
                change: function(selectbox, newValue, oldValue) {
                    mobEdu.counselor.f.onAdvCodeChange(newValue);
                }
            }
        }, {
            xtype: 'formpanel',
            id: 'counform',
            items: [{
                xtype: 'textfield',
                id: 'counAdvStudentId',
                name: 'counAdvStudentId',
                placeHolder: 'Enter StudentId',
                clearIcon: true
            }, {
                xtype: 'textfield',
                id: 'counAdvfirstName',
                name: 'counAdvfirstName',
                placeHolder: 'Enter First Name',
                clearIcon: true
            }, {
                xtype: 'textfield',
                id: 'counAdvLastName',
                name: 'counAdvLastName',
                placeHolder: 'Enter Last Name',
                clearIcon: true
            }, {
                xtype: 'checkboxfield',
                labelWidth: '80%',
                label: 'Students with Unmet Need',
                name: 'unmetNeed',
                id: 'unmetNeed'
            }, {
                xtype: 'checkboxfield',
                label: 'Students with Outstanding Requirements',
                labelWidth: '80%',
                name: 'outstandingRequirements',
                id: 'outstandingRequirements'
            }, {
                xtype: 'checkboxfield',
                labelWidth: '80%',
                label: 'Students with Holds',
                name: 'studentsHolds',
                id: 'studentsHolds'
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Search',
                ui: 'button',
                handler: function() {
                    mobEdu.counselor.f.onAdvSearchBtnTap();
                }
            }]
        }],
        flex: 1
    }
});