Ext.define('mobEdu.counselor.view.phone.advSearchList', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'advRcounselorTitle',
            title: '<h1>Financial Aid</h1>'
        }, {
            xtype: 'list',
            id: 'counselorAdvList',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr><td><h3 class="subnavText">{sid}</h3></td><td align="right" rowspan="2" width="10%"><div align="right" class="arrow" /></td></tr><tr><td ><h4 >{fullName}</h4></td></tr></table>'),
            store: mobEdu.util.getStore('mobEdu.counselor.store.advSearch'),
            singleSelect: true,
            emptyText: 'No data found',
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.counselor.f.loadAdvDetails(view, index, target, record, item, e, eOpts);
                }
            }
        }],
        flex: 1
    }
});