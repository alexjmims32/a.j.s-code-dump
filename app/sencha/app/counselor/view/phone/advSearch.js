Ext.define('mobEdu.counselor.view.phone.advSearch', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'advpcounselorTitleP',
            title: '<h1>Financial Aid</h1>'
        }, {
            xtype: 'selectfield',
            docked: 'top',
            name: 'finaidCounselorAdvCodes',
            valueField: 'code',
            displayField: 'description',
            id: 'finaidCounselorAdvCodesP',
            store: mobEdu.util.getStore('mobEdu.finaid.store.menu'),
            listeners: {
                change: function(selectbox, newValue, oldValue) {
                    mobEdu.counselor.f.onAdvCodeChange(newValue);
                }
            }
        }, {
            xtype: 'formpanel',
            items: [{
                xtype: 'textfield',
                id: 'counAdvStudentIdP',
                name: 'counAdvStudentId',
                placeHolder: 'Enter StudentId',
                clearIcon: true
            }, {
                xtype: 'textfield',
                id: 'counAdvfirstNameP',
                name: 'counAdvfirstName',
                placeHolder: 'Enter First Name',
                clearIcon: true
            }, {
                xtype: 'textfield',
                id: 'counAdvLastNameP',
                name: 'counAdvLastName',
                placeHolder: 'Enter Last Name',
                clearIcon: true
            }, {
                xtype: 'checkboxfield',
                labelWidth: '70%',
                label: 'Students with Unmet Need',
                name: 'unmetNeed',
                id: 'unmetNeedP'
            }, {
                xtype: 'checkboxfield',
                label: 'Students with Outstanding<br/> Requirements',
                labelWidth: '70%',
                name: 'outstandingRequirements',
                id: 'outstandingRequirementsP'
            }, {
                xtype: 'checkboxfield',
                labelWidth: '70%',
                label: 'Students with Holds',
                name: 'studentsHolds',
                id: 'studentsHoldsP'
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Search',
                ui: 'button',
                handler: function() {
                    mobEdu.counselor.f.onAdvSearchBtnTap();
                }
            }]
        }],
        flex: 1
    }
});