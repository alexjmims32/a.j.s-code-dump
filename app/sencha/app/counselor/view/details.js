Ext.define('mobEdu.counselor.view.details', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'counselorDetailsP',
            name: 'counselorDetailsP',
            title: '<h1>Financial Aid</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Send Message',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.showConNewMsg();
                }
            }, {
                text: 'Setup Appointment',
                ui: 'button',
                handler: function() {
                    mobEdu.advising.f.showCounNewAppt();
                }
            }, {
                text: 'Calendar',
                ui: 'button',
                handler: function() {
                    mobEdu.finaid.f.showCounCalendar();
                }
            }]
        }, {
            xtype: 'tabpanel',
            id: 'counselorTabs',
            name: 'counselorTabs',
            tabBar: {
                layout: {
                    pack: 'center'
                }
            },
            defaults: {
                style: 'border:none;background:none;'
            },
            items: [{
                layout: 'fit',
                title: '<h3>Details</h3>',
                width: '100',
                items: [{
                    id: 'counDetailsP',
                    scrollable: true,
                    name: 'counDetailsP',
                    html: ''
                }]
            }, {
                layout: 'fit',
                title: '<h3>Menu</h3>',
                scrollable: false,
                width: '100',
                items: [{
                    xtype: 'list',
                    id: 'counselorDetails',
                    itemTpl: new Ext.XTemplate('<table width="100%"><tr><td width="80%" align="left" ><h3 class="subnavText">{description}</h3></td><td width="20%" align="right" ><h4 class="listDate">{amount}</h4></td><tpl if="next==1"><td width="10%"><div align="right" class="arrow" /></td></tpl></tr></table>'),
                    store: mobEdu.util.getStore('mobEdu.finaid.store.statusLocal'),
                    singleSelect: true,
                    scrollToTopOnRefresh: false,
                    loadingText: '',
                    listeners: {
                        itemtap: function(view, index, item, e) {
                            setTimeout(function() {
                                view.deselect(index);
                            }, 500);
                            if (e.data.action != undefined || e.data.action != null) {
                                e.data.action();
                            }

                        }
                    }
                }]
            }]
        }],
        flex: 1
    }
});