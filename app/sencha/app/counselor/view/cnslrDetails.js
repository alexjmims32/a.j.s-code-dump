Ext.define('mobEdu.counselor.view.cnslrDetails', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.counselor.store.cnslrDetails'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            disableSelection: true,
            itemTpl: '<div style="padding:10px;"><div><b><h2>FA Counsler Details:</h2></b></div><br><div><p>{name}</p><div><p>{email}</p></div><div><p>{phone}</p></div>',
            store: mobEdu.util.getStore('mobEdu.counselor.store.cnslrDetails'),
            emptyText: '<center><h3>No Info Available</h3><center>',
            loadingText: '',
            grouped: true
        }, {
            xtype: 'customToolbar',
            title: '<h1>FA Counselor</h1>'
        }],
        flex: 1
    }
});