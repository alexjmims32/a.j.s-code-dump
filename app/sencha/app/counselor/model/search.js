Ext.define('mobEdu.counselor.model.search', {
  extend: 'Ext.data.Model',

  config: {
    fields: [{
      name: 'studentId'
    }, {
      name: 'name'
    }, {
      name: 'code'
    }, {
      name: 'desc',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'email',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'phone',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'levelDesc',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'residence',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'status',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'budgetGroup',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'trackingGroup',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'holds'
    }, {
      name: 'budgetAmt',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'awardAmt',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'offeredAmt',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'otherResource',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'fnBudgetAmt',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'efc',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'grossNeed',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'unmetNeed',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'pellBudget',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'pellEfc',
      convert: function(value, record) {
        if (value == '' || value == null) {
          return ((parseFloat(0)).toFixed(2));
        }
        if (isNaN(value)) {
          return ((parseFloat(0)).toFixed(2));
        } else {
          value = parseFloat(value);
          value = value.toFixed(2);
          return value;
        }
      }
    }, {
      name: 'requirements'
    }, {
      name: 'sid',
      mapping: 'ID'
    }, {
      name: 'fullName',
      mapping: 'FULL_NAME'
    }]
  }
});