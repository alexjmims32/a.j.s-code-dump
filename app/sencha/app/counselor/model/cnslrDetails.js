Ext.define('mobEdu.counselor.model.cnslrDetails', {
  extend: 'Ext.data.Model',

  config: {
    fields: [{
      name: 'name'
    }, {
      name: 'year'
    }, {
      name: 'email',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }, {
      name: 'phone',
      convert: function(value, record) {
        if (value == '' || value == null) {
          value = '';
        }
        return value;
      }
    }]
  }
});