Ext.define('mobEdu.storeUtil', {

	statics: {
		clearAll: function() {
			mobEdu.storeUtil.clearEnroll();
			mobEdu.storeUtil.clearEnroute();
			// Below are stores related to enroute stack
			// Not all clients will use these
			// So added them to a try block so that we dont have any issues clearing them

		},

		clearEnroll: function() {
			mobEdu.storeUtil.clearPendingApprovalsStore();
			mobEdu.storeUtil.clearAccLocalSummaryStore();
			mobEdu.storeUtil.clearAccPaymentCardStore();
			mobEdu.storeUtil.clearCourseListLocalStore();
			mobEdu.storeUtil.clearCoursesHolidaysLocalStore();
			mobEdu.storeUtil.clearCoursesMeetingScheduleLocalStore();
			mobEdu.storeUtil.clearMainEntourageStore();
			mobEdu.storeUtil.clearNotifLocalStore();
			mobEdu.storeUtil.clearNotifMailStore();
			mobEdu.storeUtil.clearStudentProfileStore();
			mobEdu.storeUtil.clearRegCartStore();
			mobEdu.storeUtil.clearRegCourseDetailLocalStore();
			mobEdu.storeUtil.clearRegLocalCartStore();
			mobEdu.storeUtil.clearRegMeetingScheduleLocalStore();
			mobEdu.storeUtil.clearRegPinStore();
			mobEdu.storeUtil.clearAttdMenuStore();
			mobEdu.storeUtil.clearAttendenceClassDetailsStore();
			mobEdu.storeUtil.clearAttendenceMeetingScheduleStore();
			mobEdu.storeUtil.clearAttendenceStudentDetailsStore();
			mobEdu.storeUtil.clearAttendenceTermListLocalStore();
			mobEdu.storeUtil.clearFinaidAwardLocalStore();
			mobEdu.storeUtil.clearFinaidLoanHistoryLocalStore();
			mobEdu.storeUtil.clearFinaidStatusLocalStore();
			mobEdu.storeUtil.clearMainSlidingMenuStore();
			mobEdu.storeUtil.clearPeopleMenuStore();


			//Work around for role based authentincation.

		},
		clearEnroute: function() {
			mobEdu.storeUtil.clearEnradmsLeadAppointmentDetailStore();
			mobEdu.storeUtil.clearEnradmsLeadAppointmentsStore();
			mobEdu.storeUtil.clearEnradmsLeadDocListStore();
			mobEdu.storeUtil.clearEnradmsLeadEmailStore();
			mobEdu.storeUtil.clearEnradmsLeadProfileDetailsStore();
			mobEdu.storeUtil.clearEnradmsLeadProfileListStore();
			mobEdu.storeUtil.clearEnradmsLeadProfileSearchStore();
			mobEdu.storeUtil.clearEnradmsLeadViewEmailStore();
			mobEdu.storeUtil.clearEnradmsLeadViewProfileListStore();
			mobEdu.storeUtil.clearEnradmsMainAllLeadDetailStore();
			mobEdu.storeUtil.clearEnradmsMainAllLeadsListStore();
			mobEdu.storeUtil.clearEnradmsMainAppointmentDetailStore();
			mobEdu.storeUtil.clearEnradmsMainAppointmentListStore();
			mobEdu.storeUtil.clearEnradmsMainEmailDetailStore();
			mobEdu.storeUtil.clearEnradmsMainEnrouteStore();
			mobEdu.storeUtil.clearEnradmsMainMenuStore();
			mobEdu.storeUtil.clearEnradmsMainRecruiterEmailStore();
			mobEdu.storeUtil.clearEnradmsNotifDetailsStore();
			mobEdu.storeUtil.clearEnradmsNotifListStore();
			mobEdu.storeUtil.clearEnradmsNotifLocalStore();
			mobEdu.storeUtil.clearEnradmsNotifMailStore();
			mobEdu.storeUtil.clearEnradmsNotifMarkAsUnreadStore();
			mobEdu.storeUtil.clearEnrouteLeadsProfileDetailsStore();
			mobEdu.storeUtil.clearEnrouteMainEnrouteStore();
			mobEdu.storeUtil.clearEnrouteMainMenuStore();
			mobEdu.storeUtil.clearEnrouteMainToAppStoreStore();
			mobEdu.storeUtil.clearEnrouteMainToStoreStore();

		},
		clearEnterDetailedProfileStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enter.store.detailedProfile');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteMainToStoreStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteMainToAppStoreStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteMainMenuStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.main.store.menu');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteMainEnrouteStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.main.store.enroute');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteLeadsViewProfileListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewProfileList');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteLeadsProfileDetailsStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnrouteLeadsLocalEmailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.localEmail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsNotifMarkAsUnreadStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.notif.store.markAsUnread');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsNotifMailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.notif.store.mail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsNotifLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.notif.store.local');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsNotifListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.notif.store.list');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsNotifDetailsStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.notif.store.details');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainRecruiterEmailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.recruiterEmail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearEnradmsMainMenuStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.menu');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainEnrouteStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.enroute');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainEmailDetailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.emailDetail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainAppointmentListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentsList');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainAppointmentDetailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.appointmentDetail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainAllLeadsListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadsList');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsMainAllLeadDetailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.main.store.allLeadDetail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadViewProfileListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewProfileList');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadViewEmailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.viewEmail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadProfileSearchStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileSearch');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadProfileListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileList');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadProfileDetailsStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.profileDetail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadLocalEmailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.localEmail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadEnumerationsStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.enumerations');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadEmailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.email');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadDocListStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.docList');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearEnradmsLeadAppointmentsStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointments');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearEnradmsLeadAppointmentDetailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enradms.leads.store.appointmentDetail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearPeopleMenuStore: function() {
			var store = mobEdu.util.getStore('mobEdu.people.store.menu');
			store.getProxy().clear();
			store.sync();
		},
		clearMainSlidingMenuStore: function() {
			var store = mobEdu.util.getStore('mobEdu.main.store.slidingMenu');
			store.getProxy().clear();
			store.sync();
		},

		clearFinaidStatusLocalStore: function() {
			var store = mobEdu.util.getStore('mobEdu.finaid.store.statusLocal');
			store.getProxy().clear();
			store.sync();
		},
		clearFinaidLoanHistoryLocalStore: function() {
			var store = mobEdu.util.getStore('mobEdu.finaid.store.loanHistoryLocal');
			store.getProxy().clear();
			store.sync();
		},

		clearFinaidAwardLocalStore: function() {
			var store = mobEdu.util.getStore('mobEdu.finaid.store.awardsLocal');
			store.getProxy().clear();
			store.sync();
		},

		clearAttendenceTermListLocalStore: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.termListLocal');
			store.getProxy().clear();
			store.sync();
		},

		clearAttendenceStudentDetailsStore: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.studentDetails');
			store.getProxy().clear();
			store.sync();
		},

		clearAttendenceMeetingScheduleStore: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			store.getProxy().clear();
			store.sync();
		},

		clearAttendenceClassDetailsStore: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.classDetails');
			store.getProxy().clear();
			store.sync();
		},

		clearAttendenceClassDetailsStore: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.classDetails');
			store.getProxy().clear();
			store.sync();
		},
		// clearEnterMenuStore:function(){
		//     var store=mobEdu.util.getStore('mobEdu.enter.store.enterMenu');
		//     store.getProxy().clear();
		//     store.sync();
		// },
		clearPendingApprovalsStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enter.store.pendingApprovals');
			store.getProxy().clear();
			store.sync();
		},
		clearEnterProfileStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enter.store.profile');
			store.getProxy().clear();
			store.sync();
		},
		clearEnqNofiDetailStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.notif.store.details');
			store.getProxy().clear();
			store.sync();
		},
		clearEnqNofiLocalStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.notif.store.local');
			store.getProxy().clear();
			store.sync();
		},
		clearEnqNofiMailStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.notif.store.mail');
			store.getProxy().clear();
			store.sync();
		},
		clearEntNofiDetailStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.notif.store.details');
			store.getProxy().clear();
			store.sync();
		},
		clearEntNofiLocalStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.notif.store.local');
			store.getProxy().clear();
			store.sync();
		},
		clearEntNofiMailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enroute.notif.store.mail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},
		clearCreateApplicationStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
				store.removeAll();
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearAccLocalSummaryStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.acc.store.localSummary');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},


		clearAccPaymentCardStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.acc.store.paymentCard');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},



		clearCourseListLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},



		clearCoursesHolidaysLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearCoursesMeetingScheduleLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.courses.store.meetingScheduleLocal');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearMainEntourageStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.main.store.entourage');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearNotifLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.notif.store.local');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearNotifMailStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.notif.store.mail');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearStudentProfileStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.profile.store.studentProfile');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearRegCartStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.reg.store.cart');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearRegCourseDetailLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearRegLocalCartStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.reg.store.localCart');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearRegMeetingScheduleLocalStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.reg.store.meetingScheduleLocal');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearRegPinStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.reg.store.pin');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

		clearAttdMenuStore: function() {
			try {
				var store = mobEdu.util.getStore('mobEdu.att.store.attdMenu');
				store.getProxy().clear();
				store.sync();
			} catch (err) {}
		},

	}
});