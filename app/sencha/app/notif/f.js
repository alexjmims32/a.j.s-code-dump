Ext.define('mobEdu.notif.f', {
	requires: [
		'mobEdu.notif.store.details',
		'mobEdu.notif.store.list',
		'mobEdu.notif.store.local',
		'mobEdu.notif.store.mail',
		'mobEdu.notif.store.update'
	],

	statics: {

		loadNotifPopup: function() {
			var store = mobEdu.util.getStore('mobEdu.notif.store.local');
			store.load();
			if (store.data.length > 0) {
				var popupView = mobEdu.util.get('mobEdu.notif.view.popup');

				if (!popupView.rendered) {
					popupView.setRenderTo(Ext.getBody());
				}
				var e = Ext.get("NOTIF");
				var x = e.getX();
				var y = e.getY();
				popupView.setLeft(x);
				popupView.setTop(y + 56);
				Ext.Viewport.add(popupView);
			} else {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>You have no notifications.</center>',
					buttons: Ext.MessageBox.OK
				});
			}
		},

		loadNotifications: function(isFromLogin) {
			// This is only for when the Auth string is available, i.e. if user is logged-in.
			if (mobEdu.util.getAuthString() == undefined || mobEdu.util.getAuthString() == null) {
				return;
			} else {
				count = 0;
				var maxId = null;
				var nLStore = mobEdu.util.getStore('mobEdu.notif.store.local');
				if (nLStore.data.length > 0) {
					var rec = nLStore.getAt(nLStore.data.length - 1);
					maxId = rec.get('noticeId');
				}
				//For removing mask while notifications processing
				Ext.Ajax.on('beforerequest', function() {
					// hiding the loading mask
					if (mobEdu.util.masked == false) {
						Ext.Viewport.unmask();
					}
				});

				var store = mobEdu.util.getStore('mobEdu.notif.store.list');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setExtraParams({
					maxId: maxId,
					studentId: mobEdu.util.getStudentId(),
					companyId: companyId
				});
				store.getProxy().setUrl(commonwebserver + 'notification/list');
				store.getProxy().afterRequest = function() {
					mobEdu.notif.f.notificationResponseHandler(isFromLogin, store);
				}
				store.load();
				mobEdu.util.gaTrackEvent('commons', 'notification/list');
			}
		},
		notificationResponseHandler: function(isFromLogin, store) {
			var nStore = mobEdu.util.getStore('mobEdu.notif.store.list');
			var nLStore = mobEdu.util.getStore('mobEdu.notif.store.local');
			mobEdu.notif.f.setBadge(nLStore);
			mobEdu.notif.f.checkNotif(0, store, nLStore);
			//Added mask after completeing notification process
			Ext.Ajax.on('beforerequest', function() {
				// showing the loading mask
				Ext.Viewport.setMasked({
					xtype: 'loadmask',
					message: 'Please wait...'
				});
			});
			// if (isFromLogin) {
			// 	if (mobEdu.util.getNextView() != null) {
			// 		(mobEdu.util.getNextView())();
			// 	}
			// 	mobEdu.util.updateNextView(null);
			// }
		},

		checkNotif: function(recCount, nStore, nLStore) {
			// if (recCount < nStore.data.length) {
			// var record = nStore.getAt(recCount);
			for (var i = 0; i < nStore.data.items.length; i++) {
				var record = nStore.data.items[i];
				if (!(mobEdu.notif.f.hasRec(record))) {
					var readFlag = record.get('readFlag');
					nLStore.add(record.copy());
					nLStore.sync();
					if (readFlag == '0') {
						var nMStore = mobEdu.util.getStore('mobEdu.notif.store.mail');
						nMStore.load();
						nMStore.removeAll();
						nMStore.sync();
						nMStore.removed = [];
						var title = mobEdu.util.resizeText(record.data.title, 15);
						var msg = mobEdu.util.resizeText(record.data.message, 40);
						nMStore.add({
							title: title,
							message: msg
						});
						nMStore.sync();
						mobEdu.notif.f.setBadge(nLStore);
						//mobEdu.notif.f.showMailPopup(recCount, nStore, nLStore);
					}
				}
			}
			// mobEdu.notif.f.checkNotif(recCount + 1, nStore, nLStore)
		},

		setBadge: function(nLStore) {
			count = 0;
			//To count number of unread notifications
			nLStore.each(function(record) {
				if (record.data.readFlag == '0') {
					count = count + 1;
				}
			});
			//setting count as label to button
			var notifBtn = Ext.getCmp('NOTIF');
			if (typeof notifBtn != 'undefined') {
				if (count == 0) {
					notifBtn.setBadgeText(null);
				} else {
					notifBtn.setBadgeText(count);
				}
			}
		},

		// showMailPopup: function(recCount, nStore, nLStore) {
		// 	var alertPopup = Ext.Viewport.add(
		// 		mobEdu.util.get('mobEdu.notif.view.alertPopup'));
		// 	alertPopup.show();
		// 	setTimeout(function() {
		// 		alertPopup.hide();
		// 		recCount = recCount + 1;
		// 		mobEdu.notif.f.checkNotif(recCount, nStore, nLStore);
		// 	}, 3000);

		// },

		getTitle: function(title) {
			var count = title.length;
			if (count > 15) {
				title = title.substr(0, 15) + '...';
			}
			return title;
		},

		getMessage: function(message) {
			var count = message.length;
			if (count > 40) {
				message = message.substr(0, 40) + '...';
			}
			return message;
		},

		hasRec: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.notif.store.local');
			store.load();
			var recordIndex = store.findBy(
				function(record, id) {
					if (record.data.noticeId === rec.data.noticeId) {
						return true; // a record with this data exists
					}
					return false; // there is no record in the store with this data
				}
			);
			if (recordIndex != -1) {
				return true;
			} else {
				return false;
			}

		},

		hasReadFlag: function(readFlag) {
			if (readFlag == "1") {
				return true;
			} else {
				return false;
			}
		},

		EmrType: function(type) {
			if (type == 'Emergency') {
				return true;
			} else {
				return false;
			}
		},

		loadNotificationDetail: function(viewRef, selectedItemIndex, e) {
			mobEdu.notif.f.hideNotifPopup();
			mobEdu.util.currentModule = 'NOTIF';
			var rec = e.data;
			var count = 0;
			var store = mobEdu.util.getStore('mobEdu.notif.store.details');
			store.load();
			store.removeAll();
			store.add(rec);
			store.sync();
			mobEdu.notif.f.markAsRead(rec);
		},

		markAsRead: function(rec) {
			var id = rec.noticeId;
			var store = mobEdu.util.getStore('mobEdu.notif.store.update');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				id: id,
				companyId: companyId
			});
			store.getProxy().setUrl(commonwebserver + 'notification/markAsRead');
			store.getProxy().afterRequest = function() {
				mobEdu.notif.f.markAsReadResponseHandler(rec);
			}
			store.load();
			mobEdu.util.gaTrackEvent('commons', 'notification/markAsRead');
		},

		markAsReadResponseHandler: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.notif.store.update');
			var status = store.getProxy().getReader().rawData;
			if (status == 'SUCCESS') {
				var nLStore = mobEdu.util.getStore('mobEdu.notif.store.local');
				//Get the record
				var localRecord = nLStore.findRecord('noticeId', rec.noticeId, null, null, null, true);
				localRecord.set('readFlag', '1');
				nLStore.sync();
				mobEdu.notif.f.setBadge(nLStore);
				mobEdu.notif.f.showNotifDetail();
				mobEdu.notif.f.refreshList();
			}
		},

		markAsUnread: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.notif.store.details');
			var rec = detailStore.getAt(0);
			var id = rec.get('noticeId');
			var store = mobEdu.util.getStore('mobEdu.notif.store.update');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				id: id,
				companyId: companyId
			});
			store.getProxy().setUrl(commonwebserver + 'notification/markAsUnread');
			store.getProxy().afterRequest = function() {
				mobEdu.notif.f.markAsUnreadResponseHandler(id);
			}
			store.load();
			mobEdu.util.gaTrackEvent('commons', 'notification/markAsUnread');
		},

		markAsUnreadResponseHandler: function(id) {
			var store = mobEdu.util.getStore('mobEdu.notif.store.update');
			var status = store.getProxy().getReader().rawData;
			if (status == 'SUCCESS') {
				var nLStore = mobEdu.util.getStore('mobEdu.notif.store.local');
				//Get the record
				var localRecord = nLStore.findRecord('noticeId', id, null, null, null, true);
				localRecord.set('readFlag', '0');
				nLStore.sync();
				mobEdu.notif.f.setBadge(nLStore);
				mobEdu.notif.f.refreshList();
			}
		},

		deleteNotif: function() {
			Ext.Msg.show({
				message: 'Sure? You want to delete this notification?',
				buttons: Ext.MessageBox.YESNO,
				cls: 'msgbox',
				fn: function(btn) {
					if (btn == 'yes') {
						var detailStore = mobEdu.util.getStore('mobEdu.notif.store.details');
						var rec = detailStore.getAt(0);
						var id = rec.get('noticeId');
						var store = mobEdu.util.getStore('mobEdu.notif.store.update');
						store.getProxy().setHeaders({
							Authorization: mobEdu.util.getAuthString(),
							companyID: companyId
						});
						store.getProxy().setExtraParams({
							id: id,
							companyId: companyId
						});
						store.getProxy().setUrl(commonwebserver + 'notification/delete');
						store.getProxy().afterRequest = function() {
							mobEdu.notif.f.deleteNotifResponseHandler(id);
						}
						store.load();
						mobEdu.util.gaTrackEvent('commons', 'notification/delete');
					}
				}
			});
		},

		hasStatusReadFlag: function(status) {
			if (status == "OPENED") {
				return true;
			} else {
				return false;
			}
		},

		deleteNotifResponseHandler: function(id) {
			var store = mobEdu.util.getStore('mobEdu.notif.store.update');
			var status = store.getProxy().getReader().rawData;
			if (status == 'SUCCESS') {
				var nLStore = mobEdu.util.getStore('mobEdu.notif.store.local');
				//Get the record
				var localRecord = nLStore.findRecord('noticeId', id, null, null, null, true);
				nLStore.remove(localRecord);
				nLStore.sync();
				nLStore.removed = [];
				mobEdu.notif.f.setBadge(nLStore);
				mobEdu.notif.f.refreshList();
				mobEdu.util.showMainView();
			}
		},
		refreshList: function() {
			var listCmp = Ext.getCmp('notifList');
			if (listCmp != null && listCmp != '') {
				listCmp.refresh();
			}
		},
		submitPaymentForm: function() {
			var card = Ext.getCmp('notifCardNumber').getValue();
			var cardNum = '';
			if (card != null && card != 0) {
				cardNum = card.toString();
			}
			//            var cardNum = Ext.getCmp('notifCardNumber').getValue();
			var name = Ext.getCmp('notifName').getValue();
			var amount = Ext.getCmp('notifAmount').getValue();
			var expDate = Ext.getCmp('notifExpDate').getValue();
			var cvv = Ext.getCmp('notifCvv').getValue();
			var zip = Ext.getCmp('notifZip').getValue();
			if ((cardNum == '' || cardNum == null) || (name == '' || name == null) || (amount == '' || amount == null) || (expDate == '' || expDate == null) || (cvv == '' || cvv == null) || (zip == '' || zip == null)) {
				Ext.Msg.show({
					message: 'Please enter all mandatory values.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (cardNum.length != 16) {
					Ext.Msg.show({
						message: 'Invalid card number.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {

					var el = Ext.getBody();
					var myMask = new Ext.LoadMask(el, {
						msg: 'Please wait...'
					});
					myMask.show();

					setTimeout(function() {
						Ext.Msg.show({
							message: 'Payment received successfully.',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox',
							fn: function(btn) {
								if (btn == 'ok') {
									mobEdu.notif.f.showNotifDetail();
									mobEdu.notif.f.resetPaymentForm();
								}
							}
						});
					}, 1000);
				}
			}
		},
		resetPaymentForm: function() {
			mobEdu.util.get('mobEdu.notif.view.makePayment').reset();
		},
		onCardNumberKeyup: function(cardNumberfield) {
			var cardNumber = (cardNumberfield.getValue()).toString();
			var length = cardNumber.length;
			if (length > 16) {
				cardNumberfield.setValue(cardNumber.substring(0, 16));

				return false;
			}
			return true;
		},
		onCardNumberBlur: function(cardNumberfield) {
			if (cardNumberfield.getValue() != null) {
				var cardNumber = (cardNumberfield.getValue()).toString();
				var length = cardNumber.length;
				if (length != 16)
					Ext.Msg.show({
						message: 'Invalid card number.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
			}
		},
		showNotifDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.main.f.displayPreMenu);
			mobEdu.util.show('mobEdu.notif.view.details');
		},
		showNotifMakePayment: function() {
			mobEdu.util.updatePrevView(mobEdu.notif.f.showNotifDetail);
			mobEdu.util.show('mobEdu.notif.view.makePayment');
		},

		activatePushNotifications: function() {
			if (typeof window.CustomNativeAccess != 'undefined') {
				var pushNotifCheckActive = window.CustomNativeAccess.isNotifJobEnabled();
				if (!pushNotifCheckActive) {
					setInterval(function() {
						mobEdu.notif.f.checkPushNotifications();
					}, 3000);
					window.CustomNativeAccess.setNotifJobEnabled();
				}
			}
		},
		checkPushNotifications: function() {
			if (mobEdu.main.f.getStudentId() == null) {
				return;
			}
			var newNotifCount = window.CustomNativeAccess.getNewNotifCount();
			if (newNotifCount > 0) {
				mobEdu.notif.f.loadNotifications();
				window.CustomNativeAccess.resetNotifCount();
			}
		},
		hideNotifPopup: function() {
			var popUp = Ext.getCmp('popup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.notif.view.popup');
			}
		}
	}
});