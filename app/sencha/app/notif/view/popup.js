Ext.define('mobEdu.notif.view.popup', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.notif.store.local'
	],
	config: {
		id: 'popup',
		floating: true,
		modal: true,
		hideOnMaskTap: true,
		cls: 'logo',
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '60%',
		height: '60%',
		layout: 'fit',
		items: [{
			xtype: 'list',
			id: 'notifList',
			name: 'notifList',
			itemTpl: new Ext.XTemplate('<table width="100%">',
				'<tpl if="(mobEdu.notif.f.hasReadFlag(readFlag)===true)">',
				'<tpl if="(mobEdu.notif.f.EmrType(type)===true)">',
				'<tr ><td width="5%" rowspan="2"><img width=32 height=32 src="' + mobEdu.util.getResourcePath() + 'images/openMailSmallIcon.png" align="absmiddle" />&nbsp;&nbsp;<td><h3><font color=red>!</font>&nbsp;{title}</h3></td></tr><tr><td><h6>{lastModifiedBy}</h6></td></tr>',
				'<tpl else>',
				'<tr ><td width="5%" rowspan="2"><img width=32 height=32 src="' + mobEdu.util.getResourcePath() + 'images/openMailSmallIcon.png" align="absmiddle" />&nbsp;&nbsp;<td><h3>{title}</h3></td></tr><tr><td><h6>{lastModifiedBy}</h6></td></tr>',
				'</tpl>',
				'<tpl else>',
				'<tpl if="(mobEdu.notif.f.EmrType(type)===true)">',
				'<tr><td width="5%" rowspan="2"><img width=32 height=32 src="' + mobEdu.util.getResourcePath() + 'images/mailSmallIcon.png" align="absmiddle" />&nbsp;&nbsp;<td><h3><font color=red>!</font>&nbsp;{title}</h3></td></tr><tr><td><h6><b>{lastModifiedBy}</b></h6></td></tr>',
				'<tpl else>',
				'<tr><td width="5%" rowspan="2"><img width=32 height=32 src="' + mobEdu.util.getResourcePath() + 'images/mailSmallIcon.png" align="absmiddle" />&nbsp;&nbsp;<td><h3>{title}</h3></td></tr><tr><td><h6><b>{lastModifiedBy}</b></h6></td></tr>',
				'</tpl>',
				'</tpl>',
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.notif.store.local'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, item, e) {
					mobEdu.notif.f.loadNotificationDetail(view, index, e);
				}
			}
		}],
		flex: 1,
		listeners: {
			hide: mobEdu.notif.f.hideNotifPopup
		}
	}
});