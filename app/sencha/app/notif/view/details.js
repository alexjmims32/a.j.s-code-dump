Ext.define('mobEdu.notif.view.details', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.notif.store.details'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'dataview',
			setHtmlContent: true,
			padding: '10 10 10 10',
			disableSelection: true,
			itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><h2>{title}</h2></td></tr>' + '<tr><td><i><h4>{type}</h4></i></td></tr>' + '<tr><td><h5>Sent: {lastModifiedOn}</h5></td></tr>' + '<tr><td><h5>From: {lastModifiedBy}</h5><br /></td></tr>' + '<tr><td><h3>{[this.getDescription(values.message)]}</h3></td></tr>' + '</table>', {
				compiled: true,
				getDescription: function(description) {
					return mobEdu.util.convertRelativeToAbsoluteUrls(decodeURIComponent(description));
				}
			}),
			store: mobEdu.util.getStore('mobEdu.notif.store.details'),
			singleSelect: true,
			loadingText: ''
		}, {
			xtype: 'customToolbar',
			title: '<h1>Notification Details</h1>'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
					text: 'Delete',
					ui: 'action',
					handler: function() {
						mobEdu.notif.f.deleteNotif();
					}
				}, {
					text: 'Mark as Unread',
					ui: 'action',
					handler: function() {
						mobEdu.notif.f.markAsUnread();
					}
				}
				// {
				//     text: 'Pay',
				//     ui: 'confirm',
				//     id: 'pay',
				//     name: 'pay',
				//     handler: function() {
				//         mobEdu.notif.f.showNotifMakePayment();
				//     }
				// }
			]
		}],
		flex: 1
	}
});