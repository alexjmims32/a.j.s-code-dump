Ext.define('mobEdu.notif.view.alertPopup', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.notif.store.mail'
	],
	config: {
		id: 'alertPopup',
		floating: true,
		bottom: '2%',
		right: '1%',
		width: '40%',
		height: '20%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'dataview',
			id: 'mail',
			name: 'mail',
			itemTpl: new Ext.XTemplate('<table width="100%">' +
				'<tr>' +
				'<td rowspan="2" width="20px"><img width=16 height=16 src="' + mobEdu.util.getResourcePath() + 'images/mailSmallIcon.png"  align="absmiddle" /></td>' +
				'<td style="color: #0066cc;text-align:left"><h5><b>{title}</b></h5></td>' +
				'</tr>' +
				'<tr><td><h5>{[decodeURIComponent(values.message)]}</h5></td></tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.notif.store.local'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, item, e, record) {
					mobEdu.notif.f.loadNotificationDetail(view, index, e);
				}
			}
		}],
		flex: 1
	}
});