Ext.define('mobEdu.notif.view.makePayment', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        items: [{
            xtype: 'fieldset',
            items: [{
                xtype: 'selectfield',
                labelAlign: 'left',
                labelWidth: '40%',
                label: 'Card Type',
                required: true,
                height: 'auto',
                options: [{
                    text: 'Visa',
                    value: 'visa'
                }, {
                    text: 'Master',
                    value: 'master'
                }, {
                    text: 'AmericanExpress',
                    value: 'americanexpress'
                }, {
                    text: 'Cirrus',
                    value: 'cirrus'
                }]
            }, {
                xtype: 'numberfield',
                labelAlign: 'left',
                labelWidth: '40%',
                label: 'Card Number',
                id: 'notifCardNumber',
                name: 'notifCardNumber',
                placeHolder: 'XXXX-XXXX-XXXX-XXXX',
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[0-9]*'
                    });
                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.notif.f.onCardNumberKeyup(textfield);
                    },
                    blur: function(textfield, eventObj) {
                        mobEdu.notif.f.onCardNumberBlur(textfield);
                    }
                },
                maxLength: 16,
                useClearIcon: true,
                autoCapitalize: false,
                required: true
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Name',
                id: 'notifName',
                name: 'notifName',
                placeHolder: 'Card Holder Name',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'numberfield',
                labelWidth: '40%',
                label: 'Amount',
                id: 'notifAmount',
                name: 'notifAmount',
                placeHolder: '$',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[0-9]*'
                    });
                    me.callParent(arguments);
                }
            }, {
                xtype: 'datepickerfield',
                labelWidth: '40%',
                label: 'Expiry Date',
                id: 'notifExpDate',
                name: 'notifExpDate',
                value: new Date(),
                //                placeHolder:'DD/MM/YYYY',
                picker: {
                    yearFrom: 2012,
                    yearTo: 2020,
                    slotOrder: ['day', 'month', 'year']
                },
                required: true,
                useClearIcon: true
            }, {
                xtype: 'numberfield',
                labelWidth: '40%',
                label: 'CVV',
                id: 'notifCvv',
                name: 'notifCvv',
                placeHolder: 'Number',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[0-9]*'
                    });
                    me.callParent(arguments);
                }
            }, {
                xtype: 'numberfield',
                labelWidth: '40%',
                label: 'Zip Code',
                name: 'notifZip',
                id: 'notifZip',
                placeHolder: '',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[0-9]*'
                    });
                    me.callParent(arguments);
                },
                maxLength: 6,
                autoCreate: Ext.apply({
                    maxlength: '1'
                }, Ext.form.Field.prototype.defaultAutoCreate)
            }]
        }, {
            xtype: 'customToolbar',
            title: '<h1>Payment</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Cancel',
                handler: function() {
                    mobEdu.notif.f.resetPaymentForm();
                }
            }, {
                text: 'Submit',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.notif.f.submitPaymentForm();
                }
            }]
        }]
    }
});