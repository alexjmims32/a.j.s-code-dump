Ext.define("mobEdu.notif.model.details", {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'noticeId',
			type: 'string',
			// mapping: 'id'
		}, {
			name: 'type',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'title',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'message',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'dueDate',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'expiryDate',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'lastModifiedBy',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'lastModifiedOn',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'delivered',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'username',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'posted',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'readFlag',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'status',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}, {
			name: 'notificationStatus',
			type: 'string',
			convert: function(value, record) {
				if (value == '' || value == null || value=='null') {
					return '';
				} else
					return value;
			}
		}]
	}

});