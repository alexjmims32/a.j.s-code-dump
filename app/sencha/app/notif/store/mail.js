Ext.define('mobEdu.notif.store.mail', {
    extend:'Ext.data.Store',
//    alias: 'notificationLocalStore',

    requires: [
        'mobEdu.notif.model.details'
    ],
//       model : 'notificationModel',
    config: {
        storeId: 'mobEdu.notif.store.mail',

        autoLoad: false,

        model: 'mobEdu.notif.model.details',

      proxy:{
        type:'localstorage',
        id:'mobEdu.notif.store.mail'
      }

    }
});