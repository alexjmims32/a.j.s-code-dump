Ext.define('mobEdu.notif.store.list', {
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.notif.model.details'
    ],

    config: {
        storeId: 'mobEdu.notif.store.list',

        autoLoad: false,

        model: 'mobEdu.notif.model.details'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'notificationList'        
        return proxy;
    }
});