Ext.define('mobEdu.notif.store.local', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.notif.model.details'
    ],
    config: {
        storeId: 'mobEdu.notif.store.local',

        autoLoad: true,

        model: 'mobEdu.notif.model.details',
        
        sorters: [
              {
                 property : 'noticeId',
                  direction: 'DESC'
             }
         ],

      proxy:{
        type:'localstorage',
        id:'notifLocal'
      }

    }
});