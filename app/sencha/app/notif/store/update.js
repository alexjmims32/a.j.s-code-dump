Ext.define('mobEdu.notif.store.update', {
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.notif.model.details'
    ],

    config: {
        storeId: 'mobEdu.notif.store.update',

        autoLoad: false,

        model: 'mobEdu.notif.model.details'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});