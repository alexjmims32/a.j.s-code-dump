Ext.define('mobEdu.notif.store.markAsUnread', {
	extend: 'Ext.data.Store',
	//    extend:'com.n2n.data.store',
	requires: [
		'mobEdu.notif.model.details'
	],

	config: {
		storeId: 'mobEdu.notif.store.markAsUnread',

		autoLoad: false,

		model: 'mobEdu.notif.model.details',

		proxy: {
			type: 'ajax',
			reader: {
				type: 'json'
				//                rootProperty: ''
			}
		}
	}

});