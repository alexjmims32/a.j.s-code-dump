Ext.define('mobEdu.notif.store.details', {
    extend:'Ext.data.Store',
//    alias: 'notificationDetailStore',

    requires: [
        'mobEdu.notif.model.details'
    ],

//       model : 'notificationModel',
    config: {
        storeId: 'mobEdu.notif.store.details',

        autoLoad: false,

        model: 'mobEdu.notif.model.details',

      proxy:{
        type:'memory',
        id:'notifDetails'
      }
    }

});