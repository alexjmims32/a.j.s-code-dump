Ext.define('mobEdu.notif.store.enrouteDetails', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.notif.model.details'
	],

	config: {
		storeId: 'mobEdu.notif.store.enrouteDetails',

		autoLoad: false,

		model: 'mobEdu.notif.model.details',

		//        proxy:{
		//            type:'localstorage',
		//            id:'enrouteNotifDetails'
		//        }
		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: ''
			}
		}
	}

});