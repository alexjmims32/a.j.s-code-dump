Ext.define('mobEdu.enter.model.enterMenu', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'img'
            },
            {
                name:'title'
            },
            {
                name:'action',
                type: 'function'
            }
        ]
    }
});