Ext.define("mobEdu.enter.model.profile", {
    extend:'Ext.data.Model',

    config:{
        fields : [ 
            {
                name: 'name'
            },{
                name: 'username'
            },{
                name: 'img'
            },{
                name:'hPhone'
            },{
                name:'mPhone'
            },{
                name:'email'
            },{
                name:'address'
            },{
                name:'city'
            },{
                name:'state'
            },{
                name:'postal'
            },{
                name:'eContact'
            },{
                name:'ePhone'
            },{
                name:'approveFlag'
            }
//        {
//            name : 'name',
//            type : 'string'
//        },
//        {
//            name : 'homePh',
//            type : 'string'
//        },
//        {
//            name : 'mobilePh',
//            type : 'string'
//        },
//        {
//            name : 'email',
//            type : 'string'
//        },
//        {
//            name : 'address',
//            type : 'string'
//        },
//        {
//            name : 'city',
//            type : 'string'
//        },
//        {
//            name : 'state',
//            type : 'string'
//        },
//        {
//            name : 'postal',
//            type : 'string'
//        },
//        {
//            name : 'phone',
//            type : 'string'
//        }
        ]
    }
});