Ext.define('mobEdu.enter.store.detailedProfile', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.enter.model.profile'
    ],

    config:{
        storeId: 'mobEdu.enter.store.detailedProfile',
        autoLoad: true,
        model: 'mobEdu.enter.model.profile',
        proxy:{
            type:'localstorage',
            id:'detailedProfile'
        }
    }
});