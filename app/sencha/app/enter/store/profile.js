Ext.define('mobEdu.enter.store.profile', {
    extend:'Ext.data.Store',

    requires: [
    'mobEdu.enter.model.profile'
    ],

    config:{
        storeId: 'mobEdu.enter.store.profile',
        autoLoad: false,
        model: 'mobEdu.enter.model.profile',
        proxy:{
            type:'localstorage',
            id:'profile'
        }
    }
});