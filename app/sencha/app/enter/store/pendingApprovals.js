Ext.define('mobEdu.enter.store.pendingApprovals', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.enter.model.profile'
    ],

    config:{
        storeId: 'mobEdu.enter.store.pendingApprovals',
        autoLoad: true,
        model: 'mobEdu.enter.model.profile',
        proxy:{
            type:'localstorage',
            id:'pendingApprovals'
        }
    }
});