Ext.define('mobEdu.enter.store.enterMenu', {
    extend:'Ext.data.Store',

    requires: [
    'mobEdu.enter.model.enterMenu'
    ],

    config:{
        storeId: 'mobEdu.enter.store.enterMenu',
        autoLoad: false,
        model: 'mobEdu.enter.model.enterMenu',
        proxy:{
            type:'memory',
            id:'enterMenu'
        }
    }
});