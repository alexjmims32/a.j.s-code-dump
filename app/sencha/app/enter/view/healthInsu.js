Ext.define('mobEdu.enter.view.healthInsu', {
    extend:'Ext.Panel',
    requires: [
    'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:{
            type:'vbox'
        },
        padding:'0 0 0 50',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>',
            items:[
            {
                ui:'light',
                xtype:'button',
                text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();

                }
            },
            // Back Button
            {
                xtype: 'img',
                cls: 'back',
                padding: '10 0 0 0',
                width: 32,
                height: 32,
                listeners: {
                    tap: function() {
                        mobEdu.enter.f.showMyChecklist();
                    }
                }
            }
            ]
        },
        {
            html:'<b>Health Insurance<b>',  
            padding:'0 0 20 0'
        },
        {
            xtype:'textfield',
            name:'name',
            labelWidth:'50%',
            label:'Name',
            id:'name',
            margin:'1'
        },
        {
            xtype:'textfield',
            name:'homePh',
            labelWidth:'50%',
            label:'Home Ph.',
            margin:'1',
            id:'homePh'
        },
        {
            xtype:'textfield',
            name:'mobilePh',
            labelWidth:'50%',
            label:'Mobile Ph.',
            margin:'1',
            id:'mobilePh'
        },
        {
            xtype:'textfield',
            name:'email',
            labelWidth:'50%',
            label:'e-mail',
            margin:'1',
            id:'email'
        },
        {
            xtype:'textfield',
            name:'address',
            labelWidth:'50%',
            label:'Address',
            margin:'1',
            id:'address'
        },
        {
            xtype:'textfield',
            name:'city',
            labelWidth:'50%',
            label:'City',
            margin:'1',
            id:'city'
        },
        {
            xtype:'textfield',
            name:'state',
            labelWidth:'50%',
            label:'State',
            margin:'1',
            id:'state'
        },
        {
            xtype:'textfield',
            name:'postal',
            labelWidth:'50%',
            label:'Postal',
            margin:'1',
            id:'postal'
        },
        {
            xtype:'textfield',
            name:'socialsec',
            labelWidth:'50%',
            label:'Social Sec#',
            margin:'1',
            id:'socialSec'
        },     
        {
            xtype:'textfield',
            name:'birthDate',
            labelWidth:'50%',
            label:'Birth Date',
            margin:'1',
            id:'birthDate'
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Next',
                handler:function () {
                    mobEdu.util.showMainView();
                }
            }
            ]
        }
        ]
    }

});