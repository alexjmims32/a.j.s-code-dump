Ext.define('mobEdu.enter.view.myStatus', {
    extend:'Ext.Panel',
    requires: [
         'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:'vbox',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>',
            items:[
            {
                ui:'light',
                xtype:'button',
                text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            // Home Button
            {
                xtype: 'img',
                cls: 'back',
                padding: '10 0 0 0',
                width: 32,
                height: 32,
                listeners: {
                    tap: function() {
                        mobEdu.enter.f.showeNterMenu();
                    }
                }
            }
            ]
        },
        {
            html:'<h2>My Status</h2>',
            padding:'10 0 20 5'
        },
        {
            items:[
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 0 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Forms Completed</h3>',
                    flex:2
                },
                {
                    xtype:'label',
                    html:'<h3>50%</h3>',
                    flex:1
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 0 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Online Courses Completed</h3>',
                    flex:2
                },
                {
                    xtype:'label',
                    html:'<h3>50%</h3>',
                    flex:1
                }
                ]
            }
            ]

        },
        {
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'0 0 10 5',
            items:[
            {
                xtype:'label',
                html:'<h3>Classes Completed</h3>',
                flex:2
            },
            {
                xtype:'label',
                html:'<h3>50%</h3>',
                flex:1
            }
            ]
        },
        {
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'0 0 10 5',
            items:[
            {
                xtype:'label',
                html:'<h2>Total Completed</h2>',
                flex:2
            },
            {
                xtype:'label',
                html:'<h3>50%</h3>',
                flex:1
            }
            ]
        }
        ]
    }

});


