Ext.define('mobEdu.enter.view.welcome', {
    extend:'Ext.Panel',
    requires: [
         'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        cls:'logo',
        layout:{
            type:'vbox'
        },
        padding:'0 0 0 5',
        items:[
        {
            flex:2,
            padding:'15 0 0 5',
            xtype:'label',
            html:'<p>Welcome to the Morehouse School Of Medicine Staff.'+
                'The onboarding process of bringing a new employee to the'+
                ' organization includes providing information, training,'+
                ' mentoring and coaching throughout the transition.'+
                ' The process begins at the acceptance of an offer and'+
                ' may continue for up to six months.<br /><br />'+
                'This mobile application will assist in making all aspects'+
                ' of transition a smooth one.</p>'
        },
         {
                xtype:'toolbar',
                docked:'top',
                title:'<h1>eNter</h1>',
                ui:'light'
         },
        {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                // Search Button
                items:[
                    {
                        text:'Next',
                        handler:function () {
                            mobEdu.enter.f.loadMainMenu();
                        }
                    }
                ]
            }
        ]
    }

});