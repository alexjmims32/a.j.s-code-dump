Ext.define('mobEdu.enter.view.capturePic', {
    extend:'Ext.Panel',
    requires: [
    'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:'vbox',
        styleHtmlContent: true,
        html: [],
        cls:'logo',
        items:[
        {
            xtype:'image',
            id:'cImg',
            name:'cImg'
        },{
            docked:'top',
            title:'<h1>eNter</h1>',
            xtype:'toolbar',
            
            items:[
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enter.f.showMyProfile();
                }
            },
            {
                xtype:'spacer'  
            },
            {
                ui:'light',
                xtype:'button',
                cls:'home',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.isHomeTapped=true;
                    mobEdu.util.showMainView();
                }
            }
            ]
        },{
            docked:'bottom',
            layout:{
                pack:'right'
            },
            xtype:'toolbar',
            style:'font-size:11pt',
            
            items:[
            {
                xtype:'button',
                text:'Capture',
                handler: function() {
                    capturePhoto();
                }
            }]
        }
        ],
        flex:1
    }
});