Ext.define('mobEdu.enter.view.detailedProfile', {
    extend:'Ext.Panel',
    requires: [
    'mobEdu.enter.f',
    'mobEdu.enter.store.detailedProfile'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:'vbox',
        cls:'logo',
        items:[
        {
            flex:2,
            xtype:'list',
            disableSelection:true,
            itemTpl:new Ext.XTemplate('<table width="100%">' +
                '<tr><td><h2>Name</h2></td><td>&nbsp;</td><td><h3>{name}</h3></td></tr>' +
                '<tr><td><h2>Home Ph.</h2></td><td>&nbsp;</td><td><h3>{hPhone}</h3></td></tr>' +
                '<tr><td><h2>Mobile Ph.</h2></td><td>&nbsp;</td><td><h3>{mPhone}</h3></td></tr>' +
                '<tr><td><h2>e-mail</h2></td><td>&nbsp;</td><td><h3>{email}</h3></td></tr>' +
                '<tr><td><h2>Address</h2></td><td>&nbsp;</td><td><h3>{address}</h3></td></tr>' +
                '<tr><td><h2>City</h2></td><td>&nbsp;</td><td><h3>{city}</h3></td></tr>' +
                '<tr><td><h2>State</h2></td><td>&nbsp;</td><td><h3>{state}</h3></td></tr>' +
                '<tr><td><h2>Postal</h2></td><td>&nbsp;</td><td><h3>{postal}</h3></td></tr>' +
                '<tr><td><h2>Emer. Contact</h2></td><td>&nbsp;</td><td><h3>{eContact}</h3></td></tr>' +
                '<tr><td><h2>Phone</h2></td><td>&nbsp;</td><td><h3>{ePhone}</h3></td></tr>' +
                '<tr><td><h2>Document(s)</h2></td><td>&nbsp;</td><td align="left">&nbsp;</td></tr>' +
                '<tr><td><h2>&nbsp;</h2></td><td>&nbsp;</td><td align="left"><img height="50%" src="{img}" /></td></tr>' +
                '</table></td></tr>'+
                '</table>'
                ),
            store:mobEdu.util.getStore('mobEdu.enter.store.detailedProfile'),
            singleSelect:true,
            loadingText: ''
        },
        {
            title:'<h1>eNter</h1>',
            xtype:'toolbar',
            ui:'light',
            docked:'top',
            // Back Button
            items:[
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enter.f.showPendingApprovals();
                }
            },
            // Home Button
            {
                xtype:'spacer'
            },
            {
                ui:'light',
                xtype:'button',
                cls:'home',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.isHomeTapped=true;
                    mobEdu.util.showMainView();
                }
            }
            
            ]
        }
        ],
        flex:1
    }

});
