Ext.define('mobEdu.enter.view.directory', {
    extend:'Ext.Panel',
    requires: [
         'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:{
            type:'vbox'

        },
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            title:'<h1>eNter</h1>',
            ui:'light',
            items:[
            {
                xtype:'button',
                cls:'back',
                  height: 32,
                width: 32,
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enter.f.showeNterMenu();
                }
            },
            {
                xtype:'spacer'  
            },
            // Home Button
            {
                ui:'light',
                xtype:'button',cls:'home',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.isHomeTapped=true;
                    mobEdu.util.showMainView();
                }
            }
            ]
        },
        {
            xtype:'label',
            centered:true,
            html:'<p>DIRECTORY OF PEOPLE AND EMAIL AND NUMBERS HERE</p>'
        }
        ]
    }

});