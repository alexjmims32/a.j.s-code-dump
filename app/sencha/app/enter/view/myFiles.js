Ext.define('mobEdu.enter.view.myFiles', {
    extend:'Ext.Panel',
    requires: [
    'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:{
            type:'vbox'
        },
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>',
            items:[
            {
                ui:'light',
                xtype:'button',
                text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            // Home Button
            {
                xtype: 'img',
                cls: 'back',
                padding: '10 0 0 0',
                width: 32,
                height: 32,
                listeners: {
                    tap: function() {
                        mobEdu.enter.f.showeNterMenu();
                    }
                }
            }
            ]
        },
        {
            xtype:'label',
            centered:true,
            html:'<p>MY FILES HERE</p>'
        }
        ]
    }

});