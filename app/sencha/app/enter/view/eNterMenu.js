Ext.define('mobEdu.enter.view.eNterMenu', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enter.f',
        'mobEdu.enter.store.enterMenu'
    ],
    config: {
        fullscreen: true,
        layout: {
            type: 'vbox'
        },
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'enterTitle'
        }, {
            flex: 2,
            xtype: 'list',
            cls: 'logo',
            id: 'modules',
            itemTpl: '<table width="100%"><tr><td width="5%" align="left"><img width="57px" height="57px" src="{img}"></td><td width="95%" align="left"><h3 style="font-size:15px">{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.enter.store.enterMenu'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }]
    }
});