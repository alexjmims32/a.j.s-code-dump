Ext.define('mobEdu.enter.view.pendingApprovals', {
    extend:'Ext.Panel',
    requires: [
         'mobEdu.enter.f',
         'mobEdu.enter.store.pendingApprovals'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:'vbox',
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>'
        },
        {
            flex:2,
            xtype:'list',
            disableSelection:true,
            itemTpl:new Ext.XTemplate('<table width="100%">' +
                '<tr><td><h2>{name}</h2></td><td align="right"><input type="checkbox" name="chkBox" id="chkBox_{name}" /></td></tr>' +
                '</table></td></tr>'+
                '</table>'
                ),
            store:mobEdu.util.getStore('mobEdu.enter.store.pendingApprovals'),
            singleSelect:true,
            loadingText: '',
             listeners:{
                itemtap:function (view, index, target, record, item, e, eOpts) {
                    mobEdu.enter.f.onApprovalItemTap(view, index, target, record, item, e, eOpts);
                }
            }
        },{
            xtype:'toolbar',
            docked:'bottom',
            layout:{
                pack:'right'
            },
            items:[            
            {
                text:'Approve',
                ui:'confirm',
                align:'right',
                handler:function () {
                    mobEdu.enter.f.doApprove();
                }
            }
            ]
        }
        ],
        flex:1
    }

});
