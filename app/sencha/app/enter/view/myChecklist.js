Ext.define('mobEdu.enter.view.myChecklist', {
    extend:'Ext.form.Panel',
    requires: [
    'mobEdu.enter.f'
    ],
    config:{
        padding:'0 10 0 0',
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>',
            items:[
            {
                ui:'light',
                xtype:'button',
                text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },
            // Back Button
            {
                xtype: 'img',
                cls: 'back',
                padding: '10 0 0 0',
                width: 32,
                height: 32,
                listeners: {
                    tap: function() {
                        mobEdu.enter.f.showeNterMenu();
                    }
                }
            }
            ]
        },
        {
            left:15,
            right:15,
            width:'90%',
            items:[
            {
                xtype:'label',
                html:'<center><h2>My Checklist</h2></center>',
                padding:'10 0 20 15'
            },
            {
            
                xtype:'label',
                html:'<h2>Forms</h2>',
                padding:'0 0 20 15'
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Information Tech</h3>',
                    flex:1
                },
                {
                    xtype:'label',
                    html:'<h3>Completed</h3>'
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'<h3>strech</h3>'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>1099</h3>',
                    flex:1
                },
                {
                    xtype:'label',
                    html:'<h3>Completed</h3>'
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Health Insurance</h3>',
                    flex:1
                },
                {
                    xtype:'button',
                    text:'<h3>Start</h3>',
                    handler:function () {
                        mobEdu.enter.f.showHealthInsuForm();
                    }
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Parking</h3>',
                    flex:1
                },
                {
                    xtype:'label',
                    html:'<h3>Completed</h3>'
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Security</h3>',
                    flex:1
                },
                {
                    xtype:'label',
                    html:'<h3>Completed</h3>'
                }
                ]
            },
            {
                xtype:'label',
                html:'<h2>Online Courses</h2>',
                padding:'0 10 20 5'
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Policies & Procedures</h3>',
                    flex:1
                },
                {
                    xtype:'label',
                    html:'<h3>Completed</h3>'
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Harrassment</h3>',
                    flex:1
                },
                {
                    xtype:'label',
                    html:'<h3>Completed</h3>'
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Technology</h3>',
                    flex:1
                },
                {
                    xtype:'button',
                    text:'<h3>Start</h3>'
                }
                ]
            },
            {
                xtype:'label',
                html:'<h2>Classes</h2>',
                padding:'0 10 20 5'
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Job Specific 1</h3>',
                    flex:1
                },
                {
                    xtype:'button',
                    text:'<h3>Start</h3>'
                }
                ]
            },
            {
                layout:{
                    type:'hbox',
                    align:'strech'
                },
                padding:'0 10 10 5',
                items:[
                {
                    xtype:'label',
                    html:'<h3>Job Specific 2</h3>',
                    flex:1
                },
                {
                    xtype:'button',
                    text:'<h3>Start</h3>'
                }
                ]
            }
            ]

        }
        ]
    }

});


