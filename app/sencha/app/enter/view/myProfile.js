Ext.define('mobEdu.enter.view.myProfile', {
    extend:'Ext.form.Panel',
    requires: [
    'mobEdu.enter.f'
    ],
    config:{
        padding:'0 0 0 0',
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>',
            items:[
            {
                ui:'light',
                xtype:'button',
                text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.showMenu();
                }
            },    
            // Back Button
            {
                xtype: 'img',
                cls: 'back',
                padding: '10 0 0 0',
                width: 32,
                height: 32,
                listeners: {
                    tap: function() {
                        mobEdu.enter.f.showeNterMenu();
                    }
                }
            }
            ]
        },
        {
            //            flex:2,
            html:'<h2>My Profile</h2>',
            id:'profileHeader',
            name:'profileHeader',
            padding:'10 0 20 5'
        },
        {
            autoScroll:true,
            padding:'0 0 0 5',
            items:[
            {
                xtype:'textfield',
                name:'profileName',
                labelWidth:'50%',
                label:'Name',
                id:'profileName',
                required:true,
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'homePh',
                labelWidth:'50%',
                label:'Home Ph.',
                id:'homePh',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'mobilePh',
                required:true,
                labelWidth:'50%',
                label:'Mobile Ph.',
                id:'mobilePh',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'email',
                labelWidth:'50%',
                label:'E-Mail',
                id:'email',
                required:true,
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'address',
                labelWidth:'50%',
                label:'Address',
                id:'address',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'city',
                labelWidth:'50%',
                label:'City',
                id:'city',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'state',
                labelWidth:'50%',
                label:'State',
                id:'state',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'postal',
                labelWidth:'50%',
                label:'Postal',
                id:'postal',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'emerConct',
                labelWidth:'50%',
                label:'Emergency Contact',
                id:'emerConct',
                margin:'1'
            },
            {
                xtype:'textfield',
                name:'phone',
                labelWidth:'50%',
                label:'Phone',
                id:'phone',
                margin:'1'
            }
            ]

        }, {
            xtype:'toolbar',
            docked:'bottom',
            ui:'light',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Upload Document',
                handler:function () {
                    mobEdu.enter.f.showCamera();
                }
            },
            //            
            {
                text:'Submit',
                handler:function () {
                    mobEdu.enter.f.submitProfile();
                }
            }
            ]
        }
        ]
    }

});


