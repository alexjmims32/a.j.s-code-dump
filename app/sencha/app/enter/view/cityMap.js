Ext.define("mobEdu.enter.view.cityMap", {
    extend: "Ext.Map",
    xtype: 'cityMap',
    id:'cityMap',
    config: {
        useCurrentLocation: false,
        listeners: {
            maprender : function(comp, map){
                new google.maps.Marker({
                position: new google.maps.LatLng('33.742278', '-84.410931'),
                map: map
            });
            Ext.getCmp('cityMap').setMapCenter({
                        latitude: 33.742278, 
                        longitude: -84.410931
                    });
            },
            centerchange: function(comp, map){
                new google.maps.Marker({
                position: new google.maps.LatLng('33.742278', '-84.410931'),
                map: map
            });
            }
        }
    }
});