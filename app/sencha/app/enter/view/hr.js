Ext.define('mobEdu.enter.view.hr', {
    extend:'Ext.Panel',
    requires: [
    'mobEdu.enter.f'
    ],
    config:{
        scrollable:'vertical',
        fullscreen:true,
        layout:{
            type:'vbox'
        },
        cls:'logo',
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>eNter</h1>',
            items:[
            {
                xtype:'button',
                cls:'back',
                height: 32,
                width: 32,                
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enter.f.showeNterMenu();
                }
            },    
            {
                xtype:'spacer'  
            },    
            // Home Button
            {
                ui:'light',
                xtype:'button',
                cls:'home',
                height: 32,
                width: 32,                
                style:'background:none;border:none;',
                iconMask:true,
                handler:function () {
                    mobEdu.util.isHomeTapped=true;
                    mobEdu.util.showMainView();
                }
            }
            ]
        },
        {
            xtype:'label',
            centered:true,
            html:'<p>HR WEB SITE HERE</p>'
        }
        ]
    }

});