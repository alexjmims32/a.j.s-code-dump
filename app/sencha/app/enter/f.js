Ext.define('mobEdu.enter.f', {
    requires: [
        'mobEdu.enter.store.enterMenu',
        'mobEdu.enter.store.detailedProfile',
        'mobEdu.enter.store.pendingApprovals'
    ],
    statics: {

        loadMainMenu: function() {
            var store = mobEdu.util.getStore('mobEdu.enter.store.enterMenu');
            //            store.load();
            store.removeAll();
            store.removed = [];
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/mystatus.png',
                title: '<h2>My Status</h2>',
                action: mobEdu.enter.f.showMyStatus
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/profile.png',
                title: '<h2>My Profile</h2>',
                action: mobEdu.enter.f.loadProfile
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/chklist.png',
                title: '<h2>My Checklist</h2>',
                action: mobEdu.enter.f.showMyChecklist
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/hr.png',
                title: '<h2>HR</h2>',
                action: mobEdu.enter.f.showHRView
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/classroom.png',
                title: '<h2>Class Room</h2>',
                action: mobEdu.enter.f.showMobileClassRoom
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/myfiles.png',
                title: '<h2>My Files</h2>',
                action: mobEdu.enter.f.showMyFiles
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/maps.png',
                title: '<h2>Map</h2>',
                action: mobEdu.enter.f.showCampusMap
            });
            store.sync();

            store.add({
                img: mobEdu.util.getResourcePath() + 'images/enter/parking.png',
                title: '<h2>Parking</h2>',
                action: mobEdu.enter.f.showParking
            });
            store.sync();

            //            store.add({
            //                img:mobEdu.util.getResourcePath() + 'images/enter/dir.png',
            //                title:'Directory',
            //                action: mobEdu.enter.f.showDirectory
            //            });
            //            store.sync();                
            mobEdu.enter.f.showeNterMenu();
        },

        loadPendingApprovals: function() {
            mobEdu.enter.f.showPendingApprovals();
        },
        onApprovalItemTap: function(view, index, target, record, item, e, eOpts) {
            setTimeout(function() {
                view.deselect(index);
            }, 500);
            if (item.target.name == "chkBox") {

            } else {
                var detailStore = mobEdu.util.getStore('mobEdu.enter.store.detailedProfile');
                //                detailStore.load();
                detailStore.removeAll();
                detailStore.removed = [];
                detailStore.add(record.copy());
                detailStore.sync();
                mobEdu.enter.f.showDetailedProfile();
            }
        },
        doApprove: function() {
            Ext.Msg.show({
                title: null,
                message: 'Approved successfully.',
                buttons: Ext.MessageBox.OK,
                cls: 'msgbox',
                fn: function(btn) {
                    if (btn == 'ok') {
                        var store = mobEdu.util.getStore('mobEdu.enter.store.pendingApprovals');
                        if (store.data.length > 0) {
                            for (var i = 0; i < store.data.length; i++) {
                                var rec = store.data.items[i].data;
                                var record = store.findRecord('username', rec.username);
                                var chkBox = document.getElementById('chkBox_' + rec.name);
                                if (chkBox.checked) {
                                    record.set('approveFlag', 1);
                                    //                                        record.dirty = true;
                                    store.sync();
                                }
                            }
                        }

                    }
                }
            });
        },

        loadProfile: function() {
            var store = mobEdu.util.getStore('mobEdu.enter.store.pendingApprovals');
            var record = store.findRecord('username', username);
            //           var record=store.getAt(0);
            if (record != undefined) {
                mobEdu.util.get('mobEdu.enter.view.myProfile');
                if (record.data.approveFlag == 1) {
                    Ext.getCmp('profileHeader').setHtml('<b>My Profile</b> &nbsp; : &nbsp; Profile Approved');
                } else {
                    Ext.getCmp('profileHeader').setHtml('<h1><b>My Profile</b></h1>');
                }
                Ext.getCmp('profileName').setValue(record.data.name);
                Ext.getCmp('homePh').setValue(record.data.hPhone);
                Ext.getCmp('mobilePh').setValue(record.data.mPhone);
                Ext.getCmp('email').setValue(record.data.email);
                Ext.getCmp('address').setValue(record.data.address);
                Ext.getCmp('city').setValue(record.data.city);
                Ext.getCmp('state').setValue(record.data.state);
                Ext.getCmp('postal').setValue(record.data.postal);
                Ext.getCmp('phone').setValue(record.data.ePhone);
                Ext.getCmp('emerConct').setValue(record.data.eContact);
            }
            mobEdu.enter.f.showMyProfile();
        },

        submitProfile: function() {
            var name = Ext.getCmp('profileName').getValue();
            var phone = Ext.getCmp('mobilePh').getValue();
            var email = Ext.getCmp('email').getValue();
            if (name == null || name == '' || phone == null || phone == '' || email == null || email == '') {
                Ext.Msg.show({
                    message: 'Please provide mandatory fields.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                var store = mobEdu.util.getStore('mobEdu.enter.store.pendingApprovals');
                if (!(store.findRecord('username', username))) {
                    if (username == 'smith') {
                        store.add({
                            name: 'J. R. Smith',
                            username: 'smith',
                            img: 'http://www.learn-xsl-fo-tutorial.com/Images/WinnieThePooh.gif',
                            hPhone: '404-111-2222',
                            mPhone: '404-222-3333',
                            email: 'jrs@myemail.com',
                            address: '12 Main St',
                            city: 'Atlanta',
                            state: 'Georgia',
                            postal: '30001',
                            eContact: 'A. A. Smith',
                            ePhone: '404-444-5555',
                            approveFlag: 0
                        });
                        store.sync();
                    } else if (username == 'john') {
                        store.add({
                            name: 'A. John',
                            username: 'john',
                            img: 'http://www.fas.harvard.edu/~hpcws/august28,1980.jpg',
                            hPhone: '404-485-2222',
                            mPhone: '404-222-9658',
                            email: 'john@myemail.com',
                            address: '18 Main St',
                            city: 'Atlanta',
                            state: 'Georgia',
                            postal: '30001',
                            eContact: 'A. A. Smith',
                            ePhone: '404-444-5555',
                            approveFlag: 0
                        });
                        store.sync();
                    } else if (username == 'williams') {
                        store.add({
                            name: 'Williams',
                            username: 'williams',
                            img: 'http://www.tobaccofreedom.org/issues/documents/lebanon/p1.gif',
                            hPhone: '658-111-2582',
                            mPhone: '425-222-3695',
                            email: 'williams@myemail.com',
                            address: '19 Main St',
                            city: 'Atlanta',
                            state: 'Georgia',
                            postal: '30001',
                            eContact: 'Peter',
                            ePhone: '469-424-5512',
                            approveFlag: 0
                        });
                        store.sync();
                    }
                }
                Ext.Msg.show({
                    id: 'success',
                    name: 'success',
                    title: '',
                    cls: 'msgbox',
                    message: 'Profile saved successfully!',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            //                                mobEdu.util.showMainView();
                            mobEdu.enter.f.showeNterMenu();
                        }
                    }
                });
            }
        },

        showMyStatus: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.myStatus');
        },
        showMyProfile: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.myProfile');
        },
        showMyChecklist: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.myChecklist');
        },
        showHealthInsuForm: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showMyChecklist);
            mobEdu.util.show('mobEdu.enter.view.healthInsu');
        },
        showHRView: function() {
            //            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            //            mobEdu.util.show('mobEdu.enter.view.hr');
            mobEdu.util.loadExternalUrl('http://www.bc.edu/offices/hr/');
        },
        showMobileClassRoom: function() {
            //            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            //            mobEdu.util.show('mobEdu.enter.view.mobileClsRoom');
            mobEdu.util.loadExternalUrl('http://www.spsu.edu/d2l/');
        },
        showMyFiles: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.myFiles');
        },
        showCampusMap: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.campusMap');
        },
        showParking: function() {
            //            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            //            mobEdu.util.show('mobEdu.enter.view.parking');
            mobEdu.util.loadExternalUrl('http://www.knox.edu/offices-and-services/campus-safety/about-the-department/parking-information.html');

        },
        showDirectory: function() {

            mobEdu.util.show('mobEdu.enter.view.directory');
        },
        showCamera: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.capturePic');
        },
        showeNterMenu: function() {
            var title = mobEdu.util.getTitleInfo('ENTER');
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.enter.view.eNterMenu');
            Ext.getCmp('enterTitle').setTitle('<h1>' + title + '</h1>');
        },
        showPendingApprovals: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showeNterMenu);
            mobEdu.util.show('mobEdu.enter.view.pendingApprovals');
        },
        showDetailedProfile: function() {
            mobEdu.util.updatePrevView(mobEdu.enter.f.showPendingApprovals);
            mobEdu.util.show('mobEdu.enter.view.detailedProfile');
        }
    }
});