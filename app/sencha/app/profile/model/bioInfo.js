Ext.define('mobEdu.profile.model.bioInfo', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
                name: 'addCode',
                mapping: 'ADDRESS_CODE'
            }, {
                name: 'addDesc',
                mapping: 'ADDRESS_DESCRIPTION'
            }, {
                name: 'recType',
                mapping: 'RECORD_TYPE'
            }, {
                name: 'action',
                mapping: 'ACTION'
            }, {
                name: 'seqno',
                mapping: 'SEQ_NO'
            }, {
                name: 'delAddr'
                    // mapping :'SEQ_NO'
            }, {
                name: 'status',
                mapping: 'STATUS'
            }, {
                name: 'rowId',
                mapping: 'EROWID'
            }, {
                name: 'teleCode',
                mapping: 'TELE_CODE'
            }, {
                name: 'teleDesc',
                mapping: 'TELE_DESC'
            }, {
                name: 'term',
                mapping: 'TERM'
            }, {
                name: 'termDesc',
                mapping: 'TERM_DESC'
            }, {
                name: 'handBookText',
                mapping: 'HB_TEXT'
            }, {
                name: 'email',
                mapping: 'EMAIL_ADDRESS'
            }, {
                name: 'phArea',
                mapping: 'PHONE_AREA'
            }, {
                name: 'phNum',
                mapping: 'PHONE_NUM'
            }, {
                name: 'phIntAccess',
                mapping: 'PHONE_INTL_ACCESS'
            }, {
                name: 'unlist',
                mapping: 'UNLIST_IND'
            }, {
                name: 'fromDate',
                mapping: 'FROM_DATE'
            }, {
                name: 'action',
                mapping: 'ACTION'
            }, {
                name: 'toDate',
                mapping: 'TO_DATE'
            }, {
                name: 'str1',
                mapping: 'STREET_LINE1'
            }, {
                name: 'str2',
                mapping: 'STREET_LINE2'
            }, {
                name: 'str3',
                mapping: 'STREET_LINE3'
            }, {
                name: 'city',
                mapping: 'CITY'
            }, {
                name: 'stateCode',
                mapping: 'STATE_CODE'
            }, {
                name: 'stateDesc',
                mapping: 'STATE_DESC'
            }, {
                name: 'zip',
                mapping: 'ZIP_CODE'
            }, {
                name: 'countyCode',
                mapping: 'COUNTY_CODE'
            }, {
                name: 'countyDesc',
                mapping: 'COUNTY_DESC'
            }, {
                name: 'nationCode',
                mapping: 'NATION_CODE'
            }, {
                name: 'nationDesc',
                mapping: 'NATION_DESC'
            }, {
                name: 'primaryArea',
                mapping: 'PRIMARY_PHONE_AREA'
            }, {
                name: 'primaryPhNo',
                mapping: 'PRIMARY_PHONE_NUM'
            }, {
                name: 'primaryPhInt',
                mapping: 'PRIMARY_PHONE_INTL_ACCESS'
            }, {
                name: 'primaryUnlist',
                mapping: 'PRIMARY_UNLIST_IND'
            }, {
                name: 'firstName',
                mapping: 'FIRST_NAME'
            }, {
                name: 'middleName',
                mapping: 'MIDDLE_NAME'
            }, {
                name: 'lastName',
                mapping: 'LAST_NAME'
            }, {
                name: 'relationCode',
                mapping: 'RELATION_CODE'
            }, {
                name: 'relationDesc',
                mapping: 'RELATION_DESC'
            }, {
                name: 'relationOrder',
                mapping: 'RELATION_ORDER'
            }, {
                name: 'phExt',
                mapping: 'PHONE_EXTN'
            }

            // {
            //     name : 'payments',
            //     mapping :'PAYMENT' ,
            //     convert: function(value, record) { 
            //         if(value==0 || value=='' || value==null){
            //             return '';
            //         }
            //         value = parseFloat(value);
            //         value = value.toFixed(2);
            //         return value;
            //     }
            // },
        ]
    }

});