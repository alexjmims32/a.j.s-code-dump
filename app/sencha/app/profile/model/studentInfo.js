Ext.define('mobEdu.profile.model.studentInfo',{
   extend:'Ext.data.Model',

   config:{
       fields:[
           {
               name : 'studentId',
               type : 'string'
           },
           {
               name : 'studentInf',
               type : 'string'
           },
           {
               name : 'fromTerm',
               type : 'string'
           },
           {
               name : 'toTerm',
               type : 'string'
           },
           {
               name : 'isRegistered',
               type : 'string'
           },
           {
               name : 'firstTerm',
               type : 'string'
           },
           {
               name : 'lastTerm',
               type : 'string'
           },
           {
               name : 'studentStatus',
               type : 'string'
           },
           {
               name : 'termCodeMatric',
               type : 'string'
           },
           {
               name : 'residence',
               type : 'string'
           },
           {
               name : 'citizen',
               type : 'string'
           },
           {
               name : 'studentType',
               type : 'string'
           },
           {
               name:'expGradeDate',
               type:'string'
           }

       ]
   }

});