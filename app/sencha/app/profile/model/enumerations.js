Ext.define('mobEdu.profile.model.enumerations', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'status',
            type: 'string'
        }, {
            name: 'value',
            type: 'string',
            mapping: 'enum_value'
        }, {
            name: 'description',
            type: 'string',
            mapping: 'enum_desc'
        }]
    }
});