Ext.define('mobEdu.profile.model.studentProfile', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'title'
        }, {
            name: 'action',
            type: 'function'
        }, {
            name: 'status',
            type: 'string'
        }, {
            name: 'indicator',
            type: 'string'
        }]
    }
});