Ext.define('mobEdu.profile.f', {
	requires: [
		'mobEdu.profile.store.myInfo',
		'mobEdu.profile.store.studentProfile',
		'mobEdu.profile.store.directoryOpt',
		'mobEdu.profile.store.bioInfo',
		'mobEdu.profile.store.updateInfo',
		'mobEdu.profile.store.updateLocal',
		'mobEdu.profile.store.confirmInfo',
		'mobEdu.profile.store.stateEnum',
		'mobEdu.profile.store.countyEnum',
		'mobEdu.profile.store.nationEnum',
		'mobEdu.profile.store.relationEnum'
	],
	statics: {
		showStudentProfileView: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.profile.view.studentProfile');
			mobEdu.util.setTitle('PROFILE', Ext.getCmp('studentPTitle'));
		},
		showMyInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.profile.view.myInfo');
		},
		initializeProfileView: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.studentProfile');
			store.removeAll();
			var role = mobEdu.util.getRole();
			store.add({
				title: 'My Info',
				action: mobEdu.profile.f.loadMyInfo
			});
			store.sync();
			if (role.match(/student/gi)) {
				store.add({
					title: 'Curriculum Info',
					action: mobEdu.crl.f.loadCurriculumInfo
				});
				store.sync();

				store.add({
					title: 'Credit Hours Info',
					action: mobEdu.crl.f.loadBillingInfo
				});
				store.sync();

				store.add({
					title: 'Student Info',
					action: mobEdu.profile.f.loadStudentInfo
				});
				store.sync();
			}
			if (showDirOptOut === 'true') {
				store.add({
					title: 'Directory Listing Opt-Out (FERPA)',
					action: mobEdu.profile.f.loadDirOptOut
				});
				store.sync();
			}
		},

		loadBioInfo: function() {
			var sinfoStore = mobEdu.util.getStore('mobEdu.profile.store.bioInfo');
			if (sinfoStore.data.length > 0) {
				sinfoStore.removeAll();
				// sinfoStore.removed=[];
				sinfoStore.sync();
			}
			sinfoStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			sinfoStore.getProxy().setUrl(webserver + 'bioContact');
			sinfoStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				role: 'STUDENT'
			});

			sinfoStore.getProxy().afterRequest = function() {
					mobEdu.profile.f.loadBioInfoResposeHandler();
				},
				sinfoStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'biocontact');
		},
		loadBioInfoResposeHandler: function() {
			// mobEdu.util.get('mobEdu.profile.view.studentInfo');
			//            mobEdu.util.setTitle(null,Ext.getCmp('studentTitleBar'),'Student Info');
			mobEdu.profile.f.roleFlag = 'STUDENT'
			mobEdu.profile.f.showBioInfo();
		},

		loadDirOptOut: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			store.getProxy().setUrl(webserver + 'getDirOptInd');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				action: 'GET'
			});
			store.getProxy().afterRequest = function() {
				mobEdu.profile.f.checkOptOutResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getDirOptInd');
		},

		checkOptOutResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
			var storeData = store.data.all[0];
			mobEdu.profile.f.showDirectoryOptOut();
			if (storeData.data.indicator === 'Y') {
				Ext.getCmp('checkIndicator').setHtml('<table><tr><td><h3 style="padding-top:15px;align:center;"><b>' + 'You have already signed up for OPTING-OUT from the Institute sharing my directory information. This will be for the duration of your matriculation or until you request to reverse non-disclosure in writing to the Registrar’s Office.' + '</b></h3></td></tr></table><br />');
				Ext.getCmp('optOutFieldset').hide();
			} else {
				Ext.getCmp('checkIndicator').setHtml('<table><tr><td><h3>' + 'Check the box below if you do NOT want your directory information disclosed (opt-out) according to the policy at <a href="javascript:onclick = [mobEdu.profile.f.showFerpaLink()]">http://www.wit.edu/ssc/registration/ferpa.html</a><br><br>' + '</h3></td></tr></table><br />');
				Ext.getCmp('optOutFieldset').show();
			}
		},

		submitOptOut: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
			store.getProxy().setUrl(webserver + 'getDirOptInd');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				action: 'SUBMIT'
			});
			store.getProxy().afterRequest = function() {
				mobEdu.profile.f.submitOptOutResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getDirOptInd');
		},
		submitOptOutResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
			var storeData = store.data.all[0];
			if (storeData.data.status === 'SUCCESS') {
				Ext.Msg.show({
					message: '<p>You have successfully signed up for OPTING-OUT from the Institute sharing your directory information.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn, value) {
						if (btn == 'ok') {
							mobEdu.util.showMainView();
						}
					}
				});
			} else {
				Ext.Msg.show({
					message: storeData.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showDirectoryOptOut: function() {
			mobEdu.util.show('mobEdu.profile.view.directoryOptOut');
			Ext.getCmp('dirOptcheck').uncheck();
			Ext.getCmp('dirOptBot').hide();
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
		},

		loadMyInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
			store.getProxy().setUrl(webserver + 'getContact');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.profile.f.myInfoResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'contact');
		},
		myInfoResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.myInfo');
			if (store.data.length > 0) {
				var record = store.getAt(0);
				var title = record.get('title');
				if (title == null || title == '') {
					title = record.get('fullName');
				}
			}
			mobEdu.util.get('mobEdu.profile.view.myInfo');
			if (title == undefined) {
				mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), 'My Info');
			} else {
				mobEdu.util.setTitle(null, Ext.getCmp('myInfoTitle'), title);
			}
			mobEdu.profile.f.showMyInfo();

		},
		loadStudentInfo: function() {
			var sinfoStore = mobEdu.util.getStore('mobEdu.profile.store.studentInfo');
			sinfoStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			sinfoStore.getProxy().setUrl(webserver + 'myStudentInfo');
			sinfoStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});

			sinfoStore.getProxy().afterRequest = function() {
					mobEdu.profile.f.studentInfoResposeHandler();
				},
				sinfoStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'studentinfo');
		},
		studentInfoResposeHandler: function() {
			mobEdu.util.get('mobEdu.profile.view.studentInfo');
			mobEdu.util.setTitle(null, Ext.getCmp('studentTitleBar'), 'Student Info');
			mobEdu.profile.f.showStudentInfo();
		},
		isLastTermExists: function(lastTerm) {
			if (lastTerm === '' || lastTerm === 'NA')
				return false;
			else
				return true;
		},
		isTermMatricExists: function(termCodeMatric) {
			if (termCodeMatric === '' || termCodeMatric === 'NA')
				return false;
			else
				return true;
		},
		isExpGradExists: function(expGradeDate) {
			if (expGradeDate === '' || expGradeDate === 'NA')
				return false;
			else
				return true;
		},
		showStudentInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.profile.view.studentInfo');
		},
		showFerpaLink: function() {
			var link = 'http://www.wit.edu/ssc/registration/ferpa.html';
			mobEdu.util.loadExternalUrl(link);
		},
		showBioInfo: function() {
			// var store = mobEdu.util.getStore('mobEdu.profile.store.countyEnum');
			// if (store.data.length > 0) {
			// 	store.removeAll();
			// 	store.removed = [];
			// 	store.sync();
			// }
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			// if (mobEdu.profile.f.roleFlag == 'STUDENT') {
			mobEdu.util.show('mobEdu.profile.view.bioInfo');
			// } else {
			// 	mobEdu.util.show('mobEdu.profile.view.employeeInfo');
			// }
			// mobEdu.profile.f.resetInfo();
		},
		isExists: function(str) {
			if (str === '' || str === 'NA' || str === undefined) {
				return false;
			} else {
				return true;
			}
		},
		isConfirmed: function(status) {
			if (status === 'Confirmed') {
				return true;
			} else {
				return false;
			}
		},

		isNotCellular: function(str) {
			if (str === '' || str === 'NA' || str === undefined || str === 'Cellular') {
				return false;
			} else {
				return true;
			}
		},

		toConfirm: function(status, action) {
			if (status === 'Confirm') {
				return true;
			} else {
				if (action.match(/Confirm/gi)) {
					return true;
				} else {
					return false;
				}
			}
		},
		doUpdate: function(action) {
			if (action.match(/update/gi)) {
				return true;
			} else {
				return false;
			}
		},
		isUnlistExists: function(str) {
			if (str === '' || str === 'N' || str === 'NA' || str === undefined) {
				return false;
			} else {
				return true;
			}
		},
		isRecTypeMatches: function(str) {
			if (str === 'OTHER_PHONE') {
				return true;
			} else {
				return false;
			}
		},
		onUpOrConfirmTap: function(view, index, target, record, item, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			if (item.target.name == "update") {
				mobEdu.profile.f.loadUpdateInfo(record);
			} else if (item.target.name == "confirm") {
				item.target.src = mobEdu.util.getResourcePath() + "images/confirmed.png";
				mobEdu.profile.f.loadConfirmInfo(record, item);
			}
		},
		loadEnumerations: function() {
			var enumType = 'STATE';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.profile.store.stateEnum');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(webserver + 'getEnums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.profile.f.loadEnumerationsResponseHandler();
			};
			enuStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'enums');
		},

		loadEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.profile.store.stateEnum');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			var index = 1;
			var enumList = new Array();
			if (enumStatus.length === 0) {
				Ext.Msg.show({
					id: 'stateEnum',
					title: null,
					message: 'Error in retrieving State List',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				var store = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
				var record1 = store.data.all;
				var stateDes = record1[0].data.stateDesc;
				if (stateDes === '' || stateDes === 'NA' || stateDes === undefined) {
					if (enuStore.data.length > 0) {
						enumList[0] = ({
							description: 'Select',
							value: '0'
						});
						enuStore.data.each(function(record) {
							var enumRec = enuStore.data.items[index - 1].data;
							enumList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						Ext.getCmp('upState').suspendEvents();
						Ext.getCmp('upState').setOptions(enumList);
						Ext.getCmp('upState').setValue('0');
						Ext.getCmp('upState').resumeEvents();
					}
				} else {
					var stateValue = record1[0].data.stateCode;
					Ext.getCmp('upState').suspendEvents();
					Ext.getCmp('upState').setValue(stateValue);
					Ext.getCmp('upState').resumeEvents();
				}
			}
		},

		loadRelEnumerations: function() {
			var enumType = 'RELT';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.profile.store.relationEnum');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(webserver + 'getEnums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.profile.f.loadRelEnumerationsResponseHandler();
			};
			enuStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'enums');
		},

		loadRelEnumerationsResponseHandler: function() {
			var relStore = mobEdu.util.getStore('mobEdu.profile.store.relationEnum');
			var enumStatus = relStore.getProxy().getReader().rawData;
			var index = 1;
			var RelList = new Array();
			if (enumStatus.length === 0) {
				Ext.Msg.show({
					id: 'relEnum',
					title: null,
					message: 'Error in retrieving State List',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				var store = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
				var record1 = store.data.all;
				var relDes = record1[0].data.relationDesc;
				if (relDes === '' || relDes === 'NA' || relDes === undefined) {
					if (relStore.data.length > 0) {
						RelList[0] = ({
							description: 'Select',
							value: '0'
						});
						relStore.data.each(function(record) {
							var enumRec = relStore.data.items[index - 1].data;
							RelList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						Ext.getCmp('relation').setOptions(RelList);
						Ext.getCmp('relation').setValue('0');
					}
				} else {
					if (relStore.data.length > 0) {
						RelList[0] = ({
							description: 'Select',
							value: '0'
						});
						relStore.data.each(function(record) {
							var enumRec = relStore.data.items[index - 1].data;
							RelList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						var relationValue = record1[0].data.relationCode;
						Ext.getCmp('relation').setOptions(RelList);
						Ext.getCmp('relation').setValue(relationValue);
					}
				}
			}
		},

		loadNationEnumerations: function() {
			var enumType = 'NATN';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.profile.store.nationEnum');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(webserver + 'getEnums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.profile.f.loadNationEnumerationsResponseHandler();
			};
			enuStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'enums');
		},

		loadNationEnumerationsResponseHandler: function() {
			var nationStore = mobEdu.util.getStore('mobEdu.profile.store.nationEnum');
			var enumStatus = nationStore.getProxy().getReader().rawData;
			var index = 1;
			var nationList = new Array();
			if (enumStatus.length === 0) {
				Ext.Msg.show({
					id: 'nationEnum',
					title: null,
					message: 'Error in retrieving Nation List',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				var store = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
				var record1 = store.data.all;
				var nationDes = record1[0].data.nationDesc;
				if (nationDes === '' || nationDes === 'NA' || nationDes === undefined) {
					if (nationStore.data.length > 0) {
						nationList[0] = ({
							description: 'Select',
							value: '0'
						});
						nationStore.data.each(function(record) {
							var enumRec = nationStore.data.items[index - 1].data;
							nationList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						Ext.getCmp('nation').setOptions(nationList);
						Ext.getCmp('nation').setValue('0');
					}
				} else {
					if (nationStore.data.length > 0) {
						nationList[0] = ({
							description: 'Select',
							value: '0'
						});
						nationStore.data.each(function(record) {
							var enumRec = nationStore.data.items[index - 1].data;
							nationList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						var nationValue = record1[0].data.nationCode;
						Ext.getCmp('nation').setOptions(nationList);
						Ext.getCmp('nation').setValue(nationValue);
					}
				}
			}
		},
		loadCountyEnumerations: function(value) {
			var enumType = 'CNTY';
			var stateValue = value;
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.profile.store.countyEnum');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(webserver + 'getCountyList');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				stateCode: stateValue
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.profile.f.loadCountyEnumerationsResponseHandler(value);
			};
			enuStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'enums');
		},

		loadCountyEnumerationsResponseHandler: function(value) {
			var countyStore = mobEdu.util.getStore('mobEdu.profile.store.countyEnum');
			var enumStatus = countyStore.getProxy().getReader().rawData;
			var index = 1;
			var countyList = new Array();
			if (enumStatus.length === 0) {
				Ext.Msg.show({
					id: 'countyEnum',
					title: null,
					message: 'Error in retrieving County List',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				var store = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
				var record1 = store.data.all;
				var countyDes = record1[0].data.countyDesc;
				if (countyDes === '' || countyDes === 'NA' || countyDes === undefined) {
					countyDes = value;
				}
				if (countyDes === '' || countyDes === 'NA' || countyDes === undefined) {
					if (countyStore.data.length > 0) {
						countyList[0] = ({
							description: 'Select',
							value: '0'
						});
						countyStore.data.each(function(record) {
							var enumRec = countyStore.data.items[index - 1].data;
							countyList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						Ext.getCmp('upCounty').suspendEvents();
						Ext.getCmp('upCounty').setOptions(countyList);
						Ext.getCmp('upCounty').setValue('0');
						Ext.getCmp('upCounty').resumeEvents();
					}
				} else {
					if (countyStore.data.length > 0) {
						countyList[0] = ({
							description: 'Select',
							value: '0'
						});
						countyStore.data.each(function(record) {
							var enumRec = countyStore.data.items[index - 1].data;
							countyList[index] = ({
								description: enumRec.description,
								value: enumRec.value
							});
							index = index + 1;
						});
						var countyValue = record1[0].data.countyCode;
						Ext.getCmp('upCounty').suspendEvents();
						Ext.getCmp('upCounty').setOptions(countyList);
						Ext.getCmp('upCounty').setValue(countyValue);
						Ext.getCmp('upCounty').resumeEvents();
					}
				}
			}
		},
		loadUpdateInfo: function(loadRecord) {
			mobEdu.profile.f.loadEnumerations();
			mobEdu.profile.f.loadRelEnumerations();
			mobEdu.profile.f.loadNationEnumerations();
			// mobEdu.profile.f.loadCountyEnumerations();
			// mobEdu.profile.f.loadPhCodeEnumerations();
			mobEdu.util.updatePrevView(mobEdu.profile.f.showBioInfo);
			mobEdu.util.show('mobEdu.profile.view.updateInfo');
			Ext.getCmp('updateHead').setTitle(loadRecord.data.addDesc);
			// if (mobEdu.util.getRole().match(/student/gi)) {
			var recStore = mobEdu.util.getStore('mobEdu.profile.store.bioInfo');
			// } else {
			// 	var recStore = mobEdu.util.getStore('mobEdu.profile.store.eBioInfo');
			// }
			// var phIndex = 1;

			var store = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
			store.removeAll();
			store.add(loadRecord);
			store.sync();
			if (loadRecord.data.addCode === 'EMERG') {
				Ext.getCmp('phExt').setValue(loadRecord.data.phExt);
				// Ext.getCmp('delAdd').setLabel('Remove Contact');
			}
			// else if (loadRecord.data.addCode === 'CL') {
			// 	Ext.getCmp('delAdd').setLabel('Delete');
			// } else {
			// 	Ext.getCmp('delAdd').setLabel('Delete Address');
			// }
			if (loadRecord.data.addCode !== 'SC') {
				Ext.getCmp('phNum').setRequired(false);
			} else {
				Ext.getCmp('phNum').setRequired(true);
			}

			var addCode = loadRecord.data.addCode;
			if (addCode === 'SC') {
				Ext.getCmp('updateHead').show();
				Ext.getCmp('updateSection').hide();
				Ext.getCmp('emrgSection').hide();
				Ext.getCmp('emailSection').hide();
				Ext.getCmp('sameAsPermanent').hide();
				// Ext.getCmp('otherHead').hide();
				Ext.getCmp('primaryHead').hide();
				// Ext.getCmp('unList').show();
				Ext.getCmp('phExt').hide();
				Ext.getCmp('intAcCode').show();
				Ext.getCmp('primarySection').show();
				Ext.getCmp('otherSection1').hide();
				// Ext.getCmp('otherSection2').hide();
				// Ext.getCmp('otherSection3').hide();
				// Ext.getCmp('otherSection4').hide();
				// Ext.getCmp('otherSection5').hide();
				// Ext.getCmp('upCounty').hide();
			} else if (addCode === 'P') {
				Ext.getCmp('emrgSection').hide();
				Ext.getCmp('updateHead').show();
				Ext.getCmp('updateSection').hide();
				Ext.getCmp('sameAsPermanent').hide();
				// Ext.getCmp('updateSection').show();
				Ext.getCmp('otherHead').hide();
				Ext.getCmp('primaryHead').hide();
				Ext.getCmp('emailSection').show();
				// Ext.getCmp('unList').hide();
				Ext.getCmp('phExt').hide();
				Ext.getCmp('intAcCode').hide();
				Ext.getCmp('primarySection').hide();
				// Ext.getCmp('otherSection1').hide();
				// Ext.getCmp('otherSection2').hide();
				// Ext.getCmp('otherSection3').hide();
				// Ext.getCmp('otherSection4').hide();
				// Ext.getCmp('otherSection5').hide();
				// Ext.getCmp('upCounty').hide();
			} else if (addCode === 'EMERG') {
				Ext.getCmp('emrgSection').show();
				Ext.getCmp('updateHead').show();
				Ext.getCmp('updateSection').hide();
				// Ext.getCmp('updateSection').show();
				Ext.getCmp('otherHead').hide();
				Ext.getCmp('primaryHead').hide();
				Ext.getCmp('emailSection').hide();
				// Ext.getCmp('unList').hide();
				Ext.getCmp('phExt').show();
				Ext.getCmp('intAcCode').hide();
				Ext.getCmp('primarySection').show();
				Ext.getCmp('otherSection1').hide();
				Ext.getCmp('sameAsPermanent').hide();
				// Ext.getCmp('otherSection2').hide();
				// Ext.getCmp('otherSection3').hide();
				// Ext.getCmp('otherSection4').hide();
				// Ext.getCmp('otherSection5').hide();
				// Ext.getCmp('upCounty').hide();
			} else if (addCode === '1P') {
				Ext.getCmp('emrgSection').hide();
				Ext.getCmp('updateHead').show();
				Ext.getCmp('updateSection').show();
				Ext.getCmp('sameAsPermanent').hide();
				// Ext.getCmp('otherHead').show();
				// Ext.getCmp('unList').show();
				Ext.getCmp('emailSection').hide();
				Ext.getCmp('phExt').hide();
				Ext.getCmp('intAcCode').hide();
				Ext.getCmp('primaryHead').hide();
				Ext.getCmp('primarySection').hide();
				// Ext.getCmp('otherSection1').show();
				// Ext.getCmp('otherSection2').show();
				// Ext.getCmp('otherSection3').show();
				// Ext.getCmp('otherSection4').show();
				// Ext.getCmp('otherSection5').show();
				// Ext.getCmp('upCounty').show();
			} else {
				Ext.getCmp('emrgSection').hide();
				Ext.getCmp('updateHead').show();
				Ext.getCmp('updateSection').show();
				Ext.getCmp('sameAsPermanent').show();
				Ext.getCmp('copyAdress').uncheck();
				// Ext.getCmp('otherHead').show();
				// Ext.getCmp('unList').show();
				Ext.getCmp('emailSection').hide();
				Ext.getCmp('phExt').hide();
				Ext.getCmp('intAcCode').hide();
				Ext.getCmp('primaryHead').hide();
				Ext.getCmp('primarySection').hide();
			}
			// if (loadRecord.data.addCode !== 'CL' && loadRecord.data.addCode !== 'EMERG') {
			// 	var unlistValue = '';
			// 	if (loadRecord.data.primaryUnlist === 'Y') {
			// 		unlistValue = true;
			// 	} else {
			// 		unlistValue = false;
			// 	}
			// }
			Ext.getCmp('order').setValue(loadRecord.data.relationOrder);
			Ext.getCmp('relation').setValue(loadRecord.data.relationDesc);
			var emrFirst = loadRecord.data.firstName;
			if (emrFirst !== undefined) {
				if (emrFirst.match(/NEW CONTACT/gi)) {
					emrFirst = ''
				}
			}
			Ext.getCmp('firstname').setValue(emrFirst);
			Ext.getCmp('midName').setValue(loadRecord.data.middleName);
			Ext.getCmp('lastname').setValue(loadRecord.data.lastName);
			Ext.getCmp('emailAddr').setValue(loadRecord.data.email);
			Ext.getCmp('address1').setValue(loadRecord.data.str1);
			Ext.getCmp('address2').setValue(loadRecord.data.str2);
			Ext.getCmp('address3').setValue(loadRecord.data.str3);
			Ext.getCmp('city').setValue(loadRecord.data.city);
			Ext.getCmp('upState').setValue(loadRecord.data.state);
			Ext.getCmp('upZip').setValue(loadRecord.data.zip);
			Ext.getCmp('upCounty').setValue(loadRecord.data.county);
			Ext.getCmp('nation').setValue(loadRecord.data.nation);
			if (loadRecord.data.addCode === 'EMERG' || loadRecord.data.addCode === 'SC') {
				Ext.getCmp('areaCode').setValue(loadRecord.data.phArea);
				Ext.getCmp('phNum').setValue(loadRecord.data.phNum);
				Ext.getCmp('intAcCode').setValue(loadRecord.data.phIntAccess);
				// Ext.getCmp('unList').setChecked(unlistValue);
			}
		},

		loadLocalAddress: function() {
			var recStore = mobEdu.util.getStore('mobEdu.profile.store.bioInfo');
			recStore.each(function(record) {
				if (record.data.addCode === '1P') {
					mobEdu.profile.f.loadLocalAddressInfo(record);
					return;
				}
			});
		},

		loadLocalAddressInfo: function(loadRecord) {
			mobEdu.util.get('mobEdu.profile.view.updateInfo');
			Ext.getCmp('address1').setValue(loadRecord.data.str1);
			Ext.getCmp('address2').setValue(loadRecord.data.str2);
			Ext.getCmp('address3').setValue(loadRecord.data.str3);
			Ext.getCmp('city').setValue(loadRecord.data.city);
			Ext.getCmp('upState').setValue(loadRecord.data.stateCode);
			Ext.getCmp('upZip').setValue(loadRecord.data.zip);
			Ext.getCmp('upCounty').setValue(loadRecord.data.countyCode);
			Ext.getCmp('nation').setValue(loadRecord.data.nationCode);
		},

		checkUnList: function(unListValue) {
			if (unListValue === 'Y') {
				return true;
			} else {
				return false;
			}
		},

		onAreaCodeKeyup: function(areaField) {
			var areaCode = (areaField.getValue()).toString() + '';
			areaCode = areaCode.replace(/|\(|\)/g, "");
			areaField.setValue(areaCode);
			var length = areaCode.length;
			if (length > 5) {
				areaField.setValue(areaCode.substring(0, 5));
				return false;
			}
			return true;
		},

		onPhExtKeyup: function(phoneExt) {
			var phExtention = (phoneExt.getValue()).toString() + '';
			phExtention = phExtention.replace(/|\(|\)/g, "");
			phoneExt.setValue(phExtention);
			var length = phExtention.length;
			if (length > 10) {
				phoneExt.setValue(phExtention.substring(0, 10));
				return false;
			}
			return true;
		},

		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},

		doUpdate: function(action) {
			if (action.match(/update/gi)) {
				return true;
			} else {
				return false;
			}
		},

		updateBioInfo: function() {
			var appTestDate = new Date();
			var upOrder = Ext.getCmp('order').getValue();
			var upRel = Ext.getCmp('relation').getValue();
			var upFirst = Ext.getCmp('firstname').getValue();
			var upMid = Ext.getCmp('midName').getValue();
			var upLast = Ext.getCmp('lastname').getValue();
			var upAdd1 = Ext.getCmp('address1').getValue();
			var upAdd2 = Ext.getCmp('address2').getValue();
			var upAdd3 = Ext.getCmp('address3').getValue();
			var upCity = Ext.getCmp('city').getValue();
			var upState = Ext.getCmp('upState').getValue();
			var upZip = Ext.getCmp('upZip').getValue();
			var upCounty = Ext.getCmp('upCounty').getValue();
			var upNation = Ext.getCmp('nation').getValue();
			var upAreaCode = Ext.getCmp('areaCode').getValue();
			var upPhno = Ext.getCmp('phNum').getValue();
			var upintCode = Ext.getCmp('intAcCode').getValue();
			// var upPhCodeList = Ext.getCmp('phcodeList').getValue();
			var upPhCodeList1 = Ext.getCmp('phcodeList1').getValue();
			var upAreaCode1 = Ext.getCmp('areaCode1').getValue();
			var upPhno1 = Ext.getCmp('phNum1').getValue();
			var upintCode1 = Ext.getCmp('intAcCode1').getValue();
			var upPhCodeList2 = Ext.getCmp('phcodeList2').getValue();
			var upAreaCode2 = Ext.getCmp('areaCode2').getValue();
			var upPhno2 = Ext.getCmp('phNum2').getValue();
			var upintCode2 = Ext.getCmp('intAcCode2').getValue();
			var upPhCodeList3 = Ext.getCmp('phcodeList3').getValue();
			var upAreaCode3 = Ext.getCmp('areaCode3').getValue();
			var upPhno3 = Ext.getCmp('phNum3').getValue();
			var upintCode3 = Ext.getCmp('intAcCode3').getValue();
			var upPhCodeList4 = Ext.getCmp('phcodeList4').getValue();
			var upAreaCode4 = Ext.getCmp('areaCode4').getValue();
			var upPhno4 = Ext.getCmp('phNum4').getValue();
			var upintCode4 = Ext.getCmp('intAcCode4').getValue();
			var upPhCodeList5 = Ext.getCmp('phcodeList5').getValue();
			var upAreaCode5 = Ext.getCmp('areaCode5').getValue();
			var upPhno5 = Ext.getCmp('phNum5').getValue();
			var upintCode5 = Ext.getCmp('intAcCode5').getValue();
			var firstName = Ext.getCmp('firstname');
			var regExp = new RegExp("^[a-zA-Z ]+$");
			if (upFirst != '' && (!regExp.test(upFirst))) {
				Ext.Msg.show({
					id: 'fName',
					name: 'fName',
					cls: 'msgbox',
					title: null,
					message: '<p>Invalid First Name.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							firstName.focus(true);
						}
					}
				});
			} else {
				var middleName = Ext.getCmp('midName');
				var midNameRegExp = new RegExp("^[a-zA-Z ]+$");
				if (upMid != '' && !midNameRegExp.test(upMid)) {
					Ext.Msg.show({
						id: 'mNmae',
						name: 'mNmae',
						cls: 'msgbox',
						title: null,
						message: '<p>Invalid Middle Name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								middleName.focus(true);
							}
						}
					});
				} else {
					var lastName = Ext.getCmp('lastname');
					var lastNameRegExp = new RegExp("^[a-zA-Z ]+$");
					if (upMid != '' && !lastNameRegExp.test(upMid)) {
						Ext.Msg.show({
							id: 'lName',
							name: 'lName',
							cls: 'msgbox',
							title: null,
							message: '<p>Invalid Last Name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						var zipCode = Ext.getCmp('upZip');
						var zipValue = zipCode.getValue().toString();
						if (zipValue != '' && zipValue.length != 0 && zipValue.length < 5) {
							Ext.Msg.show({
								id: 'zipMsg',
								name: 'zipMsg',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid Zip Code.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										zipCode.focus(true);
									}
								}
							});
						} else {
							var cityField = Ext.getCmp('city');
							var cityRegExp = new RegExp("^[a-zA-Z0-9_ ]+$");
							if (upCity != '' && !cityRegExp.test(upCity)) {
								Ext.Msg.show({
									id: 'cityName',
									name: 'cityName',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid City.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											cityField.focus(true);
										}
									}
								});
							} else {
								var areaNo = Ext.getCmp('areaCode');
								var areaNoValue = areaNo.getValue();
								areaNoValue = areaNoValue.replace(/-|\(|\)/g, "");
								if (areaNoValue != '' && areaNoValue.length > 6 && areaNoValue.length != 0) {
									Ext.Msg.show({
										id: 'areaMsg',
										name: 'areaMsg',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid Area Code.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												areaNo.focus(true);
											}
										}
									});
								} else {
									var phone = Ext.getCmp('phNum');
									var phoneValue = phone.getValue();
									phoneValue = phoneValue.replace(/|\(|\)/g, "");
									if (phoneValue != '' && phoneValue.length > 12 && phoneValue.length != 0) {
										Ext.Msg.show({
											id: 'phoneMsg',
											name: 'phoneMsg',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid Phone Number.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													phone.focus(true);
												}
											}
										});
									} else {
										var areaNo1 = Ext.getCmp('areaCode1');
										var areaNo1Value = areaNo1.getValue();
										areaNo1Value = areaNo1Value.replace(/-|\(|\)/g, "");
										if (areaNo1Value != '' && areaNo1Value.length > 6 && areaNo1Value.length != 0) {
											Ext.Msg.show({
												id: 'areaMsg1',
												name: 'areaMsg1',
												title: null,
												cls: 'msgbox',
												message: '<p>Invalid Area Code.</p>',
												buttons: Ext.MessageBox.OK,
												fn: function(btn) {
													if (btn == 'ok') {
														areaNo1.focus(true);
													}
												}
											});
										} else {
											var phoneNo1 = Ext.getCmp('phNum1');
											var phoneNo1Value = phoneNo1.getValue();
											phoneNo1Value = phoneNo1Value.replace(/|\(|\)/g, "");
											if (phoneNo1Value != '' && phoneNo1Value.length > 12 && phoneNo1Value.length != 0) {
												Ext.Msg.show({
													id: 'phone1Msg',
													name: 'phone1Msg',
													title: null,
													cls: 'msgbox',
													message: '<p>Invalid Phone Number.</p>',
													buttons: Ext.MessageBox.OK,
													fn: function(btn) {
														if (btn == 'ok') {
															phoneNo1.focus(true);
														}
													}
												});
											} else {
												var areaNo2 = Ext.getCmp('areaCode2');
												var areaNo2Value = areaNo2.getValue();
												areaNo2Value = areaNo2Value.replace(/-|\(|\)/g, "");
												if (areaNo2Value != '' && areaNo2Value.length > 6 && areaNo2Value.length != 0) {
													Ext.Msg.show({
														id: 'areaMsg2',
														name: 'areaMsg2',
														title: null,
														cls: 'msgbox',
														message: '<p>Invalid Area Code.</p>',
														buttons: Ext.MessageBox.OK,
														fn: function(btn) {
															if (btn == 'ok') {
																areaNo2.focus(true);
															}
														}
													});
												} else {
													var phoneNo2 = Ext.getCmp('phNum2');
													var phoneNo2Value = phoneNo2.getValue();
													phoneNo2Value = phoneNo2Value.replace(/|\(|\)/g, "");
													if (phoneNo2Value != '' && phoneNo2Value.length > 12 && phoneNo2Value.length != 0) {
														Ext.Msg.show({
															id: 'phone2Msg',
															name: 'phone2Msg',
															title: null,
															cls: 'msgbox',
															message: '<p>Invalid Phone Number.</p>',
															buttons: Ext.MessageBox.OK,
															fn: function(btn) {
																if (btn == 'ok') {
																	phoneNo2.focus(true);
																}
															}
														});
													} else {
														var areaNo3 = Ext.getCmp('areaCode3');
														var areaNo3Value = areaNo3.getValue();
														areaNo3Value = areaNo3Value.replace(/-|\(|\)/g, "");
														if (areaNo3Value != '' && areaNo3Value.length > 6 && areaNo3Value.length != 0) {
															Ext.Msg.show({
																id: 'areaMsg3',
																name: 'areaMsg3',
																title: null,
																cls: 'msgbox',
																message: '<p>Invalid Area Code.</p>',
																buttons: Ext.MessageBox.OK,
																fn: function(btn) {
																	if (btn == 'ok') {
																		areaNo3.focus(true);
																	}
																}
															});
														} else {
															var phoneNo3 = Ext.getCmp('phNum3');
															var phoneNo3Value = phoneNo3.getValue();
															phoneNo3Value = phoneNo3Value.replace(/|\(|\)/g, "");
															if (phoneNo3Value != '' && phoneNo3Value.length > 12 && phoneNo3Value.length != 0) {
																Ext.Msg.show({
																	id: 'phone3Msg',
																	name: 'phone3Msg',
																	title: null,
																	cls: 'msgbox',
																	message: '<p>Invalid Phone Number.</p>',
																	buttons: Ext.MessageBox.OK,
																	fn: function(btn) {
																		if (btn == 'ok') {
																			phoneNo3.focus(true);
																		}
																	}
																});
															} else {
																var areaNo4 = Ext.getCmp('areaCode4');
																var areaNo4Value = areaNo4.getValue();
																areaNo4Value = areaNo4Value.replace(/-|\(|\)/g, "");
																if (areaNo4Value != '' && areaNo4Value.length > 6 && areaNo4Value.length != 0) {
																	Ext.Msg.show({
																		id: 'areaMsg4',
																		name: 'areaMsg4',
																		title: null,
																		cls: 'msgbox',
																		message: '<p>Invalid Area Code.</p>',
																		buttons: Ext.MessageBox.OK,
																		fn: function(btn) {
																			if (btn == 'ok') {
																				areaNo4.focus(true);
																			}
																		}
																	});
																} else {
																	var phoneNo4 = Ext.getCmp('phNum4');
																	var phoneNo4Value = phoneNo4.getValue();
																	phoneNo4Value = phoneNo4Value.replace(/|\(|\)/g, "");
																	if (phoneNo4Value != '' && phoneNo4Value.length > 12 && phoneNo4Value.length != 0) {
																		Ext.Msg.show({
																			id: 'phone4Msg',
																			name: 'phone4Msg',
																			title: null,
																			cls: 'msgbox',
																			message: '<p>Invalid Phone Number.</p>',
																			buttons: Ext.MessageBox.OK,
																			fn: function(btn) {
																				if (btn == 'ok') {
																					phoneNo4.focus(true);
																				}
																			}
																		});
																	} else {
																		var areaNo5 = Ext.getCmp('areaCode5');
																		var areaNo5Value = areaNo5.getValue();
																		areaNo5Value = areaNo5Value.replace(/-|\(|\)/g, "");
																		if (areaNo5Value != '' && areaNo5Value.length > 6 && areaNo5Value.length != 0) {
																			Ext.Msg.show({
																				id: 'areaMsg5',
																				name: 'areaMsg5',
																				title: null,
																				cls: 'msgbox',
																				message: '<p>Invalid Area Code.</p>',
																				buttons: Ext.MessageBox.OK,
																				fn: function(btn) {
																					if (btn == 'ok') {
																						areaNo5.focus(true);
																					}
																				}
																			});
																		} else {
																			var phoneNo5 = Ext.getCmp('phNum5');
																			var phoneNo5Value = phoneNo5.getValue();
																			phoneNo5Value = phoneNo5Value.replace(/|\(|\)/g, "");
																			if (phoneNo5Value != '' && phoneNo5Value.length > 12 && phoneNo5Value.length != 0) {
																				Ext.Msg.show({
																					id: 'phone5Msg',
																					name: 'phone5Msg',
																					title: null,
																					cls: 'msgbox',
																					message: '<p>Invalid Phone Number.</p>',
																					buttons: Ext.MessageBox.OK,
																					fn: function(btn) {
																						if (btn == 'ok') {
																							phoneNo5.focus(true);
																						}
																					}
																				});
																			} else {
																				var intAccess1 = Ext.getCmp('intAcCode1');
																				var intAccess1Value = intAccess1.getValue();
																				intAccess1Value = intAccess1Value.replace(/|\(|\)/g, "");
																				if (intAccess1Value != '' && intAccess1Value.length > 16 && intAccess1Value.length != 0) {
																					Ext.Msg.show({
																						id: 'int1Msg',
																						name: 'int1Msg',
																						title: null,
																						cls: 'msgbox',
																						message: '<p>Invalid International Access Number.</p>',
																						buttons: Ext.MessageBox.OK,
																						fn: function(btn) {
																							if (btn == 'ok') {
																								intAccess1.focus(true);
																							}
																						}
																					});
																				} else {
																					var intAccess2 = Ext.getCmp('intAcCode2');
																					var intAccess2Value = intAccess2.getValue();
																					intAccess2Value = intAccess2Value.replace(/|\(|\)/g, "");
																					if (intAccess2Value != '' && intAccess2Value.length > 16 && intAccess2Value.length != 0) {
																						Ext.Msg.show({
																							id: 'int2Msg',
																							name: 'int2Msg',
																							title: null,
																							cls: 'msgbox',
																							message: '<p>Invalid International Access Number.</p>',
																							buttons: Ext.MessageBox.OK,
																							fn: function(btn) {
																								if (btn == 'ok') {
																									intAccess2.focus(true);
																								}
																							}
																						});
																					} else {
																						var intAccess3 = Ext.getCmp('intAcCode3');
																						var intAccess3Value = intAccess3.getValue();
																						intAccess3Value = intAccess3Value.replace(/|\(|\)/g, "");
																						if (intAccess3Value != '' && intAccess3Value.length > 16 && intAccess3Value.length != 0) {
																							Ext.Msg.show({
																								id: 'int3Msg',
																								name: 'int3Msg',
																								title: null,
																								cls: 'msgbox',
																								message: '<p>Invalid International Access Number.</p>',
																								buttons: Ext.MessageBox.OK,
																								fn: function(btn) {
																									if (btn == 'ok') {
																										intAccess3.focus(true);
																									}
																								}
																							});
																						} else {
																							var intAccess4 = Ext.getCmp('intAcCode4');
																							var intAccess4Value = intAccess4.getValue();
																							intAccess4Value = intAccess4Value.replace(/|\(|\)/g, "");
																							if (intAccess4Value != '' && intAccess4Value.length > 16 && intAccess4Value.length != 0) {
																								Ext.Msg.show({
																									id: 'int4Msg',
																									name: 'int4Msg',
																									title: null,
																									cls: 'msgbox',
																									message: '<p>Invalid International Access Number.</p>',
																									buttons: Ext.MessageBox.OK,
																									fn: function(btn) {
																										if (btn == 'ok') {
																											intAccess4.focus(true);
																										}
																									}
																								});
																							} else {
																								var intAccess5 = Ext.getCmp('intAcCode5');
																								var intAccess5Value = intAccess5.getValue();
																								intAccess5Value = intAccess5Value.replace(/|\(|\)/g, "");
																								if (intAccess5Value != '' && intAccess5Value.length > 16 && intAccess5Value.length != 0) {
																									Ext.Msg.show({
																										id: 'int5Msg',
																										name: 'int5Msg',
																										title: null,
																										cls: 'msgbox',
																										message: '<p>Invalid International Access Number.</p>',
																										buttons: Ext.MessageBox.OK,
																										fn: function(btn) {
																											if (btn == 'ok') {
																												intAccess5.focus(true);
																											}
																										}
																									});
																								} else {
																									var orderNo = Ext.getCmp('order');
																									var orderValue = orderNo.getValue();
																									orderValue = orderValue.replace(/-|\(|\)/g, "");
																									if (orderValue != '' && orderValue.length > 5 && orderValue.length != 0) {
																										Ext.Msg.show({
																											id: 'orderMsg',
																											name: 'orderMsg',
																											title: null,
																											cls: 'msgbox',
																											message: '<p>Invalid Order Number.</p>',
																											buttons: Ext.MessageBox.OK,
																											fn: function(btn) {
																												if (btn == 'ok') {
																													orderNo.focus(true);
																												}
																											}
																										});
																									} else {
																										var phExtNum = Ext.getCmp('phExt');
																										var phExtValue = phExtNum.getValue();
																										phExtValue = phExtValue.replace(/|\(|\)/g, "");
																										if (phExtValue != '' && phExtValue.length > 10 && phExtValue.length != 0) {
																											Ext.Msg.show({
																												id: 'phExtMsg',
																												name: 'phExtMsg',
																												title: null,
																												cls: 'msgbox',
																												message: '<p>Invalid Phone Extention.</p>',
																												buttons: Ext.MessageBox.OK,
																												fn: function(btn) {
																													if (btn == 'ok') {
																														phExtNum.focus(true);
																													}
																												}
																											});
																										} else {
																											mobEdu.profile.f.loadUpdateBioInfo();
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		},

		loadUpdateBioInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
			var record = store.data.all;
			var appTestDate = new Date();
			// var testDateFormatted = (mobEdu.profile.f.dateChecking([appTestDate.getMonth() + 1]) + '/' + mobEdu.profile.f.dateChecking(appTestDate.getDate()) + '/' + appTestDate.getFullYear());
			var testDateFormatted = '';
			var upOrder = Ext.getCmp('order').getValue();
			var upRel = Ext.getCmp('relation').getValue();
			var upFirst = Ext.getCmp('firstname').getValue();
			var upMid = Ext.getCmp('midName').getValue();
			var upLast = Ext.getCmp('lastname').getValue();
			var upAdd1 = Ext.getCmp('address1').getValue();
			var upAdd2 = Ext.getCmp('address2').getValue();
			var upAdd3 = Ext.getCmp('address3').getValue();
			var upAdd4 = '';
			var upCity = Ext.getCmp('city').getValue();
			var upState = Ext.getCmp('upState').getValue();
			var upZip = Ext.getCmp('upZip').getValue();
			var upCounty = Ext.getCmp('upCounty').getValue();
			var upNation = Ext.getCmp('nation').getValue();
			var upAreaCode = Ext.getCmp('areaCode').getValue();
			var upPhno = Ext.getCmp('phNum').getValue();
			var upintCode = Ext.getCmp('intAcCode').getValue();
			var email = Ext.getCmp('emailAddr').getValue();
			if (Ext.getCmp('copyAdress').isChecked()) {
				sameAddInd = 'Y';
			} else {
				sameAddInd = 'N';
			}
			// var upPhCodeList = Ext.getCmp('phcodeList').getValue();
			// var upPhCodeList1 = Ext.getCmp('phcodeList1').getValue();
			// var upAreaCode1 = Ext.getCmp('areaCode1').getValue();
			// var upPhno1 = Ext.getCmp('phNum1').getValue();
			// var upintCode1 = Ext.getCmp('intAcCode1').getValue();
			// var upDel1 = Ext.getCmp('delPhone1').getChecked();
			// var upDel2 = Ext.getCmp('delPhone2').getChecked();
			// var upDel3 = Ext.getCmp('delPhone3').getChecked();
			// var upDel4 = Ext.getCmp('delPhone4').getChecked();
			// var upDel5 = Ext.getCmp('delPhone5').getChecked();
			var upDelAdd = Ext.getCmp('delAdd').getChecked();
			if (upAdd1 === '' || upAdd1 === undefined) {
				if (upAdd2 !== '' && upAdd2 !== undefined) {
					upAdd1 = upAdd2;
					upAdd2 = '';
				} else {
					if (upAdd3 !== '' && upAdd3 !== undefined) {
						upAdd1 = upAdd3;
						upAdd3 = '';
					}
				}
			} else {
				if (upAdd2 === '' || upAdd2 === undefined) {
					upAdd2 = upAdd3;
					upAdd3 = '';
				}
			}

			if (upState === '0') {
				upState = '';
			}
			if (upState === '0') {
				upState = '';
			}
			if (upNation === '0') {
				upNation = '';
			}
			if (upRel === '0') {
				upRel = '';
			}
			if (upCounty === '0') {
				upCounty = '';
			}
			// if (upDelAdd === true) {
			// 	upDelAdd = 'Y';
			// } else {
			// 	upDelAdd = 'N';
			// }
			// if (upDel1 === true) {
			// 	upDel1 = 'Y';
			// } else {
			// 	upDel1 = 'N';
			// }
			// if (upDel2 === true) {
			// 	upDel2 = 'Y';
			// } else {
			// 	upDel2 = 'N';
			// }
			// if (upDel3 === true) {
			// 	upDel3 = 'Y';
			// } else {
			// 	upDel3 = 'N';
			// }
			// if (upDel4 === true) {
			// 	upDel4 = 'Y';
			// } else {
			// 	upDel4 = 'N';
			// }
			// if (upDel5 === true) {
			// 	upDel5 = 'Y';
			// } else {
			// 	upDel5 = 'N';
			// }
			// if (record[0].data.addCode === 'CL') {
			// 	Ext.getCmp('unList').setChecked(true);
			// }
			// var upUnlistInd = Ext.getCmp('unList').getChecked();
			// var upUnList1 = Ext.getCmp('unList1').getChecked();
			// var upUnList2 = Ext.getCmp('unList2').getChecked();
			// var upUnList3 = Ext.getCmp('unList3').getChecked();
			// var upUnList4 = Ext.getCmp('unList4').getChecked();
			// var upUnList5 = Ext.getCmp('unList5').getChecked();
			// if (upUnlistInd === true) {
			// 	upUnlistInd = 'Y';
			// } else {
			upUnlistInd = 'N';
			// }
			// if (upUnList1 === true) {
			// 	upUnList1 = 'Y';
			// } else {
			// 	upUnList1 = 'N';
			// }
			// if (upUnList2 === true) {
			// 	upUnList2 = 'Y';
			// } else {
			// 	upUnList2 = 'N';
			// }
			// if (upUnList3 === true) {
			// 	upUnList3 = 'Y';
			// } else {
			// 	upUnList3 = 'N';
			// }
			// if (upUnList4 === true) {
			// 	upUnList4 = 'Y';
			// } else {
			// 	upUnList4 = 'N';
			// }
			// if (upUnList5 === true) {
			// 	upUnList5 = 'Y';
			// } else {
			// 	upUnList5 = 'N';
			// }
			// var upPhCodeList2 = Ext.getCmp('phcodeList2').getValue();
			// var upAreaCode2 = Ext.getCmp('areaCode2').getValue();
			// var upPhno2 = Ext.getCmp('phNum2').getValue();
			// var upintCode2 = Ext.getCmp('intAcCode2').getValue();
			// var upPhCodeList3 = Ext.getCmp('phcodeList3').getValue();
			// var upAreaCode3 = Ext.getCmp('areaCode3').getValue();
			// var upPhno3 = Ext.getCmp('phNum3').getValue();
			// var upintCode3 = Ext.getCmp('intAcCode3').getValue();
			// var upPhCodeList4 = Ext.getCmp('phcodeList4').getValue();
			// var upAreaCode4 = Ext.getCmp('areaCode4').getValue();
			// var upPhno4 = Ext.getCmp('phNum4').getValue();
			// var upintCode4 = Ext.getCmp('intAcCode4').getValue();
			// var upPhCodeList5 = Ext.getCmp('phcodeList5').getValue();
			// var upAreaCode5 = Ext.getCmp('areaCode5').getValue();
			// var upPhno5 = Ext.getCmp('phNum5').getValue();
			// var upintCode5 = Ext.getCmp('intAcCode5').getValue();
			// if(upPhCodeList === '0'){
			//     upPhCodeList = '';
			// }
			// if (upPhCodeList1 === '0') {
			// 	upPhCodeList1 = '';
			// }
			// if (upPhCodeList2 === '0') {
			// 	upPhCodeList2 = '';
			// }
			// if (upPhCodeList3 === '0') {
			// 	upPhCodeList3 = '';
			// }
			// if (upPhCodeList4 === '0') {
			// 	upPhCodeList4 = '';
			// }
			// if (upPhCodeList5 === '0') {
			// 	upPhCodeList5 = '';
			// }
			// var seqno = Ext.getCmp('seqno').getValue();
			// var seqno1 = Ext.getCmp('seqno1').getValue();
			// var seqno2 = Ext.getCmp('seqno2').getValue();
			// var seqno3 = Ext.getCmp('seqno3').getValue();
			// var seqno4 = Ext.getCmp('seqno4').getValue();
			// var seqno5 = Ext.getCmp('seqno5').getValue();
			var phExt = Ext.getCmp('phExt').getValue();
			// var phExt1 = Ext.getCmp('phExt1').getValue();
			// var phExt2 = Ext.getCmp('phExt2').getValue();
			// var phExt3 = Ext.getCmp('phExt3').getValue();
			// var phExt4 = Ext.getCmp('phExt4').getValue();
			// var phExt5 = Ext.getCmp('phExt5').getValue();
			// var upPhList = upPhno1 + ',' + upPhno2 + ',' + upPhno3 + ',' + upPhno4 + ',' + upPhno5;
			var upPhList = ',,,,';
			// var upUnList = upUnList1 + ',' + upUnList2 + ',' + upUnList3 + ',' + upUnList4 + ',' + upUnList5;
			var upUnList = ',,,,';
			// var upAreaList = upAreaCode1 + ',' + upAreaCode2 + ',' + upAreaCode3 + ',' + upAreaCode4 + ',' + upAreaCode5;
			var upAreaList = ',,,,';
			// var upIntAccess = upintCode1 + ',' + upintCode2 + ',' + upintCode3 + ',' + upintCode4 + ',' + upintCode5;
			var upIntAccess = ',,,,';
			// var upPhoneCodeList = upPhCodeList1 + ',' + upPhCodeList2 + ',' + upPhCodeList3 + ',' + upPhCodeList4 + ',' + upPhCodeList5;
			var upPhoneCodeList = ',,,,';
			// var upDelList = upDel1 + ',' + upDel2 + ',' + upDel3 + ',' + upDel4 + ',' + upDel5;
			var upDelList = ',,,,';
			// var upSeqNumList = seqno1 + ',' + seqno2 + ',' + seqno3 + ',' + seqno4 + ',' + seqno5;
			var upSeqNumList = ',,,,';
			// var upPhExtList = phExt1 + ',' + phExt2 + ',' + phExt3 + ',' + phExt4 + ',' + phExt5;
			var upPhExtList = ',,,,';
			var upCountryList = ',,,,';
			var upCountry = '';
			var upHno = '';
			var upToDate = '';
			var upLastPfx = '';
			// var phExt = '';
			// var upSeqNumList = ',,,,';
			// 'mobEdu.profile.store.updateInfo'
			var store = mobEdu.util.getStore('mobEdu.profile.store.updateInfo');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'updateBioInfo');
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				role: mobEdu.profile.f.roleFlag,
				recType: record[0].data.recType,
				type: record[0].data.addCode,
				sequenceNo: record[0].data.seqno,
				delAddr: 'N',
				dateFmt: testDateFormatted,
				houseNo: upHno,
				tDate: upToDate,
				str1: upAdd1,
				str2: upAdd2,
				str3: upAdd3,
				str4: upAdd4,
				city: upCity,
				state: upState,
				zip: upZip,
				county: upCounty,
				country: upCountry,
				nation: upNation,
				phCodeList: upPhoneCodeList,
				seqList: upSeqNumList,
				unlist: upUnlistInd,
				countryList: upCountryList,
				areaList: upAreaList,
				unlistInd: upUnList,
				intAccess: upIntAccess,
				lastNamePfx: upLastPfx,
				pmi: upMid,
				lastName: upLast,
				phList: upPhList,
				delInd: upDelList,
				area: upAreaCode,
				order: upOrder,
				pRelation: upRel,
				firstName: upFirst,
				phExtList: upPhExtList,
				primaryExt: phExt,
				primaryNo: upPhno,
				pAccess: upintCode,
				email: email,
				rowId: record[0].data.rowId,
				sameAddInd: sameAddInd

			});
			store.getProxy().afterRequest = function() {
				mobEdu.profile.f.updateBioInfoResponseHandler();
			}
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'updatebioinfo');

		},

		updateBioInfoResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.profile.store.updateInfo');
			var nAppStatus = newAppStore.getProxy().getReader().rawData.status;
			if (nAppStatus.match(/success/gi)) {
				Ext.Msg.show({
					id: 'updateSuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Updated Successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							if (mobEdu.profile.f.roleFlag == 'STUDENT') {
								mobEdu.profile.f.loadBioInfo();
							} else {
								mobEdu.profile.f.loadEmployeeInfo();
							}
							mobEdu.profile.f.resetInfo();
						}
					}
				});
			} else {
				var nAppStatusData = nAppStatus.replace(/color=red|color=blue/g, "color=white");
				var nAppLitData = nAppStatusData.split(".");
				var nAppData = nAppStatusData.split(" ");
				if (nAppData.length > 30) {
					Ext.Msg.show({
						title: null,
						message: '<div style="overflow : hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 8;-webkit-box-orient: vertical;color:white;">' + nAppStatusData + '</div>',
						buttons: Ext.MessageBox.OK
					});
				} else if (nAppLitData.length > 0 && nAppLitData.length < 4) {
					Ext.Msg.show({
						title: null,
						message: nAppStatusData,
						buttons: Ext.MessageBox.OK
					});
				} else {
					var nAppPreciseData = nAppStatusData.split(".");
					var nAppPrecisedData = '';
					for (var i = 2; i < 5; i++) {
						if (nAppPreciseData.length > 0) {
							if (nAppPrecisedData !== '') {
								if (nAppPreciseData[i] !== undefined) {
									nAppPrecisedData = nAppPrecisedData + '.' + nAppPreciseData[i];
								}
							} else {
								nAppPrecisedData = nAppPreciseData[i];
							}
						}
					}
					Ext.Msg.show({
						title: null,
						message: nAppPrecisedData,
						buttons: Ext.MessageBox.OK
					});
				}
			}
		},
		loadConfirmInfo: function(record, item) {
			var cinfoStore = mobEdu.util.getStore('mobEdu.profile.store.confirmInfo');
			cinfoStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			cinfoStore.getProxy().setUrl(webserver + 'confirmBioInfo');
			cinfoStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				addCode: record.data.addCode,
				type: record.data.recType,
				termCode: record.data.term
			});

			cinfoStore.getProxy().afterRequest = function() {
					mobEdu.profile.f.confirmInfoResposeHandler(item);
				},
				cinfoStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'confirmbioinfo');
		},

		confirmInfoResposeHandler: function(item) {
			var cAppStore = mobEdu.util.getStore('mobEdu.profile.store.confirmInfo');
			var cInfoStore = cAppStore.getProxy().getReader().rawData;
			if (cInfoStore.status !== 'SUCCESS') {
				Ext.Msg.show({
					title: null,
					message: cInfoStore.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
				item.target.src = mobEdu.util.getResourcePath() + "images/confirm.png";
			} else {
				mobEdu.profile.f.checkIfBiographicRequired();
			}
		},

		checkIfBiographicRequired: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
			if (store.data.length > 0) {
				store.removeAll();
				store.removed = [];
			}
			store.getProxy().setUrl(webserver + 'checkIfBiographicRequired');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId()
			});
			store.getProxy().afterRequest = function() {
				mobEdu.profile.f.checkIfBioRequiredResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'checkIfBiographicRequired');
		},

		checkIfBioRequiredResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
			var storeData = store.data.all[0];
			if (storeData.data.indicator === 'Y') {
				mobEdu.util.updateBioInfoCheck(true);
			} else {
				mobEdu.util.updateBioInfoCheck(false);
				mobEdu.util.showMainView();
			}
		},

		onZipKeyup: function(zipfield) {
			var value = (zipfield.getValue()).toString() + '';
			value = value.replace(/|\(|\)/g, "");
			zipfield.setValue(value);
			var length = value.length;
			if (length > 30) {
				zipfield.setValue(value.substring(0, 30));
				return false;
			}
			return true;
		},

		onIntAccKeyup: function(intAccessField) {
			var intAccNumber = (intAccessField.getValue()).toString() + '';
			intAccNumber = intAccNumber.replace(/|\(|\)/g, "");
			intAccessField.setValue(intAccNumber);
			var length = intAccNumber.length;
			if (length > 16) {
				intAccessField.setValue(intAccNumber.substring(0, 16));
				return false;
			}
			return true;
		},

		onPhone1Keyup: function(phone1Field) {
			var phoneNumber = (phone1Field.getValue()).toString() + '';
			phoneNumber = phoneNumber.replace(/|\(|\)/g, "");
			phone1Field.setValue(phoneNumber);
			var length = phoneNumber.length;
			if (length > 12) {
				phone1Field.setValue(phoneNumber.substring(0, 12));
				return false;
			}
			return true;
		},
		onStateChange: function(newValue, countyCmp, oldValue) {
			if (newValue === '' || newValue === null) {
				newValue = 0;
			}
			var emrStore = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
			var eRecord = emrStore.data.all;
			var addCode = eRecord[0].data.addCode;
			if (addCode !== 'EMERG') {
				mobEdu.profile.f.loadCountyEnumerations(newValue);
			}
		},
		resetInfo: function() {
			mobEdu.util.get('mobEdu.profile.view.updateInfo');
		},

		resetLocalInfo: function() {
			mobEdu.profile.f.resetInfo();
			mobEdu.profile.f.resetUpdateInfo();
		},

		resetUpdateInfo: function() {
			var resetStore = mobEdu.util.getStore('mobEdu.profile.store.updateLocal');
			var resetRecord = resetStore.data.all[0];
			mobEdu.profile.f.loadUpdateInfo(resetRecord);
		}
	}
});