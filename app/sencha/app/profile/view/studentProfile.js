Ext.define('mobEdu.profile.view.studentProfile', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.profile.store.studentProfile'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            itemTpl: '<table width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.profile.store.studentProfile'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'studentPTitle'
        }],
        flex: 1
    },
    initialize: function() {
        mobEdu.profile.f.initializeProfileView();
    }
});