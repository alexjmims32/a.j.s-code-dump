Ext.define('mobEdu.profile.view.directoryOptOut', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.profile.store.directoryOpt'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
                xtype: 'customToolbar',
                title: '<h1>Directory Listing Opt-Out</h1>'
            }, {
                xtype: 'panel',
                id: 'checkIndicator',
                docked: 'top',
                padding: '0 0 0 .7em',
                // html: ['<table><tr><td><h3>' + 'Check the box below if you do NOT want your directory information disclosed (opt-out) according to the policy at <a href="javascript:onclick = [mobEdu.profile.f.showFerpaLink()]">http://www.wit.edu/ssc/registration/ferpa.html</a><br><br>' + '</h3></td></tr></table><br />']
            },
            // {
            //    layout: 'vbox',
            //    scroll: 'vertical',
            //    items: [
            {
                xtype: 'fieldset',
                id: 'optOutFieldset',
                items: [{
                        xtype: 'checkboxfield',
                        id: 'dirOptcheck',
                        labelAlign: 'bottom',
                        labelWidth: '90%',
                        label: '<h3><b style="white-space:normal;">By checking the box, I am OPTING-OUT from the Institute sharing my directory information. I also understand that this will be for the duration of my matriculation or until I request to reverse non-disclosure in writing to the Registrar\’s Office.</b><h3>',
                        listeners: {
                            check: function(checkboxfield, e, eOpts) {
                                Ext.getCmp('dirOptBot').show();
                            },
                            uncheck: function(checkboxfield, e, eOpts) {
                                Ext.getCmp('dirOptBot').hide();
                            }
                        }
                    }]
                    // }
                    // ]
            }, {
                xtype: 'toolbar',
                id: 'dirOptBot',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [{
                    text: 'Submit Changes',
                    align: 'right',
                    ui: 'button',
                    handler: function() {
                        mobEdu.profile.f.submitOptOut();
                    }
                }]
            }
        ],
        flex: 1
    }
});