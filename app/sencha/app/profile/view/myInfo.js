Ext.define('mobEdu.profile.view.myInfo', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.profile.store.myInfo'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'dataview',
			disableSelection: true,
			emptyText: '<h3 align="center">There is no Information</h3>',
			itemTpl: new Ext.XTemplate(
				'<div><center><h5>To change your information, please login to self service.</h5></center></div>',
				'<div style="width: 100%;padding-left: 10px;">' +
				'<tpl if="(mobEdu.directory.f.ShowAvatar()===true)">',
				'<div style="display: inline-block; margin-right: 10px; padding-top: 15px; padding-left: 10px;"><tpl if="(directoryImageSource===\'BANNER\')"><tpl if="(mobEdu.directory.f.isExists(imageUrl)===true)"><img height=75 width=75 src={imageUrl}><tpl else><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg"></tpl><tpl else><tpl if="((mobEdu.util.getImageUrl() != null)&&(mobEdu.util.getImageUrl() != undefined))"><img height="135" width="175" src="data:image/png;base64,{[mobEdu.util.getImageUrl()]}"><tpl else><img height=75 width=75 src="' + mobEdu.util.getResourcePath() + 'images/avatar.jpg"></tpl></tpl></div>' +
				'</tpl>',
				'<div style="display: inline-block; vertical-align: top; overflow: hidden; text-overflow: ellipsis;">' +
				'<h3 style="padding-top: 15px;"><b>{fullName}</b></h3>' +
				'<tpl if="(mobEdu.directory.f.isExists(title)===true)">',
				'<h4>{title}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(department)===true)">',
				'<h4>{department}</h4>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isExists(campus)===true)">',
				'<h4>{campus}</h4>',
				'</tpl>',
				'</div>',
				'</div>',
				'<tpl for="contactList">',
				'<tpl if="(mobEdu.directory.f.isPhoneGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/phone.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3><a href="tel:{address}">{address}</a></h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isFaxGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/phone24.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3><a href="tel:{address}">{address}</a></h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isEmailGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px; min-height: 52px; margin-right: 5px;">',
				'<div style="display: inline-block"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/mailSmallIcon.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer" style="word-wrap: break-word; word-break: break-word;"><h3><a href="mailto:{address}">{address}</a></h3><tpl if="(mobEdu.directory.f.isTypeEmpty(type)===true)"><h6>{type}</h6></tpl></div></div></div>',
				'</div>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isAddressGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block; vertical-align: top;"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/addressbook.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3>{[mobEdu.util.symbolConverter(values.address)]}</h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'<tpl if="(mobEdu.directory.f.isEmergencyGrp(grp)===true)">',
				'<div style="margin-top: 10px;padding-left: 10px;">',
				'<div style="display: inline-block; vertical-align: top;"><img height=32 width=32 src="' + mobEdu.util.getResourcePath() + 'images/addressbook.png"></div>',
				'<div style="display: inline-block; margin-left: 10px;"><div class="outerContainer"><div class="innerContainer"><h3>{[mobEdu.util.symbolConverter(values.address)]}</h3><h6>{type}</h6></div></div></div>',
				'</div>',
				'</tpl>',
				'</tpl>'
			),
			store: mobEdu.util.getStore('mobEdu.profile.store.myInfo'),
			singleSelect: true,
			loadingText: ''
		}, {
			xtype: 'customToolbar',
			title: '<h1>My Info</h1>',
			id: 'myInfoTitle'
		}],
		flex: 1
	}
});