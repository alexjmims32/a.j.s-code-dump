Ext.define('mobEdu.profile.view.updateInfo', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.profile.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'dataview',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Update Information</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',

            cls: 'bottomtoolbar',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Back',
                ui: 'button',
                align: 'right',
                handler: function() {
                    mobEdu.profile.f.showBioInfo();
                }
            }, {
                text: 'Submit',
                ui: 'button',
                align: 'right',
                handler: function() {
                    mobEdu.profile.f.updateBioInfo();
                }
            }, {
                text: 'Reset',
                ui: 'button',
                align: 'right',
                handler: function() {
                    mobEdu.profile.f.resetLocalInfo();
                }
            }]
        }, {
            xtype: 'fieldset',
            id: 'updateHead',
            title: 'Update Information'
        }, {
            xtype: 'fieldset',
            id: 'emrgSection',
            items: [{
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Order',
                name: 'order',
                hidden: true,
                id: 'order',
                // required:true,
                useClearIcon: true
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                inputCls: 'accSelectField',
                label: 'Relationship',
                name: 'relation',
                id: 'relation',
                placeHolder: 'Select',
                // required:true,
                // useClearIcon:true,
                store: mobEdu.util.getStore('mobEdu.profile.store.relationEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                cls: 'textFieldBg',
                labelWidth: '40%',
                label: 'First Name',
                name: 'firstname',
                id: 'firstname',
                // required:true,
                useClearIcon: true,
                maxLength: 60
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                cls: 'textFieldBg',
                labelWidth: '40%',
                label: 'Middle Name',
                name: 'midName',
                id: 'midName',
                // required:true,
                useClearIcon: true,
                maxLength: 60
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Last Name',
                cls: 'textFieldBg',
                name: 'lastname',
                id: 'lastname',
                // required:true,
                useClearIcon: true,
                maxLength: 60
            }]
        }, {
            xtype: 'fieldset',
            id: 'emailSection',
            items: [{
                xtype: 'emailfield',
                name: 'emailAddr',
                id: 'emailAddr',
                labelWidth: '40%',
                label: 'E-mail',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });
                    me.callParent(arguments);
                }
            }]
        }, {
            xtype: 'fieldset',
            id: 'sameAsPermanent',
            items: [{
                xtype: 'checkboxfield',
                label: 'Copy Permanent Address',
                labelWidth: '40%',
                name: 'copyAdress',
                id: 'copyAdress',
                listeners: {
                    check: function(checkboxfield, e, eOpts) {
                        mobEdu.profile.f.loadLocalAddress();
                    },
                    uncheck: function(checkboxfield, e, eOpts) {
                        mobEdu.profile.f.resetLocalInfo();
                    }
                }
            }]
        }, {
            xtype: 'fieldset',
            id: 'updateSection',
            items: [{
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Address 1',
                name: 'address1',
                id: 'address1',
                required: true,
                useClearIcon: true,
                maxLength: 75
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Address 2',
                name: 'address2',
                id: 'address2',
                useClearIcon: true,
                maxLength: 75
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Address 3',
                name: 'address3',
                id: 'address3',
                // required:true,
                useClearIcon: true,
                maxLength: 75
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'City',
                name: 'city',
                id: 'city',
                // required:true,
                useClearIcon: true,
                maxLength: 50
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'State',
                name: 'upState',
                inputCls: 'accSelectField',
                id: 'upState',
                placeHolder: 'Select',
                store: mobEdu.util.getStore('mobEdu.profile.store.stateEnum'),
                displayField: 'description',
                valueField: 'value',
                listeners: {
                    change: function(selectbox, newValue, oldValue) {
                        mobEdu.profile.f.onStateChange(newValue, Ext.getCmp('upCounty'), Ext.getCmp('upState'));
                    }
                }
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Zip',
                name: 'upZip',
                id: 'upZip',
                cls: 'textFieldBg',
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        return mobEdu.profile.f.onZipKeyup(textfield);
                    }
                },
                maxLength: 30
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'County',
                inputCls: 'accSelectField',
                name: 'upCounty',
                id: 'upCounty',
                cls: 'customField',
                placeHolder: 'SELECT',
                store: mobEdu.util.getStore('mobEdu.profile.store.countyEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'Nation',
                inputCls: 'accSelectField',
                name: 'nation',
                id: 'nation',
                placeHolder: 'Select',
                store: mobEdu.util.getStore('mobEdu.profile.store.nationEnum'),
                displayField: 'description',
                valueField: 'value'
            }, ]
        }, {
            xtype: 'fieldset',
            hidden: true,
            items: [{
                xtype: 'checkboxfield',
                label: 'Delete',
                labelWidth: '40%',
                name: 'delAdd',
                id: 'delAdd'
            }]
        }, {
            xtype: 'fieldset',
            id: 'primaryHead',
            // hidden: true,
            title: 'Primary Phone Number'
        }, {
            xtype: 'fieldset',
            // hidden: true,
            id: 'primarySection',
            items: [{
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Area Code',
                cls: 'textFieldBg',
                name: 'areaCode',
                id: 'areaCode',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onAreaCodeKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Phone Number',
                name: 'phNum',
                cls: 'textFieldBg',
                id: 'phNum',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 15
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'International Phone Number',
                name: 'intAcCode',
                cls: 'textFieldBg',
                id: 'intAcCode',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onIntAccKeyup(textfield);
                    }
                },
                maxLength: 16
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Phone Extention',
                name: 'phExt',
                id: 'phExt',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhExtKeyup(textfield);
                    }
                },
                maxLength: 10
            }, {
                xtype: 'checkboxfield',
                label: 'Unlist',
                hidden: true,
                labelWidth: '40%',
                name: 'unList',
                id: 'unList'
            }]
        }, {
            xtype: 'fieldset',
            hidden: true,
            id: 'otherHead',
            title: 'Other Phone(s)'
        }, {
            xtype: 'fieldset',
            hidden: true,
            id: 'otherSection1',
            items: [{
                xtype: 'hiddenfield',
                name: 'seqno1',
                id: 'seqno1'
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                hidden: true,
                label: 'Phone Type',
                inputCls: 'accSelectField',
                name: 'phcodeList1',
                id: 'phcodeList1',
                placeHolder: 'Select',
                // store: mobEdu.util.getStore('mobEdu.profile.store.phCodeEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Area Code',
                cls: 'textFieldBg',
                name: 'areaCode1',
                id: 'areaCode1',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onAreaCodeKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Phone Number',
                cls: 'textFieldBg',
                name: 'phNum1',
                id: 'phNum1',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 15
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'International Phone Number',
                name: 'intAcCode1',
                id: 'intAcCode1',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onIntAccKeyup(textfield);
                    }
                },
                maxLength: 16
            }, {
                xtype: 'hiddenfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Phone Extention',
                name: 'phExt1',
                id: 'phExt1',
                // required:true,
                useClearIcon: true
            }, {
                xtype: 'checkboxfield',
                label: 'Unlist',
                hidden: true,
                labelWidth: '40%',
                name: 'unList1',
                id: 'unList1'
            }, {
                xtype: 'checkboxfield',
                label: 'Delete',
                hidden: true,
                labelWidth: '40%',
                name: 'delPhone1',
                id: 'delPhone1'
            }]
        }, {
            xtype: 'fieldset',
            hidden: true,
            id: 'otherSection2',
            items: [{
                xtype: 'hiddenfield',
                name: 'seqno2',
                id: 'seqno2'
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'Phone Type',
                inputCls: 'accSelectField',
                name: 'phcodeList2',
                id: 'phcodeList2',
                placeHolder: 'Select',
                // store: mobEdu.util.getStore('mobEdu.profile.store.phCodeEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Area Code',
                cls: 'textFieldBg',
                name: 'areaCode2',
                id: 'areaCode2',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onAreaCodeKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Phone Number',
                cls: 'textFieldBg',
                name: 'phNum2',
                id: 'phNum2',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 15
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'International Phone Number',
                name: 'intAcCod2',
                id: 'intAcCode2',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onIntAccKeyup(textfield);
                    }
                },
                maxLength: 16
            }, {
                xtype: 'hiddenfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Phone Extention',
                name: 'phExt2',
                id: 'phExt2',
                // required:true,
                useClearIcon: true
            }, {
                xtype: 'checkboxfield',
                label: 'Unlist',
                labelWidth: '40%',
                name: 'unList2',
                id: 'unList2'
            }, {
                xtype: 'checkboxfield',
                label: 'Delete',
                labelWidth: '40%',
                name: 'delPhone2',
                id: 'delPhone2'
            }]
        }, {
            xtype: 'fieldset',
            hidden: true,
            id: 'otherSection3',
            items: [{
                xtype: 'hiddenfield',
                name: 'seqno3',
                id: 'seqno3'
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'Phone Type',
                inputCls: 'accSelectField',
                name: 'phcodeList3',
                id: 'phcodeList3',
                placeHolder: 'Select',
                // store: mobEdu.util.getStore('mobEdu.profile.store.phCodeEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Area Code',
                cls: 'textFieldBg',
                name: 'areaCode3',
                id: 'areaCode3',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onAreaCodeKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Phone Number',
                cls: 'textFieldBg',
                name: 'phNum3',
                id: 'phNum3',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 15
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'International Phone Number',
                name: 'intAcCode3',
                id: 'intAcCode3',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onIntAccKeyup(textfield);
                    }
                },
                maxLength: 16
            }, {
                xtype: 'hiddenfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Phone Extention',
                name: 'phExt3',
                id: 'phExt3',
                // required:true,
                useClearIcon: true
            }, {
                xtype: 'checkboxfield',
                label: 'Unlist',
                labelWidth: '40%',
                name: 'unList3',
                id: 'unList3'
            }, {
                xtype: 'checkboxfield',
                label: 'Delete',
                labelWidth: '40%',
                name: 'delPhone3',
                id: 'delPhone3'
            }]
        }, {
            xtype: 'fieldset',
            hidden: true,
            id: 'otherSection4',
            items: [{
                xtype: 'hiddenfield',
                name: 'seqno4',
                id: 'seqno4'
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'Phone Type',
                inputCls: 'accSelectField',
                name: 'phcodeList4',
                id: 'phcodeList4',
                placeHolder: 'Select',
                // store: mobEdu.util.getStore('mobEdu.profile.store.phCodeEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Area Code',
                cls: 'textFieldBg',
                name: 'areaCode4',
                id: 'areaCode4',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onAreaCodeKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Phone Number',
                cls: 'textFieldBg',
                name: 'phNum4',
                id: 'phNum4',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 15
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'International Phone Number',
                name: 'intAcCode4',
                id: 'intAcCode4',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onIntAccKeyup(textfield);
                    }
                },
                maxLength: 16
            }, {
                xtype: 'hiddenfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Phone Extention',
                name: 'phExt4',
                id: 'phExt4',
                // required:true,
                useClearIcon: true
            }, {
                xtype: 'checkboxfield',
                label: 'Unlist',
                labelWidth: '40%',
                name: 'unList4',
                id: 'unList4'
            }, {
                xtype: 'checkboxfield',
                label: 'Delete',
                labelWidth: '40%',
                name: 'delPhone4',
                id: 'delPhone4'
            }]
        }, {
            xtype: 'fieldset',
            id: 'otherSection5',
            hidden: true,
            items: [{
                xtype: 'hiddenfield',
                name: 'seqno5',
                id: 'seqno5'
            }, {
                xtype: 'selectfield',
                labelWidth: '40%',
                label: 'Phone Type',
                inputCls: 'accSelectField',
                name: 'phcodeList5',
                id: 'phcodeList5',
                placeHolder: 'Select',
                // store: mobEdu.util.getStore('mobEdu.profile.store.phCodeEnum'),
                displayField: 'description',
                valueField: 'value'
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Area Code',
                cls: 'textFieldBg',
                name: 'areaCode5',
                id: 'areaCode5',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onAreaCodeKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                label: 'Phone Number',
                cls: 'textFieldBg',
                name: 'phNum5',
                id: 'phNum5',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 15
            }, {
                xtype: 'textfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'International Phone Number',
                name: 'intAcCode5',
                id: 'intAcCode5',
                // required:true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.profile.f.onIntAccKeyup(textfield);
                    }
                },
                maxLength: 16
            }, {
                xtype: 'hiddenfield',
                labelWidth: '40%',
                cls: 'textFieldBg',
                label: 'Phone Extention',
                name: 'phExt5',
                id: 'phExt5',
                // required:true,
                useClearIcon: true
            }, {
                xtype: 'checkboxfield',
                hidden: true,
                label: 'Unlist',
                labelWidth: '40%',
                name: 'unList5',
                id: 'unList5'
            }, {
                xtype: 'checkboxfield',
                hidden: true,
                label: 'Delete',
                labelWidth: '40%',
                name: 'delPhone5',
                id: 'delPhone5'
            }]
        }]
    }
});