Ext.define('mobEdu.profile.view.studentInfo', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.profile.store.studentInfo'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td width="50%" align="right"><h2>Registered For Term </h2><td align="left"><h3>: {isRegistered}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>First Term Attended </h2></td><td align="left"><h3>: {firstTerm}</h3></td></tr>','<tpl if="(mobEdu.profi.f.isLastTermExists(lastTerm) === true)">', '<tr><td width="50%" align="right"><h2>Last Term Attended </h2></td><td align="left"><h3>: {lastTerm}</h3></td></tr>','</tpl>', '<tr><td width="50%" align="right"><h2>Status </h2></td><td align="left"><h3>: {studentStatus}</h3></td></tr>','<tpl if="(mobEdu.profi.f.isTermMatricExists(termCodeMatric) === true)">', '<tr><td width="50%" align="right"><h2>Matric Term </h2></td><td align="left"><h3>: {termCodeMatric}</h3></td></tr>','</tpl>', '<tr><td width="50%" align="right"><h2>Residence </h2></td><td align="left"><h3>: {residence}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Citizenship </h2></td><td align="left"><h3>: {citizen}</h3></td></tr>' +'<tr><td width="50%" align="right"><h2>Student Type </h2></td><td align="left"><h3>: {studentType}</h3></td></tr>','<tpl if="(mobEdu.profi.f.isExpGradExists(expGradeDate) === true)">','<tr><td width="50%" align="right"><h2>Expected Graduation Date </h2></td><td align="left"><h3>: {expGradeDate}</h3></td></tr>','</tpl>','</table>'),
            store: mobEdu.util.getStore('mobEdu.profile.store.studentInfo'),
            singleSelect: true,
            emptyText: '<center><h3>There is no information available for this term</h3><center>',
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Student Info</h1>',
            id: 'studentTitleBar'
        } ],
    flex:1
    }
});