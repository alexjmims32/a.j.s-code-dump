Ext.define('mobEdu.profile.store.confirmInfo', {
    extend: 'mobEdu.data.store',
    //    alias:'dropStore',

    requires: [
        'mobEdu.profile.model.bioInfo'
    ],
    config: {
        storeId: 'mobEdu.profile.store.confirmInfo',

        autoLoad: false,

        model: 'mobEdu.profile.model.bioInfo'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = ''
        return proxy;
    }
});