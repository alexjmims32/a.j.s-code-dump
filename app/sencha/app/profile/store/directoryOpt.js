Ext.define('mobEdu.profile.store.directoryOpt', {
  extend: 'mobEdu.data.store',

  requires: [
    'mobEdu.profile.model.studentProfile'
  ],
  config: {
    storeId: 'mobEdu.profile.store.directoryOpt',
    autoLoad: false,

    model: 'mobEdu.profile.model.studentProfile'
  },
  initProxy: function() {
    var proxy = this.callParent();
    proxy.reader.rootProperty = ''
    return proxy;
  }
});