Ext.define('mobEdu.profile.store.nationEnum', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.profile.model.enumerations'
    ],

    config: {
        storeId: 'mobEdu.profile.store.nationEnum',
        autoLoad: false,
        model: 'mobEdu.profile.model.enumerations',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enums'
            }
        }
    }

});