Ext.define('mobEdu.profile.store.updateLocal', {
    extend: 'Ext.data.Store',

    requires: [
        'mobEdu.profile.model.bioInfo'
    ],

    config: {
        storeId: 'mobEdu.profile.store.updateLocal',
        autoLoad: false,
        model: 'mobEdu.profile.model.bioInfo',
        proxy: {
            type: 'localstorage',
            id: 'updateLocal'
        }
    }
});