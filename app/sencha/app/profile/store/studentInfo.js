Ext.define('mobEdu.profile.store.studentInfo',{
extend: 'mobEdu.data.store',
   
   requires:[
       'mobEdu.profile.model.studentInfo'
   ],
   config:{
       storeId:'mobEdu.profile.store.studentInfo',
        autoLoad: false,

        model: 'mobEdu.profile.model.studentInfo'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'studentInfo'        
        return proxy;
    }
});