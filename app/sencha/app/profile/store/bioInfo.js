Ext.define('mobEdu.profile.store.bioInfo', {
    extend: 'mobEdu.data.store',

    requires: ['mobEdu.profile.model.bioInfo'],
    config: {
        storeId: 'mobEdu.profile.store.bioInfo',
        autoLoad: false,
        model: 'mobEdu.profile.model.bioInfo',
        remoteSort: true,
        grouper: {
            groupFn: function(record) {
                return record.get('addDesc') + ':';
            }
        },
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'ROWSET',
                record: 'RECORD'
            }
        }
    }
});