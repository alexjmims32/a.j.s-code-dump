Ext.define('mobEdu.profile.store.countyEnum', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.profile.model.enumerations'
    ],

    config: {
        storeId: 'mobEdu.profile.store.countyEnum',
        autoLoad: false,
        model: 'mobEdu.profile.model.enumerations',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enums'
            }
        }
    }

});