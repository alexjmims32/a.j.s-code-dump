Ext.define('mobEdu.profile.store.studentProfile', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.profile.model.studentProfile'
    ],

    config:{
        storeId: 'mobEdu.profile.store.studentProfile',
        autoLoad: false,
        model: 'mobEdu.profile.model.studentProfile',
        proxy:{
            type:'localstorage',
            id:'studentprofile'
        }
    }
});