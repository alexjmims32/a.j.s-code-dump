Ext.define('mobEdu.profile.store.myInfo', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.profile.model.myInfo'
    ],

    config:{
        storeId: 'mobEdu.profile.store.myInfo',
        autoLoad: false,
        model: 'mobEdu.profile.model.myInfo'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'personDetails';
        return proxy;
    }
});