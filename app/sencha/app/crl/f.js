Ext.define('mobEdu.crl.f', {
    requires: [
        'mobEdu.crl.store.curriculumInfo',
        'mobEdu.crl.store.holdsInfo',
        'mobEdu.crl.store.allHoldsInfo'
    ],
    statics: {
        preView: null,
        loadCurriculumInfo: function() {
            var cinfoStore = mobEdu.util.getStore('mobEdu.crl.store.curriculumInfo');
            cinfoStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            cinfoStore.getProxy().setUrl(webserver + 'myCurriculumInfo');
            cinfoStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });

            cinfoStore.getProxy().afterRequest = function() {
                mobEdu.crl.f.curriculumInfoResposeHandler();
            }
            cinfoStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'mycurriculuminfo');
        },

        curriculumInfoResposeHandler: function() {
            mobEdu.util.get('mobEdu.crl.view.curriculumInfo');
            //            if(flag=='holds'){
            //                Ext.getCmp('curriculumTitleBar').setTitle('<h1>Student Holds</h1>');
            //            }else{
            Ext.getCmp('curriculumTitleBar').setTitle('<h1>My Curriculum Info</h1>');
            //            }
            mobEdu.crl.f.showCurriculumInfo();
        },

        loadStudentHolds: function(preView) {
            if (preView == null) {
                mobEdu.crl.f.preView = mobEdu.util.showMainView;
            } else {
                mobEdu.crl.f.preView = preView;
            }
            var holdinfoStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
            holdinfoStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            holdinfoStore.getProxy().setUrl(webserver + 'getHoldsInfo');
            holdinfoStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });

            holdinfoStore.getProxy().afterRequest = function() {
                mobEdu.crl.f.studentHoldsResposeHandler();
            }
            holdinfoStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'holdsinfo');
        },

        holdsChecking: function() {
            var text;
            var holdsStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
            var holdsData = holdsStore.data.all;
            if (holdsData.length == 0 || holdsData.length == null) {
                text = 'You have no holds.';
                return text;
            }
        },

        studentHoldsResposeHandler: function() {
            //            mobEdu.util.get('mobEdu.crl.view.studentHolds');
            mobEdu.crl.f.showStudentHolds();
        },

        iscumulativeGpaExits: function(cumulativeGpa) {
            if (cumulativeGpa == null || cumulativeGpa == '' || cumulativeGpa == 'NA') {
                return false;
            } else {
                return true;
            }
        },
        isConcentrationExits: function(concentration) {
            if (concentration == null || concentration == '' || concentration == 'NA') {
                return false;
            } else {
                return true;
            }
        },
        showStudentHolds: function() {
            mobEdu.util.updatePrevView(mobEdu.crl.f.preView);
            var holdsStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
            var holdsData = holdsStore.data.all;
            if (holdsData.length == 0 || holdsData.length == null) {
                Ext.Msg.show({
                    title: null,
                    message: '<p>You have no holds.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
                mobEdu.util.show('mobEdu.crl.view.studentHolds');
            }
            //            mobEdu.util.updatePrevView(mobEdu.profile.f.showStudentProfileView);
            //            var holdsStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
            //            var holdsData = holdsStore.data.all;
            //            if(holdsData.length == 0 || holdsData.length == null){
            ////                Ext.getCmp('holds').setHidden(true);
            //                Ext.getCmp('holds').setEmptyText('<h3 align="center">You have no holds.</h3>');
            ////                var viewRef = mobEdu.util.get('mobEdu.crl.view.studentHolds');
            ////                viewRef.setHtml('<h3 align="center">You have no holds.</h3>');
            //            }else{
            ////                Ext.getCmp('holds').setHidden(false);
            //            }
        },

        showCurriculumInfo: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.crl.view.curriculumInfo');
        },
        hasHolds: function(holds) {
            var len = holds.length;
            if (len > 0) {
                return true;
            } else {
                return false;
            }
        },

        loadBillingTermList: function() {
            billingFlag = true;
            mobEdu.courses.f.loadRegTermList();
        },

        loadBillingInfo: function(termCode) {
            var binfoStore = mobEdu.util.getStore('mobEdu.crl.store.billingInfo');
            binfoStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            binfoStore.getProxy().setUrl(webserver + 'getBillingInfo');
            binfoStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                termCode: termCode,
                companyId: companyId
            });

            binfoStore.getProxy().afterRequest = function() {
                mobEdu.crl.f.billingInfoResposeHandler();
            }
            binfoStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'billinginfo');
        },

        billingInfoResposeHandler: function() {
            var binfoStore = mobEdu.util.getStore('mobEdu.crl.store.billingInfo');
            var data = binfoStore.data.first().data;
            if (data.billHours == null && data.creditHours == null && data.maxHours == null && data.minHours == null && data.termCode == null) {
                Ext.Msg.show({
                    message: '<p>No info available.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                mobEdu.crl.f.showBillingInfo();
            }
        },

        showBillingInfo: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.crl.view.billingInfo');
        },

        isExists: function(value) {
            if (value == 'false')
                return false;
            return true;
        },

        studentCheckHolds: function(preView) {
            if (preView == null) {
                mobEdu.crl.f.preView = mobEdu.util.showMainView;
            } else {
                mobEdu.crl.f.preView = preView;
            }
            var holdinfoStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
            holdinfoStore.removeAll();
            holdinfoStore.sync();
            holdinfoStore.removed = [];
            holdinfoStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            holdinfoStore.getProxy().setUrl(webserver + 'getHoldsInfo');
            holdinfoStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });

            holdinfoStore.getProxy().afterRequest = function() {
                mobEdu.crl.f.studentCheckHoldsResposeHandler();
            }
            holdinfoStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'holdsinfo');
        },

        studentCheckHoldsResposeHandler: function() {
            var holdsStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
            mobEdu.reg.f.register();
        },

        loadAllHolds: function() {
            var holdinfoStore = mobEdu.util.getStore('mobEdu.crl.store.allHoldsInfo');
            holdinfoStore.removeAll();
            holdinfoStore.sync();
            holdinfoStore.removed = [];
            holdinfoStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            holdinfoStore.getProxy().setUrl(webserver + 'getAllHoldsInfo');
            holdinfoStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });

            holdinfoStore.getProxy().afterRequest = function() {
                mobEdu.crl.f.allHoldsResposeHandler();
            }
            holdinfoStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'allHoldsInfo');
        },

        allHoldsResposeHandler: function() {
            var holdsStore = mobEdu.util.getStore('mobEdu.crl.store.allHoldsInfo');
            if (holdsStore.data.length > 0) {
                mobEdu.crl.f.showAllHolds();
            } else {
                Ext.Msg.show({
                    message: 'No Holds Info',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        showAllHolds: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.crl.view.allHoldsInfo');
        }
    }
});
