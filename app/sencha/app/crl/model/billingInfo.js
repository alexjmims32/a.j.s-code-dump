Ext.define('mobEdu.crl.model.billingInfo',{
   extend:'Ext.data.Model',

   config:{
       fields:[{name:'id'},
           {
               name : 'termCode',
               type : 'string'
           },
           {
               name : 'billHours',
               type : 'string'
           },
           {
               name : 'creditHours',
               type : 'string'
           },
           {
               name : 'maxHours',
               type : 'string'
           },
           {
               name : 'minHours',
               type : 'string'
           }
       ]
   }

});