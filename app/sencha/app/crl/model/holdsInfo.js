Ext.define('mobEdu.crl.model.holdsInfo',{
   extend:'Ext.data.Model',

   config:{
       fields:[
           {
               name : 'holdsDescription',
               type : 'string'
           },
           {
               name : 'holdAmount',
               type : 'string'
           },
           {
               name : 'holdReleaseIndicator',
               type : 'string'
           },
           {
               name : 'holdReason',
               type : 'string'
           },
           {
               name : 'holdOriginOffice',
               type : 'string'
           },
           {
               name : 'holdFromDate',
               type : 'string'
           },
           {
               name : 'holdToDate',
               type : 'string'
           },
           {
               name : 'processesAffected',
               type : 'string'
           }
       ]
   }

});