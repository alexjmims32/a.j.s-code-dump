Ext.define('mobEdu.crl.model.curriculumInfo', {
  extend: 'Ext.data.Model',

  config: {
    fields: [{
        name: 'id'
      }, {
        name: 'termCode',
        type: 'string'
      }, {
        name: 'level',
        type: 'string'
      }, {
        name: 'admitType',
        type: 'string'
      }, {
        name: 'college',
        type: 'string'
      }, {
        name: 'major',
        type: 'string'
      }, {
        name: 'program',
        type: 'string'
      }, {
        name: 'programDesc',
        type: 'string'
      }, {
        name: 'gpa',
        type: 'string'
      }, {
        name: 'earnedHours',
        type: 'string'
      }, {
        name: 'department',
        type: 'string'
      }, {
        name: 'catalogTerm',
        type: 'string'
      }, {
        name: 'campus',
        type: 'string'
      }, {
        name: 'holds',
        type: 'array',
        mapping: 'holdList.holds'
      }, {
        name: 'concentration',
        type: 'string'
      }, {
        name: 'cumulativeGpa',
        type: 'string',
        convert: function(value, record) {
          if (value == '' || value == null) {
            return '';
          }
          if (isNaN(value)) {
            return value;
          } else {
            value = parseFloat(value);
            value = value.toFixed(2);
            return value;
          }
        }
      }

    ]
  }

});