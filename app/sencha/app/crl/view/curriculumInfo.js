Ext.define('mobEdu.crl.view.curriculumInfo', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.crl.store.curriculumInfo'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td width="50%" align="right"><h2>Major :</h2></td><td align="left"><h3>{major}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Current Program :</h2></td><td align="left"><h3>{programDesc}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Level :</h2></td><td align="left"><h3>{level}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Department :</h2></td><td align="left"><h3>{department}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>College :</h2></td><td align="left"><h3>{college}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Admit Term :</h2></td><td align="left"><h3>{termCode}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Admit Type :</h2></td><td align="left"><h3>{admitType}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Catalog Term :</h2></td><td align="left"><h3>{catalogTerm}</h3></td></tr>' + '<tpl if="(mobEdu.crl.f.isConcentrationExits(concentration) === true)">', '<tr><td width="50%" align="right"><h2>Major Concentration :</h2></td><td align="left"><h3>{concentration}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.crl.f.iscumulativeGpaExits(cumulativeGpa) === true)">', '<tr><td width="50%" align="right"><h2>cumulative GPA :</h2></td><td align="left"><h3>{cumulativeGpa}</h3></td></tr>' + '</tpl>' + '<tpl if="(mobEdu.util.isShowCampus() === true)">', '<tr><td width="50%" align="right"><h2>Campus :</h2></td><td align="left"><h3>{campus}</h3></td></tr>' + '</tpl>' + '</table>'),
            store: mobEdu.util.getStore('mobEdu.crl.store.curriculumInfo'),
            singleSelect: true,
            emptyText: '<center><h3>There is no information available for this Student</h3><center>',
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>My Curriculum Info</h1>',
            id: 'curriculumTitleBar'
        }],
        flex: 1
    }
});