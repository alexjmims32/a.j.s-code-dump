Ext.define('mobEdu.crl.view.billingInfo', {
    extend: 'Ext.Panel',
    requires:[
    'mobEdu.crl.store.billingInfo'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td width="50%" align="right"><h2>Term Code :</h2></td><td align="left"><h3>{termCode}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Total Credit Hours :</h2></td><td align="left"><h3>{creditHours}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Billing Hours :</h2></td><td align="left"><h3>{billHours}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Minimum Hours :</h2></td><td align="left"><h3>{minHours}</h3></td></tr>' + '<tr><td width="50%" align="right"><h2>Maximum Hours :</h2></td><td align="left"><h3>{maxHours}</h3></td></tr>'+ '</table>'),
            store: mobEdu.util.getStore('mobEdu.crl.store.billingInfo'),
            singleSelect: true,
            emptyText: '<center><h3>There is no information available in the system</h3><center>',
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Credit Hours Info</h1>',
            id: 'billingInfoTitleBar'
        },{
            xtype: 'toolbar',
            id: 'phSumtBar',
            name: 'phSumtBar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                ui: 'button',
                text: 'Change Term',
                handler: function() {
                    mobEdu.crl.f.loadBillingTermList();
                }
            }]
        } ],
    flex:1
    }
});