Ext.define('mobEdu.crl.view.allHoldsInfo', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.crl.store.allHoldsInfo'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table>' + '<tr><td colspan="3"><h2>{holdsDescription}</h2></td></tr>' + '<tpl if="(mobEdu.crl.f.isExists(showHoldAmount)=== true)">', '<tr><td><h2>Amount</h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{holdAmount}</h3></td></tr>', '</tpl>' + '<tpl if="(mobEdu.crl.f.isExists(showHoldreleaseIndicator)=== true)">', '<tr><td><h2>Release Ind</h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{holdReleaseIndicator}</h3></td></tr>', '</tpl>' + '<tr><td><h2>Originating Office </h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{holdOriginOffice}</h3></td></tr>' + '<tr><td><h2>Date </h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{holdFromDate} to {holdToDate}</h3></td></tr>' + '</table>'),
            store: mobEdu.util.getStore('mobEdu.crl.store.allHoldsInfo'),
            singleSelect: true,
            emptyText: '<center><h3>You have no holds.</h3><center>',
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Student Holds</h1>'
        }],
        flex: 1
    }
});
