Ext.define('mobEdu.crl.view.studentHoldsGrid', {
    extend : 'Ext.ux.touch.grid.View',
    xtype  : 'holdsGrid',
	requires:[
		'mobEdu.crl.store.holdsInfo'
	],
    config : {
        title    : 'Grid',
        store    : true,
        columns  : [
            {
                header    : 'Description',
                dataIndex : 'holdsDescription',
                style     : 'text-align: left;padding-left: 1em;',
                width     : '30%',
                editor    : {
                    xtype  : 'textfield'
                }
            },{
                header    : 'Amount',
                dataIndex : 'holdAmount',
                style     : 'text-align: right;padding-right: 1em;',
                width     : '20%',
                editor    : {
                    xtype  : 'numberfield'
                }
            },
            {
                header    : 'Release Ind',
                dataIndex : 'holdReleaseIndicator',
                style     : 'text-align: right;padding-right: 1em;',
                width     : '20%',
                editor    : {
                    xtype  : 'textfield'
                }
            },
            {
                header    : 'Originating Office',
                dataIndex : 'holdOriginOffice',
                style     : 'text-align: right;padding-right: 1em;',
                width     : '30%',
                editor    : {
                    xtype  : 'textfield'
                }
            }
        ],
        features : [
            {
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    },

    applyStore : function() {
        return mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
    }
})
;
