Ext.define('mobEdu.crl.store.allHoldsInfo', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.crl.model.holdsInfo'
    ],
    config: {
        storeId: 'mobEdu.crl.store.allHoldsInfo',
        autoLoad: false,

        model: 'mobEdu.crl.model.holdsInfo'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'holdList.holds'
        return proxy;
    }
});
