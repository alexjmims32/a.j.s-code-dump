Ext.define('mobEdu.crl.store.curriculumInfo',{
extend: 'mobEdu.data.store',
   
   requires:[
       'mobEdu.crl.model.curriculumInfo'
   ],
   config:{
       storeId:'mobEdu.crl.store.curriculumInfo',
        autoLoad: false,

        model: 'mobEdu.crl.model.curriculumInfo'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'curriculumInfo'        
        return proxy;
    }
});