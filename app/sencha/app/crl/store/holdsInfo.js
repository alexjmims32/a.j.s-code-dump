Ext.define('mobEdu.crl.store.holdsInfo',{
extend: 'mobEdu.data.store',
   
   requires:[
       'mobEdu.crl.model.holdsInfo'
   ],
   config:{
       storeId:'mobEdu.crl.store.holdsInfo',
        autoLoad: false,

        model: 'mobEdu.crl.model.holdsInfo'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'holdList.holds'        
        return proxy;
    }
});