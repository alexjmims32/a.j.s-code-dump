Ext.define('mobEdu.crl.store.billingInfo',{
extend: 'mobEdu.data.store',
   
   requires:[
       'mobEdu.crl.model.billingInfo'
   ],
   config:{
       storeId:'mobEdu.crl.store.billingInfo',
        autoLoad: false,

        model: 'mobEdu.crl.model.billingInfo'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});