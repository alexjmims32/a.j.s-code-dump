Ext.define('mobEdu.reg.model.pinValidation', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'status'
            },
            {
                name:'statusMsg'
            }
        ]
    }
});