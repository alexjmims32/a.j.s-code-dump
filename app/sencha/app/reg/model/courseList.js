Ext.define('mobEdu.reg.model.courseList', {
    extend: 'Ext.data.Model',

    config: {

        fields: [{
            name: 'id'
        }, {
            name: 'registeredDate',
            type: 'string'
        }, {
            name: 'registeredBy',
            type: 'string'
        }, {
            name: 'statusMsg',
            type: 'string',
            mapping: 'statusMessage'
        }, {
            name: 'statusCode',
            type: 'string',
            mapping: 'statusCode'
        }, {
            name: 'courseNumber',
            type: 'string',
            mapping: 'course.courseNumber'
        }, {
            name: 'courseTitle',
            type: 'string',
            mapping: 'course.courseTitle'
        }, {
            name: 'subject',
            type: 'string',
            mapping: 'course.subject'
        }, {
            name: 'description',
            type: 'string',
            mapping: 'course.description'
        }, {
            name: 'level',
            type: 'string',
            mapping: 'course.level'
        }, {
            name: 'college',
            type: 'string',
            mapping: 'course.college'
        }, {
            name: 'period',
            type: 'string',
            mapping: 'course.period'
        }, {
            name: 'roomCode',
            type: 'string',
            mapping: 'course.roomCode'
        }, {
            name: 'termCode',
            type: 'string',
            mapping: 'course.termCode'
        }, {
            name: 'termDescription',
            type: 'string',
            mapping: 'course.termDescription'
        }, {
            name: 'scheduleType',
            type: 'string',
            mapping: 'course.scheduleType'
        }, {
            name: 'department',
            type: 'string',
            mapping: 'course.department'
        }, {
            name: 'campus',
            type: 'string',
            mapping: 'course.campus'
        }, {
            name: 'lectureHours',
            type: 'string',
            mapping: 'course.lectureHours'
        }, {
            name: 'crn',
            type: 'string',
            mapping: 'course.crn'
        }, {
            name: 'scheduleTime',
            type: 'string',
            mapping: 'course.scheduleTime'
        }, {
            name: 'faculty',
            type: 'string',
            mapping: 'course.faculty'
        }, {
            name: 'noOfSeatsAvailable',
            type: 'string',
            mapping: 'course.noOfSeatsAvailable'
        }, {
            name: 'totalSeats',
            type: 'string',
            mapping: 'course.totalSeats'
        }, {
            name: 'seatsAvailable',
            type: 'string',
            mapping: 'course.seatsAvailable'
        }, {
            name: 'waitedList',
            type: 'int',
            mapping: 'course.waitedList'
        }, {
            name: 'meeting',
            type: 'array',
            mapping: 'course.meetingList.meeting'
        }, {
            name : 'prerequisite',
            type : 'string',
            mapping:'course.prerequisite'
        },{
            name : 'prerequisiteArray',
            type : 'array',
            convert: function(value, record) {
                var itemList = new Array();
                var items = new Array();
                var prerequisites=record.get('prerequisite');
                if(prerequisites!=null && prerequisites!="undefined"){
                    itemList= prerequisites.split(':');
                    Ext.Array.each(itemList,function(item, index, itemsItSelf){
                        items.push({prerequisite:item});
                    });
                    return items;
                }
            }
        },{
            name: 'facultyEmail',
            type: 'string',
            mapping: 'course.facultyEmail'
        }, {
            name: 'courseComments',
            type: 'string',
            mapping: 'course.courseComments'
        }, {
            name: 'seqNo',
            type: 'string',
            mapping: 'course.seqNo'
        }, {
            name: 'credits',
            type: 'int',
            mapping: 'course.creditHrs'
        }, {
            name: 'linkId',
            type: 'string',
            mapping: 'course.linkId'
        }, {
            name: 'olrInd',
            type: 'string',
            mapping: 'course.olrInd'
        }]
    }
});