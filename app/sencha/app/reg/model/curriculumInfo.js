Ext.define('mobEdu.reg.model.curriculumInfo', {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'majorCodeDescription',
			mapping: 'MAJOR_CODE_DESCRIPTION',
			convert: function(value, record) {
				if (value == '' || value == null) {
					return '';
				} else
					return value;
			}
		}, {
			name: 'catalogYear',
			mapping: 'CATALOG_YEAR',
			convert: function(value, record) {
				if (value == '' || value == null) {
					return '';
				} else
					return value;
			}
		}, {
			name: 'majorCode',
			mapping: 'MAJOR_CODE',
			convert: function(value, record) {
				if (value == '' || value == null) {
					return '';
				} else
					return value;
			}
		}, {
			name: 'catalogTermCode',
			mapping: 'CATALOG_TERM_CODE',
			convert: function(value, record) {
				if (value == '' || value == null) {
					return '';
				} else
					return value;
			}
		}, {
			name: 'termCodeDescription',
			mapping: 'TERM_CODE_DESCRIPTION',
			convert: function(value, record) {
				if (value == '' || value == null) {
					return '';
				} else
					return value;
			}
		}, {
			name: 'termCode',
			mapping: 'TERM_CODE',
			convert: function(value, record) {
				if (value == '' || value == null) {
					return '';
				} else
					return value;
			}
		}, {
			name: 'seqNo',
			mapping: 'SEQ_NO'
		}, {
			name: 'defaultVal',
			mapping: 'DEFAULT'
		}, ]
	}
});