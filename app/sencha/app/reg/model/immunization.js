Ext.define('mobEdu.reg.model.immunization', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'html'
            },
            {
                name:'statusMsg'
            }
        ]
    }
});