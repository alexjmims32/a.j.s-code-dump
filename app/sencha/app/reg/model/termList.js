Ext.define('mobEdu.reg.model.termList', {
    extend: 'Ext.data.Model',
    config: {
        fields: [{
            name: 'code',
            type: 'string'
        }, {
            name: 'description',
            type: 'string'
        }, {
            name: 'defaultTermInd',
            type: 'string'
        }]
    }
});