Ext.define('mobEdu.reg.model.cartSchedule', {
    extend:'Ext.data.Model',

    config:{

        fields : [ {
            name:'id'
        },{
            name : 'registeredDate',
            type : 'string'
        },{
            name : 'registeredBy',
            type : 'string'
        },{
            name : 'statusMsg',
            type : 'string',
            mapping :'statusMessage'    
        },{
            name : 'statusCode',
            type : 'string',
            mapping : 'statusCode'
        },{
            name : 'courseNumber',
            type : 'string',
            mapping:'courseNumber'
        },{
            name : 'courseTitle',
            type : 'string',
            mapping:'courseTitle'
        },{
            name : 'subject',
            type : 'string',
            mapping:'subject'
        },{
            name : 'description',
            type : 'string',
            mapping:'description'
        },{
            name : 'level',
            type : 'string',
            mapping:'level'
        },{
            name : 'college',
            type : 'string',
            mapping:'college'
        },{
            name : 'period',
            type : 'string',
            mapping:'period'
        },{
            name : 'roomCode',
            type : 'string',
            mapping:'roomCode'
        },{
            name : 'termCode',
            type : 'string',
            mapping:'termCode'
        },{
            name : 'termDescription',
            type : 'string',
            mapping:'termDescription'
        },{
            name : 'scheduleType',
            type : 'string',
            mapping:'scheduleType'
        },{
            name : 'department',
            type : 'string',
            mapping:'department'
        },{
            name : 'campus',
            type : 'string',
            mapping:'campus'
        },{
            name : 'lectureHours',
            type : 'string',
            mapping:'lectureHours'
        },{
            name : 'crn',
            type : 'string',
            mapping:'crn'
        },{
            name : 'scheduleTime',
            type : 'string',
            mapping:'scheduleTime'
        },{
            name : 'faculty',
            type : 'string',
            mapping:'faculty'
        },{
            name : 'meeting',
            type : 'array',
            mapping:'meetingList.meeting'
        }
        ]
    }
});