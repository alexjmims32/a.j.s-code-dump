Ext.define('mobEdu.reg.model.list', {
	extend: 'Ext.data.Model',
	config: {
		fields: [{
			name: 'code',
			type: 'String'
		}, {
			name: 'description',
			type: 'String'
		}]
	}
});