Ext.define("mobEdu.reg.model.register", {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'statusCode',
            type: 'string'
        }, {
            name: 'courseTitle',
            type: 'string'
        }, {
            name: 'college',
            type: 'string'
        }, {
            name: 'crn',
            type: 'string'
        }, {
            name: 'department',
            type: 'string'
        }, {
            name: 'level',
            type: 'string'
        }, {
            name: 'statusMessage',
            type: 'string'
        }, {
            name: 'subject',
            type: 'string'
        }, {
            name: 'courseNumber',
            type: 'string'
        }, {
            name: 'faculty',
            type: 'string'
        }, {
            name: 'errorFlag',
            type: 'string'
        }, {
            name: 'termAndConditions',
            type: 'string'
        }]
    }
});