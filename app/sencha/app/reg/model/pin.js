Ext.define('mobEdu.reg.model.pin', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'termCode'
            },
            {
                name:'pinStatus'
            },
            {
                name:'eligibilityStatus'
            },{
                name:'value'
            }
        ]
    }
});