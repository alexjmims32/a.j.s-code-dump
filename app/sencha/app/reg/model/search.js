Ext.define("mobEdu.reg.model.search", {
    extend: 'Ext.data.Model',

    config: {

        fields: [{
            name: 'id'
        }, {
            name: 'status',
            type: 'string'
        }, {
            name: 'statusMessage',
            type: 'string'
        }, {
            name: 'courseNumber',
            type: 'string'
        }, {
            name: 'courseTitle',
            type: 'string'
        }, {
            name: 'subject',
            type: 'string'
        }, {
            name: 'description',
            type: 'string'
        }, {
            name: 'creditHrs',
            type: 'string',
            mapping: 'creditHrs'
        }, {
            name: 'level',
            type: 'string'
        }, {
            name: 'college',
            type: 'string'
        }, {
            name: 'period',
            type: 'string'
        }, {
            name: 'roomCode',
            type: 'string'
        }, {
            name: 'termCode',
            type: 'string'
        }, {
            name: 'termDescription',
            type: 'string'
        }, {
            name: 'lectureHours',
            type: 'string'
        }, {
            name: 'scheduleType',
            type: 'string'
        }, {
            name: 'department',
            type: 'string'
        }, {
            name: 'campus',
            type: 'string'
        }, {
            name: 'crn',
            type: 'string'
        }, {
            name: 'scheduleTime',
            type: 'string'
        }, {
            name: 'faculty',
            type: 'string'
        }, {
            name: 'meeting',
            type: 'array',
            mapping: 'meetingList.meeting'
        }, {
            name : 'prerequisite',
            type : 'string',
            mapping:'prerequisite'
        },{
            name : 'prerequisiteArray',
            type : 'array',
            convert: function(value, record) {
                var itemList = new Array();
                var items = new Array();
                var prerequisites=record.get('prerequisite');
                if(prerequisites!=null && prerequisites!="undefined"){
                    itemList= prerequisites.split(':');
                    Ext.Array.each(itemList,function(item, index, itemsItSelf){
                        items.push({prerequisite:item});
                    });
                    return items;
                }
            }
        },{
            name: 'seatsAvailable',
            type: 'string'
        }, {
            name: 'noOfSeatsAvailable',
            type: 'string'
        }, {
            name: 'totalSeats',
            type: 'string'
        }, {
            name: 'waitedList',
            type: 'int'
        }, {
            name: 'rstsCode',
            type: 'string'
        }, {
            name: 'courseIndicator',
            type: 'string'
        }, {
            name: 'campusCode',
            type: 'string',
            convert: function(value, record) {
                if (value == 'B') {
                    return 0;
                } else if (value == 'W') {
                    return 1;
                } else if (value == 'GML') {
                    return 2;
                } else {
                    return value;
                }
            }
        }, {
            name: 'seqNo',
            type: 'string'
        }, {
            name: 'linkId',
            type: 'string'
        }, {
            name: 'courseComments',
            type: 'string'
        }, {
            name: 'prereqcheck',
            type: 'string'
        }, {
            name: 'courseMeetings',
            type: 'string'
        }, {
            name: 'courseDates',
            type: 'string'
        }, {
            name: 'olrInd',
            type: 'string'
        }, {
            name: 'duration',
            type: 'string'
        }, {
            name: 'startDate',
            type: 'string'
        }, {
            name: 'endDate',
            type: 'string'
        }, {
            name: 'errMsg',
            type: 'string'
        }]
    }
});