Ext.define('mobEdu.reg.model.finResAgreement', {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'indicator'
		}, {
			name: 'status'
		}, {
			name: 'termDesc'
		}]
	}
});