Ext.define("mobEdu.reg.model.prerequisite", {
    extend:'Ext.data.Model',

  config:{
	fields : [ {
        name : 'subjectCode',
        type : 'string',
        mapping:'preRequisites.subjectCode'
    },{
        name : 'courseNumber',
        type : 'string',
        mapping:'preRequisites.courseNumber'
    },{
        name : 'courseTitle',
        type : 'string',
        mapping:'preRequisites.courseTitle'
    }]
  }
});