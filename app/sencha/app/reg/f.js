Ext.define('mobEdu.reg.f', {
	requires: [
		'mobEdu.reg.store.addCart',
		'mobEdu.reg.store.advSearch',
		'mobEdu.reg.store.cart',
		'mobEdu.reg.store.clearCart',
		'mobEdu.reg.store.courseDetail',
		'mobEdu.reg.store.courseDetailLocal',
		'mobEdu.reg.store.courseList',
		'mobEdu.reg.store.subjectList',
		'mobEdu.reg.store.courseLevelList',
		'mobEdu.reg.store.partOfTermList',
		'mobEdu.reg.store.scheduleTypeList',
		'mobEdu.reg.store.drop',
		'mobEdu.reg.store.eligibility',
		'mobEdu.reg.store.localCart',
		'mobEdu.reg.store.meetingScheduleLocal',
		'mobEdu.reg.store.pin',
		'mobEdu.reg.store.pinValidation',
		'mobEdu.reg.store.prerequisite',
		'mobEdu.reg.store.register',
		'mobEdu.reg.store.removeCart',
		'mobEdu.reg.store.restrictions',
		'mobEdu.reg.store.search',
		'mobEdu.reg.store.immunization',
		'mobEdu.reg.store.termList',
		'mobEdu.reg.store.viewCart',
		'mobEdu.crl.store.holdsInfo',
		'mobEdu.reg.store.campusList',
		'mobEdu.reg.store.instructionalMethodsList',
		'mobEdu.reg.store.curriculumInfo',
		'mobEdu.reg.store.olrStore',
		'mobEdu.reg.store.termsandConditions',
		'mobEdu.reg.store.finResAgreement'
	],
	statics: {
		loadCartTriggeredBy: null,
		prevViewRef: null,
		pinMsgCount: 1,
		selectedMajorCode: null,
		selectedCatalogCode: null,
		selectedGradCode: null,
		item: null,
		startDate: null,
		endDate: null,
		finResAgreement: false,

		preLoadRegistration: function() {
			if (immunizationEnabled == 'true') {
				mobEdu.main.f.immunizationNextView = mobEdu.reg.f.loadRegistration;
				mobEdu.reg.f.loadImmunization();
			} else {
				mobEdu.reg.f.loadRegistration();
			}
		},

		loadImmunization: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.immunization');
			store.getProxy().setUrl(webserver + 'getImmunizationContent');
			if (mobEdu.util.getAuthString() != null && mobEdu.util.getAuthString() != '') {
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
			} else {
				store.getProxy().setHeaders({
					companyID: companyId
				});
			}
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.immunizationResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'immunization');
		},

		immunizationResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.immunization');
			var html = store.getAt(0).get('html');
			if (html == null || html == '') {
				(mobEdu.main.f.immunizationNextView)();
			} else {
				mobEdu.reg.f.showImmunization();
				var text = mobEdu.util.formatUrls(html);
				Ext.getCmp('immunization').setHtml('<p>' + text + '</p>');
				var aquesLength = document.getElementsByName('a_ques').length;
				var bquesLength = document.getElementsByName('b_ques').length;
				if (aquesLength == 0 && bquesLength == 0) {
					Ext.getCmp('immuBtmBar').hide();
				} else {
					if (aquesLength > 0) {
						Ext.getCmp('immuBtmBar').show();
						var imgTag1 = '<a href="javascript:mobEdu.reg.f.onImgTap(\'aques1\',\'aques2\');"><img style="vertical-align:middle;margin-bottom: .25em" width="20px" height="20px" src="' + mobEdu.util.getResourcePath() + 'images/radioOff.png" id="aques1" title="off"/></a>';
						var imgTag2 = '<a href="javascript:mobEdu.reg.f.onImgTap(\'aques2\',\'aques1\');"><img style="vertical-align:middle;margin-bottom: .25em" width="20px" height="20px" src="' + mobEdu.util.getResourcePath() + 'images/radioOff.png" id="aques2" title="off"/></a>';
						text = text.replace(/<input type="radio".*?>/, imgTag1);
						text = text.replace(/<input type="radio".*?>/, imgTag2);
					}
					if (bquesLength > 0) {
						Ext.getCmp('immuBtmBar').show();
						var imgTag3 = '<a href="javascript:mobEdu.reg.f.onImgTap(\'bques1\',\'bques2\');"><img style="vertical-align:middle;margin-bottom: .25em" width="20px" height="20px" src="' + mobEdu.util.getResourcePath() + 'images/radioOff.png" id="bques1" title="off"/></a>';
						var imgTag4 = '<a href="javascript:mobEdu.reg.f.onImgTap(\'bques2\',\'bques1\');"><img style="vertical-align:middle;margin-bottom: .25em" width="20px" height="20px" src="' + mobEdu.util.getResourcePath() + 'images/radioOff.png" id="bques2" title="off" /></a>';
						text = text.replace(/<input type="radio".*?>/, imgTag3);
						text = text.replace(/<input type="radio".*?>/, imgTag4);
					}
				}
				Ext.getCmp('immunization').setHtml('<p>' + text + '</p>');
			}
		},

		onImgTap: function(id, pid) {
			var ele = document.getElementById(id);
			var pele = document.getElementById(pid);
			if (ele.title == 'off') {
				ele.src = mobEdu.util.getResourcePath() + 'images/radioOn.png';
				ele.title = 'on';
				if (pele.title == 'on') {
					pele.src = mobEdu.util.getResourcePath() + 'images/radioOff.png';
					pele.title = 'off';
				}
			}
		},

		submitImmunization: function() {
			var aquesExists = 'N',
				bquesExists = 'N';
			var aques, bques;
			if (document.getElementById('aques1') != null) {
				aquesExists = 'Y'
			}
			if (document.getElementById('bques1') != null) {
				bquesExists = 'Y'
			}

			if (aquesExists == 'Y' && bquesExists == 'Y') {
				if ((document.getElementById('aques1').title == 'off' && document.getElementById('aques2').title == 'off') ||
					(document.getElementById('bques1').title == 'off' && document.getElementById('bques2').title == 'off')) {
					Ext.Msg.show({
						message: 'You are required to select one of the statements.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					aques = mobEdu.reg.f.getElementInfo('aques1');
					bques = mobEdu.reg.f.getElementInfo('bques1');
					mobEdu.reg.f.submitService(aques, bques);
				}
			} else if (aquesExists == 'Y') {
				if ((document.getElementById('aques1').title == 'off' && document.getElementById('aques2').title == 'off')) {
					Ext.Msg.show({
						message: 'You are required to select one of the statements.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					aques = mobEdu.reg.f.getElementInfo('aques1');
					mobEdu.reg.f.submitService(aques, bques);
				}
			} else if (bquesExists == 'Y') {
				if ((document.getElementById('bques1').title == 'off' && document.getElementById('bques2').title == 'off')) {
					Ext.Msg.show({
						message: 'You are required to select one of the statements.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					bques = mobEdu.reg.f.getElementInfo('bques1');
					mobEdu.reg.f.submitService(aques, bques);
				}
			}
		},

		getElementInfo: function(id) {
			if (document.getElementById(id).title == 'on') {
				return ('Y');
			} else {
				return ('N');
			}
		},

		submitService: function(aques, bques) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.immunization');
			store.getProxy().setUrl(webserver + 'submitImmunizationContent');
			if (mobEdu.util.getAuthString() != null && mobEdu.util.getAuthString() != '') {
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
			} else {
				store.getProxy().setHeaders({
					companyID: companyId
				});
			}
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				aques: aques,
				bques: bques,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.submitImmunizationResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'submitImmunization');
		},

		submitImmunizationResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.immunization');
			var html = store.getAt(0).get('statusMsg');
			if (html == null || html == '') {
				(mobEdu.main.f.immunizationNextView)();
			} else {
				Ext.Msg.show({
					message: 'Server error has occurred.<br> Please try again',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadRegistration: function() {

			if (Ext.os.is.Phone) {
				mobEdu.util.get('mobEdu.reg.view.phone.search');
			} else {
				mobEdu.util.get('mobEdu.reg.view.tablet.search');
			}

			Ext.getCmp('searchItem').setValue('');
			searchFalg = false;
			if (Ext.getCmp('pagingplugin') != null) {
				Ext.getCmp('pagingplugin').getLoadMoreCmp().hide();
			}
			Ext.getCmp('searchRegList').setPlugins(null);
			Ext.getCmp('searchRegList').setEmptyText(null);
			mobEdu.reg.f.clearLocalSearchStore();

			mobEdu.reg.f.loadTermList('mobEdu.reg.f.showSearch');
		},

		loadTermList: function(nextView) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.termList');
			store.getProxy().setUrl(webserver + 'getRegistrationTerms');
			if (mobEdu.util.getAuthString() != null && mobEdu.util.getAuthString() != '') {
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
			} else {
				store.getProxy().setHeaders({
					companyID: companyId
				});
			}
			store.getProxy().setExtraParams({
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.termListResponseHandler(nextView);
			}
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'registrationterms');
		},

		termListResponseHandler: function(nextView) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.termList');
			if (store.data.length == 0) {
				Ext.Msg.show({
					message: '<p>No terms available for registration.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				eval(nextView + '()');
			}

		},

		validate: function(crseRec, item) {
			pinFlag = 'add';
			if (pinFlag == 'add') {
				mobEdu.reg.f.checkPin(crseRec, item);
			} else if (pinFlag == 'drop') {
				mobEdu.courses.f.doDrop(crseRec);
			} else {
				pinFlag = 'add';
				mobEdu.reg.f.checkPin(crseRec, item);
			}
		},

		checkPin: function(crseRec, item) {
			var pinValidationStore = mobEdu.util.getStore('mobEdu.reg.store.pinValidation');
			var termCode;
			pinValidationStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (pinFlag == 'register') {
				termCode = mobEdu.util.getCartTermCode();
			} else {
				termCode = crseRec.data.termCode;
			}
			pinValidationStore.getProxy().setUrl(webserver + 'checkPin');
			pinValidationStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				companyId: companyId
			});
			pinValidationStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.checkPinResponseHandler(crseRec, termCode, item);
			};
			pinValidationStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'checkpin');
		},
		checkPinResponseHandler: function(crseRec, termCode, item) {
			var pinValidationStore = mobEdu.util.getStore('mobEdu.reg.store.pinValidation');
			var pinStatus = pinValidationStore.data.items[0].data;
			var pinValue = null;
			if (pinStatus.status == 'Y') {
				// mobEdu.reg.f.pinMsgCount = 1;
				var pinStore = mobEdu.util.getStore('mobEdu.reg.store.pin');
				var pinRec = pinStore.findRecord('termCode', termCode);
				if (pinRec != null) {
					if (pinRec.data.pinStatus == 'success') {
						pinValue = pinRec.data.value;
					}
				}
				if (pinValue == null || pinValue == '') {
					mobEdu.reg.f.showAlert(crseRec, item);
				} else {
					mobEdu.reg.f.validatePin(pinValue, crseRec, item);
				}
			} else if (pinStatus.status == 'N' && (pinStatus.statusMsg == null || pinStatus.statusMsg == '')) {
				mobEdu.reg.f.addPinStatus(termCode, null);
				if (pinFlag == 'register') {
					mobEdu.reg.f.doRegister();
				} else if (pinFlag == 'drop') {
					mobEdu.reg.f.checkEligibilityForDrop(crseRec, item);
				} else {
					mobEdu.reg.f.checkEligibility(crseRec, item);
				}
			} else {
				Ext.Msg.show({
					message: pinStatus.statusMsg,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showAlert: function(crseRec, item) {
			Ext.Msg.show({
				title: 'Enter the alternate pin.',
				prompt: {
					xtype: 'numberfield',
					inputCls: 'hideInput'
				},
				msg: '',
				cls: 'msgbox',
				buttons: Ext.MessageBox.OKCANCEL,
				fn: function(btn, pinValue) {
					if (btn == 'ok') {
						if (pinValue == "") {
							Ext.Msg.show({
								message: '<p>Please enter the pin.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox',
								fn: function(btn) {
									mobEdu.reg.f.showAlert(crseRec, item);
								}
							});
						} else {
							mobEdu.reg.f.validatePin(pinValue, crseRec, item);
						}
					}
				}
			});
		},

		validatePin: function(pinValue, crseRec, item) {
			var termCode;
			var pinValidationStore = mobEdu.util.getStore('mobEdu.reg.store.pinValidation');
			pinValidationStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (pinFlag == 'register') {
				termCode = mobEdu.util.getCartTermCode();
			} else {
				termCode = crseRec.data.termCode;
			}
			pinValidationStore.getProxy().setUrl(webserver + 'validatePin');
			pinValidationStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				pin: pinValue,
				processname: "",
				companyId: companyId
			});
			pinValidationStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.validatePinResponseHandler(crseRec, termCode, item, pinValue);
			};
			pinValidationStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'validatepin');
		},
		validatePinResponseHandler: function(crseRec, termCode, item, pinValue) {
			var pinValidationStore = mobEdu.util.getStore('mobEdu.reg.store.pinValidation');
			var pinStatus = pinValidationStore.data.items[0].data;
			if (pinStatus.status == 'Y') {
				mobEdu.reg.f.addPinStatus(termCode, pinValue);
				if (pinFlag == 'register') {
					mobEdu.reg.f.doRegister();
				} else if (pinFlag == 'drop') {
					mobEdu.reg.f.checkEligibilityForDrop(crseRec, item);
				} else {
					mobEdu.reg.f.checkEligibility(crseRec, item);
				}
			} else {
				Ext.Msg.show({
					message: pinStatus.statusMsg,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						pinMsgCount = parseInt(pinMsgCount);
						if (mobEdu.reg.f.pinMsgCount < pinMsgCount) {
							mobEdu.reg.f.pinMsgCount = mobEdu.reg.f.pinMsgCount + 1;
							Ext.Msg.show({
								title: 'Enter the alternate pin',
								prompt: {
									xtype: 'passwordfield'
								},
								msg: '',
								cls: 'msgbox',
								buttons: Ext.MessageBox.OKCANCEL,
								fn: function(btn, pinValue) {
									if (btn == 'ok') {
										if (pinValue == "") {
											Ext.Msg.show({
												message: '<p>Please enter the pin.</p>',
												buttons: Ext.MessageBox.OK,
												cls: 'msgbox',
												fn: function(btn) {
													mobEdu.reg.f.showAlert(crseRec, item);
												}
											});
										} else {
											mobEdu.reg.f.validatePin(pinValue, crseRec, item);
										}
									}
								}
							});
						} else {
							mobEdu.reg.f.pinMsgCount = 1;
						}
					}
				});
			}
		},

		checkEligibility: function(crseRec, item) {
			var regTermCode = crseRec.data.termCode;
			var eliStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			eliStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eliStore.getProxy().setUrl(webserver + 'getEligibility');
			eliStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: regTermCode,
				companyId: companyId
			});
			eliStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.eligibilityResponseHandler(crseRec, item);
			}
			eliStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'eligibility');
		},

		eligibilityResponseHandler: function(crseRec, item) {
			var eliStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			var eliStatus = eliStore.getProxy().getReader().rawData;
			if (eliStatus.status != '' && eliStatus.status != null) {
				Ext.Msg.show({
					title: 'Eligibility violation.',
					message: eliStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				//                mobEdu.reg.f.updateEligibilityStatus(crseRec);
				if (pinFlag == 'add') {
					mobEdu.reg.f.addItemToCart(crseRec, item);
				} else if (pinFlag == 'drop') {
					mobEdu.courses.f.doDropMultipleCourses(crseRec);
				} else if (pinFlag == 'updateCredits') {
					mobEdu.courses.f.getVariableCreditHrs();
				}
			}
		},

		checkEligibilityForDrop: function(crseRec, item) {
			var regTermCode = crseRec.data.termCode;
			var eliStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			eliStore.removeAll();
			eliStore.sync();
			eliStore.removed = [];
			eliStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eliStore.getProxy().setUrl(webserver + 'getEligibility');
			eliStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: regTermCode,
				companyId: companyId
			});
			eliStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.eligibilityResponseHandlerForDrop(crseRec, item);
			}
			eliStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'eligibility');
		},

		eligibilityResponseHandlerForDrop: function(crseRec, item) {
			var eliStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			var eliData = eliStore.data.all[0];
			var eliStatus = eliData.data.status;
			if (eliStatus !== '' && eliStatus !== null) {
				Ext.Msg.show({
					title: 'Eligibility violation.',
					message: eliStatus,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.courses.f.doDropMultipleCourses(crseRec);
			}
		},
		getsearchResults: function(searchItemCmp, searchListCmp, termIdCmp) {
			var searchKey = '';
			var regTermCode = '';
			searchKey = searchItemCmp.getValue();
			regTermCode = termIdCmp.getValue();

			var store = mobEdu.util.getStore('mobEdu.reg.store.search');
			if (mobEdu.util.getAuthString() != null && mobEdu.util.getAuthString() != '') {
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
			} else {
				store.getProxy().setHeaders({
					companyID: companyId
				});
			}
			store.getProxy().setUrl(webserver + 'search');
			store.getProxy().setExtraParams({
				termCode: regTermCode,
				query: searchKey,
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.searchResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'search');
		},

		searchResponseHandler: function() {
			removeFromCartFlag = 'courseList';
			var store = mobEdu.util.getStore('mobEdu.reg.store.search');
			var status = store.getProxy().getReader().rawData.status;
			if (status.toUpperCase() != 'SUCCESS') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			if (store.data.length == 0) {
				if (mobEdu.util.currentModule == 'REG') {
					Ext.getCmp('searchRegList').setEmptyText('<h3 align="center">No courses available</h3>');
				} else {
					Ext.getCmp('searchCatList').setEmptyText('<h3 align="center">No courses available</h3>');
				}
			} else {
				if (mobEdu.util.currentModule == 'REG') {
					Ext.getCmp('searchRegList').setEmptyText(null);
				} else {
					Ext.getCmp('searchCatList').setEmptyText(null);
				}
			}
		},

		searchDetail: function(selectedItemIndex, viewRef, record, flag, prevViewRef) {
			mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);

			var store = mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal');
			store.removeAll();
			store.removed = [];
			store.sync();
			this.getCourseInfo(record.data.termCode, record.data.crn, 'search');
			if (flag == 'catlog') {
				mobEdu.catlogue.f.showSearchDetail();
			} else {
				mobEdu.reg.f.showSearchDetail(prevViewRef);
				var recExixtsFlag = mobEdu.reg.f.checkingCart(record.data.termCode, record.data.crn);
				if (recExixtsFlag == false) {
					Ext.getCmp("addToCart").show();
				} else {
					Ext.getCmp("addToCart").hide();
				}
			}

		},

		checkingCart: function(termCode, crn) {
			var lCartStore = mobEdu.util.getStore('mobEdu.reg.store.localCart');
			var status = false;
			lCartStore.each(function(record) {
				if (record.data.termCode == termCode && record.data.crn == crn)
					status = true;
			});
			return status;
		},

		hasSeatsAvailable: function(seatsAvailable, waitList) {
			if (seatsAvailable == 'NO' && waitList == 0) {
				return 0;
			} else if (seatsAvailable == 'NO' && waitList > 0) {
				return 1;
			} else {
				return 2;
			}
		},

		onViewCartItemTap: function(view, index, target, record, e, o) {
			if (e.target.title == "remove") {
				mobEdu.reg.f.removeItemFromCart(index, view, e, record);
			} else {
				if (Ext.os.is.Phone) {
					mobEdu.reg.f.searchDetail(index, view, record, null, mobEdu.reg.f.showViewCart);
				} else {
					mobEdu.reg.f.getCourseInfo(record.data.termCode, record.data.crn);
					var crseDetailStore = mobEdu.util.getStore('mobEdu.reg.store.courseDetail');
					if (crseDetailStore.data.length > 0) {
						record = crseDetailStore.data.items[0];

					}
				}

			}
		},
		getCourseInfo: function(termCode, crn, flag) {
			var crseDetailStore = mobEdu.util.getStore('mobEdu.reg.store.courseDetail');
			if (mobEdu.util.getAuthString() != null && mobEdu.util.getAuthString() != '') {
				crseDetailStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
			} else {
				crseDetailStore.getProxy().setHeaders({
					companyID: companyId
				});
			}
			crseDetailStore.getProxy().setUrl(webserver + 'getCourse');
			crseDetailStore.getProxy().setExtraParams({
				termCode: termCode,
				crn: crn,
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			crseDetailStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.courseInfoResponseHandler(flag);
			};
			crseDetailStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'course');
		},

		courseInfoResponseHandler: function(flag) {
			var crseDetailStore = mobEdu.util.getStore('mobEdu.reg.store.courseDetail');
			if (crseDetailStore.data.length > 0) {
				var record = crseDetailStore.data.items[0];
				var store = mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal');
				store.removeAll();
				store.add(record);
				store.sync();
			}

		},

		onSearchCourseItemTap: function(view, index, target, item, e, o, flag, viewRef) {
			addToCartFlag = 'courseList';
			mobEdu.util.deselectSelectedItem(index, view);
			var record = view.getStore().getAt(index);
			if (showFinResAgreement === 'true' && mobEdu.util.currentModule === 'REG') {
				mobEdu.reg.f.checkFinResAgreement(record, view, index, target, item, e, o, flag, viewRef);
			} else {
				if (item.target.title == "add") {
					mobEdu.reg.f.pinMsgCount = 1;
					mobEdu.reg.f.validate(record, item);
				} else if (item.target.title == "remove") {
					mobEdu.reg.f.removeFromCart(record, item);
				} else {
					mobEdu.reg.f.searchDetail(index, view, record, flag, viewRef);
				}
			}
		},

		onAdvSearchCourseItemTap: function(view, index, target, item, e, o, flag, viewRef) {
			addToCartFlag = 'advcourseList';
			mobEdu.util.deselectSelectedItem(index, view);
			var record = view.getStore().getAt(index);
			if (showFinResAgreement === 'true') {
				mobEdu.reg.f.checkFinResAgreement(record, view, index, target, item, e, o, flag, viewRef);
			} else {
				if (item.target.title == "add") {
					mobEdu.reg.f.pinMsgCount = 1;
					mobEdu.reg.f.validate(record, item);
				} else if (item.target.title == "remove") {
					removeFromCartFlag = 'advcourseList';
					mobEdu.reg.f.removeFromCart(record, item);
				} else {
					mobEdu.reg.f.searchDetail(index, view, record, flag, viewRef);
				}
			}
		},

		checkFinResAgreement: function(record, view, index, target, item, e, o, flag, viewRef) {
			var finResAgreementStore = mobEdu.util.getStore('mobEdu.reg.store.finResAgreement');
			finResAgreementStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			finResAgreementStore.getProxy().setUrl(webserver + 'getFinResAgreementInd');
			finResAgreementStore.getProxy().setExtraParams({
				termCode: record.get('termCode'),
				studentId: mobEdu.main.f.getStudentId(),
				action: 'GET'
			});
			finResAgreementStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.checkFinResAgreementResponseHandler(record, view, index, target, item, e, o, flag, viewRef);
			};
			finResAgreementStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'getFinResAgreementInd');
		},

		checkFinResAgreementResponseHandler: function(record, view, index, target, item, e, o, flag, viewRef) {
			var finResAgreementStore = mobEdu.util.getStore('mobEdu.reg.store.finResAgreement');
			if (finResAgreementStore.data.length > 0) {
				var finRecord = finResAgreementStore.data.all[0];
				var termDesc = finRecord.data.termDesc;
				if (finRecord.data.indicator !== 'Y') {
					mobEdu.reg.f.showFinResAgreement(record, termDesc);
				} else {
					if (addToCartFlag === 'courseList') {
						if (item.target.title == "add") {
							mobEdu.reg.f.pinMsgCount = 1;
							mobEdu.reg.f.validate(record, item);
						} else if (item.target.title == "remove") {
							removeFromCartFlag = 'advcourseList';
							mobEdu.reg.f.removeFromCart(record, item);
						} else {
							mobEdu.reg.f.searchDetail(index, view, record, flag, viewRef);
						}
					} else {
						if (item.target.title == "add") {
							mobEdu.reg.f.pinMsgCount = 1;
							mobEdu.reg.f.validate(record, item);
						} else if (item.target.title == "remove") {
							removeFromCartFlag = 'advcourseList';
							mobEdu.reg.f.removeFromCart(record, item);
						} else {
							mobEdu.reg.f.searchDetail(index, view, record, flag, viewRef);
						}
					}
				}
			}
		},

		showFinResAgreement: function(record, termDesc) {
			mobEdu.reg.f.finResAgreementRecord = record;
			mobEdu.util.show('mobEdu.reg.view.finResAgreement');
			Ext.getCmp('finResAgreementData').setHtml('<h2><b>Acknowledgment of Financial Responsibility and Promise to pay for ' + termDesc + '</b></h2><br><ol class="numberslist"><li>I understand that by submitting course registration, I am incurring a legal obligation to pay all tuition and fees associated with this registration. </li><br><li> I acknowledge that I am accepting the responsibility for payment of all charges even if I become ineligible for any or all of my financial aid. I further understand that I must begin attendance and fully complete all of my classes to qualify for my grants, loans and scholarships this term. </li><br><li> I understand that my Wentworth E-Bill will be posted online through L-Connect, and that all billing notices will be sent to my Wentworth assigned email address. It is my responsibility to review and pay my bill on or before the published due date on the E-Bill. </li><br><li> I acknowledge that failure to drop class(es) by the end of the Add/Drop registration period will not relieve me of the financial obligation for those class(es). </li><br><li> I understand that if I later decide NOT to attend these classes, I must officially withdraw, in writing, by the last date of withdrawal. The withdrawal form must be submitted to the Student Service Center and the WIT laptop must be returned to DTS. Any balance due as a result of financial aid adjustments will become due immediately. <u><b>NON-ATTENDANCE DOES NOT CONSTITUTE OFFICIAL WITHDRAWAL</b></u>. </li><br><li> I give Wentworth permission to alert me on important information by live or automated phone calls to any of the phone numbers I have provided. </li><br><li> I understand that account balance(s) not paid when due are subject to a late fee, a hold preventing registration and access to grades and transcripts, and/or deregistration or housing cancellation. Seriously delinquent accounts may be placed with an outside collection agency. I understand that if my account is sent to an outside collection agency, I will be liable to pay all late payment fees, collection charges, attorney fees, court fees, interest and any other costs and charges necessary for the collection of any amount not paid when due. </li><br><li><u><b> I acknowledge and agree that this Acknowledgment of Financial Responsibility and Promise to Pay is a promissory note, a legal debt instrument giving Wentworth Institute of Technology the right to take legal action to collect the debt I have promised to pay.</b></u></li></ol> ');
			if (addToCartFlag === 'courseList') {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			} else if (addToCartFlag === 'advcourseList') {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showAdvancedSearchResult);
			}
		},

		loadFinResAgreement: function() {
			var record = mobEdu.reg.f.finResAgreementRecord;
			var finResAgreementStore = mobEdu.util.getStore('mobEdu.reg.store.finResAgreement');
			finResAgreementStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			finResAgreementStore.getProxy().setUrl(webserver + 'getFinResAgreementInd');
			finResAgreementStore.getProxy().setExtraParams({
				termCode: record.get('termCode'),
				studentId: mobEdu.main.f.getStudentId(),
				action: 'SUBMIT'
			});
			finResAgreementStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.finResAgreementResponseHandler();
			};
			finResAgreementStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'getFinResAgreementInd');
		},

		finResAgreementResponseHandler: function() {
			var finResAgreementStore = mobEdu.util.getStore('mobEdu.reg.store.finResAgreement');
			if (finResAgreementStore.data.length > 0) {
				var record = finResAgreementStore.data.all[0];
				if (record.data.status === 'SUCCESS') {
					if (addToCartFlag === 'courseList') {
						mobEdu.reg.f.showSearch();
					} else if (addToCartFlag === 'advcourseList') {
						mobEdu.reg.f.showAdvancedSearchResult();
					}
				} else {
					Ext.Msg.show({
						message: record.data.status,
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}
		},


		removeFromCart: function(record, item, index) {
			var crn = record.data.crn;
			var regTermCode = record.data.termCode;
			var rcartStore = mobEdu.util.getStore('mobEdu.reg.store.removeCart');
			rcartStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			rcartStore.getProxy().setUrl(webserver + 'removeFromCart');
			rcartStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: regTermCode,
				crn: crn,
				companyId: companyId
			});
			rcartStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.removeCartResponseHandler(record, item, index);
			};
			rcartStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'removefromcart');
		},

		removeCartResponseHandler: function(cartRecord, item, index) {
			var rcartStore = mobEdu.util.getStore('mobEdu.reg.store.removeCart');
			var rCartStore = rcartStore.getProxy().getReader().rawData;

			if (rCartStore.status == 'SUCCESS') {
				var crseDetailStore = mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal');
				crseDetailStore.removeAll();
				crseDetailStore.sync();
				crseDetailStore.removed = [];

				var lCartStore = mobEdu.util.getStore('mobEdu.reg.store.localCart');
				lCartStore.each(function(record) {
					if (record.data.termCode == cartRecord.data.termCode && record.data.crn == cartRecord.data.crn) {
						lCartStore.remove(record);
						lCartStore.sync();
						lCartStore.removed = [];
					}
				});

				if (removeFromCartFlag == 'courseList') {
					item.target.setAttribute("class", "addcart");
					item.target.title = "add";
					Ext.getCmp('searchRegList').refresh();

				} else if (removeFromCartFlag == 'advcourseList') {
					item.target.setAttribute("class", "addcart");
					item.target.title = "add";
					Ext.getCmp('advSearchRegList').refresh();

				} else if (removeFromCartFlag == 'viewCart') {

					var vCartStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
					vCartStore.getProxy().setUrl('');
					vCartStore.remove(cartRecord);
					vCartStore.removed = [];
					if (vCartStore.data.length == 0) {
						setTimeout(function() {
							var searchKey = Ext.getCmp('searchItem').getValue();
							var regTermCode = Ext.getCmp('instSearchTermId').getValue();
							var store = mobEdu.util.getStore('mobEdu.reg.store.search');
							store.currentPage = 1;
							store.getProxy().setUrl(webserver + 'search');
							store.getProxy().setExtraParams({
								termCode: regTermCode,
								query: searchKey,
								studentId: mobEdu.main.f.getStudentId(),
								companyId: companyId
							});
							store.load();
							mobEdu.util.gaTrackEvent('enroll', 'search');
							mobEdu.reg.f.showSearch();
						}, 1000);
					} else {
						var cartRec = vCartStore.getAt(0);
						if (cartRec != null) {
							if (Ext.os.is.Phone) {
								Ext.getCmp('pViewCartList').refresh();
							} else {
								Ext.getCmp('tViewCartList').refresh();
								Ext.getCmp('tViewCartList').select(0);
							}
							mobEdu.reg.f.getCourseInfo(cartRec.data.termCode, cartRec.data.crn);
						}
					}
				}

			} else {
				Ext.Msg.show({
					message: rCartStore.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		addItemToCart: function(record, item) {
			var seatsAvailable = record.data.seatsAvailable;
			var waitList = record.data.waitedList;
			var olrInd = record.data.olrInd;
			if (seatsAvailable == 'NO' && waitList == 0) {
				if (studentCapPermitted === 'true') {
					Ext.Msg.show({
						title: null,
						cls: 'msgbox',
						message: '<p>There are no seats available. You still want to add this course to cart?</p>',
						buttons: Ext.MessageBox.YESNO,
						fn: function(btn) {
							if (btn == 'yes') {
								record.data.rstsCode = 'RW';
								mobEdu.reg.f.addToCart(record, item);
							}
						}
					});
				} else {
					Ext.Msg.show({
						title: null,
						cls: 'msgbox',
						message: '<p>Seats are closed.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								var addbtn = Ext.getCmp("addToCart");
								if (addbtn != null) {
									addbtn.show();
								}
							}
						}
					});
				}
			} else if (seatsAvailable == 'NO' && waitList > 0) {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<p>Seats are closed. Would you like to be added to the waiting list?</p>',
					buttons: Ext.MessageBox.YESNO,
					fn: function(btn) {
						if (btn == 'yes') {
							record.data.rstsCode = 'WL';
							mobEdu.reg.f.addToCart(record, item);
						} else if (btn == 'no') {
							var addbtn = Ext.getCmp("addToCart");
							if (addbtn != null) {
								addbtn.show();
							}
						}
					}
				});
			} else {
				record.data.rstsCode = 'RW';
				if (olrInd === 'Y') {
					mobEdu.reg.f.getOnlineRegDates(record, item);
				} else {
					mobEdu.reg.f.addToCart(record, item);
				}
			}
		},

		getOnlineRegDates: function(record, item) {
			var crn = record.data.crn;
			var regTermCode = record.data.termCode;
			var olrStore = mobEdu.util.getStore('mobEdu.reg.store.olrStore');
			olrStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			olrStore.getProxy().setUrl(webserver + 'getOlrDates');
			olrStore.getProxy().setExtraParams({
				termCode: regTermCode,
				crn: crn,
				companyId: companyId
			});
			olrStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.olrDatesResponseHandler(record, item);
			};
			olrStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'olrProcedure');
		},

		olrDatesResponseHandler: function(rec, item) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.olrStore');
			var storeData = store.data.all[0];
			var olrRecord = store.getAt(0);
			if (storeData.data.errMsg === '' || storeData.data.errMsg === undefined || storeData.data.errMsg === null) {
				mobEdu.reg.f.showOlrPopup();
				olrRecord.set('termCode', rec.data.termCode);
				olrRecord.set('crn', rec.data.crn);
				olrRecord.set('rstsCode', rec.data.rstsCode);
				olrRecord.set('courseTitle', rec.data.courseTitle);
				olrRecord.set('olrInd', rec.data.olrInd);
				mobEdu.reg.f.item = item;
			} else {
				Ext.Msg.show({
					message: storeData.data.errMsg,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		addToCart: function(record, item) {
			var crn = record.data.crn;
			var regTermCode = Ext.getCmp('instSearchTermId').getValue();
			var addCartStore = mobEdu.util.getStore('mobEdu.reg.store.addCart');
			addCartStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			addCartStore.getProxy().setUrl(webserver + 'addToCart');
			addCartStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: regTermCode,
				crn: crn,
				rstsCode: record.get('rstsCode'),
				startDate: mobEdu.reg.f.startDate,
				endDate: mobEdu.reg.f.endDate,
				companyId: companyId
			});
			addCartStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.addCartResponseHandler(item, record);
			};
			addCartStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'addtocart');
		},

		addCartResponseHandler: function(item, cartRecord) {
			var olrInd = cartRecord.get('olrInd');
			var addCartStore = mobEdu.util.getStore('mobEdu.reg.store.addCart');
			var addCartStatus = addCartStore.getProxy().getReader().rawData;
			mobEdu.reg.f.startDate = null;
			mobEdu.reg.f.endDate = null;
			mobEdu.reg.f.item = null;
			if (addCartStatus.status == 'SUCCESS') {
				if (olrInd === 'Y') {
					mobEdu.reg.f.closeOlrPopup();
				}
				if (addToCartFlag == 'courseList' || addToCartFlag == 'courseDetail' || addToCartFlag == 'advcourseList') {
					var lCartStore = mobEdu.util.getStore('mobEdu.reg.store.localCart');
					lCartStore.add({
						termCode: cartRecord.data.termCode,
						crn: cartRecord.data.crn
					});
					lCartStore.sync();
					if (item !== undefined && item !== null) {
						item.target.setAttribute("class", "removecart");
						item.target.title = "remove";
					}
					if (addToCartFlag == 'courseList')
						Ext.getCmp('searchRegList').refresh();
					else if (addToCartFlag == 'advcourseList')
						Ext.getCmp('advSearchRegList').refresh();
					else {}
				}
				Ext.getCmp('searchRegList').refresh();
			} else {
				Ext.Msg.show({
					message: addCartStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		searchCourseDetailAddToCart: function() {
			addToCartFlag = 'courseDetail';
			Ext.getCmp("addToCart").hide();
			var crseDetailStore = mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal');
			var crseDetail = crseDetailStore.getAt(0);
			mobEdu.reg.f.validate(crseDetail);
		},

		onViewCartButtonTap: function() {
			var termCode = Ext.getCmp('instSearchTermId').getValue();
			mobEdu.util.updateCartTermCode(termCode);
			var termDescr = Ext.getCmp('instSearchTermId').record.data.description;

			mobEdu.reg.f.loadViewCart(termCode, termDescr);
		},

		onTermItemTap: function(view, index, target, record, e, eOpts) {
			mobEdu.util.updateCartTermCode(record.data.code);
			mobEdu.reg.f.loadViewCart(record.data.code, record.data.description);
			mobEdu.reg.f.hideTermListPopup();
		},

		loadViewCart: function(termCode, termDescr) {

			var vCartstore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			vCartstore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			vCartstore.getProxy().setUrl(webserver + 'viewCart');
			vCartstore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				companyId: companyId
			});
			vCartstore.getProxy().afterRequest = function() {
				mobEdu.reg.f.viewCartResponseHandler(termDescr);
			};
			vCartstore.load();
			mobEdu.util.gaTrackEvent('enroll', 'viewcart');
		},

		viewCartResponseHandler: function(termDescr) {
			var vCartStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			mobEdu.reg.f.viewCartButtons();
			var store = mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal');
			store.removeAll();
			store.removed = [];
			store.sync();
			if (!(Ext.os.is.Phone) && vCartStore.data.length > 0) {
				var cartRec = vCartStore.data.items[0].data;
				this.getCourseInfo(cartRec.termCode, cartRec.crn);
				mobEdu.util.get("mobEdu.reg.view.tablet.viewCart");
				Ext.getCmp("tViewCartList").select(0);
			}

			mobEdu.reg.f.showViewCart();
			mobEdu.reg.f.setViewCartTitle(termDescr);
		},

		setViewCartTitle: function(title) {
			if (Ext.os.is.Phone) {
				mobEdu.util.setTitle(null, Ext.getCmp('viewCartP'), title);
			} else {
				mobEdu.util.setTitle(null, Ext.getCmp('viewCartT'), title);
			}
		},

		viewCartButtons: function() {
			mobEdu.util.get("mobEdu.reg.view.tablet.viewCart");
			mobEdu.util.get("mobEdu.reg.view.phone.viewCart");
			var vCartStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			if (vCartStore.data.length > 0) {

				if (Ext.os.is.Phone) {
					Ext.getCmp('registerP').show();
					Ext.getCmp('clearCartP').show();
					Ext.getCmp('calendarP').show();
				} else {
					Ext.getCmp('clearCartT').show();
					Ext.getCmp('registerT').show();
					Ext.getCmp('calendarT').show();
				}
			} else {

				if (Ext.os.is.Phone) {
					Ext.getCmp('registerP').hide();
					Ext.getCmp('clearCartP').hide();
					Ext.getCmp('calendarP').hide();
				} else {
					Ext.getCmp('clearCartT').hide();
					Ext.getCmp('registerT').hide();
					Ext.getCmp('calendarT').hide();
				}
			}
		},

		dumpCartInfoToLocalStore: function() {
			var vCartStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			var lCartStore = mobEdu.util.getStore('mobEdu.reg.store.localCart');
			if (lCartStore.data.length > 0) {
				lCartStore.removeAll();
				lCartStore.sync();
				lCartStore.removed = [];
			}
			vCartStore.each(function(record) {
				lCartStore.add(record.copy());
				lCartStore.sync();
			})
		},

		removeItemFromCart: function(selectedItemIndex, viewRef, e) {
			removeFromCartFlag = 'viewCart';
			mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
			var record = viewRef.getStore().getAt(selectedItemIndex);
			var target = e.target;
			if (target.title == "remove") {
				mobEdu.reg.f.removeFromCart(record, null, selectedItemIndex);
			}
		},

		register: function() {
			//            var holdinfoStore = mobEdu.util.getStore('mobEdu.crl.store.holdsInfo');
			//            if (holdinfoStore.data.all.length == 0) {
			pinFlag = 'register';
			mobEdu.reg.f.pinMsgCount = 1;
			if (showTermsAndConditions == 'true') {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showViewCart);
				mobEdu.reg.f.loadTermsAndConditions(null, null, 'A');
				mobEdu.reg.view.acceptAction = mobEdu.reg.f.registerCourses;
				mobEdu.reg.view.params = null;
			} else if (curriculmChangePreReg == 'true') {
				mobEdu.reg.f.getCurriculumPreRegistrationInfo();
			} else {
				mobEdu.reg.f.registerCourses();
			}
			//            }else {
			//                if (holdinfoStore.data.all.length != 0) {
			//                    Ext.Msg.show({
			//                        title: 'Not Permited to Register',
			//                        message: '<p>Holds Exist</p>',
			//                        buttons: Ext.MessageBox.OK,
			//                        cls: 'msgbox'
			//                    });
			//                }
			//            }
		},
		registerCourses: function() {
			mobEdu.reg.f.checkPin();
		},

		showTermsAndConditions: function() {
			mobEdu.util.show('mobEdu.reg.view.termsAndConditions');
		},

		loadTermsAndConditions: function(termCode, crn, indicator) {
			var code = mobEdu.util.getSchoolCode();
			if (code == null || code == '') {
				code = campusCode
			}

			if (termCode == null || termCode == '') {
				termCode = mobEdu.util.getCartTermCode();
			}
			var store = mobEdu.util.getStore('mobEdu.reg.store.termsandConditions');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getTermsConditions');
			store.getProxy().setExtraParams({
				campusCode: code,
				termCode: termCode,
				crn: crn,
				indicator: indicator,
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.loadTandCResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'termsconditions');
		},

		loadTandCResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.termsandConditions');
			var data = store.data.all;
			var text = data[0].data.termAndConditions;
			if (text == '' || text == null) {
				text = '';
				if (campusCode == 'T') {
					(mobEdu.reg.view.acceptAction)(mobEdu.reg.view.params);
				} else {
					mobEdu.reg.f.showTermsAndConditions();
					var text1 = mobEdu.util.formatUrls(text);
					Ext.getCmp('aboutTandC').setHtml('<p>' + text1 + '</p>');
				}
			} else {
				mobEdu.util.updateAboutText(text);
				mobEdu.reg.f.showTermsAndConditions();
				var text1 = mobEdu.util.formatUrls(text);
				Ext.getCmp('aboutTandC').setHtml('<p>' + text1 + '</p>');
				if (data[0].data.campusCode == 'T') {
					Ext.getCmp('rejectTerms').setText('Cancel');
					Ext.getCmp('acceptTerms').setText('Continue');
				}
			}
		},

		loadDropTandC: function() {
			var store = mobEdu.util.getStore('mobEdu.courses.store.drop');
			var text = store.getProxy().getReader().rawData.message;
			if (text == '' || text == null) {
				text = '';
				if (campusCode == 'T') {
					(mobEdu.reg.view.acceptAction)(mobEdu.reg.view.params);
				} else {
					mobEdu.reg.f.showTermsAndConditions();
					var text1 = mobEdu.util.formatUrls(text);
					Ext.getCmp('aboutTandC').setHtml('<p>' + text1 + '</p>');
				}
			} else {
				mobEdu.util.updateAboutText(text);
				mobEdu.reg.f.showTermsAndConditions();
				var text1 = mobEdu.util.formatUrls(text);
				Ext.getCmp('aboutTandC').setHtml('<p>' + text1 + '</p>');
				if (campusCode == 'T') {
					Ext.getCmp('rejectTerms').setText('Cancel');
					Ext.getCmp('acceptTerms').setText('Continue');
				}
			}
		},

		validateRegisterPin: function() {
			regTermCode = mobEdu.util.getCartTermCode();
			pinFlag = 'register';
			mobEdu.reg.f.checkPin();
		},



		doRegister: function() {
			searchFalg = true;
			var regStore = mobEdu.util.getStore('mobEdu.reg.store.register');
			regStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			regStore.getProxy().setUrl(webserver + 'register');
			regStore.getProxy().setExtraParams({
				termCode: mobEdu.util.getCartTermCode(),
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			regStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.registerResponseHandler();
			};
			regStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'register');
		},

		registerResponseHandler: function() {
			var rStore = mobEdu.util.getStore('mobEdu.reg.store.register');
			var regStatus = rStore.getProxy().getReader().rawData;
			if (regStatus.status == 'SUCCESS') {
				mobEdu.reg.f.refreshLocalCart();
				mobEdu.reg.f.showRegister();
			} else {
				Ext.Msg.show({
					message: regStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		getStatusCode: function(statusCode) {
			if (statusCode == 'WL') {
				return 1;
			} else if (statusCode == 'FAILURE') {
				return 2;
			}
			return 0;
		},

		refreshLocalCart: function() {
			var vCartstore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			vCartstore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			vCartstore.getProxy().setUrl(webserver + 'viewCart');
			vCartstore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: mobEdu.util.getCartTermCode(),
				companyId: companyId
			});
			vCartstore.getProxy().afterRequest = function() {
				mobEdu.reg.f.refreshLocalCartResponseHandler();
			};
			vCartstore.load();
			mobEdu.util.gaTrackEvent('enroll', 'viewcart');
		},

		refreshLocalCartResponseHandler: function() {
			mobEdu.reg.f.dumpCartInfoToLocalStore();
		},

		showRegPervious: function() {
			var vCartStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			if (vCartStore.data.length == 0) {
				mobEdu.reg.f.showSearch();
			} else {
				mobEdu.reg.f.showViewCart();
				mobEdu.reg.f.viewCartButtons();
			}
		},

		clearCart: function() {
			Ext.Msg.show({
				title: null,
				cls: 'msgbox',
				message: '<p>Are you sure you want to remove all courses from your Cart?</p>',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						var cCartStore = mobEdu.util.getStore('mobEdu.reg.store.clearCart');
						cCartStore.getProxy().setHeaders({
							Authorization: mobEdu.util.getAuthString(),
							companyID: companyId
						});
						cCartStore.getProxy().setUrl(webserver + 'clearCart');
						cCartStore.getProxy().setExtraParams({
							studentId: mobEdu.main.f.getStudentId(),
							termCode: mobEdu.util.getCartTermCode(),
							companyId: companyId
						});
						cCartStore.getProxy().afterRequest = function() {
							mobEdu.reg.f.clearCartResponseHandler(cCartStore);
						};
						cCartStore.load();
						mobEdu.util.gaTrackEvent('enroll', 'clearcart');
					}
				}
			});
		},

		clearCartResponseHandler: function(cCartStore) {
			var clearCartStatus = cCartStore.getProxy().getReader().rawData;
			if (clearCartStatus.status == 'SUCCESS') {
				var lCartStore = mobEdu.util.getStore('mobEdu.reg.store.localCart');
				lCartStore.removeAll();
				lCartStore.sync();
				lCartStore.removed = [];
				var store = null;
				var searchType;
				if (mobEdu.reg.f.prevViewRef == mobEdu.reg.f.showSearch) {
					searchType = 'search';
					store = mobEdu.util.getStore('mobEdu.reg.store.search');
				} else {
					searchType = 'advSearch';
					store = mobEdu.util.getStore('mobEdu.reg.store.advSearch');
				}
				store.load();
				// (mobEdu.reg.f.prevViewRef)();
				if (searchType === 'search') {
					mobEdu.reg.f.showSearch();
				} else {
					mobEdu.reg.f.showAdvancedSearchResult();
				}

			} else {
				Ext.Msg.show({
					message: clearCartStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		hasRoomCode: function(roomCode) {
			return !!roomCode;
		},
		hasStatus: function(rstsCode, status) {
			if (status == 'SUCCESS') {
				return 0;
			} else if (status == 'FAIL') {
				return 1;
			} else {
				return 2;
			}
		},
		hasPrerequisites: function(prerequisites) {
			var len = prerequisites.length;
			if (len > 0) {
				return true;
			} else {
				return false;
			}
		},

		hasMeetings: function(meetings) {
			var len = meetings.length;
			if (len > 0) {
				return true;
			} else {
				return false;
			}
		},

		doCheckBeforeSearch: function(textfield, searchListCmp, termIdCmp) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				var searchString = '';

				searchString = textfield.getValue();
				if (searchString == searchValue) {

					if (Ext.getCmp('pagingplugin') == null) {
						var pArray = new Array();
						pArray[0] = mobEdu.reg.view.paging.create();
						searchListCmp.setPlugins(pArray);
					}
					Ext.getCmp('pagingplugin').getLoadMoreCmp().show();

					mobEdu.reg.f.clearLocalSearchStore();
					mobEdu.util.hideKeyboard();
					mobEdu.reg.f.getsearchResults(textfield, searchListCmp, termIdCmp);
				}
			} else {

				if (Ext.getCmp('pagingplugin') != null) {
					Ext.getCmp('pagingplugin').getLoadMoreCmp().hide();
				}
				searchListCmp.setPlugins(null);
				searchListCmp.setEmptyText(null);
				mobEdu.reg.f.clearLocalSearchStore();
			}
		},

		clearLocalSearchStore: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.search');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
		},

		clearLocalAdvSearchStore: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.advSearch');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
		},
		addPinStatus: function(termCode, value) {
			var pinStore = mobEdu.util.getStore('mobEdu.reg.store.pin');
			var pinRec = pinStore.findRecord('termCode', termCode);
			if (pinRec == null) {
				pinStore.add({
					termCode: termCode,
					pinStatus: 'success',
					value: value
				});
			} else {
				pinRec.set('value', value);
			}
			pinStore.sync();
		},
		updateEligibilityStatus: function(crseRec) {
			var regTermCode = crseRec.data.termCode;
			var pinStore = mobEdu.util.getStore('mobEdu.reg.store.pin');
			var record = pinStore.findRecord('termCode', regTermCode);
			if (record != null && record != '') {
				record.set('eligibilityStatus', 'success');
			}
			pinStore.sync();
		},
		checkForSearchListEnd: function(store, records, isSuccessful) {
			var pageSize = store.getPageSize();
			var pageIndex = store.currentPage - 1; // Page numbers start at 1

			if (isSuccessful && records.length < pageSize) {
				//Set count to disable 'loading' message
				var totalRecords = pageIndex * pageSize + records.length;
				store.setTotalCount(totalRecords);
			} else
				store.setTotalCount(null);

		},

		loadTipsPopup: function(popupView) {
			//            mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.reg.view.tips')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		//        hideTipsPopup:function(){
		//            mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
		//            var popUp=Ext.getCmp('tipsPopup');
		//            popUp.hide();
		//            Ext.Viewport.unmask();
		//        },

		showTermListPopup: function(popupView) {
			mobEdu.util.updatePrevView(mobEdu.reg.f.hideTermListPopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.reg.view.tablet.termList')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		hideTermListPopup: function() {
			mobEdu.util.updatePrevView(mobEdu.reg.f.showViewCart);
			var popUp = Ext.getCmp('regTermListPopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
			}
			Ext.Viewport.unmask();
		},

		loadAllCartItems: function() {
			if (!(mobEdu.util.checkInfo('cartTermDescription'))) {
				mobEdu.util.loadCurrentTerm();
			}
			var vCartstore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
			vCartstore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			vCartstore.getProxy().setUrl(webserver + 'viewCart');
			vCartstore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			vCartstore.getProxy().afterRequest = function() {
				mobEdu.reg.f.allCartItemsResponseHandler();
			};
			vCartstore.load();
			mobEdu.util.gaTrackEvent('enroll', 'viewcart');
		},

		loadCartScheduleInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.courseList');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getCartSchedule');
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: mobEdu.util.getCartTermCode(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.cartScheduleInfoResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'cartschedule');
		},

		cartScheduleInfoResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.courseList');
			var status = store.getProxy().getReader().rawData;
			if (status.status == 'SUCCESS') {
				mobEdu.reg.f.showCalenderView();
			} else {
				Ext.Msg.show({
					message: status.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		//Show the calendar view
		calendarViewShow: function() {
			mobEdu.main.f.calendarInstance = 'cart';
			mobEdu.util.get('mobEdu.reg.view.cartSchedule');
			var cartdock = Ext.getCmp('cartDock');
			cartdock.remove(calendar);

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'cartCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			cartdock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.reg.f.setCartScheduleInfo();

		},

		onCalendarBackTap: function() {
			mobEdu.reg.f.showViewCart();
		},

		//Meeting schedule display in initial loading
		setCartScheduleInfo: function(selectedDate) {
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.reg.store.courseList');
			var scheduleInfo = '';
			// We are using local store to store the meetings to be displayed
			// Clear it first
			mobEdu.util.getStore('mobEdu.reg.store.meetingScheduleLocal').removeAll();

			store.each(function(rec) {
				//get the meeting info
				var meetings = rec.get('meeting');
				Ext.each(meetings, function(item) {
					var meetingStartDate = Ext.Date.parse(item.startDate, "m-d-Y");
					var meetingEndDate = Ext.Date.parse(item.endDate, "m-d-Y");
					if (Ext.Date.between(selectedDate, meetingStartDate, meetingEndDate)) {
						var dayOfWeek = Ext.Date.format(selectedDate, "D");
						dayOfWeek = dayOfWeek.toUpperCase();
						var period = item.period;
						//Split the periods from list
						var periodList = new Array();
						periodList = period.split(':');

						if (Ext.Array.contains(periodList, dayOfWeek)) {
							var cTitle = rec.get('courseTitle');
							//display the first start date meetings schedule info
							mobEdu.reg.f.addClassToStore(cTitle, item.beginTime, item.endTime, item.mtypCode, item.bldgCode, item.roomCode, item.latitude, item.longitude);
						}
					}
				}, this);
			});
			scheduleInfo = this.formatScheduleInfo(scheduleInfo);
			Ext.getCmp('cartLabel').setHtml('<h3>' + scheduleInfo + '</h3>');
		},

		//To highlight Schedule dates
		doHighlightCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.reg.store.courseList');
			store.each(function(rec) {
				var meetings = rec.get('meeting');
				Ext.each(meetings, function(item) {
					var startDate = Ext.Date.parse(item.startDate, "m-d-Y");
					var endDate = Ext.Date.parse(item.endDate, "m-d-Y");
					var period = item.period;
					//Split the periods from list
					var periodList = period.split(':');
					do {
						var dayOfWeek = Ext.Date.format(startDate, "D");
						dayOfWeek = dayOfWeek.toUpperCase();
						if (Ext.Array.contains(periodList, dayOfWeek)) {
							if (!(startDate < values.date || startDate > values.date)) {
								classes.push(highlightedItemCls);
							}
						}
						//Incrementing startDate
						startDate = new Date(startDate.getTime() + 86400000);
					} while (startDate <= endDate)
				}, this);
			});
			return classes;
		},

		addClassToStore: function(cTitle, sTime, eTime, type, buildgNo, roomNo, latitude, longitude) {
			var meetingsLocalStore = mobEdu.util.getStore('mobEdu.reg.store.meetingScheduleLocal');
			meetingsLocalStore.add({
				'title': cTitle,
				'startTime': sTime,
				'endTime': eTime,
				'type': type,
				'buildgNo': buildgNo,
				'roomNo': roomNo,
				'latitude': latitude,
				'longitude': longitude
			});
			meetingsLocalStore.sync();
		},

		//selection to change display the meetings schedule info
		formatScheduleInfo: function(scheduleInfo, cTitle, startTime, endTime, type, buildgNo, roomNo, latitude, longitude) {
			var meetingsLocalStore = mobEdu.util.getStore('mobEdu.reg.store.meetingScheduleLocal');
			var sData = meetingsLocalStore.data.all;
			sData.sort(function(first, second) {
				var fst = first.data.startTime;
				var secSt = second.data.startTime;
				if (fst.indexOf('AM', 1) == secSt.indexOf('AM', 1) || fst.indexOf('PM', 1) == secSt.indexOf('PM', 1)) {
					var fTime = fst.replace('AM', '');
					fTime = fTime.replace('PM', '');
					var sTime = secSt.replace('AM', '');
					sTime = sTime.replace('PM', '');
					var fh = fTime.substring(0, fTime.indexOf(':'));
					var sh = sTime.substring(0, sTime.indexOf(':'));
					if (eval(fh) < eval(sh)) {
						return -1;
					} else if (eval(fh) > eval(sh)) {
						return 1;
					} else if (eval(fh) == eval(sh)) {
						var fm = fTime.substr(fTime.indexOf(':') + 1);
						var sm = sTime.substr(sTime.indexOf(':') + 1);
						if (eval(fm) < eval(sm)) {
							return -1;
						} else if (eval(fm) < eval(sm)) {
							return 1;
						}
					}
				} else if (fst.indexOf('AM', 1) == 1) {
					return -1;
				} else if (secSt.indexOf('AM', 1) == 1) {
					return 1;
				}
			});
			for (var c = 0; c < sData.length; c++) {

				if (type == 'CLAS') {
					scheduleInfo = scheduleInfo + '<b>' + sData[c].data.title + '</b>' + '<br/>';
				} else if (type != 'LAB') {
					scheduleInfo = scheduleInfo + '<b>' + sData[c].data.title + '</b>' + '<br/>';
				}
				scheduleInfo = scheduleInfo + sData[c].data.startTime + '-' + sData[c].data.endTime + '<br/>';
				if (type == 'CLAS') {
					scheduleInfo = scheduleInfo + 'ClassType: Lecture' + '<br/>';
				} else {
					scheduleInfo = scheduleInfo + 'ClassType:' + sData[c].data.type + '<br/>';
				}
				if (sData[c].data.buildgNo != 'NA' && sData[c].data.latitude != null && sData[c].data.longitude != null && sData[c].data.latitude != 'NA' && sData[c].data.longitude != 'NA') {
					scheduleInfo = scheduleInfo + 'Building:' + sData[c].data.buildgNo + '(<h8><a href="javascript:mobEdu.courses.f.showCartMapDirections(' + sData[c].data.latitude + ',' + sData[c].data.longitude + ',\'cartCalender\')">Click Here</a> to get directions)</h8> <br/>';
				} else {
					scheduleInfo = scheduleInfo + 'Building:' + sData[c].data.buildgNo + '<br/>';
				}
				scheduleInfo = scheduleInfo + 'Room:' + sData[c].data.roomNo + '<br/>' + '<br/>';
			}

			return scheduleInfo;

		},
		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},
		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.reg.f.dateChecking([currentDate.getMonth() + 1]) + '-' + mobEdu.reg.f.dateChecking(currentDate.getDate()) + '-' + currentDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.reg.f.dateChecking([presentDate.getMonth() + 1]) + '-' + mobEdu.reg.f.dateChecking(presentDate.getDate()) + '-' + presentDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.reg.f.setCartScheduleInfo(date)
			}
		},

		isOnlineCourse: function(campusCode) {
			if (campusCode == 'W' || campusCode == 'GML')
				return true;
			else
				return false;
		},

		allCartItemsResponseHandler: function() {
			mobEdu.reg.f.dumpCartInfoToLocalStore();

			// If this load cart was triggered by proxy,
			// we need to show this message once the load is complete
			// Hence we are using this static variable
			if (mobEdu.reg.f.loadCartTriggeredBy === 'PROXY') {
				mobEdu.reg.f.loadCartTriggeredBy = null;
				Ext.Msg.show({
					message: mobEdu.util.getProxyUserId() + ' session started',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('txtProxyId').setValue('');
							mobEdu.settings.f.showMenu();
						}
					}
				});
			}
		},

		isExists: function(value) {
			if (value == null || value == 'NA' || value == 'No schedule specified')
				return false;
			return true;
		},

		isDuplicate: function(courseMeetings, courseDates) {
			if ((courseMeetings == 'No schedule specified') || (courseMeetings == courseDates))
				return false;
			return true;
		},

		isExistsFaculty: function(faculty) {
			if (faculty == null || faculty == 'NA' || faculty == ' ' || faculty == '')
				return false;
			else
				return true;
		},

		isEmailExist: function(facultyEmail) {
			if (facultyEmail == null || facultyEmail == 'NA')
				return false;
			else
				return true;
		},

		isScheduleDesc: function(scheduleDesc) {
			if (scheduleDesc == null || scheduleDesc == 'NA')
				return false;
			return true;
		},

		onSearchBackTap: function() {
			Ext.getCmp('searchItem').setValue('');
			if (Ext.getCmp('pagingplugin') != null) {
				Ext.getCmp('pagingplugin').getLoadMoreCmp().hide();
			}
			Ext.getCmp('searchRegList').setPlugins(null);
			Ext.getCmp('searchRegList').setEmptyText(null);
			mobEdu.reg.f.clearLocalSearchStore();
			mobEdu.util.showMainView();
		},

		showSearch: function() {
			mobEdu.reg.f.prevViewRef = mobEdu.reg.f.showSearch;
			mobEdu.util.updatePrevView(mobEdu.reg.f.onSearchBackTap);
			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.reg.view.phone.search');
			} else {
				mobEdu.util.show('mobEdu.reg.view.tablet.search');
			}

			Ext.getCmp('searchRegList').refresh();
			if (searchFalg == true) {
				searchFalg = false;
				var store = mobEdu.util.getStore('mobEdu.reg.store.search');
				store.load();
			}
		},
		showSearchDetail: function(prevViewRef) {
			if (prevViewRef == null || prevViewRef == '') {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			} else {
				mobEdu.util.updatePrevView(prevViewRef);
			}
			mobEdu.util.show('mobEdu.reg.view.searchCourseDetail');
		},
		showViewCart: function() {
			var prevViewRef = mobEdu.reg.f.prevViewRef;
			var proxyUser = mobEdu.util.getProxyUserId();

			if (prevViewRef == null || prevViewRef == '') {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			} else {
				mobEdu.util.updatePrevView(prevViewRef);
			}

			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.reg.view.phone.viewCart');
				Ext.getCmp('pViewCartList').refresh();
				var vStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
				if (vStore.data.length == 0) {
					Ext.getCmp('registerP').hide();
				} else {
					if (proxyUser != null && proxyUser != '') {
						if (mobEdu.util.getProxyAccessFlag() == '0') {
							Ext.getCmp('registerP').hide();
						} else {
							Ext.getCmp('registerP').show();
						}
					} else {
						Ext.getCmp('registerP').show();
					}
				}
			} else {
				mobEdu.util.show('mobEdu.reg.view.tablet.viewCart');
				Ext.getCmp('tViewCartList').refresh();
				var vStore = mobEdu.util.getStore('mobEdu.reg.store.viewCart');
				if (vStore.data.length == 0) {
					Ext.getCmp('registerT').hide();
				} else {
					if (proxyUser != null && proxyUser != '') {
						if (mobEdu.util.getProxyAccessFlag() == '0') {
							Ext.getCmp('registerT').hide();
						} else {
							Ext.getCmp('registerT').show();
						}
					} else {
						Ext.getCmp('registerT').show();
					}
				}
			}
		},

		onAdvSearchBtnTap: function() {
			var crn = Ext.getCmp('advCrn').getValue();
			var subCmp = null;
			if (enableAdvSubjectSearch == 'true') {
				subCmp = Ext.getCmp('txtAdvSub');
			} else {
				subCmp = Ext.getCmp('advSub');
			}
			var sub = null;
			if (subCmp.getValue() != '' && subCmp.getValue() != null && subCmp.getValue() != 'Select Subject') {
				sub = subCmp.getComponent().getValue();
			}
			var crseLevel = null;
			if (Ext.getCmp('advCrseLevel').getValue() != '' && Ext.getCmp('advCrseLevel').getValue() != null) {
				crseLevel = Ext.getCmp('advCrseLevel').getComponent().getValue();
			}
			var crseNum = Ext.getCmp('advCrseNum').getValue();
			var title = Ext.getCmp('advTitle').getValue();
			var term = Ext.getCmp('instSearchTermId').getValue();
			var partOfTerm = Ext.getCmp('advPartOfTerm').getValue();
			var scheduleType = Ext.getCmp('advScheduleType').getValue();
			var campusCode = Ext.getCmp('advCampus').getValue();
			var instructionalCode = Ext.getCmp('advInstructnlMethod').getValue();
			mobEdu.util.get('mobEdu.reg.view.advSearchResult');
			var searchListCmp = Ext.getCmp('advSearchRegList');
			if ((crn == null || crn == '') && (sub == null || sub == '') && (crseLevel == null || crseLevel == '') && (crseNum == null || crseNum == '') && (title == null || title == '') && (partOfTerm == null || partOfTerm == '') && (scheduleType == null || scheduleType == '') && (campusCode == null || campusCode == '') && (instructionalCode == null || instructionalCode == '')) {

				if (Ext.getCmp('advsearchpagingplugin') != null) {
					Ext.getCmp('advsearchpagingplugin').getLoadMoreCmp().hide();
				}
				searchListCmp.setPlugins(null);
				searchListCmp.setEmptyText(null);
				Ext.Msg.show({
					message: '<p>Please provide at least one value to search.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (Ext.getCmp('advsearchpagingplugin') == null) {
					var pArray = new Array();
					pArray[0] = mobEdu.reg.view.advsearchpaging.create();
					searchListCmp.setPlugins(pArray);
					searchListCmp.setEmptyText('<h3 align="center">No courses available</h3>');
				}
				Ext.getCmp('advsearchpagingplugin').getLoadMoreCmp().show();
				mobEdu.reg.f.clearLocalAdvSearchStore();

				var store = mobEdu.util.getStore('mobEdu.reg.store.advSearch');
				if (mobEdu.util.getAuthString() != null && mobEdu.util.getAuthString() != '') {
					store.getProxy().setHeaders({
						Authorization: mobEdu.util.getAuthString(),
						companyID: companyId
					});
				} else {
					store.getProxy().setHeaders({
						companyID: companyId
					});
				}
				store.getProxy().setUrl(webserver + 'advSearch');
				store.getProxy().setExtraParams({
					term: term,
					crn: crn,
					subject: sub,
					crseLevel: crseLevel,
					crseNum: crseNum,
					title: title,
					partOfTerm: partOfTerm,
					scheduleType: scheduleType,
					campusCode: campusCode,
					instructionalCode: instructionalCode,
					studentId: mobEdu.main.f.getStudentId(),
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.reg.f.advSearchResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('enroll', 'advsearch');
			}
		},

		advSearchResponseHandler: function() {
			removeFromCartFlag = 'courseList';
			var store = mobEdu.util.getStore('mobEdu.reg.store.advSearch');
			var status = store.getProxy().getReader().rawData.status;
			if (status.toUpperCase() != 'SUCCESS') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.reg.f.showAdvancedSearchResult();
			}
		},

		loadAdvanceSearch: function() {
			var term = Ext.getCmp('instSearchTermId').getValue();
			mobEdu.reg.f.loadSubjectList(term);
			mobEdu.reg.f.loadCourseLevelList();
			mobEdu.reg.f.loadPartOfTermList(term);
			mobEdu.reg.f.loadScheduleTypeList();
			mobEdu.reg.f.loadAdvSearchCampusList();
			mobEdu.reg.f.loadInstructionalMethodList();
			mobEdu.reg.f.showAdvancedSearch();
			if (enableAdvSubjectSearch == 'true') {
				Ext.getCmp('txtAdvSub').show();
				Ext.getCmp('advSub').hide();
			} else {
				Ext.getCmp('txtAdvSub').hide();
				Ext.getCmp('advSub').show();
			}
			mobEdu.reg.f.clearedAdvSearchFields();
		},

		loadSubjectList: function(term) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.subjectList');
			store.getProxy().setUrl(webserver + 'getSubjectList');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				termCode: term,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				if (enableAdvSubjectSearch != 'true') {
					mobEdu.reg.f.updateDropdown(mobEdu.util.getStore('mobEdu.reg.store.subjectList'), Ext.getCmp('advSub'), 'Select Subject');
				}
			};
			store.load();
		},

		loadCourseLevelList: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.courseLevelList');
			store.getProxy().setUrl(webserver + 'getCourseLevelList');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.updateDropdown(mobEdu.util.getStore('mobEdu.reg.store.courseLevelList'), Ext.getCmp('advCrseLevel'), 'Select Course Level', customCourseLevelSorting, crseLevelDefaultValue);
			};
			store.load();
		},

		loadPartOfTermList: function(term) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.partOfTermList');
			store.getProxy().setUrl(webserver + 'getPartOfTermList');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				termCode: term,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.updateDropdown(mobEdu.util.getStore('mobEdu.reg.store.partOfTermList'), Ext.getCmp('advPartOfTerm'), 'Select Part of Term');
			};
			store.load();
		},

		loadScheduleTypeList: function(term) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.scheduleTypeList');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getScheduleTypeList');
			store.getProxy().setExtraParams({
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.updateDropdown(mobEdu.util.getStore('mobEdu.reg.store.scheduleTypeList'), Ext.getCmp('advScheduleType'), 'Select Schedule Type');
			};
			store.load();
		},
		loadAdvSearchCampusList: function(term) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.campusList');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getAdvSearchCampusList');
			store.getProxy().setExtraParams({
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.updateDropdown(mobEdu.util.getStore('mobEdu.reg.store.campusList'), Ext.getCmp('advCampus'), 'Select Campus');
			};
			store.load();
		},
		loadInstructionalMethodList: function(term) {
			var store = mobEdu.util.getStore('mobEdu.reg.store.instructionalMethodsList');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getInstructionalMethodsList');
			store.getProxy().setExtraParams({
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.updateDropdown(mobEdu.util.getStore('mobEdu.reg.store.instructionalMethodsList'), Ext.getCmp('advInstructnlMethod'), 'Select Instructional Method');
			};
			store.load();
		},
		updateDropdown: function(store, cmp, text, customSorting, defaultValue) {
			if (store.data.length > 0) {
				var items = new Array();
				var index = 1;
				items[0] = ({
					text: text,
					value: null
				});
				if (customSorting == 'true') {
					var customRec = store.findRecord('code', 'UG');
					if (customRec != null) {
						items[index] = ({
							text: customRec.data.description,
							value: customRec.data.code
						});
						index = index + 1;
						store.each(function(record) {
							if (record.data.code != customRec.data.code) {
								items[index] = ({
									text: record.data.description,
									value: record.data.code
								});
								index = index + 1;
							}
						});
					}

				} else {
					store.each(function(record) {
						items[index] = ({
							text: record.data.description,
							value: record.data.code
						});
						index = index + 1;
					});
				}


				cmp.setOptions(items);
				if (defaultValue != null && defaultValue != '') {
					cmp.setValue(defaultValue);
				}
				//                cmp.select(0);
			} else
				cmp.hide();
		},

		onSubjectKeyup: function(textfield, e) {
			//Restricting TAB key
			if (e.browserEvent.keyCode != 9) {
				mobEdu.reg.f.doCheckBeforeSubjectSearch(textfield);
			}
		},

		doCheckBeforeSubjectSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			var store = mobEdu.util.getStore('mobEdu.reg.store.subjectList');
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						store.clearFilter();
						searchString = searchString.toLowerCase();
						store.filter(function(record) {
							var subStr = (record.get('description')).toLowerCase();
							if (subStr.indexOf(searchString) !== -1) {
								return true;
							}
							return false;
						});
					}
				}, 1000);
			} else {
				store.clearFilter();
			}
		},

		onSubjectItemClear: function(textfield, e) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length == 0 || length == '') {
				mobEdu.reg.f.doCheckBeforeSubjectSearch(textfield);
			}
		},

		onSubjectItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			// mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
			Ext.getCmp('txtAdvSub').setValue(record.get('code'));
			mobEdu.reg.f.hideSubjectPopup();
			var task = new Ext.util.DelayedTask(function() {
				Ext.getCmp('advCrseNum').focus();
			});
			task.delay(500);
		},

		loadSubjectPopup: function() {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('txtAdvSub').blur();
			if (mobEdu.reg.view.subject.clearIconTapped) {
				mobEdu.reg.view.subject.clearIconTapped = false;
				return;
			}
			mobEdu.util.updatePrevView(mobEdu.reg.f.hideSubjectPopup);
			var popupView = mobEdu.util.get('mobEdu.reg.view.subject');
			Ext.Viewport.add(popupView);
		},

		hideSubjectPopup: function() {
			mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			var popUp = Ext.getCmp('subjectPopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.reg.view.subject');
			}
			Ext.getCmp('txtAdvSub').blur();
			mobEdu.util.getStore('mobEdu.reg.store.subjectList').clearFilter();
		},

		clearedAdvSearchFields: function() {
			mobEdu.util.get('mobEdu.reg.view.advSearch');
			Ext.getCmp('advCrn').setValue('');
			if (enableAdvSubjectSearch == 'true') {
				Ext.getCmp('txtAdvSub').setValue('');
			} else {
				Ext.getCmp('advSub').setValue('Select Subject');
			}
			Ext.getCmp('advCrseNum').setValue('');
			if (crseLevelDefaultValue != null && crseLevelDefaultValue != '') {
				Ext.getCmp('advCrseLevel').setValue(crseLevelDefaultValue);
			} else {
				Ext.getCmp('advCrseLevel').setValue('Select Course Level');
			}
			Ext.getCmp('advTitle').setValue('');
			Ext.getCmp('advPartOfTerm').setValue('Select Part of Term');
			Ext.getCmp('advScheduleType').setValue('Select Schedule Type');
			Ext.getCmp('advCampus').setValue('Select Campus');
			Ext.getCmp('advInstructnlMethod').setValue('Select Instructional Method');
		},

		onSearchResultsBackTap: function() {
			mobEdu.reg.f.clearedAdvSearchFields();
			mobEdu.reg.f.showAdvancedSearch();
		},

		showAdvancedSearch: function() {
			mobEdu.util.updatePrevView(mobEdu.reg.f.advSearchBack);
			mobEdu.util.show('mobEdu.reg.view.advSearch');
		},

		advSearchBack: function() {
			Ext.getCmp('searchItem').setValue('');
			if (Ext.getCmp('pagingplugin') != null) {
				Ext.getCmp('pagingplugin').getLoadMoreCmp().hide();
			}
			Ext.getCmp('searchRegList').setPlugins(null);
			Ext.getCmp('searchRegList').setEmptyText(null);
			mobEdu.reg.f.clearLocalSearchStore();
			mobEdu.reg.f.showSearch();
		},

		showAdvancedSearchResult: function() {
			mobEdu.reg.f.prevViewRef = mobEdu.reg.f.showAdvancedSearchResult;
			mobEdu.util.updatePrevView(mobEdu.reg.f.onSearchResultsBackTap);
			mobEdu.util.show('mobEdu.reg.view.advSearchResult');
		},

		showRegister: function() {
			var prevViewRef = mobEdu.reg.f.prevViewRef;
			mobEdu.util.show('mobEdu.reg.view.registerStatus');
			if (prevViewRef == null) {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			} else {
				mobEdu.util.updatePrevView(prevViewRef);
			}

		},
		showTermList: function() {
			mobEdu.util.updatePrevView(mobEdu.reg.f.showViewCart);
			mobEdu.util.show('mobEdu.reg.view.phone.termList');
		},

		showImmunization: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.reg.view.immunization');
		},

		showCalenderView: function() {
			mobEdu.util.updatePrevView(mobEdu.reg.f.showViewCart);
			mobEdu.reg.f.calendarViewShow();
			mobEdu.util.show('mobEdu.reg.view.cartSchedule');
		},
		isPreRequestCheck: function(prereqcheck) {
			if (prereqcheck == 'N')
				return false;

			return true;
		},

		isWarning: function(errorFlag) {
			if (errorFlag == 'W')
				return true;

			return false;
		},

		isClosed: function(seatsAvailable, waitedList) {
			if (seatsAvailable == 'NO' && waitedList == 0)
				return true;
			else
				return false;
		},

		isRegistrationStatusExist: function(statusMessage) {
			if (statusMessage != null && statusMessage != '' && statusMessage != 'NA') {
				return true;
			} else {
				return false;
			}
		},
		getCurriculumPreRegistrationInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.curriculumInfo');
			if (store.getCount() > 0) {
				store.removeAll();
				store.removed = [];
			}
			store.getProxy().setUrl(webserver + 'getRegCurriculumInfo');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				termCode: mobEdu.util.getCartTermCode(),
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.reg.f.loadCurriculumInfo();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getRegCurriculumInfo');
		},

		loadCurriculumInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.curriculumInfo');
			if (store.data.length > 0) {
				mobEdu.reg.f.showCurriculumPreReg();
				mobEdu.reg.f.selectedMajorCode = mobEdu.reg.f.updateCurriculumPreRegInfandGetCode(store, Ext.getCmp('selectCurriclum'), 'Select Curriculum', 'CRCLM');
				//mobEdu.reg.f.selectedCatalogCode = mobEdu.reg.f.updateCurriculumPreRegInfandGetCode(store, Ext.getCmp('selectCatalog'), 'Select Catalog Year', 'CATALOG');
				mobEdu.reg.f.selectedGradCode = mobEdu.reg.f.updateCurriculumPreRegInfandGetCode(store, Ext.getCmp('selectGrad'), 'Select Graduation Term', 'GRAD');
			} else {
				mobEdu.reg.f.registerCourses();
			}
		},

		showCurriculumPreReg: function() {
			mobEdu.util.updatePrevView(mobEdu.reg.f.showViewCart);
			mobEdu.util.show('mobEdu.reg.view.updateCurriculumInfo');
		},

		submitCurriculumData: function() {
			var majorCode = mobEdu.reg.f.getSelectedCurriculumValues(Ext.getCmp('selectCurriclum').getValue(), mobEdu.reg.f.selectedMajorCode);
			// var catalogCode = mobEdu.reg.f.getSelectedCurriculumValues(Ext.getCmp('selectCatalog').getValue(), mobEdu.reg.f.selectedCatalogCode);
			var gradCode = mobEdu.reg.f.getSelectedCurriculumValues(Ext.getCmp('selectGrad').getValue(), mobEdu.reg.f.selectedGradCode);
			var proxyStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			proxyStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			proxyStore.getProxy().setUrl(webserver + 'updateRegCurriculumInfo');
			proxyStore.getProxy().setExtraParams({
				termCode: mobEdu.util.getCartTermCode(),
				studentId: mobEdu.main.f.getStudentId(),
				majorCode: majorCode,
				catalogCode: null,
				gradCode: gradCode,
				companyId: companyId
			});
			proxyStore.getProxy().afterRequest = function() {
				mobEdu.reg.f.updateCurriculumResponseHandler();
			}
			proxyStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'updateCurriculumInfo');
		},

		updateCurriculumResponseHandler: function() {
			var proxyStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			var status = proxyStore.getProxy().getReader().rawData.status;
			if (status == 'SUCCESS') {
				mobEdu.reg.f.registerCourses();
			} else {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		getSelectedCurriculumValues: function(presentValue, prevValue) {
			if (presentValue == prevValue) {
				return null;
			} else {
				return presentValue;
			}
		},

		updateCurriculumPreRegInfandGetCode: function(store, cmp, text, infcode) {
			var items = new Array();
			var index = 1;
			var selValIndex = 0;
			var opttext = '';
			var optvalue = '';
			items[0] = ({
				text: text,
				value: null
			});
			store.each(function(record) {
				if (infcode == 'CRCLM') {
					opttext = record.data.majorCodeDescription;
					optvalue = record.data.majorCode;
				} else if (infcode == 'CATALOG') {
					opttext = record.data.catalogYear;
					optvalue = record.data.catalogTermCode;
				} else if (infcode == 'GRAD') {
					opttext = record.data.termCodeDescription;
					optvalue = record.data.termCode;
				}
				if (opttext != '') {
					items[index] = ({
						text: opttext,
						value: optvalue
					});
					if (record.data.defaultVal === 'Y') {
						selValIndex = index;
					}
					index = index + 1;
				}
			});
			cmp.setOptions(items);
			cmp.setValue(items[selValIndex].text);
			return items[selValIndex].value;
		},

		showOlrPopup: function(popupView) {
			mobEdu.util.get('mobEdu.reg.view.olrPopup');
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.reg.view.olrPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		closeOlrPopup: function() {
			Ext.getCmp('olrStartDate').setValue('');
			Ext.getCmp('olrEndDate').setValue('');
			var popUp = Ext.getCmp('olrPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		submitPopup: function() {
			var store = mobEdu.util.getStore('mobEdu.reg.store.olrStore');
			mobEdu.util.get('mobEdu.reg.view.olrPopup');
			var record = store.data.items[0];
			var item = mobEdu.reg.f.item;
			var startDate = Ext.getCmp('olrStartDate').getValue();
			mobEdu.reg.f.startDate = startDate;
			var endDate = Ext.getCmp('olrEndDate').getValue();
			mobEdu.reg.f.endDate = endDate;
			mobEdu.reg.f.addToCart(record, item);
		}
	}
});