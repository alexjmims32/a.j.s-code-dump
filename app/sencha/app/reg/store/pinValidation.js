Ext.define('mobEdu.reg.store.pinValidation', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.pinValidation'
    ],

    config: {
        storeId: 'mobEdu.reg.store.pinValidation',

        autoLoad: false,

        model: 'mobEdu.reg.model.pinValidation'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }

});