Ext.define('mobEdu.reg.store.restrictions', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.restrictions'
    ],

    config: {
        storeId: 'mobEdu.reg.store.restrictions',

        autoLoad: false,

        model: 'mobEdu.reg.model.restrictions'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'crseRestrictions'        
        return proxy;
    }
});