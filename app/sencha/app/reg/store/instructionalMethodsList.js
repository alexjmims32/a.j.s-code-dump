Ext.define('mobEdu.reg.store.instructionalMethodsList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.list'
	],
	config: {
		storeId: 'mobEdu.reg.store.instructionalMethodsList',
		autoLoad: false,
		model: 'mobEdu.reg.model.list'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'methodList';
		return proxy;
	}
});