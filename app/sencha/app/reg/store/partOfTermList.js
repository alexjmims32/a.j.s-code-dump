Ext.define('mobEdu.reg.store.partOfTermList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.list'
	],
	config: {
		storeId: 'mobEdu.reg.store.partOfTermList',
		autoLoad: false,
		model: 'mobEdu.reg.model.list'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'partOfTermList';
		return proxy;
	}
});
