Ext.define('mobEdu.reg.store.courseDetailLocal', {
    extend:'Ext.data.Store',
//    alias: 'courseDetailStore',

    requires: [
        'mobEdu.reg.model.courseList'
    ],

    config: {
        storeId: 'mobEdu.reg.store.courseDetailLocal',

        autoLoad: false,

        model: 'mobEdu.reg.model.courseList',

      proxy:{
        type:'localstorage',
        id:'courseDetailLocal'
      }
    }
});
