Ext.define('mobEdu.reg.store.cart', {
    extend:'Ext.data.Store',
//    alias: 'cartStore',

    requires: [
        'mobEdu.reg.model.search'
    ],

    config: {
        storeId: 'mobEdu.reg.store.cart',

        autoLoad: true,

        model: 'mobEdu.reg.model.search',

      proxy:{
        type:'localstorage',
        id:'cart'
      }
    }
});
