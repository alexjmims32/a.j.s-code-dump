Ext.define('mobEdu.reg.store.immunization', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.immunization'
    ],

    config: {
        storeId: 'mobEdu.reg.store.immunization',

        autoLoad: false,

        model: 'mobEdu.reg.model.immunization'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }

});