Ext.define('mobEdu.reg.store.termList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.termList'
	],
	config: {
		storeId: 'mobEdu.reg.store.termList',
		autoLoad: false,
		model: 'mobEdu.reg.model.termList'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'registerTermList';
		return proxy;
	}
});
