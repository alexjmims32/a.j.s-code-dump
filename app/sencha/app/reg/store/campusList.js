Ext.define('mobEdu.reg.store.campusList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.list'
	],
	config: {
		storeId: 'mobEdu.reg.store.campusList',
		autoLoad: false,
		model: 'mobEdu.reg.model.list'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'campusList';
		return proxy;
	}
});