Ext.define('mobEdu.reg.store.curriculumInfo', {
	extend: 'mobEdu.data.store',

	requires: [
		'mobEdu.reg.model.curriculumInfo'
	],

	config: {
		storeId: 'mobEdu.reg.store.curriculumInfo',

		autoLoad: false,

		model: 'mobEdu.reg.model.curriculumInfo',
		proxy: {
			type: 'ajax',
			reader: {
				type: 'xml',
				rootProperty: 'ROWSET',
				record: 'RECORD'
			}
		}
	}
});