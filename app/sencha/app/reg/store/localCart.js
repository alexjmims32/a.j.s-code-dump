Ext.define('mobEdu.reg.store.localCart', {
    extend:'Ext.data.Store',
//    alias: 'searchStore',

    requires: [
        'mobEdu.reg.model.viewCart'
    ],
    config: {
        storeId: 'mobEdu.reg.store.localCart',

        autoLoad: true,

        model: 'mobEdu.reg.model.viewCart',

	proxy:{
            type:'localstorage',
            id:'cartLocal'
        }
    }
});