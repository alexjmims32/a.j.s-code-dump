Ext.define('mobEdu.reg.store.olrStore', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.search'
    ],

    config: {
        storeId: 'mobEdu.reg.store.olrStore',

        autoLoad: false,

        model: 'mobEdu.reg.model.search'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = ''
        return proxy;
    }

});