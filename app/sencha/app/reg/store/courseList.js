Ext.define('mobEdu.reg.store.courseList', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.cartSchedule'
    ],

    config: {
        storeId: 'mobEdu.reg.store.courseList',

        autoLoad: false,

        model: 'mobEdu.reg.model.cartSchedule'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'courseList.course'        
        return proxy;
    }
});
