Ext.define('mobEdu.reg.store.removeCart', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.eligibility'
    ],

    config: {
        storeId: 'mobEdu.reg.store.removeCart',

        autoLoad: false,

        model: 'mobEdu.reg.model.eligibility'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }

});
