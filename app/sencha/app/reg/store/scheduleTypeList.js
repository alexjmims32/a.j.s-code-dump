Ext.define('mobEdu.reg.store.scheduleTypeList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.list'
	],
	config: {
		storeId: 'mobEdu.reg.store.scheduleTypeList',
		autoLoad: false,
		model: 'mobEdu.reg.model.list'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'scheduleTypeList';
		return proxy;
	}
});
