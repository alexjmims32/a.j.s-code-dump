Ext.define('mobEdu.reg.store.search', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.search'
	],
	config: {
		storeId: 'mobEdu.reg.store.search',
		autoLoad: false,
		pageSize: pageSize,
		model: 'mobEdu.reg.model.search'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'searchResult';
		return proxy;
	}
});