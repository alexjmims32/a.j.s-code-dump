Ext.define('mobEdu.reg.store.register', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.register'
    ],

    config: {
        storeId: 'mobEdu.reg.store.register',

        autoLoad: false,

        model: 'mobEdu.reg.model.register'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'registerList.register'        
        return proxy;
    }
});