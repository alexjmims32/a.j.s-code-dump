Ext.define('mobEdu.reg.store.courseLevelList', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.list'
	],
	config: {
		storeId: 'mobEdu.reg.store.courseLevelList',
		autoLoad: false,
		model: 'mobEdu.reg.model.list'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'courseLevelList';
		return proxy;
	}
});
