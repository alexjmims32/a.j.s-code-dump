Ext.define('mobEdu.reg.store.drop',{
extend: 'mobEdu.data.store',
    
    requires:[
        'mobEdu.reg.model.register'
    ],
    config: {
        storeId: 'mobEdu.reg.store.drop',

        autoLoad: false,

        model: 'mobEdu.reg.model.register'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
})