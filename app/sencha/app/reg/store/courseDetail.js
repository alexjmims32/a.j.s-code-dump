Ext.define('mobEdu.reg.store.courseDetail', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.courseList'
    ],

    config: {
        storeId: 'mobEdu.reg.store.courseDetail',

        autoLoad: false,

        model: 'mobEdu.reg.model.courseList'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
