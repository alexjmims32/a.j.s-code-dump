Ext.define('mobEdu.reg.store.prerequisite', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.prerequisite'
    ],

    config: {
        storeId: 'mobEdu.reg.store.prerequisite',

        autoLoad: false,

        model: 'mobEdu.reg.model.prerequisite'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }

});