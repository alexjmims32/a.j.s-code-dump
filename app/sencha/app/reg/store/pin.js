Ext.define('mobEdu.reg.store.pin', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.reg.model.pin'
    ],

    config:{
        storeId: 'mobEdu.reg.store.pin',
        autoLoad: true,
        model: 'mobEdu.reg.model.pin',
        proxy:{
            type:'localstorage',
            id:'pin'
        }
    }
});