Ext.define('mobEdu.reg.store.viewCart', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.reg.model.viewCart'
	],
	config: {
		storeId: 'mobEdu.reg.store.viewCart',
		autoLoad: false,
		model: 'mobEdu.reg.model.viewCart'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'course';
		return proxy;
	}
});