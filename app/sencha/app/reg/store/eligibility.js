Ext.define('mobEdu.reg.store.eligibility', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.eligibility'
    ],

    config: {
        storeId: 'mobEdu.reg.store.eligibility',

        autoLoad: false,

        model: 'mobEdu.reg.model.eligibility'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }

});