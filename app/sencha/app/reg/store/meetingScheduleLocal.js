Ext.define('mobEdu.reg.store.meetingScheduleLocal', {
    extend:'Ext.data.Store',
//    alias: 'courseDetailStore',

    requires: [
        'mobEdu.reg.model.meetingScheduleLocal'
    ],

    config: {
        storeId: 'mobEdu.reg.store.meetingScheduleLocal',

        autoLoad: false,

        model: 'mobEdu.reg.model.meetingScheduleLocal',

        proxy:{
            type:'localstorage',
            id:'cartSchedule'
        }
    }
});
