Ext.define('mobEdu.reg.store.finResAgreement', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.reg.model.finResAgreement'
    ],

    config: {
        storeId: 'mobEdu.reg.store.finResAgreement',

        autoLoad: false,

        model: 'mobEdu.reg.model.finResAgreement'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = ''
        return proxy;
    }

});