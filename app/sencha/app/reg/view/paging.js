Ext.define('mobEdu.reg.view.paging', {
	extend: 'Ext.plugin.ListPaging',
	config: {
		id: 'pagingplugin',
		type: 'listpaging',
		autoPaging: true,
		loadMoreText: 'Load Courses...',
		noMoreRecordsText: ''
	}
});
