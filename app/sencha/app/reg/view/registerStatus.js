Ext.define('mobEdu.reg.view.registerStatus', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.reg.store.register'
    ],
    id: 'registerStatusView',
    scroll: 'vertical',
    fullscreen: true,
    config: {
        layout: 'fit',
        items: [{
            xtype: 'list',
            id: 'registerStatusList',
            name: 'registerStatusList',
            disableSelection: true,
            cls: 'logo',
            itemTpl: new Ext.XTemplate('<div>',
                '<tpl if="(mobEdu.reg.f.getStatusCode(statusCode)===2)">',
                '<h2 class="fail"></tpl>',
                '<tpl if="(mobEdu.reg.f.getStatusCode(statusCode)===0)">',
                '<h2 class="success"></tpl>',
                '<tpl if="(mobEdu.reg.f.getStatusCode(statusCode)===1)">',
                '<h2 class="waitlist"></tpl>',
                '{courseTitle} ({crn})</h2>',
                '<ul style="width: 50%;">',
                '<li style="padding-top:5px">{subject} {courseNumber}</li>',
                '<li>{department}</li>',
                '</ul>',
                '<ul style="width: 50%;">',
                '<li><h3>{faculty}</h3></li>',
                '<li><h3>{college}</h3></li>',
                '</ul>',
                '<tpl if="(mobEdu.reg.f.isWarning(errorFlag) === true)"><h4 style="color: #f2f602;clear: both;">{[mobEdu.util.formatUrls(values.statusMessage)]}</h4>',
                '<tpl elseif="(mobEdu.reg.f.getStatusCode(statusCode)===2)">',
                '<h4 class="failMsg" style="clear: both;">{[mobEdu.util.formatUrls(values.statusMessage)]}</h4>',
                '<tpl elseif="(mobEdu.reg.f.getStatusCode(statusCode)===0)">',
                '<h4 class="successMsg" style="clear: both;">{[mobEdu.util.formatUrls(values.statusMessage)]}</h4>',
                '<tpl elseif="(mobEdu.reg.f.getStatusCode(statusCode)===1)">',
                '<h4 class="waitlistMsg" style="clear: both;">{[mobEdu.util.formatUrls(values.statusMessage)]}</h4>',
                '</tpl></div>'),
            store: mobEdu.util.getStore('mobEdu.reg.store.register'),
            singleSelect: true,
            loadingText: ''
        }, {
            title: '<h1>Reg Status</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});