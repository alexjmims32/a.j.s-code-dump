Ext.define('mobEdu.reg.view.immunization', {
    extend: 'Ext.form.Panel',
    statics:{
      acceptAction: null
    },
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: 'fit',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>TTU Immunization</h1>'
        },{
            xtype:'panel',
            id:'immunization',
            padding:'10px',
            html:''
        },
        {
            xtype: 'toolbar',
            docked: 'bottom',
            id:'immuBtmBar',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Submit',
                ui: 'button',
                id: 'immuSubmit',
                handler: function() {
                    mobEdu.reg.f.submitImmunization();
                }
            }
            ]
        }
        ]
    }
});
