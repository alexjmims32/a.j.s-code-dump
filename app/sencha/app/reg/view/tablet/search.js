Ext.define('mobEdu.reg.view.tablet.search', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.reg.store.termList',
        'mobEdu.reg.store.search'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Search</h1>'
        }, {
            xtype: 'fieldset',
            docked: 'top',
            cls: 'searchfieldset',
            items: [{
                xtype: 'selectfield',
                name: 'instSearchTermId',
                valueField: 'code',
                displayField: 'description',
                id: 'instSearchTermId',
                store: mobEdu.util.getStore('mobEdu.reg.store.termList')
            }, {
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    name: 'searchItem',
                    id: 'searchItem',
                    flex: 1,
                    placeHolder: 'Search for a course',
                    autoComplete: false,
                    autoCorrect: false
                }, {
                    xtype: 'button',
                    name: 'searchIcon',
                    id: 'searchIcon',
                    maxWidth: '32px',
                    flex: 3,
                    text: '<img src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png" height="24px" width="24px">',
                    style: {
                        border: 'none',
                        background: 'none'
                    },
                    handler: function() {
                        mobEdu.reg.f.doCheckBeforeSearch(Ext.getCmp('searchItem'), Ext.getCmp('searchRegList'), Ext.getCmp('instSearchTermId'));
                    }
                }, {
                    xtype: 'button',
                    name: 'searchHelp',
                    id: 'searchHelp',
                    maxWidth: '32px',
                    flex: 3,
                    text: '<img src="' + mobEdu.util.getResourcePath() + 'images/tip.png" height="32px" width="32px">',
                    style: {
                        border: 'none',
                        background: 'none'
                    },
                    handler: function() {
                        mobEdu.reg.f.loadTipsPopup();
                    }
                }]
            }]
        }, {
            xtype: 'list',
            name: 'searchRegList',
            id: 'searchRegList',
            style: 'background: transparent',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tpl if="(mobEdu.reg.f.checkingCart(termCode,crn)=== true)">', '<tr><th colspan="2" class="cartCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="cartCourse">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center" title="remove" ><div class="removecart" title="remove" align="center"> </div>', '<tpl if="(mobEdu.reg.f.isOnlineCourse(campusCode)=== false)">', '<h5 title="remove">{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="cartCourse"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(mobEdu.reg.f.isClosed(seatsAvailable,waitedList)=== true)">', '<tr><th colspan="2" class="closedCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="closedCourse">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center" title="add"><div class="addcartred" title="add" align="center"> </div>', '<tpl if="(mobEdu.reg.f.isOnlineCourse(campusCode)=== false)">', '<h5 title="add">{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="closedCourse"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(mobEdu.reg.f.isPreRequestCheck(prereqcheck)=== false)">', '<tr><th colspan="2" class="precheckCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="precheckCourse">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><div class="addcart" title="add" align="center"> </div>', '<tpl if="(mobEdu.reg.f.isOnlineCourse(campusCode)=== false)">', '<h5 title="add" >{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="precheckCourse"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(campusCode === 0)">', '<tr><th colspan="2" class="onlineCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="onlineCourse"  width="80%">', '<td>', '<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><div class="addcart" title="add" align="center"> </div><h5 title="add">{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="onlineCourse"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(campusCode === 1)">', '<tr><th colspan="2" class="wCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="wCourse">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><div class="addcart" title="add" align="center"> </div></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="wCourse"><td colspan="3"><p><h4>{courseComments}</p></h4></td>', '</tpl>', '<tpl elseif="(campusCode === 2)">', '<tr><th colspan="2" class="gmlCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="gmlCourse"  width="80%">', '<td>', '<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><div class="addcart" title="add" align="center"> </div></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="gmlCourse"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(mobEdu.reg.f.isOnlineCourse(campusCode) === true)">', '<tr><th colspan="2" class="gmlCourse"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="gmlCourse"  width="80%">', '<td>', '<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><div class="addcart" title="add" align="center"> </div></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr class="gmlCourse"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl else>', '<tr><th colspan="2" class="course"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  class="course" width="80%">', '<td>', '<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(creditHrs)===true)">', ' ({creditHrs} hrs)', '</tpl>', '</h3></td>', '<td><h3>{[mobEdu.util.facSplit(values.faculty)]}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>', '<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>', '</tpl>', '<tpl if="(mobEdu.reg.f.isDuplicate(courseMeetings,courseDates)===true)"><h3>{courseMeetings}</h3>', '</tpl>', '</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><div class="addcart" title="add" align="center"> </div>' + '<h5 title="add">{noOfSeatsAvailable} of {totalSeats}<br />Available</h5>' + '</td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr  class="course" ><td colspan="3"><p><h3>{courseComments}</h3></p></td>', '</tpl>', '</tpl>', '</tr>' + '</table>'),
            store: mobEdu.util.getStore('mobEdu.reg.store.search'),
            loadingText: '',
            singleSelect: false,
            //          Once we click on add/remove cart button, the list is scrolling to top. the below flag will prevent this scrolling
            scrollToTopOnRefresh: false,
            listeners: {
                itemtap: function(view, index, target, item, e, o) {
                    mobEdu.reg.f.onSearchCourseItemTap(view, index, item, e);
                }
            }
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Adv Search',
                ui: 'button',
                handler: mobEdu.reg.f.loadAdvanceSearch
            }, {
                text: 'View Cart',
                id: 'viewCart',
                ui: 'button',
                handler: function() {
                    mobEdu.reg.f.onViewCartButtonTap();
                }
            }, {
                text: 'Holds',
                id: 'studentHolds',
                ui: 'button',
                handler: function() {
                    mobEdu.crl.f.loadStudentHolds(mobEdu.reg.f.showSearch);
                }
            }]
        }],
        flex: 1
    },
    initialize: function() {
        var searchStore = mobEdu.util.getStore('mobEdu.reg.store.search');
        searchStore.addBeforeListener('load', mobEdu.reg.f.checkForSearchListEnd, this);
    }
});