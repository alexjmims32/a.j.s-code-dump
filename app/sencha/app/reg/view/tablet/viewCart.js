Ext.define('mobEdu.reg.view.tablet.viewCart', {
	extend: 'Ext.Container',
	requires: [
		'mobEdu.reg.store.viewCart',
		'mobEdu.reg.store.courseDetailLocal'
	],
	config: {
		fullscreen: true,
		layout: {
			type: 'card',
			animation: {
				type: 'slide',
				direction: 'left',
				duration: 250
			}
		},
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Cart</h1>',
			id: 'viewCartT'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Calendar',
				ui: 'button',
				id: 'calendarT',
				handler: function() {
					mobEdu.reg.f.loadCartScheduleInfo();
				}
			}, {
				text: 'Clear Cart',
				ui: 'button',
				id: 'clearCartT',
				handler: function() {
					mobEdu.reg.f.clearCart();
				}
			}, {
				text: 'Register',
				ui: 'button',
				id: 'registerT',
				handler: function() {
					mobEdu.reg.f.register();
				}
			}, {
				text: 'Change Term',
				ui: 'button',
				id: 'cartChangeTerm',
				handler: function() {
					mobEdu.util.loadRegTermList(0);
				}
			}]
		}, {
			//            useTitleAsBackText: false,
			docked: 'left',
			width: '30%',
			xtype: 'list',
			cls: 'border',
			name: 'tViewCartList',
			id: 'tViewCartList',
			emptyText: '<center><h3>No course selected</h3><center>',
			itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><th colspan="2"><h2 class="textOverflow">{courseName}</h2></th>' + '<td width="20%" style="align:right" title="remove" ><div class="removecart" title="remove" align:"right"> </div></td>' + '</tr>' + '<tr><td><h3>{subject} {courseNumber}</h3></td></tr>' + '</table>'),
			store: mobEdu.util.getStore('mobEdu.reg.store.viewCart'),
			loadingText: '',
			singleSelect: true,
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.reg.f.onViewCartItemTap(view, index, target, record, e, eOpts);
				}
			}
		}, {
			layout: 'fit',
			items: [{
				xtype: 'dataview',
				name: 'viewCartDetail',
				id: 'viewCartDetail',
				emptyText: '<center><h3>No course selected</h3><center>',
				disableSelection: true,
				itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td width="40%"><h2>Course Title</h2></td><td><h2>:</h2></td><td align="left"><h3>{courseTitle} ({crn})</h3></td></tr>' + '<tr><td width="40%"><h2>Course Id</h2></td><td><h2>:</h2></td><td align="left"><h3>{subject} {courseNumber}' + '<tpl if="(mobEdu.reg.f.isExistsFaculty(seqNo)===true)">', '/ {seqNo}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExistsFaculty(linkId)===true)">', ' - {linkId}', '</tpl></h3></td></tr>' + '<tr><td width="40%"><h2>Subject</h2></td><td><h2>:</h2></td><td align="left"><h3>{description}</h3></td></tr>' + '<tr><td width="40%"><h2>Seats Available</h2></td><td><h2>:</h2></td><td align="left"><h3>{noOfSeatsAvailable} of {totalSeats}</h3></td></tr>' + '<tr><td width="40%"><h2>Instructor</h2></td><td><h2>:</h2></td><td align="left"><h3>', '<tpl if="(mobEdu.reg.f.isEmailExist(facultyEmail) === false)">', '{faculty}', '<tpl else>', '<a href="mailto:{facultyEmail}" >{faculty}</a>', '</tpl>', '</h3></td></tr>' +
					//                    '<tr><td align="right" width="50%"><h2>Lecture Hours :</h2></td><td align="left"><h3>{lectureHours}</h3></td></tr>' +
					'<tr><td width="40%"><h2>Term</h2></td><td><h2>:</h2></td><td align="left"><h3>{termDescription}</h3></td></tr>' + '<tr><td width="40%"><h2>Credit Hours</h2></td><td><h2>:</h2></td><td align="left"><h3>{credits}</h3></td></tr>' + '<tr><td width="40%"><h2>Department</h2></td><td><h2>:</h2></td><td align="left"><h3>{department}</h3></td></tr>' + '<tr><td width="40%"><h2>College</h2></td><td><h2>:</h2></td><td align="left"><h3>{college}</h3></td></tr>' + '<tr><td valign="top" width="40%"><h2>Level</h2></td><td><h2>:</h2></td><td align="left"><p><h3>{[this.getLevel(values.level)]}</h3></p></td></tr>' + '<tr><td width="40%"><h2>Campus</h2></td><td><h2>:</h2></td><td align="left"><h3>{campus}</h3></td></tr>' + '<tr><td valign="top" width="40%"><h2>Schedule</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>' + '<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===true)">', '<tpl for="meeting">', '<tpl if="(mobEdu.reg.f.isExists(startDate)===true && mobEdu.reg.f.isExists(endDate)===true)">', '{startDate} : {endDate}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(beginTime)===true && mobEdu.reg.f.isExists(endTime)===true)">', '{beginTime} - {endTime}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(period)===true)">', '{period}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(bldgCode)===true)">', 'Building: {bldgCode}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(roomCode)===true)">', 'Room: {roomCode}<br /><br />', '</tpl>', '</tpl>', '</tpl>', '<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===false)">', 'No meeting info available', '</tpl>', '</h4></td></tr>' + '</table>', {
						compiled: true,
						getLevel: function(level) {
							return String(level).replace(/[,]/g, "<br />");
						}
					}),
				store: mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal'),
				loadingText: ''
			}]
		}]
	}
});