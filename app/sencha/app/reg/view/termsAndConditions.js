Ext.define('mobEdu.reg.view.termsAndConditions', {
    extend: 'Ext.form.Panel',
    statics: {
        acceptAction: null,
        params: null
    },
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: 'fit',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Terms and Conditions</h1>'
        }, {
            xtype: 'panel',
            id: 'aboutTandC',
            padding: '10px',
            html: '',
            cls: 'ul-styled'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Cancel',
                ui: 'button',
                id: 'rejectTerms',
                handler: function() {
                    (mobEdu.util.getPrevView())();
                }
            }, {
                text: 'Continue',
                ui: 'button',
                id: 'acceptTerms',
                handler: function() {
                    (mobEdu.reg.view.acceptAction)(mobEdu.reg.view.params);
                }
            }]
        }]
    }
});