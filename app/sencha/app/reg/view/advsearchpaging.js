Ext.define('mobEdu.reg.view.advsearchpaging', {
	extend: 'Ext.plugin.ListPaging',
	config: {
		id: 'advsearchpagingplugin',
		type: 'listpaging',
		autoPaging: true,
		loadMoreText: 'Load Courses...',
		noMoreRecordsText: ''
	}
});
