Ext.define('mobEdu.reg.view.cartSchedule', {
    extend: 'Ext.Container',
    alias: 'cartSchedule',
    config: {
        fullscreen: 'true',
        cls:'backGround',
        layout: {
            type: 'vbox',
            align: 'strech'
        },
        defaults: {
            flex: 0
        },
        scrollable: true,
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            docked: 'top',
            id: 'cartDock',
            name: 'cartDock'
        }, {
            xtype: 'panel',
            id: 'cartCalList',
            name: 'cartCalList',
            disableSelection: true,
            items: [{
                padding: '10 0 0 0'
            }, {
                xtype: 'label',
                id: 'cartLabel',
                padding: '0 0 0 10',
                html: ''
            }]
        }]
    }
});