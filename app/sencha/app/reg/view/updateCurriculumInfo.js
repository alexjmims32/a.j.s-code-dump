Ext.define('mobEdu.reg.view.updateCurriculumInfo', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.reg.store.curriculumInfo'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		items: [{
			xtype: 'fieldset',
			items: [{
				xtype: 'selectfield',
				labelAlign: 'left',
				labelWidth: '40%',
				id: 'selectCurriclum',
				name: 'selectCurriclum',
				label: 'Curriculum',
				height: 'auto',
			}, 
			// {
			// 	xtype: 'selectfield',
			// 	labelAlign: 'left',
			// 	labelWidth: '40%',
			// 	id: 'selectCatalog',
			// 	name: 'selectCatalog',
			// 	label: 'Catalog',
			// 	height: 'auto',
			// },
			 {
				xtype: 'selectfield',
				labelAlign: 'left',
				labelWidth: '40%',
				id: 'selectGrad',
				name: 'selectGrad',
				label: 'Graduation',
				height: 'auto',
			}]
		}, {
			xtype: 'customToolbar',
			title: '<h1>Confirm Your Attributes</h1>'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				ui: 'button',
				text: 'Submit',
				handler: function() {
					mobEdu.reg.f.submitCurriculumData();
				}
			}]
		}]
	}
});