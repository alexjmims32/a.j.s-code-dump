Ext.define('mobEdu.reg.view.phone.search', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.reg.store.termList',
		'mobEdu.reg.store.search'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Search</h1>'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset',
			items: [{
				xtype: 'selectfield',
				name: 'instSearchTermId',
				valueField: 'code',
				displayField: 'description',
				id: 'instSearchTermId',
				store: mobEdu.util.getStore('mobEdu.reg.store.termList')
			}, {
				layout: 'hbox',
				items: [{
					xtype: 'textfield',
					name: 'searchItem',
					id: 'searchItem',
					style: 'padding-left: 10px;',
					flex: 1,
					cls: 'cleartextfield',
					placeHolder: 'Search Course',
					autoComplete: false,
					autoCorrect: false
				}, {
					xtype: 'button',
					name: 'searchIcon',
					id: 'searchIcon',
					maxWidth: '32px',
					height: '40px',
					flex: 3,
					text: '<img src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png" height="24px" width="24px">',
					style: {
						border: 'none',
						background: 'none'
					},
					handler: function() {
						mobEdu.reg.f.doCheckBeforeSearch(Ext.getCmp('searchItem'), Ext.getCmp('searchRegList'), Ext.getCmp('instSearchTermId'));
					}
				}, {
					xtype: 'button',
					name: 'searchHelp',
					id: 'searchHelp',
					maxWidth: '32px',
					height: '40px',
					flex: 3,
					text: '<img src="' + mobEdu.util.getResourcePath() + 'images/tip.png" height="24px" width="24px">',
					style: {
						border: 'none',
						background: 'none'
					},
					handler: function() {
						mobEdu.reg.f.loadTipsPopup();
					}
				}]
			}]
		}, {
			xtype: 'list',
			name: 'searchRegList',
			id: 'searchRegList',
			style: 'background: transparent',
			itemTpl: new Ext.XTemplate(advSearchTemplate),
			store: mobEdu.util.getStore('mobEdu.reg.store.search'),
			loadingText: '',
			singleSelect: false,
			//          Once we click on add/remove cart button, the list is scrolling to top. the below flag will prevent this scrolling
			scrollToTopOnRefresh: false,
			listeners: {
				itemtap: function(view, index, target, item, e, o) {
					mobEdu.reg.f.onSearchCourseItemTap(view, index, item, e);
				}
			}
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Adv Search',
				ui: 'button',
				handler: mobEdu.reg.f.loadAdvanceSearch
			}, {
				text: 'View Cart',
				id: 'viewCart',
				ui: 'button',
				handler: function() {
					mobEdu.reg.f.onViewCartButtonTap();
				}
			}, {
				text: 'Holds',
				id: 'studentHolds',
				ui: 'button',
				handler: function() {
					mobEdu.crl.f.loadStudentHolds(mobEdu.reg.f.showSearch);
				}
			}]
		}],
		flex: 1
	},
	initialize: function() {
		var searchStore = mobEdu.util.getStore('mobEdu.reg.store.search');
		searchStore.addBeforeListener('load', mobEdu.reg.f.checkForSearchListEnd, this);
	}
});