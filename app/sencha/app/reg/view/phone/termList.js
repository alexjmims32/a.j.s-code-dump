Ext.define('mobEdu.reg.view.phone.termList', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.main.store.termList'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        items: [{
            xtype: 'list',
            id: 'termList',
            name: 'termList',
            cls: 'logo',
            itemTpl: '<h3>{description}</h3>',
            store: mobEdu.util.getStore('mobEdu.main.store.termList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts) {
                    mobEdu.reg.f.onTermItemTap(view, index, target, record, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>Terms</h1>'
        }],
        flex: 1
    }
});