Ext.define('mobEdu.reg.view.phone.viewCart', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.reg.store.viewCart'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			cls: 'logo',
			id: 'pViewCartList',
			itemTpl: new Ext.XTemplate('<div><h2>{courseName} ({crn})</h2></div> <div> <div style="width: 80%;display:inline-block;"> <div style="margin: 3px"> <div style="display:inline-block;width: 50%"><h3>{subject} {courseNumber} <tpl if="(mobEdu.reg.f.isExistsFaculty(seqNo)===true)">', '/ {seqNo}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExistsFaculty(linkId)===true)">', ' - {linkId} ', '</tpl>', '</h3></div><div style="display:inline-block;float:right; width: 50%"><h3>{faculty}</h3></div> </div> <div style="margin: 3px"> <div style="display:inline-block; width: 50%"><h3>{department}</h3></div><div style="display:inline-block;float:right;  width: 50%"><h3>{college}</h3></div> </div> </div> <div class="removecart" title="remove" style="width: 20%; position: absolute; height: 100%; display:inline-block; float: right; align: center"></div> </div>'),
			store: mobEdu.util.getStore('mobEdu.reg.store.viewCart'),
			loadingText: '',
			singleSelect: false,
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.reg.f.onViewCartItemTap(view, index, target, record, e, eOpts);
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>Cart</h1>',
			id: 'viewCartP'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				ui: 'button',
				xtype: 'button',
				text: 'Calendar',
				id: 'calendarP',
				handler: function() {
					mobEdu.reg.f.loadCartScheduleInfo();
				}
			}, {
				text: 'Clear',
				ui: 'button',
				id: 'clearCartP',
				handler: function() {
					mobEdu.reg.f.clearCart();
				}
			}, {
				text: 'Terms',
				ui: 'button',
				id: 'cartChangeTerm',
				handler: function() {
					mobEdu.util.loadRegTermList(0);
				}
			}, {
				text: 'Register',
				ui: 'button',
				id: 'registerP',
				handler: function() {
					mobEdu.reg.f.register();
				}
			}]
		}],
		flex: 1
	}
});