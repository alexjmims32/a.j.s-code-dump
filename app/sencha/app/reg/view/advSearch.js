Ext.define('mobEdu.reg.view.advSearch', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.reg.store.register'
	],
	fullscreen: true,
	config: {
		layout: 'fit',
		scrollable: true,
		items: [{
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				name: 'advCrn',
				id: 'advCrn',
				placeHolder: 'Enter CRN',
				clearIcon: true,
				autoComplete: false,
				autoCorrect: false
			}, {
				xtype: 'selectfield',
				name: 'advSub',
				id: 'advSub',
				options: [{
					text: 'Select Subject',
					value: null
				}]
			}, {
				xtype: 'searchfield',
				name: 'txtAdvSub',
				id: 'txtAdvSub',
				placeHolder: 'Enter Subject',
				clearIcon: true,
				listeners: {
					focus: function() {
						mobEdu.reg.f.loadSubjectPopup();
					},
					clearicontap: function(textfield, e, eOpts) {
						// In Android, clearing the field causes focus event to be raised
						// So use a flag and do not show the popup, immediately after the clear icon is tapped
						if (Ext.os.is.Android) {
							mobEdu.reg.view.subject.clearIconTapped = true;
						}
					},
				}
			}, {
				xtype: 'textfield',
				name: 'advCrseNum',
				id: 'advCrseNum',
				placeHolder: 'Enter Course Number',
				clearIcon: true,
				autoComplete: false,
				autoCorrect: false
			}, {
				xtype: 'selectfield',
				name: 'advCrseLevel',
				id: 'advCrseLevel',
				options: [{
					text: 'Select Course Level',
					value: null
				}]
			}, {
				xtype: 'textfield',
				name: 'advTitle',
				id: 'advTitle',
				placeHolder: 'Enter Title',
				clearIcon: true,
				autoComplete: false,
				autoCorrect: false
			}, {
				xtype: 'selectfield',
				name: 'advPartOfTerm',
				id: 'advPartOfTerm',
				placeHolder: 'Enter Part of term',
				options: [{
					text: 'Select Part of Term',
					value: null
				}]
			}, {
				xtype: 'selectfield',
				name: 'advScheduleType',
				id: 'advScheduleType',
				options: [{
					text: 'Select Schedule Type',
					value: null
				}]
			}, {
				xtype: 'selectfield',
				name: 'advCampus',
				id: 'advCampus',
				options: [{
					text: 'Select Campus',
					value: null
				}]
			}, {
				xtype: 'selectfield',
				name: 'advInstructnlMethod',
				id: 'advInstructnlMethod',
				options: [{
					text: 'Select Instructional Method',
					value: null
				}]
			}]
		}, {
			title: '<h1>Advanced Search</h1>',
			xtype: 'customToolbar'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Search',
				ui: 'button',
				handler: function() {
					mobEdu.reg.f.onAdvSearchBtnTap();
				}
			}]
		}],
		flex: 1
	}
});