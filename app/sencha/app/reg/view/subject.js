Ext.define('mobEdu.reg.view.subject', {
	extend: 'Ext.Panel',
	statics: {
		clearIconTapped: false
	},
	config: {
		id: 'subjectPopup',
		scrollable: 'none',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '70%',
		height: '50%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'textfield',
			name: 'subjectDesc',
			id: 'subjectDesc',
			placeHolder: 'Enter Subject',
			docked: 'top',
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.reg.f.onSubjectKeyup(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.reg.f.onSubjectItemClear(textfield, e);
				},
				blur: function(textfield, e, eOpts) {
					mobEdu.util.hideKeyboard();
				}
			}
		}, {
			xtype: 'dataview',
			id: 'subjectList',
			name: 'subjectList',
			cls: 'logo subjectlist',
			itemTpl: '<h3>{description}</h3>',
			store: mobEdu.util.getStore('mobEdu.reg.store.subjectList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.reg.f.onSubjectItemTap(view, index, target, record, e, eOpts);
				}
			}
		}],
		flex: 1,
		listeners: {
			hide: function() {
				mobEdu.reg.f.hideSubjectPopup();
			}
		}
	}
});