Ext.define('mobEdu.reg.view.advSearchResult', {
    extend: 'Ext.Panel',
    requires:[
		'mobEdu.reg.store.termList',
		'mobEdu.reg.store.search'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Results</h1>'
        }, {
            xtype: 'list',
            name: 'advSearchRegList',
            id: 'advSearchRegList',
            cls: 'logo',
            style: 'background: transparent',
            emptyText: 'No courses available',
            //itemTpl: new Ext.XTemplate('<table width="100%">' + '<tpl if="(mobEdu.reg.f.checkingCart(termCode,crn)=== true)">', '<tr><th colspan="2" style="color:#12BB15;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#12BB15">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>','</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/removecart.png" name="remove" /><br />', '<tpl if="(mobEdu.reg.f.isOnlineCourse(campusCode)=== false)">', '<h5>{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#12BB15"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(mobEdu.reg.f.isClosed(seatsAvailable,waitedList)=== true)">', '<tr><th colspan="2" style="color:#000000;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#000000">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>','<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcartred.png" name="add" /><br />', '<tpl if="(mobEdu.reg.f.isOnlineCourse(campusCode)=== false)">', '<h5>{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#000000"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>' ,'<tpl elseif="(mobEdu.reg.f.isPreRequestCheck(prereqcheck)=== false)">', '<tr><th colspan="2" style="color:#FF0000;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#FF0000">', '<td>','<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>','<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcart.png" name="add" /><br />', '<tpl if="(mobEdu.reg.f.isOnlineCourse(campusCode)=== false)">', '<h5>{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#FF0000"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(campusCode === 0)">', '<tr><th colspan="2" style="color:#0000FF;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#0000FF"  width="80%">', '<td>','<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>','<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcart.png" name="add" /><br /><h5>{noOfSeatsAvailable} of {totalSeats}<br />Available</h5></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#0000FF"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl elseif="(campusCode === 1)">', '<tr><th colspan="2" style="color:#A901DB;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#A901DB">', '<td>','<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>','</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcart.png" name="add" /></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#A901DB"><td colspan="3"><p><h4>{courseComments}</p></h4></td>', '</tpl>','<tpl elseif="(campusCode === 2)">', '<tr><th colspan="2" style="color:#868A08;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#868A08"  width="80%">', '<td>','<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>','<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcart.png" name="add" /></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#868A08"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>','<tpl elseif="(mobEdu.reg.f.isOnlineCourse(campusCode) === true)">', '<tr><th colspan="2" style="color:#868A08;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#868A08"  width="80%">', '<td>','<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>','</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcart.png" name="add" /></td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#868A08"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '<tpl else>', '<tr><th colspan="2" style="color:#000000;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#000000" width="80%">', '<td>','<table width="100%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>','<tr>', '<td>','<tpl if="(mobEdu.reg.f.isExists(courseDates)===true)"><h3>{courseDates}</h3>','</tpl>','<tpl if="(mobEdu.reg.f.isExists(courseMeetings)===true)"><h3>{courseMeetings}</h3>','</tpl>','</td>', '</tr>', '</table>', '</td>', '<td width="20%" align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/addcart.png" name="add" /><br />' + '<h5>{noOfSeatsAvailable} of {totalSeats}<br />Available</h5>' + '</td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr  style="color:#000000" ><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '</tpl>', '</tr>' + '</table>'),
            itemTpl: new Ext.XTemplate(advSearchTemplate),
            store: mobEdu.util.getStore('mobEdu.reg.store.advSearch'),
            loadingText: '',
            singleSelect: false,
            //          Once we click on add/remove cart button, the list is scrolling to top. the below flag will prevent this scrolling
            scrollToTopOnRefresh: false,
            listeners: {
                itemtap: function(view, index, target, item, e, o) {
                    mobEdu.reg.f.onAdvSearchCourseItemTap(view, index, item, e,null,null,null,mobEdu.reg.f.showAdvancedSearchResult);
                }
            }
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'View Cart',
                id: 'advViewCart',
                ui: 'button',
                handler: function(){
                    mobEdu.reg.f.onViewCartButtonTap();
                }
            }, {
                text: 'Holds',
                id: 'advStudentHolds',
                ui: 'button',
                handler:  function() {
                    mobEdu.crl.f.loadStudentHolds(mobEdu.reg.f.showAdvancedSearchResult);
                }
            }]
        }],
        flex: 1
    },
    initialize: function() {
        var searchStore = mobEdu.util.getStore('mobEdu.reg.store.advSearch');
        searchStore.addBeforeListener('load', mobEdu.reg.f.checkForSearchListEnd, this);
    }
});