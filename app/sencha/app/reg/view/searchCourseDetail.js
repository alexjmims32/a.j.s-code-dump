Ext.define('mobEdu.reg.view.searchCourseDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.reg.store.courseDetailLocal'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            id: 'searchDetailList',
            name: 'searchDetailList',
            disableSelection: true,
            emptyText: '<center><h3>No course selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td  width="30%"><h2>Course Title</h2></td><td valign="top"><h2>:</h2></td><td valign="top" align="left"><h3>{courseTitle} ({crn})</h3></td></tr>' + '<tr><td  width="30%"><h2>Course Id</h2></td><td><h2>:</h2></td><td align="left"><h3>{subject} {courseNumber} ' + '<tpl if="(mobEdu.reg.f.isExistsFaculty(seqNo)===true)">', '/ {seqNo}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExistsFaculty(linkId)===true)">', ' - {linkId}', '</tpl></h3></td></tr>' + '<tr><td  width="30%"><h2>Subject</h2></td><td><h2>:</h2></td><td align="left"><h3>{description}</h3></td></tr>' + '<tr><td  width="30%"><h2>Credit Hours</h2></td><td><h2>:</h2></td><td align="left"><h3>{credits}</h3></td></tr>' + '<tr><td  width="30%"><h2>Seats Available</h2></td><td><h2>:</h2></td><td align="left"><h3>{noOfSeatsAvailable} of {totalSeats}</h3></td></tr>' + '<tpl if="(mobEdu.reg.f.isExistsFaculty(faculty)===true)">', '<tr><td  width="30%"><h2>Instructor</h2></td><td><h2>:</h2></td><td align="left"><h3>', '<tpl if="(mobEdu.reg.f.isEmailExist(facultyEmail) === false)">', '{[mobEdu.util.insSplit(values.faculty)]}', '<tpl else>', '<a href="mailto:{facultyEmail}" >{[mobEdu.util.insSplit(values.faculty)]}</a>', '</tpl>', '</h3></td></tr>', '</tpl>' +
                //                '<tr><td align="right" width="50%"><h2>Lecture Hours :</h2></td><td align="left"><h3>{lectureHours}</h3></td></tr>' +
                '<tr><td  width="30%"><h2>Term</h2></td><td><h2>:</h2></td><td align="left"><h3>{termDescription}</h3></td></tr>' + '<tr><td  width="30%"><h2>Department</h2></td><td><h2>:</h2></td><td align="left"><h3>{department}</h3></td></tr>' + '<tr><td  width="30%"><h2>College</h2></td><td><h2>:</h2></td><td align="left"><h3>{college}</h3></td></tr>' + '<tr><td  valign="top" width="30%"><h2>Level</h2></td><td valign="top"><h2>:</h2></td><td align="left"><p><h3>{[this.getLevel(values.level)]}</h3></p></td></tr>' + '<tr><td  width="30%"><h2>Campus</h2></td><td><h2>:</h2></td><td align="left"><h3>{campus}</h3></td></tr>' + '<tr><td valign="top"  width="40%"><h2>Prerequisite</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>' +
                '<tpl if="(mobEdu.reg.f.hasPrerequisites(prerequisiteArray)===true)">',
                '<table>' +
                '<tpl for="prerequisiteArray">',
                '<tr><td>' +
                '{prerequisite}'+
                '</td></tr>' +
                '</tpl>',
                '</table>',
                '</tpl>',
                '<tpl if="(mobEdu.reg.f.hasPrerequisites(prerequisiteArray)===false)">',
                'No prerequisites available',
                '</tpl>',
                '</h4></td></tr>' +'<tr><td valign="top"  width="30%"><h2>Schedule</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>' + '<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===true)">', '<table>' + '<tpl for="meeting">', '<tr><td>' + '<tpl if="(mobEdu.reg.f.isExists(startDate)===true && mobEdu.reg.f.isExists(endDate)===true)">', '{[this.changeDateFormat(values.startDate)]}' + ' - ' + '{[this.changeDateFormat(values.endDate)]}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(beginTime)===true && mobEdu.reg.f.isExists(endTime)===true)">', '{beginTime} - {endTime}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(period)===true)">', '{period}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(bldgCode)===true)">', 'Building: {bldgCode}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(roomCode)===true)">', 'Room: {roomCode}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isScheduleDesc(scheduleDesc)===true)">', 'Type: {scheduleDesc}<br />' + '</tpl>', '<hr>' + '</td></tr>' + '</tpl>', '</table>' + '</tpl>', '<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===false)">', 'No meeting info available', '</tpl>', '</h3></td></tr>' + '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '<tr><td valign="top"  width="30%">', '<h2>Comments</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h4>{courseComments}' + '</h4></td></tr>' + '</tpl>', '</table>', {
                    compiled: true,
                    getLevel: function(level) {
                        return String(level).replace(/[,]/g, "<br />");
                    },
                    changeDateFormat: function(date) {
                        var dateValue;
                        var dateValueFormat = Ext.Date.parse(date, "m-d-Y");
                        dateValue = Ext.Date.format(dateValueFormat, meetingsDateFormat);
                        return dateValue;
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal'),
            loadingText: '',
            singleSelect: true
        }, {
            xtype: 'customToolbar',
            title: '<h1>Course Details</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Add to Cart',
                id: 'addToCart',
                name: 'addToCart',
                handler: function() {
                    mobEdu.reg.f.searchCourseDetailAddToCart();
                }
            }, {
                text: 'View Cart',
                ui: 'button',
                handler: function() {
                    mobEdu.reg.f.onViewCartButtonTap();
                }
            }]
        }],
        flex: 1
    }
});