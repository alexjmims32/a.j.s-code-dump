Ext.define('mobEdu.reg.view.olrPopup', {
	extend: 'Ext.Panel',
	config: {
		id: 'olrPopup',
		scroll: 'vertical',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		layout: 'fit',
		width: '70%',
		height: '90%',
		items: [{
			xtype: 'toolbar',
			docked: 'top',
			title: '<h1>Online Registration</h1>',
			ui: 'light'
		}, {
			xtype: 'textfield',
			name: 'olrStartDate',
			id: 'olrStartDate',
			placeHolder: 'MM/DD/YYYY',
			labelWidth: '40%',
			label: 'Start Date',
			required: false,
			useClearIcon: true,
			docked: 'top'
		}, {
			xtype: 'textfield',
			name: 'olrEndDate',
			id: 'olrEndDate',
			placeHolder: 'MM/DD/YYYY',
			labelWidth: '40%',
			label: 'End Date',
			required: false,
			useClearIcon: true,
			docked: 'top'
		}, {
			xtype: 'dataview',
			name: 'olrDetails',
			id: 'olrDetails',
			itemTpl: new Ext.XTemplate('<table width="100%">' +
				'<tr><td align="right" valign="top" width="50%"><h2>CRN:</h2></td><td align="left"><h3>{crn}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Course Title:</h2></td><td align="left" class="wordWrap"><h3>{courseTitle}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Duration:</h2></td><td align="left"><h3>{duration}</h3></td></tr>' +
				'<tr><td align="right" valign="top" width="50%" class="wordWrap"><h2>Permitted Start Dates:</h2></td><td align="left" class="wordWrap"><h3>{startDate}</h3></td></tr>' +
				'<tr><td align="right" valign="top" width="50%" class="wordWrap"><h2>Permitted End Dates:</h2></td><td align="left" class="wordWrap"><h3>{endDate}</h3></td></tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.reg.store.olrStore'),
			singleSelect: true,
			loadingText: ''
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Close',
				ui: 'button',
				handler: function() {
					mobEdu.reg.f.closeOlrPopup();
				}
			}, {
				text: 'Submit',
				ui: 'button',
				handler: function() {
					mobEdu.reg.f.submitPopup();
				}
			}]
		}],
		flex: 1
	}
});