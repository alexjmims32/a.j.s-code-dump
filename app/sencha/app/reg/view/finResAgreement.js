Ext.define('mobEdu.reg.view.finResAgreement', {
    extend: 'Ext.form.Panel',
    statics: {
        acceptAction: null,
        params: null
    },
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: 'fit',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Financial Responsibility Agreement</h1>'
        }, {
            xtype: 'panel',
            id: 'finResAgreementData',
            scrollable: {
                direction: 'vertical',
                directionLock: true
            },
            padding: '10px',
            cls: 'ol-styled'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Agree',
                ui: 'button',
                id: 'acceptAgreement',
                handler: function() {
                    mobEdu.reg.f.loadFinResAgreement();
                }
            }, {
                text: 'Disagree',
                ui: 'button',
                id: 'rejectAgreement',
                handler: function() {
                    (mobEdu.util.getPrevView())();
                }
            }]
        }]
    }
});