Ext.define('mobEdu.main.store.preModules', {
	extend: 'Ext.data.Store',
	requires: [
		'mobEdu.main.model.modules'
	],
	config: {
		storeId: 'mobEdu.main.store.preModules',
		autoLoad: true,
		model: 'mobEdu.main.model.modules',
		data: [
			{code: 'ENQUIRE', order: '104', description: 'Prospective student', icon:'prospective-student.png', position:'MAIN',authRequired:false},
			{code: 'STUDENT', order: '101', description: 'Student', icon:'student-profile.png', position:'MAIN',authRequired:true},
			{code: 'FACULTY', order: '102', description: 'Faculty', icon:'advisor.png', position:'MAIN',authRequired:true},
			{code: 'ALU', order: '105', description: 'Alumni', icon:'alumni.png', position:'MAIN',authRequired:false},
			{code: 'ADVISOR', order: '103', description: 'Advisor', icon:'advisor.png', position:'MAIN',authRequired:true},
			{code: 'NEWS', order: '106', description: 'News', icon:'campus-news.png', position:'MAIN',authRequired:false},
			{code: 'MAPS', order: '107', description: 'Maps', icon:'campus-map.png', position:'MAIN',authRequired:false},
			{code: 'EVENTS', order: '108', description: 'Events', icon:'events.png', position:'MAIN',authRequired:false},
			{code: 'HELP', order: '109', description: 'Help/FAQs', icon:'help.png', position:'MAIN',authRequired:false},
			{code: 'VIDEOS', order: '110', description: 'Videos', icon:'video.png', position:'MAIN',authRequired:false},
			{code: 'SPORTS', order: '111', description: 'Athletics', icon:'athletics.png', position:'MAIN',authRequired:false},
			{code: 'EMER', order: '112', description: 'Emergencies', icon:'directory.png', position:'MAIN',authRequired:false},
			{code: 'BS', order: '113', description: 'Bookstore', icon:'bookstore.png', position:'MAIN',authRequired:false},
			{code: 'TWITTER', order: '114', description: 'Social Media', icon:'socialmedia.png', position:'MAIN',authRequired:false},
			{code: 'SETTINGS', order: '115', description: 'Settings', icon:'settings.png', position:'MAIN',authRequired:false}
		]
	}
});