Ext.define('mobEdu.main.store.modulesLocal', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.main.model.modules'
    ],

    config:{
        storeId: 'mobEdu.main.store.modulesLocal',
        autoLoad: true,
        autoSync: true,
        model: 'mobEdu.main.model.modules',
        sorters:{
             property :'order',        direction:'ASC'
        },
        proxy:{
            type:'localstorage',
            id:'modulesLocalLocalStorage'
        }
    }
});