Ext.define('mobEdu.main.store.modules', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.data.store',
		'mobEdu.main.model.modules'
	],
	config: {
		storeId: 'mobEdu.main.store.modules',
		autoLoad: false,
		model: 'mobEdu.main.model.modules'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'modules';
		return proxy;
	}
});