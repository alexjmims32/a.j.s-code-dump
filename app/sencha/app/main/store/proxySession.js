Ext.define('mobEdu.main.store.proxySession', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.main.model.proxySession'
    ],

    config:{
        storeId: 'mobEdu.main.store.proxySession',
        autoLoad: false,
        model: 'mobEdu.main.model.proxySession'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= '';
        return proxy;
    }
});