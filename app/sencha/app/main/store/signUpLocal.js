Ext.define('mobEdu.main.store.signUpLocal', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.Store',
        'mobEdu.main.model.signUp'
    ],

    config: {
        storeId: 'mobEdu.main.store.signUpLocal',
        autoLoad: true,
        model: 'mobEdu.main.model.signUp',
        proxy: {
            type: 'localstorage',
            id: 'signUpStorage'
        }
    }
});