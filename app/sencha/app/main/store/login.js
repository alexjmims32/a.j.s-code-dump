Ext.define('mobEdu.main.store.login', {
	extend: 'mobEdu.data.store',

	requires: [
		'mobEdu.main.model.login'
	],

	config: {
		storeId: 'mobEdu.main.store.login',

		autoLoad: false,

		model: 'mobEdu.main.model.login'
	}
});