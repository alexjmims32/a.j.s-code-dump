Ext.define('mobEdu.main.store.slidingMenu', {
    extend: 'Ext.data.Store',
    requires: [
            'mobEdu.main.model.modules'
    ],
    config: {
        storeId: 'mobEdu.main.store.slidingMenu',
        autoLoad: true,
        model: 'mobEdu.main.model.modules',
        sorters: {
            property: 'order',
            direction: 'ASC'
        },
        proxy: {
            type: 'localstorage',
            id: 'slidingMenuLocalStorage'
        }
    }
});