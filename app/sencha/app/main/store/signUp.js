Ext.define('mobEdu.main.store.signUp',{
extend: 'mobEdu.data.store',
    requires:[
        'mobEdu.main.model.signUp'
    ],
    config:{
        storeId:'mobEdu.main.store.signUp',
        autoLoad: false,

        model: 'mobEdu.main.model.signUp'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''
        return proxy;
    }
})