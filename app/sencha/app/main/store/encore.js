Ext.define('mobEdu.main.store.encore', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.main.model.encore'
    ],

    config:{
        storeId: 'mobEdu.main.store.encore',
        autoLoad: true,
        model: 'mobEdu.main.model.encore',
        proxy:{
            type:'localstorage',
            id:'encoreStorage'
        }
    }
});