Ext.define('mobEdu.main.store.termList', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.main.model.termList'
    ],

    config:{
        storeId: 'mobEdu.main.store.termList',

        autoLoad: false,

        model: 'mobEdu.main.model.termList'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'registerTermList'        
        return proxy;
    }
});
