Ext.define('mobEdu.main.store.menu', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.main.model.menu'
    ],

    config:{
        storeId: 'mobEdu.main.store.menu',
        autoLoad: false,
        model: 'mobEdu.main.model.menu',
        proxy:{
            type:'localstorage',
            id:'mainmenu'
        }
    }
});