Ext.define('mobEdu.main.store.entourage', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.Store',
        'mobEdu.main.model.entourage'
    ],

    config: {
        storeId: 'mobEdu.main.store.entourage',
        autoLoad: true,
        model: 'mobEdu.main.model.entourage',
        proxy: {
            type: 'localstorage',
            id: 'entourageStorage'
        }
    }
});