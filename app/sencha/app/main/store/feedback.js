Ext.define('mobEdu.main.store.feedback',{
extend: 'mobEdu.data.store',
    requires:[
        'mobEdu.main.model.recipient'
    ],
    config:{
        storeId:'mobEdu.main.store.feedback',
        autoLoad: false,

        model: 'mobEdu.main.model.recipient'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''
        return proxy;
    }
})