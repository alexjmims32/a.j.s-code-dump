Ext.define('mobEdu.main.store.term',{
extend: 'mobEdu.data.store',
     requires:[
       'mobEdu.main.model.term'
   ],
   config:{
       storeId:'mobEdu.main.store.term',
        autoLoad: false,

        model: 'mobEdu.main.model.term'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
})