Ext.define('mobEdu.main.store.addressChecking', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.main.model.login'
    ],

    config: {
        storeId: 'mobEdu.main.store.addressChecking',

        autoLoad: false,

        model: 'mobEdu.main.model.login',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//            root: ' '
            }
        }
    }

});