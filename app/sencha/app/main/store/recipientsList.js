Ext.define('mobEdu.main.store.recipientsList', {
  extend: 'Ext.ux.OfflineSyncStore',

  requires: [
    'mobEdu.main.model.recipient'
  ],

  config: {
    model: 'mobEdu.main.model.recipient',

    trackLocalSync: false,
    autoServerSync: false,

    // define a LOCAL proxy for saving the store's data locally
    localProxy: {
      type: 'localstorage',
      id: 'feedback-sync-store'
    },

    // define a SERVER proxy for saving the store's data on the server
    serverProxy: {
      type: 'ajax',
      api: {
        read: encorewebserver + 'open/feedbackReceipients'
      },
      extraParams: {
        clientCode: campusCode,
        companyId: companyId
      },
      headers: {
        companyID: companyId
      },
      reader: {
        type: 'json',
        rootProperty: 'feedback'
      }
    }
  },

  initialize: function() {
    this.loadLocal();
  }
});