Ext.define('mobEdu.main.store.favourites', {
	extend: 'mobEdu.data.store',
	requires: [
		'mobEdu.data.store',
		'mobEdu.main.model.modules'
	],
	config: {
		storeId: 'mobEdu.main.store.favourites',
		autoLoad: false,
		model: 'mobEdu.main.model.modules'
	},
	initProxy: function() {
		var proxy = this.callParent();
		proxy.reader.rootProperty = 'favourites';
		return proxy;
	}
});