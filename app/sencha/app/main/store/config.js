Ext.define('mobEdu.main.store.config',{
extend: 'mobEdu.data.store',
     requires:[
       'mobEdu.main.model.config'
   ],
   config:{
       storeId:'mobEdu.main.store.config',
        autoLoad: false,

        model: 'mobEdu.main.model.config'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'configuration'        
        return proxy;
    }
})