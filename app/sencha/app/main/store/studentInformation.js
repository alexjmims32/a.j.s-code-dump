Ext.define('mobEdu.main.store.studentInformation', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.main.model.studentInformation'
    ],

    config:{
        storeId: 'mobEdu.main.store.studentInformation',
        autoLoad: true,
        model: 'mobEdu.main.model.studentInformation',
        proxy:{
            type:'localstorage',
            id:'studentInfoLocalStorage'
        }
    }
});
