Ext.define('mobEdu.main.view.menu', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.main.f'
    ],
    config: {
        //style: 'background: #fff;',
        scrollable: 'vertical',
        fullscreen: true,
        styleHtmlContent: true,
        layout: 'vbox',
        cls: ["logo",
            "clientlogo",
            "gridMenuBackGroundClr"
        ],
        items: [{
            html: '',
            id: 'mainModules',
            cls: "backimg"

        }, {
            xtype: 'toolbar',
            id: 'mainBotTb',
            name: 'mainBotTb',
            docked: 'bottom',
            layout: {
                pack: 'middle'
            },
            minHeight: '10px'
        }, {
            title: mobEdu.main.f.getTitle(),
            xtype: 'toolbar',
            name: 'mainTopTb',
            id: 'mainTopTb',
            docked: 'top',
            ui: 'light',
            // html: '<div class="x-center"><div class="logocircle"><div class="logoinnerhome"></div></div></div>',
            minHeight: '50px'
        }]
    }
});