Ext.define('mobEdu.main.view.signUp', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.main.store.signUp'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
            docked: 'top',
            xtype: 'toolbar',
            cls: 'beveltoptoolbar',
            id: 'signUp',
            title: mobEdu.main.f.getTitle(),
            items: [{
                xtype: 'panel',
                pack: 'center',
                html: '<div class="x-center custcenter"><div class="divpositioncss"><div class="logoinnerhome"></div><div class="logocircle"></div></div></div>',
                flex: 1
            }]
        }, {
            xtype: 'panel',
            padding: '0 0 0 .7em',
            html: '<P class="cntustxt">Thank you for downloading N2N Mobile app. Please sign-up to continue.</P>',
            style: 'font-size: 30px;line-height:1.3em;color: #717073;text-align:center'
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                label: 'Name ',
                labelWrap: true,
                labelWidth: Ext.os.is.Phone ? '45%' : '30%',
                id: 'signupName',
                required: true,
                useClearIcon: true,
                name: 'Name',
                placeHolder: 'Enter your name'
            }, {
                xtype: 'textfield',
                label: 'Organization ',
                id: 'signupOrgs',
                labelWidth: Ext.os.is.Phone ? '45%' : '30%',
                labelWrap: true,
                required: true,
                useClearIcon: true,
                name: 'Organisation ',
                placeHolder: 'Enter your Organization'
            }, {
                xtype: 'emailfield',
                label: 'Email ',
                name: 'email',
                labelWidth: Ext.os.is.Phone ? '45%' : '30%',
                id: 'signupEmail',
                placeHolder: 'E-mail',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });
                    me.callParent(arguments);
                }
            }, {
                xtype: 'textfield',
                label: 'Phone number',
                useClearIcon: true,
                labelWidth: Ext.os.is.Phone ? '45%' : '30%',
                labelWrap: true,
                id: 'signupNumber',
                name: 'phone number',
                placeHolder: 'Phone number'
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Sign Up',
                align: 'right',
                handler: function() {
                    mobEdu.main.f.submitSignUpForm();
                }
            }]
        }]
    }
});