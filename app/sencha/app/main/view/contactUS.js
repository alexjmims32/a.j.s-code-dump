Ext.define('mobEdu.main.view.contactUS', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.main.store.recipientsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',        
        items: [{
            xtype: 'customToolbar',
            id: 'cntUsTitle'
        }, {
             layout: 'vbox',
               items: [{
            xtype: 'panel',
            padding: '40px 0px 40px 0px',
            html:'<P class="cntustxt">For more information or to request a demo,<br/>please contact us. We look forward to hearing from you.</P>',            
            //html: ['<table><tr><td><h3>' + 'We welcome all comments. Please complete the fields below.' + '</h3></td></tr></table><br />']
        }, {
            xtype:'panel',
            layout:'hbox',
            cls:'cusimages',
             items: [{
                html:'<table align="center"><tr><td><img src="' + mobEdu.util.getResourcePath() + 'images/menu/mail.png" class="cusimg"></td></tr><tr><td class="cUS">+1 (888) 651-3309</td></tr><tr>&nbsp</tr><tr><td class="cUS">Phone</td></tr>',
                flex:1,
                style:'text-align:center'
              },
             {
                html:'<table align="center"><tr><td><img src="' + mobEdu.util.getResourcePath() + 'images/menu/mail.png" class="cusimg"></td></tr><tr><td class="cUS">info@n2nservices.com</td></tr><tr>&nbsp</tr><tr><td class="cUS">email</td></tr>',
                flex: 1,
                style:'text-align:center'
             }]
        },{
            xtype:'panel',
            items: [{
            xtype: 'panel',
            padding: '40px 0px 40px 0px',
            html:'<P>REACH OUT</P>',
            style:'font-size: 30px;line-height:1.3em;color: #717073;text-align:center'
        }
        ,{
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                placeHolder: 'Full Name',
                name: 'fullName',
                id: 'cntFullName',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'emailfield',
                name: 'email',
                id: 'cntEmail',
                placeHolder:'E-mail',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });
                    me.callParent(arguments);
                }
            }, {
                xtype: 'textareafield',
                placeHolder:'Comments',
                maxRows: 4,
                name: 'comments',
                id: 'cntComments',
                required: true,
                useClearIcon: true
            }]
        },{
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'center'
            },
            items: [{
                text: 'Submit',
                align: 'right',
                handler: function() {
                    mobEdu.main.f.loadCntFeedback();
                }
            }]
        }]
    }]
        }]
    }
});