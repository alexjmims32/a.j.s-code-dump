Ext.define('mobEdu.main.view.smenu', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.main.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'card',
        flex: 1,
        items: [{
            xtype:'panel',
            layout:'vbox',
            // scrollable:false,
            id:'category',
            name:'category',
            scrollable: {
                direction: 'vertical'
            },
            flex:3
        }, {
            xtype: 'toolbar',
            id: 'mainBotTb',
            name: 'mainBotTb',
            docked: 'bottom',
            ui: 'flat',
            layout: {
                pack: 'middle'
            },
            minHeight: '0px'
        }, {
            title: mobEdu.main.f.getTitle(),
            xtype: 'toolbar',
            id: 'mainTopTb',
            name: 'mainTopTb',
            docked: 'top',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            cls: 'beveltoptoolbar',
            items: [{
                xtype: 'button',
                id: 'NOTIF',
                align: 'left',
                ui: 'normal',
                text: '<div class="notif"></div>',
                cls: 'notifbutton',
                handler: function() {
                    var record = mobEdu.util.getModuleByCode('NOTIF');
                    mobEdu.util.doLoginCheck(record.get('authRequired'), record.get('action'), record.get('code'));
                }
            }, {
                xtype: 'panel',
                pack: 'center',
                html: '<div class="x-center"><div class="logocircle"><div class="logoinnerhome"></div></div></div>',
                flex: 1
            }, {
                xtype: 'img',
                cls: 'login',
                id: 'logBtn',
                align: 'left',
                listeners: {
                    tap: function() {
                        mobEdu.main.f.logout();
                    }
                }
            }]
        }]
    }
});
