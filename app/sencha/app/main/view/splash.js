Ext.define('mobEdu.main.view.splash', {
    extend:'Ext.Panel',
    config:{
        fullscreen:true,
        layout: 'hbox',
        cls:'splash',
        items:[{
            xtype: 'image',
            src: mobEdu.util.getResourcePath() + 'images/clientLogo.png',
            flex: 1
        }]
    }
});
