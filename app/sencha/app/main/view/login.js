Ext.define('mobEdu.main.view.login', {
    extend: 'Ext.form.Panel',
    statics: {
        tablet: {
            landscape: {
                width: '30%',
                height: '35%'
            },
            portrait: {
                width: '40%',
                height: '20%'
            }
        },
        phone: {
            landscape: {
                width: '40%',
                height: '85%'
            },
            portrait: {
                width: '60%',
                height: '45%'
            }
        }
    },
    config: {
        id: 'loginPopup',
        scrollable: false,
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '50%',
        height: '50%',
        style: 'background-color: transparent;',
        layout: {
            type: 'vbox',
            pack: 'middle'
        },
        padding: '5 5 0 5',
        items: [{
            xtype: 'textfield',
            name: 'name',
            id: 'id',
            placeHolder: loginLabel != '' ? loginLabel : "Username",
            clearIcon: true,
            autoCapitalize: false,
            required: true,
            margin: 5,
            hidden: loginType === 'demo' ? true : false,
            listeners: {
                keyup: function(view, e, eOpts) {
                    mobEdu.main.f.onUsernameKeyup(view, e, eOpts);
                },
                blur: function(textfield, e, eOpts) {
                    mobEdu.util.hideKeyboard();
                }
            }
        }, {
            xtype: 'passwordfield',
            name: 'password',
            id: 'password',
            placeHolder: passwordLabel != '' ? passwordLabel : "Password",
            required: true,
            clearIcon: true,
            margin: 5,
            hidden: loginType === 'demo' ? true : false,
            listeners: {
                keyup: function(view, e, eOpts) {
                    mobEdu.main.f.onPasswordKeyup(view, e, eOpts);
                },
                blur: function(textfield, e, eOpts) {
                    mobEdu.util.hideKeyboard();
                }
            }
        }, {
            xtype: 'label',
            width: '100%',
            html: 'Select your role',
            name: 'name',
            id: 'id',
            margin: '0 0 25 0',
            hidden: loginType === 'demo' ? false : true
        }, {
            xtype: 'selectfield',
            name: 'role',
            id: 'role',
            label: '',
            margin: '0 0 25 0',
            hidden: loginType === 'demo' ? false : true,
            options: [{
                text: 'Student',
                value: 'STUDENT'
            }, {
                text: 'Faculty',
                value: 'FACULTY'
            }, {
                text: 'Student/Faculty',
                value: 'STUDENTFACULTY'
            }, {
                text: 'Supervisor',
                value: 'SUPERVISOR'
            }, {
                text: 'Recruiter',
                value: 'RECRUITER'
            }, {
                text: 'Advisor',
                value: 'ADVISOR'
            }, {
                text: 'Counselor',
                value: 'COUNSELLOR'
            }, ]
        }, {
            margin: 5,
            xtype: 'button',
            text: '<h7>Login</h7>',
            ui: 'action',
            cls: 'loginBtn',
            handler: function() {
                mobEdu.main.f.submitLoginForm();
            }
        }],
        flex: 1
    },

    initialize: function() {
        // Add a Listener. Listen for [Viewport ~ Orientation] Change.
        Ext.Viewport.on('orientationchange', 'handleOrientationChange', this);
        this.callParent(arguments);
    },

    handleOrientationChange: function(viewport, orientation, width, height) {
        // Execute the code that needs to fire on Orientation Change.
        mobEdu.util.onOrientationChange(orientation, this, mobEdu.main.view.login);
    }

});