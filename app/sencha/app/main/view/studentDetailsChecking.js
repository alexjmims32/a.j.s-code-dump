Ext.define('mobEdu.main.view.studentDetailsChecking',{
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.main.f',
        'mobEdu.main.store.studentInformation'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Student Profile</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Submit',
                        align:'right',
                        handler:function () {
                            mobEdu.main.f.loadStudentSubmitDetails();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype: 'checkboxfield',
                        labelWidth: '70%',
                        label:'You have reviewed the Student Handbook',
                        name:'stuHandBook',
                        id:'stuHandBook',
//                        checked: true,
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'checkboxfield',
                        labelWidth: '70%',
                        label:'You have provided the University with your local address',
                        name:'localAddress',
                        id:'localAddress',
                        checked: true,
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'checkboxfield',
                        labelWidth: '70%',
                        label:'You have provided the University with your emergency contact information',
                        name:'emergencyInfo',
                        id:'emergencyInfo',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'emailfield',
                        labelWidth:'100%',
                        labelAlign:'top',
                        label:'Verify email address',
                        name:'verifyEmail',
                        id:'verifyEmail',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                            });

                            me.callParent(arguments);
                        }
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'top',
                        labelWidth:'100%',
                        label:'Verify mobile phone number',
                        name:'verifyPhone',
                        id:'verifyPhone',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                mobEdu.main.f.onPhoneKeyup(textfield);
                            }
                        },
                        maxLength:13
                    }
                ]
            },
            {
                xtype: 'fieldset',
                title: 'Verify local address',
                items: [
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Address1',
                        value:'3063 Peachtree',
                        name:'verifyLAddress',
                        id:'verifyLAddress',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        value:'Industrial Blvd',
                        label:'Address2',
                        name:'verifyLAdd2',
                        id:'verifyLAdd2',
//                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        name:'verifyLCity',
                        id:'verifyLCity',
                        required:true,
                        useClearIcon:true,
                        label:'City',
                        value:'Duluth'
                    },
                    {
                        xtype:'selectfield',
                        labelWidth:'50%',
                        name:'verifyLState',
                        id:'verifyLState',
                        useClearIcon:true,
                        required:true,
                        label:'State',
                        value:'Georgia(GA)',
                        options:[{
                                text:'Select',
                                value:'select'
                            },{
                                text:'Alabama(AL)',
                                value:'AL'
                            },{
                                text:'Alaska(AK)',
                                value:'AK'
                            },{
                                text:'Alberta(AB)',
                                value:'AB'
                            },{
                                text:'American Samoa(AS)',
                                value:'AS'
                            },{
                                text:'Arizona(AZ)',
                                value:'AZ'
                            },{
                                text:'Arkansas(AR)',
                                value:'AR'
                            },{
                                text:'British Columbia(BC)',
                                value:'BC'
                            },{
                                text:'California(CA)',
                                value:'CA'
                            },{
                                text:'Colorado(CO)',
                                value:'CO'
                            },{
                                text:'Connecticut(CT)',
                                value:'CT'
                            },{
                                text:'Delaware(DE)',
                                value:'DE'
                            },{
                                text:'District of Columbia(DC)',
                                value:'DC'
                            },{
                                text:'Eastern Province(EP)',
                                value:'EP'
                            },{
                                text:'Federated States of Micronesia(FM)',
                                value:'FM'
                            },{
                                text:'Florida(FL)',
                                value:'FL'
                            },{
                                text:'Georgia(GA)',
                                value:'GA'
                            },{
                                text:'Guam(GU)',
                                value:'GU'
                            },{
                                text:'Hawaii(HI)',
                                value:'HI'
                            },{
                                text:'Idaho(ID)',
                                value:'ID'
                            },{
                                text:'Illinois(IL)',
                                value:'IL'
                            },{
                                text:'Indiana(IN)',
                                value:'IN'
                            },{
                                text:'Iowa(IA)',
                                value:'IA'
                            },{
                                text:'Kansas(KS)',
                                value:'KS'
                            },{
                                text:'Kentucky(KY)',
                                value:'KY'
                            },{
                                text:'Louisiana(LA)',
                                value:'LA'
                            },{
                                text:'Maine(ME)',
                                value:'ME'
                            },{
                                text:'Manitoba(MB)',
                                value:'MB'
                            },{
                                text:'Marshall Islands(MH)',
                                value:'MH'
                            },{
                                text:'Maryland(MD)',
                                value:'MD'
                            },{
                                text:'Massachusetts(MA)',
                                value:'MA'
                            },{
                                text:'Michigan(MI)',
                                value:'MI'
                            },{
                                text:'Minnesota(MN)',
                                value:'MN'
                            },{
                                text:'Mississippi(MS)',
                                value:'MS'
                            },{
                                text:'Missouri(MO)',
                                value:'MO'
                            },{
                                text:'Montana(MT)',
                                value:'MT'
                            },{
                                text:'Nebraska(NE)',
                                value:'NE'
                            },{
                                text:'Nevada(NV)',
                                value:'NV'
                            },{
                                text:'New Brunswick(NB)',
                                value:'NB'
                            },
                            {
                                text:'New Hampshire(NH)',
                                value:'NH'
                            },
                            {
                                text:'New Jersey(NJ)',
                                value:'NJ'
                            },
                            {
                                text:'New Mexico(NM)',
                                value:'NM'
                            },
                            {
                                text:'New York(NY)',
                                value:'NY'
                            },
                            {
                                text:'Newfoundland and Labrador(NL)',
                                value:'Nl'
                            },
                            {
                                text:'North Carolina(NC)',
                                value:'NC'
                            },
                            {
                                text:'North Dakota(ND)',
                                value:'ND'
                            },
                            {
                                text:'Northern Mariana Islands(CM)',
                                value:'CM'
                            },
                            {
                                text:'Northwest Territories(NT)',
                                value:'NT'
                            },
                            {
                                text:'Nova Scotia(NS)',
                                value:'NS'
                            },
                            {
                                text:'Nunavut(NU)',
                                value:'NU'
                            },
                            {
                                text:'Ohio(OH)',
                                value:'OH'
                            },
                            {
                                text:'Oklahoma(OK)',
                                value:'OK'
                            },
                            {
                                text:'Ontario(ON)',
                                value:'ON'
                            },
                            {
                                text:'Oregon(OR)',
                                value:'OR'
                            },
                            {
                                text:'Palau(PW)',
                                value:'PW'
                            },
                            {
                                text:'Pennsylvania(PA)',
                                value:'PA'
                            },
                            {
                                text:'Prince Edward Island(PE)',
                                value:'PE'
                            },
                            {
                                text:'Puerto Rico(PR)',
                                value:'PR'
                            },
                            {
                                text:'Quebec(QC)',
                                value:'QC'
                            },
                            {
                                text:'Rhode Island(RI)',
                                value:'RI'
                            },
                            {
                                text:'Saskatchewan(SK)',
                                value:'SK'
                            },
                            {
                                text:'Saskatchewan(SK)',
                                value:'SK'
                            },
                            {
                                text:'South Carolina(SC)',
                                value:'SC'
                            },
                            {
                                text:'South Dakota(SD)',
                                value:'SD'
                            },
                            {
                                text:'Tennessee(TN)',
                                value:'TN'
                            },
                            {
                                text:'Texas(TX)',
                                value:'TX'
                            },
                            {
                                text:'Utah(UT)',
                                value:'UT'
                            },
                            {
                                text:'Vermont(VT)',
                                value:'VT'
                            },
                            {
                                text:'Virgin Islands(VI)',
                                value:'VI'
                            },
                            {
                                text:'Virginia(VA)',
                                value:'VA'
                            },
                            {
                                text:'Washington(WA)',
                                value:'WA'
                            },
                            {
                                text:'West Virginia(WV)',
                                value:'WV'
                            },
                            {
                                text:'Wisconsin(WI)',
                                value:'WI'
                            },
                            {
                                text:'Wyoming(WY)',
                                value:'WY'
                            },
                            {
                                text:'Yukon(YU)',
                                value:'YU'
                            }
                        ]
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Zip',
                        name:'verifyLZip',
                        id:'verifyLZip',
                        useClearIcon:true,
                        required:true,
                        value:'30097',
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                return mobEdu.main.f.onZipKeyup(textfield);
                            }
                        },
                        maxLength:5
                    }
                ]
            },{
                xtype: 'fieldset',
                title: 'Verify home address',
                items: [
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Address1',
                        name:'verifyHomeAdd',
                        id:'verifyHomeAdd',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Address2',
                        name:'verifyHomeAdd2',
                        id:'verifyHomeAdd2',
//                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        name:'verifyHomeCity',
                        id:'verifyHomeCity',
                        useClearIcon:true,
                        required:true,
                        label:'City'
                    },
                    {
                        xtype:'selectfield',
                        labelWidth:'50%',
                        name:'verifyHomeState',
                        id:'verifyHomeState',
                        useClearIcon:true,
                        required:true,
                        label:'State',
                        options:[{
                            text:'Select',
                            value:'select'
                        },{
                            text:'Alabama(AL)',
                            value:'AL'
                        },{
                            text:'Alaska(AK)',
                            value:'AK'
                        },{
                            text:'Alberta(AB)',
                            value:'AB'
                        },{
                            text:'American Samoa(AS)',
                            value:'AS'
                        },{
                            text:'Arizona(AZ)',
                            value:'AZ'
                        },{
                            text:'Arkansas(AR)',
                            value:'AR'
                        },{
                            text:'British Columbia(BC)',
                            value:'BC'
                        },{
                            text:'California(CA)',
                            value:'CA'
                        },{
                            text:'Colorado(CO)',
                            value:'CO'
                        },{
                            text:'Connecticut(CT)',
                            value:'CT'
                        },{
                            text:'Delaware(DE)',
                            value:'DE'
                        },{
                            text:'District of Columbia(DC)',
                            value:'DC'
                        },{
                            text:'Eastern Province(EP)',
                            value:'EP'
                        },{
                            text:'Federated States of Micronesia(FM)',
                            value:'FM'
                        },{
                            text:'Florida(FL)',
                            value:'FL'
                        },{
                            text:'Georgia(GA)',
                            value:'GA'
                        },{
                            text:'Guam(GU)',
                            value:'GU'
                        },{
                            text:'Hawaii(HI)',
                            value:'HI'
                        },{
                            text:'Idaho(ID)',
                            value:'ID'
                        },{
                            text:'Illinois(IL)',
                            value:'IL'
                        },{
                            text:'Indiana(IN)',
                            value:'IN'
                        },{
                            text:'Iowa(IA)',
                            value:'IA'
                        },{
                            text:'Kansas(KS)',
                            value:'KS'
                        },{
                            text:'Kentucky(KY)',
                            value:'KY'
                        },{
                            text:'Louisiana(LA)',
                            value:'LA'
                        },{
                            text:'Maine(ME)',
                            value:'ME'
                        },{
                            text:'Manitoba(MB)',
                            value:'MB'
                        },{
                            text:'Marshall Islands(MH)',
                            value:'MH'
                        },{
                            text:'Maryland(MD)',
                            value:'MD'
                        },{
                            text:'Massachusetts(MA)',
                            value:'MA'
                        },{
                            text:'Michigan(MI)',
                            value:'MI'
                        },{
                            text:'Minnesota(MN)',
                            value:'MN'
                        },{
                            text:'Mississippi(MS)',
                            value:'MS'
                        },{
                            text:'Missouri(MO)',
                            value:'MO'
                        },{
                            text:'Montana(MT)',
                            value:'MT'
                        },{
                            text:'Nebraska(NE)',
                            value:'NE'
                        },{
                            text:'Nevada(NV)',
                            value:'NV'
                        },{
                            text:'New Brunswick(NB)',
                            value:'NB'
                        },
                            {
                                text:'New Hampshire(NH)',
                                value:'NH'
                            },
                            {
                                text:'New Jersey(NJ)',
                                value:'NJ'
                            },
                            {
                                text:'New Mexico(NM)',
                                value:'NM'
                            },
                            {
                                text:'New York(NY)',
                                value:'NY'
                            },
                            {
                                text:'Newfoundland and Labrador(NL)',
                                value:'Nl'
                            },
                            {
                                text:'North Carolina(NC)',
                                value:'NC'
                            },
                            {
                                text:'North Dakota(ND)',
                                value:'ND'
                            },
                            {
                                text:'Northern Mariana Islands(CM)',
                                value:'CM'
                            },
                            {
                                text:'Northwest Territories(NT)',
                                value:'NT'
                            },
                            {
                                text:'Nova Scotia(NS)',
                                value:'NS'
                            },
                            {
                                text:'Nunavut(NU)',
                                value:'NU'
                            },
                            {
                                text:'Ohio(OH)',
                                value:'OH'
                            },
                            {
                                text:'Oklahoma(OK)',
                                value:'OK'
                            },
                            {
                                text:'Ontario(ON)',
                                value:'ON'
                            },
                            {
                                text:'Oregon(OR)',
                                value:'OR'
                            },
                            {
                                text:'Palau(PW)',
                                value:'PW'
                            },
                            {
                                text:'Pennsylvania(PA)',
                                value:'PA'
                            },
                            {
                                text:'Prince Edward Island(PE)',
                                value:'PE'
                            },
                            {
                                text:'Puerto Rico(PR)',
                                value:'PR'
                            },
                            {
                                text:'Quebec(QC)',
                                value:'QC'
                            },
                            {
                                text:'Rhode Island(RI)',
                                value:'RI'
                            },
                            {
                                text:'Saskatchewan(SK)',
                                value:'SK'
                            },
                            {
                                text:'Saskatchewan(SK)',
                                value:'SK'
                            },
                            {
                                text:'South Carolina(SC)',
                                value:'SC'
                            },
                            {
                                text:'South Dakota(SD)',
                                value:'SD'
                            },
                            {
                                text:'Tennessee(TN)',
                                value:'TN'
                            },
                            {
                                text:'Texas(TX)',
                                value:'TX'
                            },
                            {
                                text:'Utah(UT)',
                                value:'UT'
                            },
                            {
                                text:'Vermont(VT)',
                                value:'VT'
                            },
                            {
                                text:'Virgin Islands(VI)',
                                value:'VI'
                            },
                            {
                                text:'Virginia(VA)',
                                value:'VA'
                            },
                            {
                                text:'Washington(WA)',
                                value:'WA'
                            },
                            {
                                text:'West Virginia(WV)',
                                value:'WV'
                            },
                            {
                                text:'Wisconsin(WI)',
                                value:'WI'
                            },
                            {
                                text:'Wyoming(WY)',
                                value:'WY'
                            },
                            {
                                text:'Yukon(YU)',
                                value:'YU'
                            }
                        ]
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Zip',
                        name:'verifyHomeZip',
                        id:'verifyHomeZip',
                        useClearIcon:true,
                        required:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                return mobEdu.main.f.onZipKeyup(textfield);
                            }
                        },
                        maxLength:5
                    }
                    ]
            },
            {
                xtype: 'fieldset',
                title: 'Verify emergency<br/>contact information',
                items: [
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Name',
                        name:'vEmrName',
                        id:'vEmrName',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Relationship',
                        name:'vEmrRel',
                        id:'vEmrRel',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'emailfield',
                        labelWidth:'50%',
                        label:'Email',
                        name:'verifyEmrEmail',
                        id:'verifyEmrEmail',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                            });

                            me.callParent(arguments);
                        }
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Mobile',
                        name:'verifyEmrM',
                        id:'verifyEmrM',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                mobEdu.main.f.onPhoneKeyup(textfield);
                            }
                        },
                        maxLength:13
                    }
                    ]
            }
        ]
    }
});
