Ext.define('mobEdu.main.view.dashboard', {
    extend: 'Ext.Panel',
    config: {
        fullscreen: true,
        styleHtmlContent: true,
        layout: 'fit',
        items: [{
                html: '',
                cls: 'dashboardBackground',
                height: '100%',
                width: '100%'
            }, {
                title: mobEdu.main.f.getTitle(),
                xtype: 'toolbar',
                id: 'mainTopTb',
                name: 'mainTopTb',
                docked: 'top',
                ui: 'light',
                minHeight: '40px'
            }
        ]
    }
});