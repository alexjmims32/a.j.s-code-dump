Ext.define('mobEdu.main.view.modules', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'editModulesList',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' +
                '<td width="5%" align="left" >' +
                '<tpl if="mobEdu.main.f.isFavourite(select)===true">',
                '<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" height=24 name="on" />' +
                '<tpl else>',
                '<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" height=24 name="off" />',
                '</tpl>' +
                '</td>' +
                '<td width="90%" align="left" ><h3 id="modulename">{description}</h3></td>' +
                //                '<td width="2%" align="left" ><img width="16px" height="16px" src="' + mobEdu.util.getResourcePath() + 'images/edit.png" name="edit" /></td>'+
                // '<td width="2%" align="left" ><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/up.png" name="up" /></td>'+
                // '<td width="1%">&nbsp</td>'+
                // '<td width="2%" align="left" ><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/down.png" name="down" /></td>'+
                '</tr></table>'),
            store: mobEdu.util.getStore('mobEdu.main.store.modulesLocal'),
            disableSelection: true,
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.main.f.onModuleSelection(view, index, target, record, item, e, eOpts);
                }
            }
        },{            
            id: 'editModulesTitle',
            title: '<h1>Modules</h1>',
            xtype: 'customToolbar'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Save',
                handler: function() {
                    mobEdu.main.f.updateFavourites();
                }
            }]
        }
        ],
        flex: 1
    }
});