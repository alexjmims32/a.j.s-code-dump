Ext.define('mobEdu.main.view.studentFeedback', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.main.store.recipientsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'feedbackTitle'
        }, {
            xtype: 'panel',
            padding: '0 0 0 .7em',
            html: ['<table><tr><td><h3>' + 'We welcome all comments. Please complete the fields below.' + '</h3></td></tr></table><br />']
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'selectfield',
                label: 'Recipients',
                labelWidth: '50%',
                name: 'recipients',
                id: 'recipients',
                required: true,
                useClearIcon: true,
                displayField: 'title',
                valueField: 'email',
                store: mobEdu.util.getStore('mobEdu.main.store.recipientsList')
            }, {
                xtype: 'textfield',
                label: 'Full Name',
                labelWidth: '50%',
                name: 'fullName',
                id: 'fullName',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'emailfield',
                name: 'email',
                id: 'email',
                labelWidth: '50%',
                label: 'E-mail',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');
                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });
                    me.callParent(arguments);
                }
            }, {
                xtype: 'textareafield',
                label: 'Comments',
                labelAlign: 'top',
                labelWidth: '50%',
                maxRows: 4,
                name: 'comments',
                id: 'comments',
                required: true,
                useClearIcon: true
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Send',
                align: 'right',
                handler: function() {
                    mobEdu.main.f.loadStudentFeedback();
                }
            }]
        }]
    }
});