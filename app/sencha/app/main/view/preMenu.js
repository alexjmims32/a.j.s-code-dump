Ext.define('mobEdu.main.view.preMenu', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.main.f'
	],
	config: {
		scrollable: 'vertical',
		fullscreen: true,
		layout: 'vbox',
		flex: 1,
		items: [{
			html: '',
			cls: "backimg",
			id: 'preModules'
		}, {
			xtype: 'toolbar',
			id: 'mainBotTb',
			name: 'mainBotTb',
			docked: 'bottom',
			ui: 'flat',
			layout: {
				pack: 'middle'
			},
			minHeight: '0px'
		}, {
			// title: '<img src="' + mobEdu.util.getResourcePath() + 'images/wentworth-full.png" width="260px" />',
			xtype: 'toolbar',
			id: 'mainTopPreTb',
			name: 'mainTopPreTb',
			docked: 'top',
			layout: {
				type: 'hbox',
				pack: 'center'
			},
			cls: 'beveltoptoolbar',
			items: [{
				xtype: 'button',
				id: 'NOTIF',
				align: 'left',
				ui: 'normal',
				text: '<div class="notif"></div>',
				cls: 'notifbutton',
				handler: function() {
					mobEdu.util.doLoginCheck(true, 'mobEdu.notif.f.loadNotifPopup', 'NOTIF', null, null, null);
					// if(mobEdu.util.getStudentId() == null && mobEdu.util.getAuthString() == null){
					//                   mobEdu.main.f.showLogin();
					//               	}else{
					//                   mobEdu.notif.f.loadNotifPopup();
					//               	}

				}
			}, {
				xtype: 'panel',
				pack: 'center',
				html: '<div class="x-center precustcenter"><div class="predivpositioncss"><div class="logoinnerhome"></div><div class="logocircle"></div></div></div>',
				flex: 1
			}, {
				xtype: 'img',
				// cls: 'login',
				id: 'logBtn',
				align: 'left',
				listeners: {
					tap: function() {
						mobEdu.main.f.doAction();
						// mobEdu.main.f.logout();
					}
				}
			}]
		}]
	}
});