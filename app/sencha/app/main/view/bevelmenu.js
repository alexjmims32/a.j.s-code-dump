Ext.define('mobEdu.main.view.bevelmenu', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.main.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'card',
		flex: 1,
		items: [{
			xtype: 'carousel',
			cls: 'couroselbevel',
			fullscreen: true,
			defaults: {
				styleHtmlContent: true
			},
			id: 'mainModules',
			paintedHeight: 0,
			paintedWidth: 0,
			listeners: {
				'resize': function(e, opts) {
					this.paintedHeight = e.dom.scrollHeight;
					this.paintedWidth = e.dom.scrollWidth;
				}
			}
		}, {
			xtype: 'toolbar',
			id: 'mainBotTb',
			name: 'mainBotTb',
			docked: 'bottom',
			ui: 'flat',
			layout: {
				pack: 'middle'
			},
			minHeight: '0px'
		}, {
            xtype:'customToolbar',
            title:'<h1>WIT Mobile</h1>',
            id:'bevelTitle'
        
		}]
	}
});