Ext.define('mobEdu.main.view.campus', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: {
            type: 'vbox',
            pack: 'center',
            align: 'center'
        },
        cls: 'logo',
        defaults: {
            style: 'background: transparent'
        },
        items: [
        {
            xtype:'spacer',
            flex:1
        },
        {
            html: '<img src="' + mobEdu.util.getResourcePath() + 'images/clientLogo.png">',
            margin:10
        },
        {
//            height: 100,
            items: [
            {
                xtype: 'selectfield',
                name: 'campusCode',
                margin: '1',
                id: 'campusCode',
                placeHolder: 'Select a School',
                //						options: [
                //							{
                //								text: 'Select a School',
                //								value: '0'
                //							}, {
                //								text: 'Fairmont',
                //								value: 'FM'
                //							}, {
                //								text: 'Georgetown',
                //								value: 'GT'
                //							}, {
                //								text: 'Grant Line',
                //								value: 'GL'
                //							}, {
                //								text: 'Green Valley',
                //								value: 'GVA'
                //							}, {
                //								text: 'Greenville',
                //								value: 'GVI'
                //							}, {
                //								text: 'Floyds Knobs',
                //								value: 'FK'
                //							}, {
                //								text: 'Mount Tabor',
                //								value: 'MT'
                //							}, {
                //								text: 'Slate Run',
                //								value: 'SR'
                //							}, {
                //								text: 'S. Ellen Jones',
                //								value: 'SE'
                //							}, {
                //								text: 'Childrens Academy',
                //								value: 'CA'
                //							}, {
                //								text: 'New Albany High School',
                //								value: 'NAHS'
                //							}, {
                //								text: 'Floyd Central High School',
                //								value: 'FCHS'
                //							}, {
                //								text: 'Hazelwood',
                //								value: 'HW'
                //							}, {
                //								text: 'Highland Hills',
                //								value: 'HH'
                //							}, {
                //								text: 'Scribner',
                //								value: 'SC'
                //							}, {
                //								text: 'Prosser',
                //								value: 'PR'
                //							}
                //						]
                options:[
                {
                    text:'Select a School',
                    value:'0'
                },{
                    text:'Abraham Baldwin Agricultural College',
                    value:'ABAC'
                },{
                    text:'Albany State University',
                    value:'ASU'
                },{
                    text:'Armstrong Atlantic State University',
                    value:'AASU'
                },{
                    text:'Atlanta Metropolitan State College',
                    value:'AMSC'
                },{
                    text:'Bainbridge State College',
                    value:'BSU'
                },{
                    text:'Clayton State University',
                    value:'CTSU'
                },{
                    text:'College of Coastal Georgia',
                    value:'CCG'
                },{
                    text:'Columbus State University',
                    value:'CBSU'
                },{
                    text:'Dalton State College',
                    value:'DLSC'
                },{
                    text:'Darton State College',
                    value:'DRSC'
                },{
                    text:'East Georgia State College',
                    value:'EGSC'
                },{
                    text:'Fort Valley State University',
                    value:'FVSC'
                },{
                    text:'Georgia College & State University',
                    value:'GCSU'
                },{
                    text:'Georgia Gwinnett College',
                    value:'GGC'
                },{
                    text:'Georgia Highlands College',
                    value:'GHC'
                },{
                    text:'Georgia Institute of Technology',
                    value:'GIT'
                },{
                    text:'Georgia Perimeter College',
                    value:'GPC'
                },{
                    text:'Georgia Public Library Service',
                    value:'GPLC'
                },{
                    text:'Georgia Regents University',
                    value:'GRU'
                },{
                    text:'Georgia Southern University',
                    value:'GSOU'
                },{
                    text:'Georgia Southwestern State University',
                    value:'GSSU'
                },{
                    text:'Georgia State University',
                    value:'GSU'
                },{
                    text:'Gordon State College',
                    value:'GDSU'
                },{
                    text:'Kennesaw State University',
                    value:'KSU'
                },{
                    text:'Middle Georgia State College',
                    value:'MGSC'
                },{
                    text:'Savannah State University',
                    value:'SSU'
                },{
                    text:'Skidaway Institute of Oceanography',
                    value:'SIO'
                },{
                    text:'South Georgia State College',
                    value:'SGSC'
                },{
                    text:'Southern Polytechnic State University',
                    value:'SPSU'
                },{
                    text:'University of Georgia',
                    value:'UG'
                },{
                    text:'University of North Georgia',
                    value:'UNG'
                },{
                    text:'University of West Georgia',
                    value:'UWG'
                },{
                    text:'Valdosta State University',
                    value:'VSU'
                }
                ]
            }]
        },
        {
            layout: 'hbox',
            items: [{
                xtype: 'button',
                text: '<h3>&nbsp;&nbsp;Go&nbsp;&nbsp;</h3>',
//                ui: 'action',
                margin: 10,
                handler: function() {
                    mobEdu.main.f.updateCampusCode();
                }
            }
            ]
        },
        {
            xtype:'spacer',
            flex:1
        }
        ]
    }
});
