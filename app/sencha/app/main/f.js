Ext.define('mobEdu.main.f', {
    requires: [
        'mobEdu.main.store.modules',
        'mobEdu.main.store.modulesLocal',
        'mobEdu.main.store.slidingMenu',
        'mobEdu.main.store.login',
        'mobEdu.feeds.store.feedsMenus',
        'mobEdu.main.store.encore',
        'mobEdu.main.store.term',
        'mobEdu.main.store.config',
        'mobEdu.main.store.entourage',
        'mobEdu.main.store.feedback',
        'mobEdu.main.store.menu',
        'mobEdu.main.store.proxySession',
        'mobEdu.main.store.recipientsList',
        'mobEdu.main.store.termList',
        'mobEdu.main.store.login',
        'mobEdu.main.store.signUp',
        'mobEdu.main.store.signUpLocal',
        'mobEdu.main.store.preModules',
        'mobEdu.main.store.addressChecking',
        'mobEdu.main.store.favourites',
        'mobEdu.main.store.studentInformation'
    ],
    statics: {
        prevView: null,
        calendarInstance: null,
        immunizationNextView: null,
        androidSreenWidth: null,
        signUpStatus: false,
        category: null,
        categoryDesc: null,
        subMenuCategory: false,
        iconWidthValue: null,
        initializeMainView: function() {

            if (mobEdu.util.getStudentId() != null && mobEdu.util.getAuthString() != null) {
                if (showPremenu == 'always') {
                    mobEdu.main.f.displayPreMenu();
                } else {
                    mobEdu.main.f.displayModules();
                }
                mobEdu.main.f.showSlidingMenu();
                if (showLoginName === 'true') {
                    mobEdu.util.get('mobEdu.main.view.bevelmenu');
                    Ext.getCmp('loginName').setHtml('<div class="loginTextWrapper">' + mobEdu.util.getFirstName() + '</div>');
                }
                // Need to recreate the sliding menu when the app is refreshed or reopened
                // with user session still active
                mobEdu.main.f.updateSlidingMenu(mobEdu.util.getStore('mobEdu.main.store.modulesLocal'));
                if (mobEdu.main.f.isNotificationEnabled()) {
                    //Load notifications for FirstTime
                    mobEdu.notif.f.loadNotifications();
                }
                mobEdu.main.f.getFeedsInfo();
            } else {
                // Main menu is not loaded when reopening the app on android for NAFCS
                // They have primary module set to campus.
                // Hence uncommenting this check
                // Will have to check the behavour when main module = login
                // if(primaryModule == 'main') {
                mobEdu.main.f.getMainModules();
                if (mainMenuLayout == 'SGRID')
                    mobEdu.main.f.displayPreMenu();
                // }
            }
        },

        getMainModules: function(isUpdate) {
            var code = mobEdu.util.getSchoolCode();
            if (code == null || code == '') {
                code = campusCode
            }
            var showByDefault = 'Y';
            var store = mobEdu.util.getStore('mobEdu.main.store.modules');
            store.getProxy().setHeaders({
                companyID: companyId
            });
            store.getProxy().setUrl(encorewebserver + 'open/modules');
            store.getProxy().setExtraParams({
                showByDefault: showByDefault,
                clientCode: campusCode
            });
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.mainModulesResponseHandler(isUpdate);
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'mainmodules');
        },
        mainModulesResponseHandler: function(isUpdate) {
            var store = mobEdu.util.getStore('mobEdu.main.store.modules');
            if (store.data.length > 0) {
                var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
                modulesLocalStore.removeAll();
                modulesLocalStore.add(store.getRange());
                modulesLocalStore.sync();
                mobEdu.main.f.getFeedsInfo();
                mobEdu.main.f.updateSlidingMenu(modulesLocalStore);
                var signUpRequired = mobEdu.util.getSingupStatus();
                if (showSignUpview == 'true') {
                    if (mobEdu.util.isShowSignUP() || mobEdu.util.getSingupStatus() == 'Y') {
                        mobEdu.main.f.displaySignUpForm();
                    }
                }
                if (showPremenu != null && showPremenu != undefined) {
                    if (isUpdate != true)
                        mobEdu.main.f.displayPreMenu();
                } else {
                    mobEdu.main.f.displayModules();
                }

                mobEdu.main.f.showSlidingMenu();
            } else {
                var status = store.getProxy().getReader().rawData.status;
                if (status != 'SUCCESS') {
                    Ext.Msg.show({
                        cls: 'msgbox',
                        title: null,
                        message: status,
                        buttons: Ext.MessageBox.OK
                    });
                }
            }
            mobEdu.notif.f.loadNotifications();
        },

        getFeedsInfo: function() {
            var feedsStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
            feedsStore.loadServer();
            mobEdu.util.gaTrackEvent('enroll', 'feeds');
        },

        displayModules: function(code) {

            if (showPremenu != null) {
                if (code == null) {
                    code = mobEdu.main.f.category;
                } else {
                    mobEdu.main.f.category = code;
                }
            }


            if (mainMenuLayout === 'BGRID') {
                mobEdu.main.f.displayBevelGridMenu(code);
                return;
            }

            if (mainMenuLayout === 'SGRID') {
                mobEdu.main.f.displaySMenu();
                return;
            }

            var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');

            //Dynamic width logic
            var maxIconsPerRow = 3;
            // Considering each icon needs a max of 150px,
            // If the width is high, (e.g. ipad), show 4 icons per row
            var viewportWidth = Ext.Viewport.getSize().width;
            if (viewportWidth > 450) {
                maxIconsPerRow = 4;
            }

            // Take 89% of the width for each icon. This 89% will fit 3 icons on phone perfectly.
            var iconWidth = Math.round((viewportWidth * 84 / 100) / maxIconsPerRow);
            var iconWidthStyle = 'style="width:' + iconWidth + 'px;"';
            var mainMenuIconCount = 0;
            var index = 0;

            var path = 'images/menu/';

            var botItems = new Array();
            var topLeftItems = new Array();
            var topRightItems = new Array();

            // Home button not required in main page for GRID layout.
            if (mainMenuLayout === 'AP' || mainMenuLayout === 'BOTH') {

                // On the main menu screen we need to explicitly add the button to operate sliding menu
                var slidingMenuBtn = new Ext.Button({
                    xtype: 'button',
                    ui: 'normal',
                    text: '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/home.png" style="vertical-align: middle">',
                    style: 'background:none;border:none; padding:0px;',
                    iconMask: true,
                    handler: function() {
                        mobEdu.util.showMenu();
                    }
                });

                topLeftItems.push(slidingMenuBtn);
            }

            if (modulesLocalStore.data.length > 0) {

                // This variable holds the html for the main menu GRID
                var temp = '';

                modulesLocalStore.each(function(record) {
                    // Check if the record has the show flag set
                    // Modules can be hidden using preferences menu
                    if (!mobEdu.util.isModuleExists(record.get('code'))) {

                        var code = record.get('code');

                        // Place the icons in the main menu based on this value
                        // MAIN - Put them in the GRID
                        // BOT - Add them to bottom toolbar
                        // TOP - LEFT and RIGHT - Place them in top toolbar
                        var position = record.get('position');
                        var button = null;
                        if (position != 'MAIN') {
                            button = new Ext.Button({
                                id: code,
                                ui: 'normal',
                                text: '<img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/menu/' + record.get('icon') + '"  style="vertical-align: middle">',
                                style: 'background: none; border:none;',
                                handler: function() {
                                    mobEdu.util.doLoginCheck(record.get('authRequired'), record.get('action'), record.get('code'), null, null);
                                }
                            });
                        }
                        if (position.toUpperCase() == 'BOT') {
                            botItems.push(button);
                        } else if (position.toUpperCase() == 'TOPLEFT') {
                            topLeftItems.push(button);
                        } else if (position.toUpperCase() == 'TOPRIGHT') {
                            topRightItems.push(button);
                        }
                    }
                });

                // Add the logout button by default, with hidden = true
                // If the user session existing, show it
                // This will also be shown after the login is successful


                // Check if the user is already logged in, so that we can show the logout button

                if (mainMenuLayout === 'GRID' || mainMenuLayout === 'SGRID' || mainMenuLayout === 'BGRID' || mainMenuLayout === 'BOTH') {
                    logoutItem = new Ext.Button({
                        id: 'logBtn',
                        ui: 'normal',
                        hidden: true,
                        text: isShowLogoutButton === 'true' ? '<img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/logout.png" style="vertical-align: middle">' : 'EXIT',
                        style: 'background:none;border:none;',
                        handler: function() {
                            mobEdu.main.f.logout();
                        }
                    });

                    // Check if the user is already logged in, so that we can show the logout button
                    if (mobEdu.util.getStudentId() != null || mobEdu.util.getAuthString() != null || mobEdu.util.getLeadAuthString() != null || mobEdu.util.getLeadId() != null || mobEdu.util.getRecruiterAuthString() != null || mobEdu.util.getRecruiterId() != null) {
                        logoutItem.setHidden(false);
                    }

                    topRightItems.push(logoutItem);
                }

                var topItems = new Array();
                // For Longer titles, do not use the title attribute
                // Just add a new label into the toolbar so that
                // its aligned after the buttons
                if (Ext.os.deviceType === 'Phone' && mobEdu.main.f.getTitle().length > 30) {
                    Ext.getCmp('mainTopTb').setTitle('');
                    var title = new Ext.Label({
                        html: "<center>" + mobEdu.main.f.getTitle() + "</center>",
                        padding: 0
                    });
                    // 200px max is ideal on Phones
                    title.setMaxWidth("200px");
                    topItems = topItems.concat(topLeftItems, title, new Ext.Spacer(), topRightItems);
                } else {
                    topItems = topItems.concat(topLeftItems, topRightItems);
                }

                // Ext.getCmp('mainTopTb').removeAll();
                Ext.getCmp('mainTopTb').setItems(topItems);

                var bottomToolbar = Ext.getCmp('mainBotTb');
                if (typeof bottomToolbar != "undefined") {
                    bottomToolbar.removeAll();
                    bottomToolbar.setItems(botItems);
                }
                if (mainMenuLayout === 'GRID' || mainMenuLayout === 'BOTH') {
                    mobEdu.main.f.displayGridMenu();
                }
            }
        },
        doAction: function() {
            if (settingsOnToolbar == 'true') {
                var record = mobEdu.util.getModuleByCode('SETTINGS');
                mobEdu.util.doLoginCheck(record.get('authRequired'), record.get('action'), record.get('code'));
            } else {
                if (mobEdu.util.getStudentId() == null && mobEdu.util.getAuthString() == null) {
                    mobEdu.main.f.showLogin();
                } else {
                    mobEdu.main.f.logout();
                }
            }
        },
        displayBevelGridMenu: function() {
            mobEdu.main.f.subMenuCategory = false;
            if (showPremenu == 'always')
                mobEdu.util.updatePrevView(mobEdu.main.f.displayPreMenu);
            mobEdu.main.f.updateBevelGridMenu();
            mobEdu.util.show('mobEdu.main.view.bevelmenu');
            Ext.getCmp('bevelTitle').setTitle('<h1>' + mobEdu.main.f.categoryDesc + '</h1>');
        },
        updateBevelGridMenu: function(category) {
            mobEdu.util.get('mobEdu.main.view.bevelmenu');
            if (Ext.getCmp('mainModules') != null) {
                Ext.getCmp('mainModules').removeAll();
            }
            var iHeight = window.screen.height;
            if (Ext.os.deviceType === 'Phone') {
                if (iHeight == 568) {
                    menuIconSize = 60;
                } else {
                    menuIconSize = 52;
                }
            } else {
                menuIconSize = 110;
            }

            var gridLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');

            gridLocalStore.clearFilter();
            if (mobEdu.util.parentModule == 'FAVS') {
                gridLocalStore.filter('select', 'Y');
            } else {
                gridLocalStore.filter("category", mobEdu.util.parentModule);
            }


            var maxIconsPerRow = 3;
            var viewportWidth = null;
            var maxRows = 3;

            if (Ext.os.is.Android) {
                if (Ext.os.version.Major != undefined) {
                    if (!(Ext.os.version.Major > 4)) {
                        if (!(Ext.os.version.Minor > 3)) {
                            var viewportWidth = screen.width;
                            var viewportHeight = screen.height;
                        } else {
                            var viewportWidth = Ext.Viewport.getSize().width;
                            var viewportHeight = Ext.Viewport.getSize().height;
                        }
                    } else {
                        var viewportWidth = Ext.Viewport.getSize().width;
                        var viewportHeight = Ext.Viewport.getSize().height;
                    }
                } else {
                    var iconWidth = Math.round((Ext.getCmp('mainModules').element.dom.scrollWidth * 80 / 100) / maxIconsPerRow);
                }
            } else {
                var viewportWidth = Ext.Viewport.getSize().width;
                var viewportHeight = Ext.Viewport.getSize().height;
            }
            if (viewportWidth != undefined) {
                if (viewportWidth > 600) {
                    var iconWidth = Math.round((viewportWidth * (Ext.os.is.Phone ? 80 : 84) / 100) / maxIconsPerRow) - menuIconSize - 36;
                } else {
                    var iconWidth = Math.round((viewportWidth * (Ext.os.is.Phone ? 80 : 84) / 100) / maxIconsPerRow) - menuIconSize;
                }
            }
            if (mobEdu.main.f.iconWidthValue == null)
                mobEdu.main.f.iconWidthValue = iconWidth;
            // if (viewportWidth > 450) {
            //  maxIconsPerRow = 4;
            // }
            // var iconWidth = Math.round((viewportWidth * 84 / 100) / maxIconsPerRow);
            if (Ext.os.version.Minor == undefined) {
                var iconWidthStyle = 'style="width:' + mobEdu.main.f.iconWidthValue + 'px;"';
            } else {
                var iconWidthStyle = 'style="width:' + iconWidth + 'px;"';
            }
            var mainMenuIconCount = 0;
            var path = 'images/menu/';
            var pageNo = 1;

            var gridMenuIconImageWidth = Ext.os.is.Phone ? menuIconSize : mobEdu.util.getIconSize();

            if (gridLocalStore.data.length > 0) {

                var temp = '';
                var rowHtml = '';
                gridLocalStore.each(function(record) {
                    // if(mobEdu.main.f.category==record.get('category')){

                    if (!mobEdu.util.isModuleExists(record.get('code'))) {
                        var code = record.get('code');
                    }

                    var position = record.get('position');
                    var subCategory = record.get('subCategory');
                    if ((position.toUpperCase() == 'MAIN' || position.toUpperCase() == 'SUBMENU') && subCategory == undefined) {
                        mainMenuIconCount += 1;

                        rowHtml = rowHtml + '<div class="icons" ' + iconWidthStyle + '><a href="javascript:mobEdu.util.doLoginCheck(' + record.get('authRequired') + ",'" + record.get('action') + "','" + record.get('code') + "','" + record.get('icon') + "','" + mainMenuIconCount + "','" + record.get('category') + "','" + record.get('subCategory') + "','" + record.get('position') + '\');"><img style="" id=' + mainMenuIconCount + ' height=' + gridMenuIconImageWidth + 'px" width="' + gridMenuIconImageWidth + 'px" src="' + mobEdu.util.getResourcePath() + path + record.get('icon') + '" ></a><div style="height: 20px;padding-top: 3px">' + record.get('description') + '</div></div>'
                        if (mainMenuIconCount % maxIconsPerRow === 0) {
                            rowHtml = '<div class="iconrowbg">' + rowHtml + '</div>';
                            temp = temp + rowHtml;
                            rowHtml = '';
                            // When we reach end of row, we stop and put the icons in a div and add it to final html
                        }
                    }

                    if (mainMenuIconCount >= pageNo * maxRows * maxIconsPerRow) {
                        pageNo += 1;

                        if (Ext.getCmp('mainModules') != null) {
                            if (rowHtml != '') {
                                temp = temp + rowHtml;
                                rowHtml = '';
                            }
                            temp = '<div align=center>' + temp + "</div>";
                            var myPanel = Ext.create('Ext.Panel', {
                                html: temp
                            });
                            Ext.getCmp('mainModules').add([myPanel]);
                        }
                        temp = '';
                        // When we reach end of a page, we stop and add a page in carousel and put our icons in there.
                    }
                    // }
                });

                // If we have something in temp here, that means its the last page
                // with fewer icons
                if (rowHtml != '') {
                    temp = temp + rowHtml;
                }

                if (temp != '' && Ext.getCmp('mainModules') != null) {

                    for (i = 0; i < maxIconsPerRow - (mainMenuIconCount % maxIconsPerRow); i++) {
                        temp += '<div class="icons" ' + iconWidthStyle + '>&nbsp;</div>';
                    }

                    temp = '<div align=center>' + temp + "</div>";
                    var myPanel = Ext.create('Ext.Panel', {
                        html: temp
                    });
                    Ext.getCmp('mainModules').add([myPanel]);
                }
                Ext.getCmp('mainModules').setActiveItem(0);
            }

        },

        loadSubMenu: function(code, category) {
            mobEdu.main.f.subMenuCategory = true;
            mobEdu.util.updatePrevView(mobEdu.main.f.displayBevelGridMenu);
            mobEdu.main.f.updateSubMenu(code);
            mobEdu.util.show('mobEdu.main.view.subMenu');
            Ext.getCmp('subMenuTitle').setTitle('<h1>' + mobEdu.main.f.categoryDesc + '</h1>');
        },
        updateSubMenu: function(category) {

            mobEdu.util.get('mobEdu.main.view.subMenu');
            if (Ext.getCmp('subModules') != null) {
                Ext.getCmp('subModules').removeAll();
            }
            var iHeight = window.screen.height;
            if (Ext.os.deviceType === 'Phone') {
                if (iHeight == 568) {
                    menuIconSize = 60;
                } else {
                    menuIconSize = 52;
                }
            } else {
                menuIconSize = 110;
            }

            var gridLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');

            gridLocalStore.clearFilter();
            if (mobEdu.util.parentModule == 'FAVS')
                gridLocalStore.filter('select', 'Y');
            else
                gridLocalStore.filter("subCategory", category);


            var maxIconsPerRow = 3;
            var viewportWidth = null;
            var maxRows = 3;

            if (Ext.os.is.Android) {
                if (Ext.os.version.Major != undefined) {
                    if (!(Ext.os.version.Major > 4)) {
                        if (!(Ext.os.version.Minor > 3)) {
                            var viewportWidth = screen.width;
                            var viewportHeight = screen.height;
                        } else {
                            var viewportWidth = Ext.Viewport.getSize().width;
                            var viewportHeight = Ext.Viewport.getSize().height;
                        }
                    } else {
                        var viewportWidth = Ext.Viewport.getSize().width;
                        var viewportHeight = Ext.Viewport.getSize().height;
                    }
                } else {
                    var iconWidth = Math.round((Ext.getCmp('subModules').element.dom.scrollWidth * 80 / 100) / maxIconsPerRow);
                }
            } else {
                var viewportWidth = Ext.Viewport.getSize().width;
                var viewportHeight = Ext.Viewport.getSize().height;
            }
            if (viewportWidth != undefined) {
                if (viewportWidth > 600) {
                    var iconWidth = Math.round((viewportWidth * (Ext.os.is.Phone ? 80 : 84) / 100) / maxIconsPerRow) - menuIconSize - 36;
                } else {
                    var iconWidth = Math.round((viewportWidth * (Ext.os.is.Phone ? 80 : 84) / 100) / maxIconsPerRow) - menuIconSize;
                }
            }
            if (mobEdu.main.f.iconWidthValue == null)
                mobEdu.main.f.iconWidthValue = iconWidth;
            // if (viewportWidth > 450) {
            //  maxIconsPerRow = 4;
            // }
            // var iconWidth = Math.round((viewportWidth * 84 / 100) / maxIconsPerRow);
            if (Ext.os.version.Minor == undefined) {
                var iconWidthStyle = 'style="width:' + mobEdu.main.f.iconWidthValue + 'px;"';
            } else {
                var iconWidthStyle = 'style="width:' + iconWidth + 'px;"';
            }
            var mainMenuIconCount = 0;
            var path = 'images/menu/';
            var pageNo = 1;

            var gridMenuIconImageWidth = Ext.os.is.Phone ? menuIconSize : mobEdu.util.getIconSize();

            if (gridLocalStore.data.length > 0) {

                var temp = '';
                var rowHtml = '';
                gridLocalStore.each(function(record) {
                    // if(mobEdu.main.f.category==record.get('category')){

                    if (!mobEdu.util.isModuleExists(record.get('code'))) {
                        var code = record.get('code');
                    }

                    var position = record.get('position');
                    if (position.toUpperCase() == 'MAIN') {
                        mainMenuIconCount += 1;

                        rowHtml = rowHtml + '<div class="icons" ' + iconWidthStyle + '><a href="javascript:mobEdu.util.doLoginCheck(' + record.get('authRequired') + ",'" + record.get('action') + "','" + record.get('code') + "','" + record.get('icon') + "','" + mainMenuIconCount + "','" + record.get('category') + "','" + record.get('subCategory') + "','" + record.get('position') + '\');"><img style="" id=' + mainMenuIconCount + ' height=' + gridMenuIconImageWidth + 'px" width="' + gridMenuIconImageWidth + 'px" src="' + mobEdu.util.getResourcePath() + path + record.get('icon') + '" ></a><div style="height: 20px;padding-top: 3px">' + record.get('description') + '</div></div>'
                        if (mainMenuIconCount % maxIconsPerRow === 0) {
                            rowHtml = '<div class="iconrowbg">' + rowHtml + '</div>';
                            temp = temp + rowHtml;
                            rowHtml = '';
                            // When we reach end of row, we stop and put the icons in a div and add it to final html
                        }
                    }

                    if (mainMenuIconCount >= pageNo * maxRows * maxIconsPerRow) {
                        pageNo += 1;

                        if (Ext.getCmp('subModules') != null) {
                            if (rowHtml != '') {
                                temp = temp + rowHtml;
                                rowHtml = '';
                            }
                            temp = '<div align=center>' + temp + "</div>";
                            var myPanel = Ext.create('Ext.Panel', {
                                html: temp
                            });
                            Ext.getCmp('subModules').add([myPanel]);
                        }
                        temp = '';
                        // When we reach end of a page, we stop and add a page in carousel and put our icons in there.
                    }
                    // }
                });

                // If we have something in temp here, that means its the last page
                // with fewer icons
                if (rowHtml != '') {
                    temp = temp + rowHtml;
                }

                if (temp != '' && Ext.getCmp('subModules') != null) {

                    for (i = 0; i < maxIconsPerRow - (mainMenuIconCount % maxIconsPerRow); i++) {
                        temp += '<div class="icons" ' + iconWidthStyle + '>&nbsp;</div>';
                    }

                    temp = '<div align=center>' + temp + "</div>";
                    var myPanel = Ext.create('Ext.Panel', {
                        html: temp
                    });
                    Ext.getCmp('subModules').add([myPanel]);
                }
                Ext.getCmp('subModules').setActiveItem(0);
            }

        },


        displayGridMenu: function() {
            var gridLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var maxIconsPerRow = 3;
            if (Ext.os.is.Android) {
                var viewportWidth = screen.width;
            } else {
                var viewportWidth = Ext.Viewport.getSize().width;
            }
            if (viewportWidth > 450) {
                maxIconsPerRow = 4;
            }
            var iconWidth = Math.round((viewportWidth * 84 / 100) / maxIconsPerRow);
            var iconWidthStyle = 'style="width:' + iconWidth + 'px;"';
            var mainMenuIconCount = 0;
            var path = 'images/menu/';
            if (gridLocalStore.data.length > 0) {
                var temp = '';
                gridLocalStore.each(function(record) {
                    if (!mobEdu.util.isModuleExists(record.get('code')))
                        var code = record.get('code');
                    var position = record.get('position');
                    if (position.toUpperCase() == 'MAIN') {
                        mainMenuIconCount += 1;
                        temp = temp + '<div class="icons" ' + iconWidthStyle + '><a href="javascript:mobEdu.util.doLoginCheck(' + record.get('authRequired') + ",'" + record.get('action') + "','" + record.get('code') + "," + null + "," + null + '\');"><img height=' + mobEdu.util.getIconSize() + 'px" width="' + mobEdu.util.getIconSize() + 'px" src="' + mobEdu.util.getResourcePath() + path + record.get('icon') + '" ></a><div style="height: 20px;padding-top: 3px">' + record.get('description') + '</div></div>'
                    }
                });
                for (i = 0; i < maxIconsPerRow - (mainMenuIconCount % maxIconsPerRow); i++) {
                    temp += '<div class="icons" ' + iconWidthStyle + '>&nbsp;</div>';
                }

                temp = '<div align=center>' + temp + "</div>";

                if (Ext.getCmp('mainModules') != null) {
                    Ext.getCmp('mainModules').setHtml(temp);
                }
            }
        },

        displaySMenu: function() {
            var iHeight = window.screen.height;
            if (Ext.os.deviceType === 'Phone') {
                listHeight = 90;
                if (iHeight == 568) {
                    menuIconSize = 60;
                } else {
                    menuIconSize = 55;
                }
            } else {
                menuIconSize = 110;
                if (Ext.Viewport.getOrientation() === 'portrait') {
                    listHeight = Math.round(((Ext.getCmp('category').element.dom.scrollHeight) - 100) / 3);
                } else {
                    listHeight = Math.round(((Ext.getCmp('category').element.dom.scrollHeight) - 20) / 3);
                }
            }
            Ext.getCmp('logBtn').setCls(mobEdu.util.getStudentId() == null && mobEdu.util.getAuthString() == null ? 'login' : 'logout');
            var gridLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var listItems = new Array();
            var panelItems = new Array();
            var iconWidth = 130;
            var iconWidthStyle = 'style="width:' + iconWidth + 'px;"';
            var gridMenuIconImageWidth = Ext.os.is.Phone ? menuIconSize : mobEdu.util.getIconSize();
            var path = 'images/menu/';
            var lists = new Array();
            var mainMenuIconCount = 0;
            if (Ext.Viewport.getOrientation() === 'portrait') {
                if (iHeight != 568) {
                    Ext.getCmp('category').setScrollable(true);
                } else {
                    Ext.getCmp('category').setScrollable(false);
                }
            } else {
                Ext.getCmp('category').setScrollable(true);
            }
            gridLocalStore.each(function(rec) {
                if (!mobEdu.util.isModuleExists(rec.get('code'))) {
                    var code = rec.get('code');
                }
                var position = rec.get('position');
                var category = rec.get('category');
                if (position.toUpperCase() == 'MAIN') {
                    if (!(Ext.Array.contains(lists, category))) {
                        lists.push(category);
                        listItems = new Array();
                        gridLocalStore.each(function(record) {
                            if (record.get('category') == category && (record.get('position')).toUpperCase() == 'MAIN') {
                                mainMenuIconCount += 1;
                                listItems.push({
                                    // data: '<div align=center class="icons"' + iconWidthStyle + ' ><a href="javascript:mobEdu.util.doLoginCheck(' + record.get('authRequired') + ",'" + record.get('action') + "','" + record.get('code') + "','" + record.get('icon') + "','" + mainMenuIconCount + '\');"><img height=' + mobEdu.util.getIconSize() + 'px" width="' + mobEdu.util.getIconSize() + 'px" src="' + mobEdu.util.getResourcePath() + path + record.get('icon') + '" ></a><div style="height: 20px;padding-top: 3px">' + record.get('description') + '</div></div>'
                                    data: '<table><tr><td align="center" ' + iconWidthStyle + ' ><a href="javascript:mobEdu.util.doLoginCheck(' + record.get('authRequired') + ",'" + record.get('action') + "','" + record.get('code') + "','" + record.get('icon') + "','" + mainMenuIconCount + '\');"><img id=' + mainMenuIconCount + ' height=' + gridMenuIconImageWidth + 'px" width="' + gridMenuIconImageWidth + 'px" src="' + mobEdu.util.getResourcePath() + path + record.get('icon') + '" ></a><br />' + record.get('description') + '</td></tr></table>'
                                });
                            }
                        });

                        if (Ext.getCmp(category) != null) {
                            Ext.getCmp('category').remove(Ext.getCmp(category));
                        }

                        var label = new Ext.Label({
                            html: '<h2>' + category + '</h2>',
                            margin: Ext.os.is.Tablet ? '0 0 20 25' : '0 0 0 25'
                        });

                        var list = new Ext.DataView({
                            id: category,
                            itemTpl: '{data}',
                            data: listItems,
                            height: listHeight,
                            inline: {
                                wrap: false
                            },
                            scrollable: {
                                direction: 'horizontal',
                                directionLock: true
                            }
                        });
                        panelItems.push(label);
                        panelItems.push(list);
                    }

                }
            });

            Ext.getCmp('category').setItems(panelItems);

        },
        displaySignUpForm: function() {
            //mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            // Ext.getCmp('signUp').hide();
            mobEdu.util.show('mobEdu.main.view.signUp');


        },
        submitSignUpForm: function() {

            var nameField = Ext.getCmp('signupName');
            var nameValue = nameField.getValue();
            var cEmail = Ext.getCmp('signupEmail');
            var cEmailValue = cEmail.getValue();
            var organisationField = Ext.getCmp('signupOrgs');
            var organisation = organisationField.getValue();
            var phoneField = Ext.getCmp('signupNumber');
            var phone = phoneField.getValue();
            phone = phone.replace(/-|\(|\)/g, "");

            if (nameValue != null && cEmailValue != null && organisation != null && nameValue != '' && cEmailValue != '' && organisation != '') {
                var firstName = Ext.getCmp('signupName');
                var regExp = new RegExp("^[a-zA-Z ]+$");
                if (!regExp.test(nameValue)) {
                    Ext.Msg.show({
                        id: 'name',
                        name: 'name',
                        cls: 'msgbox',
                        title: null,
                        message: '<p>Invalid full name.</p>',
                        buttons: Ext.MessageBox.OK,
                        fn: function(btn) {
                            if (btn == 'ok') {
                                firstName.focus(true);
                            }
                        }
                    });
                } else {
                    var mail = Ext.getCmp('signupEmail');
                    var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
                    if (regMail.test(cEmailValue) == false) {
                        Ext.Msg.show({
                            id: 'email',
                            name: 'email',
                            title: null,
                            cls: 'msgbox',
                            message: '<p>Invalid e-mail address.</p>',
                            buttons: Ext.MessageBox.OK,
                            fn: function(btn) {
                                if (btn == 'ok') {
                                    mail.focus(true);
                                }
                            }
                        });
                    } else {
                        if (nameValue.charAt(0) == ' ') {
                            Ext.Msg.show({
                                id: 'name',
                                name: 'comment',
                                title: null,
                                cls: 'msgbox',
                                message: '<p>Name first letter must be alphanumeric.</p>',
                                buttons: Ext.MessageBox.OK,
                                fn: function(btn) {
                                    if (btn == 'ok') {
                                        nameField.focus(true);
                                        nameField.setValue('');
                                    }
                                }
                            });
                        } else {
                            if (organisation.charAt(0) == ' ') {
                                Ext.Msg.show({
                                    id: 'comment',
                                    name: 'comment',
                                    title: null,
                                    cls: 'msgbox',
                                    message: '<p> Organisation name must be alphanumeric.</p>',
                                    buttons: Ext.MessageBox.OK,
                                    fn: function(btn) {
                                        if (btn == 'ok') {
                                            organisation.focus(true);
                                            organisation.setValue('');
                                        }
                                    }
                                });
                            } else if (phone.length != 10 && phone.length > 0) {
                                Ext.Msg.show({
                                    id: 'phone1msg',
                                    name: 'phone1msg',
                                    title: null,
                                    cls: 'msgbox',
                                    message: '<p>Invalid Phone Number.</p>',
                                    buttons: Ext.MessageBox.OK,
                                    fn: function(btn) {
                                        if (btn == 'ok') {
                                            phone.focus(true);
                                        }
                                    }
                                });
                            } else {
                                mobEdu.main.f.sendSignUpForm(nameValue, cEmailValue, organisation, phone);
                            }
                        }
                    }
                }
            } else {
                Ext.Msg.show({
                    message: '<p>Please provide all mandatory fields.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }

        },
        sendSignUpForm: function(name, email, organisation, phone) {

            var store = mobEdu.util.getStore('mobEdu.main.store.signUp');
            store.getProxy().setHeaders({
                companyID: companyId
            });
            store.getProxy().setUrl(encorewebserver + 'submitSignUpForm');
            store.getProxy().setExtraParams({
                name: name,
                email: email,
                organisation: organisation,
                phone: phone
            });
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.sendSignUpResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'N2N Sign Up');
        },
        sendSignUpResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.signUp');
            if (store.data.length > 0) {
                var signUpLocalStore = mobEdu.util.getStore('mobEdu.main.store.signUpLocal');
                // signUpLocalStore.removeAll();
                // signUpLocalStore.add(store.getRange());
                // signUpLocalStore.sync();
                var status = store.getProxy().getReader().rawData.status;
                if (status == 'SUCCESS') {
                    mobEdu.util.updateSingupStatus('N');
                    Ext.Msg.show({
                        id: 'svsubmitsuccess',
                        title: null,
                        cls: 'msgbox',
                        message: '<p>Thank you  for signing up with us.</p>',
                        buttons: Ext.MessageBox.OK,
                        fn: function(btn) {
                            if (btn == 'ok') {
                                mobEdu.main.f.displayModules();
                                mobEdu.main.f.showSlidingMenu();
                            }
                        }
                    });
                }
                if (showPremenu != null && showPremenu != undefined) {
                    mobEdu.main.f.displayPreMenu();
                } else {
                    mobEdu.util.showMainView();
                }
            } else {
                var status = store.getProxy().getReader().rawData.status;
                if (status != 'SUCCESS') {
                    Ext.Msg.show({
                        cls: 'msgbox',
                        title: null,
                        message: status,
                        buttons: Ext.MessageBox.OK
                    });
                }
            }

        },
        logout: function() {
            if (mobEdu.util.sessionCASValidated === true) {
                mobEdu.util.casLogout();
            }
            if (mobEdu.util.getStudentId() != null || mobEdu.util.getAuthString() != null || mobEdu.util.getLeadAuthString() != null || mobEdu.util.getRecruiterAuthString() != null || mobEdu.util.getRecruiterId() != null || mobEdu.util.getLeadId() != null) {
                mobEdu.storeUtil.clearAll();


                if (typeof navigator.splashscreen != "undefined") {
                    navigator.splashscreen.show();
                }

                setTimeout(function() {
                    window.open('main.html', '_self');
                }, 2000);

                advisorflag = false;
                admisflag = false;
            } else {
                if (showPremenu != null && showPremenu != undefined) {
                    mobEdu.main.f.displayPreMenu();
                } else {
                    mobEdu.util.updateNextView(mobEdu.util.showMainView);
                    mobEdu.main.f.showLogin();
                }
            }

        },
        resetstudentFeedback: function() {
            mobEdu.util.get('mobEdu.main.view.studentFeedback');
            if (mainMenuLayout === 'AP') {
                mobEdu.util.get('mobEdu.main.view.studentFeedback').reset();
            }
        },
        loadRecipientList: function(code, from) {
            var store = mobEdu.util.getStore('mobEdu.main.store.recipientsList');
            store.clearFilter();
            if (from != null) {
                store.filter('category', from);
            } else {
                store.filterBy(function(record) {
                    if (record.get('category') != 'HOUSING') {
                        return record;
                    }
                });
            }
            if (store.getCount() > 0) {
                mobEdu.main.f.showStudentFeedback(from);
            }
            store.loadServer(function() {
                mobEdu.main.f.showStudentFeedback(from);
            });
        },

        loadStudentFeedback: function() {
            var recipientsField = Ext.getCmp('recipients');
            var recipientsValue = recipientsField.getValue();
            var store = mobEdu.util.getStore('mobEdu.main.store.recipientsList');
            var index = 1;
            store.each(function(record) {
                if (record.get('email') === recipientsValue) {
                    if (index > 9) {
                        recipientsValue = recipientsValue.substring(0, recipientsValue.length - 2);
                    } else {
                        recipientsValue = recipientsValue.substring(0, recipientsValue.length - 1);
                    }
                }
                index++;
            });
            if (recipientsValue == 'select') {
                recipientsValue = '';
            }
            var fNameField = Ext.getCmp('fullName');
            var fNameValue = fNameField.getValue();
            var cEmail = Ext.getCmp('email');
            var cEmailValue = cEmail.getValue();
            var vReason = Ext.getCmp('comments');
            var reason = vReason.getValue();
            if (fNameValue != null && cEmailValue != null && reason != null && fNameValue != '' && cEmailValue != '' && reason != '' && recipientsValue != null && recipientsValue != '') {
                var firstName = Ext.getCmp('fullName');
                var regExp = new RegExp("^[a-zA-Z ]+$");
                if (!regExp.test(fNameValue)) {
                    Ext.Msg.show({
                        id: 'firstNmae',
                        name: 'firstNmae',
                        cls: 'msgbox',
                        title: null,
                        message: '<p>Invalid full name.</p>',
                        buttons: Ext.MessageBox.OK,
                        fn: function(btn) {
                            if (btn == 'ok') {
                                firstName.focus(true);
                            }
                        }
                    });
                } else {
                    var mail = Ext.getCmp('email');
                    var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
                    if (regMail.test(cEmailValue) == false) {
                        Ext.Msg.show({
                            id: 'email',
                            name: 'email',
                            title: null,
                            cls: 'msgbox',
                            message: '<p>Invalid e-mail address.</p>',
                            buttons: Ext.MessageBox.OK,
                            fn: function(btn) {
                                if (btn == 'ok') {
                                    mail.focus(true);
                                }
                            }
                        });
                    } else {
                        if (fNameValue.charAt(0) == ' ') {
                            Ext.Msg.show({
                                id: 'fullname',
                                name: 'comment',
                                title: null,
                                cls: 'msgbox',
                                message: '<p>Name first letter must be alphanumeric.</p>',
                                buttons: Ext.MessageBox.OK,
                                fn: function(btn) {
                                    if (btn == 'ok') {
                                        fNameField.focus(true);
                                        fNameField.setValue('');
                                    }
                                }
                            });
                        } else {
                            if (reason.charAt(0) == ' ') {
                                Ext.Msg.show({
                                    id: 'comment',
                                    name: 'comment',
                                    title: null,
                                    cls: 'msgbox',
                                    message: '<p>Comment first letter must be alphanumeric.</p>',
                                    buttons: Ext.MessageBox.OK,
                                    fn: function(btn) {
                                        if (btn == 'ok') {
                                            vReason.focus(true);
                                            vReason.setValue('');
                                        }
                                    }
                                });
                            } else {
                                mobEdu.main.f.sentStudentFeedback();
                            }
                        }
                    }
                }
                //                Ext.getCmp('menuHome').refresh();
            } else {
                Ext.Msg.show({
                    message: '<p>Please provide all mandatory fields.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        sentStudentFeedback: function() {
            var recipientsField = Ext.getCmp('recipients');
            var recipientsValue = recipientsField.getValue();
            var store = mobEdu.util.getStore('mobEdu.main.store.recipientsList');
            var index = 1;
            store.each(function(record) {
                if (record.get('email') === recipientsValue) {
                    if (index > 9) {
                        recipientsValue = recipientsValue.substring(0, recipientsValue.length - 2);
                    } else {
                        recipientsValue = recipientsValue.substring(0, recipientsValue.length - 1);
                    }
                }
                index++;
            });
            if (recipientsValue == 'select') {
                recipientsValue = '';
            }
            var fNameField = Ext.getCmp('fullName');
            var fNameValue = fNameField.getValue();
            var cEmail = Ext.getCmp('email');
            var cEmailValue = cEmail.getValue();
            var vReason = Ext.getCmp('comments');
            var reason = vReason.getValue();
            if (includeRecipientDetails === 'true') {
                reason = reason + '<br><br>' + fNameValue + '<br>' + cEmailValue;
            }
            var fbSubject = 'Feedback';
            var feedbackStore = mobEdu.util.getStore('mobEdu.main.store.feedback');
            feedbackStore.getProxy().setUrl(webserver + 'submitFeedback');
            if (mobEdu.util.getAuthString() != null) {
                feedbackStore.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
            } else {
                feedbackStore.getProxy().setHeaders({
                    companyID: companyId
                });
            }
            feedbackStore.getProxy().setExtraParams({
                fromAddress: cEmailValue,
                fromName: fNameValue,
                recipients: recipientsValue,
                subject: fbSubject,
                comments: reason,
                comments2: '',
                courseId: '',
                confidential: '',
                companyId: companyId
            });
            feedbackStore.getProxy().afterRequest = function() {
                mobEdu.main.f.sentStudentFeedbackResponseHandler();
            };
            feedbackStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'feedback');
        },
        sentStudentFeedbackResponseHandler: function() {
            var feedbackStore = mobEdu.util.getStore('mobEdu.main.store.feedback');
            var response = feedbackStore.getProxy().getReader().rawData;
            if (response == "SUCCESS") {
                Ext.Msg.show({
                    id: 'svsubmitsuccess',
                    title: null,
                    cls: 'msgbox',
                    message: '<center>Thanks for your feedback.</center>',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            mobEdu.main.f.resetstudentFeedback();
                            mobEdu.util.showMainView();
                        }
                    }
                });
            } else {
                Ext.Msg.show({
                    message: response,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        getTitle: function() {
            var title = mobEdu.util.getSchoolName();
            if (title != null && title != '') {
                return '<h1>' + title + '</h1>';
            } else if (clientLogoExist == 'yes') {
                return '<img height="80px" width="80px" src="' + mobEdu.util.getResourcePath() + clientLogoPath + '" style="vertical-align: middle">';
            } else {

                return '<h1>' + clientTitle + '</h1>';

            }
        },
        updateCampusCode: function() {
            var code = Ext.getCmp('campusCode').getValue();
            var campus = Ext.getCmp('campusCode').getRecord().getData().text;
            if (code == '0') {
                Ext.Msg.show({
                    cls: 'msgbox',
                    title: null,
                    message: 'Please select a school.',
                    buttons: Ext.MessageBox.OK
                });
            } else {
                mobEdu.storeUtil.clearAll();
                mobEdu.util.updateDefaultCampus(code);
                mobEdu.util.updateSchoolName(campus);
                mobEdu.util.updateSchoolCode(code);
                campusCode = code;
                var oldPath = mobEdu.util.getResourcePath();
                //                mobEdu.util.updateResourcePath(resourcesPath + code + '/');
                primaryModule = 'main';
                //mobEdu.util.replaceClientFiles(oldPath);
                // As of now we only have campus view in the viewport
                // Just remove everything, load the theme and then load all the views
                Ext.Viewport.removeAll();
                if (code == 'SPSU') {
                    mobEdu.util.loadExternalUrl('https://denver.spsu.edu/entourage/');
                } else {
                    window.open('main.html', '_self');
                }
                //                mobEdu.util.updateTheme();
                //                mobEdu.main.f.loadViews();
            }
        },
        loadSchools: function() {
            Ext.Msg.show({
                cls: 'msgbox',
                title: null,
                message: 'Would you like to select a different school?',
                buttons: Ext.MessageBox.YESNO,
                fn: function(btn) {
                    if (btn == 'yes') {
                        mobEdu.main.f.showCampus();
                    }
                }
            });
        },

        loadUG: function() {
            mobEdu.storeUtil.clearAll();
            mobEdu.util.updateSchoolName('University of Georgia');
            mobEdu.util.updateSchoolCode('UG');
            campusCode = 'UG';
            mobEdu.util.updateDefaultCampus('UG');
            var oldPath = mobEdu.util.getResourcePath();
            //                mobEdu.util.updateResourcePath(resourcesPath + code + '/');
            primaryModule = 'main';
            //mobEdu.util.replaceClientFiles(oldPath);
            // As of now we only have campus view in the viewport
            // Just remove everything, load the theme and then load all the views
            Ext.Viewport.removeAll();
            window.open('main.html', '_self');
        },

        onProxyRegButtonTap: function() {
            var proxyId = mobEdu.util.getProxyUserId();
            if (proxyId == null || proxyId == '') {
                mobEdu.settings.f.showProxySession();
            } else {
                Ext.Msg.show({
                    message: 'You are already proxying ' + proxyId + '. Do you wish to clear it?',
                    buttons: Ext.MessageBox.YESNO,
                    cls: 'msgbox',
                    fn: function(btn) {
                        if (btn == 'yes') {
                            mobEdu.main.f.stopProxySession(proxyId);
                        }
                    }
                });
            }
        },
        onProxySubmitButtonTap: function() {
            var proxyId = Ext.getCmp('txtProxyId').getValue();

            if (proxyId === "" || proxyId === null) {
                Ext.Msg.show({
                    message: 'Please provide UTAD username',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else if (proxyId.toLowerCase() === mobEdu.util.getStudentId().toLowerCase()) {
                Ext.Msg.show({
                    message: 'You are already logged in as ' + proxyId + '. Please use another username.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                mobEdu.main.f.startProxySession(proxyId);
            }
        },
        startProxySession: function(proxyId) {
            var store = mobEdu.util.getStore('mobEdu.main.store.proxySession');
            store.getProxy().setUrl(webserver + 'start');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: proxyId,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.startProxySessionResponseHandler(proxyId);
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'startproxy');
        },
        startProxySessionResponseHandler: function(proxyId) {
            var store = mobEdu.util.getStore('mobEdu.main.store.proxySession');
            var status = store.getAt(0).get('status');
            if (status == 'SUCCESS') {
                var id = store.getAt(0).get('studentId');

                if (id === mobEdu.util.getStudentId()) {
                    Ext.Msg.show({
                        message: 'You are already logged in as ' + proxyId + '. Please use another username.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                    return;
                }

                mobEdu.util.updateProxyUserId(id);
                mobEdu.util.updateProxyLdapId(proxyId);
                mobEdu.reg.f.loadCartTriggeredBy = 'PROXY';
                mobEdu.reg.f.loadAllCartItems();
            } else {
                Ext.Msg.show({
                    message: status,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        stopProxySession: function(proxyId) {
            var store = mobEdu.util.getStore('mobEdu.main.store.proxySession');
            store.getProxy().setUrl(webserver + 'stop');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: proxyId,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.stopProxySessionResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'stopproxy');
        },
        stopProxySessionResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.proxySession');
            var status = store.getAt(0).get('status');
            if (status == 'SUCCESS') {
                mobEdu.util.updateProxyUserId(null);
                mobEdu.util.updateProxyLdapId(null);
                mobEdu.reg.f.loadAllCartItems();
                mobEdu.settings.f.showProxySession();
            } else {
                Ext.Msg.show({
                    message: status,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        getStudentId: function() {
            var id = mobEdu.util.getProxyUserId();
            if (id == null || id == '') {
                id = mobEdu.util.getStudentId();
            }
            return id;
        },

        getStudentLdapId: function() {
            var id = mobEdu.util.getProxyLdapId();
            if (id == null || id == '') {
                id = mobEdu.util.getStudentLdapId();
            }
            return id;
        },

        isModuleExists: function(flag) {
            if (flag == true) {
                return true;
            }
            return false;
        },

        onEditModuleItemTap: function(view, index, target, record, item, e, eOpts) {
            mobEdu.util.deselectSelectedItem(index, view);
            if (item.target.name == "on") {
                if (record.data.code != 'SETTINGS') {
                    mobEdu.main.f.moduleOff(record.data.code);
                } else {
                    Ext.Msg.show({
                        cls: 'msgbox',
                        title: null,
                        message: 'Settings is a default module, cannot be disabled',
                        buttons: Ext.MessageBox.OK
                    });
                }
            } else if (item.target.name == "off") {
                mobEdu.main.f.moduleOn(record.data.code);
            } else if (item.target.name == "up") {
                mobEdu.main.f.moveUp(index);
            } else if (item.target.name == "down") {
                mobEdu.main.f.moveDown(index);
            } else if (item.target.id == "modulename") {
                if (disableModuleNameEdit != 'true') {
                    var value = record.get('description');
                    Ext.Msg.prompt('', 'Enter the Module Name', function(btn, modValue) {
                        if (btn == 'ok') {
                            if (modValue == "") {
                                Ext.Msg.show({
                                    message: '<p>Please enter the module name</p>',
                                    buttons: Ext.MessageBox.OK,
                                    cls: 'msgbox'
                                });
                            } else {
                                mobEdu.main.f.updateModuleDescription(modValue, record);
                            }
                        }
                    }, this, false, value, {
                        maxlength: 100
                    });
                }
            }
            mobEdu.main.f.displayModules();
        },

        updateModuleDescription: function(modValue, record) {
            var store = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var rec = store.findRecord('code', record.get('code'));
            if (rec != null) {
                rec.set('description', modValue);
                store.sync();
                mobEdu.main.f.displayModules();
                mobEdu.main.f.updateSlidingMenu(store);
                mobEdu.main.f.updateFeedsStore(record.get('moduleId'), modValue);
            }
        },

        moveUp: function(index) {
            var store = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            if (index != 0) {
                var currentRec = store.getAt(index);
                var targetRec = store.getAt(index - 1);
                var currentOrder = currentRec.get('order');
                var targetOrder = targetRec.get('order');
                currentRec.set('order', targetOrder);
                targetRec.set('order', currentOrder);
                store.sync();
                Ext.getCmp('editModulesList').refresh();
                mobEdu.main.f.updateSlidingMenu(store);
            }

        },

        moveDown: function(index) {
            var store = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            if (index != store.data.length - 1) {
                var currentRec = store.getAt(index);
                var targetRec = store.getAt(index + 1);
                var currentOrder = currentRec.get('order');
                var targetOrder = targetRec.get('order');
                currentRec.set('order', targetOrder);
                targetRec.set('order', currentOrder);
                store.sync();
                Ext.getCmp('editModulesList').refresh();
                mobEdu.main.f.updateSlidingMenu(store);
            }
        },

        moduleOn: function(code) {
            var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var moduleRec = modulesLocalStore.findRecord('code', code);
            if (moduleRec != null) {
                mobEdu.util.removeModuleFromHiddenModules(code);
            }
            Ext.getCmp('editModulesList').refresh();
            mobEdu.main.f.updateSlidingMenu(modulesLocalStore);
        },

        moduleOff: function(code) {
            var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var moduleRec = modulesLocalStore.findRecord('code', code);
            if (moduleRec != null) {
                mobEdu.util.addModuleToHiddenModules(code);
            }
            Ext.getCmp('editModulesList').refresh();
            mobEdu.main.f.updateSlidingMenu(modulesLocalStore);
        },

        updateSlidingMenu: function(store) {
            var slidingStore = mobEdu.util.getStore('mobEdu.main.store.slidingMenu');
            if (slidingStore.data.length > 0) {
                slidingStore.removeAll();
                slidingStore.sync();
            }
            // Loading all the data used to throw errors for missing icons in apmenu folder
            // Now we load only MAIN position records
            var menuData = store.data.all;
            for (var i = 0; i <= menuData.length - 1; i++) {
                if (menuData[i].get('position') === 'MAIN') {
                    slidingStore.add(menuData[i]);
                }
            };

            if (mobEdu.util.getStudentId() != null && slidingStore.find('code', 'LOGOUT') == -1) {
                var moduleOrder = mobEdu.util.getStore('mobEdu.main.store.modulesLocal').max('order');
                slidingStore.add({
                    authRequired: false,
                    code: "LOGOUT",
                    description: "Logout",
                    icon: "logout.png",
                    order: moduleOrder + 1,
                    position: "MAIN",
                    show: true
                });
            }

            slidingStore.sync();
            slidingStore.filter(function(record) {
                if (!mobEdu.util.isModuleExists(record.get('code'))) {
                    return true;
                }
                return false;
            });
        },

        onSearchKeyUp: function(field) {
            var value = field.getValue();
            var store = mobEdu.util.getStore('mobEdu.main.store.slidingMenu');

            //first clear any current filters on the store. If there is a new value, then suppress the refresh event
            store.clearFilter(!!value);
            //check if a value is set first, as if it isnt we dont have to do anything
            if (value) {
                //the user could have entered spaces, so we must split them so we can loop through them all
                var searches = value.split(','),
                    regexps = [],
                    i, regex;

                //loop them all
                for (i = 0; i < searches.length; i++) {
                    //if it is nothing, continue
                    if (!searches[i]) continue;

                    regex = searches[i].trim();
                    regex = regex.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");

                    //if found, create a new regular expression which is case insenstive
                    regexps.push(new RegExp(regex.trim(), 'i'));
                }

                //now filter the store by passing a method
                //the passed method will be called for each record in the store
                store.filter(function(record) {
                    var matched = [];
                    if (!mobEdu.util.isModuleExists(record.get('code')) && record.get('code') != 'NOTIF') {
                        //loop through each of the regular expressions
                        for (i = 0; i < regexps.length; i++) {
                            var search = regexps[i],
                                didMatch = search.test(record.get('description'));
                            //if it matched the first or last name, push it into the matches array
                            matched.push(didMatch);
                        }
                    }

                    return (regexps.length && matched.indexOf(true) !== -1);
                });
            } else {

                store.removeAll();
                store.sync();
                mobEdu.main.f.updateSlidingMenu(mobEdu.util.getStore('mobEdu.main.store.modulesLocal'));
            }
        },

        updateFeedsStore: function(id, value) {
            var store = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
            var rec = store.findRecord('feedId', id);
            if (rec != null) {
                rec.set('name', value);
                store.sync();
            }
        },

        updateApMenuStore: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.slidingMenu');
            store.removeAt(0);
            store.sync();
        },

        onSearchClearIconTap: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.slidingMenu');
            //call the clearFilter method on the store instance
            store.clearFilter();
            store.filter(function(record) {
                if (!mobEdu.util.isModuleExists(record.get('code'))) {
                    return true;
                }
                return false;
            });
            // mobEdu.main.f.displayModules();
        },

        submitLoginForm: function() {
            var form_values = this.getFormValues();
            var username = form_values['name'];
            var password = form_values['password'];

            if (loginType === 'demo') {
                var role = Ext.getCmp('role').getValue();

                if (role === 'STUDENT') {
                    username = 'JAMES';
                    password = 'test123';
                } else if (role === 'FACULTY') {
                    username = 'DAVID';
                    password = 'test123';
                } else if (role === 'STUDENTFACULTY') {
                    username = 'JOHN';
                    password = 'test123';
                } else if (role === 'SUPERVISOR') {
                    username = 'MARK';
                    password = 'test123';
                } else if (role === 'RECRUITER') {
                    username = 'MARY';
                    password = 'test123';
                } else if (role === 'ADVISOR') {
                    username = 'DAVID';
                    password = 'test123';
                } else if (role === 'COUNSELLOR') {
                    username = 'TONY';
                    password = 'test123';
                }
            }

            var serviceText = 'login';
            var operationText = 'recruiterlogin';

            var cred = username + ':' + password;
            var encCred = Base64.encode(cred);
            var authString = 'Basic ' + encCred;

            if (username == '' || password == '') {
                var labelName = loginLabel + ' and ' + passwordLabel;
                var userName = 'Username and ' + passwordLabel;
                var message = passwordLabel != '' ? 'Please provide ' + (loginLabel != '' ? labelName : userName) : 'Please provide Username and Password';
                Ext.Msg.show({
                    title: 'Login error',
                    message: message,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {

                var store = mobEdu.util.getStore('mobEdu.main.store.login');
                var regId;
                var platform = null;
                if (Ext.os.is.Android) {
                    platform = "ANDROID";
                    // Pick up device id if app is running inside phogap
                    if (typeof window.CustomNativeAccess != 'undefined') {
                        regId = window.CustomNativeAccess.getGCMRegisterId();
                    }
                } else if (Ext.os.is.iOS) {
                    platform = "IOS";
                    regId = deviceToken;
                }

                store.getProxy().setHeaders({
                    Authorization: authString,
                    companyID: companyId,
                    // Platform: platform
                });
                store.getProxy().setUrl(loginserver + 'login');

                store.getProxy().setExtraParams({
                    regId: regId,
                    companyId: companyId
                });
                store.getProxy().afterRequest = function() {
                    mobEdu.main.f.loginResponseHandler(authString, username);
                }
                store.load();
                mobEdu.util.gaTrackEvent('enroll', 'login');
            }
        },

        resetLoginForm: function() {
            if (primaryModule == 'login') {
                Ext.getCmp('id').setValue('');
                Ext.getCmp('password').setValue('');
            } else {
                mobEdu.util.showMainView();
            }
        },

        loginResponseHandler: function(authString, username) {
            var store = mobEdu.util.getStore('mobEdu.main.store.login');
            var response = store.getProxy().getReader().rawData;
            var storeData = store.data.all;
            var status = response.status;
            if (status == null) {
                Ext.Msg.show({
                    message: 'Server error has occurred.<br> Please try again.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else if (status === "success") {
                mobEdu.util.updateIsLogin();
                if (settingsOnToolbar === 'true') {
                    Ext.getCmp('logBtn').setCls('settings');
                } else {
                    Ext.getCmp('logBtn').setCls('logout');

                }
                if (showLoginName == 'true')
                    Ext.getCmp('loginName').setHtml('<div class="loginTextWrapper">' + response.userInfo.firstName + '</div>');
                if (isSessionTimeoutEnabled == 'true') {
                    task = Ext.create('Ext.util.DelayedTask', function() {
                        mobEdu.util.loadTask();
                    }, this);
                }

                //loadModules
                if (response.privileges.length === 0) {
                    Ext.Msg.show({
                        message: 'No privileges defined for your role.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                    return;
                } else {
                    var hasModuleAccess = false;
                    var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
                    modulesLocalStore.clearFilter();

                    // By default we will show all modules
                    // Based on the ROLE privileges, we remove modules that are not returned
                    // in Login Response ROLE Privileges

                    // Do not remove directly in this loop.
                    // Collect all items to be removed and remove at once.
                    var mLength = modulesLocalStore.data.all.length;
                    var removeItems = new Array();
                    for (var mi = mLength - 1; mi >= 0; mi--) {

                        var localCode = modulesLocalStore.data.all[mi].data.code;
                        var localCodeFound = false;

                        for (var i = 0; i < response.privileges.length; i++) {
                            var type = response.privileges[i].type;
                            if (type == 'MODULE_ACCESS') {

                                if (response.privileges[i].name === localCode) {
                                    localCodeFound = true;
                                    if (mobEdu.util.getLoginModuleCode() == response.privileges[i].name) {
                                        hasModuleAccess = true;
                                    }
                                    break;
                                }
                            }
                        }
                        if (!localCodeFound) {
                            removeItems.push(localCode);
                        }
                    }

                    for (var mi = removeItems.length - 1; mi >= 0; mi--) {
                        var index = modulesLocalStore.findExact('code', removeItems[mi]);
                        var record = modulesLocalStore.getAt(index);
                        modulesLocalStore.remove(record);
                    };
                    modulesLocalStore.sync();
                }

                // Update the remaining modules auth required flag
                if (!(showBioInfo === 'true')) {
                    modulesLocalStore.each(function(item, index, length) {
                        item.set('authRequired', 'N');
                    });
                }
                if (mainMenuLayout == 'GRID') {
                    Ext.getCmp('logBtn').setHidden(false);
                }

                modulesLocalStore.sync();

                // Check proxy privileges now
                for (var i = 0; i < response.privileges.length; i++) {
                    if (response.privileges[i].type === 'PROXY_REG') {
                        mobEdu.util.updateProxyReg(response.privileges[i].value);
                        mobEdu.util.updateProxyAccessFlag(response.privileges[i].accessFlag);
                        break;
                    }
                }

                if (mobEdu.main.f.subMenuCategory != false) {
                    modulesLocalStore.filter('subCategory', mobEdu.main.f.category);
                } else {
                    modulesLocalStore.filter('category', mobEdu.main.f.category);
                }

                if (modulesLocalStore.data.all.length == 0) {
                    Ext.Msg.show({
                        message: 'You do not have access to any of the modules.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                    return;
                }

                mobEdu.main.f.hidePopup();
                if (!hasModuleAccess) {
                    mobEdu.util.toggleSlidingMenu();
                    mobEdu.util.updateNextView(null);
                }
                var studentId = response.userInfo.userName;
                var applicationId = response.userInfo.applicationId;
                var firstName = storeData[0].raw.userInfo.firstName;
                var lastName = storeData[0].raw.userInfo.lastName;
                var role = response.userInfo.roleCode;
                var imageUrl = response.userInfo.imageUrl;
                mobEdu.util.updateResourcePath(resourcesPath);
                mobEdu.util.loadClientFiles();
                if (primaryModule === 'login') {
                    mobEdu.main.f.loadViews();
                    mobEdu.main.f.getMainModules();
                }
                mobEdu.util.updateRole(role);
                mobEdu.util.updateImageUrl(imageUrl);
                mobEdu.util.updateApplicationId(applicationId);
                mobEdu.util.updateStudentId(studentId);
                mobEdu.util.updateStudentLdapId(studentId);
                mobEdu.util.updateAuthString(authString);
                mobEdu.util.updateFirstName(firstName);
                mobEdu.util.updateLastName(lastName);
                mobEdu.main.f.loadModulesList();
                if (showBioInfo === 'true') {
                    mobEdu.main.f.checkIfBiographicRequired(studentId, role);
                } else {
                    mobEdu.main.f.showNextView();
                }
                if (mobEdu.courses.f.isRegistrationAllowed() == true) {
                    mobEdu.reg.f.loadAllCartItems();
                }
                if (mainMenuLayout === 'AP') {
                    mobEdu.main.f.updateSlidingMenu(mobEdu.util.getStore('mobEdu.main.store.modulesLocal'));
                } else if (mainMenuLayout === 'GRID') {
                    mobEdu.main.f.displayGridMenu();
                } else if (mainMenuLayout === 'SGRID') {
                    mobEdu.main.f.displaySMenu();
                } else if (mainMenuLayout === 'BGRID') {
                    if (mobEdu.util.currentModule != 'NOTIF') {
                        if (mobEdu.util.getNextView() != null) {
                            if (mobEdu.main.f.subMenuCategory != false) {
                                mobEdu.main.f.loadSubMenu(mobEdu.main.f.category);
                            } else {
                                mobEdu.main.f.displayBevelGridMenu();
                            }
                        }
                    }
                }

                if (mobEdu.main.f.isNotificationEnabled()) {
                    //Load notifications for FirstTime
                    mobEdu.notif.f.loadNotifications(true);
                }

            } else if (status.match(/expire/gi)) {
                Ext.Msg.show({
                    title: 'Login error',
                    message: status,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                authString = '';
                Ext.Msg.show({
                    title: 'Login Error',
                    message: 'Invalid username or password.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        checkIfBiographicRequired: function(studentId, role) {
            var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
            if (store.data.length > 0) {
                store.removeAll();
                store.removed = [];
            }
            store.getProxy().setUrl(webserver + 'checkIfBiographicRequired');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId()
            });
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.checkIfBioRequiredResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'checkIfBiographicRequired');
        },

        checkIfBioRequiredResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.profile.store.directoryOpt');
            var storeData = store.data.all[0];
            if (storeData.data.indicator === 'Y') {
                mobEdu.profile.f.loadBioInfo();
                mobEdu.util.updateNextView('mobEdu.profile.f.showBioInfo');
                mobEdu.util.updateBioInfoCheck(true);
            } else {
                mobEdu.main.f.showNextView();
                mobEdu.util.updateBioInfoCheck(false);
            }
        },

        loadViews: function() {
            // Using the spinner from spin.js instead of default one
            // This is because when we load the themes on campus selection the loading mask gets ugly
            // Ext.select('#loading-mask').show();
            //            var spinnerTarget = document.getElementById('loading-mask');
            //            spinner = new Spinner(spinnerOpts).spin(spinnerTarget);
            //
            //            var fileref = document.createElement('script');
            //            fileref.setAttribute("type", "text/javascript");
            //            fileref.setAttribute("src", "app-views.js");
            //            fileref.onload = mobEdu.main.f.continueLoading;
            //            document.getElementsByTagName("head")[0].appendChild(fileref);
            mobEdu.main.f.continueLoading();
        },

        continueLoading: function() {

            // Once the views are loaded, stop the spinner
            // Ext.select('#loading-mask').hide();
            //            spinner.stop();
            if (showPremenu != null) {
                mobEdu.util.updatePrevView(null);
                mobEdu.util.show('mobEdu.main.view.preMenu');
            } else {
                mobEdu.util.showMainView();
            }
            mobEdu.main.f.initializeMainView();

            if (typeof navigator.splashscreen != "undefined") {
                navigator.splashscreen.hide();
            }

            if (primaryModule == 'login') {
                mobEdu.reg.f.loadAllCartItems();
            }
        },

        loadModulesList: function() {

            var modulesList = mobEdu.util.getModules();
            if (modulesList != null) {
                var modules = modulesList.split(',');
                for (var j = 0; j < modules.length; j++) {
                    var module = document.getElementById(modules[j]);
                    if (module != null) {
                        module.style.display = "";
                    }
                }
            }
        },

        getFormValues: function() {
            return (mobEdu.util.get('mobEdu.main.view.login').getValues());
        },

        showNextView: function() {
            if (mobEdu.util.getNextView() == null) {
                if (showPremenu == 'always' && mainMenuLayout == 'BGRID') {
                    if (mobEdu.main.f.category == 'FAVS') {
                        mobEdu.main.f.loadUserFavourites();
                    } else {
                        mobEdu.util.isHomeTapped = true;
                        mobEdu.util.showMainView();
                    }
                } else
                    mobEdu.util.showMainView();
            } else {
                if (mobEdu.main.f.category == 'FAVS') {
                    mobEdu.main.f.loadUserFavourites();
                } else {
                    (mobEdu.util.getNextView())();
                }
            }
        },

        isNotificationEnabled: function() {
            // With the new sliding menu notifications are static in the menu
            // There wont be a NOTIF in the modules.
            return true;
        },

        onUsernameKeyup: function(view, e, eOpts) {
            if (e.browserEvent.keyCode == 13) {
                Ext.getCmp('password').focus();
            }
        },

        onPasswordKeyup: function(view, e, eOpts) {
            if (e.browserEvent.keyCode == 13) {
                mobEdu.main.f.submitLoginForm();
            }
        },

        showLogin: function(popupView) {
            mobEdu.main.f.prevView = mobEdu.util.getPrevView();
            mobEdu.util.updatePrevView(mobEdu.main.f.hidePopup);
            if (!popupView) {
                var popup = popupView = Ext.Viewport.add(
                    mobEdu.util.get('mobEdu.main.view.login'));
                Ext.Viewport.add(popupView);
            }
            mobEdu.util.onOrientationChange(Ext.Viewport.getOrientation(), popupView, mobEdu.main.view.login);
            popupView.show();
        },
        hidePopup: function() {
            mobEdu.util.updatePrevView(mobEdu.main.f.prevView);
            var popUp = Ext.getCmp('loginPopup');
            popUp.hide();
            Ext.Viewport.unmask();
        },

        showEditModuleView: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            var store = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            store.clearFilter();
            store.filter(function(record) {
                if (record.get('position') == 'MAIN' && record.get('category') != '') {
                    return true;
                }
                return false;
            });
            mobEdu.util.show('mobEdu.main.view.modules');
        },

        showExtLinkView: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.main.view.extLink');
        },
        showPreferences: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.main.view.preferences');
        },
        showStudentFeedback: function(from) {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.main.view.studentFeedback');
            if (mainMenuLayout === 'AP') {
                mobEdu.util.get('mobEdu.main.view.studentFeedback').reset();
            }
            var store = mobEdu.util.getStore('mobEdu.main.store.recipientsList');
            if (store.data.length > 0) {
                var index = 1;
                var email = '';
                store.each(function(record) {
                    email = record.get('email');
                    email = email + index;
                    record.set('email', email);
                    index++;
                });
            }
            Ext.getCmp('recipients').setStore(store);
            if (from != null)
                mobEdu.util.setTitle('HFEEDBACK', Ext.getCmp('feedbackTitle'));
            else
                mobEdu.util.setTitle('FEEDBACK', Ext.getCmp('feedbackTitle'));
        },
        showTermList: function() {
            mobEdu.util.updatePrevView(mobEdu.main.f.showPreviousView);
            mobEdu.util.show('mobEdu.main.view.phone.termList');
        },
        showCampus: function() {
            if (mobEdu.util.getResourcePath() == resourcesPath) {
                mobEdu.util.updatePrevView(null);
            } else {
                mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            }
            if (typeof navigator.splashscreen != "undefined") {
                navigator.splashscreen.hide();
            }
            mobEdu.util.show('mobEdu.main.view.campus');
        },

        showSlidingMenu: function() {
            // Show the sliding menu for 10 secs when the app loads
            if (mainMenuLayout === "AP") {
                mobEdu.util.toggleSlidingMenu();
                Ext.Function.defer(function() {
                    if (mobEdu.util.isSlideMenuOpen === true) {
                        mobEdu.util.toggleSlidingMenu();
                    }
                }, 10000);
            }
        },
        showStudentDetailsView: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.main.view.studentDetailsChecking');
        },

        onZipKeyup: function(zipfield) {
            var value = (zipfield.getValue()).toString() + '';
            value = value.replace(/\D/g, '');
            zipfield.setValue(value);
            var length = value.length;
            if (length > 5) {
                zipfield.setValue(value.substring(0, 5));
                return false;
            }
            return true;
        },
        onPhoneKeyup: function(phone1Field) {
            var phoneNumber = (phone1Field.getValue()).toString() + '';
            phoneNumber = phoneNumber.replace(/\D/g, '');
            phone1Field.setValue(phoneNumber);
            var length = phoneNumber.length;
            if (length > 10) {
                phone1Field.setValue(phoneNumber.substring(0, 10));
                return false;
            }
            return true;
        },

        getStudentFormValues: function() {
            return (mobEdu.util.get('mobEdu.main.view.studentDetailsChecking').getValues());
        },

        displayPreMenu: function() {
            mobEdu.util.updatePrevView(null);
            var gridLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            if (doCategoryCheck == 'true') {
                gridLocalStore.clearFilter(true);
            }
            mobEdu.main.f.loadPreMenu();
            mobEdu.util.show('mobEdu.main.view.preMenu');
        },
        loadPreMenu: function() {
            mobEdu.util.get('mobEdu.main.view.preMenu');
            if (settingsOnToolbar === 'true') {
                Ext.getCmp('logBtn').setCls('settings');
            } else {
                Ext.getCmp('logBtn').setCls(mobEdu.util.getStudentId() == null && mobEdu.util.getAuthString() == null ? 'login' : 'logout');

            }
            var iHeight = window.screen.height;
            if (Ext.os.deviceType === 'Phone') {
                if (iHeight == 568) {
                    menuIconSize = 60;
                } else {
                    menuIconSize = 55;
                }
            } else {
                menuIconSize = 110;
            }
            var gridLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            gridLocalStore.clearFilter();
            var maxIconsPerRow = 3;
            if (Ext.os.is.Android) {
                if (Ext.os.version.Major != undefined) {
                    if (!(Ext.os.version.Major > 4)) {
                        if (!(Ext.os.version.Minor > 3)) {
                            var viewportWidth = screen.width;
                            var viewportHeight = screen.height;
                        } else {
                            var viewportWidth = Ext.Viewport.getSize().width;
                            var viewportHeight = Ext.Viewport.getSize().height;
                        }
                    } else {
                        var viewportWidth = Ext.Viewport.getSize().width;
                        var viewportHeight = Ext.Viewport.getSize().height;
                    }
                } else {
                    var iconWidth = Math.round((Ext.getCmp('preModules').element.dom.scrollWidth * 80 / 100) / maxIconsPerRow);
                }
            } else {
                var viewportWidth = Ext.Viewport.getSize().width;
                var viewportHeight = Ext.Viewport.getSize().height;
            }
            if (viewportWidth != undefined) {
                // if (viewportWidth > 600) {
                //     var iconWidth = Math.round((viewportWidth * (Ext.os.is.Phone ? 80 : 84) / 100) / maxIconsPerRow) - menuIconSize - 36;
                // } else {
                var iconWidth = Math.round((viewportWidth * (Ext.os.is.Phone ? 80 : 84) / 100) / maxIconsPerRow) - menuIconSize;
                // }
            }
            if (mobEdu.main.f.iconWidthValue == null)
                mobEdu.main.f.iconWidthValue = iconWidth;
            // if (viewportWidth > 450) {
            //  maxIconsPerRow = 4;
            // }
            // var iconWidth = Math.round((viewportWidth * 84 / 100) / maxIconsPerRow);
            if (Ext.os.version.Minor == undefined && Ext.os.is.Android) {
                var iconWidthStyle = 'style="width:' + mobEdu.main.f.iconWidthValue + 'px;"';
            } else {
                var iconWidthStyle = 'style="width:' + iconWidth + 'px;"';
            }
            var mainMenuIconCount = 100;
            var path = 'images/menu/';
            var gridMenuIconImageWidth = Ext.os.is.Phone ? menuIconSize : mobEdu.util.getIconSize();
            if (gridLocalStore.data.length > 0) {
                var temp = '';
                gridLocalStore.each(function(record) {
                    if (!mobEdu.util.isModuleExists(record.get('code')))
                        var code = record.get('code');
                    var position = record.get('position');
                    var category = record.get('category');
                    if (position.toUpperCase() == 'MAIN' && category == '') {
                        mainMenuIconCount += 1;
                        temp = temp + '<div class="icons" ' + iconWidthStyle + '><a href="javascript:mobEdu.util.doLoginCheck(' + record.get('authRequired') + ",'mobEdu.main.f.displayModules','" + record.get('code') + "','" + record.get('icon') + "','" + mainMenuIconCount + "','" + record.get('category') + '\');"><img style="" id=' + mainMenuIconCount + ' height=' + gridMenuIconImageWidth + 'px" width="' + gridMenuIconImageWidth + 'px" src="' + mobEdu.util.getResourcePath() + path + record.get('icon') + '" ></a><div style="height: 20px;padding-top: 3px">' + record.get('description') + '</div></div>'
                    }
                });
                for (i = 0; i < maxIconsPerRow - (((mainMenuIconCount / 100) % maxIconsPerRow) - 1); i++) {
                    temp += '<div class="icons" ' + iconWidthStyle + '>&nbsp;</div>';
                }
                temp = '<div align=center>' + temp + "</div>";
                if (Ext.getCmp('preModules') != null) {
                    Ext.getCmp('preModules').setHtml(temp);
                }
            }

            var width = Ext.os.is.Phone ? 150 : 300;
            var height = Ext.os.is.Phone ? 20 : 40;
            Ext.getCmp('mainTopPreTb').setTitle('<img src="' + mobEdu.util.getResourcePath() + 'images/wentworth-full.png" height="' + height + 'px" width="' + width + 'px"  />');
        },

        cntusTap: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.main.view.contactUS');
            mobEdu.util.setTitle('CNTUS', Ext.getCmp('cntUsTitle'));
        },

        loadCntFeedback: function() {
            var fNameField = Ext.getCmp('cntFullName');
            var fNameValue = fNameField.getValue();
            var cEmail = Ext.getCmp('cntEmail');
            var cEmailValue = cEmail.getValue();
            var vReason = Ext.getCmp('cntComments');
            var reason = vReason.getValue();
            if (fNameValue != null && cEmailValue != null && reason != null && fNameValue != '' && cEmailValue != '' && reason != '') {
                var firstName = Ext.getCmp('cntFullName');
                var regExp = new RegExp("^[a-zA-Z ]+$");
                if (!regExp.test(fNameValue)) {
                    Ext.Msg.show({
                        id: 'firstNmae',
                        name: 'firstNmae',
                        cls: 'msgbox',
                        title: null,
                        message: '<p>Invalid full name.</p>',
                        buttons: Ext.MessageBox.OK,
                        fn: function(btn) {
                            if (btn == 'ok') {
                                firstName.focus(true);
                            }
                        }
                    });
                } else {
                    var mail = Ext.getCmp('cntEmail');
                    var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
                    if (regMail.test(cEmailValue) == false) {
                        Ext.Msg.show({
                            id: 'email',
                            name: 'email',
                            title: null,
                            cls: 'msgbox',
                            message: '<p>Invalid e-mail address.</p>',
                            buttons: Ext.MessageBox.OK,
                            fn: function(btn) {
                                if (btn == 'ok') {
                                    mail.focus(true);
                                }
                            }
                        });
                    } else {
                        if (fNameValue.charAt(0) == ' ') {
                            Ext.Msg.show({
                                id: 'fullname',
                                name: 'comment',
                                title: null,
                                cls: 'msgbox',
                                message: '<p>Name first letter must be alphanumeric.</p>',
                                buttons: Ext.MessageBox.OK,
                                fn: function(btn) {
                                    if (btn == 'ok') {
                                        fNameField.focus(true);
                                        fNameField.setValue('');
                                    }
                                }
                            });
                        } else {
                            if (reason.charAt(0) == ' ') {
                                Ext.Msg.show({
                                    id: 'comment',
                                    name: 'comment',
                                    title: null,
                                    cls: 'msgbox',
                                    message: '<p>Comment first letter must be alphanumeric.</p>',
                                    buttons: Ext.MessageBox.OK,
                                    fn: function(btn) {
                                        if (btn == 'ok') {
                                            vReason.focus(true);
                                            vReason.setValue('');
                                        }
                                    }
                                });
                            } else {
                                mobEdu.main.f.sentCntFeedback();
                            }
                        }
                    }
                }
            } else {
                Ext.Msg.show({
                    message: '<p>Please provide all mandatory fields.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        sentCntFeedback: function() {
            var fNameField = Ext.getCmp('cntFullName');
            var fNameValue = fNameField.getValue();
            var cEmail = Ext.getCmp('cntEmail');
            var cEmailValue = cEmail.getValue();
            var vReason = Ext.getCmp('cntComments');
            var reason = vReason.getValue();
            if (includeRecipientDetails === 'true') {
                reason = reason + '<br><br>' + fNameValue + '<br>' + cEmailValue;
            }
            var fbSubject = 'Feedback';
            var feedbackStore = mobEdu.util.getStore('mobEdu.main.store.feedback');
            feedbackStore.getProxy().setUrl(webserver + 'submitFeedback');
            if (mobEdu.util.getAuthString() != null) {
                feedbackStore.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
            } else {
                feedbackStore.getProxy().setHeaders({
                    companyID: companyId
                });
            }
            feedbackStore.getProxy().setExtraParams({
                fromAddress: cEmailValue,
                fromName: fNameValue,
                recipients: 'info@n2nservices.com',
                subject: fbSubject,
                comments: reason,
                comments2: '',
                courseId: '',
                confidential: '',
                companyId: companyId
            });
            feedbackStore.getProxy().afterRequest = function() {
                mobEdu.main.f.sentCntFeedbackResponseHandler();
            };
            feedbackStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'feedback');
        },
        sentCntFeedbackResponseHandler: function() {
            var feedbackStore = mobEdu.util.getStore('mobEdu.main.store.feedback');
            var response = feedbackStore.getProxy().getReader().rawData;
            if (response == "SUCCESS") {
                Ext.Msg.show({
                    id: 'svsubmitsuccess',
                    title: null,
                    cls: 'msgbox',
                    message: '<center>We will contact you shortly.Thank You.</center>',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            mobEdu.main.f.resetstudentFeedback();
                            mobEdu.util.showMainView();
                        }
                    }
                });
            } else {
                Ext.Msg.show({
                    message: response,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        loadUserFavourites: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.favourites');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setUrl(commonwebserver + 'userFavourites');
            store.getProxy().setExtraParams({
                userID: mobEdu.util.getApplicationId()
            });
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.favsResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('encore', 'userFavourites');
        },

        favsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.favourites');
            var modulesStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            if (doCategoryCheck) {
                modulesStore.clearFilter();
            }
            store.each(function(record) {
                var rec = modulesStore.findRecord('moduleId', record.get('moduleId'));
                if (rec != null) {
                    rec.set('select', 'Y');
                }
            });
            // var rec = modulesStore.findRecord('code', 'SETTINGS');
            // rec.set('select', 'Y');
            modulesStore.sync();
            modulesStore.filter('select', 'Y');
            mobEdu.main.f.displayModules();
        },

        updateFavourites: function() {
            var favsStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
            var favourites = '';
            favsStore.each(function(record) {
                if (record.get('select') === 'Y') {
                    if (favourites === '') {
                        favourites = favourites + record.get('moduleId');
                    } else {
                        favourites = favourites + ',' + record.get('moduleId');
                    }
                }
            })
            mobEdu.main.f.updateUserFavourites(favourites);
        },

        updateUserFavourites: function(favourites) {
            var store = mobEdu.util.getStore('mobEdu.main.store.feedback');

            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                userID: mobEdu.util.getApplicationId(),
                favourites: favourites
            });
            store.getProxy().setUrl(commonwebserver + 'updateFavourites');
            store.getProxy().afterRequest = function() {
                mobEdu.main.f.updateFavouritesResponseHandler();
            }
            store.load();
            mobEdu.util.gaTrackEvent('commons', 'updateFavourites');
        },

        updateFavouritesResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.main.store.feedback');
            var storeData = store.data.all[0];
            var status = storeData.raw.status;
            if (status.match(/success/gi)) {
                Ext.Msg.show({
                    message: "Favourites are updated successfully.",
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox',
                    fn: function(btn) {
                        mobEdu.util.showMainView();
                    }
                });
            } else {
                Ext.Msg.show({
                    message: status,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        isFavourite: function(select) {
            if (select === 'Y') {
                return true;
            } else {
                return 'false';
            }
        },

        onModuleSelection: function(view, index, target, record, item, e, eOpts) {
            if (item.target.name == 'off') {
                record.set('select', 'Y');
                item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
                item.target.name == "on";
            } else {
                record.set('select', 'N');
                item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
                item.target.name == "off";
            }

        },

        onPreMenuItemTap: function() {

            mobEdu.main.f.displayModules();
        }


    },
});