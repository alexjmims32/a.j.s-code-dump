Ext.define('mobEdu.main.model.encore', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'hiddenFeeds',
                type: 'array',
                mapping:'hiddenFeeds'
            },{
                name: 'hiddenModules',
                type: 'array',
                mapping:'hiddenModules'
            }
        ]
    }
});