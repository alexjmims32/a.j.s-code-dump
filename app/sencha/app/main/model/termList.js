Ext.define('mobEdu.main.model.termList', {
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'code',
            type : 'string'
        },{
            name : 'description',
            type : 'string'
        }]
    }
});
