Ext.define('mobEdu.main.model.term',{
    extend:'Ext.data.Model',
    
    config:{
        fields:[
            {
                name : 'termCode',
                type : 'string'
            },{
                name : 'description',
                type : 'string'
            },{
                name : 'errMsg',
                type : 'string'
            }
        ]
    }
})