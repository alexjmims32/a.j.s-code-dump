Ext.define('mobEdu.main.model.proxySession', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:"status",
                type:'string'
            },{
                name:"studentId",
                type:'string'
            }
        ]}
});