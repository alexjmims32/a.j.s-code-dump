Ext.define('mobEdu.main.model.modules', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'moduleId',
            mapping: 'moduleId'
        }, {
            name: 'code'
        }, {
            name: 'select'
        }, {
            name: 'order',
            type: 'int'
        }, {
            name: 'description'
        }, {
            name: 'icon'
        }, {
            name: 'position'
        }, {
            name: 'category'
        }, {
            name: 'subCategory'
        }, {
            name: 'authRequired',
            convert: function(value, record) {
                if (value == 'Y' || value == true) {
                    return true;
                } else {
                    return false;
                }
            }
        }, {
            name: 'show',
            defaultValue: true
        }, {
            name: 'action',
            convert: function(value, record) {
                var val = record.data.code;
                var authValue = record.data.authRequired;
                if (val == 'ATHLETICS') {
                    return 'mobEdu.sports.f.loadSports';
                } else if (val == 'MAPS') {
                    return 'mobEdu.maps.f.loadMaps';
                } else if (val == 'COURSES') {
                    return 'mobEdu.courses.f.preLoadMyCourses';
                } else if (val == 'DIRECTORY') {
                    return 'mobEdu.directory.f.loadDirectory';
                } else if (val == 'MYINF') {
                    return 'mobEdu.profile.f.loadMyInfo';
                } else if (val == 'CCINF') {
                    return 'mobEdu.crl.f.loadCurriculumInfo';
                } else if (val == 'CHINF') {
                    return 'mobEdu.crl.f.loadBillingInfo';
                } else if (val == 'STINF') {
                    return 'mobEdu.profile.f.loadStudentInfo';
                } else if (val == 'DROPT') {
                    return 'mobEdu.profile.f.loadDirOptOut';
                } else if (val == 'FAID') {
                    return 'mobEdu.finaid.f.loadFinaidDropdown';
                } else if (val == 'REG') {
                    return 'mobEdu.reg.f.preLoadRegistration';
                } else if (val == 'COUNSELOR') {
                    return 'mobEdu.counselor.f.loadCounselorDetails';
                } else if (val == 'CATLOGUE') {
                    return 'mobEdu.catlogue.f.loadCrseCatlog';
                } else if (val == 'HELP') {
                    return 'mobEdu.help.f.initializeHelpView';
                } else if (val == 'PREFS') {
                    return 'mobEdu.settings.f.loadPreferences';
                } else if (val == 'CUFAV') {
                    return 'mobEdu.settings.f.loadEditModuleView';
                } else if (val == 'FEEDBACK') {
                    return 'mobEdu.main.f.loadRecipientList';
                } else if (val == 'HFEEDBACK') {
                    return 'mobEdu.housing.f.loadRecipientList';
                } else if (val == 'ENTER') {
                    return 'mobEdu.enter.f.loadMainMenu';
                } else if (val == 'ENROUTE') {
                    return 'mobEdu.enroute.main.f.showEnrouteMainview';
                } else if (val == 'EMER') {
                    return 'mobEdu.directory.f.loadEmergencyContacts';
                } else if (val == 'IMPNUM') {
                    return 'mobEdu.directory.f.loadImportantNumbers';
                } else if (val == 'HISTORY') {
                    return 'mobEdu.hist.f.showStudentHistoryView';
                } else if (val == 'NOTIF') {
                    return 'mobEdu.notif.f.loadNotifPopup';
                } else if (val == 'SCHOOLS') {
                    return 'mobEdu.main.f.loadSchools';
                } else if (val == 'MCLST') {
                    return 'mobEdu.people.f.loadTermList';
                } else if (val == 'MFACS') {
                    return 'mobEdu.people.f.loadTermListForFaculty';
                } else if (val == 'MADVS') {
                    return 'mobEdu.people.f.loadAdvisors';
                } else if (val == 'CUSTSERV') {
                    return 'mobEdu.custserv.f.loadAreas';
                } else if (val == 'ENRADMS') {
                    return 'mobEdu.util.showAdmissionMainView';
                } else if (val == 'EDITMOD') {
                    return 'mobEdu.main.f.showEditModuleView';
                } else if (val == 'ATTD') {
                    return 'mobEdu.att.f.loadAttdMenu';
                } else if (val == 'ATDCE') {
                    return 'mobEdu.att.f.checkRolePriorToGetCurrentClass';
                } else if (val == 'ATT') {
                    return 'mobEdu.att.f.getCurrentClass';
                } else if (val == 'ADVISING') {
                    return 'mobEdu.advising.f.showMenu';
                } else if (val == 'LOGOUT') {
                    return 'mobEdu.main.f.logout';
                } else if (val == 'MSG') {
                    return 'mobEdu.messages.f.loadFromMyMessageModule';
                } else if (val == 'CALENDAR') {
                    return 'mobEdu.appointments.f.loadFromCalendarModule';
                } else if (val == 'ENQUIRE') {
                    return 'mobEdu.util.doLeadLoginCheck';
                } else if (val == 'CNTUS') {
                    return 'mobEdu.main.f.cntusTap';
                } else if (val == 'REPORT') {
                    return 'mobEdu.report.f.loadReportsModule';
                } else if (val == 'FAQS') {
                    return 'mobEdu.help.f.loadFAQs';
                } else if (val == 'SHI') {
                    return 'mobEdu.housing.f.loadHousingInfo';
                } else if (val == 'SI') {
                    return 'mobEdu.housing.f.loadBuildingsDropdown';
                } else if (val == 'FORMS') {
                    return 'mobEdu.housing.f.loadHousingForms';
                } else if (val == 'BCSLR') {
                    return 'mobEdu.acc.f.loadBillingCounselorDetails';
                } else if (val == 'UNOFT') {
                    return 'mobEdu.transcripts.f.loadTranscripts';
                } else if (val == 'HOLDS') {
                    return 'mobEdu.crl.f.loadAllHolds';
                } else if (val == 'ACCBT') {
                    return 'mobEdu.acc.f.loadSummaryListByTerm';
                } else if (val == 'ACCAT') {
                    return 'mobEdu.acc.f.loadSummaryList';
                } else if (val == 'EBILL') {
                    return 'mobEdu.acc.f.loadEbillTermList';
                } else {
                    return 'mobEdu.feeds.f.loadFeeds';
                }
            }
        }]
    }
});