Ext.define("mobEdu.main.model.studentInformation", {
    extend:'Ext.data.Model',

    config:{
        fields : [
            {
                name : 'studentHandBook',
                type : 'string'
            },
            {
                name : 'providedLocalAddress',
                type : 'string'
            },
            {
                name : 'providedEmrInfo',
                type : 'string'
            },
            {
                name : 'verifyEmail',
                type : 'string'
            },
            {
                name : 'verifyMobile',
                type : 'string'
            },
            {
                name : 'verifyLAdd1',
                type : 'string'
            },
            {
                name : 'verifyLAdd2',
                type : 'string'
            },
            {
                name : 'verifyLCity',
                type : 'string'
            },
            {
                name : 'verifyLState',
                type : 'string'
            },
            {
                name : 'verifyLZip',
                type : 'string'
            },
            {
                name : 'verifyHomeAdd1',
                type : 'string'
            },
            {
                name : 'verifyHomeAdd2',
                type : 'string'
            },
            {
                name : 'verifyHomeCity',
                type : 'string'
            },
            {
                name : 'verifyHomeState',
                type : 'string'
            },
            {
                name : 'verifyHomeZip',
                type : 'string'
            },
            {
                name : 'verifyEmrEmail',
                type : 'string'
            },
            {
                name : 'verifyEmrMobile',
                type : 'string'
            },{
                name:'verifyEmrName',
                type:'string'
            },{
                name:'verifyEmrRel',
                type:'string'
            }

        ]
    }
});