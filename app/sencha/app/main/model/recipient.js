Ext.define('mobEdu.main.model.recipient', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'id',
            mapping: 'feedbckId',
            type: 'string'
        }, {
            name: 'title',
            type: 'string'
        }, {
            name: 'email',
            type: 'string'
        }, {
            name: 'status'
        }, {
            name: 'category'
        }]
    }
})