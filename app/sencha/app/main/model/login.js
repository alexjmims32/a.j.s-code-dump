Ext.define('mobEdu.main.model.login', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: "status",
            type: 'string'
        }, {
            name: "applicationId",
            type: 'string'
        }, {
            name: 'role',
            type: 'string'
        }, {
            name: 'userID',
            type: 'string'
        }, {
            name: 'email',
            type: 'string'
        }, {
            name: 'firstName'
        }, {
            name: 'lastName'
        }, {
            name: 'imageUrl',
            type: 'string'
        }]
    }
});