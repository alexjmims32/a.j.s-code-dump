Ext.define('mobEdu.main.model.config', {
    extend:'Ext.data.Model',

    config:{
        fields:[
        {
            name:'news'
        },
        {
            name:'events'
        },
        {
            name:'videos'
        }
        ]
    }
});