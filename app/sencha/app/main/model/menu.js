Ext.define('mobEdu.main.model.menu', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'img'
            },
            {
                name:'title'
            },
            {
                name:'action',
                type: 'function'
            }
        ]
    }
});