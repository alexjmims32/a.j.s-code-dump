Ext.define('mobEdu.acc.f', {
    requires: [
        'mobEdu.acc.store.details',
        'mobEdu.acc.store.localSummary',
        'mobEdu.acc.store.menu',
        'mobEdu.acc.store.paymentCard',
        'mobEdu.acc.store.payment',
        'mobEdu.acc.store.summary',
        'mobEdu.acc.store.summaryDetails',
        'mobEdu.acc.store.summaryList',
        'mobEdu.acc.store.termList',
        'mobEdu.acc.store.ebillSummary',
        'mobEdu.acc.store.bcDetails'
    ],
    statics: {
        accountView: null,
        loadAccMenu: function(code, nextViewRef) {
            mobEdu.finaid.f.showFinMenu = null;
            var store = mobEdu.util.getStore('mobEdu.acc.store.menu');
            store.load();
            store.removeAll();
            store.removed = [];
            store.getProxy().clear();
            store.sync();
            if (accountMenu.indexOf('ebill') != -1) {
                store.add({
                    img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                    title: 'E-Statement By Term',
                    action: mobEdu.acc.f.loadEbillTermList
                });
                store.sync();
            }
            if (accountMenu.indexOf('history') != -1) {
                store.add({
                    img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                    title: 'Transaction History by Term',
                    action: mobEdu.acc.f.loadSummaryListByTerm
                });
                store.sync();
                store.add({
                    img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                    title: 'Transaction History - All Terms',
                    action: mobEdu.acc.f.loadSummaryList
                });
                if (campusCode == 'UNG') {
                    store.add({
                        img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                        title: ' Nighthawks card ',
                        action: mobEdu.acc.f.loadNighthawks
                    });
                }
                store.sync();
            }
            mobEdu.acc.f.showAccMenu(nextViewRef);

        },
        loadFinMenu: function(code, nextViewRef) {
            var store = mobEdu.util.getStore('mobEdu.acc.store.menu');
            store.load();
            store.removeAll();
            store.removed = [];
            store.getProxy().clear();
            store.sync();
            if (accountMenu.indexOf('ebill') != -1) {
                store.add({
                    img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                    title: 'E-Statement By Term',
                    action: mobEdu.acc.f.loadEbillTermList
                });
                store.sync();
            }
            if (accountMenu.indexOf('history') != -1) {
                store.add({
                    img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                    title: 'Transaction History by Term',
                    action: mobEdu.acc.f.loadSummaryListByTerm
                });
                store.sync();
                store.add({
                    img: mobEdu.util.getResourcePath() + 'images/menu/summary.png',
                    title: 'Transaction History - All Terms',
                    action: mobEdu.acc.f.loadSummaryList
                });
                store.sync();
            }
            mobEdu.acc.f.showAccMenu(nextViewRef);

        },
        loadSummaryList: function() {
            mobEdu.util.updateSummaryFlag('list');
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            if (store.getCount() > 0) {
                store.removeAll();
                store.removed = [];
            }
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setUrl(webserver + 'getSummaryList');
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.acc.f.summaryListResponseHandler();
            }
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'summarylist');
        },
        summaryListResponseHandler: function() {
            mobEdu.acc.f.getTotalAmounts();
            if (!(Ext.os.is.Phone)) {
                mobEdu.util.get("mobEdu.acc.view.tablet.summary");
                mobEdu.acc.f.getSummaryDetails("Charges");
                Ext.getCmp("summaryList").select(0);
                Ext.getCmp('summaryT').setTitle('<h1>Summary</h1>');
                Ext.getCmp('Tsum').hide();
            } else {
                mobEdu.util.get("mobEdu.acc.view.phone.summary");
                Ext.getCmp('summaryP').setTitle('<h1>Summary</h1>');
                Ext.getCmp('Psum').hide();
            }
            mobEdu.acc.f.showSummary();
        },
        loadSummaryListByTerm: function() {
            if (!(mobEdu.util.checkInfo('accTermCode'))) {
                mobEdu.util.loadAccCurrentTerm();
            }
            mobEdu.util.updateSummaryFlag('term');
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            if (store.getCount() > 0) {
                store.removeAll();
                store.removed = [];
            }
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setUrl(webserver + 'getSummaryListByTerm');
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                termCode: mobEdu.util.getAccTermCode(),
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.acc.f.summaryListByTermResponseHandler();
            }
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'summarylistbyterm');
        },
        summaryListByTermResponseHandler: function() {
            mobEdu.acc.f.getTotalAmounts();
            if (!(Ext.os.is.Phone)) {
                mobEdu.util.get("mobEdu.acc.view.tablet.summary");
                mobEdu.acc.f.getSummaryDetails("Charges");
                Ext.getCmp("summaryList").select(0);
                Ext.getCmp('summaryT').setTitle('<h1>' + mobEdu.util.getAccTermDescription() + '</h1>');
                Ext.getCmp('tabSumtBar').show();
                Ext.getCmp('Tsum').show();
            } else {
                mobEdu.util.get("mobEdu.acc.view.phone.summary");
                Ext.getCmp('summaryP').setTitle('<h1>' + mobEdu.util.getAccTermDescription() + '</h1>');
                Ext.getCmp('phSumtBar').show();
                Ext.getCmp('Psum').show();
            }
            mobEdu.acc.f.showSummary();
        },
        onAccSummaryItemTap: function(viewRef, selectedItemIndex, target, item, e, o) {
            if (Ext.os.is.Phone)
                mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
            var title = item.data.title;
            //            if (e.target.name == "show") {
            //                mobEdu.acc.f.showMakePayment();
            //            } else {
            this.getSummaryDetails(title);
            //}
        },
        getSummaryDetails: function(title) {
            // var payBtn = document.getElementById('show');
            if (title == "Charges") {
                //                if (!(Ext.os.is.Phone))
                //                    payBtn.style.display = "none";
                mobEdu.acc.f.getChargesInfo();
            } else if (title == "Payments") {
                //                if (!(Ext.os.is.Phone))
                //                    payBtn.style.display = "none";
                mobEdu.acc.f.getPaymentsInfo();
            } else if (title == "Due") {
                mobEdu.acc.f.getDueInfo();
            }
        },
        getPaymentsInfo: function() {
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            var sumStore = mobEdu.util.getStore('mobEdu.acc.store.details');
            sumStore.load();
            sumStore.removeAll();
            sumStore.sync();
            sumStore.removed = [];
            var list = store.data.items;
            for (var i = 0; i < list.length; i++) {
                var payments = list[i].data.payments;
                if (payments != null) {
                    for (var j = 0; j < payments.length; j++) {
                        sumStore.add({
                            description: payments[j].description,
                            amount: payments[j].amount,
                            category: payments[j].category
                        });
                        sumStore.sync();
                    }
                }
            }
            if (Ext.os.is.Phone)
                mobEdu.acc.f.showPayment();
            else {
                Ext.getCmp('detailsList').setEmptyText('<center><h3>No Payments</h3><center>');
                Ext.getCmp('detailsList').refresh();
            }
        },
        getChargesInfo: function() {
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            var sumStore = mobEdu.util.getStore('mobEdu.acc.store.details');
            sumStore.removeAll();
            sumStore.sync();
            sumStore.removed = [];
            var list = store.data.items;
            for (var i = 0; i < list.length; i++) {
                var charges = list[i].data.charges;
                if (charges != null) {
                    for (var j = 0; j < charges.length; j++) {
                        sumStore.add({
                            description: charges[j].description,
                            amount: charges[j].amount,
                            category: charges[j].category
                        });
                        sumStore.sync();
                    }
                }
            }
            if (Ext.os.is.Phone)
                mobEdu.acc.f.showCharges();
            else {
                Ext.getCmp('detailsList').setEmptyText('<center><h3>No Charges</h3><center>');
                Ext.getCmp('detailsList').refresh();
            }
        },
        getDueInfo: function() {
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            var sumStore = mobEdu.util.getStore('mobEdu.acc.store.details');
            sumStore.load();
            sumStore.removeAll();
            sumStore.sync();
            sumStore.removed = [];
            var list = store.data.items;
            for (var i = 0; i < list.length; i++) {
                var due = list[i].data.due;
                if (due != null) {
                    if (due.length > 0) {
                        mobEdu.acc.f.showOrHidePayButton(false);
                    }
                    for (var j = 0; j < due.length; j++) {

                        if (mobEdu.acc.f.isNegitiveAmount(due[j].amount)) {
                            mobEdu.acc.f.showOrHidePayButton(true);
                        }
                        sumStore.add({
                            description: due[j].description,
                            amount: due[j].amount,
                            category: due[j].category
                        });
                        sumStore.sync();
                    }
                }
            }
            if (Ext.os.is.Phone)
                mobEdu.acc.f.showDue();
            else {
                Ext.getCmp('detailsList').setEmptyText('<center><h3>No Payment Due</h3><center>');
                Ext.getCmp('detailsList').refresh();
            }
        },

        showOrHidePayButton: function(showOrHideFlag) {

            var proxyUser = mobEdu.util.getProxyUserId();
            var payBtn = null;
            if (!showOrHideFlag) {
                if (proxyUser != null && proxyUser != '') {
                    if (mobEdu.util.getProxyAccessFlag() == '0') {
                        if (Ext.os.is.Phone) {
                            mobEdu.util.get('mobEdu.acc.view.phone.due');
                            Ext.getCmp('payToolbar').setHidden(true);
                        }
                        //                        else {
                        //                            payBtn = document.getElementById('show');
                        //                            payBtn.style.display = "none";
                        //                        }
                    } else {
                        if (Ext.os.is.Phone) {
                            mobEdu.util.get('mobEdu.acc.view.phone.due');
                            Ext.getCmp('payToolbar').setHidden(false);
                        }
                        //                        else {
                        //                            payBtn = document.getElementById('show');
                        //                            payBtn.style.display = "";
                        //                        }
                    }
                } else {
                    if (Ext.os.is.Phone) {
                        mobEdu.util.get('mobEdu.acc.view.phone.due');
                        Ext.getCmp('payToolbar').setHidden(showOrHideFlag);
                    }
                    //                    else {
                    //                        payBtn = document.getElementById('show');
                    //                        payBtn.style.display = "";
                    //                    }
                }
            } else {
                if (Ext.os.is.Phone) {
                    mobEdu.util.get('mobEdu.acc.view.phone.due');
                    Ext.getCmp('payToolbar').setHidden(showOrHideFlag);
                }
                //                else {
                //                    payBtn = document.getElementById('show');
                //                    payBtn.style.display = "none";
                //                }
            }
        },

        getTotalAmounts: function(title) {
            var store = mobEdu.util.getStore('mobEdu.acc.store.localSummary');
            store.load();
            store.removeAll();
            store.sync();
            store.removed = [];
            store.add({
                title: 'Charges',
                totalAmount: mobEdu.acc.f.getTotalCharges()
            });
            store.sync();
            store.add({
                title: 'Payments',
                totalAmount: mobEdu.acc.f.getTotalPayments()
            });
            store.sync();
            store.add({
                title: 'Due',
                totalAmount: mobEdu.acc.f.getTotalDue()
            });
            store.sync();
        },
        getTotalCharges: function() {
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            var list = store.data.items;
            for (var i = 0; i < list.length; i++) {
                var charges = list[i].data.charges;
                if (charges != null) {
                    for (var j = 0; j < charges.length; j++) {
                        if (mobEdu.acc.f.isTotal(charges[j].description) == true) {
                            return charges[j].amount;
                        }
                    }
                }
                return '0';
            }
        },
        getTotalPayments: function() {
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            var list = store.data.items;
            for (var i = 0; i < list.length; i++) {
                var payments = list[i].data.payments;
                if (payments != null) {
                    for (var j = 0; j < payments.length; j++) {
                        if (mobEdu.acc.f.isTotal(payments[j].description) == true) {
                            return payments[j].amount;
                        }
                    }
                }
                return '0';
            }
        },
        getTotalDue: function() {
            var store = mobEdu.util.getStore('mobEdu.acc.store.summaryList');
            var list = store.data.items;
            for (var i = 0; i < list.length; i++) {
                var due = list[i].data.due;
                if (due != null) {
                    for (var j = 0; j < due.length; j++) {
                        if (mobEdu.acc.f.isTotal(due[j].description) == true) {
                            return due[j].amount;
                        }
                    }
                }
                return '0';
            }
        },
        loadTermList: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.termList');
            if (tStore.getCount() > 0) {
                tStore.removeAll();
                tStore.removed = [];
            }
            tStore.getProxy().setUrl(webserver + 'getBursarTerms');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = function() {
                mobEdu.acc.f.termListResponseHandler();
            }
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'bursarterms');
        },
        termListResponseHandler: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.termList');
            if (tStore.data.length > 0) {
                if (Ext.os.is.Phone) {
                    mobEdu.acc.f.showTermList();
                } else {
                    mobEdu.acc.f.loadTermListPopup();
                }
            } else {
                Ext.Msg.show({
                    message: 'No Transaction history Available.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        loadTermListPopup: function(popupView) {
            showView = mobEdu.acc.f.hidePopup;
            if (!popupView) {
                var popup = popupView = Ext.Viewport.add(
                    mobEdu.util.get('mobEdu.acc.view.tablet.termList')
                );
                Ext.Viewport.add(popupView);
            }
            popupView.show();
        },
        hidePopup: function() {
            showView = mobEdu.acc.f.showSummary;
            var popUp = Ext.getCmp('burTermListPopup');
            popUp.hide();
            Ext.Viewport.unmask();
        },
        onTermItemTap: function(view, index, target, item, e, o) {
            mobEdu.util.deselectSelectedItem(index, view);
            mobEdu.util.updateAccTermCode(item.data.code);
            mobEdu.util.updateAccTermDescription(item.data.description);
            if (!(Ext.os.is.Phone)) {
                mobEdu.acc.f.hidePopup();
                Ext.getCmp('summaryT').setTitle('<h1>' + mobEdu.util.getAccTermDescription() + '</h1>');
            } else {
                Ext.getCmp('summaryP').setTitle('<h1>' + mobEdu.util.getAccTermDescription() + '</h1>');
            }
            mobEdu.acc.f.loadSummaryListByTerm();
        },
        isNegitiveAmount: function(amount) {
            var num = new Number(amount);
            if (amount <= 0)
                return true;
            else
                return false;
        },
        isDue: function(title) {
            if (title == 'Due')
                return 1;
            return 0;
        },
        isDueExits: function(title, amount) {
            var num = new Number(amount);
            if (title == 'Due') {
                if (amount != '' && amount != null && amount != 'NA' && amount != "0" && num > 0) {
                    return true;
                }
            }
            return false;
        },
        onZipKeyup: function(zipfield) {
            var value = (zipfield.getValue()).toString() + '';
            value = value.replace(/\D/g, '');
            zipfield.setValue(value);
            var length = value.length;
            if (length > 5) {
                zipfield.setValue(value.substring(0, 5));
                return false;
            }
            return true;
        },
        cardValidation: function() {
            var cardName = Ext.getCmp('cardName').getValue();
            var cardStore = mobEdu.util.getStore('mobEdu.acc.store.paymentCard');
            var storeData = cardStore.data.all;
            for (var c = 0; c < storeData.length; c++) {
                if (cardName == storeData[c].data.cardName) {
                    return storeData[c].data.cardLength;
                }
            }
        },
        onExpiryDateKeyup: function(datefield) {
            var newDate = new Date();
            var presentDate = datefield.getValue();
            var timeH = newDate.getHours();
            var timeM = newDate.getMinutes();
            var newDateFormatted;
            if (newDate == null) {
                newDateFormatted = '';
            } else {
                newDateFormatted = (newDate.getFullYear() + '-' + [newDate.getMonth() + 1] + '-' + newDate.getDate());
            }
            var presentDateFormatted;
            if (presentDate == null) {
                presentDateFormatted = '';
            } else {
                presentDateFormatted = (presentDate.getFullYear() + '-' + [presentDate.getMonth() + 1] + '-' + presentDate.getDate());
            }
            if (newDateFormatted > presentDateFormatted) {
                Ext.Msg.show({
                    title: 'Invalid Date',
                    message: '<p>Please select a future date.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox',
                    fn: function(btn) {
                        if (btn == 'ok') {
                            Ext.getCmp('expDate').setValue(new Date());
                        }
                    }
                });
            }
        },
        submitPaymentForm: function() {
            var cardName = Ext.getCmp('cardName').getValue();
            var card = Ext.getCmp('cardNumber').getValue().toString();
            card = card.replace(/\D/g, '');
            var cardNum = '';
            if (card != null && card != 0) {
                cardNum = card.toString();
            }
            var name = Ext.getCmp('name').getValue();
            var amount = Ext.getCmp('amount').getValue().toString();
            amount = amount.replace(/\D/g, '');
            var amountNo = '';
            if (amount != null && amount != 0) {
                amountNo = amount.toString();
            }
            var expDate = Ext.getCmp('expDate').getValue();
            var cvv = Ext.getCmp('cvv').getValue().toString();
            cvv = cvv.replace(/\D/g, '');
            var zip = Ext.getCmp('payzip').getValue().toString();
            zip = zip.replace(/\D/g, '');
            if (cardNum != null && cardNum != '' && name != null && name != '' && zip != null && zip != '' && amountNo != null && amountNo != '' && expDate != null && expDate != '' && cvv != null && cvv != '') {
                if (cardNum.length != mobEdu.acc.f.cardValidation()) {
                    Ext.Msg.show({
                        message: '<p>Invalid card number.</p>',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                } else {
                    var cardHolderName = Ext.getCmp('name');
                    var regExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
                    if (!regExp.test(name)) {
                        Ext.Msg.show({
                            id: 'fullName',
                            name: 'fullName',
                            cls: 'msgbox',
                            title: null,
                            message: '<p>Invalid full name.</p>',
                            buttons: Ext.MessageBox.OK,
                            fn: function(btn) {
                                if (btn == 'ok') {
                                    cardHolderName.focus(true);
                                }
                            }
                        });
                    } else {
                        var zipCode = Ext.getCmp('payzip');
                        var zipValue = zipCode.getValue();
                        zipValue = zipValue.replace(/-|\(|\)/g, "");
                        if (zipValue.length != 5) {
                            Ext.Msg.show({
                                id: 'zipCode',
                                name: 'zipCode',
                                title: null,
                                cls: 'msgbox',
                                message: '<p>Invalid zip code.</p>',
                                buttons: Ext.MessageBox.OK,
                                fn: function(btn) {
                                    if (btn == 'ok') {
                                        zipCode.focus(true);
                                    }
                                }
                            });
                        } else {
                            var price = Ext.getCmp('amount');
                            var priceValue = price.getValue();
                            priceValue = priceValue.replace(/-|\(|\)/g, "");
                            if (amount < 0) {
                                Ext.Msg.show({
                                    id: 'amountValue',
                                    name: 'amountValue',
                                    cls: 'msgbox',
                                    title: null,
                                    message: '<p>Invalid Amount.</p>',
                                    buttons: Ext.MessageBox.OK,
                                    fn: function(btn) {
                                        if (btn == 'ok') {
                                            price.focus(true);
                                        }
                                    }
                                });
                            } else {
                                var presentDate = new Date();
                                var presentDateFormatted;
                                if (presentDate == null) {
                                    presentDateFormatted = '';
                                } else {
                                    presentDateFormatted = (presentDate.getFullYear() + '-' + [presentDate.getMonth() + 1] + '-' + presentDate.getDate());
                                }
                                var selectDate = Ext.getCmp('expDate').getValue();
                                var selectDateFormatted;
                                if (selectDate == null) {
                                    selectDateFormatted = '';
                                } else {
                                    selectDateFormatted = (selectDate.getFullYear() + '-' + [selectDate.getMonth() + 1] + '-' + selectDate.getDate());
                                }
                                if (presentDateFormatted > selectDateFormatted) {
                                    Ext.Msg.show({
                                        title: 'Invalid Date',
                                        message: '<p>Please select a future date.</p>',
                                        buttons: Ext.MessageBox.OK,
                                        cls: 'msgbox',
                                        fn: function(btn) {
                                            if (btn == 'ok') {
                                                Ext.getCmp('expDate').setValue(new Date());
                                            }
                                        }
                                    });
                                } else {
                                    setTimeout(function() {
                                        Ext.Msg.show({
                                            message: '<p>Payment received successfully.</p>',
                                            buttons: Ext.MessageBox.OK,
                                            cls: 'msgbox',
                                            fn: function(btn) {
                                                if (btn == 'ok') {
                                                    mobEdu.util.showMainView();
                                                    mobEdu.acc.f.resetPaymentForm();
                                                }
                                            }
                                        });
                                    }, 1000);
                                }
                            }
                        }
                    }
                }
            } else {
                Ext.Msg.show({
                    title: null,
                    message: '<p>Please provide all mandatory fields.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        resetPaymentForm: function() {
            mobEdu.util.get('mobEdu.acc.view.makePayment').reset();
        },
        cancelPaymentFrom: function() {
            //mobEdu.util.get('mobEdu.acc.view.makePayment').reset();
            Ext.getCmp('cardName').reset();
            Ext.getCmp('cardNumber').reset();
            Ext.getCmp('name').reset();
            Ext.getCmp('amount').reset();
            Ext.getCmp('expDate').reset();
            Ext.getCmp('cvv').reset();
            Ext.getCmp('payzip').reset();
        },
        onCardNumberKeyup: function(cardNumberfield) {
            var cardNumber = (cardNumberfield.getValue()).toString();
            cardNumber = cardNumber.replace(/\D/g, '');
            cardNumberfield.setValue(cardNumber);
            var length = cardNumber.length;
            if (length > mobEdu.acc.f.cardValidation()) {
                cardNumberfield.setValue(cardNumber.substring(0, mobEdu.acc.f.cardValidation()));
                return false;
            }
            return true;
        },
        onCardNumberBlur: function(cardNumberfield) {
            var cardNumber = (cardNumberfield.getValue()).toString();
            var length = cardNumber.length;
            if (length != mobEdu.acc.f.cardValidation())
                Ext.Msg.show({
                    message: 'Invalid card Number.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
        },
        onAmountKeyUp: function(amountfield) {
            var amount = amountfield.getValue().toString() + '';
            amount = amount.replace(/\D/g, '');
            amountfield.setValue(amount);
            if (amount < 0) {
                amountfield.setValue(0);
            }
        },
        onCvvKeyup: function(cvvfield) {
            var cvvNo = cvvfield.getValue().toString() + '';
            cvvNo = cvvNo.replace(/\D/g, '');
            cvvfield.setValue(cvvNo);
        },
        onMakePaymentBackTap: function() {
            if (Ext.os.is.Phone) {
                mobEdu.acc.f.showDue();
            } else {
                mobEdu.acc.f.showSummary();
            }
        },
        loadDetails: function(view, index, target, record, e, eOpts, detailsFlag) {
            var summaryFlag = mobEdu.util.getSummaryFlag();
            var event = null;
            mobEdu.util.updateDetailsFlag(detailsFlag);
            var title = record.data.description;
            if (!(title == 'Total' || title == 'Total amount due' || title == 'Residency Status')) {
                var sumDetailsStore = mobEdu.util.getStore('mobEdu.acc.store.summaryDetails');
                if (sumDetailsStore.getCount() > 0) {
                    sumDetailsStore.removeAll();
                    sumDetailsStore.removed = [];
                }
                if (Ext.os.is.Phone) {
                    mobEdu.util.get('mobEdu.acc.view.phone.summaryDetails');
                } else {
                    mobEdu.util.get('mobEdu.acc.view.tablet.summaryDetails');
                }
                Ext.getCmp('stuDetTitle').setTitle('<h1>' + title + '</h1>');
                if (summaryFlag == 'list') {
                    sumDetailsStore.getProxy().setUrl(webserver + 'getSummaryDetails');
                    sumDetailsStore.getProxy().setExtraParams({
                        studentId: mobEdu.main.f.getStudentId(),
                        category: record.data.category,
                        companyId: companyId
                    });
                    event = 'summarydetails';
                } else {
                    sumDetailsStore.getProxy().setUrl(webserver + 'getSummaryDetailsByTerm');
                    sumDetailsStore.getProxy().setExtraParams({
                        studentId: mobEdu.main.f.getStudentId(),
                        category: record.data.category,
                        termCode: mobEdu.util.getAccTermCode(),
                        companyId: companyId
                    });
                    event = 'summarydetailsbyterm';
                }
                sumDetailsStore.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
                sumDetailsStore.getProxy().afterRequest = function() {
                    mobEdu.acc.f.loadDetailsResponseHandler();
                }
                sumDetailsStore.load();
                mobEdu.util.gaTrackEvent('enroll', event);
            }
        },
        loadDetailsResponseHandler: function() {
            if (Ext.os.is.Phone) {
                mobEdu.util.get('mobEdu.acc.view.phone.summaryDetails');
                Ext.getCmp('sumDetails').getHeader().setCls('x-phone-grid-header');
                var css = Ext.getCmp('sumDetails').getItemCls();
                if (css.indexOf("size") == -1)
                    Ext.getCmp('sumDetails').setItemCls(css + ' size');
                Ext.getCmp('sumDetails').refresh();
                mobEdu.acc.f.showDetails();
            } else {
                mobEdu.acc.f.loadSummaryDetailsPopup();
            }
        },
        loadSummaryDetailsPopup: function(popupView) {
            showView = mobEdu.main.f.hideSummaryDetailsPopup;
            if (!popupView) {
                var popup = popupView = Ext.Viewport.add(mobEdu.util.get('mobEdu.acc.view.tablet.summaryDetails'));
                Ext.Viewport.add(popupView);
            }
            Ext.getCmp('summaryDet').refresh();
            popupView.show();
        },
        hideSummaryDetailsPopup: function() {
            showView = mobEdu.main.f.showSummary;
            var popUp = Ext.getCmp('sumDetailsPopup');
            popUp.hide();
        },
        onDetailsBackTap: function() {
            var detailsFlag = mobEdu.util.getDetailsFlag();
            if (detailsFlag == 'payments')
                mobEdu.acc.f.showPayment();
            else if (detailsFlag == 'charges')
                mobEdu.acc.f.showCharges();
            else
                mobEdu.acc.f.showDue();
        },
        showCharges: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showSummary);
            mobEdu.util.show('mobEdu.acc.view.phone.charges');
        },
        showDue: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showSummary);
            mobEdu.util.show('mobEdu.acc.view.phone.due');
        },
        showOtherCharges: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showSummary);
            mobEdu.util.show('mobEdu.acc.view.phone.otherCharges');
        },
        showPayment: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showSummary);
            mobEdu.util.show('mobEdu.acc.view.phone.payments');
        },
        showDetails: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.onDetailsBackTap);
            mobEdu.util.show('mobEdu.acc.view.phone.summaryDetails');
        },
        showSummary: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            if (Ext.os.is.Phone)
                mobEdu.util.show('mobEdu.acc.view.phone.summary');
            else {
                mobEdu.util.show('mobEdu.acc.view.tablet.summary');
            }
        },
        showEPayment: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showEbillSummary);
            mobEdu.util.show('mobEdu.acc.view.makePayment');
        },
        showEPaymentView: function() {
            if (paymentUrlSource === 'LINK')
                mobEdu.acc.f.getMakePaymentUrl();
            else if (paymentUrlSource === 'BANNERPROC')
                mobEdu.acc.f.showMakePayment();
            else
                mobEdu.acc.f.showEPayment();
        },
        showSPaymentView: function() {
            if (paymentUrlSource === 'LINK')
                mobEdu.acc.f.getMakePaymentUrl();
            else if (paymentUrlSource === 'BANNERPROC')
                mobEdu.acc.f.showMakePayment();
            else
                mobEdu.acc.f.showSPayment();
        },
        showSPayment: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showSummary);
            mobEdu.util.show('mobEdu.acc.view.makePayment');
        },
        showMakePayment: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.payment');
            tStore.getProxy().setUrl(webserver + 'getPaymentUrl');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = mobEdu.acc.f.showMakePaymentResponseHandler;
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'paymenturl');
        },
        showMakePaymentResponseHandler: function() {
            var url = '';
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.payment');
            if (tStore.data.length > 0) {
                url = tStore.data.items[0].data.url;
                console.log(url);
                if (url != null && url != '') {
                    mobEdu.util.loadExternalUrl(url);
                }
            } else {
                Ext.Msg.show({
                    message: 'Unable to retrieve Payment Gateway URL.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        showTermList: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showSummary);
            mobEdu.util.show('mobEdu.acc.view.phone.termList');
        },
        showAccMenu: function() {
            if (mobEdu.finaid.f.showFinMenu != null && mobEdu.finaid.f.showFinMenu != '') {
                mobEdu.util.updatePrevView(mobEdu.finaid.f.showFinMenu);
            } else {
                mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            }
            mobEdu.util.show('mobEdu.acc.view.menu');
            mobEdu.util.setTitle('ACCOUNTS', Ext.getCmp('accTitle'));
        },
        isTotal: function(title) {
            if (title == "Total" || title == "Total amount due")
                return true;
            return false;
        },

        loadEbillByTerm: function() {
            if (!(mobEdu.util.checkInfo('accTermCode'))) {
                mobEdu.util.loadCurrentTerm();
            }
            mobEdu.util.updateSummaryFlag('term');
            var store = mobEdu.util.getStore('mobEdu.acc.store.ebillSummary');
            if (store.getCount() > 0) {
                store.removeAll();
                store.removed = [];
            }
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setUrl(webserver + 'getEbillSummary');
            store.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                termCode: mobEdu.util.getAccTermCode(),
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.acc.f.ebillByTermResponseHandler();
            }
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'ebillsummary');
        },
        ebillByTermResponseHandler: function() {
            mobEdu.acc.f.showEbillSummary();
            var store = mobEdu.util.getStore('mobEdu.acc.store.ebillSummary');
            store.clearFilter();
            store.each(function(record) {
                var stu = record.get('studentId');
                if (stu != null && stu != '') {
                    var info = '<h3>' + record.get('studentName') +
                        '<br />Id : ' + record.get('studentId') +
                        '<br />Title IV Status : ' + record.get('status') + '</h3>';
                    if (Ext.os.is.Phone) {
                        Ext.getCmp('ebillInfoP').setHtml(info);
                    } else {
                        Ext.getCmp('ebillInfoT').setHtml(info);
                    }
                }
            });

            store.filter(function(record) {
                if (record.get('studentId') != null && record.get('studentId') != '') {
                    return false;
                }
                return true;
            });
        },
        loadEbillTermList: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.termList');
            if (tStore.getCount() > 0) {
                tStore.removeAll();
                tStore.removed = [];
            }
            tStore.getProxy().setUrl(webserver + 'getBillingInfoTerms');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = function() {
                mobEdu.acc.f.termEListResponseHandler();
            }
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'billinginfoterms');
        },
        termEListResponseHandler: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.termList');
            if (tStore.data.length > 0) {
                mobEdu.acc.f.showEbillTermList();
            } else {
                Ext.Msg.show({
                    message: 'No Transaction history Available.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        loadEbillTermListPopup: function(popupView) {
            showView = mobEdu.acc.f.hidePopup;
            if (!popupView) {
                var popup = popupView = Ext.Viewport.add(
                    mobEdu.util.get('mobEdu.acc.view.tablet.ebillTermList')
                );
                Ext.Viewport.add(popupView);
            }
            popupView.show();
        },
        hideEPopup: function() {
            showView = mobEdu.acc.f.showEbillSummary;
            var popUp = Ext.getCmp('burETermListPopup');
            popUp.hide();
            Ext.Viewport.unmask();
        },
        onETermItemTap: function(view, index, target, item, e, o) {
            mobEdu.util.deselectSelectedItem(index, view);
            mobEdu.util.updateAccTermCode(item.data.code);
            mobEdu.util.updateAccTermDescription(item.data.description);
            mobEdu.acc.f.loadEbillByTerm();
        },
        isSummaryExists: function(summary) {
            if (summary == 'N') {
                return true;
            }
            return false;
        },

        isPaymentExists: function(payment, amount) {
            if (payment == null || payment == '' || payment == 'NaN') {
                if (amount == null || amount == '' || amount == 'NaN')
                    return false;
                else
                    return true;
            }
            return true;
        },
        showEbillSummary: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showEbillTermList);
            if (Ext.os.is.Phone) {
                mobEdu.util.show('mobEdu.acc.view.phone.ebillSummary');
                Ext.getCmp('termEbillTitleP').setTitle('<h1>' + mobEdu.util.getAccTermDescription() + '</h1>');
            } else {
                mobEdu.util.show('mobEdu.acc.view.tablet.ebillSummary');
                Ext.getCmp('termEbillTitleT').setTitle('<h1>' + mobEdu.util.getAccTermDescription() + '</h1>');
            }
        },
        showEbillTermList: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.acc.view.phone.ebillTermList');
        },
        isAmountZero: function(amount) {
            if (amount == '0')
                return true;
            else
                return false;
        },
        getMakePaymentUrl: function() {
            var url = '';
            var paymentStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
            var paymentData = paymentStore.data.all;
            var format;
            for (var i = 0; i < paymentData.length; i++) {
                var code = paymentData[i].data.moduleCode;
                if (code === 'PAYMENT') {
                    var record = paymentData[i].data.feeds[0];
                    format = record.format;
                    url = record.link;
                }
            }
            if (url != null && url != '') {
                if (format == 'CAS') {
                    mobEdu.util.openURLWithCAS(url);

                } else {
                    mobEdu.util.loadExternalUrl(url);
                }

            } else {
                Ext.Msg.show({
                    message: 'Unable to retrieve Payment Gateway URL.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },
        onPaymentButtonTap: function() {
            if (paymentUrlSource === 'LINK')
                mobEdu.acc.f.getMakePaymentUrl();
            else if (paymentUrlSource === 'BANNERPROC')
                mobEdu.acc.f.showMakePayment();
            else
                mobEdu.acc.f.showDuePayment();
        },
        showDuePayment: function() {
            mobEdu.util.updatePrevView(mobEdu.acc.f.showDue);
            mobEdu.util.show('mobEdu.acc.view.makePayment');
        },

        loadNighthawks: function() {
            if (Ext.os.is.Android) {
                var url = mobEdu.util.convertPdfUrl('http://ung.edu/business-office/_uploads/files/Wait-24-hours.pdf');
                mobEdu.util.loadExternalUrl(url)
            } else {
                mobEdu.util.loadExternalUrl('http://ung.edu/business-office/_uploads/files/Wait-24-hours.pdf')
            }
        },

        isResidence: function(residence) {
            var reg = new RegExp(/[0-9]/g);
            if (reg.test(residence))
                return false;
            return true;
        },

        loadBillingCounselorDetails: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.bcDetails');
            if (tStore.getCount() > 0) {
                tStore.removeAll();
                tStore.removed = [];
            }
            tStore.getProxy().setUrl(webserver + 'getBillingCounselorInfo');
            tStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId()
            });
            tStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            tStore.getProxy().afterRequest = function() {
                mobEdu.acc.f.bcDetailsResHandler();
            }
            tStore.load();
            mobEdu.util.gaTrackEvent('enroll', 'billingCounselorInfo');
        },
        bcDetailsResHandler: function() {
            var tStore = mobEdu.util.getStore('mobEdu.acc.store.bcDetails');
            var comments = tStore.data.all[0].data.comments;
            if (tStore.data.length > 0) {
                mobEdu.acc.f.showBillingCounselorDetails();
                // Ext.getCmp('billingCounselorDetails').setHtml(comments);
            } else {
                Ext.Msg.show({
                    message: comments,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        showBillingCounselorDetails: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.acc.view.billingCounselorInfo');
        },

        formatComments: function(comments) {
            var details = comments.split('\n');
            comments = '';
            for (var i = 0; i < details.length; i++) {
                comments = comments + '<br>' + details[i];
            }
            return comments;
        }
    }
});
