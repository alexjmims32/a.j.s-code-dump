Ext.define('mobEdu.acc.store.termList', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.acc.model.termList'
    ],

    config:{
        storeId: 'mobEdu.acc.store.termList',
        
        autoLoad: false,
        
        model: 'mobEdu.acc.model.termList'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'bursarTermList'        
        return proxy;
    }
});
