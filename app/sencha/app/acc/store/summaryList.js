Ext.define('mobEdu.acc.store.summaryList', {
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.acc.model.summaryList'
    ],

    config: {
        storeId: 'mobEdu.acc.store.summaryList',

        autoLoad: false,

        model: 'mobEdu.acc.model.summaryList'
        
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});