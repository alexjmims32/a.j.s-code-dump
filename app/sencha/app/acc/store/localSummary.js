Ext.define('mobEdu.acc.store.localSummary', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.acc.model.summary'
    ],

    config:{
        storeId: 'mobEdu.acc.store.localSummary',
        autoLoad: false,
        model: 'mobEdu.acc.model.summary',
        proxy:{
            type:'localstorage',
            id:'acclocalSummary'
        }
    }
});