Ext.define('mobEdu.acc.store.ebillSummary', {
    extend: 'mobEdu.data.store',

    requires: ['mobEdu.acc.model.ebillSummary'],
    config: {
        storeId: 'mobEdu.acc.store.ebillSummary',
        autoLoad: false,
        model: 'mobEdu.acc.model.ebillSummary',
        sorters: {
            property: 'seqNum',
            direction: 'ASC'
        },
        grouper: {
            sortProperty: 'seqNum',
            groupFn: function(record) {
                return record.get('group') + ':';
            }
        },
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'ROWSET',
                record: 'RECORD'
            }
        }
    }
});