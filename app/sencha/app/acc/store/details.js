Ext.define('mobEdu.acc.store.details', {
	extend: 'Ext.data.Store',
	requires: [
		'mobEdu.acc.model.details'
	],
	config: {
		storeId: 'mobEdu.acc.store.details',
		autoLoad: true,
		model: 'mobEdu.acc.model.details',
		proxy: {
			type: 'memory',
			id: 'details'
		}
	}
});