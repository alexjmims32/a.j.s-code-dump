Ext.define('mobEdu.acc.store.summaryDetails', {
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.acc.model.summaryDetails'
    ],

    config: {
        storeId: 'mobEdu.acc.store.summaryDetails',

        autoLoad: false,

        model: 'mobEdu.acc.model.summaryDetails'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'summaryDetails'        
        return proxy;
    }
});