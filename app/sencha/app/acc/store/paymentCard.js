Ext.define('mobEdu.acc.store.paymentCard', {
	extend: 'Ext.data.Store',
	requires: [
		'mobEdu.acc.model.paymentCard'
	],
	config: {
		storeId: 'mobEdu.acc.store.paymentCard',
		autoLoad: true,
		model: 'mobEdu.acc.model.paymentCard',
		data: [
			{cardName: 'American Express', cardType: 'American Express', cardLength: '15'},
			{cardName: 'Visa', cardType: 'Visa', cardLength: '16'},
			{cardName: 'MasterCard', cardType: 'MasterCard', cardLength: '16'},
			{cardName: 'Discover', cardType: 'Discover', cardLength: '16'}
		]
	}
});