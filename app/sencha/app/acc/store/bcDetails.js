Ext.define('mobEdu.acc.store.bcDetails', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.acc.model.ebillSummary'
    ],

    config: {
        storeId: 'mobEdu.acc.store.bcDetails',

        autoLoad: false,

        model: 'mobEdu.acc.model.ebillSummary'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = ''
        return proxy;
    }
});
