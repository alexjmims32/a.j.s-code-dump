Ext.define('mobEdu.acc.store.payment', {
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.acc.model.payment'
    ],

    config: {
        storeId: 'mobEdu.acc.store.payment',

        autoLoad: false,

        model: 'mobEdu.acc.model.payment'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});