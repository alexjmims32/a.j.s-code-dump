Ext.define('mobEdu.acc.store.summary', {
extend: 'mobEdu.data.store',

    requires: [
    'mobEdu.acc.model.details'
    ],

    config: {
        storeId: 'mobEdu.acc.store.summary',

        autoLoad: false,

        model: 'mobEdu.acc.model.details'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'accountSummary'        
        return proxy;
    }
});