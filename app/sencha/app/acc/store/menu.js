Ext.define('mobEdu.acc.store.menu', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.acc.model.menu'
    ],

    config:{
        storeId: 'mobEdu.acc.store.menu',
        autoLoad: false,
        model: 'mobEdu.acc.model.menu',
        proxy:{
            type:'memory',
            id:'accMenu'
        }
    }
});