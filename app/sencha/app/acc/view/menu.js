Ext.define('mobEdu.acc.view.menu', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.acc.f',
		'mobEdu.acc.store.menu',
		'mobEdu.main.store.entourage'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		items: [{
			xtype: 'list',
			cls: 'logo',
			itemTpl: '<table width="100%"><tr><td width="5%" align="left" class="esummaryimg"></td><td width="85%" align="left"><h3>{title}</h3></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.acc.store.menu'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, item, e) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					e.data.action();
				}
			}
		}, {
			xtype: 'customToolbar',
			id: 'accTitle'
		}],
		flex: 1
	}
});