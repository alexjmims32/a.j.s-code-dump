Ext.define('mobEdu.acc.view.phone.ebillSummary', {
	extend: 'Ext.Panel',
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		items: [{
			title: '<h1>Student Details</h1>',
			id: 'termEbillTitleP',
			xtype: 'customToolbar',
			ui: 'light',
			docked: 'top'
		}, {
			xtype: 'label',
			id: 'ebillInfoP',
			name: 'ebillInfoP',
			docked: 'top'
		}, {
			xtype: 'toolbar',
			id: 'ebillToolbar',
			name: 'ebillToolbar',
			docked: 'top',
			maxHeight: 10,
			bevelEscape: true,
			style: 'background: rgba(0,0,0,0);',
			defaults: {
				style: 'background:none;border:none;'
			},
			items: [{
				text: '<h5>Description</h5>',
				width: '35%'
			}, {
				html: '<h5>Charges</h5>',
				width: '18%'
			}, {
				html: '<h5>Credits</h5>',
				width: '18%'
			}, {
				html: '<h5>Balance</h5>',
				width: '18%'
			}]
		}, {
			xtype: 'list',
			grouped: true,
			loadingText: '',
			disableSelection: true,
			itemTpl: new Ext.XTemplate(
				'<table width="100%"><tr>' +
				'<tpl if="(mobEdu.util.isValueExists(info) === false)">',
				'<tpl if="(mobEdu.acc.f.isSummaryExists(summary) === true)">',
				'<td width="30%"><h5>{[this.getDescription(values.description)]}</h5></td>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(charges,dueDate) === true)">',
				'<td width="25%" align="center"><h6>{charges}</h6></td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(dueDate) === true)">',
				'<td width="25%" align="center"><h6>{dueDate}</h6></td>' +
				'<tpl else>',
				'<td width="25%" align="center">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(payments,amount) === true)">',
				'<td width="25%" align="center"><h6>{payments}</h6></td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(amount) === true)">',
				'<td width="25%" align="center"><h6>{amount}</h6></td>' +
				'<tpl else>',
				'<td width="25%" align="center">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.util.isValueExists(due) === true)">',
				'<td width="25%" align="center"><h6>{due}</h6></td>' +
				'<tpl else>',
				'<td width="25%" align="center">&nbsp;</td>' +
				'</tpl>' +


				'<tpl else>' +
				'<td width="35%"><h5><b>{[this.getDescription(values.description)]}</b></h5></td>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(charges,dueDate) === true)">',
				'<td width="25%" align="center"><h6><b>{charges}</b></h6></td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(dueDate) === true)">',
				'<td width="25%" align="center"><h6><b>{dueDate}</b></h6></td>' +
				'<tpl else>',
				'<td width="25%" align="center">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(payments,amount) === true)">',
				'<td width="25%" align="center"><h6><b>{payments}</b></h6></td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(amount) === true)">',
				'<td width="25%" align="center"><h6><b>{amount}</b></h6></td>' +
				'<tpl else>',
				'<td width="25%" align="center">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.util.isValueExists(due) === true)">',
				'<td width="25%" align="center"><h6><b>{due}</b></h6></td>' +
				'<tpl else>',
				'<td width="25%" align="center">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl else>' +
				'<td width="100%" colspan="4">{[this.getDescription(values.info)]}</td>' +
				'</tpl>' +
				'</tr></table>', {
					compiled: true,
					getDescription: function(description) {
						return mobEdu.util.convertRelativeToAbsoluteUrls(description);
					}
				}
			),
			store: mobEdu.util.getStore('mobEdu.acc.store.ebillSummary')
		}, {
			xtype: 'toolbar',
			id: 'phSumtBar',
			name: 'phSumtBar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				ui: 'button',
				text: 'Make Payment',
				handler: function() {
					mobEdu.acc.f.showEPaymentView();
				}
			}, {
				ui: 'button',
				text: 'Refresh',
				handler: function() {
					mobEdu.acc.f.loadEbillByTerm();
				}
			}]
		}],
		flex: 1
	}
});