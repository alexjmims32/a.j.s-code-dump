Ext.define('mobEdu.acc.view.phone.summary', {
    extend: 'Ext.Panel',
	requires: [
		'mobEdu.acc.store.localSummary'
	],		
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'accSummary',
            name: 'accSummary',
            emptyText: '<h3 align="center">No Summary info available</h3>',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr style="vertical-align:top;">' + '<td>'+ '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === true)">', '<b class="due">{title}</b>', '</tpl>', '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === false)">','<h3>{title}</h3>', '</tpl>', '</td>' + '<td align="right">', '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === true)">', '<b class="due">$ {totalAmount}</b>', '</tpl>', '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === false)">', '<h2>$ {totalAmount}</h2>', '</tpl>', '</td>'+ '<td width="10%" align="right" style="padding: 6px 0px 0px 4px;"><div class="arrow" /></td></tr></table>'),
            store: mobEdu.util.getStore('mobEdu.acc.store.localSummary'),
            //            data: [
            //                    { title: 'Charges' },
            //                    { title: 'Payments' },
            //                    { title: 'Due' }
            //                ],
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, item, e, o) {
                    mobEdu.acc.f.onAccSummaryItemTap(view, index, target, item, e, o);
                }
            }
        }, {
            title: '<h1>Fee Details</h1>',
            xtype: 'customToolbar',
            id: 'summaryP'
        },
        {
            xtype: 'toolbar',
            id: 'phSumtBar',
            name: 'phSumtBar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                ui: 'button',
                id: 'Psum',
                text: 'Change Term',
                handler: function() {
                    mobEdu.acc.f.loadTermList();
                }
            }
            //         {
            //     ui: 'button',
            //     text: 'Make Payment',
            //     handler: function() {
            //         mobEdu.acc.f.showSPaymentView();
            //     }
            // }
        ]
        }],
        flex: 1
    }
});