Ext.define('mobEdu.acc.view.phone.grid', {
	extend: 'Ext.ux.touch.grid.View',
	xtype: 'phoneSumGrid',
	requires: [
		'mobEdu.acc.store.summaryDetails'
	],
	config: {
		title: 'Grid',
		store: true,
		columns: [{
			header: 'Term',
			dataIndex: 'termDescription',
			style: 'text-align: left;padding-left: 1em;',
			cls: 'x-grid-cell x-grid-cell-hdleft',
			width: '50%'
		}, {
			header: 'Balance',
			dataIndex: 'balance',
			style: 'text-align: right;padding-right: 1em;',
			cls: 'x-grid-cell x-grid-cell-hdright',
			width: '25%'
		}, {
			header: 'Amount',
			dataIndex: 'amount',
			style: 'text-align: right;padding-right: 1em;',
			cls: 'x-grid-cell x-grid-cell-hdright',
			width: '25%'
		}],
		features: [{
			ftype: 'Ext.ux.touch.grid.feature.Sorter',
			launchFn: 'initialize'
		}, {
			ftype: 'Ext.ux.touch.grid.feature.Editable',
			launchFn: 'initialize'
		}]
	},
	applyStore: function() {
		return mobEdu.util.getStore('mobEdu.acc.store.summaryDetails');
	}
});