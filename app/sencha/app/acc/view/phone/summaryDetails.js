Ext.define('mobEdu.acc.view.phone.summaryDetails', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        items: [{
            title: '<h1>Details</h1>',
            xtype: 'customToolbar',
            id: 'stuDetTitle'
        },
        // {
        //     title: '<h1>Student Details</h1>',
        //     id: 'stuDetTitle',
        //     xtype: 'toolbar',
        //     style: 'font-size:10pt',
        //     ui: 'light',
        //     docked: 'top'
        // }, 
        {
            xtype: 'phoneSumGrid',
            id: 'sumDetails',
            name: 'sumDetails'
        }],
        flex: 1
    }
});