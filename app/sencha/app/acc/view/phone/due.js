Ext.define('mobEdu.acc.view.phone.due', {
    extend: 'Ext.Panel',
	requires: [
		'mobEdu.acc.store.details'
	],	
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'dueList',
            name: 'dueList',
            emptyText: '<h3 align="center">No Payment Due</h3>',
            itemTpl: new Ext.XTemplate('<tpl if="(mobEdu.acc.f.isTotal(description)===false)">', '<table width="100%"><tr><td width="50%"><h3>{description}</h3></td><td width="50%" align="right"><h3>$ {amount}</h3></td><td width="10%" align="right" style="padding: 9px 0px 0px 4px;" ><div class="arrow"/></td></tr></table>', '<tpl else>', '<table width="100%"><tr><td width="50%"><h2>{description}</h2></td><td width="50%" align="right"><h2>$ {amount}</h2></td><td width="10%">&nbsp;</td></tr></table>', '</tpl>'),
            store: mobEdu.util.getStore('mobEdu.acc.store.details'),
            singleSelect: true,
            disableSelection: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts) {
                    mobEdu.acc.f.loadDetails(view, index, target, record, e, eOpts, 'due')
                }
            }
        }, {
            title: '<h1>Due Details</h1>',
            xtype: 'customToolbar'
        }, {
            xtype: 'toolbar',
            id: 'payToolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                id: 'mkPayBtn',
                text: 'Make a Payment',
                handler: function() {
                    mobEdu.acc.f.onPaymentButtonTap();
                }
            }]
        }],
        flex: 1
    }
});