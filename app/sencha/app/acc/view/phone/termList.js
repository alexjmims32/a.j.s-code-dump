Ext.define('mobEdu.acc.view.phone.termList', {
    extend: 'Ext.Panel',
	requires: [
		'mobEdu.acc.store.termList'
	],	
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'burTermList',
            name: 'burTermList',
            cls: 'logo',
            itemTpl: '<h3>{description}</h3>',
            store: mobEdu.util.getStore('mobEdu.acc.store.termList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, item, e, o) {
                    mobEdu.acc.f.onTermItemTap(view, index, target, item, e, o);
                }
            }
        }, {
            title: '<h1>Details</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});