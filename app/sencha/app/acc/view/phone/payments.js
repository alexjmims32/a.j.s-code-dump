Ext.define('mobEdu.acc.view.phone.payments', {
    extend: 'Ext.Panel',
	requires: [
		'mobEdu.acc.store.details'
	],	
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'paymentsList',
            name: 'paymentsList',
            emptyText: '<h3 align="center">No Payments</h3>',
            itemTpl: new Ext.XTemplate('<tpl if="(mobEdu.acc.f.isTotal(description)===false)">', '<table width="100%"><tr><td width="50%"><h3>{description}</h3></td><td width="50%" align="right"><h3>$ {amount}</h3></td><td width="10%" align="right" style="padding: 9px 0px 0px 4px;"><div class="arrow" /></td></tr></table>', '<tpl else>', '<table width="100%"><tr><td width="50%"><h2>{description}</h2></td><td width="50%" align="right"><h2>$ {amount}</h2></td><td width="10%">&nbsp;</td></tr></table>', '</tpl>'),
            store: mobEdu.util.getStore('mobEdu.acc.store.details'),
            singleSelect: true,
            disableSelection: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, e, eOpts) {
                    mobEdu.acc.f.loadDetails(view, index, target, record, e, eOpts, 'payments')
                }
            }
        }, {
            title: '<h1>Payment Details</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});