Ext.define('mobEdu.acc.view.phone.ebillGrid', {
	extend: 'Ext.ux.touch.grid.View',
	xtype: 'phoneEbillSumGrid',
	requires: [
		'mobEdu.acc.store.ebillSummary'
	],
	config: {
		title: 'Grid',
		store: true,
                
		columns: [
			{ 
				header: 'Description',
				dataIndex: 'description',
				style: 'text-align: left;padding-left: 1em;',
				width: '49%'
                                
			}, {
				header: 'Charges',
				dataIndex: 'charges',
				style: 'text-align: right;padding-right: 1em;',
				width: '17%'
			},
			{
				header: 'Credits',
				dataIndex: 'payments',
				style: 'text-align: right;padding-right: 1em;',
				width: '17%'
			},
			{
				header: 'Due',
				dataIndex: 'due',
				style: 'text-align: right;padding-right: 1em;',                               
				width: '17%'
			}
		],
		features: [
			{
				ftype: 'Ext.ux.touch.grid.feature.Editable',
				launchFn: 'initialize'
			}
		]
	},
	applyStore: function() {
		return mobEdu.util.getStore('mobEdu.acc.store.ebillSummary');
	}
});
