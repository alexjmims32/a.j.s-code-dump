Ext.define('mobEdu.acc.view.phone.ebillTermList', {
    extend: 'Ext.Panel',
	requires: [
		'mobEdu.acc.store.termList'
	],	
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'burETermList',
            name: 'burETermList',
            cls: 'logo',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr><td width="50%"><h3>{description}<h3></td><td width="2%" align="right"><div class="arrow" /></td></tr></table>'),
            store: mobEdu.util.getStore('mobEdu.acc.store.termList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, item, e, o) {
                    mobEdu.acc.f.onETermItemTap(view, index, target, item, e, o);
                }
            }
        }, {
            title: '<h1>Select Term</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});