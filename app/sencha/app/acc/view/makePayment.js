Ext.define('mobEdu.acc.view.makePayment', {
	extend: 'Ext.form.Panel',

	requires: [
		'mobEdu.acc.store.paymentCard'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		items: [{
			xtype: 'fieldset',
			items: [{
				xtype: 'selectfield',
				labelAlign: 'left',
				labelWidth: '40%',
				id: 'cardName',
				name: 'cardName',
				label: 'Card Type',
				required: true,
				height: 'auto',
				store: mobEdu.util.getStore('mobEdu.acc.store.paymentCard'),
				displayField: 'cardName',
				valueField: 'cardType'
			}, {
				xtype: 'textfield',
				labelAlign: 'left',
				labelWidth: '40%',
				label: 'Card Number',
				id: 'cardNumber',
				name: 'cardNumber',
				placeHolder: 'XXXXXXXXXXXXXXXX',
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');
					input.set({
						pattern: '[0-9]*'
					});
					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.acc.f.onCardNumberKeyup(textfield);
					}
				},
				useClearIcon: true,
				autoCapitalize: false,
				required: true
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'Full Name',
				id: 'name',
				name: 'name',
				placeHolder: 'Card Holder Name',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'Amount',
				id: 'amount',
				name: 'amount',
				placeHolder: '$',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');
					input.set({
						pattern: '[0-9]*'
					});
					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, eventObj) {
						mobEdu.acc.f.onAmountKeyUp(textfield);
					}
				}
			}, {
				xtype: 'datepickerfield',
				labelWidth: '40%',
				label: 'Expiration Date',
				id: 'expDate',
				value: new Date(),
				name: 'expDate',
				//                placeHolder:'Select',
				picker: {
					yearFrom: new Date().getFullYear(),
					yearTo: new Date().getFullYear() + 20,
					slotOrder: ['day', 'month', 'year']
				},
				listeners: {
					change: function(datefield, e, eOpts) {
						mobEdu.acc.f.onExpiryDateKeyup(datefield)
					}
				},
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'CVV',
				id: 'cvv',
				name: 'cvv',
				placeHolder: 'Number',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');
					input.set({
						pattern: '[0-9]*'
					});
					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, e, eOpts) {
						return mobEdu.acc.f.onCvvKeyup(textfield);
					}
				}
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'Zip Code',
				name: 'payzip',
				id: 'payzip',
				placeHolder: '',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');
					input.set({
						pattern: '[0-9]*'
					});
					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, e, eOpts) {
						return mobEdu.acc.f.onZipKeyup(textfield);
					}
				},
				maxLength: 5,
				autoCreate: Ext.apply({
					maxlength: '1'
				}, Ext.form.Field.prototype.defaultAutoCreate)
			}]
		}, {
			xtype: 'customToolbar',
			title: '<h1>Payment</h1>'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Clear',
				handler: function() {
					mobEdu.acc.f.cancelPaymentFrom();
				}
			}, {
				text: 'Submit',
				align: 'right',
				handler: function() {
					mobEdu.acc.f.submitPaymentForm();
				}
			}]
		}]
	}
});