Ext.define('mobEdu.acc.view.tablet.grid', {
    extend: 'Ext.ux.touch.grid.View',
    xtype: 'tabletSumGrid',
    requires: [
        'mobEdu.acc.store.summaryDetails'
    ],
    config: {
        title: 'Grid',
        store: true,
        columns: [{
            header: 'Term',
            dataIndex: 'termDescription',
            cls: 'x-grid-cell x-grid-cell-hdleft',
            width: '40%'
        }, {
            header: 'Balance',
            dataIndex: 'balance',
            cls: 'x-grid-cell x-grid-cell-hdright',
            width: '30%'
        }, {
            header: 'Amount',
            dataIndex: 'amount',
            cls: 'x-grid-cell x-grid-cell-hdright',
            width: '30%'
        }],
        features: [{
            ftype: 'Ext.ux.touch.grid.feature.Sorter',
            launchFn: 'initialize'
        }, {
            ftype: 'Ext.ux.touch.grid.feature.Editable',
            launchFn: 'initialize'
        }]
    },
    applyStore: function() {
        return mobEdu.util.getStore('mobEdu.acc.store.summaryDetails');
    }
});