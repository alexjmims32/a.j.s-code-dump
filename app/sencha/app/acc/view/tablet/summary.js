Ext.define('mobEdu.acc.view.tablet.summary', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.main.store.entourage',
        'mobEdu.acc.store.details',
        'mobEdu.acc.store.localSummary'
    ],
    config: {
        fullscreen: true,
        layout: {
            type: 'card',
            animation: {
                type: 'slide',
                direction: 'left',
                duration: 250
            }
        },
        cls: 'logo',
        items: [{
            title: '<h1>Payment Details</h1>',
            xtype: 'customToolbar',
            id: 'summaryT'
        }, {
            xtype: 'toolbar',
            id: 'tabSumtBar',
            name: 'tabSumtBar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                ui: 'button',
                id: 'Tsum',
                text: 'Change Term',
                handler: function() {
                    mobEdu.acc.f.loadTermList();
                }
            }
            // {
            //     ui: 'button',
            //     text: 'Make Payment',
            //     handler: function() {
            //         mobEdu.acc.f.showSPaymentView();
            //     }
            // }
            ]
        }, {
            //            useTitleAsBackText: false,
            xtype: 'list',
            docked: 'left',
            width: '40%',
            cls: 'border',
            id: 'summaryList',
            name: 'summaryList',
            emptyText: '<h3 align="center">No Summary info available</h3>',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + '<td>' + '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === true)">', '<b class="due">{title}</b>', '</tpl>', '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === false)">', '<h3>{title}</h3>', '</tpl>', '</td>' + '<td align="right">', '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === true)">', '<b class="due">$<tpl if="(mobEdu.acc.f.isAmountZero(totalAmount) === true)"> 0.00 <tpl else>{totalAmount}</tpl></b>', '</tpl>', '<tpl if="(mobEdu.acc.f.isDueExits(title,totalAmount) === false)">', '<h2>$<tpl if="(mobEdu.acc.f.isAmountZero(totalAmount) === true)"> 0.00 <tpl else>{totalAmount}</h2></tpl>', '</tpl>', '</td>' + '</tr></table>'),
            store: mobEdu.util.getStore('mobEdu.acc.store.localSummary'),

            //<tpl if="(mobEdu.acc.f.isDue(title)===1)"><img src="' + mobEdu.util.getResourcePath() + 'images/pay.png" id="show" name="show" style="align:right;display:none"/></tpl>
            //            data: [
            //            {
            //                title: 'Charges'
            //            },
            //            {
            //                title: 'Payments'
            //            },
            //            {
            //                title: 'Due'
            //            }
            //            ],
            singleSelect: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, item, e, o) {
                    mobEdu.acc.f.onAccSummaryItemTap(view, index, target, item, e, o);
                }
            }
        }, {
            layout: 'fit',
            scrollable: true,
            items: [{
                xtype: 'list',
                id: 'detailsList',
                name: 'detailsList',
                styleHtmlCls: 'removeBorders',
                styleHtmlContent: true,
                disableSelection: true,
                itemTpl: new Ext.XTemplate('<tpl if="(mobEdu.acc.f.isTotal(description)===false)">', '<table width="100%"><tr style="vertical-align:top;"><td width="50%"><h3>{description}<h3></td><tpl if="(mobEdu.acc.f.isResidence(amount)===false)"><td width="15%" align="right"><h3>$ {amount}</h3></td><td width="2%" align="right"  style="padding: 9px 0px 0px 4px;"><div class="arrow" /></td><tpl else><td width="15%" align="right"><h3>{amount}</h3></td></tpl></tr></table>', '<tpl else>', '<table width="100%"><tr "vertical-align:top;"><td width="50%" ><h2>{description}</h2></td><tpl if="(mobEdu.acc.f.isResidence(amount)===false)"><td width="15%" align="right"><h2>$ {amount}</h2></td><tpl else><td width="15%" align="right"><h2>{amount}</h2></td></tpl><td width="2%">&nbsp;</td></tr></table>', '</tpl>'),
                store: mobEdu.util.getStore('mobEdu.acc.store.details'),
                singleSelect: true,
                loadingText: '',
                listeners: {
                    itemtap: function(view, index, target, record, e, eOpts) {
                        mobEdu.acc.f.loadDetails(view, index, target, record, e, eOpts)
                    }
                }
            }]
        }],
        flex: 1
    }
});