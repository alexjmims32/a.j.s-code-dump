Ext.define('mobEdu.acc.view.tablet.termList', {
    extend:'Ext.Panel',
	requires: [
		'mobEdu.acc.store.termList'
	],	
    config:{
        id:'burTermListPopup',
        scroll:'vertical',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'60%',
        height:'60%',
        layout:'fit',
        items:[
        {
            xtype:'list',
            id:'burTermListP',
            name:'burTermListP',
            cls:'logo',
            itemTpl:'<h3>{description}</h3>',
            store: mobEdu.util.getStore('mobEdu.acc.store.termList'),
            singleSelect:true,
            loadingText:'',
            listeners:{
                itemtap:function (view, index, target,item,e,o) {
                    mobEdu.acc.f.onTermItemTap(view, index, target,item,e,o);
                }
            }
        }
        ],
        flex:1
    }

});
