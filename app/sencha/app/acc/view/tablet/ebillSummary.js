Ext.define('mobEdu.acc.view.tablet.ebillSummary', {
	extend: 'Ext.Panel',
	config: {
		fullscreen: true,
		layout: 'vbox',
		cls: 'logo',
		items: [{
			title: '<h1>Student Details</h1>',
			id: 'termEbillTitleT',
			xtype: 'customToolbar',
			ui: 'light',
			docked: 'top'
		}, {
			xtype: 'label',
			id: 'ebillInfoT',
			name: 'ebillInfoT',
			docked: 'top'
		}, {
			xtype: 'toolbar',
			id: 'ebillToolbar',
			name: 'ebillToolbar',
			docked: 'top',
			maxHeight: 10,
			bevelEscape: true,
			style: 'background: rgba(0,0,0,0);',
			defaults: {
				style: 'background:none;border:none;'
			},
			items: [{
				text: '<h2>Description</h2>',
				width: '62%'
			}, {
				html: '<h2>Charges</h2>',
				width: '12%'
			}, {
				html: '<h2>Credits</h2>',
				width: '12%'
			}, {
				html: '<h2>Balance</h2>',
				width: '14%'
			}]
		}, {
			flex: 1,
			xtype: 'list',
			grouped: true,
			loadingText: '',
			itemTpl: new Ext.XTemplate(

				'<table width="100%"><tr>' +
				'<tpl if="(mobEdu.util.isValueExists(info) === false)">',
				'<tpl if="(mobEdu.acc.f.isSummaryExists(summary) === true)">',
				'<td width="40%">{[this.getDescription(values.description)]}</td>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(charges,dueDate) === true)">',
				'<td width="10%" align="right">{charges}</td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(dueDate) === true)">',
				'<td width="10%" align="right">{dueDate}</td>' +
				'<tpl else>',
				'<td width="10%" align="right">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(payments,amount) === true)">',
				'<td width="10%" align="right">{payments}</td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(amount) === true)">',
				'<td width="10%" align="right">{amount}</td>' +
				'<tpl else>',
				'<td width="10%" align="right">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.util.isValueExists(due) === true)">',
				'<td width="10%" align="right">{due}</td>' +
				'<tpl else>',
				'<td width="10%" align="right">&nbsp;</td>' +
				'</tpl>' +
				'<tpl else>' +
				'<td width="40%"><b>{[this.getDescription(values.description)]}</b></td>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(charges,dueDate) === true)">',
				'<td width="10%" align="right"><b>{charges}</b></td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(dueDate) === true)">',
				'<td width="10%" align="right"><b>{dueDate}</b></td>' +
				'<tpl else>',
				'<td width="10%" align="right">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.acc.f.isPaymentExists(payments,amount) === true)">',
				'<td width="10%" align="right"><b>{payments}</b></td>' +
				'<tpl else>',
				'<tpl if="(mobEdu.util.isValueExists(amount) === true)">',
				'<td width="10%" align="right"><b>{amount}</b></td>' +
				'<tpl else>',
				'<td width="10%" align="right">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl if="(mobEdu.util.isValueExists(due) === true)">',
				'<td width="10%" align="right"><b>{due}</b></td>' +
				'<tpl else>',
				'<td width="10%" align="right">&nbsp;</td>' +
				'</tpl>' +
				'</tpl>' +
				'<tpl else>' +
				'<td width="100%" colspan="4">{[this.getDescription(values.info)]}</td>' +
				'</tpl>' +
				'</tr></table>', {
					compiled: true,
					getDescription: function(description) {
						return mobEdu.util.convertRelativeToAbsoluteUrls(description);
					}
				}
			),
			store: mobEdu.util.getStore('mobEdu.acc.store.ebillSummary'),
			disableSelection: true
		}, {
			xtype: 'toolbar',
			id: 'tabSumtBar',
			name: 'tabSumtBar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				ui: 'button',
				text: 'Make Payment',
				handler: function() {
					mobEdu.acc.f.showEPaymentView();
				}
			}, {
				ui: 'button',
				text: 'Refresh',
				handler: function() {
					mobEdu.acc.f.loadEbillByTerm();
				}
			}]
		}, {
			xtype: 'label',
			id: 'ebillInfo',
			name: 'ebillInfo',
			docked: 'bottom'
		}]
	}
});