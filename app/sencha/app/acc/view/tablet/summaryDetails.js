Ext.define('mobEdu.acc.view.tablet.summaryDetails', {
    extend:'Ext.Panel',

    config:{
        id:'sumDetailsPopup',
        scroll:'vertical',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'75%',
        height:'60%',
        layout:'fit',
        items:[{
               title:'<h1>Student Details</h1>',
               id:'stuDetTitle',
               xtype:'toolbar',
               ui:'light',
               docked:'top'  
            },
            {
                xtype:'tabletSumGrid',
                id:'summaryDet',
                name:'summaryDet'
            }
        ],
        flex:1
    }

});
