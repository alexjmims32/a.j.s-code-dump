Ext.define('mobEdu.acc.view.tablet.ebillGrid', {
	extend: 'Ext.ux.touch.grid.View',
	xtype: 'tabletEbillSumGrid',
	requires: [
		'mobEdu.acc.store.ebillSummary'
	],
	config: {
		title: 'Grid',
		store: true,
		columns: [
			{
				header: 'Description',
				dataIndex: 'description',
				style: 'text-align: left;padding-left: 1em;',
				width: '30%'
			}, {
				header: 'Charges',
				dataIndex: 'charges',
				style: 'text-align: center;padding-right: 1em;',
				width: '15%'
			},
			{
				header: 'Credits',
				dataIndex: 'payments',
				style: 'text-align: center;padding-right: 1em;',
				width: '15%'
			},
                        {
				header: 'Due',
				dataIndex: 'due',
				style: 'text-align: center;padding-right: 1em;',
				width: '15%'
			}
		],
		features: [
			{
				ftype: 'Ext.ux.touch.grid.feature.Editable',
				launchFn: 'initialize'
			}
		]
        },	
	applyStore: function() {
		return mobEdu.util.getStore('mobEdu.acc.store.ebillSummary');
	}
});
