Ext.define('mobEdu.acc.view.billingCounselorInfo', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.acc.store.bcDetails'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            setHtmlContent: true,
            padding: '10 10 10 10',
            xtype: 'dataview',
            id: 'billingCounselorDetails',
            cls: 'logo',
            disableSelection: true,
            itemTpl: '<div style="padding:10px;"><div><b><h2>Billing Counsler Details:</h2></b></div><p>{[mobEdu.acc.f.formatComments(values.comments)]}</p></div>',
            store: mobEdu.util.getStore('mobEdu.acc.store.bcDetails'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Counselor Details</h1>'
        }],
        flex: 1
    }
});
