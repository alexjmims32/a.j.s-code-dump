Ext.define('mobEdu.acc.model.summaryList', {
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'payments',
            type : 'array',
            mapping:'payments'
        },{
            name : 'charges',
            type : 'array',
            mapping:'charges'
        },{
            name : 'due',
            type : 'array',
            mapping:'due'
        }]
    }

});