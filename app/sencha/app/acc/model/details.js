Ext.define('mobEdu.acc.model.details', {
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'amount',
            type : 'string'
        },
        {
            name : 'description',
            type : 'string'
        },{
            name : 'category',
            type : 'string'
        }]
    }

});