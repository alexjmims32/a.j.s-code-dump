Ext.define('mobEdu.acc.model.ebillSummary', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'description',
            mapping: 'DETAIL_CODE_DESCRIPTION'
        }, {
            name: 'studentId',
            mapping: 'STUDENT'
        }, {
            name: 'studentName',
            mapping: 'STUDENT_NAME'
        }, {
            name: 'termCode',
            mapping: 'TERM'
        }, {
            name: 'termDesc',
            mapping: 'TERM_DESC'
        }, {
            name: 'status',
            mapping: 'TITLE_IV_STATUS'
        }, {
            name: 'asOnDate',
            mapping: 'AS_ON_DATE'
        }, {
            name: 'payments',
            mapping: 'PAYMENT',
            convert: function(value, record) {
                if (value == '' || value == null) {
                    return '';
                }
                if (isNaN(value)) {
                    return value;
                } else {
                    value = parseFloat(value);
                    value = value.toFixed(2);
                    return '$' + value;
                }
            }
        }, {
            name: 'charges',
            mapping: 'CHARGE',
            convert: function(value, record) {
                if (value == '' || value == null) {
                    return '';
                }
                if (isNaN(value)) {
                    return value;
                } else {
                    value = parseFloat(value);
                    value = value.toFixed(2);
                    return '$' + value;
                }
            }
        }, {
            name: 'due',
            mapping: 'BALANCE',
            convert: function(value, record) {
                if (value == '' || value == null) {
                    return '';
                }
                if (isNaN(value)) {
                    return value;
                } else {
                    value = parseFloat(value);
                    value = value.toFixed(2);
                    return '$' + value;
                }
            }
        }, {
            name: 'details',
            mapping: 'CODE_DETAILS'
        }, {
            name: 'group',
            mapping: 'GROUP'
        }, {
            name: 'summary',
            mapping: 'SUMMARY'
        }, {
            name: 'dueDate',
            mapping: 'DUE_DATE'
        }, {
            name: 'amount',
            mapping: 'AMOUNT',
            convert: function(value, record) {
                if (value == '' || value == null) {
                    return '';
                }
                if (isNaN(value)) {
                    return value;
                } else {
                    value = parseFloat(value);
                    value = value.toFixed(2);
                    return '$' + value;
                }
            }
        }, {
            name: 'info',
            mapping: 'INFO'
        }, {
            name: 'seqNum',
            type: 'int',
            mapping: 'SEQ_NO'
        }, {
            name: 'comments',
            mapping: 'comments'
        }]
    }

});
