Ext.define('mobEdu.acc.model.paymentCard', {
	extend: 'Ext.data.Model',
	config: {
		fields: [{
				name: 'cardName',
				type: 'string'
			},
			{
				name: 'cardType',
				type: 'string'
			}, {
				name: 'cardLength',
				type: 'string'
			}]
	}
});
