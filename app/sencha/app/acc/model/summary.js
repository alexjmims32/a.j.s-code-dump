Ext.define('mobEdu.acc.model.summary', {
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'title',
            type : 'string'
        },
        {
            name : 'totalAmount',
            type : 'string'
        }]
    }

});