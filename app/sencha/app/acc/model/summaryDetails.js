Ext.define('mobEdu.acc.model.summaryDetails', {
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'termCode'
        },{
            name : 'amount'
        },{
            name : 'balance'
        },{
            name : 'billDate'
        },{
            name : 'dueDate'
        },{
            name : 'termDescription'
        },{
            name : 'category'
        },{
            name : 'categoryDescription'
        }]
    }

});