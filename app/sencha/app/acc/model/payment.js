Ext.define('mobEdu.acc.model.payment', {
	extend: 'Ext.data.Model',
	config: {
		fields: [{
				name: 'url',
				type: 'string'
			},
			{
				name: 'status',
				type: 'string'
			}]
	}

});