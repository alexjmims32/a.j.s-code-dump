Ext.define('mobEdu.finaid.f', {
    requires: [
        'mobEdu.finaid.store.menu',
        'mobEdu.finaid.store.status',
        'mobEdu.finaid.store.statusLocal',
        'mobEdu.finaid.store.loanHistory',
        'mobEdu.finaid.store.loanHistoryLocal',
        'mobEdu.finaid.store.awards',
        'mobEdu.finaid.store.resources',
        'mobEdu.finaid.store.termsConditions',
        'mobEdu.finaid.store.costAttendance',
        'mobEdu.finaid.store.requirements',
        'mobEdu.finaid.store.awardsLocal',
        'mobEdu.finaid.store.awardStatus',
        'mobEdu.finaid.store.links',
        'mobEdu.finaid.store.paymentSchedule',
        'mobEdu.finaid.store.holdsInfo',
        'mobEdu.finaid.store.viewAppts',
        'mobEdu.finaid.store.emailsList',
        'mobEdu.finaid.store.viewEmail',
        'mobEdu.finaid.store.statusaward',
        'mobEdu.finaid.store.appAcceptance',
        'mobEdu.finaid.store.studentDetails'
    ],
    statics: {
        code: null,
        aidyCode: null,
        fundCode: null,
        value: null,
        amt: null,
        studentId: null,
        awardsPrevView: null,
        showFinMenu: null,
        cmp: null,
        studentMsgId: null,
        loadFinaidDropdown: function(code, nextView) {
            // mobEdu.finaid.f.studentId = mobEdu.main.f.getStudentId();
            if (Ext.os.is.Phone) {
                mobEdu.util.get('mobEdu.finaid.view.phone.menu');
                mobEdu.finaid.f.cmp = Ext.getCmp('finaidCodes');
            } else {
                mobEdu.util.get('mobEdu.finaid.view.tablet.menu');
                mobEdu.finaid.f.cmp = Ext.getCmp('finaidCodesT');
            }

            var statusStore = mobEdu.util.getStore('mobEdu.finaid.store.status');
            if (statusStore.data.length > 0) {
                statusStore.removeAll();
            }
            var statusStoreLocal = mobEdu.util.getStore('mobEdu.finaid.store.statusLocal')
            if (statusStoreLocal.data.length > 0) {
                statusStoreLocal.removeAll();
                statusStoreLocal.removed = [];
            }
            (mobEdu.finaid.f.cmp).suspendEvents();
            var store = mobEdu.util.getStore('mobEdu.finaid.store.menu');
            store.getProxy().setUrl(webserver + 'getFinAidMenu');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            if (nextView == undefined || nextView == null) {
                mobEdu.counselor.f.role=null;
            }else{
                mobEdu.counselor.f.role='counselor';
            }
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                role: mobEdu.counselor.f.role,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.finaidDropdownResponseHandler(nextView);
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidMenu');
        },

        finaidDropdownResponseHandler: function(nextView) {
            if (nextView == undefined || nextView == null) {
                (mobEdu.finaid.f.cmp).resumeEvents(true);
                var value = (mobEdu.finaid.f.cmp).getValue();
                if (value !== null && value !== '') {
                    mobEdu.finaid.f.aidyCode = value;
                    mobEdu.finaid.f.onCodeChange(value);
                } else {
                    mobEdu.finaid.f.aidyCode = null;
                    Ext.Msg.show({

                        message: '<p>You have no Financial Aid terms.</p>',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox',
                        fn: function(btn) {
                            mobEdu.util.showMainView();

                        }
                    });
                }
                mobEdu.counselor.f.role = null;
                mobEdu.finaid.f.showMenu();
                mobEdu.finaid.f.awardsPrevView = null;
            } else {
                mobEdu.counselor.f.role = 'counselor';
                mobEdu.finaid.f.awardsPrevView = mobEdu.counselor.f.showDetails;
                (nextView)();
            }
        },

        onCodeChange: function(code) {
            mobEdu.finaid.f.aidyCode = code;
            var store = mobEdu.util.getStore('mobEdu.finaid.store.status');
            if (store.data.length > 0) {
                store.removeAll();
            }
            var statusStoreLocal = mobEdu.util.getStore('mobEdu.finaid.store.statusLocal')
            if (statusStoreLocal.data.length > 0) {
                statusStoreLocal.removeAll();
                statusStoreLocal.removed = [];
            }
            store.getProxy().setUrl(webserver + 'getFinAidStatus');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.codeChangeResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidStatus');
        },

        codeChangeResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.status');
            var localStore = mobEdu.util.getStore('mobEdu.finaid.store.statusLocal');
            var group = '';
            var awardsTot;
            var loanHistory;
            if (localStore.data.length == 0) {
                if (Ext.os.is.Phone) {
                    Ext.getCmp('finaidList').setEmptyText('<h3 align="center">No data available</h3>');
                }
            }
            if (localStore.data.length > 0) {
                localStore.removeAll();
                localStore.sync();
                localStore.removed = [];
            }
            store.each(function(record) {
                var value;

                value = record.get('budjetAmt');
                if (value === '' || value == null || value === 'NaN') {
                    value = 0;
                }
                value = parseFloat(value);
                value = value.toFixed(2);

                if (record.get('group') == undefined) {
                    group = '';
                } else {
                    group = record.get('group') + '<br><b>(Budget Group)</b></h6>';;
                }
                localStore.add({
                    description: 'Cost of Attendance <br> <h6>' + group + '',
                    amount: '$' + mobEdu.util.formatCurrency(value),
                    next: 1,
                    action: mobEdu.finaid.f.loadCostAttendance
                });


                value = record.get('awardAmt')
                value = parseFloat(value);
                value = value.toFixed(2);
                var amtt = record.get('holds')
                amtt = parseFloat(amtt);
                amtt = amtt.toFixed(2);
                awardsTot = '$' + mobEdu.util.formatCurrency(value);

                localStore.add({
                    description: 'Awards',
                    amount: '$' + mobEdu.util.formatCurrency(value),
                    next: 1,
                    action: mobEdu.finaid.f.loadAwards
                });

                localStore.add({
                    description: 'Resources',
                    next: 1,
                    action: mobEdu.finaid.f.loadResources
                });


                // localStore.add({
                //     description: 'Academic Progress <br> <h5>' + record.get('status') + '</h5>',
                //     amount: '',
                //     next: 0
                // });

                if (!record.get('requirements').match('0/0') || !record.get('requirements').match('/')) {
                    localStore.add({
                        description: 'Requirements',
                        amount: record.get('requirements'),
                        next: 1,
                        action: mobEdu.finaid.f.loadRequirements
                    });
                }

                if (amtt > 0) {
                    localStore.add({
                        description: 'You Have Holds',
                        amount: record.get('holds'),
                        next: 1,
                        action: mobEdu.finaid.f.loadHolds
                    });
                } else {
                    localStore.add({
                        description: 'You Have No Holds',
                        amount: '',
                        next: 0
                    });
                }

                // localStore.add({
                //     description: 'Account Summary',
                //     amount: '',
                //     next: 1,
                //     action: mobEdu.finaid.f.loadAccMenu
                // });

                value = record.get('loanHistory')
                value = parseFloat(value);
                value = value.toFixed(2);
                loanHistory = '$' + mobEdu.util.formatCurrency(value);

                localStore.add({
                    description: 'Loan History',
                    amount: '$' + mobEdu.util.formatCurrency(value),
                    next: 1,
                    action: mobEdu.finaid.f.loadLoanHistory
                });

                value = record.get('paymentAmount')
                value = parseFloat(value);
                value = value.toFixed(2);
                localStore.add({
                    description: 'Payment Schedule',
                    amount: '$' + mobEdu.util.formatCurrency(value),
                    next: 1,
                    action: mobEdu.finaid.f.loadPaymentSchedule
                });

                localStore.add({
                    description: 'Get Financial Aid',
                    amount: '',
                    next: 1,
                    action: mobEdu.finaid.f.loadLinks
                });

                localStore.add({
                    description: 'Appointments',
                    amount: '',
                    next: 1,
                    action: mobEdu.finaid.f.loadAppointments
                });

                localStore.add({
                    description: 'Messages',
                    amount: '',
                    next: 1,
                    action: mobEdu.finaid.f.loadStudentMessages
                });

                localStore.sync();
            });
            if (!Ext.os.is.Phone) {
                Ext.getCmp('finBudgetGrp').setHtml('<h4 align="center">' + group + '</h4>');
                Ext.getCmp('finAwardsTot').setHtml('<br /><h4 align="center"><b>Total - ' + awardsTot + '</b></h4><br />');
                Ext.getCmp('finLoanHisTot').setHtml('<br /><h4 align="center"><b>Total - ' + loanHistory + '</b></h4><br />');
                var cmp = Ext.getCmp('finaidTabs');
                var activeItem = cmp.getInnerItems().indexOf(cmp.getActiveItem());
                if (activeItem == 1) {
                    mobEdu.finaid.f.loadCostAttendance();
                } else {
                    Ext.getCmp('finaidTabs').setActiveItem(1);
                }
            }
        },

        loadHolds: function(prevView, studentId) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.holdsInfo');
            store.removeAll();
            store.removed = [];
            store.sync();
            store.getProxy().setUrl(webserver + 'getFinAidHoldsInfo');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                role: mobEdu.counselor.f.role,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.holdsResponseHandler(prevView);
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidHolds');
        },
        holdsResponseHandler: function(prevView) {
            if (Ext.os.is.Phone) {
                if (prevView == null) {
                    mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
                } else {
                    mobEdu.util.updatePrevView(prevView);
                }
                mobEdu.util.show('mobEdu.finaid.view.phone.holdsInfo');
            }
        },

        loadAppointments: function(prevView, studentId) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.viewAppts');
            if (store.data.length > 0) {
                store.removeAll();
                store.removed = [];
            }
            // if (mobEdu.finaid.f.studentId != null && mobEdu.finaid.f.studentId != '') {

            if (mobEdu.util.getRole() == 'STUDENT') {
                store.getProxy().setUrl(commonwebserver + 'appointment/search');
                store.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
                store.getProxy().setExtraParams({
                    //userID: mobEdu.util.getStudentId(),
                    companyId: companyId,
                    loginID: mobEdu.util.getStudentId()
                });
                store.getProxy().afterRequest = function() {
                    mobEdu.finaid.f.viewAppointmentsResponseHandler(prevView);
                };
                store.load();
            } else {
                store.getProxy().setUrl(commonwebserver + 'appointment/createdsearch');
                store.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
                store.getProxy().setExtraParams({
                    bannerID: mobEdu.finaid.f.studentId,
                    companyId: companyId,
                    loginID: mobEdu.util.getStudentId()
                });
                store.getProxy().afterRequest = function() {
                    mobEdu.finaid.f.viewAppointmentsResponseHandler(prevView);
                };
                store.load();
            }


            mobEdu.util.gaTrackEvent('finaid', 'finViewAppointments');
            // } else {
            //     if (store.data.length > 0) {
            //         store.removeAll();
            //         store.removed = [];
            //     }
            // }
        },

        viewAppointmentsResponseHandler: function(prevView) {
            if (Ext.os.is.Phone) {
                if (prevView == null) {
                    mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
                } else {
                    mobEdu.util.updatePrevView(prevView);
                }
                mobEdu.util.destroyView('mobEdu.finaid.view.phone.appointments');
                mobEdu.util.show('mobEdu.finaid.view.phone.appointments');
            }
        },

        loadStudentMessages: function() {
            mobEdu.finaid.f.getFinStudentId(mobEdu.finaid.f.studentId);
        },

        loadMessages: function(prevView, studentId) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.emailsList');
            if (store.data.length > 0) {
                store.removeAll();
                store.removed = [];
            }
            // if (mobEdu.finaid.f.studentId != null && mobEdu.finaid.f.studentId != '') {
            if (mobEdu.util.getRole() == 'STUDENT') {
                store.getProxy().setUrl(commonwebserver + 'message/myMessages');
                store.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
                store.getProxy().setExtraParams({
                    companyId: companyId,
                    loginID: mobEdu.util.getStudentId()
                });
                store.getProxy().afterRequest = function() {
                    mobEdu.finaid.f.viewMessagesResponseHandler(prevView);
                };
                store.loadPage(1);
            } else {
                store.getProxy().setUrl(commonwebserver + 'message/messageList');
                store.getProxy().setHeaders({
                    Authorization: mobEdu.util.getAuthString(),
                    companyID: companyId
                });
                store.getProxy().setExtraParams({
                    bannerID: mobEdu.finaid.f.studentId,
                    //fromID: mobEdu.util.getStudentId(),
                    reportId: mobEdu.finaid.f.studentMsgId,
                    companyId: companyId,
                    loginID: mobEdu.util.getStudentId()
                });
                store.getProxy().afterRequest = function() {
                    mobEdu.finaid.f.viewMessagesResponseHandler(prevView);
                };
                store.load();
            }

            mobEdu.util.gaTrackEvent('finaid', 'finViewMessages');
            // } else {
            //     if (store.data.length > 0) {
            //         store.removeAll();
            //         store.removed = [];
            //     }
            // }
        },

        viewMessagesResponseHandler: function(prevView) {
            if (Ext.os.is.Phone) {
                if (prevView == null) {
                    mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
                } else {
                    mobEdu.util.updatePrevView(prevView);
                }
                mobEdu.util.show('mobEdu.finaid.view.phone.messages');
            }
        },

        loadEmailDetail: function(record) {
            var rec = record.data;
            rec.readFlag = '1';
            var eId = rec.messageID;
            var detailStore = mobEdu.util.getStore('mobEdu.finaid.store.viewEmail');

            detailStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString()
            });
            detailStore.getProxy().setUrl(commonwebserver + 'message/get');
            detailStore.getProxy().setExtraParams({
                messageID: eId,
                companyId: companyId,
                loginID: mobEdu.util.getStudentId()
            });
            detailStore.getProxy().afterRequest = function() {
                mobEdu.finaid.f.loadEmailDetailResponseHandler();
            };
            detailStore.load();
        },

        loadEmailDetailResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.viewEmail');
            if (store.data.length > 0) {
                if (Ext.os.is.phone) {
                    mobEdu.finaid.f.showDetailMsg();
                } else {
                    mobEdu.finaid.f.showMsgDetailsPopup();
                }
            } else {
                Ext.Msg.show({
                    message: '<p>No messages exists.</p>',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }
        },

        showMsgDetailsPopup: function(popupView) {
            if (!popupView) {
                var popup = popupView = Ext.Viewport.add(
                    mobEdu.util.get('mobEdu.finaid.view.tablet.msgPopup')
                );
                Ext.Viewport.add(popupView);
            }
            popupView.show();
        },

        closeFinMsgDetailPopup: function() {
            var popUp = Ext.getCmp('newMsgDetPopup');
            popUp.hide();
            Ext.Viewport.unmask();
        },

        showDetailMsg: function() {
            mobEdu.util.updatePrevView(mobEdu.finaid.f.showMessageView);
            mobEdu.util.show('mobEdu.finaid.view.emailDetail');
        },

        loadAwards: function(prevView) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.awards');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'getFinAidAwards');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                role: mobEdu.counselor.f.role,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.awardsResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidAwards');

        },

        awardsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.awards');
            var awardsLocalStore = mobEdu.util.getStore('mobEdu.finaid.store.awardsLocal');
            if (awardsLocalStore.data.length > 0) {
                awardsLocalStore.removeAll();
                awardsLocalStore.sync();
                awardsLocalStore.removed = [];
            }
            var awards = new Array();
            var array = new Array();
            if (store.data.length > 0) {
                var groups = new Array();
                store.each(function(outerRecord) {
                    awards = [];
                    if (!(Ext.Array.contains(groups, outerRecord.get('fundDesc')))) {
                        groups.push(outerRecord.get('fundDesc'));
                        store.each(function(innerRecord) {
                            if (innerRecord.get('fundDesc') == outerRecord.get('fundDesc')) {
                                array = [];
                                array.periodDesc = innerRecord.get('periodDesc');
                                array.fundAmt = innerRecord.get('fundAmt');
                                awards.push(array);
                            }
                        });
                        outerRecord.data.item = awards;
                        awardsLocalStore.add(outerRecord.copy());
                        awardsLocalStore.sync();
                    }
                });
                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showAwards();
                } else {
                    mobEdu.util.show('mobEdu.finaid.view.tablet.menu');
                    Ext.getCmp('finAwardsTot').show();
                    mobEdu.finaid.f.onCodeChangeforAward(mobEdu.finaid.f.aidyCode);
                }
            } else {
                if (!Ext.os.is.Phone) {
                    Ext.getCmp('finAwardsTot').hide();
                }
            }
        },

        onAwardItemTap: function(me, index, target, record, e, eOpts) {
            if (e.target.getAttribute("name") === "accept") {
                mobEdu.finaid.f.onAcceptButtonTap(record);
            } else if (e.target.getAttribute("name") === "decline") {
                mobEdu.finaid.f.onDeclineButtonTap(record);
            }
        },

        onAcceptButtonTap: function(record) {
            if (record.get('parrialAccept') == 'N') {
                mobEdu.finaid.f.amt = record.get('offerAmt');
                mobEdu.finaid.f.code = record.get('code');
                mobEdu.finaid.f.fundCode = record.get('fundCode');
                mobEdu.finaid.f.value = 'Accept';
                mobEdu.finaid.f.loadTermsConditions();
                // mobEdu.finaid.f.updateAwardStatus(record.get('code'), record.get('fundCode'), value);
            } else {
                mobEdu.finaid.f.showAlert('Accept', record);
            }
        },

        onDeclineButtonTap: function(record) {
            mobEdu.finaid.f.code = record.get('code');
            mobEdu.finaid.f.fundCode = record.get('fundCode');
            mobEdu.finaid.f.value = 'Decline';
            mobEdu.finaid.f.amt = '0';
            mobEdu.finaid.f.loadTermsConditions();
        },

        showAlert: function(value, record) {
            // var store = mobEdu.util.getStore('mobEdu.finaid.store.awardsLocal');
            // var record = store.getAt(0);
            var min = record.get('fundMinAmt');
            if (min == null || min == "") {
                min = 0;
            }

            var max = record.get('offerAmt');

            if (max == null || max == "") {
                max = 0;
            }
            Ext.Msg.show({
                title: 'Enter amount',
                msg: '',
                prompt: {
                    xtype: 'numberfield'
                },
                value: max,
                cls: 'msgbox',
                buttons: Ext.MessageBox.OKCANCEL,
                fn: function(btn, amt) {
                    if (btn == 'ok') {
                        if ((amt == null || amt == "") || (amt < parseInt(min) || amt > parseInt(max))) {
                            if (parseInt(min) < parseInt(max)) {
                                Ext.Msg.show({
                                    message: '<p>Please enter the amount between $' + min + ' and $' + max + '</p>',
                                    buttons: Ext.MessageBox.OK,
                                    cls: 'msgbox',
                                    fn: function(btn) {
                                        mobEdu.finaid.f.showAlert();
                                    }
                                });
                            } else {
                                mobEdu.finaid.f.code = record.get('code');
                                mobEdu.finaid.f.fundCode = record.get('fundCode');
                                mobEdu.finaid.f.value = value;
                                mobEdu.finaid.f.amt = amt;
                                mobEdu.finaid.f.loadTermsConditions();
                            }
                        } else {
                            mobEdu.finaid.f.code = record.get('code');
                            mobEdu.finaid.f.fundCode = record.get('fundCode');
                            mobEdu.finaid.f.value = value;
                            mobEdu.finaid.f.amt = amt;
                            mobEdu.finaid.f.loadTermsConditions();
                        }
                    }
                }
            });
        },

        getStudentId: function(studentId) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.studentDetails');
            store.getProxy().setUrl(webserver + 'getLdapId');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.studentResponseHandler();
            };
            store.getProxy().setExtraParams({
                reportId: studentId,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'ldapId');
        },

        studentResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.studentDetails');
            var storeData = store.data.all[0];
            var ldapId = null;
            if (store.data.length > 0) {
                ldapId = storeData.data.ldapId;
                mobEdu.advising.f.studentMsgId = ldapId;
            } else {
                mobEdu.advising.f.studentMsgId = mobEdu.advising.f.studentId;
            }
            mobEdu.advising.f.createMessage();
        },

        getFinStudentId: function(studentId) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.studentDetails');
            if (store.data.length > 0) {
                store.removeAll();
                store.removed = [];
            }
            store.getProxy().setUrl(webserver + 'getLdapId');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.finStudentResponseHandler();
            };
            store.getProxy().setExtraParams({
                reportId: studentId,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'finLdapId');
        },

        finStudentResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.studentDetails');
            var storeData = store.data.all[0];
            var ldapId = null;
            if (store.data.length > 0) {
                ldapId = storeData.data.ldapId;
                mobEdu.finaid.f.studentMsgId = ldapId;
            } else {
                mobEdu.finaid.f.studentMsgId = mobEdu.finaid.f.studentId;
            }
            mobEdu.finaid.f.loadMessages();
        },

        loadTermsConditions: function() {
            var code = (mobEdu.finaid.f.cmp).getValue();
            var store = mobEdu.util.getStore('mobEdu.finaid.store.termsConditions');
            store.getProxy().setUrl(webserver + 'getFinAidTermsConditions');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.termsConditionsResponseHandler();
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidTermsConditions');
        },

        termsConditionsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.termsConditions');
            if (store.data.length > 0) {
                var indicator = store.getAt(0).get('acceptInd');
                if (indicator != undefined) {
                    if (indicator == 'Y') {
                        mobEdu.reg.f.showTermsAndConditions();
                        if (Ext.os.is.Phone) {
                            mobEdu.util.updatePrevView(mobEdu.finaid.f.showAwards);
                        } else {
                            mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
                        }
                        mobEdu.reg.view.acceptAction = mobEdu.finaid.f.acceptTerms;
                        mobEdu.reg.view.params = null;
                        Ext.getCmp('aboutTandC').setHtml('<p>' + (store.getAt(0)).get('text') + '</p>');
                    } else {
                        mobEdu.finaid.f.updateAwardStatus();
                    }
                } else {
                    Ext.Msg.show({
                        title: null,
                        cls: 'msgbox',
                        message: '<p> Unable to get Term and conditions page </p>'
                    });
                }

            }
        },

        acceptTerms: function() {
            var code = (mobEdu.finaid.f.cmp).getValue();
            var store = mobEdu.util.getStore('mobEdu.finaid.store.termsConditions');
            store.getProxy().setUrl(webserver + 'acceptFinAidTerms');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.acceptTermsResponseHandler();
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidTermsConditions');
        },

        acceptTermsResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.termsConditions');
            if (store.data.length > 0) {
                var errorMsg = (store.getAt(0)).get('errorMsg');
                if (errorMsg == 'Terms and Conditions Successfully updated.') {
                    Ext.Msg.show({
                        title: null,
                        message: 'Terms and Conditions Successfully updated.',
                        cls: 'msgbox',
                        fn: function(btn) {
                            if (btn == 'ok') {
                                mobEdu.finaid.f.updateAwardStatus();
                            }
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title: null,
                        cls: 'msgbox',
                        message: '<p>' + errorMsg + '</p>'
                    });
                }
            }
        },

        updateAwardStatus: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.awardStatus');
            store.getProxy().setUrl(webserver + 'updateFinAidAwardStatus');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                code: mobEdu.finaid.f.code,
                fundCode: mobEdu.finaid.f.fundCode,
                statusCode: mobEdu.finaid.f.value,
                amount: mobEdu.finaid.f.amt,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.updateAwardStatusResponseHandler();
            };
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidAwardstatus');

        },

        updateAwardStatusResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.awardStatus');
            if (store.data.length > 0) {
                var record = store.getAt(0);
                if (record.get('status') == 'success') {
                    Ext.Msg.show({
                        title: null,
                        cls: 'msgbox',
                        message: '<p>Updated status successfully.</p>',
                        buttons: Ext.MessageBox.OK,
                        fn: function(btn) {
                            mobEdu.finaid.f.loadAwards();
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title: null,
                        cls: 'msgbox',
                        message: '<p>' + record.get('errorMsg') + '</p>',
                        buttons: Ext.MessageBox.OK
                    });
                }
            }
        },

        loadResources: function() {
            var code = (mobEdu.finaid.f.cmp).getValue();
            var store = mobEdu.util.getStore('mobEdu.finaid.store.resources');
            store.removeAll();
            store.removed = [];
            store.sync();
            store.getProxy().setUrl(webserver + 'getFinAidResources');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.resourcesResponseHandler();
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidResources');

        },

        resourcesResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.resources');
            var amount = 0;
            if (store.data.length > 0) {
                store.each(function(record) {
                    var expAmt = record.get('expectedAmt');
                    if (expAmt != null && expAmt != '' && expAmt != 'NaN') {
                        amount = amount + parseFloat(expAmt);
                    }
                });
                amount = amount.toFixed(2);
                store.add({
                    description: 'Total',
                    expectedAmt: amount
                });

                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showResources();
                }
            }
        },

        loadCostAttendance: function(prevView) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.costAttendance');
            if (store.data.length > 0) {
                store.removeAll();
                store.sync();
                store.removed = [];
            }
            store.getProxy().setUrl(webserver + 'getCostAttendance');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.costAttendanceResponseHandler(prevView);
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                role: mobEdu.counselor.f.role,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidCostAttendance');
        },

        costAttendanceResponseHandler: function(prevView) {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.costAttendance');
            if (store.data.length > 0) {
                var tot = 0;

                store.each(function(record) {
                    tot = tot + parseFloat(record.get('amt'))
                });

                store.add({
                    component: 'Total',
                    amt: tot
                });
                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showCostAttendance(prevView);
                } else {
                    Ext.getCmp('finBudgetGrp').show();
                }
            } else {
                if (!Ext.os.is.Phone) {
                    Ext.getCmp('finBudgetGrp').hide();
                }
            }
        },

        loadRequirements: function(prevView) {
            var finAidLable = (mobEdu.finaid.f.cmp).getRecord().data.description;
            var store = mobEdu.util.getStore('mobEdu.finaid.store.requirements');
            store.getProxy().setUrl(webserver + 'getFinAidRequirements');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showRequirements(finAidLable, prevView);
                }
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                role: mobEdu.counselor.f.role,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidRequirements');
        },

        loadPaymentSchedule: function() {
            var code = (mobEdu.finaid.f.cmp).getValue();
            var store = mobEdu.util.getStore('mobEdu.finaid.store.paymentSchedule');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'getFinAidPaymentSchedule');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.paymentScheduleResponseHandler();
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidPaymentSchedule');
        },

        paymentScheduleResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.paymentSchedule');
            if (store.data.length > 0) {
                store.add({
                    fundDesc: 'Total',
                    total: store.getAt(0).get('total')
                });
                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showPaymentSchedule();
                }
            }
        },

        loadLoanHistory: function() {
            var code = (mobEdu.finaid.f.cmp).getValue();
            var store = mobEdu.util.getStore('mobEdu.finaid.store.loanHistory');
            store.getProxy().setUrl(webserver + 'getFinAidLoanHistory');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.loanHistoryResponseHandler();
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidLoanHistory');
        },

        loanHistoryResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.loanHistory');
            if (store.data.length > 0) {
                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showLoanHistory();
                } else {
                    Ext.getCmp('finLoanHisTot').show();
                }
            } else {
                if (!Ext.os.is.Phone) {
                    Ext.getCmp('finLoanHisTot').hide();
                }
            }
        },

        loadLinks: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.links');
            store.getProxy().setUrl(webserver + 'getFinAidLinks');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().afterRequest = function() {
                if (Ext.os.is.Phone) {
                    mobEdu.finaid.f.showLinks();
                }
            };
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                reportId: mobEdu.finaid.f.studentId,
                companyId: companyId
            });
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'financialaidLinks');
        },

        loadAccMenu: function() {
            mobEdu.finaid.f.showFinMenu = mobEdu.finaid.f.showMenu;
            mobEdu.acc.f.loadFinMenu(null, mobEdu.finaid.f.showMenu)
        },

        showMenu: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            if (Ext.os.is.Phone) {
                mobEdu.util.show('mobEdu.finaid.view.phone.menu');
            } else {
                mobEdu.util.show('mobEdu.finaid.view.tablet.menu');
                Ext.getCmp('counToolbar').hide();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(0).hide();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(6).show();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(7).show();
                Ext.getCmp('finaidTabs').getTabBar().getComponent(8).show();
                Ext.getCmp('finaidCodesT').setStyle('display:flex;');
                Ext.getCmp('finaidTitleT').setTitle('<h1>Financial Aid</h1>');
            }
        },

        showAwards: function() {
            if (mobEdu.finaid.f.awardsPrevView == null) {
                mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            } else {
                mobEdu.util.updatePrevView(mobEdu.finaid.f.awardsPrevView);
            }
            mobEdu.util.show('mobEdu.finaid.view.phone.awards');
        },

        showCostAttendance: function(prevView) {
            if (prevView == null) {
                mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            } else {
                mobEdu.util.updatePrevView(prevView);
            }
            mobEdu.util.show('mobEdu.finaid.view.phone.costAttendance');
            var costStore = mobEdu.util.getStore('mobEdu.finaid.store.status');
            var costData = costStore.data.all[0];
            // var costTitle = costData.data.group;
            // Ext.getCmp('costTitle').setTitle('<h1>' + costTitle + '</h1>');
        },

        showRequirements: function(finAidLable, prevView) {
            if (prevView == null) {
                mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            } else {
                mobEdu.util.updatePrevView(prevView);
            }
            mobEdu.util.show('mobEdu.finaid.view.phone.requirements');
            Ext.getCmp('customRequire').setTitle('<h1> Requirements </h1>');
        },

        showPaymentSchedule: function() {
            mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            mobEdu.util.show('mobEdu.finaid.view.phone.paymentSchedule');
        },

        showLoanHistory: function() {
            mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            mobEdu.util.show('mobEdu.finaid.view.phone.loanHistory');
        },

        showLinks: function() {
            mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            mobEdu.util.show('mobEdu.finaid.view.phone.links');
        },

        showResources: function() {
            mobEdu.util.updatePrevView(mobEdu.finaid.f.showMenu);
            mobEdu.util.show('mobEdu.finaid.view.phone.resources');
        },

        onCodeChangeforAward: function(code) {
            mobEdu.finaid.f.aidyCode = code;
            var store = mobEdu.util.getStore('mobEdu.finaid.store.statusaward');
            if (store.data.length > 0) {
                store.removeAll();
            }
            store.getProxy().setUrl(webserver + 'getFinAidStatus');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                code: mobEdu.finaid.f.aidyCode,
                companyId: companyId
            });
            store.getProxy().afterRequest = function() {
                mobEdu.finaid.f.codeChangeawardResponseHandler();
            };
            store.load();
        },

        codeChangeawardResponseHandler: function() {
            var store = mobEdu.util.getStore('mobEdu.finaid.store.statusaward');
            if (store.data.length > 0) {
                var record = store.data.items[0];
                var awardamount = record.get('awardAmt');
                value = parseFloat(awardamount);
                value = value.toFixed(2);
                value = '$' + mobEdu.util.formatCurrency(value);
                if (mobEdu.counselor.f.role == 'counselor') {
                    Ext.getCmp('finAwardsTot').setHtml('<br /><h4 align="center"><b>Total - ' + mobEdu.counselor.f.awardsTot + '</b></h4><br />');
                } else {
                    Ext.getCmp('finAwardsTot').setHtml('<br /><h4 align="center"><b>Total - ' + value + '</b></h4><br />');
                }

            }
        },


        showMessageView: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.finaid.view.phone.messages');
        },
        appTap: function(me, index, target, record, e, eOpts) {
            if (e.target.getAttribute("name") === "accept") {
                var tap = e.target.getAttribute("name");
                mobEdu.finaid.f.acceptAppointment(tap, record);
            } else if (e.target.getAttribute("name") === "decline") {
                var tap = e.target.getAttribute("name");
                mobEdu.finaid.f.acceptAppointment(tap, record);
            }
        },
        acceptAppointment: function(value, record) {
            var sAcceptance = null;
            var role = mobEdu.util.getRole();
            var status;
            if (value == 'accept') {
                sAcceptance = 'Y';
                status = 'Scheduled';
            } else {
                sAcceptance = 'N';
                status = 'Cancelled';

            };
            var appId = record.data.appointmentID;
            var store = mobEdu.util.getStore('mobEdu.finaid.store.appAcceptance');
            store.getProxy().setUrl(commonwebserver + 'appointment/statusUpdate');
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setExtraParams({
                appointmentID: appId,
                acceptance: sAcceptance,
                companyId: companyId,
                status: status
            });
            store.getProxy().afterRequest = function() {
                mobEdu.enroute.main.f.acceptanceResponseHandler();
            };
            store.load();
            if (value == 'accept') {
                Ext.Msg.show({
                    id: 'aAccept',
                    title: null,
                    cls: 'msgbox',
                    message: '<p>Appointment is Accepted.</p>',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            mobEdu.finaid.f.loadAppointments();
                        }
                    }
                });
            }
            if (value == 'decline') {
                Ext.Msg.show({
                    id: 'aDecline',
                    title: null,
                    cls: 'msgbox',
                    message: '<p>Appointment is Declined.</p>',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            mobEdu.finaid.f.loadAppointments();
                        }
                    }
                });
            }
        },
        acceptanceResponseHandler: function() {
            var newAppStore = mobEdu.util.getStore('mobEdu.finaid.store.appAcceptance');
        },

        showCounCalendar: function() {
            mobEdu.finaid.f.loadCalendarView();
            mobEdu.util.updatePrevView(mobEdu.counselor.f.showDetails);
            mobEdu.util.show('mobEdu.finaid.view.appointmentsCalendar');
        },

        loadCalendarView: function() {
            mobEdu.main.f.calendarInstance = 'finAppt';
            mobEdu.util.get('mobEdu.finaid.view.appointmentsCalendar');
            var appointmentDock = Ext.getCmp('finAppointmentsDock');
            appointmentDock.remove(calendar);

            calendar = Ext.create('Ext.ux.TouchCalendarView', {
                id: 'finCalend',
                mode: 'month',
                weekStart: 0,
                value: new Date(),
                minHeight: '230px'
            });
            appointmentDock.add(calendar);
            //Set scheduleInfo for First time
            mobEdu.finaid.f.setAppointmentScheduleInfo(new Date());
        },
        setAppointmentScheduleInfo: function(selectedDate) {
            if (selectedDate == null || selectedDate == undefined) {
                selectedDate = new Date();
            }
            var newDateValue = (mobEdu.finaid.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.finaid.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());
            var scheduleInfo = '';
            var appointmentsStore = mobEdu.util.getStore('mobEdu.finaid.store.viewAppts')
            appointmentsStore.each(function(rec) {
                var appDate = rec.get('appointmentDate');
                var appDateValue = appDate.split(' ');
                var dateFormatted = appDateValue[0];
                var appDesc = ' ';
                if (newDateValue == dateFormatted) {
                    var appTime = appDateValue[1];
                    var appSubject = rec.get('subject');
                    var appLocation = rec.get('location');
                    scheduleInfo = mobEdu.finaid.f.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc);
                }
            });

            //set the event schedule info in calendar view label
            Ext.getCmp('finAppointmentLabel').setHtml(scheduleInfo);
        },
        formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc) {
            //var user = mobEdu.util.getStore('mobEdu.enquire.login.store.login').data.first().raw.userInfo.userName;
            if (appSubject == null) {
                appSubject = '';
            }
            if (appTime == null) {
                appTime = '';
            }
            if (appLocation == null) {
                appLocation = '';
            }
            scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + decodeURIComponent(appSubject) + '</h3>';
            scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
            scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + decodeURIComponent(appLocation) + '</h3>';
            scheduleInfo = scheduleInfo + '<br></br>';
            return scheduleInfo;
        },
        doHighlightConAppntsCalenderViewCell: function(classes, values, highlightedItemCls) {
            //get the store info
            var store = mobEdu.util.getStore('mobEdu.finaid.store.viewAppts');
            if (store.data.length > 0) {
                var sData = store.data.all;
                //var user = mobEdu.util.getStore('mobEdu.enquire.login.store.login').data.first().raw.userInfo.userName;
                //get the meeting info
                for (var a = 0; a < sData.length; a++) {
                    //if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
                    var appDate = sData[a].data.appointmentDate;
                    var appDateValue = appDate.split(' ');
                    var dateFormatted = appDateValue[0];
                    var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
                    if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
                        classes.push(highlightedItemCls);
                    }
                    //Incrementing startDate
                    // newStartDate = new Date(newStartDate.getTime() + 86400000);
                }
            }
            return classes
        },

        nextPreviousMonthChecking: function(date) {
            var presentDate = date;
            var presentDateValue;
            var currentDate = new Date();
            var currentDateValue;
            if (currentDate == null) {
                currentDateValue = '';
            } else {
                currentDateValue = (mobEdu.finaid.f.dateChecking([currentDate.getMonth() + 1]) + '/' + mobEdu.finaid.f.dateChecking(currentDate.getDate()) + '/' + currentDate.getFullYear());
            }

            if (presentDate == null) {
                presentDateValue = '';
            } else {
                presentDateValue = (mobEdu.finaid.f.dateChecking([presentDate.getMonth() + 1]) + '/' + mobEdu.finaid.f.dateChecking(presentDate.getDate()) + '/' + presentDate.getFullYear());
            }
            if (currentDateValue == presentDateValue) {
                mobEdu.finaid.f.setAppointmentScheduleInfo(date)
            }
        },

        dateChecking: function(val) {
            if (val < 10) {
                val = '0' + val;
            }
            return val;
        }
    }
});