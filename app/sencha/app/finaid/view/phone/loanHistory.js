Ext.define('mobEdu.finaid.view.phone.loanHistory', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Loan History</h1>'
        }, {
            xtype: 'loanHistory',            
            id: 'loanHistoryList'
        }],
        flex: 1
    }
});