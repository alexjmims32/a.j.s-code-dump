Ext.define('mobEdu.finaid.view.phone.resources', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title:'<h1>Resources</h1>'
        },{
            xtype: 'resources',
            id: 'resourcesList',
        } ],
        flex: 1
    }
});