Ext.define('mobEdu.finaid.view.phone.requirements', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id:'customRequire',
        },{
            xtype: 'requirements',
            id: 'requirementList',
        } ],
        flex: 1
    }
});