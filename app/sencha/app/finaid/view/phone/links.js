Ext.define('mobEdu.finaid.view.phone.links', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title:'<h1>General Links</h1>'
        },{
            xtype: 'links',
            id: 'linkList'           
        } ],
        flex: 1
    }
});