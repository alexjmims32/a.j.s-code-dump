Ext.define('mobEdu.finaid.view.phone.messages', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Messages</h1>'
        }, {
            xtype: 'messages',
            id: 'listFinAidMsgs'
        }],
        flex: 1
    }
});