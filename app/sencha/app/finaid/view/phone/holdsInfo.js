Ext.define('mobEdu.finaid.view.phone.holdsInfo', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title:'<h1>Holds Info</h1>'
        },{
            xtype: 'holds',
            id: 'listFinAidHolds'
        } ],
        flex: 1
    }
});