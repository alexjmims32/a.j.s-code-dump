Ext.define('mobEdu.finaid.view.phone.paymentSchedule', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Payment Schedule</h1>'
        }, {
            xtype: 'paymentSchedule',
            id: 'paymentScheduleList',
            name:'paymentScheduleList'
        }],
        flex: 1
    }
});