Ext.define('mobEdu.finaid.view.phone.menu', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'finaidTitle',
            title: '<h1>Financial Aid</h1>'
        }, {
            //            xtype: 'fieldset',
            //            docked: 'top',
            //            items: [{
            xtype: 'selectfield',
            docked: 'top',
            name: 'finaidCodes',
            valueField: 'code',
            displayField: 'description',
            id: 'finaidCodes',
            store: mobEdu.util.getStore('mobEdu.finaid.store.menu'),
            listeners: {
                change: function(selectbox, newValue, oldValue) {
                    mobEdu.finaid.f.onCodeChange(newValue);
                    Ext.getCmp('finaidList').setEmptyText('');

                }
            }
            //            }]
        }, {
            xtype: 'list',
            id: 'finaidList',
            emptyText: '',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr><td width="80%" align="left" ><h3 class="subnavText">{description}</h3></td><td width="20%" align="right" ><h4 class="listDate">{amount}</h4></td><tpl if="next==1"><td width="10%"><div align="right" class="arrow" /></td></tpl></tr></table>'),
            store: mobEdu.util.getStore('mobEdu.finaid.store.statusLocal'),
            singleSelect: true,
            scrollToTopOnRefresh: false,

            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    if (e.data.action != undefined || e.data.action != null) {
                        e.data.action();
                    }
                }
            }
        }],
        flex: 1
    }
});