Ext.define('mobEdu.finaid.view.phone.costAttendance', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Cost Of Attendance</h1>'
        }, {
            xtype: 'toolbar',
            id: 'costTitle',
            docked: 'bottom',
            title: ''
        }, {
            xtype: 'coa',
            id: 'costAttendanceList'
        }],
        flex: 1
    }
});