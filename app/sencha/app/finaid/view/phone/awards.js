Ext.define('mobEdu.finaid.view.phone.awards', {
    extend: 'Ext.Panel',
    requires: [
		'mobEdu.finaid.store.awards',
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'awardTitle',
            title:'<h1>Award Package</h1>'
        },{            
            
            xtype: 'award',
            name: 'awardsList',
            id: 'awardsList'
        }, {
                layout: 'fit',
                title: '<h3>Awards</h3>',
                scrollable: false,
                width: '100',
                items:[{
                    xtype:'award',
                    name: 'awardsList',
                    id: 'awardsList',
                    listeners: {
                        itemtap: function(me, index, target, record, e, eOpts) {
                            mobEdu.finaid.f.onAwardItemTap(me, index, target, record, e, eOpts);
                        }
                    }
                }],                    
                listeners: {
                    activate: function(newActiveItem, container, oldActiveItem, eOpts) {
                        mobEdu.finaid.f.loadAwards();
                    }
                }
            }],
        flex: 1
    }
});