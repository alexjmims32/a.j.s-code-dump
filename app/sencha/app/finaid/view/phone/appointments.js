Ext.define('mobEdu.finaid.view.phone.appointments', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Appointments</h1>'
        }, {
            xtype: 'appointments',
            id: 'listFinAidAppts'
        }],
        flex: 1
    }
});