Ext.define('mobEdu.finaid.view.award', {
    extend: 'Ext.List',
    xtype: 'award',
    config: {
        itemTpl: new Ext.XTemplate(
            '<table width="100%">' +
            '<tpl if="(periodDesc === \'Total\')">',
            '<tr><td width="70%"><h3>{periodDesc}</h3><br /><h3>${[mobEdu.util.formatCurrency(values.fundAmt)]}</h3></td>' +
            '<tpl else>',
            '<tr><td width="70%"><table>' +
            '<tpl for="item">',
            '<tr><td><h4>{periodDesc} - ${[mobEdu.util.formatCurrency(values.fundAmt)]}</h4></td></tr>' +
            '</tpl>' +
            '</table></td>' +
            '</tpl>' +
            '<tpl if="(webUpdate === \'Y\' && mobEdu.counselor.f.role != \'counselor\')">',
            '<td width="30%" align="right"><img name="accept" width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/accept.png"/> &nbsp; <img name="decline" width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/reject.png"/></td>',
            '<tpl else>',
            '<td width="30%" align="right">{awardStatus}</td>' +
            '</tpl>' +
            '</tr>' +
            '</table>'
        ),
        store: mobEdu.util.getStore('mobEdu.finaid.store.awardsLocal'),
        grouped: true,
        emptyText: '<h3 align="center">Student has no Awards.</h3>',
        disableSelection: true,
        scrollToTopOnRefresh: false,
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});