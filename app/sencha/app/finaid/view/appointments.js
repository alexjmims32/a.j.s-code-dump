Ext.define('mobEdu.finaid.view.appointments', {
    extend: 'Ext.List',
    xtype: 'appointments',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tpl if="(mobEdu.util.isValueExists(subject) === true)">', '<tr><td width="20%"><h2>Subject</h2></td><td width="20%"><h3>: {subject}</h3></td>' + '</tpl>' +
            '<tpl if="((mobEdu.util.isRecieved(createdBy) === true)&&(mobEdu.util.isVisible(fromID,toIDs,loginUserID) === true))">', '<td width="60%" align="right" rowspan=4><img name="accept" width="40px" height="40px" src="' + mobEdu.util.getResourcePath() + 'images/accept.png"/> &nbsp; <img name="decline" width="40px" height="40px" src="' + mobEdu.util.getResourcePath() + 'images/reject.png"/></td></tr>' + '</tpl>' +
            '<tpl if="(mobEdu.util.isValueExists(appointmentDate) === true)">', '<tr><td width="35%"><h2>Date</h2></td><td width="35%"><h3>: {appointmentDate}</h3></td></tr>' + '</tpl>' +
            '<tpl if="(mobEdu.util.isValueExists(location) === true)">', '<tr><td width="50%"><h2>Location</h2></td><td width="50%"><h3>: {location}</h3></td></tr>' + '</tpl>' +
            '<tr><td width="50%"><h2>Status</h2></td><td width="50%"><h3>:{[mobEdu.util.getStatus(values.fromDetails,values.toIDs,values.loginUserID)]}</h3></td></tr>' +
            '<tpl if="(mobEdu.util.isValueExists(body) === true)">', '<tr><td colspan="2" width="50%"><h2>Description:</h2></td></tr>', '<tr><td colspan="2" width="50%"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' + '</tpl>' +
            '</table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.viewAppts'),
        disableSelection: true,
        scrollToTopOnRefresh: false,
        emptyText: '<h3 align="center">Student has no Appointments</h3>',
        loadingText: '',
        id: 'finappt',
        listeners: {
            itemtap: function(me, index, target, record, e, eOpts) {
                mobEdu.finaid.f.appTap(me, index, target, record, e, eOpts);
            }
        },
    },
    initialize: function() {
        this.callParent();
        mobEdu.enroute.leads.f.setPagingPlugin(Ext.getCmp('finappt'));
        var searchStore = mobEdu.util.getStore('mobEdu.finaid.store.viewAppts');
        searchStore.addBeforeListener('load', mobEdu.enroute.leads.f.checkForSearchListEnd, this);
    }
});