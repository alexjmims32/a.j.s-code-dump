Ext.define('mobEdu.finaid.view.emailDetail', {
	extend: 'Ext.Panel',
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'dataview',
			name: 'emaildetail',
			id: 'emaildetail',
			emptyText: '<center><h3>No Message selected</h3><center>',
			itemTpl: new Ext.XTemplate('<table width="100%">' +
				'<tr><td align="right" valign="top" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>From:</h2></td><td align="left"><h3>{fromName}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>To:</h2></td><td align="left"><h3>{toName}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Date:</h2></td><td align="left"><h3>{versionDate}</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{messageStatus}</h3></td></tr>' +
				'<tr><td align="right" valign="top" width="50%"><h2>Message:</h2></td><td align="left"><h3>{body}</h3></td></tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.finaid.store.viewEmail'),
			singleSelect: true,
			loadingText: ''
		}, {
			xtype: 'customToolbar',
			title: '<h1>Message Detail</h1>'
		}],
		flex: 1
	}
});