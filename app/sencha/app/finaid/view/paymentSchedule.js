Ext.define('mobEdu.finaid.view.paymentSchedule', {
    extend: 'Ext.List',
    xtype: 'paymentSchedule',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tpl if="(fundDesc === \'Total\')">',
            '<tr><td width="80%"><h3>Total</h3></td><td width="20%"><h3>${[mobEdu.util.formatCurrency(values.total)]}</h3></td></tr>' +
            '<tpl else>',
            '<tr><td width="80%"><h3>{fundDesc}</h3></td><td width="20%"><h3>${[mobEdu.util.formatCurrency(values.disburseAmt)]}</h3></td></tr>' +
            '<tr><td width="60%"><h4>{termDesc}</h4></td><td width="40%"><h4>Expected Date:{date}</h4></td></tr>' +
            '</tpl>' +
            '</table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.paymentSchedule'),
        disableSelection: true,
        emptyText: '<h3 align="center">Student has no Payment Schedule.</h3>',
        scrollToTopOnRefresh: false,
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});