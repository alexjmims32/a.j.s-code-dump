Ext.define('mobEdu.finaid.view.resources', {
    extend: 'Ext.List',
    xtype: 'resources',
    config: {
        itemTpl: new Ext.XTemplate(
            '<table width="100%">' +
            '<tr><td width="80%"><h3>{description}</h3><br /><h4>{termDesc}</h4></td><td width="15%" align="right"><h3>${expectedAmt}</h3></td></tr>' +
            '</table>'
        ),
        store: mobEdu.util.getStore('mobEdu.finaid.store.resources'),
        disableSelection: true,
        scrollToTopOnRefresh: false,
        emptyText: '<h3 align="center">Student has no Resources.</h3>',
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});