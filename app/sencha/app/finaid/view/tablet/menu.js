Ext.define('mobEdu.finaid.view.tablet.menu', {
	extend: 'Ext.Container',
	config: {
		fullscreen: true,
		layout: {
			type: 'card',
			animation: {
				type: 'slide',
				direction: 'left',
				duration: 250
			}
		},
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			id: 'finaidTitleT',
			title: '<h1>Financial Aid</h1>'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			items: [{
				xtype: 'selectfield',
				name: 'finaidCodesT',
				valueField: 'code',
				displayField: 'description',
				id: 'finaidCodesT',
				store: mobEdu.util.getStore('mobEdu.finaid.store.menu'),
				listeners: {
					change: function(selectbox, newValue, oldValue) {
						mobEdu.finaid.f.onCodeChange(newValue);

					}
				}
			}],
		}, {
			xtype: 'tabpanel',
			id: 'finaidTabs',
			name: 'finaidTabs',
			tabBar: {
				layout: {
					pack: 'center'
				}
			},
			defaults: {
				style: 'border:none;background:none;'
			},
			items: [{
				layout: 'fit',
				id: 'counDetails',
				name: 'counDetails',
				title: '<h3>Details</h3>',
				width: '100',
				items: [{
					id: 'counDetailsT',
					scrollable: true,
					name: 'counDetailsT',
					html: ''
				}]
			}, {
				layout: 'fit',
				title: '<h3>Cost Of Attendance</h3>',
				scrollable: false,
				width: '100',
				items: [{
					id: 'finBudgetGrp',
					name: 'finBudgetGrp',
					docked: 'top',
					cls: 'finaid',
					html: ''
				}, {
					xtype: 'coa'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadCostAttendance();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Awards</h3>',
				scrollable: false,
				width: '100',
				items: [{
					id: 'finAwardsTot',
					name: 'finAwardsTot',
					docked: 'top',
					cls: 'finaid',
					html: ''
				}, {
					xtype: 'award',
					name: 'awardsListT',
					id: 'awardsListT',
					listeners: {
						itemtap: function(me, index, target, record, e, eOpts) {
							mobEdu.finaid.f.onAwardItemTap(me, index, target, record, e, eOpts);
						}
					}
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadAwards();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Resources</h3>',
				scrollable: false,
				width: '100',
				items: [{
					xtype: 'resources'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadResources();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Requirements</h3>',
				scrollable: false,
				width: '100',
				items: [{
					xtype: 'requirements'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadRequirements();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Holds</h3>',
				scrollable: false,
				width: '100',
				items: [{
					xtype: 'holds'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadHolds();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Loan</h3>',
				scrollable: false,
				width: '100',
				items: [{
					id: 'finLoanHisTot',
					name: 'finLoanHisTot',
					docked: 'top',
					cls: 'finaid',
					html: ''
				}, {
					xtype: 'loanHistory'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadLoanHistory();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Payment</h3>',
				scrollable: false,
				width: '100',
				items: [{
					xtype: 'paymentSchedule'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadPaymentSchedule();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Links</h3>',
				scrollable: false,
				width: '100',
				items: [{
					xtype: 'links'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadLinks();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Appts</h3>',
				hidden:true,
				scrollable: false,
				width: '100',
				items: [{
					xtype: 'appointments'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadAppointments();
					}
				}
			}, {
				layout: 'fit',
				title: '<h3>Msgs</h3>',
				scrollable: false,
				hidden:true,
				width: '100',
				items: [{
					xtype: 'messages'
				}],
				listeners: {
					activate: function(newActiveItem, container, oldActiveItem, eOpts) {
						mobEdu.finaid.f.loadMessages();
					}
				}
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			id: 'counToolbar',
			name: 'counToolbar',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Send Message',
				ui: 'button',
				handler: function() {
					mobEdu.advising.f.showConNewMsg();
				}
			}, {
				text: 'Setup Appointment',
				ui: 'button',
				handler: function() {
					mobEdu.advising.f.showCounNewAppt();
				}
			}, {
				text: 'Calendar',
				ui: 'button',
				handler: function() {
					mobEdu.finaid.f.showCounCalendar();
				}
			}]
		}],
		flex: 1
	}
});