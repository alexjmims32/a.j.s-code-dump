Ext.define('mobEdu.finaid.view.appointmentsCalendar', {
    extend: 'Ext.Container',
    alias: 'appointmentsCalendar',
    config: {
        fullscreen: 'true',
        scrollable: true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            id: 'finAppointmentsDock',
            name: 'finAppointmentsDock'
        }, {
            xtype: 'label',
            id: 'finAppointmentLabel',
            padding: 10
        }]
    }
});