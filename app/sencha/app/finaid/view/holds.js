Ext.define('mobEdu.finaid.view.holds', {
    extend: 'Ext.List',
    xtype: 'holds',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tr><td width="30%"><h4>Hold Date</h4></td><td width="70%" align="right"><h4>{holdFromDate}</h4></td></tr>' +
            '<tr><td width="30%"><h4>Hold Description</h4></td><td width="70%" align="right"><h4>{holdsDescription}</h4></td></tr>' +
            '<tr><td width="30%"><h4>Hold Reason</h4></td><td width="70%" align="right"><h4>{holdReason}</h4></td></tr>' +
            '</table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.holdsInfo'),
        disableSelection: true,
        scrollToTopOnRefresh: false,
        emptyText: '<h3 align="center">Student has no Holds.</h3>',
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});