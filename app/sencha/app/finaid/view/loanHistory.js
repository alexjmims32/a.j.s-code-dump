Ext.define('mobEdu.finaid.view.loanHistory', {
    extend: 'Ext.List',
    xtype: 'loanHistory',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tr><td width="70%"><h3><b>{fundTitle}</b></h3></td></tr>' +
            '<tr><td width="70%"><h3>{aidyDesc}</h3></td></tr>' +
            '<tr><td width="40%"><h4>Loan Amount</h4></td><td width="60%" align="right"><h4>${[mobEdu.util.formatCurrency(values.approveAmt)]}</h4></td></tr>' +
            '<tr><td width="40%"><h4>Paid</h4></td><td width="60%" align="right"><h4>${[mobEdu.util.formatCurrency(values.paidAmt)]}</h4></td></tr>' +
            '<tr><td width="40%"><h4>Returned</h4></td><td width="60%" align="right"><h4>${[mobEdu.util.formatCurrency(values.returnAmt)]}</h4></td></tr>' +
            '<tr><td width="40%"><h4>School Status</h4></td><td width="60%" align="right"><h4>{status}</h4></td></tr>' +
            '<tr><td width="40%"><h4>Loan Period</h4></td><td width="40%" align="right"><h4>{periodDesc}</h4></td></tr>' +
            '<tpl if="(mobEdu.util.isValueExists(component) === true)">',
            '<tr><td width="40%"><h4>Servicer</h4></td><td width="60%" align="right"><a href="javascript:mobEdu.util.loadExternalUrl({url})">DLServicer</a></td></tr>' +
            '</tpl>' +
            '</table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.loanHistory'),
        grouped: true,
        disableSelection: true,
        emptyText: '<h3 align="center">Student has no Loan History.</h3>',
        scrollToTopOnRefresh: false,
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});