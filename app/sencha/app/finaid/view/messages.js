Ext.define('mobEdu.finaid.view.messages', {
    extend: 'Ext.List',
    xtype: 'messages',
    config: {
        itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
            '<tr>',
            '<tpl if="(mobEdu.advising.f.hasEmailDirection(label)===true)">',
            '<td align="right"><b><h2>{subject}</h2></b><br/><div class="bodyclass" style="vertical-align: top; overflow: hidden; text-overflow: ellipsis; width:200px; font-weight:normal;">{body}</div><br/><b><h4>{versionDate}</h4></b></td>',
            '<tpl else>',
            '<td align="left"><b><h2>{subject}</h2></b><br/><div class="bodyclass" style="vertical-align: top; overflow: hidden; text-overflow: ellipsis; width:200px; font-weight:normal">{body}</div><br/><b><h4>{versionDate}</h4></b></td>',
            '</tpl>',
            '</tr>' +
            '</table>'
        ),
        store: mobEdu.util.getStore('mobEdu.finaid.store.emailsList'),
        disableSelection: true,
        id: 'finmsglist',
        scrollToTopOnRefresh: false,
        emptyText: '<h3 align="center">Student has no Messages.</h3>',
        loadingText: '',
        listeners: {
            itemtap: function(view, index, target, record, item, e, eOpts) {
                setTimeout(function() {
                    view.deselect(index);
                }, 500);
                mobEdu.finaid.f.loadEmailDetail(record);
            }
        }
    },
    initialize: function() {
        this.callParent();
        mobEdu.enroute.leads.f.setPagingPlugin(Ext.getCmp('finmsglist'));
        var searchStore = mobEdu.util.getStore('mobEdu.finaid.store.emailsList');
        searchStore.addBeforeListener('load', mobEdu.enroute.leads.f.checkForSearchListEnd, this);
    }
});