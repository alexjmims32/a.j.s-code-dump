Ext.define('mobEdu.finaid.view.req', {
    extend: 'Ext.List',
    xtype: 'requirements',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tr><td width="70%"><h3>{description}</h3></td><td width="30%" align="right"><h4>{status}</h4></td>' +
            '</tr></table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.requirements'),
        grouped: true,
        disableSelection: true,
        scrollToTopOnRefresh: false,
        emptyText: '<h3 align="center">Student has no Requirements.</h3>',
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});