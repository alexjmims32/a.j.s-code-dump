Ext.define('mobEdu.finaid.view.links', {
    extend: 'Ext.List',
    xtype: 'links',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tr><td width="100%"><a href="javascript:mobEdu.util.loadExternalUrl(\'{url}\');"><h4>{urlText}</h4></a></td></tr>' +
            '<tr><td width="100%"><h5>{urlDesc}</h5></td></tr>' +
            '</table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.links'),
        disableSelection: true,
        scrollToTopOnRefresh: false,
        emptyText: '<h3 align="center">Student has no Links.</h3>',
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});