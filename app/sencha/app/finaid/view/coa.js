Ext.define('mobEdu.finaid.view.coa', {
    extend: 'Ext.List',
    xtype: 'coa',
    config: {
        itemTpl: new Ext.XTemplate('<table width="100%">' +
            '<tpl if="(component === \'Total\')">',
            '<tr><td width="80%"><h3>{component}</h3></td><td width="20%" align="right"><h3>${[mobEdu.util.formatCurrency(values.amt)]}</h3></td></tr>' +
            '<tpl else>',
            '<tr><td width="80%"><h4>{component}</h4></td><td width="20%" align="right"><h4>${[mobEdu.util.formatCurrency(values.amt)]}</h4></td></tr>' +
            '</tpl>' +
            '</table>'),
        store: mobEdu.util.getStore('mobEdu.finaid.store.costAttendance'),
        disableSelection: true,
        emptyText: '<h3 align="center">Student has no Cost Of Attendance.</h3>',
        scrollToTopOnRefresh: false,
        loadingText: ''
    },
    initialize: function() {
        this.callParent();
    }
});