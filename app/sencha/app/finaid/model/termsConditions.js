Ext.define('mobEdu.finaid.model.termsConditions', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'heading'
            },{
                name: 'errorMsg'
            },{
                name: 'code'
            },{
                name: 'acceptInd'
            },{
                name:'text',
                convert: function(value, record) {
					if(value!=null && value !=''){
						value = mobEdu.util.formatUrls(value);
					}
					return value;
				}
            }
    
        ]
    }
});