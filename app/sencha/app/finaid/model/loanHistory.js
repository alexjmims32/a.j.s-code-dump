Ext.define('mobEdu.finaid.model.loanHistory', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'aidyCode'
            },{
                name:'aidyDesc'
            },{
                name:'fundCode'
            },{
                name: 'fundTitle'
            },{
                name:'approveAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((0.00).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((0.00).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }        
                }
            },{
                name:'status'
            },{
                name:'periodDesc'
            },{
                name:'paidAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((0.00).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((0.00).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }        
                }
            },{
                name:'returnAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((0.00).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((0.00).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }        
                }
            },{
                name:'servicer'
            },{
                name:'url'
            },{
                name:'description'
            },{
                name:'value'
            },{
                name:'group'
            }
    
        ]
    }
});