Ext.define('mobEdu.finaid.model.requirements', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'description'
        }, {
            name: 'statDate'
        }, {
            name: 'status'
        }, {
            name: 'satInd'
        }, {
            name: 'fundTitle',
            convert: function(value, record) {
                if (value == '' || value == null || value == undefined) {
                    value = 'Other';
                    return value;
                } else {
                    return value;
                }
            }
        }]
    }
});