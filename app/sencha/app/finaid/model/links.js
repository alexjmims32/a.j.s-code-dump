Ext.define('mobEdu.finaid.model.links', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'urlText'
            },{
                name:'url'
            },{
                name:'urlDesc'
            }    
        ]
    }
});