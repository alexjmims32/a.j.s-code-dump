
Ext.define('mobEdu.finaid.model.awardStatus', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'status'
            },{
                name:'errorMsg'
            }    
        ]
    }
});