Ext.define('mobEdu.finaid.model.menu', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'code'
            },{
                name:'description'
            },{
                name:'amount'
            },{
                name:'next'
            },{
                name:'action',
                type: 'function'
            }
        ]
    }
});