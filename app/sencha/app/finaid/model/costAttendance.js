Ext.define('mobEdu.finaid.model.costAttendance', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'component'
            },{
                name:'period'
            },{
                name:'totalAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((parseFloat(0)).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((parseFloat(0)).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }         
                }
            },{
                name: 'amt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((parseFloat(0)).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((parseFloat(0)).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }         
                }
            }
        ]
    }
});