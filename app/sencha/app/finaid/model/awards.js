Ext.define('mobEdu.finaid.model.awards', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'period'
            },{
                name:'periodDesc'
            },{
                name:'fundCode'
            },{
                name: 'fundDesc'
            },{
                name:'awardStatus'
            },{
                name:'offerAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((0.00).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((0.00).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }        
                }
            },{
                name:'code'
            },{
                name:'webUpdate'
            },{
                name:'parrialAccept'
            },{
                name:'fundAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((0.00).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((0.00).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }        
                }
            },{
                name:'fundMinAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return ((0.00).toFixed(2));
                    }
                    if(isNaN(value)){
                       return ((0.00).toFixed(2));
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }        
                }
            },{
                name:'description'
            },{
                name:'value'
            },{
              name:'item',
              type:'array'
            }
    
        ]
    }
});