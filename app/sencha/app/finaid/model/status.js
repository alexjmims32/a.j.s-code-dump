Ext.define('mobEdu.finaid.model.status', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'group'
            },{
                name:'budjetAmt'
            },{
                name:'awardAmt'
            },{
                name: 'status'
            },{
                name:'requirements'
            },{
                name:'holds'
            },{
                name:'loanHistory'
            },{
                name:'paymentAmount'
            }
        ]
    }
});