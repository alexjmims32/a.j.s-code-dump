Ext.define('mobEdu.finaid.model.paymentSchedule', {
  extend: 'Ext.data.Model',

  config: {
    fields: [{
        name: 'termDesc'
      }, {
        name: 'fundCode'
      }, {
        name: 'fundDesc'
      }, {
        name: 'disburseAmt',
        convert: function(value, record) {
          if (value == '' || value == null) {
            return ((0.00).toFixed(2));
          }
          if (isNaN(value)) {
            return ((0.00).toFixed(2));
          } else {
            value = parseFloat(value);
            value = value.toFixed(2);
            return value;
          }
        }
      }, {
        name: 'date',
        convert: function(value, record) {
          if (value != '' && value != null) {
            value = new Date(value.replace(' ', 'T'));
            value = Ext.Date.format(value, 'M d, Y');
          }
          return value;
        }
      }, {
        name: 'total',
        convert: function(value, record) {
          if (value == '' || value == null) {
            return ((0.00).toFixed(2));
          }
          if (isNaN(value)) {
            return ((0.00).toFixed(2));
          } else {
            value = parseFloat(value);
            value = value.toFixed(2);
            return value;
          }
        }
      }

    ]
  }
});