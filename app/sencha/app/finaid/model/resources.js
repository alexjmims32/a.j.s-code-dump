Ext.define('mobEdu.finaid.model.resources', {
    extend:'Ext.data.Model',

    config:{
        fields:[{
                name: 'code'
            },{
                name:'description'
            },{
                name:'termDesc'
            },{
                name: 'expectedAmt',
                convert: function(value, record) { 
                    if(value=='' || value==null){
                        return '';
                    }
                    if(isNaN(value)){
                       return value;
                    }else{
                       value = parseFloat(value);
                       value = value.toFixed(2);
                       return value;
                   }         
                }
            }
        ]
    }
});