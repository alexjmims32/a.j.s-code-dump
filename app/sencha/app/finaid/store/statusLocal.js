Ext.define('mobEdu.finaid.store.statusLocal', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.finaid.model.menu'
    ],

    config:{
        storeId: 'mobEdu.finaid.store.statusLocal',
        autoLoad: true,
        model: 'mobEdu.finaid.model.menu',
        proxy:{
            type:'localstorage',
            id:'statusLocalStorage'
        }
    }
});