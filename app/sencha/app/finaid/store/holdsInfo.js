Ext.define('mobEdu.finaid.store.holdsInfo',{
extend: 'mobEdu.data.store',
   
   requires:[
       'mobEdu.crl.model.holdsInfo'
   ],
   config:{
       storeId:'mobEdu.finaid.store.holdsInfo',
        autoLoad: false,
        model: 'mobEdu.crl.model.holdsInfo'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'holdsList';        
        return proxy;
    }
});