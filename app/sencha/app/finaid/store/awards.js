Ext.define('mobEdu.finaid.store.awards', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.awards'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.awards',
        autoLoad: false,
        model: 'mobEdu.finaid.model.awards',
        grouper: {
            groupFn: function(record) {
                return record.get('fundDesc');
            }
        }, 
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'awardsList'        
        return proxy;
    }
    
});