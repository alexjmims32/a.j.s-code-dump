Ext.define('mobEdu.finaid.store.appAcceptance', {
    extend: 'mobEdu.data.store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.finaid.model.appointment'
    ],
    config: {
        storeId: 'mobEdu.finaid.store.appAcceptance',
        autoLoad: false,
        model: 'mobEdu.finaid.model.appointment',
        initProxy: function() {
            var proxy = this.callParent();
            proxy.reader.rootProperty = 'resultSet';
            return proxy;
        }
    }
});