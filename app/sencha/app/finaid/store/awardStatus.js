Ext.define('mobEdu.finaid.store.awardStatus', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.awardStatus'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.awardStatus',
        autoLoad: false,
        model: 'mobEdu.finaid.model.awardStatus'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
    
});