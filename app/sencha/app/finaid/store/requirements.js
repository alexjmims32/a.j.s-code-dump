Ext.define('mobEdu.finaid.store.requirements', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.finaid.model.requirements'
    ],

    config: {
        storeId: 'mobEdu.finaid.store.requirements',
        autoLoad: false,
        model: 'mobEdu.finaid.model.requirements',
        grouper: {
            groupFn: function(record) {
                return record.get('fundTitle');
            }
        },
    },

    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'requirementList'
        return proxy;
    }

});