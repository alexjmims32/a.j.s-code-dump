Ext.define('mobEdu.finaid.store.loanHistory', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.finaid.model.loanHistory'
    ],

    config: {
        storeId: 'mobEdu.finaid.store.loanHistory',
        autoLoad: false,
        model: 'mobEdu.finaid.model.loanHistory',
        grouper: {
            groupFn: function(record) {
                return record.get('aidyDesc');
            },
            sortProperty: 'aidyDesc',
            direction: 'DESC'
        },
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'loanHistoryList'
        return proxy;
    }

});