Ext.define('mobEdu.finaid.store.costAttendance', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.costAttendance'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.costAttendance',
        autoLoad: false,
        model: 'mobEdu.finaid.model.costAttendance'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'costAttendanceList'        
        return proxy;
    }
    
});