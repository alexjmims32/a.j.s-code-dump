Ext.define('mobEdu.finaid.store.viewAppts', {
    extend: 'mobEdu.data.store',
    requires: [
        'mobEdu.finaid.model.appointment'
    ],
    config: {
        storeId: 'mobEdu.finaid.store.viewAppts',
        autoLoad: false,
        model: 'mobEdu.finaid.model.appointment',
        sorters: {
            property: 'appointmentDate',
            direction: 'ASC'
        }
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'resultSet';
        return proxy;
    }
});