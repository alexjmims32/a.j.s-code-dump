Ext.define('mobEdu.finaid.store.loanHistoryLocal', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.finaid.model.loanHistory'
    ],

    config:{
        storeId: 'mobEdu.finaid.store.loanHistoryLocal',
        autoLoad: true,
        model: 'mobEdu.finaid.model.loanHistory',        
        proxy:{
            type:'localstorage',
            id:'loanHistoryLocalStorage'
        }
    }
});