Ext.define('mobEdu.finaid.store.statusaward', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.finaid.model.status'
    ],

    config: {
        storeId: 'mobEdu.finaid.store.statusaward',
        autoLoad: false,
        model: 'mobEdu.finaid.model.status'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'statusList'
        return proxy;
    }

});