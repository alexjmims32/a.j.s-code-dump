Ext.define('mobEdu.finaid.store.paymentSchedule', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.paymentSchedule'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.paymentSchedule',
        autoLoad: false,
        model: 'mobEdu.finaid.model.paymentSchedule'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'paymentScheduleList'        
        return proxy;
    }
    
});