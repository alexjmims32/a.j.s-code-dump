Ext.define('mobEdu.finaid.store.resources', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.resources'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.resources',
        autoLoad: false,
        model: 'mobEdu.finaid.model.resources'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'resourceList'        
        return proxy;
    }
    
});