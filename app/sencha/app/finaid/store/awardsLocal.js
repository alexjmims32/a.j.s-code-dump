Ext.define('mobEdu.finaid.store.awardsLocal', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.finaid.model.awards'
    ],

    config:{
        storeId: 'mobEdu.finaid.store.awardsLocal',
        autoLoad: true,
        model: 'mobEdu.finaid.model.awards',    
        grouper: {
            groupFn: function(record) {
                return record.get('fundDesc');
            }
        },    
        proxy:{
            type:'localstorage',
            id:'awardsLocalStorage'
        }
    }
});