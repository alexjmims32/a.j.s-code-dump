Ext.define('mobEdu.finaid.store.menu', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.menu'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.menu',
        autoLoad: false,
        model: 'mobEdu.finaid.model.menu'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'menuList'        
        return proxy;
    }
    
});