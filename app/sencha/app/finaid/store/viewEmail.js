Ext.define('mobEdu.finaid.store.viewEmail', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.finaid.model.emailsList'
    ],
    config: {
        storeId: 'mobEdu.finaid.store.viewEmail',

        autoLoad: false,

        model: 'mobEdu.finaid.model.emailsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json'
                //                rootProperty: 'emails'
            }
        }
    }
});