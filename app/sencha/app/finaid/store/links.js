Ext.define('mobEdu.finaid.store.links', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.links'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.links',
        autoLoad: false,
        model: 'mobEdu.finaid.model.links'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'linkList'        
        return proxy;
    }
    
});