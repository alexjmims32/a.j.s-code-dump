Ext.define('mobEdu.finaid.store.termsConditions', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.termsConditions'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.termsConditions',
        autoLoad: false,
        model: 'mobEdu.finaid.model.termsConditions'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'termsConditions'        
        return proxy;
    }
    
});