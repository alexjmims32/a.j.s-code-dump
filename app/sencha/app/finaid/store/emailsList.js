Ext.define('mobEdu.finaid.store.emailsList', {
    extend: 'mobEdu.data.store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.finaid.model.emailsList'
    ],
    config: {
        storeId: 'mobEdu.finaid.store.emailsList',

        autoLoad: false,

        model: 'mobEdu.finaid.model.emailsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'messages'
            }
        }
    }
});