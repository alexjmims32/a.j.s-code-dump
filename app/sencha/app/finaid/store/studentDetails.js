Ext.define('mobEdu.finaid.store.studentDetails', {
  extend: 'mobEdu.data.store',

  requires: [
    'mobEdu.finaid.model.emailsList'
  ],
  config: {
    storeId: 'mobEdu.finaid.store.studentDetails',
    autoLoad: false,
    model: 'mobEdu.finaid.model.emailsList'
  },
  initProxy: function() {
    var proxy = this.callParent();
    return proxy;
  }
});