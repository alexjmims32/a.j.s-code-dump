Ext.define('mobEdu.finaid.store.status', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.finaid.model.status'
    ],
    
    config:{
        storeId: 'mobEdu.finaid.store.status',
        autoLoad: false,
        model: 'mobEdu.finaid.model.status'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'statusList'        
        return proxy;
    }
    
});