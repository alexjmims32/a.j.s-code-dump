Ext.define('mobEdu.messages.view.rEmailDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.messages.f',
        'mobEdu.messages.store.emailDetail'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{versionDate}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{messageStatus}</h3></td></tr>' +
                '<tpl if="(mobEdu.enroute.main.f.hasReqEmailDirection(labels)===false)">',
                '<tr><td align="right" width="50%"><h2>From:</h2></td><td align="left"><h3>{fromName}</h3></td></tr>',
                '<tpl else>',
                '<tr><td align="right" width="50%"><h2>To:</h2></td><td align="left"><h3>{[mobEdu.util.toSplit(values.to)]}</h3></td></tr>',
                '</tpl>',
                '<tr><td valign="top"  align="right" width="50%"><h2>Message:</h2></td><td align="left"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
                //                    '</table></td></tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.messages.store.emailDetail'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Message Details</h1>'

        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            id: 'bottomToolbar',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Reply All',
                id: 'replyAllMsg',
                name: 'replyAllMsg',
                handler: function() {
                    mobEdu.messages.f.ShowReplayAllMessagesView();
                }
            }, {
                text: 'Reply',
                align: 'right',
                id: 'replyMeg',
                name: 'replyMeg',
                handler: function() {
                    mobEdu.messages.f.showResopndEmail();
                }
            }]
        }],
        flex: 1
    }

});