Ext.define('mobEdu.messages.view.paging', {
	extend: 'Ext.plugin.ListPaging',
	config: {
		//id: 'pagingplugin',
		id: 'stMessageplugin',
		type: 'listpaging',
		autoPaging: true,
		loadMoreText: 'Load messages..',
		noMoreRecordsText: 'No more messages'
	}
});