Ext.define('mobEdu.messages.view.recruiterEmailsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.messages.f',
		'mobEdu.messages.store.recruiterEmail'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',

		items: [{
			xtype: 'list',
			id: 'stEmailsList',
			name: 'stEmailsList',
			// scrollToTopOnRefresh: true,
			emptyText: '<h3 align="center">No Messages</h3>',
			cls: 'logo',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>',
				'<tpl if="(mobEdu.enroute.main.f.hasReqEmailDirection(labels)===true)">',
				'<td width="5%"><img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
				'<tpl else>',
				'<td width="5%"><img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
				'</tpl>',
				'<tpl if="(mobEdu.enroute.main.f.hasRReadFlag(messageStatus)===true)">',
				'<td><h3>{subject}</h3></td>' +
				'<td align="right"><h5>{versionDate}</h5><br/><h5>{toName}</h5></td>' +
				'<tpl else>',
				'<td style="color: #030303;"><h2>{subject}</h2></td>' +
				'<td style="color: #030303;" align="right"><h5>{versionDate}</h5><br/><h5>{toName}</h5></td>' +
				'</tpl>',
				'</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.messages.f.loadReqEmailDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>Messages</h1>'

		}, {
			xtype: 'toolbar',
			docked: 'bottom',

			layout: {
				pack: 'right'
			},
			items: [{
				text: 'New Message',
				handler: function() {
					mobEdu.messages.f.showNewMessage();
				}
			}]
		}],
		flex: 1
	},
	initialize: function() {
		mobEdu.enroute.leads.f.setPagingPlugin(Ext.getCmp('stEmailsList'));
		var searchStore = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');
		searchStore.addBeforeListener('load', mobEdu.enroute.leads.f.checkForSearchListEnd, this);
	}

});