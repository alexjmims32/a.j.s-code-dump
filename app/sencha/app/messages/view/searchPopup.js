	Ext.define('mobEdu.messages.view.searchPopup', {
		extend: 'Ext.Panel',
		requires: [
			'mobEdu.messages.f',
			'mobEdu.messages.store.userList',
			'mobEdu.messages.store.profileSearch',
		],
		config: {
			id: 'usersearchPopup',
			floating: true,
			modal: true,
			centered: true,
			hideOnMaskTap: true,
			showAnimation: {
				type: 'popIn',
				duration: 250,
				easing: 'ease-out'
			},
			hideAnimation: {
				type: 'popOut',
				duration: 250,
				easing: 'ease-out'
			},
			width: '80%',
			height: '70%',
			layout: 'fit',
			items: [{
				xtype: 'textfield',
				id: 'searchstId',
				name: 'searchstId',
				docked: 'top',
				placeHolder: 'Search',
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.messages.f.doCheckBeforeSearch(textfield);
					},
					clearicontap: function(textfield, e, eOpts) {
						mobEdu.messages.f.clearRecSearchStore();
					}
				}
			}, {
				xtype: 'list',
				name: 'selectAll',
				scrollable: {
					direction: 'vertical',
					directionLock: true
				},
				id: 'selectStAll',
				emptyText: 'No search results',
				itemTpl: '<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
				store: mobEdu.util.getStore('mobEdu.messages.store.userList'),
				singleSelect: true,
				loadingText: '',
				listeners: {
					itemtap: function(view, index, target, record, item, e, eOpts) {
						mobEdu.messages.f.onSearchToIdItemTap(index, view, record, item);
					}
				}
			}, {
				xtype: 'toolbar',
				docked: 'bottom',
				layout: {
					pack: 'right'
				},
				items: [{
					text: 'Add',
					handler: function() {
						mobEdu.messages.f.selectedToNames();
					}
				}]
			}],
			flex: 1
		},
		initialize: function() {
			var searchStore = mobEdu.util.getStore('mobEdu.messages.store.userList');
			searchStore.addBeforeListener('load', mobEdu.directory.f.checkForDirectoryListEnd, this);
		}
	});