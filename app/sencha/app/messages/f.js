Ext.define('mobEdu.messages.f', {
	requires: [
		'mobEdu.messages.store.emailDetail',
		'mobEdu.messages.store.recruiterEmail',
		'mobEdu.messages.store.respondEmail',
		'mobEdu.messages.store.toStore',
		'mobEdu.messages.store.userList',
		'mobEdu.messages.store.profileSearch',
		'mobEdu.enroute.main.store.supLeadsList'
	],
	statics: {
		prevViewRef: null,
		count: 0,
		targetFlag: false,
		newMsg: false,
		replyMeg: false,
		loadFromMyMessageModule: function() {
			mobEdu.messages.f.loadMailsList('MSG');
			var reqEListStore = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');
			reqEListStore.currentPage = 1;
			reqEListStore.start = 0;
			reqEListStore.load();
		},
		loadMailsList: function(val) {
			/*var c = mobEdu.messages.f.count;
			if (c == 0) {
				mobEdu.util.get('mobEdu.messages.view.recruiterEmailsList');
				var searchListCmp = Ext.getCmp('stEmailsList');
				searchListCmp.setPlugins(null);
				var pArray = new Array();
				pArray[0] = mobEdu.messages.view.paging.create();
				searchListCmp.setPlugins(pArray);
				searchListCmp.setEmptyText('<h3 align="center">No messages.</h3>');
				Ext.getCmp('stMessageplugin').getLoadMoreCmp().show();
				mobEdu.messages.f.count = 1;
			}*/
			var entType = 'RECRUITER';
			mobEdu.enroute.main.f.role = mobEdu.util.getRole();
			var eStore = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/myMessages');
			eStore.getProxy().setExtraParams({
				companyId: companyId,
				loginID: mobEdu.util.getStudentId()
			});
			eStore.getProxy().afterRequest = function() {
				mobEdu.messages.f.loadEmailListResponseHandler(val);
			};
			eStore.loadPage(1);
		},

		loadEmailListResponseHandler: function(val) {
			var emailStore = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');
			mobEdu.util.get('mobEdu.enroute.leads.view.paging');
			if (emailStore.data.length > 0) {
				Ext.getCmp('leadplugin').setNoMoreRecordsText('No More Records');
			} else {
				Ext.getCmp('leadplugin').setNoMoreRecordsText('');
			}
			emailStore.sort('versionDate', 'DESC');
			var eStatus = emailStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success' || eStatus.status == 'No Data') {
				mobEdu.messages.f.showRecruiterEList(val);
			} else {
				Ext.Msg.show({
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		showRecruiterEList: function(val) {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.messages.view.recruiterEmailsList');

		},
		loadReqEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;
			var eDetailStore = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');

			eDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eDetailStore.getProxy().setUrl(commonwebserver + 'message/get');
			eDetailStore.getProxy().setExtraParams({
				messageID: eId,
				companyId: companyId
			});

			eDetailStore.getProxy().afterRequest = function() {
				mobEdu.messages.f.loadReqEmaildetailResponseHandler();
			};
			eDetailStore.load();
		},

		loadReqEmaildetailResponseHandler: function() {
			var eDetailStore = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');
			var eDetailStatus = eDetailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status == 'success') {
				mobEdu.messages.f.showReqEmailDetail();
			} else {
				Ext.Msg.show({
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		showReqEmailDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.messages.f.reqEmailListBack);
			mobEdu.util.show('mobEdu.messages.view.rEmailDetail');
			var messagesStore = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');
			var messageData = messagesStore.first();
			var toIdsList = messageData.data.to;
			var from = messageData.data.fromName;

			var dirLabel = messageData.data.labels[0].label;

			if (dirLabel == "SENT") {
				Ext.getCmp('bottomToolbar').hide();
			} else {
				Ext.getCmp('bottomToolbar').show();
			}
			if (toIdsList.length == 1) {
				Ext.getCmp('replyAllMsg').hide();
				Ext.getCmp('replyMeg').show();
			} else {
				Ext.getCmp('replyAllMsg').show();
				Ext.getCmp('replyMeg').hide();
			}
			if (from == "System Generated") {
				Ext.getCmp('bottomToolbar').hide();
			}
		},
		showRecruiterELists: function() {
			var store = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');
			store.currentPage = 1;
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enroute.main.view.menu');
		},
		showNewMessage: function() {
			mobEdu.messages.f.newMsg = true;
			mobEdu.messages.f.replyMeg = false;
			mobEdu.util.updatePrevView(mobEdu.messages.f.showRecruiterEList);
			mobEdu.util.show('mobEdu.messages.view.respondEmail');
			Ext.getCmp('resEmailTitle').setTitle('<h1>New Message</h1>');
			Ext.getCmp('searchHelp').show();
			var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			localStore.removeAll();
			mobEdu.messages.f.resetSendReplyEmail();
		},

		showResopndEmail: function() {
			mobEdu.messages.f.newMsg = false;
			mobEdu.messages.f.replyMeg = true;
			mobEdu.util.updatePrevView(mobEdu.messages.f.showReqEmailDetail);
			mobEdu.util.show('mobEdu.messages.view.respondEmail');
			Ext.getCmp('searchHelp').hide();
			Ext.getCmp('rToName').setReadOnly(true);
			Ext.getCmp('resEmailTitle').setTitle('<h1>Reply Message</h1>');
			var msgDetailStore = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');
			var messageData = msgDetailStore.data.all;
			var toIds = new Array();
			var labelsText = new Array();
			var formIds = new Array();
			var toNames = '';
			var toIdValues = '';
			var subject = messageData[0].data.subject;
			var rmsg = messageData[0].data.body;
			formIds = messageData[0].data.from;
			toIds = messageData[0].data.to;
			labelsText = messageData[0].data.labels;
			for (var i = 0; i < labelsText.length; i++) {
				var labelName = labelsText[0].label;
			}
			if (labelName == "SENT") {
				for (var j = 0; j < toIds.length; j++) {
					toNames = toIds[j].toName;
					toIdValues = toIds[j].toID;
				}
			} else {
				toNames = formIds.fromName;
			}
			Ext.getCmp('rToName').setValue(toNames);
			Ext.getCmp('rsub').setValue(subject);
			var toIdsLocalStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			toIdsLocalStore.removeAll();
			toIdsLocalStore.removed = [];
			toIdsLocalStore.sync();
		},

		toIdSearchList: function(searchString) {
			var role = mobEdu.util.getRole();
			//var searchCategory = 'All';
			var searchCategory = Ext.getCmp('searchMsgCategory').getValue();
			var searchlastName = '';
			var store = mobEdu.util.getStore('mobEdu.messages.store.userList');
			store.removeAll();
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (role.match(/SUPERVISOR/gi)) {
				store.getProxy().setUrl(commonwebserver + 'secured/admin/user/search');
				store.getProxy().setExtraParams({
					searchText: searchString,
					limit: 500,
					companyId: companyId
				});
			} else {
				store.getProxy().setUrl(commonwebserver + 'secured/admin/user/search');
				//store.getProxy().setUrl(commonwebserver + 'secured/admin/user/remotesearch');
				store.getProxy().setExtraParams({
					searchText: searchString,
					//category: searchCategory,
					companyId: companyId
				});
			}

			store.getProxy().afterRequest = function() {
				mobEdu.messages.f.toIdSearchListResponseHandler();
			};
			store.load();
		},

		toIdSearchListResponseHandler: function() {
			Ext.getCmp('allleads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allleadNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
			Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
			Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
			var store = mobEdu.util.getStore('mobEdu.messages.store.userList');
			var status = store.getProxy().getReader().rawData.status;
			if (status != 'success') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			if (store.currentPage == 1) {
				if (store.data.length > 0) {
					store.insert(0, {
						firstName: "Select All",
						lastName: " "
					});
				}
			};
		},

		reqEmailListBack: function() {
			var reqEListStore = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');
			reqEListStore.currentPage = 1;
			reqEListStore.load();
			mobEdu.messages.f.showRecruiterEList();
		},
		doCheckBeforeSearch: function(textfield) {
			mobEdu.util.get('mobEdu.messages.view.menu');
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						if (Ext.getCmp('pagingdirectoryplugin') == null) {
							var pArray = new Array();
							pArray[0] = mobEdu.directory.view.paging.create();
							Ext.getCmp('selectAll').setPlugins(pArray);
							Ext.getCmp('selectAll').setEmptyText('No search results');
						}
						Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().show();
						mobEdu.util.hideKeyboard();
						mobEdu.messages.f.clearMsgStore();
						mobEdu.messages.f.toIdSearchList(searchString);
					}
				}, 1000);
			} else {
				if (Ext.getCmp('pagingdirectoryplugin') != null) {
					Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
				}
				Ext.getCmp('selectAll').setPlugins(null);
				Ext.getCmp('selectAll').setEmptyText(null);
				mobEdu.messages.f.clearMsgStore();
			}
		},

		clearMsgStore: function() {
			var store = mobEdu.util.getStore('mobEdu.messages.store.userList');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
			//            store.sync();
		},

		doCheckBeforeMyLeadSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						mobEdu.util.hideKeyboard();
						mobEdu.enroute.leads.f.profileSearch();
					}
				}, 1000);
			}
		},
		doCheckBeforeAllLeadSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						mobEdu.util.hideKeyboard();
						mobEdu.messages.f.toIdSearchList(searchString);
					}
				}, 1000);
			}
		},
		onSearchToIdItemTap: function(viewRef, selectedIndex, target, record, item, e, eOpts) {
			var role = mobEdu.util.getRole();
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
			if (role.match(/SUPERVISOR/gi)) {
				if (selectedIndex == 0) {
					if (item.target.id == "on") {
						Ext.getCmp('allleads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allleadNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						// Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = true;
					} else {
						Ext.getCmp('allleads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allleadNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						// Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = false;
					}
				}

				//if selectall is checked and any item is unchecked
				if (mobEdu.messages.f.targetFlag == true && item.target.id == 'off') {
					var items = Ext.DomQuery.select("img[name='allleadNames']");
					items[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
					items[0].id = 'off';
				};
				//if all items are checked and Selectall is unchecked
				var items = Ext.DomQuery.select("img[name='allleadNames']");
				var allSelected = true;
				Ext.each(items, function(item, i) {
					if (item.id == 'off' && i != 0) {
						allSelected = false;
					};
				});
				if (allSelected) {
					items[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
					items[0].id = 'on';
					mobEdu.messages.f.targetFlag = true;
				};
			} else if (role.match(/RECRUITER/gi)) {
				if (selectedIndex == 0) {
					if (item.target.id == "on") {
						Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = true;
					} else {
						Ext.getCmp('selectMyLeads').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = false;
					}
				}
			} else {
				if (selectedIndex == 0) {
					if (item.target.id == "on") {
						Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = true;
					} else {
						Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
						mobEdu.messages.f.targetFlag = false;
					}
				}
			}
			//if selectall is checked and any item is unchecked
			if (mobEdu.messages.f.targetFlag == true && item.target.id == 'off') {
				var items = Ext.DomQuery.select("img[name='allNames']");
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				items[0].id = 'off';
			};
			//if all items are checked and Selectall is unchecked
			var items = Ext.DomQuery.select("img[name='allNames']");
			var allSelected = true;
			Ext.each(items, function(item, i) {
				if (item.id == 'off' && i != 0) {
					allSelected = false;
				};
			});
			if (allSelected) {
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				items[0].id = 'on';
				mobEdu.messages.f.targetFlag = true;
			};

		},

		onSearchUserToIdItemTap: function(selectedIndex, viewRef, record, item) {
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
		},

		selectedToNames: function() {
			var role = mobEdu.util.getRole();
			mobEdu.util.updatePrevView(mobEdu.messages.f.showRecruiterEList);
			if (role.match(/SUPERVISOR/gi)) {
				var searchList = mobEdu.util.getStore('mobEdu.messages.store.userList');
				var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
				localStore.removeAll();
				localStore.removeAll();
				var items = Ext.DomQuery.select("img[name='allleadNames']");
				Ext.each(items, function(item, i) {
					if (item.id == "on" && i != 0) {
						var rec = searchList.findRecord('userID', item.title);
						var fullName = rec.data.firstName + ' ' + rec.data.lastName;
						if (rec != null) {
							localStore.add({
								fullName: fullName,
								pidm: rec.data.userID
							});
							localStore.sync();
						}
					}
				});
			} else if (role.match(/RECRUITER/gi)) {
				var searchList = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
				var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
				localStore.removeAll();
				localStore.removeAll();
				var items = Ext.DomQuery.select("img[name='allNames']");
				Ext.each(items, function(item, i) {
					if (item.id == "on" && i != 0) {
						var rec = searchList.findRecord('leadID', item.title);
						var fullName = rec.data.firstName + ' ' + rec.data.lastName;
						if (rec != null) {
							localStore.add({
								fullName: fullName,
								pidm: rec.data.leadID
							});
							localStore.sync();
						}
					}
				});
			} else {
				var searchList = mobEdu.util.getStore('mobEdu.messages.store.userList');
				var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
				//localStore.removeAll();
				//localStore.removeAll();
				var items = Ext.DomQuery.select("img[name='allNames']");
				Ext.each(items, function(item, i) {
					if (item.id == "on" && i != 0) {
						var rec = searchList.findRecord('userID', item.title);
						var fullName = rec.data.firstName + ' ' + rec.data.lastName;
						if (rec != null) {
							localStore.add({
								fullName: fullName,
								pidm: rec.data.userID
							});
							localStore.sync();
						}
					}
				});
			}

			if (localStore.data.length == 0) {
				Ext.Msg.show({
					message: '<p>Please select user to send a message.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.updatePrevView(mobEdu.messages.f.showNewMessage);
							//mobEdu.messages.f.showNewMessage();
						}
					}
				});
			} else {
				mobEdu.util.show('mobEdu.messages.view.respondEmail');
				mobEdu.messages.f.setToField();
			}
			// var searchList = mobEdu.util.getStore('mobEdu.messages.store.userList');


		},
		selectedUserToNames: function() {
			mobEdu.messages.f.hideSearchPopup();
			var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			localStore.removeAll();
			localStore.removeAll();
			var items = Ext.DomQuery.select("img[name='allNames']");
			Ext.each(items, function(item, i) {
				if (item.id == "on") {
					var rec = searchList.findRecord('userID', item.title);
					var fullName = rec.data.firstName + ' ' + rec.data.lastName;
					if (rec != null) {
						localStore.add({
							fullName: fullName,
							pidm: rec.data.userID
						});
						localStore.sync();
					}
				}
			});
			mobEdu.messages.f.setToField();
		},
		hideSearchPopup: function() {
			//mobEdu.util.updatePrevView();
			var popUp = Ext.getCmp('usersearchPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},
		setToField: function() {
			var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			var selectVlaue = '';
			localStore.each(function(record) {
				if (selectVlaue == '') {
					selectVlaue = record.data.fullName;
				} else {
					selectVlaue = selectVlaue + ',' + record.data.fullName;
				}
			});
			Ext.getCmp('rToName').setValue(selectVlaue);
			Ext.getCmp('searchstId').setValue('');
			var store = mobEdu.util.getStore('mobEdu.messages.store.userList');
			store.removeAll();
			store.sync();
			store.removed = [];
		},
		ShowReplayAllMessagesView: function() {
			mobEdu.messages.f.newMsg = false;
			mobEdu.messages.f.replyMeg = true;
			mobEdu.util.updatePrevView(mobEdu.messages.f.showReqEmailDetail);
			mobEdu.util.show('mobEdu.messages.view.respondEmail');
			Ext.getCmp('resEmailTitle').setTitle('<h1>Reply Message</h1>');
			var msgDetailStore = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');
			var messageData = msgDetailStore.data.all;
			var toIds = new Array();
			var labelsText = new Array();
			var formIds = new Array();
			var toIdValues = '';
			var toNames = '';
			var subject = messageData[0].data.subject;
			var rmsg = messageData[0].data.body;
			formIds = messageData[0].data.from;
			toIds = messageData[0].data.to;
			labelsText = messageData[0].data.labels;
			for (var i = 0; i < labelsText.length; i++) {
				var labelName = labelsText[0].label;
			}
			if (labelName == "SENT") {
				for (var j = 0; j < toIds.length; j++) {
					if (toNames == '') {
						toNames = toIds[j].toName;
					} else {
						toNames = toNames + ',' + toIds[j].toName;
					}
				}
			} else {
				toNames = formIds.fromName;
			}
			Ext.getCmp('rToName').setValue(toNames);
			Ext.getCmp('rsub').setValue(subject);
			var toIdsLocalStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			toIdsLocalStore.removeAll();
			toIdsLocalStore.removed = [];
			toIdsLocalStore.sync();
		},

		sentUpdateEmail: function() {
			var upToIds = Ext.getCmp('rToName').getValue();
			var upsub = Ext.getCmp('rsub').getValue();
			var upArea = Ext.getCmp('rmsg').getValue();
			upsub = upsub.trim();
			upArea = upArea.trim();
			upToIds = upToIds.trim();
			if (upArea != '' && upsub != '' && upToIds != '') {
				mobEdu.messages.f.updateEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateEmail: function() {
			var upToIds = '';
			var esub = Ext.getCmp('rsub').getValue();
			var msgArea = Ext.getCmp('rmsg').getValue();
			var conversationID;
			var localStore = mobEdu.util.getStore('mobEdu.messages.store.toStore');
			var localStoreData = localStore.data.all;
			if (mobEdu.messages.f.newMsg) {
				if (localStoreData.length == 0) {
					upToIds = '';
				} else {
					localStore.each(function(record) {
						if (upToIds == '') {
							upToIds = record.data.pidm;
						} else {
							upToIds = upToIds + ',' + record.data.pidm;
						}
					});
				}
			};
			if (mobEdu.messages.f.replyMeg) {
				var store = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');
				var data = store.data.all;
				upToIds = data[0].data.from.fromID;
				conversationID = data[0].data.conversationID;
			}
			/*var store = mobEdu.util.getStore('mobEdu.messages.store.emailDetail');
			var data = store.data.all;
			var toIds = new Array();
			if (data.length == 0) {
				conversationID = '';
			} else {
				conversationID = data[0].data.conversationID;
				upToIds = data[0].data.to.toID;
			}*/
			var eStore = mobEdu.util.getStore('mobEdu.messages.store.respondEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/send');
			eStore.getProxy().setExtraParams({
				to: upToIds,
				subject: esub,
				body: msgArea,
				conversationID: conversationID,
				companyId: companyId,
				loginID: mobEdu.util.getStudentId()
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.messages.f.updateEmailResponseHandler();
			};
			eStore.load();
		},
		updateEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.messages.store.respondEmail');
			var NEStatus = nEStore.getProxy().getReader().rawData;
			if (NEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsendsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Message sent successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var store = mobEdu.util.getStore('mobEdu.messages.store.recruiterEmail');
							mobEdu.messages.f.reqEmailListBack();
							mobEdu.messages.f.resetSendReplyEmail();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: NEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetSendReplyEmail: function() {
			Ext.getCmp('rToName').reset();
			Ext.getCmp('rsub').reset();
			Ext.getCmp('rmsg').reset();
		},
		clearRecSearchStore: function() {
			var role = mobEdu.util.getRole();
			if (!role.match(/SUPERVISOR/gi)) {
				mobEdu.messages.f.toIdSearchList();
			} else {
				var store = mobEdu.util.getStore('mobEdu.directory.store.searchList');
				store.removeAll();
			}
		},

		clearSystemStore: function(component) {
			if (component != undefined) {
				component.setValue('');
			}
			var searchList = mobEdu.util.getStore('mobEdu.messages.store.userList');
			searchList.removeAll();
		},

		clearLeadsStore: function(component) {
			if (component != undefined) {
				component.setValue('');
			}
			var searchList = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			searchList.removeAll();
		},

		showSearchToIdsPopup: function(popupView) {
			mobEdu.util.get('mobEdu.messages.view.menu');
			Ext.getCmp('searchstId').setValue('');
			Ext.getCmp('searchItem').setValue('');
			Ext.getCmp('searchMsgCategory').reset();
			Ext.getCmp('sALItem').setValue('');
			var searchList = mobEdu.util.getStore('mobEdu.messages.store.userList');
			searchList.removeAll();
			var searchList = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			searchList.removeAll();
			var role = mobEdu.util.getRole();
			if (role.match(/SUPERVISOR/gi)) {
				Ext.getCmp('enrouteTabs').getTabBar().getComponent(0).hide();
				Ext.getCmp('enrouteTabs').getTabBar().getComponent(1).hide();
				Ext.getCmp('enrouteTabs').setActiveItem(2);
			} else if (role.match(/RECRUITER/gi)) {
				Ext.getCmp('enrouteTabs').getTabBar().getComponent(0).hide();
				Ext.getCmp('enrouteTabs').getTabBar().getComponent(2).hide();
				Ext.getCmp('enrouteTabs').setActiveItem(1);
			} else {
				Ext.getCmp('enrouteTabs').getTabBar().hide();
			}
			mobEdu.util.updatePrevView(mobEdu.messages.f.toBackTap);
			mobEdu.util.show('mobEdu.messages.view.menu');
			if (Ext.getCmp('pagingdirectoryplugin') != null) {
				Ext.getCmp('pagingdirectoryplugin').getLoadMoreCmp().hide();
			}
			Ext.getCmp('selectAll').setPlugins(null);
			Ext.getCmp('selectAll').setEmptyText(null);
			mobEdu.messages.f.clearMsgStore();
			// Ext.getCmp('enrouteTabs').setActiveItem(0);

		},

		toBackTap: function() {
			mobEdu.messages.f.newMsg = true;
			mobEdu.messages.f.replyMeg = false;
			mobEdu.util.updatePrevView(mobEdu.messages.f.showRecruiterEList);
			mobEdu.util.show('mobEdu.messages.view.respondEmail');
			Ext.getCmp('resEmailTitle').setTitle('<h1>New Message</h1>');
			Ext.getCmp('searchHelp').show();
		},

		clearMsgStore: function() {
			var store = mobEdu.util.getStore('mobEdu.messages.store.userList');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
		}

	}
});