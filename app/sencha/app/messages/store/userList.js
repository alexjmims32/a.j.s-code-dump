Ext.define('mobEdu.messages.store.userList', {    
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.enroute.main.model.allLeadsList'
    ],
    config: {
        storeId: 'mobEdu.messages.store.userList',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.allLeadsList'

    },

    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'rows'
        return proxy;
    }
});