Ext.define('mobEdu.messages.store.emailDetail', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.messages.model.recruiterEmail'
    ],

    config: {
        storeId: 'mobEdu.messages.store.emailDetail',

        autoLoad: false,

        model: 'mobEdu.messages.model.recruiterEmail',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
            }
        }

    }

});