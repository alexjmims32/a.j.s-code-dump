Ext.define('mobEdu.messages.store.toStore', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.directory.model.people'
    ],

    config:{
        storeId: 'mobEdu.messages.store.toStore',
        autoLoad: true,
        model: 'mobEdu.directory.model.people',
        proxy:{
            type:'localstorage',
            id:'toStoreLocal'
        }
    }
});