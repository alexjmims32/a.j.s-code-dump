Ext.define('mobEdu.messages.store.profileSearch', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enroute.leads.model.profileList'
    ],
    config: {
        storeId: 'mobEdu.messages.store.profileSearch',

        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.profileList',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'leads'
            }
        }
    }
});