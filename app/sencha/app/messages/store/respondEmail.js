Ext.define('mobEdu.messages.store.respondEmail', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enroute.main.model.recruiterEmail'
    ],
    config: {
        storeId: 'mobEdu.messages.store.respondEmail',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.recruiterEmail',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
            }
        }
    }
});