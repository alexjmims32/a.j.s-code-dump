Ext.define('mobEdu.feeds.view.rssList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.feeds.store.rssFeeds'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'feedsLinks',
            plugins: [{
                xclass: 'Ext.plugin.ListPaging',
                autoPaging: true,
                id: 'feedsPagingPlugin'
            }],
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>', '<tpl if="(mobEdu.feeds.f.isExists(icon)===true)">', '<td width="10%" align="left"><img width="120pt" height="90pt" src="{icon}"></td>', '</tpl>', '<td width="100%" align="left" ><h3 class="subnavText">{[mobEdu.util.filterSpecialCharacters(values.title)]}</h3></td><td width="5%" align="middle" nowrap><h5 class="listDate">{day}<br />{date}</h5></td><td width="10%"><div align="right" class="arrow" /></td></tr>', '<tpl if="(mobEdu.feeds.f.isExists(beginTime)===true)">', '<tr><td>{beginTime}', '<tpl if="(mobEdu.feeds.f.isExists(endTime)===true)"> until {endTime}</tpl></td></tr>', '<tr><td>{buildingCode}<tpl if="(mobEdu.feeds.f.isExists(roomCode)===true)"> Room No: {roomCode}</tpl></td></tr>', '</tpl>', '</table>'),
            store: mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds'),
            emptyText: 'No feeds data available.',
            singleSelect: true,
             scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.feeds.f.loadFeedsDetail(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            id: 'eventsBar',
            name: 'eventsBar',
            hidden: true,
            items: [{
                xtype: 'spacer'
            }, {
                xtype: 'button',
                text: 'Calendar',
                handler: function() {
                    mobEdu.feeds.f.showEventsCalendarView();
                }
            }]
        }, {
            xtype: 'customToolbar',
            id: 'rssFeedsTitle'
        }],
        flex: 1
    }
});