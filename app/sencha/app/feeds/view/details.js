Ext.define('mobEdu.feeds.view.details', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.feeds.store.feedsDetail'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            setHtmlContent: true,
            padding: '10 10 10 10',
            xtype: 'dataview',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td><b>{[mobEdu.util.filterSpecialCharacters(values.title)]}<b></td></tr><tpl if="(mobEdu.feeds.f.curModuleCode===\'EVENTS\')">' + '<tpl if="(mobEdu.feeds.f.showExtraDetails()===false)"><tr><td><h2 class="listDate">{day} {date}</h2></td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.showExtraDetails()===true)">' + '<tpl if="(mobEdu.feeds.f.isExists(siteDesc)===true)"><tr><td>{siteDesc}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(crn)===true)"><tr><td><b>CRN</b> : {crn}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(pubDate)===true)">', '<tr><td><b>When</b> : {[mobEdu.util.displayDateFormat(values.startDate)]}</tpl>', '<tpl if="(mobEdu.feeds.f.isExists(beginTime)===true)"> at {beginTime}', '<tpl if="(mobEdu.feeds.f.isExists(endTime)===true)"> until {endTime}</tpl></tpl>', '<tpl if="(mobEdu.feeds.f.isEndDateExists(startDate,endDate)===true)"><br>Repeats on every {dayDesc} each week until {[mobEdu.util.displayDateFormat(values.endDate)]}  </td> </tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(buildingDesc)===true)"><tr><td><b>Where</b> : {buildingDesc}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(roomDesc)===true)"><tr><td style="padding-top:10px;">{roomDesc}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(campDesc)===true)"><tr><td>{campDesc}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(localAdd1)===true)"><tr><td>{localAdd1}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(localAdd2)===true)"><tr><td>{localAdd2}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(locCity)===true)"><tr><td>{locCity}<tpl if="(mobEdu.feeds.f.isExists(locState)===true)">,{locState}</tpl><tpl if="(mobEdu.feeds.f.isExists(locZip)===true)"> {locZip}</tpl></td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(agencyName)===true)"><tr><td><b>Agency Sponsor</b> : {agencyName}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(cntName)===true)"><tr><td><b>Contact</b> : {cntName}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(cntPhone)===true)"><tr><td>{cntPhone}</td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(cntMail)===true)"><tr><td>{cntMail}</td></tr></tpl></tpl></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(link)===true)"><tr><td><b>Event Website :</b> <h6>{link}</h6></td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.isExists(comments)===true)"><tr><td style="padding-top:10px;"><b>Comments</b> : <h7>{comments}</h7></td></tr></tpl>', '<tpl if="(mobEdu.feeds.f.curModuleCode===\'JOBS\')"><tr><td><h2><p>Published Date: {[mobEdu.feeds.f.removeCharacters(values.pubDate)]}</p></h2></td></tr>' + '<tr><td><h2><p>Last UpdatedDate: {[mobEdu.feeds.f.removeCharacters(values.updated)]}</p></h2></td></tr></tpl>' + '<tr><td><h3><p class="text">{[this.getDescription(values.description)]}</p></h3></td></tr>', '<tpl if="(mobEdu.feeds.f.isContentExists(encoded)===true)">', '<tr><td><h3><p>{[this.getContent(values.encoded)]}</p></h3></td></tr>', '</tpl>', '<tpl if="(mobEdu.feeds.f.isLinkExists(linkurl)===true)">', '<tr><td><a href="javascript:mobEdu.util.loadExternalUrl(\'{linkurl}\')"><p class="listReadMore">More Info</p></a></td></tr>', '</tpl>', '<tpl if="(mobEdu.feeds.f.isLinkExists(link)===true)">', '<tr><td><a href="javascript:mobEdu.util.loadExternalUrl(\'{link}\')"><p class="listReadMore">More Info</p></a></td></tr>', '</tpl>', '<tpl if="(mobEdu.feeds.f.curModuleCode===\'JOBS\')">', '<tr><td><a href="javascript:mobEdu.util.loadExternalUrl(\'{[mobEdu.feeds.f.getJobsLink(values.jobLinks)]}\')"><p class="listReadMore">More Info</p></a></td></tr>', '</tpl>', '<tpl for="url">', '<tpl if="(mobEdu.feeds.f.isImageExists(description, url)===true)">', '<tr><td><img class="imageWidth" src={url} /></td></tr>', '</tpl>', '</tpl>', '</table>', {
                compiled: true,
                getDescription: function(description) {
                    return mobEdu.util.convertRelativeToAbsoluteUrls(description);
                },
                getContent: function(content) {
                    return mobEdu.util.convertRelativeToAbsoluteUrls(content);
                }
            }),
            store: mobEdu.util.getStore('mobEdu.feeds.store.feedsDetail'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Details</h1>'
        }],
        flex: 1
    }
});