Ext.define('mobEdu.feeds.view.socialMedia', {
    extend:'Ext.Panel',
    requires: [

    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
            {
                xtype:'list',
                cls:'logo',
                name:'sMediaList',
                id:'sMediaList',
                itemTpl:'<table class="menu" width="100%"><tr><td width="5%" align="left"><img src="{img}"></td><td width="3%" /><td width="85%" align="left"><p class="listTitle">{title}</p></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal'),
                singleSelect:true,
                loadingText:'',
                listeners:{
                    itemtap:function (view, index, item, e) {
                        setTimeout(function () {
                            view.deselect(index);
                        }, 500);
                        e.data.action();
                    }
                }
            },
            {
                title: '<h1>Social Media</h1>',
                xtype: 'customToolbar',
                id:'sMediaTitle',
                name:'sMediaTitle'
            }
        ],
        flex:1
    },
    initialize: function(){
        mobEdu.feeds.f.initializeSocialMedia();
    }

});
