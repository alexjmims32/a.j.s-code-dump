Ext.define('mobEdu.feeds.view.list', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.feeds.store.feedsLocal'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'feedsList',
            itemTpl: '<table width="100%"><tr><td width="80%" align="left" ><h3 class="subnavText">{name}</h3></td><td width="20%" align="right" ><h4 class="listDate">{date}</h4></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal'),
            singleSelect: true,
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.feeds.f.onFeedsItemTap(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'feedsTitles'
        }, {
            layout: 'hbox',
            docked: 'top',
            id: 'evntSearch',
            items: [{
                xtype: 'textfield',
                name: 'eventSearchText',
                id: 'eventSearchText',
                flex: 1,
                placeHolder: 'Search for a course'
            }, {
                xtype: 'button',
                name: 'searchEvent',
                id: 'searchEvent',
                maxWidth: '32px',
                cls: 'iconBackground',
                flex: 3,
                text: '<img src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png" height="24px" width="24px">',
                style: {
                    border: 'none',
                    background: 'none',
                    backgroundColor: '#fff'
                },
                handler: function() {
                    mobEdu.feeds.f.searchEvents(Ext.getCmp('eventSearchText'));
                }
            }]
        }],
        flex: 1
    }
});