Ext.define('mobEdu.feeds.view.videoView', {
	extend: 'Ext.Container',
	alias: 'videoView',
	config: {
		fullscreen: 'true',
		layout: {
			type: 'vbox',
			align: 'strech'
		},
		cls: 'logo',
		defaults: {
			flex: 0
		},
		scrollable: true,
		items: [{
				xtype: 'panel',
				id: 'videoPanel',
				html: ''
			}, {
				xtype: 'customToolbar',
				docked: 'top',
				title: '<h1>Video</h1>'
			}]
	}
});