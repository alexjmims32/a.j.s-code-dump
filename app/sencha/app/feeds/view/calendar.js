Ext.define('mobEdu.feeds.view.calendar', {
	extend: 'Ext.Panel',
	alias: 'eventsCalendar',
	config: {
		fullscreen: 'true',
                cls: 'backGround',
		scrollable: true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Events</h1>'
        }, {
            id: 'eventsDock',
            name: 'eventsDock',
            items: [{}]
        }, {
            xtype: 'label',
            id: 'eventsLabel',
            padding: 10
        }]
    }
});
		