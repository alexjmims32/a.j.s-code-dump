Ext.define('mobEdu.feeds.f', {
	requires: [
		'mobEdu.main.store.modulesLocal',
		'mobEdu.feeds.store.feedsMenus',
		'mobEdu.feeds.store.feedsLocal',
		'mobEdu.feeds.store.feedsDetail',
		'mobEdu.feeds.store.rssFeeds',
		'mobEdu.main.store.encore'
	],
	statics: {
		curParentID: null,
		curModuleID: null,
		curModuleCode: null,
		curFeedID: null,
		editMode: false,
		moduleTitle: null,
		// This is a stack to be used for back button navigation in the feeds module
		navStack: new Array(),

		loadFeeds: function(code) {
			if (code === undefined || code === null) {
				code = mobEdu.util.loginModuleCode;
			}
			var id = mobEdu.util.getModuleId(code);
			mobEdu.feeds.f.curModuleID = id;
			mobEdu.feeds.f.curModuleCode = code;
			mobEdu.feeds.f.editMode = false;
			mobEdu.util.get('mobEdu.feeds.view.list');

			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
			var currentRecord;
			feedLocalStore.each(function(record) {
				if (record.data.moduleId === id) {
					currentRecord = record;
				}
			});

			feedLocalStore.getServerProxy().setExtraParams({
				clientCode: campusCode,
				page: 0
			});

			// Handle one case directly here
			// If the module is just an external link - load it directly
			// Else take it through the loadFeedList recursion
			if (currentRecord.data.feeds.length == 1 && currentRecord.data.feeds[0].format == 'html') {
				if (code === 'MOODLE') {
					mobEdu.courses.f.moodlePageLoad();
				} else {
					mobEdu.util.loadExternalUrl(currentRecord.data.feeds[0].link);
				}
			} else if (currentRecord.data.feeds.length == 1 && currentRecord.data.feeds[0].format == 'CAS') {
				mobEdu.util.openURLWithCAS(currentRecord.data.feeds[0].link);
			} else {
				// We just started loading a module
				// Clear the navigation stack and start...
				this.navStack = [];
				mobEdu.feeds.f.loadFeedList(currentRecord);
			}
		},

		loadFeedList: function(currentRecord, tplFlag) {

			// 3 cases to be handled here
			// Each menu item can have
			// 1 A link itself - leaf node
			// 2 A list of items to be shown as a menu
			// 3 Only one sub-item in which case we go one level deep to avoid showing single menus

			mobEdu.util.get('mobEdu.feeds.view.list');

			var cmp = Ext.getCmp('feedsTitles');

			var records = null;
			if (currentRecord.data == undefined) {
				records = currentRecord.feeds;
				currentRecord.data = currentRecord;
			} else {
				records = currentRecord.data.feeds;
			}

			if (records.length == 0) {
				if (!mobEdu.feeds.f.editMode) {
					if (currentRecord.data.format == 'html') {
						mobEdu.util.loadExternalUrl(currentRecord.data.link);
					} else if (currentRecord.data.format == 'VIDEO') {
						mobEdu.feeds.f.loadVideoView(currentRecord.data);
					} else if (currentRecord.data.type == 'LINK' || currentRecord.data.type == 'ical' || currentRecord.data.type == 'GDATA') {
						mobEdu.feeds.f.loadRssFeed(currentRecord.data);
					} else if (currentRecord.data.type == 'MENU') {
						mobEdu.util.showMainView();
						Ext.Msg.show({
							title: null,
							cls: 'msgbox',
							message: '<center>No info, please check back soon.</center>',
							buttons: Ext.MessageBox.OK
						});
					}
				} else {
					Ext.Msg.show({
						title: null,
						cls: 'msgbox',
						message: '<center>No submenu inside this item.</center>',
						buttons: Ext.MessageBox.OK
					});
				}
			} else if (records.length == 1) {
				// Case # 3
				mobEdu.feeds.f.loadFeedList(records[0]);
			} else {
				// Case # 2
				mobEdu.util.setTitle(null, cmp, currentRecord.data.name);
				this.navStack.push(currentRecord);
				var feedsLocal = mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal');
				if (feedsLocal.data.length > 0) {
					feedsLocal.removeAll();
				}
				var encoreStore = mobEdu.util.getStore('mobEdu.main.store.encore');
				var feedsArray = new Array();
				var rec = encoreStore.getAt(0);
				if (rec != null && rec != '') {
					feedsArray = rec.data.hiddenFeeds;
				}
				for (i = 0; i < records.length; i++) {
					if (!mobEdu.feeds.f.editMode) {
						if (!(Ext.Array.contains(feedsArray, records[i].feedId))) {
							feedsLocal.add(records[i]);
						}
					} else {
						feedsLocal.add(records[i]);
					}
				}
				if (!mobEdu.feeds.f.editMode) {
					mobEdu.feeds.f.showFeeds();
				} else {
					mobEdu.settings.f.updateItemTpl(tplFlag);
				}
			}
		},

		searchEvents: function(searchCmp) {
			var searchText = searchCmp.getValue();
			searchText = searchText.trim();
			if (searchText === '') {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>Please enter some text to search.</center>',
					buttons: Ext.MessageBox.OK
				});
			} else {
				var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
				store.removed = [];
				store.removeAll();
				store.getProxy().setHeaders({
					companyID: companyId
				});
				store.getProxy().setUrl(webserver + 'getFeedsFromDB');
				store.getProxy().setExtraParams({
					category: null,
					searchText: searchText
				});
				store.getProxy().afterRequest = function() {
					mobEdu.feeds.f.searchEventsResponseHandler();
				};
				store.loadPage(1);
				mobEdu.util.gaTrackEvent('enroll', 'eventSearch');
			}
		},

		searchEventsResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			mobEdu.feeds.f.showRssFeeds();
		},

		loadRssFeed: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			store.removeAll();
			store.getProxy().setHeaders({
				companyID: companyId
			});
			if (feedsSource === 'Banner' && mobEdu.feeds.f.curModuleCode === 'EVENTS') {
				store.getProxy().setUrl(rec.link);
			} else {
				store.getProxy().setUrl(encorewebserver + 'open/feeds/content');
				store.getProxy().setExtraParams({
					feedId: rec.feedId,
					campusCode: campusCode,
					url: rec.link,
					type: rec.type,
					format: rec.format,
					name: rec.name,
					clientCode: campusCode
				});
			}
			store.getProxy().afterRequest = function() {
				mobEdu.feeds.f.rssFeedResponseHandler(rec);
			};
			store.loadPage(1);
			mobEdu.util.gaTrackEvent('encore', 'feedcontent');
		},

		rssFeedResponseHandler: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			if (store.data.length > 0) {
				if (mobEdu.feeds.f.curModuleCode == 'EVENTS') {
					store.sort('date', 'ASC');
				} else {
					store.sort('date', 'DESC');
				}

				if (store.data.length == 1) {
					var record = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds').data.items[0];
					var view = mobEdu.util.get('mobEdu.feeds.view.rssList');
					mobEdu.feeds.f.loadFeedsDetail(view, 0, null, record, null, null, null);
				} else {
					mobEdu.feeds.f.showRssFeeds();
					Ext.getCmp('rssFeedsTitle').setTitle('<h1>' + rec.name + '</h1>');
				}
			} else {
				mobEdu.util.showMainView();
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>No info, please check back soon.</center>',
					buttons: Ext.MessageBox.OK
				});
			}
		},

		loadVideoView: function(record) {
			mobEdu.util.destroyView('mobEdu.feeds.view.videoView');
			mobEdu.util.get('mobEdu.feeds.view.videoView');
			var link = record.link.replace('watch?v=', 'embed/');
			link = link.replace('&', '?');

			if (Ext.os.is.Android) {
				var videoId = link.replace("http://www.youtube.com/embed/", "");
				videoId = videoId.split("&")[0];
				videoId = videoId.split("?")[0];
				window.plugins.youtube.show({
					videoid: videoId
				}, function() {}, function() {});
			} else {
				Ext.getCmp('videoPanel').setHtml('<embed id="player" width="100%" height="100%" src="' + link + '" frameborder="0" allowfullscreen></embed>');
				if (mainMenuLayout == 'BGRID') {
					if (Ext.Viewport.getOrientation() === 'portrait') {
						Ext.getCmp('videoPanel').setHtml('<embed id="player" style="height:60vh;" width="100%"  src="' + link + '" frameborder="0" allowfullscreen></embed>');
					} else {
						Ext.getCmp('videoPanel').setHtml('<embed id="player" style="height:50vh;" width="100%"  src="' + link + '" frameborder="0" allowfullscreen></embed>');
					}

				} else {
					Ext.getCmp('videoPanel').setHtml('<embed id="player" style="height:94vh;" width="100%"  src="' + link + '" frameborder="0" allowfullscreen></embed>');
				}

				mobEdu.feeds.f.showVideoView();
			}
		},

		onRssBackTap: function() {
			var poppedItem = mobEdu.feeds.f.navStack.pop();
			if (poppedItem != null) {
				mobEdu.feeds.f.navStack.push(poppedItem);
				mobEdu.feeds.f.showFeeds();
			} else {
				mobEdu.util.showMainView();
				if (mainMenuLayout == 'BGRID' && Ext.os.is.Phone) {
					mobEdu.main.f.displayBevelGridMenu();
				}
			}
		},

		onBackButtonTap: function() {
			var poppedItem = mobEdu.feeds.f.navStack.pop();
			poppedItem = mobEdu.feeds.f.navStack.pop();
			if (poppedItem != null) {
				var item = mobEdu.feeds.f.navStack.pop();
				if (item == null) {
					mobEdu.feeds.f.loadFeedList(poppedItem, false);
				} else {
					mobEdu.feeds.f.navStack.push(item);
					mobEdu.feeds.f.loadFeedList(poppedItem, true);
				}
			} else {
				if (mobEdu.feeds.f.editMode) {
					mobEdu.settings.f.showMenu();
				} else {
					mobEdu.util.showMainView();
				}
			}
		},

		loadFeedsDetail: function(view, index, target, record, item, e, eOpts) {
			if (view != 'mobEdu.feeds.view.calendar') {
				mobEdu.util.deselectSelectedItem(index, view);
			}
			if (mobEdu.feeds.f.curModuleCode == 'VIDEOS') {
				mobEdu.feeds.f.loadVideoView(record.data);
			} else {
				var store = mobEdu.util.getStore('mobEdu.feeds.store.feedsDetail');
				store.removeAll();
				store.removed = [];
				store.sync();
				store.add(record.copy());
				store.sync();
				var type = null;
				if (mobEdu.feeds.f.curModuleCode == 'NEWS') {
					type = newsDetailType;
				}
				if (type == 'LINK') {
					if (record.data.link != null && record.data.link != '') {
						mobEdu.util.loadExternalUrl(record.data.link);
					} else {
						Ext.Msg.show({
							title: null,
							cls: 'msgbox',
							message: '<center>No link exists.</center>',
							buttons: Ext.MessageBox.OK
						});
					}
				} else {
					if ((record.data.description == null || record.data.description == '') &&
						(record.data.link != null && record.data.link != '')) {
						//mobEdu.news.f.showLinkView();
						mobEdu.util.loadExternalUrl(record.data.link);
					} else {
						mobEdu.feeds.f.showFeedsDetail(view);
					}
				}
			}
		},

		updateItemTpl: function(listCmp) {
			if (mobEdu.feeds.f.editMode) {
				listCmp.setItemTpl(new Ext.XTemplate('<table width="100%"><tr>' +
					'<td width="75%" align="left" ><h3>{name}</h3></td>' +
					'<td width="20%" align="right" ><h4>{date}</h4></td>' +
					'<td width="5%"><img src="' + mobEdu.util.getResourcePath() + 'images/arrow.png" align="right"></td>' +
					'</tr></table>'));
			} else {
				listCmp.setItemTpl(new Ext.XTemplate('<table width="100%"><tr>' +
					'<td width="5%">' +
					'<div style="background: transparent; width: 50px;" class="x-field-checkbox x-field">',
					'<tpl if="mobEdu.util.isFeedIdExists(feedId)===false">',
					'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 name="on" />' +
					'<tpl else>',
					'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 name="off" />',
					'</tpl>' +
					'</div>' +
					'<td width="75%" align="left" ><h3>{name}</h3></td>' +
					'<td width="20%" align="right" ><h4>{date}</h4></td>' +
					'</tr></table>'));
			}
			listCmp.refresh();
		},

		showFeeds: function() {
			if (showSearchInEvents === 'true' && mobEdu.feeds.f.curModuleCode == 'EVENTS') {
				Ext.getCmp('evntSearch').show();
			} else {
				Ext.getCmp('evntSearch').hide();
			}
			mobEdu.util.updatePrevView(mobEdu.feeds.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.feeds.view.list');
		},
		showRssFeeds: function() {
			mobEdu.util.updatePrevView(mobEdu.feeds.f.onRssBackTap);
			mobEdu.util.show('mobEdu.feeds.view.rssList');
			if (mobEdu.feeds.f.curModuleCode == 'EVENTS') {
				Ext.getCmp('eventsBar').show();
			} else {
				Ext.getCmp('eventsBar').hide();
			}
		},
		showFeedsDetail: function(view) {
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			if (view === 'mobEdu.feeds.view.calendar') {
				mobEdu.util.updatePrevView(mobEdu.feeds.f.showEventsCalView);
			} else {
				if (store.data.length == 1) {
					mobEdu.util.updatePrevView(mobEdu.util.showMainView);
				} else {
					mobEdu.util.updatePrevView(mobEdu.feeds.f.showRssFeeds);
				}
			}
			mobEdu.util.show('mobEdu.feeds.view.details');
		},

		showEventsCalendarView: function() {
			mobEdu.util.updatePrevView(mobEdu.feeds.f.showRssFeeds);
			mobEdu.feeds.f.calendarViewShow();
			mobEdu.util.show('mobEdu.feeds.view.calendar');
		},

		showEventsCalView: function() {
			mobEdu.util.updatePrevView(mobEdu.feeds.f.showRssFeeds);
			mobEdu.util.show('mobEdu.feeds.view.calendar');
		},

		onFeedsItemTap: function(view, index, target, record, item, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			if (!(mobEdu.feeds.f.editMode)) {
				if (record.data.type == 'MENU') {
					if (record.data.feeds.length > 0) {
						mobEdu.feeds.f.loadFeedList(record);
					} else {
						Ext.Msg.show({
							title: null,
							cls: 'msgbox',
							message: '<center>No info, please check back soon.</center>',
							buttons: Ext.MessageBox.OK
						});
					}
				} else {
					mobEdu.feeds.f.loadFeedList(record);
				}
			} else {
				if (item.target.name == "on") {
					mobEdu.feeds.f.feedsOn(record.data.feedId);
				} else if (item.target.name == "off") {
					mobEdu.feeds.f.feedsOff(record.data.feedId);
				}
			}
		},

		feedsOn: function(feedId) {
			var cmp = Ext.getCmp('feedsList');
			mobEdu.util.addFeedToHiddenFeeds(feedId, cmp);
		},

		feedsOff: function(feedId) {
			var cmp = Ext.getCmp('feedsList');
			mobEdu.util.removeFeedFromHiddenFeeds(feedId, cmp);
		},

		onVideoBackButtonTap: function() {
			var myPlayer = document.getElementById('player');
			if (myPlayer != null) {
				myPlayer.parentNode.removeChild(myPlayer);
			}
			var videosListStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal');
			var videosListData = videosListStore.data.all;
			if (videosListData.length != 0) {
				if (videosListData[0].data.format == 'VIDEO') {
					mobEdu.feeds.f.showFeeds();
				} else {
					mobEdu.feeds.f.showRssFeeds();
				}
			} else {
				mobEdu.feeds.f.showRssFeeds();
			}
		},

		isImageExists: function(description, url) {
			if (url == null || url == '') {
				return false;
			} else if (description == null || description == '') {
				return true;
			} else if (description.search(url) != -1) {
				return false;
			}
			return true;
		},
		isLinkExists: function(url) {
			if (url == null || url == '') {
				return false;
			}
			return true;
		},
		isContentExists: function(content) {
			if (content == null || content == '' || content == undefined) {
				return false;
			}
			return true;
		},

		onEditButtonTap: function() {
			var cmp = Ext.getCmp('feedsEdit');
			mobEdu.feeds.f.updateFeeds(false);
			mobEdu.feeds.f.updateItemTpl(Ext.getCmp('feedsList'));
			cmp.hide();
			mobEdu.feeds.f.editMode = true;
			Ext.getCmp('feedsList').refresh();
		},

		updateFeeds: function(doFilter) {
			if (mobEdu.feeds.f.curParentID == mobEdu.feeds.f.curModuleID) {
				mobEdu.feeds.f.loadFeedList(mobEdu.feeds.f.curModuleID, 0, doFilter)
			} else {
				mobEdu.feeds.f.loadFeedList(mobEdu.feeds.f.curModuleID, mobEdu.feeds.f.curParentID, doFilter);
			}
		},

		//Sorting the min date
		getMinDate: function() {
			var dates = new Array();
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			store.each(function(record) {
				var date = new Date(record.get('pubDate'));
				dates.push(date);
				//                    dates.push(Ext.Date.format(date, "m-d-Y"));
			});
			dates = mobEdu.util.sortDates(dates);
			var minDate = Ext.Date.format(dates[0], "m-d-Y");
			var minEventDate = Ext.Date.parse(minDate, "m-d-Y");
			return minEventDate;
		},
		//To highlight Schedule dates
		doHighlightEventsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			store.each(function(record) {
				var eventdate = new Date(record.get('pubDate'));
				if (eventdate.getDate() == values.date.getDate() && eventdate.getMonth() == values.date.getMonth() && eventdate.getFullYear() == values.date.getFullYear()) {
					classes.push(highlightedItemCls);
				}
			});
			return classes;
		},
		isEventExists: function(eventDate) {
			var calDate = calendar.getValue();
			eventDate = Ext.Date.parse(eventDate, "d-m-Y");
			if (!(calDate < eventDate || calDate > eventDate)) {
				return true;
			}
			return false;
		},
		isImageExists: function(description, url) {
			if (url == null || url == '') {
				return false;
			} else if (description == null || description == '') {
				return true;
			} else if (description.search(url) != -1) {
				return false;
			}
			return true;
		},
		//Show the calendar view
		calendarViewShow: function() {
			mobEdu.main.f.calendarInstance = 'events';
			mobEdu.util.get('mobEdu.feeds.view.calendar');
			var eventsDock = Ext.getCmp('eventsDock');
			eventsDock.remove(calendar);
			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'calend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			eventsDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.feeds.f.getEventInfo();
		},

		dateFormatted: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},

		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.feeds.f.dateFormatted([currentDate.getMonth() + 1]) + '-' + mobEdu.feeds.f.dateFormatted(currentDate.getDate()) + '-' + currentDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.feeds.f.dateFormatted([presentDate.getMonth() + 1]) + '-' + mobEdu.feeds.f.dateFormatted(presentDate.getDate()) + '-' + presentDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.feeds.f.getEventInfo(date)
			}
		},

		getEventInfo: function(date) {
			if (date == null) {
				date = new Date();
			}
			//get the store info 
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			var scheduleInfo = '';
			scheduleInfo = scheduleInfo + '<table>';
			var index = 0;
			store.each(function(record) {
				var storeDateValue = new Date(record.get('pubDate'));
				if (date.getDate() == storeDateValue.getDate() && date.getMonth() == storeDateValue.getMonth() && date.getFullYear() == storeDateValue.getFullYear()) {
					//display the first start date events schedule info 
					scheduleInfo = mobEdu.feeds.f.formatEventInfo(scheduleInfo, record.get('title'), record.get('description'), index);
				}
				index++;
			});
			//set the event schedule info in calendar view label 
			scheduleInfo = scheduleInfo + '</table>';
			scheduleInfo = mobEdu.util.formatUrls(scheduleInfo);
			Ext.getCmp('eventsLabel').setHtml(scheduleInfo);
		},
		formatEventInfo: function(scheduleInfo, eTitle, eDescription, index) {
			if (eTitle == null) {
				eTitle = '';
			}
			if (eDescription == null) {
				eDescription = '';
			}
			if (Ext.os.is.Phone) {
				scheduleInfo = scheduleInfo + '<tr><td onclick="javascript:mobEdu.feeds.f.loadFeedDetailsByIndex(' + index + ');"><div class="wordWrap"><p><b><h7>' + eTitle + '</h7></b></p></div></td></tr>';
				scheduleInfo = scheduleInfo + '<tr><td><div class="wordWrap"><p><h4>' + eDescription.replace(' ', '') + '</h4></p></div></td></tr>';
			} else {
				scheduleInfo = scheduleInfo + '<tr><td onclick="javascript:mobEdu.feeds.f.loadFeedDetailsByIndex(' + index + ');"><div class="wordWrap"><p><h2>' + eTitle + '</h2></p></div></td></tr>';
				scheduleInfo = scheduleInfo + '<tr><td><div class="wordWrap"><p><h4>' + eDescription.replace(' ', '') + '</h4></p></div></td></tr>';
			}

			return scheduleInfo;
		},

		loadFeedDetailsByIndex: function(index) {
			var store = mobEdu.util.getStore('mobEdu.feeds.store.rssFeeds');
			var record = store.getAt(index);
			mobEdu.feeds.f.loadFeedsDetail('mobEdu.feeds.view.calendar', 0, null, record, null, null, null);
		},

		initializeSocialMedia: function() {
			var store = mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal');
			store.removeAll();
			store.removed = [];
			store.sync();


			store.add({
				img: mobEdu.util.getResourcePath() + 'images/socialMedia/facebook.png',
				title: '<div class="listTitle">Facebook</div>',
				action: mobEdu.feeds.f.facebookPageLoad
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/socialMedia/twitter.png',
				title: '<div class="listTitle">Twitter</div>',
				action: mobEdu.feeds.f.twitterPageLoad
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/socialMedia/instagram.png',
				title: '<div class="listTitle">Instagram</div>',
				action: mobEdu.feeds.f.instagramPageLoad
			});
			store.sync();

		},
		showSocialMedia: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.feeds.view.socialMedia');
		},

		showVideoView: function() {
			mobEdu.util.updatePrevView(mobEdu.feeds.f.onVideoBackButtonTap);
			mobEdu.util.show('mobEdu.feeds.view.videoView');
		},
		removeCharacters: function(data) {
			if (data != null) {
				data = data.replace('T', ' ');
				return data;
			}
			return '';
		},
		getJobsLink: function(jobLinks) {
			var jobLink = jobLinks[0].url;
			return jobLink;
		},
		isExists: function(value) {
			if (value === '' || value === undefined || value === null || value === 'All Day Event') {
				return false;
			} else {
				return true;
			}
		},
		isEndDateExists: function(startDate, endDate) {
			if (endDate !== undefined || endDate !== null || endDate !== '') {
				if (startDate === endDate) {
					return false;
				} else {
					return true;
				}
			} else {
				return false;
			}
		},
		showExtraDetails: function() {
			if (showExtraEventDetails === 'true') {
				return true;
			} else {
				return false;
			}
		}
	}
});