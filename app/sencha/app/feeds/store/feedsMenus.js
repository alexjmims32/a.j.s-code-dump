Ext.define('mobEdu.feeds.store.feedsMenus', {
	extend: 'Ext.ux.OfflineSyncStore',

	requires: [
		'mobEdu.feeds.model.feedsMenus'
	],

	config: {
		model: 'mobEdu.feeds.model.feedsMenus',

		trackLocalSync: false,
		autoServerSync: false,

		// define a LOCAL proxy for saving the store's data locally
		localProxy: {
			type: 'localstorage',
			id: 'feeds-menus-sync-store'
		},

		// define a SERVER proxy for saving the store's data on the server
		serverProxy: {
			type: 'ajax',
			api: {
				read: encorewebserver + 'open/feeds'
			},
			extraParams: {
				clientCode: campusCode,
				companyId: companyId
			},
			headers: {
				companyID: companyId
			},
			reader: {
				type: 'json',
				rootProperty: 'feederList'
			}
		}
	},

	initialize: function() {
		this.loadLocal();
	}
});