Ext.define('mobEdu.feeds.store.feedsDetail', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.feeds.model.rssFeeds'
	],

	config: {
		storeId: 'mobEdu.feeds.store.feedsDetail',
		model: 'mobEdu.feeds.model.rssFeeds',
		proxy: {
			type: 'memory',
			reader: {
				type: 'xml'
			}
		}
	}
});