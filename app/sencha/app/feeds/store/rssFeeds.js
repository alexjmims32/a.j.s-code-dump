Ext.define('mobEdu.feeds.store.rssFeeds', {
    extend: 'Ext.data.Store',

    requires: ['mobEdu.feeds.model.rssFeeds'],
    config: {
        storeId: 'mobEdu.feeds.store.rssFeeds',
        model: 'mobEdu.feeds.model.rssFeeds',
        autoLoad: false,
        // sorters: {
        //     property: 'date',
        //     direction: 'ASC'
        // },
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'rss',
                record: 'item',
                totalProperty: 'total'
            }
        }
    }
});