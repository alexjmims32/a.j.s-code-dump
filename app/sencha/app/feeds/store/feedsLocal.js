Ext.define('mobEdu.feeds.store.feedsLocal', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.feeds.model.feedsMenus'
	],

	config: {
		storeId: 'mobEdu.feeds.store.feedsLocal',
		autoLoad: true,
		model: 'mobEdu.feeds.model.feedsMenus',
		proxy: {
			type: 'memory',
			reader: {
				type: 'json'
			}
		}
	}
});