Ext.define('mobEdu.feeds.model.feedsMenus', {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'feedId'

		}, {
			name: 'parentID'
		}, {
			name: 'type'
		}, {
			name: 'link'
		}, {
			name: 'format'
		}, {
			name: 'moduleId'
		}, {
			name: 'moduleCode'
		}, {
			name: 'name'
		}, {
			name: 'icon'
		}, {
			name: 'category'
		}, {
			name: 'feeds',
			expanded: true
		}]
	}
});