Ext.define('mobEdu.feeds.model.rssFeeds', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'title'
        }, {
            name: 'author'
        }, {
            name: 'updated'
        }, {
            name: 'comments'
        }, {
            name: 'link'
        }, {
            name: 'linkurl'
        }, {
            name: 'jobLinks',
            type: 'array',
            mapping: '',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var arrayItem = [];
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'link') {
                            var attributes = node.attributes;
                            var url = '';
                            var type = '';
                            var flag = 0;
                            if (attributes != null) {
                                for (var a = 0; a < attributes.length; a++) {
                                    var attribute = attributes[a];
                                    if (attribute.name == 'href') {
                                        url = attribute.value;
                                        flag = 0;
                                        arrayItem.push({
                                            type: 'link',
                                            url: url
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                return arrayItem;
            }
        }, {
            name: 'pubDate',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var date = '';
                var val = '';
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'datePosted') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = val;
                            }
                        } else if (node.nodeName == 'pubDate') {
                            val = node.textContent;
                            if (val != null && val != '' && val != '[auto]') {
                                date = val;
                            } else {
                                date = '';
                            }
                        }
                    }
                }
                return date;
            }
        }, {
            name: 'newsPubDate',
            mapping: 'pubDate'
        }, {
            name: 'dc:creator'
        }, {
            name: 'category'
        }, {
            name: 'guid'
        }, {
            name: 'description'
        }, {
            name: 'dayDesc'
        }, {
            name: 'encoded'
        }, {
            name: 'content'
        }, {
            name: 'wfw:commentRss'
        }, {
            name: 'slash:comments'
        }, {
            name: 'url',
            mapping: '',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var arrayItem = [];
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'enclosure') {
                            var attributes = node.attributes;
                            var url = '';
                            var type = '';
                            var flag = 0;
                            if (attributes != null) {
                                for (var a = 0; a < attributes.length; a++) {
                                    var attribute = attributes[a];
                                    if (attribute.name == 'type' && (attribute.value == 'image/jpeg' || attribute.value == 'image/gif')) {
                                        type = attribute.value;
                                        flag = 1;
                                    }
                                    if (attribute.name == 'url' && flag == 1) {
                                        url = attribute.value;
                                        flag = 0;
                                        arrayItem.push({
                                            type: type,
                                            url: url
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                return arrayItem;
            }
        }, {
            name: 'day',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var date = '';
                var val = '';
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'dtstart') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'datePosted') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'pubDate') {
                            val = node.textContent;
                            if (val != null && val != '' && val != '[auto]') {
                                date = new Date(val.replace('Z', ' -0800'));
                            } else {
                                date = '';
                            }
                        }
                    }
                    if (date != '') {
                        date = Ext.Date.format(date, 'D');
                    }
                }
                return date;
            }
        }, {
            name: 'date',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var date = '';
                var val = '';
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'dtstart') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'datePosted') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'pubDate') {
                            val = node.textContent;
                            if (val != null && val != '' && val != '[auto]') {
                                date = new Date(val.replace('Z', ' -0800'));
                            } else {
                                date = '';
                            }
                        }
                    }
                    if (date != '') {
                        date = Ext.Date.format(date, 'm/d');
                    }
                }
                return date;
            }
        }, {
            name: 'startDate',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var date = '';
                var val = '';
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'dtstart') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'datePosted') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'startDate') {
                            val = node.textContent;
                            if (val != null && val != '' && val != '[auto]') {
                                date = new Date(val);
                            } else {
                                date = '';
                            }
                        }
                    }
                    if (date != '') {
                        date = Ext.Date.format(date, 'M d,Y');
                    }
                }
                return date;
            }
        }, {
            name: 'beginTime'
        }, {
            name: 'endTime'
        }, {
            name: 'buildingCode'
        }, {
            name: 'buildingDesc'
        }, {
            name: 'campDesc'
        }, {
            name: 'cntMail'
        }, {
            name: 'cntName'
        }, {
            name: 'cntPhone'
        }, {
            name: 'endDate',
            convert: function(value, record) {
                var nodes = record.raw.childNodes;
                var date = '';
                var val = '';
                if (nodes != null) {
                    var l = nodes.length;
                    for (var i = 0; i < l; i++) {
                        var node = nodes[i];
                        if (node.nodeName == 'dtstart') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'datePosted') {
                            val = node.textContent;
                            if (val != null && val != '') {
                                date = new Date(val);
                            }
                        } else if (node.nodeName == 'endDate') {
                            val = node.textContent;
                            if (val != null && val != '' && val != '[auto]') {
                                date = new Date(val);
                            } else {
                                date = '';
                            }
                        }
                    }
                    if (date != '') {
                        date = Ext.Date.format(date, 'M d,Y');
                    }
                }
                return date;
            }
        }, {
            name: 'locCity'
        }, {
            name: 'locState'
        }, {
            name: 'locZip'
        }, {
            name: 'localAdd1'
        }, {
            name: 'localAdd2'
        }, {
            name: 'roomCode'
        }, {
            name: 'roomDesc'
        }, {
            name: 'siteDesc'
        }, {
            name: 'crn'
        }]
    }
});