Ext.define('mobEdu.feeds.model.feeds', {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'img'
		}, {
			name: 'feedId',
			mapping: 'feedId'
		}, {
			name: 'parentID'
		}, {
			name: 'type'
		}, {
			name: 'link'
		}, {
			name: 'format'
		}, {
			name: 'moduleID'
		}, {
			name: 'name'
		}, {
			name: 'title'
		}, {
			name: 'icon'
		}]
	}
});