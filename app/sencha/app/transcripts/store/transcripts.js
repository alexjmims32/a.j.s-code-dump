Ext.define('mobEdu.transcripts.store.transcripts', {
    extend: 'mobEdu.data.store',

    requires: ['mobEdu.transcripts.model.transcripts'],
    config: {
        storeId: 'mobEdu.transcripts.store.transcripts',
        autoLoad: false,
        model: 'mobEdu.transcripts.model.transcripts',
        grouper: {
            groupFn: function(record) {
                return record.get('group') + ':';
            }
        },
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'ROWSET',
                record: 'RECORD'
            }
        }
    }
});
