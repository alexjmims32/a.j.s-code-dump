Ext.define('mobEdu.transcripts.view.transcripts', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.transcripts.store.transcripts'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table>' + '<tpl if="(mobEdu.transcripts.f.isExists(detailCodeDesc)=== true)"><tr><td colspan="3"><h2>{detailCodeDesc}</h2></td></tr>' + '<tpl if="(mobEdu.transcripts.f.isExists(subjCode)=== true)">', '<tr><td><h2>Subject Code</h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{subjCode}</h3></td></tr>', '</tpl>' + '<tpl if="(mobEdu.transcripts.f.isExists(crseNum)=== true)">', '<tr><td><h2>Course Number</h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{crseNum}</h3></td></tr>', '</tpl>' + '<tpl if="(mobEdu.transcripts.f.isExists(crseTitle)=== true)">', '<tr><td><h2>Course Title</h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{crseTitle}</h3></td></tr>', '</tpl>' + '<tpl if="(mobEdu.transcripts.f.isExists(grade)=== true)">', '<tr><td><h2>Grade</h2></td><td width="5%"><h2>:</h2></td><td align="left"><h3>{grade}</h3></td></tr>', '</tpl>' + '<tpl else><h3>No Details</h3></tpl>' + '</table > '),
            store: mobEdu.util.getStore('mobEdu.transcripts.store.transcripts'),
            emptyText: '<center><h3>No Info Available</h3><center>',
            loadingText: '',
            grouped: true
        }, {
            xtype: 'customToolbar',
            title: '<h1>Unofficial Transcript</h1>'
        }],
        flex: 1
    }
});