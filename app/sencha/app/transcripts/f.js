Ext.define('mobEdu.transcripts.f', {
    requires: [
        'mobEdu.transcripts.store.transcripts'
    ],
    statics: {

        loadTranscripts: function(jsonData) {
            var store = mobEdu.util.getStore('mobEdu.transcripts.store.transcripts');
            if (store.getCount() > 0) {
                store.removeAll();
                store.removed = [];
            }
            store.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            store.getProxy().setUrl(webserver + 'getUnoffTranscript');
            store.getProxy().setExtraParams({
                studentId: mobEdu.util.getStudentId(),
                tprtCode: 'WEB',
                level: 'UG'
            })
            store.getProxy().afterRequest = function() {
                mobEdu.transcripts.f.transcriptsResponseHandler();
            }
            store.load();
            mobEdu.util.gaTrackEvent('enroll', 'transcripts');
        },

        transcriptsResponseHandler: function() {
            mobEdu.transcripts.f.showTranscripts();
        },

        showTranscripts: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.transcripts.view.transcripts');
        },

        isExists: function(value) {
            if (value == '' || value == null || value == 'NA')
                return false;
            return true;
        }
    }
});
