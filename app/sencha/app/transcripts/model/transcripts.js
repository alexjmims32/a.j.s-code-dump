Ext.define('mobEdu.transcripts.model.transcripts', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'student',
            mapping: 'STUDENT'
        }, {
            name: 'studentName',
            mapping: 'STUDENT_NAME'
        }, {
            name: 'detailCodeDesc',
            mapping: 'DETAIL_CODE_DESCRIPTION'
        }, {
            name: 'subjCode',
            mapping: 'SUBJ_CODE'
        }, {
            name: 'crseNum',
            mapping: 'CRSE_NUM'
        }, {
            name: 'crseTitle',
            mapping: 'CRSE_TITLE'
        }, {
            name: 'grade',
            mapping: 'GRADE'
        }, {
            name: 'group',
            mapping: 'GROUP'
        }, {
            name: 'summary',
            mapping: 'SUMMARY'
        }]
    }

});
