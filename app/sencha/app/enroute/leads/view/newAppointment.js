Ext.define('mobEdu.enroute.leads.view.newAppointment', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.main.store.enumerations'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>New Appointment</h1>'
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '40%',
                name: 'appsub',
                id: 'appsub',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'datepickerfield',
                name: 'appdate',
                id: 'appdate',
                //                        labelAlign:'top',
                labelWidth: '40%',
                label: 'Date',
                required: true,
                useClearIcon: true,
                value: new Date(),
                picker: {
                    yearFrom: new Date().getFullYear(),
                    yearTo: new Date().getFullYear() + 1
                    //                            slotOrder:['day', 'month', 'year']
                },
                listeners: {
                    change: function(datefield, e, eOpts) {
                        mobEdu.enroute.leads.f.onAppDateKeyup(datefield)
                    }
                }
            }, {
                xtype: 'textfield',
                name: 'appTime',
                id: 'appTime',
                labelWidth: '100%',
                label: 'Appointment Time',
                useClearIcon: true
            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'appTimeHours',
                name: 'appTimeHours',
                required: true,
                useClearIcon: true,
                minValue: 8,
                maxValue: 18,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'appTimeMinutes',
                name: 'appTimeMinutes',
                //                        required:true,
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                label: 'Location',
                labelWidth: '40%',
                name: 'apploc',
                id: 'apploc',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textareafield',
                label: 'Description',
                labelWidth: '40%',
                labelAlign: 'top',
                name: 'appdes',
                id: 'appdes',
                required: true,
                useClearIcon: true,
                maxRows: 10
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Save',
                //                        ui:'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enroute.leads.f.sendAppointment();
                }
            }]
        }]
    }
});