Ext.define('mobEdu.enroute.leads.view.updateAppointment', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.main.store.enumerations'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Appointment Update</h1>'
        }, {
            xtype: 'fieldset',
            items: [{
                    xtype: 'textfield',
                    label: 'Subject',
                    labelWidth: '40%',
                    name: 'upsub',
                    id: 'upsub',
                    required: true,
                    useClearIcon: true
                }, {
                    xtype: 'datepickerfield',
                    name: 'update',
                    //                        labelAlign:'top',
                    id: 'update',
                    labelWidth: '40%',
                    label: 'Date',
                    required: true,
                    useClearIcon: true,
                    value: new Date(),
                    picker: {
                        yearFrom: new Date().getFullYear(),
                        yearTo: new Date().getFullYear() + 1
                    },
                    listeners: {
                        change: function(datefield, e, eOpts) {
                            mobEdu.enroute.leads.f.onUpAppDateKeyup(datefield)
                        }
                    }

                },

                {
                    xtype: 'spinnerfield',
                    label: 'Hours',
                    labelWidth: '40%',
                    id: 'upTimeHours',
                    name: 'upTimeHours',
                    required: true,
                    useClearIcon: true,
                    minValue: 8,
                    maxValue: 18,
                    stepValue: 1,
                    increment: 1,
                    cycle: true
                }, {
                    xtype: 'spinnerfield',
                    label: 'Minutes',
                    labelWidth: '40%',
                    id: 'upTimeMinutes',
                    name: 'upTimeMinutes',
                    //                        required:true,
                    useClearIcon: true,
                    minValue: 0,
                    maxValue: 59,
                    stepValue: 5,
                    increment: 5,
                    cycle: true
                }, {
                    xtype: 'textfield',
                    label: 'Location',
                    labelWidth: '40%',
                    name: 'uploc',
                    id: 'uploc',
                    required: true,
                    useClearIcon: true
                }, {
                    xtype: 'textareafield',
                    label: 'Description',
                    labelWidth: '40%',
                    labelAlign: 'top',
                    name: 'updes',
                    id: 'updes',
                    required: true,
                    useClearIcon: true,
                    maxRows: 10
                }
            ]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Save',
                //                        ui:'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enroute.leads.f.updateAppointmentSend();
                }
            }]
        }]
    }
});