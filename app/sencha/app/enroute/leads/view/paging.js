Ext.define('mobEdu.enroute.leads.view.paging', {
	extend: 'Ext.plugin.ListPaging',
	config: {
		//id: 'pagingplugin',
		id: 'leadplugin',
		type: 'listpaging',
		autoPaging: true,
		loadMoreText: 'Load More...',
		noMoreRecordsText: 'No more Records..'
	}
});