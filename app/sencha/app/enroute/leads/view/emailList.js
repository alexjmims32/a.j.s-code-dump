Ext.define('mobEdu.enroute.leads.view.emailList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enroute.leads.f',
		'mobEdu.enroute.leads.store.email'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'emailLst',
			name: 'emailLst',
			emptyText: '<h3 align="center">No Messages</h3>',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>',
				'<tpl if="(mobEdu.enroute.leads.f.hasEmailDirection(labels)===true)">',
				'<td width="5%"><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
				'<tpl else>',
				'<td width="5%"><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
				'</tpl>',
				'<tpl if="(mobEdu.enroute.leads.f.hasReadFlag(messageStatus)===true)">',
				'<td><h3>{subject}</h3></td>' +
				'<td align="right"><h5>{versionDate}</h5><br/><h5>{toName}</h5></td>' +
				'<tpl else>',
				'<td style="color: #030303;"><h2>{subject}</h2></td>' +
				'<td style="color: #030303;" align="right"><h5>{versionDate}</h5><br/><h5>{toName}</h5></td>' +
				'</tpl>',
				'</tr>' +
				'</table>'
			),

			store: mobEdu.util.getStore('mobEdu.enroute.leads.store.email'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enroute.leads.f.loadEmailDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>Messages</h1>'

		}, {
			xtype: 'toolbar',
			docked: 'bottom',

			layout: {
				pack: 'right'
			},
			items: [{
				text: 'New Message',
				//                        ui:'confirm',
				handler: function() {
					mobEdu.enroute.leads.f.showLeadEmail();
				}
			}]
		}],
		flex: 1
	},
	initialize: function() {
		var searchStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.email');
		searchStore.addBeforeListener('load', mobEdu.enroute.leads.f.checkForSearchListEnd, this);
	}
});