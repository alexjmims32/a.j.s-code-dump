Ext.define('mobEdu.enroute.leads.view.notificationDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.leads.store.notificationDetail'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><td valign="top" width="40%"><h2>Title</h2></td><td>&nbsp;</td><td><h3>{title}</h3></td></tr>' +
                '<tr><td valign="top" width="40%"><h2>Message</h2></td><td>&nbsp;</td><td ><h3>{message}</h3></td></tr>' +
                '<tr><td width="40%"><h2>Due Date</h2></td><td>&nbsp;</td><td><h3>{[mobEdu.util.datSplit(values.dueDate)]}</h3></td></tr>' +
                '<tr><td width="40%"><h2>Expiry Date</h2></td><td>&nbsp;</td><td><h3>{[mobEdu.util.datSplit(values.expiryDate)]}</h3></td></tr>' +
                '<tr><td width="40%"><h2>Type</h2></td><td>&nbsp;</td><td><h3>{type}</h3></td></tr>' +
                '<tr><td width="40%"><h2>Status</h2></td><td>&nbsp;</td><td><h3>{status}</h3></td></tr>' +
                '</table></td></tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enroute.leads.store.notificationDetail'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            name: 'nltfiDatilTitle',
            id: 'nltfiDatilTitle'
        }, {
            xtype: 'toolbar',
            docked: 'top',
            cls: 'headerColor',
            title: '<h1>Notification Details</h1>'
        }],
        flex: 1
    }

});