Ext.define('mobEdu.enroute.leads.view.updateProfile', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enroute.leads.f',
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.leads.store.enumerations'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		xtype: 'toolbar',
		items: [{
			xtype: 'customToolbar',
			title:'<h1>Update Profile</h1>'			
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'First Name',
				labelWidth: '40%',
				name: 'upFirstName',
				required: true,
				useClearIcon: true,
				id: 'upFirstName'
			}, {
				xtype: 'textfield',
				label: 'Last Name',
				labelWidth: '40%',
				name: 'upLastName',
				required: true,
				useClearIcon: true,
				id: 'upLastName'
			}, {
				xtype: 'datepickerfield',
				name: 'updateDateValue',
				id: 'updateDateValue',
				labelWidth: '40%',
				required: true,
				useClearIcon: true,
				label: 'Birth Date',
				picker: {
					yearFrom: 1960,
					yearTo: new Date().getFullYear() + 1
					//                            slotOrder:['day', 'month', 'year']
				}

			}, {
				xtype: 'emailfield',
				name: 'upEmailText',
				id: 'upEmailText',
				labelWidth: '40%',
				label: 'Email',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');

					input.set({
						pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
					});

					me.callParent(arguments);
				}
			}, {
				xtype: 'textfield',
				labelAlign: 'left',
				labelWidth: '40%',
				label: 'Phone',
				name: 'upPhoneNo',
				id: 'upPhoneNo',
				required: true,
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');

					input.set({
						pattern: '[0-9]*'
					});

					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.enroute.main.f.onPhone1Keyup(textfield);
					}
				},
				maxLength: 13
			}, {
				xtype: 'selectfield',
				labelAlign: 'left',
				labelWidth: '40%',
				label: 'Classification',
				name: 'upClass',
				id: 'upClass',
				useClearIcon: true,
				options: [{
					text: 'Select',
					value: 'select'
				}, {
					text: 'Freshman',
					value: 'Freshman'
				}, {
					text: 'Sophomore',
					value: 'Sophomore'
				}, {
					text: 'Junior',
					value: 'Junior'
				}, {
					text: 'Senior',
					value: 'Senior'
				}, {
					text: 'Graduate',
					value: 'Graduate'
				}]
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'High School',
				name: 'upHighS',
				id: 'upHighS',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'Intended Major',
				name: 'upIntMaj',
				id: 'upIntMaj',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textfield',
				labelWidth: '40%',
				label: 'ACT/SAT Score',
				name: 'upAsScore',
				id: 'upAsScore',
				useClearIcon: true,
				initialize: function() {
					var me = this,
						input = me.getInput().element.down('input');

					input.set({
						pattern: '[0-9]*'
					});

					me.callParent(arguments);
				},
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.enroute.leads.f.onActScoreKeyup(textfield);
					}
				},
				maxLength: 3
			}, {
				xtype: 'selectfield',
				labelAlign: 'top',
				label: 'Who is requesting the <br/> information?',
				name: 'upReqInf',
				id: 'upReqInf',
				useClearIcon: true,
				options: [{
					text: 'Select',
					value: 'select'
				}, {
					text: 'Student',
					value: 'Student'
				}, {
					text: 'Parent/Guardian',
					value: 'Parent/Guardian'
				}, {
					text: 'Other',
					value: 'Other'
				}]
			}, {
				xtype: 'selectfield',
				labelAlign: 'top',
				label: 'What type of information <br/> are you looking for?',
				name: 'upInf',
				id: 'upInf',
				useClearIcon: true,
				options: [{
					text: 'Select',
					value: 'select'
				}, {
					text: 'Financial Aid',
					value: 'Financial Aid'
				}, {
					text: 'Resident Life (Housing)',
					value: 'Resident Life'
				}, {
					text: 'Scholarships',
					value: 'Scholarships'
				}, {
					text: 'Intended Major',
					value: 'Intended Major'
				}, {
					text: 'Other',
					value: 'Other'
				}]
			}, {
				xtype: 'selectfield',
				label: 'Status',
				labelWidth: '40%',
				name: 'upStatusText',
				id: 'upStatusText',
				required: true,
				useClearIcon: true,
				store: mobEdu.util.getStore('mobEdu.enroute.leads.store.enumerations'),
				displayField: 'displayValue',
				valueField: 'enumValue'
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Save',
				align: 'right',
				handler: function() {
					mobEdu.enroute.leads.f.saveLeadProfile();
				}
			}]
		}]
	}
});