Ext.define('mobEdu.enroute.leads.view.adminAppList', {
    extend:'Ext.Panel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.leads.store.adminApp'       
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
            {
                xtype:'list',
                id:'adminAppList',
                itemTpl:'<table class="menu" width="100%"><tr><td width="80%" align="left" ><h2>{title}</h2></td><td width="20%" align="right"><h4>{date}<br/>{time}</h4></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.enroute.leads.store.adminApp'),
                singleSelect:true,
                loadingText: '',
                listeners:{
                    itemtap:function (view, index, target, record, item, e, eOpts) {
                        setTimeout(function () {
                            view.deselect(index);
                        }, 500);
                        mobEdu.enroute.leads.f.showAdmissionApp();
                    }
                }
            },
            {
                xtype:'customToolbar',
                title:'<h1>Sample User</h1>'

            },
            {
                xtype:'toolbar',
                docked:'top',
                title:'<h1>Admission Applications</h1>',
                cls:'headerColor'
            }
        ],
        flex:1
    }
});


