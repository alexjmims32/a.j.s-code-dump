Ext.define('mobEdu.enroute.leads.view.appointmentsList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.leads.store.appointments'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'appList',
            emptyText: '<h3 align="center">No Appointments</h3>',
            itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                '<tr>',
                //                    '<tpl if="(mobEdu.enroute.leads.f.hasAppointmentDirection(source)===true)">',
                //                    '<td width="5%"><img src="resources/images/sent1.png" align="absmiddle" /></td>',
                //                    '<tpl else>',
                //                    '<td width="5%"><img src="resources/images/recieve1.png" align="absmiddle" /></td>',
                //                    '</tpl>',
                //                    '<tpl if="(mobEdu.enroute.leads.f.hasReadFlag(readFlag)===true)">',
                //                    '<td style="color: #dcdcdc;" ><h2>{subject}</h2></td>'+
                //                        '<td style="color: #dcdcdc;" align="right"><h5>{appointmentDate}</h5></td>'+
                //                        '<tpl else>',
                '<td ><h2>{subject}</h2></td>' +
                '<td align="right"><h5>{appointmentDate}</h5></td>' +
                //                        '</tpl>',
                '</tr>' +
                '</table>'
            ),

            store: mobEdu.util.getStore('mobEdu.enroute.leads.store.appointments'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    mobEdu.enroute.leads.f.loadAppointDetail(record);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>Appointments</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',

            layout: {
                pack: 'right'
            },
            items: [{
                text: 'New Appointment',
                //                        ui:'confirm',
                handler: function() {
                    mobEdu.enroute.leads.f.showNewAppointment();
                }
            }, {
                text: 'Calendar',
                id: 'rCal',
                handler: function() {
                    mobEdu.enroute.leads.f.showCalendar();
                }
            }]
        }],
        flex: 1
    }
});