Ext.define('mobEdu.enroute.leads.view.profileDetail', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.leads.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Profile Summary</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'View Application',
                        id:'viewApp',
                        name:'viewApp',
                        handler:function(){
                            mobEdu.enroute.leads.f.getLeadViewApplication();
                        }
                    },
                    {
                        xtype:'spacer'
                    },
                    {
                        text:'Update Profile',
                        handler:function(){
                            mobEdu.enroute.leads.f.ProfileUpdate();
                        }
                    }
                ]
             },            
            {
                xtype:'panel',
                name:'profilesummary',
                id:'profilesummary'
            }

        ]
    }

});