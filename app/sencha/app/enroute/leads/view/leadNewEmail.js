Ext.define('mobEdu.enroute.leads.view.leadNewEmail', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.leads.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>New Message</h1>'
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '25%',
                name: 'newleadsub',
                id: 'newleadsub',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textareafield',
                label: 'Message',
                labelWidth: '30%',
                labelAlign: 'top',
                name: 'newleadmsg',
                id: 'newleadmsg',
                maxRows: 15,
                required: true
                //                        useClearIcon:true
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Send',
                //                        ui:'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enroute.leads.f.sentmail();
                }
            }]
        }]
    }
});