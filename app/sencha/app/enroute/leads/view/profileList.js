Ext.define('mobEdu.enroute.leads.view.profileList', {
    extend: 'Ext.Panel',
    xtype: 'profileSearchList',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.main.store.supLeadsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>My Leads</h1>'
        }, {
            xtype: 'fieldset',
            docked: 'top',
            cls: 'searchfieldset',
            //                margin: 10,
            items: [{
                xtype: 'textfield',
                name: 'searchItem',
                id: 'searchItem',
                width: '100%',
                placeHolder: 'Search lead',
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.enroute.leads.f.myLeadsSearch(textfield);
                    },
                    clearicontap: function(textfield, e, eOpts) {
                        mobEdu.enroute.leads.f.myLeadsSearch();
                    }
                }
            }]
        }, {
            xtype: 'list',
            id: 'profileList',
            name: 'profileList',
            emptyText: '<h3 align="center">No Leads</h3>',
            //                plugins: [{
            //                    type: 'listpaging',
            //                    autoPaging: true,
            //                    loadMoreText: 'Load More...',
            //                    noMoreRecordsText: ''
            //                }],
            itemTpl: '<table  width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr>' +
                '<tr><h7><td width="30%" valign="top">{status}</td></h7><h4><td width="70%" align="right">{versionDate}</td></h4></tr>' +
                '<tr><h4><td width="40%" align="left">{phone1}</td></h4><h7><td width="60%" align="right">{email}</td></h7></tr></table>',
            store: mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    mobEdu.enroute.leads.f.showViewProfile(index, view, record, item);
                }
            }
        }],
        flex: 1
    }
});