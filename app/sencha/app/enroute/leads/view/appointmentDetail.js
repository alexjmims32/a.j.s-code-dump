Ext.define('mobEdu.enroute.leads.view.appointmentDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.leads.store.appointmentDetail'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            //                cls:'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{appointmentDate}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Location:</h2></td><td align="left"><h3>{location}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{[mobEdu.util.getStatus(values.fromDetails,values.toIDs,values.loginUserID)]}</h3></td></tr>' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Description:</h2></td><td align="left"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail'),
            singleSelect: true,
            loadingText: ''
        }, {
            xtype: 'customToolbar',
            title: '<h1>Appointment Details</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',

            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Accept',
                id: 'laccept',
                handler: function() {
                    mobEdu.enroute.leads.f.acceptAppointment('Accept');
                }
            }, {
                text: 'Decline',
                id: 'ldecline',
                handler: function() {
                    mobEdu.enroute.leads.f.acceptAppointment('Decline');
                }
            }, {
                text: 'Update Appointment',
                handler: function() {
                    mobEdu.enroute.leads.f.loadUpdateAppointment();
                }
            }]
        }],
        flex: 1
    }
});