Ext.define('mobEdu.enroute.leads.view.rAppointmentCalendar', {
    extend: 'Ext.Panel',
    alias: 'rAppointmentCalendar',
    config: {
        fullscreen: 'true',
        scrollable: true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            id: 'lappointmentsDock',
            name: 'lappointmentsDock',
            items: [{}]
        }, {
            xtype: 'label',
            id: 'lAppointmentLabel',
            padding: 10
        }]
    }
});