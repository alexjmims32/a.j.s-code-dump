Ext.define('mobEdu.enroute.leads.view.notificationsList', {
    extend:'Ext.Panel',
    requires: [
        'mobEdu.enroute.leads.f',
        'mobEdu.enroute.leads.store.notifications'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
            {
                xtype:'list',
                id:'notificationsList',
                itemTpl:'<table class="menu" width="100%"><tr><td width="50%" align="left"><h2>{title}</h2></td><td width="50%" align="right" ><h5>{dueDate}</h5></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.enroute.leads.store.notifications'),
                singleSelect:true,
                loadingText: '',
                listeners:{
                    itemtap:function (view, index, target, record, item, e, eOpts) {
                        setTimeout(function () {
                            view.deselect(index);
                        }, 500);
                        mobEdu.enroute.leads.f.loadNotificationListDetail(record);
                    }
                }
            },
            {
                xtype:'customToolbar',
                name:'notifiListTitle',
                id:'notifiListTitle'
            },
            {
                xtype:'toolbar',
                docked:'top',
                cls:'headerColor',
                title:'<h1>Notifications</h1>'
            }
        ],
        flex:1
    }
});


