Ext.define('mobEdu.enroute.leads.view.leadViewApplication', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.leads.f'      
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>View Application</h1>'
            },            
            {
                xtype:'panel',
                name:'leadViewApp',
                id:'leadViewApp'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        xtype:'spacer'
                    },
                    {
                        text:'Push To Banner',
                        id:'pushApp',
                        name:'pushApp',
                        handler:function () {
                            mobEdu.enroute.leads.f.loadLeadPushBannerApp();
                        }
                    }
                ]
            }

        ]
    }

});
