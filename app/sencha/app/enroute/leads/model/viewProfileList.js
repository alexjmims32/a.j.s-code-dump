Ext.define('mobEdu.enroute.leads.model.viewProfileList', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'img'
            },
            {
                name:'title'
            },
            {
                name:'action',
                type: 'function'
            }
        ]
    }
});