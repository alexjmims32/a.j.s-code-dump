Ext.define('mobEdu.enroute.leads.model.newEmail', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'emailID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});