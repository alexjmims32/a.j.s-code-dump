Ext.define('mobEdu.enroute.leads.model.newAppointment', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'appointmentID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});