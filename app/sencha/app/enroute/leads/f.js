Ext.define('mobEdu.enroute.leads.f', {
	requires: [
		'mobEdu.enroute.main.store.supLeadsList',
		'mobEdu.enroute.leads.store.viewProfileList',
		'mobEdu.enroute.leads.store.profileDetail',
		'mobEdu.enroute.leads.store.profileList',
		'mobEdu.enroute.leads.store.viewApplication',
		'mobEdu.enroute.leads.store.newAppointment',
		'mobEdu.enroute.leads.store.enumerations',
		'mobEdu.enroute.leads.store.email',
		'mobEdu.enroute.leads.store.viewEmail',
		'mobEdu.enroute.leads.store.newEmail',
		'mobEdu.enroute.leads.store.appointments',
		'mobEdu.enroute.leads.store.appointmentDetail',
		'mobEdu.enroute.leads.store.notifications',
		'mobEdu.enroute.leads.store.notificationDetail',
		'mobEdu.enroute.main.store.leadProfileUpdate',
		'mobEdu.enroute.leads.store.lacceptance'
	],
	statics: {
		count: 0,

		loadProfileList: function() {
			mobEdu.enroute.leads.f.myLeadsSearch();
			Ext.getCmp('searchItem').setValue('');
		},

		myLeadsSearch: function(search) {
			var searchItem;
			if (search != undefined && search != null) {
				searchItem = search.getValue();
			} else {
				searchItem = '';
			}
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');

			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getMyLeads');
			store.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: mobEdu.util.getStudentId(),
				limit: 500,
				companyId: companyId
			});

			store.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.myLeadsSearchResponseHandler();
			};
			store.load();
		},

		myLeadsSearchResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			store.sort("versionDate", "DESC");
			var storeStatus = store.getProxy().getReader().rawData;
			if (storeStatus.status != 'success') {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enroute.leads.f.showProfileList();
			}
		},

		profileSearch: function() {
			var searchItem = Ext.getCmp('searchItem').getValue();
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			store.removeAll();
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getMyLeads');
			store.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: mobEdu.util.getStudentId(),
				maxRecords: 100,
				companyId: companyId
			});

			store.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.profileSearchResponseHandler();
			};
			store.load();
		},

		profileSearchResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			store.sort("versionDate", "DESC");
			var storeStatus = store.getProxy().getReader().rawData;
			if (storeStatus.status != 'success') {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			if (store.data.length > 0) {
				store.insert(0, {
					firstName: "Select All",
					lastName: " "
				});
			}


		},

		initializeViewProfile: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewProfileList');
			//            store.load();
			store.removeAll();
			store.removed = [];
			//            store.getProxy().clear();
			store.sync();

			store.add({
				title: '<h2>Profile</h2>',
				action: mobEdu.enroute.leads.f.viewProfileDetail
			});
			store.sync();

			store.add({
				title: '<h2>Messages</h2>',
				action: mobEdu.enroute.leads.f.loadEmail
			});
			store.sync();

			store.add({
				title: '<h2>Appointments</h2>',
				action: mobEdu.enroute.leads.f.loadAppointments
			});
			store.sync();

			//            store.add({
			////                img:'resources/images/main-menu/StudentProfile.png',
			//                title:'Notifications'
			////                action: mobEdu.enroute.leads.f.loadNotificationsList
			//            });
			//            store.sync();


			//            store.load();
		},

		showViewProfile: function(selectedIndex, viewRef, record, item) {
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			store.removeAll();
			store.add(record);
			// store.sync();

			mobEdu.enroute.leads.f.loadViewProfile();
		},
		loadViewProfile: function() {
			var emailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.email');
			emailStore.currentPage = 1;
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var fName = data[0].data.firstName;
			var lName = data[0].data.lastName;
			leadId = data[0].data.leadID;
			var lId = data[0].data.leadID;
			leadTitle = fName + ' ' + lName;
			if (mobEdu.enroute.main.f.leadSearchType === 'UNLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsList);
				mobEdu.util.show('mobEdu.enroute.main.view.viewProfileList');
				Ext.getCmp('viewSupProfTool').setTitle('<h1>' + leadTitle + '</h1>');
			} else if (mobEdu.enroute.main.f.allLeadFlag == true) {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadList);
				mobEdu.util.show('mobEdu.enroute.main.view.allLeadProfileList');
				Ext.getCmp('allLeadProfTool').setTitle('<h1>' + leadTitle + '</h1>');
			} else if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
				mobEdu.util.show('mobEdu.enroute.main.view.viewProfileList');
				Ext.getCmp('viewSupProfTool').setTitle('<h1>' + leadTitle + '</h1>');
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showProfileList);
				mobEdu.util.show('mobEdu.enroute.leads.view.viewProfileList');
				Ext.getCmp('viewProfTool').setTitle('<h1>' + leadTitle + '</h1>');
			}
		},

		viewProfileDetail: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;

			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');

			detailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			detailStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/get');
			detailStore.getProxy().setExtraParams({
				leadID: lId,
				companyId: companyId
			});

			detailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.ProfileDetailResponseHandler();
			};
			detailStore.load();
		},
		ProfileDetailResponseHandler: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');
			var storeStatus = detailStore.getProxy().getReader().rawData;
			if (storeStatus.status == 'success') {
				mobEdu.enroute.leads.f.loadProfileSummary();
			} else {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadProfileSummary: function(lId) {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');
			var profileData = detailStore.data.all;
			var appId = profileData[0].data.applicationID;
			var viewLeadId = profileData[0].data.leadID;
			var middleValue;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			middleValue = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			if (dateOfBirth == null) {
				dateOfBirth = '';
			}
			var address1 = profileData[0].data.address1;
			var address2 = profileData[0].data.address2;
			var cityValue = profileData[0].data.city;
			var stateValue = profileData[0].data.state;
			var countryName = profileData[0].data.country;
			var emailId1 = profileData[0].data.email;
			var phoneNo1 = profileData[0].data.phone1;
			if (phoneNo1 != null && phoneNo1 != '') {
				phoneNo1 = phoneNo1.replace(/-|\(|\)/g, "");
			}
			var phoneNo2 = profileData[0].data.phone2;
			if (phoneNo2 != null && phoneNo2 != '') {
				phoneNo2 = phoneNo2.replace(/-|\(|\)/g, "");
			}
			var addType = profileData[0].data.addressType;
			var ldStatus = profileData[0].data.leadStatus;
			if (ldStatus == null) {
				ldStatus = '';
			}
			var recqName = profileData[0].data.recruiterName;
			var updatedDate = profileData[0].data.versionDate;
			var updatedBy = profileData[0].data.versionUser;
			var classText = profileData[0].data.classification;
			var schoolName = profileData[0].data.highSchool;
			var intMajor = profileData[0].data.intendedMajor;
			var scoresValues = profileData[0].data.actScore;
			var reqInfo = profileData[0].data.requestInfo;
			var lookInfo = profileData[0].data.lookInfo;
			if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD' || mobEdu.enroute.main.f.leadSearchType === 'UNLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
				mobEdu.util.show('mobEdu.enroute.main.view.supLeadDetail');
				if (appId != null) {
					Ext.getCmp('unassignedViewApp').show();
				} else {
					Ext.getCmp('unassignedViewApp').hide();
				}
				var finalLeadSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Full Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>BirthDate:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>';
				if (classText != '' && classText != null) {
					finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>Classification:</h2></td>' + '<td align="left"><h3>' + classText + '</h3></td></tr>';
				}
				if (schoolName != '' && schoolName != null) {
					finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>High School:</h2></td>' + '<td align="left"><h3>' + schoolName + '</h3></td></tr>';
				}
				if (intMajor != '' && intMajor != null) {
					finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>Intended Major:</h2></td>' + '<td align="left"><h3>' + intMajor + '</h3></td></tr>';
				}
				if (scoresValues != '' && scoresValues != null) {
					finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>ACT/SAT Score:</h2></td>' + '<td align="left"><h3>' + scoresValues + '</h3></td></tr>';
				}
				if (reqInfo != '' && reqInfo != null) {
					finalLeadSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>Who is reguesting the <br/> information?:</h2></td>' + '<td align="left"><h3>' + reqInfo + '</h3></td></tr>';
				}
				if (lookInfo != '' && lookInfo != null) {
					finalLeadSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>What type of information <br/> are you looking for?:</h2></td>' + '<td align="left"><h3>' + lookInfo + '</h3></td></tr>';
				}
				finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + recqName + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr></table>';

				Ext.getCmp('supLeadDetailSummary').setHtml(finalLeadSummaryHtml);
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
				mobEdu.util.show('mobEdu.enroute.leads.view.profileDetail');
				// Ext.getCmp('profDetail').setTitle('<h1>' + leadTitle + '</h1>');
				if (appId != null) {
					Ext.getCmp('viewApp').show();
				} else {
					Ext.getCmp('viewApp').hide();
				}

				var finalSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>';
				if (classText != '' && classText != null) {
					finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Classification:</h2></td>' + '<td align="left"><h3>' + classText + '</h3></td></tr>';
				}
				if (schoolName != '' && schoolName != null) {
					finalSummaryHtml += '<tr><td align="right" width="50%"><h2>High School:</h2></td>' + '<td align="left"><h3>' + schoolName + '</h3></td></tr>';
				}
				if (intMajor != '' && intMajor != null) {
					finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Intended Major:</h2></td>' + '<td align="left"><h3>' + intMajor + '</h3></td></tr>';
				}
				if (scoresValues != '' && scoresValues != null) {
					finalSummaryHtml += '<tr><td align="right" width="50%"><h2>ACT/SAT Score:</h2></td>' + '<td align="left"><h3>' + scoresValues + '</h3></td></tr>';
				}
				if (reqInfo != '' && reqInfo != null) {
					finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>Who is reguesting the <br/> information?:</h2></td>' + '<td align="left"><h3>' + reqInfo + '</h3></td></tr>';
				}
				if (lookInfo != '' && lookInfo != null) {
					finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>What type of information <br/> are you looking for?:</h2></td>' + '<td align="left"><h3>' + lookInfo + '</h3></td></tr>';
				}
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + recqName + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Last Updated Date:</h2></td>' + '<td align="left"><h3>' + updatedDate + '</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Last Updated By:</h2></td>' + '<td align="left"><h3>' + updatedBy + '</h3></td></tr>';
				Ext.getCmp('profilesummary').setHtml(finalSummaryHtml);
			}

		},

		getLeadViewApplication: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');
			var profileData = detailStore.data.all;
			var appId = profileData[0].data.applicationID;
			var viewLeadId = profileData[0].data.leadID;
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');

			applicationStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			applicationStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getapplication');
			applicationStore.getProxy().setExtraParams({
				leadID: viewLeadId,
				companyId: companyId
			});
			applicationStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.getLeadViewApplicationResponseHandler();
			};
			applicationStore.load();
		},
		getLeadViewApplicationResponseHandler: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			var applicationStatus = applicationStore.getProxy().getReader().rawData;
			if (applicationStatus.status == 'success') {
				mobEdu.enroute.leads.f.loadLeadViewApplication();
			} else {
				Ext.Msg.show({
					id: 'appSuccess',
					title: null,
					message: applicationStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadLeadViewApplication: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadProfileSummary);
			mobEdu.util.show('mobEdu.enroute.leads.view.leadViewApplication');
			var profileData = applicationStore.data.all;
			var appSisInfo = profileData[0].data.sisInfo;
			var appIndicator;
			var applicationStatus;
			var appComments;
			var appBannerId;
			if (appSisInfo != 0 && appSisInfo != null && appSisInfo != '' && appSisInfo != undefined) {
				appIndicator = appSisInfo.applIndicator;
				applicationStatus = appSisInfo.apstDescription;
				appComments = appSisInfo.comments;
				appBannerId = appSisInfo.bannerID;
			} else {
				appIndicator = '';
				applicationStatus = '';
				appComments = '';
				appBannerId = '';
			}
			if (appIndicator == "Y") {
				Ext.getCmp('pushApp').hide();
			} else {
				Ext.getCmp('pushApp').show();
			}
			var applicationId = profileData[0].data.applicationID;
			var appLeadId = profileData[0].data.leadID;
			var appStatus = profileData[0].data.status;
			var appVersionNo = profileData[0].data.versionNo;
			var appVersionDate = profileData[0].data.versionDate;
			var appVersionUser = profileData[0].data.versionUser;
			var appAdTermCode = profileData[0].data.termCode;
			var appAdTermCodeDes = profileData[0].data.termCodeDescr;
			var appAdLevelCode = profileData[0].data.levelCode;
			var appAdLevelCodeDes = profileData[0].data.levelCodeDescr;
			var appAdMajorCode = profileData[0].data.majorCode;
			var appAdMajorCodeDes = profileData[0].data.majorCodeDescr;
			var appAdStudentType = profileData[0].data.studentType;
			var appAdStudentTypeDes = profileData[0].data.studentTypeDescr;
			var appAdAdmissionType = profileData[0].data.admissionType;
			var appAdAdmissionTypeDes = profileData[0].data.admissionTypeDescr;
			var appAdResidenceCode = profileData[0].data.residenceCode;
			var appAdResidenceCodeDes = profileData[0].data.residenceCodeDescr;
			var appAdCollegeCode = profileData[0].data.collegeCode;
			var appAdCollegeCodeDes = profileData[0].data.collegeCodeDescr;
			var appAdDegreeCode = profileData[0].data.degreeCode;
			var appAdDegreeCodeDes = profileData[0].data.degreeCodeDescr;
			var appAdDepartment = profileData[0].data.department;
			var appAdDepartmentDes = profileData[0].data.departmentDescr;
			var appAdCampus = profileData[0].data.campus;
			var appAdCampusDes = profileData[0].data.campusDescr;
			var appAdEduGoal = profileData[0].data.educationGoal;
			var appAdEduGoalDes = profileData[0].data.educationGoalDescr;
			var middleValue;
			var appPerFirstName = profileData[0].data.firstName;
			var appPerLastName = profileData[0].data.lastName;
			//            middleValue = appPerFirstName + ' ' + appPerLastName;
			var appPerMiddleName = profileData[0].data.middleName;
			if (appPerMiddleName != " " && appPerMiddleName != null) {
				middleValue = appPerFirstName + ' ' + appPerMiddleName + ' ' + appPerLastName;
			} else {
				middleValue = appPerFirstName + ' ' + appPerLastName;
			}
			var appPerGender = profileData[0].data.gender;
			if (appPerGender == "F") {
				appPerGender = 'Female';
			} else {
				appPerGender = 'Male';
			}
			var appPerRace = profileData[0].data.race;
			var appPerRaceDes = profileData[0].data.raceDescr;
			if (appPerRaceDes == 'select') {
				appPerRace = '';
			}
			var appPerEthnicity = profileData[0].data.ethnicity;
			var appPerEthnicityDes = profileData[0].data.ethnicityDescr;
			var appPerDateOfBirth = profileData[0].data.dob;
			var appPerSsn = profileData[0].data.ssn;
			var address;
			var zipAndCountry;
			var appContAddress1 = profileData[0].data.address1;
			var appContAddress2 = profileData[0].data.address2;
			var appContAddress3 = profileData[0].data.address3;
			var appContCity = profileData[0].data.city;
			var appContState = profileData[0].data.state;
			var appContStateDes = profileData[0].data.stateDescr;
			var appContCounty = profileData[0].data.county;
			var appContZip = profileData[0].data.zip;
			var appContCountry = profileData[0].data.country;
			if (appContAddress2 != '' && appContAddress3) {
				address = appContAddress1 + ',' + appContAddress2 + ',' + appContAddress3;
			} else {
				address = appContAddress1;
			}
			if (appContCountry != '') {
				zipAndCountry = appContZip + ',' + appContCountry
			} else {
				zipAndCountry = appContZip;
			}

			var appContEmailId = profileData[0].data.email;
			var appContPhoneNo1 = profileData[0].data.phone1;
			var appContPhoneNo2 = profileData[0].data.phone2;
			var visaDetails;
			var appVisaType = profileData[0].data.visaType;
			var appVisaNationality = profileData[0].data.nationality;
			var appVisaNationalityDes = profileData[0].data.nationalityDescr;
			var appVisaNo = profileData[0].data.visaNumber;
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				visaDetails = appVisaType + ',' + appVisaNationalityDes + ',' + appVisaNo;
			}
			var parent1Details;
			var parent2Details;
			var appParRelation1 = profileData[0].data.parent1Relation;
			var appParRelation1Des = profileData[0].data.parent1RelationDescr;
			var appParFName1 = profileData[0].data.parent1FirstName;
			var appParLName1 = profileData[0].data.parent1LastName;
			var appParMName1 = profileData[0].data.parent1MiddleName;
			if (appParFName1 != '' && appParLName1 != '') {
				parent1Details = appParFName1 + ',' + appParLName1;
			} else if (appParLName1 != '') {
				parent1Details = appParLName1;
			} else {
				parent1Details = appParFName1;
			}
			var appParRelation2 = profileData[0].data.parent2Relation;
			var appParRelation2Des = profileData[0].data.parent2RelationDescr;;
			var appParFName2 = profileData[0].data.parent2FirstName;
			var appParLName2 = profileData[0].data.parent2LastName;
			var appParMName2 = profileData[0].data.parent2MiddleName;
			if (appParFName2 != '' && appParLName2 != '') {
				parent2Details = appParFName2 + ',' + appParLName2;
			} else if (appParFName2 != '') {
				parent2Details = appParFName2;
			} else {
				parent2Details = appParLName2;
			}
			var appSchoolName = profileData[0].data.schoolName;
			var appSchoolGpa = profileData[0].data.schoolGpa;
			var appSchoolCode = profileData[0].data.schoolCode;
			var appSchoolCodeDes = profileData[0].data.schoolCodeDescr;
			//            var appSchoolCity = profileData[0].data.schoolCity;
			//            var appSchoolState = profileData[0].data.schoolState;
			var appSchoolGYear = profileData[0].data.schoolGradYear;
			var testCode;
			var testDate;
			var testScore;
			var appTsTestScores = profileData[0].data.testScores;
			if (appTsTestScores.length != 0) {
				for (var i = 0; i < appTsTestScores.length; i++) {
					testCode = appTsTestScores[i].testCode;
					testDate = appTsTestScores[i].testDate;
					testScore = appTsTestScores[i].testScore;
				}
			} else {
				testCode = "";
				testDate = "";
				testScore = "";
			}
			var collegeCode;
			var degree;
			var grdDate;
			var collegeCity;
			var collegeState;
			var appCInfoColleges = profileData[0].data.colleges;
			if (appCInfoColleges.length != 0) {
				for (var c = 0; c < appCInfoColleges.length; c++) {
					collegeCode = appCInfoColleges[c].collegeCode;
					degree = appCInfoColleges[c].degree;
					collegeCity = appCInfoColleges[c].city;
					collegeState = appCInfoColleges[c].state;
					grdDate = appCInfoColleges[c].gradDate;
				}
			} else {
				collegeCode = "";
				degree = "";
				grdDate = "";
				collegeCity = "";
				collegeState = "";
			}
			var appInterestPN = profileData[0].data.primaryInterest;
			var appInterestSN = profileData[0].data.secondaryInterest;
			var appInterestLevelN = profileData[0].data.levelOfInterest;
			var appInterestFactor = profileData[0].data.factorForChoosing;
			var appInterestOtherN = profileData[0].data.otherInterests;

			var applicationSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + appPerDateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Gender:</h2></td>' + '<td align="left"><h3>' + appPerGender + '</h3></td></tr>';
			if (appPerRaceDes != '' && appPerRaceDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Race:</h2></td>' + '<td align="left"><h3>' + appPerRaceDes + '</h3></td></tr>';
			}
			if (appPerEthnicityDes != '' && appPerEthnicityDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Ethnicity:</h2></td>' + '<td align="left"><h3>' + appPerEthnicityDes + '</h3></td></tr>';
			}
			if (appPerSsn != '' && appPerSsn != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>SSN:</h2></td>' + '<td align="left"><h3>' + appPerSsn + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Address:</h2></td>' + '<td align="left"><h3>' + address + '<br />' + appContCity + ',' + appContState + '<br />' + zipAndCountry + '</h3></td></tr>';
			if (appContEmailId != '' && appContEmailId != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + appContEmailId + '</h3></td></tr>';
			}
			if (appContPhoneNo1 != '' && appContPhoneNo1 != null) {
				appContPhoneNo1 = appContPhoneNo1.replace(/-|\(|\)/g, "");
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone1:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo1 + '</h3></td></tr>';
			}
			if (appContPhoneNo2 != '' && appContPhoneNo2 != null) {
				appContPhoneNo2 = appContPhoneNo2.replace(/-|\(|\)/g, "");
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone2:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo2 + '</h3></td></tr>';
			}
			if (appParRelation1Des != '' && appParRelation1Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 1 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation1Des + '<br />' + parent1Details + '</h3></td></tr>';
			}
			if (appParRelation2Des != '' && appParRelation2Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 2 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation2Des + '<br />' + parent2Details + '</h3></td></tr>';
			}
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Visa Details:</h2></td>' + '<td align="left"><h3>' + appVisaType + '<br />' + appVisaNationalityDes + ',' + appVisaNo + '</h3></td></tr>';
			}
			if (appSchoolName != '' && appSchoolCodeDes != '' && appSchoolGpa != '' && appSchoolGYear != '' && appSchoolGpa != null && appSchoolGYear != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>School Details:</h2></td>' + '<td align="left"><h3>' + appSchoolName + '<br />' + appSchoolCodeDes + ',' + appSchoolGpa + ',' + appSchoolGYear + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Term Code:</h2></td>' + '<td align="left"><h3>' + appAdTermCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Level Code:</h2></td>' + '<td align="left"><h3>' + appAdLevelCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Major Code:</h2></td>' + '<td align="left"><h3>' + appAdMajorCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Student Type:</h2></td>' + '<td align="left"><h3>' + appAdStudentTypeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Residence Code:</h2></td>' + '<td align="left"><h3>' + appAdResidenceCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + appAdCollegeCode + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + appAdDegreeCodeDes + '</h3></td></tr>';
			if (appAdAdmissionTypeDes != '' && appAdAdmissionTypeDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Admission Type:</h2></td>' + '<td align="left"><h3>' + appAdAdmissionTypeDes + '</h3></td></tr>';
			}
			if (appAdDepartmentDes != '' && appAdDepartmentDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Department:</h2></td>' + '<td align="left"><h3>' + appAdDepartmentDes + '</h3></td></tr>';
			}
			if (appAdCampusDes != '' && appAdCampusDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Campus:</h2></td>' + '<td align="left"><h3>' + appAdCampusDes + '</h3></td></tr>';
			}
			if (appAdEduGoalDes != '' && appAdEduGoalDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Education Goal:</h2></td>' + '<td align="left"><h3>' + appAdEduGoalDes + '</h3></td></tr>';
			}
			if (testCode != '' && testCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Code:</h2></td>' + '<td align="left"><h3>' + testCode + '</h3></td></tr>';
			}
			if (testDate != '' && testDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Date:</h2></td>' + '<td align="left"><h3>' + testDate + '</h3></td></tr>';
			}
			if (testScore != '' && testScore != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Score:</h2></td>' + '<td align="left"><h3>' + testScore + '</h3></td></tr>';
			}
			if (collegeCode != '' && collegeCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + collegeCode + '</h3></td></tr>';
			}
			if (collegeCity != '' && collegeState != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Address:</h2></td>' + '<td align="left"><h3>' + collegeCity + ',' + collegeState + '</h3></td></tr>';
			}
			if (degree != '' && degree != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + degree + '</h3></td></tr>';
			}
			if (grdDate != '' && grdDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Graduation Date:</h2></td>' + '<td align="left"><h3>' + grdDate + '</h3></td></tr>';
			}
			if (appInterestPN != '' && appInterestPN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Primary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestPN + '</h3></td></tr>';
			}
			if (appInterestSN != '' && appInterestSN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Secondary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestSN + '</h3></td></tr>';
			}
			if (appInterestLevelN != '' && appInterestLevelN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Level Of Interest:</h2></td>' + '<td align="left"><h3>' + appInterestLevelN + '</h3></td></tr>';
			}
			if (appInterestFactor != '' && appInterestFactor != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Factor For Choosing:</h2></td>' + '<td align="left"><h3>' + appInterestFactor + '</h3></td></tr>';
			}
			if (appInterestOtherN != '' && appInterestOtherN == null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Other Interests:</h2></td>' + '<td align="left"><h3>' + appInterestOtherN + '</h3></td></tr>';
			}
			if (appBannerId != '' && appBannerId != undefined && appBannerId != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Banner ID:</h2></td>' + '<td align="left"><h3>' + appBannerId + '</h3></td></tr>';
			}
			if (applicationStatus != '' && applicationStatus != undefined && applicationStatus != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Application Status:</h2></td>' + '<td align="left"><h3>' + applicationStatus + '</h3></td></tr>';
			}
			if (appComments != '' && appComments != undefined && appComments != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Comments:</h2></td>' + '<td align="left"><h3>' + appComments + '</h3></td></tr></table>';
			}

			Ext.getCmp('leadViewApp').setHtml(applicationSummaryHtml);
		},

		loadLeadPushBannerApp: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			var profileData = applicationStore.data.all;
			var applicationId = profileData[0].data.applicationID;
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/pushtosis');
			enuStore.getProxy().setExtraParams({
				applicationID: applicationId,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.loadLeadPushBannerAppResponseHandler();
			};
			enuStore.load();
		},
		loadLeadPushBannerAppResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				Ext.Msg.show({
					id: 'pushmsg',
					title: null,
					message: '<p>Application pushed to banner successfully.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.leads.f.loadProfileSummary();
						}
					}
				});
			}
		},

		loadEnumerations: function() {
			var enumType = 'LEAD_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.enumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.loadEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.enumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onActScoreKeyup: function(scoreField) {
			var scoreNumber = (scoreField.getValue()).toString() + '';
			scoreNumber = scoreNumber.replace(/\D/g, '');
			scoreField.setValue(scoreNumber);
			var length = scoreNumber.length;
			if (length > 3) {
				scoreField.setValue(scoreNumber.substring(0, 3));
				return false;
			}
			return true;
		},

		ProfileUpdate: function() {

			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');
			var storeData = detailStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;

			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}


			if (mobEdu.enroute.main.f.leadSearchType === 'UNLEAD' || mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
				mobEdu.enroute.main.f.loadEnumerations();
				mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadProfileSummary);

				mobEdu.util.show('mobEdu.enroute.main.view.leadProfileUpdate');
				// Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
				Ext.getCmp('upFName').setValue(storeData[0].data.firstName);
				Ext.getCmp('upLName').setValue(storeData[0].data.lastName);
				if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
					Ext.getCmp('update').setValue(new Date(dobFormatted));
				}
				Ext.getCmp('upEmail').setValue(storeData[0].data.email);
				Ext.getCmp('upPhone').setValue(storeData[0].data.phone1);
				if (storeData[0].data.classification != '' && storeData[0].data.classification != undefined && storeData[0].data.classification != null) {
					Ext.getCmp('upClassText').setValue(storeData[0].data.classification);
				}
				Ext.getCmp('upHighSName').setValue(storeData[0].data.highSchool);
				Ext.getCmp('upIntMajText').setValue(storeData[0].data.intendedMajor);
				Ext.getCmp('upAsScoreNo').setValue(storeData[0].data.actScore);
				if (storeData[0].data.requestInfo != '' && storeData[0].data.requestInfo != undefined && storeData[0].data.requestInfo != null) {
					Ext.getCmp('upReqInfValue').setValue(storeData[0].data.requestInfo);
				}
				if (storeData[0].data.lookInfo != '' && storeData[0].data.lookInfo != undefined && storeData[0].data.lookInfo != null) {
					Ext.getCmp('upInfValue').setValue(storeData[0].data.lookInfo);
				}
				if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
					Ext.getCmp('upStatus').setValue(storeData[0].data.leadStatus);
				}
			} else {
				mobEdu.enroute.leads.f.loadEnumerations();
				mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.profileUpdates);
				mobEdu.util.show('mobEdu.enroute.leads.view.updateProfile');
				// Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
				Ext.getCmp('upFirstName').setValue(storeData[0].data.firstName);
				Ext.getCmp('upLastName').setValue(storeData[0].data.lastName);
				if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
					Ext.getCmp('updateDateValue').setValue(new Date(dobFormatted));
				}
				Ext.getCmp('upEmailText').setValue(storeData[0].data.email);
				Ext.getCmp('upPhoneNo').setValue(storeData[0].data.phone1);
				if (storeData[0].data.classification != '' && storeData[0].data.classification != undefined && storeData[0].data.classification != null) {
					Ext.getCmp('upClass').setValue(storeData[0].data.classification);
				}
				Ext.getCmp('upHighS').setValue(storeData[0].data.highSchool);
				Ext.getCmp('upIntMaj').setValue(storeData[0].data.intendedMajor);
				Ext.getCmp('upAsScore').setValue(storeData[0].data.actScore);
				if (storeData[0].data.requestInfo != '' && storeData[0].data.requestInfo != undefined && storeData[0].data.requestInfo != null) {
					Ext.getCmp('upReqInf').setValue(storeData[0].data.requestInfo);
				}
				if (storeData[0].data.lookInfo != '' && storeData[0].data.lookInfo != undefined && storeData[0].data.lookInfo != null) {
					Ext.getCmp('upInf').setValue(storeData[0].data.lookInfo);
				}
				if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
					Ext.getCmp('upStatusText').setValue(storeData[0].data.leadStatus);
				}
			}
		},
		profileUpdates: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.profileDetail');
		},
		saveLeadProfile: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');
			var storeData = detailStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstName').getValue();
			var updateLName = Ext.getCmp('upLastName').getValue();
			var updateBirthDate = Ext.getCmp('updateDateValue').getValue();
			var updateEmail = Ext.getCmp('upEmailText').getValue();
			var updatePhone = Ext.getCmp('upPhoneNo').getValue();
			var updateStatus = Ext.getCmp('upStatusText').getValue();
			var updateClassText = Ext.getCmp('upClass').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMaj').getValue();
			var updateSchoolName = Ext.getCmp('upHighS').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInf').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScore').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInf').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateStatus != null && updateStatus != '' &&
				updateIntMajor != null && updateIntMajor != '' && updateSchoolName != null && updateSchoolName != '') {
				var firstName = Ext.getCmp('upFirstName');
				var fName = firstName.getValue();
				var lastName = Ext.getCmp('upLastName');
				var lName = lastName.getValue();
				var regExp = new RegExp("^[a-zA-Z\\- ]+$");
				if (fName.charAt(0) == ' ') {
					Ext.Msg.show({
						id: 'firstNmae',
						name: 'firstNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>FirstName must starts with alphabet.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					if (lName.charAt(0) == ' ') {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							title: null,
							cls: 'msgbox',
							message: '<p>LastName must starts with alphabet.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						if (!regExp.test(fName)) {
							Ext.Msg.show({
								id: 'firstNmae',
								name: 'firstNmae',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid First Name.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										firstName.focus(true);
									}
								}
							});
						} else {
							var lastName = Ext.getCmp('lastname');
							var lastNRegExp = new RegExp("^[a-zA-Z\\- ]+$");
							if (!lastNRegExp.test(lName)) {
								Ext.Msg.show({
									id: 'lastNmae',
									name: 'lastNmae',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid Last Name.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											lastName.focus(true);
										}
									}
								});
							} else {
								var updatePhone1 = Ext.getCmp('upPhoneNo');
								var updatePhoneValue = updatePhone1.getValue();
								updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

								var length = updatePhoneValue.length;
								if (updatePhoneValue.length != 10) {
									Ext.Msg.show({
										id: 'phonemsg',
										name: 'phonemsg',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid phone number.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												updatePhone1.focus(true);
											}
										}
									});
								} else {
									var a = updatePhoneValue.replace(/\D/g, '');
									var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
									updatePhone1.setValue(newValue);
									var updateMail = Ext.getCmp('upEmailText');
									var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
									if (updateEmail != '' && regMail.test(updateEmail) == false) {
										Ext.Msg.show({
											id: 'upMail',
											name: 'upMail',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid email address.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													updateMail.focus(true);
												}
											}
										});
									} else {
										var schoolName = Ext.getCmp('upHighS');
										var schoolNameRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
										if (!schoolNameRegExp.test(updateSchoolName)) {
											Ext.Msg.show({
												id: 'hSchool',
												name: 'hSchool',
												title: null,
												cls: 'msgbox',
												message: '<p>Invalid high school.</p>',
												buttons: Ext.MessageBox.OK,
												fn: function(btn) {
													if (btn == 'ok') {
														schoolName.focus(true);
													}
												}
											});
										} else {
											var intMajor = Ext.getCmp('upIntMaj');
											var intMajorRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
											if (!intMajorRegExp.test(updateIntMajor)) {
												Ext.Msg.show({
													id: 'intMName',
													name: 'intMName',
													title: null,
													cls: 'msgbox',
													message: '<p>Invalid intended major.</p>',
													buttons: Ext.MessageBox.OK,
													fn: function(btn) {
														if (btn == 'ok') {
															intMajor.focus(true);
														}
													}
												});
											} else {
												mobEdu.enroute.leads.f.loadLeadUpdateProfile();
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadLeadUpdateProfile: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileList');
			var storeData = detailStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstName').getValue();
			var updateLName = Ext.getCmp('upLastName').getValue();
			var updateBirthDate = Ext.getCmp('updateDateValue').getValue();
			var updateEmail = Ext.getCmp('upEmailText').getValue();
			var updatePhone = Ext.getCmp('upPhoneNo').getValue();
			var updateStatus = Ext.getCmp('upStatusText').getValue();
			var updateClassText = Ext.getCmp('upClass').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMaj').getValue();
			var updateSchoolName = Ext.getCmp('upHighS').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInf').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScore').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInf').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadProfileUpdate');

			updateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			updateStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/update');
			updateStore.getProxy().setExtraParams({
				leadID: leadId,
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				status: updateStatus,
				classification: updateClassText,
				highSchool: updateSchoolName,
				intendedMajor: updateIntMajor,
				actScore: updateActSatScore,
				requestInfo: updateRequestInfo,
				lookInfo: updateWhatTypeInf,
				companyId: companyId
			});
			updateStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.loadLeadUpdateProfileResponseHandler();
			};
			updateStore.load();
		},

		loadLeadUpdateProfileResponseHandler: function() {
			var updateStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadProfileUpdate');
			var updateStatus = updateStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showProfileList);
							mobEdu.util.show('mobEdu.enroute.leads.view.viewProfileList');
						}
					}
				});
			} else {
				Ext.Msg.alert(null, updateStatus.status);
			}
		},

		loadEmail: function() {
			var c = mobEdu.enroute.leads.f.count;
			if (c == 0) {
				mobEdu.util.get('mobEdu.enroute.leads.view.emailList');
				var searchListCmp = Ext.getCmp('emailLst');
				searchListCmp.setPlugins(null);
				var pArray = new Array();
				pArray[0] = mobEdu.enroute.main.view.paging.create();
				searchListCmp.setPlugins(pArray);
				searchListCmp.setEmptyText('<h3 align="center">No messages.</h3>');
				Ext.getCmp('messageplugin').getLoadMoreCmp().show();
				mobEdu.enroute.leads.f.count = 1;
			}
			var label = '';
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;
			var serviceText = 'recruiteremail';
			var operationText = 'leademails';
			var searchText = '';
			var entityType = 'LEAD';
			var eStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.email');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/messageList');
			eStore.getProxy().setExtraParams({
				// label: label,
				// searchText: searchText,
				fromID: lId,
				companyId: companyId
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.loadEmailResponseHandler();
			};
			eStore.load();
		},
		loadEmailResponseHandler: function() {
			var emailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.email');
			emailStore.sort('versionDate', 'DESC');
			var eStatus = emailStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success') {
				mobEdu.enroute.leads.f.showEmailList();
			} else {
				Ext.Msg.show({
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;

			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewEmail');

			detailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			detailStore.getProxy().setUrl(commonwebserver + 'message/get');
			detailStore.getProxy().setExtraParams({
				messageID: eId,
				companyId: companyId
			});

			detailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.loadEmailDetailResponseHandler();
			};
			detailStore.load();

		},
		loadEmailDetailResponseHandler: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewEmail');
			var eDetailStatus = detailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status == 'success') {
				mobEdu.enroute.leads.f.showEmailDetail();
			} else {
				Ext.Msg.show({
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		hasEmailDirection: function(labels) {
			var dirLabel = labels[0].label;
			if (dirLabel == "SENT") {
				return true;
			} else {
				return false;
			}
		},

		hasReadFlag: function(status) {
			if (status == "Opened" || status === '1') {
				return true;
			} else {
				return false;
			}
		},

		sentmail: function() {
			var esub = Ext.getCmp('newleadsub').getValue();
			var msgArea = Ext.getCmp('newleadmsg').getValue();
			esub = esub.trim();
			msgArea = msgArea.trim();
			if (msgArea != '' && esub != '') {
				mobEdu.enroute.leads.f.leadSentEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		leadSentEmail: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;
			var esub = Ext.getCmp('newleadsub').getValue();
			var msgArea = Ext.getCmp('newleadsub').getValue();
			var conversationID = '';
			var eStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/send');
			eStore.getProxy().setExtraParams({
				to: lId,
				subject: esub,
				body: msgArea,
				conversationID: conversationID,
				companyId: companyId
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.newEmailResponseHandler();
			};
			eStore.load();
		},
		sentReplyMessage: function() {
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();
			esub = esub.trim();
			msgArea = msgArea.trim();
			if (msgArea != '' && esub != '') {
				mobEdu.enroute.leads.f.sentNewEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sentNewEmail: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();
			var conversationID = '';
			var eStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/send');
			eStore.getProxy().setExtraParams({
				to: lId,
				subject: esub,
				body: msgArea,
				conversationID: conversationID,
				companyId: companyId
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.newEmailResponseHandler();
			};
			eStore.load();
		},

		newEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newEmail');
			var nEStatus = nEStore.getProxy().getReader().rawData;
			if (nEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsubmitsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Message sent successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.leads.f.emailListBackButton();
							mobEdu.enroute.leads.f.resetMail();
							mobEdu.enroute.leads.f.resetSendReplyMail();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetMail: function() {
			mobEdu.util.get('mobEdu.enroute.leads.view.leadNewEmail');
			Ext.getCmp('newleadsub').setValue('');
			Ext.getCmp('newleadmsg').setValue('');
		},
		resetSendReplyMail: function() {
			mobEdu.util.get('mobEdu.enroute.leads.view.newMail');
			Ext.getCmp('newsub').setValue('');
			Ext.getCmp('newmsg').setValue('');
		},

		loadAppointments: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;

			var searchText = '';

			var appStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointments');

			appStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			appStore.getProxy().setUrl(commonwebserver + 'appointment/createdsearch');
			appStore.getProxy().setExtraParams({
				to: lId,
				searchText: searchText,
				maxRecords: 100,
				companyId: companyId
			});

			appStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.appointmentsResponseHandler();
			};
			appStore.load();
		},
		appointmentsResponseHandler: function() {
			var appStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointments');
			appStore.sort('appointmentDate', 'DESC');
			var appStatus = appStore.getProxy().getReader().rawData;
			mobEdu.enroute.leads.f.showAppointmentsList();
			/*if (appStatus.status == 'success') {
				mobEdu.enroute.leads.f.showAppointmentsList();
			} else {
				Ext.Msg.show({
					message: appStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}*/
		},

		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},

		sendAppointment: function() {
			var aSub = Ext.getCmp('appsub').getValue();
			var aDes = Ext.getCmp('appdes').getValue();
			var aDate = Ext.getCmp('appdate').getValue();
			var aLoc = Ext.getCmp('apploc').getValue();
			var aTimeHours = Ext.getCmp('appTimeHours').getValue();
			var aTimeMinutes = Ext.getCmp('appTimeMinutes').getValue();
			if (aTimeHours == 0 || aTimeHours == null) {
				aTimeHours = '';
			}
			if (aSub.charAt(0) == ' ' || aDes.charAt(0) == ' ') {
				Ext.Msg.show({
					message: '<p>Subject & Description must start with alphanumeric.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aTimeHours != '') {
					var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);
					var selectDate = Ext.getCmp('appdate').getValue();
					selectDate = Ext.Date.format(selectDate, 'm/d/Y');
					if (aTimeMinutes < 10) {
						aTimeMinutes = "0" + aTimeMinutes;
					}
					var selectDateFormatted = Ext.Date.parse(selectDate + " " + aTimeHours + ":" + aTimeMinutes, 'm/d/Y G:i');
					if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
						mobEdu.enroute.leads.f.appointmentResponse();
					} else {
						Ext.Msg.show({
							title: 'Invalid Date',
							message: '<p>Please select a future date..</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
					if (aTimeHours >= 18) {
						Ext.Msg.show({
							title: 'Invalid Time',
							message: '<p>Appointment time between 8AM to 18PM.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}

				} else {
					Ext.Msg.show({
						message: '<p>Please provide all mandatory fields.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}

		},
		appointmentResponse: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;
			var aSub = Ext.getCmp('appsub').getValue();
			var aDes = Ext.getCmp('appdes').getValue();
			var aDate = Ext.getCmp('appdate').getValue();
			var aLoc = Ext.getCmp('apploc').getValue();
			var aStatus = 'SCHEDULED';
			//            var aTime = Ext.getCmp('appTime').getValue();
			var aTimeHours = Ext.getCmp('appTimeHours').getValue();
			var aTimeMinutes = Ext.getCmp('appTimeMinutes').getValue();
			var appTime;
			if (aTimeHours == null) {
				aTimeHours = '';
			}
			if (aTimeMinutes == null) {
				aTimeMinutes = '';
			}
			appTime = aTimeHours + ':' + aTimeMinutes;
			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(aDate, 'm/d/Y');
			}
			var appDateTime = appDateFormatted + ' ' + appTime;
			if (aTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.loadAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.enroute.leads.view.newAppointment');
						}
					}
				});
			} else {
				var newAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(commonwebserver + 'appointment/add');
				newAppStore.getProxy().setExtraParams({
					to: lId,
					subject: aSub,
					body: aDes,
					appointmentDate: appDateTime,
					location: aLoc,
					status: aStatus,
					companyId: companyId
				});

				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enroute.leads.f.newAppointmentResponseHandler();
				};
				newAppStore.load();
			}
		},
		newAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment created successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.leads.f.appointmentListBack();
							mobEdu.enroute.leads.f.resetAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadUpdateAppointment: function() {
			mobEdu.enroute.main.f.loadAppointmentEnumerations();
			var upAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');
			var upDateStore = upAppStore.data.all;
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadAppDetail);
			mobEdu.util.show('mobEdu.enroute.leads.view.updateAppointment');
			// Ext.getCmp('upAppTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upsub').setValue(upDateStore[0].data.subject);
			Ext.getCmp('updes').setValue(upDateStore[0].data.body);
			var appDate = upDateStore[0].data.appointmentDate;
			var appDateValue = appDate.split(' ');
			var dateFormatted = appDateValue[0];
			var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
			var appTime = appDateValue[1];
			var appTimeFormatted = appTime.split(':');
			var appTimeHours = appTimeFormatted[0];
			var appTimeMints = appTimeFormatted[1];

			if (upDateStore[0].data.appointmentDate != '' && upDateStore[0].data.appointmentDate != undefined && upDateStore[0].data.appointmentDate != null) {
				Ext.getCmp('update').setValue(new Date(newStartDate));
			}
			Ext.getCmp('upTimeHours').setValue(appTimeHours);
			Ext.getCmp('upTimeMinutes').setValue(appTimeMints);
			Ext.getCmp('uploc').setValue(upDateStore[0].data.location);
			// if (upDateStore[0].data.appointmentStatus != '' && upDateStore[0].data.appointmentStatus != undefined && upDateStore[0].data.appointmentStatus != null) {
			// 	Ext.getCmp('upsta').setValue(upDateStore[0].data.appointmentStatus);
			// }
		},

		updateAppointmentSend: function() {
			var upSub = Ext.getCmp('upsub').getValue();
			var upDes = Ext.getCmp('updes').getValue();
			var upDate = Ext.getCmp('update').getValue();
			var upLoc = Ext.getCmp('uploc').getValue();
			var upStatus = 'RESCHEDULED';
			//            var uppTime = Ext.getCmp('upTime').getValue();
			var upAppTimeHours = Ext.getCmp('upTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('upTimeMinutes').getValue();

			if (upSub != '' && upDes != '' && upDate != '' && upLoc != '' && upAppTimeHours != '') {
				var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);


				var selectDate = Ext.getCmp('update').getValue();
				selectDate = Ext.Date.format(selectDate, 'm/d/Y');
				if (upAppTimeMints < 10) {
					upAppTimeMints = "0" + upAppTimeMints;
				}
				var selectDateFormatted = Ext.Date.parse(selectDate + " " + upAppTimeHours + ":" + upAppTimeMints, 'm/d/Y G:i');

				if (upAppTimeHours >= 18) {
					Ext.Msg.show({
						title: 'Invalid Time',
						message: '<p>Appointment time between 8AM to 18PM.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
					mobEdu.enroute.leads.f.updateAppointmentRequestSent();
				} else {
					Ext.Msg.show({
						title: 'Invalid Date',
						message: '<p>Please select a future date..</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateAppointmentRequestSent: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;
			var appDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');
			var appDeatil = appDetailStore.data.all;
			var appointmentId = appDeatil[0].data.appointmentID;
			//var from = appDeatil[0].data.fromID;
			//var to = appDeatil[0].data.toID;
			var appDetail = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');
			var detailData = appDetail.data.all;
			//            var appStatus = detailData[0].data.status;
			var upSub = Ext.getCmp('upsub').getValue();
			var upDes = Ext.getCmp('updes').getValue();
			var upDate = Ext.getCmp('update').getValue();
			var upLoc = Ext.getCmp('uploc').getValue();
			var upStatus = 'RESCHEDULED';
			//            var uppTime = Ext.getCmp('upTime').getValue();
			var from = '';
			var fromDetails = appDeatil[0].raw.fromDetails;
			var userID = fromDetails.fromID;
			var acceptance = fromDetails.acceptance;
			from = userID + ',' + acceptance + ';';

			var to = appDeatil[0].raw.toIDs;
			var versionUser = appDeatil[0].raw.createdBy;
			var participant = '';

			var res = '';
			for (var i = 0; i < to.length; i++) {
				var userID = to[i].toID;
				var acceptance = to[i].acceptance;
				res = res + userID + ',' + acceptance + ';';
			}
			var upAppTimeHours = Ext.getCmp('upTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('upTimeMinutes').getValue();
			var upAppTime;
			if (upAppTimeHours == null) {
				upAppTimeHours = '';
			}
			if (upAppTimeMints == null) {
				upAppTimeMints = '';
			}
			upAppTime = upAppTimeHours + ':' + upAppTimeMints;
			var appDateFormatted;
			if (upDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(upDate, 'm/d/Y');
				//appDateFormatted = ([upDate.getMonth() + 1] + '/' + upDate.getDate() + '/' + upDate.getFullYear());
			}
			var appDateTime = appDateFormatted + ' ' + upAppTime;
			var currentDate = new Date();
			var currentDateFormatted = Ext.Date.format(currentDate, 'm/d/Y');
			//var currentDateFormatted = ([currentDate.getMonth() + 1] + '/' + currentDate.getDate() + '/' + currentDate.getFullYear());
			if (upAppTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.loadAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.enroute.leads.view.newAppointment');
						}
					}
				});
			} else {
				var newAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(commonwebserver + 'appointment/update');
				newAppStore.getProxy().setExtraParams({
					appointmentID: appointmentId,
					subject: upSub,
					body: upDes,
					location: upLoc,
					status: upStatus,
					appointmentDate: appDateTime,
					companyId: companyId,
					versionUser: mobEdu.util.getStudentId(),
					to: res,
					from: from
				});
				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enroute.leads.f.updateAppointmentResponseHandler();
				};
				newAppStore.load();
			}
		},

		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.leads.f.appointmentListBack();
							mobEdu.enroute.leads.f.resetUpDateAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetAppointment: function() {
			mobEdu.util.get('mobEdu.enroute.leads.view.newAppointment');
			Ext.getCmp('appsub').reset();
			Ext.getCmp('apploc').reset();
			Ext.getCmp('appdes').reset();
		},

		resetUpDateAppointment: function() {
			mobEdu.util.get('mobEdu.enroute.leads.view.updateAppointment');
			Ext.getCmp('upsub').reset();
			Ext.getCmp('uploc').reset();
			Ext.getCmp('updes').reset();
		},

		onAppTimeKeyup: function(appTimeField) {
			var timeField = Ext.getCmp('appTime');
			var reg = new RegExp('^(20|21|22|23|[01]\d|\d)(([:][0-5]\d){1,2})$');
			if (reg.test(appTimeField.getValue()).toString() == false) {
				Ext.Msg.show({
					id: 'timeMsg',
					name: 'timeMsg',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid time.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							timeField.focus(true);
						}
					}
				});
			}
			//            var phoneNumber=(appTimeField.getValue()).toString()+'';
			////            phoneNumber = phoneNumber.replace(/\D/g, '');
			//            appTimeField.setValue(phoneNumber);
			//            var length=phoneNumber.length;
			//            if(length>5){
			//                appTimeField.setValue(phoneNumber.substring(0,5));
			//                return false;
			//            }
			//            return true;
		},

		loadNotificationsList: function() {
			var lIdStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var lIDdata = lIdStore.data.all;
			var lId = lIDdata[0].data.leadID;
			var upappServiceText = 'notification';
			var upOperationText = 'leadnotifications';
			var notfiID = 1;
			var notfiStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.notifications');

			notfiStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			notfiStore.getProxy().setUrl(enrouteWebserver + 'enquire');
			notfiStore.getProxy().setExtraParams({
				service: upappServiceText,
				operation: upOperationText,
				leadID: lId,
				companyId: companyId
			});

			notfiStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.loadNotificationsResponseHandler();
			};
			notfiStore.load();
		},
		loadNotificationsResponseHandler: function() {
			var notfiStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.notifications');
			var notfiStatus = notfiStore.getProxy().getReader().rawData;
			if (notfiStatus.id != '') {
				mobEdu.enroute.leads.f.showNotificationsList();
			} else {
				Ext.Msg.show({
					message: '<p>No notifications.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadNotificationListDetail: function(record) {
			var notfiID = record.data.id;
			var notifServiceText = 'notification';
			var notifOperationText = 'view';

			var notifiDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.notificationDetail');

			notifiDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			notifiDetailStore.getProxy().setUrl(enrouteWebserver + 'enquire');
			notifiDetailStore.getProxy().setExtraParams({
				service: notifServiceText,
				operation: notifOperationText,
				notificationID: notfiID,
				companyId: companyId
			});

			notifiDetailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.lNotificationListDetailResponseHandler();
			};
			notifiDetailStore.load();
		},

		lNotificationListDetailResponseHandler: function() {
			var notifiDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.notificationDetail');
			var notfiDetailStatus = notifiDetailStore.getProxy().getReader().rawData;
			if (notfiDetailStatus.id != '') {
				mobEdu.enroute.leads.f.showNotificationDetail();
			} else {
				Ext.Msg.show({
					message: '<p>No notifications.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAppointDetail: function(record) {
			var acceptance = '';
			var appId = record.data.appointmentID;
			var appId = record.data.appointmentID;
			var acceptance = record.data.cAcceptance;
			var versionUser = record.data.createdBy;
			var user = mobEdu.util.getStudentId();
			var fromDetails = record.data.fromDetails;
			var toArry = record.data.toIDs;
			var fromID = fromDetails.fromID;
			if (fromID == record.data.loginUserID) {
				acceptance = fromDetails.acceptance;
			}

			if (acceptance == '' || acceptance == undefined || acceptance == null) {
				for (var i = 0; i < toArry.length; i++) {
					var toID = toArry[i].toID;
					if (toID == record.data.loginUserID) {
						acceptance = toArry[i].acceptance;
					}
				}
			}

			if (versionUser == user) {
				mobEdu.util.get('mobEdu.enroute.leads.view.appointmentDetail');
				Ext.getCmp('laccept').hide();
				Ext.getCmp('ldecline').hide();
			} else {
				mobEdu.util.get('mobEdu.enroute.leads.view.appointmentDetail');
				Ext.getCmp('laccept').show();
				Ext.getCmp('ldecline').show();
				if (acceptance == 'Y' || acceptance == 'N') {
					mobEdu.util.get('mobEdu.enroute.leads.view.appointmentDetail');
					Ext.getCmp('laccept').hide();
					Ext.getCmp('ldecline').hide();
				} else {
					mobEdu.util.get('mobEdu.enroute.leads.view.appointmentDetail');
					Ext.getCmp('laccept').show();
					Ext.getCmp('ldecline').show();
				}
			}
			var appDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');

			appDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			appDetailStore.getProxy().setUrl(commonwebserver + 'appointment/get');
			appDetailStore.getProxy().setExtraParams({
				appointmentID: appId,
				companyId: companyId
			});

			appDetailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.appointmentDetailResponseHandler();
			};
			appDetailStore.load();
		},
		appointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');
			var appDetailStatus = appDetailStore.getProxy().getReader().rawData;
			mobEdu.enroute.leads.f.loadAppDetail();
			/*if (appDetailStatus.status == 'success') {
				mobEdu.enroute.leads.f.loadAppDetail();
			} else {
				Ext.Msg.show({
					message: appDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}*/
		},

		appointmentListBack: function() {
			var appListStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointments');
			appListStore.load();
			mobEdu.enroute.leads.f.showAppointmentsList();
		},

		hasAppointmentDirection: function(appSource) {
			if (appSource == "IN") {
				return true;
			} else {
				return false;
			}
		},

		showProfileDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.profileDetail')
		},

		showProfileList: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showProfileLists);
			mobEdu.util.show('mobEdu.enroute.leads.view.profileList');
		},
		showProfileLists: function() {
			if (mobEdu.enroute.main.f.leadSearchType === 'UNLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsList);
				mobEdu.util.show('mobEdu.enroute.main.view.viewProfileList');
				// Ext.getCmp('viewSupProfTool').setTitle('<h1>' + leadTitle + '</h1>');
			} else if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
				mobEdu.util.show('mobEdu.enroute.main.view.viewProfileList');
				// Ext.getCmp('viewSupProfTool').setTitle('<h1>' + leadTitle + '</h1>');
			} else {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
				mobEdu.util.show('mobEdu.enroute.main.view.menu');
			}
		},
		emailListBackButton: function() {
			var eListStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.email');
			eListStore.currentPage = 1;
			eListStore.load();
			mobEdu.enroute.leads.f.showEmailList();
		},
		setPagingPlugin: function(listComponent) {

			if (Ext.getCmp('leadplugin') == null) {
				var pArray = new Array();
				pArray[0] = mobEdu.enroute.leads.view.paging.create();
				listComponent.setPlugins(pArray);
			}
			Ext.getCmp('leadplugin').getLoadMoreCmp().show();

		},

		showEmailList: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.emailList');
		},
		showEmailDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.emailListBackButton);
			mobEdu.util.show('mobEdu.enroute.leads.view.emailDetail');
			var messagesStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewEmail');
			var messageData = messagesStore.data.all;
			for (var i = 0; i < messageData.length; i++) {
				var directionLabels = messageData[i].data.labels;
				for (var l = 0; l < directionLabels.length; l++) {
					var direction = directionLabels[l].label;
				}
			}
			if (direction == "SENT") {
				Ext.getCmp('msgToolbar').hide();
			} else {
				Ext.getCmp('msgToolbar').show();
			}
		},

		showNotificationsList: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.notificationsList');
			// Ext.getCmp('notifiListTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showAppointmentsList: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.appointmentsList');
			// Ext.getCmp('appTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showAdminAppList: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.adminAppList');
		},
		showNewMail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showEmailDetail);
			mobEdu.util.show('mobEdu.enroute.leads.view.newMail');
			var messagesStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewEmail');
			var messageData = messagesStore.data.all;
			for (var i = 0; i < messageData.length; i++) {
				var subject = messageData[i].data.subject;
			}
			// Ext.getCmp('newETitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('newsub').setValue(subject);
		},
		showLeadEmail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showEmailList);
			mobEdu.util.show('mobEdu.enroute.leads.view.leadNewEmail');
		},
		showAdmissionApp: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.admissionApp');
		},
		showAppointmentDetail: function(record) {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');
			appDetailStore.removeAll();
			appDetailStore.add(record);
			appDetailStore.sync();
			mobEdu.enroute.leads.f.loadAppDetail();
		},
		loadAppDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.appointmentListBack);
			mobEdu.util.show('mobEdu.enroute.leads.view.appointmentDetail');
			// Ext.getCmp('appDetailTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showNotificationDetail: function(record) {
			//            var notifiDetailStore=mobEdu.util.getStore('mobEdu.enroute.leads.store.notificationDetail');
			//            notifiDetailStore.removeAll();
			//            notifiDetailStore.add(record);
			//            notifiDetailStore.sync();
			mobEdu.enroute.leads.f.loadNotifiDetail();
		},
		loadNotifiDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showNotificationsList);
			mobEdu.util.show('mobEdu.enroute.leads.view.notificationDetail');
			// Ext.getCmp('nltfiDatilTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		showFinancialApp: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadViewProfile);
			mobEdu.util.show('mobEdu.enroute.leads.view.financialApp');
			// Ext.getCmp('financialTitle').setTitle('<h1>' + leadTitle + '</h1>');
		},
		onAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
				//newDateFormatted = ([newDate.getMonth() + 1] + '/' + newDate.getDate() + '/' + newDate.getFullYear());
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
				//presentDateFormatted = ([presentDate.getMonth() + 1] + '/' + presentDate.getDate() + '/' + presentDate.getFullYear());
			}

			if (newDateFormatted == presentDateFormatted) {
				if (Ext.getCmp('appTimeHours') != undefined) {
					Ext.getCmp('appTimeHours').setMinValue(timeH);
					Ext.getCmp('appTimeHours').setValue(timeH);
				}
				if (Ext.getCmp('appTimeMinutes') != undefined) {
					Ext.getCmp('appTimeMinutes').setMinValue(timeM);
					Ext.getCmp('appTimeMinutes').setValue(timeM);
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('appdate').setValue(new Date());
							Ext.getCmp('appTimeHours').setMinValue(timeH);
							Ext.getCmp('appTimeMinutes').setMinValue(timeM);
							Ext.getCmp('appTimeHours').setValue(timeH);
							Ext.getCmp('appTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('appTimeHours').setMinValue(8);
				Ext.getCmp('appTimeMinutes').setMinValue(0);
				Ext.getCmp('appTimeHours').setValue(8);
				Ext.getCmp('appTimeMinutes').setValue(0);
			}
		},

		onUpAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
				//newDateFormatted = ([newDate.getMonth() + 1] + '/' + newDate.getDate() + '/' + newDate.getFullYear());
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
				//presentDateFormatted = ([presentDate.getMonth() + 1] + '/' + presentDate.getDate() + '/' + presentDate.getFullYear());
			}

			if (newDateFormatted == presentDateFormatted) {
				if (Ext.getCmp('upTimeHours') != undefined) {
					Ext.getCmp('upTimeHours').setMinValue(timeH);
					Ext.getCmp('upTimeHours').setValue(timeH);
				}
				if (Ext.getCmp('upTimeMinutes') != undefined) {
					Ext.getCmp('upTimeMinutes').setMinValue(timeM);
					Ext.getCmp('upTimeMinutes').setValue(timeM);
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('update').setValue(new Date());
							Ext.getCmp('upTimeHours').setMinValue(timeH);
							Ext.getCmp('upTimeMinutes').setMinValue(timeM);
							Ext.getCmp('upTimeHours').setValue(timeH);
							Ext.getCmp('upTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('upTimeHours').setMinValue(8);
				Ext.getCmp('upTimeMinutes').setMinValue(0);
				Ext.getCmp('upTimeHours').setValue(8);
				Ext.getCmp('upTimeMinutes').setValue(0);
			}
		},

		showNewAppointment: function() {
			mobEdu.enroute.main.f.loadAppointmentEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.enroute.leads.view.newAppointment');
			var newDate = Ext.getCmp('appdate').getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			if (timeH < 8) {
				Ext.getCmp('appTimeHours').setMinValue(8);
				Ext.getCmp('appTimeMinutes').setMinValue(timeM);
				Ext.getCmp('appTimeHours').setValue(8);
				Ext.getCmp('appTimeMinutes').setValue(timeM);
			} else {
				Ext.getCmp('appTimeHours').setMinValue(timeH);
				Ext.getCmp('appTimeMinutes').setMinValue(timeM);
				Ext.getCmp('appTimeHours').setValue(timeH);
				Ext.getCmp('appTimeMinutes').setValue(timeM);
			}
		},
		checkForSearchListEnd: function(store, records, isSuccessful) {
			var pageSize = store.getPageSize();
			var pageIndex = store.currentPage - 1; // Page numbers start at 1

			if (isSuccessful && records.length < pageSize) {
				//Set count to disable 'loading' message
				var totalRecords = pageIndex * pageSize + records.length;
				store.setTotalCount(totalRecords);
			} else
				store.setTotalCount(null);

		},
		acceptAppointment: function(value) {
			if (value == 'Accept') {
				Ext.Msg.show({
					id: 'aAccept',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Accepted.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.leads.f.appointmentListBack();
						}
					}
				});
			}
			if (value == 'Decline') {
				Ext.Msg.show({
					id: 'aDecline',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Declined.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.leads.f.appointmentListBack();
						}
					}
				});
			}
			var appDetail = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointmentDetail');
			mobEdu.util.get('mobEdu.enroute.leads.view.appointmentDetail');
			Ext.getCmp('laccept').hide();
			Ext.getCmp('ldecline').hide();

			var detailData = appDetail.data.all;
			var appId = detailData[0].data.appointmentID;
			var acceptance;
			var status;
			if (value == 'Accept') {
				acceptance = 'Y';
				status = 'accepted';
			} else {
				acceptance = 'N';
				status = 'declined';
			};
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.lacceptance');
			newAppStore.getProxy().setUrl(commonwebserver + 'appointment/statusUpdate');
			newAppStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			newAppStore.getProxy().setExtraParams({
				appointmentID: appId,
				acceptance: acceptance,
				companyId: companyId,
				status: status
			});
			newAppStore.getProxy().afterRequest = function() {
				mobEdu.enroute.leads.f.acceptanceResponseHandler();
			};
			newAppStore.load();
		},
		acceptanceResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.lacceptance');
		},
		showCalendar: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.appointmentListBack);
			mobEdu.enroute.leads.f.loadCalendarView();
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.appointmentListBack);
			mobEdu.util.show('mobEdu.enroute.leads.view.rAppointmentCalendar');
		},

		loadCalendarView: function() {
			mobEdu.main.f.calendarInstance = 'leadsAppt';
			mobEdu.util.get('mobEdu.enroute.leads.view.rAppointmentCalendar');
			var appointmentDock = Ext.getCmp('lappointmentsDock');
			appointmentDock.removeAll(true, true);
			appointmentDock.removed = [];

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'leadsCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			appointmentDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.enroute.leads.f.setAppointmentScheduleInfo(new Date());
		},
		setAppointmentScheduleInfo: function(selectedDate) {
			var scheduleInfo = '';
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			var newDateValue = (mobEdu.enroute.leads.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enroute.leads.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());

			var appointmentsStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointments');
			appointmentsStore.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var leadAcceptance = rec.get('sAcceptance');
				var recAcceptance = rec.get('cAcceptance');

				var appDateValue = appDate.split(' ');
				var dateFormatted = appDateValue[0];
				if (newDateValue == dateFormatted) {
					var appTime = appDateValue[1];
					var appSubject = rec.get('subject');
					var appDesc = rec.get('body');
					var appLocation = rec.get('location');
					scheduleInfo = mobEdu.enroute.leads.f.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc, leadAcceptance, recAcceptance);
				}
			});
			//set the event schedule info in calendar view label
			Ext.getCmp('lAppointmentLabel').setHtml(scheduleInfo);
		},
		formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc, leadAcceptance, recAcceptance) {
			//			if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
			if (leadAcceptance != 'N' && recAcceptance != 'N') {

				if (appSubject == null) {
					appSubject = '';
				}
				if (appTime == null) {
					appTime = '';
				}
				if (appDesc == null) {
					appDesc = '';
				}
				if (appLocation == null) {
					appLocation = '';
				}
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + decodeURIComponent(appSubject) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + decodeURIComponent(appLocation) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Description: ' + '</b>' + decodeURIComponent(appDesc) + '</h3>' + '<br/>';

				return scheduleInfo;
			} else {
				return scheduleInfo;
			}
		},
		doHighlightAppointmentsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.appointments');
			store.each(function(rec) {
				var leadAcceptance = rec.get('sAcceptance');
				var recAcceptance = rec.get('cAcceptance');
				//	if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
				if (leadAcceptance != 'N' && recAcceptance != 'N') {
					var appDate = rec.get('appointmentDate');
					var appDateValue = appDate.split(' ');
					var dateFormatted = appDateValue[0];
					var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
					if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
						classes.push(highlightedItemCls);
					}
				}
			});
			return classes;
		},
		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},
		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.enroute.leads.f.dateChecking([currentDate.getMonth() + 1]) + '/' + mobEdu.enroute.leads.f.dateChecking(currentDate.getDate()) + '/' + currentDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.enroute.leads.f.dateChecking([presentDate.getMonth() + 1]) + '/' + mobEdu.enroute.leads.f.dateChecking(presentDate.getDate()) + '/' + presentDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.enroute.leads.f.setAppointmentScheduleInfo(date)
			}
		},


	}
});