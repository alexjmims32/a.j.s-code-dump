Ext.define('mobEdu.enroute.leads.store.newAppointment', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.newAppointment'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.newAppointment',

        autoLoad: false,

        model: 'mobEdu.enroute.leads.model.newAppointment',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});