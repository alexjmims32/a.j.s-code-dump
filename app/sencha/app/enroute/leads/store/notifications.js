Ext.define('mobEdu.enroute.leads.store.notifications', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.notifications'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.notifications',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.notifications',

                    proxy: {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'notificationList.notification'
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'notificationList.notification'
//        return proxy;
//    }
});