Ext.define('mobEdu.enroute.leads.store.email', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.email'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.email',

        autoLoad: false,
        pageSize: 20,

        model: 'mobEdu.enroute.leads.model.email',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'messages'
            }
        }
    }
    //    ,
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= 'emails'
    //        return proxy;
    //    }

});