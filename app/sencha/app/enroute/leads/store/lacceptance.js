Ext.define('mobEdu.enroute.leads.store.lacceptance', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.enroute.leads.model.appointments'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.lacceptance',

        autoLoad: false,

        model: 'mobEdu.enroute.leads.model.appointments',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});