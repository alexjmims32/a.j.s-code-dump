Ext.define('mobEdu.enroute.leads.store.viewProfileList', {
    extend:'Ext.data.Store',


    requires: [
        'mobEdu.enroute.leads.model.viewProfileList'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.viewProfileList',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.viewProfileList',
        proxy:{
            type:'localstorage',
            id:'viewProfileList'
        }
    }
});
