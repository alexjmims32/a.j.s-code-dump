Ext.define('mobEdu.enroute.leads.store.appointmentDetail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.appointments'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.appointmentDetail',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.appointments',

             proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});
