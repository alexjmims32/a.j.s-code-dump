Ext.define('mobEdu.enroute.leads.store.enumerations', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.enumerations',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.enumerations',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enumerations'
            }
        }
    }
});