Ext.define('mobEdu.enroute.leads.store.profileSearch', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.profileList'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.profileSearch',

        autoLoad: false,
//        pageSize:pageSize,

        model: 'mobEdu.enroute.leads.model.profileList',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'leads'
            }
        }
    }
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'leads'
//        return proxy;
//    }

});