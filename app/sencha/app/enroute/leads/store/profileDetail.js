Ext.define('mobEdu.enroute.leads.store.profileDetail', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enroute.leads.model.profileList'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.profileDetail',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.profileList',
        proxy:{
            type:'localstorage',
            id:'profileDetail'
        }
    }
});