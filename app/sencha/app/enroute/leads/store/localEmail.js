Ext.define('mobEdu.enroute.leads.store.localEmail', {
    extend:'Ext.data.Store',
//    alias: 'notificationLocalStore',

    requires: [
        'mobEdu.enroute.leads.model.email'
    ],
//       model : 'notificationModel',
    config: {
        storeId: 'mobEdu.enroute.leads.store.localEmail',

        autoLoad: false,

        model: 'mobEdu.enroute.leads.model.email',

        proxy:{
            type:'localstorage',
            id:'emailLocal'
        }

    }
});