Ext.define('mobEdu.enroute.leads.store.viewApplication', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enroute.leads.model.viewApplication'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.viewApplication',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.viewApplication',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'application'
            }
        }
    }
});
