Ext.define('mobEdu.enroute.leads.store.appointments', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.appointments'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.appointments',

        autoLoad: false,

        model: 'mobEdu.enroute.leads.model.appointments',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'resultSet'
            }
        }
    }
    //    ,
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= 'appointments'
    //        return proxy;
    //    }

});