Ext.define('mobEdu.enroute.leads.store.notificationDetail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.notifications'
    ],

    config:{
        storeId: 'mobEdu.enroute.leads.store.notificationDetail',
        autoLoad: false,
        model: 'mobEdu.enroute.leads.model.notifications',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});
