Ext.define('mobEdu.enroute.leads.store.profileList', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.supLeadsList'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.profileList',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.supLeadsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json'
                //                rootProperty: ''
            }
        }
    }
    //    ,
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= ''
    //        return proxy;
    //    }
});