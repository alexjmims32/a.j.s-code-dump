Ext.define('mobEdu.enroute.leads.store.newEmail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.newEmail'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.newEmail',

        autoLoad: false,

        model: 'mobEdu.enroute.leads.model.newEmail',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});