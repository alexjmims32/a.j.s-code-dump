Ext.define('mobEdu.enroute.leads.store.viewEmail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.leads.model.email'
    ],
    config: {
        storeId: 'mobEdu.enroute.leads.store.viewEmail',

        autoLoad: false,

        model: 'mobEdu.enroute.leads.model.email',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }

//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});