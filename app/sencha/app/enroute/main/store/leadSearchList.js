Ext.define('mobEdu.enroute.main.store.leadSearchList', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enroute.main.model.supLeadsList'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.leadSearchList',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.supLeadsList',

               proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'leads'
            }
        }
    }
});