Ext.define('mobEdu.enroute.main.store.appointmentsList', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.appointmentsList'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.appointmentsList',

        autoLoad: false,
        //        pageSize:pageSize,

        model: 'mobEdu.enroute.main.model.appointmentsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'resultSet'
            }
        }
    }
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= 'appointments'
    //        return proxy;
    //    }

});
