Ext.define('mobEdu.enroute.main.store.stateEnumerations', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enroute.main.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.stateEnumerations',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.enumerations',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enumerations'
            }
        }
    }
});