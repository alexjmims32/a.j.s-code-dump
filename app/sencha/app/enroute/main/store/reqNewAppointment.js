Ext.define('mobEdu.enroute.main.store.reqNewAppointment', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.reqNewAppointment'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.reqNewAppointment',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.reqNewAppointment',

            proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }

});
