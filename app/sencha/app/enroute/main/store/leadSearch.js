Ext.define('mobEdu.enroute.main.store.leadSearch', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enroute.main.model.menu'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.leadSearch',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.menu',
        proxy:{
            type:'localstorage',
            id:'leadSearchStorage'
        }
    }
});