Ext.define('mobEdu.enroute.main.store.toAppStore', {
    extend: 'Ext.data.Store',

    requires: [
        'mobEdu.directory.model.people'
    ],

    config: {
        storeId: 'mobEdu.enroute.main.store.toAppStore',
        autoLoad: true,
        model: 'mobEdu.directory.model.people',
        proxy: {
            type: 'localstorage',
            id: 'toStoreLocal'
        }
    }
});