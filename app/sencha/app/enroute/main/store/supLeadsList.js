Ext.define('mobEdu.enroute.main.store.supLeadsList', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.supLeadsList'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.supLeadsList',

        autoLoad: false,
        //        pageSize:pageSize,

        model: 'mobEdu.enroute.main.model.supLeadsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'leads'
            }
        }
    }
    //    ,
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= 'leads'
    //        return proxy;
    //    }
});