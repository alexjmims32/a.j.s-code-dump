Ext.define('mobEdu.enroute.main.store.recruiterSearch', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.recruiterSearch'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.recruiterSearch',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.recruiterSearch',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'users'
            }
        }
    }
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'recruiters'
//        return proxy;
//    }
});
