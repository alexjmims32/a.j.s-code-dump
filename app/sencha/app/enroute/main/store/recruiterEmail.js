Ext.define('mobEdu.enroute.main.store.recruiterEmail', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.recruiterEmail'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.recruiterEmail',

        autoLoad: false,
        pageSize: 20,

        model: 'mobEdu.enroute.main.model.recruiterEmail',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'messages'
            }
        }
    }
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= 'messages'
    //        return proxy;
    //    }
});