Ext.define('mobEdu.enroute.main.store.appUserList', {
    //    extend:'Ext.data.Store',
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.enroute.main.model.allLeadsList'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.appUserList',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.allLeadsList'

    },

    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'rows'
        return proxy;
    }
});