Ext.define('mobEdu.enroute.main.store.menu', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enroute.main.model.menu'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.menu',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.menu',
        proxy:{
            type:'localstorage',
            id:'enroutemenu'
        }
    }
});