Ext.define('mobEdu.enroute.main.store.enumerations', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.enumerations',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.enumerations',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enumerations'
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});