Ext.define('mobEdu.enroute.main.store.leadAppExist', {
    extend: 'Ext.data.Store',
    //    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.leadAppExist'
    ],

    config: {
        storeId: 'mobEdu.enroute.main.store.leadAppExist',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.leadAppExist',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json'
                //                rootProperty: ''
            }
        }
    }
    //    ,
    //    initProxy: function() {
    //        var proxy = this.callParent();
    //        proxy.reader.rootProperty= ''
    //        return proxy;
    //    }
});