Ext.define('mobEdu.enroute.main.store.leadProfileUpdate', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.assignLead'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.leadProfileUpdate',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.assignLead',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});