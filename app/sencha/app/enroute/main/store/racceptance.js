Ext.define('mobEdu.enroute.main.store.racceptance', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.enroute.main.model.appointmentsList'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.racceptance',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.appointmentsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});