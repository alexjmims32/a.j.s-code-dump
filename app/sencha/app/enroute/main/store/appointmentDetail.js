Ext.define('mobEdu.enroute.main.store.appointmentDetail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.appointmentsList'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.appointmentDetail',
        autoLoad: false,
        model: 'mobEdu.enroute.main.model.appointmentsList',

//        proxy:{
//            type:'localstorage',
//            id:'appointmentDetail'
//        }
        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }

    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});
