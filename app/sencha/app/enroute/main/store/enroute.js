Ext.define('mobEdu.enroute.main.store.enroute', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enroute.main.model.enroute'
    ],

    config:{
        storeId: 'mobEdu.enroute.main.store.enroute',
        autoLoad: true,
        model: 'mobEdu.enroute.main.model.enroute',
        proxy:{
            type:'localstorage',
            id:'enrouteStorage'
        }
    }
});