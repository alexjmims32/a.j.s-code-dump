Ext.define('mobEdu.enroute.main.store.respondEmail', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enroute.main.model.recruiterEmail'
    ],
    config: {
        storeId: 'mobEdu.enroute.main.store.respondEmail',

        autoLoad: false,

        model: 'mobEdu.enroute.main.model.recruiterEmail',

              proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: ''
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});