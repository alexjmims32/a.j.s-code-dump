Ext.define('mobEdu.enroute.main.view.leadAssignPopup', {
    extend:'Ext.Panel',
    requires: [
         'mobEdu.enroute.main.f',
         'mobEdu.enroute.main.store.recruiterSearch'
    ],
    config:{
        id:'leadsPopup',
        scroll:'vertical',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'60%',
        height:'250',
        layout:'fit',
        items:[
            {
                xtype:'textfield',
                docked:'top',
                id:'searchReq',
                name:'searchReq',
                placeHolder:'Search Recruiter',
//                flex:1,
                listeners:{
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enroute.main.f.recruiterSearchList();
                    },
                    clearicontap: function(textfield, e, eOpts) {
                        mobEdu.enroute.main.f.assignedLeadList();
                    }
                }

            },
            {
                xtype:'list',
                docked:'bottom',
                height:350,
                name:'searchResultList',
                id:'searchResultList',
                itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch'),
                singleSelect:true,
                loadingText:'',
                listeners:{
                    itemtap:function (view, index, item, e) {
                        mobEdu.util.deselectSelectedItem(index,view);
                        mobEdu.enroute.main.f.showReqProfile(index,e);
                    }
                },
                flex:4
            }
        ],
        flex:1
    }

});
