Ext.define('mobEdu.enroute.main.view.updateReqAppointment', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.main.store.enumerations'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		xtype: 'toolbar',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Appointment Update</h1>'
		}, {
			xtype: 'fieldset',
			items: [{
				xtype: 'textfield',
				label: 'Subject',
				labelWidth: '40%',
				name: 'requpappsub',
				id: 'requpappsub',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'datepickerfield',
				name: 'reqappdate',
				id: 'reqappdate',
				labelWidth: '40%',
				label: 'Date',
				required: true,
				useClearIcon: true,
				value: new Date(),
				picker: {
					yearFrom: new Date().getFullYear(),
					yearTo: new Date().getFullYear() + 1
					//                            slotOrder:['day', 'month', 'year']
				},
				listeners: {
					change: function(datefield, e, eOpts) {
						mobEdu.enroute.main.f.onAppDateKeyup(datefield)
					}
				}
			}, {
				xtype: 'spinnerfield',
				label: 'Hours',
				labelWidth: '40%',
				id: 'reqUpTimeHours',
				name: 'reqUpTimeHours',
				required: true,
				useClearIcon: true,
				minValue: 8,
				maxValue: 18,
				stepValue: 1,
				increment: 1,
				cycle: true
			}, {
				xtype: 'spinnerfield',
				label: 'Minutes',
				labelWidth: '40%',
				id: 'reqUpTimeMinutes',
				name: 'reqUpTimeMinutes',
				//                        required:true,
				useClearIcon: true,
				minValue: 0,
				maxValue: 59,
				stepValue: 5,
				increment: 5,
				cycle: true
			}, {
				xtype: 'textfield',
				label: 'Location',
				labelWidth: '40%',
				name: 'requpapploc',
				id: 'requpapploc',
				required: true,
				useClearIcon: true
			}, {
				xtype: 'textareafield',
				label: 'Description',
				labelWidth: '40%',
				labelAlign: 'top',
				name: 'requpappdes',
				id: 'requpappdes',
				required: true,
				useClearIcon: true,
				maxRows: 10
			}]
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Send',
				align: 'right',
				handler: function() {
					mobEdu.enroute.main.f.updateReqAppointmentSend();
				}
			}]
		}]
	}
});