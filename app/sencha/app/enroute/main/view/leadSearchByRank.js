Ext.define('mobEdu.enroute.main.view.leadSearchByRank', {
	extend: 'Ext.Panel',
	statics: {
		clearIconTapped: false
	},
	config: {
		id: 'rankPopup',
		scrollable: 'none',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '70%',
		height: '50%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'textfield',
			name: 'rank',
			id: 'rank',
			placeHolder: 'Enter Rank',
			docked: 'top',
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.onRankKeyup(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.onRankItemClear(textfield, e);
				},
				blur: function(textfield, e, eOpts) {
					mobEdu.util.hideKeyboard();
				}
			}
		}, {
			xtype: 'list',
			id: 'rankList',
			emptyText: 'No results found.',
			name: 'rankList',			
			itemTpl: '<h3>{actScore}</h3>',
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.enroute.main.f.onRankItemTap(view, index, target, record, e, eOpts);
				}
			}
		}],
		flex: 1,
		listeners: {
			hide: function() {
				mobEdu.enroute.main.f.hideSchoolPopup();
			}
		}
	}
});