Ext.define('mobEdu.enroute.main.view.supLeadDetail', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.main.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [{
                        text: 'Update',
                        handler: function() {
                            mobEdu.enroute.leads.f.ProfileUpdate();
                        }
                    }, {
                        text: 'View',
                        id: 'unassignedViewApp',
                        name: 'unassignedViewApp',
                        handler: function() {
                            mobEdu.enroute.main.f.loadUnAssignedLeadApplication();
                        }
                    }, {
                        xtype: 'spacer'
                    }, {
                        text: 'Assign Recruiter',
                        //                        ui:'confirm',
                        handler: function() {
                            mobEdu.enroute.main.f.showReqSerach();

                        }
                    }

                ]
            }, {
                xtype: 'customToolbar',
                title: '<h1>Lead Details</h1>'
            }, {
                xtype: 'panel',
                name: 'supLeadDetailSummary',
                id: 'supLeadDetailSummary'
            }

        ]
    }

});