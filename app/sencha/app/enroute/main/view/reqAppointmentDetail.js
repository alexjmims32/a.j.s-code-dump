Ext.define('mobEdu.enroute.main.view.reqAppointmentDetail', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enroute.main.f',
        'mobEdu.enroute.main.store.appointmentDetail'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Date:</h2></td><td><h3 align="left">{appointmentDate}</h3></td></tr>' +
                '<tr><td  align="right" width="50%"><h2>Location:</h2></td><td align="left"><h3>{location}</h3></td></tr>' +
                '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{[mobEdu.util.getStatus(values.fromDetails,values.toIDs,values.loginUserID)]}</h3></td></tr>' +
                '<tr><td valign="top"  align="right" width="50%"><h2>Description:</h2></td><td align="left"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
                //                    '</table></td></tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentDetail'),
            singleSelect: true,
            loadingText: ''
        }, {
            title: '<h1>Appointment Details</h1>',
            xtype: 'customToolbar'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                xtype: 'spacer'
            }, {
                text: 'Accept',
                id: 'raccept',
                handler: function() {
                    mobEdu.enroute.main.f.acceptAppointment('Accept');
                }
            }, {
                text: 'Decline',
                id: 'rdecline',
                handler: function() {
                    mobEdu.enroute.main.f.acceptAppointment('Decline');
                }
            }, {
                text: 'Update Appointment',
                id: 'updateApp',
                handler: function() {
                    mobEdu.enroute.main.f.loadReqUpdateAppointment();
                }
            }]
        }],
        flex: 1
    }

});