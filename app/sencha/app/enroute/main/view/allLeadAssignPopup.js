Ext.define('mobEdu.enroute.main.view.allLeadAssignPopup', {
    extend:'Ext.Panel',
    requires: [
         'mobEdu.enroute.main.store.recruiterSearch',
         'mobEdu.enroute.main.f'
    ],
    config:{
        id:'allLeadsPopup',
        scroll:'vertical',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'60%',
        height:'250',
        layout:'fit',
        items:[
            {
                xtype:'textfield',
                docked:'top',
                id:'searchReq',
                name:'searchReq',
                placeHolder:'Search Recruiter',
//                flex:1,
                listeners:{
                    keyup: function( textfield, e, eOpts ) {
                        mobEdu.enroute.main.f.allLeadsReqSearch();
                    },
                    clearicontap: function(textfield, e, eOpts) {
                        mobEdu.enroute.main.f.allLeadsAssignedLeadList();
                    }
                }

            },
            {
                xtype:'list',
                docked:'bottom',
                height:350,
                name:'allSearchResultList',
                id:'allSearchResultList',
                itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch'),
                singleSelect:true,
                loadingText:'',
                listeners:{
                    itemtap:function (view, index, item, e) {
                        mobEdu.util.deselectSelectedItem(index,view);
                        mobEdu.enroute.main.f.showAllLeadsReqProfile(index, e);
                    }
                },
                flex:1
            }
        ],
        flex:1
    }

});
