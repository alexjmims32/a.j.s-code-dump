Ext.define('mobEdu.enroute.main.view.recruiterAppointmentCalendar', {
    extend:'Ext.Panel',
    alias:'recruiterAppointmentCalendar',
    config:{
        fullscreen:'true',
        scrollable:true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
             id:'appointmentsDock',
                name:'appointmentsDock',
            items: [{}]
        }, {
            xtype: 'label',
            id: 'reqAppointmentLabel',
            padding: 10
        }]
    }
});
