Ext.define('mobEdu.enroute.main.view.leadSearchMenu', {
	extend: 'Ext.Panel',

	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.main.store.leadSearch',
		'mobEdu.enroute.main.store.leadSearchList'
	],

	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>Search Lead</h1>'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			items: [{
				xtype: 'selectfield',
				name: 'leadSrchMenu',
				id: 'leadSrchMenu',
				valueField: 'title',
				displayField: 'title',
				store: mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearch'),
				listeners: {
					change: function(selectbox, newValue, oldValue, eOpts) {
						mobEdu.enroute.main.f.searchLeadOnMenutap(newValue);
						Ext.getCmp('leadStList').setValue('');
						Ext.getCmp('leadScList').setValue('');
						Ext.getCmp('leadRkList').setValue('');
					}
				}
			}, {
				xtype: 'searchfield',
				labelWidth: '50%',
				name: 'leadStList',
				id: 'leadStList',
				label: 'State',
				placeHolder: 'Enter State',
				clearIcon: true,
				hidden: true,
				listeners: {
					focus: function() {
						mobEdu.enroute.main.f.loadLeadStatePopup();
					},
					clearicontap: function(textfield, e, eOpts) {
						// In Android, clearing the field causes focus event to be raised
						// So use a flag and do not show the popup, immediately after the clear icon is tapped
						if (Ext.os.is.Android) {
							mobEdu.enroute.main.view.leadSearchByState.clearIconTapped = true;
						}
					},
				}
			}, {
				xtype: 'searchfield',
				labelWidth: '50%',
				name: 'leadScList',
				id: 'leadScList',
				label: 'School',
				placeHolder: 'Enter School',
				clearIcon: true,
				hidden: true,
				listeners: {
					focus: function() {
						mobEdu.enroute.main.f.loadLeadSchoolPopup();
					},
					clearicontap: function(textfield, e, eOpts) {
						// In Android, clearing the field causes focus event to be raised
						// So use a flag and do not show the popup, immediately after the clear icon is tapped
						if (Ext.os.is.Android) {
							mobEdu.enroute.main.view.leadSearchBySchool.clearIconTapped = true;
						}
					},
				}
			}, {
				xtype: 'searchfield',
				labelWidth: '50%',
				name: 'leadRkList',
				id: 'leadRkList',
				label: 'Rank',
				placeHolder: 'Enter Rank',
				clearIcon: true,
				hidden: true,
				listeners: {
					focus: function() {
						mobEdu.enroute.main.f.loadLeadRankPopup();
					},
					clearicontap: function(textfield, e, eOpts) {
						// In Android, clearing the field causes focus event to be raised
						// So use a flag and do not show the popup, immediately after the clear icon is tapped
						if (Ext.os.is.Android) {
							mobEdu.enroute.main.view.leadSearchByRank.clearIconTapped = true;
						}
					},
				}
			},]
		}, {
			xtype: 'list',
			id: 'leadsSearchList',
			name: 'leadsSearchList',
			emptyText: '<h3 align="center">No Leads</h3>',
			itemTpl: '<table  width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr>' +
				'<tr><h7><td width="30%" valign="top">{status}</td></h7><h4><td width="70%" align="right">{versionDate}</td></h4></tr>' +
				'<tr><h4><td width="40%" align="left">{phone1}</td></h4><h7><td width="60%" align="right">{email}</td></h7></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enroute.main.f.showViewProfile(index, view, record, item, 'SEARCHLEAD');
				}
			}
		}],
		flex: 1
	},
	initialize: function() {
		mobEdu.enroute.main.f.loadLeadSearchReportsMenu();
	}
});