Ext.define('mobEdu.enroute.main.view.allLeadDetail', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.main.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
                xtype: 'customToolbar',
                title: '<h1>Lead Details</h1>'
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [{
                    text: 'Update',
                    handler: function() {
                        mobEdu.enroute.main.f.allLeadsLeadProfileUpdate();
                    }
                }, {
                    text: 'View',
                    id: 'leadViewApp',
                    name: 'leadViewApp',
                    handler: function() {
                        mobEdu.enroute.main.f.getLeadApplication();
                    }
                }, {
                    xtype: 'spacer'
                }, {
                    text: 'Assign Recruiter',
                    //                        ui:'confirm',
                    id: 'assign',
                    name: 'assign',
                    handler: function() {
                        mobEdu.enroute.main.f.loadAllLeadsReqSearch();

                    }
                }, {
                    text: 'Un-assign Recruiter',
                    //                        ui:'confirm',
                    id: 'unAssign',
                    name: 'unAssign',
                    handler: function() {
                        mobEdu.enroute.main.f.appointmentsExist();

                    }
                }]
            }, {
                xtype: 'panel',
                name: 'allLeadsDetailSummary',
                id: 'allLeadsDetailSummary'
            }

        ]
    }

});