Ext.define('mobEdu.enroute.main.view.viewProfileList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enroute.main.f',
        'mobEdu.enroute.leads.store.viewProfileList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            cls: 'logo',
            name: 'viewProfileList',
            itemTpl: '<table class="menu" width="100%"><tr><td align="left">{title}</td><td width="2%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.enroute.leads.store.viewProfileList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }, {

            xtype: 'customToolbar',
            id: 'viewSupProfTool',
            name: 'viewSupProfTool'
        }],
        flex: 1
    },
    initialize: function() {
        mobEdu.enroute.main.f.initializeSupLeadProfile();
    }

});