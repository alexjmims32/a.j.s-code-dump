Ext.define('mobEdu.enroute.main.view.leadSearchByState', {
	extend: 'Ext.Panel',
	statics: {
		clearIconTapped: false
	},
	config: {
		id: 'statePopup',
		scrollable: 'none',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '70%',
		height: '50%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'textfield',
			name: 'stateDesc',
			id: 'stateDesc',
			placeHolder: 'Enter State',
			docked: 'top',
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.onStateKeyup(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.onStateItemClear(textfield, e);
				},
				blur: function(textfield, e, eOpts) {
					mobEdu.util.hideKeyboard();
				}
			}
		}, {
			xtype: 'list',
			id: 'stateList',
			name: 'stateList',
			emptyText: '<h1 align=center>No results found.</h1>',
			cls: 'logo subjectlist',
			itemTpl: '<h3>{displayValue}</h3>',
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.stateEnumerations'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.enroute.main.f.onStateItemTap(view, index, target, record, e, eOpts);
				}
			}
		}],
		flex: 1,
		listeners: {
			hide: function() {
				mobEdu.enroute.main.f.hideStatePopup();
			}
		}
	}
});