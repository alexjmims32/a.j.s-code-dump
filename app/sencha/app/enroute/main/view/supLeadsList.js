Ext.define('mobEdu.enroute.main.view.supLeadsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.main.store.supLeadsList'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			name: 'leadsList',
			id: 'leadsList',
			title: '<h1>Unassigned Leads</h1>'

		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset',
			items: [{
				xtype: 'textfield',
				name: 'sUALeads',
				id: 'sUALeads',
				width: '100%',
				placeHolder: 'Search lead',
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.enroute.main.f.searchUnassignedLeads();
					},
					clearicontap: function(textfield, e, eOpts) {
						mobEdu.enroute.main.f.loadSupLeadsList();
					}
				}
			}]
		}, {
			xtype: 'list',
			id: 'supLeadsList',
			name: 'supLeadsList',
			emptyText: '<h3 align="center">No Leads</h3>',
			itemTpl: '<table  width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr>' +
				'<tr><h7><td width="30%" valign="top">{status}</td></h7><h4><td width="70%" align="right">{versionDate}</td></h4></tr>' +
				'<tr><h4><td width="40%" align="left">{phone1}</td></h4><h7><td width="60%" align="right">{email}</td></h7></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enroute.main.f.showViewProfile(index, view, record, item, 'UNLEAD');
				}
			}
		}],
		flex: 1
	}
});