Ext.define('mobEdu.enroute.main.view.allLeadsList', {
	extend: 'Ext.Panel',
	xtype: 'allLeadsList',
	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.main.store.supLeadsList'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			title: '<h1>All Leads</h1>'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset',
			items: [{
				xtype: 'textfield',
				name: 'sALItem',
				id: 'sALItem',
				width: '100%',
				placeHolder: 'Search lead',
				listeners: {
					keyup: function(textfield, e, eOpts) {
						mobEdu.enroute.main.f.loadAllLeads();
					},
					clearicontap: function(textfield, e, eOpts) {
						mobEdu.enroute.main.f.loadAllLeads();
					}
				}
			}]
		}, {
			xtype: 'list',
			id: 'allLeadList',
			name: 'allLeadList',
			emptyText: '<h3 align="center">No Leads</h3>',
			//                plugins: [{
			//                    type: 'listpaging',
			//                    autoPaging: true,
			//                    loadMoreText: 'Load More...',
			//                    noMoreRecordsText: ''
			//                }],		
			// itemTpl: '<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr>' + '<tr><td width="30%" valign="top" style="color: blue;"><h5>{status}</h5></td><td width="70%" align="right"><h5>{versionDate}</h5></td></tr>' + '<tr><td width="40%" align="left"><h5>{phone1}</h5></td><td width="60%" align="right"><h5>{email}</h5></td></tr></table>',
			itemTpl: '<table  width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr>' +
				'<tr><h7><td width="30%" valign="top">{status}</td></h7><h4><td width="70%" align="right">{versionDate}</td></h4></tr>' +
				'<tr><h4><td width="40%" align="left">{phone1}</td></h4><h7><td width="60%" align="right">{email}</td></h7></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enroute.main.f.showAllLeadDetailProfile(index, view, record, item);
				}
			}
		}],
		flex: 1
	}
});