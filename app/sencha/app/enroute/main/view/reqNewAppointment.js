Ext.define('mobEdu.enroute.main.view.reqNewAppointment', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.main.f',
        'mobEdu.enroute.main.store.appUserList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>New Appointment</h1>'
        }, {
            xtype: 'fieldset',
            items: [{
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    label: 'To',
                    labelWidth: '25%',
                    id: 'rTo',
                    name: 'rTo',
                    width: '85%',
                    required: true,
                    readOnly: true
                }, {
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    name: 'searchHelp',
                    id: 'search',
                    text: '<a href="javascript:mobEdu.enroute.main.f.showSearchAppToIdsPopup();"><img width="30px" height="30px" src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png"></a>',
                    style: {
                        border: 'none',
                        background: 'none'
                    }
                }]
            }, {
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '40%',
                name: 'reqappsub',
                id: 'reqappsub',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'datepickerfield',
                name: 'reqappdate',
                id: 'reqappdate',
                labelAlign: 'top',
                labelWidth: '40%',
                label: 'Appointment Date',
                required: true,
                useClearIcon: true,
                value: new Date(),
                picker: {
                    yearFrom: new Date().getFullYear(),
                    yearTo: new Date().getFullYear() + 1
                    //                            slotOrder:['day', 'month', 'year']
                },
                listeners: {
                    change: function(datefield, e, eOpts) {
                        mobEdu.enroute.main.f.onReqAppDateKeyup(datefield)
                    }
                }
            }, {
                xtype: 'textfield',
                labelWidth: '100%',
                label: 'Appointment Time',
                required: true,
                useClearIcon: true

            }, {
                xtype: 'spinnerfield',
                label: 'Hours',
                labelWidth: '40%',
                id: 'reqappTimeH',
                name: 'reqappTimeH',
                required: true,
                useClearIcon: true,
                minValue: 8,
                maxValue: 18,
                stepValue: 1,
                increment: 1,
                cycle: true
            }, {
                xtype: 'spinnerfield',
                label: 'Minutes',
                labelWidth: '40%',
                id: 'reqappTimeM',
                name: 'reqappTimeM',
                //                        required:true,
                useClearIcon: true,
                minValue: 0,
                maxValue: 59,
                stepValue: 5,
                increment: 5,
                cycle: true
            }, {
                xtype: 'textfield',
                label: 'Location',
                labelWidth: '40%',
                name: 'reqapploc',
                id: 'reqapploc',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textareafield',
                label: 'Description',
                labelWidth: '40%',
                labelAlign: 'top',
                name: 'reqappdes',
                id: 'reqappdes',
                required: true,
                useClearIcon: true,
                maxRows: 15
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Save',
                //                        ui:'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enroute.main.f.sendReqNewAppointment();
                }
            }]
        }]
    }
});