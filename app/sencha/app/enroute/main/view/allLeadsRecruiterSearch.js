Ext.define('mobEdu.enroute.main.view.allLeadsRecruiterSearch', {
    extend:'Ext.Panel',
    xtype: 'recruiterSearchList',
    requires: [
         'mobEdu.enroute.main.f',
         'mobEdu.enroute.main.store.recruiterSearch'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        layout:'fit',
        cls:'logo',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Recruiter Search</h1>'
            },
            {
                xtype:'fieldset',
                docked:'top',
                cls:'searchfieldset',
//                margin: 10,
                items:[
                    {
                        xtype:'textfield',
                        name:'searchReq',
                        id:'searchReq',
                        width:'100%',
//                        placeHolder:'Please enter value to search',
                        placeHolder:'Search by Recruiter',
                        listeners:{
                            keyup: function( textfield, e, eOpts ) {
                                mobEdu.enroute.main.f.recruiterSearchList();
                            },
                            clearicontap: function(textfield, e, eOpts) {
                                mobEdu.enroute.main.f.allLeadsRecruiterSearchList();
                            }
                        }
                    }
                ]
            },
            {
                xtype:'list',
                id:'profileList',
                itemTpl:'<table class="menu" width="100%"><tr><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
                store: mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch'),
                singleSelect:true,
                loadingText: '',
                listeners:{
                    itemtap:function (view, index, item, e) {
                        mobEdu.util.deselectSelectedItem(index,view);
                        mobEdu.enroute.main.f.assignRecruiter(index,e);
                    }
                }
            }
        ],
        flex:1
    }
});