Ext.define('mobEdu.enroute.main.view.respondEmail', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.main.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id:'resEmailTitle'
        },  {
            xtype: 'fieldset',
            items: [{
                layout: 'hbox',
                items: [{
                    xtype: 'textfield',
                    label: 'To',
                    labelWidth: '25%',
                    id: 'rToName',
                    name: 'rToName',
                    width: '85%',
                    required: true,
                    readOnly: true
                }, {
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    name: 'searchHelp',
                    id: 'searchHelp',
                    text: '<a href="javascript:mobEdu.enroute.main.f.showSearchToIdsPopup();"><img width="30px" height="30px" src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png"></a>',
                    style: {
                        border: 'none',
                        background: 'none'
                    }
                }]
            }, {
                xtype: 'textfield',
                label: 'Subject',
                labelWidth: '21%',
                name: 'rsub',
                id: 'rsub',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textareafield',
                label: 'Message',
                labelWidth: '30%',
                labelAlign: 'top',
                name: 'rmsg',
                id: 'rmsg',
                maxRows: 12,
                required: true
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Send',
                //                        ui:'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enroute.main.f.sentUpdateEmail();
                }
            }]
        }]
    }
});