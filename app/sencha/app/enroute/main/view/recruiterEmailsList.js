Ext.define('mobEdu.enroute.main.view.recruiterEmailsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.main.store.recruiterEmail'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',

		items: [{
			xtype: 'list',
			id: 'recruiterEmailsList',
			name: 'recruiterEmailsList',
			// scrollToTopOnRefresh: true,
			emptyText: '<h3 align="center">No Messages</h3>',
			cls: 'logo',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>',
				'<tpl if="(mobEdu.enroute.main.f.hasReqEmailDirection(labels)===true)">',
				'<td width="5%"><img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
				'<tpl else>',
				'<td width="5%"><img height="32px" width="32px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
				'</tpl>',
				'<tpl if="(mobEdu.enroute.main.f.hasRReadFlag(messageStatus)===true)">',
				'<td><h3>{subject}</h3></td>' +
				'<td align="right"><h5>{versionDate}</h5><br/><h5>{toName}</h5></td>' +
				'<tpl else>',
				'<td style="color: #030303;"><h2>{subject}</h2></td>' +
				'<td style="color: #030303;" align="right"><h5>{versionDate}</h5><br/><h5>{toName}</h5></td>' +
				'</tpl>',
				'</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					mobEdu.enroute.main.f.loadReqEmailDetail(record);
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>Messages</h1>'

		}, {
			xtype: 'toolbar',
			docked: 'bottom',

			layout: {
				pack: 'right'
			},
			items: [{
				text: 'New Message',
				handler: function() {
					mobEdu.enroute.main.f.showNewMessage();
				}
			}]
		}],
		flex: 1
	},
	initialize: function() {
		var searchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail');
		searchStore.addBeforeListener('load', mobEdu.enroute.main.f.checkForSearchListEnd, this);
	}

});