Ext.define('mobEdu.enroute.main.view.leadSearchBySchool', {
	extend: 'Ext.Panel',
	statics: {
		clearIconTapped: false
	},
	config: {
		id: 'schoolPopup',
		scrollable: 'none',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '70%',
		height: '50%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'textfield',
			name: 'schoolDesc',
			id: 'schoolDesc',
			placeHolder: 'Enter School',
			docked: 'top',
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.onSchoolKeyup(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.onSchoolItemClear(textfield, e);
				},
				blur: function(textfield, e, eOpts) {
					mobEdu.util.hideKeyboard();
				}
			}
		}, {
			xtype: 'list',
			id: 'schoolList',
			emptyText: 'No results found.',
			name: 'stateList',
			cls: 'logo subjectlist',
			itemTpl: '<h3>{highSchool}</h3>',
			store: mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.enroute.main.f.onSchoolItemTap(view, index, target, record, e, eOpts);
				}
			}
		}],
		flex: 1,
		listeners: {
			hide: function() {
				mobEdu.enroute.main.f.hideSchoolPopup();
			}
		}
	}
});