Ext.define('mobEdu.enroute.main.view.reqAppointmentsList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enroute.main.f',
        'mobEdu.enroute.main.store.appointmentsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'appointmentList',
            name: 'appointmentList',
            emptyText: '<h3 align="center">No Appointments</h3>',
            cls: 'logo',
            itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                '<tr>',
                '<td ><h3>{subject}</h3></td>' + '<td align="right"><h5>{appointmentDate}</h5></td>' + '</tr>' + '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    mobEdu.enroute.main.f.showReqAppointmentDetail(record);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>My Appointments</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'New Appointment',
                //                        ui:'confirm',
                handler: function() {
                    mobEdu.enroute.main.f.showReqNewAppointment();
                }
            }, {
                text: 'Calendar',
                id: 'reqAppCal',
                handler: function() {
                    mobEdu.enroute.main.f.showCalendar();
                }
            }]
        }],
        flex: 1
    }
});