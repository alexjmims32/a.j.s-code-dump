Ext.define('mobEdu.enroute.main.view.searchPopup', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.main.store.userList',
		'mobEdu.enroute.leads.store.profileSearch',
		'mobEdu.enroute.leads.f'
	],
	config: {
		id: 'searchPopup',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '80%',
		height: '70%',
		layout: 'fit',
		items: [{
			xtype: 'textfield',
			id: 'searchRecId',
			name: 'searchRecId',
			docked: 'top',
			placeHolder: 'Search',
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.doCheckBeforeSearch(textfield);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.clearRecSearchStore();
				}
			}
		}, {
			xtype: 'list',
			name: 'selectAll',
			scrollable: {
				direction: 'vertical',
				directionLock: true
			},
			id: 'selectAll',
			emptyText: 'No search results',
			itemTpl: '<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					if (!mobEdu.util.getRole().match(/SUPERVISOR/gi)) {
						mobEdu.enroute.main.f.onSearchToIdItemTap(index, view, record, item);
					} else {
						mobEdu.enroute.main.f.onSearchUserToIdItemTap(index, view, record, item);
					}
				}
			}
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Add',
				handler: function() {
					if (!mobEdu.util.getRole().match(/SUPERVISOR/gi)) {
						mobEdu.enroute.main.f.selectedToNames();
					} else {
						mobEdu.enroute.main.f.selectedUserToNames();
					}
				}
			}]
		}],
		flex: 1
	}

});