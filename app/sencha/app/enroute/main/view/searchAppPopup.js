Ext.define('mobEdu.enroute.main.view.searchAppPopup', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enroute.main.f',
		'mobEdu.enroute.leads.store.profileSearch'
	],
	config: {
		id: 'searchAppPopup',
		scroll: 'vertical',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '80%',
		height: '70%',
		layout: 'fit',
		items: [{
			xtype: 'textfield',
			docked: 'top',
			id: 'searchAppToIds',
			name: 'searchAppToIds',
			placeHolder: 'Search',
			//                flex:1,
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.doCheckSearch(textfield);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enroute.main.f.clearSearchStore();
				}
			}

		}, {
			xtype: 'list',
			name: 'toAppSearchList',
			id: 'toAppSearchList',
			emptyText: 'No search results',
			itemTpl: '<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					//var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
					//store.removeAll();
					//store.sync();
					//mobEdu.enroute.main.f.selectedAppToNames(view, index, target, record, item, e, eOpts);
					mobEdu.enroute.main.f.onAppSearchItemTap(index, view, record, item);
				}
			},
			flex: 4
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Add',
				handler: function() {
					if (!mobEdu.util.getRole().match(/SUPERVISOR/gi)) {
						mobEdu.enroute.main.f.selectedAppToNames();
					} else {
						mobEdu.enroute.main.f.selectedAppToNames();
					}
				}
			}]
		}],

		flex: 1
	}

});