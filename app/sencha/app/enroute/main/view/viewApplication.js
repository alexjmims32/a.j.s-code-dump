Ext.define('mobEdu.enroute.main.view.viewApplication', {
    extend:'Ext.form.FormPanel',
    requires: [
         'mobEdu.enroute.main.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>View Application</h1>'
            },            
            {
                xtype:'panel',
                name:'leadAppView',
                id:'leadAppView'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        xtype:'spacer'
                    },
                    {
                        text:'Push To Banner',
                        id:'leadPushApp',
                        name:'leadPushApp',
                        handler:function () {
                            mobEdu.enroute.main.f.loadPushBannerApp();
                        }
                    }
                ]
            }

        ]
    }

});
