Ext.define('mobEdu.enroute.main.view.reqNewEmail', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enroute.main.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        xtype: 'toolbar',
        cls: 'logo',
        items: [{
                xtype: 'customToolbar',
                title: '<h1>New Message</h1>'
            },

            {
                xtype: 'fieldset',
                items: [{
                    xtype: 'textfield',
                    label: 'To',
                    id: 'toValue',
                    name: 'toValue',
                    required: true,
                    readOnly: true
                }, {
                    xtype: 'textfield',
                    label: 'Subject',
                    labelWidth: '30%',
                    name: 'reqNewSub',
                    id: 'reqNewSub',
                    required: true,
                    useClearIcon: true
                }, {
                    xtype: 'textareafield',
                    label: 'Message',
                    labelWidth: '30%',
                    labelAlign: 'top',
                    name: 'reqNewMsg',
                    id: 'reqNewMsg',
                    maxRows: 15,
                    required: true
                }]
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                //                style:'font-size:12pt',
                layout: {
                    pack: 'right'
                },
                items: [{
                    text: 'Send',
                    //                        ui:'confirm',
                    align: 'right',
                    handler: function() {
                        mobEdu.enroute.main.f.sendNewMail();
                    }
                }]
            }
        ]
    }
});