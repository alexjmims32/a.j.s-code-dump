Ext.define('mobEdu.enroute.main.model.enroute', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'recruiterId'
            },
            {
                name:'authString'
            },
            {
                name:'role'
            }
        ]
    }
});