Ext.define('mobEdu.enroute.main.model.reqNewAppointment', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'appointmentID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});