Ext.define('mobEdu.enroute.main.model.leadAppExist', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
                name: 'status',
                type: 'string'
            }, {
                name: 'leadID',
                type: 'string'
            }
            //            {
            //                name:'createdDate',
            //                type:'string'
            //            }
        ]
    }
});