Ext.define('mobEdu.enroute.main.model.respondEmail', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'emailID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            }
        ]
    }
});
