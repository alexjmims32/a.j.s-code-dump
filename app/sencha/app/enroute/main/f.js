Ext.define('mobEdu.enroute.main.f', {
	requires: [
		'mobEdu.enroute.main.store.menu',
		'mobEdu.enroute.main.store.supLeadsList',
		'mobEdu.enroute.main.store.supLeadDetail',
		'mobEdu.enroute.main.store.leadProfileUpdate',
		// 'mobEdu.enroute.main.store.allLeadDetail',
		'mobEdu.enroute.main.store.enumerations',
		'mobEdu.enroute.main.store.recruiterSearch',
		'mobEdu.enroute.main.store.assignLead',
		// 'mobEdu.enroute.main.store.allLeadsList',
		'mobEdu.enroute.main.store.unAssignLead',
		'mobEdu.enroute.main.store.appointmentsList',
		'mobEdu.enroute.main.store.appointmentDetail',
		'mobEdu.enroute.main.store.reqNewAppointment',
		'mobEdu.enroute.main.store.emailDetail',
		'mobEdu.enroute.main.store.recruiterEmail',
		'mobEdu.enroute.main.store.respondEmail',
		'mobEdu.enroute.main.store.enroute',
		'mobEdu.main.store.entourage',
		'mobEdu.reg.store.pin',
		'mobEdu.enroute.leads.store.viewApplication',
		'mobEdu.enroute.leads.store.newAppointment',
		'mobEdu.enroute.main.store.toStore',
		'mobEdu.enroute.main.store.userList',
		'mobEdu.enroute.main.store.appUserList',
		'mobEdu.enroute.main.store.toAppStore',
		'mobEdu.enroute.main.store.racceptance',
		'mobEdu.enroute.main.store.leadAppExist',
		'mobEdu.enroute.leads.store.profileSearch',
		'mobEdu.enroute.main.store.leadSearch',
		'mobEdu.enroute.main.store.leadSearchList',
		'mobEdu.enroute.main.store.stateEnumerations',
		'mobEdu.enroute.main.store.leadSchoolList',
		'mobEdu.enroute.leads.store.viewProfileList',
		'mobEdu.enroute.leads.store.profileDetail',
		'mobEdu.enroute.leads.store.profileList'
	],
	statics: {
		prevViewRef: null,
		update: false,
		leadUpdateFlag: false,
		leadSearchType: null,
		supLeadFlag: false,
		allLeadFlag: false,
		targetFlag: false,
		role: null,
		count: 0,
		showEnrouteMainview: function() {
			var title = mobEdu.util.getTitleInfo('ENROUTE');
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enroute.main.view.menu');
			Ext.getCmp('enrouteTitle').setTitle('<h1>' + title + '</h1>');
			Ext.getCmp('modules').refresh();
		},

		initializeMainView: function() {

			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.menu');
			store.removeAll();
			store.removed = [];
			store.sync();

			//            mobEdu.enroute.main.f.getRecruiterId();
			//            mobEdu.enroute.main.f.getPrivileges();
			//            var role = mobEdu.enroute.main.f.getRole();
			if (mobEdu.enroute.main.f.privilegesChecking('UNLEADS')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/profile.png',
					title: '<h2>Unassigned Leads</h2>',
					action: mobEdu.enroute.main.f.loadSupLeadsList
				});
				store.sync();
			}

			if (mobEdu.enroute.main.f.privilegesChecking('ALLLEADS')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/profile.png',
					title: '<h2>All Leads</h2>',
					action: mobEdu.enroute.main.f.loadAllLeads
				});
				store.sync();
			}

			if (mobEdu.enroute.main.f.privilegesChecking('MYLEADS')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/profile.png',
					title: '<h2>My Leads</h2>',
					action: mobEdu.enroute.leads.f.loadProfileList
				});
				store.sync();
			}

			if (mobEdu.enroute.main.f.privilegesChecking('APPT')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/student-history.png',
					title: '<h2>Appointments</h2>',
					action: mobEdu.enroute.main.f.loadAppointmentsList
				});
				store.sync();
			}
			if (mobEdu.enroute.main.f.privilegesChecking('MESSAGES')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/student-mail.png',
					title: '<h2>Messages</h2>',
					action: mobEdu.enroute.main.f.loadMailsList
				});
				store.sync();
			}
			if (mobEdu.enroute.main.f.privilegesChecking('UNLEADREPORTS')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/reports.png',
					title: '<h2>Reports</h2>',
					action: mobEdu.enroute.main.f.showSearchMenu
				});
				store.sync();
			}
		},
		initializeSupLeadProfile: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewProfileList');
			//            store.load();
			store.removeAll();
			store.removed = [];
			//            store.getProxy().clear();
			store.sync();

			store.add({
				title: '<h2>Profile</h2>',
				action: mobEdu.enroute.leads.f.viewProfileDetail
			});
			store.sync();

			store.add({
				title: '<h2>Messages</h2>',
				action: mobEdu.enroute.leads.f.loadEmail
			});
			store.sync();

			// store.add({
			// 	title: '<h2>Appointments</h2>',
			// 	action: mobEdu.enroute.leads.f.loadAppointments
			// });
			store.sync();
		},

		supLeadViewProfileDetail: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var lId = data[0].data.leadID;

			var detailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');

			detailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			detailStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/get');
			detailStore.getProxy().setExtraParams({
				leadID: lId,
				companyId: companyId
			});

			detailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.supLeadViewProfileDetailResponseHandler();
			};
			detailStore.load();
		},
		supLeadViewProfileDetailResponseHandler: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var storeStatus = detailStore.getProxy().getReader().rawData;
			if (storeStatus.status == 'success') {
				mobEdu.enroute.main.f.showLeadDetail();
			} else {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},



		showViewProfile: function(selectedIndex, viewRef, record, item, type) {
			if (type != null) {
				mobEdu.enroute.main.f.leadSearchType = type;
			}
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			store.removeAll();
			store.add(record);
			// store.sync();

			mobEdu.enroute.main.f.loadViewProfile();
		},
		loadViewProfile: function() {
			var emailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.email');
			emailStore.currentPage = 1;
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var data = store.data.all;
			var fName = data[0].data.firstName;
			var lName = data[0].data.lastName;
			leadId = data[0].data.leadID;
			var lId = data[0].data.leadID;
			leadTitle = fName + ' ' + lName;
			if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsList);
			}
			mobEdu.util.show('mobEdu.enroute.main.view.viewProfileList');
			Ext.getCmp('viewSupProfTool').setTitle('<h1>' + leadTitle + '</h1>');
		},

		loadFromMyMessageModule: function() {
			mobEdu.enroute.main.f.loadMailsList('MSG');
		},

		loadFromCalendarModule: function() {
			mobEdu.enroute.main.f.loadAppointmentsList('CALENDAR');
		},

		privilegesChecking: function(priStaticName) {
			var loginStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
			var response = loginStore.data.all;
			for (var j = 0; j < response.length; j++) {
				var position = response[j].data.position;
				var moduledesCode = response[j].data.code;
				if (position == 'ENROUTE' && moduledesCode == priStaticName) {
					return true;
				}
			}
			return false;
		},

		getRecruiterId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var recruiterId = rec.data.recruiterId;
				return recruiterId;
			}
			return null;
		},
		getRole: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var role = rec.data.role;
				return role;
			}
			return null;
		},
		getCompanyId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var companyId = rec.data.companyId;
				return companyId;
			}
			return null;
		},

		getRoleId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var roleId = rec.data.roleId;
				return roleId;
			}
			return null;
		},

		getAuthString: function() {
			//            var enrouteStore=mobEdu.util.getStore('mobEdu.enroute.main.store.enroute');
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var authString = rec.data.authString;
				return authString;
			}
			return null;
		},

		getPrivileges: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var privileges = rec.data.privileges;
				return privileges;
			}
			return null;
		},

		loadSupLeadsList: function() {
			var searchItem = '';
			var reqId = 0;
			mobEdu.enroute.main.f.supLeadFlag = true;
			var leadsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');

			leadsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadsStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getUnAssignedLeads');
			leadsStore.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: reqId,
				limit: 500,
				companyId: companyId
			});

			leadsStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.supLeadsListResponseHandler();
			};
			leadsStore.load();
		},
		supLeadsListResponseHandler: function() {
			var leadsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			leadsStore.sort("versionDate", "DESC");
			var leadStatus = leadsStore.getProxy().getReader().rawData;
			if (leadStatus.status == 'success') {
				mobEdu.enroute.main.f.showSupLeadsList();
				mobEdu.enroute.main.f.leadupdateFlag = false;
			} else {
				Ext.Msg.show({
					message: leadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		searchUnassignedLeads: function() {
			var searchItem = Ext.getCmp('sUALeads').getValue();
			var reqId = 0;
			var leadsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');

			leadsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadsStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getUnAssignedLeads');
			leadsStore.getProxy().setExtraParams({
				searchText: searchItem,
				recruiterID: reqId,
				maxRecords: 100,
				companyId: companyId
			});
			//            recruiterID:mobEdu.enroute.main.f.getRecruiterId(),
			leadsStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.UnassignedLeadsResponseHandler();
			};
			leadsStore.load();
		},
		UnassignedLeadsResponseHandler: function() {
			var leadsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			leadsStore.sort("versionDate", "DESC");
			var leadStatus = leadsStore.getProxy().getReader().rawData;
			if (leadStatus.status != 'success') {
				Ext.Msg.show({
					message: leadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadSupLeadDetail: function(record, type) {
			var lId = record.data.leadID;

			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');

			leadDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadDetailStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/get');
			leadDetailStore.getProxy().setExtraParams({
				leadID: lId,
				companyId: companyId
			});

			leadDetailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.supLeadDetailResponseHandler();
			};
			leadDetailStore.load();

		},

		supLeadDetailResponseHandler: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var leadDetailStatus = leadDetailStore.getProxy().getReader().rawData;
			if (leadDetailStatus.status == 'success') {
				mobEdu.enroute.main.f.leadUpdateFlag = false;
				mobEdu.enroute.main.f.showLeadDetail();
			} else {
				Ext.Msg.show({
					message: leadDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showLeadUpdateProfile: function() {
			mobEdu.enroute.main.f.loadEnumerations();
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var storeData = profileStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadProfileSummary);
			mobEdu.util.show('mobEdu.enroute.main.view.leadProfileUpdate');
			// Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upFName').setValue(storeData[0].data.firstName);
			Ext.getCmp('upLName').setValue(storeData[0].data.lastName);
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('update').setValue(new Date(dobFormatted));
			}
			Ext.getCmp('upEmail').setValue(storeData[0].data.email);
			Ext.getCmp('upPhone').setValue(storeData[0].data.phone1);
			if (storeData[0].data.classification != '' && storeData[0].data.classification != undefined && storeData[0].data.classification != null) {
				Ext.getCmp('upClassText').setValue(storeData[0].data.classification);
			}
			Ext.getCmp('upHighSName').setValue(storeData[0].data.highSchool);
			Ext.getCmp('upIntMajText').setValue(storeData[0].data.intendedMajor);
			Ext.getCmp('upAsScoreNo').setValue(storeData[0].data.actScore);
			if (storeData[0].data.requestInfo != '' && storeData[0].data.requestInfo != undefined && storeData[0].data.requestInfo != null) {
				Ext.getCmp('upReqInfValue').setValue(storeData[0].data.requestInfo);
			}
			if (storeData[0].data.lookInfo != '' && storeData[0].data.lookInfo != undefined && storeData[0].data.lookInfo != null) {
				Ext.getCmp('upInfValue').setValue(storeData[0].data.lookInfo);
			}
			if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
				Ext.getCmp('upStatus').setValue(storeData[0].data.leadStatus);
			}
		},

		saveLeadUpdateProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFName').getValue();
			var updateLName = Ext.getCmp('upLName').getValue();
			var updateBirthDate = Ext.getCmp('update').getValue();
			var updateEmail = Ext.getCmp('upEmail').getValue();
			var updatePhone = Ext.getCmp('upPhone').getValue();
			var updateStatus = Ext.getCmp('upStatus').getValue();
			var updateClassText = Ext.getCmp('upClassText').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMajText').getValue();
			var updateSchoolName = Ext.getCmp('upHighSName').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInfValue').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScoreNo').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInfValue').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateStatus != null && updateStatus != '' && updateIntMajor != null && updateIntMajor != '' && updateSchoolName != null && updateSchoolName != '') {
				var firstName = Ext.getCmp('upFName');
				var fName = firstName.getValue();
				var lastName = Ext.getCmp('upLName');
				var lName = lastName.getValue();
				var regExp = new RegExp("^[a-zA-Z\\- ]+$");
				if (fName.charAt(0) == ' ') {
					Ext.Msg.show({
						id: 'firstNmae',
						name: 'firstNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>FirstName must starts with alphabet.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					if (lName.charAt(0) == ' ') {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							title: null,
							cls: 'msgbox',
							message: '<p>LastName must starts with alphabet.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						if (!regExp.test(fName)) {
							Ext.Msg.show({
								id: 'firstNmae',
								name: 'firstNmae',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid First Name.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										firstName.focus(true);
									}
								}
							});
						} else {
							var lastName = Ext.getCmp('upLName');
							var lastNRegExp = new RegExp("^[a-zA-Z\\- ]+$");
							if (!lastNRegExp.test(lName)) {
								Ext.Msg.show({
									id: 'lastNmae',
									name: 'lastNmae',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid Last Name.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											lastName.focus(true);
										}
									}
								});
							} else {
								var updatePhone1 = Ext.getCmp('upPhone');
								var updatePhoneValue = updatePhone1.getValue();
								updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

								var length = updatePhoneValue.length;
								if (updatePhoneValue.length != 10) {
									Ext.Msg.show({
										id: 'phonemsg',
										name: 'phonemsg',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid phone number.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												updatePhone1.focus(true);
											}
										}
									});
								} else {
									var a = updatePhoneValue.replace(/\D/g, '');
									var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
									updatePhone1.setValue(newValue);
									var updateMail = Ext.getCmp('upEmail');
									var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
									if (updateEmail != '' && regMail.test(updateEmail) == false) {
										Ext.Msg.show({
											id: 'upMail',
											name: 'upMail',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid email address.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													updateMail.focus(true);
												}
											}
										});
									} else {
										var schoolName = Ext.getCmp('upHighS');
										var schoolNameRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
										if (!schoolNameRegExp.test(updateSchoolName)) {
											Ext.Msg.show({
												id: 'hSchool',
												name: 'hSchool',
												title: null,
												cls: 'msgbox',
												message: '<p>Invalid high school.</p>',
												buttons: Ext.MessageBox.OK,
												fn: function(btn) {
													if (btn == 'ok') {
														schoolName.focus(true);
													}
												}
											});
										} else {
											var intMajor = Ext.getCmp('upIntMaj');
											var intMajorRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
											if (!intMajorRegExp.test(updateIntMajor)) {
												Ext.Msg.show({
													id: 'intMName',
													name: 'intMName',
													title: null,
													cls: 'msgbox',
													message: '<p>Invalid intended major.</p>',
													buttons: Ext.MessageBox.OK,
													fn: function(btn) {
														if (btn == 'ok') {
															intMajor.focus(true);
														}
													}
												});
											} else {
												mobEdu.enroute.main.f.loadLeadUpdate();
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},

		loadLeadUpdate: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFName').getValue();
			var updateLName = Ext.getCmp('upLName').getValue();
			var updateBirthDate = Ext.getCmp('update').getValue();
			var updateEmail = Ext.getCmp('upEmail').getValue();
			var updatePhone = Ext.getCmp('upPhone').getValue();
			var updateStatus = Ext.getCmp('upStatus').getValue();
			var updateClassText = Ext.getCmp('upClassText').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMajText').getValue();
			var updateSchoolName = Ext.getCmp('upHighSName').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInfValue').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScoreNo').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInfValue').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadProfileUpdate');

			updateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			updateStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/update');
			updateStore.getProxy().setExtraParams({
				leadID: leadId,
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				status: updateStatus,
				classification: updateClassText,
				highSchool: updateSchoolName,
				intendedMajor: updateIntMajor,
				actScore: updateActSatScore,
				requestInfo: updateRequestInfo,
				lookInfo: updateWhatTypeInf,
				companyId: companyId
			});

			updateStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadLeadUpdateResponseHandler();
			};
			updateStore.load();
		},

		loadLeadUpdateResponseHandler: function() {
			var updateStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadProfileUpdate');
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var updateData = profileStore.data.all[0];
			var updateStatus = updateStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.leadupdateFlag = true;
							if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
								mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
							} else {
								mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsList);
							}
							mobEdu.util.show('mobEdu.enroute.main.view.viewProfileList');
						}
					}
				});
			} else {
				Ext.Msg.show({
					message: updateStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
				//                Ext.Msg.alert(null,updateStatus.status);
			}
		},

		supLeadsListRefresh: function() {
			var supLeadsListStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			supLeadsListStore.load();
			mobEdu.enroute.main.f.showSupLeadsList();
		},

		showSupLeadsList: function() {
			if (mobEdu.enroute.main.f.leadupdateFlag == true) {
				mobEdu.enroute.main.f.loadSupLeadsList();
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsLists);
				mobEdu.util.show('mobEdu.enroute.main.view.supLeadsList');
			}
		},
		showSupLeadsLists: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enroute.main.view.menu');
		},
		showLeadDetail: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var profileData = detailStore.data.all;
			var appId = profileData[0].data.applicationID;
			if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsList);
			}
			mobEdu.util.show('mobEdu.enroute.main.view.supLeadDetail');
			if (appId != null) {
				Ext.getCmp('unassignedViewApp').show();
			} else {
				Ext.getCmp('unassignedViewApp').hide();
			}
			var viewLeadId = profileData[0].data.leadID;
			var fullName;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			fullName = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			if (dateOfBirth == null) {
				dateOfBirth = '';
			}
			var emailId1 = profileData[0].data.email;
			var phoneNo1 = profileData[0].data.phone1;
			var ldStatus = profileData[0].data.leadStatus;
			if (ldStatus == null) {
				ldStatus = '';
			}
			var reqName = profileData[0].data.recruiterName;
			var address1 = profileData[0].data.address1;
			var address2 = profileData[0].data.address2;
			var classText = profileData[0].data.classification;
			var schoolName = profileData[0].data.highSchool;
			var intMajor = profileData[0].data.intendedMajor;
			var scoresValues = profileData[0].data.actScore;
			var reqInfo = profileData[0].data.requestInfo;
			var lookInfo = profileData[0].data.lookInfo;

			var finalLeadSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Full Name:</h2></td>' + '<td align="left"><h3>' + fullName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>BirthDate:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>';
			if (classText != '' && classText != null) {
				finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>Classification:</h2></td>' + '<td align="left"><h3>' + classText + '</h3></td></tr>';
			}
			if (schoolName != '' && schoolName != null) {
				finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>High School:</h2></td>' + '<td align="left"><h3>' + schoolName + '</h3></td></tr>';
			}
			if (intMajor != '' && intMajor != null) {
				finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>Intended Major:</h2></td>' + '<td align="left"><h3>' + intMajor + '</h3></td></tr>';
			}
			if (scoresValues != '' && scoresValues != null) {
				finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>ACT/SAT Score:</h2></td>' + '<td align="left"><h3>' + scoresValues + '</h3></td></tr>';
			}
			if (reqInfo != '' && reqInfo != null) {
				finalLeadSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>Who is reguesting the <br/> information?:</h2></td>' + '<td align="left"><h3>' + reqInfo + '</h3></td></tr>';
			}
			if (lookInfo != '' && lookInfo != null) {
				finalLeadSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>What type of information <br/> are you looking for?:</h2></td>' + '<td align="left"><h3>' + lookInfo + '</h3></td></tr>';
			}
			finalLeadSummaryHtml += '<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + reqName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr></table>';

			Ext.getCmp('supLeadDetailSummary').setHtml(finalLeadSummaryHtml);
		},

		getLeadApplication: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var profileData = detailStore.data.all;
			//            var appId = profileData[0].data.applicationID;
			var viewLeadId = profileData[0].data.leadID;
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');

			applicationStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			applicationStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getapplication');
			applicationStore.getProxy().setExtraParams({
				leadID: viewLeadId,
				companyId: companyId
			});
			applicationStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.getLeadApplicationResponseHandler();
			};
			applicationStore.load();
		},

		getLeadApplicationResponseHandler: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			var applicationStatus = applicationStore.getProxy().getReader().rawData;
			if (applicationStatus.status == 'success') {
				mobEdu.enroute.main.f.loadLeadApplication();
			} else {
				Ext.Msg.show({
					id: 'appSuccess',
					title: null,
					message: applicationStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadLeadApplication: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.viewApplication');
			var profileData = applicationStore.data.all;
			var appSisInfo = profileData[0].data.sisInfo;
			var appIndicator;
			var applicationStatus;
			var appComments;
			var appBannerId;
			if (appSisInfo != 0 && appSisInfo != null && appSisInfo != '' && appSisInfo != undefined) {
				appIndicator = appSisInfo.applIndicator;
				applicationStatus = appSisInfo.apstDescription;
				appComments = appSisInfo.comments;
				appBannerId = appSisInfo.bannerID;
			} else {
				appIndicator = '';
				applicationStatus = '';
				appComments = '';
				appBannerId = '';
			}
			if (appIndicator == "Y") {
				Ext.getCmp('leadPushApp').hide();
			} else {
				Ext.getCmp('leadPushApp').show();
			}
			var applicationId = profileData[0].data.applicationID;
			var appLeadId = profileData[0].data.leadID;
			var appStatus = profileData[0].data.status;
			var appVersionNo = profileData[0].data.versionNo;
			var appVersionDate = profileData[0].data.versionDate;
			var appVersionUser = profileData[0].data.versionUser;
			var appAdTermCode = profileData[0].data.termCode;
			var appAdTermCodeDes = profileData[0].data.termCodeDescr;
			var appAdLevelCode = profileData[0].data.levelCode;
			var appAdLevelCodeDes = profileData[0].data.levelCodeDescr;
			var appAdMajorCode = profileData[0].data.majorCode;
			var appAdMajorCodeDes = profileData[0].data.majorCodeDescr;
			var appAdStudentType = profileData[0].data.studentType;
			var appAdStudentTypeDes = profileData[0].data.studentTypeDescr;
			var appAdAdmissionType = profileData[0].data.admissionType;
			var appAdAdmissionTypeDes = profileData[0].data.admissionTypeDescr;
			var appAdResidenceCode = profileData[0].data.residenceCode;
			var appAdResidenceCodeDes = profileData[0].data.residenceCodeDescr;
			var appAdCollegeCode = profileData[0].data.collegeCode;
			var appAdCollegeCodeDes = profileData[0].data.collegeCodeDescr;
			var appAdDegreeCode = profileData[0].data.degreeCode;
			var appAdDegreeCodeDes = profileData[0].data.degreeCodeDescr;
			var appAdDepartment = profileData[0].data.department;
			var appAdDepartmentDes = profileData[0].data.departmentDescr;
			var appAdCampus = profileData[0].data.campus;
			var appAdCampusDes = profileData[0].data.campusDescr;
			var appAdEduGoal = profileData[0].data.educationGoal;
			var appAdEduGoalDes = profileData[0].data.educationGoalDescr;
			var middleValue;
			var appPerFirstName = profileData[0].data.firstName;
			var appPerLastName = profileData[0].data.lastName;
			//            middleValue = appPerFirstName + ' ' + appPerLastName;
			var appPerMiddleName = profileData[0].data.middleName;
			if (appPerMiddleName != " " && appPerMiddleName != null) {
				middleValue = appPerFirstName + ' ' + appPerMiddleName + ' ' + appPerLastName;
			} else {
				middleValue = appPerFirstName + ' ' + appPerLastName;
			}
			var appPerGender = profileData[0].data.gender;
			if (appPerGender == "F") {
				appPerGender = 'Female';
			} else {
				appPerGender = 'Male';
			}
			var appPerRace = profileData[0].data.race;
			var appPerRaceDes = profileData[0].data.raceDescr;
			if (appPerRaceDes == 'select') {
				appPerRaceDes = '';
			}
			var appPerEthnicity = profileData[0].data.ethnicity;
			var appPerEthnicityDes = profileData[0].data.ethnicityDescr;
			var appPerDateOfBirth = profileData[0].data.dob;
			var appPerSsn = profileData[0].data.ssn;
			var address;
			var zipAndCountry;
			var appContAddress1 = profileData[0].data.address1;
			var appContAddress2 = profileData[0].data.address2;
			var appContAddress3 = profileData[0].data.address3;
			var appContCity = profileData[0].data.city;
			var appContState = profileData[0].data.state;
			var appContStateDes = profileData[0].data.stateDescr;
			var appContCounty = profileData[0].data.county;
			var appContZip = profileData[0].data.zip;
			var appContCountry = profileData[0].data.country;
			if (appContAddress2 != '' && appContAddress3) {
				address = appContAddress1 + ',' + appContAddress2 + ',' + appContAddress3;
			} else {
				address = appContAddress1;
			}
			if (appContCountry != '') {
				zipAndCountry = appContZip + ',' + appContCountry
			} else {
				zipAndCountry = appContZip;
			}

			var appContEmailId = profileData[0].data.email;
			var appContPhoneNo1 = profileData[0].data.phone1;
			var appContPhoneNo2 = profileData[0].data.phone2;
			var visaDetails;
			var appVisaType = profileData[0].data.visaType;
			var appVisaNationality = profileData[0].data.nationality;
			var appVisaNationalityDes = profileData[0].data.nationalityDescr;
			var appVisaNo = profileData[0].data.visaNumber;
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				visaDetails = appVisaType + ',' + appVisaNationalityDes + ',' + appVisaNo;
			}
			var parent1Details;
			var parent2Details;
			var appParRelation1 = profileData[0].data.parent1Relation;
			var appParRelation1Des = profileData[0].data.parent1RelationDescr;
			var appParFName1 = profileData[0].data.parent1FirstName;
			var appParLName1 = profileData[0].data.parent1LastName;
			var appParMName1 = profileData[0].data.parent1MiddleName;
			if (appParFName1 != '' && appParLName1 != '') {
				parent1Details = appParFName1 + ',' + appParLName1;
			} else if (appParLName1 != '') {
				parent1Details = appParLName1;
			} else {
				parent1Details = appParFName1;
			}
			var appParRelation2 = profileData[0].data.parent2Relation;
			var appParRelation2Des = profileData[0].data.parent2RelationDescr;
			var appParFName2 = profileData[0].data.parent2FirstName;
			var appParLName2 = profileData[0].data.parent2LastName;
			var appParMName2 = profileData[0].data.parent2MiddleName;
			if (appParFName2 != '' && appParLName2 != '') {
				parent2Details = appParFName2 + ',' + appParLName2;
			} else if (appParLName2 != '') {
				parent2Details = appParLName2;
			} else {
				parent2Details = appParFName2;
			}
			var appSchoolName = profileData[0].data.schoolName;
			var appSchoolGpa = profileData[0].data.schoolGpa;
			var appSchoolCode = profileData[0].data.schoolCode;
			var appSchoolCodeDes = profileData[0].data.schoolCodeDescr;
			//            var appSchoolCity = profileData[0].data.schoolCity;
			//            var appSchoolState = profileData[0].data.schoolState;
			var appSchoolGYear = profileData[0].data.schoolGradYear;
			var testCode;
			var testDate;
			var testScore;
			var appTsTestScores = profileData[0].data.testScores;
			if (appTsTestScores.length != 0) {
				for (var i = 0; i < appTsTestScores.length; i++) {
					testCode = appTsTestScores[i].testCode;
					testDate = appTsTestScores[i].testDate;
					testScore = appTsTestScores[i].testScore;
				}
			} else {
				testCode = "";
				testDate = "";
				testScore = "";
			}
			var collegeCode;
			var degree;
			var grdDate;
			var collegeCity;
			var collegeState;
			var appCInfoColleges = profileData[0].data.colleges;
			if (appCInfoColleges.length != 0) {
				for (var c = 0; c < appCInfoColleges.length; c++) {
					collegeCode = appCInfoColleges[c].collegeCode;
					degree = appCInfoColleges[c].degree;
					collegeCity = appCInfoColleges[c].city;
					collegeState = appCInfoColleges[c].state;
					grdDate = appCInfoColleges[c].gradDate;
				}
			} else {
				collegeCode = "";
				degree = "";
				grdDate = "";
				collegeCity = "";
				collegeState = "";
			}
			var appInterestPN = profileData[0].data.primaryInterest;
			var appInterestSN = profileData[0].data.secondaryInterest;
			var appInterestLevelN = profileData[0].data.levelOfInterest;
			var appInterestFactor = profileData[0].data.factorForChoosing;
			var appInterestOtherN = profileData[0].data.otherInterests;

			var applicationSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + appPerDateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Gender:</h2></td>' + '<td align="left"><h3>' + appPerGender + '</h3></td></tr>';
			if (appPerRaceDes != '' && appPerRaceDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Race:</h2></td>' + '<td align="left"><h3>' + appPerRaceDes + '</h3></td></tr>';
			}
			if (appPerEthnicityDes != '' && appPerEthnicityDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Ethnicity:</h2></td>' + '<td align="left"><h3>' + appPerEthnicityDes + '</h3></td></tr>';
			}
			if (appPerSsn != '' && appPerSsn != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>SSN:</h2></td>' + '<td align="left"><h3>' + appPerSsn + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Address:</h2></td>' + '<td align="left"><h3>' + address + '<br />' + appContCity + ',' + appContStateDes + '<br />' + zipAndCountry + '</h3></td></tr>';
			if (appContEmailId != '' && appContEmailId != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + appContEmailId + '</h3></td></tr>';
			}
			if (appContPhoneNo1 != '' && appContPhoneNo1 != null) {
				appContPhoneNo1 = appContPhoneNo1.replace(/-|\(|\)/g, "");
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone1:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo1 + '</h3></td></tr>';
			}
			if (appContPhoneNo2 != '' && appContPhoneNo2 != null) {
				appContPhoneNo2 = appContPhoneNo2.replace(/-|\(|\)/g, "");
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone2:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo2 + '</h3></td></tr>';
			}
			if (appParRelation1Des != '' && appParRelation1Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 1 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation1Des + '<br />' + parent1Details + '</h3></td></tr>';
			}
			if (appParRelation2Des != '' && appParRelation2Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 2 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation2Des + '<br />' + parent2Details + '</h3></td></tr>';
			}
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Visa Details:</h2></td>' + '<td align="left"><h3>' + appVisaType + '<br />' + appVisaNationalityDes + ',' + appVisaNo + '</h3></td></tr>';
			}
			if (appSchoolName != '' && appSchoolCodeDes != '' && appSchoolGpa != '' && appSchoolGYear != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>School Details:</h2></td>' + '<td align="left"><h3>' + appSchoolName + '<br />' + appSchoolCodeDes + appSchoolGpa + ',' + appSchoolGYear + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Term Code:</h2></td>' + '<td align="left"><h3>' + appAdTermCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Level Code:</h2></td>' + '<td align="left"><h3>' + appAdLevelCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Major Code:</h2></td>' + '<td align="left"><h3>' + appAdMajorCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Student Type:</h2></td>' + '<td align="left"><h3>' + appAdStudentTypeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Residence Code:</h2></td>' + '<td align="left"><h3>' + appAdResidenceCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + appAdCollegeCode + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + appAdDegreeCodeDes + '</h3></td></tr>';
			if (appAdAdmissionTypeDes != '' && appAdAdmissionTypeDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Admission Type:</h2></td>' + '<td align="left"><h3>' + appAdAdmissionTypeDes + '</h3></td></tr>';
			}
			if (appAdDepartmentDes != '' && appAdDepartmentDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Department:</h2></td>' + '<td align="left"><h3>' + appAdDepartmentDes + '</h3></td></tr>';
			}
			if (appAdCampusDes != '' && appAdCampusDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Campus:</h2></td>' + '<td align="left"><h3>' + appAdCampusDes + '</h3></td></tr>';
			}
			if (appAdEduGoalDes != '' && appAdEduGoalDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Education Goal:</h2></td>' + '<td align="left"><h3>' + appAdEduGoalDes + '</h3></td></tr>';
			}
			if (testCode != '' && testCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Code:</h2></td>' + '<td align="left"><h3>' + testCode + '</h3></td></tr>';
			}
			if (testDate != '' && testDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Date:</h2></td>' + '<td align="left"><h3>' + testDate + '</h3></td></tr>';
			}
			if (testScore != '' && testScore != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Score:</h2></td>' + '<td align="left"><h3>' + testScore + '</h3></td></tr>';
			}
			if (collegeCode != '' && collegeCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + collegeCode + '</h3></td></tr>';
			}
			if (collegeCity != '' && collegeState != '') {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Address:</h2></td>' + '<td align="left"><h3>' + collegeCity + ',' + collegeState + '</h3></td></tr>';
			}
			if (degree != '' && degree != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + degree + '</h3></td></tr>';
			}
			if (grdDate != '' && grdDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Graduation Date:</h2></td>' + '<td align="left"><h3>' + grdDate + '</h3></td></tr>';
			}
			if (appInterestPN != '' && appInterestPN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Primary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestPN + '</h3></td></tr>';
			}
			if (appInterestSN != '' && appInterestSN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Secondary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestSN + '</h3></td></tr>';
			}
			if (appInterestLevelN != '' && appInterestLevelN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Level Of Interest:</h2></td>' + '<td align="left"><h3>' + appInterestLevelN + '</h3></td></tr>';
			}
			if (appInterestFactor != '' && appInterestFactor != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Factor For Choosing:</h2></td>' + '<td align="left"><h3>' + appInterestFactor + '</h3></td></tr>';
			}
			if (appInterestOtherN != '' && appInterestOtherN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Other Interests:</h2></td>' + '<td align="left"><h3>' + appInterestOtherN + '</h3></td></tr>';
			}
			if (appBannerId != '' && appBannerId != undefined) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Banner ID:</h2></td>' + '<td align="left"><h3>' + appBannerId + '</h3></td></tr>';
			}
			if (applicationStatus != '' && applicationStatus != undefined) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Application Status:</h2></td>' + '<td align="left"><h3>' + applicationStatus + '</h3></td></tr>';
			}
			if (appComments != '' && appComments != undefined) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Comments:</h2></td>' + '<td align="left"><h3>' + appComments + '</h3></td></tr></table>';
			}

			Ext.getCmp('leadAppView').setHtml(applicationSummaryHtml);
		},

		loadPushBannerApp: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			var profileData = applicationStore.data.all;
			var applicationId = profileData[0].data.applicationID;
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/pushtosis');
			enuStore.getProxy().setExtraParams({
				applicationID: applicationId,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadPushBannerAppResponseHandler();
			};
			enuStore.load();
		},
		loadPushBannerAppResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.newAppointment');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				Ext.Msg.show({
					id: 'pushmsg',
					title: null,
					message: '<p>Application pushed to banner successfully.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							//mobEdu.enroute.leads.f.loadProfileSummary();
							mobEdu.enroute.main.f.getLeadApplication();
						}
					}
				});
			}
		},

		showReqSerach: function() {
			if (Ext.os.is.Phone) {
				//                mobEdu.enroute.main.f.showReqSearchView();
				mobEdu.enroute.main.f.recruiterUnAssignSearchList();
			} else {
				mobEdu.enroute.main.f.assignedLeadList();
			}
		},

		allLeadsLeadProfileUpdate: function() {
			mobEdu.enroute.main.f.loadEnumerations();
			var leadDeatilStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var storeData = leadDeatilStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.updateLeadProfile');
			// Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upFirstN').setValue(storeData[0].data.firstName);
			Ext.getCmp('upLastN').setValue(storeData[0].data.lastName);
			Ext.getCmp('updateDate').setValue(new Date(storeData[0].data.birthDate));
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('updateDate').setValue(new Date(dobFormatted));
			}
			Ext.getCmp('upEmailvalue').setValue(storeData[0].data.email);
			Ext.getCmp('upPhoneValue').setValue(storeData[0].data.phone1);
			if (storeData[0].data.leadStatus != '' && storeData[0].data.leadStatus != undefined && storeData[0].data.leadStatus != null) {
				Ext.getCmp('upStatusValue').setValue(storeData[0].data.leadStatus);
			}
			if (storeData[0].data.classification != '' && storeData[0].data.classification != undefined && storeData[0].data.classification != null) {
				Ext.getCmp('upClass').setValue(storeData[0].data.classification);
			}
			Ext.getCmp('upHighS').setValue(storeData[0].data.highSchool);
			Ext.getCmp('upIntMaj').setValue(storeData[0].data.intendedMajor);
			Ext.getCmp('upAsScore').setValue(storeData[0].data.actScore);
			if (storeData[0].data.requestInfo != '' && storeData[0].data.requestInfo != undefined && storeData[0].data.requestInfo != null) {
				Ext.getCmp('upReqInf').setValue(storeData[0].data.requestInfo);
			}
			if (storeData[0].data.lookInfo != '' && storeData[0].data.lookInfo != undefined && storeData[0].data.lookInfo != null) {
				Ext.getCmp('upInf').setValue(storeData[0].data.lookInfo);
			}
		},

		loadEnumerations: function() {
			var enumType = 'LEAD_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.enumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.enumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		saveSupLeadProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstN').getValue();
			var updateLName = Ext.getCmp('upLastN').getValue();
			var updateBirthDate = Ext.getCmp('updateDate').getValue();
			var updateEmail = Ext.getCmp('upEmailvalue').getValue();
			var updatePhone = Ext.getCmp('upPhoneValue').getValue();
			var updateStatus = Ext.getCmp('upStatusValue').getValue();
			var updateClassText = Ext.getCmp('upClass').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMaj').getValue();
			var updateSchoolName = Ext.getCmp('upHighS').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInf').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScore').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInf').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateStatus != null && updateStatus != '' && updateIntMajor != null && updateIntMajor != '' && updateSchoolName != null && updateSchoolName != '') {
				var firstName = Ext.getCmp('upFirstN');
				var fName = firstName.getValue();
				var lastName = Ext.getCmp('upLastN');
				var lName = lastName.getValue();
				var regExp = new RegExp("^[a-zA-Z\\- ]+$");
				if (fName.charAt(0) == ' ') {
					Ext.Msg.show({
						id: 'firstNmae',
						name: 'firstNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>FirstName must starts with alphabet.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					if (lName.charAt(0) == ' ') {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							title: null,
							cls: 'msgbox',
							message: '<p>LastName must starts with alphabet.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						if (!regExp.test(fName)) {
							Ext.Msg.show({
								id: 'firstNmae',
								name: 'firstNmae',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid First Name.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										firstName.focus(true);
									}
								}
							});
						} else {
							var lastName = Ext.getCmp('upLastN');
							var lastNRegExp = new RegExp("^[a-zA-Z\\- ]+$");
							if (!lastNRegExp.test(lName)) {
								Ext.Msg.show({
									id: 'lastNmae',
									name: 'lastNmae',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid Last Name.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											lastName.focus(true);
										}
									}
								});
							} else {
								var updatePhone1 = Ext.getCmp('upPhoneValue');
								var updatePhoneValue = updatePhone1.getValue();
								updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

								var length = updatePhoneValue.length;
								if (updatePhoneValue.length != 10) {
									Ext.Msg.show({
										id: 'phonemsg',
										name: 'phonemsg',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid phone number.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												updatePhone1.focus(true);
											}
										}
									});
								} else {
									var a = updatePhoneValue.replace(/\D/g, '');
									var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
									updatePhone1.setValue(newValue);
									var updateMail = Ext.getCmp('upEmailvalue');
									var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
									if (updateEmail != '' && regMail.test(updateEmail) == false) {
										Ext.Msg.show({
											id: 'upMail',
											name: 'upMail',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid email address.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													updateMail.focus(true);
												}
											}
										});
									} else {
										var schoolName = Ext.getCmp('upHighS');
										var schoolNameRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
										if (!schoolNameRegExp.test(updateSchoolName)) {
											Ext.Msg.show({
												id: 'hSchool',
												name: 'hSchool',
												title: null,
												cls: 'msgbox',
												message: '<p>Invalid high school.</p>',
												buttons: Ext.MessageBox.OK,
												fn: function(btn) {
													if (btn == 'ok') {
														schoolName.focus(true);
													}
												}
											});
										} else {
											var intMajor = Ext.getCmp('upIntMaj');
											var intMajorRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
											if (!intMajorRegExp.test(updateIntMajor)) {
												Ext.Msg.show({
													id: 'intMName',
													name: 'intMName',
													title: null,
													cls: 'msgbox',
													message: '<p>Invalid intended major.</p>',
													buttons: Ext.MessageBox.OK,
													fn: function(btn) {
														if (btn == 'ok') {
															intMajor.focus(true);
														}
													}
												});
											} else {
												mobEdu.enroute.main.f.loadSupLeadUpdateProfile();
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadSupLeadUpdateProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var storeData = profileStore.data.all;
			var leadId = storeData[0].data.leadID;
			var updateFName = Ext.getCmp('upFirstN').getValue();
			var updateLName = Ext.getCmp('upLastN').getValue();
			var updateBirthDate = Ext.getCmp('updateDate').getValue();
			var updateEmail = Ext.getCmp('upEmailvalue').getValue();
			var updatePhone = Ext.getCmp('upPhoneValue').getValue();
			var updateStatus = Ext.getCmp('upStatusValue').getValue();
			var updateClassText = Ext.getCmp('upClass').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMaj').getValue();
			var updateSchoolName = Ext.getCmp('upHighS').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInf').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScore').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInf').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadProfileUpdate');

			updateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			updateStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/update');
			updateStore.getProxy().setExtraParams({
				leadID: leadId,
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				status: updateStatus,
				highSchool: updateSchoolName,
				intendedMajor: updateIntMajor,
				actScore: updateActSatScore,
				requestInfo: updateRequestInfo,
				lookInfo: updateWhatTypeInf,
				companyId: companyId
			});
			updateStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadSupLeadUpdateProfileResponseHandler();
			};
			updateStore.load();
		},
		loadSupLeadUpdateProfileResponseHandler: function() {
			var updateStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadProfileUpdate');
			var profileStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var storeData = profileStore.data.all[0];
			var updateStatus = updateStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.leadupdateFlag = true;
							mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadLists);
							mobEdu.util.show('mobEdu.enroute.main.view.allLeadsList');
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: updateStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onPhone1Keyup: function(phone1Field) {
			var phoneNumber = (phone1Field.getValue()).toString() + '';
			phoneNumber = phoneNumber.replace(/\D/g, '');
			phone1Field.setValue(phoneNumber);
			var length = phoneNumber.length;
			if (length > 10) {
				phone1Field.setValue(phoneNumber.substring(0, 10));
				return false;
			}
			return true;
		},

		loadAllLeadsReqSearch: function() {
			if (Ext.os.is.Phone) {
				mobEdu.enroute.main.f.allLeadsRecruiterSearchList();
			} else {
				mobEdu.enroute.main.f.allLeadsAssignedLeadList();
			}
		},

		leadPopup: function(popupView) {
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enroute.main.view.leadAssignPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
			Ext.getCmp('searchResultList').refresh();
		},

		leadAllLeadAssignPopup: function(popupView) {
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enroute.main.view.allLeadAssignPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
			Ext.getCmp('allSearchResultList').refresh();
		},

		showReqSearchView: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.recruiterSearch');
		},

		showAllLeadsReqSearch: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.allLeadsRecruiterSearch');
		},

		assignedLeadList: function() {
			var searchItem = '';

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100,
				companyId: companyId
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.assignedLeadListResponseHandler();
			};
			reqSearchStore.load();
		},

		assignedLeadListResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			reqSearchStore.sort("firstName", "ASC");
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				//                mobEdu.enroute.main.f.hidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enroute.main.f.leadPopup();
			}
		},

		recruiterUnAssignSearchList: function() {
			//            var searchItem = Ext.getCmp('searchReq').getValue();
			var searchItem = '';

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100,
				companyId: companyId
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.recruiterUnAssignSearchResponseHandler();
			};
			reqSearchStore.load();
		},
		recruiterUnAssignSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			reqSearchStore.sort("firstName", "ASC");
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enroute.main.f.showReqSearchView();
			}
		},

		allLeadsAssignedLeadList: function() {
			var searchItem = '';

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100,
				companyId: companyId
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.allLeadsAssignedLeadListResponseHandler();
			};
			reqSearchStore.load();
		},
		allLeadsAssignedLeadListResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			reqSearchStore.sort("firstName", "ASC");
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				//                mobEdu.enroute.main.f.allLeadsHidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enroute.main.f.leadAllLeadAssignPopup();
			}
		},

		allLeadsRecruiterSearchList: function() {
			//            var searchItem = Ext.getCmp('searchReq').getValue();
			var searchItem = '';
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100,
				companyId: companyId
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.allLeadsRecruiterSearchResponseHandler();
			};
			reqSearchStore.load();
		},
		allLeadsRecruiterSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			reqSearchStore.sort("firstName", "ASC");
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enroute.main.f.showAllLeadsReqSearch();
			}
		},

		recruiterSearchList: function() {
			var searchItem = Ext.getCmp('searchReq').getValue();

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100,
				companyId: companyId
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.recruiterSearchResponseHandler();
			};
			reqSearchStore.load();
		},
		recruiterSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				//                mobEdu.enroute.main.f.hidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});

			}
		},

		allLeadsReqSearch: function() {
			var searchItem = Ext.getCmp('searchReq').getValue();

			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');

			reqSearchStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqSearchStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/search');
			reqSearchStore.getProxy().setExtraParams({
				searchText: searchItem,
				maxRecords: 100,
				companyId: companyId
			});

			reqSearchStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.allLeadsReqSearchResponseHandler();
			};
			reqSearchStore.load();
		},

		allLeadsReqSearchResponseHandler: function() {
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			var searchStatus = reqSearchStore.getProxy().getReader().rawData;
			if (searchStatus.status != 'success') {
				mobEdu.enroute.main.f.allLeadsHidePopup();
				Ext.Msg.show({
					message: searchStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});

			}
		},

		showReqProfile: function(index, e) {
			mobEdu.enroute.main.f.hidePopup();
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enroute.main.f.hidePopup();
						mobEdu.enroute.main.f.assignLead(index, e);
					}
					if (btn == 'no') {
						mobEdu.enroute.main.f.leadPopup();
					}
				}
			});
		},

		assignRecruiter: function(index, e) {
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enroute.main.f.assignRecruiterInAllLeads(index, e);
					}

				}
			});
		},

		assignRecruiterUnAssignLeads: function(index, e) {
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enroute.main.f.selectUnAssignLeadsRecruiter(index, e);
					}

				}
			});
		},

		showAllLeadsReqProfile: function(index, e) {
			mobEdu.enroute.main.f.allLeadsHidePopup();
			Ext.Msg.show({
				message: '<p>Assign lead to this recruiter?</p>',
				cls: 'msgbox',
				buttons: Ext.MessageBox.YESNO,
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.enroute.main.f.allLeadsHidePopup();
						mobEdu.enroute.main.f.assignAllLeadsList(index, e);
					}
					if (btn == 'no') {
						mobEdu.enroute.main.f.leadAllLeadAssignPopup();
					}
				}
			});
		},

		assignLead: function(index, e) {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var deatilData = leadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId,
				companyId: companyId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.assignLeadResponseHandler(assreqId, assLId);
			};
			assignStore.load();
		},
		assignLeadResponseHandler: function(rid, lid) {
			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Lead assigned successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.hidePopup();
							mobEdu.enroute.main.f.generateMsg('assigned', rid, lid);
							if (mobEdu.enroute.main.f.leadSearchType === 'SEARCHLEAD') {
								mobEdu.enroute.main.f.supSearchLeadsListRefresh();
							} else {
								mobEdu.enroute.main.f.supLeadsListRefresh();
							}
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		assignRecruiterInAllLeads: function(index, e) {
			var allLeadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var deatilData = allLeadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			console.log(e.data);
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId,
				companyId: companyId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.assignRecruiterInAllLeadsResponseHandler(assreqId, assLId);
			};
			assignStore.load();
		},
		assignRecruiterInAllLeadsResponseHandler: function(rid, lid) {
			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Lead assigned successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.allLeadsListRefresh();
							mobEdu.enroute.main.f.generateMsg('assigned', rid, lid);
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		selectUnAssignLeadsRecruiter: function(index, e) {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var deatilData = leadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId,
				companyId: companyId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.selectUnAssignLeadsRecruiterResponseHandler(assreqId, assLId);
			};
			assignStore.load();
		},
		selectUnAssignLeadsRecruiterResponseHandler: function(rid, lid) {
			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Lead assigned successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.supLeadsListRefresh();
							mobEdu.enroute.main.f.generateMsg('assigned', rid, lid);
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		assignAllLeadsList: function(index, e) {
			var allLeadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var deatilData = allLeadDetailStore.data.all;
			var assLId = deatilData[0].data.leadID;
			var reqSearchStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterSearch');
			var reqData = reqSearchStore.data.all;
			//            var assreqId = reqData[0].data.recruiterID;
			var assreqId = e.data.userID;

			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');

			assignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			assignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/assignlead');
			assignStore.getProxy().setExtraParams({
				leadID: assLId,
				recruiterID: assreqId,
				companyId: companyId
			});

			assignStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.assignAllLeadsListResponseHandler(assreqId, assLId);
			};
			assignStore.load();
		},

		assignAllLeadsListResponseHandler: function(rid, lid) {
			var assignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');
			var assignStatus = assignStore.getProxy().getReader().rawData;
			if (assignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'assignsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Lead assigned successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							//                            mobEdu.enroute.main.f.allLeadsHidePopup();
							mobEdu.enroute.main.f.allLeadsListRefresh();
							mobEdu.enroute.main.f.generateMsg('assigned', rid, lid);
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: assignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAllLeadsList: function() {
			var searchLeadItem = Ext.getCmp('sALItem').getValue();

			var allLeadStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			allLeadStore.removeAll();
			allLeadStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			allLeadStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getAllLeads');
			allLeadStore.getProxy().setExtraParams({
				searchText: searchLeadItem,
				maxRecords: 100,
				companyId: companyId
			});

			allLeadStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadAllLeadsListResponseHandler();
			};
			allLeadStore.load();
		},
		loadAllLeadsListResponseHandler: function() {
			var allLeadStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			allLeadStore.sort("versionDate", "DESC");
			var allLeadStatus = allLeadStore.getProxy().getReader().rawData;
			if (allLeadStatus.status != 'success') {
				Ext.Msg.show({
					message: allLeadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

			if (allLeadStore.data.length > 0) {
				allLeadStore.insert(0, {
					firstName: "Select All",
					lastName: " "
				});
			}
		},

		loadAllLeads: function() {
			mobEdu.util.get('mobEdu.enroute.main.view.allLeadsList');
			var searchLeadItem = Ext.getCmp('sALItem').getValue();

			var allLeadStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');

			allLeadStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			allLeadStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getAllLeads');
			allLeadStore.getProxy().setExtraParams({
				searchText: searchLeadItem,
				limit: 500,
				companyId: companyId
			});
			allLeadStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadAllLeadsResponseHandler();
			};
			allLeadStore.load();
		},
		loadAllLeadsResponseHandler: function() {
			var allLeadStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			allLeadStore.sort("versionDate", "DESC");
			var allLeadStatus = allLeadStore.getProxy().getReader().rawData;
			if (allLeadStatus.status != 'success') {
				Ext.Msg.show({
					message: allLeadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enroute.main.f.showAllLeadList();
			}
		},

		showAllLeadList: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadLists);
			mobEdu.util.show('mobEdu.enroute.main.view.allLeadsList');
		},
		showAllLeadLists: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enroute.main.view.menu');
		},

		showAllLeadDetailProfile: function(index, view, record, item) {
			var lId = record.data.leadID;

			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');

			leadDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadDetailStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/get');
			leadDetailStore.getProxy().setExtraParams({
				leadID: lId,
				companyId: companyId
			});

			leadDetailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.showAllLeadDetailResponseHandler();
			};
			leadDetailStore.load();
		},

		showAllLeadDetailResponseHandler: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var detailStatus = leadDetailStore.getProxy().getReader().rawData;
			if (detailStatus.status == 'success') {
				mobEdu.enroute.main.f.showAllLeadDetail();
				if (detailStatus.recruiterID == 0) {
					Ext.getCmp('assign').show();
					Ext.getCmp('unAssign').hide();
				} else {
					Ext.getCmp('assign').hide();
					Ext.getCmp('unAssign').show();
				}
			} else {
				Ext.Msg.show({
					message: detailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		allLeadsListRefresh: function() {
			var allLeadsListStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadsList');
			allLeadsListStore.load();
			mobEdu.enroute.main.f.showAllLeadList();
		},

		showAllLeadDetail: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var profileData = detailStore.data.all;
			var appId = profileData[0].data.applicationID;
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadList);
			mobEdu.util.show('mobEdu.enroute.main.view.allLeadDetail');
			if (appId != null) {
				Ext.getCmp('leadViewApp').show();
			} else {
				Ext.getCmp('leadViewApp').hide();
			}
			//            var detailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			//            var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var fullName;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			fullName = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			if (dateOfBirth == null) {
				dateOfBirth = '';
			}
			var emailId1 = profileData[0].data.email;
			var phoneNo1 = profileData[0].data.phone1;
			var ldStatus = profileData[0].data.leadStatus;
			if (ldStatus == null) {
				ldStatus = '';
			}
			var reqName = profileData[0].data.recruiterName;
			var address1 = profileData[0].data.address1;
			var address2 = profileData[0].data.address2;
			var classificationText = profileData[0].data.classification;
			var hSchoolName = profileData[0].data.highSchool;
			var intMajorValue = profileData[0].data.intendedMajor;
			var actScores = profileData[0].data.actScore;
			var reqInfoText = profileData[0].data.requestInfo;
			var lookInfoText = profileData[0].data.lookInfo;
			//            var address;
			//            if(address2 == ''){
			//                address = address1;
			//            } else{
			//                address = address1 + ',' + address2;
			//            }

			var finalSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Full Name:</h2></td>' + '<td align="left"><h3>' + fullName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>BirthDate:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>';
			if (classificationText != '' && classificationText != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Classification:</h2></td>' + '<td align="left"><h3>' + classificationText + '</h3></td></tr>';
			}
			if (hSchoolName != '' && hSchoolName != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>High School:</h2></td>' + '<td align="left"><h3>' + hSchoolName + '</h3></td></tr>';
			}
			if (intMajorValue != '' && intMajorValue != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Intended Major:</h2></td>' + '<td align="left"><h3>' + intMajorValue + '</h3></td></tr>';
			}
			if (actScores != '' && actScores != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>ACT/SAT Score:</h2></td>' + '<td align="left"><h3>' + actScores + '</h3></td></tr>';
			}
			if (reqInfoText != '' && reqInfoText != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>Who is reguesting the <br/> information?:</h2></td>' + '<td align="left"><h3>' + reqInfoText + '</h3></td></tr>';
			}
			if (lookInfoText != '' && lookInfoText != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>What type of information <br/> are you looking for?:</h2></td>' + '<td align="left"><h3>' + lookInfoText + '</h3></td></tr>';
			}
			finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + reqName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + ldStatus + '</h3></td></tr></table>';

			Ext.getCmp('allLeadsDetailSummary').setHtml(finalSummaryHtml);
		},

		appointmentsExist: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var leadData = leadDetailStore.data.all;
			var unAssignLeadId = leadData[0].data.leadID;
			var leadAppExistStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadAppExist');

			leadAppExistStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadAppExistStore.getProxy().setUrl(commonwebserver + 'appointment/appointmentsExist');
			leadAppExistStore.getProxy().setExtraParams({
				leadID: unAssignLeadId,
				companyId: companyId
			});

			leadAppExistStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.appointmentsExistResponseHandler();
			};
			leadAppExistStore.load();
		},

		deleteExistAppointments: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var leadData = leadDetailStore.data.all;
			var unAssignLeadId = leadData[0].data.leadID;
			var leadAppExistStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadAppExist');

			leadAppExistStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadAppExistStore.getProxy().setUrl(commonwebserver + 'appointment/deleteLeadExistAppointment');
			leadAppExistStore.getProxy().setExtraParams({
				leadID: unAssignLeadId,
				companyId: companyId
			});
			leadAppExistStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadUnAssignLead();
			};
			leadAppExistStore.load();
		},

		appointmentsExistResponseHandler: function() {
			var leadAppExistStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadAppExist');
			var appStatus = leadAppExistStore.getProxy().getReader().rawData;
			if (appStatus.status == 'true') {
				Ext.Msg.show({
					id: 'deletionApp',
					title: null,
					cls: 'msgbox',
					message: '<p>The lead have open appointments.Do you want to cancel all appointments?</p>',
					buttons: Ext.MessageBox.OKCANCEL,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.deleteExistAppointments();
						}
					}

				});
			} else {
				mobEdu.enroute.main.f.loadUnAssignLead();
			}
		},

		loadUnAssignLead: function() {
			var leadDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.supLeadDetail');
			var leadData = leadDetailStore.data.all;
			var unAssignLeadId = leadData[0].data.leadID;
			var recruiterId = leadData[0].data.recruiterID;
			var unAssignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.unAssignLead');

			unAssignStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			unAssignStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/unassignlead');
			unAssignStore.getProxy().setExtraParams({
				recruiterID: recruiterId,
				leadID: unAssignLeadId,
				companyId: companyId
			});

			unAssignStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.UnAssignLeadResponseHandler(recruiterId, unAssignLeadId);
			};
			unAssignStore.load();
		},
		UnAssignLeadResponseHandler: function(recruiterId, unAssignLeadId) {
			var unAssignStore = mobEdu.util.getStore('mobEdu.enroute.main.store.unAssignLead');
			var unAssignStatus = unAssignStore.getProxy().getReader().rawData;
			if (unAssignStatus.status == 'success') {
				Ext.Msg.show({
					id: 'unAssignSuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Successfully unassigned the lead.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.allLeadsListRefresh();
							mobEdu.enroute.main.f.generateMsg('unassigned', recruiterId, unAssignLeadId);
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: unAssignStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAppointmentsList: function(val) {
			var searchText = '';
			var rId = mobEdu.enroute.main.f.getRecruiterId();
			var reqStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList');

			reqStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			reqStore.getProxy().setUrl(commonwebserver + 'appointment/search');
			reqStore.getProxy().setExtraParams({
				searchText: searchText,
				limit: 500,
				companyId: companyId
			});

			reqStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.appointmentsListResponseHandler(val);
			};
			reqStore.load();
		},
		appointmentsListResponseHandler: function(val) {
			var reqStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList');
			reqStore.sort('appointmentDate', 'DESC');
			var appStatus = reqStore.getProxy().getReader().rawData;
			mobEdu.enroute.main.f.showAppointmentsList(val);
			/*if (appStatus.status == 'success') {
				mobEdu.enroute.main.f.showAppointmentsList(val);
			} else {
				Ext.Msg.show({
					message: appStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}*/
		},

		reqAppListBack: function() {
			Ext.getCmp('appointmentList').refresh();
			var reqAppListStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList');
			reqAppListStore.load();
			mobEdu.enroute.main.f.showAppointmentsList();
		},

		showAppointmentsList: function(val) {
			if (val == 'CALENDAR') {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.loadEnrouteMainMenu);
			}
			var reqStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList');
			var reqStoreData = reqStore.data.all;
			mobEdu.util.show('mobEdu.enroute.main.view.reqAppointmentsList');

		},

		showReqAppointmentDetail: function(record) {

			var appId = record.data.appointmentID;
			var acceptance = '';
			var versionUser = record.data.createdBy;
			var user = mobEdu.util.getStudentId();
			versionUser = versionUser.toLowerCase();
			user = user.toLowerCase();
			var fromDetails = record.data.fromDetails;
			var toArry = record.data.toIDs;
			var fromID = fromDetails.fromID;
			if (fromID == record.data.loginUserID) {
				acceptance = fromDetails.acceptance;
			}

			if (acceptance == '' || acceptance == undefined || acceptance == null) {
				for (var i = 0; i < toArry.length; i++) {
					var toID = toArry[i].toID;
					if (toID == record.data.loginUserID) {
						acceptance = toArry[i].acceptance;
					}
				}
			}

			if (versionUser == user) {
				mobEdu.util.get('mobEdu.enroute.main.view.reqAppointmentDetail');
				Ext.getCmp('raccept').hide();
				Ext.getCmp('rdecline').hide();
			} else {
				mobEdu.util.get('mobEdu.enroute.main.view.reqAppointmentDetail');
				Ext.getCmp('raccept').show();
				Ext.getCmp('rdecline').show();
				if (acceptance == 'Y' || acceptance == 'N') {
					mobEdu.util.get('mobEdu.enroute.main.view.reqAppointmentDetail');
					Ext.getCmp('raccept').hide();
					Ext.getCmp('rdecline').hide();
				} else {
					mobEdu.util.get('mobEdu.enroute.main.view.reqAppointmentDetail');
					Ext.getCmp('raccept').show();
					Ext.getCmp('rdecline').show();
				}
			}

			var appId = record.data.appointmentID;

			var appDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentDetail');

			appDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			appDetailStore.getProxy().setUrl(commonwebserver + 'appointment/get');
			appDetailStore.getProxy().setExtraParams({
				appointmentID: appId,
				companyId: companyId
			});

			appDetailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.reqAppointmentDetailResponseHandler();
			};
			appDetailStore.load();
		},

		reqAppointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentDetail');
			var appDetailStatus = appDetailStore.getProxy().getReader().rawData;
			mobEdu.enroute.main.f.loadReqDetail();
			/*if (appDetailStatus.status == 'success') {
				mobEdu.enroute.main.f.loadReqDetail();
			} else {
				Ext.Msg.show({
					message: appDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
*/
		},

		showReqNewAppointment: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAppointmentsList);
			mobEdu.util.get('mobEdu.enroute.main.view.reqNewAppointment');
			mobEdu.util.show('mobEdu.enroute.main.view.reqNewAppointment');
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
			localStore.removeAll();
			var newDate = new Date();
			Ext.getCmp('reqappdate').setValue(newDate);
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			Ext.getCmp('reqappTimeH').setMinValue(timeH);
			Ext.getCmp('reqappTimeM').setMinValue(timeM);
			Ext.getCmp('reqappTimeH').setValue(timeH);
			Ext.getCmp('reqappTimeM').setValue(timeM);
			Ext.getCmp('rTo').reset();
		},

		sendReqNewAppointment: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			store.removeAll();
			store.sync();
			var lead = Ext.getCmp('rTo').getValue();
			var aSub = Ext.getCmp('reqappsub').getValue();
			var aDes = Ext.getCmp('reqappdes').getValue();
			var aDate = Ext.getCmp('reqappdate').getValue();
			var aLoc = Ext.getCmp('reqapploc').getValue();
			var aStatus = 'SCHEDULED';
			var aTimeH = Ext.getCmp('reqappTimeH').getValue();
			var aTimeM = Ext.getCmp('reqappTimeM').getValue();
			if (aStatus == 'select') {
				aStatus = '';
			}

			if (aTimeH == 0) {
				aTimeH = '';
			}
			if (aSub.charAt(0) == ' ' || aDes.charAt(0) == ' ') {
				Ext.Msg.show({
					message: '<p>Subject & Description must start with alphanumeric.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (lead != '' && aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aStatus != '' && aTimeH != '') {
					var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);
					var selectDate = Ext.getCmp('reqappdate').getValue();
					selectDate = Ext.Date.format(selectDate, 'm/d/Y');
					if (aTimeM < 10) {
						aTimeM = "0" + aTimeM;
					}
					var selectDateFormatted = Ext.Date.parse(selectDate + " " + aTimeH + ":" + aTimeM, 'm/d/Y G:i');
					if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
						mobEdu.enroute.main.f.newReqAppointmentResponse();
					} else {
						Ext.Msg.show({
							title: 'Invalid Date',
							message: '<p>Please select a future date..</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
					if (aTimeH >= 18 || aTimeH < 8) {
						Ext.Msg.show({
							title: 'Invalid Time',
							message: '<p>Appointment time between 8AM to 18PM.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				} else {
					Ext.Msg.show({
						title: null,
						message: '<p>Please enter all mandatory fields.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}

		},
		newReqAppointmentResponse: function() {
			//var toIds = mobEdu.enroute.main.f.appLeadId;


			var toIds = '';
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
			if (localStore.data.length == 0) {
				toIds = '';
			} else {
				localStore.each(function(record) {
					if (toIds == '') {
						toIds = record.data.pidm;
					} else {
						toIds = toIds + ',' + record.data.pidm;
					}
				});
			}

			mobEdu.enroute.main.f.appLeadId = '';
			var aSub = Ext.getCmp('reqappsub').getValue();
			var aDes = Ext.getCmp('reqappdes').getValue();
			var aDate = Ext.getCmp('reqappdate').getValue();
			var aLoc = Ext.getCmp('reqapploc').getValue();
			var aTimeHours = Ext.getCmp('reqappTimeH').getValue();
			var aTimeMinutes = Ext.getCmp('reqappTimeM').getValue();
			var toNames = Ext.getCmp('rTo').getValue();
			var appTime;
			if (aTimeHours == null) {
				aTimeHours = '';
			}
			if (aTimeMinutes == null) {
				aTimeMinutes = '';
			}
			appTime = aTimeHours + ':' + aTimeMinutes;
			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(aDate, 'm/d/Y');
			}
			var appDateFormatted = appDateFormatted + ' ' + appTime;
			var appText = 'appointment';
			var appOperationText = 'create';
			var creBy = 1;
			var leadApp = 'OUT';
			var leadStatus = 'SCHEDULED';
			if (aTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.loadAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.enroute.main.view.reqNewAppointment');
						}
					}
				});
			} else {
				var newAppStore = mobEdu.util.getStore('mobEdu.enroute.main.store.reqNewAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(commonwebserver + 'appointment/add');
				newAppStore.getProxy().setExtraParams({
					to: toIds,
					subject: aSub,
					body: aDes,
					appointmentDate: appDateFormatted,
					location: aLoc,
					createdBy: creBy,
					source: leadApp,
					status: leadStatus,
					companyId: companyId
				});

				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enroute.main.f.newReqAppointmentResponseHandler();
				};
				newAppStore.load();
			}
		},
		resetAppointment: function() {
			mobEdu.util.get('mobEdu.enroute.main.view.reqNewAppointment');
			Ext.getCmp('rTo').reset();
			Ext.getCmp('reqappsub').reset();
			Ext.getCmp('reqapploc').reset();
			Ext.getCmp('reqappdes').reset();
		},

		newReqAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.main.store.reqNewAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment created successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList').load();
							mobEdu.enroute.main.f.showAppointmentsList();
							mobEdu.enroute.main.f.resetAppointment();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
				//newDateFormatted = ([newDate.getMonth() + 1] + '/' + newDate.getDate() + '/' + newDate.getFullYear());
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
				//presentDateFormatted = ([presentDate.getMonth() + 1] + '/' + presentDate.getDate() + '/' + presentDate.getFullYear());
			}
			if (newDateFormatted == presentDateFormatted) {
				if (timeH < 8) {
					if (Ext.getCmp('reqUpTimeHours') != undefined) {
						Ext.getCmp('reqUpTimeHours').setMinValue(8);
						Ext.getCmp('reqUpTimeHours').setValue(8);
					}
					if (Ext.getCmp('reqUpTimeMinutes') != undefined) {
						Ext.getCmp('reqUpTimeMinutes').setMinValue(0);
						Ext.getCmp('reqUpTimeMinutes').setValue(0);
					}
				} else {
					if (Ext.getCmp('reqUpTimeHours') != undefined) {
						Ext.getCmp('reqUpTimeHours').setMinValue(timeH);
						Ext.getCmp('reqUpTimeHours').setValue(timeH);
					}
					if (Ext.getCmp('reqUpTimeMinutes') != undefined) {
						Ext.getCmp('reqUpTimeMinutes').setMinValue(timeM);
						Ext.getCmp('reqUpTimeMinutes').setValue(timeM);
					}
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('requpappdate').setValue(new Date());
							Ext.getCmp('reqUpTimeHours').setMinValue(timeH);
							Ext.getCmp('reqUpTimeMinutes').setMinValue(timeM);
							Ext.getCmp('reqUpTimeHours').setValue(timeH);
							Ext.getCmp('reqUpTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('reqUpTimeHours').setMinValue(8);
				Ext.getCmp('reqUpTimeMinutes').setMinValue(0);
				Ext.getCmp('reqUpTimeHours').setValue(8);
				Ext.getCmp('reqUpTimeMinutes').setValue(0);
			}
		},
		onReqAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
				//newDateFormatted = ([newDate.getMonth() + 1] + '/' + newDate.getDate() + '/' + newDate.getFullYear());
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
				//presentDateFormatted = ([presentDate.getMonth() + 1] + '/' + presentDate.getDate() + '/' + presentDate.getFullYear());
			}
			if (newDateFormatted == presentDateFormatted) {
				if (timeH < 8) {
					if (Ext.getCmp('reqappTimeH') != undefined) {
						Ext.getCmp('reqappTimeH').setMinValue(8);
						Ext.getCmp('reqappTimeH').setValue(8);
					}
					if (Ext.getCmp('reqappTimeM') != undefined) {
						Ext.getCmp('reqappTimeM').setMinValue(0);
						Ext.getCmp('reqappTimeM').setValue(0);
					}
				} else {
					if (Ext.getCmp('reqappTimeH') != undefined) {
						Ext.getCmp('reqappTimeH').setMinValue(timeH);
						Ext.getCmp('reqappTimeH').setValue(timeH);
					}
					if (Ext.getCmp('reqappTimeM') != undefined) {
						Ext.getCmp('reqappTimeM').setMinValue(timeM);
						Ext.getCmp('reqappTimeM').setValue(timeM);
					}
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('requpappdate').setValue(new Date());
							Ext.getCmp('reqappTimeH').setMinValue(timeH);
							Ext.getCmp('reqappTimeM').setMinValue(timeM);
							Ext.getCmp('reqappTimeH').setValue(timeH);
							Ext.getCmp('reqappTimeM').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('reqappTimeH').setMinValue(8);
				Ext.getCmp('reqappTimeM').setMinValue(0);
				Ext.getCmp('reqappTimeH').setValue(8);
				Ext.getCmp('reqappTimeM').setValue(0);
			}
		},

		loadReqUpdateAppointment: function() {
			mobEdu.enroute.main.f.loadAppointmentEnumerations();
			var upAppReqStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentDetail');
			var upDateStore = upAppReqStore.data.all;
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.loadReqDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.updateReqAppointment');
			Ext.getCmp('requpappsub').setValue(upDateStore[0].data.subject);
			Ext.getCmp('requpappdes').setValue(upDateStore[0].data.body);
			var appDate = upDateStore[0].data.appointmentDate;
			var appDateValue = appDate.split(' ');
			var dateFormatted = appDateValue[0];
			var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
			var appTime = appDateValue[1];
			var appTimeFormatted = appTime.split(':');
			var appTimeHours = appTimeFormatted[0];
			var appTimeMints = appTimeFormatted[1];
			if (appTimeHours < 10) {
				appTimeHours = '0' + appTimeHours;
			}
			if (appTimeMints < 10) {
				appTimeMints = '0' + appTimeMints;
			}
			if (upDateStore[0].data.appointmentDate != '' && upDateStore[0].data.appointmentDate != undefined && upDateStore[0].data.appointmentDate != null) {
				Ext.getCmp('reqappdate').setValue(new Date(newStartDate));
			}
			Ext.getCmp('reqUpTimeHours').setValue(appTimeHours);
			Ext.getCmp('reqUpTimeMinutes').setValue(appTimeMints);
			Ext.getCmp('requpapploc').setValue(upDateStore[0].data.location);
			// if (upDateStore[0].data.appointmentStatus != '' && upDateStore[0].data.appointmentStatus != undefined && upDateStore[0].data.appointmentStatus != null) {
			// 	Ext.getCmp('requpappsta').setValue('RESCHEDULED');
			// }

		},

		loadAppointmentEnumerations: function() {
			var enumType = 'APPOINTMENT_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.enumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadEnumerationsResponseHandler();
			};
			enuStore.load();
		},

		updateReqAppointmentSend: function() {
			var upSub = Ext.getCmp('requpappsub').getValue();
			var upDes = Ext.getCmp('requpappdes').getValue();
			var upDate = Ext.getCmp('reqappdate').getValue();
			var upLoc = Ext.getCmp('requpapploc').getValue();
			var upStatus = 'RESCHEDULED';
			var upAppTimeHours = Ext.getCmp('reqUpTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('reqUpTimeMinutes').getValue();

			if (upSub != '' && upDes != '' && upDate != '' && upLoc != '' && upAppTimeHours != '') {
				var presentDate = new Date();
				var presentDateFormatted;
				if (presentDate == null) {
					presentDateFormatted = '';
				} else {
					presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
					//presentDateFormatted = (mobEdu.enroute.main.f.dateChecking([presentDate.getMonth() + 1]) + '/' + mobEdu.enroute.main.f.dateChecking(presentDate.getDate()) + '/' + presentDate.getFullYear());
				}
				var selectDate = Ext.getCmp('reqappdate').getValue();
				var selectDateFormatted;
				if (selectDate == null) {
					selectDateFormatted = '';
				} else {
					selectDateFormatted = Ext.Date.format(selectDate, 'm/d/Y');
					//selectDateFormatted = (mobEdu.enroute.main.f.dateChecking([selectDate.getMonth() + 1]) + '/' + mobEdu.enroute.main.f.dateChecking(selectDate.getDate()) + '/' + selectDate.getFullYear());
				}
				if (presentDateFormatted > selectDateFormatted) {
					Ext.Msg.show({
						title: 'Invalid Date',
						message: '<p>Please select a future date.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					if (upAppTimeHours >= 18) {
						Ext.Msg.show({
							title: 'Invalid Time',
							message: '<p>Appointment time between 8:00 to 18:00.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					} else {
						mobEdu.enroute.main.f.updateAppointmentSentRequest();
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateAppointmentSentRequest: function() {
			var upAppReqStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentDetail');
			var upDateStore = upAppReqStore.data.all;
			var appointmentId = upDateStore[0].data.appointmentID;
			//var from = upDateStore[0].data.fromID;
			//var to = upDateStore[0].data.toID;
			var upSub = Ext.getCmp('requpappsub').getValue();
			var upDes = Ext.getCmp('requpappdes').getValue();
			var upDate = Ext.getCmp('reqappdate').getValue();
			var upLoc = Ext.getCmp('requpapploc').getValue();
			var upStatus = 'RESCHEDULED';
			var upAppTimeHours = Ext.getCmp('reqUpTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('reqUpTimeMinutes').getValue();
			var upAppTime;
			var from = '';
			var fromDetails = upDateStore[0].raw.fromDetails;

			var userID = fromDetails.fromID;
			var acceptance = fromDetails.acceptance;
			from = userID + ',' + acceptance + ';';

			var to = upDateStore[0].raw.toIDs;
			var versionUser = upDateStore[0].raw.createdBy;
			var participant = '';

			var res = '';
			for (var i = 0; i < to.length; i++) {
				var userID = to[i].toID;
				var acceptance = to[i].acceptance;
				res = res + userID + ',' + acceptance + ';';
			}

			if (upAppTimeHours == null) {
				upAppTimeHours = '';
			}
			if (upAppTimeMints == null) {
				upAppTimeMints = '';
			}
			if (upAppTimeHours < 10) {
				upAppTimeHours = '0' + upAppTimeHours;
			}
			if (upAppTimeMints < 10) {
				upAppTimeMints = '0' + upAppTimeMints;
			}
			upAppTime = upAppTimeHours + ':' + upAppTimeMints;
			var appDateFormatted;
			if (upDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(upDate, 'm/d/Y');
				//appDateFormatted = ([upDate.getMonth() + 1] + '/' + upDate.getDate() + '/' + upDate.getFullYear());
			}
			var appDateTime = appDateFormatted + ' ' + upAppTime;
			var currentDate = new Date();
			var currentDateFormatted = Ext.Date.format(currentDate, 'm/d/Y');
			//var currentDateFormatted = ([currentDate.getMonth() + 1] + '/' + currentDate.getDate() + '/' + currentDate.getFullYear());
			if (upAppTimeHours >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.loadAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.enroute.main.view.updateReqAppointment');
						}
					}
				});
			} else {
				var newAppStore = mobEdu.util.getStore('mobEdu.enroute.main.store.reqNewAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(commonwebserver + 'appointment/update');
				newAppStore.getProxy().setExtraParams({
					appointmentID: appointmentId,
					subject: upSub,
					body: upDes,
					appointmentDate: appDateTime,
					location: upLoc,
					status: upStatus,
					companyId: companyId,
					versionUser: mobEdu.util.getStudentId(),
					from: from,
					to: res
				});

				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enroute.main.f.updateAppointmentResponseHandler();
				};
				newAppStore.load();
			}
		},

		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.main.store.reqNewAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappreqsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.reqAppListBack();
							mobEdu.enroute.main.f.resetUppDateAppointment();
							mobEdu.util.get('mobEdu.enroute.main.view.reqAppointmentDetail');
							Ext.getCmp('raccept').show();
							Ext.getCmp('rdecline').show();

						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetUppDateAppointment: function() {
			mobEdu.util.get('mobEdu.enroute.main.view.updateReqAppointment').reset();
		},

		hasReqAppointmentDirection: function(appSource) {
			if (appSource == "IN") {
				return true;
			} else {
				return false;
			}
		},
		hasAppReadFlag: function(readFlag) {
			if (readFlag == "Opened") {
				return true;
			} else {
				return false;
			}
		},
		hasRReadFlag: function(readFlag) {
			if (readFlag == "1") {
				return true;
			} else {
				return false;
			}
		},

		//        hasReqEmailDirection:function (direction) {
		//            if (direction == "IN") {
		//                return true;
		//            } else {
		//                return false;
		//            }
		//        },
		hasReqEmailDirection: function(labels) {
			//            for(var j=0; j<labels.length; j++){
			var dirLabel = labels[0].label;
			//            }
			if (dirLabel == "SENT") {
				return true;
			} else {
				return false;
			}
		},

		hasReqReadFlag: function(status) {

			if (status == "Opened") {
				return true;
			} else {
				return false;
			}
		},
		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},
		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.enroute.main.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enroute.main.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.enroute.main.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enroute.main.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.enroute.main.f.setAppointmentScheduleInfo(date)
			}
		},
		setAppointmentScheduleInfo: function(selectedDate) {
			var scheduleInfo = '';
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			var newDateValue = (mobEdu.enroute.main.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enroute.main.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());

			var appointmentsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList');
			appointmentsStore.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var leadAcceptance = rec.get('sAcceptance');
				var recAcceptance = rec.get('cAcceptance');

				var appDateValue = appDate.split(' ');
				var dateFormatted = appDateValue[0];
				if (newDateValue == dateFormatted) {
					var appTime = appDateValue[1];
					var appSubject = rec.get('subject');
					var appDesc = rec.get('body');
					var appLocation = rec.get('location');
					scheduleInfo = mobEdu.enroute.main.f.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc, leadAcceptance, recAcceptance);
				}
			});
			//set the event schedule info in calendar view label
			Ext.getCmp('reqAppointmentLabel').setHtml(scheduleInfo);
		},

		formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc, leadAcceptance, recAcceptance) {
			if (leadAcceptance != 'N' && recAcceptance != 'N') {

				if (appSubject == null) {
					appSubject = '';
				}
				if (appTime == null) {
					appTime = '';
				}
				if (appDesc == null) {
					appDesc = '';
				}
				if (appLocation == null) {
					appLocation = '';
				}
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + decodeURIComponent(appSubject) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + decodeURIComponent(appLocation) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Description: ' + '</b>' + decodeURIComponent(appDesc) + '</h3>' + '<br/>';

				return scheduleInfo;
			} else {
				return scheduleInfo;
			}
		},

		//To highlight Schedule dates
		doHighlightAppointmentsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentsList');
			store.each(function(rec) {
				var leadAcceptance = rec.get('sAcceptance');
				var recAcceptance = rec.get('cAcceptance');
				if (leadAcceptance != 'N' && recAcceptance != 'N') {
					var appDate = rec.get('appointmentDate');
					var appDateValue = appDate.split(' ');
					var dateFormatted = appDateValue[0];
					var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
					if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
						classes.push(highlightedItemCls);
					}
				}
			});
			return classes;
		},

		loadCalendarView: function() {
			mobEdu.util.show('mobEdu.enroute.main.view.recruiterAppointmentCalendar');
			mobEdu.main.f.calendarInstance = 'reqappt';
			mobEdu.util.get('mobEdu.enroute.main.view.recruiterAppointmentCalendar');
			var appointmentDock = Ext.getCmp('appointmentsDock');
			appointmentDock.removeAll(true, true);
			appointmentDock.removed = [];

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'appointmentsCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			appointmentDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.enroute.main.f.setAppointmentScheduleInfo(new Date());
		},

		showCalendar: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.reqAppListBack);
			mobEdu.enroute.main.f.loadCalendarView();
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.reqAppListBack);
			mobEdu.util.show('mobEdu.enroute.main.view.recruiterAppointmentCalendar');
		},

		loadEnrouteMainMenu: function() {
			Ext.getCmp('modules').refresh();
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enroute.main.view.menu');
		},

		showReqNewEmail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showRecruiterEList);
			mobEdu.util.show('mobEdu.enroute.main.view.reqNewEmail');
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			localStore.removeAll();
		},

		showUpdateAppointment: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.loadReqDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.updateReqAppointment');
		},

		loadReqDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.reqAppListBack);
			mobEdu.util.show('mobEdu.enroute.main.view.reqAppointmentDetail');
		},

		showEmail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.enroute.main.view.email');
		},

		showRecruiterEList: function(val) {
			if (val == 'MSG') {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showRecruiterELists);
			}
			mobEdu.util.show('mobEdu.enroute.main.view.recruiterEmailsList');

		},
		showRecruiterELists: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail');
			store.currentPage = 1;
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enroute.main.view.menu');
		},

		showNewMessage: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			store.removeAll();
			var store1 = mobEdu.util.getStore('mobEdu.enroute.main.store.userList');
			var records = store1.getRange();
			store1.remove(records);
			store1.removeAll();
			store1.removed = [];
			store1.sync();
			mobEdu.util.get('mobEdu.enroute.main.view.searchPopup');
			Ext.getCmp('selectAll').refresh();
			var toIdsLocalStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			toIdsLocalStore.removeAll();
			toIdsLocalStore.removed = [];
			toIdsLocalStore.sync();
			var msgDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			msgDetailStore.removeAll();
			msgDetailStore.removed = [];
			msgDetailStore.sync();
			mobEdu.enroute.main.f.resetSendReplyEmail();
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showRecruiterEList);
			mobEdu.util.show('mobEdu.enroute.main.view.respondEmail');
			Ext.getCmp('rsub').setValue('');
			Ext.getCmp('rmsg').setValue('');
			Ext.getCmp('rToName').setValue('');
			Ext.getCmp('searchHelp').show();
			Ext.getCmp('resEmailTitle').setTitle('<h1>New Message</h1>');
			mobEdu.util.get('mobEdu.enroute.main.view.searchPopup');
			Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
			Ext.getCmp('searchRecId').setValue('');
			if (!mobEdu.util.getRole().match(/SUPERVISOR/gi)) {
				mobEdu.enroute.main.f.toIdsSearchList();
			}
		},

		showResopndEmail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showReqEmailDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.respondEmail');
			Ext.getCmp('searchHelp').hide();
			Ext.getCmp('rToName').setReadOnly(true);
			Ext.getCmp('resEmailTitle').setTitle('<h1>Reply Message</h1>');
			var msgDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			var messageData = msgDetailStore.data.all;
			var toIds = new Array();
			var labelsText = new Array();
			var formIds = new Array();
			var toNames = '';
			var toIdValues = '';
			var subject = messageData[0].data.subject;
			var rmsg = messageData[0].data.body;
			formIds = messageData[0].data.from;
			toIds = messageData[0].data.to;
			labelsText = messageData[0].data.labels;
			for (var i = 0; i < labelsText.length; i++) {
				var labelName = labelsText[0].label;
			}
			if (labelName == "SENT") {
				for (var j = 0; j < toIds.length; j++) {
					toNames = toIds[j].toName;
					toIdValues = toIds[j].toID;
				}
			} else {
				toNames = formIds.fromName;
			}
			Ext.getCmp('rToName').setValue(toNames);
			Ext.getCmp('rsub').setValue(subject);
			var toIdsLocalStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			toIdsLocalStore.removeAll();
			toIdsLocalStore.removed = [];
			toIdsLocalStore.sync();
		},

		ShowReplayAllMessagesView: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showReqEmailDetail);
			mobEdu.util.show('mobEdu.enroute.main.view.respondEmail');
			Ext.getCmp('resEmailTitle').setTitle('<h1>Reply Message</h1>');
			var msgDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			var messageData = msgDetailStore.data.all;
			var toIds = new Array();
			var labelsText = new Array();
			var formIds = new Array();
			var toIdValues = '';
			var toNames = '';
			var subject = messageData[0].data.subject;
			var rmsg = messageData[0].data.body;
			formIds = messageData[0].data.from;
			toIds = messageData[0].data.to;
			labelsText = messageData[0].data.labels;
			for (var i = 0; i < labelsText.length; i++) {
				var labelName = labelsText[0].label;
			}
			if (labelName == "SENT") {
				for (var j = 0; j < toIds.length; j++) {
					if (toNames == '') {
						toNames = toIds[j].toName;
					} else {
						toNames = toNames + ',' + toIds[j].toName;
					}
				}
			} else {
				toNames = formIds.fromName;
			}
			Ext.getCmp('rToName').setValue(toNames);
			Ext.getCmp('rsub').setValue(subject);
			var toIdsLocalStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			toIdsLocalStore.removeAll();
			toIdsLocalStore.removed = [];
			toIdsLocalStore.sync();
		},

		setPagingPlugin: function(listComponent) {

			if (Ext.getCmp('messageplugin') == null) {
				var pArray = new Array();
				pArray[0] = mobEdu.enroute.main.view.paging.create();
				listComponent.setPlugins(pArray);
			}
			Ext.getCmp('messageplugin').getLoadMoreCmp().show();

		},

		loadMailsList: function(val) {
			var c = mobEdu.enroute.main.f.count;
			if (c == 0) {
				mobEdu.util.get('mobEdu.enroute.main.view.recruiterEmailsList');
				var searchListCmp = Ext.getCmp('recruiterEmailsList');
				searchListCmp.setPlugins(null);
				var pArray = new Array();
				pArray[0] = mobEdu.enroute.main.view.paging.create();
				searchListCmp.setPlugins(pArray);
				searchListCmp.setEmptyText('<h3 align="center">No messages.</h3>');
				Ext.getCmp('messageplugin').getLoadMoreCmp().show();
				mobEdu.enroute.main.f.count = 1;
			}
			var entType = 'RECRUITER';
			mobEdu.enroute.main.f.role = mobEdu.util.getRole();
			var eStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/myMessages');
			eStore.getProxy().setExtraParams({
				companyId: companyId
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadEmailListResponseHandler(val);
			};
			eStore.load()
		},

		loadEmailListResponseHandler: function(val) {
			var emailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail');
			emailStore.sort('versionDate', 'DESC');
			var eStatus = emailStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success' || eStatus.status == 'No Data') {
				mobEdu.enroute.main.f.showRecruiterEList(val);
			} else {
				Ext.Msg.show({
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadReqEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;
			var eDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');

			eDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eDetailStore.getProxy().setUrl(commonwebserver + 'message/get');
			eDetailStore.getProxy().setExtraParams({
				messageID: eId,
				companyId: companyId
			});

			eDetailStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadReqEmaildetailResponseHandler();
			};
			eDetailStore.load();
		},

		loadReqEmaildetailResponseHandler: function() {
			var eDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			var eDetailStatus = eDetailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status == 'success') {
				mobEdu.enroute.main.f.showReqEmailDetail();
			} else {
				Ext.Msg.show({
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		reqEmailListBack: function() {
			var reqEListStore = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail');
			reqEListStore.currentPage = 1;
			reqEListStore.load();
			mobEdu.enroute.main.f.showRecruiterEList();
		},

		showEDetail: function(record) {
			var eDetailStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			eDetailStore.removeAll();
			eDetailStore.add(record);
			eDetailStore.sync();
			mobEdu.enroute.main.f.showReqEmailDetail();

		},
		showReqEmailDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.reqEmailListBack);
			mobEdu.util.show('mobEdu.enroute.main.view.rEmailDetail');
			var messagesStore = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			var messageData = messagesStore.first();
			var toIdsList = messageData.data.to;
			var from = messageData.data.fromName;

			var dirLabel = messageData.data.labels[0].label;

			if (dirLabel == "SENT") {
				Ext.getCmp('bottomToolbar').hide();
			} else {
				Ext.getCmp('bottomToolbar').show();
			}
			if (toIdsList.length == 1) {
				Ext.getCmp('replyAllMsg').hide();
				Ext.getCmp('replyMeg').show();
			} else {
				Ext.getCmp('replyAllMsg').show();
				Ext.getCmp('replyMeg').hide();
			}
			if (from == "System Generated") {
				Ext.getCmp('bottomToolbar').hide();
			}
		},

		showSearchToIdsPopup: function(popupView) {
			var popUp = Ext.getCmp('searchPopup');
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enroute.main.view.searchPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
			Ext.getCmp('selectAll').refresh();
		},
		showSearchAppToIdsPopup: function(popupView) {
			//            mobEdu.util.updatePrevView(mobEdu.enroute.main.f.hideSearchPopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enroute.main.view.searchAppPopup')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
			Ext.getCmp('searchAppToIds').setValue('');
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			store.removeAll();
		},
		hideSearchPopup: function() {
			//mobEdu.util.updatePrevView();
			var popUp = Ext.getCmp('searchPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},
		hideAppSearchPopup: function() {
			//mobEdu.util.updatePrevView();
			//var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			//store.removeAll();
			//var store1 = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
			//store1.removeAll();
			//store1.sync();
			var popUp = Ext.getCmp('searchAppPopup');
			Ext.getCmp('searchAppToIds').reset();
			popUp.hide();
			Ext.Viewport.unmask();
		},
		doCheckBeforeSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						mobEdu.util.hideKeyboard();
						mobEdu.enroute.main.f.toIdsSearchList(searchString);
					}
				}, 1000);
			}
			if (length == 0) {
				if (!mobEdu.util.getRole().match(/SUPERVISOR/gi)) {
					mobEdu.enroute.main.f.toIdsSearchList();
					Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
				}
			};
		},
		doCheckSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length > 2) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						mobEdu.util.hideKeyboard();
						mobEdu.enroute.main.f.toIdSearchList(searchString);
					}
				}, 1000);
			}
		},

		clearSupSearchStore: function() {
			var store = mobEdu.util.getRole('mobEdu.enroute.main.store.userList');
			store.removeAll();
		},

		toIdsSearchList: function(searchString) {
			var role = mobEdu.util.getRole();
			if (!role.match(/SUPERVISOR/gi)) {
				mobEdu.util.get('mobEdu.enroute.main.view.searchPopup');
				Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
				var searchCategory = 'All';
				var searchlastName = '';
				var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
				store.removeAll();
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getMyLeads');
				store.getProxy().setExtraParams({
					searchText: searchString,
					limit: 500,
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.enroute.main.f.toIdsSearchListResponseHandler();
				};
				store.load();
			} else {
				Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={userID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
				var store = mobEdu.util.getStore('mobEdu.enroute.main.store.userList');
				store.removeAll();
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setUrl(commonwebserver + 'secured/admin/user/search');
				store.getProxy().setExtraParams({
					searchText: searchString,
					limit: 500,
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.enroute.main.f.toIdSearchListResponseHandler();
				};
				store.load();
				//store.suspendEvents();
				//store.resumeEvents();
				Ext.getCmp('selectAll').setStore(store);
				Ext.getCmp('selectAll').refresh();
			}
		},

		toIdSearchListResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.userList');
			var status = store.getProxy().getReader().rawData.status;
			if (status != 'success') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		toIdsSearchListResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			var status = store.getProxy().getReader().rawData.status;
			if (status != 'success') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			if (store.data.length > 0) {
				store.insert(0, {
					firstName: "Select All",
					lastName: " "
				});
			}
		},

		toAppIdsSearchListResponseHandler: function() {
			Ext.getCmp('toAppSearchList').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			var status = store.getProxy().getReader().rawData.status;
			if (status != 'success') {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			if (store.data.length > 0) {
				store.insert(0, {
					firstName: "Select All",
					lastName: " "
				});
			}
		},

		toIdSearchList: function(searchString) {
			var searchCategory = 'All';
			var searchlastName = '';
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			store.removeAll();
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getMyLeads');
			store.getProxy().setExtraParams({
				searchText: searchString,
				limit: 500,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.toAppIdsSearchListResponseHandler();
			};
			store.load();
		},

		onAppSearchItemTap: function(selectedIndex, viewRef, record, item) {
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
			if (selectedIndex == 0) {
				if (item.target.id == "on") {
					Ext.getCmp('toAppSearchList').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
					mobEdu.enroute.main.f.targetFlag = true;
				} else {
					Ext.getCmp('toAppSearchList').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
					mobEdu.enroute.main.f.targetFlag = false;
				}
			}
			//if selectall is checked and any item is unchecked
			if (mobEdu.enroute.main.f.targetFlag == true && item.target.id == 'off') {
				var items = Ext.DomQuery.select("img[name='allNames']");
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				items[0].id = 'off';
			};
			//if all items are checked and Selectall is unchecked
			var items = Ext.DomQuery.select("img[name='allNames']");
			var allSelected = true;
			Ext.each(items, function(item, i) {
				if (item.id == 'off' && i != 0) {
					allSelected = false;
				};
			});
			if (allSelected) {
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				items[0].id = 'on';
				mobEdu.enroute.main.f.targetFlag = true;
			};
		},

		onSearchToIdItemTap: function(selectedIndex, viewRef, record, item) {
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
			if (selectedIndex == 0) {
				if (item.target.id == "on") {
					Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 id="on" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
					mobEdu.enroute.main.f.targetFlag = true;
				} else {
					Ext.getCmp('selectAll').setItemTpl('<table class="menu" width="100%"><tr><td width="3%" align="center" style="background: transparent;"><img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="allNames" title={leadID} /></td><td colspan="2" align="left"><h2>{firstName} {lastName}</h2></td></tr></table>');
					mobEdu.enroute.main.f.targetFlag = false;
				}
			}
			//if selectall is checked and any item is unchecked
			if (mobEdu.enroute.main.f.targetFlag == true && item.target.id == 'off') {
				var items = Ext.DomQuery.select("img[name='allNames']");
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				items[0].id = 'off';
			};
			//if all items are checked and Selectall is unchecked
			var items = Ext.DomQuery.select("img[name='allNames']");
			var allSelected = true;
			Ext.each(items, function(item, i) {
				if (item.id == 'off' && i != 0) {
					allSelected = false;
				};
			});
			if (allSelected) {
				items[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				items[0].id = 'on';
				mobEdu.enroute.main.f.targetFlag = true;
			};

		},
		onSearchUserToIdItemTap: function(selectedIndex, viewRef, record, item) {
			mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			if (item.target.id == "on") {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
				item.target.id = "off";
			} else {
				item.target.src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
				item.target.id = "on";
			}
		},

		selectedToNames: function() {
			mobEdu.enroute.main.f.hideSearchPopup();
			var searchList = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			//localStore.removeAll();
			//localStore.removeAll();
			var items = Ext.DomQuery.select("img[name='allNames']");
			Ext.each(items, function(item, i) {
				if (item.id == "on" && i != 0) {
					var rec = searchList.findRecord('leadID', item.title);
					var fullName = rec.data.firstName + ' ' + rec.data.lastName;
					if (rec != null) {
						localStore.add({
							fullName: fullName,
							pidm: rec.data.leadID
						});
						localStore.sync();
					}
				}
			});
			mobEdu.enroute.main.f.setToField();
		},

		selectedUserToNames: function() {
			mobEdu.enroute.main.f.hideSearchPopup();
			var searchList = mobEdu.util.getStore('mobEdu.enroute.main.store.userList');
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			//localStore.removeAll();
			//localStore.removeAll();
			var items = Ext.DomQuery.select("img[name='allNames']");
			Ext.each(items, function(item, i) {
				if (item.id == "on") {
					var rec = searchList.findRecord('userID', item.title);
					var fullName = rec.data.firstName + ' ' + rec.data.lastName;
					if (rec != null) {
						localStore.add({
							fullName: fullName,
							pidm: rec.data.userID
						});
						localStore.sync();
					}
				}
			});
			mobEdu.enroute.main.f.setToField();
		},

		selectedAppToNames: function(view, index, target, record, item, e, eOpts) {
			mobEdu.enroute.main.f.hideAppSearchPopup();
			var searchList = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch');
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
			//localStore.removeAll();
			//localStore.removeAll();
			var items = Ext.DomQuery.select("img[name='allNames']");
			Ext.each(items, function(item, i) {
				if (item.id == "on" && i != 0) {
					var rec = searchList.findRecord('leadID', item.title);
					var fullName = rec.data.firstName + ' ' + rec.data.lastName;
					if (rec != null) {
						localStore.add({
							fullName: fullName,
							pidm: rec.data.leadID
						});
						localStore.sync();
					}
				}
			});
			mobEdu.enroute.main.f.setAppToField();

			// mobEdu.enroute.main.f.hideAppSearchPopup();
			// var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
			// localStore.removeAll();

			// if (record != null) {
			// 	localStore.add({
			// 		fullName: record.data.firstName + ' ' + record.data.lastName,
			// 		pidm: record.data.leadID
			// 	});
			// 	localStore.sync();
			// }

			// mobEdu.enroute.main.f.setAppToField(record);
		},
		setToField: function() {
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			var selectVlaue = '';
			localStore.each(function(record) {
				if (selectVlaue == '') {
					selectVlaue = record.data.fullName;
				} else {
					selectVlaue = selectVlaue + ',' + record.data.fullName;
				}
			});
			Ext.getCmp('rToName').setValue(selectVlaue);
		},
		setAppToField: function(record1) {
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toAppStore');
			var selectVlaue = '';
			localStore.each(function(record) {
				if (selectVlaue == '') {
					selectVlaue = record.data.fullName;
				} else {
					selectVlaue = selectVlaue + ',' + record.data.fullName;
				}
			});
			mobEdu.util.get('mobEdu.enroute.main.view.reqNewAppointment');
			Ext.getCmp('rTo').setValue(selectVlaue);

			//mobEdu.enroute.main.f.appLeadId = record1.data.leadID;

		},
		clearRecSearchStore: function() {
			var role = mobEdu.util.getRole();
			if (!role.match(/SUPERVISOR/gi)) {
				mobEdu.enroute.main.f.toIdsSearchList();
			} else {
				var store = mobEdu.util.getStore('mobEdu.directory.store.searchList');
				store.removeAll();
			}
		},

		clearSearchStore: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileSearch')
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
			//            store.sync();
		},
		clearDirectorySearchStore: function() {
			var store = mobEdu.util.getStore('mobEdu.directory.store.searchList');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
			//            store.sync();
		},

		loadNewMail: function() {
			mobEdu.util.show('mobEdu.enroute.main.view.email');
		},

		sendNewMail: function() {
			var esub = Ext.getCmp('reqNewSub').getValue();
			var msgArea = Ext.getCmp('reqNewMsg').getValue();
			esub = esub.trim();
			msgArea = msgArea.trim();
			if (msgArea != '' && esub != '') {
				mobEdu.enroute.main.f.sendReqNewEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory values.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sendReqNewEmail: function() {
			//            var msgField= Ext.getCmp('rto').getValue();
			var esub = Ext.getCmp('reqNewSub').getValue();
			var msgArea = Ext.getCmp('reqNewMsg').getValue();

			var eStore = mobEdu.util.getStore('mobEdu.enroute.main.store.respondEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(enrouteWebserver + 'enroute/recruiter/sendmessage');
			eStore.getProxy().setExtraParams({
				leadID: leadId,
				subject: esub,
				body: msgArea,
				companyId: companyId
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.rEmailResponseHandler();
			};
			eStore.load();
		},
		rEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enroute.main.store.respondEmail');
			var nEStatus = nEStore.getProxy().getReader().rawData;
			if (nEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsendsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Message sent successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var store = mobEdu.util.getStore('mobEdu.enroute.main.store.respondEmail');
							store.getProxy().setExtraParams({
								page: 1,
								start: 0
							});
							mobEdu.enroute.main.f.reqEmailListBack();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: nEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sentUpdateEmail: function() {
			var upToIds = Ext.getCmp('rToName').getValue();
			var upsub = Ext.getCmp('rsub').getValue();
			var upArea = Ext.getCmp('rmsg').getValue();
			upsub = upsub.trim();
			upArea = upArea.trim();
			upToIds = upToIds.trim();
			if (upArea != '' && upsub != '' && upToIds != '') {
				mobEdu.enroute.main.f.updateEmail();
			} else {
				Ext.Msg.show({
					message: '<p>Please provide all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateEmail: function() {
			var upToIds = '';
			var esub = Ext.getCmp('rsub').getValue();
			var msgArea = Ext.getCmp('rmsg').getValue();
			var conversationID;
			var localStore = mobEdu.util.getStore('mobEdu.enroute.main.store.toStore');
			var localStoreData = localStore.data.all;
			if (localStoreData.length == 0) {
				upToIds = '';
			} else {
				localStore.each(function(record) {
					if (upToIds == '') {
						upToIds = record.data.pidm;
					} else {
						upToIds = upToIds + ',' + record.data.pidm;
					}
				});
			}
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.emailDetail');
			var data = store.data.all;
			var toIds = new Array();
			if (data.length == 0) {
				conversationID = '';
			} else {
				conversationID = data[0].data.conversationID;
				upToIds = data[0].data.from.fromID;
				// for (var j = 0; j < toIds.length; j++) {
				// 	if (upToIds == '') {
				// 		upToIds = toIds[j].toID;
				// 	} else {
				// 		upToIds = upToIds + ',' + toIds[j].toID;
				// 	}
				// }
			}
			var eStore = mobEdu.util.getStore('mobEdu.enroute.main.store.respondEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/send');
			eStore.getProxy().setExtraParams({
				to: upToIds,
				subject: esub,
				body: msgArea,
				conversationID: conversationID,
				companyId: companyId
			});

			eStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.updateEmailResponseHandler();
			};
			eStore.load();
		},
		updateEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enroute.main.store.respondEmail');
			var NEStatus = nEStore.getProxy().getReader().rawData;
			if (NEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'rsendsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Message sent successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var store = mobEdu.util.getStore('mobEdu.enroute.main.store.recruiterEmail');

							store.getProxy().setExtraParams({
								page: 1,
								start: 0
							});
							mobEdu.enroute.main.f.reqEmailListBack();
							mobEdu.enroute.main.f.resetSendReplyEmail();
						}
					}

				});
			} else {
				Ext.Msg.show({
					message: NEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetSendReplyEmail: function() {
			mobEdu.util.get('mobEdu.enroute.main.view.respondEmail');
		},

		loadUnAssignedLeadApplication: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.profileDetail');
			var profileData = detailStore.data.all;
			//            var appId = profileData[0].data.applicationID;
			var viewLeadId = profileData[0].data.leadID;
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');

			applicationStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			applicationStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getapplication');
			applicationStore.getProxy().setExtraParams({
				leadID: viewLeadId,
				companyId: companyId
			});
			applicationStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadUnAssignedLeadApplicationResponseHandler();
			};
			applicationStore.load();
		},
		loadUnAssignedLeadApplicationResponseHandler: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			var applicationStatus = applicationStore.getProxy().getReader().rawData;
			if (applicationStatus.status == 'success') {
				mobEdu.enroute.main.f.showUnAssignedLeadApplication();
			} else {
				Ext.Msg.show({
					id: 'appSuccess',
					title: null,
					message: applicationStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showUnAssignedLeadApplication: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enroute.leads.store.viewApplication');
			mobEdu.util.updatePrevView(mobEdu.enroute.leads.f.loadProfileSummary);
			mobEdu.util.show('mobEdu.enroute.main.view.viewApplication');
			var profileData = applicationStore.data.all;
			var appSisInfo = profileData[0].data.sisInfo;
			var appIndicator;
			var applicationStatus;
			var appComments;
			var appBannerId;
			if (appSisInfo != 0 && appSisInfo != null && appSisInfo != '' && appSisInfo != undefined) {
				appIndicator = appSisInfo.applIndicator;
				applicationStatus = appSisInfo.apstDescription;
				appComments = appSisInfo.comments;
				appBannerId = appSisInfo.bannerID;
			} else {
				appIndicator = '';
				applicationStatus = '';
				appComments = '';
				appBannerId = '';
			}
			if (appIndicator == "Y") {
				Ext.getCmp('leadPushApp').hide();
			} else {
				Ext.getCmp('leadPushApp').show();
			}
			var applicationId = profileData[0].data.applicationID;
			var appLeadId = profileData[0].data.leadID;
			var appStatus = profileData[0].data.status;
			var appVersionNo = profileData[0].data.versionNo;
			var appVersionDate = profileData[0].data.versionDate;
			var appVersionUser = profileData[0].data.versionUser;
			var appAdTermCode = profileData[0].data.termCode;
			var appAdTermCodeDes = profileData[0].data.termCodeDescr;
			var appAdLevelCode = profileData[0].data.levelCode;
			var appAdLevelCodeDes = profileData[0].data.levelCodeDescr;
			var appAdMajorCode = profileData[0].data.majorCode;
			var appAdMajorCodeDes = profileData[0].data.majorCodeDescr;
			var appAdStudentType = profileData[0].data.studentType;
			var appAdStudentTypeDes = profileData[0].data.studentTypeDescr;
			var appAdAdmissionType = profileData[0].data.admissionType;
			var appAdAdmissionTypeDes = profileData[0].data.admissionTypeDescr;
			var appAdResidenceCode = profileData[0].data.residenceCode;
			var appAdResidenceCodeDes = profileData[0].data.residenceCodeDescr;
			var appAdCollegeCode = profileData[0].data.collegeCode;
			var appAdCollegeCodeDes = profileData[0].data.collegeCodeDescr;
			var appAdDegreeCode = profileData[0].data.degreeCode;
			var appAdDegreeCodeDes = profileData[0].data.degreeCodeDescr;
			var appAdDepartment = profileData[0].data.department;
			var appAdDepartmentDes = profileData[0].data.departmentDescr;
			var appAdCampus = profileData[0].data.campus;
			var appAdCampusDes = profileData[0].data.campusDescr;
			var appAdEduGoal = profileData[0].data.educationGoal;
			var appAdEduGoalDes = profileData[0].data.educationGoalDescr;
			var middleValue;
			var appPerFirstName = profileData[0].data.firstName;
			var appPerLastName = profileData[0].data.lastName;
			var appPerMiddleName = profileData[0].data.middleName;
			if (appPerMiddleName != " " && appPerMiddleName != null) {
				middleValue = appPerFirstName + ' ' + appPerMiddleName + ' ' + appPerLastName;
			} else {
				middleValue = appPerFirstName + ' ' + appPerLastName;
			}
			var appPerGender = profileData[0].data.gender;
			if (appPerGender == "F") {
				appPerGender = 'Female';
			} else {
				appPerGender = 'Male';
			}
			var appPerRaceDes = profileData[0].data.raceDescr;
			if (appPerRaceDes == 'select') {
				appPerRaceDes = '';
			}
			var appPerEthnicityDes = profileData[0].data.ethnicityDescr;
			var appPerDateOfBirth = profileData[0].data.dob;
			var appPerSsn = profileData[0].data.ssn;
			var address;
			var zipAndCountry;
			var appContAddress1 = profileData[0].data.address1;
			var appContAddress2 = profileData[0].data.address2;
			var appContAddress3 = profileData[0].data.address3;
			var appContCity = profileData[0].data.city;
			var appContState = profileData[0].data.state;
			var appContStateDes = profileData[0].data.stateDescr;
			var appContZip = profileData[0].data.zip;
			var appContCountry = profileData[0].data.country;
			if (appContAddress2 != '' && appContAddress3) {
				address = appContAddress1 + ',' + appContAddress2 + ',' + appContAddress3;
			} else {
				address = appContAddress1;
			}
			if (appContCountry != '') {
				zipAndCountry = appContZip + ',' + appContCountry
			} else {
				zipAndCountry = appContZip;
			}

			var appContEmailId = profileData[0].data.email;
			var appContPhoneNo1 = profileData[0].data.phone1;
			var appContPhoneNo2 = profileData[0].data.phone2;
			var visaDetails;
			var appVisaType = profileData[0].data.visaType;
			var appVisaNationality = profileData[0].data.nationality;
			var appVisaNationalityDes = profileData[0].data.nationalityDescr;
			var appVisaNo = profileData[0].data.visaNumber;
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				visaDetails = appVisaType + ',' + appVisaNationalityDes + ',' + appVisaNo;
			}
			var parent1Details;
			var parent2Details;
			var appParRelation1 = profileData[0].data.parent1Relation;
			var appParRelation1Des = profileData[0].data.parent1RelationDescr;
			var appParFName1 = profileData[0].data.parent1FirstName;
			var appParLName1 = profileData[0].data.parent1LastName;
			var appParMName1 = profileData[0].data.parent1MiddleName;
			if (appParFName1 != '' && appParLName1 != '') {
				parent1Details = appParFName1 + ',' + appParLName1;
			} else if (appParLName1 != '') {
				parent1Details = appParLName1;
			} else {
				parent1Details = appParFName1;
			}
			var appParRelation2 = profileData[0].data.parent2Relation;
			var appParRelation2Des = profileData[0].data.parent2RelationDescr;
			var appParFName2 = profileData[0].data.parent2FirstName;
			var appParLName2 = profileData[0].data.parent2LastName;
			var appParMName2 = profileData[0].data.parent2MiddleName;
			if (appParFName2 != '' && appParLName2 != '') {
				parent2Details = appParFName2 + ',' + appParLName2;
			} else if (appParLName2 != '') {
				parent2Details = appParLName2;
			} else {
				parent2Details = appParFName2;
			}
			var appSchoolName = profileData[0].data.schoolName;
			var appSchoolGpa = profileData[0].data.schoolGpa;
			var appSchoolCode = profileData[0].data.schoolCode;
			var appSchoolCodeDes = profileData[0].data.schoolCodeDescr;
			var appSchoolGYear = profileData[0].data.schoolGradYear;
			var testCode;
			var testDate;
			var testScore;
			var appTsTestScores = profileData[0].data.testScores;
			if (appTsTestScores.length != 0) {
				for (var i = 0; i < appTsTestScores.length; i++) {
					testCode = appTsTestScores[i].testCode;
					testDate = appTsTestScores[i].testDate;
					testScore = appTsTestScores[i].testScore;
				}
			} else {
				testCode = "";
				testDate = "";
				testScore = "";
			}
			var collegeCode;
			var degree;
			var grdDate;
			var collegeCity;
			var collegeState;
			var appCInfoColleges = profileData[0].data.colleges;
			if (appCInfoColleges.length != 0) {
				for (var c = 0; c < appCInfoColleges.length; c++) {
					collegeCode = appCInfoColleges[c].collegeCode;
					degree = appCInfoColleges[c].degree;
					collegeCity = appCInfoColleges[c].city;
					collegeState = appCInfoColleges[c].state;
					grdDate = appCInfoColleges[c].gradDate;
				}
			} else {
				collegeCode = "";
				degree = "";
				grdDate = "";
				collegeCity = "";
				collegeState = "";
			}
			var appInterestPN = profileData[0].data.primaryInterest;
			var appInterestSN = profileData[0].data.secondaryInterest;
			var appInterestLevelN = profileData[0].data.levelOfInterest;
			var appInterestFactor = profileData[0].data.factorForChoosing;
			var appInterestOtherN = profileData[0].data.otherInterests;

			var applicationSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + appPerDateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Gender:</h2></td>' + '<td align="left"><h3>' + appPerGender + '</h3></td></tr>';
			if (appPerRaceDes != '' && appPerRaceDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Race:</h2></td>' + '<td align="left"><h3>' + appPerRaceDes + '</h3></td></tr>';
			}
			if (appPerEthnicityDes != '' && appPerEthnicityDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Ethnicity:</h2></td>' + '<td align="left"><h3>' + appPerEthnicityDes + '</h3></td></tr>';
			}
			if (appPerSsn != '' && appPerSsn != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>SSN:</h2></td>' + '<td align="left"><h3>' + appPerSsn + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Address:</h2></td>' + '<td align="left"><h3>' + address + '<br />' + appContCity + ',' + appContStateDes + '<br />' + zipAndCountry + '</h3></td></tr>';
			if (appContEmailId != '' && appContEmailId != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + appContEmailId + '</h3></td></tr>';
			}
			if (appContPhoneNo1 != '' && appContPhoneNo1 != null) {
				appContPhoneNo1 = appContPhoneNo1.replace(/-|\(|\)/g, "");
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone1:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo1 + '</h3></td></tr>';
			}
			if (appContPhoneNo2 != '' && appContPhoneNo2 != null) {
				appContPhoneNo2 = appContPhoneNo2.replace(/-|\(|\)/g, "");
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone2:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo2 + '</h3></td></tr>';
			}
			if (appParRelation1Des != '' && appParRelation1Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 1 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation1Des + '<br />' + parent1Details + '</h3></td></tr>';
			}
			if (appParRelation2Des != '' && appParRelation2Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 2 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation2Des + '<br />' + parent2Details + '</h3></td></tr>';
			}
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Visa Details:</h2></td>' + '<td align="left"><h3>' + appVisaType + '<br />' + appVisaNationalityDes + ',' + appVisaNo + '</h3></td></tr>';
			}
			if (appSchoolName != '' && appSchoolCodeDes != '' && appSchoolGpa != '' && appSchoolGYear != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>School Details:</h2></td>' + '<td align="left"><h3>' + appSchoolName + '<br />' + appSchoolCodeDes + appSchoolGpa + ',' + appSchoolGYear + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Term Code:</h2></td>' + '<td align="left"><h3>' + appAdTermCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Level Code:</h2></td>' + '<td align="left"><h3>' + appAdLevelCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Major Code:</h2></td>' + '<td align="left"><h3>' + appAdMajorCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Student Type:</h2></td>' + '<td align="left"><h3>' + appAdStudentTypeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Residence Code:</h2></td>' + '<td align="left"><h3>' + appAdResidenceCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + appAdCollegeCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + appAdDegreeCodeDes + '</h3></td></tr>';
			if (appAdAdmissionTypeDes != '' && appAdAdmissionTypeDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Admission Type:</h2></td>' + '<td align="left"><h3>' + appAdAdmissionTypeDes + '</h3></td></tr>';
			}
			if (appAdDepartmentDes != '' && appAdDepartmentDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Department:</h2></td>' + '<td align="left"><h3>' + appAdDepartmentDes + '</h3></td></tr>';
			}
			if (appAdCampusDes != '' && appAdCampusDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Campus:</h2></td>' + '<td align="left"><h3>' + appAdCampusDes + '</h3></td></tr>';
			}
			if (appAdEduGoalDes != '' && appAdEduGoalDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Education Goal:</h2></td>' + '<td align="left"><h3>' + appAdEduGoalDes + '</h3></td></tr>';
			}
			if (testCode != '' && testCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Code:</h2></td>' + '<td align="left"><h3>' + testCode + '</h3></td></tr>';
			}
			if (testDate != '' && testDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Date:</h2></td>' + '<td align="left"><h3>' + testDate + '</h3></td></tr>';
			}
			if (testScore != '' && testScore != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Score:</h2></td>' + '<td align="left"><h3>' + testScore + '</h3></td></tr>';
			}
			if (collegeCode != '' && collegeCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + collegeCode + '</h3></td></tr>';
			}
			if (collegeCity != '' && collegeState != '') {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Address:</h2></td>' + '<td align="left"><h3>' + collegeCity + ',' + collegeState + '</h3></td></tr>';
			}
			if (degree != '' && degree != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + degree + '</h3></td></tr>';
			}
			if (grdDate != '' && grdDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Graduation Date:</h2></td>' + '<td align="left"><h3>' + grdDate + '</h3></td></tr>';
			}
			if (appInterestPN != '' && appInterestPN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Primary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestPN + '</h3></td></tr>';
			}
			if (appInterestSN != '' && appInterestSN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Secondary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestSN + '</h3></td></tr>';
			}
			if (appInterestLevelN != '' && appInterestLevelN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Level Of Interest:</h2></td>' + '<td align="left"><h3>' + appInterestLevelN + '</h3></td></tr>';
			}
			if (appInterestFactor != '' && appInterestFactor != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Factor For Choosing:</h2></td>' + '<td align="left"><h3>' + appInterestFactor + '</h3></td></tr>';
			}
			if (appInterestOtherN != '' && appInterestOtherN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Other Interests:</h2></td>' + '<td align="left"><h3>' + appInterestOtherN + '</h3></td></tr>';
			}
			if (appBannerId != '' && appBannerId != undefined) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Banner ID:</h2></td>' + '<td align="left"><h3>' + appBannerId + '</h3></td></tr>';
			}
			if (applicationStatus != '' && applicationStatus != undefined) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Application Status:</h2></td>' + '<td align="left"><h3>' + applicationStatus + '</h3></td></tr>';
			}
			if (appComments != '' && appComments != undefined) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Comments:</h2></td>' + '<td align="left"><h3>' + appComments + '</h3></td></tr></table>';
			}

			Ext.getCmp('leadAppView').setHtml(applicationSummaryHtml);
		},

		enrouteLogout: function() {
			//Removing pin store
			var pinStore = mobEdu.util.getStore('mobEdu.reg.store.pin');
			pinStore.getProxy().clear();
			pinStore.sync();

			var enrouteStore = mobEdu.util.getStore('mobEdu.enroute.main.store.enroute');
			var rec = enrouteStore.getAt(0);
			rec.set('recruiterId', null);
			rec.set('authString', null);
			rec.set('role', null);
			enrouteStore.sync();

			window.open('index.html', '_self');
		},

		senchaChangeOrientation: function() {
			if (Ext.getOrientation() == "landscape") {
				popup.setWidth("75%");
				popup.setHeight("55%");
			} else {
				popup.setWidth("80%");
				popup.setHeight("60%");
			}
		},


		hidePopup: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadDetail);
			var popUp = Ext.getCmp('leadsPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		allLeadsHidePopup: function() {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showAllLeadDetail);
			var popUp = Ext.getCmp('allLeadsPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		showEnrouteLogin: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showRecruiterMainView);
			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.enroute.login.view.phone.login');
			} else {
				mobEdu.util.show('mobEdu.enroute.login.view.tablet.login');
			}
		},
		checkForSearchListEnd: function(store, records, isSuccessful) {
			var pageSize = store.getPageSize();
			var pageIndex = store.currentPage - 1; // Page numbers start at 1

			if (isSuccessful && records.length < pageSize) {
				//Set count to disable 'loading' message
				var totalRecords = pageIndex * pageSize + records.length;
				store.setTotalCount(totalRecords);
			} else
				store.setTotalCount(null);

		},
		acceptAppointment: function(value) {
			if (value == 'Accept') {
				Ext.Msg.show({
					id: 'aAccept',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Accepted.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.reqAppListBack();
						}
					}
				});
			}
			if (value == 'Decline') {
				Ext.Msg.show({
					id: 'aDecline',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Declined.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enroute.main.f.reqAppListBack();
						}
					}
				});
			}
			var status;
			var appDetail = mobEdu.util.getStore('mobEdu.enroute.main.store.appointmentDetail');
			mobEdu.util.get('mobEdu.enroute.main.view.reqAppointmentDetail');
			Ext.getCmp('raccept').hide();
			Ext.getCmp('rdecline').hide();

			var detailData = appDetail.data.all;
			var appId = detailData[0].data.appointmentID;
			var acceptance;
			if (value == 'Accept') {
				acceptance = 'Y';
				status = 'Scheduled';
			} else {
				acceptance = 'N';
				status = 'declined';

			};
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.main.store.racceptance');
			newAppStore.getProxy().setUrl(commonwebserver + 'appointment/statusUpdate');
			newAppStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			newAppStore.getProxy().setExtraParams({
				appointmentID: appId,
				acceptance: acceptance,
				companyId: companyId,
				status: status
			});
			newAppStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.acceptanceResponseHandler();
			};
			newAppStore.load();
		},
		acceptanceResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enroute.main.store.racceptance');
		},

		loadLeadSearchReportsMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearch');
			store.removeAll();
			store.sync();
			if (store.data.length == 0) {
				store.add({
					title: 'By State'
				});
				store.add({
					title: 'By High School'
				});
				store.add({
					title: 'By Rank'
				});
				store.sync();
			}
		},
		showSearchMenu: function() {
			var supLeadsListStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			supLeadsListStore.removeAll();
			mobEdu.util.get('mobEdu.enroute.main.view.leadSearchMenu');
			Ext.getCmp('leadScList').setValue('');
			Ext.getCmp('leadStList').setValue('');
			Ext.getCmp('leadRkList').setValue('');
			mobEdu.enroute.main.f.showLeadSearchMenu();
		},

		showLeadSearchMenu: function() {
			if (mobEdu.enroute.main.f.leadupdateFlag == true) {
				mobEdu.enroute.main.f.leadupdateFlag = false;
				mobEdu.enroute.main.f.supSearchLeadsListRefresh();
			} else {
				mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showSupLeadsLists);
				mobEdu.util.show('mobEdu.enroute.main.view.leadSearchMenu');
			}
		},

		loadStateEnumerations: function() {
			var enumType = 'STVSTAT';
			//            var roleId = mobEdu.util.getRoleId();
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			store.removeAll();
			store.removed = [];
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.stateEnumerations');
			enuStore.removeAll();
			enuStore.removed = [];
			enuStore.sync();
			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enrouteWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.loadStateEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadStateEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enroute.main.store.stateEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadLeadStatePopup: function() {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('leadStList').blur();
			if (mobEdu.enroute.main.view.leadSearchByState.clearIconTapped) {
				mobEdu.enroute.main.view.leadSearchByState.clearIconTapped = false;
				return;
			}
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.hideStatePopup);
			var popupView = mobEdu.util.get('mobEdu.enroute.main.view.leadSearchByState');
			Ext.Viewport.add(popupView);

		},

		loadLeadSchoolPopup: function() {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('leadScList').blur();
			if (mobEdu.enroute.main.view.leadSearchByState.clearIconTapped) {
				mobEdu.enroute.main.view.leadSearchByState.clearIconTapped = false;
				return;
			}
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.hideSchoolPopup);
			var popupView = mobEdu.util.get('mobEdu.enroute.main.view.leadSearchBySchool');
			Ext.Viewport.add(popupView);
		},

		loadLeadRankPopup: function() {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('leadRkList').blur();
			if (mobEdu.enroute.main.view.leadSearchByRank.clearIconTapped) {
				mobEdu.enroute.main.view.leadSearchByRank.clearIconTapped = false;
				return;
			}
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.hideRankPopup);
			var popupView = mobEdu.util.get('mobEdu.enroute.main.view.leadSearchByRank');
			Ext.Viewport.add(popupView);
		},

		hideStatePopup: function(record) {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
			var popUp = Ext.getCmp('statePopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.enroute.main.view.leadSearchByState');
			}
			Ext.getCmp('leadStList').blur();
			mobEdu.util.getStore('mobEdu.enroute.main.store.stateEnumerations').clearFilter();

		},

		hideSchoolPopup: function(record) {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
			var popUp = Ext.getCmp('schoolPopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.enroute.main.view.leadSearchBySchool');
			}
			Ext.getCmp('leadScList').blur();
			mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList').clearFilter();
		},

		hideRankPopup: function(record) {
			mobEdu.util.updatePrevView(mobEdu.enroute.main.f.showLeadSearchMenu);
			var popUp = Ext.getCmp('rankPopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.enroute.main.view.leadSearchByRank');
			}
			Ext.getCmp('leadRkList').blur();
			mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList').clearFilter();
		},

		onStateKeyup: function(textfield, e) {
			//Restricting TAB key
			if (e.browserEvent.keyCode != 9) {
				mobEdu.enroute.main.f.doCheckBeforeSubjectSearch(textfield);
			}
		},
		onSchoolKeyup: function(textfield, e) {
			if (e.browserEvent.keyCode != 9) {
				mobEdu.enroute.main.f.doCheckBeforeSchoolSearch(textfield);
			}
		},
		onRankKeyup: function(textfield, e) {
			if (e.browserEvent.keyCode != 9) {
				mobEdu.enroute.main.f.doCheckBeforeRankSearch(textfield);
			}
		},

		doCheckBeforeRankSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList');
			if (length > 1) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						store.clearFilter();
						searchString = searchString.toLowerCase();
						store.filter(function(record) {
							var subStr = (record.get('actScore'));
							if (subStr.indexOf(searchString) !== -1) {
								return true;
							}
							return false;
						});
					}
				}, 1000);
			} else {
				store.clearFilter();
			}
		},
		doCheckBeforeSchoolSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList');
			if (length > 1) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						store.clearFilter();
						searchString = searchString.toLowerCase();
						store.filter(function(record) {
							var subStr = (record.get('highSchool')).toLowerCase();
							if (subStr.indexOf(searchString) !== -1) {
								return true;
							}
							return false;
						});
					}
				}, 1000);
			} else {
				store.clearFilter();
			}
		},

		doCheckBeforeSubjectSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.stateEnumerations');
			if (length > 1) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						store.clearFilter();
						searchString = searchString.toLowerCase();
						store.filter(function(record) {
							var subStr = (record.get('displayValue')).toLowerCase();
							if (subStr.indexOf(searchString) !== -1) {
								return true;
							}
							return false;
						});
					}
				}, 1000);
			} else {
				store.clearFilter();
			}
		},

		onStateItemClear: function(textfield, e) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length == 0 || length == '') {
				mobEdu.enroute.main.f.doCheckBeforeSubjectSearch(textfield);
			}
		},

		onSchoolItemClear: function(textfield, e) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length == 0 || length == '') {
				mobEdu.enroute.main.f.doCheckBeforeSchoolSearch(textfield);
			}
		},

		onRankItemClear: function(textfield, e) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length == 0 || length == '') {
				mobEdu.enroute.main.f.doCheckBeforeRankSearch(textfield);
			}
		},
		onStateItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			// mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
			mobEdu.enroute.main.f.rinfstateCode = record.get('enumValue');
			Ext.getCmp('leadStList').setValue(record.get('displayValue'));
			mobEdu.enroute.main.f.hideStatePopup(record);
			mobEdu.enroute.main.f.loadSupLeadsSearchList('addresses.state', record.get('enumValue'));
		},

		onSchoolItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			Ext.getCmp('leadScList').setValue(record.get('highSchool'));
			mobEdu.enroute.main.f.hideSchoolPopup(record);
			mobEdu.enroute.main.f.loadSupLeadsSearchList('highSchool', record.get('highSchool'));
		},

		onRankItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			Ext.getCmp('leadRkList').setValue(record.get('actScore'));
			mobEdu.enroute.main.f.hideRankPopup(record);
			mobEdu.enroute.main.f.loadSupLeadsSearchList('actScore', record.get('actScore'));
		},

		getLeadSchoolList: function(field) {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			store.removeAll();
			store.removed = [];
			var searchItem = '';
			var reqId = 0;
			var schoolStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList');
			schoolStore.removeAll();
			schoolStore.removed = [];
			schoolStore.sync();

			schoolStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			schoolStore.getProxy().setUrl(enrouteWebserver + '/enroute/lead/getDistinctSchoolList');
			schoolStore.getProxy().setExtraParams({
				searchField: field,
				recruiterID: reqId,
				limit: 500,
				companyId: companyId
			});

			schoolStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.leadSchoolListResponseHandler();
			};
			schoolStore.load();
		},

		getLeadsByRank: function(field) {
			var store = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			store.removeAll();
			store.removed = [];
			var searchItem = '';
			var reqId = 0;
			var schoolStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList');
			schoolStore.removeAll();
			schoolStore.removed = [];
			schoolStore.sync();

			schoolStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			schoolStore.getProxy().setUrl(enrouteWebserver + '/enroute/lead/getLeadsByRank');
			schoolStore.getProxy().setExtraParams({
				searchField: field,
				recruiterID: reqId,
				limit: 500,
				companyId: companyId
			});

			schoolStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.leadSchoolListResponseHandler();
			};
			schoolStore.load();
		},


		leadSchoolListResponseHandler: function() {
			var schoolStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSchoolList');
			var storeStatus = schoolStore.getProxy().getReader().rawData;
			if (storeStatus.status == 'success') {
				//Ext.getCmp('leadSchoolList').showPicker();
			} else {
				Ext.Msg.show({
					message: storeStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadSupLeadsSearchList: function(selectedFld, selectedVal) {
			var searchItem = '';
			var reqId = 0;
			var leadsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			leadsStore.removeAll();
			leadsStore.removed = [];
			leadsStore.sync();
			var srchType = null;
			if (selectedFld != null && selectedFld != '' && selectedVal != null && selectedVal != '') {
				srchType = selectedFld + ',' + selectedVal;
			}
			leadsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			leadsStore.getProxy().setUrl(enrouteWebserver + 'enroute/lead/getUnAssignedLeads');
			leadsStore.getProxy().setExtraParams({
				searchText: searchItem,
				searchType: srchType,
				recruiterID: reqId,
				limit: 500,
				companyId: companyId
			});

			leadsStore.getProxy().afterRequest = function() {
				mobEdu.enroute.main.f.supLeadsSearchListResponseHandler();
			};
			leadsStore.load();
		},

		supLeadsSearchListResponseHandler: function() {
			var leadsStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			leadsStore.sort("versionDate", "DESC");
			var leadStatus = leadsStore.getProxy().getReader().rawData;
			if (leadStatus.status == 'success') {
				mobEdu.enroute.main.f.leadupdateFlag = false;
			} else {
				Ext.Msg.show({
					message: leadStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		searchLeadOnMenutap: function(value) {
			if (value == 'By State') {
				Ext.getCmp('leadStList').setHidden(false);
				Ext.getCmp('leadScList').setHidden(true);
				Ext.getCmp('leadRkList').setHidden(true);
				mobEdu.enroute.main.f.loadStateEnumerations();
			} else if (value == 'By High School') {
				Ext.getCmp('leadStList').setHidden(true);
				Ext.getCmp('leadRkList').setHidden(true);
				Ext.getCmp('leadScList').setHidden(false);
				mobEdu.enroute.main.f.getLeadSchoolList('highSchool');
			} else {
				Ext.getCmp('leadStList').setHidden(true);
				Ext.getCmp('leadScList').setHidden(true);
				Ext.getCmp('leadRkList').setHidden(false);
				mobEdu.enroute.main.f.getLeadsByRank('actScore');
			}

		},

		generateMsg: function(value, rid, lid) {
			var eStore = mobEdu.util.getStore('mobEdu.enroute.main.store.assignLead');
			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/leadSystemGenerate');
			eStore.getProxy().setExtraParams({
				companyId: companyId,
				informatoion: value,
				recruiterID: rid,
				leadID: lid
			});
			eStore.getProxy().afterRequest = function() {
				//mobEdu.enquire.f.loadEmailResponseHandler();
			};
			eStore.load();
		},

		supSearchLeadsListRefresh: function() {
			var supLeadsListStore = mobEdu.util.getStore('mobEdu.enroute.main.store.leadSearchList');
			supLeadsListStore.load();
			mobEdu.enroute.main.f.showLeadSearchMenu();
		},
		isOff: function(value) {
			if (value == 'off') {
				return true;
			} else {
				return false;
			}
		}
	}
});