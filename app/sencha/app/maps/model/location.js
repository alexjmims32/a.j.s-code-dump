Ext.define('mobEdu.maps.model.location', {
	extend: 'Ext.data.Model',

	config: {
		fields: [{
			name: 'principal'
		}, {
			name: 'website'
		}, {
			name: 'address'
		}, {
			name: 'phone'
		}, {
			name: 'email'
		}, {
			name: 'latitude'
		}, {
			name: 'longitude'
		}, {
			name: 'city'
		}, {
			name: 'state'
		}, {
			name: 'category'
		}, {
			name: 'notes'
		}, {
			name: 'img',
			mapping: 'imgUrl'
		}, {
			name: 'buildingCode'
		}, {
			name: 'buildingDesc'
		}, {
			name: 'buildingName'
		}, {
			name: 'campusCode'
		}, {
			name: 'mapId',
			mapping: 'mapId'
		}, {
			name: 'title'
		}, {
			name: 'centerFlag'
		}, {
			name: 'icon',
		}, {
			name: 'filterId'
		}, {
			name: 'filter',
			mapping: 'showByDefault'
		}]
	}
});