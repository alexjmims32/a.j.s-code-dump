Ext.define('mobEdu.maps.view.locationDetails', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.maps.store.locationDetails'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        items: [{
            layout:'fit',
            scrollable:'false',
            items:[{
                xtype:'container',
                layout:'fit',
                id:'generalDetails',
                name:'generalDetails',
                scrollable: false,
                items:[{
                    xtype: 'dataview',
                    padding: '5 0 0 5',
                    cls: 'logo',
                    disableSelection: true,
                    deferEmptyText: false,
                    emptyText: '<center><h3>No Details</h3><center>',
                    itemTpl: new Ext.XTemplate('<table width="100%" align="center">',
                        '<tpl if="(mobEdu.maps.f.isImgExists(img)===true)">',
                        '<tr><td><img class="imageWidth" src="{img}"/></td></tr>',
                        '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isBuildingNameExists(buildingName)===true)">',
                        '<tr><td ><h2>{buildingName}</h2></td></tr>',   '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isCategoryExists(category)===true)">',
                        '<tr><td ><h3>{[mobEdu.maps.f.getCategoryDesc(values.category)]}</h3></td></tr>', '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isPhoneExists(phone)===true)">',
                        '<tr><td ><h3>Phone: <a class="hypeLink" href="tel:{phone}">{phone}</a></h3></td></tr>',
                        '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isEmailExists(email)===true)">',
                        '<tr><td ><h3>Email: <a class="hypeLink" href="mailto:{email}" >{email}</a></h3></td></tr>',
                        '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isAddressExists(address)===true)">',
                        '<tr><td ><h3>{address}</h3></td></tr>', '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isPrincipalExists(principal)===true)">',
                        '<tr><td ><h2>Principal: {principal}</h2></td></tr>', '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isDescriptionExists(buildingDesc)===true)">',
                        '<tr><td ><h3>{[decodeURIComponent(values.buildingDesc)]}</h3></td></tr>', '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isWebsiteExists(website)===true)">',
                        '<tr><td ><h3>{website}</h3></td></tr>', '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isShowWalkingDirections()===true)">',
                        '<tr><td><a href="javascript:mobEdu.maps.f.getMapDirections({latitude},{longitude},null,\'DRIVING\')"><p class="getDirections">Get Driving Directions</p></a></td></tr>',
                        '<tr><td><a href="javascript:mobEdu.maps.f.getMapDirections({latitude},{longitude},null,\'WALKING\')"><p class="getDirections">Get Walking Directions</p></a></td></tr>',
                        '<tpl else>',
                        '<tr><td><a href="javascript:mobEdu.maps.f.getMapDirections({latitude},{longitude})"><p class="getDirections">Get Directions</p></a></td></tr>',
                        '</tpl>',
                        '<tpl if="(mobEdu.maps.f.isNotesExists(notes)===true)">',
                        '<tr><td><h2>Notes:</h2><h3>{notes}</h3></td></tr>', '</tpl>',
                        '</table>'),
                    store: mobEdu.util.getStore('mobEdu.maps.store.locationDetails'),
                    singleSelect: true,
                    loadingText: ''
                }]
            }]
        },{
            xtype:'container',
            id:'detailsFromUrl',
            scrollable: 'vertical'
        }, {
            title: '<h1>Location Details</h1>',
            xtype: 'customToolbar',
            id: 'locationDetailsTitle'
        }],
        flex: 1
    }
});