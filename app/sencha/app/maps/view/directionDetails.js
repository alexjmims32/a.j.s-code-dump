Ext.define('mobEdu.maps.view.directionDetails', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        id: 'mapDirInst',
        padding:'10px',
        html: '',
        items: [{
            title: '<h1>Direction Instructions</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});