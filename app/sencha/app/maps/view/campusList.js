Ext.define('mobEdu.maps.view.campusList', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.maps.store.campusList'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'mapCampusList',
            itemTpl: '<table width="100%"><tr><td width="80%" align="left" ><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.maps.store.campusList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.maps.f.onCampusItemTap(view, index, record);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'mapCampusTitle'
        }],
        flex: 1
    }
});