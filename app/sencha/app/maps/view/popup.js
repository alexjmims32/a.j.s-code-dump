Ext.define('mobEdu.maps.view.popup', {
    extend:'Ext.Panel',
	requires:[
		'mobEdu.notif.store.local'
	],
    config:{
        id:'mapsPopup',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        cls:'logo',
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'38%',
        height:'16%',
        layout:'fit',
        items:[
        {
            padding:'0 0 5 8',
            id:'markerInfo',
            xtype:'panel'
//            cls:'logo'
        }
        ],
        flex:1
    }
});