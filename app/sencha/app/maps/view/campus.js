Ext.define('mobEdu.maps.view.campus', {
	extend: 'Ext.Panel',
	config: {
		scrollable: 'none',
		fullscreen: true,
		layout: {
			type: 'vbox'
		},
		items: [{
			xtype: 'customToolbar',
			id: 'campusTitle',
			title: '<h1>Campus Map</h1>',
			docked: 'top'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				xtype: 'button',
				text: 'Campuses',
				handler: function() {
					mobEdu.maps.f.showSelect();
				}
			}, {
				xtype: 'button',
				text: 'Search',
				handler: function() {
					mobEdu.maps.f.loadSearch();
				}
			}, {
				xtype: 'button',
				text: 'Browse',
				handler: function() {
					mobEdu.maps.f.loadBrowseCampuses();
				}
			}, {
				xtype: 'button',
				text: 'Filter',
				handler: function() {
					mobEdu.maps.f.loadFilter('mobEdu.maps.f.showFilter');
				}
			}]
		}, {
			xtype: 'googleMap',
			id: 'campMaps',
			flex: 1
		}]
	}
});