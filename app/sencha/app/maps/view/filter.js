Ext.define('mobEdu.maps.view.filter', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.maps.store.filter'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'filterCampusList',
            itemCls: 'mapfilter',
            itemTpl: new Ext.XTemplate(
                '<div style="display: inline-block; background: transparent; padding-right: 5px"><div class="outerContainer"><div class="innerContainer">',
                '<tpl if="filter===true">',
                '<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 name="on" width=24/>' +
                '<tpl else>',
                '<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 name="off" width=24/>' +
                '</tpl> </div></div></div>' +
                '<tpl if="(mobEdu.maps.f.isMapCustomIcons()===true)">',
                '<div style="display: inline-block;"><div class="outerContainer"><div class="innerContainer"><img style="padding-right: 5px;" src="' + mobEdu.util.getResourcePath() + 'images/maps/{[mobEdu.maps.f.getMapIcon(values.icon)]}.png" width=32 /></div></div></div>' +
                '</tpl>' +
                '<div style="display: inline-block;"><div class="outerContainer"><div class="innerContainer"><h3>{category}</h3></div></div></div>', {
                    formatImagePath: function(path) {
                        return path.replace(" ", "_");
                    }
                }
            ),

            store: mobEdu.util.getStore('mobEdu.maps.store.filter'),
            disableSelection: true,
            scrollToTopOnRefresh: false,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.maps.f.onFilterItemTap(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>Filter Map Features</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                xtype: 'button',
                text: 'Done',
                handler: function() {
                    mobEdu.maps.f.onFilterBackTap();
                }
            }]
        }],
        flex: 1
    }
});