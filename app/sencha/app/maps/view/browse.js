Ext.define('mobEdu.maps.view.browse', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.maps.store.browse'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'browseCampusList',
			grouped: true,
			itemTpl: '<table width="100%"><tr><td width="80%" align="left" ><h3>{buildingName} - {buildingCode} <tpl if="(mobEdu.maps.f.isShowCampusCodes()===false)"> <tpl else>({campusCode})</tpl></h3></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.maps.store.browse'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					mobEdu.util.deselectSelectedItem(index, view);
					mobEdu.maps.f.loadLocationDetails(record, mobEdu.maps.f.showBrowseCampuses);
				}
			}
		}, {
			xtype: 'customToolbar',
			id: 'browseTitle',
			name: 'browseTitle',
			title: '<h1>Browse All Campuses</h1>',
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			id: 'mapsToolbar',
			name: 'mapsToolbar',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Maps',
				handler: function() {
					mobEdu.maps.f.onMapButtonTap();
				}
			}]
		}],
		flex: 1
	}
});