Ext.define('mobEdu.maps.view.select', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.maps.store.campusList'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'selectCampusList',
            itemTpl: '<table width="100%"><tr><td width="80%" align="left" ><h3>{title}</h3></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.maps.store.campusList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.maps.f.onCampusItemTap(view, index, record);
                }
            }
        }, {
            xtype: 'customToolbar',
            title:'<h1>Select a Campus</h1>',
        }],
        flex: 1
    }
});