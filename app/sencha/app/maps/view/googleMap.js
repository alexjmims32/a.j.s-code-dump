Ext.define("mobEdu.maps.view.googleMap", {
	extend: "Ext.Map",
	xtype: 'googleMap',
	config: {

		mapOptions: {
			center: new google.maps.LatLng(mapInitLatitude, mapInitLongitude),
			zoom: 15,

			panControl: false,
			zoomControl: true,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
		},
		useCurrentLocation: false,
		listeners: {
			// This event is called the first time map control is actually rendered
			// We will start loading our apps after the map component is rendered
			// to eliminate timing issues with setting the pins on the map component.
			'maprender': function(me, map, eOpts) {
				mobEdu.maps.f.mapRendered = true;
				if (mobEdu.util.getDefaultMapView() == 2) {
					mobEdu.maps.f.campusCodesResponseHandler();
				} else {
					mobEdu.maps.f.loadCampusCodes();
				}
			}
		}
	},
});