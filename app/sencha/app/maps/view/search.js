Ext.define('mobEdu.maps.view.search', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.maps.store.search'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			deferEmptyText: false,
			id: 'searchCampusList',
			emptyText: '',
			itemTpl: '<table width="100%"><tr><td width="100%" align="left" ><h3>{buildingName} - {buildingCode} <tpl if="(mobEdu.maps.f.isShowCampusCodes()===false)"> <tpl else>({campusCode})</tpl></h3></td><td><div align="right" class="arrow" /></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.maps.store.search'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					mobEdu.util.deselectSelectedItem(index, view);
					mobEdu.maps.f.loadLocationDetails(record, mobEdu.maps.f.showSearch);
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>Search in all Campuses</h1>',
		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset',
			items: [{
				layout: 'hbox',
				items: [{
					xtype: 'textfield',
					name: 'txtSearchCampus',
					id: 'txtSearchCampus',
					width: '100%',
					placeHolder: 'Find building by name or code',
					listeners: {
						keyup: function(textfield, e, eOpts) {
							mobEdu.maps.f.onSearchKeyup(textfield, e, Ext.getCmp('searchCampusList'));
						},
						clearicontap: function(textfield, e, eOpts) {
							mobEdu.maps.f.onSearchItemClear(textfield, e, Ext.getCmp('searchCampusList'));
						}
					}
				}]
			}]
		}],
		flex: 1
	}
});