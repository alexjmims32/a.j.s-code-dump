Ext.define('mobEdu.maps.view.directionMap', {
    extend: 'Ext.Container',
    requires: 'Ext.Map',
    config: {
        title: 'Location',
        iconCls: 'locate',
        layout: 'fit',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Directions</h1>'
        }, {
            xtype: 'map',
            height:'100%',
            width:'100%',
            id: 'directionMap',
            mapOptions: {
                //                zoom: 20,
                center: new google.maps.LatLng(mapInitLatitude,mapInitLongitude),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            },
            mapRendererOptions: {
                draggable: true,
                panel: document.getElementById('map-directions'),
                hideRouteList: true
            },
            listeners:{
                maprender:function(mapRef, map, eOpts ){
                    mobEdu.maps.f.setDirection(mapRef);
                }
            }
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                ui: 'button',
                text: 'Get Instructions',
                handler: function() {
                    mobEdu.maps.f.getDirInstructions();
                }
            }]
        }]
    }
});