Ext.define('mobEdu.maps.f', {
	requires: [
		'mobEdu.maps.store.campusLocal',
		'mobEdu.maps.store.campus',
		'mobEdu.maps.store.browse',
		'mobEdu.maps.store.filter',
		'mobEdu.maps.store.html',
		'mobEdu.maps.store.locationDetails',
		'mobEdu.maps.store.search',
		'mobEdu.maps.store.campusList',
		'mobEdu.maps.store.location'
	],
	statics: {
		infoBubble: null,
		bound: null,
		center: null,
		categoriesHash: null,
		mapRendered: false,
		directionTravelType: 'DRIVING',
		setLocations: function(mapRef) {
			var store = mobEdu.util.getStore('mobEdu.maps.store.campus');

			// Clear all Existing markers
			for (var i = 0; i < markersArray.length; i++) {
				markersArray[i].setMap(null);
			}
			markersArray = [];

			mobEdu.maps.f.bound = new google.maps.LatLngBounds();
			var centerflag = false;
			store.each(function(record) {
				if (mobEdu.maps.f.isFilterOn(record.get('category'))) {
					mobEdu.maps.f.addPoint(record, centerflag, mapRef);
				}
			});

			// If there are no buildings found to be shown after applying the filter
			// Throw a message. Return here will avoid the blue screen being shown instead of map
			if (markersArray.length === 0) {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>No buildings matching selected filters.</center>',
					buttons: Ext.MessageBox.OK
				});
				return;
			}
			if (mobEdu.maps.f.center != null && mobEdu.maps.f.center != '') {
				var latLng = (mobEdu.maps.f.center).getPosition(); // returns LatLng object
				mapRef.setMapCenter(latLng); // setCenter takes a LatLng object
				mobEdu.maps.f.center = null;
			} else {
				mapRef.setMapCenter((mobEdu.maps.f.bound).getCenter());
			}

			mapRef.getMap().setZoom(zoomValue);
		},

		getCategoryDesc: function(category) {
			var categoryDesc = '';
			var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
			if (filterStore.data.length == 0) {
				mobEdu.maps.f.loadFilter();
			}
			var catList = category.split(',');
			for (var cnt = 0; cnt < catList.length; cnt++) {
				var rec = filterStore.findRecord('filterId', catList[cnt]);
				if (rec != null) {
					if (categoryDesc !== '')
						categoryDesc = categoryDesc + ',' + rec.get('category');
					else
						categoryDesc = rec.get('category');
				}
			}
			return categoryDesc;
		},

		isFilterOn: function(category) {
			var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
			if (filterStore.data.length == 0) {
				mobEdu.maps.f.loadFilter();
			}
			var catList = category.split(',');
			for (var cnt = 0; cnt < catList.length; cnt++) {
				var rec = filterStore.findRecord('filterId', catList[cnt]);
				if (rec != null) {
					if (rec.get('filter') == true) {
						return true;
					}
				}
			}
			return false;
		},

		addPoint: function(record, centerflag, mapRef) {
			if (record.get('centerFlag') == 'true') {
				centerflag = true;
				mobEdu.maps.f.bound = new google.maps.LatLngBounds();
				(mobEdu.maps.f.bound).extend(new google.maps.LatLng(record.get('latitude'), record.get('longitude')));
			}
			if (centerflag == false) {
				(mobEdu.maps.f.bound).extend(new google.maps.LatLng(record.get('latitude'), record.get('longitude')));
			}

			var marker = null;
			if (mobEdu.maps.f.isMapCustomIcons()) {
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(record.get('latitude'), record.get('longitude')),
					title: 'Click to see ' + record.get('buildingName'),
					icon: mobEdu.util.getResourcePath() + 'images/maps/' + mobEdu.maps.f.getShowCategory(record.get('category')) + '.png',
					map: mapRef.getMap()
				});
			} else {
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(record.get('latitude'), record.get('longitude')),
					title: 'Click to see ' + record.get('buildingName'),
					map: mapRef.getMap()
				});
			}
			if (centerflag === true) {
				mobEdu.maps.f.center = marker;
			}

			mobEdu.maps.f.attachMessage(marker, record);
			markersArray.push(marker);
		},
		attachMessage: function(marker, record) {
			var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
			var categoryIds = record.get('category').split(',');
			var otherthanParkngCategryExst = false;
			var parkingCategroyExist = false;
			for (var cnt = 0; cnt < categoryIds.length; cnt++) {
				var categoryId = filterStore.find('filterId', categoryIds[cnt]);
				var categoryName = filterStore.data.items[categoryId].data.category.toString().toLowerCase();
				if (categoryName.search('parking') != -1) {
					parkingCategroyExist = true;
				} else {
					otherthanParkngCategryExst = true;
				}
			}
			//This will assume that parking lot as category 3
			var popupInfo = '';
			var popupInfoWithName = '';
			if (parkingCategroyExist == true && otherthanParkngCategryExst == false) {
				if (mobEdu.maps.f.isShowWalkingDirections() === true) {
					popupInfo = '<a href="javascript:mobEdu.maps.f.onDirectionsItemTap(' + record.get('latitude') + "," + record.get('longitude') + ',\'DRIVING\')"><div style="font-size: 1.2em;">Get Driving Directions</div></a>' + '<a href="javascript:mobEdu.maps.f.onDirectionsItemTap(' + record.get('latitude') + "," + record.get('longitude') + ',\'WALKING\')"><div style="font-size: 1.2em;">Get Walking Directions</div></a>';
				} else {
					popupInfo = '<a href="javascript:mobEdu.maps.f.onDirectionsItemTap(' + record.get('latitude') + "," + record.get('longitude') + ')"><div style="font-size: 1.2em;">Get Directions</div></a>';
				}
				popupInfoWithName = '<div style="width: 150px;font-size: 1.2em;"><b>' + record.get('buildingName') + '<b></div>' + popupInfo;

			} else {
				if (mobEdu.maps.f.isShowWalkingDirections() === true) {
					popupInfo = '<a href="javascript:mobEdu.maps.f.buildingInfo(\'' + mobEdu.maps.f.getBuildingInfoURL(record.get('buildingCode')) + '\');"><div style="font-size: 1.2em;">Building Info</div></a>' + '<a href="javascript:mobEdu.maps.f.onDirectionsItemTap(' + record.get('latitude') + "," + record.get('longitude') + ',\'DRIVING\')"><div style="font-size: 1.2em;">Get Driving Directions</div></a>' + '<a href="javascript:mobEdu.maps.f.onDirectionsItemTap(' + record.get('latitude') + "," + record.get('longitude') + ',\'WALKING\')"><div style="font-size: 1.2em;">Get Walking Directions</div></a>';
				} else {
					popupInfo = '<a href="javascript:mobEdu.maps.f.buildingInfo(\'' + mobEdu.maps.f.getBuildingInfoURL(record.get('buildingCode')) + '\');"><div style="font-size: 1.2em;">Building Info</div></a>' + '<a href="javascript:mobEdu.maps.f.onDirectionsItemTap(' + record.get('latitude') + "," + record.get('longitude') + ')"><div style="font-size: 1.2em;">Get Directions</div></a>';
				}
				popupInfoWithName = '<div style="font-size: 1.2em;"><b>' + record.get('buildingName') + '<b></div>' + popupInfo;
			}
			mobEdu.maps.f.infoBubble = new InfoBubble({
				shadowStyle: 1,
				padding: 10,
				borderRadius: 10,
				arrowSize: 25,
				borderWidth: 1,
				borderColor: '#ccc',
				maxWidth: '150px',
				maxHeight: '50px',
				disableAutoPan: true,
				hideCloseButton: false,
				arrowPosition: 30,
				backgroundClassName: 'phoney',
				pixelOffset: new google.maps.Size(130, 120),
				arrowStyle: 2
			});
			google.maps.event.addListener(marker, 'click', function(event) {
				if (showInfoWindow == 'true') {
					mobEdu.maps.f.infoBubble.setContent(popupInfoWithName);
					mobEdu.maps.f.infoBubble.open(marker.get('map'), marker);
				} else {
					mobEdu.maps.f.loadLocationDetails(record, mobEdu.maps.f.showCampus);
				}
			});

		},
		getBuildingInfoURL: function(buildingCode) {
			var infoUrl = infoWindowUrl;
			if (buildingCodeLower != null && buildingCodeLower == 'true') {
				buildingCode = buildingCode.toString().toLowerCase();
			}
			if (infoUrl.search('BUILDINGCODE') != -1) {
				infoUrl = infoUrl.replace('BUILDINGCODE', buildingCode);
			}
			return infoUrl;
		},
		getShowCategory: function(category) {
			var catList = category.split(',');
			if (catList.length >= 1) {
				var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
				if (filterStore.data.length == 0) {
					mobEdu.maps.f.loadFilter();
				}
				for (var cnt = 0; cnt < catList.length; cnt++) {
					var rec = filterStore.findRecord('filterId', catList[cnt]);
					if (rec != null) {
						if (rec.get('filter') == true) {

							return rec.get('icon') != null ? rec.get('icon') : 0;
							//return catList[cnt];
						}
					}
				}
				return catList[0];
			}
			return category;
		},

		loadCampusCodes: function(nextView, prevViewRef) {
			// Show the map view first so that the Map object is initialized
			// This need to be done before adding markers as the map will take few ms to initialize
			if (mobEdu.util.getDefaultMapView() == 1) {
				if (mainMenuLayout === 'BGRID') {
					mobEdu.util.get('mobEdu.maps.view.campus').setLayout('fit');
				}
				mobEdu.util.show('mobEdu.maps.view.campus');
			}
			var campusStore = mobEdu.util.getStore('mobEdu.maps.store.location');
			campusStore.clearFilter();
			if (mobEdu.util.getDefaultMapView() == 1) {
				if (campusStore.getCount() > 0) {
					mobEdu.maps.f.showCampus();
				}
			}
			campusStore.getServerProxy().setExtraParams({
				active: 'Y',
				clientCode: campusCode,
				page: 0
			});
			if (isMapsCampusCodeEnabled == 'true') {
				campusStore.getServerProxy().setExtraParam('campusCode', campusCode);
			}
			campusStore.loadServer(function() {
				mobEdu.maps.f.campusCodesResponseHandler(nextView, prevViewRef);
			});
			mobEdu.util.gaTrackEvent('encore', 'maps');
		},
		campusCodesResponseHandler: function(nextView, prevViewRef) {
			var defaultCampus = mobEdu.util.getDefaultCampus();

			// Populate the filters store
			var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
			filterStore.removeAll();
			mobEdu.maps.f.loadFilter();

			var campusListStore = mobEdu.util.getStore('mobEdu.maps.store.campusList');
			campusListStore.removeAll();
			campusListStore.sync();

			// Copy from proxy store to local store
			var campusStore = mobEdu.util.getStore('mobEdu.maps.store.location');

			if (campusStore.data.length > 0) {

				var campusLocal = mobEdu.util.getStore('mobEdu.maps.store.campusLocal');
				if (campusLocal.data.length > 0) {
					campusLocal.removeAll();
					campusLocal.sync();
				}
				var campusCodes = new Array();
				var campusTitle = new Array();
				var codes = new Array();
				var index = 0;
				campusLocal.add(campusStore.getRange());
				campusLocal.sync();
				campusStore.each(function(record) {
					if (campusCodes.length > 0) {
						if (!(Ext.Array.contains(campusCodes, record.data.campusCode))) {
							campusCodes[index] = record.data.campusCode;
							campusTitle[index] = record.data.title;
							index = index + 1;
							campusListStore.add({
								'title': record.data.title,
								'campusCode': record.data.campusCode
							});
							campusStore.sync();
						}
					} else {
						campusCodes[index] = record.data.campusCode;
						campusTitle[index] = record.data.title;
						index = index + 1;
						if (record.data.title != undefined || record.data.campusCode != undefined) {
							campusListStore.add({
								'title': record.data.title,
								'campusCode': record.data.campusCode
							});
							campusListStore.sync();
						}
					}

				});
				if (mobEdu.util.isShowMapSubMenu() == true) {
					Ext.getCmp('campusCodes').hide();
					mobEdu.maps.f.showMapsSubMenu();
				} else {
					var rec = null;
					if (defaultCampus != null && defaultCampus != '') {
						rec = campusListStore.findRecord('campusCode', defaultCampus);
					} else {
						rec = campusListStore.getAt(0);
					}
					if (rec != null) {
						
						
						if (nextView != null) {
							mobEdu.maps.f.loadBrowseCampuses(prevViewRef);
						} else {
							mobEdu.util.get('mobEdu.maps.view.campus');
							Ext.getCmp('campusTitle').setTitle('<h1>' + rec.get('title') + '</h1>');
							mobEdu.maps.f.getCampusPoints(rec.get('campusCode'), null, Ext.getCmp('campMaps'), 'mobEdu.maps.f.showCampus');
							mobEdu.maps.f.setLocations(Ext.getCmp('campMaps'));
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>No maps info.</center>',
					buttons: Ext.MessageBox.OK
				});
			}
		},

		onCampusItemTap: function(view, index, record) {
			mobEdu.maps.f.showCampus();
			Ext.getCmp('campusTitle').setTitle('<h1>' + record.get('title') + '</h1>');
			mobEdu.maps.f.getCampusPoints(record.data.campusCode, 'onChange', Ext.getCmp('campMaps'), 'mobEdu.maps.f.showCampus');
		},

		getCampusPoints: function(campusCode, flag, mapRef, viewRef) {
			var campusStore = mobEdu.util.getStore('mobEdu.maps.store.location');
			var localCampusStore = mobEdu.util.getStore('mobEdu.maps.store.campus');
			if (localCampusStore.data.length > 0) {
				localCampusStore.removeAll();
				localCampusStore.sync();
				localCampusStore.removed = [];
			}
			if (campusCode != null && campusCode != '') {
				campusStore.each(function(record) {
					if (record.get('campusCode') == campusCode) {
						localCampusStore.add(record.copy());
						localCampusStore.sync();
					}
				});
			}
			if (flag == 'onChange') {
				mobEdu.maps.f.setLocations(mapRef);
			} else if (viewRef != null && viewRef != '') {
				eval(viewRef + '()');
			}

		},

		loadLocationDetails: function(record, viewRef) {

			var dstore = mobEdu.util.getStore('mobEdu.maps.store.locationDetails');
			dstore.removeAll();
			var title;
			if (record != null) {
				title = record.data.buildingName;
				dstore.add(record.copy());
				dstore.sync();
			}


			mobEdu.util.get('mobEdu.maps.view.locationDetails');
			if (buildingDetailsType == 'LINK') {
				var store = mobEdu.util.getStore('mobEdu.maps.store.html');
				store.getProxy().setUrl(encorewebserver + 'open/getHTMLContent');
				store.getProxy().setExtraParams({
					url: 'https://mobileappstest.utoledo.edu/buildings/uh.html'
				});
				store.getProxy().afterRequest = function() {
					mobEdu.maps.f.htmlResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('encore', 'htmlcontent');
				Ext.getCmp('detailsFromUrl').show();
				Ext.getCmp('generalDetails').hide();

			} else if (buildingDetailsType == 'VIEW') {
				Ext.getCmp('generalDetails').show();
				Ext.getCmp('detailsFromUrl').hide();
			}
			if (title != null && title != '') {
				Ext.getCmp('locationDetailsTitle').setTitle('<h1 class="titleOverflow">' + title + '</h1>');
			}
			mobEdu.maps.f.showLocationDetails(viewRef);
		},

		htmlResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.maps.store.html');
			var html = store.getProxy().getReader().rawData;
			html = mobEdu.util.convertRelativeToAbsoluteUrls(html);
			Ext.getCmp('detailsFromUrl').setHtml(html);
		},

		isImgExists: function(img) {
			if (img == null || img == '') {
				return false;
			}
			return true;
		},

		isAddressExists: function(address) {
			if (address == null || address == '') {
				return false;
			}
			return true;
		},

		isDescriptionExists: function(decription) {
			if (decription == null || decription == '') {
				return false;
			}
			return true;
		},

		isWebsiteExists: function(website) {
			if (website == null || website == '') {
				return false;
			}
			return true;
		},

		isPrincipalExists: function(principal) {
			if (principal == null || principal == '') {
				return false;
			}
			return true;
		},

		isBuildingNameExists: function(buildingName) {
			if (buildingName == null || buildingName == '') {
				return false;
			}
			return true;
		},
		isBuildingDescExists: function(buildingDesc) {
			if (buildingDesc == null || buildingDesc == '') {
				return false;
			}
			return true;
		},
		isEmailExists: function(email) {
			if (email == null || email == '') {
				return false;
			}
			return true;
		},
		isCategoryExists: function(category) {
			if (category == null || category == '') {
				return false;
			}
			return true;
		},
		isCityExists: function(city) {
			if (city == null || city == '') {
				return false;
			}
			return true;
		},
		isStateExists: function(state) {
			if (state == null || state == '') {
				return false;
			}
			return true;
		},

		isPhoneExists: function(phone) {
			if (phone == null || phone == '') {
				return false;
			}
			return true;
		},
		isNotesExists: function(notes) {
			if (notes == null || notes == '') {
				return false;
			}
			return true;
		},

		getMapDirections: function(latitude, longitude, viewRef, travelType) {
			mobEdu.maps.f.directionTravelType = travelType;
			mobEdu.maps.f.reNewMap();
			mobEdu.maps.f.showMapDirections(viewRef);
			if (!(latitude == undefined || latitude == '' || latitude == null || longitude == undefined || longitude == null || longitude == '')) {
				destPosition = new google.maps.LatLng(latitude, longitude);
			}
		},

		getDirInstructions: function() {
			var geoLocationOptions = {
				maximumAge: 3000,
				timeout: 5000,
				enableHighAccuracy: true
			};
			var directionsDisplay, directionsService;
			navigator.geolocation.getCurrentPosition(geoLocationSuccess, geoLocationError, geoLocationOptions);

			function geoLocationSuccess(position) {
				directionsService = new google.maps.DirectionsService();
				directionsDisplay = new google.maps.DirectionsRenderer();
				//                directionsDisplay.setPanel(document.getElementById('dirdiv'));
				var start = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //cor
				var travelType;
				if (mobEdu.maps.f.directionTravelType === 'WALKING') {
					travelType = google.maps.DirectionsTravelMode.WALKING; // DRIVING, WALKING, BICYCLING
				} else {
					travelType = google.maps.DirectionsTravelMode.DRIVING; // DRIVING, WALKING, BICYCLING
				}
				var request = {
					origin: start,
					destination: destPosition,
					travelMode: travelType
				};
				directionsService.route(request, function(response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
					}
					var x = 0;
					var inst = new Array(),
						dist = new Array();
					Ext.each(response.routes[0].legs[0].steps, function(y) {
						inst.push(response.routes[0].legs[0].steps[x].instructions);
						dist.push(response.routes[0].legs[0].steps[x].distance.text);
						x = x + 1;
					});
					var instructions = '<table>';
					var i = 0;
					Ext.each(inst, function() {
						instructions += '<tr><td><h4>';
						instructions += inst[i];
						instructions += '</h4></td><td width="50px"><h4>';
						instructions += dist[i].toString();
						instructions += '</h4></td></tr>';
						i = i + 1;
					});
					instructions += '</table>';
					mobEdu.util.get('mobEdu.maps.view.directionDetails');
					Ext.getCmp('mapDirInst').setHtml(instructions);
					mobEdu.maps.f.showMapDirInst();
				});
			}

			function geoLocationError() {
				Ext.Msg.alert('Error', 'Error while getting geolocation.');
			}
		},

		onSearchKeyup: function(textfield, e, searchListCmp) {
			//Restricting TAB key
			if (e.browserEvent.keyCode != 9) {
				mobEdu.maps.f.doCheckBeforeSearch(textfield, searchListCmp);
			}
		},

		onSearchItemClear: function(textfield, e, searchListCmp) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length == 0 || length == '') {
				mobEdu.maps.f.doCheckBeforeSearch(textfield, searchListCmp);
			}
		},

		doCheckBeforeSearch: function(textfield, searchListCmp) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length >= 1) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						searchListCmp.setEmptyText('<h3 align="center">No results found.</h3>');
						mobEdu.maps.f.clearLocalSearchStore();
						mobEdu.maps.f.getsearchResults(textfield, searchListCmp);
					}
				}, 1000);
			} else {
				searchListCmp.setPlugins(null);
				searchListCmp.setEmptyText(null);
				mobEdu.maps.f.clearLocalSearchStore();
			}
		},

		clearLocalSearchStore: function() {
			var store = mobEdu.util.getStore('mobEdu.maps.store.search');
			store.removeAll();
			//To reset pagenation params
			store.currentPage = 1;
			store.setTotalCount(null);
		},

		getsearchResults: function(textfield, searchListCmp) {
			var searchValue = textfield.getValue();
			var localCampusStore = mobEdu.util.getStore('mobEdu.maps.store.campusLocal');
			var searchStore = mobEdu.util.getStore('mobEdu.maps.store.search');
			localCampusStore.each(function(record) {
				var buildingName = record.get('buildingName');
				var buildingCode = record.get('buildingCode');

				var buildingNameResult = buildingName.search(new RegExp(searchValue, "i"));
				var buildingCodeResult = buildingCode.search(new RegExp(searchValue, "i"));
				if (buildingNameResult >= 0 || buildingCodeResult >= 0) {
					searchStore.add(record.copy());
					searchStore.sync();
				}
			});
			searchListCmp.refresh();
		},

		loadSearch: function() {
			mobEdu.util.get('mobEdu.maps.view.search');
			Ext.getCmp('txtSearchCampus').setValue('');
			mobEdu.maps.f.clearLocalSearchStore();
			mobEdu.maps.f.showSearch();
		},

		loadBrowseCampuses: function(prevViewRef) {
			var localCampusStore = mobEdu.util.getStore('mobEdu.maps.store.campusLocal');
			var browseStore = mobEdu.util.getStore('mobEdu.maps.store.browse');
			if (browseStore.data.length > 0) {
				browseStore.removeAll();
			}

			var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
			mobEdu.maps.f.categoriesHash = new Ext.util.HashMap();
			filterStore.each(function(record) {
				mobEdu.maps.f.categoriesHash.add(record.get('filterId'), record.get('category'));
			});

			var recordsToBeAdded = new Array();
			localCampusStore.each(function(record) {
				var category = record.get('category');
				var categoryList = category.split(',');

				for (var i = 0; i < categoryList.length; i++) {
					categoryDesc = mobEdu.maps.f.categoriesHash.get(categoryList[i]);
					if (categoryDesc != '' && categoryDesc != 'Parking lots') {
						record.data.category = categoryList[i];
						recordsToBeAdded.push(record.copy());
					}
				}
				record.data.category = category;
			});

			browseStore.setData(recordsToBeAdded);

			mobEdu.maps.f.showBrowseCampuses(prevViewRef);
		},

		loadFilter: function(viewRef) {
			var filterStore = mobEdu.util.getStore('mobEdu.maps.store.filter');
			if (filterStore.data.length == 0) {
				var campusStore = mobEdu.util.getStore('mobEdu.maps.store.location');
				var filters = campusStore.getServerProxy().getReader().rawData.filter;
				for (var cnt = 0; cnt < filters.length; cnt++) {
					var flag = false;
					if (filters[cnt].showByDefault == 'Y')
						flag = true;

					filterStore.add({
						filterId: filters[cnt].categoryId,
						category: filters[cnt].category,
						icon: filters[cnt].icon,
						filter: flag
					});
				}
			}
			if (viewRef != null && viewRef != '') {
				eval(viewRef + '()');
			}
		},

		filterOn: function(category) {
			var store = mobEdu.util.getStore('mobEdu.maps.store.filter');
			var localRecord = null;
			localRecord = store.findRecord('category', category, null, null, null, true);
			localRecord.set('filter', true);
			store.sync();
		},

		filterOff: function(category) {
			var store = mobEdu.util.getStore('mobEdu.maps.store.filter');

			var offCount = 0;
			store.each(function(record) {
				if (record.data.filter == true) {
					offCount++;
				}
			})
			if (offCount <= 1) {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>At least one filter must be selected.</center>',
					buttons: Ext.MessageBox.OK
				});
				return;
			}

			var localRecord = null;
			localRecord = store.findRecord('category', category, null, null, null, true);
			localRecord.set('filter', false);
			store.sync();
		},

		onFilterBackTap: function() {
			var campusTitle = Ext.getCmp('campusTitle').getTitle().getTitle();
			campusTitle = campusTitle.substring(4, campusTitle.length - 5);
			var campusLocal = mobEdu.util.getStore('mobEdu.maps.store.campusLocal');
			var rec = campusLocal.findRecord('title', campusTitle);
			mobEdu.maps.f.getCampusPoints(rec.get('campusCode'), 'onChange', Ext.getCmp('campMaps'), 'mobEdu.maps.f.showCampus');
			mobEdu.maps.f.showCampus();
		},

		onFilterItemTap: function(view, index, target, record, item, e, eOpts) {
			if (item.target.name == "on") {
				mobEdu.maps.f.filterOff(record.data.category);
			} else if (item.target.name == "off") {
				mobEdu.maps.f.filterOn(record.data.category);
			}
		},

		buildingInfo: function(url) {
			mobEdu.maps.f.hidePopup();
			mobEdu.util.loadExternalUrl(url);
		},

		onDirectionsItemTap: function(latitude, longitude, travelType) {
			mapFlag = 'campusMap';
			mobEdu.maps.f.hidePopup();
			var viewRef = null;
			if (mobEdu.util.isShowMapSubMenu() == true) {
				viewRef = mobEdu.maps.f.showMapsSubMenu;
			} else {
				viewRef = mobEdu.maps.f.showCampus;
			}
			mobEdu.maps.f.getMapDirections(latitude, longitude, viewRef, travelType);
		},

		hidePopup: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.maps.f.infoBubble.close();
		},

		showCampusMap: function(title) {
			if (title == '' || title == null || title == undefined)
				title = mobEdu.util.getTitleInfo('MAPS');
			if (mobEdu.util.isShowMapSubMenu() == true) {
				mobEdu.util.updatePrevView(mobEdu.maps.f.showMapsSubMenu);
			} else {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			}
			mobEdu.util.show('mobEdu.maps.view.campusMap');
			Ext.getCmp('mapTitle').setTitle('<h1>' + title + '</h1>');
		},

		isMapCustomIcons: function() {
			if (mapCustomIcons == 'yes') {
				return true;
			}
			return false;
		},

		loadMaps: function() {
			if (mobEdu.util.getDefaultMapView() == 2) {
				mobEdu.maps.f.loadCampusCodes('mobEdu.maps.f.showBrowseCampuses', mobEdu.util.showMainView);
				mobEdu.maps.f.showBrowseCampuses(mobEdu.util.showMainView);
				Ext.getCmp('browseTitle').setTitle('<h1>Buildings</h1>');
				Ext.getCmp('mapsToolbar').show();
			} else {
				mobEdu.maps.f.showCampus();
				if (mobEdu.maps.f.mapRendered) {
					mobEdu.maps.f.loadCampusCodes();
				}
			}
		},

		onMapButtonTap: function() {
			mobEdu.maps.f.showCampus();
			Ext.getCmp('browseTitle').setTitle('<h1>Browse All Campuses</h1>');
		},

		showCampus: function() {
			if (mainMenuLayout === 'BGRID') {
				mobEdu.util.get('mobEdu.maps.view.campus').setLayout('fit');
			}
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.maps.view.campus');
		},

		showSelect: function() {
			mobEdu.util.updatePrevView(mobEdu.maps.f.showCampus);
			mobEdu.util.show('mobEdu.maps.view.select');
		},

		showSearch: function() {
			mobEdu.util.updatePrevView(mobEdu.maps.f.showCampus);
			mobEdu.util.show('mobEdu.maps.view.search');
			Ext.getCmp('searchCampusList').setEmptyText('');
		},

		showBrowseCampuses: function(prevViewRef) {
			if (prevViewRef != null) {
				mobEdu.util.updatePrevView(prevViewRef);
			} else {
				if (mobEdu.util.getDefaultMapView() == 2) {
					mobEdu.util.updatePrevView(mobEdu.util.showMainView);
				} else {
					mobEdu.util.updatePrevView(mobEdu.maps.f.showCampus);
				}
			}
			mobEdu.util.show('mobEdu.maps.view.browse');
		},

		showFilter: function() {
			mobEdu.util.updatePrevView(mobEdu.maps.f.onFilterBackTap);
			mobEdu.util.show('mobEdu.maps.view.filter');
		},

		showMapsSubMenu: function() {
			var title = mobEdu.util.getTitleInfo('MAPS');
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.maps.view.campusList');
			Ext.getCmp('mapCampusTitle').setTitle('<h1>' + title + '</h1>');
		},

		showLocationDetails: function(viewRef) {
			if (viewRef == null) {
				mobEdu.util.updatePrevView(mobEdu.maps.f.showCampus);
			} else {
				mobEdu.util.updatePrevView(viewRef);
			}
			mobEdu.util.show('mobEdu.maps.view.locationDetails');

		},

		showMapDirections: function(viewRef) {
			if (mapFlag == 'campusMap') {
				mobEdu.util.updatePrevView(mobEdu.maps.f.showCampus);
			} else if (viewRef == null) {
				mobEdu.util.updatePrevView(mobEdu.maps.f.showLocationDetails);
			} else {
				mobEdu.util.updatePrevView(viewRef);
			}
			mobEdu.util.show('mobEdu.maps.view.directionMap');
		},

		showMapDirInst: function() {
			mobEdu.util.updatePrevView(mobEdu.maps.f.showMapDirections);
			mobEdu.util.show('mobEdu.maps.view.directionDetails');
		},

		reNewMap: function() {
			if (dirMap == null) {
				mobEdu.util.get('mobEdu.maps.view.directionMap');
				dirMap = Ext.getCmp('directionMap');
			} else {
				var viewRef = mobEdu.util.get('mobEdu.maps.view.directionMap');
				viewRef.remove(dirMap, true);
				dirMap = new Ext.Map({
					cls: 'bevelcontainer',
					height: '100%',
					width: '100%',
					mapOptions: {
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: new google.maps.LatLng(mapInitLatitude, mapInitLongitude)
					},
					mapRendererOptions: {
						draggable: true,
						hideRouteList: true
					},
					listeners: {
						maprender: function(mapRef, map, eOpts) {
							mobEdu.maps.f.setDirection(mapRef);
						}
					}
				});
				viewRef.add(dirMap);
			}
		},

		setDirection: function() {
			var geoLocationOptions = {
				maximumAge: 3000,
				timeout: 5000,
				enableHighAccuracy: true
			};
			var marker, directionsRenderer, directionsService;

			navigator.geolocation.getCurrentPosition(geoLocationSuccess, geoLocationError, geoLocationOptions);

			function geoLocationSuccess(position) {

				directionsService = new google.maps.DirectionsService();
				directionsRenderer = new google.maps.DirectionsRenderer(dirMap.mapRendererOptions);
				directionsRenderer.setMap(dirMap.getMap());

				marker = new google.maps.Marker({
					map: dirMap.getMap(),
					visible: true
				});

				var start = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //cor
				// set the request options
				var travelType;
				if (mobEdu.maps.f.directionTravelType === 'WALKING') {
					travelType = google.maps.DirectionsTravelMode.WALKING; // DRIVING, WALKING, BICYCLING
				} else {
					travelType = google.maps.DirectionsTravelMode.DRIVING; // DRIVING, WALKING, BICYCLING
				}
				var request = {
					origin: start,
					destination: destPosition,
					travelMode: travelType // DRIVING, WALKING, BICYCLING
				};
				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						// Display the directions using Google's Directions Renderer.
						directionsRenderer.setPanel(document.getElementById('map-directions'));
						directionsRenderer.setDirections(result);
					} else {
						Ext.Msg.show({
							message: '<p>Error while getting direction.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox',
							fn: function(btn) {
								if (btn == 'ok') {
									mobEdu.maps.f.showLocationDetails();
								}
							}
						});
					}
					Ext.Viewport.unmask();
				});
			}

			function geoLocationError() {
				Ext.Viewport.unmask();
				Ext.Msg.show({
					message: '<p>Error while getting geolocation.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.maps.f.showLocationDetails();
						}
					}
				});
			}
		},
		isShowCampusCodes: function() {
			if (showMapCampusCodes == 'false') {
				return false;
			} else {
				return true;
			}
		},
		getMapIcon: function(icon) {
			if (icon != null || icon != undefined || icon.trim() != "") {
				return icon;
			} else {
				return 0;
			}

		},
		isShowWalkingDirections: function() {
			if (showWalkingDirections == 'true') {
				return true;
			} else {
				return false;
			}
		}
	}
});