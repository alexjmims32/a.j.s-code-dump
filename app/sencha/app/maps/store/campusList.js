Ext.define('mobEdu.maps.store.campusList', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.maps.model.location'
	],

	config: {
		storeId: 'mobEdu.maps.store.campusList',
		autoLoad: true,
		model: 'mobEdu.maps.model.location',
		proxy: {
			type: 'memory',
			id: 'campusListLocalStore'
		}
	}
});