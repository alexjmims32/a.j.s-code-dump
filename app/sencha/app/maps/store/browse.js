Ext.define('mobEdu.maps.store.browse', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.maps.model.location'
	],

	config: {
		storeId: 'mobEdu.maps.store.browse',
		autoLoad: false,
		model: 'mobEdu.maps.model.location',
		grouper: {
			groupFn: function(record) {
				return mobEdu.maps.f.categoriesHash.get(record.get('category'));
			}
		},
		sorters: [
			'buildingName'
		],
		proxy: {
			type: 'memory',
			id: 'browseLocalStore'
		}
	}
});