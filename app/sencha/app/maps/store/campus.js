Ext.define('mobEdu.maps.store.campus', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.maps.model.location'
	],

	config: {
		storeId: 'mobEdu.maps.store.campus',
		autoLoad: true,
		model: 'mobEdu.maps.model.location',
		proxy: {
			type: 'memory',
			id: 'campusLocalStore'
		}
	}
});