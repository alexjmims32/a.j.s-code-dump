Ext.define('mobEdu.maps.store.locationDetails', {
	extend: 'Ext.data.Store',
	//      alias: 'mainStore',

	requires: [
		'mobEdu.maps.model.location'
	],

	config: {
		storeId: 'mobEdu.maps.store.locationDetails',
		autoLoad: false,
		model: 'mobEdu.maps.model.location',
		proxy: {
			type: 'memory',
			id: 'locationDetails'
		}
	}
});