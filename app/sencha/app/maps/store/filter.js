Ext.define('mobEdu.maps.store.filter', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.maps.model.location'
	],

	config: {
		storeId: 'mobEdu.maps.store.filter',
		autoLoad: true,
		model: 'mobEdu.maps.model.location',
		proxy: {
			type: 'memory',
			id: 'filterLocalStore'
		}
	}
});