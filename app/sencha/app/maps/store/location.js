Ext.define('mobEdu.maps.store.location', {
    extend: 'Ext.ux.OfflineSyncStore',

    requires: [
        'mobEdu.maps.model.location'
    ],

    config: {
        model: 'mobEdu.maps.model.location',

        trackLocalSync: false,
        autoServerSync: false,

        // define a LOCAL proxy for saving the store's data locally
        localProxy: {
            type: 'localstorage',
            id: 'maps-sync-store'
        },

        // define a SERVER proxy for saving the store's data on the server
        serverProxy: {
            type: 'ajax',
            api: {
                read: encorewebserver + 'open/maps'
            },
            extraParams: {
                companyId: companyId
            },
            headers: {
                companyID: companyId
            },
            reader: {
                type: 'json',
                rootProperty: 'map'
            }
        }
    },

    initialize: function() {
        this.loadLocal();
    }
});