Ext.define('mobEdu.maps.store.search', {
	extend: 'Ext.data.Store',

	requires: [
		'mobEdu.maps.model.location'
	],

	config: {
		storeId: 'mobEdu.maps.store.search',
		autoLoad: true,
		model: 'mobEdu.maps.model.location',
		proxy: {
			type: 'memory',
			id: 'searchLocalStore'
		}
	}
});