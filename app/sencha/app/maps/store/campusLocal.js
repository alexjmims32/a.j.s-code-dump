Ext.define('mobEdu.maps.store.campusLocal', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.maps.model.location'
    ],

    config:{
        storeId: 'mobEdu.maps.store.campusLocal',
        autoLoad: true,
        model: 'mobEdu.maps.model.location',
        proxy:{
            type:'localstorage',
            id:'campusLocalStorage'
        }
    }
});