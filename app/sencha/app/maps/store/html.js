Ext.define('mobEdu.maps.store.html', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.feeds.model.feeds'
    ],
    
    config:{
        storeId: 'mobEdu.maps.store.html',
        autoLoad: false,
        model: 'mobEdu.feeds.model.feeds'
    },
    initProxy: function() {
        var proxy = this.callParent();
//        proxy.reader.rootProperty= 'feederList'        
        return proxy;
    }
    
});