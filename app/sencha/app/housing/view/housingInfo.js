Ext.define('mobEdu.housing.view.housingInfo', {
	extend: 'Ext.Container',
	config: {
		fullscreen: true,
		layot:'vbox',
		scrollable:'vertical',		
		cls: 'logo',
		items:[{
			xtype: 'customToolbar',
            title: '<h1>Student Housing Info</h1>'
        },{
            xtype: 'fieldset',
            title: 'Housing/Meal plan application',
            items: [{
                xtype:'textfield',
                label:'Application Type',
                labelWidth:'30%',
                id:'shiAppType',
                readOnly: true
            },{
                xtype:'textfield',
                label:'Meal Plan',
                labelWidth:'30%',
                id:'shiMealPlan',
                readOnly: true
            },{
                xtype:'textfield',
                label:'From Term',
                labelWidth:'30%',
                id:'shiFromTerm',
                readOnly: true
            },{
                xtype:'textfield',
                label:'To Term',
                labelWidth:'30%',
                id:'shiToTerm',
                readOnly: true
            },{
                xtype:'textfield',
                label:'Application Status',
                labelWidth:'30%',
                id:'shiAppStatus',
                readOnly: true
            }]
        },{
            xtype: 'fieldset',
            title: 'Housing assignment',
            items: [{
                xtype:'textfield',
                label:'Room Assigned',
                labelWidth:'30%',
                id:'shiRoomAssigned',
                readOnly: true
            },{
                xtype:'textfield',
                label:'Room Rate',
                labelWidth:'30%',
                id:'shiRoomRate',
                readOnly: true
            }]
        },{
            xtype: 'fieldset',
            title: 'Meal plan assignment',
            items: [{
                xtype:'textfield',
                label:'Meal Plan',
                labelWidth:'30%',
                id:'shiaMealPlan',
                readOnly: true
            },{
                xtype:'textfield',
                label:'From Date',
                labelWidth:'30%',
                id:'shiFromDate',
                readOnly: true
            },{
                xtype:'textfield',
                label:'To Date',
                labelWidth:'30%',
                id:'shiToDate',
                readOnly: true
            }]
		}],
		flex: 1
	}
});