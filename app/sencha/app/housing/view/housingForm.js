Ext.define('mobEdu.housing.view.housingForm', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],

    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        items: [{
            xtype: 'customToolbar',
            id: 'dirFTitle',
            title: '<h1>Housing Request Form</h1>'


        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Submit',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.housing.f.onSubmitButtonTap();
                }
            }]

        }, {

            xtype: 'fieldset',
            id: 'dirFieldset',

            items: [{
                    xtype: 'textfield',
                    label: 'WID',
                    hidden: false,
                    id: 'dirwid',
                    labelWidth: '40%',
                }, {
                    xtype: 'textfield',
                    label: 'First Name',
                    hidden: false,
                    id: 'dirSearchfame',
                    labelWidth: '40%',

                }, {
                    xtype: 'textfield',
                    label: 'Last Name',
                    id: 'dirSearchlame',
                    labelWidth: '40%',



                }, {
                    xtype: 'emailfield',
                    name: 'email',
                    id: 'dirEmail',
                    labelWidth: '40%',


                    label: 'Wentworth Email',

                    useClearIcon: true,
                    initialize: function() {
                        var me = this,
                            input = me.getInput().element.down('input');

                        input.set({
                            pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                        });

                        me.callParent(arguments);
                    }

                }, {

                    xtype: 'textareafield',
                    label: 'Local Address ',
                    id: 'dirStreet1',
                    labelWidth: '40%',
                    maxRows: 4,


                },

                {

                    xtype: 'textfield',
                    labelAlign: 'left',
                    labelWidth: '40%',
                    label: 'Contact Number',
                    name: 'phone',
                    id: 'dirphNumber',

                    useClearIcon: true,
                    //initialize: function() {
                    //  var me = this,
                    //    input = me.getInput().element.down('input');

                    //input.set({
                    //  pattern: '[0-9]*'
                    //});

                    //me.callParent(arguments);
                    // },
                    maxLength: 12
                }, {

                    xtype: 'selectfield',
                    name: 'instSearchTermId',
                    labelWidth: '40%',
                    valueField: 'value',
                    displayField: 'text',
                    id: 'dirServiceCatId',
                    label: 'Service Category',
                    //     options: [
                    //         {description: '',  code :''}, 
                    //         {description: 'Work Order Request',        code :'physicalplant@wit.edu'},

                    //         {description: 'Help Desk Ticket', code: 'helpdesk@wit.edu'},
                    //         {description: 'Residential Work Order',  code: 'housing@wit.edu'}

                    // ],
                    listeners: {
                        change: function(selectbox, newValue, oldValue) {
                            mobEdu.housing.f.onChangeCategory(selectbox, newValue, oldValue);
                        }
                    }


                }, {
                    xtype: 'selectfield',
                    labelAlign: 'left',
                    labelWidth: '40%',
                    label: 'Building ',
                    name: 'class',
                    hidden: true,
                    id: 'hBuilSel',

                    useClearIcon: true,
                    options: [{
                            text: 'Select',
                            value: ''
                        }, {
                            text: 'Vancouver',
                            value: 'Vancouver'
                        }, {
                            text: 'Apartments @525',
                            value: 'Apartments @525'
                        },

                        {
                            text: '610 Huntington',
                            value: '610 Huntington'
                        }, {
                            text: '555 Huntington',
                            value: '555 Huntington'
                        }, {
                            text: 'Baker Hall',
                            value: 'Baker Hall'
                        }, {
                            text: 'Edwards',
                            value: 'Edwards'
                        }, {
                            text: 'Evans Way Hall',
                            value: 'Evans Way Hall'
                        },

                        {
                            text: 'Louis Prang',
                            value: 'Louis Prang '
                        },

                        {
                            text: 'Rodger',
                            value: 'Rodgers'

                        }, {
                            text: 'Tadbury Hall',
                            value: 'Tadbury Hall'

                        }
                    ]


                }, {

                    xtype: 'textfield',
                    label: 'Room',
                    id: 'dirApRoom',
                    hidden: true,
                    labelWidth: '40%',
                }, {
                    xtype: 'selectfield',
                    labelWidth: '40%',
                    label: 'Work Order Category',
                    name: 'intMaj',
                    hidden: true,
                    id: 'dirWOrdCat',
                    useClearIcon: true,
                    options: [{
                        text: 'Select',
                        value: ''
                    }, {
                        text: 'Carpentry',
                        value: 'Carpentry'
                    }, {
                        text: 'Construction',
                        value: 'Construction'
                    }, {
                        text: 'Custodial',
                        value: 'Custodial'
                    }, {
                        text: 'Electrical',
                        value: 'Electrical'
                    }, {
                        text: 'Elevator',
                        value: 'Elevator'
                    }, {
                        text: 'Fire Alarm System',
                        value: 'Fire Alarm System'
                    }, {
                        text: 'Grounds',
                        value: 'Grounds'
                    }, {
                        text: 'Heating/Ventilation/Air Conditioning',
                        value: 'Heating/Ventilation/Air Conditioning'
                    }, {
                        text: 'Key and Lock',
                        value: 'Key and Lock'
                    }, {
                        text: 'Masonry',
                        value: 'Masonry'
                    }, {
                        text: 'Moving',
                        value: 'Moving'

                    }, {
                        text: 'Painting',
                        value: 'Painting'
                    }, {
                        text: 'Plumbing',
                        value: 'Plumbing'
                    }, {
                        text: 'Signage',
                        value: 'Signage'
                    }, {
                        text: 'Roof',
                        value: 'Roof'
                    }, {
                        text: 'Miscellaneous',
                        value: 'Miscellaneous'
                    }]
                }, {
                    xtype: 'textfield',
                    label: '',
                    hidden: true,
                    id: 'dirWODes',
                    labelWidth: '40%',
                }



            ]

        }]
    }


});