Ext.define('mobEdu.housing.view.studentInfo', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.housing.store.search'
	],
	config: {
		fullscreen: true,
		layout:'fit',
		scroll: 'vertical',	
		cls: 'logo',
		items: [{
            xtype: 'customToolbar',
            title: '<h1>Student\'s Information</h1>'
        },{
            xtype: 'fieldset',
            docked:'top',
            items: [{
                xtype:'selectfield',
                label:'Building',
                labelWidth:'40%',
                id:'siBuilding',
                placeHolder:'Select Building',
            },{
                xtype:'textfield',
                label:'Room',
                labelWidth:'40%',
                id:'siRoom',
                placeHolder:'Enter Room Number'
            }]
        },{
            layout:'hbox',
            style: 'padding-left: 10px;',
            docked:'top',
            items:[{
                xtype:'textfield',
                id:'siSearchItem',
                flex: 1,
                width:'95%',
                placeHolder:'Enter Student name'
            },{
                xtype:'button',
                maxWidth: '32px',
                flex: 3,
                text: '<img src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png" height="24px" width="24px">',
				style: {
					border: 'none',
					background: 'none'
				},
				handler: function() {
					mobEdu.housing.f.onSearchItemTap();
				}
            }]
        },{            
            xtype:'list',
            id:'siInfoList',
            disableSelection: true,
            emptyText: '<h3 align="center">No Student Info</h3>',
            itemTpl: '<table width="100%">'
            			+ '<tr><td width="100%" align="left"><h2>{fullName}</h2></td></tr>'
            			+ '<tr><td width="100%" align="left"><h3>{roomDesc}</h3></td></tr>'
            			+ '</table>',
            store: mobEdu.util.getStore('mobEdu.housing.store.search'),
            loadingText: '',
        }],
		flex: 1
	}
});