Ext.define('mobEdu.housing.f', {
	requires: [
		'mobEdu.housing.store.housingInfo',
		'mobEdu.housing.store.search',
		'mobEdu.housing.store.buildings',
		'mobEdu.housing.store.emails',
		'mobEdu.housing.store.studentInfo',
		'mobEdu.housing.store.studentInfoMail',

	],
	statics: {
		loadHousingInfo: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.housingInfo');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getHousingInfo');
			store.getProxy().setExtraParams({
				studentId: mobEdu.util.getStudentId()
			});
			store.getProxy().afterRequest = function() {
				mobEdu.housing.f.housingInfoResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getHousingInfo');

		},
		housingInfoResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.housingInfo');
			if (store.data.length > 0) {
				mobEdu.util.get('mobEdu.housing.view.housingInfo');
				Ext.getCmp('shiAppType').setValue(store.getAt(0).get('appType'));
				Ext.getCmp('shiMealPlan').setValue(store.getAt(0).get('mealPlan'));
				Ext.getCmp('shiFromTerm').setValue(store.getAt(0).get('fromTerm'));
				Ext.getCmp('shiToTerm').setValue(store.getAt(0).get('toTerm'));
				Ext.getCmp('shiAppStatus').setValue(store.getAt(0).get('appStatus'));
				Ext.getCmp('shiRoomAssigned').setValue(store.getAt(0).get('roomAssigned'));
				Ext.getCmp('shiRoomRate').setValue(store.getAt(0).get('roomRate'));
				Ext.getCmp('shiaMealPlan').setValue(store.getAt(0).get('mealPlan'));
				Ext.getCmp('shiFromDate').setValue(store.getAt(0).get('fromDate'));
				Ext.getCmp('shiToDate').setValue(store.getAt(0).get('toDate'));
				mobEdu.housing.f.showHousingInfo();
			} else {
				Ext.Msg.show({
					message: 'No Housing Info',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},
		loadBuildingsDropdown: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.buildings');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getBuildings');
			store.getProxy().afterRequest = function() {
				mobEdu.housing.f.buildingsResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getBuildings');
		},
		buildingsResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.buildings');
			if (store.data.length > 0) {
				mobEdu.util.get('mobEdu.housing.view.studentInfo');
				var buildings = new Array();
				buildings.push({
					text: 'Select Building',
					value: 0
				});
				store.each(function(item, index, length) {
					buildings.push({
						text: item.get('description'),
						value: item.get('code')
					});
				});
				Ext.getCmp('siBuilding').setOptions(buildings);
				Ext.getCmp('siBuilding').setValue(buildings[0]);
				mobEdu.housing.f.showStudentInfo();
			} else {
				Ext.Msg.show({
					message: 'No Buildings',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadEmails: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.emails');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(commonwebserver + 'acrm/getEnumerations');
			store.getProxy().afterRequest = function() {
				mobEdu.housing.f.emailsResponseHandler();
			};
			store.getProxy().setExtraParams({
				enumType: "housingemails",
				enumVal: "",
				assocEnumType: ""
			});
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getEmailConfig');
		},
		emailsResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.emails');
			if (store.data.length <= 0) {
				Ext.Msg.show({
					message: 'No Emails got from configuration',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		onSearchItemTap: function() {
			var building = Ext.getCmp('siBuilding').getValue();
			var room = Ext.getCmp('siRoom').getValue();
			var studentName = Ext.getCmp('siSearchItem').getValue();
			if ((building == null || building == 0) && (room == null || room == '') && (studentName == null || studentName == '')) {
				Ext.Msg.show({
					message: 'Please provide any one field to search',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (building == null || building == 0) {
					building = '';
				}
				var store = mobEdu.util.getStore('mobEdu.housing.store.search');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setUrl(webserver + 'housingSearch');
				store.getProxy().setExtraParams({
					building: building,
					room: room,
					name: studentName
				});
				store.load();
				mobEdu.util.gaTrackEvent('enroll', 'housingSearch');
			}
		},
		loadHousingForms: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.studentInfo');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'getHousingStudentInfo');
			store.getProxy().setExtraParams({
				studentId: mobEdu.util.getStudentId()
			});
			store.getProxy().afterRequest = function() {
				mobEdu.housing.f.housingstudentInfoResponseHandler();
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'getHousingStudentInfo');
		},
		housingstudentInfoResponseHandler: function() {
			mobEdu.util.get('mobEdu.housing.view.housingForm');
			var store = mobEdu.util.getStore('mobEdu.housing.store.studentInfo');
			if (store.data.length > 0) {
				mobEdu.housing.f.loadEmails();
				var record = store.getAt(0);
				if (record != undefined) {
					var street1 = record.data.street1;
					var city = record.data.city;
					var state = record.data.state;
					var country = record.data.county;
					var zip = record.data.zip;
					var address = "";
					if (street1 != '' && street1 != undefined && street1 != 'null')
						address = address + "Street1 : " + street1 + ";";
					if (city != '' && city != undefined && city != 'null')
						address = address + "City : " + city + ";";
					if (state != '' && state != undefined && state != 'null')
						address = address + "State : " + state + ";";
					if (country != '' && country != undefined && country != 'null')
						address = address + "Country : " + country + ";";
					if (zip != '' && zip != undefined && zip != 'null') {
						address = address + "zip : " + zip + ";";
					} else {
						var address = '';
					}

					Ext.getCmp('dirwid').setValue(record.data.id);
					Ext.getCmp('dirSearchfame').setValue(record.data.fullName);
					Ext.getCmp('dirSearchlame').setValue(record.data.lastName);
					Ext.getCmp('dirEmail').setValue(record.data.email);
					Ext.getCmp('dirStreet1').setValue(address);

					// var phoneno= record.data.phone;
					// var phoneNo1 = phoneno.replace(/^(\d{3})(\d{3})(\d{4})$/, '$1-$2-$3');
					Ext.getCmp('dirphNumber').setValue(record.data.phone);
				} else {
					Ext.Msg.show({
						message: 'No Information from banner.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}
			Ext.getCmp('hBuilSel').setHidden(true);
			Ext.getCmp('dirApRoom').setHidden(true);
			Ext.getCmp('dirWOrdCat').setHidden(true);
			Ext.getCmp('dirWODes').setHidden(true);
			// Ext.getCmp('dirServiceCatId').reset();
			// mobEdu.housing.f.onChangeCategory();
			mobEdu.housing.f.showHousingForm();
		},
		isDataExist: function(value) {
			if (value == '' || value == 'undefined' || value == 'null')
				return false;
			return true;
		},
		onSubmitButtonTap: function() {
			var subject = "";
			var recipient = "";
			var mailBody = "<p>";
			var wid = Ext.getCmp('dirwid').getValue();
			var firstName = Ext.getCmp('dirSearchfame').getValue();
			var lastName = Ext.getCmp('dirSearchlame').getValue();
			var email = Ext.getCmp('dirEmail').getValue();
			var address = Ext.getCmp('dirStreet1').getValue();
			var phone = Ext.getCmp('dirphNumber').getValue();
			var servicecategory = Ext.getCmp('dirServiceCatId').getValue();
			// var buildingselection = Ext.getCmp('hBuilSel').getValue();
			// var course = Ext.getCmp('feedbackCourseId').getValue();
			if (mobEdu.housing.f.isDataExist(wid) && mobEdu.housing.f.isDataExist(firstName) && mobEdu.housing.f.isDataExist(lastName) && mobEdu.housing.f.isDataExist(email) && mobEdu.housing.f.isDataExist(address) && mobEdu.housing.f.isDataExist(phone) && mobEdu.housing.f.isDataExist(servicecategory)) {
				var mail = Ext.getCmp('email');
				var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
				if (regMail.test(email) == false) {
					Ext.Msg.show({
						id: 'email',
						name: 'email',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid e-mail address.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								mail.focus(true);
							}
						}
					});
				} else if (phone.length < 10) {
					Ext.Msg.show({
						id: 'phone1msg',
						name: 'phone1msg',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid Phone Number.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								phone.focus(true);
							}
						}
					});
				} else {
					mailBody = mailBody + "WID : " + wid + "<br>";
					mailBody = mailBody + "First Name : " + firstName + "<br>";
					mailBody = mailBody + " Last Name : " + lastName + "<br>";
					mailBody = mailBody + "Wentworth Email : " + email + "<br>";
					mailBody = mailBody + "Local address : " + address + "<br>";
					mailBody = mailBody + "Cell Phone Number : " + phone + "<br>";
					recipient = mobEdu.housing.f.getCategoryEmailRecipent(servicecategory);
					if (servicecategory == 'Help Desk Ticket') {
						// recipient = "helpdesk@wit.edu";
						var WorkOrderDescription = Ext.getCmp('dirWODes').getValue();
						if (mobEdu.housing.f.isDataExist(WorkOrderDescription)) {
							subject = "Help Desk Ticket";
							mailBody = mailBody + " Work Order Description : " +
								WorkOrderDescription;
						} else {
							Ext.Msg.show({
								title: null,
								message: '<p>Please Enter all Mandatory Fields</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						}
					} else if (servicecategory == 'Work Order Request') {
						subject = " Student Work Request Form";
						// recipient = "physicalplant@wit.edu";
						var buildingselection = Ext.getCmp('hBuilSel').getValue();
						var ApartRoom = Ext.getCmp('dirApRoom').getValue();
						var WorkOrderCategory = Ext.getCmp('dirWOrdCat').getValue();
						var WorkOrderDescription = Ext.getCmp('dirWODes').getValue();
						if (mobEdu.housing.f.isDataExist(WorkOrderDescription)) {
							mailBody = mailBody + "Building Selection : " + buildingselection + "<br>";
							mailBody = mailBody + "Room  : " + ApartRoom + "<br>";
							mailBody = mailBody + "Work Order Category : " + WorkOrderCategory + "<br>";
							mailBody = mailBody + "Work Order Description : " + WorkOrderDescription + "<br>";

						} else {
							Ext.Msg.show({
								title: null,
								message: '<p>Please Enter all Mandatory Fields</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						}
					} else if (servicecategory == 'Residential Work Order') {
						subject = "Residential Student Work Request Form";
						// recipient = "housing@wit.edu";
						var buildingselection = Ext.getCmp('hBuilSel').getValue();
						var ApartRoom = Ext.getCmp('dirApRoom').getValue();
						var WorkOrderDescription = Ext.getCmp('dirWODes').getValue();
						if (mobEdu.housing.f.isDataExist(WorkOrderDescription) && mobEdu.housing.f.isDataExist(ApartRoom) && mobEdu.housing.f.isDataExist(buildingselection)) {
							mailBody = mailBody + "Building Selection : " + buildingselection + "<br>";
							mailBody = mailBody + "Room  : " + ApartRoom + "<br>";
							// mailBody = mailBody + "Work Order Category : " + WorkOrderCategory;
							mailBody = mailBody + "Work Order Description : " + WorkOrderDescription + "<br>";
						} else {
							Ext.Msg.show({
								title: null,
								message: '<p>Please Enter all Mandatory Fields</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						}

					} else {
						Ext.Msg.show({
							title: null,
							message: '<p>Please Enter all Mandatory Fields</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please Enter all Mandatory Fields</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});

			}
			if (recipient != null) {
				mailBody = mailBody + "</p>";
				var store = mobEdu.util.getStore('mobEdu.housing.store.studentInfoMail');
				store.getProxy().setHeaders({
					Authorization: mobEdu.util.getAuthString(),
					companyID: companyId
				});
				store.getProxy().setUrl(webserver + 'sendFormDataToMail');
				store.getProxy().setExtraParams({
					to: recipient,
					subject: subject,
					body: mailBody,

				});
				store.getProxy().afterRequest = function() {
					mobEdu.housing.f.sendFormDataToMailResponseHandler();
				};
				store.load();
				mobEdu.util.gaTrackEvent('enroll', 'submitFormdata');
			}

		},
		sendFormDataToMailResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.housing.store.studentInfoMail');
			if (store.data.length > 0) {
				var status = store.getProxy().getReader().rawData.status;
				if (status == 'SUCCESS') {
					Ext.Msg.show({
						id: 'svsubmitsuccess',
						title: null,
						cls: 'msgbox',
						message: '<p>Thank you  your form submitted successfully.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								//mobEdu.main.f.displayModules();
								//mobEdu.util.get('mobEdu.housing.view.housingForm');
								//Ext.getCmp('dirFieldset').reset();
								mobEdu.housing.f.resetFormSubmission();
								mobEdu.main.f.displayModules();

							}
						}
					});
				}

			} else {
				var status = store.getProxy().getReader().rawData.status;
				if (status != 'SUCCESS') {
					Ext.Msg.show({
						cls: 'msgbox',
						title: null,
						message: status,
						buttons: Ext.MessageBox.OK
					});
				}
			}

		},
		resetFormSubmission: function() {
			mobEdu.util.get('mobEdu.housing.view.housingForm');
			// Ext.getCmp('housingFormfieldset').reset();
			Ext.getCmp('dirServiceCatId').reset();
			Ext.getCmp('hBuilSel').reset();
			Ext.getCmp('dirWOrdCat').reset();
			Ext.getCmp('dirWODes').reset();
			Ext.getCmp('dirApRoom').reset();

		},

		showHousingForm: function() {
			var role = mobEdu.util.getRole();
			if (role.match(/student/gi)) {
				Ext.getCmp('dirServiceCatId').setOptions([{
					text: 'Select service Category',
					value: 'null'
				}, {
					text: 'Help Desk Ticket',
					value: 'Help Desk Ticket'
				}, {
					text: 'Residential Work Order',
					value: 'Residential Work Order'
				}]);
			} else if (role.match(/faculty/gi)) {
				Ext.getCmp('dirServiceCatId').setOptions([{
					text: 'Select service Category',
					value: 'null'
				}, {
					text: 'Help Desk Ticket',
					value: 'Help Desk Ticket'
				}, {
					text: 'Work Order Request',
					value: 'Work Order Request'
				}]);
			} else {
				Ext.getCmp('dirServiceCatId').setOptions([{
					text: 'Select service Category',
					value: 'null'
				}, {
					text: 'Help Desk Ticket',
					value: 'Help Desk Ticket'
				}]);

			}
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.housing.view.housingForm');
		},
		getCategoryEmailRecipent: function(category) {
			var store = mobEdu.util.getStore('mobEdu.housing.store.emails');
			var rec = store.findRecord('displayValue', category);
			if (rec != null) {
				return rec.data.enumValue;
			} else {
				Ext.Msg.show({
					message: 'No Emails got from configuration',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadRecipientList: function() {
			mobEdu.main.f.loadRecipientList(null, mobEdu.util.parentModule);
		},
		showHousingInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.housing.view.housingInfo');
			mobEdu.util.setTitle('FORMS', Ext.getCmp('dirFTitle'));
		},
		showStudentInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.housing.view.studentInfo');
		},
		onChangeCategory: function(selectbox, newValue, oldValue) {
			if (selectbox.record.data.text === 'Help Desk Ticket') {
				Ext.getCmp('dirWODes').setHidden(false).setLabel('Description');
				Ext.getCmp('dirWOrdCat').setHidden(true);
				Ext.getCmp('dirApRoom').setHidden(true);
				Ext.getCmp('hBuilSel').setHidden(true);
			} else if (selectbox.record.data.text === 'Residential Work Order') {
				Ext.getCmp('dirWOrdCat').setHidden(true);
				Ext.getCmp('hBuilSel').setHidden(false).setLabel('Building Selection');
				Ext.getCmp('dirApRoom').setHidden(false).setLabel('Apartment/Room Number');
				Ext.getCmp('dirWODes').setHidden(false).setLabel('Description');
			} else if (selectbox.record.data.text === 'Work Order Request') {
				Ext.getCmp('hBuilSel').setHidden(false);
				Ext.getCmp('dirApRoom').setHidden(false);
				Ext.getCmp('dirWOrdCat').setHidden(false);
				Ext.getCmp('dirWODes').setHidden(false).setLabel('Work Order Description');
			} else {
				Ext.getCmp('hBuilSel').setHidden(true);
				Ext.getCmp('dirApRoom').setHidden(true);
				Ext.getCmp('dirWOrdCat').setHidden(true);
				Ext.getCmp('dirWODes').setHidden(true);
			}
		}
	}
});