Ext.define('mobEdu.housing.model.emails', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'enumValue'
        }, {
            name: 'displayValue'
        }, {
            name: 'defaultSelect'
        }, {
            name: 'order'
        }]
    }
});