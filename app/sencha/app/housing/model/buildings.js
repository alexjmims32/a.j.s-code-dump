Ext.define('mobEdu.housing.model.buildings', {
    extend:'Ext.data.Model',

    config:{
        fields:[
        {
            name:'code'
        },
        {
            name:'description'
        }
        ]
    }
});