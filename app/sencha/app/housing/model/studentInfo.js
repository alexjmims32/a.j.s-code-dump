Ext.define('mobEdu.housing.model.studentInfo', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'id'
        }, {
            name: 'fullName'
        }, {
            name: 'lastName'
        }, {
            name: 'email'
        }, {
            name: 'street1'
        }, {
            name: 'street2'
        }, {
            name: 'city'
        }, {
            name: 'state'
        }, {
            name: 'county'
        }, {
            name: 'nation'
        }, {
            name: 'zip'
        }, {
            name: 'status'
        }, {
            name: 'phone'
        }]
    }
});