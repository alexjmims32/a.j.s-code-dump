Ext.define('mobEdu.housing.store.housingInfo', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.housing.model.housingInfo'
    ],

    config:{
        storeId: 'mobEdu.housing.store.housingInfo',

        autoLoad: false,

        model: 'mobEdu.housing.model.housingInfo'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
