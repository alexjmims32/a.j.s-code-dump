Ext.define('mobEdu.housing.store.buildings', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.housing.model.buildings'
    ],

    config:{
        storeId: 'mobEdu.housing.store.buildings',

        autoLoad: false,

        model: 'mobEdu.housing.model.buildings'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
