Ext.define('mobEdu.housing.store.studentInfoMail', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.housing.model.studentInfo'
    ],

    config: {
        storeId: 'mobEdu.housing.store.studentInfoMail',

        autoLoad: false,

        model: 'mobEdu.housing.model.studentInfo'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = ''
        return proxy;
    }
});