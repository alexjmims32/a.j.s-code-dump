Ext.define('mobEdu.housing.store.emails', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.housing.model.emails'
    ],

    config: {
        storeId: 'mobEdu.housing.store.emails',

        autoLoad: false,

        model: 'mobEdu.housing.model.emails'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = 'enumList'
        return proxy;
    }
});