Ext.define('mobEdu.housing.store.search', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.housing.model.search'
    ],

    config:{
        storeId: 'mobEdu.housing.store.search',

        autoLoad: false,

        model: 'mobEdu.housing.model.search'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
