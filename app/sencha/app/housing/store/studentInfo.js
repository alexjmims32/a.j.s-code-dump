Ext.define('mobEdu.housing.store.studentInfo', {
    extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.housing.model.studentInfo'
    ],

    config: {
        storeId: 'mobEdu.housing.store.studentInfo',

        autoLoad: false,

        model: 'mobEdu.housing.model.studentInfo'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty = ''
        return proxy;
    }
});