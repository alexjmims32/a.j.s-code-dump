Ext.define('mobEdu.catlogue.view.searchCourseDetail', {
    extend: 'Ext.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'dataview',
            id: 'searchCatDetailList',
            name: 'searchCatDetailList',
            disableSelection: true,
            emptyText: '<center><h3>No course selected</h3><center>',
            itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><td align="right" width="50%"><h2>Course Title :</h2></td><td align="left"><h3>{courseTitle} ({crn})</h3></td></tr>' + '<tr><td align="right" width="50%"><h2>Course Id :</h2></td><td align="left"><h3>{subject} {courseNumber}</h3></td></tr>' + '<tr><td align="right" width="50%"><h2>Subject :</h2></td><td align="left"><h3>{description}</h3></td></tr>' + '<tr><td align="right" width="50%"><h2>Seats Available :</h2></td><td align="left"><h3>{noOfSeatsAvailable} of {totalSeats}</h3></td></tr>' + '<tr><td align="right" width="50%"><h2>Instructor :</h2></td><td align="left"><h3>', '<tpl if="(mobEdu.reg.f.isEmailExist(facultyEmail) === false)">', '{faculty}', '<tpl else>', '<a href="mailto:{facultyEmail}" >{faculty}</a>', '</tpl>', '</h3></td></tr>' +
            //                '<tr><td align="right" width="50%"><h2>Lecture Hours :</h2></td><td align="left"><h3>{lectureHours}</h3></td></tr>' +
            '<tr><td align="right" width="50%"><h2>Term :</h2></td><td align="left"><h3>{termDescription}</h3></td></tr>' + '<tr><td align="right" width="50%"><h2>Department :</h2></td><td align="left"><h3>{department}</h3></td></tr>' + '<tr><td align="right" width="50%"><h2>College :</h2></td><td align="left"><h3>{college}</h3></td></tr>' + '<tr><td align="right" valign="top" width="50%"><h2>Level :</h2></td><td align="left"><p><h3>{[this.getLevel(values.level)]}</h3></p></td></tr>' + '<tr><td align="right" width="50%"><h2>Campus :</h2></td><td align="left"><h3>{campus}</h3></td></tr>' + '<tr><td valign="top" align="right" width="50%"><h2>Schedule :</h2></td><td align="left"><h3>' + '<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===true)">', '<tpl for="meeting">', '<tpl if="(mobEdu.reg.f.isExists(startDate)===true && mobEdu.reg.f.isExists(endDate)===true)">', '{startDate} : {endDate}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(beginTime)===true && mobEdu.reg.f.isExists(endTime)===true)">', '{beginTime} - {endTime}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(period)===true)">', '{period}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(bldgCode)===true)">', 'Building: {bldgCode}<br />' + '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(roomCode)===true)">', 'Room: {roomCode}<br /><br />', '</tpl>', '</tpl>', '</tpl>', '<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===false)">', 'No meeting info available', '</tpl>', '</h3></td></tr>' + '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '<tr><td valign="top" align="right" width="50%">', '<h2>Comments :</h2></td><td align="left"><h4>{courseComments}' + '</h4></td></tr>' + '</tpl>', '</table>', {
                compiled: true,
                getLevel: function(level) {
                    return String(level).replace(/[,]/g, "<br />");
                }
            }),
            store: mobEdu.util.getStore('mobEdu.reg.store.courseDetailLocal'),
            loadingText: '',
            singleSelect: true
        },
        {
            xtype: 'customToolbar',
            title: '<h1>Course Details</h1>'
        }],
        flex: 1
    }
});