Ext.define('mobEdu.catlogue.view.tablet.search', {
	extend: 'Ext.Panel',
	//    requires: 'Ext.plugin.PullRefresh',
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'customToolbar',
			id: 'catalogT'
		}, {
			xtype: 'fieldset',
			docked: 'top',
			cls: 'searchfieldset',
			items: [{
				xtype: 'selectfield',
				name: 'instCrseCatTermId',
				valueField: 'code',
				displayField: 'description',
				id: 'instCrseCatTermId',
				store: mobEdu.util.getStore('mobEdu.reg.store.termList')
			}, {
				layout: 'hbox',
				items: [{
					xtype: 'textfield',
					name: 'crseCatItem',
					id: 'crseCatItem',
					flex: 1,
					placeHolder: 'Search for a course'
				}, {
					xtype: 'button',
					name: 'searchCatIcon',
					id: 'searchCatIcon',
					maxWidth: '32px',
					flex: 3,
					text: '<img src="' + mobEdu.util.getResourcePath() + 'images/searchIcon.png" height="24px" width="24px">',
					style: {
						border: 'none',
						background: 'none'
					},
					handler: function() {
						mobEdu.reg.f.doCheckBeforeSearch(Ext.getCmp('crseCatItem'), Ext.getCmp('searchCatList'), Ext.getCmp('instCrseCatTermId'));
					}
				}, {
					xtype: 'button',
					name: 'searchCatHelp',
					id: 'searchCatHelp',
					maxWidth: '32px',
					flex: 3,
					text: '<a href="javascript:mobEdu.reg.f.loadTipsPopup();"><img src="' + mobEdu.util.getResourcePath() + 'images/tip.png" height="32px" width="32px"></a>',
					style: {
						border: 'none',
						background: 'none'
					},
					handler: function() {
						mobEdu.catlogue.f.loadTipsPopup();
					}
				}]
			}]
		}, {
			xtype: 'list',
			name: 'searchCatList',
			id: 'searchCatList',
			style: 'background: transparent',
			emptyText: 'No courses available',
			itemTpl: new Ext.XTemplate('<table width="100%">' + '<tr><th colspan="2" style="color:#000000;"><h2>{courseTitle} ({crn})</h2></th></tr>' + '<tr  style="color:#000000">', '<td>', '<table width="100%"  width="80%">', '<tr>', '<td width="50%"><h3>{subject} {courseNumber} /{seqNo}', '<tpl if="(mobEdu.reg.f.isExists(linkId)===true)">', ' {linkId}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExists(lectureHours)===true)">', ' ({lectureHours} hrs)', '</tpl>', '</h3></td>', '<td><h3>{faculty}</h3></td>', '</tr>', '<tr>', '<td><h3>{department}</h3></td>', '<td><h3>{college}</h3></td>', '</tr>', '</table>', '</td>', '<tpl if="(mobEdu.reg.f.isExists(courseComments)===true)">', '</tr><tr style="color:#000000"><td colspan="3"><p><h4>{courseComments}</h4></p></td>', '</tpl>', '</tr>', '</table>'),
			store: mobEdu.util.getStore('mobEdu.reg.store.search'),
			loadingText: '',
			singleSelect: true,
			listeners: {
				itemtap: function(view, index, target, item, e, o) {
					mobEdu.reg.f.onSearchCourseItemTap(view, index, item, e, null, null, 'catlog');
				}
			}
		}],
		flex: 1
	},
	initialize: function() {
		var searchStore = mobEdu.util.getStore('mobEdu.reg.store.search');
		searchStore.addBeforeListener('load', mobEdu.reg.f.checkForSearchListEnd, this);
	}
});