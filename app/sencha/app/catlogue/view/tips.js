Ext.define('mobEdu.catalogue.view.tips', {
    extend: 'Ext.Panel',
    config: {
        id: 'tipsPopup',
        scrollable: true,
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '70%',
        height: '50%',
        layout: 'fit',
        cls: 'logo',
        items: [{
            padding: '0 10 10 10',
            scrollable: true,
            html: '<p><h3>Here you can search for a course based on different parameters like course number, crn, description, subject, instructor etc...</p><br/><br/>For e.g.,<br/>Math 101<br/>Anthropology<br/>Accounting Jonathan</h3><br/></p><br/>'
        }],
        plugins: [new Ext.ux.PanelAction({
            iconClass: 'x-panel-action-icon-close',
            position: 'tr'
        })],
        flex: 1
    }
});