Ext.define('mobEdu.catlogue.f', {

    statics: {
        loadCrseCatlog: function() {

            if (Ext.os.is.Phone) {
                mobEdu.util.get('mobEdu.catlogue.view.phone.search');
            } else {
                mobEdu.util.get('mobEdu.catlogue.view.tablet.search');
            }

            Ext.getCmp('crseCatItem').setValue('');
            if (Ext.getCmp('pagingplugin') != null) {
                Ext.getCmp('pagingplugin').getLoadMoreCmp().hide();
            }
            Ext.getCmp('searchCatList').setPlugins(null);
            Ext.getCmp('searchCatList').setEmptyText(null);
            mobEdu.reg.f.clearLocalSearchStore();
            mobEdu.reg.f.loadTermList('mobEdu.catlogue.f.showSearch');
        },
        showSearch: function() {
            mobEdu.util.updatePrevView(mobEdu.catlogue.f.onCrseCatBackTap);
            var title = mobEdu.util.getTitleInfo();
            if (Ext.os.is.Phone) {
                mobEdu.util.show('mobEdu.catlogue.view.phone.search');
                mobEdu.util.setTitle('CATLOGUE', Ext.getCmp('catalogP'));
            } else {
                mobEdu.util.show('mobEdu.catlogue.view.tablet.search');
                mobEdu.util.setTitle('CATLOGUE', Ext.getCmp('catalogT'));
            }

            Ext.getCmp('searchCatList').refresh();
        },
        onCrseCatBackTap: function() {
            Ext.getCmp('crseCatItem').setValue('');
            if (Ext.getCmp('pagingplugin') != null) {
                Ext.getCmp('pagingplugin').getLoadMoreCmp().hide();
            }
            Ext.getCmp('searchCatList').setPlugins(null);
            Ext.getCmp('searchCatList').setEmptyText(null);
            mobEdu.reg.f.clearLocalSearchStore();
            mobEdu.util.showMainView();
        },
        loadTipsPopup: function(popupView) {
            if (!popupView) {
                var popup = popupView = Ext.Viewport.add(
                    mobEdu.util.get('mobEdu.catalogue.view.tips')
                );
                Ext.Viewport.add(popupView);
            }
            popupView.show();
        },
        showSearchDetail: function() {
            mobEdu.util.updatePrevView(mobEdu.catlogue.f.showSearch);
            mobEdu.util.show('mobEdu.catlogue.view.searchCourseDetail');
        }
    }
});