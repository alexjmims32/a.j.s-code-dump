Ext.define('mobEdu.sports.view.results', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.rssSports'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'resultsLinks',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + 
                '<td colspan="3" width="100%" align="left" ><h2>{[this.getTitle(values.title)]}</h2></td>' + 
                '</tr>'+
                '<tr><td width="94%" valign="top" align="left" ><h4><i>{[this.getDescription(values.description)]}</i></h4></td>'+
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(stats)===true)">', 
                    '<td width="3%" valign="bottom" align="right"><a href="javascript:mobEdu.util.loadExternalUrl(\'{stats}\')"><img width="32" height="32" src="' + mobEdu.util.getResourcePath() + 'images/athletics/finalstats.png"  name="url" /></a></td>'+
                '</tpl>',
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(article)===true)">',
                    '<td width="3%" valign="bottom" align="right"><a href="javascript:mobEdu.util.loadExternalUrl(\'{article}\')"><img width="32" height="32" src="' + mobEdu.util.getResourcePath() + 'images/athletics/fulltext.png" name="url"  /></a></td>'+
                '</tpl>',
                '</tr></table>', 
                {   
                    compiled: true,
                    getTitle: function(title) {
                        var nTitle = title.split(']', 2);
                        if(nTitle.length == 2) {
                            return nTitle[1];
                        } else {
                            return title;
                        }
                    },
                    getDescription:function(description){
                        
                        var a, parent, div = document.createElement('div');
                        div.innerHTML = description;
                        a = div.getElementsByTagName( 'A' );
                        while( a[0] ) {
                            parent = a[0].parentNode;                            
                            parent.removeChild(a[0]);
                        }
                        description =  div.innerHTML;
                        if(description.substring(description.length-46,description.length-45)=='-'){
                            description = description.substring(0,description.length-46) + 
                                description.substring(description.length-45,description.length-1);
                        }
                        return description;
                        
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.sports.store.rssSports'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.loadSportsDetail(view, index, target, record, item, e, eOpts);
                }
            }
        },{
            xtype: 'customToolbar',
            id: 'rssResultsTitle'
        },
        {
            xtype: 'selectfield',
            docked:'top',
            name: 'resultsParentField',
            id: 'resultsParentField',
            required: true,
            listeners: {
                change: function(selectfield, newValue, oldValue, eOpts) {
                    mobEdu.sports.f.onParentFieldSelectionChange(selectfield, newValue, oldValue, eOpts);
                }
            }
        }],
        flex: 1
    }
});