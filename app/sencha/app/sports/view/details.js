Ext.define('mobEdu.sports.view.details', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.sportsDetail'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            setHtmlContent: true,
            padding: '10 10 10 10',
            xtype: 'dataview',
            id:'sportsDetails',
            name:'sportsDetails',
            cls: 'logo',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%">' +                 
                '<tr><td width="5%" align="left">'+
                '<tpl if="campusCode==\'UT\'">', 
                '<img height=47 width=27 src="' + mobEdu.util.getResourcePath() + 'images/athletics/back.png" name="detailsBack" />',
                '</tpl>',
                '</td>'+
                '<td valign="top" align="center">'+
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(url)===true)">', 
                '<img class="imageWidth" src="{url}" alt="">'+
                '</tpl>', 
                '</td>', 
                '<td width="5%" align="right">'+
                '<tpl if="campusCode==\'UT\'">', 
                '<img height=47 width=27 src="' + mobEdu.util.getResourcePath() + 'images/athletics/forward.png" name="detailsForward" />',
                '</tpl>',
                '</td></tr>'+ 
                '<tr><td colspan="3"><br /></td></tr>'+
                '<tr><td colspan="2" valign="top"><h2>{[this.getTitle(values.title)]}</h2></td>'+
                '<td rowspan="2">'+
                '<tpl if="campusCode==\'UT\'">', 
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(article)===true)">', 
                '<img height=52 width=72 src="' + mobEdu.util.getResourcePath() + 'images/athletics/detailsfulltext.png" name="detailsFullText" />',
                '</tpl>', 
                '</tpl>',
                '</td></tr>' + 
                '<tr><td colspan="3"><h4><i>{pubDate}</i></h4></td></tr>', 
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(description)===true)">', 
                '<tr><td colspan="3"><h3>{[this.getDescription(values.description)]}</h3></td></tr>', 
                '</tpl>',            
                '<tpl if="(mobEdu.feeds.f.isLinkExists(link)===true)">', '<tr><td><a href="javascript:mobEdu.util.loadExternalUrl(\'{link}\')"><p style="width:100px" class="listReadMore">More Info</p></a></td></tr>', '</tpl>',
                '</table>', {
                    compiled: true,
                    getTitle: function(title) {
                        var nTitle = title.split(']', 2);
                        if(nTitle.length == 2) {
                            return nTitle[1];

                        } else {
                            return title;
                        }
                    },
                    getDescription: function(description) {
                        return mobEdu.util.convertRelativeToAbsoluteUrls(description);
                    }
                }),

            store: mobEdu.util.getStore('mobEdu.sports.store.sportsDetail'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.onDetailsItemTap(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            title: '<h1>Details</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }

});