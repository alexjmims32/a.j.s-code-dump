Ext.define('mobEdu.sports.view.schedules', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.rssSports'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'schedulesLinks',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + 
                '<td colspan="4" width="100%" align="left" ><h2>{[this.getTitle(values.title)]}</h2></td>' + 
                '</tr>'+
                '<tr><td width="91%" valign="top" align="left" ><h4><i>{[this.getDescription(values.description)]}</i></h4></td>'+
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(video)===true)">',
                    '<td width="3%" valign="bottom" align="right"><a href="javascript:mobEdu.util.loadExternalUrl(\'{video}\')"><img width="32" height="32" src="' + mobEdu.util.getResourcePath() + 'images/athletics/livevideo.png" name="url" /></a></td>'+
                '</tpl>',
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(audio)===true)">',
                    '<td width="3%" valign="bottom" align="right"><a href="javascript:mobEdu.util.loadExternalUrl(\'{audio}\')"><img width="32" height="32" src="' + mobEdu.util.getResourcePath() + 'images/athletics/liveaudio.png" name="url" /></td>'+
                '</tpl>',
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(stats)===true)">',
                    '<td width="3%" valign="bottom" align="right"><a href="javascript:mobEdu.util.loadExternalUrl(\'{article}\')"><img width="32" height="32" src="' + mobEdu.util.getResourcePath() + 'images/athletics/livestats.png" name="url" /></a></td>'+
                '</tpl>',
                '</tr></table>', 
                {   
                    compiled: true,
                    getTitle: function(title) {
                        var nTitle = title.split(']', 2);
                        if(nTitle.length == 2) {
                            return nTitle[1];
                        } else {
                            return title;
                        }
                    },
                    getDescription:function(description){
                        var fmtDescription=description.replace(/<(IMG|img)[^>]*>/g,"");
                        
                        if(fmtDescription.substring(fmtDescription.length-15,fmtDescription.length-14)=='-'){
                            fmtDescription = fmtDescription.substring(0,fmtDescription.length-15) + 
                                fmtDescription.substring(fmtDescription.length-14,fmtDescription.length-1);
                        }
                        return fmtDescription;
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.sports.store.rssSports'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.loadSportsDetail(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'rssSchedulesTitle'
        },
        {
            xtype: 'selectfield',
            docked:'top',
            name: 'schedulesParentField',
            id: 'schedulesParentField',
            required: true,
            listeners: {
                change: function(selectfield, newValue, oldValue, eOpts) {
                    mobEdu.sports.f.onParentFieldSelectionChange(selectfield, newValue, oldValue, eOpts);
                }
            }
        }],
        flex: 1
    }
});