Ext.define('mobEdu.sports.view.rssList', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.rssSports'	
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'sportsLinks',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + '<td width="100%" align="left" ><h3>{[this.getTitle(values.title)]}</h3></td>' + '<td width="10%"><div align="right" class="arrow" /></td></tr></table>', {
                compiled: true,
                getTitle: function(title) {
                    var nTitle = title.split(']', 2);
                    if(nTitle.length == 2) {
                        return nTitle[1];
                    } else {
                        return title;
                    }
                }
            }),
            store: mobEdu.util.getStore('mobEdu.sports.store.rssSports'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.loadSportsDetail(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'rssSportsTitle'
        }],
        flex: 1
    }
});