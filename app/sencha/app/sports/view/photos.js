Ext.define('mobEdu.sports.view.photos', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.rssSports'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'photosLinks',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + 
                '<td width="60%" align="left" ><h2>{[this.getTitle(values.title)]}</h2></td>' + 
                '<td width="40%" class="photos" rowspan="2">'+
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(url)===true)">',
                '<img style="background:url({url}) center no-repeat;background-size:72px 72px;" width="100" height="100" src="'+ mobEdu.util.getResourcePath() +'images/athletics/albumtransparent.png" alt="" />',
                '</tpl>'+
                '<tr><td width="100%" align="left" valign="top"><h4><i>{[this.getDate(values.pubDate)]}</i></h4></td></tr></table>', 
                {   
                    compiled: true,
                    getTitle: function(title) {
                        var nTitle = title.split(']', 2);
                        if(nTitle.length == 2) {
                            return nTitle[1];
                        } else {
                            return title;
                        }
                    },
                    getDate:function(date){
                        var fmtDate = new Date(date);
                        fmtDate = Ext.Date.format(fmtDate, 'D, d M Y');
                        return fmtDate;
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.sports.store.rssSports'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.loadSportsDetail(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'rssPhotosTitle'
        },
        {
            xtype: 'selectfield',
            docked:'top',
            name: 'photosParentField',
            id: 'photosParentField',
            required: true,
            listeners: {
                change: function(selectfield, newValue, oldValue, eOpts) {
                    mobEdu.sports.f.onParentFieldSelectionChange(selectfield, newValue, oldValue, eOpts);
                }
            }
        }],
        flex: 1
    }
});