Ext.define('mobEdu.sports.view.media', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.rssSports'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'multimediaLinks',
            disableSelection: true,
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + 
                '<td width="80%" align="left" ><h2>{[this.getTitle(values.title)]}</h2></td>' + 
                '<td width="10%" rowspan="2" >'+
                '<tpl if="(mobEdu.sports.f.isDescriptionExists(video)===true)">',
                '<a href="javascript:mobEdu.util.loadExternalUrl(\'{video}\')"><img style="background:url({url}) center no-repeat;background-size:72px 43px;" width="75" height="45" src="'+ mobEdu.util.getResourcePath() +'images/athletics/mediaframe.png" alt="" /></a>',
                '</tpl>'+
                '</td></tr>'+
                '<tr><td width="100%" align="left" ><h4><i>{pubDate}</i></h4></td></tr></table>', 
                {   
                    compiled: true,
                    getTitle: function(title) {
                        var nTitle = title.split(']', 2);
                        if(nTitle.length == 2) {
                            return nTitle[1];
                        } else {
                            return title;
                        }
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.sports.store.rssSports'),
            singleSelect: true,
            loadingText: ''
//            listeners: {
//                itemtap: function(view, index, target, record, item, e, eOpts) {
//                    mobEdu.sports.f.loadSportsDetail(view, index, target, record, item, e, eOpts);
//                }
//            }
        }, {
            xtype: 'customToolbar',
            id: 'rssMultimediaTitle'
        },
        {
            xtype: 'selectfield',
            docked:'top',
            name: 'multimediaParentField',
            id: 'multimediaParentField',
            required: true,
            listeners: {
                change: function(selectfield, newValue, oldValue, eOpts) {
                    mobEdu.sports.f.onParentFieldSelectionChange(selectfield, newValue, oldValue, eOpts);
                }
            }
        }],
        flex: 1
    }
});