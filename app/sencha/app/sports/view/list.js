Ext.define('mobEdu.sports.view.list', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.sportsLocal'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'sportsList',
            itemTpl: '<table width="100%"><tr><td width="80%" align="left" ><h3>{name}</h3></td><td width="20%" align="right" ><h4>{date}</h4></td><td width="10%"><div align="right" class="arrow" /></td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.sports.store.sportsLocal'),
            singleSelect: true,
            loadingText: '',
            scrollToTopOnRefresh: false,
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.onSportsItemTap(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'sportsTitle'
        }        
        ],
        flex: 1
    }
});