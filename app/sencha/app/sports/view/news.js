Ext.define('mobEdu.sports.view.news', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.sports.store.rssSports'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'newsLinks',
            itemTpl: new Ext.XTemplate('<table width="100%"><tr>' + 
                '<td width="100%" align="left" ><h2>{[this.getTitle(values.title)]}</h2></td>' + 
                '<td width="10%" rowspan="2"><div align="right" class="arrow" /></td></tr>'+
                '<tr><td width="100%" align="left" ><h4><i>{[this.getDate(values.pubDate)]}</i></h4></td></tr></table>', 
                {   
                    compiled: true,
                    getTitle: function(title) {
                        var nTitle = title.split(']', 2);
                        if(nTitle.length == 2) {
                            return nTitle[1];
                        } else {
                            return title;
                        }
                    },
                    getDate:function(date){
                        var fmtDate = new Date(date);
                        fmtDate = Ext.Date.format(fmtDate, 'l, F d, Y h:i A');
                        return fmtDate;
                    }
                }),
            store: mobEdu.util.getStore('mobEdu.sports.store.rssSports'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.sports.f.loadSportsDetail(view, index, target, record, item, e, eOpts);
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'rssNewsTitle'
        },
        {
            xtype: 'selectfield',
            docked:'top',
            name: 'newsParentField',
            id: 'newsParentField',
            required: true,
            listeners: {
                change: function(selectfield, newValue, oldValue, eOpts) {
                    mobEdu.sports.f.onParentFieldSelectionChange(selectfield, newValue, oldValue, eOpts);
                }
            }
        }
    ],
        flex: 1
    }
});