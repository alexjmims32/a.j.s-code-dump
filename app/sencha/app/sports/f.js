Ext.define('mobEdu.sports.f', {
	requires: [
		'mobEdu.sports.store.rssSports',
		'mobEdu.sports.store.sports',
		'mobEdu.sports.store.sportsDetail',
		'mobEdu.sports.store.sportsLocal'
	],
	statics: {
		curParentID: null,
		curModuleID: null,
		curFeedID: null,
		title: null,
		sports: null,
		editMode: false,
		loadSports: function() {
			// Get the module ID for sports and store it as static for future usage
			var id = mobEdu.sports.f.getModuleId();
			mobEdu.sports.f.curModuleID = id;
			if (id != null && id != '') {
				mobEdu.util.get('mobEdu.sports.view.list');
				var btnEdit = Ext.getCmp('sportsEdit');
				if (mobEdu.sports.f.editMode) {
					mobEdu.sports.f.updateItemTpl(Ext.getCmp('sportsList'));
					btnEdit.show();
					mobEdu.sports.f.editMode = false;
				}
				mobEdu.sports.f.loadFeedList(id, 0);
			} else {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>Invalid main menu configuration.</center>',
					buttons: Ext.MessageBox.OK
				});
			}
		},
		loadFeedList: function(m, p, doFilter, doBack) {
			mobEdu.sports.f.curFeedID = null;
			mobEdu.sports.f.curParentID = null;
			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
			var records = new Array();
			feedLocalStore.each(function(record) {
				if (record.data.moduleID == m && record.data.parentID == p) {
					records.push(record);
				}
			});
			if (records.length == 1) {
				if (records.length == 1 && records[0].data.type == 'ELINK') {
					mobEdu.util.loadExternalUrl(records[0].data.link);
				} else {
					if (doBack) {
						var backRec = feedLocalStore.findRecord('feedId', records[0].data.parentID);
						mobEdu.sports.f.loadFeedList(m, backRec.data.parentID, doFilter);
						mobEdu.sports.f.setTitle(backRec.get('parentID'));
					} else {
						mobEdu.sports.f.loadFeedList(m, records[0].data.feedId, doFilter, doBack);
						mobEdu.sports.f.setTitle(records[0].get('feedId'));
					}

					return;
				}
			} else if (records.length == 0) {
				var records = new Array();
				feedLocalStore.each(function(record) {
					if (record.data.moduleID == m && record.data.feedId == p) {
						records.push(record);
					}
				});
				if (records.length == 1 && records[0].data.type == 'LINK') {
					mobEdu.sports.f.curFeedID = records[0].data.feedId;
					mobEdu.sports.f.curParentID = records[0].data.feedId;
					mobEdu.sports.f.loadRssFeed(records[0]);
				} else {
					Ext.Msg.show({
						title: null,
						cls: 'msgbox',
						message: '<center>No activities, please check back soon.</center>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								mobEdu.util.showMainView();
							}
						}
					});
				}
			} else {
				var sportsLocal = mobEdu.util.getStore('mobEdu.sports.store.sportsLocal');
				if (sportsLocal.data.length > 0) {
					sportsLocal.removeAll();
					sportsLocal.sync();
					sportsLocal.removed = [];
				}
				for (i = 0; i < records.length; i++) {
					if (doFilter == false) {
						sportsLocal.add(records[i]);
						sportsLocal.sync();
					} else {
						if (mobEdu.util.isFeedIdExists(records[i].data.feedId) == false) {
							sportsLocal.add(records[i]);
							sportsLocal.sync();
						}
					}
				}
				mobEdu.sports.f.curParentID = p;
				if (doFilter == null) {
					mobEdu.sports.f.showSports();
				}
				feedLocalStore.each(function(record) {
					if (record.data.moduleID == m && record.data.feedId == p) {
						mobEdu.sports.f.setTitle(record.get('feedId'));
						return false;
					}
				});
			}
		},

		loadRssFeed: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.sports.store.rssSports');
			store.getProxy().setUrl(encorewebserver + '/open/feeds/content');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setExtraParams({
				feedId: rec.data.feedId,
				campusCode: campusCode,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.sports.f.rssFeedResponseHandler(rec);
			};
			store.load();
			mobEdu.util.gaTrackEvent('encore', 'feedcontent');
		},
		rssFeedResponseHandler: function(rec) {
			var store = mobEdu.util.getStore('mobEdu.sports.store.rssSports');
			if (store.data.length > 0) {
				if (campusCode == 'UT') {
					var name = rec.get('name');
					if (name == 'News') {
						mobEdu.sports.f.title = 'News';
						mobEdu.sports.f.showNews();
						Ext.getCmp('rssNewsTitle').setTitle('<h1>' + mobEdu.util.getTitleInfo('ATHLETICS') + ': ' + name + '</h1>');
						mobEdu.sports.f.parentList('News', Ext.getCmp('newsParentField'));
						mobEdu.sports.f.showNews();
					} else if (name == 'Results') {
						mobEdu.sports.f.title = 'Results';
						mobEdu.sports.f.showResults();
						Ext.getCmp('rssResultsTitle').setTitle('<h1>' + mobEdu.util.getTitleInfo('ATHLETICS') + ': ' + name + '</h1>');
						mobEdu.sports.f.parentList('Results', Ext.getCmp('resultsParentField'));
						mobEdu.sports.f.showResults();
					} else if (name == 'Schedules') {
						mobEdu.sports.f.title = 'Schedules';
						mobEdu.sports.f.showSchedules();
						Ext.getCmp('rssSchedulesTitle').setTitle('<h1>' + mobEdu.util.getTitleInfo('ATHLETICS') + ': ' + name + '</h1>');
						mobEdu.sports.f.parentList('Schedules', Ext.getCmp('schedulesParentField'));
						mobEdu.sports.f.showSchedules();
					} else if (name == 'Photos') {
						mobEdu.sports.f.title = 'Photos';
						mobEdu.sports.f.showPhotos();
						Ext.getCmp('rssPhotosTitle').setTitle('<h1>' + mobEdu.util.getTitleInfo('ATHLETICS') + ': ' + name + '</h1>');
						mobEdu.sports.f.parentList('Photos', Ext.getCmp('photosParentField'));
						mobEdu.sports.f.showPhotos();
					} else if (name == 'Multimedia') {
						mobEdu.sports.f.title = 'Multimedia';
						mobEdu.sports.f.showMultimedia();
						Ext.getCmp('rssMultimediaTitle').setTitle('<h1>' + mobEdu.util.getTitleInfo('ATHLETICS') + ': ' + name + '</h1>');
						mobEdu.sports.f.parentList('Multimedia', Ext.getCmp('multimediaParentField'));
						mobEdu.sports.f.showMultimedia();
					} else {
						mobEdu.sports.f.showRssSports();
						Ext.getCmp('rssSportsTitle').setTitle('<h1>' + rec.get('name') + '</h1>');
					}
				} else {
					mobEdu.sports.f.showRssSports();
					Ext.getCmp('rssSportsTitle').setTitle('<h1>' + rec.get('name') + '</h1>');
				}
			} else {
				Ext.Msg.show({
					title: null,
					cls: 'msgbox',
					message: '<center>Please check back soon.</center>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
							var record = feedLocalStore.findRecord('feedId', rec.get('parentID'));
							if (record != null) {
								mobEdu.sports.f.curParentID = record.get('parentID');
							}
						}
					}
				});
			}
		},
		loadSportsDetail: function(view, index, target, record, item, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			if (item.target.name != 'url') {
				var store = mobEdu.util.getStore('mobEdu.sports.store.sportsDetail');
				store.removeAll();
				store.removed = [];
				store.sync();
				store.add(record.copy());
				store.sync();
				mobEdu.sports.f.showSportsDetail();
			}
		},
		onSportsItemTap: function(view, index, target, record, item, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			if (!(mobEdu.sports.f.editMode)) {
				mobEdu.sports.f.loadFeedList(record.data.moduleID, record.data.feedId, record.data.description);
			} else {
				if (item.target.name == "on") {
					mobEdu.sports.f.sportsOn(record.data.feedId);
				} else if (item.target.name == "off") {
					mobEdu.sports.f.sportsOff(record.data.feedId);
				}
			}
		},
		getSportsTitle: function() {
			var modStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
			var rec = modStore.findRecord('code', 'ATHLETICS', null, null, null, true);
			var title = null;
			if (rec != null) {
				title = rec.get('description');
			}
			return title;
		},
		getModuleId: function() {
			var modStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
			var rec = modStore.findRecord('code', 'ATHLETICS', null, null, null, true);
			var id = null;
			if (rec != null) {
				id = rec.get('moduleId');
			}
			return id;
		},
		isImageExists: function(description, url) {
			if (url == null || url == '') {
				return false;
			} else if (description == null || description == '') {
				return true;
			} else if (description.search(url) != -1) {
				return false;
			}
			return true;
		},
		isDescriptionExists: function(description) {
			if (description == null || description == '') {
				return false;
			}
			return true;
		},
		onBackButtonTap: function() {
			var btnEdit = Ext.getCmp('sportsEdit')
			if (mobEdu.sports.f.editMode) {
				mobEdu.sports.f.updateItemTpl(Ext.getCmp('sportsList'));
				mobEdu.sports.f.updateFeeds(true);
				//                cmp.setHidden(false);
				btnEdit.show();
				mobEdu.sports.f.editMode = false;


			} else {
				if (mobEdu.sports.f.curParentID == 0) { // || mobEdu.sports.f.curParentID == mobEdu.sports.f.curModuleID) {
					mobEdu.util.showMainView();
					return;
				}
				var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
				feedLocalStore.each(function(record) {
					if (record.data.feedId == mobEdu.sports.f.curParentID) {
						if (record.data.parentID == 0) {
							mobEdu.util.showMainView();
						} else {
							mobEdu.sports.f.loadFeedList(mobEdu.sports.f.curModuleID, record.data.parentID, null, true);
						}
						return false;
					}
				});
			}
		},

		onDetailsItemTap: function(view, index, target, item, e, o, eOpts) {
			var detailsStore = mobEdu.util.getStore('mobEdu.sports.store.sportsDetail');
			var id = detailsStore.getAt(0).get('title');
			var store = mobEdu.util.getStore('mobEdu.sports.store.rssSports');
			var rec;
			var cnt = 0;
			store.each(function(record) {
				if (record.data.title == id) {
					index = cnt;
				}
				cnt = cnt + 1;
			});
			if (e.target.name == "detailsBack") {
				if (index == 0) {
					index = store.data.length;
				}
				rec = store.getAt(index - 1);
				detailsStore.removeAll();
				detailsStore.sync();
				detailsStore.removed = [];

				detailsStore.add(rec.copy());
				detailsStore.sync();

			} else if (e.target.name == "detailsForward") {
				if (index == store.data.length - 1) {
					index = -1;
				}
				rec = store.getAt(index + 1);
				detailsStore.removeAll();
				detailsStore.sync();
				detailsStore.removed = [];

				detailsStore.add(rec.copy());
				detailsStore.sync();
			} else if (e.target.name == "detailsFullText") {
				detailsStore = mobEdu.util.getStore('mobEdu.sports.store.sportsDetail');
				var record = detailsStore.getAt(0);
				var link = record.get('article');
				if (link != null && link != '') {
					mobEdu.util.loadExternalUrl(record.get('article'));
				}
			}


			Ext.getCmp('sportsDetails').refresh();
		},

		updateTitle: function(id, skipRoot) {
			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
			var rec = feedLocalStore.getAt(feedLocalStore.findExact('feedId', id));
			var parentId = rec.get('parentID');
			if (mobEdu.sports.f.title == null) {
				mobEdu.sports.f.title = rec.get('name');
			} else {
				if (skipRoot) {
					if (parentId != 0) {
						mobEdu.sports.f.title = rec.get('name') + ' : ' + mobEdu.sports.f.title;
					}
				} else {
					mobEdu.sports.f.title = rec.get('name') + ' : ' + mobEdu.sports.f.title;
				}
			}
			if (parentId != 0) {
				mobEdu.sports.f.updateTitle(parentId, skipRoot);
			}
		},

		setTitle: function(id) {
			mobEdu.sports.f.title = null;
			mobEdu.sports.f.updateTitle(id);
			// Set the title on events list view
			var titleEle = Ext.getCmp('sportsTitle');
			if (titleEle != null) {
				titleEle.setTitle('<h1>' + mobEdu.sports.f.title + '</h1>');

				// On Android tablets this title will be set but the changes wont appear immediately
				// If we tap the view or scroll it, then the title changes. Its just a refresh issue.
				// Fixed it by a workaround. Hide and Show the view. This will re-render all components in the view.
				mobEdu.util.get('mobEdu.sports.view.list').hide();
				mobEdu.util.get('mobEdu.sports.view.list').show();
			}
			//            mobEdu.sports.f.title=null;
		},

		parentList: function(type, cmp) {
			var parent = mobEdu.sports.f.getModuleId();
			mobEdu.sports.f.sports = new Array();
			mobEdu.sports.f.findItem(parent, type);
			cmp.suspendEvents(false);
			cmp.setOptions(mobEdu.sports.f.sports);
			cmp.setValue(mobEdu.sports.f.curFeedID)
			cmp.resumeEvents(true);
		},

		findItem: function(recordId, type) {
			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
			feedLocalStore.each(function(record) {
				if ((record.data.parentID == recordId) || (record.data.parentID == 0 && record.data.moduleID == recordId)) {
					if (record.data.name == type) {
						mobEdu.sports.f.title = null;
						mobEdu.sports.f.updateTitle(record.data.parentID, true);
						var index = (mobEdu.sports.f.sports).length;
						mobEdu.sports.f.sports[index] = ({
							text: mobEdu.sports.f.title,
							value: record.data.feedId
						});
					} else {
						mobEdu.sports.f.findItem(record.data.feedId, type);
					}
				}
			});
		},

		onParentFieldSelectionChange: function(selectfield, newValue, oldValue, eOpts) {
			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenusLocal');
			var rec = feedLocalStore.findRecord('feedId', newValue);

			var store = mobEdu.util.getStore('mobEdu.sports.store.rssSports');
			store.getProxy().setHeaders({
				companyID: companyId
			});
			store.getProxy().setUrl(encorewebserver + 'getFeedContent');
			store.getProxy().setExtraParams({
				feedId: rec.data.feedId,
				campusCode: campusCode,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				var store = mobEdu.util.getStore('mobEdu.sports.store.rssSports');
				if (store.data.length == 0) {
					Ext.Msg.show({
						title: null,
						cls: 'msgbox',
						message: '<center>Please check back soon.</center>',
						buttons: Ext.MessageBox.OK
					});
				}
			};
			store.load();
			mobEdu.util.gaTrackEvent('encore', 'feedcontent');
		},

		onEditButtonTap: function() {
			var cmp = Ext.getCmp('sportsEdit');
			mobEdu.sports.f.updateFeeds(false);
			mobEdu.sports.f.updateItemTpl(Ext.getCmp('sportsList'));
			cmp.hide();
			mobEdu.sports.f.editMode = true;
			Ext.getCmp('sportsList').refresh();
		},

		updateFeeds: function(doFilter) {
			if (mobEdu.sports.f.curParentID == mobEdu.sports.f.curModuleID) {
				var id = mobEdu.sports.f.getModuleId();
				mobEdu.sports.f.loadFeedList(id, 0, doFilter)
			} else {
				mobEdu.sports.f.loadFeedList(mobEdu.sports.f.curModuleID, mobEdu.sports.f.curParentID, doFilter);
			}
		},

		updateItemTpl: function(listCmp) {
			if (mobEdu.sports.f.editMode) {
				listCmp.setItemTpl(new Ext.XTemplate('<table width="100%"><tr>' +
					'<td width="75%" align="left" ><h3>{name}</h3></td>' +
					'<td width="20%" align="right" ><h4>{date}</h4></td>' +
					'<td width="5%"><img src="' + mobEdu.util.getResourcePath() + 'images/arrow.png" align="right"></td>' +
					'</tr></table>'));
			} else {
				listCmp.setItemTpl(new Ext.XTemplate('<table width="100%"><tr>' +
					'<td width="5%">' +
					'<div style="background: transparent; width: 50px;" class="x-field-checkbox x-field">',
					'<tpl if="mobEdu.util.isFeedIdExists(feedId)===false">',
					'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 name="on" />' +
					'<tpl else>',
					'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 name="off" />',
					'</tpl>' +
					'</div>' +
					'<td width="75%" align="left" ><h3>{name}</h3></td>' +
					'<td width="20%" align="right" ><h4>{date}</h4></td>' +
					'</tr></table>'));
			}
			listCmp.refresh();
		},

		sportsOn: function(feedId) {
			var cmp = Ext.getCmp('sportsList');
			mobEdu.util.addFeedToHiddenFeeds(feedId, cmp);
		},

		sportsOff: function(feedId) {
			var cmp = Ext.getCmp('sportsList');
			mobEdu.util.removeFeedFromHiddenFeeds(feedId, cmp);
		},

		showSports: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.list');
		},
		showRssSports: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.rssList');
		},

		showNews: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.news');
		},

		showPhotos: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.photos');
		},

		showMultimedia: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.media');
		},

		showResults: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.results');
		},

		showSchedules: function() {
			mobEdu.util.updatePrevView(mobEdu.sports.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.sports.view.schedules');
		},

		showSportsDetail: function() {
			if (campusCode == 'UT') {
				mobEdu.sports.f.title = null;
				mobEdu.sports.f.updateTitle(mobEdu.sports.f.curParentID);
				if ((mobEdu.sports.f.title).indexOf("News") !== -1) {
					mobEdu.util.updatePrevView(mobEdu.sports.f.showNews);
				} else if ((mobEdu.sports.f.title).indexOf("Results") !== -1) {
					mobEdu.util.updatePrevView(mobEdu.sports.f.showResults);
				} else if ((mobEdu.sports.f.title).indexOf("Schedules") !== -1) {
					mobEdu.util.updatePrevView(mobEdu.sports.f.showSchedules);
				} else if ((mobEdu.sports.f.title).indexOf("Photos") !== -1) {
					mobEdu.util.updatePrevView(mobEdu.sports.f.showPhotos);
				} else if ((mobEdu.sports.f.title).indexOf("Multimedia") !== -1) {
					mobEdu.util.updatePrevView(mobEdu.sports.f.showMultimedia);
				}
			} else {
				mobEdu.util.updatePrevView(mobEdu.sports.f.showRssSports);
			}

			mobEdu.util.show('mobEdu.sports.view.details');
		}
	}
});