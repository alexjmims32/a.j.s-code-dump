Ext.define('mobEdu.sports.store.sportsLocal', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.sports.model.sports'
    ],

    config:{
        storeId: 'mobEdu.sports.store.sportsLocal',
        autoLoad: true,
        model: 'mobEdu.sports.model.sports',
        proxy:{
            type:'localstorage',
            id:'sportsLocalStorage'
        }
    }
});