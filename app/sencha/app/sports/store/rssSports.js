Ext.define('mobEdu.sports.store.rssSports', {
    extend:'Ext.data.Store',
    
    requires: ['mobEdu.sports.model.rssSports'],
    config: {
        storeId: 'mobEdu.sports.store.rssSports',
        model: 'mobEdu.sports.model.rssSports',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            reader: {
                type: 'xml',
                rootProperty: 'rss',
                record: 'item'
            }
        }
    }    
});