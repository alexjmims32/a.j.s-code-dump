Ext.define('mobEdu.sports.store.sportsDetail', {
    extend:'Ext.data.Store',
    
    requires: [
    'mobEdu.sports.model.details'
    ],

    config:{
        storeId: 'mobEdu.sports.store.sportsDetail',
        autoLoad: true,
        model: 'mobEdu.sports.model.details',
        proxy:{
            type:'memory',
            id:'sportsDetail'
        }
    }
});