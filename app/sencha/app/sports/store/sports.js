Ext.define('mobEdu.sports.store.sports', {
extend: 'mobEdu.data.store',
    
    requires: [
    'mobEdu.sports.model.sports'
    ],
    
    config:{
        storeId: 'mobEdu.sports.store.sports',
        autoLoad: false,
        model: 'mobEdu.sports.model.sports'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'feederList'        
        return proxy;
    }
    
});