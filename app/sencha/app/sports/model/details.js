Ext.define('mobEdu.sports.model.details', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'id'
            },{
                name:'title'
            },{
                name:'link'
            },{
                name:'comments'
            },{
                name:'pubDate'
            },{
                name:'dc:creator'
            },{
                name:'category'
            },{
                name:'guid'
            },{
                name:'description'
            },{
                name:'content'
            },{
                name:'wfw:commentRss'
            },{
                name:'slash:comments'
            },{
                name: 'url'
            },{
                name: 'video'
            },{
                name: 'article'
            },{
                name: 'stats'
            }
        ]
    }
});