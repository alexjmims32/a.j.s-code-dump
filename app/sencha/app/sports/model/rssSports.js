Ext.define('mobEdu.sports.model.rssSports', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'id'
            },{
                name:'title'
            },{
                name:'link'
            },{
                name:'comments'
            },{
                name:'pubDate'
            },{
                name:'dc:creator'
            },{
                name:'category'
            },{
                name:'guid'
            },{
                name:'description'
            },{
                name:'content'
            },{
                name:'wfw:commentRss'
            },{
                name:'slash:comments'
            },{
                name: 'url',
                mapping : '',
                convert: function(value, record) {
                    var nodes = record.raw.childNodes;
                    var arrayItem = null;
                    if(nodes!=null){
                        var l = nodes.length;
                        for( var i = 0; i < l; i++){
                            var node = nodes[i];
                            if(node.nodeName=='enclosure'){
                                var attributes = node.attributes;
                                var url='';
                                var type='';
                                var flag=0;
                                if(attributes!=null){
                                    for (var a = 0; a < attributes.length; a++) {
                                        var attribute = attributes[a];
                                        if (attribute.name == 'type' && (attribute.value=='image/jpeg' || attribute.value=='image/gif')){
                                            type = attribute.value;                                    
                                            flag=1;
                                        }
                                        if (attribute.name == 'url' && flag==1){
                                            url = attribute.value;   
                                            flag=0;
                                            arrayItem=url;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return arrayItem;
                }
            },{
                name: 'video',
                mapping : '',
                convert: function(value, record) {
                    if(campusCode!='UT'){
                        var nodes = record.raw.childNodes;
                        var arrayItem = null;
                        if(nodes!=null){
                            var l = nodes.length;
                            for( var i = 0; i < l; i++){
                                var node = nodes[i];
                                if(node.nodeName=='enclosure'){
                                    var attributes = node.attributes;
                                    var url='';
                                    var type='';
                                    var flag=0;
                                    if(attributes!=null){
                                        for (var a = 0; a < attributes.length; a++) {
                                            var attribute = attributes[a];
                                            if (attribute.name == 'type' && (attribute.value=='text/html')){
                                                type = attribute.value;
                                                flag=1;
                                            }
                                            if (attribute.name == 'url' && flag==1){
                                                url = attribute.value;
                                                flag=0;
                                                arrayItem=url;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        return arrayItem;
                    }else{
                        var desc = record.data.description;
                        var href='';
                        desc = mobEdu.util.convertRelativeToAbsoluteUrls(desc,true);
                        var container = document.createElement("p");
                        container.innerHTML = desc;
                        var anchors = container.getElementsByTagName("a");
                        for (var i = 0; i < anchors.length; i++) {                        
                            var title = anchors[i].title;
                            if(title.toLowerCase().indexOf('play video clip')!= -1){
                                href = anchors[i].href;
                            }                       
                        }
                        return href;
                    }
                }
            },{
                name: 'article',
                convert: function(value, record) {
                    var desc = record.data.description;
                    var href='';
                    desc = mobEdu.util.convertRelativeToAbsoluteUrls(desc,true);
                    var container = document.createElement("p");
                    container.innerHTML = desc;
                    var anchors = container.getElementsByTagName("a");
                    for (var i = 0; i < anchors.length; i++) {                        
                        var title = anchors[i].title;
                        if(title.toLowerCase().indexOf('article')!= -1){
                            href = anchors[i].href;
                        }                       
                    }
                    return href;
                }
            },{
                name: 'audio',
                convert: function(value, record) {
                    var desc = record.data.description;
                    var href='';
                    desc = mobEdu.util.convertRelativeToAbsoluteUrls(desc,true);
                    var container = document.createElement("p");
                    container.innerHTML = desc;
                    var anchors = container.getElementsByTagName("a");
                    for (var i = 0; i < anchors.length; i++) {                        
                        var title = anchors[i].title;
                        if(title.toLowerCase().indexOf('play video clip')!= -1){
                            href = anchors[i].href;
                        }                       
                    }
                    return href;
                }
            },{
                name: 'stats',
                convert: function(value, record) {
                    var desc = record.data.description;
                    var href='';
                    desc = mobEdu.util.convertRelativeToAbsoluteUrls(desc,true);
                    var container = document.createElement("p");
                    container.innerHTML = desc;
                    var anchors = container.getElementsByTagName("a");
                    for (var i = 0; i < anchors.length; i++) {                        
                        var title = anchors[i].title;
                        if(title.toLowerCase().indexOf('stats')!= -1){
                            href = anchors[i].href;
                        }               
                    }
                    return href;
                }
            }
        ]
    }
});