Ext.define('mobEdu.courses.store.messages', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.messages'
    ],

    config: {
        storeId: 'mobEdu.courses.store.messages',

        autoLoad: false,

        model: 'mobEdu.courses.model.messages'
    },
    initProxy: function() {
        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
