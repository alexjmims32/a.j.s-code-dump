Ext.define('mobEdu.courses.store.courseDetail', {
    extend:'Ext.data.Store',
//    alias: 'courseDetailStore',

    requires: [
        'mobEdu.courses.model.courseList'
    ],

    config: {
        storeId: 'mobEdu.courses.store.courseDetail',

        autoLoad: false,

        model: 'mobEdu.courses.model.courseList',

      proxy:{
        type:'memory',
        id:'courseDetail'
      }
    }
});
