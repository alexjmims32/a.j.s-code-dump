Ext.define('mobEdu.courses.store.courseListLocal', {
    extend:'Ext.data.Store',

    requires: [
    'mobEdu.courses.model.courseList'
    ],

    config: {
        storeId: 'mobEdu.courses.store.courseListLocal',

        autoLoad: true,

        model: 'mobEdu.courses.model.courseList',
        proxy:{
            type:'localstorage',
            id:'courseListLocal'
        }
    }
    
});
