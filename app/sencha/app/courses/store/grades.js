Ext.define('mobEdu.courses.store.grades', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.grades'
    ],

    config: {
        storeId: 'mobEdu.courses.store.grades',

        autoLoad: false,

        model: 'mobEdu.courses.model.grades'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
