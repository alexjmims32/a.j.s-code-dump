Ext.define('mobEdu.courses.store.drop',{
extend: 'mobEdu.data.store',
//    alias:'dropStore',
    
    requires:[
        'mobEdu.courses.model.drop'
    ],
    config: {
        storeId: 'mobEdu.courses.store.drop',

        autoLoad: false,

        model: 'mobEdu.courses.model.drop'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'dropList.drop'        
        return proxy;
    }
})