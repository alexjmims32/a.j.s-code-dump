Ext.define('mobEdu.courses.store.assignments', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.assignments'
    ],

    config: {
        storeId: 'mobEdu.courses.store.assignments',

        autoLoad: false,

        model: 'mobEdu.courses.model.assignments'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
