Ext.define('mobEdu.courses.store.holidays', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.holidays'
    ],

    config: {
        storeId: 'mobEdu.courses.store.holidays',

        autoLoad: false,

        model: 'mobEdu.courses.model.holidays'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'holidayList.holidays'
        return proxy;
    }
});
