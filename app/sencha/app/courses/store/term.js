Ext.define('mobEdu.courses.store.term',{
extend: 'mobEdu.data.store',
     requires:[
       'mobEdu.courses.model.term'
   ],
   config:{
       storeId:'mobEdu.courses.store.term',
        autoLoad: false,

        model: 'mobEdu.courses.model.term'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
})