Ext.define('mobEdu.courses.store.holidaysLocal', {
    extend:'Ext.data.Store',
//    alias: 'courseDetailStore',

    requires: [
        'mobEdu.courses.model.holidays'
    ],

    config: {
        storeId: 'mobEdu.courses.store.holidaysLocal',

        autoLoad: false,

        model: 'mobEdu.courses.model.holidays',

        proxy:{
            type:'localstorage',
            id:'holidaysLocal'
        }
    }
});
