Ext.define('mobEdu.courses.store.classmates', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.classmates'
    ],

    config: {
        storeId: 'mobEdu.courses.store.classmates',

        autoLoad: false,

        model: 'mobEdu.courses.model.classmates'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'facultyList' ;       
        return proxy;
    }
});
