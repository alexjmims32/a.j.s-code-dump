Ext.define('mobEdu.courses.store.meetingScheduleLocal', {
    extend:'Ext.data.Store',
//    alias: 'courseDetailStore',

    requires: [
        'mobEdu.courses.model.meetingScheduleLocal'
    ],

    config: {
        storeId: 'mobEdu.courses.store.meetingScheduleLocal',

        autoLoad: false,

        model: 'mobEdu.courses.model.meetingScheduleLocal',

        proxy:{
            type:'localstorage',
            id:'meetingSchedule'
        }
    }
});
