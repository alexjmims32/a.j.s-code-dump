Ext.define('mobEdu.courses.store.advisors', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.advisors'
    ],

    config: {
        storeId: 'mobEdu.courses.store.advisors',

        autoLoad: false,

        model: 'mobEdu.courses.model.advisors'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'facultyList' ;       
        return proxy;
    }
});
