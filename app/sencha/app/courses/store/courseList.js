Ext.define('mobEdu.courses.store.courseList', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.courseList'
    ],

    config: {
        storeId: 'mobEdu.courses.store.courseList',

        autoLoad: false,

        model: 'mobEdu.courses.model.courseList'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'registeredCourseList.registeredCourse'        
        return proxy;
    }
});
