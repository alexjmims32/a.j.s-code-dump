Ext.define('mobEdu.courses.store.announcements', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.announcements'
    ],

    config: {
        storeId: 'mobEdu.courses.store.announcements',

        autoLoad: false,

        model: 'mobEdu.courses.model.announcements'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
