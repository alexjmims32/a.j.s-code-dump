Ext.define('mobEdu.courses.store.faculty', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.faculty'
    ],

    config: {
        storeId: 'mobEdu.courses.store.faculty',

        autoLoad: false,

        model: 'mobEdu.courses.model.faculty'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'facultyList' ;       
        return proxy;
    }
});
