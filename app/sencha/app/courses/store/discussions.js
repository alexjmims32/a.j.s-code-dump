Ext.define('mobEdu.courses.store.discussions', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.courses.model.discussions'
    ],

    config: {
        storeId: 'mobEdu.courses.store.discussions',

        autoLoad: false,

        model: 'mobEdu.courses.model.discussions'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
});
