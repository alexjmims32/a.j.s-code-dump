Ext.define('mobEdu.courses.view.mapDirectionInst', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        id: 'crsDirInst',
        padding:'10px',
        html: '',
        items: [{
            title: '<h1>Direction Instructions</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});        