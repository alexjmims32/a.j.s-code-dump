Ext.define('mobEdu.courses.view.phone.courseList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.courses.store.courseListLocal'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			id: 'courseList',
			name: 'courseList',
			cls: 'logo',
			width: '100%',
			emptyText: '',
			itemTpl: new Ext.XTemplate('<tpl if="(mobEdu.courses.f.withdrawnCourses(statusCode) === false)"> <div> <h2 class="withdraw">{[mobEdu.courses.f.separatorConverter(values.courseTitle)]} </h2> </div> <tpl else> <div> <h2>{[mobEdu.courses.f.separatorConverter(values.courseTitle)]} </h2> </div> </tpl> <div style="width: 90%; display: inline-block;"><div> <h3>{subject} {courseNumber}&nbsp;&nbsp; <tpl if="(mobEdu.reg.f.isExists(credits)===true)"> ({credits} hrs) </tpl> </h3> </div> <tpl if="(mobEdu.util.getCurrentRole() != \'FACULTY\')"> <div> <h3>{faculty} </h3> </div> </tpl></div> <tpl if="(mobEdu.courses.f.isAccessAllowed(dropFlag)=== true && mobEdu.courses.f.isActionAllowed(actionCode)=== true && mobEdu.courses.f.isRegistrationAllowed()=== true)"><div name="mulDrop" style="display: inline-block; float: right; background: transparent; width: 20%; height: 100%; position: absolute; top:13px;right:8px" class="x-field-checkbox x-field"> <img style="margin-top: 20px" src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 id="off" name="mulDrop" title={crn} /> </div><tpl if="(mobEdu.courses.f.isAccessAllowed(dropFlag)=== true && mobEdu.courses.f.isActionAllowed(actionCode)=== true && mobEdu.courses.f.isRegistrationAllowed()=== true)"> <div align="right" style="padding-top:35%"> <h5>{actionDesc} </h5> </div> </tpl> </tpl>'),
			store: mobEdu.util.getStore('mobEdu.courses.store.courseListLocal'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(me, index, target, record, e, eOpts) {
					mobEdu.courses.f.onCourseItemTap(index, me, record, e);
				}
			}
		}, {
			xtype: 'customToolbar',
			title: '<h1>My Courses</h1>',
			id: 'coursesP'
		}, {
			docked: 'top',
			xtype: 'selectfield',
			name: 'courseRole',
			id: 'courseRole',
			listeners: {
				change: function(selectbox, newValue, oldValue) {
					mobEdu.courses.f.onChangeRole(selectbox, newValue, oldValue);
				}
			}
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Attendance',
				name: 'attendance',
				id: 'attendance',
				handler: function() {
					mobEdu.courses.f.getCurrentClass();
				}
			}, {
				text: registrationAllowed === "false" ? 'Calendar' : 'Cal',
				name: 'calendar',
				pressedCls: 'my-pressed',
				id: 'calendar',
				handler: function() {
					mobEdu.courses.f.checkHolidaysList();
				}
			}, {
				text: 'Terms',
				pressedCls: 'my-pressed',
				align: 'right',
				handler: function() {
					mobEdu.courses.f.loadRegTermList();
				}
			}, {
				text: 'Drop',
				name: 'dropCourses',
				id: 'dropCourses',
				handler: function() {
					mobEdu.courses.f.dropCourse();
				}
			}]
		}],
		flex: 1
	}
});