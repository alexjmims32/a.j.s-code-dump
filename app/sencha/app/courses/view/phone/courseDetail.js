Ext.define('mobEdu.courses.view.phone.courseDetail', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.courses.store.courseDetail',
		'mobEdu.courses.store.announcements',
		'mobEdu.courses.store.assignments',
		'mobEdu.courses.store.grades',
		'mobEdu.courses.store.discussions',
		'mobEdu.courses.store.classmates',
		'mobEdu.courses.store.faculty',
		'mobEdu.courses.store.advisors',
		'mobEdu.courses.store.messages'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'dataview',
			id: 'courseDetailsList',
			name: 'courseDetailsList',
			disableSelection: true,
			itemTpl: new Ext.XTemplate('<table width="100%">' +
				'<div align="center" style="padding-bottom: 5px; padding-top: 5px;"><h3><b>{courseTitle} ({crn})</b> <tpl if="(mobEdu.courses.f.isMoodleExist()===true)"> <tr><td><td></td></td><td align="left"><a href="javascript:mobEdu.courses.f.moodlePageLoad({crn},{termCode})">Moodle Course site</a></td></tr></tpl><tpl if="(mobEdu.courses.f.isSyllabusLinkExist()===true)"> <tr><td></td><td></td><td><a href="javascript:mobEdu.courses.f.showCourseSylabus({crn},{termCode})">Syllabus</a></td></tr></tpl></h3></div>',
				'<tr><td  width="40%"><h2>Course Id</h2></td><td><h2>:</h2></td><td align="left"><h3>{subject} {courseNumber} ' + '<tpl if="(mobEdu.reg.f.isExistsFaculty(seqNo)===true)">', '/ {seqNo}', '</tpl>', '<tpl if="(mobEdu.reg.f.isExistsFaculty(linkId)===true)">', ' - {linkId}', '</tpl></h3></td></tr>' +
				'<tpl if="(mobEdu.reg.f.isRegistrationStatusExist(statusMessage)===true)"><tr><td  width="40%"><h2>Registration Status</h2></td><td><h2>:</h2></td><td align="left"><h3>{statusMessage}</h3></td></tr></tpl>' +
				'<tr><td  width="40%"><h2>Subject</h2></td><td><h2>:</h2></td><td align="left"><h3>{description}</h3></td></tr>' +
				'<tr><td  width="40%"><h2>Instructor</h2></td><td><h2>:</h2></td><td align="left"><h3>',
				'<tpl if="(mobEdu.reg.f.isEmailExist(facultyEmail) === false)">',
				'{faculty}',
				'<tpl else>',
				'<a href="mailto:{facultyEmail}" >{faculty}</a>',
				'</tpl>',
				'</h3></td></tr>' +
				//                    '<tr><td align="right" width="50%"><h2>Lecture Hours :</h2></td><td align="left"><h3>{lectureHours}</h3></td></tr>' +
				'<tpl if="(mobEdu.reg.f.isExistsFaculty(credits)===true)"><tr><td  width="40%"><h2>Credit Hours</h2></td><td><h2>:</h2></td><td align="left">' +
				'<tpl if="(mobEdu.courses.f.isCreditLink(varCredit)===true)">',
				'<h3>{credits} (<a href="javascript:mobEdu.courses.f.checkUpdateCreditHrsEligibilty()">Click here</a> to update)</h3></td></tr>',
				'<tpl else>',
				'<h3>{credits}</h3></td></tr>',
				'</tpl></tpl>',
				// '<tr><td  width="40%"><h2>Term</h2></td><td><h2>:</h2></td><td align="left"><h3>{termDescription}</h3></td></tr>' +
				'<tr><td  width="40%"><h2>Department</h2></td><td><h2>:</h2></td><td align="left"><h3>{department}</h3></td></tr>' +
				'<tr><td  width="40%"><h2>College</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>{college}</h3></td></tr>' +
				'<tpl if="(mobEdu.courses.f.islevelExist(level)===true)">', '<tr><td width="20%"><h2>Level</h2></td><td><h2>:</h2></td><td align="left">', '{level}', '</td></tr></tpl>' +
				// '<tr><td  valign="top" width="40%"><h2>Level</h2></td><td valign="top"><h2>:</h2></td><td align="left"><p><h3>{[this.getLevel(values.level)]}</h3></p></td></tr>' +
				'<tr><td  width="40%"><h2>Campus</h2></td><td><h2>:</h2></td><td align="left"><h3>{campus}</h3></td></tr>' +				

				'<tr><td valign="top"  width="40%"><h2>Schedule</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>' +
				'<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===true)">',
				'<table>' +
				'<tpl for="meeting">',
				'<tr><td>' +
				'{startDate} : {endDate}<br />' +
				'{beginTime} - {endTime}<br />' +
				'{period}<br />' +
				'<tpl if="(mobEdu.courses.f.isBuildingStringExist(bldgDesc)===true)">{bldgDesc}<tpl else>Building: {bldgDesc}</tpl>' +
				'<tpl if="(mobEdu.courses.f.isValidCoordinates(latitude, longitude)===true)">',
				'<h8>(<a href="javascript:mobEdu.courses.f.showCoursesMapDirections({latitude},{longitude})">Directions</a>)</h8>' +
				'</tpl>',
				'<br />' +
				'<tpl if="(mobEdu.courses.f.isRoomCodeExist(roomCode)==true)">Room:{roomCode}<tpl else><tpl if="(mobEdu.courses.f.isRoomDescExist(roomDesc)==true)">{roomDesc}<tpl else>Room:{roomDesc} </tpl></tpl>' +
				'<tpl if="(mobEdu.reg.f.isScheduleDesc(scheduleDesc)===true)">',
				'Type: {scheduleDesc}<br />',
				'</tpl>',
				'<hr>' +
				'</td></tr>' +
				'</tpl>',
				'</table>',
				'</tpl>',
				'<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===false)">',
				'No meeting info available',
				'</tpl>',
				'</h4></td></tr>' +
				'</table>', {
					compiled: true,
					getLevel: function(level) {
						return String(level).replace(/[,]/g, "<br />");
					}
				}
			),
			store: mobEdu.util.getStore('mobEdu.courses.store.courseDetail'),
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					mobEdu.courses.f.onCourseDetailItemTap(index, view, record, item);
				}
			}
		}, {
			xtype: 'tabpanel',
			id: 'courseDetailsTabs',
			name: 'courseDetailsTabs',
			scrollable: false,
			tabBar: {
				layout: {
					pack: 'center'
				}
			},
			items: [{
					layout: 'fit',
					//            title:'<img src="' + mobEdu.util.getResourcePath() + 'images/menu/coursedetails.png" />',
					title: '<h4>Details</h4>',
					scrollable: false,
					items: [{
						layout: 'fit',
						scrollable: false,
						items: [{
							xtype: 'dataview',
							name: 'crseDetail',
							id: 'crseDetail',
							emptyText: '<center><h3>No course selected</h3><center>',
							itemTpl: new Ext.XTemplate('<table width="100%">' +
								'<tr><td  width="40%"><h2>Course Title</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>{courseTitle} ({crn}) <tpl if="(mobEdu.courses.f.isMoodleExist()===true)"> <img src="' + mobEdu.util.getResourcePath() + 'images/moodle.png" width=24 id="off" name="coursemoodle" title=Moodle style="vertical-align:text-top"/> </tpl><tpl if="(mobEdu.courses.f.isSyllabusLinkExist()===true)"> <img src="' + mobEdu.util.getResourcePath() + 'images/syllubas.png" width=24 id="off" name="syllabus" title=Syllabus style="vertical-align:text-top" /></tpl></h3></td></tr>' +
								'<tr><td  width="40%"><h2>Course Id</h2></td><td><h2>:</h2></td><td align="left"><h3>{subject} {courseNumber}</h3></td></tr>' +
								'<tr><td  width="40%"><h2>Subject</h2></td><td><h2>:</h2></td><td align="left"><h3>{description}</h3></td></tr>' +
								'<tr><td  width="40%"><h2>Instructor</h2></td><td><h2>:</h2></td><td align="left"><h3>',
								'<tpl if="(mobEdu.reg.f.isEmailExist(facultyEmail) === false)">',
								'{faculty}',
								'<tpl else>',
								'<a href="mailto:{facultyEmail}" >{faculty}</a>',
								'</tpl>',
								'</h3></td></tr>' +
								//                    '<tr><td align="right" width="50%"><h2>Lecture Hours :</h2></td><td align="left"><h3>{lectureHours}</h3></td></tr>' +
								'<tr><td  width="40%"><h2>Credit Hours</h2></td><td><h2>:</h2></td><td align="left">' +
								'<tpl if="(mobEdu.courses.f.isCreditLink(varCredit)===true)">',
								'<h3>{credits} (<a href="javascript:mobEdu.courses.f.checkUpdateCreditHrsEligibilty()">Click here</a> to update)</h3></td></tr>',
								'<tpl else>',
								'<h3>{credits}</h3></td></tr>',
								'</tpl>',
								'<tr><td  width="40%"><h2>Term</h2></td><td><h2>:</h2></td><td align="left"><h3>{termDescription}</h3></td></tr>' +
								'<tr><td  width="40%"><h2>Department</h2></td><td><h2>:</h2></td><td align="left"><h3>{department}</h3></td></tr>' +
								'<tr><td  width="40%"><h2>College</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>{college}</h3></td></tr>' +
								'<tr><td  valign="top" width="40%"><h2>Level</h2></td><td valign="top"><h2>:</h2></td><td align="left"><p><h3>{[this.getLevel(values.level)]}</h3></p></td></tr>' +
								'<tr><td  width="40%"><h2>Campus</h2></td><td><h2>:</h2></td><td align="left"><h3>{campus}</h3></td></tr>' +
								'<tr><td valign="top"  width="40%"><h2>Schedule</h2></td><td valign="top"><h2>:</h2></td><td align="left"><h3>' +
								'<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===true)">',
								'<table>' +
								'<tpl for="meeting">',
								'<tr><td>' +
								'{[this.changeDateFormat(values.startDate)]}' + ' - ' + '{[this.changeDateFormat(values.endDate)]}<br />' +
								'{beginTime} - {endTime}<br />' +
								'{period}<br />' +
								'Building: {bldgCode}<br />' +
								'Room: {roomCode}<br />',
								'<tpl if="(mobEdu.reg.f.isScheduleDesc(scheduleDesc)===true)">',
								'Type: {scheduleDesc}<br />',
								'</tpl>',
								'<hr>' +
								'</td></tr>' +
								'</tpl>',
								'</table>',
								'</tpl>',
								'<tpl if="(mobEdu.reg.f.hasMeetings(meeting)===false)">',
								'No meeting info available',
								'</tpl>',
								'</h4></td></tr>' +
								'</table>', {
									compiled: true,
									getLevel: function(level) {
										return String(level).replace(/[,]/g, "<br />");
									},
									changeDateFormat: function(date) {
										var dateValue;
										var dateValueFormat = Ext.Date.parse(date, "m-d-Y");
										dateValue = Ext.Date.format(dateValueFormat, meetingsDateFormat);
										return dateValue;
									}
								}
							),
							store: mobEdu.util.getStore('mobEdu.courses.store.courseDetail'),
							singleSelect: true,
							loadingText: '',
							listeners: {
								itemtap: function(view, index, target, record, item, e, eOpts) {
									mobEdu.courses.f.onCourseDetailItemTap(index, view, record, item);
								}
							}
						}]
					}]
				},
				// {
				// 	layout: 'fit',
				// 	//            title:'<img src="' + mobEdu.util.getResourcePath() + 'images/menu/announcements.png" />',
				// 	title: '<h4>Announcements</h4>',
				// 	scrollable: false,
				// 	items: [{
				// 		layout: 'fit',
				// 		scrollable: false,
				// 		items: [{
				// 			xtype: 'list',
				// 			name: 'announcements',
				// 			id: 'announcements',
				// 			disableSelection: true,
				// 			deferEmptyText: false,
				// 			style: {
				// 				'word-wrap': 'break-word'
				// 			},
				// 			emptyText: '<center><h3>No Announcements</h3><center>',
				// 			itemTpl: new Ext.XTemplate(
				// 				'<h2>{title}</h2><br/>' +
				// 				'<h3>{[mobEdu.util.convertRelativeToAbsoluteUrls(values.body)]}</h3>'
				// 			),
				// 			store: mobEdu.util.getStore('mobEdu.courses.store.announcements'),
				// 			loadingText: ''
				// 		}]
				// 	}],
				// 	listeners: {
				// 		activate: function(newActiveItem, container, oldActiveItem, eOpts) {
				// 			mobEdu.courses.f.loadAnnouncements();
				// 		}
				// 	}
				// },
				{
					layout: 'fit',
					scrollable: false,
					title: '<h4>Assignments</h4>',
					items: [{
						layout: 'fit',
						scrollable: false,
						items: [{
							xtype: 'list',
							name: 'assignments',
							id: 'assignments',
							disableSelection: true,
							deferEmptyText: false,
							style: {
								'word-wrap': 'break-word'
							},
							itemTpl: new Ext.XTemplate(
								'<h2>{title}</h2><br/>' +
								'<h3>{instructions}</h3>'
							),
							store: mobEdu.util.getStore('mobEdu.courses.store.assignments'),
							loadingText: ''
						}]
					}],
					listeners: {
						activate: function(newActiveItem, container, oldActiveItem, eOpts) {
							mobEdu.courses.f.loadAssignments();
						}
					}
				}, {
					layout: 'fit',
					title: '<h4>Grades</h4>',
					scrollable: false,
					items: [{
						layout: 'fit',
						scrollable: false,
						items: [{
							xtype: 'list',
							name: 'grades',
							id: 'grades',
							disableSelection: true,
							itemTpl: new Ext.XTemplate('<table width="100%">' +
								'<tpl if="(mobEdu.reg.f.isExists(name)===true)">',
								'<tr><td><h3><b>' +
								'{name}' + '</b></h3></td></tr>' +
								'</tpl>' +
								'<tpl if="(mobEdu.reg.f.isExists(midGrade)===true)">',
								'<tr><td><h3><b>Mid Grade - </b>' +
								'{midGrade}' + '</h3></td></tr>' +
								'</tpl>' +
								'<tpl if="(mobEdu.reg.f.isExists(finalGrade)===true)">',
								'<tr><td><h3><b>Final Grade - </b>' +
								'{finalGrade}' + '</h3></td></tr>' +
								'</tpl>' +
								'</table>'),
							store: mobEdu.util.getStore('mobEdu.courses.store.grades'),
							loadingText: ''
						}]
					}],
					listeners: {
						activate: function(newActiveItem, container, oldActiveItem, eOpts) {
							mobEdu.courses.f.loadGrades();
						}
					}
				}, {
					layout: 'fit',
					title: '<h4>Discussions</h4>',
					scrollable: false,
					items: [{
						xtype: 'container',
						id: 'forumsContainer',
						layout: 'fit',
						scrollable: false,
						items: [{
							xtype: 'list',
							name: 'discussions',
							id: 'discussions',
							disableSelection: true,
							deferEmptyText: false,
							itemTpl: new Ext.XTemplate(
								'<table width="100%">' +
								'<tr><td><h2>{forumTitle} - {threadSubject}</h2></td></tr>' +
								'</table>'
							),
							store: mobEdu.util.getStore('mobEdu.courses.store.discussions'),
							loadingText: '',
							listeners: {
								itemtap: function(view, index, target, record, item, e, eOpts) {
									mobEdu.courses.f.onDiscussionsItemTap(view, index, target, record, item, e, eOpts);
								}
							}
						}]
					}, {
						xtype: 'container',
						id: 'msgContainer',
						hidden: true,
						layout: 'fit',
						scrollable: false,
						items: [{
							docked: 'top',
							layout: {
								pack: 'right'
							},
							padding: '5 0 5 10',
							html: '<a href="javascript:mobEdu.courses.f.onForumsLinkTap();">Back to Threads</a>'
						}, {
							xtype: 'list',
							name: 'messages',
							id: 'messages',
							disableSelection: true,
							deferEmptyText: false,
							emptyText: '<center><h3>No Messages</h3><center>',
							itemTpl: new Ext.XTemplate(
								'<table width="100%">' +
								'<tr><td><h2>{subject} : </h2><h3>{body}</h3></td></tr>' +
								'<tr><td align="right"><i><h4>{postedName}</h4></i></td></tr>' +
								'</table>'
							),
							store: mobEdu.util.getStore('mobEdu.courses.store.messages'),
							loadingText: ''
						}]
					}],
					listeners: {
						activate: function(newActiveItem, container, oldActiveItem, eOpts) {
							mobEdu.courses.f.loadDiscussions();
						}
					}
				}
			]
		}, {
			xtype: 'customToolbar',
			title: '<h1>Course Details</h1>'
		}, {
			xtype: 'toolbar',
			id: 'dropBar',
			name: 'dropBar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'Feedback',
				id: 'crseFeedback',
				name: 'crseFeedback',
				handler: function() {
					mobEdu.courses.f.loadFeedback();
				}
			}, {
				text: withdrawEnable === "true" ? "Drop/Withdraw" : "Drop",
				id: 'dropCourse',
				name: 'dropCourse',
				handler: function() {
					mobEdu.courses.f.dropDetailCourse();
				}
			}]
		}],
		flex: 1
	}

});