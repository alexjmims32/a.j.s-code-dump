Ext.define('mobEdu.courses.view.tablet.termList', {
    extend:'Ext.Panel',
	requires: [
		'mobEdu.main.store.termList'
	],
    config:{
        id:'coursesTermListPopup',
        scroll:'vertical',
        floating:true,
        modal:true,
        centered:true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width:'60%',
        height:'60%',
        layout:'fit',
        items:[
        {
            xtype:'list',
            id:'coursestermListP',
            name:'coursestermListP',
            cls:'logo',
            itemTpl:'<h3>{description}</h3>',
            store: mobEdu.util.getStore('mobEdu.main.store.termList'),
            singleSelect:true,
            loadingText:'',
            listeners:{
                itemtap:function (view, index, target, record, e, eOpts) {
                    mobEdu.courses.f.onTermItemTap(view, index, target, record, e, eOpts);
                }
            }
        }
        ],
        flex:1
    }

});
