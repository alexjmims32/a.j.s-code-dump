Ext.define('mobEdu.courses.view.dropStatus', {
    extend: 'Ext.Panel',
    id: 'dropStatusView',
    scroll: 'vertical',
    fullscreen: true,
    config: {
        layout: 'fit',
        items: [{
            xtype: 'list',
            id: 'dropStatusList',
            name: 'dropStatusList',
            disableSelection: true,
            cls: 'logo',
            itemTpl: new Ext.XTemplate('<div>',
                '<tpl if="(mobEdu.courses.f.isSuccess(status)===false)">',
                '<h2 class="fail"></tpl>',
                '<tpl if="(mobEdu.courses.f.isSuccess(status)===true)">',
                '<h2 class="success"></tpl>',
                '{courseTitle} ({crn})</h2>',
                '<ul style="width: 50%;">',
                '<li style="padding-top:5px">{subject} {courseNumber}</li>',
                '<li>{department}</li>',
                '</ul>',
                '<ul style="width: 50%;">',
                '<li>{faculty}</li>',
                '<li>{college}</li>',
                '</ul>',
                '<h4 class="successMsg"><tpl if="(mobEdu.courses.f.isSuccess(status)===true)">Course dropped successfully</tpl></h4>',
                '<h4 class="failMsg"><tpl if="(mobEdu.courses.f.isSuccess(status)===false)">{errorMessage}</tpl></h4>',
                '</div>'),
            store: 'mobEdu.courses.store.drop',
            singleSelect: true,
            loadingText: ''
        }, {
            title: '<h1>Drop Status</h1>',
            xtype: 'customToolbar'
        }],
        flex: 1
    }
});