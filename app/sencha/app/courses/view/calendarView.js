Ext.define('mobEdu.courses.view.calendarView', {
    extend: 'Ext.Panel',
    alias: 'calendarView',
    config: {
        fullscreen: 'true',
        cls: 'backGround',
        scrollable: true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            id: 'coursesDock',
            name: 'coursesDock',
            items: [{}]
        }, {
            xtype: 'label',
            id: 'lablST',
            padding: 10
        }]
    }
});