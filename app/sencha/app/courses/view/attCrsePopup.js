Ext.define('mobEdu.courses.view.attCrsePopup', {
	extend: 'Ext.form.Panel',
	config: {
		id: 'attCrsePopup',
		scrollable: true,
		floating: true,
		cls: 'crsPop',
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		layout: {
			type: 'fit',
			pack: 'middle'
		},
		docked: 'top',
		items: [{
			xtype: 'toolbar',
			title: '',
			id: 'attCrseTop',
			docked: 'top'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			cls: 'crsPop',
			id: 'attCrseBottom',
			layout: {
				type: 'hbox',
				pack: 'middle'
			},
			items: [{
				text: '<h7>Yes</h7>',
				id: 'generate',
				margin: 10,
				ui: 'action',
				name: 'replyAllMsg',
				handler: function() {
					mobEdu.courses.f.handleGenerateCode();
				}
			}, {
				text: '<h7>No</h7>',
				ui: 'action',
				margin: 10,
				id: 'Notgenerate',
				handler: function() {
					mobEdu.courses.f.hideAttCoursePopup();
				}
			}]
		}],
		flex: 1,
		listeners: {
			hide: function() {
				mobEdu.courses.f.hideAttCoursePopup();
			}
		}
	}
});