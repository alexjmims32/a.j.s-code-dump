Ext.define("mobEdu.courses.model.faculty", {
    extend: 'Ext.data.Model',
    config: {
        fields: [{
            name: 'name',
            type: 'string'
        }, {
            name: 'email',
            type: 'string'
        },{
            name: 'crn',
            type: 'string'
        },{
            name: 'courseTitle',
            type: 'string'
        }, {
            name: 'officeLocation',
            type: 'string'
        }, {
            name: 'id',
            type: 'string'
        }]
    }
});