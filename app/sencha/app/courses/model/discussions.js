Ext.define("mobEdu.courses.model.discussions", {
    extend:'Ext.data.Model',

  config:{
	fields : [ {
        name : 'forumId'
    },{
        name : 'threadId'
    },{
        name: 'forumTitle'
    },{
        name: 'forumDescription'
    },{
        name: 'isForumAvailable'
    },{
        name: 'forumStartDate'
    },{
        name: 'forumEndDate'
    },{
        name: 'threadSubject'
    },{
        name: 'threadBody'
    },{
        name: 'threadParentId'
    },{
        name: 'threadPostDate'
    },{
        name: 'threadEditDate'
    },{
        name: 'threadResponses'
    },{
        name: 'threadPostedName'
    },{
        name: 'status'
    }]
  }
});