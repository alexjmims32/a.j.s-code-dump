Ext.define("mobEdu.courses.model.assignments", {
    extend:'Ext.data.Model',

  config:{
	fields : [ {
        name : 'title'
    },{
        name: 'instructions'
    },{
        name: 'status'
    }]
  }
});