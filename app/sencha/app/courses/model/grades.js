Ext.define("mobEdu.courses.model.grades", {
    extend:'Ext.data.Model',

  config:{
	fields : [ {
        name : 'name',
        mapping:'title'
    },{
        name: 'finalGrade',
        mapping:'finalGrade'
    },{
        name: 'midGrade',
        mapping:'midGrade'
    },{
        name: 'status'
    }]
  }
});