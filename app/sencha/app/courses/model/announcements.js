Ext.define("mobEdu.courses.model.announcements", {
    extend:'Ext.data.Model',

  config:{
	fields : [ {
        name : 'title'
    },{
        name: 'body'
    },{
        name: 'status'
    }]
  }
});