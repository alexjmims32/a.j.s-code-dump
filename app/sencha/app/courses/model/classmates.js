Ext.define("mobEdu.courses.model.classmates", {
    extend: 'Ext.data.Model',
    config: {
        fields: [{
                name: 'name',
                type: 'string'
            },
            {
                name: 'email',
                type: 'string'
            },
            {
                name: 'id',
                type: 'string'
            }
        ]
    }
});