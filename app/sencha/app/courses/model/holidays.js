Ext.define('mobEdu.courses.model.holidays',{
    extend:'Ext.data.Model',

    config:{
        fields : [ {
            name : 'date',
            type : 'string'
        },{
            name: 'year',
            type: 'string'
        },{
            name: 'holidayDesc',
            type: 'string'
        }
        ]
    }
});