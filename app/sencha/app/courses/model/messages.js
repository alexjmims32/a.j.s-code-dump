Ext.define("mobEdu.courses.model.messages", {
    extend:'Ext.data.Model',

  config:{
	fields : [ {
        name : 'subject'
    },{
        name: 'body'
    },{
        name: 'id'
    },{
        name: 'forumId'
    },{
        name: 'parentId'
    },{
        name: 'postDate'
    },{
        name: 'editDate'
    },{
        name: 'responses'
    },{
        name: 'postedName'
    },{
        name: 'status'
    }]
  }
});