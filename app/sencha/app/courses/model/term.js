Ext.define('mobEdu.courses.model.term',{
    extend:'Ext.data.Model',
    
    config:{
        fields:[
            {
                name : 'termCode',
                type : 'string'
            }
        ]
    }
})