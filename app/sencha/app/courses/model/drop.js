Ext.define("mobEdu.courses.model.drop", {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'status',
            type: 'string'
        }, {
            name: 'crn',
            type: 'string'
        }, {
            name: 'courseTitle',
            type: 'string'
        }, {
            name: 'college',
            type: 'string'
        }, {
            name: 'department',
            type: 'string'
        }, {
            name: 'level',
            type: 'string'
        }, {
            name: 'errorMessage',
            type: 'string'
        }, {
            name: 'subject',
            type: 'string'
        }, {
            name: 'courseNumber',
            type: 'string'
        }, {
            name: 'faculty',
            type: 'string'
        }, {
            name: 'message',
            type: 'string'
        }]
    }
});