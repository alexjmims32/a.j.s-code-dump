Ext.define('mobEdu.courses.f', {
	requires: [
		'mobEdu.courses.store.announcements',
		'mobEdu.courses.store.assignments',
		'mobEdu.courses.store.courseDetail',
		'mobEdu.courses.store.courseList',
		'mobEdu.courses.store.discussions',
		'mobEdu.courses.store.classmates',
		'mobEdu.courses.store.faculty',
		'mobEdu.courses.store.advisors',
		'mobEdu.courses.store.drop',
		'mobEdu.courses.store.grades',
		'mobEdu.courses.store.holidays',
		'mobEdu.courses.store.holidaysLocal',
		'mobEdu.courses.store.meetingScheduleLocal',
		'mobEdu.courses.store.messages',
		'mobEdu.courses.store.term',
		'mobEdu.reg.store.eligibility',
		'mobEdu.courses.store.courseListLocal',
		'mobEdu.att.store.mettingScheduleLocal'
	],
	statics: {
		actionType: null,
		action: null,
		regCoursesCallCount: 0,
		preLoadMyCourses: function() {
			if (immunizationEnabled == 'true') {
				mobEdu.main.f.immunizationNextView = mobEdu.courses.f.loadMyCourses;
				mobEdu.reg.f.loadImmunization();
			} else {
				mobEdu.courses.f.loadMyCourses();
			}
		},
		loadMyCourses: function() {
			if (Ext.os.is.Phone) {
				mobEdu.util.get('mobEdu.courses.view.phone.courseList');
			} else {
				mobEdu.util.get('mobEdu.courses.view.tablet.courseList');
			}
			if (mobEdu.util.isDualRole()) {
				var options = new Array();
				options[1] = ({
					text: 'Registered Courses',
					value: 'STUDENT'
				});
				options[0] = ({
					text: 'Teaching Courses',
					value: 'FACULTY'
				});

				// Just set the options without firing an change requests
				Ext.getCmp('courseRole').show(true);
				Ext.getCmp('courseRole').suspendEvents();
				Ext.getCmp('courseRole').setOptions(options);
				// In case of dual role, show teaching courses first.
				Ext.getCmp('courseRole').setValue('FACULTY');
				Ext.getCmp('courseRole').resumeEvents(true);

				mobEdu.courses.f.onChangeRole();

				//                mobEdu.courses.f.showCourseList();
			} else {
				Ext.getCmp('courseRole').setHidden(true);
				mobEdu.courses.f.loadRegisteredCourses();
			}
		},

		onChangeRole: function(selectbox, newValue, oldValue) {
			mobEdu.courses.f.updateRole(newValue);
			mobEdu.courses.f.loadRegisteredCourses();
		},

		loadRegisteredCourses: function(termCode, flag) {
			if (Ext.os.is.Phone) {
				mobEdu.util.get('mobEdu.courses.view.phone.courseList');
				mobEdu.util.setTitle('COURSES', Ext.getCmp('coursesP'));
			} else {
				mobEdu.util.get('mobEdu.courses.view.tablet.courseList');
				mobEdu.util.setTitle('COURSES', Ext.getCmp('coursesT'));
			}
			var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
			coursesStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			if (mobEdu.courses.f.isFacultyRole()) {
				coursesStore.getProxy().setUrl(webserver + 'facultyCourses');
			} else {
				coursesStore.getProxy().setUrl(webserver + 'getRegisteredCourses');
			}
			coursesStore.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				companyId: companyId
			});
			coursesStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.registeredCourseResponseHandler(termCode, flag);
			};
			coursesStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'registeredcourses');
		},

		registeredCourseResponseHandler: function(termCode, flag) {
			if (flag == 'feedback') {
				mobEdu.custserv.f.updateCoursesDropdown();
			} else {
				var coursesStore = mobEdu.util.getStore('mobEdu.courses.store.courseList');
				coursesStore.sort('courseTitle', 'ASC');
				var coursesLocal = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
				coursesLocal.removeAll();
				coursesLocal.sync();
				coursesLocal.add(coursesStore.getRange());
				coursesLocal.sync();
				var couseDetailStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
				couseDetailStore.removeAll();
				couseDetailStore.sync();
				if (coursesStore.data.length == 0) {
					var newTerm = mobEdu.util.getCoursesTermCode();
					if (newTerm == termCode && !mobEdu.courses.f.isFacultyRole()) {
						if (newTerm == null || newTerm == undefined) {
							mobEdu.courses.f.regCoursesCallCount++;
						}
						if (!mobEdu.courses.f.regCoursesCallCount > 1) {
							mobEdu.courses.f.loadRegisteredCourses(null);
						} else {
							if (mobEdu.util.isDualRole()) {
								mobEdu.courses.f.showCourseList();
							} else {
								mobEdu.util.showMainView();
							}
							Ext.getCmp('courseList').setEmptyText('<h3 align="center">No courses registered for this term.</h3>');
							Ext.Msg.show({

								message: '<p>You do not have courses in current term.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						}
					} else {

						if (mobEdu.courses.f.isFacultyRole()) {
							Ext.getCmp('courseList').setEmptyText('<h3 align="center">You have no classes assigned.</h3> ');
							Ext.Msg.show({

								message: '<p>You have no classes assigned.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox',
							});
						} else {
							Ext.getCmp('courseList').setEmptyText('<h3 align="center">No courses registered for this term.</h3>');
							Ext.Msg.show({

								message: '<p>You do not have courses in current term.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});

						}

						if (mobEdu.util.isDualRole()) {
							mobEdu.courses.f.showCourseList();
						} else {
							mobEdu.util.showMainView();
						}

					}



				} else {
					var store = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
					//            store.load();
					store.removeAll();
					store.sync();
					if (!(Ext.os.is.Phone)) {
						mobEdu.util.get('mobEdu.courses.view.tablet.courseList');
						if (coursesStore.data.length > 0) {
							mobEdu.util.get("mobEdu.courses.view.tablet.courseList");
							var crseRec = coursesStore.data.items[0].data;
							mobEdu.util.setTitle(null, Ext.getCmp('coursesT'), crseRec.termDescription);
							Ext.getCmp("courseList").select(0);
							if (isOnlyCourseDetails == 'false') {
								var tabs = ['crseDetail', 'assignments', 'grades', 'discussions'];
								for (var j = 0; j < tabs.length; j++) {
									Ext.getCmp('courseDetailsTabs').getTabBar().getComponent(j).hide();
								}
								var tabsAllowed = d2ltabs;
								if (tabsAllowed.length > 0) {
									for (var i = 0; i < tabs.length; i++) {
										for (var k = 0; k < tabsAllowed.length; k++) {
											if (tabs[i] === tabsAllowed[k]) {
												Ext.getCmp('courseDetailsTabs').getTabBar().getComponent(i).show();
											}
										}
									}
								}
							}
							this.loadCourseDetails(coursesStore.getAt(0));
							if (mobEdu.courses.f.isAccessAllowed(crseRec.dropFlag) == true) {
								Ext.getCmp('dropCourses').show();
							} else {
								Ext.getCmp('dropCourses').hide();
							}
						}
					} else {
						mobEdu.util.get('mobEdu.courses.view.phone.courseList');
						if (coursesStore.data.length > 0) {
							var crseRec = coursesStore.data.items[0].data;
							mobEdu.util.setTitle(null, Ext.getCmp('coursesP'), crseRec.termDescription);
							var dropEnabled = false;
							coursesStore.each(function(rec) {
								if (mobEdu.courses.f.isAccessAllowed(rec.data.dropFlag)) {
									dropEnabled = true;
								}
							});
							if (dropEnabled) {
								Ext.getCmp('dropCourses').show();
							} else {
								Ext.getCmp('dropCourses').hide();
							}
						}
					}
					//If courses exists , show calendar button otherwise hide
					if (coursesStore.data.length == 0)
						Ext.getCmp('calendar').hide();
					else
						Ext.getCmp('calendar').show();

					mobEdu.courses.f.showCourseList();
				}
			}
		},

		getStatusCode: function(statusCode) {
			if (statusCode == 'WL') {
				return 1;
			} else if (statusCode == 'FAIL') {
				return 2;
			}
			return 0;
		},

		loadCourseDetails: function(record) {
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			store.removeAll();
			store.add(record);
			store.sync();
			if (Ext.os.is.Phone) {
				mobEdu.courses.f.showCourseDetails();
				if (mobEdu.courses.f.isRegistrationAllowed() == false) {
					Ext.getCmp('dropCourse').hide();
				} else {
					if (mobEdu.courses.f.withdrawnCourses(record.data.statusCode) == true && mobEdu.courses.f.isWithdrawAllowed(record.data.actionCode) === true) {
						Ext.getCmp('dropCourse').show();
					} else {
						Ext.getCmp('dropCourse').hide();
					}
				}
			}
			if (showFeedbackButtonInCourses == 'true') {
				var role = mobEdu.util.getRole();
				if ((role.indexOf('FACULTY') > -1 && role.indexOf('STUDENT') == -1) || mobEdu.util.getCurrentRole() === 'FACULTY') {
					Ext.getCmp('crseFeedback').hide();
				} else {
					Ext.getCmp('crseFeedback').show();
				}
			} else {
				Ext.getCmp('crseFeedback').hide();
			}
			if (isOnlyCourseDetails == 'false') {
				Ext.getCmp('courseDetailsList').hide();
				Ext.getCmp('courseDetailsTabs').show();
				Ext.getCmp('courseDetailsTabs').setActiveItem(0);
			} else {
				Ext.getCmp('courseDetailsList').show();
				Ext.getCmp('courseDetailsTabs').hide();
			}
		},

		onCourseItemTap: function(selectedIndex, viewRef, record, item) {
			if (Ext.os.is.Phone) {
				mobEdu.util.deselectSelectedItem(selectedIndex, viewRef);
			}
			dropCourseFlag = 'courseList';

			if (item.target.getAttribute("name") === "mulDrop") {
				var clickedItem = item.target;
				if (item.target.tagName != "DIV") {
					clickedItem = item.target.parentElement;
				}
				if (clickedItem.getElementsByTagName('img')[0].id == "on") {
					clickedItem.getElementsByTagName('img')[0].src = mobEdu.util.getResourcePath() + "images/checkboxoff.png";
					clickedItem.getElementsByTagName('img')[0].id = "off";
				} else {
					clickedItem.getElementsByTagName('img')[0].src = mobEdu.util.getResourcePath() + "images/checkboxon.png";
					clickedItem.getElementsByTagName('img')[0].id = "on";
				}
			} else {
				mobEdu.courses.f.loadCourseDetails(record);
			}
		},

		dropCourse: function() {
			var coursesList = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var items = Ext.DomQuery.select("img[name='mulDrop']");
			var crn = '';
			var actionCode = '';
			Ext.each(items, function(item, i) {
				if (item.id == "on") {
					var rec = coursesList.findRecord('crn', item.title);
					if (rec != null) {
						if (crn == '') {
							crn = rec.data.crn;
							actionCode = rec.data.actionCode;
							crn = crn + '-' + actionCode;
						} else {
							crn = crn + ',' + (crn = rec.data.crn) + '-' + (actionCode = rec.data.actionCode);
						}
					}
				}
			});
			var coursesList = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var record = coursesList.getAt(0);
			var crse = record.copy();
			crse.set('crn', crn);
			if (crn != '') {
				if (dropFormat == 'VIEW') {
					mobEdu.util.updatePrevView(mobEdu.courses.f.showCourseList);
					mobEdu.courses.f.actionType = 'DROP';
					// mobEdu.reg.f.loadTermsAndConditions(crse.get('termCode'), crse.get('crn'), 'D');
					// mobEdu.reg.view.acceptAction = mobEdu.courses.f.doDrop;
					mobEdu.reg.view.params = crse;
					mobEdu.courses.f.doDrop(crse);
				} else {
					Ext.Msg.show({
						message: '<p>Would you like to drop selected courses?</p>',
						buttons: Ext.MessageBox.YESNO,
						cls: 'msgbox',
						fn: function(btn) {
							if (btn == 'yes') {
								mobEdu.courses.f.actionType = 'DROP';
								mobEdu.courses.f.doDrop(crse);
							}
						}
					});
				}
			} else {
				Ext.Msg.show({
					message: 'Select courses to drop.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		dropTermsConditions: function(crse) {
			var eliStore = mobEdu.util.getStore('mobEdu.reg.store.eligibility');
			var eliStatus = eliStore.getProxy().getReader().rawData;
			if (eliStatus.status == '' || eliStatus.status == null) {
				mobEdu.courses.f.doDrop(crse);
			} else {
				if (eliStatus.status != '' || eliStatus.status != null) {
					Ext.Msg.show({
						title: 'Eligibility violation.',
						message: '<p>' + eliStatus.status + '</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}
		},
		dropCourses: function() {
			var coursesList = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var items = Ext.DomQuery.select("img[name='mulDrop']");
			var crn = '';
			Ext.each(items, function(item, i) {
				if (item.id == "on") {
					if (crn == '') {
						crn = coursesList.getAt(i).data.crn;
					} else {
						crn = crn + ',' + coursesList.getAt(i).data.crn;
					}
				}
			});
			var coursesList = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var record = coursesList.getAt(0);
			var crse = record.copy();
			crse.set('crn', crn);
			if (crn != '') {
				mobEdu.courses.f.doDrop(crse);
			}
		},

		doDrop: function(crse) {
			pinFlag = 'drop';
			mobEdu.reg.f.pinMsgCount = 1;
			mobEdu.reg.f.checkPin(crse);
		},

		doDropMultipleCourses: function(record) {
			var store = mobEdu.util.getStore('mobEdu.courses.store.drop');
			if (mobEdu.courses.f.actionType === 'DROP') {
				mobEdu.courses.f.action = null;
			}
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'dropCourse');
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: record.get('termCode'),
				crn: record.get('crn'),
				actionType: mobEdu.courses.f.actionType,
				action: mobEdu.courses.f.action,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.courses.f.dropMultipleCourseResponseHandler(record.get('termCode'));
			}
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'dropcourse');
		},

		dropMultipleCourseResponseHandler: function(termCode) {
			mobEdu.util.updateCoursesTermCode(termCode);
			var store = mobEdu.util.getStore('mobEdu.courses.store.drop');
			var status = store.getProxy().getReader().rawData.status;
			var message = store.getProxy().getReader().rawData.message;
			if (status == 'SUCCESS') {
				if (store.data.length > 0) {
					mobEdu.courses.f.showDropStatus();
				}
			} else if (status == 'TERMS') {
				mobEdu.courses.f.actionType = 'TERMS';
				mobEdu.reg.f.loadDropTandC();
				mobEdu.reg.view.acceptAction = mobEdu.courses.f.doDrop;
				mobEdu.courses.f.action = 'ACCEPT';
			} else if (status == 'LINK') {
				mobEdu.courses.f.actionType = 'LINK';
				Ext.Msg.show({
					message: message,
					buttons: Ext.MessageBox.YESNO,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'yes') {
							mobEdu.courses.f.action = 'DROPLINK';
						} else {
							mobEdu.courses.f.action = 'DONOTDROPLINK';
						}
						mobEdu.courses.f.doDrop(mobEdu.reg.view.params);
					}
				});
			} else {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		dropDetailCourse: function() {
			dropCourseFlag = 'courseDetail';
			mobEdu.util.get('mobEdu.courses.view.phone.courseDetail');
			var crseDetailStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var crseDetail = crseDetailStore.data.items[0];
			var crn = crseDetail.data.crn;
			var actionCode = crseDetail.data.actionCode;
			crn = crn + '-' + actionCode;
			var crse = crseDetail.copy();
			crse.set('crn', crn);
			mobEdu.reg.view.params = crse;
			Ext.Msg.show({
				message: '<p>Would you like to drop course?</p>',
				buttons: Ext.MessageBox.YESNO,
				cls: 'msgbox',
				fn: function(btn) {
					if (btn == 'yes') {
						mobEdu.courses.f.actionType = 'DROP';
						mobEdu.courses.f.doDrop(crse);
					}
				}
			});
		},

		onCalendarBackTap: function() {
			Ext.getCmp('courseList').refresh();
			mobEdu.courses.f.showCourseList();
		},

		getWeek: function(date) {
			switch (date.getDay()) {
				case 0:
					return "Sun";
				case 1:
					return "Mon";
				case 2:
					return "Tue";
				case 3:
					return "Wed";
				case 4:
					return "Thu";
				case 5:
					return "Fri";
				case 6:
					return "Sat";
			}
		},

		//Sorting the min date
		getMinDate: function() {
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var courses = store.data.all;
			var dates = new Array();
			for (var index in courses) {

				var courseRegstatus = courses[index].data.statusCode;

				// Only look for meetings for successfully registered courses.
				if (courseRegstatus == "RW") {

					var meetings = courses[index].data.meeting;
					for (var i = 0; i < meetings.length; i++) {
						var sDate = meetings[i].startDate;
						dates.push(sDate);
					}

				}
			}
			var minDate = dates.sort()[0];
			return minDate;
		},

		//Meeting schedule display in initial loading
		setScheduleInfo: function(selectedDate) {
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}

			var scheduleInfo = '';
			scheduleInfo = mobEdu.courses.f.getHolidaySchedule(selectedDate);
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var isFaculty = mobEdu.courses.f.isFacultyRole();

			// We are using local store to store the meetings to be displayed
			// Clear it first
			mobEdu.util.getStore('mobEdu.courses.store.meetingScheduleLocal').removeAll();

			store.each(function(rec) {
				var statusCode = rec.get('statusCode');

				if (isFaculty || statusCode.indexOf('R') >= 0) {
					var title = rec.get('courseTitle');
					var meetings = rec.get('meeting');
					Ext.each(meetings, function(item) {
						var meetingStartDate = Ext.Date.parse(item.startDate, 'm-d-Y');
						var meetingEndDate = Ext.Date.parse(item.endDate, 'm-d-Y');

						if (Ext.Date.between(selectedDate, meetingStartDate, meetingEndDate)) {

							var dayOfWeek = Ext.Date.format(selectedDate, "D");

							//Split the periods from list
							var period = item.period;
							var periodList = period.split(',');

							if (Ext.Array.contains(periodList, dayOfWeek)) {
								mobEdu.courses.f.addClassToStore(title, item.beginTime, item.endTime, item.mtypCode, item.bldgCode, item.roomCode, item.latitude, item.longitude);
							}
						}
					}, this);
				}
			});
			if (scheduleInfo != '') {
				scheduleInfo = '<b>' + scheduleInfo + '</b>' + '<br/>';
				Ext.getCmp('lablST').setHtml('<h3>' + scheduleInfo + '</h3>');
			} else {
				scheduleInfo = mobEdu.courses.f.formatScheduleInfo();
				Ext.getCmp('lablST').setHtml('<h3>' + scheduleInfo + '</h3>');
			}
		},

		//Show the calendar view
		calendarViewShow: function() {
			mobEdu.main.f.calendarInstance = 'courses';
			mobEdu.util.get('mobEdu.courses.view.calendarView');
			var coursesDock = Ext.getCmp('coursesDock');
			coursesDock.remove(calendar);

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'crseCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			coursesDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.courses.f.setScheduleInfo();

		},

		//To highlight Schedule dates
		doHighlightCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			store.each(function(rec) {
				var meetings = rec.get('meeting');
				var statusType = rec.get('statusCode');
				// Only look for meetings for successfully registered courses.
				if (mobEdu.courses.f.isFacultyRole() == true || statusType.indexOf('R') >= 0) {
					Ext.each(meetings, function(item) {
						var startDate = Ext.Date.parse(item.startDate, "m-d-Y");
						var endDate = Ext.Date.parse(item.endDate, "m-d-Y");
						var period = item.period;
						//Split the periods from list
						var periodList = period.split(',');
						do {
							var dayOfWeek = Ext.Date.format(startDate, "D");
							if (Ext.Array.contains(periodList, dayOfWeek)) {
								if (!(startDate < values.date || startDate > values.date)) {
									classes.push(highlightedItemCls);
								}
							}
							//Incrementing startDate
							var startDateStr = Ext.Date.format(Ext.Date.clearTime(startDate), 'm-d-Y');
							var startDatePlusOneStr = Ext.Date.format(Ext.Date.add(startDate, Ext.Date.DAY, 1), 'm-d-Y');

							if (startDateStr == startDatePlusOneStr) {
								startDate = Ext.Date.add(startDate, Ext.Date.DAY, 1);
							}

							startDate = Ext.Date.add(startDate, Ext.Date.DAY, 1);
						} while (startDate <= endDate)
					}, this);
				}
			});
			return classes;
		},

		doHighlightHolidaysCalenderViewCell: function(classes, values, highlightedHolidayItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
			store.each(function(rec) {
				var startDate = Ext.Date.parse(rec.get('date'), "m-d-Y");
				// var days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
				var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
				var dayOfWeek = Ext.Date.format(startDate, "D");
				if (Ext.Array.contains(days, dayOfWeek)) {
					if (!(startDate < values.date || startDate > values.date)) {
						classes.push(highlightedHolidayItemCls);
					}
				}
			});
			return classes;
		},

		addClassToStore: function(cTitle, sTime, eTime, type, buildgNo, roomNo, latitude, longitude) {
			var meetingsLocalStore = mobEdu.util.getStore('mobEdu.courses.store.meetingScheduleLocal');
			meetingsLocalStore.add({
				'title': cTitle,
				'startTime': sTime,
				'endTime': eTime,
				'type': type,
				'buildgNo': buildgNo,
				'roomNo': roomNo,
				'latitude': latitude,
				'longitude': longitude
			});
			meetingsLocalStore.sync();
		},

		//selection to change display the meetings schedule info
		formatScheduleInfo: function() {
			var meetingsLocalStore = mobEdu.util.getStore('mobEdu.courses.store.meetingScheduleLocal');
			var sData = meetingsLocalStore.data.all;
			sData.sort(function(first, second) {
				var fst = first.data.startTime;
				var secSt = second.data.startTime;
				if (fst.indexOf('AM', 1) == secSt.indexOf('AM', 1) || fst.indexOf('PM', 1) == secSt.indexOf('PM', 1)) {
					var fTime = fst.replace('AM', '');
					fTime = fTime.replace('PM', '');
					var sTime = secSt.replace('AM', '');
					sTime = sTime.replace('PM', '');
					var fh = fTime.substring(0, fTime.indexOf(':'));
					var sh = sTime.substring(0, sTime.indexOf(':'));
					if (eval(fh) < eval(sh)) {
						return -1;
					} else if (eval(fh) > eval(sh)) {
						return 1;
					} else if (eval(fh) == eval(sh)) {
						var fm = fTime.substr(fTime.indexOf(':') + 1);
						var sm = sTime.substr(sTime.indexOf(':') + 1);
						if (eval(fm) < eval(sm)) {
							return -1;
						} else if (eval(fm) < eval(sm)) {
							return 1;
						}
					}
				} else if (fst.indexOf('AM', 1) == 1) {
					return -1;
				} else if (secSt.indexOf('AM', 1) == 1) {
					return 1;
				}
			});

			var scheduleInfo = '';
			for (var c = 0; c < sData.length; c++) {
				var type = sData[c].data.type;
				if (type != 'CLAS') {
					scheduleInfo = scheduleInfo + '<b>' + sData[c].data.title + '</b>' + '<br/>';
				} else if (type != 'LAB') {
					scheduleInfo = scheduleInfo + '<b>' + sData[c].data.title + '</b>' + '<br/>';
				}
				scheduleInfo = scheduleInfo + sData[c].data.startTime + '-' + sData[c].data.endTime + '<br/>';
				if (type == 'CLAS') {
					scheduleInfo = scheduleInfo + 'ClassType: Lecture' + '<br/>';
				} else {
					scheduleInfo = scheduleInfo + 'ClassType:' + sData[c].data.type + '<br/>';
				}
				if (sData[c].data.buildgNo != 'NA' && sData[c].data.latitude != null && sData[c].data.longitude != null && sData[c].data.latitude != 'NA' && sData[c].data.longitude != 'NA') {
					scheduleInfo = scheduleInfo + 'Building:' + sData[c].data.buildgNo + '(<h8><a href="javascript:mobEdu.courses.f.showCalenderMapDirections(' + sData[c].data.latitude + ',' + sData[c].data.longitude + ')">Click Here</a> to get directions)</h8> <br/>';
				} else {
					scheduleInfo = scheduleInfo + 'Building:' + sData[c].data.buildgNo + '<br/>';
				}
				scheduleInfo = scheduleInfo + 'Room:' + sData[c].data.roomNo + '<br/>' + '<br/>';
			}

			return scheduleInfo;
		},

		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},

		getHolidaySchedule: function(selectedDate) {

			var newDateValue = Ext.Date.format(selectedDate, 'm-d-Y');

			var holidaysScheduleInfo = "";
			var holidaysLocalStore = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
			var holidaysData = holidaysLocalStore.data.all;
			for (var h = 0; h < holidaysData.length; h++) {
				var holidayDate = holidaysData[h].data.date;
				if (newDateValue == holidayDate) {
					holidaysScheduleInfo = holidaysData[h].data.holidayDesc;
				}
			}
			return holidaysScheduleInfo;
		},

		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.courses.f.dateChecking([currentDate.getMonth() + 1]) + '-' + mobEdu.courses.f.dateChecking(currentDate.getDate()) + '-' + currentDate.getFullYear());
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.courses.f.dateChecking([presentDate.getMonth() + 1]) + '-' + mobEdu.courses.f.dateChecking(presentDate.getDate()) + '-' + presentDate.getFullYear());
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.courses.f.setScheduleInfo(date);
			}
		},

		checkHolidaysList: function() {
			var holidaysLocalStore = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
			var holidaysLocalData = holidaysLocalStore.data.all;
			if (holidaysLocalData.length == 0 || holidaysLocalData.length == null) {
				mobEdu.courses.f.getHolidaysList();
			} else {
				mobEdu.courses.f.showCalendarView();
			}
		},

		getHolidaysList: function() {
			var currentYear = new Date().getFullYear();
			//            var currentYear = 2010;
			var holidaysStore = mobEdu.util.getStore('mobEdu.courses.store.holidays');
			holidaysStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			holidaysStore.getProxy().setUrl(webserver + 'getHolidays');
			holidaysStore.getProxy().setExtraParams({
				year: currentYear,
				companyId: companyId
			});
			holidaysStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.getHolidaysListResponseHandler();
			};
			holidaysStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'holidays');
		},

		getHolidaysListResponseHandler: function() {
			var holidaysLocalStore = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
			var holidaysStore = mobEdu.util.getStore('mobEdu.courses.store.holidays');
			var response = holidaysStore.getProxy().getReader().rawData;
			var holidaysData = holidaysStore.data.all;
			if (response.status == "SUCCESS") {
				holidaysLocalStore.add(holidaysData);
				holidaysLocalStore.sync();
				mobEdu.courses.f.showCalendarView();
			} else {
				Ext.Msg.show({
					message: holidaysData.data.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		holidaysCheck: function(date) {
			// Check if we have holidays data for the upcoming year
			var year = date.getFullYear();
			var holidaysLocalStore = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
			var recordIndex = holidaysLocalStore.find('year', year);
			if (recordIndex === -1) {
				// year not found in local store, pull year's holidays from banner
				mobEdu.courses.f.holidaysHighlight(year);
			}
		},

		holidaysHighlight: function(year) {
			var holidaysStore = mobEdu.util.getStore('mobEdu.courses.store.holidays');
			holidaysStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			holidaysStore.getProxy().setUrl(webserver + 'getHolidays');
			holidaysStore.getProxy().setExtraParams({
				year: year,
				companyId: companyId
			});
			holidaysStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.holidaysHighlightResponseHandler();
			};
			holidaysStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'holidays');
		},

		holidaysHighlightResponseHandler: function() {
			var holidaysLocalStore = mobEdu.util.getStore('mobEdu.courses.store.holidaysLocal');
			var holidaysStore = mobEdu.util.getStore('mobEdu.courses.store.holidays');
			var response = holidaysStore.getProxy().getReader().rawData;
			var holidaysData = holidaysStore.data.all;
			if (response.status == "SUCCESS") {
				holidaysLocalStore.add(holidaysData);
				holidaysLocalStore.sync();
			} else {
				Ext.Msg.show({
					message: holidaysData.data.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCoursesMapDirections: function(latitude, longitude) {
			if (Ext.os.is.Phone) {
				mapFlag = 'courseDetails';
			} else {
				mapFlag = 'courseList';
			}

			mobEdu.courses.f.getMapDirections(latitude, longitude);
		},

		showCartMapDirections: function(latitude, longitude, flag) {
			mapFlag = flag;

			mobEdu.courses.f.getMapDirections(latitude, longitude);
		},

		getMapDirections: function(latitude, longitude) {
			mobEdu.courses.f.reNewMap();
			mobEdu.courses.f.showMapDirections();
			if (!(latitude == undefined || latitude == '' || latitude == null || longitude == undefined || longitude == null || longitude == '')) {
				destPosition = new google.maps.LatLng(latitude, longitude);
			}
			var geoLocationOptions = {
				maximumAge: 3000,
				timeout: 5000,
				enableHighAccuracy: true
			};
			var marker, directionsRenderer, directionsService;

			navigator.geolocation.getCurrentPosition(geoLocationSuccess, geoLocationError, geoLocationOptions);

			function geoLocationSuccess(position) {

				directionsService = new google.maps.DirectionsService();
				directionsRenderer = new google.maps.DirectionsRenderer(courseDirMap.mapRendererOptions);
				directionsRenderer.setMap(courseDirMap.getMap());

				marker = new google.maps.Marker({
					map: courseDirMap.getMap(),
					visible: true
				});

				var start = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //cor
				// set the request options

				var request = {
					origin: start,
					destination: destPosition,
					provideRouteAlternatives: false,
					travelMode: google.maps.DirectionsTravelMode.DRIVING // DRIVING, WALKING, BICYCLING
				};
				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						// Display the directions using Google's Directions Renderer.
						directionsRenderer.setPanel(document.getElementById('map-directions'));
						directionsRenderer.setDirections(result);
					} else {
						Ext.Msg.show({
							message: '<p>Error while getting direction.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				});
			}

			function geoLocationError() {
				Ext.Msg.show({
					message: '<p>Error while getting current position.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCalenderMapDirections: function(latitude, longitude) {
			mapFlag = 'calender';
			mobEdu.courses.f.getMapDirections(latitude, longitude);
		},

		showMapDirections: function(latitude, longitude, flag) {
			if (!(latitude == undefined || latitude == '' || latitude == null || longitude == undefined || longitude == null || longitude == '')) {
				destPosition = new google.maps.LatLng(latitude, longitude);
			}
			if (mapFlag == 'cartCalender') {
				mobEdu.util.updatePrevView(mobEdu.reg.f.showCalenderView);
			} else if (mapFlag == 'courseList') {
				mobEdu.util.updatePrevView(mobEdu.courses.f.showCourseList);
			} else if (mapFlag == 'courseDetails') {
				mobEdu.util.updatePrevView(mobEdu.courses.f.showCrseDetailsTab);
			} else {
				mobEdu.util.updatePrevView(mobEdu.courses.f.showCalendarView);
			}
			mobEdu.util.show('mobEdu.courses.view.coursesMap');

		},

		showTermListPopup: function(popupView) {
			mobEdu.util.updatePrevView(mobEdu.courses.f.hideTermListPopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.courses.view.tablet.termList'));
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},

		hideTermListPopup: function() {
			if (billingFlag == true) {
				mobEdu.util.updatePrevView(mobEdu.crl.f.showBillingInfo);
			} else {
				mobEdu.util.updatePrevView(mobEdu.courses.f.showCourseList);
			}
			//            mobEdu.util.updatePrevView(mobEdu.courses.f.showCourseList);
			var popUp = Ext.getCmp('coursesTermListPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		onTermItemTap: function(view, index, target, record, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			if (billingFlag != true) {
				mobEdu.util.updateCoursesTermCode(record.data.code);
			}
			// mobEdu.util.updateCoursesTermDescription(record.data.description);
			// if(Ext.os.is.Phone) {
			//     Ext.getCmp('coursesP').setTitle('<h1>' + mobEdu.util.getCoursesTermDescription() + '</h1>');
			// } else {
			//     mobEdu.courses.f.hideTermListPopup();
			//     Ext.getCmp('coursesT').setTitle('<h1>' + mobEdu.util.getCoursesTermDescription() + '</h1>');
			// }
			if (!(Ext.os.is.Phone)) {
				mobEdu.courses.f.hideTermListPopup();
			}
			if (billingFlag == true) {
				mobEdu.crl.f.loadBillingInfo(record.data.code);
				billingFlag = false;
			} else {
				mobEdu.courses.f.loadRegisteredCourses(record.data.code);
			}
		},

		isCreditLink: function(varCredit) {
			if (varCredit == true && enableUpdateVarCredits == 'true') {
				return true;
			} else {
				return false;
			}
		},

		checkUpdateCreditHrsEligibilty: function() {
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var record = store.getAt(0);
			pinFlag = 'updateCredits';
			mobEdu.reg.f.checkEligibility(record);
		},

		getVariableCreditHrs: function() {
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var rec = store.getAt(0);
			var hrs = rec.get('varCreditHrs');
			if (hrs != null && hrs != '') {
				var hrsOptions = mobEdu.courses.f.getHoursOptions(hrs);
				Ext.Msg.show({
					message: '<p>Please select variable credit hours.</p>',
					prompt: {
						xtype: 'selectfield',
						options: hrsOptions
					},
					buttons: Ext.MessageBox.OKCANCEL,
					cls: 'msgbox',
					fn: function(btn, value) {
						if (btn == 'ok') {
							mobEdu.courses.f.updateCreditHrs(rec.get('termCode'), rec.get('crn'), value, rec.get('credits'));
						}
					}
				});
			} else {
				var min = rec.get('minCredit');
				var max = rec.get('maxCredit');
				Ext.Msg.show({
					message: '<p>Please provide variable credit hours between' + min + ' and ' + max + '.</p>',
					prompt: {
						xtype: 'numberfield'
					},
					buttons: Ext.MessageBox.OKCANCEL,
					cls: 'msgbox',
					fn: function(btn, value) {
						if (btn == 'ok') {
							if (value >= min && value <= max) {
								mobEdu.courses.f.updateCreditHrs(rec.get('termCode'), rec.get('crn'), value, rec.get('credits'));
							} else {
								Ext.Msg.show({
									message: '<p>Invalid credit hours.</p>',
									buttons: Ext.MessageBox.OK,
									cls: 'msgbox'
								});
							}
						}
					}
				});
			}
		},
		updateCreditHrs: function(termCode, crn, creditHrs, oldCreditHrs) {
			var store = mobEdu.util.get('mobEdu.courses.store.drop');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(webserver + 'updateCreditHrs');
			store.getProxy().setExtraParams({
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				crn: crn,
				creditHrs: creditHrs,
				oldCreditHrs: oldCreditHrs,
				companyId: companyId
			});
			store.getProxy().afterRequest = function() {
				mobEdu.courses.f.updateCreditHrsResponseHandler(termCode);
			};
			store.load();
			mobEdu.util.gaTrackEvent('enroll', 'updatecredithrs');
		},

		updateCreditHrsResponseHandler: function(termCode) {
			var store = mobEdu.util.get('mobEdu.courses.store.drop');
			var status = store.getProxy().getReader().rawData.status;
			if (status == 'SUCCESS') {
				mobEdu.courses.f.loadRegisteredCourses(termCode);
			} else {
				Ext.Msg.show({
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		getHoursOptions: function(hrs) {
			var hrsOptions = new Array();
			var array = hrs.split(',');
			for (var cnt = 0; cnt < array.length; cnt++) {
				hrsOptions.push({
					text: array[cnt],
					value: array[cnt]
				});
			}
			return hrsOptions;

		},

		getMapDirInstructions: function() {
			var geoLocationOptions = {
				maximumAge: 3000,
				timeout: 5000,
				enableHighAccuracy: true
			};
			var directionsDisplay, directionsService;
			navigator.geolocation.getCurrentPosition(geoLocation, geoLocationError, geoLocationOptions);

			function geoLocation(position) {
				directionsService = new google.maps.DirectionsService();
				directionsDisplay = new google.maps.DirectionsRenderer();
				directionsDisplay.setPanel(document.getElementById('dirdiv'));
				var start = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //cor

				var request = {
					origin: start,
					destination: destPosition,
					travelMode: google.maps.DirectionsTravelMode.DRIVING
				};
				directionsService.route(request, function(response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
					}
					var x = 0;
					var inst = new Array(),
						dist = new Array();
					Ext.each(response.routes[0].legs[0].steps, function(y) {
						inst.push(response.routes[0].legs[0].steps[x].instructions);
						dist.push(response.routes[0].legs[0].steps[x].distance.text);
						x = x + 1;
					});
					var instructions = '<table>';
					var i = 0;
					Ext.each(inst, function() {
						instructions += '<tr><td><h4>';
						instructions += inst[i];
						instructions += '</h4></td><td width="50px"><h4>';
						instructions += dist[i].toString();
						instructions += '</h4></td></tr>';
						i = i + 1;
					});
					instructions += '</table>';

					mobEdu.util.get('mobEdu.courses.view.mapDirectionInst');
					Ext.getCmp('crsDirInst').setHtml(instructions);
					mobEdu.courses.f.showMapDirInst();
				});
			}

			function geoLocationError() {
				Ext.Msg.alert('Error', 'Error while getting geolocation.');
			}
		},

		loadFeedback: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			if (detailStore.data.length > 0) {
				mobEdu.custserv.f.feedbackPrevView = 'courses'
				mobEdu.util.doLoginCheck(true, 'mobEdu.custserv.f.showFeedback', 'COURSES');
			} else {
				Ext.Msg.show({
					message: '<p>Please select a course.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCrseDetailsTab: function() {
			mobEdu.courses.f.showCourseDetails();
		},

		showCourseDetails: function() {
			mobEdu.util.updatePrevView(mobEdu.courses.f.showCourseList);
			mobEdu.util.show('mobEdu.courses.view.phone.courseDetail');
			if (isOnlyCourseDetails == 'false') {
				var tabs = ['crseDetail', 'assignments', 'grades', 'discussions'];
				for (var j = 0; j < tabs.length; j++) {
					Ext.getCmp('courseDetailsTabs').getTabBar().getComponent(j).hide();
				}
				var tabsAllowed = d2ltabs;
				if (tabsAllowed.length > 0) {
					for (var i = 0; i < tabs.length; i++) {
						for (var k = 0; k < tabsAllowed.length; k++) {
							if (tabs[i] === tabsAllowed[k]) {
								Ext.getCmp('courseDetailsTabs').getTabBar().getComponent(i).show();
							}
						}
					}
				}
			}
			var proxyUser = mobEdu.util.getProxyUserId();
			if (proxyUser != null && proxyUser != '') {
				if (mobEdu.util.getProxyAccessFlag() == '0') {
					Ext.getCmp('dropCourse').hide();
				} else {
					Ext.getCmp('dropCourse').show();
				}
			} else {
				Ext.getCmp('dropCourse').show();
			}
		},

		isRegistrationAllowed: function() {
			var moduleStore1 = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
			var regEnabled = false;
			for (i = 0; i < moduleStore1.data.length; i++) {
				var modCode = moduleStore1.data.all[i].data.code;
				if (modCode === 'REG')
					regEnabled = true;
			}
			if (!regEnabled)
				return false;
			else
				return true;
		},

		showCourseList: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			var moduleStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');

			var registrationEnabled = false;
			var attendanceEnabled = false;

			for (i = 0; i < moduleStore.data.length; i++) {
				var moduleCode = moduleStore.data.all[i].data.code;
				if (moduleCode === 'REG') {
					registrationEnabled = true;
				} else if (moduleCode === 'ATTD') {
					attendanceEnabled = true;
				}
			}

			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.courses.view.phone.courseList');
			} else {
				if (isOnlyCourseDetails == 'false') {
					var tabs = ['crseDetail', 'assignments', 'grades', 'discussions'];
					for (var j = 0; j < tabs.length; j++) {
						Ext.getCmp('courseDetailsTabs').getTabBar().getComponent(j).hide();
					}
					var tabsAllowed = d2ltabs;
					if (tabsAllowed.length > 0) {
						for (var i = 0; i < tabs.length; i++) {
							for (var k = 0; k < tabsAllowed.length; k++) {
								if (tabs[i] === tabsAllowed[k]) {
									Ext.getCmp('courseDetailsTabs').getTabBar().getComponent(i).show();
								}
							}
						}
					}
				}
				mobEdu.util.show('mobEdu.courses.view.tablet.courseList');
			}
			if (!attendanceEnabled) {
				Ext.getCmp('attendance').hide();
			}
			if (!registrationEnabled) {
				Ext.getCmp('dropCourses').hide();
			}

		},
		dropCourseStatus: function() {
			var dropTermCode = mobEdu.util.getCoursesTermCode();
			mobEdu.courses.f.loadRegisteredCourses(dropTermCode);
		},
		showDropStatus: function() {
			mobEdu.util.updatePrevView(mobEdu.courses.f.dropCourseStatus);
			mobEdu.util.show('mobEdu.courses.view.dropStatus');
		},

		isSuccess: function(status) {
			if (status == 'F') {
				return false;
			} else {
				return true;
			}
		},

		showCalendarView: function() {
			mobEdu.util.updatePrevView(mobEdu.courses.f.onCalendarBackTap);
			mobEdu.courses.f.calendarViewShow();
			mobEdu.util.show('mobEdu.courses.view.calendarView');

		},

		showTermList: function() {
			if (billingFlag == true) {
				mobEdu.util.updatePrevView(mobEdu.crl.f.showBillingInfo);
			} else {
				mobEdu.util.updatePrevView(mobEdu.courses.f.showCourseList);
			}
			mobEdu.util.show('mobEdu.courses.view.phone.termList');
		},

		showMapDirInst: function() {
			mobEdu.util.updatePrevView(mobEdu.courses.f.showMapDirections);
			mobEdu.util.show('mobEdu.courses.view.mapDirectionInst');
		},


		loadRegTermList: function(flag) {
			var tStore = mobEdu.util.getStore('mobEdu.main.store.termList');
			//            if(tStore.getCount() > 0) {
			//                tStore.removeAll();
			//                tStore.sync();
			//                tStore.removed = [];
			//
			//            }
			if (mobEdu.courses.f.isFacultyRole()) {
				tStore.getProxy().setUrl(webserver + 'getFacultyTerms');
				tStore.getProxy().setExtraParams({
					'facultyId': mobEdu.main.f.getStudentId(),
					companyId: companyId
				});
			} else {
				tStore.getProxy().setUrl(webserver + 'getMyCoursesTerms');
				tStore.getProxy().setExtraParams({
					studentId: mobEdu.main.f.getStudentId(),
					companyId: companyId
				});
			}
			tStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			tStore.getProxy().afterRequest = function() {
				if (flag != 'feedback') {
					mobEdu.courses.f.regTermListResponseHandler();
				}
			}
			tStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'mycoursesterms');
		},

		regTermListResponseHandler: function() {
			// flag:0-Request received from reg module
			// flag:1-Request received from courses module
			var tStore = mobEdu.util.getStore('mobEdu.main.store.termList');
			if (tStore.data.length == 0) {
				Ext.Msg.show({
					message: '<p>You do not have courses registered in any term.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (Ext.os.is.Phone) {
					mobEdu.courses.f.showTermList();
				} else {
					mobEdu.courses.f.showTermListPopup();
				}
			}
		},

		checkValue: function(value) {
			if (value < 10) {
				return '0' + value;
			}

			return value;
		},


		loadAnnouncements: function() {
			var announcementsStore = mobEdu.util.getStore('mobEdu.courses.store.announcements');
			if (announcementsStore.data.length > 0) {
				announcementsStore.removeAll();
				announcementsStore.removed = [];
			}

			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var courseId = courseStore.getAt(0).get('courseDet');
			announcementsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			announcementsStore.getProxy().setUrl(webserver + 'getAnnouncements');
			announcementsStore.getProxy().setExtraParams({
				courseID: courseId,
				studentId: mobEdu.main.f.getStudentId(),
				studentLdapId: mobEdu.main.f.getStudentLdapId(),
				companyId: companyId
			});
			announcementsStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.announcementsResponseHandler();
			};
			announcementsStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'announcements');
		},

		announcementsResponseHandler: function() {
			var announcementsStore = mobEdu.util.getStore('mobEdu.courses.store.announcements');
			if (announcementsStore.data.length == 1) {
				var status = announcementsStore.getAt(0).get('status');
				if (status != 'success' && status != null) {
					announcementsStore.removeAll();
					announcementsStore.removed = [];
					Ext.Msg.show({
						message: status,
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					var title = announcementsStore.getAt(0).get('title');
					var body = announcementsStore.getAt(0).get('body');
					if ((title == '' || title == null) && (body == '' || body == null)) {
						announcementsStore.removeAll();
						announcementsStore.removed = [];
						if (!(Ext.os.is.Phone)) {
							Ext.getCmp('announcements').refresh();
						}
					}
				}
			}
		},

		loadAssignments: function() {
			var assignmentsStore = mobEdu.util.getStore('mobEdu.courses.store.assignments');
			if (assignmentsStore.data.length > 0) {
				assignmentsStore.removeAll();
				assignmentsStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			assignmentsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});

			var crn = courseStore.getAt(0).get('crn');
			var termCode = courseStore.getAt(0).get('termCode');

			assignmentsStore.getProxy().setUrl(webserver + 'getAssignments');
			assignmentsStore.getProxy().setExtraParams({
				crn: crn,
				termCode: termCode,
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			assignmentsStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.assignmentsResponseHandler();
			};
			assignmentsStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'assignments');
		},
		assignmentsResponseHandler: function() {
			var assignmentsStore = mobEdu.util.getStore('mobEdu.courses.store.assignments');
			if (assignmentsStore.data.length === 0) {
				Ext.getCmp('assignments').setEmptyText('<center><h3>No Assignments</h3><center>');
			} else {
				Ext.getCmp('assignments').setEmptyText(null);
			}
		},

		loadGrades: function() {
			var gradesStore = mobEdu.util.getStore('mobEdu.courses.store.grades');
			if (gradesStore.data.length > 0) {
				gradesStore.removeAll();
				gradesStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var crn = courseStore.getAt(0).get('crn');
			var termCode = courseStore.getAt(0).get('termCode');
			gradesStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			gradesStore.getProxy().setUrl(webserver + 'getGrades');
			gradesStore.getProxy().setExtraParams({
				crn: crn,
				termCode: termCode,
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			gradesStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.gradesResponseHandler();
			};
			gradesStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'grades');
		},

		gradesResponseHandler: function() {
			var gradesStore = mobEdu.util.getStore('mobEdu.courses.store.grades');
			if (gradesStore.data.length === 0) {
				Ext.getCmp('grades').setEmptyText('<center><h3>No Grades</h3><center>');
			} else {
				Ext.getCmp('grades').setEmptyText(null);
			}
		},
		formatTime: function(time) {
			formattedTime = [time.slice(0, 5), " ", time.slice(5)].join('');
			return formattedTime;
		},
		loadClassmates: function() {
			var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
			if (classmatesStore.data.length > 0) {
				classmatesStore.removeAll();
				classmatesStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var courseId = courseStore.getAt(0).get('courseDet');
			var crn = courseStore.getAt(0).get('crn');
			var termCode = courseStore.getAt(0).get('termCode');
			classmatesStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			classmatesStore.getProxy().setUrl(webserver + 'getClassmates');
			classmatesStore.getProxy().setExtraParams({
				courseID: courseId,
				crn: crn,
				termCode: termCode,
				companyId: companyId
			});
			classmatesStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.classmatesResponseHandler();
			};
			classmatesStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'classmates');
		},
		classmatesResponseHandler: function() {
			var classmatesStore = mobEdu.util.getStore('mobEdu.courses.store.classmates');
			classmatesStore.sort('name', 'ASC');
			if (classmatesStore.data.length == 1) {
				var name = classmatesStore.getAt(0).get('name');
				var email = classmatesStore.getAt(0).get('email');
				if ((name == '' || name == null) && (email == '' || email == null)) {
					classmatesStore.removeAll();
					classmatesStore.removed = [];
					if (!(Ext.os.is.Phone)) {
						Ext.getCmp('Cclassmates').refresh();
					}
				}
			}
		},
		loadFaculty: function() {
			var facultyStore = mobEdu.util.getStore('mobEdu.courses.store.faculty');
			if (facultyStore.data.length > 0) {
				facultyStore.removeAll();
				facultyStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var courseId = courseStore.getAt(0).get('courseDet');
			var termCode = courseStore.getAt(0).get('termCode');
			var crn = courseStore.getAt(0).get('crn');
			facultyStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			facultyStore.getProxy().setUrl(webserver + 'getFaculty');
			facultyStore.getProxy().setExtraParams({
				courseID: courseId,
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				crn: crn,
				companyId: companyId
			});
			facultyStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.facultyResponseHandler();
			};
			facultyStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'faculty');
		},

		facultyResponseHandler: function() {
			var facultyStore = mobEdu.util.getStore('mobEdu.courses.store.faculty');
			facultyStore.sort('name', 'ASC');
			if (facultyStore.data.length == 1) {
				var name = facultyStore.getAt(0).get('name');
				var email = facultyStore.getAt(0).get('email');
				if ((name == '' || name == null) && (email == '' || email == null)) {
					facultyStore.removeAll();
					facultyStore.removed = [];
					if (!(Ext.os.is.Phone)) {
						Ext.getCmp('Ffaculty').refresh();
					}
				}
			}
		},
		loadAdvisors: function() {
			var advisorsStore = mobEdu.util.getStore('mobEdu.courses.store.advisors');
			if (advisorsStore.data.length > 0) {
				advisorsStore.removeAll();
				advisorsStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var courseId = courseStore.getAt(0).get('courseDet');
			var termCode = courseStore.getAt(0).get('termCode');
			advisorsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			advisorsStore.getProxy().setUrl(webserver + 'getAdvisors');
			advisorsStore.getProxy().setExtraParams({
				courseID: courseId,
				studentId: mobEdu.main.f.getStudentId(),
				termCode: termCode,
				companyId: companyId
			});
			advisorsStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.advisorsResponseHandler();
			};
			advisorsStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'advisors');
		},

		advisorsResponseHandler: function() {
			var advisorsStore = mobEdu.util.getStore('mobEdu.courses.store.advisors');
			advisorsStore.sort('name', 'ASC');
			if (advisorsStore.data.length == 1) {
				var name = advisorsStore.getAt(0).get('name');
				var email = advisorsStore.getAt(0).get('email');
				if ((name == '' || name == null) && (email == '' || email == null)) {
					advisorsStore.removeAll();
					advisorsStore.removed = [];
					if (!(Ext.os.is.Phone)) {
						Ext.getCmp('Aadvisors').refresh();
					}
				}
			}
		},
		loadDiscussions: function() {
			var discussionsStore = mobEdu.util.getStore('mobEdu.courses.store.discussions');
			if (discussionsStore.data.length > 0) {
				discussionsStore.removeAll();
				discussionsStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var courseId = courseStore.getAt(0).get('courseDet');
			var termCode = courseStore.getAt(0).get('termCode');
			var crn = courseStore.getAt(0).get('crn');

			discussionsStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			discussionsStore.getProxy().setUrl(webserver + 'getForums');
			discussionsStore.getProxy().setExtraParams({
				crn: crn,
				termCode: termCode,
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			discussionsStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.discussionsResponseHandler();
			};
			discussionsStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'forums');
		},

		discussionsResponseHandler: function() {
			var assignmentsStore = mobEdu.util.getStore('mobEdu.courses.store.discussions');
			if (assignmentsStore.data.length === 0) {
				Ext.getCmp('discussions').setEmptyText('<center><h3>No Discussions</h3><center>');
			} else {
				Ext.getCmp('discussions').setEmptyText(null);
			}
		},

		onDiscussionsItemTap: function(view, index, target, record, item, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			var messagesStore = mobEdu.util.getStore('mobEdu.courses.store.messages');
			if (messagesStore.data.length > 0) {
				messagesStore.removeAll();
				messagesStore.removed = [];
			}
			var courseStore = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
			var crn = courseStore.getAt(0).get('crn');
			var termCode = courseStore.getAt(0).get('termCode');
			messagesStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			messagesStore.getProxy().setUrl(webserver + 'getThreadMessages');
			messagesStore.getProxy().setExtraParams({
				crn: crn,
				termCode: termCode,
				forumID: record.get('forumId'),
				msgID: record.get('threadId'),
				studentId: mobEdu.main.f.getStudentId(),
				companyId: companyId
			});
			messagesStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.messagesResponseHandler();
			};
			messagesStore.load();
			mobEdu.util.gaTrackEvent('enroll', 'threadmessages');
		},

		messagesResponseHandler: function() {
			var messagesStore = mobEdu.util.getStore('mobEdu.courses.store.messages');
			if (messagesStore.data.length > 0) {
				var status = messagesStore.getAt(0).get('status');
				if (status != 'success' && status != null) {
					messagesStore.removeAll();
					messagesStore.removed = [];
					Ext.Msg.show({
						message: status,
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					Ext.getCmp('forumsContainer').hide();
					Ext.getCmp('msgContainer').show();
					Ext.getCmp('messages').refresh();
				}
			} else {
				Ext.Msg.show({
					message: '<p>No messages.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		onForumsLinkTap: function() {
			Ext.getCmp('forumsContainer').show();
			Ext.getCmp('msgContainer').hide();
		},

		separatorConverter: function(title) {
			var result = title.replace(/[;&,]/g, "<br>");
			return result;
		},

		isAccessAllowed: function(dropFlag) {
			if (dropFlag == 'Y') {
				dropFlag = true;
			} else {
				dropFlag = false
			}
			var proxyUser = mobEdu.util.getProxyUserId();
			if (proxyUser != null && proxyUser != '') {
				if (mobEdu.util.getProxyAccessFlag() == '0') {
					return false;
				} else {
					return dropFlag;
				}
			} else {
				return dropFlag;
			}
		},

		withdrawnCourses: function(statusCode) {
			if (statusCode != null && statusCode.match('R')) {
				return true;
			} else {
				return false;
			}
		},

		isActionAllowed: function(actionCode) {
			if (withdrawEnable === 'true') {
				if (actionCode === null || actionCode === '' || actionCode === 'NA')
					return false;
				else
					return true;
			} else if (actionCode == null || actionCode == '' || actionCode == 'NA') {
				return false;
			} else {
				return true;
			}
		},

		isWithdrawAllowed: function(actionCode) {
			if (withdrawEnable === 'true') {
				if (actionCode == null || actionCode == '' || actionCode == 'NA')
					return false;
				else
					return true;
			} else if (actionCode == null || actionCode == '' || actionCode == 'NA') {
				return false;
			} else {
				return true;
			}
		},

		isValidCoordinates: function(latitude, longitude) {
			if (latitude != null && latitude != '' && latitude != 'NA' && longitude != null && longitude != '' && longitude != 'NA')
				return true;
			else
				return false;
		},

		reNewMap: function() {
			if (courseDirMap == null) {
				mobEdu.util.get('mobEdu.courses.view.coursesMap');
				courseDirMap = Ext.getCmp('courseDirectionMap');
			} else {
				var viewRef = mobEdu.util.get('mobEdu.courses.view.coursesMap');
				viewRef.remove(courseDirMap, true);
				courseDirMap = new Ext.Map({
					height: '100%',
					width: '100%',
					mapOptions: {
						zoom: 10,
						center: new google.maps.LatLng(mapInitLatitude, mapInitLongitude),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					},
					mapRendererOptions: {
						draggable: true,
						hideRouteList: true
					},
					listeners: {
						maprender: function(mapRef, map, eOpts) {
							mobEdu.courses.f.setDirection(mapRef);
						}
					}
				});
				viewRef.add(courseDirMap);
			}
		},

		setDirection: function() {
			var geoLocationOptions = {
				maximumAge: 3000,
				timeout: 5000,
				enableHighAccuracy: true
			};
			var marker, directionsRenderer, directionsService;

			navigator.geolocation.getCurrentPosition(geoLocationSuccess, geoLocationError, geoLocationOptions);

			function geoLocationSuccess(position) {

				directionsService = new google.maps.DirectionsService();
				directionsRenderer = new google.maps.DirectionsRenderer(courseDirMap.mapRendererOptions);
				directionsRenderer.setMap(courseDirMap.getMap());

				marker = new google.maps.Marker({
					map: courseDirMap.getMap(),
					visible: true
				});

				var start = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); //cor
				// set the request options

				var request = {
					origin: start,
					destination: destPosition,
					travelMode: google.maps.DirectionsTravelMode.DRIVING // DRIVING, WALKING, BICYCLING
				};
				directionsService.route(request, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						// Display the directions using Google's Directions Renderer.
						directionsRenderer.setPanel(document.getElementById('map-directions'));
						directionsRenderer.setDirections(result);
					} else {
						Ext.Msg.show({
							message: '<p>Error while getting direction.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox',
							fn: function(btn) {
								if (btn == 'ok') {
									(mobEdu.util.getPrevView())();
								}
							}
						});
					}
					Ext.Viewport.unmask();
				});
			}

			function geoLocationError() {
				Ext.Viewport.unmask();
				Ext.Msg.show({
					message: '<p>Error while getting geolocation.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							(mobEdu.util.getPrevView())();
						}
					}
				});
			}
		},

		isFacultyRole: function() {
			if (mobEdu.util.isDualRole() == true && mobEdu.util.getCurrentRole() == 'FACULTY') {
				return true;
			} else if (mobEdu.util.isDualRole() == false) {
				var resRole = mobEdu.util.getRole();
				if (resRole != null) {
					var role = '';
					var userRole = resRole.split(',');
					for (var len = 0; len < userRole.length; len++) {
						if (userRole[len] == 'FACULTY') {
							return true;
						}
					}
				}
			}
			return false;
		},
		getCurrentClass: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			store.clearFilter();
			store.removeAll();
			store.removed = [];
			var currDate = new Date();
			//            var currDate = new Date("08/29/2013 9:10:00");
			var store = mobEdu.util.getStore('mobEdu.courses.store.courseListLocal');
			var sData = store.data.all;
			for (var c = 0; c < sData.length; c++) {
				var statusType = sData[c].data.statusCode;
				var crn = sData[c].data.crn;
				var termCode = sData[c].data.termCode;
				var title = sData[c].data.courseTitle;

				// Only look for meetings for successfully registered courses.
				//get the meeting info
				var meetings = sData[c].data.meeting;
				for (var i = 0; i < meetings.length; i++) {
					var meetingStartDate = Ext.Date.parse(meetings[i].startDate, 'm-d-Y');
					var meetingEndDate = Ext.Date.parse(meetings[i].endDate, 'm-d-Y');

					if (meetingStartDate != undefined && meetingEndDate != undefined && Ext.Date.between(Ext.Date.clearTime(currDate, true), meetingStartDate, meetingEndDate)) {
						var checkDate = Ext.Date.clearTime(meetingStartDate, true);
						while (Ext.Date.between(Ext.Date.clearTime(checkDate, true), meetingStartDate, meetingEndDate)) {

							var dayOfWeek = Ext.Date.format(checkDate, "D");

							//Split the periods from list
							var period = meetings[i].period;
							var periodList = period.split(',');

							if (Ext.Array.contains(periodList, dayOfWeek)) {

								var meetingStartDateTime = Ext.Date.parse(Ext.Date.format(checkDate, "m-d-Y") + " " + meetings[i].beginTime, "m-d-Y h:iA");
								var meetingEndDateTime = Ext.Date.parse(Ext.Date.format(checkDate, "m-d-Y") + " " + meetings[i].endTime, "m-d-Y h:iA");

								if (meetingStartDateTime != undefined && meetingEndDateTime != undefined && Ext.Date.between(currDate, meetingStartDateTime, meetingEndDateTime)) {
									var bgTime = meetings[i].beginTime;
									var edTime = meetings[i].endTime;
									if (mobEdu.courses.f.isFacultyRole() == true) {
										var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
										store.add({
											'title': title,
											'startTime': bgTime,
											'endTime': edTime,
											'termCode': termCode,
											'crn': crn
										});
										store.sync();
									} else {
										var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
										store.add({
											'title': title,
											'startTime': bgTime,
											'endTime': edTime,
											'termCode': termCode,
											'crn': crn
										});
										store.sync();
									}
								}
							}
							checkDate = Ext.Date.add(checkDate, Ext.Date.DAY, 1);
						}
					}
				}
			}
			mobEdu.courses.f.checkCourses();
		},

		showCrsPopup: function() {
			var popup = mobEdu.util.get('mobEdu.courses.view.attCrsePopup');
			Ext.Viewport.add(popup);
		},

		checkCourses: function() {
			mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal').clearFilter();
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			var title, bgTime, edTime, termCode, crn, currDate;
			if (store.data.length == 0) {
				Ext.Msg.show({
					message: '<p>You don\'t have active session now.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else if (store.data.length == 1) {
				title = store.data.all[0].data.title;
				bgTime = store.data.all[0].data.startTime;
				edTime = store.data.all[0].data.endTime;
				termCode = store.data.all[0].data.termCode;
				crn = store.data.all[0].data.crn;
				currDate = new Date();
				if (mobEdu.courses.f.isFacultyRole() == true) {
					Ext.Msg.show({
						message: '<p>Do you want to generate code for<br/><br/><b>' + title + '( ' + crn + ' )' + '</b></p>',
						buttons: Ext.MessageBox.YESNO,
						cls: 'msgbox',
						fn: function(btn) {
							if (btn == 'yes') {
								mobEdu.courses.f.generateCode(title, bgTime, edTime, termCode, crn, currDate);
							}
						}
					});
				} else {
					Ext.Msg.show({
						message: '<p>Do you want to submit code for<br/><br/><b>' + title + '( ' + crn + ' )' + '</b></p>',
						buttons: Ext.MessageBox.YESNO,
						cls: 'msgbox',
						fn: function(btn) {
							if (btn == 'yes') {
								mobEdu.att.f.showSubmitCodePopup();
							}
						}
					});
				}
			} else {
				if (mobEdu.courses.f.isFacultyRole() == true) {
					mobEdu.util.get('mobEdu.courses.view.attCrsePopup');
					var coursePanel = Ext.getCmp('attCrsePopup');
					var courses = new Array();
					for (var i = 0; i < store.data.length; i++) {
						title = store.data.all[i].data.title;
						bgTime = store.data.all[i].data.startTime;
						edTime = store.data.all[i].data.endTime;
						termCode = store.data.all[i].data.termCode;
						crn = store.data.all[i].data.crn;
						currDate = new Date();
						var item = Ext.create('Ext.field.Radio', {
							label: '<p class="labelText">' + title + ' (' + crn + ') </p>',
							labelWidth: Ext.os.is.Phone ? '55%' : '85%',
							labelCls: 'rLabel',
							cls: 'rLabelBg',
							name: 'courses',
							docked: 'top',
							id: 'course' + i,
							checked: i == 0 ? true : false
						});
						courses.push(item);
					}
					coursePanel.setItems(courses);
					mobEdu.courses.f.showCrsPopup();
					Ext.getCmp('attCrseTop').setHtml('<p class="labelText" align="center">Do you want to generate code for </p>');
				} else {
					mobEdu.util.get('mobEdu.courses.view.attCrsePopup');
					var coursePanel = Ext.getCmp('attCrsePopup');
					var courses = new Array();
					for (var i = 0; i < store.data.length; i++) {
						title = store.data.all[i].data.title;
						bgTime = store.data.all[i].data.startTime;
						edTime = store.data.all[i].data.endTime;
						termCode = store.data.all[i].data.termCode;
						crn = store.data.all[i].data.crn;
						currDate = new Date();
						var item = Ext.create('Ext.field.Radio', {
							label: '<p class="labelText">' + title + ' (' + crn + ') </p>',
							labelWidth: Ext.os.is.Phone ? '55%' : '85%',
							labelCls: 'rLabel',
							cls: 'rLabelBg',
							name: 'courses',
							docked: 'top',
							id: 'course' + i,
							checked: i == 0 ? true : false
						});
						courses.push(item);
					}
					coursePanel.setItems(courses);
					mobEdu.courses.f.showCrsPopup();
					Ext.getCmp('attCrseTop').setHtml('<p class="labelText" align="center" >Do you want to submit code for </p>');
				}
			}
		},

		handleGenerateCode: function() {
			if (mobEdu.courses.f.isFacultyRole() == true) {
				mobEdu.courses.f.multigenerateCode();
			} else {
				mobEdu.att.f.showSubmitCodePopup();
			}
		},

		multigenerateCode: function() {
			var title, bgTime, edTime, termCode, crn, currDate;
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			mobEdu.util.get('mobEdu.courses.view.attCrsePopup');
			for (var i = 0; i < store.data.length; i++) {
				var check = Ext.getCmp('course' + i).isChecked();
				if (check) {
					title = store.data.all[i].data.title;
					bgTime = store.data.all[i].data.startTime;
					edTime = store.data.all[i].data.endTime;
					termCode = store.data.all[i].data.termCode;
					crn = store.data.all[i].data.crn;
					currDate = new Date();
				};
			};
			mobEdu.courses.f.generateCode(title, bgTime, edTime, termCode, crn, currDate);
		},

		generateCode: function(cTitle, sTime, eTime, termCode, crn, currAttDate) {
			var codeGenerateStore = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			codeGenerateStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getAuthString(),
				companyID: companyId
			});
			codeGenerateStore.getProxy().setUrl(webserver + 'generateCode');
			codeGenerateStore.getProxy().setExtraParams({
				instructorId: mobEdu.main.f.getStudentId(), //'T00001617',
				termCode: termCode, //'201380',
				crn: crn, //'82865',
				startTime: mobEdu.att.f.formatTimeToSave(sTime), //'1500',
				endTime: mobEdu.att.f.formatTimeToSave(eTime), //'1700',
				attDate: Ext.Date.format(currAttDate, 'm/d/Y'),
				companyId: companyId
			});
			codeGenerateStore.getProxy().afterRequest = function() {
				mobEdu.courses.f.generateCodeResponseHandler();
			};
			codeGenerateStore.load();
		},
		generateCodeResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.att.store.codeGenerate');
			var status = store.data.all[0].data.status;
			if (status == "SUCCESS") {
				var code = store.data.all[0].data.code;
				mobEdu.att.f.showCodeGenerate(code);
			} else {
				Ext.Msg.show({
					message: '<p>' + status + '.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		showGenerateCodePopup: function(popupView) {
			var store = mobEdu.util.getStore('mobEdu.att.store.mettingScheduleLocal');
			mobEdu.util.get('mobEdu.courses.view.attCrsePopup');
			var title;
			if (store.data.length > 1) {
				for (var i = 0; i < store.data.length; i++) {
					var check = Ext.getCmp('course' + i).isChecked();
					if (check) {
						title = store.data.all[i].data.title;
					};
				};
				store.filter(function(rec) {
					var title1 = rec.get('title');
					if (title == title1) {
						return true;
					}
					return false;
				});
			};
			mobEdu.courses.f.hideAttCoursePopup();
			mobEdu.att.f.prevView = mobEdu.util.getPrevView();
			mobEdu.util.updatePrevView(mobEdu.courses.f.hideGenerateCodePopup);
			if (!popupView) {
				var popupView = mobEdu.util.get('mobEdu.att.view.codeGenerator');
				Ext.Viewport.add(popupView);
			}
			mobEdu.util.onOrientationChange(Ext.Viewport.getOrientation(), popupView, mobEdu.att.view.codeGenerator);
			popupView.show();
		},
		hideGenerateCodePopup: function() {
			// var popUp = Ext.getCmp('codePopup');
			// popUp.hide();
			// Ext.Viewport.unmask();
			var popUp = Ext.getCmp('codePopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.att.view.codeGenerator');
			}
		},

		updateRole: function() {
			var selectedRole = Ext.getCmp('courseRole').getValue();
			if (mobEdu.util.isDualRole()) {
				mobEdu.util.updateCurrentRole(selectedRole);
			}
		},
		moodlePageLoad: function(crn, termCode) {
			var url = '';
			var moodleStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
			var moodleData = moodleStore.data.all;
			var format;
			for (var i = 0; i < moodleData.length; i++) {
				var code = moodleData[i].data.moduleCode;
				if (code === 'MOODLE') {
					var record = moodleData[i].data.feeds[0];
					format = record.format;
					url = record.link;
					if (format == 'CAS' && url != null && url != '') {
						url = url + crn + '.' + termCode;
						mobEdu.util.openURLWithCAS(url);
					} else {
						var authStringHeader = mobEdu.util.getAuthString();
						var encodeData = authStringHeader.split(' ');
						var basicEncodeData = encodeData[1];
						var deCred = Base64.decode(basicEncodeData);
						var decodeData = deCred.split(':');
						var userName = decodeData[0];
						var password = decodeData[1];
						url = 'moodle.html' + '?' + 'username=' + userName + '&password=' + password;
						if (url != null && url != '') {
							mobEdu.util.loadExternalUrl(url);
						}

					}
				}
			}
		},
		isMoodleExist: function() {
			if (isMoodleLinkExist == 'yes') {
				return true;
			} else {
				return false;
			}
		},
		isSyllabusLinkExist: function() {
			if (isSyllabusLinkExist == 'yes') {
				return true;
			} else {
				return false;
			}
		},
		islevelExist: function(value) {
			if (value == 'NA' || value == '' || value == null) {
				return false;
			} else {
				return true;
			}
		},
		isRoomCodeExist: function(value) {
			if (value == 'NA' || value == '' || value == null || campusCode == 'SMC') {
				return false;
			} else {
				return true;
			}
		},
		isRoomDescExist: function(value) {
			if (value == 'NA' || value == '' || value == null) {
				return false;
			} else {
				return true;
			}
		},
		onCourseDetailItemTap: function(selectedIndex, viewRef, record, item) {
			if (item.target.name == "coursemoodle") {
				mobEdu.courses.f.moodlePageLoad();
			} else if (item.target.name == "syllabus") {
				mobEdu.courses.f.showCourseSylabus(record.data.crn, record.data.termCode);
			}
		},
		showCourseSylabus: function(crn, termCode) {
			url = 'http://www.swmich.edu/webed/syllabi/' + termCode + '/' + termCode + '-' + crn + '-syllabus.pdf';
			if (url != null && url != '') {
				mobEdu.util.loadExternalUrl(url);
			}
		},
		isBuildingStringExist: function(bldgDesc) {
			if (bldgDesc.match(/Building/gi)) {
				return true;
			} else {
				return false;
			}
		},
		hideAttCoursePopup: function() {
			var popUp = Ext.getCmp('attCrsePopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.courses.view.attCrsePopup');
			}
		}
	}
});