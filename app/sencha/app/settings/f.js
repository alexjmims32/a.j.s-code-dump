Ext.define('mobEdu.settings.f', {
	requires: [
		'mobEdu.settings.store.menu',
		'mobEdu.settings.store.feedsLocal'
	],
	statics: {

		curParentID: null,
		curModuleID: null,
		curFeedID: null,

		initializeSettingsView: function() {
			var store = mobEdu.util.getStore('mobEdu.settings.store.menu');
			store.load();
			if (store.data.length > 0) {
				store.removeAll();
				store.sync();
				store.removed = [];
			}
			store.add({
				title: 'Preferences',
				action: mobEdu.settings.f.loadPreferences
			});
			store.sync();

			if (mobEdu.util.getStudentId() != null && mobEdu.util.getAuthString() != null) {
				store.add({
					title: 'Customize Favorites',
					action: mobEdu.settings.f.loadEditModuleView
				});
				store.sync();
			}

			if (mainMenuLayout !== 'GRID' && customizationEnabled === 'true') {
				store.add({
					title: 'Customize Main Menu',
					action: mobEdu.main.f.showEditModuleView
				});
				store.sync();
			}

			if (customizationEnabled === 'true') {
				store.add({
					title: 'Customize Feeds Menu',
					action: mobEdu.settings.f.loadFeeds
				});
				store.sync();
			}

			if (mobEdu.util.isProxyRegEnabled()) {
				store.add({
					title: 'Proxy Session',
					action: mobEdu.main.f.onProxyRegButtonTap
				});
				store.sync();
			}
			mobEdu.settings.f.showMenu();
		},
		loadPreferences: function() {
			mobEdu.settings.f.loadCampusCodes();
			mobEdu.settings.f.showPreferences();
			var defaultMapsView = mobEdu.util.getDefaultMapView();
			if (defaultMapsView != null) {
				Ext.getCmp('mapView').setValue(defaultMapsView);
			}
		},

		loadCampusCodes: function() {
			if (mobEdu.util.isShowDefaultCampus() == true) {
				mobEdu.util.get('mobEdu.settings.view.preferences');
				var campusStore = mobEdu.util.getStore('mobEdu.maps.store.location');
				campusStore.clearFilter();
				if (isMapsCampusCodeEnabled == 'true') {
					campusStore.getServerProxy().setExtraParams({
						campusCode: campusCode
					});
				}
				campusStore.loadServer(function() {
					mobEdu.settings.f.campusCodesResponseHandler();
				});
				mobEdu.util.gaTrackEvent('encore', 'maps');
			} else {
				mobEdu.util.get('mobEdu.settings.view.preferences');
				Ext.getCmp('defaultCampus').setHidden(true);
			}
		},

		campusCodesResponseHandler: function() {
			var defaultCampus = mobEdu.util.getDefaultCampus();
			var campusStore = mobEdu.util.getStore('mobEdu.maps.store.location');
			if (campusStore.data.length > 0) {
				var campusCodes = new Array();
				var campusTitle = new Array();
				var codes = new Array();
				var index = 0;
				campusStore.each(function(record) {
					if (campusCodes.length > 0) {
						if (!(Ext.Array.contains(campusCodes, record.data.campusCode))) {
							campusCodes[index] = record.data.campusCode;
							campusTitle[index] = record.data.title;
							index = index + 1;
						}
					} else {
						campusCodes[index] = record.data.campusCode;
						campusTitle[index] = record.data.title;
						index = index + 1;
					}

				});

				for (var cnt = 0; cnt < campusCodes.length; cnt++) {
					codes[cnt] = ({
						text: campusTitle[cnt],
						value: campusCodes[cnt]
					});
				}

				Ext.getCmp('defaultCampus').setOptions(codes);
				if (defaultCampus != null && defaultCampus != '') {
					Ext.getCmp('defaultCampus').setValue(defaultCampus);
				}
			}
		},

		savePreferences: function() {
			var defaultCampus = Ext.getCmp('defaultCampus').getValue();
			if (defaultCampus != null && defaultCampus != '') {
				mobEdu.util.updateDefaultCampus(defaultCampus);
			}

			var defaultMapView = Ext.getCmp('mapView').getValue();
			if (defaultMapView != null && defaultMapView != '') {
				mobEdu.util.updateDefaultMapView(defaultMapView);
			}

			var resetApp = Ext.getCmp('resetApp').getChecked();
			var clear = Ext.getCmp('clearCache').getChecked();
			if (resetApp == true) {
				Ext.Msg.show({
					cls: 'msgbox',
					title: null,
					message: 'This will reset all your settings and clear local cache.Please confirm to proceed.',
					buttons: Ext.MessageBox.OKCANCEL,
					fn: function(btn) {
						if (btn == 'ok') {
							localStorage.clear();
							if (typeof navigator.splashscreen != "undefined") {
								navigator.splashscreen.show();
							}
							window.open('main.html', '_self');
						}
					}
				});
			} else if (clear == true) {
				mobEdu.storeUtil.clearAll();
				window.open('main.html', '_self');
			} else {
				Ext.Msg.show({
					cls: 'msgbox',
					title: null,
					message: 'Saved preferences successfully.',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							if (showPreferencesInHelp == 'false') {
								mobEdu.util.showMainView();
							} else {
								mobEdu.help.f.showMenu();
							}
						}
					}
				});
			}
			if (mobEdu.util.isShowThemes() == true) {
				var theme = Ext.getCmp('themes').getValue();
				Ext.ux.Loader.load([
						'resources/css/' + theme + '.css'
					],
					function() { // callback when finished loading
						console.log('Loaded files successfully.');
					},
					this // scope
				);
			}
		},

		onFeedsItemTap: function(view, index, target, record, item, e, eOpts) {
			mobEdu.util.deselectSelectedItem(index, view);
			if (item.target.name == "on") {
				mobEdu.settings.f.feedsOn(record.data.feedId);
			} else if (item.target.name == "off") {
				mobEdu.settings.f.feedsOff(record.data.feedId);
			} else {
				mobEdu.feeds.f.loadFeedList(record, true);
			}
		},

		updateItemTpl: function(editMode) {
			if (editMode) {
				Ext.getCmp('settingsFeedsList').setItemTpl(new Ext.XTemplate('<table width="100%"><tr>' +
					'<td width="5%" align="left">',
					'<tpl if="mobEdu.util.isFeedIdExists(feedId)===false">',
					'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 name="on" />' +
					'<tpl else>',
					'<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 name="off" />',
					'</tpl>',
					'</td>',
					'<td width="100%" align="left"><h3>{name}</h3></td>',
					'<tpl if="mobEdu.settings.f.isMenuItem(type)===true">',
					'<td><img width="15px" height="15px" src="' + mobEdu.util.getResourcePath() + 'images/arrow.png" /></td>',
					'</tpl>',
					'</tr></table>'));
			} else {
				Ext.getCmp('settingsFeedsList').setItemTpl(new Ext.XTemplate('<table width="100%"><tr>' +
					'<td width="100%" align="left" ><h3>{name}</h3></td>' +
					'<td><div align="right" class="arrow" /></td>' +
					'</tr></table>'));
			}
		},

		loadFeeds: function() {
			mobEdu.feeds.f.editMode = true;
			mobEdu.util.get('mobEdu.settings.view.feeds');
			var store = mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal');
			if (store.data.length > 0) {
				store.removeAll();
				store.sync();
				store.removed = [];
			}

			var feedLocalStore = mobEdu.util.getStore('mobEdu.feeds.store.feedsMenus');
			var records = new Array();
			feedLocalStore.each(function(record) {
				records.push(record.data);
			});

			var record = new Array();
			var tmp = new Array();
			tmp.name = 'Feeds';
			tmp.feeds = records;
			record.data = tmp;

			mobEdu.feeds.f.loadFeedList(record, false);
			mobEdu.settings.f.showFeeds();
			mobEdu.settings.f.updateItemTpl(false);
		},

		feedsOn: function(feedId) {
			var cmp = Ext.getCmp('settingsFeedsList');
			mobEdu.util.addFeedToHiddenFeeds(feedId, cmp);
		},

		feedsOff: function(feedId) {
			var cmp = Ext.getCmp('settingsFeedsList');
			mobEdu.util.removeFeedFromHiddenFeeds(feedId, cmp);
		},

		showFeeds: function() {
			mobEdu.util.updatePrevView(mobEdu.feeds.f.onBackButtonTap);
			mobEdu.util.show('mobEdu.settings.view.feeds');
		},

		showPreferences: function() {
			// if (showPreferencesInHelp == 'false') {
			// 	mobEdu.util.updatePrevView(mobEdu.settings.f.showMenu);
			// } else {
			// 	mobEdu.util.updatePrevView(mobEdu.help.f.showMenu);
			// }
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.settings.view.preferences');
			if (mobEdu.util.isShowThemes() == false) {
				Ext.getCmp('themes').setHidden(true);
			}
		},
		showMenu: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.settings.view.menu');
			mobEdu.util.setTitle('SETTINGS', Ext.getCmp('settingsTitle'));
		},

		showProxySession: function() {
			mobEdu.util.updatePrevView(mobEdu.settings.f.showMenu);
			mobEdu.util.show('mobEdu.settings.view.proxySession');
		},

		isMenuItem: function(type) {
			if (type == 'MENU') {
				return true;
			}
			return false;
		},
		loadEditModuleView: function() {
			var store = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
			store.clearFilter();
			store.filterBy(function(record, id) {
				if (record.get("category") != '' && record.get("code") != 'SETTINGS') {
					return true;
				}
				return false;
			}, this);
			mobEdu.main.f.showEditModuleView();

		}
	}
});