Ext.define('mobEdu.settings.store.feedsLocal', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',
    
    requires: [
    'mobEdu.feeds.model.feedsMenus'
    ],

    config:{
        storeId: 'mobEdu.settings.store.feedsLocal',
        autoLoad: true,
        model: 'mobEdu.feeds.model.feedsMenus',
        proxy:{
            type:'localstorage',
            id:'feedsLocalStorage'
        }
    }
});