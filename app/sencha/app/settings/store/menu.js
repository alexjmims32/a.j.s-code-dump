Ext.define('mobEdu.settings.store.menu', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.settings.model.menu'
    ],

    config:{
        storeId: 'mobEdu.settings.store.menu',
        autoLoad: false,
        model: 'mobEdu.settings.model.menu',
        proxy:{
            type:'memory',
            id:'settingsStorage'
        }
    }
});