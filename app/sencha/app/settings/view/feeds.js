Ext.define('mobEdu.settings.view.feeds', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.settings.store.feedsLocal'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
                xtype: 'list',
                id: 'settingsFeedsList',
                itemTpl: new Ext.XTemplate(
                        '<div style="display: inline-block; background: transparent; padding-right: 5px"><div class="outerContainer"><div class="innerContainer">',
                        '<tpl if="mobEdu.util.isFeedIdExists(feedId)===false">',
                        '<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxon.png" width=24 name="on" />' +
                        '<tpl else>',
                        '<img src="' + mobEdu.util.getResourcePath() + 'images/checkboxoff.png" width=24 name="off" />',
                        '</tpl></div></div></div>',
                        '<div style="display: inline-block;"><div class="outerContainer"><div class="innerContainer"><h3>{name}</h3></div></div></div>',
                        '<tpl if="mobEdu.settings.f.isMenuItem(type)===true">',
                        '<div align="right" class="arrow" />',
                        '</tpl>'),
                store: mobEdu.util.getStore('mobEdu.feeds.store.feedsLocal'),
                singleSelect: true,
                scrollToTopOnRefresh: false,
                loadingText: '',
                listeners: {
                    itemtap: function(view, index, target, record, item, e, eOpts) {
                        mobEdu.settings.f.onFeedsItemTap(view, index, target, record, item, e, eOpts);
                    }
                }
            },
            {
                xtype: 'customToolbar',
                title: '<h1>Feeds</h1>',
                id: 'feedsTitle'
            }
        ],
        flex: 1
    }
});