Ext.define('mobEdu.settings.view.proxySession', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: {
            type: 'vbox'
        },
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Proxy Session</h1>'
        }, {
            xtype: 'container',
            items: [{
                xtype: 'label',
                padding: '5 0 0 5',
                html: '<h4><b>Enter UTAD Username</b></h4>'
            }, {
                xtype: 'textfield',
                name: 'txtProxyId',
                id: 'txtProxyId',
                required: true,
                useClearIcon: true,
                listeners: {
                    blur:function(textfield,e,eOpts){
                        mobEdu.util.hideKeyboard();
                    }
                }
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Submit',
                align: 'right',
                handler: function() {
                    mobEdu.main.f.onProxySubmitButtonTap();
                }
            }]
        }]
    }
});