Ext.define('mobEdu.settings.view.menu', {
    extend: 'Ext.Panel',
	requires:[
		'mobEdu.settings.store.menu'
	],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'settingsList',
            itemTpl: '<table class="menu" width="100%"><tr><td width="100%" align="left"><h3>{title}</h3></td><td width="10%"><div align="right" class="arrow" /> </td></tr></table>',
            store: mobEdu.util.getStore('mobEdu.settings.store.menu'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, item, e) {
                    setTimeout(function() {
                        view.deselect(index);
                    }, 500);
                    e.data.action();
                }
            }
        }, {
            xtype: 'customToolbar',
            id: 'settingsTitle'
        }],
        flex: 1
    }
});