Ext.define('mobEdu.settings.view.preferences', {
	extend: 'Ext.form.Panel',
	requires:[
		'mobEdu.maps.store.location'
	],
	config: {
		xtype: 'formpanel',
		draggable: false,
		cls: 'logo',
		items: [{
				xtype: 'toolbar',
				docked: 'bottom',
				layout: {
					pack: 'right'
				},
				items: [{
						text: 'Save',
						handler: function() {
							mobEdu.settings.f.savePreferences();
						}
					}]
			}, {
				xtype: 'fieldset',
				items: [{
						layout: 'hbox',
						items: [{
								xtype: 'label',
								width: '50%',
                                                                cls:'clearCacheText',
								html: 'Clear Cache' + clearCacheSubText,
								margin: '13 20 0 4',
								style: 'line-height: 0.5em;'
							}, {
								xtype: 'spacer'
							}, {
								layout: {
									type: 'vbox',
									pack: 'center',
									align: 'center'
								},
								items: [{
										xtype: 'checkboxfield',
										width: '90%',
										name: 'clearCache',
										id: 'clearCache'
									}
								]
							}]
					},{
						layout: 'hbox',
						items: [{
								xtype: 'label',
								width: '50%',
								html: 'Reset App',
                                                                cls:'clearCacheText',
								margin: '13 20 0 4',
								style: 'line-height: 0.5em;'
							}, {
								xtype: 'spacer'
							}, {
								layout: {
									type: 'vbox',
									pack: 'center',
									align: 'center'
								},
								items: [{
										xtype: 'checkboxfield',
										width: '90%',
										name: 'resetApp',
										id: 'resetApp'
									}
								]
							}]
					}, {
						xtype: 'selectfield',
						label: 'Default Campus',
                                                labelWidth:'50%',
						name: 'defaultCampus',
						id: 'defaultCampus'
					},{
						xtype: 'selectfield',
						label: 'Default Map View',
                                                labelWidth:'50%',
						name: 'mapView',
						id: 'mapView',
                                                options:[
                                                    { 'text':'Map', value:1 },
                                                    { 'text':'Buildings', value:2 }
                                                ]
                                        },{
						xtype: 'selectfield',
						label: 'Theme',
						name: 'themes',
                                                labelWidth:'50%',
						id: 'themes',
                                                options:[
                                                    { 'text':'Theme Grey', value:'theme1' },
                                                    { 'text':'Theme Yellow-Grey', value:'theme2' },
                                                    { 'text':'Theme Blue-White', value:'theme3' },
                                                    { 'text':'Theme Grey-White', value:'theme4' },
                                                    { 'text':'Theme Blue-Yellow', value:'theme5' },
                                                    { 'text':'Theme Red-Yellow', value:'theme6' },
                                                    { 'text':'Theme Grey-Yellow', value:'theme8' }
                                                ]
                                        }]
			}, {
				xtype: 'customToolbar',
				title: '<h1>Preferences</h1>'
			}],
		flex: 1
	}
});