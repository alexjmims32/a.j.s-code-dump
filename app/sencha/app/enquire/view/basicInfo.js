Ext.define('mobEdu.enquire.view.basicInfo', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'                        
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'toolbar',
                docked:'top',
                ui:'light',
                title:'<h1>Submit Profile</h1>',
                // Back Button
                items:[
                    {
                        xtype:'spacer'
                    },
                    {
                        ui:'light',
                        xtype:'button',cls:'home',
                        style:'background:none;border:none;',
                        iconMask:true,
                        handler:function () {
                            mobEdu.util.showMainView();
                        }
                    }

                ]
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadContactAddressInfo();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'Basic Information'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'First Name',
                        name:'firstname',
                        id:'firstname',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Last Name',
                        name:'lastname',
                        id:'lastname',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'selectfield',
                        labelWidth:'50%',
                        label:'Gender',
                        name:'gender',
                        id:'gender',
                        required:true,
                        useClearIcon:true,
                        options:[
                            {
                                text:'[Select]',
                                value:'select'
                            },

                            {
                                text:'Male',
                                value:'M'
                            },

                            {
                                text:'Female',
                                value:'F'
                            }
                        ]
                    },
                    {
                        xtype:'datepickerfield',
                        labelWidth:'50%',
                        label:'Date of Birth',
                        name:'dateofbirth',
                        id:'dateofbirth',
                        placeHolder:'[Select]',
                        picker:{
                            yearFrom:1960,
                            yearTo:new Date().getFullYear() + 1
//                            slotOrder:['day', 'month', 'year']
                        },
                        required:true
                    }
                ]
            }

        ]
    }
});
