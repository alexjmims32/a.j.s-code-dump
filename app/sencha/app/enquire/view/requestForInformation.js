Ext.define('mobEdu.enquire.view.requestForInformation', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            docked: 'top',
            ui: 'light',
            title: '<h1>Submit Profile</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Next',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.loadEnquireSubmitForm();
                }
            }]
        }, {
            xtype: 'fieldset',
            title: 'Request for Information'

        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'First Name',
                name: 'firstname',
                id: 'firstname',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Last Name',
                name: 'lastname',
                id: 'lastname',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'selectfield',
                labelWidth: '50%',
                label: 'Gender',
                name: 'gender',
                id: 'gender',
                required: true,
                useClearIcon: true,
                options: [{
                        text: '[Select]',
                        value: 'select'
                    },

                    {
                        text: 'Male',
                        value: 'M'
                    },

                    {
                        text: 'Female',
                        value: 'F'
                    }
                ]
            }, {
                xtype: 'datepickerfield',
                labelWidth: '50%',
                label: 'Date of Birth',
                name: 'dateofbirth',
                id: 'dateofbirth',
                placeHolder: '[Select]',
                picker: {
                    yearFrom: 1960,
                    yearTo: new Date().getFullYear()
                    //                            slotOrder:['day', 'month', 'year']
                },
                required: true
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Address1',
                name: 'address',
                id: 'address',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Address2',
                name: 'add2',
                id: 'add2',
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                name: 'city',
                id: 'city',
                required: true,
                useClearIcon: true,
                label: 'City'
            }, {
                xtype: 'searchfield',
                label: 'state',
                labelWidth: '50%',
                name: 'stList',
                id: 'stList',
                placeHolder: 'Enter State',
                required: true,
                clearIcon: true,
                listeners: {
                    focus: function() {
                        mobEdu.enquire.f.loadStatePopup();
                    },
                    clearicontap: function(textfield, e, eOpts) {
                        // In Android, clearing the field causes focus event to be raised
                        // So use a flag and do not show the popup, immediately after the clear icon is tapped
                        if (Ext.os.is.Android) {
                            mobEdu.enquire.view.state.clearIconTapped = true;
                        }
                    },
                }
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Zip',
                name: 'zip',
                id: 'zip',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        return mobEdu.enquire.f.onZipKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Country',
                name: 'country',
                id: 'country',
                required: true,
                useClearIcon: true,
                value: 'USA'
                //                        readOnly:true
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Phone',
                name: 'phone1',
                id: 'phone1',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.enquire.f.onPhone1Keyup(textfield);
                    }
                },
                maxLength: 13
            }, {
                xtype: 'emailfield',
                name: 'email',
                id: 'email',
                labelWidth: '50%',
                label: 'Email',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });

                    me.callParent(arguments);
                }
            }, {
                xtype: 'emailfield',
                name: 'email2',
                id: 'email2',
                labelWidth: '50%',
                label: 'Verify Email',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                    });

                    me.callParent(arguments);
                }

            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '100%',
                label: 'Create an user name <br/> and password',
                readOnly: true
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Username',
                autoCapitalize: false,
                id: 'userName',
                name: 'userName',
                placeHolder: 'Username should be atleast 4 characters',
                required: true,
                useClearIcon: true

            }, {
                xtype: 'passwordfield',
                name: 'password',
                id: 'password',
                labelWidth: '50%',
                label: 'Password',
                required: true,
                useClearIcon: true,
                maxLength: 12,
                mainLength: 4
            }, {
                xtype: 'passwordfield',
                name: 'vpassword',
                id: 'vpassword',
                labelWidth: '50%',
                required: true,
                label: 'Verify Password',
                useClearIcon: true,
                maxLength: 12,
                mainLength: 4
            }, {
                xtype: 'selectfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Classification',
                name: 'class',
                id: 'class',
                //                        required:true,
                useClearIcon: true,
                options: [{
                    text: 'Select',
                    value: 'select'
                }, {
                    text: 'Freshman',
                    value: 'Freshman'
                }, {
                    text: 'Sophomore',
                    value: 'Sophomore'
                }, {
                    text: 'Junior',
                    value: 'Junior'
                }, {
                    text: 'Senior',
                    value: 'Senior'
                }, {
                    text: 'Graduate',
                    value: 'Graduate'
                }]
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'High School',
                name: 'highS',
                id: 'highS',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'selectfield',
                labelWidth: '50%',
                label: 'Intended Major',
                name: 'intMaj',
                id: 'intMaj',
                required: true,
                useClearIcon: true,
                options: [{
                    text: 'Select',
                    value: 'select'
                }, {
                    text: 'Accounting',
                    value: 'Accounting'
                }, {
                    text: 'Administration And Supervision',
                    value: 'Administration And Supervision'
                }, {
                    text: 'Agricultural Science',
                    value: 'Agricultural Science'
                }, {
                    text: 'Anthropology',
                    value: 'Anthropology'
                }, {
                    text: 'Applied Science And Technology',
                    value: 'Applied Science And Technology'
                }, {
                    text: 'Biology',
                    value: 'Biology'
                }, {
                    text: 'Business Administration',
                    value: 'Business Administration'
                }, {
                    text: 'Computer Science',
                    value: 'Computer Science'
                }, {
                    text: 'criminal justice',
                    value: 'criminal justice'
                }, {
                    text: 'Economics',
                    value: 'Economics'
                }, {
                    text: 'Electrical Engineering',
                    value: 'Electrical Engineering'
                }, {
                    text: 'Speech Communications',
                    value: 'Speech Communications'
                }, {
                    text: 'Urban Forestry',
                    value: 'Urban Forestry'
                }, {
                    text: 'Vocational Education',
                    value: 'Vocational Education'
                }, {
                    text: 'others',
                    value: 'others'
                }],
                listeners: {
                    change: function(field, newValue) {
                        mobEdu.enquire.f.intendedChange(newValue);
                    }
                }
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Others',
                name: 'intendText',
                id: 'intendText',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'ACT/SAT Score',
                name: 'asScore',
                id: 'asScore',
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        mobEdu.enquire.f.onActScoreKeyup(textfield);
                    }
                },
                maxLength: 3
            }, {
                xtype: 'selectfield',
                labelAlign: 'top',
                label: 'Who is requesting the <br/> information?',
                name: 'reqInf',
                id: 'reqInf',
                useClearIcon: true,
                options: [{
                    text: 'Select',
                    value: 'select'
                }, {
                    text: 'Student',
                    value: 'Student'
                }, {
                    text: 'Parent/Guardian',
                    value: 'Parent/Guardian'
                }, {
                    text: 'Other',
                    value: 'Other'
                }]
            }, {
                xtype: 'selectfield',
                labelAlign: 'top',
                label: 'What type of information <br/> are you looking for?',
                name: 'inf',
                id: 'inf',
                //                        required:true,
                useClearIcon: true,
                options: [{
                    text: 'Select',
                    value: 'select'
                }, {
                    text: 'Financial Aid',
                    value: 'Financial Aid'
                }, {
                    text: 'Resident Life (Housing)',
                    value: 'Resident Life (Housing)'
                }, {
                    text: 'Scholarships',
                    value: 'Scholarships'
                }, {
                    text: 'Intended Major',
                    value: 'Intended Major'
                }, {
                    text: 'Other',
                    value: 'Other'
                }]
            }]
        }]
    }
});