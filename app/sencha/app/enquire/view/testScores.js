Ext.define('mobEdu.enquire.view.testScores', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.testScoresGrid'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'testScoresList',
            name: 'testScoresList',
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><th colspan="2"><h2>{testCode}</h2></th></tr>' +
                '<tr>' +
                '<td><h3>' +
                '<table width="100%">' +
                '<tr>' +
                '<td width="50%">{testDate}</td>' +
                '<td >{testScore}</td>' +
                '</tr>' +
                '</table></h3>' +
                '</td>' +
                '</td><td width="5%"  align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/dropcourse.png" width="32px" height="32px" name="testScoreDrop"/></td>' +
                '</tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.enquire.f.onTestScoresItemTap(index, view, record, item);
                }
            }
        }, {
            title: '<h1>Create Application</h1>',
            xtype: 'customToolbar'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Add',
                id: 'addButton',
                name: 'addButton',
                handler: function() {
                    mobEdu.enquire.f.showTestScoresPopup();
                }
            }, {
                xtype: 'spacer'
            }, {
                text: 'Previous',
                handler: function() {
                    mobEdu.enquire.f.showCreateAppInterests();
                }
            }, {
                text: 'Next',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.loadCreateAppTestScores();
                }
            }]
        }, {
            xtype: 'fieldset',
            title: 'Test Scores',
            docked: 'top'
        }],
        flex: 1
    }
});