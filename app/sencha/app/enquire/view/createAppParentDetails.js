Ext.define('mobEdu.enquire.view.createAppParentDetails', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f',
	'mobEdu.enquire.store.parentEnumerations'              
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Create Application</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Previous',
                        handler:function () {
                            mobEdu.enquire.f.showCreateAppContactInfo();
                        }
                    },
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadCreateParentDetails();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'Parent Details'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'selectfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Parent1 Relation',
                        name:'appPRel1',
                        id:'appPRel1',
                        useClearIcon:true,
                        store:mobEdu.util.getStore('mobEdu.enquire.store.parentEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'First Name',
                        name:'appPFName1',
                        id:'appPFName1',
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Last Name',
                        name:'appPLName1',
                        id:'appPLName1',
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Middle Name',
                        name:'appPMName1',
                        id:'appPMName1',
                        useClearIcon:true
                    },
                    {
                        xtype:'selectfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Parent2 Relation',
                        name:'appPRel2',
                        id:'appPRel2',
                        useClearIcon:true,
                        store:mobEdu.util.getStore('mobEdu.enquire.store.parentEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'First Name',
                        name:'appPFName2',
                        id:'appPFName2',
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Last Name',
                        name:'appPLName2',
                        id:'appPLName2',
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Middle Name',
                        name:'appPMName2',
                        id:'appPMName2',
                        useClearIcon:true
                    }

                ]
            }

        ]
    }
});

