Ext.define('mobEdu.enquire.view.contactPhoneInfo',{
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'                 
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Submit Profile</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Previous',
                        handler:function () {
                            mobEdu.enquire.f.showContactAddressInfo();
                        }
                    },
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadAdminssionsInfo();
                        }
                    }
                ]
            },
            {
                title:'Contact Information',
                xtype:'fieldset'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Phone',
                        name:'phone1',
                        id:'phone1',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                mobEdu.enquire.f.onPhone1Keyup(textfield);
                            }
                        },
                        maxLength:13
                    },
                    {
                        xtype:'emailfield',
                        name:'email',
                        id:'email',
                        labelWidth:'50%',
                        label:'Email',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                            });

                            me.callParent(arguments);
                        }
                    },
                    {
                        xtype:'emailfield',
                        name: 'email2',
                        id:'email2',
                        labelWidth:'50%',
                        label:'Verify Email',
                        required:true,
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                            });

                            me.callParent(arguments);
                        }

                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'100%',
                        readOnly:true,
                        label:'Create an user name and password'
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Username',
                        id:'userName',
                        name:'userName',
                        required:true,
                        useClearIcon:true

                    },
                    {
                        xtype:'passwordfield',
                        name:'password',
                        id:'password',
                        labelWidth:'50%',
                        label:'Password',
                        required:true,
                        useClearIcon:true,
                        maxLength:16 ,
                        mainLength:4
                    },
                    {
                        xtype:'passwordfield',
                        name: 'vpassword',
                        id:'vpassword',
                        labelWidth:'50%',
                        required:true,
                        label:'Verify Password',
                        useClearIcon:true,
                        maxLength:16,
                        mainLength:4
                    }
                ]
            }
        ]
    }
});
