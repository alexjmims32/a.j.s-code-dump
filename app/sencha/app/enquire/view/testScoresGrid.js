Ext.define('mobEdu.enquire.view.testScoresGrid', {
    extend : 'Ext.ux.touch.grid.View',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.testScoresGrid'     
    ],
    xtype  : 'testScoreGrid',
    config : {
        title    : 'Grid',
        store    : true,
        columns  : [
            {
                header    : 'Test Code',
                dataIndex : 'testCode',
                width     : '35%',
                editor    : {
                    xtype  : 'textfield'
                }
            },{
                header    : 'Test Date',
                dataIndex : 'testDate',
                width     : '35%',
                editor    : {
                    xtype  : 'textfield'
                }
            },
            {
                header    : 'Test Score',
                dataIndex : 'testScore',
                width     : '30%',
                editor    : {
                    xtype  : 'textfield'
                }
            }
        ],
        features : [
            {
                ftype    : 'Ext.ux.touch.grid.feature.Sorter',
                launchFn : 'initialize'
            },
            {
                ftype    : 'Ext.ux.touch.grid.feature.Editable',
                launchFn : 'initialize'
            }
        ]
    },

    applyStore : function() {
        return mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid');
    }
});
