Ext.define('mobEdu.enquire.view.updateAppointment', {
	extend: 'Ext.form.FormPanel',
	requires: [
		'mobEdu.enquire.f',
		'mobEdu.enquire.store.enumerations'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		cls: 'logo',
		xtype: 'toolbar',
		items: [{
				xtype: 'customToolbar',
				title: '<h1>Appointment</h1>',
				name: 'upAppTitle',
				id: 'upAppTitle'
			},
			//            {
			//                xtype:'toolbar',
			//                docked:'top',
			//                title:'New Appointment',
			//                style:'font-size:09pt; background:#1e90ff'
			//            },
			{
				xtype: 'fieldset',
				items: [{
						xtype: 'textfield',
						label: 'Subject',
						labelWidth: '40%',
						//                        value:'user@n2n.com',
						name: 'upsub',
						id: 'upsub',
						required: true,
						//                        readOnly:true,
						useClearIcon: true
					},

					{
						xtype: 'datepickerfield',
						name: 'update',
						//                        labelAlign:'top',
						id: 'update',
						labelWidth: '40%',
						label: 'Appointment Date',
						required: true,
						useClearIcon: true,
						value: new Date(),
						picker: {
							yearFrom: new Date().getFullYear(),
							yearTo: new Date().getFullYear() + 1
						},
						// listeners: {
						// 	change: function(datefield, e, eOpts) {
						// 		mobEdu.enquire.f.onUpAppDateKeyup(datefield)
						// 	}
						// }

					}, {
						xtype: 'spinnerfield',
						label: 'Hours',
						labelWidth: '40%',
						id: 'upTimeHours',
						name: 'upTimeHours',
						required: true,
						useClearIcon: true,
						minValue: 8,
						maxValue: 18,
						stepValue: 1,
						increment: 1,
						cycle: true
					}, {
						xtype: 'spinnerfield',
						label: 'Minutes',
						labelWidth: '40%',
						id: 'upTimeMinutes',
						name: 'upTimeMinutes',
						//                        required:true,
						useClearIcon: true,
						minValue: 0,
						maxValue: 59,
						stepValue: 5,
						increment: 5,
						cycle: true
					}, {
						xtype: 'textfield',
						label: 'location',
						labelWidth: '40%',
						//                        labelAlign:'top',
						name: 'uploc',
						id: 'uploc',
						required: true,
						useClearIcon: true
					}, {
						xtype: 'selectfield',
						label: 'Status',
						labelWidth: '40%',
						name: 'upsta',
						id: 'upsta',
						required: true,
						useClearIcon: true,
						store: mobEdu.util.getStore('mobEdu.enquire.store.enumerations'),
						displayField: 'displayValue',
						valueField: 'enumValue'
					}, {
						xtype: 'textareafield',
						label: 'Description',
						labelWidth: '40%',
						labelAlign: 'top',
						//                                value:'recruiter@n2n.edu',
						name: 'updes',
						id: 'updes',
						required: true,
						useClearIcon: true,
						maxRows: 10
					}

				]
			}, {
				xtype: 'toolbar',
				docked: 'bottom',
				layout: {
					pack: 'right'
				},
				items: [{
					text: 'Save',
					align: 'right',
					handler: function() {
						mobEdu.enquire.f.updateAppointmentSend();
					}
				}]
			}
		]
	}
});