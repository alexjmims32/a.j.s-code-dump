Ext.define('mobEdu.enquire.view.appState', {
	extend: 'Ext.Panel',
	statics: {
		clearIconTapped: false
	},
	config: {
		id: 'stateAppPopup',
		scrollable: 'none',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '70%',
		height: '50%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'textfield',
			name: 'stateAppDesc',
			id: 'stateAppDesc',
			placeHolder: 'Enter State',
			docked: 'top',
			// clearIcon: true,
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enquire.f.onStateKeyup(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enquire.f.onStateItemClear(textfield, e);
				},
				blur: function(textfield, e, eOpts) {
					mobEdu.util.hideKeyboard();
				}
			}
		}, {
			xtype: 'dataview',
			id: 'stateAppList',
			name: 'stateList',
			cls: 'logo subjectlist',
			itemTpl: '<h3>{displayValue}</h3>',
			store: mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.enquire.f.onAppStateItemTap(view, index, target, record, e, eOpts);
				}
			}
		}],
		plugins: [new Ext.ux.PanelAction({
			iconClass: 'x-panel-action-icon-close',
			position: 'tr'
		})],
		flex: 1,
		listeners: {
			hide: function() {
				var value = Ext.getCmp('stAppList').getValue();
				if (value == null || value == '') {
					Ext.getCmp('stAppList').setValue('select');
				}
				Ext.getCmp('stateAppDesc').setValue('');
				// If the search did not result any data
				// Clear the filters for next time
				mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations').clearFilter();
			}
		}
	}
});