Ext.define('mobEdu.enquire.view.subrMenu', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enquire.f',
		// 'mobEdu.enquire.notif.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
				xtype: 'list',
				cls: 'logo',
				name: 'inqSubrMenuList',
				id: 'inqSubrMenuList',
				itemTpl: '<table class="menu" width="100%"><tr><td <p>{title}</p></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
				store: mobEdu.util.getStore('mobEdu.enquire.store.subrMenu'),
				singleSelect: true,
				loadingText: '',
				listeners: {
					itemtap: function(view, index, item, e) {
						setTimeout(function() {
							view.deselect(index);
						}, 500);
						e.data.action();
					}
				}
			}, {
				xtype: 'customToolbar',
				title: '<h1>' + clientTitle + '</h1>',
				docked: 'top'
			}
			// {
			// 	title: '<h1>' + clientTitle + '</h1>',
			// 	xtype: 'toolbar',
			// 	ui: 'light',
			// 	docked: 'top',
			// 	id: 'aboutSubr',
			// 	items: [{
			// 		xtype: 'button',
			// 		cls: 'home',
			// 		id: 'customHome',
			// 		height: 32,
			// 		width: 32,
			// 		style: 'background:none;border:none;',
			// 		iconMask: true,
			// 		handler: function() {
			// 			mobEdu.enquire.f.showLeadMainView();
			// 		}
			// 	}, {
			// 		xtype: 'button',
			// 		cls: 'back',
			// 		height: 32,
			// 		width: 32,
			// 		iconMask: true,
			// 		padding: '10 0 0 0',
			// 		style: 'background:none;border:none;',
			// 		handler: function() {
			// 			(mobEdu.util.getPrevView())();
			// 		}
			// 	}]
			// }
		],
		flex: 1
	},
	initialize: function() {
		if (campusCode == 'SUBR') {
			mobEdu.enquire.f.initializeSubrMenu();
		}
		if (campusCode == 'N2N') {
			mobEdu.enquire.f.initializeN2nMenu();
		}
		if (campusCode == 'UNG') {
			mobEdu.enquire.f.initializeUNGMenu();
		}
		if (campusCode == 'FVSU') {
			mobEdu.enquire.f.initializeFVSUMenu();
		}
		if (campusCode == 'WSSU') {
			mobEdu.enquire.f.initializeWSSUMenu();
		}
		if (campusCode == 'SCF') {
			mobEdu.enquire.f.initializeSCFMenu();
		}
		if (campusCode == 'OC') {
			mobEdu.enquire.f.initializeOCMenu();
		}
		if (campusCode == 'RSV') {
			mobEdu.enquire.f.initializeRSVMenu();
		}
		if (campusCode == 'BBE') {
			mobEdu.enquire.f.initializeBBEMenu();
		}
		if (campusCode == 'UNCFSU') {
			mobEdu.enquire.f.initializeUNCFSUMenu();
		}
		if (campusCode == 'LCC') {
			mobEdu.enquire.f.initializeLCCMenu();
		}
		if (campusCode == 'CSCC') {
			mobEdu.enquire.f.initializeCSCCMenu();
		}


	}

});