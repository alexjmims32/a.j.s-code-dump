Ext.define('mobEdu.enquire.view.createAppBasicInfo', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f',                
        'mobEdu.enquire.store.raceEnumerations'               
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Create Application</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadCreateBasicInfo();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'Basic Information'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'First Name',
                        name:'appFName',
                        id:'appFName',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Last Name',
                        name:'appLName',
                        id:'appLName',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'selectfield',
                        labelWidth:'50%',
                        label:'Gender',
                        name:'appGender',
                        id:'appGender',
                        required:true,
                        useClearIcon:true,
                        options:[
                            {
                                text:'Select',
                                value:'select'
                            },

                            {
                                text:'Male',
                                value:'M'
                            },

                            {
                                text:'Female',
                                value:'F'
                            }
                        ]
                    },
                    {
                        xtype:'datepickerfield',
                        labelWidth:'50%',
                        label:'Date of Birth',
                        name:'appDateofbirth',
                        id:'appDateofbirth',
                        placeHolder:'Select',
                        picker:{
                            yearFrom:1960,
                            yearTo:new Date().getFullYear() + 1
//                            slotOrder:['month', 'day', 'year']
                        },
                        required:true
                    },
                    {
                        xtype:'selectfield',
                        labelWidth:'50%',
                        label:'Race',
                        name:'appRace',
                        id:'appRace',
                        useClearIcon:true,
                        store:mobEdu.util.getStore('mobEdu.enquire.store.raceEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
//                        value:'select'
//                        options:[
//                            {
//                                text:'Select',
//                                value:'select'
//                            },
//
//                            {
//                                text:'Black or African American',
//                                value:'2'
//                            },
//
//                            {
//                                text:'American Indian',
//                                value:'3'
//                            },
//
//                            {
//                                text:'Asian',
//                                value:'4'
//                            },
//
//                            {
//                                text:'White',
//                                value:'6'
//                            },
//
//                            {
//                                text:'Native Hawaiian',
//                                value:'7'
//                            },
//
//                            {
//                                text:'Unknown',
//                                value:'8'
//                            }
//                        ]
                    },
//                    {
//                        xtype:'selectfield',
//                        labelWidth:'50%',
//                        label:'Ethnicity',
//                        name:'appECity',
//                        id:'appECity',
//                        useClearIcon:true,
//                        store:mobEdu.util.getStore('mobEdu.enquire.store.ethnicityEnumerations'),
//                        displayField:'displayValue',
//                        valueField:'enumValue'
//                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'SSN',
                        name:'appssn',
                        id:'appssn',
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                return mobEdu.enquire.f.onSSNKeyUp(textfield);
                            }
                        },
                        maxLength:9
                    }
                ]
            }

        ]
    }
});

