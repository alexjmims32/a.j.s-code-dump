Ext.define('mobEdu.enquire.view.whyEnroll', {
    extend:'Ext.Panel',
    requires: [
        'mobEdu.enquire.f'     
    ],
    config:{
        scrollable:'vertical',
        padding:'10 10',
        fullscreen:true,
        layout:'fit',
        cls:'logo',

        html:[
        '<table><tr><td>The mission of Southern University and A&M College, an Historically Black,1890 land-grant institution, is to provide opportunities for a diverse student population to achieve a high-quality, global educational experience, to engage in scholarly, research, and creative activities, and to give meaningful public service to the community, the state, the nation, and the world so that Southern University graduates are competent, informed, and productive citizens.</td></tr></table>'
        ],
        items:[
        {
            xtype:'toolbar',
            docked:'top',
            title:'Why SUBR?',
            ui:'light',
            // Back Button
            items:[
            {
                xtype:'button',
                cls:'back',
                style:'background:none;border:none;',
                handler:function () {
                    mobEdu.enquire.f.showSubMenu();
                }
            },                
            {
                xtype:'spacer'
            },
// Home Button
      {
                ui:'light',
                xtype:'button',cls:'home',
                iconMask:true,
                handler:function () {
                    showMainView();
                }
            }
            ]

        }
        ]
    }
      
});
