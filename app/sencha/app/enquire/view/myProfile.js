Ext.define('mobEdu.enquire.view.myProfile', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'            
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                // title:'<h1>Profile Summary</h1>'
                id:'profDetail',

            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                      text:'Create Application',
                        id:'createApp',
                        name:'createApp',
                        handler:function(){
                           mobEdu.enquire.f.checkLocalDataBasicInfo();
                        }
                    },
                    {
                        text:'View Application',
                        id:'viewApp',
                        name:'viewApp',
                        handler:function(){
                            mobEdu.enquire.f.getViewApplication();
                        }
                    },
                    {
                      xtype:'spacer'
                    },
                    {
                        text:'Update',
                        handler:function () {
                            mobEdu.enquire.f.showUpdateProfile();
                        }
                    },{
//                        ui: 'light',
//                        cls:'changePassword',
//                        style: 'background:none;border:none;',
//                        iconMask: true,
                        text:'Reset Password',
                        handler:function () {
                            mobEdu.enquire.f.showResetPassword();
                        }
                    }
                ]
            },
            {
                xtype:'panel',
                name:'leadProfileSummary',
                id:'leadProfileSummary'
            }

        ]
    }

});
