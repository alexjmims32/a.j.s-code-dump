Ext.define('mobEdu.enquire.view.subMenu', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enquire.f'
		// 'mobEdu.enquire.notif.f'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'list',
			cls: 'logo',
			name: 'inqSubMenuList',
			id: 'inqSubMenuList',
			itemTpl: '<table class="menu" width="100%"><tr><td width="5%" align="left"><img width=57px src="{img}"></td><td width="85%" align="left"><p>{title}</p></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
			store: mobEdu.util.getStore('mobEdu.enquire.store.subMenu'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, item, e) {
					setTimeout(function() {
						view.deselect(index);
					}, 500);
					e.data.action();
				}
			}
		}, {
			title: '<h1>' + campusCode + ' Admissions</h1>',
			xtype: 'customToolbar'
		}],
		flex: 1
	},
	initialize: function() {
		mobEdu.enquire.f.initializeSubMenu();
	}

});