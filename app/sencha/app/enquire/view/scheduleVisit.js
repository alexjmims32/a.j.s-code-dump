Ext.define('mobEdu.enquire.view.scheduleVisit',{
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
        {
            xtype:'customToolbar',
            title:'<h1>Schedule Visit</h1>'
        },
        {
            xtype:'panel',  
            html:['<p>'
            +'There\'s no better way to get to know Southern University than to visit.'
            +' By meeting with one of our admission counselors, you\'ll have the opportunity'
            +' to tour the campus and find out everything you need to know about the application process,'
            +' financial aid and scholarships, and which programs best suit your career goals.'
            +'</p>'
            ]
        },
        {
            xtype:'label',
            title:'',
            html:['<p><br />To schedule a visit, fill out the form below</p>']
        },
        {
            xtype:'fieldset',
            items:[
            {
                xtype:'textfield',
                label:'Full Name',
                labelWidth:'50%',
                name:'fullName',
                id:'fullName',
                required:true,
                useClearIcon:true                    
            },
            {
                xtype:'numberfield',
                label:'Number',
                labelWidth:'50%',
                name:'contactNo',
                id:'contactNo',
                required:true,
                useClearIcon:true,
                listeners: {
                    keyup: function( textfield, e, eOpts ) {
                        return mobEdu.enquire.f.onContctNoKeyup(textfield);
                    }
                },
                maxLength:10
            },
            {
                xtype:'datepickerfield',
                labelWidth:'50%',
                label:'Visit Date',
                name:'visitDate',
                id:'visitDate',
                //                    placeHolder:'DAY/MM/YYYY',
                value:new Date(),
                picker:{
                    yearFrom:2012, 
                    yearTo:2015,
                    slotOrder:['day', 'month', 'year']
                },
                required:true,
                useClearIcon:true
            },
            {
                xtype:'selectfield',
                label:'Recruiter',
                labelWidth:'50%',
                name:'recruiter',
                id:'recruiter',
                required:true,
                useClearIcon:true,                    
                options:[
                {
                    text:'Samuel David', 
                    value:'Samuel David'
                },
                {
                    text:'Killian Jarell', 
                    value:'Killian Jarell'
                },
                {
                    text:'Denisha Green', 
                    value:'Denisha Green'
                },
                {
                    text:'Freddernique King', 
                    value:'Freddernique King'
                },
                {
                    text:'Rafael Russ', 
                    value:'Rafael Russ'
                },
                {
                    text:'Javonte Markelle', 
                    value:'Javonte Markelle'
                },
                {
                    text:'Lauron Nicolle', 
                    value:'Lauron Nicolle'
                },
                {
                    text:'Taylor Kitts', 
                    value:'Taylor Kitts'
                },
                {
                    text:'Caitlin Hampton', 
                    value:'Caitlin Hampton'
                },
                {
                    text:'Elizabeth Chaney', 
                    value:'Elizabeth Chaney'
                }
                ]
            },
            {
                xtype:'textareafield',
                label:'Reason for Visit',
                labelAlign:'top',
                labelWidth:'50%',
                maxRows:4,
                name:'vReason',
                id:'vReason',
                required:true,
                useClearIcon:true                    
            }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            style:'font-size:12pt',
            layout:{
                pack:'right'
            },
            items:[
            {
                text:'Submit',
                ui:'confirm',
                align:'right',
                handler:function () {
                    mobEdu.enquire.f.scheduleVisitSubmit();
                }
            }
            ]
        }
        ]
    }
});

