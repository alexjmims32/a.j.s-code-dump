Ext.define('mobEdu.enquire.view.otherInfo',{
    extend:'Ext.form.FormPanel',
//    alias:'inquiryOtherInfoView',
//    xtype:'inquiryOtherInfoView',
    requires: [
        'mobEdu.enquire.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
         {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>Submit Profile</h1>',
            // Back Button
            items:[
                {
                    xtype:'button',
                cls:'back',
                style:'background:none;border:none;',
                    handler:function () {
                          mobEdu.enquire.f.showEnrollInfo();
                    }
                },
                // Home Button
                { xtype:'spacer' },
                {
                    ui:'light',
                    xtype:'button',cls:'home',
                    iconMask:true,
                    handler:function () {
//                        showMainView();
                        mobEdu.enquire.f.showBasicInfo();
                    }
                }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            style:'font-size:12pt',
            layout:{ pack:'right' },
            items:[
                {
                    text:'Previous',
                    handler:function () {
//                          mobEdu.inq.f.showEnrollInfo();
                        mobEdu.enquire.f.loadFinalSummary();
                    }
                },
                {
                    text:'Submit',
                    ui:'confirm',
                    align:'right',
                    handler:function () {
                        mobEdu.enquire.f.inquirySubmit();
                    }
                }
            ]
        },
        {
            xtype:'fieldset',
            title:'Other Information'
        },
        {
            xtype:'fieldset',
//            title:'<div>What is your level of interest in N2N State University?</div>',
            items:[
                {
                    xtype:'selectfield',
                    labelAlign:'top',
                    label:'What is your level of interest in<br>Southern University?',
                    labelWrap:true,
                    placeHolder:'None',
                    name:'level',
                    id:'level',
                    options:[
                        {text:'Select', value:'None'},
                        {text:'Definitely plan to attend', value:'Definitelyplantoattend'},
                        {text:'Very interested', value:'Veryinterested'},
                        {text:'Somewhat interested', value:'Somewhatinterested'},
                        {text:'Just curious', value:'Justcurious'}
                    ]
                },
                {
                    xtype:'selectfield',
                    labelAlign:'top',
                    label:'What is the most important factor<br>in choosing a university?',
                    labelWrap:true,
                    placeHolder:'None',
                    name:'factor',
                    id:'factor',
                    options:[
                        {text:'Select', value:'None'},
                        {text:'Academic Program', value:'AcademicProgram'},
                        {text:'Academic Reputation', value:'AcademicReputation'},
                        {text:'Appearance of Campus', value:'AppearanceofCampus'},
                        {text:'Athletic Programs', value:'AthleticPrograms'},
                        {text:'Athletic Reputation', value:'AthleticReputation'},
                        {text:'Campus Life', value:'CampusLife'},
                        {text:'Class Size', value:'ClassSize'},
                        {text:'Cost', value:'Cost'},
                        {text:'Diversity', value:'Diversity'},
                        {text:'Faculty', value:'Faculty'},
                        {text:'Financial Aid', value:'FinancialAid'},
                        {text:'Location', value:'Location'},
                        {text:'Other', value:'Other'},
                        {text:'Parent/Family Alumni', value:'ParentFamilyAlumni'},
                        {text:'Rather Not Say', value:'RatherNotSay'},
                        {text:'Religious Affiliation', value:'ReligiousAffiliation'},
                        {text:'Residence Halls', value:'ResidenceHalls'},
                        {text:'Safety Concerns', value:'SafetyConcerns'},
                        {text:'Scholarships', value:'Scholarships'},
                        {text:'Size', value:'Size'},
                        {text:'Student to Faculty Ratio', value:'StudenttoFacultyRatio'},
                        {text:'Transferability of Courses', value:'TransferabilityofCourses'}
                    ]
                }
            ]
        }
//        {
//            xtype:'fieldset',
//            title:'<div>What is the most important factor in choosing a university?</div>',
//            items:[
//
//                {
//                    xtype:'selectfield',
//                    name:'factor',
//                    id:'factor',
//                    options:[
////                        {text:'', value:''},
//                        {text:'Academic Program', value:'AcademicProgram'},
//                        {text:'Academic Reputation', value:'AcademicReputation'},
//                        {text:'Appearance of Campus', value:'AppearanceofCampus'},
//                        {text:'Athletic Programs', value:'AthleticPrograms'},
//                        {text:'Athletic Reputation', value:'AthleticReputation'},
//                        {text:'Campus Life', value:'CampusLife'},
//                        {text:'Class Size', value:'ClassSize'},
//                        {text:'Cost', value:'Cost'},
//                        {text:'Diversity', value:'Diversity'},
//                        {text:'Faculty', value:'Faculty'},
//                        {text:'Financial Aid', value:'FinancialAid'},
//                        {text:'Location', value:'Location'},
//                        {text:'Other', value:'Other'},
//                        {text:'Parent/Family Alumni', value:'ParentFamilyAlumni'},
//                        {text:'Rather Not Say', value:'RatherNotSay'},
//                        {text:'Religious Affiliation', value:'ReligiousAffiliation'},
//                        {text:'Residence Halls', value:'ResidenceHalls'},
//                        {text:'Safety Concerns', value:'SafetyConcerns'},
//                        {text:'Scholarships', value:'Scholarships'},
//                        {text:'Size', value:'Size'},
//                        {text:'Student to Faculty Ratio', value:'StudenttoFacultyRatio'},
//                        {text:'Transferability of Courses', value:'TransferabilityofCourses'}
//                    ]
//                }
//            ]}
        ]
    }
       
    
});
