Ext.define('mobEdu.enquire.view.viewApplication', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'     
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>View Application</h1>'
            },           
            {
                xtype:'panel',
                name:'viewApp',
                id:'viewApp'
            }
//            {
//                xtype:'toolbar',
//                docked:'bottom',
//                layout:{
//                    pack:'right'
//                },
//                items:[
//                    {
//                        xtype:'spacer'
//                    },
//                    {
//                        text:'Push To Banner',
//                        id:'pushApp',
//                        name:'pushApp',
//                        handler:function () {
//                            mobEdu.enquire.f.loadPushApplication();
//                        }
//                    }
//                ]
//            }

        ]
    }

});
