Ext.define('mobEdu.enquire.view.createAppInterests',{
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f', 
	'mobEdu.enquire.store.pInterestEnumerations'               
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Create Application</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{ pack:'right' },
                items:[
                    {
                        text:'Previous',
                        handler:function () {
                            mobEdu.enquire.f.showCreateAdmissionDetails();
                        }
                    },
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadCreateAppInterests();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'Interests'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'selectfield',
                        labelAlign:'top',
                        label:'Primary Academic Area of Interest',
                        labelWrap:true,
                        name:'appPrimary',
                        id:'appPrimary',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.pInterestEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
//                        options:[
//                            {text:'Select', value:'select'},
//                            {text:'Accounting', value:'Accounting'},
//                            {text:'Art Education', value:'ArtEducation'},
//                            {text:'Biology Education', value:'BiologyEducation'},
//                            {text:'Chemistry Education', value:'ChemistryEducation'},
//                            {text:'Communication Education', value:'CommunicationEducation'},
//                            {text:'Economics', value:'Economics'},
//                            {text:'English Education', value:'EnglishEducation'},
//                            {text:'Exercise Science', value:'ExerciseScience'},
//                            {text:'French Education', value:'FrenchEducation'},
//                            {text:'International Business', value:'InternationalBusiness'},
//                            {text:'Leadership Studies (minor)', value:'LeadershipStudies(minor)'},
//                            {text:'Spanish Education', value:'SpanishEducation'}
//                        ]
                    },
                    {
                        xtype:'selectfield',
                        labelAlign:'top',
                        label:'Secondary Academic Area of Interest',
                        labelWrap:true,
                        name:'appSecondary',
                        id:'appSecondary',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.pInterestEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
//                        options:[
//                            {text:'Select', value:'select'},
//                            {text:'Accounting', value:'Accounting'},
//                            {text:'Art Education', value:'ArtEducation'},
//                            {text:'Biology Education', value:'BiologyEducation'},
//                            {text:'Chemistry Education', value:'ChemistryEducation'},
//                            {text:'Communication Education', value:'CommunicationEducation'},
//                            {text:'Economics', value:'Economics'},
//                            {text:'English Education', value:'EnglishEducation'},
//                            {text:'Exercise Science', value:'ExerciseScience'},
//                            {text:'French Education', value:'FrenchEducation'},
//                            {text:'International Business', value:'InternationalBusiness'},
//                            {text:'Leadership Studies (minor)', value:'LeadershipStudies(minor)'},
//                            {text:'Spanish Education', value:'SpanishEducation'}
//                        ]
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'top',
                        label:'Level of Interest',
                        labelWrap:true,
                        name:'appLevel',
                        id:'appLevel'
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'top',
                        label:'Factor for Choosing',
                        labelWrap:true,
                        name:'appFactor',
                        id:'appFactor'
                    },
                    {
                        xtype:'selectfield',
                        labelAlign:'top',
                        label:'Other Interests',
                        labelWrap:true,
                        name:'appOtherI',
                        id:'appOtherI',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.pInterestEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    }
                ]}
        ]
    }
});
