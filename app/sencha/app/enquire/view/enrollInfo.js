Ext.define('mobEdu.enquire.view.enrollInfo',{
   extend:'Ext.form.FormPanel',
//   alias:'inquiryEnrollInfoView',
//   xtype:'inquiryEnrollInfoView',
    requires: [
        'mobEdu.enquire.f'	            
    ],
    config:{
       scroll:'vertical',
       fullscreen:true,
       cls:'logo',
       xtype:'toolbar',
       items:[
         {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>Submit Profile</h1>',
            // Back Button
            items:[
                {
                    xtype:'button',
                cls:'back',
                style:'background:none;border:none;',
                    handler:function () {
                          mobEdu.enquire.f.showTestScores();
                    }
                },
                // Home Button
                { xtype:'spacer' },
                {
                    ui:'light',
                    xtype:'button',cls:'home',
                    iconMask:true,
                    handler:function () {
                          showMainView();
                    }
                }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            style:'font-size:12pt',
            layout:{ pack:'right' },
            items:[
                {
                    text:'Previous',
                    handler:function () {
                           mobEdu.enquire.f.showTestScores();
                    }
                },
                {
                    text:'Next',
                    ui:'confirm',
                    align:'right',
                    handler:function () {
                          mobEdu.enquire.f.loadOtherInfo();
                    }
                }
            ]
        },
        {
            xtype:'fieldset',
            title:'Enrollment Information'
        },
        {
            xtype:'fieldset',
//            title:'Start Term*',
            //title:'<div style="font-weight:lighter;font-size:12pt">Start Term*</div>',
            items:[

                {
                    xtype:'selectfield',
                    name:'startTerm',
                    labelAlign:'top',
                    label:'Start Term',
                    required:true,
                    id:'startTerm',
                    options:[
                        {text:'Select', value:'select'},
                        {text:'Summer', value:'201010'},
                        {text:'Spring', value:'201210'},
                        {text:'Winter', value:'201011'},
                        {text:'Fall', value:'201012'}
                    ]
                },
                 {
                    xtype:'selectfield',
                    labelAlign:'top',
                    label:'Start Year',
                    required:true,
                    name:'startYear',
                    id:'startYear',
                    options:[
                        {text:'Select', value:'select'},
                        {text:'2012', value:'2012'},
                        {text:'2013', value:'2013'},
                        {text:'2014', value:'2014'},
                        {text:'2015', value:'2015'}
                    ]
                },
                 {
                    xtype:'selectfield',
                    labelAlign:'top',
                    label:'Primary Academic Area of Interest',
                    labelWrap:true,
                    required:true,
                    name:'primary',
                    id:'primary',
                    options:[
                        {text:'Select', value:'select'},
                        {text:'Accounting', value:'Accounting'},
                        {text:'Art Education', value:'ArtEducation'},
                        {text:'Biology Education', value:'BiologyEducation'},
                        {text:'Chemistry Education', value:'ChemistryEducation'},
                        {text:'Communication Education', value:'CommunicationEducation'},
                        {text:'Economics', value:'Economics'},
                        {text:'English Education', value:'EnglishEducation'},
                        {text:'Exercise Science', value:'ExerciseScience'},
                        {text:'French Education', value:'FrenchEducation'},
                        {text:'International Business', value:'InternationalBusiness'},
                        {text:'Leadership Studies (minor)', value:'LeadershipStudies(minor)'},
                        {text:'Spanish Education', value:'SpanishEducation'}
                    ]
                },
                {
                    xtype:'selectfield',
                    labelAlign:'top',
//                    labelWidth:'50%',
                    label:'Secondary Academic Area of Interest',
                    labelWrap:true,
//                    placeHolder:'None',
                    name:'secondary',
                    id:'secondary',
                    options:[
                        {text:'Select', value:'select'},
                        {text:'Accounting', value:'Accounting'},
                        {text:'Art Education', value:'ArtEducation'},
                        {text:'Biology Education', value:'BiologyEducation'},
                        {text:'Chemistry Education', value:'ChemistryEducation'},
                        {text:'Communication Education', value:'CommunicationEducation'},
                        {text:'Economics', value:'Economics'},
                        {text:'English Education', value:'EnglishEducation'},
                        {text:'Exercise Science', value:'ExerciseScience'},
                        {text:'French Education', value:'FrenchEducation'},
                        {text:'International Business', value:'InternationalBusiness'},
                        {text:'Leadership Studies (minor)', value:'LeadershipStudies(minor)'},
                        {text:'Spanish Education', value:'SpanishEducation'}
                    ]
                }
            ]}
//        {
//            xtype:'fieldset',
//            title:'Start Year*',
//
//            items:[
//                {
//                    xtype:'selectfield',
//                    name:'startYear',
//                    id:'startYear',
//                    options:[
////                        {text:'', value:''},
//                        {text:'2012', value:'2012'},
//                        {text:'2011', value:'2011'},
//                        {text:'2010', value:'2010'},
//                        {text:'2009', value:'2009'}
//                    ]
//                }]
//        },
//        {
//            xtype:'fieldset',
//            title:'Primary Academic Area of Interest*',
//            items:[
//
//                {
//                    xtype:'selectfield',
//                    name:'primary',
//                    id:'primary',
//                    options:[
////                        {text:'', value:''},
//                        {text:'Accounting', value:'Accounting'},
//                        {text:'Art Education', value:'ArtEducation'},
//                        {text:'Biology Education', value:'BiologyEducation'},
//                        {text:'Chemistry Education', value:'ChemistryEducation'},
//                        {text:'Communication Education', value:'CommunicationEducation'},
//                        {text:'Economics', value:'Economics'},
//                        {text:'English Education', value:'EnglishEducation'},
//                        {text:'Exercise Science', value:'ExerciseScience'},
//                        {text:'French Education', value:'FrenchEducation'},
//                        {text:'International Business', value:'InternationalBusiness'},
//                        {text:'Leadership Studies (minor)', value:'LeadershipStudies(minor)'},
//                        {text:'Spanish Education', value:'SpanishEducation'}
//                    ]
//                }
//            ]},
//        {
//            xtype:'fieldset',
//            title:'Secondary Academic Area of Interest',
//            items:[
//                {
//                    xtype:'selectfield',
//                    name:'secondary',
//                    id:'secondary',
//                    options:[
////                        {text:'', value:''},
//                        {text:'Accounting', value:'Accounting'},
//                        {text:'Art Education', value:'ArtEducation'},
//                        {text:'Biology Education', value:'BiologyEducation'},
//                        {text:'Chemistry Education', value:'ChemistryEducation'},
//                        {text:'Communication Education', value:'CommunicationEducation'},
//                        {text:'Economics', value:'Economics'},
//                        {text:'English Education', value:'EnglishEducation'},
//                        {text:'Exercise Science', value:'ExerciseScience'},
//                        {text:'French Education', value:'FrenchEducation'},
//                        {text:'International Business', value:'InternationalBusiness'},
//                        {text:'Leadership Studies (minor)', value:'LeadershipStudies(minor)'},
//                        {text:'Spanish Education', value:'SpanishEducation'}
//                    ]
//                }
//            ]}
       ]
   }
});
