Ext.define('mobEdu.enquire.view.phone.appointmentsList', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.appointmentsList'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
                xtype: 'list',
                id: 'appList',
                name: 'appList',
                emptyText: '<h3 align="center">No Appointments</h3>',
                //                plugins: [{
                //                    type: 'listpaging',
                //                    autoPaging: true,
                //                    loadMoreText: 'Load More...',
                //                    noMoreRecordsText: ''
                //                }],
                itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                    '<tr>',
                    '<td ><h2>{subject}</h2></td>' +
                    '<td align="right"><h5>{appointmentDate}</h5></td>' +
                    //                        '</tpl>',
                    '</tr>' +
                    '</table>'
                ),
                store: mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList'),
                singleSelect: true,
                loadingText: '',
                listeners: {
                    itemtap: function(view, index, target, record, item, e, eOpts) {
                        setTimeout(function() {
                            view.deselect(index);
                        }, 500);
                        mobEdu.enquire.f.loadAppointmentDetail(record);
                    }
                }
            }, {
                xtype: 'customToolbar',
                name: 'appTitle',
                id: 'appTitle',
                title: '<h1>Appointments</h1>'
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                items: [{
                        text: 'New Appointment',
                        id: 'newPhnAppointment',
                        handler: function() {
                            mobEdu.enquire.f.showNewAppointment();
                        }
                    }, {
                        text: 'Calendar',
                        id: 'enqAppCal',
                        handler: function() {
                            mobEdu.enquire.f.showCalendar();
                        }
                    }

                ]
            }

        ],
        flex: 1
    }
});