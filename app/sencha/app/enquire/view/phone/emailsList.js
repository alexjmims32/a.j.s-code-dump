Ext.define('mobEdu.enquire.view.phone.emailsList', {
	extend: 'Ext.Panel',
	requires: [
		'mobEdu.enquire.f',
		'mobEdu.enquire.store.emailsList'
	],
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		cls: 'logo',
		items: [{
				xtype: 'list',
				id: 'emailsList',
				name: 'emailsList',
				emptyText: '<h3 align="center">No Messages</h3>',
				//                plugins: [{
				//                    type: 'listpaging',
				//                    autoPaging: true,
				//                    loadMoreText: 'Load More...',
				//                    noMoreRecordsText: ''
				//                }],
				itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
					'<tr>',
					'<tpl if="(mobEdu.enquire.f.hasEmailDirection(label)===true)">',
					'<td width="5%"><img width="16px" height="16px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
					'<tpl else>',
					'<td width="5%"><img width="16px" height="16px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
					'</tpl>',
					'<tpl if="(mobEdu.enquire.f.hasReadFlag(messageStatus)===true)">',
					'<td><h3>{subject}</h3></td>' +
					'<td align="right"><h5>{versionDate}</h5><br/></td>' +
					'<tpl else>',
					'<td style="color: #030303;"><h2>{subject}</h2></td>' +
					'<td style="color: #030303;" align="right"><h5>{versionDate}</h5><br/></td>' +
					'</tpl>',
					'</tr>' +
					'</table>'
				),

				store: mobEdu.util.getStore('mobEdu.enquire.store.emailsList'),
				singleSelect: true,
				loadingText: '',
				listeners: {
					itemtap: function(view, index, target, record, item, e, eOpts) {
						setTimeout(function() {
							view.deselect(index);
						}, 500);
						mobEdu.enquire.f.loadEmailDetail(record);
					}
				}
			}, {
				xtype: 'customToolbar',
				title: '<h1>Messages</h1>'
			}, {
				xtype: 'toolbar',
				//                style:'font-size:12pt',
				docked: 'bottom',
				layout: {
					pack: 'right'
				},
				items: [{
					text: 'New Message',
					//                        ui:'confirm',
					align: 'right',
					handler: function() {
						mobEdu.enquire.f.showNewMail();
					}
				}]
			}

		],
		flex: 1
	},
	// initialize: function() {
	// 	mobEdu.enquire.f.setPagingPlugin(Ext.getCmp('emailsList'));
	// 	var searchStore = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
	// 	searchStore.addBeforeListener('load', mobEdu.enquire.f.checkForSearchListEnd, this);
	// }
});