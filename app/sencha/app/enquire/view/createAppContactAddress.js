Ext.define('mobEdu.enquire.view.createAppContactAddress', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.stateEnumerations',
        'mobEdu.enquire.store.countryEnumerations'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Create Application</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Previous',
                handler: function() {
                    mobEdu.enquire.f.showCreateAppBasicInfo();
                }
            }, {
                text: 'Next',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.loadCreateContactInfo();
                }
            }]
        }, {
            xtype: 'fieldset',
            title: 'Contact Information'

        }, {
            xtype: 'fieldset',
            items: [{
                    xtype: 'textfield',
                    labelAlign: 'left',
                    labelWidth: '50%',
                    label: 'Address1',
                    name: 'appAddress1',
                    id: 'appAddress1',
                    required: true,
                    useClearIcon: true
                }, {
                    xtype: 'textfield',
                    labelAlign: 'left',
                    labelWidth: '50%',
                    label: 'Address2',
                    name: 'appAddress2',
                    id: 'appAddress2',
                    useClearIcon: true
                },
                //                    {
                //                        xtype:'textfield',
                //                        labelAlign:'left',
                //                        labelWidth:'50%',
                //                        label:'Address3',
                //                        name:'appAddress3',
                //                        id:'appAddress3',
                //                        useClearIcon:true
                //                    },
                {
                    xtype: 'textfield',
                    labelWidth: '50%',
                    name: 'appCity',
                    id: 'appCity',
                    required: true,
                    useClearIcon: true,
                    label: 'City'
                }, {
                    xtype: 'searchfield',
                    labelWidth: '50%',
                    name: 'stAppList',
                    id: 'stAppList',
                    label: 'state',
                    required: true,
                    placeHolder: 'Enter State',
                    clearIcon: true,
                    listeners: {
                        focus: function() {
                            mobEdu.enquire.f.loadAppStatePopup();
                        },
                        clearicontap: function(textfield, e, eOpts) {
                            // In Android, clearing the field causes focus event to be raised
                            // So use a flag and do not show the popup, immediately after the clear icon is tapped
                            if (Ext.os.is.Android) {
                                mobEdu.enquire.view.state.clearIconTapped = true;
                            }
                        },
                    }
                }, {
                    xtype: 'textfield',
                    labelWidth: '50%',
                    label: 'Zip',
                    name: 'appZip',
                    id: 'appZip',
                    required: true,
                    useClearIcon: true,
                    initialize: function() {
                        var me = this,
                            input = me.getInput().element.down('input');

                        input.set({
                            pattern: '[0-9]*'
                        });

                        me.callParent(arguments);
                    },
                    listeners: {
                        keyup: function(textfield, e, eOpts) {
                            return mobEdu.enquire.f.onZipKeyup(textfield);
                        }
                    },
                    maxLength: 5
                }, {
                    //                        xtype:'selectfield',
                    xtype: 'textfield',
                    labelWidth: '50%',
                    label: 'Country',
                    name: 'appCountry',
                    id: 'appCountry',
                    required: true,
                    useClearIcon: true,
                    value: 'USA',
                    //                        readOnly:true
                    store: mobEdu.util.getStore('mobEdu.enquire.store.countryEnumerations'),
                    displayField: 'displayValue',
                    valueField: 'enumValue'
                },
                //                    {dd
                //                        xtype:'selectfield',
                //                        labelWidth:'50%',
                //                        label:'County',
                //                        name:'appCounty',
                //                        id:'appCounty',
                //                        useClearIcon:true,
                //                        store:mobEdu.util.getStore('mobEdu.enquire.store.countyEnumerations'),
                //                        displayField:'displayValue',
                //                        valueField:'enumValue'
                //                    },
                {
                    xtype: 'textfield',
                    labelAlign: 'left',
                    labelWidth: '50%',
                    label: 'Home',
                    name: 'appPhone1',
                    id: 'appPhone1',
                    useClearIcon: true,
                    initialize: function() {
                        var me = this,
                            input = me.getInput().element.down('input');

                        input.set({
                            pattern: '[0-9]*'
                        });

                        me.callParent(arguments);
                    },
                    listeners: {
                        keyup: function(textfield, e, eOpts) {
                            mobEdu.enquire.f.onPhone1Keyup(textfield);
                        }
                    },
                    maxLength: 13
                }, {
                    xtype: 'textfield',
                    labelAlign: 'left',
                    labelWidth: '50%',
                    label: 'Mobile',
                    name: 'appPhone2',
                    id: 'appPhone2',
                    useClearIcon: true,
                    initialize: function() {
                        var me = this,
                            input = me.getInput().element.down('input');

                        input.set({
                            pattern: '[0-9]*'
                        });

                        me.callParent(arguments);
                    },
                    listeners: {
                        keyup: function(textfield, e, eOpts) {
                            mobEdu.enquire.f.onPhone2Keyup(textfield);
                        }
                    },
                    maxLength: 13
                }, {
                    xtype: 'emailfield',
                    name: 'appEmail',
                    id: 'appEmail',
                    labelWidth: '50%',
                    label: 'Email',
                    useClearIcon: true,
                    initialize: function() {
                        var me = this,
                            input = me.getInput().element.down('input');

                        input.set({
                            pattern: '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                        });

                        me.callParent(arguments);
                    }
                }
            ]
        }]
    }
});