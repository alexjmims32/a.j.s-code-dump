Ext.define('mobEdu.enquire.view.createAppAdmissionDetails',{
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f',                 
        'mobEdu.enquire.store.termEnumerations',
        'mobEdu.enquire.store.levelCodeEnumerations',
        'mobEdu.enquire.store.majorCodeEnumerations',
        'mobEdu.enquire.store.studentTypeEnumerations',
        'mobEdu.enquire.store.admissionTypeEnumerations',
        'mobEdu.enquire.store.residenceCodeEnumerations',
        'mobEdu.enquire.store.collegeCodeEnumerations',
        'mobEdu.enquire.store.degreeCodeEnumerations',
        'mobEdu.enquire.store.departmentEnumerations',
        'mobEdu.enquire.store.campusEnumerations',
        'mobEdu.enquire.store.eduGoalEnumerations',
        'mobEdu.enquire.store.appStatusEnumerations'               
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Create Application</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{ pack:'right' },
                items:[
                    {
                        text:'Previous',
                        handler:function () {
                            mobEdu.enquire.f.showCreateAppSchoolDetails();
                        }
                    },
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadCreateAdmissionDetails();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'Admission Details'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'selectfield',
                        name:'appStartTerm',
                        label:'Start Term',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appStartTerm',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.termEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
//                        options:[
//                            {text:'Select', value:'select'},
//                            {text:'Summer', value:'201010'},
//                            {text:'Spring', value:'201210'},
//                            {text:'Winter', value:'201011'},
//                            {text:'Fall', value:'201012'}
//                        ]
                    },
                    {
                        xtype:'selectfield',
                        name:'appLCode',
                        label:'Level Code',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appLCode',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.levelCodeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appMCode',
                        label:'Major Code',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appMCode',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.majorCodeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appSType',
                        label:'Student Type',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appSType',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.studentTypeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appAType',
                        label:'Admission Type',
                        labelAlign:'left',
                        labelWidth:'50%',
                        id:'appAType',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.admissionTypeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appRCode',
                        label:'Residence Code',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appRCode',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.residenceCodeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appLCode',
                        label:'College Code',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appAdCCode',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.collegeCodeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appDCode',
                        label:'Degree Code',
                        labelAlign:'left',
                        labelWidth:'50%',
                        required:true,
                        id:'appDCode',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.degreeCodeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appDep',
                        label:'Department',
                        labelAlign:'left',
                        labelWidth:'50%',
                        id:'appDep',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.departmentEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appCampus',
                        label:'Campus',
                        labelAlign:'left',
                        labelWidth:'50%',
                        id:'appCampus',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.campusEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appEGoal',
                        label:'Education Goal',
                        labelAlign:'left',
                        labelWidth:'50%',
                        id:'appEGoal',
                        store:mobEdu.util.getStore('mobEdu.enquire.store.eduGoalEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        name:'appAppStatus',
                        label:'Application Status',
                        labelAlign:'left',
                        labelWidth:'50%',
                        id:'appAppStatus',
                        required:true,
                        store:mobEdu.util.getStore('mobEdu.enquire.store.appStatusEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    }


                ]}
        ]
    }
});
