Ext.define('mobEdu.enquire.view.testScoresPopup', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.testCodeEnumerations'
    ],
    config: {
        id: 'tsPopup',
        scroll: 'vertical',
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        cls: 'logo',
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '100%',
        height: '80%',
        layout: 'fit',
        items: [{
            xtype: 'fieldset',
            items: [{
                xtype: 'selectfield',
                labelWidth: '50%',
                label: 'Test Code',
                name: 'appTCode',
                id: 'appTCode',
                required: true,
                useClearIcon: true,
                store: mobEdu.util.getStore('mobEdu.enquire.store.testCodeEnumerations'),
                displayField: 'displayValue',
                valueField: 'enumValue'
            }, {
                xtype: 'datepickerfield',
                labelWidth: '50%',
                label: 'Test Date',
                name: 'appTDate',
                id: 'appTDate',
                required: true,
                placeHolder: 'Select',
                useClearIcon: true

            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Test Score',
                name: 'appTScore',
                id: 'appTScore',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        return mobEdu.enquire.f.onTestScoreKeyUp(textfield);
                    }
                },
                maxLength: 3
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Add',
                handler: function() {
                    mobEdu.enquire.f.testScoresAddToGrid();
                }
            }, {
                text: 'Reset',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.resetTsPopup();
                }
            }, {
                text: 'Close',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.hideTestPopup();
                }
            }]
        }],
        flex: 1

    }
});