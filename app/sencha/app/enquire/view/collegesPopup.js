Ext.define('mobEdu.enquire.view.collegesPopup', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.priorCCodeEnumerations',
        'mobEdu.enquire.store.stateEnumerations',
        'mobEdu.enquire.store.priorDegreeEnumerations'
    ],
    config: {
        id: 'collegePopup',
        floating: true,
        scroll: 'vertical',
        modal: true,
        scrollable: true,
        centered: true,
        hideOnMaskTap: true,
        cls: 'logo',
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '100%',
        height: '80%',
        layout: 'fit',
        items: [{
            xtype: 'fieldset',
            items: [

                {
                    xtype: 'selectfield',
                    labelWidth: '50%',
                    name: 'appCCode',
                    id: 'appCCode',
                    required: true,
                    useClearIcon: true,
                    label: 'College Code',
                    store: mobEdu.util.getStore('mobEdu.enquire.store.priorCCodeEnumerations'),
                    displayField: 'displayValue',
                    valueField: 'enumValue'
                }, {
                    xtype: 'textfield',
                    labelWidth: '50%',
                    name: 'appCCity',
                    id: 'appCCity',
                    required: true,
                    useClearIcon: true,
                    label: 'City'
                }, {
                    xtype: 'selectfield',
                    labelWidth: '50%',
                    label: 'State',
                    required: true,
                    useClearIcon: true,
                    name: 'appCState',
                    id: 'appCState',
                    store: mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations'),
                    displayField: 'displayValue',
                    valueField: 'enumValue'
                }, {
                    xtype: 'selectfield',
                    labelWidth: '50%',
                    label: 'Degree Code',
                    name: 'appCDCode',
                    id: 'appCDCode',
                    required: true,
                    useClearIcon: true,
                    store: mobEdu.util.getStore('mobEdu.enquire.store.priorDegreeEnumerations'),
                    displayField: 'displayValue',
                    valueField: 'enumValue'

                }, {
                    xtype: 'datepickerfield',
                    labelWidth: '50%',
                    label: 'Graduation Date',
                    name: 'appCGDate',
                    id: 'appCGDate',
                    required: true,
                    placeHolder: 'Select',
                    useClearIcon: true
                    //                        picker:{
                    //                            yearFrom:1980,
                    //                            yearTo:2012,
                    //                            slotOrder:['month', 'day', 'year']
                    //                        }
                }
                //                    {
                //                        xtype:'datepickerfield',
                //                        labelWidth:'50%',
                //                        label:'Attend From',
                //                        name:'appCAtFrom',
                //                        id:'appCAtFrom',
                //                        placeHolder:'[Select]',
                //                        picker:{
                //                            yearFrom:1980,
                //                            yearTo:2012,
                //                            slotOrder:['day', 'month', 'year']
                //                        }
                //                    },
                //                    {
                //                        xtype:'datepickerfield',
                //                        labelWidth:'50%',
                //                        label:'Attend To',
                //                        name:'appCAtTo',
                //                        id:'appCAtTo',
                //                        placeHolder:'[Select]',
                //                        picker:{
                //                            yearFrom:1980,
                //                            yearTo:2012,
                //                            slotOrder:['day', 'month', 'year']
                //                        }
                //                    }

            ]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Add',
                handler: function() {
                    mobEdu.enquire.f.collegesAddToList();
                }
            }, {
                text: 'Reset',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.resetCollegesList();
                }
            }, {
                text: 'Close',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.hideCollegePopup();
                }
            }]
        }],
        flex: 1
    }
});