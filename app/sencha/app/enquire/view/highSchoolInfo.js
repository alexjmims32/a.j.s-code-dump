Ext.define('mobEdu.enquire.view.highSchoolInfo',{
    extend:'Ext.form.FormPanel',
//    alias:'inquiryHighSchoolInfoView',
//    xtype:'inquiryHighSchoolInfoView',
    requires: [
        'mobEdu.enquire.f'	            
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
            xtype:'customToolbar',
            title:'<h1>Submit Profile</h1>'
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            style:'font-size:12pt',
            layout:{pack:'right'},
            items:[
                {
                    text:'Previous',
                    handler:function () {
                          mobEdu.enquire.f.showContactPhoneInfo();
                    }
                },
                {
                    text:'Next',
                    ui:'confirm',
                    align:'right',
                    handler:function () {
                          mobEdu.enquire.f.loadTestScores();
                    }
                }
            ]
        },
          {
            title:'High School Information',
            xtype:'fieldset'
        },
        {
            xtype:'fieldset',
            items:[
                {
                    xtype:'textfield',
                    labelAlign:'left',
                    labelWidth:'50%',
                    label:'Name',
                    name:'hsname',
                    id:'hsname',
                    required:true,
                    useClearIcon:true
                },
                {
                    xtype:'textfield',
                    labelAlign:'left',
                    labelWidth:'50%',
                    label:'City*',
                    name:'hscity',
                    id:'hscity',
                    useClearIcon:true
                },
                {
                    xtype:'selectfield',
                    labelWidth:'50%',
                    label:'State',
                    required:true,
                    useClearIcon:true,
                    name:'hsstate',
                    id:'hsstate',
                    options:[
                        {text:'[Select]', value:'select'},
                        {text:'Alabama(AL)', value:'AL'},
                        {text:'Alaska(AK)', value:'AK'},
                        {text:'Alberta(AB)', value:'AB'},
                        {text:'American Samoa(AS)', value:'AS'},
                        {text:'Arizona(AZ)', value:'AZ'},
                        {text:'Arkansas(AR)', value:'AR'},
                        {text:'British Columbia(BC)', value:'BC'},
                        {text:'California(CA)', value:'CA'},
                        {text:'Colorado(CO)', value:'CO'},
                        {text:'Connecticut(CT)', value:'CT'},
                        {text:'Delaware(DE)', value:'DE'},
                        {text:'District of Columbia(DC)', value:'DC'},
                        {text:'Eastern Province(EP)', value:'EP'},
                        {text:'Federated States of Micronesia(FM)', value:'FM'},
                        {text:'Florida(FL)', value:'FL'},
                        {text:'Georgia(GA)', value:'GA'},
                        {text:'Guam(GU)', value:'GU'},
                        {text:'Hawaii(HI)', value:'HI'},
                        {text:'Idaho(ID)', value:'ID'},
                        {text:'Illinois(IL)', value:'IL'},
                        {text:'Indiana(IN)', value:'IN'},
                        {text:'Iowa(IA)', value:'IA'},
                        {text:'Kansas(KS)', value:'KS'},
                        {text:'Kentucky(KY)', value:'KY'},
                        {text:'Louisiana(LA)', value:'LA'},
                        {text:'Maine(ME)', value:'ME'},
                        {text:'Manitoba(MB)', value:'MB'},
                        {text:'Marshall Islands(MH)', value:'MH'},
                        {text:'Maryland(MD)', value:'MD'},
                        {text:'Massachusetts(MA)', value:'MA'},
                        {text:'Michigan(MI)', value:'MI'},
                        {text:'Minnesota(MN)', value:'MN'},
                        {text:'Mississippi(MS)', value:'MS'},
                        {text:'Missouri(MO)', value:'MO'},
                        {text:'Montana(MT)', value:'MT'},
                        {text:'Nebraska(NE)', value:'NE'},
                        {text:'Nevada(NV)', value:'NV'},
                        {text:'New Brunswick(NB)', value:'NB'},
                        {text:'New Hampshire(NH)', value:'NH'},
                        {text:'New Jersey(NJ)', value:'NJ'},
                        {text:'New Mexico(NM)', value:'NM'},
                        {text:'New York(NY)', value:'NY'},
                        {text:'Newfoundland and Labrador(NL)', value:'Nl'},
                        {text:'North Carolina(NC)', value:'NC'},
                        {text:'North Dakota(ND)', value:'ND'},
                        {text:'Northern Mariana Islands(CM)', value:'CM'},
                        {text:'Northwest Territories(NT)', value:'NT'},
                        {text:'Nova Scotia(NS)', value:'NS'},
                        {text:'Nunavut(NU)', value:'NU'},
                        {text:'Ohio(OH)', value:'OH'},
                        {text:'Oklahoma(OK)', value:'OK'},
                        {text:'Ontario(ON)', value:'ON'},
                        {text:'Oregon(OR)', value:'OR'},
                        {text:'Palau(PW)', value:'PW'},
                        {text:'Pennsylvania(PA)', value:'PA'},
                        {text:'Prince Edward Island(PE)', value:'PE'},
                        {text:'Puerto Rico(PR)', value:'PR'},
                        {text:'Quebec(QC)', value:'QC'},
                        {text:'Rhode Island(RI)', value:'RI'},
                        {text:'Saskatchewan(SK)', value:'SK'},
                        {text:'Saskatchewan(SK)', value:'SK'},
                        {text:'South Carolina(SC)', value:'SC'},
                        {text:'South Dakota(SD)', value:'SD'},
                        {text:'Tennessee(TN)', value:'TN'},
                        {text:'Texas(TX)', value:'TX'},
                        {text:'Utah(UT)', value:'UT'},
                        {text:'Vermont(VT)', value:'VT'},
                        {text:'Virgin Islands(VI)', value:'VI'},
                        {text:'Virginia(VA)', value:'VA'},
                        {text:'Washington(WA)', value:'WA'},
                        {text:'West Virginia(WV)', value:'WV'},
                        {text:'Wisconsin(WI)', value:'WI'},
                        {text:'Wyoming(WY)', value:'WY'},
                        {text:'Yukon(YU)', value:'YU'}
                    ]
                },
                {
                    xtype:'numberfield',
                    name:'zipCode',
                    labelWidth:'50%',
                    label:'Postal Code',
                    id:'zipCode',
                    required:true,
                    useClearIcon:true,
                    listeners:{
                        keyup:function (textfield, e, eOpts) {
                            return mobEdu.inq.f.onZipKeyup(textfield);
                        }
                    },
                    maxLength:5
                },
                {
                    xtype:'numberfield',
                    labelWidth:'50%',
                    label:'Graduation Year',
                    labelWrap:true,
                    name:'hsgyear',
                    id:'hsgyear',
                        required:true,
                    useClearIcon:true,
                    listeners: {
                        keyup: function( textfield, e, eOpts ) {
                              return mobEdu.enquire.f.onGradYearKeyup(textfield);
                        }
                    },
                    maxLength:4
                },
                {
                    xtype:'numberfield',
                    format:'0.0',
                    dataIndex:'floatje',
                    header:'Floatje',
                    type:'float',
                    labelWidth:'50%',
                    label:'GPA',
                    name:'hsgpa',
                    id:'hsgpa',
                    required:true,
                    decimalPrecision : 2,
                    forceDecimals : true,
                    allowDecimals: true,
                    useClearIcon:true,
                    listeners: {
                        keyup: function( numberfield, e, eOpts ) {
                              return mobEdu.enquire.f.onGPAKeyup(numberfield);
                        }

                    },
                    maxLength:3
                }
            ]}
        ]
    }
});
