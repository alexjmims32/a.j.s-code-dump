Ext.define('mobEdu.enquire.view.welcome', {
    extend: 'Ext.Panel',
    //    alias:'inquiryWelcomeView',
    //    xtype:'inquiryWelcomeView',
    requires: [
        'mobEdu.enquire.f'
    ],
    config: {
        scrollable: 'vertical',
        padding: '10 10',
        cls: 'logo',
        fullscreen: true,
        layout: 'fit',
        //        styleHtmlContent: true,
        html: [
            '<table><tr><td>The mission of Southern University and A&M College, an Historically Black,1890 land-grant institution, is to provide opportunities for a diverse student population to achieve a high-quality, global educational experience, to engage in scholarly, research, and creative activities, and to give meaningful public service to the community, the state, the nation, and the world so that Southern University graduates are competent, informed, and productive citizens.</td></tr></table>'
        ],
        items: [{
            xtype: 'toolbar',
            docked: 'top',
            title: '<h1>About' + campusCode + '</h1>',
            ui: 'light',
            // Back Button
            items: [{
                    xtype: 'button',
                    cls: 'back',
                    style: 'background:none;border:none;',
                    handler: function() {
                        mobEdu.enquire.f.showSubMenu();
                    }
                }, {
                    xtype: 'spacer'
                },
                // Home Button
                {
                    ui: 'light',
                    xtype: 'button',
                    cls: 'home',
                    iconMask: true,
                    handler: function() {
                        showMainView();
                    }
                }
            ]

        }]
    }

});