Ext.define('mobEdu.enquire.view.paging', {
	extend: 'Ext.plugin.ListPaging',
	config: {
		//id: 'pagingplugin',
		id: 'messageplugin',
		type: 'listpaging',
		autoPaging: true,
		loadMoreText: 'Load messages..',
		noMoreRecordsText: ''
	}
});