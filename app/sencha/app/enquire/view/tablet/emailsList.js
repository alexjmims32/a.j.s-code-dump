Ext.define('mobEdu.enquire.view.tablet.emailsList', {
	extend: 'Ext.Container',
	config: {
		fullscreen: true,
		layout: {
			type: 'card',
			animation: {
				type: 'slide',
				direction: 'left',
				duration: 250
			}
		},
		cls: 'logo',
		items: [{
			title: '<h1>Messages</h1>',
			xtype: 'customToolbar',
			id: 'emailsT'
		}, {
			xtype: 'toolbar',
			docked: 'bottom',
			layout: {
				pack: 'right'
			},
			items: [{
				text: 'New Message',
				//                        ui:'confirm',
				align: 'right',
				handler: function() {
					mobEdu.enquire.f.showNewMail();
				}
			}, {
				text: 'Send Reply',
				id: 'replyT',
				name: 'replyT',
				//                        ui:'confirm',
				align: 'right',
				handler: function() {
					mobEdu.enquire.f.showReplyMessage();
				}
			}]
		}, {
			useTitleAsBackText: false,
			docked: 'left',
			width: '30%',
			xtype: 'list',
			id: 'emailsListT',
			cls: 'border',
			name: 'emailsListT',
			//                plugins: [{
			//                    type: 'listpaging',
			//                    autoPaging: true,
			//                    loadMoreText: 'Load More...',
			//                    noMoreRecordsText: ''
			//                }],
			emptyText: '<h3 align="center">No Messages</h3>',
			itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
				'<tr>',
				'<tpl if="(mobEdu.enquire.f.hasEmailDirection(label)===true)">',
				'<td width="5%"><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/sent1.png" align="absmiddle" /></td>',
				'<tpl else>',
				'<td width="5%"><img width="32px" height="32px" src="' + mobEdu.util.getResourcePath() + 'images/recieve1.png" align="absmiddle" /></td>',
				'</tpl>',
				'<tpl if="(mobEdu.enquire.f.hasReadFlag(status)===true)">',
				'<td><h3>{subject}</h3></td>' +
				'<td align="right"><h5>{versionDate}</h5><br/></td>' +
				'<tpl else>',
				'<td style="color: #030303;"><h2>{subject}</h2></td>' +
				'<td style="color: #030303;" align="right"><h5>{versionDate}</h5></td>' +
				'</tpl>',
				'</tr>' +
				'</table>'
			),
			store: mobEdu.util.getStore('mobEdu.enquire.store.emailsList'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, item, e, eOpts) {
					mobEdu.enquire.f.loadTabletEmailDetail(record);
				}
			}
		}, {
			layout: 'fit',
			scrollable: true,
			items: [{
				xtype: 'dataview',
				name: 'emaildetail',
				id: 'emaildetail',
				emptyText: '<center><h3>No Message selected</h3><center>',
				itemTpl: new Ext.XTemplate('<table width="100%">' +
					'<tr><td align="right" valign="top" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Date:</h2></td><td align="left"><h3>{versionDate}</h3></td></tr>' +
					'<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{messageStatus}</h3></td></tr>' +
					'<tr><td align="right" valign="top" width="50%"><h2>Message:</h2></td><td align="left"><h3>{[decodeURIComponent(values.body)]}</h3></td></tr>' +
					//                    '</table></td></tr>' +
					'</table>'
				),
				store: mobEdu.util.getStore('mobEdu.enquire.store.viewEmail'),
				singleSelect: true,
				loadingText: ''
			}]
		}],
		flex: 1
	},
	// initialize: function() {
	// 	mobEdu.enquire.f.setPagingPlugin(Ext.getCmp('emailsList'));
	// 	var searchStore = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
	// 	searchStore.addBeforeListener('load', mobEdu.enquire.f.checkForSearchListEnd, this);
	// }

});