Ext.define('mobEdu.enquire.view.tablet.appointmentsList', {
    extend: 'Ext.Container',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.appointmentsList',
        'mobEdu.enquire.store.appointmentDetail'
    ],
    config: {
        fullscreen: true,
        layout: {
            type: 'card',
            animation: {
                type: 'slide',
                direction: 'left',
                duration: 250
            }
        },
        cls: 'logo',
        items: [{
            title: '<h1>Appointments</h1>',
            xtype: 'customToolbar',
            id: 'appointmentsT'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'New Appointment',
                id: 'newAppointment',
                handler: function() {
                    mobEdu.enquire.f.showNewAppointment();
                }
            }, {
                text: 'Calendar',
                handler: function() {
                    mobEdu.enquire.f.showCalendar();
                }
            },{
                text: 'Accept',
                id: 'acceptT',
                handler: function() {
                    mobEdu.enquire.f.acceptAppointment('Accept');
                }
            }, {
                text: 'Decline',
                id: 'declineT',
                handler: function() {
                    mobEdu.enquire.f.acceptAppointment('Decline');
                }
            }]
        }, {
            useTitleAsBackText: false,
            docked: 'left',
            width: '30%',
            xtype: 'list',
            id: 'appointmentsList',
            cls: 'border',
            name: 'appointmentsList',
            //                plugins: [{
            //                    type: 'listpaging',
            //                    autoPaging: true,
            //                    loadMoreText: 'Load More...',
            //                    noMoreRecordsText: ''
            //                }],
            emptyText: '<h3 align="center">No Appointments</h3>',
            itemTpl: new Ext.XTemplate('<table class="menu" width="100%">' +
                '<tr>',
                '<td ><h2>{subject}</h2></td>' +
                '<td align="right"><h5>{appointmentDate}</h5></td>' +
                '</tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.enquire.f.loadTabletAppointmentDetail(record);
                }
            }
        }, {
            layout: 'fit',
            scrollable: true,
            items: [{
                xtype: 'dataview',
                name: 'appointmentdetail',
                id: 'appointmentdetail',
                emptyText: '<center><h3>No course selected</h3><center>',
                itemTpl: new Ext.XTemplate('<table width="100%">' +
                    '<tr><td align="right" valign="top" width="50%"><h2>Subject:</h2></td><td align="left"><h3>{subject}</h3></td></tr>' +
                    '<tr><td align="right" width="50%"><h2>Appointment Date:</h2></td><td align="left"><h3>{appointmentDate}</h3></td></tr>' +
                    '<tr><td align="right" width="50%"><h2>Location:</h2></td><td align="left"><h3>{location}</h3></td></tr>' +
                    '<tr><td align="right" width="50%"><h2>Status:</h2></td><td align="left"><h3>{appointmentStatus}</h3></td></tr>' +
                    '<tr><td align="right" valign="top" width="50%"><h2>Description:</h2></td><td align="left"><h3>{description}</h3></td></tr>' +
                    '</table>'
                ),
                store: mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail'),
                singleSelect: true,
                loadingText: ''
            }]
        }],
        flex: 1
    }

});