Ext.define('mobEdu.enquire.view.createAppVisaDetails', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f',
	'mobEdu.enquire.store.visaTypeEnumerations',
        'mobEdu.enquire.store.visaNationEnumerations'              
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Create Application</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Previous',
                        handler:function () {
                            mobEdu.enquire.f.showCreateAppParentDetails();
                        }
                    },
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadCreateVisaDetails();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'VISA Details'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'selectfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'VISA Type',
                        name:'appVType',
                        id:'appVType',
                        useClearIcon:true,
                        store:mobEdu.util.getStore('mobEdu.enquire.store.visaTypeEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'selectfield',
                        labelWidth:'50%',
                        label:'Nation of Citizenship',
                        name:'appCitizenship',
                        id:'appCitizenship',
                        useClearIcon:true,
                        store:mobEdu.util.getStore('mobEdu.enquire.store.visaNationEnumerations'),
                        displayField:'displayValue',
                        valueField:'enumValue'
                    },
                    {
                        xtype:'textfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'VISA Number',
                        name:'appVNo',
                        id:'appVNo',
                        useClearIcon:true,
                        initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[0-9]*'
                            });

                            me.callParent(arguments);
                        },
                        listeners: {
                            keyup: function( textfield, e, eOpts ) {
                                mobEdu.enquire.f.onVisaNoKeyUp(textfield);
                            }
                        },
                        maxLength:16
                    }
                ]
            }

        ]
    }
});
