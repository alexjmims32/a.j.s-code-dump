Ext.define('mobEdu.enquire.view.admissions', {
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'                       
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Submit Profile</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Previous',
                        handler:function () {
                            mobEdu.enquire.f.showContactPhoneInfo();
                        }
                    },
                    {
                        text:'Next',
                        ui:'confirm',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.loadAdmissions();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                title:'Admissions'
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'selectfield',
                        labelAlign:'left',
                        labelWidth:'50%',
                        label:'Classification',
                        name:'class',
                        id:'class',
                        required:true,
                        useClearIcon:true,
                        options:[
                            {
                                text:'Select',
                                value:'select'
                            },
                            {
                                text:'Freshman',
                                value:'Freshman'
                            },
                            {
                                text:'Sophomore',
                                value:'Sophomore'
                            },
                            {
                                text:'Junior',
                                value:'Junior'
                            },
                            {
                                text:'Senior',
                                value:'Senior'
                            },
                            {
                                text:'Graduate',
                                value:'Graduate'
                            }
                        ]
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'High School',
                        name:'highS',
                        id:'highS',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'textfield',
                        labelWidth:'50%',
                        label:'Intended Major',
                        name:'intMaj',
                        id:'intMaj',
                        required:true,
                        useClearIcon:true
                    },
                    {
                        xtype:'selectfield',
//                        labelWidth:'50%',
                        labelAlign:'top',
                        label:'What type of information <br/> are you looking for?',
                        name:'inf',
                        id:'inf',
                        required:true,
                        useClearIcon:true,
                        options:[
                            {
                                text:'Select',
                                value:'select'
                            },
                            {
                                text:'Financial Aid',
                                value:'Financial Aid'
                            },
                            {
                                text:'Resident Life (Housing)',
                                value:'Resident Life'
                            },{
                                text:'Scholarships',
                                value:'Scholarships'
                            },{
                                text:'Intended Major',
                                value:'Intended Major'
                            },{
                                text:'Other',
                                value:'Other'
                            }
                        ]
                    }
                ]
            }

        ]
    }
});
