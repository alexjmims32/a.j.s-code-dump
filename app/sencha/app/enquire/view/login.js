Ext.define('mobEdu.enquire.view.login', {
    extend: 'Ext.form.Panel',
    statics: {
        tablet: {
            landscape: {
                width: '35%',
                height: '33%'
            },
            portrait: {
                width: '45%',
                height: '25%'
            }
        },
        phone: {
            landscape: {
                width: '40%',
                height: '85%'
            },
            portrait: {
                width: '80%',
                height: '45%'
            }
        }
    },
    config: {
        id: 'enquireLoginPopup',
        scrollable: false,
        floating: true,
        modal: true,
        centered: true,
        hideOnMaskTap: true,
        showAnimation: {
            type: 'popIn',
            duration: 250,
            easing: 'ease-out'
        },
        hideAnimation: {
            type: 'popOut',
            duration: 250,
            easing: 'ease-out'
        },
        width: '50%',
        height: '50%',
        style: 'background-color: transparent;',
        layout: {
            type: 'vbox',
            pack: 'middle'
        },
        padding: '5 5 0 5',
        items: [
            //     {
            //         xtype: 'customToolbar',
            //         title: '<h1>Enquire</h1>'

            // },
            {
                xtype: 'textfield',
                name: 'enquireName',
                id: 'enquireName',
                placeHolder: 'Username',
                clearIcon: true,
                autoCapitalize: false,
                required: true,
                margin: 5,
                listeners: {
                    keyup: function(view, e, eOpts) {
                        mobEdu.enquire.f.onUsernameKeyup(view, e, eOpts);
                    }
                }
            }, {
                xtype: 'passwordfield',
                name: 'enquirePassword',
                id: 'enquirePassword',
                placeHolder: 'Password',
                required: true,
                clearIcon: true,
                margin: 5,
                listeners: {
                    keyup: function(view, e, eOpts) {
                        mobEdu.enquire.f.onPasswordKeyup(view, e, eOpts);
                    }
                }
            }, {
                layout: 'hbox',
                items: [{
                    xtype: 'spacer'
                }, {
                    xtype: 'button',
                    text: '<h7>Login</h7>',
                    ui: 'action',
                    margin: 10,
                    handler: function() {
                        mobEdu.enquire.f.submitLoginForm();
                    }
                }, {
                    xtype: 'button',
                    text: '<h7>Sign Up</h7>',
                    ui: 'action',
                    margin: 10,
                    handler: function() {
                        mobEdu.enquire.f.showSignUp();
                    }
                }, {
                    xtype: 'button',
                    text: campusCode === 'UNCFSU' ? '<h7>About FSU</h7>' : '<h7>About ' + campusCode,
                    //text: '<h7>About ' + campusCode === 'UNCFSU' ? 'FSU' : campusCode + ' </h7>',
                    ui: 'action',
                    margin: 10,
                    handler: function() {
                        mobEdu.enquire.f.showAboutSubr();
                    }
                }]
            }
        ],
        flex: 1
    },

    initialize: function() {
        console.log('Panel initialize');
        // Add a Listener. Listen for [Viewport ~ Orientation] Change.
        if (!Ext.os.is.Android) {
            Ext.Viewport.on('orientationchange', 'handleOrientationChange', this);
        } else {
            this.on({
                resize: function() {
                    if (Ext.Viewport.element.getWidth() > Ext.Viewport.element.getHeight()) {
                        mobEdu.enquire.f.onOrientationChange('landscope');
                    } else {
                        mobEdu.enquire.f.onOrientationChange('portrait');
                    }
                }
            });
        }
        this.callParent(arguments);
    },

    handleOrientationChange: function(viewport, orientation, width, height) {
        // Execute the code that needs to fire on Orientation Change.
        mobEdu.enquire.f.onOrientationChange(orientation);

    }

});