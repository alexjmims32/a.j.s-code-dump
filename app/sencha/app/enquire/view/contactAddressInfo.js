Ext.define('mobEdu.enquire.view.contactAddressInfo', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Submit Profile</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Previous',
                handler: function() {
                    mobEdu.enquire.f.showBasicInfo();
                }
            }, {
                text: 'Next',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.loadContactPhoneInfo();
                }
            }]
        }, {
            xtype: 'fieldset',
            title: 'Contact Information'

        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Address1',
                name: 'address',
                id: 'address',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Address2',
                name: 'add2',
                id: 'add2',
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'Address3',
                name: 'add3',
                id: 'add3',
                useClearIcon: true
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                name: 'city',
                id: 'city',
                required: true,
                useClearIcon: true,
                label: 'City'
            }, {
                xtype: 'selectfield',
                labelWidth: '50%',
                label: 'County',
                name: 'county',
                id: 'county',
                useClearIcon: true,
                hidden: true,
                options: [

                    {
                        text: 'Abbeville',
                        value: '001'
                    },

                    {
                        text: 'Aiken',
                        value: '002'
                    },

                    {
                        text: 'Allendale',
                        value: '003'
                    },

                    {
                        text: 'Anderson',
                        value: '004'
                    },

                    {
                        text: 'Bamberg',
                        value: '005'
                    },

                    {
                        text: 'Barnwell',
                        value: '006'
                    },

                    {
                        text: 'Beaufort',
                        value: '007'
                    },

                    {
                        text: 'Berkley',
                        value: '008'
                    },

                    {
                        text: 'Calhoun',
                        value: '009'
                    },

                    {
                        text: 'Charleston',
                        value: '010'
                    },

                    {
                        text: 'Cherokee',
                        value: '011'
                    },

                    {
                        text: 'Chester',
                        value: '012'
                    },

                    {
                        text: 'Chesterfield',
                        value: '013'
                    },

                    {
                        text: 'Clarendon',
                        value: '014'
                    },

                    {
                        text: 'Colleton',
                        value: '015'
                    }
                ]

            }, {
                xtype: 'searchfield',
                name: 'stList',
                id: 'stList',
                placeHolder: 'Enter State',
                clearIcon: true,
                listeners: {
                    focus: function() {
                        mobEdu.enquire.f.loadStatePopup();
                    },
                    clearicontap: function(textfield, e, eOpts) {
                        // In Android, clearing the field causes focus event to be raised
                        // So use a flag and do not show the popup, immediately after the clear icon is tapped
                        if (Ext.os.is.Android) {
                            mobEdu.enquire.view.state.clearIconTapped = true;
                        }
                    },
                }
            }, {
                xtype: 'numberfield',
                labelWidth: '50%',
                label: 'Zip',
                name: 'zip',
                id: 'zip',
                required: true,
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        return mobEdu.enquire.f.onZipKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Country',
                name: 'country',
                id: 'country',
                required: true,
                useClearIcon: true
            }]
        }]
    }
});