Ext.define('mobEdu.enquire.view.resetPassword',{
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
            {
                xtype:'customToolbar',
                title:'<h1>Reset Password</h1>'
            },
            {
                xtype:'toolbar',
                docked:'bottom',
                layout:{
                    pack:'right'
                },
                items:[
                    {
                        text:'Save',
                        align:'right',
                        handler:function () {
                            mobEdu.enquire.f.saveResetPassword();
                        }
                    }
                ]
            },
            {
                xtype:'fieldset',
                items:[
                    {
                        xtype:'passwordfield',
                        name:'newPassword',
                        id:'newPassword',
                        labelWidth:'50%',
                        label:'New Password',
                        required:true,
                        useClearIcon:true,
                        maxLength:12 ,
                        mainLength:4
                    },
                    {
                        xtype:'passwordfield',
                        name: 'vNewPassword',
                        id:'vNewPassword',
                        labelWidth:'50%',
                        required:true,
                        label:'Verify Password',
                        useClearIcon:true,
                        maxLength:12,
                        mainLength:4
                    }
                ]
            }
        ]
    }
});
