Ext.define('mobEdu.enquire.view.appointmentScheduleCalendar', {
    extend:'Ext.Container',
    alias:'appointmentScheduleCalendar',
    config:{
        fullscreen:'true',
        scrollable:true,
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Calendar</h1>'
        }, {
            id:'appointmentsDock',
                name:'appointmentsDock'
        }, {
            xtype: 'label',
            id: 'appointmentLabel',
            padding: 10
        }]
    }
});
