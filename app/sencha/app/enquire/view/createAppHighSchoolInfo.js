Ext.define('mobEdu.enquire.view.createAppHighSchoolInfo', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.schoolCodeEnumerations',
        'mobEdu.enquire.store.stateEnumerations'
    ],
    config: {        
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Create Application</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Previous',
                handler: function() {
                    mobEdu.enquire.f.showCreateAppVisaDetail();
                }
            }, {
                text: 'Next',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.loadCreateSchoolDetails();
                }
            }]
        }, {
            title: 'School Details',
            xtype: 'fieldset'
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'School Name',
                name: 'appSName',
                id: 'appSName',
                useClearIcon: true
            }, {
                xtype: 'selectfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'School Code',
                name: 'appSCode',
                id: 'appSCode',
                useClearIcon: true,
                store: mobEdu.util.getStore('mobEdu.enquire.store.schoolCodeEnumerations'),
                displayField: 'displayValue',
                valueField: 'enumValue'
            }, {
                xtype: 'numberfield',
                format: '0.0',
                dataIndex: 'floatje',
                header: 'Floatje',
                type: 'float',
                labelWidth: '50%',
                label: 'School GPA',
                name: 'appSGpa',
                id: 'appSGpa',
                decimalPrecision: 2,
                forceDecimals: true,
                allowDecimals: true,
                useClearIcon: true,
                listeners: {
                    keyup: function(numberfield, e, eOpts) {
                        return mobEdu.enquire.f.onGPAKeyup(numberfield);
                    }

                },
                maxLength: 3
            }, {
                xtype: 'textfield',
                labelAlign: 'left',
                labelWidth: '50%',
                label: 'City',
                name: 'appSCity',
                id: 'appSCity',
                useClearIcon: true
            }, {
                xtype: 'searchfield',
                labelWidth: '50%',
                name: 'schStList',
                id: 'schStList',
                label: 'state',
                required: true,
                placeHolder: 'Enter State',
                clearIcon: true,
                listeners: {
                    focus: function() {
                        mobEdu.enquire.f.loadSchStatePopup();
                    },
                    clearicontap: function(textfield, e, eOpts) {
                        // In Android, clearing the field causes focus event to be raised
                        // So use a flag and do not show the popup, immediately after the clear icon is tapped
                        if (Ext.os.is.Android) {
                            mobEdu.enquire.view.state.clearIconTapped = true;
                        }
                    },
                }
            }, {
                xtype: 'numberfield',
                name: 'appSZipCode',
                labelWidth: '50%',
                label: 'Zip Code',
                id: 'appSZipCode',
                useClearIcon: true,
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        return mobEdu.enquire.f.onZipKeyup(textfield);
                    }
                },
                maxLength: 5
            }, {
                xtype: 'numberfield',
                labelWidth: '50%',
                label: 'Graduation Year',
                labelWrap: true,
                name: 'appSGYear',
                id: 'appSGYear',
                useClearIcon: true,
                listeners: {
                    keyup: function(textfield, e, eOpts) {
                        return mobEdu.enquire.f.onGradYearKeyup(textfield);
                    }
                },
                maxLength: 4
            }]
        }]
    }
});