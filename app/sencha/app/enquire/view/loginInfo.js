Ext.define('mobEdu.enquire.view.loginInfo',{
    extend:'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'	            
    ],
    config:{
        scroll:'vertical',
        fullscreen:true,
        cls:'logo',
        xtype:'toolbar',
        items:[
         {
            xtype:'toolbar',
            docked:'top',
            ui:'light',
            title:'<h1>Submit Profile</h1>',
            // Back Button
            items:[
                {
                    xtype:'button',
                cls:'back',
                style:'background:none;border:none;',
                    handler:function () {
                          mobEdu.enquire.f.showOtherInfo();
                    }
                },
                // Home Button
                { xtype:'spacer' },
                {
                    ui:'light',
                    xtype:'button',cls:'home',
                    iconMask:true,
                    handler:function () {
                         showMainView();
                    }
                }
            ]
        },
        {
            xtype:'toolbar',
            docked:'bottom',
            style:'font-size:12pt',
            layout:{ pack:'right' },
            items:[
                {
                    text:'Previous',
                    handler:function () {
                          mobEdu.enquire.f.showOtherInfo();
                    }
                },
                {
                    text:'Submit',
                    ui:'confirm',
                    align:'right',
                    handler:function () {
                           mobEdu.enquire.f.inquirySubmit();
                    }
                }
            ]
        },
        {
            xtype:'fieldset',
            title:'Login Information'
        },
        {
            xtype:'fieldset',
            style: 'text-align: left',
            ui:'round',
            instructions:'<P>We would like to create your own personal'
            +' page to track your admissions progress and'
                +' communications.</p><br>'
                +'<p>Please create your login and password in'
                +' the space below. Logins and passwords are'
                +' case-sensitive and must be greater than'
                +' five characters long. An e-mail will be sent'
                +' to you with login information.</p>',
            items:[
                {
                    xtype:'textfield',
                    labelWidth:'60%',
                    label:'Username',
                    required:'true',
                    name:'inqUsername',
                    id:'inqUsername'
                },{
                    xtype:'passwordfield',
                    labelWidth:'60%',
                    label:'Password',
                    required:'true',
                    name:'inqPwd',
                    id:'inqPwd'
                },{
                    xtype:'passwordfield',
                    labelWidth:'60%',
                    label:'Verify Password',
                    required:'true',
                    name:'inqVerifyPwd',
                    id:'inqVerifyPwd'
                }
            ]
        }
        ]
    }
});
