Ext.define('mobEdu.enquire.view.state', {
	extend: 'Ext.Panel',
	statics: {
		clearIconTapped: false
	},
	config: {
		id: 'statePopup',
		scrollable: 'none',
		floating: true,
		modal: true,
		centered: true,
		hideOnMaskTap: true,
		showAnimation: {
			type: 'popIn',
			duration: 250,
			easing: 'ease-out'
		},
		hideAnimation: {
			type: 'popOut',
			duration: 250,
			easing: 'ease-out'
		},
		width: '70%',
		height: '50%',
		layout: 'fit',
		cls: 'logo',
		items: [{
			xtype: 'textfield',
			name: 'stateDesc',
			id: 'stateDesc',
			placeHolder: 'Enter State',
			docked: 'top',
			// clearIcon: true,
			listeners: {
				keyup: function(textfield, e, eOpts) {
					mobEdu.enquire.f.onStateKeyup(textfield, e);
				},
				clearicontap: function(textfield, e, eOpts) {
					mobEdu.enquire.f.onStateItemClear(textfield, e);
				},
				blur: function(textfield, e, eOpts) {
					mobEdu.util.hideKeyboard();
				}
			}
		}, {
			xtype: 'dataview',
			id: 'stateList',
			name: 'stateList',
			cls: 'logo subjectlist',
			itemTpl: '<h3>{displayValue}</h3>',
			store: mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations'),
			singleSelect: true,
			loadingText: '',
			listeners: {
				itemtap: function(view, index, target, record, e, eOpts) {
					mobEdu.enquire.f.onStateItemTap(view, index, target, record, e, eOpts);
				}
			}
		}],		
		flex: 1,
		listeners: {
			hide: function() {
				var value = Ext.getCmp('stList').getValue();
				if (value == null || value == '') {
					Ext.getCmp('stList').setValue('select');
				}
				Ext.getCmp('stateDesc').setValue('');
				mobEdu.enquire.f.hideStatePopup();				
			}
		}
	}
});