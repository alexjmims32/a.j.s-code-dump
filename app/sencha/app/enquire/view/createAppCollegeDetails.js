Ext.define('mobEdu.enquire.view.createAppCollegeDetails', {
    extend: 'Ext.Panel',
    requires: [
        'mobEdu.enquire.f',
        'mobEdu.enquire.store.collegesJsonString'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        layout: 'fit',
        cls: 'logo',
        items: [{
            xtype: 'list',
            id: 'collegesList',
            name: 'collegesList',
            itemTpl: new Ext.XTemplate('<table width="100%">' +
                '<tr><th colspan="2"><h2>{collegeCode}</h2></th></tr>' +
                '<tr>' +
                '<td><h3>' +
                '<table width="100%">' +
                '<tr>' +
                '<td width="50%">{state}</td>' +
                '<td >{city}</td>' +
                '</tr>' +
                '<tr>' +
                '<td>{degree}</td>' +
                '<td>{gradDate}</td>' +
                '</tr>' +
                '</table></h3>' +
                '</td>' +
                '</td><td width="5%"  align="center"><img src="' + mobEdu.util.getResourcePath() + 'images/dropcourse.png"  width="32px" height="32px" name="collegeDrop"/></td>' +
                '</tr>' +
                '</table>'
            ),
            store: mobEdu.util.getStore('mobEdu.enquire.store.collegesJsonString'),
            singleSelect: true,
            loadingText: '',
            listeners: {
                itemtap: function(view, index, target, record, item, e, eOpts) {
                    mobEdu.enquire.f.onCollegesItemTap(index, view, record, item);
                }
            }
        }, {
            xtype: 'customToolbar',
            title: '<h1>Create Application</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Add',
                id: 'collegesAddButton',
                name: 'collegesAddButton',
                handler: function() {
                    mobEdu.enquire.f.showCollegesPopup();
                }
            }, {
                xtype: 'spacer'
            }, {
                text: 'Previous',
                handler: function() {
                    mobEdu.enquire.f.showCreateAppTestScores();
                }
            }, {
                text: 'Submit',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.submitCreateApplication();
                }
            }]
        }, {
            xtype: 'fieldset',
            title: 'Prior College Details',
            docked: 'top'
        }],
        flex: 1
    }
});