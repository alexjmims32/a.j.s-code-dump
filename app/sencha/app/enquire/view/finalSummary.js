Ext.define('mobEdu.enquire.view.finalSummary', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
                xtype: 'customToolbar',
                title: '<h1>Submit Profile</h1>'
            },
            // {
            //     xtype:'toolbar',
            //     docked:'top',
            //     title:'<h1>Summary</h1>',
            //     cls:'headerColor'
            // },
            {
                xtype: 'panel',
                name: 'finalsummary',
                id: 'finalsummary'
            }, {
                xtype: 'toolbar',
                docked: 'bottom',
                layout: {
                    pack: 'right'
                },
                // Previous Button
                items: [{
                        text: 'Previous',
                        handler: function() {
                            mobEdu.enquire.f.showInformationForm();
                        }
                    },
                    // Submit Button
                    {
                        text: 'Submit',
                        ui: 'confirm',
                        align: 'right',
                        handler: function() {
                            mobEdu.enquire.f.inquirySubmit();
                        }
                    }
                ]
            }

        ]
    }

});