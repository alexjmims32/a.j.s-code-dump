Ext.define('mobEdu.enquire.view.createAppTestScores', {
    extend: 'Ext.form.FormPanel',
    requires: [
        'mobEdu.enquire.f'
    ],
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        xtype: 'toolbar',
        items: [{
            xtype: 'customToolbar',
            title: '<h1>Create Application</h1>'
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Previous',
                handler: function() {
                    mobEdu.enquire.f.showCreateAppInterests();
                }
            }, {
                text: 'Next',
                ui: 'confirm',
                align: 'right',
                handler: function() {
                    mobEdu.enquire.f.loadCreateAppTestScores();
                }
            }]
        }, {
            xtype: 'fieldset',
            title: 'Test Scores'
        }, {
            xtype: 'fieldset',
            items: [{
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Test Code',
                name: 'appTCode',
                id: 'appTCode',
                useClearIcon: true,
                listeners: {
                    //                    keyup: function( textfield, e, eOpts ) {
                    //                        return mobEdu.inq.f.satScoreingKeyup(textfield);
                    //                    },
                    //                    blur: function( textfield, e, eOpts ) {
                    //                        mobEdu.enquire.f.satScoreing();
                    //                    }
                }
                //                maxLength:3
            }, {
                xtype: 'datepickerfield',
                labelWidth: '50%',
                label: 'Test Date',
                name: 'appTDate',
                id: 'appTDate',
                useClearIcon: true,
                listeners: {
                    //                        keyup: function( textfield, e, eOpts ) {
                    //                            return mobEdu.inq.f.satMathScoreingKeyup(textfield);
                    //                        },
                    //                        blur: function( textfield, e, eOpts ) {
                    //                            mobEdu.enquire.f.satMathScoreing();
                    //                        }
                }
                //                    maxLength:3
            }, {
                xtype: 'textfield',
                labelWidth: '50%',
                label: 'Test Score',
                name: 'appTScore',
                id: 'appTScore',
                useClearIcon: true,
                initialize: function() {
                    var me = this,
                        input = me.getInput().element.down('input');

                    input.set({
                        pattern: '[0-9]*'
                    });

                    me.callParent(arguments);
                },
                listeners: {
                    //                        keyup: function( textfield, e, eOpts ) {
                    //                            return mobEdu.inq.f.actScoreingKeyup(textfield);
                    //                        },
                    //                        blur: function( textfield, e, eOpts ) {
                    //                            mobEdu.enquire.f.actScoreing();
                    //                        }
                }
                //                    maxLength:3
            }]
        }]
    }
});