Ext.define('mobEdu.enquire.store.otherInterestsJsonString', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.otherInterestsJsonString'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.otherInterestsJsonString',
        autoLoad: false,
        model: 'mobEdu.enquire.model.otherInterestsJsonString',
        proxy:{
            type:'localstorage',
            id:'otherInterestsJson'
        }
    }
});