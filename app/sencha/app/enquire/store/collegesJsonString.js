Ext.define('mobEdu.enquire.store.collegesJsonString', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.collegesJsonString'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.collegesJsonString',
        autoLoad: false,
        model: 'mobEdu.enquire.model.collegesJsonString',
        proxy:{
            type:'localstorage',
            id:'collegesJson'
        }
    }
});