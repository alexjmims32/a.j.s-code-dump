Ext.define('mobEdu.enquire.store.subMenu', {
    extend: 'Ext.data.Store',
    //    alias: 'inquirySubMenuStore',

    requires: [
        'mobEdu.enquire.model.subMenu'
    ],

    config: {
        storeId: 'mobEdu.enquire.store.subMenu',
        autoLoad: false,
        model: 'mobEdu.enquire.model.subMenu',
        proxy: {
            type: 'localstorage',
            id: 'subMenu'
        }
    }
});