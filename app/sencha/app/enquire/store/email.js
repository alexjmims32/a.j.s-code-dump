Ext.define('mobEdu.enquire.store.email', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.email'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.email',
        autoLoad: false,
        model: 'mobEdu.enquire.model.email',
        proxy:{
            type:'localstorage',
            id:'email'
        }
    }
});