Ext.define('mobEdu.enquire.store.ethnicityEnumerations', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enquire.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.ethnicityEnumerations',
        autoLoad: false,
        model: 'mobEdu.enquire.model.enumerations',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enumerations'
            }
        }
    }

});