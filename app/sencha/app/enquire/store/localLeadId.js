Ext.define('mobEdu.enquire.store.localLeadId', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.email'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.localLeadId',
        autoLoad: false,
        model: 'mobEdu.enquire.model.email',
        proxy:{
            type:'localstorage',
            id:'email'
        }
    }
});