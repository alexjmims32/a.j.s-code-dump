Ext.define('mobEdu.enquire.store.testScoresGrid', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.testScoreJsonString'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.testScoresGrid',
        autoLoad: false,
        model: 'mobEdu.enquire.model.testScoreJsonString',
//        data:[{testCode:'abc' , testDate:'10/05/2011', testScore:'500'}]
        proxy:{
            type:'localstorage',
            id:'localtestScoresGrid'
        }
    }
});