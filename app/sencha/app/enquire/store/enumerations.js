Ext.define('mobEdu.enquire.store.enumerations', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enquire.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.enumerations',
        autoLoad: false,
        model: 'mobEdu.enquire.model.enumerations',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enumerations'
            }
        }
    }
//    ,
//    initProxy: function() {
//        var proxy = this.callParent();
//        proxy.reader.rootProperty= ''
//        return proxy;
//    }
});