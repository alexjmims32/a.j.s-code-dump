Ext.define('mobEdu.enquire.store.subrMenu', {
    extend: 'Ext.data.Store',
    //    alias: 'inquirySubMenuStore',

    requires: [
        'mobEdu.enquire.model.subrMenu'
    ],

    config: {
        storeId: 'mobEdu.enquire.store.subrMenu',
        autoLoad: false,
        model: 'mobEdu.enquire.model.subrMenu',
        proxy: {
            type: 'localstorage',
            id: 'subrMenu'
        }
    }
});