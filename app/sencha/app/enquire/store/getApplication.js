Ext.define('mobEdu.enquire.store.getApplication', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.getApplication'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.getApplication',
        autoLoad: false,
        model: 'mobEdu.enquire.model.getApplication',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'application'
            }
        }
    }
});
