Ext.define('mobEdu.enquire.store.application', {
    extend:'Ext.data.Store',
//    alias: 'inquiryApplicationStore',

    requires: [
        'mobEdu.enquire.model.application'
    ],

    config: {
        storeId: 'mobEdu.enquire.store.application',

        autoLoad: false,

        model: 'mobEdu.enquire.model.application',

      proxy : {
        type : 'ajax',
        reader: {
            type: 'json'
//            root: ' '
        }
        }
    }

});