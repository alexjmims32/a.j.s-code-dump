Ext.define( 'mobEdu.enquire.store.login',{
    extend:'Ext.data.Store',
//    alias: 'loginStore',

    requires: [
        'mobEdu.enquire.model.login'
    ],

    config: {
        storeId: 'mobEdu.enquire.store.login',

        autoLoad: false,

        model: 'mobEdu.enquire.model.login',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});
