Ext.define('mobEdu.enquire.store.profile', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.profile'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.profile',
        autoLoad: false,
        model: 'mobEdu.enquire.model.profile',
        proxy:{
            type:'localstorage',
            id:'profile'
        }
    }
});