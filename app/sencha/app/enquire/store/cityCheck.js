Ext.define('mobEdu.enquire.store.cityCheck', {
    extend:'Ext.data.Store',
//    alias: 'inquiryApplicationStore',

    requires: [
        'mobEdu.enquire.model.checkValues'
    ],

    config: {
        storeId: 'mobEdu.enquire.store.cityCheck',

        autoLoad: false,

        model: 'mobEdu.enquire.model.checkValues',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'xml',
                record:'Table',
                rootProperty:'NewDataSet'
            }
        }
    }

});
