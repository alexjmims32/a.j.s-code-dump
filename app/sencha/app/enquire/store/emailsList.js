Ext.define('mobEdu.enquire.store.emailsList', {
	extend: 'Ext.data.Store',
	//    alias: 'searchStore',

	requires: [
		'mobEdu.enquire.model.emailsList'
	],
	config: {
		storeId: 'mobEdu.enquire.store.emailsList',

		autoLoad: false,
		pageSize: 20,

		model: 'mobEdu.enquire.model.emailsList',

		proxy: {
			type: 'ajax',
			reader: {
				type: 'json',
				rootProperty: 'messages'
			}
		}
	}
});