Ext.define('mobEdu.enquire.store.localCreateApplication', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.localCreateApplication'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.localCreateApplication',
        autoLoad: false,
        model: 'mobEdu.enquire.model.localCreateApplication',
        proxy:{
            type:'localstorage',
            id:'localCreateApp'
        }
    }
});