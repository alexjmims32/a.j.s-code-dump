Ext.define('mobEdu.enquire.store.applicationJsonString', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.applicationJsonString'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.applicationJsonString',
        autoLoad: false,
        model: 'mobEdu.enquire.model.applicationJsonString',
        proxy:{
            type:'localstorage',
            id:'localCreateApp'
        }
    }
});