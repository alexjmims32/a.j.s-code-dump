Ext.define('mobEdu.enquire.store.visaTypeEnumerations', {
    extend:'Ext.data.Store',
//    extend:'com.n2n.data.store',

    requires: [
        'mobEdu.enquire.model.enumerations'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.visaTypeEnumerations',
        autoLoad: false,
        model: 'mobEdu.enquire.model.enumerations',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'enumerations'
            }
        }
    }

});