Ext.define('mobEdu.enquire.store.newEmail', {
    extend:'Ext.data.Store',
//    alias: 'searchStore',

    requires: [
        'mobEdu.enquire.model.newEmail'
    ],
    config: {
        storeId: 'mobEdu.enquire.store.newEmail',

        autoLoad: false,

        model: 'mobEdu.enquire.model.newEmail',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: 'emails'
            }
        }
    }
});