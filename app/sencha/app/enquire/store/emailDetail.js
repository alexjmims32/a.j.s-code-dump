Ext.define('mobEdu.enquire.store.emailDetail', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.emailsList'
    ],
    config: {
        storeId: 'mobEdu.enquire.model.emailsList',

        autoLoad: false,

        model: 'mobEdu.enquire.model.email',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: 'email'
            }
        }
    }
});