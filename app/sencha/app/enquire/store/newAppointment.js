Ext.define('mobEdu.enquire.store.newAppointment', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.enquire.model.newAppointment'
    ],
    config: {
        storeId: 'mobEdu.enquire.store.newAppointment',

        autoLoad: false,

        model: 'mobEdu.enquire.model.newAppointment',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json'
                //                rootProperty: 'emails'
            }
        }
    }
});