Ext.define('mobEdu.enquire.store.resetPassword', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.resetPassword'
    ],
    config: {
        storeId: 'mobEdu.enquire.store.resetPassword',

        autoLoad: false,

        model: 'mobEdu.enquire.model.resetPassword',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});