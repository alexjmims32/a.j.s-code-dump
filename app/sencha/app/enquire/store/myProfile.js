Ext.define('mobEdu.enquire.store.myProfile', {
    extend:'Ext.data.Store',
//    alias: 'searchStore',

    requires: [
        'mobEdu.enquire.model.myProfile'
    ],
    config: {
        storeId: 'mobEdu.enquire.store.myProfile',

        autoLoad: false,

        model: 'mobEdu.enquire.model.myProfile',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});
