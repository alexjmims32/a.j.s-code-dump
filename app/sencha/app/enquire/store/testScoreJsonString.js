Ext.define('mobEdu.enquire.store.testScoreJsonString', {
    extend:'Ext.data.Store',

    requires: [
        'mobEdu.enquire.model.testScoreJsonString'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.testScoreJsonString',
        autoLoad: false,
        model: 'mobEdu.enquire.model.testScoreJsonString',
        proxy:{
            type:'localstorage',
            id:'testScoreJson'
        }
    }
});