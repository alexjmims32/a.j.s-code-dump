Ext.define('mobEdu.enquire.store.appointmentDetail', {
    extend:'Ext.data.Store',
//      alias: 'mainStore',

    requires: [
        'mobEdu.enquire.model.appointmentsList'
    ],

    config:{
        storeId: 'mobEdu.enquire.store.appointmentDetail',
        autoLoad: false,
        model: 'mobEdu.enquire.model.appointmentsList',
//        proxy:{
//            type:'localstorage',
//            id:'appointmentDetail'
//        }

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: 'emails'
            }
        }
    }
});
