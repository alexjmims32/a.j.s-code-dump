Ext.define('mobEdu.enquire.store.viewEmail', {
    extend:'Ext.data.Store',
//    alias: 'searchStore',

    requires: [
        'mobEdu.enquire.model.emailsList'
    ],
    config: {
        storeId: 'mobEdu.enquire.store.viewEmail',

        autoLoad: false,

        model: 'mobEdu.enquire.model.emailsList',

        proxy : {
            type : 'ajax',
            reader: {
                type: 'json'
//                rootProperty: 'emails'
            }
        }
    }
});