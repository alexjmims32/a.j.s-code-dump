Ext.define('mobEdu.enquire.store.acceptance', {
    extend: 'Ext.data.Store',
    //    alias: 'searchStore',

    requires: [
        'mobEdu.enquire.model.appointmentsList'
    ],
    config: {
        storeId: 'mobEdu.enquire.store.acceptance',

        autoLoad: false,

        model: 'mobEdu.enquire.model.appointmentsList',

        proxy: {
            type: 'ajax',
            reader: {
                type: 'json',
                rootProperty: ''
            }
        }
    }
});