Ext.define('mobEdu.enquire.f', {
	requires: [
		'mobEdu.enquire.store.profile',
		'mobEdu.enquire.store.login',
		'mobEdu.enquire.store.subMenu',
		'mobEdu.enquire.store.myProfile',
		'mobEdu.enquire.store.resetPassword',
		'mobEdu.enquire.store.application',
		'mobEdu.enquire.store.emailsList',
		'mobEdu.enquire.store.viewEmail',
		'mobEdu.enquire.store.appointmentsList',
		'mobEdu.enquire.store.appointmentDetail',
		'mobEdu.enquire.store.enumerations',
		'mobEdu.enquire.store.newAppointment',
		'mobEdu.enquire.store.newEmail',
		'mobEdu.enquire.store.email',
		'mobEdu.enquire.store.getApplication',
		'mobEdu.enquire.store.localCreateApplication',
		'mobEdu.enquire.store.raceEnumerations',
		'mobEdu.enquire.store.ethnicityEnumerations',
		'mobEdu.enquire.store.stateEnumerations',
		'mobEdu.enquire.store.countryEnumerations',
		'mobEdu.enquire.store.countyEnumerations',
		'mobEdu.enquire.store.visaTypeEnumerations',
		'mobEdu.enquire.store.visaNationEnumerations',
		'mobEdu.enquire.store.schoolCodeEnumerations',
		'mobEdu.enquire.store.parentEnumerations',
		'mobEdu.enquire.store.termEnumerations',
		'mobEdu.enquire.store.levelCodeEnumerations',
		'mobEdu.enquire.store.majorCodeEnumerations',
		'mobEdu.enquire.store.studentTypeEnumerations',
		'mobEdu.enquire.store.admissionTypeEnumerations',
		'mobEdu.enquire.store.residenceCodeEnumerations',
		'mobEdu.enquire.store.collegeCodeEnumerations',
		'mobEdu.enquire.store.degreeCodeEnumerations',
		'mobEdu.enquire.store.departmentEnumerations',
		'mobEdu.enquire.store.campusEnumerations',
		'mobEdu.enquire.store.eduGoalEnumerations',
		'mobEdu.enquire.store.appStatusEnumerations',
		'mobEdu.enquire.store.testScoresGrid',
		'mobEdu.enquire.store.collegesJsonString',
		'mobEdu.enquire.store.pInterestEnumerations',
		'mobEdu.enquire.store.testCodeEnumerations',
		'mobEdu.enquire.store.priorCCodeEnumerations',
		'mobEdu.enquire.store.priorDegreeEnumerations',
		'mobEdu.enquire.store.applicationJsonString',
		'mobEdu.enquire.store.appointmentDetail',
		'mobEdu.enquire.store.subrMenu',
		// 'mobEdu.enquire.main.store.entourage',
		'mobEdu.enquire.model.testScoreJsonString',
		'mobEdu.enquire.model.collegesJsonString',
		'mobEdu.enquire.store.acceptance'
	],
	statics: {
		rinfstateCode: null,
		cAppStateCode: null,
		highSchoolStateCode: null,
		showLeadMainView: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
			store.currentPage = 1;
			var title = mobEdu.util.getTitleInfo('ENQ');
			mobEdu.enquire.f.showSubMenu();
			//            Ext.getCmp('enquireTitle').setTitle('<h1>' + title + '</h1>');
			Ext.getCmp('inqSubMenuList').refresh();
		},

		showBasicInfo: function() {
			mobEdu.util.show('mobEdu.enquire.view.basicInfo');
			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();

			var record = store.getAt(0);
			if (record != undefined) {
				Ext.getCmp('firstname').setValue(record.data.firstName);
				Ext.getCmp('lastname').setValue(record.data.lastName);
				if (record.data.gender != '' && record.data.gender != undefined && record.data.gender != null) {
					Ext.getCmp('gender').setValue(record.data.gender);
				}
				if (record.data.dateOfBirth != '' && record.data.dateOfBirth != undefined && record.data.dateOfBirth != null) {
					Ext.getCmp('dateofbirth').setValue(new Date(record.data.dateOfBirth));
				}
			}
		},
		showContactAddressInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showBasicInfo);
			mobEdu.util.show('mobEdu.enquire.view.contactAddressInfo');
			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				Ext.getCmp('address').setValue(record.data.cAddress);
				Ext.getCmp('add2').setValue(record.data.cAddress2);
				Ext.getCmp('add3').setValue(record.data.cAddress3);
				Ext.getCmp('city').setValue(record.data.cCity);
				if (record.data.cState != '' && record.data.cState != undefined && record.data.cState != null) {
					Ext.getCmp('states').setValue(record.data.cState);
				}
				Ext.getCmp('zip').setValue(record.data.cZip);
				Ext.getCmp('country').setValue(record.data.cCountry);
			}
		},
		showContactPhoneInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showContactAddressInfo);
			mobEdu.util.show('mobEdu.enquire.view.contactPhoneInfo');
			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				Ext.getCmp('phone1').setValue(record.data.phone1);
				Ext.getCmp('email').setValue(record.data.email);
				Ext.getCmp('email2').setValue(record.data.vemail);
				Ext.getCmp('password').setValue(record.data.password);
				Ext.getCmp('vpassword').setValue(record.data.vPassword);
				Ext.getCmp('userName').setValue(record.data.userName);
			}
		},

		showAdmissionsInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showContactPhoneInfo);
			mobEdu.util.show('mobEdu.enquire.view.admissions');
			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				if (record.data.classification != '' && record.data.classification != undefined && record.data.classification != null) {
					Ext.getCmp('class').setValue(record.data.classification);
				}
				Ext.getCmp('intMaj').setValue(record.data.intendedMajor);
				Ext.getCmp('highS').setValue(record.data.highSchool);
				if (record.data.whatTypeInf != '' && record.data.whatTypeInf != undefined && record.data.whatTypeInf != null) {
					Ext.getCmp('inf').setValue(record.data.whatTypeInf);
				}
			}
		},

		showRequestInformationFrom: function() {
			mobEdu.enquire.f.loadStateEnumerations();
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enquire.view.requestForInformation');
			Ext.getCmp('intendText').hide();
			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				Ext.getCmp('firstname').setValue(record.data.firstName);
				Ext.getCmp('lastname').setValue(record.data.lastName);
				if (record.data.gender != '' && record.data.gender != undefined && record.data.gender != null) {
					Ext.getCmp('gender').setValue(record.data.gender);
				}
				if (record.data.dateOfBirth != '' && record.data.dateOfBirth != undefined && record.data.dateOfBirth != null) {
					Ext.getCmp('dateofbirth').setValue(new Date(record.data.dateOfBirth));
				}
				Ext.getCmp('address').setValue(record.data.cAddress);
				Ext.getCmp('add2').setValue(record.data.cAddress2);
				//                Ext.getCmp('add3').setValue(record.data.cAddress3);
				Ext.getCmp('city').setValue(record.data.cCity);
				if (record.data.cState != '' && record.data.cState != undefined && record.data.cState != null) {
					Ext.getCmp('stList').setValue(record.data.cStateDesc);
					mobEdu.enquire.f.rinfstateCode = record.data.cState;
				}
				Ext.getCmp('zip').setValue(record.data.cZip);
				Ext.getCmp('country').setValue(record.data.cCountry);
				Ext.getCmp('phone1').setValue(record.data.phone1);
				Ext.getCmp('email').setValue(record.data.email);
				Ext.getCmp('email2').setValue(record.data.vemail);
				Ext.getCmp('password').setValue(record.data.password);
				Ext.getCmp('vpassword').setValue(record.data.vPassword);
				Ext.getCmp('userName').setValue(record.data.userName);
				if (record.data.classification != '' && record.data.classification != undefined && record.data.classification != null) {
					Ext.getCmp('class').setValue(record.data.classification);
				}
				Ext.getCmp('intMaj').setValue(record.data.intendedMajor);
				Ext.getCmp('asScore').setValue(record.data.actScore);
				Ext.getCmp('highS').setValue(record.data.highSchool);
				if (record.data.reqInfo != '' && record.data.reqInfo != undefined && record.data.reqInfo != null) {
					Ext.getCmp('reqInf').setValue(record.data.reqInfo);
				}
				if (record.data.whatTypeInf != '' && record.data.whatTypeInf != undefined && record.data.whatTypeInf != null) {
					Ext.getCmp('inf').setValue(record.data.whatTypeInf);
				}
			}
		},

		logout: function() {
			mobEdu.util.updateLeadId(null);
			mobEdu.util.updateLeadAuthString(null);

			window.open('index.html', '_self');

		},

		loadLoginView: function() {
			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.enquire.view.phone.login');
			} else {
				mobEdu.util.show('mobEdu.enquire.view.tablet.login');
			}
		},

		showInformationForm: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enquire.view.requestForInformation');
		},
		showSubMenu: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enquire.view.subMenu');
			Ext.getCmp('inqSubMenuList').refresh();
		},
		showContactRecruiter: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enquire.view.contactRecruiter');
		},
		showScheduleVisit: function() {
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.util.show('mobEdu.enquire.view.scheduleVisit');
		},

		resetBasicInfo: function() {
			mobEdu.util.get('mobEdu.enquire.view.basicInfo');
		},
		resetContactAddressInfo: function() {
			mobEdu.util.get('mobEdu.enquire.view.contactAddressInfo');
		},
		resetContactPhoneInfo: function() {
			mobEdu.util.get('mobEdu.enquire.view.contactPhoneInfo');
		},

		resetContactRecruiter: function() {
			mobEdu.util.get('mobEdu.enquire.view.contactRecruiter');
		},
		resetScheduleVisit: function() {
			mobEdu.util.get('mobEdu.enquire.view.scheduleVisit');
		},
		initializeSubMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subMenu');
			//            store.load();
			store.removeAll();
			store.removed = [];
			//            store.getProxy().clear();
			store.sync();

			mobEdu.enquire.f.getLeadId();
			mobEdu.enquire.f.getPrivileges();

			if (mobEdu.enquire.f.leadPrivilegesChecking('MY PROFILE')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/profile.png',
					title: 'My Profile',
					action: mobEdu.enquire.f.loadProfileDetail
				});
				store.sync();
			}
			if (mobEdu.enquire.f.leadPrivilegesChecking('MESSAGES')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/student-mail.png',
					title: 'Messages',
					action: mobEdu.enquire.f.loadEmailList
				});
				store.sync();
			}

			if (mobEdu.enquire.f.leadPrivilegesChecking('APPOINTMENTS')) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/student-history.png',
					title: 'Appointments',
					action: mobEdu.enquire.f.showAppointments
				});
				store.sync();
			}

			if (mobEdu.enquire.f.leadPrivilegesChecking('ABOUT ' + campusCode)) {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About ' + campusCode,
					action: mobEdu.enquire.f.showAboutSubr
				});
				store.sync();
			}
			if (campusCode == 'UNG') {
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			}
			if (campusCode == 'FVSU') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadFVSUAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About FVSU',
					action: mobEdu.enquire.f.loadAboutFVSU
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'WSSU') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadWSSUAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About WSSU',
					action: mobEdu.enquire.f.loadAboutWSSU
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'SCF') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadSCFAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About SCF',
					action: mobEdu.enquire.f.loadAboutSCF
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'OC') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadOCAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About OC',
					action: mobEdu.enquire.f.loadAboutOC
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'RSV') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadRSVAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About OC',
					action: mobEdu.enquire.f.loadAboutRSV
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'UNCFSU') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadUNCFSUAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About OC',
					action: mobEdu.enquire.f.loadAboutUNCFSU
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'BBEDEMO') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadBBEAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About OC',
					action: mobEdu.enquire.f.loadAboutBBE
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'LCC') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadLCCAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About OC',
					action: mobEdu.enquire.f.loadAboutLCC
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else if (campusCode == 'CSCC') {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admissions',
					action: mobEdu.enquire.f.loadCSCCAdmissions
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'About CSCC',

					action: mobEdu.enquire.f.loadAboutCSCC
				});
				store.sync();
				// Ext.getCmp('enquireTitle').setTitle('UNG Admissions');
			} else {
				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Quick Facts About ' + campusCode,
					action: mobEdu.enquire.f.loadQuickFacts
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'Admission Requirements',
					action: mobEdu.enquire.f.loadAdmissionRequirements
				});
				store.sync();

				store.add({
					img: mobEdu.util.getResourcePath() + 'images/menu/clientLogo.png',
					title: 'First Time Freshmen Admissions Application',
					action: mobEdu.enquire.f.loadApplyingAdmissions
				});
				store.sync();
			}



			// if (mobEdu.enquire.f.leadPrivilegesChecking('NOTIFICATIONS')) {
			// 	Ext.getCmp("enquireNotificationBtn").show();
			// 	if (mobEdu.util.getLeadId() != null && mobEdu.util.getLeadAuthString() != null) {
			// 		//                Ext.getCmp("logBtn").setIcon(mobEdu.util.getResourcePath() + 'images/logout.png');
			// 		//                cartFlag='login';
			// 		Ext.getCmp("enquireNotificationBtn").show();

			// 		//Load notifications for FirstTime
			// 		mobEdu.enquire.notif.f.enquireLoadNotifications();
			// 		//Load notifications for every minute
			// 		var interval = setInterval(function() {
			// 			mobEdu.enquire.notif.f.enquireLoadNotifications();
			// 		}, 60000);

			// 	} else {
			// 		Ext.getCmp("enquireNotificationBtn").hide();
			// 	}
			// }
			//            store.load();
		},

		getLeadId: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var leadId = rec.data.leadId;
				return leadId;
			}
			return null;
		},

		getPrivileges: function() {
			var entourageStore = mobEdu.util.getStore('mobEdu.main.store.entourage');
			if (entourageStore.data.length > 0) {
				var rec = entourageStore.getAt(0);
				var privileges = rec.data.privileges;
				return privileges;
			}
			return null;
		},
		leadPrivilegesChecking: function(priStaticName) {
			var loginStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
			var response = loginStore.data.all;
			for (var j = 0; j < response.length; j++) {
				var position = response[j].data.position;
				var moduledesCode = response[j].data.code;
				if (position == 'ENQUIRE' && moduledesCode == priStaticName) {
					return true;
				}
			}
			return false;
			/*var privileges = mobEdu.enquire.f.getPrivileges();
			if (privileges != null) {
				for (var i = 0; i < privileges.length; i++) {
					var priName = privileges[i].name;
					if (priStaticName == priName) {
						return true;
					}
				}
			}
			return false;*/
		},

		loadProfileDetail: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');

			profileStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			profileStore.getProxy().setUrl(enquireWebserver + 'enquire/lead/get');
			profileStore.getProxy().setExtraParams({
				companyId: companyId
			});
			profileStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadProfileDetailResponseHandler();
			};
			profileStore.load();
		},
		loadProfileDetailResponseHandler: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var eStatus = profileStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success') {
				mobEdu.enquire.f.showProfileDetails();
			} else {
				Ext.Msg.show({
					title: null,
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showProfileDetails: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var storeData = profileStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showLeadMainView);
			mobEdu.util.show('mobEdu.enquire.view.myProfile');
			var applicationId = storeData[0].data.applicationID;
			if (applicationId == null) {
				Ext.getCmp('createApp').show();
				Ext.getCmp('viewApp').hide();
			} else {
				Ext.getCmp('createApp').hide();
				Ext.getCmp('viewApp').show();
			}
			Ext.getCmp('profDetail').setTitle('<h1>' + leadTitle + '</h1>');
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var middleValue;
			var firstName = profileData[0].data.firstName;
			var lastName = profileData[0].data.lastName;
			middleValue = firstName + ' ' + lastName;
			var dateOfBirth = profileData[0].data.birthDate;
			var leadStatus = profileData[0].data.leadStatus;
			var address1 = profileData[0].data.address1;
			var address2 = profileData[0].data.address2;
			var address3 = profileData[0].data.address3;
			var cityValue = profileData[0].data.city;
			var stateValue = profileData[0].data.state;
			var countryName = profileData[0].data.country;
			var emailId1 = profileData[0].data.email;
			var phoneNo11 = profileData[0].data.phone1;
			var phoneNo1 = phoneNo11.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
			var recqName = profileData[0].data.recruiterName;
			var addType = profileData[0].data.addressType;
			var ldStatus = profileData[0].data.leadStatus;
			var updatedDate = profileData[0].data.versionDate;
			var updatedBy = profileData[0].data.versionUser;
			var classText = profileData[0].data.classification;
			var schoolName = profileData[0].data.highSchool;
			var intMajor = profileData[0].data.intendedMajor;
			var scoresValues = profileData[0].data.actScore;
			var reqInfo = profileData[0].data.requestInfo;
			var lookInfo = profileData[0].data.lookInfo;
			//            var addressText;
			//            if(address2 == ''){
			//                addressText = address1;
			//            } else{
			//                addressText = address1 + ',' + address2;
			//            }

			var finalSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + dateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>';
			if (classText != '' && classText != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Classification:</h2></td>' + '<td align="left"><h3>' + classText + '</h3></td></tr>';
			}
			if (schoolName != '' && schoolName != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>High School:</h2></td>' + '<td align="left"><h3>' + schoolName + '</h3></td></tr>';
			}
			if (intMajor != '' && intMajor != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Intended Major:</h2></td>' + '<td align="left"><h3>' + intMajor + '</h3></td></tr>';
			}
			if (scoresValues != '' && scoresValues != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>ACT/SAT Score:</h2></td>' + '<td align="left"><h3>' + scoresValues + '</h3></td></tr>';
			}
			if (reqInfo != '' && reqInfo != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>Who is requesting the <br/> information?:</h2></td>' + '<td align="left"><h3>' + reqInfo + '</h3></td></tr>';
			}
			if (lookInfo != '' && lookInfo != null) {
				finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>What type of information <br/> are you looking for?:</h2></td>' + '<td align="left"><h3>' + lookInfo + '</h3></td></tr>';
			}
			finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Status:</h2></td>' + '<td align="left"><h3>' + leadStatus + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Recruiter Name:</h2></td>' + '<td align="left"><h3>' + recqName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Last updated date:</h2></td>' + '<td align="left"><h3>' + updatedDate + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Last updated by:</h2></td>' + '<td align="left"><h3>' + updatedBy + '</h3></td></tr></table>';


			Ext.getCmp('leadProfileSummary').setHtml(finalSummaryHtml);

		},

		showResetPassword: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showProfileDetails);
			mobEdu.util.show('mobEdu.enquire.view.resetPassword');
		},

		saveResetPassword: function() {
			var newPass = Ext.getCmp('newPassword').getValue();
			var newVPassword = Ext.getCmp('vNewPassword').getValue();
			if (newPass != null && newPass != '' && newVPassword != null && newVPassword != '') {
				var passwordLength = Ext.getCmp('newPassword').getValue().length;
				if (passwordLength < 4) {
					Ext.Msg.show({
						id: 'newPassMsg',
						name: 'newPassMsg',
						title: null,
						cls: 'msgbox',
						message: '<p>Password should be greater than 4 characters.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								newPasswordField.focus(true);
							}
						}
					});
				} else {
					if (newPass != newVPassword) {
						Ext.Msg.show({
							title: null,
							message: '<p>New Password and Verify Password must be same.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					} else {
						var regExp = new RegExp("^[a-zA-Z0-9@*+=$#&_()/|\\-]+$");
						if (!regExp.test(newPass)) {
							Ext.Msg.show({
								title: null,
								message: '<p>password allows  @  _ * + = $ # & _ ( ) / |  special characters only.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						} else {
							mobEdu.enquire.f.submitResetPassword();
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		submitResetPassword: function() {
			var newPass = Ext.getCmp('newPassword').getValue();
			var newVPassword = Ext.getCmp('vNewPassword').getValue();
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var resetStore = mobEdu.util.getStore('mobEdu.enquire.store.resetPassword');

			resetStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			resetStore.getProxy().setUrl(enquireWebserver + 'enquire/lead/resetpassword');
			resetStore.getProxy().setExtraParams({
				leadID: viewLeadId,
				password: newPass,
				companyId: companyId
			});
			resetStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.submitResetPasswordResponseHandler();
			};
			resetStore.load();
		},
		submitResetPasswordResponseHandler: function() {
			var resetStore = mobEdu.util.getStore('mobEdu.enquire.store.resetPassword');
			var eStatus = resetStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success') {
				Ext.Msg.show({
					id: 'passchange',
					title: null,
					cls: 'msgbox',
					message: '<p>Password updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.util.updateStudentId(null);
							//                            mobEdu.enquire.f.showProfileDetails();
							mobEdu.util.updateLeadAuthString(null);
							mobEdu.util.updateAuthString(null);
							mobEdu.util.updateLeadId(null);
							// mobEdu.storeUtil.clearEnqNofiLocalStore();
							window.open('index.html', '_self');
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},

		showUpdateProfile: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var storeData = profileStore.data.all;
			var fName = storeData[0].data.firstName;
			var lName = storeData[0].data.lastName;
			var leadTitle = fName + ' ' + lName;
			var dateOfBirth = storeData[0].data.birthDate;
			var email = storeData[0].data.email;
			var phone = storeData[0].data.phone1;
			var newStartDate = Ext.Date.parse(dateOfBirth, "Y-m-d");
			var dobFormatted;
			if (dateOfBirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([newStartDate.getMonth() + 1] + '/' + newStartDate.getDate() + '/' + newStartDate.getFullYear());
			}
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showProfileDetails);
			mobEdu.util.show('mobEdu.enquire.view.profileUpdate');
			Ext.getCmp('updateTitle').setTitle('<h1>' + leadTitle + '</h1>');
			Ext.getCmp('upFName').setValue(storeData[0].data.firstName);
			Ext.getCmp('upLName').setValue(storeData[0].data.lastName);
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('upProdate').setValue(new Date(dobFormatted));
			}
			Ext.getCmp('upEmail').setValue(storeData[0].data.email);
			Ext.getCmp('upEmail').setValue(storeData[0].data.email);
			Ext.getCmp('upPhone').setValue(storeData[0].data.phone1);
			if (storeData[0].data.classification != '' && storeData[0].data.classification != undefined && storeData[0].data.classification != null) {
				Ext.getCmp('upClass').setValue(storeData[0].data.classification);
			}
			Ext.getCmp('upHighS').setValue(storeData[0].data.highSchool);
			Ext.getCmp('upIntMaj').setValue(storeData[0].data.intendedMajor);
			Ext.getCmp('upAsScore').setValue(storeData[0].data.actScore);
			if (storeData[0].data.requestInfo != '' && storeData[0].data.requestInfo != undefined && storeData[0].data.requestInfo != null) {
				Ext.getCmp('upReqInf').setValue(storeData[0].data.requestInfo);
			}
			if (storeData[0].data.lookInfo != '' && storeData[0].data.lookInfo != undefined && storeData[0].data.lookInfo != null) {
				Ext.getCmp('upInf').setValue(storeData[0].data.lookInfo);
			}
		},

		saveUpdateProfile: function() {
			var updateFName = Ext.getCmp('upFName').getValue();
			var updateLName = Ext.getCmp('upLName').getValue();
			var updateBirthDate = Ext.getCmp('upProdate').getValue();
			var updateEmail = Ext.getCmp('upEmail').getValue();
			var updatePhone = Ext.getCmp('upPhone').getValue();
			updatePhone = updatePhone.replace(/\D/g, '');
			var updateClassText = Ext.getCmp('upClass').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMaj').getValue();
			var updateSchoolName = Ext.getCmp('upHighS').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInf').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScore').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInf').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = Ext.Date.format(updateBirthDate, 'Y-m-d');
			}
			if (updateFName != null && updateFName != '' && updateLName != null && updateLName != '' && dobFormatted != null && dobFormatted != '' && updateEmail != null && updateEmail != '' && updatePhone != null && updatePhone != '' && updateIntMajor != null && updateIntMajor != '' && updateSchoolName != null && updateSchoolName != '') {
				var firstName = Ext.getCmp('upFName');
				var regExp = new RegExp("^[a-zA-Z ]+$");
				if (!regExp.test(updateFName)) {
					Ext.Msg.show({
						id: 'updateFNmae',
						name: 'updateFNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid First Name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					if (updateFName.charAt(0) == ' ') {
						Ext.Msg.show({
							id: 'updateFNmae',
							name: 'updateFNmae',
							title: null,
							cls: 'msgbox',
							message: '<p>First Name must start with alphabet.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									firstName.focus(true);
								}
							}
						});
					} else {
						var lastName = Ext.getCmp('upLName');
						var lastNRegExp = new RegExp("^[a-zA-Z ]+$");
						if (!lastNRegExp.test(updateLName)) {
							Ext.Msg.show({
								id: 'updateLName',
								name: 'updateLName',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid Last Name.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										lastName.focus(true);
									}
								}
							});
						} else {
							if (updateLName.charAt(0) == ' ') {
								Ext.Msg.show({
									id: 'updateLName',
									name: 'updateLName',
									title: null,
									cls: 'msgbox',
									message: '<p>Last Name must start with alphabet.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											firstName.focus(true);
										}
									}
								});
							} else {
								var updatePhone1 = Ext.getCmp('upPhone');
								var updatePhoneValue = updatePhone1.getValue();
								updatePhoneValue = updatePhoneValue.replace(/-|\(|\)/g, "");

								var length = updatePhoneValue.length;
								if (updatePhoneValue.length != 10) {
									Ext.Msg.show({
										id: 'phonemsg',
										name: 'phonemsg',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid Phone Number.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												updatePhone1.focus(true);
											}
										}
									});
								} else {
									var a = updatePhoneValue.replace(/\D/g, '');
									var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
									updatePhone1.setValue(newValue);
									var updateMail = Ext.getCmp('upEmail');
									var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
									if (updateEmail != '' && regMail.test(updateEmail) == false) {
										Ext.Msg.show({
											id: 'upMail',
											name: 'upMail',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid Email address.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													updateMail.focus(true);
												}
											}
										});
									} else {
										var schoolName = Ext.getCmp('upHighS');
										var schoolNameRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
										if (!schoolNameRegExp.test(updateSchoolName)) {
											Ext.Msg.show({
												id: 'hSchool',
												name: 'hSchool',
												title: null,
												cls: 'msgbox',
												message: '<p>Invalid High School.</p>',
												buttons: Ext.MessageBox.OK,
												fn: function(btn) {
													if (btn == 'ok') {
														schoolName.focus(true);
													}
												}
											});
										} else {
											var intMajor = Ext.getCmp('upIntMaj');
											var intMajorRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
											if (!intMajorRegExp.test(updateIntMajor)) {
												Ext.Msg.show({
													id: 'intMName',
													name: 'intMName',
													title: null,
													cls: 'msgbox',
													message: '<p>Invalid Intended Major.</p>',
													buttons: Ext.MessageBox.OK,
													fn: function(btn) {
														if (btn == 'ok') {
															intMajor.focus(true);
														}
													}
												});
											} else {
												mobEdu.enquire.f.loadUpdateProfile();
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadUpdateProfile: function() {
			var updateFName = Ext.getCmp('upFName').getValue();
			var updateLName = Ext.getCmp('upLName').getValue();
			var updateBirthDate = Ext.getCmp('upProdate').getValue();
			var updateEmail = Ext.getCmp('upEmail').getValue();
			var updatePhone = Ext.getCmp('upPhone').getValue();
			var updateClassText = Ext.getCmp('upClass').getValue();
			if (updateClassText == 'select') {
				updateClassText = '';
			}
			var updateIntMajor = Ext.getCmp('upIntMaj').getValue();
			var updateSchoolName = Ext.getCmp('upHighS').getValue();
			var updateWhatTypeInf = Ext.getCmp('upInf').getValue();
			if (updateWhatTypeInf == 'select') {
				updateWhatTypeInf = '';
			}
			var updateActSatScore = Ext.getCmp('upAsScore').getValue();
			updateActSatScore = updateActSatScore.replace(/\D/g, '');
			var updateRequestInfo = Ext.getCmp('upReqInf').getValue();
			if (updateRequestInfo == 'select') {
				updateRequestInfo = '';
			}
			var dobFormatted;
			if (updateBirthDate == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (updateBirthDate.getFullYear() + '-' + [updateBirthDate.getMonth() + 1] + '-' + updateBirthDate.getDate());
			}
			var updateProfileStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
			updateProfileStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			updateProfileStore.getProxy().setUrl(enquireWebserver + 'enquire/lead/update');
			updateProfileStore.getProxy().setExtraParams({
				firstName: updateFName,
				lastName: updateLName,
				email: updateEmail,
				phone1: updatePhone,
				birthDate: dobFormatted,
				classification: updateClassText,
				highSchool: updateSchoolName,
				intendedMajor: updateIntMajor,
				actScore: updateActSatScore,
				requestInfo: updateRequestInfo,
				lookInfo: updateWhatTypeInf,
				companyId: companyId
			});
			updateProfileStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadUpdateProfileResponseHandler();
			};
			updateProfileStore.load();
		},

		loadUpdateProfileResponseHandler: function() {
			var updateProfileStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
			var updateStatus = updateProfileStore.getProxy().getReader().rawData;
			if (updateStatus.status == 'success') {
				Ext.Msg.show({
					id: 'updateProfile',
					title: null,
					cls: 'msgbox',
					message: '<p>Profile updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var str = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
							str.load();
							mobEdu.util.show('mobEdu.enquire.view.myProfile');
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: updateStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadEmailList: function() {
			mobEdu.enquire.f.getLeadRecruiterID();
			var searchText = '';
			var label = '';
			var fromId = '';
			var entType = 'LEAD';
			var eStore = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/myMessages');
			eStore.getProxy().setExtraParams({
				label: label,
				searchText: searchText,
				fromID: fromId,
				companyId: companyId
			});
			eStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadEmailResponseHandler();
			};
			eStore.load();
		},
		loadEmailResponseHandler: function() {
			var emailStore = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
			emailStore.sort('versionDate', 'DESC');
			var eStatus = emailStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success' || eStatus.status == 'No Data') {
				mobEdu.enquire.f.showEmailList();
				if ((!(Ext.os.is.Phone))) {
					if (emailStore.data.length > 0) {
						mobEdu.enquire.f.loadTabletEmailDetail(emailStore.getAt(0));
						Ext.getCmp("emailsListT").select(0);
						Ext.getCmp('replyT').show();
					} else {
						Ext.getCmp('replyT').hide();
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: eStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		setPagingPlugin: function(listComponent) {
			if (Ext.getCmp('messageplugin') == null) {
				var pArray = new Array();
				pArray[0] = mobEdu.enquire.view.paging.create();
				listComponent.setPlugins(pArray);
			}
			Ext.getCmp('messageplugin').getLoadMoreCmp().show();
		},
		checkForSearchListEnd: function(store, records, isSuccessful) {
			var pageSize = store.getPageSize();
			var pageIndex = store.currentPage - 1; // Page numbers start at 1

			if (isSuccessful && records.length < pageSize) {
				//Set count to disable 'loading' message
				var totalRecords = pageIndex * pageSize + records.length;
				store.setTotalCount(totalRecords);
			} else
				store.setTotalCount(null);

		},
		hasEmailDirection: function(direction) {
			if (direction == "INBOX") {
				return true;
			} else {
				return false;
			}
		},

		hasReadFlag: function(status) {
			if (status == "1") {
				return false;
			} else {
				return true;
			}
		},

		showEmailList: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showLeadMainView);
			if (Ext.os.is.Phone) {
				mobEdu.util.show('mobEdu.enquire.view.phone.emailsList');
			} else {
				mobEdu.util.show('mobEdu.enquire.view.tablet.emailsList');
			}
		},

		emailListBackButton: function() {
			var eListStore = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
			eListStore.currentPage = 1;
			eListStore.load();
			mobEdu.enquire.f.showEmailList();
		},

		loadTabletEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');

			detailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			detailStore.getProxy().setUrl(commonwebserver + 'message/get');
			detailStore.getProxy().setExtraParams({
				messageID: eId,
				companyId: companyId
			});
			//            detailStore.getProxy().afterRequest = mobEdu.enquire.f.loadEmaildetailResponseHandler;
			detailStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadTabletEmailDetailResponseHandler();
			};
			detailStore.load();
		},
		loadTabletEmailDetailResponseHandler: function() {
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');
			if (detailStore.data.length > 0) {
				if (detailStore.data.first().data.label == 'INBOX' && detailStore.data.first().data.fromName != 'System Generated') {
					Ext.getCmp('replyT').show();
				} else {
					Ext.getCmp('replyT').hide();
				}
			};
			var eDetailStatus = detailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status != 'success') {
				Ext.Msg.show({
					title: null,
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},

		loadEmailDetail: function(record) {
			var rec = record.data;
			rec.readFlag = '1';
			var eId = rec.messageID;

			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');

			detailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			detailStore.getProxy().setUrl(commonwebserver + 'message/get');
			detailStore.getProxy().setExtraParams({
				messageID: eId,
				companyId: companyId
			});
			detailStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadEmaildetailResponseHandler();
			};
			detailStore.load();
		},
		loadEmaildetailResponseHandler: function(testObj) {
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');
			var eDetailStatus = detailStore.getProxy().getReader().rawData;
			if (eDetailStatus.status == 'success') {
				mobEdu.enquire.f.showEmailDetail();
			} else {
				Ext.Msg.show({
					title: null,
					message: eDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showEmailDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.emailListBackButton);
			mobEdu.util.show('mobEdu.enquire.view.phone.emailDetail');
			var messagesStore = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');
			var messageData = messagesStore.data.all;
			for (var i = 0; i < messageData.length; i++) {
				var directionLabels = messageData[i].data.labels;
				var from = messageData[i].data.fromName;
				for (var l = 0; l < directionLabels.length; l++) {
					var direction = directionLabels[l].label;
				}
			}
			if (direction == "SENT") {
				Ext.getCmp('replyToolbar').hide();
			} else {
				Ext.getCmp('replyToolbar').show();
			}
			if (from == "System Generated") {
				Ext.getCmp('replyToolbar').hide();
			}
		},

		showNewMail: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showEmailList);
			mobEdu.util.show('mobEdu.enquire.view.newEmail');
		},

		sendMail: function() {
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();
			esub = esub.trim();
			msgArea = msgArea.trim();
			if (msgArea != '' && esub != '') {
				mobEdu.enquire.f.getLeadRecruiterID();
				mobEdu.enquire.f.sentNewEmail();
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadLeadProfile: function() {
			var Store = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');

			Store.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			Store.getProxy().setUrl(enquireWebserver + 'enquire/lead/get');
			Store.getProxy().setExtraParams({
				companyId: companyId
			});
			Store.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadLeadProfileResponseHandler();
			};
			Store.load();
		},
		loadLeadProfileResponseHandler: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var eStatus = profileStore.getProxy().getReader().rawData;
			if (eStatus.status == 'success') {
				var storeData = profileStore.data.all;
				reqId = storeData[0].data.recruiterID;
			}
		},

		getLeadRecruiterID: function() {
			mobEdu.enquire.f.loadLeadProfile();
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var storeData = profileStore.data.all;
			if (storeData.length == 0) {
				reqId = '';
			} else {
				reqId = storeData[0].data.recruiterID;
			}
			if (reqId == undefined) {
				reqId = '';
			}
		},

		sentNewEmail: function() {
			var esub = Ext.getCmp('newsub').getValue();
			var msgArea = Ext.getCmp('newmsg').getValue();
			var conversationID = '';
			var eStore = mobEdu.util.getStore('mobEdu.enquire.store.newEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/send');
			eStore.getProxy().setExtraParams({
				to: reqId,
				subject: esub,
				body: msgArea,
				conversationID: conversationID,
				companyId: companyId
			});
			eStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.newEmailResponseHandler();
			};
			eStore.load();
		},

		newEmailResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enquire.store.newEmail');
			var NEStatus = nEStore.getProxy().getReader().rawData;
			if (NEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'mailsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Message sent successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							var store = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
							store.getProxy().setExtraParams({
								page: 1,
								start: 0
							});
							mobEdu.enquire.f.resetNewEmail();
							mobEdu.enquire.f.emailListBackButton();
						}
					}

				});
			} else {
				Ext.Msg.show({
					title: null,
					message: NEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetNewEmail: function() {
			Ext.getCmp('newsub').reset();
			Ext.getCmp('newmsg').reset();
		},

		showReplyMessage: function() {
			if (Ext.os.is.Phone) {
				mobEdu.util.updatePrevView(mobEdu.enquire.f.showEmailDetail);
			} else {
				mobEdu.util.updatePrevView(mobEdu.enquire.f.showEmailList);
			}
			mobEdu.util.show('mobEdu.enquire.view.replyMessage');
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');
			var messageData = detailStore.data.all;
			var subject;
			for (var i = 0; i < messageData.length; i++) {
				subject = messageData[i].data.subject;
			}
			Ext.getCmp('replySub').setValue(subject);
		},

		sendReplayMessage: function() {
			var replySub = Ext.getCmp('replySub').getValue();
			var replyRMsgArea = Ext.getCmp('replyMsg').getValue();
			replySub = replySub.trim();
			replyRMsgArea = replyRMsgArea.trim();
			if (replySub != '' && replyRMsgArea != '') {
				mobEdu.enquire.f.getLeadRecruiterID();
				mobEdu.enquire.f.sentReplyMessage();
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		sentReplyMessage: function() {
			var replySub = Ext.getCmp('replySub').getValue();
			var replyRMsgArea = Ext.getCmp('replyMsg').getValue();
			var conversationID = '';
			var store = mobEdu.util.getStore('mobEdu.enquire.store.viewEmail');
			var data = store.data.all;
			var toIds = new Array();
			if (data.length == 0) {
				conversationID = '';
			} else {
				conversationID = data[0].data.conversationID;
			}
			var eStore = mobEdu.util.getStore('mobEdu.enquire.store.newEmail');

			eStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/send');
			eStore.getProxy().setExtraParams({
				to: reqId,
				subject: replySub,
				body: replyRMsgArea,
				conversationID: conversationID,
				companyId: companyId
			});
			eStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.sentReplyMessageResponseHandler();
			};
			eStore.load();
		},
		sentReplyMessageResponseHandler: function() {
			var nEStore = mobEdu.util.getStore('mobEdu.enquire.store.newEmail');
			var NEStatus = nEStore.getProxy().getReader().rawData;
			if (NEStatus.status == 'success') {
				Ext.Msg.show({
					id: 'messageSuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Message sent successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.resetReplyMessage();
							mobEdu.enquire.f.emailListBackButton();
						}
					}

				});
			} else {
				Ext.Msg.show({
					title: null,
					message: NEStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetReplyMessage: function() {
			mobEdu.util.get('mobEdu.enquire.view.replyMessage');
		},

		showAppointments: function() {
			mobEdu.enquire.f.getLeadRecruiterID();
			var searchText = '';
			var appStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList');

			appStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			appStore.getProxy().setUrl(commonwebserver + 'appointment/search');
			appStore.getProxy().setExtraParams({
				searchText: searchText,
				maxRecords: 20,
				companyId: companyId
			});
			appStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.appointmentsResponseHandler();
			};
			appStore.load();
		},
		appointmentsResponseHandler: function() {
			var appStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList');
			appStore.sort('appointmentDate', 'DESC');
			mobEdu.enquire.f.showAppointmentsList();
			/*			var appStatus = appStore.getProxy().getReader().rawData;
			var totalRecords = appStore.getProxy().getReader().rawData.recordCount;
			if (appStatus.status == 'success' || totalRecords == 0) {
				mobEdu.enquire.f.showAppointmentsList();
				if ((!(Ext.os.is.Phone))) {
					if (appStore.data.length > 0) {
						mobEdu.enquire.f.loadTabletAppointmentDetail(appStore.getAt(0));
						Ext.getCmp("appointmentsList").select(0);
						Ext.getCmp('acceptT').show();
						Ext.getCmp('declineT').show();
					} else {
						Ext.getCmp('acceptT').hide();
						Ext.getCmp('declineT').hide();
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: appStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}*/
		},

		showAppointmentsList: function() {
			var appStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList');
			var appStoreData = appStore.data.all;
			var cmp;
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showSubMenu);
			// if(Ext.os.is.Phone){
			mobEdu.util.show('mobEdu.enquire.view.phone.appointmentsList');
			cmp = Ext.getCmp("newPhnAppointment");
			// }else{
			// 	mobEdu.util.show('mobEdu.enquire.view.tablet.appointmentsList');
			// 	cmp=Ext.getCmp("newAppointment");
			// }
			//var rid = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList').getProxy().getReader().rawData.recruiterId.recruiterId;
			if (reqId === null || reqId === undefined || reqId === '' || reqId === '0') {
				cmp.hide();
			} else {
				cmp.show();
			}

		},

		dateChecking: function(val) {
			if (val < 10) {
				val = '0' + val;
			}
			return val;
		},
		nextPreviousMonthChecking: function(date) {
			var presentDate = date;
			var presentDateValue;
			var currentDate = new Date();
			var currentDateValue;
			if (currentDate == null) {
				currentDateValue = '';
			} else {
				currentDateValue = (mobEdu.enquire.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enquire.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear())
			}

			if (presentDate == null) {
				presentDateValue = '';
			} else {
				presentDateValue = (mobEdu.enquire.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enquire.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear())
			}
			if (currentDateValue == presentDateValue) {
				mobEdu.enquire.f.setAppointmentScheduleInfo(date)
			}
		},
		setAppointmentScheduleInfo: function(selectedDate) {
			if (selectedDate == null || selectedDate == undefined) {
				selectedDate = new Date();
			}
			var newDateValue = (mobEdu.enquire.f.dateChecking([selectedDate.getMonth() + 1]) + '/' + mobEdu.enquire.f.dateChecking(selectedDate.getDate()) + '/' + selectedDate.getFullYear());
			var scheduleInfo = '';
			var appointmentsStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList');
			appointmentsStore.each(function(rec) {
				var appDate = rec.get('appointmentDate');
				var appDateValue = appDate.split(' ');
				var versionUser = rec.get('versionUser');
				var dateFormatted = appDateValue[0];
				if (newDateValue == dateFormatted) {
					var recAcceptance = rec.get('sAcceptance');
					var leadAcceptance = rec.get('cAcceptance');
					var appTime = appDateValue[1];
					var appSubject = rec.get('subject');
					var appDesc = rec.get('body');
					var appLocation = rec.get('location');
					scheduleInfo = mobEdu.enquire.f.formatAppointmentInfo(scheduleInfo, appSubject, appTime, appLocation, appDesc, recAcceptance, leadAcceptance, versionUser);
				}
			});

			//set the event schedule info in calendar view label
			Ext.getCmp('appointmentLabel').setHtml(scheduleInfo);
		},

		formatAppointmentInfo: function(scheduleInfo, appSubject, appTime, appLocation, appDesc, recAcceptance, leadAcceptance, versionUser) {
			// var user = mobEdu.util.getStore('mobEdu.enquire.store.login').data.first().raw.userInfo.userName;
			// if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
			if (leadAcceptance != 'N' && recAcceptance != 'N') {
				if (appSubject == null) {
					appSubject = '';
				}
				if (appTime == null) {
					appTime = '';
				}

				if (appDesc == null) {
					appDesc = '';
				}
				if (appLocation == null) {
					appLocation = '';
				}
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Subject: ' + '</b>' + decodeURIComponent(appSubject) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Time: ' + '</b>' + appTime + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Location: ' + '</b>' + decodeURIComponent(appLocation) + '</h3>';
				scheduleInfo = scheduleInfo + '<h3>' + '<b>' + 'Description: ' + '</b>' + decodeURIComponent(appDesc) + '</h3>' + '<br/>';

				return scheduleInfo;
			} else {
				return scheduleInfo;
			}
		},

		//To highlight Schedule dates
		doHighlightAppointmentsCalenderViewCell: function(classes, values, highlightedItemCls) {
			//get the store info
			var store = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList');
			if (store.data.length > 0) {
				var sData = store.data.all;
				//var user = mobEdu.util.getStore('mobEdu.enquire.store.login').data.first().raw.userInfo.userName;
				//get the meeting info
				for (var a = 0; a < sData.length; a++) {
					var versionUser = sData[a].data.versionUser;
					var leadAcceptance = sData[a].data.sAcceptance;
					var recAcceptance = sData[a].data.cAcceptance;
					//if ((leadAcceptance == 'Y' && recAcceptance == null) || (leadAcceptance == null && recAcceptance == 'Y') || (leadAcceptance == null && recAcceptance == null)) {
					//if (leadAcceptance != 'N' && recAcceptance != 'N') {
					var appDate = sData[a].data.appointmentDate;
					var appDateValue = appDate.split(' ');
					var dateFormatted = appDateValue[0];
					var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
					if (newStartDate.getDate() == values.date.getDate() && newStartDate.getMonth() == values.date.getMonth() && newStartDate.getYear() == values.date.getYear()) {
						classes.push(highlightedItemCls);
					}
					//Incrementing startDate
					newStartDate = new Date(newStartDate.getTime() + 86400000);
					//};
				}
			}
			return classes
		},

		loadCalendarView: function() {
			mobEdu.main.f.calendarInstance = 'appointment';
			mobEdu.util.get('mobEdu.enquire.view.appointmentScheduleCalendar');
			var appointmentDock = Ext.getCmp('appointmentsDock');
			appointmentDock.remove(calendar);

			calendar = Ext.create('Ext.ux.TouchCalendarView', {
				id: 'crseCalend',
				mode: 'month',
				weekStart: 0,
				value: new Date(),
				minHeight: '230px'
			});
			appointmentDock.add(calendar);
			//Set scheduleInfo for First time
			mobEdu.enquire.f.setAppointmentScheduleInfo(new Date());
		},

		showCalendar: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.appListBack);
			mobEdu.enquire.f.loadCalendarView();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.appListBack);
			mobEdu.util.show('mobEdu.enquire.view.appointmentScheduleCalendar');
		},


		loadTabletAppointmentDetail: function(record) {
			var appId = record.data.appointmentID;

			var appDetailStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			appDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			appDetailStore.getProxy().setUrl(commonwebserver + 'appointment/get');
			appDetailStore.getProxy().setExtraParams({
				appointmentID: appId,
				companyId: companyId
			});
			appDetailStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.TabletAppointmentDetailResponseHandler();
			};
			appDetailStore.load();
		},
		TabletAppointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			var appDetailStatus = appDetailStore.getProxy().getReader().rawData;
			if (appDetailStatus.status != 'success') {
				Ext.Msg.show({
					title: null,
					message: appDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},
		loadAppointmentDetail: function(record) {
			var appId = record.data.appointmentID;
			var acceptance = '';
			var versionUser = record.data.createdBy;
			var user = mobEdu.util.getUsername();
			versionUser = versionUser.toLowerCase();
			user = user.toLowerCase();

			var fromDetail = record.data.fromDetails;
			var toArry = record.data.toIDs;
			var fromID = fromDetail.fromID;
			if (fromID == record.data.loginUserID) {
				acceptance = fromDetail.acceptance;
			}

			if (acceptance == '' || acceptance == undefined || acceptance == null) {
				for (var i = 0; i < toArry.length; i++) {
					var toID = toArry[i].toID;
					if (toID == record.data.loginUserID) {
						acceptance = toArry[i].acceptance;
					}
				}
			}

			if (versionUser == user) {
				// if(Ext.os.is.Phone){
				mobEdu.util.get('mobEdu.enquire.view.phone.appointmentDetail');
				Ext.getCmp('accept').hide();
				Ext.getCmp('decline').hide();
				// }else{
				// 	Ext.getCmp('acceptT').hide();
				// 	Ext.getCmp('declineT').hide();
				// }
			} else {
				if (acceptance == 'Y' || acceptance == 'N') {
					// if(Ext.os.is.Phone){
					mobEdu.util.get('mobEdu.enquire.view.phone.appointmentDetail');
					Ext.getCmp('accept').hide();
					Ext.getCmp('decline').hide();
					// }else{
					// 	Ext.getCmp('acceptT').hide();
					// 	Ext.getCmp('declineT').hide();
					// }
				} else {
					// if(Ext.os.is.Phone){
					mobEdu.util.get('mobEdu.enquire.view.phone.appointmentDetail');
					Ext.getCmp('accept').show();
					Ext.getCmp('decline').show();
					// }else{
					// 	Ext.getCmp('acceptT').show();
					// 	Ext.getCmp('declineT').show();
					// }
				}
			}
			if (acceptance == 'rescheduled') {
				// if(Ext.os.is.Phone){
				mobEdu.util.get('mobEdu.enquire.view.phone.appointmentDetail');
				Ext.getCmp('accept').show();
				Ext.getCmp('decline').show();
				// }else{
				// 	Ext.getCmp('acceptT').show();
				// 	Ext.getCmp('declineT').show();
				// }
			}


			var appDetailStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			appDetailStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			appDetailStore.getProxy().setUrl(commonwebserver + 'appointment/get');
			appDetailStore.getProxy().setExtraParams({
				appointmentID: appId,
				companyId: companyId
			});
			appDetailStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.appointmentDetailResponseHandler();
			};
			appDetailStore.load();
		},
		appointmentDetailResponseHandler: function() {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			mobEdu.enquire.f.loadAppDetail();
			/* var appDetailStatus = appDetailStore.getProxy().getReader().rawData;			
				if (appDetailStatus.status == 'success') {
				mobEdu.enquire.f.loadAppDetail();
			} else {
				Ext.Msg.show({
					title: null,
					message: appDetailStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}*/
		},

		hasAppointmentDirection: function(appSource) {
			if (appSource == "IN") {
				return true;
			} else {
				return false;
			}
		},
		hasAppReadFlag: function(estatus) {
			if (estatus == "Opened") {
				return true;
			} else {
				return false;
			}
		},

		appListBack: function() {
			var appStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentsList');
			appStore.load();
			mobEdu.enquire.f.showAppointmentsList();
		},

		showAppointmentDetail: function(record) {
			var appDetailStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			appDetailStore.removeAll();
			appDetailStore.add(record);
			appDetailStore.sync();
			mobEdu.enquire.f.loadAppDetail();
		},

		loadAppDetail: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.appListBack);
			mobEdu.util.show('mobEdu.enquire.view.phone.appointmentDetail');
		},
		loadUpdateAppointment: function(value) {

			mobEdu.enquire.f.loadEnquireAppointmentEnumerations();
			var upAppStore = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			var upDateStore = upAppStore.data.all;
			mobEdu.util.updatePrevView(mobEdu.enquire.f.loadAppDetail);
			mobEdu.util.show('mobEdu.enquire.view.updateAppointment');

			Ext.getCmp('upsub').setValue(upDateStore[0].data.subject);
			Ext.getCmp('updes').setValue(upDateStore[0].data.body);
			var appDate = upDateStore[0].data.appointmentDate;
			var appDateValue = appDate.split(' ');
			var dateFormatted = appDateValue[0];
			var newStartDate = Ext.Date.parse(dateFormatted, "m/d/Y");
			var appTime = appDateValue[1];
			var appTimeFormatted = appTime.split(':');
			var appTimeHours = appTimeFormatted[0];
			var appTimeMints = appTimeFormatted[1];

			if (upDateStore[0].data.appointmentDate != '' && upDateStore[0].data.appointmentDate != undefined && upDateStore[0].data.appointmentDate != null) {
				Ext.getCmp('update').setValue(new Date(newStartDate));
			}
			//Ext.getCmp('update').setValue(upDateStore[0].data.appointmentDate);
			Ext.getCmp('upTimeHours').setValue(appTimeHours);
			Ext.getCmp('upTimeMinutes').setValue(appTimeMints);
			Ext.getCmp('uploc').setValue(upDateStore[0].data.location);
			if (upDateStore[0].data.appointmentStatus != '' && upDateStore[0].data.appointmentStatus != undefined && upDateStore[0].data.appointmentStatus != null) {
				Ext.getCmp('upsta').setValue(upDateStore[0].data.appointmentStatus);
			}
		},

		showNewAppointment: function() {
			mobEdu.enquire.f.loadEnquireAppointmentEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showAppointmentsList);
			mobEdu.util.show('mobEdu.enquire.view.newAppointment');
			var newDate = new Date();
			Ext.getCmp('appdate').setValue(newDate);
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			Ext.getCmp('appTimeHours').setMinValue(timeH);
			Ext.getCmp('appTimeMinutes').setMinValue(timeM);
			//            Ext.getCmp('appTimeHours').setMaxValue(timeH);
			Ext.getCmp('appTimeHours').setValue(timeH);
			Ext.getCmp('appTimeMinutes').setValue(timeM);
		},

		loadEnquireAppointmentEnumerations: function() {
			var enumType = 'APPOINTMENT_STATUS';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.enumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadEnumerationsResponseHandler();
			};
			enuStore.load();
		},

		loadEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.enumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			// var dt = Ext.Date.add(presentDate, Ext.Date.MINUTE, 30);
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(presentDate, 'm/d/Y');
			}
			if (newDateFormatted == presentDateFormatted) {
				//                if(timeH < 18){
				//                    Ext.Msg.show({
				//                        title:'Invalid Time',
				//                        message:'Appointment time between 8AM to 18PM.',
				//                        buttons:Ext.MessageBox.OK,
				//                        cls:'msgbox',
				//                        fn:function (btn) {
				//                            if (btn == 'ok') {
				//                                Ext.getCmp('appdate').setValue(new Date());
				//                                Ext.getCmp('appTimeHours').setMinValue(8);
				//                                Ext.getCmp('appTimeMinutes').setMinValue(0);
				//                                Ext.getCmp('appTimeHours').setValue(8);
				//                                Ext.getCmp('appTimeMinutes').setValue(0);
				//                            }
				//                        }
				//                    });
				//                }else{
				if (Ext.getCmp('appTimeHours') != undefined) {
					Ext.getCmp('appTimeHours').setMinValue(timeH);
					Ext.getCmp('appTimeHours').setValue(timeH);
				}
				if (Ext.getCmp('appTimeMinutes') != undefined) {
					Ext.getCmp('appTimeMinutes').setMinValue(timeM);
					Ext.getCmp('appTimeMinutes').setValue(timeM);
				}

				//                }
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('appdate').setValue(new Date());
							Ext.getCmp('appTimeHours').setMinValue(timeH);
							Ext.getCmp('appTimeMinutes').setMinValue(timeM);
							Ext.getCmp('appTimeHours').setValue(timeH);
							Ext.getCmp('appTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('appTimeHours').setMinValue(8);
				Ext.getCmp('appTimeHours').setValue(8);
				Ext.getCmp('appTimeMinutes').setMinValue(0);
				Ext.getCmp('appTimeMinutes').setValue(0);
			}

		},
		onUpAppDateKeyup: function(datefield) {
			var newDate = new Date();
			var presentDate = datefield.getValue();
			var timeH = newDate.getHours();
			var timeM = newDate.getMinutes();
			var newDateFormatted;
			if (newDate == null) {
				newDateFormatted = '';
			} else {
				newDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
			}
			var presentDateFormatted;
			if (presentDate == null) {
				presentDateFormatted = '';
			} else {
				presentDateFormatted = Ext.Date.format(newDate, 'm/d/Y');
			}

			if (newDateFormatted == presentDateFormatted) {
				if (Ext.getCmp('upTimeHours') != undefined) {
					Ext.getCmp('upTimeHours').setMinValue(timeH);
					Ext.getCmp('upTimeHours').setValue(timeH);
				}
				if (Ext.getCmp('upTimeMinutes') != undefined) {
					Ext.getCmp('upTimeMinutes').setMinValue(timeM);
					Ext.getCmp('upTimeMinutes').setValue(timeM);
				}
			} else if (newDate > presentDate) {
				Ext.Msg.show({
					title: 'Invalid Date',
					message: '<p>Please select a future date.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							Ext.getCmp('update').setValue(new Date());
							Ext.getCmp('upTimeHours').setMinValue(timeH);
							Ext.getCmp('upTimeMinutes').setMinValue(timeM);
							Ext.getCmp('upTimeHours').setValue(timeH);
							Ext.getCmp('upTimeMinutes').setValue(timeM);
						}
					}
				});
			} else {
				Ext.getCmp('upTimeHours').setMinValue(8);
				Ext.getCmp('upTimeMinutes').setMinValue(0);
				Ext.getCmp('upTimeHours').setValue(8);
				Ext.getCmp('upTimeMinutes').setValue(0);
			}
		},
		sendAppointment: function() {
			var aSub = Ext.getCmp('appsub').getValue();
			var aDes = Ext.getCmp('appdes').getValue();
			var aDate = Ext.getCmp('appdate').getValue();
			var aLoc = Ext.getCmp('apploc').getValue();
			//            var aTime = Ext.getCmp('appTime').getValue();

			var aStatus = 'REQUESTED';
			var aTimeH = Ext.getCmp('appTimeHours').getValue();
			var aTimeM = Ext.getCmp('appTimeMinutes').getValue();
			if (aStatus == 'select') {
				aStatus = ''
			}
			if (aTimeH == null) {
				aTimeH = '';
			}
			if (aSub.charAt(0) == ' ' || aDes.charAt(0) == ' ') {
				Ext.Msg.show({
					message: '<p>Subject & Description must start with alphanumeric.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				if (aSub != '' && aDes != '' && aDate != '' && aLoc != '' && aTimeH != '' && aStatus != '') {
					var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);
					var selectDate = Ext.getCmp('appdate').getValue();
					selectDate = Ext.Date.format(selectDate, 'm/d/Y');
					if (aTimeM < 10) {
						aTimeM = "0" + aTimeM;
					}
					var selectDateFormatted = Ext.Date.parse(selectDate + " " + aTimeH + ":" + aTimeM, 'm/d/Y G:i');
					if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
						mobEdu.enquire.f.appointmentResponse();
					} else {
						Ext.Msg.show({
							title: 'Invalid Date',
							message: '<p>Please select a future date..</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
					if (aTimeH >= 18 || aTimeH < 8) {
						Ext.Msg.show({
							title: 'Invalid Time',
							message: '<p>Appointment time between 8AM to 18PM.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				} else {
					Ext.Msg.show({
						title: null,
						message: '<p>Please enter all mandatory fields.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}
			}

		},
		appointmentResponse: function() {
			var aSub = Ext.getCmp('appsub').getValue();
			var aDes = Ext.getCmp('appdes').getValue();
			var aDate = Ext.getCmp('appdate').getValue();
			var aLoc = Ext.getCmp('apploc').getValue();
			var aStatus = 'REQUESTED';
			var dt = Ext.Date.add(aDate, Ext.Date.MINUTE, 30);

			var aTimeH = Ext.getCmp('appTimeHours').getValue();
			var aTimeM = Ext.getCmp('appTimeMinutes').getValue();
			var timeHours = dt.getHours();
			var timeMinutes = dt.getMinutes();
			var appTime;
			var updateTime = timeHours + ':' + timeMinutes;

			if (aTimeH == null) {
				aTimeH = '';
			}
			if (aTimeM == null) {
				aTimeM = '';
			}

			appTime = aTimeH + ':' + aTimeM;


			var appDateFormatted;
			if (aDate == null) {
				appDateFormatted = '';
			} else {
				appDateFormatted = Ext.Date.format(aDate, 'm/d/Y');
			}

			if (aTimeH >= 18) {
				Ext.Msg.show({
					title: 'Invalid Time',
					message: 'Appointment time between 8AM to 18PM.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.loadEnquireAppointmentEnumerations();
							mobEdu.util.updatePrevView(mobEdu.enquire.f.showAppointmentsList);
							mobEdu.util.show('mobEdu.enquire.view.newAppointment');
						}
					}
				});
			} else {

				var appDateTime = appDateFormatted + ' ' + appTime;

				var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.newAppointment');

				newAppStore.getProxy().setHeaders({
					Authorization: mobEdu.util.getLeadAuthString(),
					companyID: companyId
				});
				newAppStore.getProxy().setUrl(commonwebserver + 'appointment/add');
				newAppStore.getProxy().setExtraParams({
					to: reqId,
					subject: aSub,
					body: aDes,
					appointmentDate: appDateTime,
					location: aLoc,
					status: aStatus,
					companyId: companyId
				});
				newAppStore.getProxy().afterRequest = function() {
					mobEdu.enquire.f.newAppointmentResponseHandler();
				};
				newAppStore.load();
			}

		},
		newAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'appsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment requested successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.resetNewAppointment();
							mobEdu.enquire.f.appListBack();
						}
					}

				});
			} else {
				Ext.Msg.show({
					title: null,
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetNewAppointment: function() {
			Ext.getCmp('appsub').reset();
			Ext.getCmp('appdate').reset();
			Ext.getCmp('appTimeHours').reset();
			Ext.getCmp('appTimeMinutes').reset();
			Ext.getCmp('apploc').reset();
			Ext.getCmp('appdes').reset();
		},

		updateAppointmentSend: function(value) {
			var appDetail = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			var detailData = appDetail.data.all;
			var appStatus = detailData[0].data.status;
			var upSub = Ext.getCmp('upsub').getValue();
			var upDes = Ext.getCmp('updes').getValue();
			var upDate = Ext.getCmp('update').getValue();
			var upLoc = Ext.getCmp('uploc').getValue();
			var upStatus = Ext.getCmp('upsta').getValue();
			var upAppTimeHours = Ext.getCmp('upTimeHours').getValue();
			var upAppTimeMints = Ext.getCmp('upTimeMinutes').getValue();
			var minConvert = upAppTimeHours * 60 + upAppTimeMints;
			var presentDate = new Date();
			var hours = presentDate.getHours();
			var minutes = presentDate.getMinutes();
			var presentMinConvert = hours * 60 + minutes;

			if (upSub != '' && upDes != '' && upDate != '' && upLoc != '' && upAppTimeHours != '' && upStatus != '') {

				var presentDate = Ext.Date.add(new Date(), Ext.Date.MINUTE, 30);


				var selectDate = Ext.getCmp('update').getValue();
				selectDate = Ext.Date.format(selectDate, 'm/d/Y');
				if (upAppTimeMints < 10) {
					upAppTimeMints = "0" + upAppTimeMints;
				}
				var selectDateFormatted = Ext.Date.parse(selectDate + " " + upAppTimeHours + ":" + upAppTimeMints, 'm/d/Y G:i');

				if (upAppTimeHours >= 18) {
					Ext.Msg.show({
						title: 'Invalid Time',
						message: '<p>Appointment time between 8AM to 18PM.</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else if (Ext.Date.diff(presentDate, selectDateFormatted, Ext.Date.SECOND) > 0) {
					var upappText = 'appointment';
					var upOperationText = 'update';
					var appointmentId = detailData[0].data.appointmentID;
					var from = detailData[0].data.fromID;
					var to = detailData[0].data.toID;
					var upAppTime;
					if (upAppTimeHours == null) {
						upAppTimeHours = '';
					}
					if (upAppTimeMints == null) {
						upAppTimeMints = '';
					}
					upAppTime = upAppTimeHours + ':' + upAppTimeMints;
					var appDateFormatted;
					if (upDate == null) {
						appDateFormatted = '';
					} else {
						appDateFormatted = Ext.Date.format(upDate, 'm/d/Y');
					}
					var appDateTime = appDateFormatted + ' ' + upAppTime;
					var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.newAppointment');
					newAppStore.getProxy().setUrl(commonwebserver + '/appointment/update');
					newAppStore.getProxy().setHeaders({
						Authorization: mobEdu.util.getLeadAuthString(),
						companyID: companyId
					});
					newAppStore.getProxy().setExtraParams({
						appointmentID: appointmentId,
						subject: upSub,
						body: upDes,
						appointmentDate: appDateTime,
						location: upLoc,
						status: upStatus,
						companyId: companyId,
						from: from,
						to: to
					});
					newAppStore.getProxy().afterRequest = function() {
						mobEdu.enquire.f.updateAppointmentResponseHandler();
					};
					newAppStore.load();
				} else {
					Ext.Msg.show({
						title: 'Invalid Date',
						message: '<p>Please select a future date..</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				}

			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},
		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.appListBack();
							mobEdu.enquire.f.resetUpDateAppointment();
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		acceptAppointment: function(value) {
			if (value == 'Accept') {
				Ext.Msg.show({
					id: 'appAccept',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Accepted.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.appListBack();
						}
					}
				});
			}
			if (value == 'Decline') {
				Ext.Msg.show({
					id: 'appDAccept',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment is Declined.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.appListBack();
						}
					}
				});
			}
			var appDetail = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			//if(Ext.os.is.Phone){
			mobEdu.util.get('mobEdu.enquire.view.phone.appointmentDetail');
			Ext.getCmp('accept').hide();
			Ext.getCmp('decline').hide();
			// }else{
			// 	Ext.getCmp('acceptT').hide();
			// 	Ext.getCmp('declineT').hide();
			// }
			var detailData = appDetail.data.all;
			var appId = detailData[0].data.appointmentID;
			var acceptance;
			var status;
			if (value == 'Accept') {
				acceptance = 'Y';
				status = 'scheduled';
			} else {
				acceptance = 'N';
				status = 'declined';
			};
			// var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.newAppointment');
			var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.acceptance');
			newAppStore.getProxy().setUrl(commonwebserver + 'appointment/statusUpdate');
			newAppStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			newAppStore.getProxy().setExtraParams({
				appointmentID: appId,
				acceptance: acceptance,
				companyId: companyId,
				status: status
			});
			newAppStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.acceptanceResponseHandler();
			};
			newAppStore.load();
		},

		acceptanceResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.acceptance');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.data.length > 0) {
				Ext.Msg.show({
					id: 'upappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.appListBack();
							mobEdu.enquire.f.resetUpDateAppointment();
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		updateAppointmentResponseHandler: function() {
			var newAppStore = mobEdu.util.getStore('mobEdu.enquire.store.newAppointment');
			var nAppStatus = newAppStore.getProxy().getReader().rawData;
			if (nAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'upappsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Appointment updated successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.appListBack();
							mobEdu.enquire.f.resetUpDateAppointment();
						}
					}
				});
			} else {
				Ext.Msg.show({
					title: null,
					message: nAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		resetUpDateAppointment: function() {
			mobEdu.util.get('mobEdu.enquire.view.updateAppointment');
		},

		onZipKeyup: function(zipfield) {
			var value = (zipfield.getValue()).toString() + '';
			value = value.replace(/\D/g, '');
			zipfield.setValue(value);
			var length = value.length;
			if (length > 5) {
				zipfield.setValue(value.substring(0, 5));
				return false;
			}
			return true;
			//            var value=zipfield.getValue() +'';
			//            var length=value.length;
			//            if (length > 5){
			//                zipfield.setValue(value.substring(0, 5));
			//                return false;
			//            }
			//            return true;

		},

		onTestScoreKeyUp: function(testScoreField) {
			var value = (testScoreField.getValue()).toString() + '';
			value = value.replace(/\D/g, '');
			testScoreField.setValue(value);
			//            var length=value.length;
			//            if(length>5){
			//                testScoreField.setValue(value.substring(0,5));
			//                return false;
			//            }
			//            return true;
		},

		onSSNKeyUp: function(ssnfield) {
			var ssnNumber = (ssnfield.getValue()).toString() + '';
			ssnNumber = ssnNumber.replace(/\D/g, '');
			ssnfield.setValue(ssnNumber);
			var length = ssnNumber.length;
			if (length > 9) {
				ssnfield.setValue(ssnNumber.substring(0, 9));
				return false;
			}
			return true;
		},

		onCountryKeyup: function(countryfield) {
			var value = countryfield.getValue();
			var length = value.length;
			var countryRegExp = new RegExp("^[a-zA-Z]+$");
			if (!countryRegExp.test(value)) {
				Ext.Msg.show({
					id: 'countryName',
					name: 'countryName',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid Country Name.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							countryfield.focus(true);
						}
					}
				});
			}

		},
		onCityKeyup: function(cityfield) {
			var Cityvalue = cityfield.getValue();
			var length = Cityvalue.length;
			var cityRegExp = new RegExp("^[a-zA-Z]+$");
			if (!cityRegExp.test(Cityvalue)) {
				Ext.Msg.show({
					id: 'cityName',
					name: 'cityName',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid City Name.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							cityfield.focus(true);
						}
					}
				});
			}
		},

		firstNameChecking: function(textfield) {
			var fNameText = textfield.getValue();
			var regExp = new RegExp("^[a-zA-Z]+$");
			if (!regExp.test(fNameText)) {
				Ext.Msg.show({
					id: 'firstNmae',
					name: 'firstNmae',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid First Name.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							textfield.focus(true);
						}
					}
				});
			}
		},

		lastNameChecking: function(lastNameField) {
			var lNameText = lastNameField.getValue();
			var regExp = new RegExp("^[a-zA-Z]+$");
			if (!regExp.test(lNameText)) {
				Ext.Msg.show({
					id: 'lastNmae',
					name: 'lastNmae',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid Last Name.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							lastNameField.focus(true);
						}
					}
				});
			}
		},

		loadContactAddressInfo: function() {
			var fName = Ext.getCmp('firstname').getValue();
			var lName = Ext.getCmp('lastname').getValue();
			var gender = Ext.getCmp('gender').getValue();
			if (gender == 'select') {
				gender = '';
			}
			var dateofbirth = Ext.getCmp('dateofbirth').getValue();
			var dobFormatted;
			if (dateofbirth == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([dateofbirth.getMonth() + 1] + '/' + dateofbirth.getDate() + '/' + dateofbirth.getFullYear());
			}
			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.firstName = fName;
				record.data.lastName = lName;
				record.data.gender = gender;
				record.data.dateOfBirth = dobFormatted;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					firstName: fName,
					lastName: lName,
					gender: gender,
					dateOfBirth: dateofbirth
				});
				store.sync();
			}
			if (fName != null && fName != '' && lName != null && lName != '' && gender != null && gender != '' && gender != '' && dateofbirth != null && dateofbirth != '') {
				var firstName = Ext.getCmp('firstname');
				var regExp = new RegExp("^[a-zA-Z]+$");
				if (!regExp.test(fName)) {
					Ext.Msg.show({
						id: 'firstNmae',
						name: 'firstNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid First Name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					var lastName = Ext.getCmp('lastname');
					var lastNRegExp = new RegExp("^[a-zA-Z]+$");
					if (!lastNRegExp.test(lName)) {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid Last Name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						mobEdu.enquire.f.inquiryContactResponseHandler();
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		inquiryContactResponseHandler: function() {
			mobEdu.enquire.f.showContactAddressInfo();
		},
		onPhone1Keyup: function(phone1Field) {
			var phoneNumber = (phone1Field.getValue()).toString() + '';
			phoneNumber = phoneNumber.replace(/\D/g, '');
			phone1Field.setValue(phoneNumber);
			var length = phoneNumber.length;
			if (length > 10) {
				phone1Field.setValue(phoneNumber.substring(0, 10));
				return false;
			}
			return true;
		},
		onPhone2Keyup: function(phone1Field) {
			var phoneNumber2 = (phone1Field.getValue()).toString() + '';
			phoneNumber2 = phoneNumber2.replace(/\D/g, '');
			phone1Field.setValue(phoneNumber2);
			var length = phoneNumber2.length;
			if (length > 10) {
				phone1Field.setValue(phoneNumber2.substring(0, 10));
				return false;
			}
			return true;
		},
		onActScoreKeyup: function(scoreField) {
			var scoreNumber = (scoreField.getValue()).toString() + '';
			scoreNumber = scoreNumber.replace(/\D/g, '');
			scoreField.setValue(scoreNumber);
			var length = scoreNumber.length;
			if (length > 3) {
				scoreField.setValue(scoreNumber.substring(0, 3));
				return false;
			}
			return true;
		},
		onEmailKeyup: function(phone1Field) {
			var phoneNumber = (phone1Field.getValue()).toString() + '';
			phoneNumber = phoneNumber.replace(/\D/g, '');
			phone1Field.setValue(phoneNumber);
			var length = phoneNumber.length;
			if (length > 10) {
				phone1Field.setValue(phoneNumber.substring(0, 10));
				return false;
			}
			return true;
		},
		onVisaNoKeyUp: function(visaNoField) {
			var visaNO = (visaNoField.getValue()).toString() + '';
			visaNO = visaNO.replace(/\D/g, '');
			visaNoField.setValue(visaNO);
			var length = visaNO.length;
			if (length > 16) {
				visaNoField.setValue(visaNO.substring(0, 16));
				return false;
			}
			return true;
		},

		onPasswordKeyup: function(passwordfield) {
			var password = passwordfield.getValue();
			var passwordLength = password.length;
			// var PasswordRegExp = new RegExp("((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{4,20})");
			if (passwordLength < 4) {
				Ext.Msg.show({
					id: 'password',
					name: 'password',
					title: null,
					cls: 'msgbox',
					message: '<p>Password should be greater than 4 characters.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							passwordfield.focus(true);
						}
					}
				});
			}
		},

		onUserNameKeyup: function(usernamefield) {
			var userName = usernamefield.getValue();
			var userNameRegExp = new RegExp("^[a-z0-9_-]{3,15}$");
			if (!userNameRegExp.test(userName)) {
				Ext.Msg.show({
					id: 'username',
					name: 'username',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid Username.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							usernamefield.focus(true);
						}
					}
				});
			}
		},

		loadContactPhoneInfo: function() {
			var add = Ext.getCmp('address').getValue();
			var addre2 = Ext.getCmp('add2').getValue();
			var addre3 = Ext.getCmp('add3').getValue();
			var city = Ext.getCmp('city').getValue();
			var stat = Ext.getCmp('states').getValue();
			if (stat == 'select') {
				stat = '';
			}
			var zip = Ext.getCmp('zip').getValue();
			var count = Ext.getCmp('country').getValue();

			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			store.removeAll();
			store.sync();
			if (record != undefined) {
				record.data.cAddress = add;
				record.data.cAddress2 = addre2;
				record.data.cAddress3 = addre3;
				record.data.cCity = city;
				record.data.cState = stat;
				record.data.cZip = zip;
				record.data.cCountry = count;
				store.add(record.copy());
				store.sync();
			}
			if (add != '' && add != null && city != '' && city != null &&
				stat != '' && stat != null && zip != '' && zip != null && count != '' && count != null) {
				var zipCode = Ext.getCmp('zip');
				var zipValue = zipCode.getValue().toString();
				if (zipValue.length != 5) {
					console.log('loaded');
					Ext.Msg.show({
						id: 'zipcodevalue',
						name: 'zipcodevalue',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid Zip Code.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								zipCode.focus(true);
							}
						}
					});
				} else {
					var cStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
					cStore.getProxy().setHeaders({
						companyID: companyId
					});
					cStore.getProxy().setUrl(enquireWebserver + 'enquire/common/validatelocation?city=' + city + '&state=' + stat + '&zip=' + zip);
					cStore.getProxy().setExtraParams({
						companyId: companyId
					});
					cStore.getProxy().afterRequest = function() {
						var cityStatus = cStore.getProxy().getReader().rawData;
						if (cityStatus.status != 'success') {
							Ext.Msg.show({
								title: null,
								message: '<p>Invalid City,State and Zip code combination.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						} else {
							var cityField = Ext.getCmp('city');
							var cityRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
							if (!cityRegExp.test(city)) {
								Ext.Msg.show({
									id: 'cityName',
									name: 'cityName',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid City Name.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											cityField.focus(true);
										}
									}
								});
							} else {
								var country = Ext.getCmp('country');
								var countryRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
								if (!countryRegExp.test(count)) {
									Ext.Msg.show({
										id: 'countryNmae',
										name: 'countryNmae',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid Country Name.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												country.focus(true);
											}
										}
									});
								} else {
									mobEdu.enquire.f.contactInfoResponseHandler();
								}
							}
						}
					}
					cStore.load();
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		contactInfoResponseHandler: function() {
			mobEdu.util.updatePrevView(showContactPhoneInfo);
			mobEdu.enquire.f.showContactPhoneInfo();
		},

		loadAdmissions: function() {
			var classificationText = Ext.getCmp('class').getValue();
			if (classificationText == 'select') {
				classificationText = '';
			}
			var intendedMajorText = Ext.getCmp('intMaj').getValue();
			if (intendedMajorText == 'others') {
				intendedMajorText = Ext.getCmp('intendText').getValue();
			}
			var highSchoolText = Ext.getCmp('highS').getValue();
			var whatTypeInformation = Ext.getCmp('inf').getValue();
			if (whatTypeInformation == 'select') {
				whatTypeInformation = '';
			}

			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			store.removeAll();
			store.sync();
			if (record != undefined) {
				record.data.classification = classificationText;
				record.data.intendedMajor = intendedMajorText;
				record.data.highSchool = highSchoolText;
				record.data.whatTypeInf = whatTypeInformation;
				store.add(record.copy());
				store.sync();
			}
			if (classificationText != '' && classificationText != null && intendedMajorText != '' && intendedMajorText != null &&
				highSchoolText != '' && highSchoolText != null && whatTypeInformation != '' && whatTypeInformation != null) {
				var schoolName = Ext.getCmp('highS');
				var schoolNameRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
				if (!schoolNameRegExp.test(highSchoolText)) {
					Ext.Msg.show({
						id: 'hSchool',
						name: 'hSchool',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid High School.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								schoolName.focus(true);
							}
						}
					});
				} else {
					var intMajor = Ext.getCmp('city');
					var intMajorRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
					if (!intMajorRegExp.test(intendedMajorText)) {
						Ext.Msg.show({
							id: 'intMName',
							name: 'intMName',
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid Intended Major.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									intMajor.focus(true);
								}
							}
						});
					} else {
						mobEdu.enquire.f.loadFinalSummary();
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadAdminssionsInfo: function() {
			var ph = Ext.getCmp('phone1').getValue().toString();
			ph = ph.replace(/\D/g, '');
			var mail = Ext.getCmp('email').getValue();
			var vmail = Ext.getCmp('email2').getValue();
			var pass = Ext.getCmp('password').getValue();
			var vPass = Ext.getCmp('vpassword').getValue();
			var userName = Ext.getCmp('userName').getValue();

			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			var emailStore = mobEdu.util.getStore('mobEdu.enquire.store.email');
			store.load();
			var record = store.getAt(0);
			store.removeAll();
			store.sync();
			if (record != undefined) {
				record.data.phone1 = ph;
				record.data.email = mail;
				record.data.vemail = vmail;
				record.data.password = pass;
				record.data.vPassword = vPass
				record.data.userName = userName;
				store.add(record.copy());
				store.sync();
				emailStore.add(record.copy());
				emailStore.sync();
				emailStore.load();
			}
			if (ph != null && ph != '' &&
				mail != null && mail != '' && pass != null && pass != '' && vPass != null && vPass != '' && userName != null && userName != '') {
				var phone1 = Ext.getCmp('phone1');
				var phone1Value = phone1.getValue();
				phone1Value = phone1Value.replace(/-|\(|\)/g, "");

				var length = phone1Value.length;
				if (phone1Value.length != 10) {
					Ext.Msg.show({
						id: 'phone1msg',
						name: 'phone1msg',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid Phone Number.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								phone1.focus(true);
							}
						}
					});
				} else {
					var a = phone1Value.replace(/\D/g, '');
					var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
					phone1.setValue(newValue);
					var checkMail = Ext.getCmp('email');
					if (mail == vmail) {
						var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
						if (regMail.test(mail) == false) {
							Ext.Msg.show({
								id: 'checkEmail',
								name: 'checkEmail',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid Email address.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										checkMail.focus(true);
									}
								}
							});
						} else {
							if (pass != vPass) {
								Ext.Msg.show({
									title: null,
									message: '<p>Password and Verify Password must be same.</p>',
									buttons: Ext.MessageBox.OK,
									cls: 'msgbox'
								});
							} else {
								var userNameField = Ext.getCmp('userName');
								var userNameRegExp = new RegExp("^[A-Za-z0-9_.@-]{4,15}$");
								if (!userNameRegExp.test(userName)) {
									Ext.Msg.show({
										id: 'username',
										name: 'username',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid Username.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												userNameField.focus(true);
											}
										}
									});
								} else {
									var passwordField = Ext.getCmp('password');
									var PasswordRegExp = new RegExp("^[A-Za-z0-9_.@-]{4,15}$");
									if (!PasswordRegExp.test(pass)) {
										Ext.Msg.show({
											id: 'password',
											name: 'password',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid Password.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													passwordField.focus(true);
												}
											}
										});
									} else {
										mobEdu.enquire.f.showAdmissionsInfo();
									}
								}
							}
						}
					} else {
						Ext.Msg.show({
							title: null,
							message: '<p>Email and Verify Email must be same.</p>',
							buttons: Ext.MessageBox.OK,
							cls: 'msgbox'
						});
					}
				}
				//                    }
				//                }
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			//            }
		},
		highSchoolInfoResponseHandler: function() {
			mobEdu.enquire.f.showHighSchoolInfo();
		},
		intendedChange: function(newValue) {
			if (newValue == 'others') {
				Ext.getCmp('intendText').show();
				Ext.getCmp('intendText').setValue('');
			} else {
				Ext.getCmp('intendText').hide();
			}


		},
		loadEnquireSubmitForm: function() {
			var fName = Ext.getCmp('firstname').getValue();
			var lName = Ext.getCmp('lastname').getValue();
			var gender = Ext.getCmp('gender').getValue();
			if (gender == 'select') {
				gender = '';
			}
			var dateofbirth = Ext.getCmp('dateofbirth').getValue();
			var add = Ext.getCmp('address').getValue();
			var addre2 = Ext.getCmp('add2').getValue();
			//            var addre3 = Ext.getCmp('add3').getValue();
			var city = Ext.getCmp('city').getValue();
			var statDesc = Ext.getCmp('stList').getValue();
			var stat = mobEdu.enquire.f.rinfstateCode;
			if (stat == 'select') {
				stat = '';
			}
			var zip = Ext.getCmp('zip').getValue().toString();
			zip = zip.replace(/\D/g, '');
			var count = Ext.getCmp('country').getValue();
			var ph = Ext.getCmp('phone1').getValue().toString();
			ph = ph.replace(/\D/g, '');
			var mail = Ext.getCmp('email').getValue();
			var vmail = Ext.getCmp('email2').getValue();
			var pass = Ext.getCmp('password').getValue();
			var vPass = Ext.getCmp('vpassword').getValue();
			var userName = Ext.getCmp('userName').getValue();
			var classificationText = Ext.getCmp('class').getValue();
			if (classificationText == 'select') {
				classificationText = '';
			}

			var intendedMajorText = Ext.getCmp('intMaj').getValue();
			if (intendedMajorText == 'select') {
				intendedMajorText = '';
			}
			if (intendedMajorText == 'others') {
				intendedMajorText = Ext.getCmp('intendText').getValue();
			}
			var highSchoolText = Ext.getCmp('highS').getValue();
			var whatTypeInformation = Ext.getCmp('inf').getValue();
			if (whatTypeInformation == 'select') {
				whatTypeInformation = '';
			}
			var actSatScore = Ext.getCmp('asScore').getValue();
			var requestInfo = Ext.getCmp('reqInf').getValue();
			if (requestInfo == 'select') {
				requestInfo = '';
			}

			var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
			store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.firstName = fName;
				record.data.lastName = lName;
				record.data.gender = gender;
				record.data.dateOfBirth = dateofbirth;
				record.data.cAddress = add;
				record.data.cAddress2 = addre2;
				record.data.cCity = city;
				record.data.cStateDesc = statDesc
				record.data.cState = stat;
				record.data.cZip = zip;
				record.data.cCountry = count;
				record.data.phone1 = ph;
				record.data.email = mail;
				record.data.vemail = vmail;
				record.data.password = pass;
				record.data.vPassword = vPass;
				record.data.userName = userName;
				record.data.classification = classificationText;
				record.data.intendedMajor = intendedMajorText;
				record.data.highSchool = highSchoolText;
				record.data.whatTypeInf = whatTypeInformation;
				record.data.actScore = actSatScore;
				record.data.reqInfo = requestInfo;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					firstName: fName,
					lastName: lName,
					gender: gender,
					dateOfBirth: dateofbirth,
					cAddress: add,
					cAddress2: addre2,
					cCity: city,
					cStateDesc: statDesc,
					cState: stat,
					cZip: zip,
					cCountry: count,
					phone1: ph,
					email: mail,
					vemail: vmail,
					password: pass,
					vPassword: vPass,
					userName: userName,
					classification: classificationText,
					intendedMajor: intendedMajorText,
					highSchool: highSchoolText,
					whatTypeInf: whatTypeInformation,
					actScore: actSatScore,
					reqInfo: requestInfo
				});
				store.sync();
			}
			if (fName != null && fName != '' && lName != null && lName != '' && gender != null && gender != '' && gender != '' &&
				dateofbirth != null && dateofbirth != '' && add != '' && add != null && city != '' && city != null &&
				stat != '' && stat != null && zip != '' && zip != null && count != '' && count != null && ph != null && ph != '' &&
				mail != null && mail != '' && pass != null && pass != '' && vPass != null && vPass != '' &&
				userName != null && userName != '' && intendedMajorText != '' &&
				intendedMajorText != null && highSchoolText != '' && highSchoolText != null) {
				var firstName = Ext.getCmp('firstname');
				var fName = firstName.getValue();
				var lastName = Ext.getCmp('lastname');
				var regExp = new RegExp("^[a-zA-Z\\- ]+$");
				if (fName.charAt(0) == ' ') {
					Ext.Msg.show({
						id: 'firstNmae',
						name: 'firstNmae',
						title: null,
						cls: 'msgbox',
						message: '<p>FirstName must starts with alphabet</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					if (lName.charAt(0) == ' ') {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							title: null,
							cls: 'msgbox',
							message: '<p>LastName must starts with alphabet</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									firstName.focus(true);
								}
							}
						});
					} else {
						if (!regExp.test(fName)) {
							Ext.Msg.show({
								id: 'firstNmae',
								name: 'firstNmae',
								title: null,
								cls: 'msgbox',
								message: '<p>Invalid First Name.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										firstName.focus(true);
									}
								}
							});
						} else {
							var lastName = Ext.getCmp('lastname');
							var lastNRegExp = new RegExp("^[a-zA-Z\\- ]+$");
							if (!lastNRegExp.test(lName)) {
								Ext.Msg.show({
									id: 'lastNmae',
									name: 'lastNmae',
									title: null,
									cls: 'msgbox',
									message: '<p>Invalid Last Name.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											lastName.focus(true);
										}
									}
								});
							} else {
								var dateofbirth = Ext.getCmp('dateofbirth');
								var dateofbirthValue = Ext.getCmp('dateofbirth').getValue();
								var currentDate = new Date();
								if (dateofbirthValue >= currentDate) {
									Ext.Msg.show({
										id: 'dateofbirth',
										name: 'dateofbirth',
										title: null,
										cls: 'msgbox',
										message: '<p>Birth Date should be in past.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												dateofbirth.focus(true);
											}
										}
									});
								} else {
									var zipCode = Ext.getCmp('zip');
									var zipValue = zipCode.getValue();
									zipValue = zipValue.replace(/-|\(|\)/g, "");
									//                        var zipValue = zipCode.getValue().toString();
									if (zipValue.length != 5) {
										Ext.Msg.show({
											id: 'zipcodevalue',
											name: 'zipcodevalue',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid Zip Code.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													zipCode.focus(true);
												}
											}
										});
									} else {
										var cStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
										cStore.getProxy().setHeaders({
											companyID: companyId
										});
										cStore.getProxy().setUrl(enquireWebserver + 'enquire/common/validatelocation?city=' + city + '&state=' + stat + '&zip=' + zip);
										cStore.getProxy().setExtraParams({
											companyId: companyId
										});
										cStore.getProxy().afterRequest = function() {
											var cityStatus = cStore.getProxy().getReader().rawData;
											if (cityStatus.status != 'success') {
												Ext.Msg.show({
													title: null,
													message: '<p>Invalid City,State and Zip code combination.</p>',
													buttons: Ext.MessageBox.OK,
													cls: 'msgbox'
												});
											} else {
												var cityField = Ext.getCmp('city');
												var cityRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
												if (!cityRegExp.test(city)) {
													Ext.Msg.show({
														id: 'cityName',
														name: 'cityName',
														title: null,
														cls: 'msgbox',
														message: '<p>Invalid City Name.</p>',
														buttons: Ext.MessageBox.OK,
														fn: function(btn) {
															if (btn == 'ok') {
																cityField.focus(true);
															}
														}
													});
												} else {
													var country = Ext.getCmp('country');
													var countryRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
													if (!countryRegExp.test(count)) {
														Ext.Msg.show({
															id: 'countryNmae',
															name: 'countryNmae',
															title: null,
															cls: 'msgbox',
															message: '<p>Invalid Country Name.</p>',
															buttons: Ext.MessageBox.OK,
															fn: function(btn) {
																if (btn == 'ok') {
																	country.focus(true);
																}
															}
														});
													} else {
														var phone1 = Ext.getCmp('phone1');
														var phone1Value = phone1.getValue();
														phone1Value = phone1Value.replace(/-|\(|\)/g, "");

														var length = phone1Value.length;
														if (phone1Value.length != 10) {
															Ext.Msg.show({
																id: 'phone1msg',
																name: 'phone1msg',
																title: null,
																cls: 'msgbox',
																message: '<p>Invalid Phone Number.</p>',
																buttons: Ext.MessageBox.OK,
																fn: function(btn) {
																	if (btn == 'ok') {
																		phone1.focus(true);
																	}
																}
															});
														} else {
															var a = phone1Value.replace(/\D/g, '');
															var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
															phone1.setValue(newValue);
															var checkMail = Ext.getCmp('email');
															if (mail == vmail) {
																var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
																if (regMail.test(mail) == false) {
																	Ext.Msg.show({
																		id: 'checkEmail',
																		name: 'checkEmail',
																		title: null,
																		cls: 'msgbox',
																		message: '<p>Invalid Email address.</p>',
																		buttons: Ext.MessageBox.OK,
																		fn: function(btn) {
																			if (btn == 'ok') {
																				checkMail.focus(true);
																			}
																		}
																	});
																} else {
																	if (pass != vPass) {
																		Ext.Msg.show({
																			title: null,
																			message: '<p>Password and Verify Password must be same.</p>',
																			buttons: Ext.MessageBox.OK,
																			cls: 'msgbox'
																		});
																	} else {
																		if (pass.match(';') || pass.match(':')) {
																			Ext.Msg.show({
																				title: null,
																				message: '<p>Password must not contain " : " and " ; " characters</p>',
																				buttons: Ext.MessageBox.OK,
																				cls: 'msgbox'
																			});
																		} else {
																			var regExp = new RegExp("^[a-zA-Z0-9@*+=$#&_()/|\\-]+$");
																			if (!regExp.test(pass)) {
																				Ext.Msg.show({
																					title: null,
																					message: '<p>Password allows   @ * + = $ # & _ - ( ) / |  special characters only</p>',
																					buttons: Ext.MessageBox.OK,
																					cls: 'msgbox'
																				});
																			} else {
																				var userNameField = Ext.getCmp('userName');
																				var userNameLength = Ext.getCmp('userName').getValue().length;
																				if (userNameLength < 4) {
																					Ext.Msg.show({
																						id: 'username',
																						name: 'username',
																						title: null,
																						cls: 'msgbox',
																						message: '<p>Username should be atleast 4 characters.</p>',
																						buttons: Ext.MessageBox.OK,
																						fn: function(btn) {
																							if (btn == 'ok') {
																								userNameField.focus(true);
																							}
																						}
																					});
																				} else {
																					var userNameRegExp = new RegExp("^[A-Za-z0-9_.@-]{4,15}$");
																					if (!userNameRegExp.test(userName)) {
																						Ext.Msg.show({
																							id: 'username',
																							name: 'username',
																							title: null,
																							cls: 'msgbox',
																							message: '<p>Invalid Username.</p>',
																							buttons: Ext.MessageBox.OK,
																							fn: function(btn) {
																								if (btn == 'ok') {
																									userNameField.focus(true);
																								}
																							}
																						});
																					} else {
																						var passwordField = Ext.getCmp('password');
																						var passwordLength = Ext.getCmp('password').getValue().length;
																						if (passwordLength < 4) {
																							Ext.Msg.show({
																								id: 'password',
																								name: 'password',
																								title: null,
																								cls: 'msgbox',
																								message: '<p>Password must be atleast 4 characters.</p>',
																								buttons: Ext.MessageBox.OK,
																								fn: function(btn) {
																									if (btn == 'ok') {
																										passwordField.focus(true);
																									}
																								}
																							});
																						} else {
																							var schoolName = Ext.getCmp('highS');
																							var schoolNameRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
																							if (!schoolNameRegExp.test(highSchoolText)) {
																								Ext.Msg.show({
																									id: 'hSchool',
																									name: 'hSchool',
																									title: null,
																									cls: 'msgbox',
																									message: '<p>Invalid High School.</p>',
																									buttons: Ext.MessageBox.OK,
																									fn: function(btn) {
																										if (btn == 'ok') {
																											schoolName.focus(true);
																										}
																									}
																								});
																							} else {
																								//var intMajor = Ext.getCmp('city');
																								var intMajorRegExp = new RegExp("^[a-zA-Z]+(( )+[a-zA-z]+)*$");
																								if (!intMajorRegExp.test(intendedMajorText)) {
																									Ext.Msg.show({
																										id: 'intMName',
																										name: 'intMName',
																										title: null,
																										cls: 'msgbox',
																										message: '<p>Invalid Intended Major.</p>',
																										buttons: Ext.MessageBox.OK,
																										fn: function(btn) {
																											if (btn == 'ok') {
																												intMajor.focus(true);
																											}
																										}
																									});
																								} else {
																									mobEdu.enquire.f.loadFinalSummary();
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															} else {
																Ext.Msg.show({
																	title: null,
																	message: '<p>Email and Verify Email must be same.</p>',
																	buttons: Ext.MessageBox.OK,
																	cls: 'msgbox'
																});
															}
														}
													}
												}
											}
										}
									}
								}
							}
							cStore.load();
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},

		inquirySubmit: function() {
			var infname = Ext.getCmp('firstname');
			var inqFName = infname.getValue();
			var inlname = Ext.getCmp('lastname');
			var inqLName = inlname.getValue();
			var ingender = Ext.getCmp('gender');
			var inqGender = ingender.getValue();
			var indob = Ext.getCmp('dateofbirth');
			var inqDob = indob.getValue();
			var inaddress = Ext.getCmp('address');
			var inqAddress = inaddress.getValue();
			var inqAddre2 = Ext.getCmp('add2').getValue();
			//            var inqAddre3 = Ext.getCmp('add3').getValue();
			var incity = Ext.getCmp('city');
			var inqCity = incity.getValue();
			var dobFormatted;
			if (inqDob == null) {
				dobFormatted = '';
			} else {
				dobFormatted = Ext.Date.format(inqDob, 'Y-m-d');
			}
			//var instate = Ext.getCmp('states');
			var inqState = mobEdu.enquire.f.rinfstateCode;
			var inzip = Ext.getCmp('zip');
			var inqZip = inzip.getValue();
			var incountry = Ext.getCmp('country');
			var inqCountry = incountry.getValue();
			var inphoneI = Ext.getCmp('phone1');
			var inqPhone = inphoneI.getValue().toString();
			inqPhone = inqPhone.replace(/-|\(|\)/g, "");
			var inphone = Ext.getCmp('phone');
			var inemail = Ext.getCmp('email');
			var inqEmail = inemail.getValue();
			var inqPass = Ext.getCmp('password').getValue();
			var inqUserName = Ext.getCmp('userName').getValue();
			var classificationText = Ext.getCmp('class').getValue();
			if (classificationText == 'select') {
				classificationText = '';
			}
			var intendedMajorText = Ext.getCmp('intMaj').getValue();
			if (intendedMajorText == 'others') {
				intendedMajorText = Ext.getCmp('intendText').getValue();
			}
			var highSchoolText = Ext.getCmp('highS').getValue();
			var whatTypeInformation = Ext.getCmp('inf').getValue();
			if (whatTypeInformation == 'select') {
				whatTypeInformation = '';
			}
			var actSatScore = Ext.getCmp('asScore').getValue();
			var requestInfo = Ext.getCmp('reqInf').getValue();
			if (requestInfo == 'select') {
				requestInfo = '';
			}

			var inquiryStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
			inquiryStore.getProxy().setHeaders({
				companyID: companyId
			});
			inquiryStore.getProxy().setUrl(enquireWebserver + 'enquire/lead/create?firstName=' + inqFName + '&lastName=' + inqLName + '&address1=' + inqAddress + '&address2=' + inqAddre2 + '&city=' + inqCity + '&state=' + inqState + '&country=' + inqCountry + '&zipCode=' + inqZip + '&phone1=' + inqPhone + '&userName=' + inqUserName + '&password=' + encodeURIComponent(inqPass) + '&email=' + inqEmail + '&gender=' + inqGender + '&birthDate=' + dobFormatted + '&classification=' + classificationText + '&highSchool=' + highSchoolText + '&intendedMajor=' + intendedMajorText + '&actScore=' + actSatScore + '&requestInfo=' + requestInfo + '&lookInfo=' + whatTypeInformation);
			inquiryStore.getProxy().setExtraParams({
				companyId: companyId
			});
			inquiryStore.getProxy().afterRequest = function() {
				var inquiryEliStatus = inquiryStore.getProxy().getReader().rawData;

				console.log('storeStatus:' + inquiryEliStatus);
				if (inquiryEliStatus.status != 'success') {
					Ext.Msg.show({
						title: null,
						message: inquiryEliStatus.status,
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
				} else {
					Ext.Msg.show({
						id: 'formSuccess',
						title: null,
						cls: 'msgbox',
						message: '<p style="font-weight:lighter;">Form submitted successfully.</p>' + '<p style="font-weight:lighter;">Thank you for taking the time to submit your' + ' information to </p>' + mobEdu.main.f.getTitle() + ' <p style="font-weight:lighter;">A university recruiter will be contacting you' + ' very soon to discuss the next steps.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								mobEdu.enquire.f.resetBasicInfo();
								mobEdu.enquire.f.resetContactAddressInfo();
								mobEdu.enquire.f.resetContactPhoneInfo();
								var store = mobEdu.util.getStore('mobEdu.enquire.store.profile');
								store.load();
								store.removeAll();
								store.sync();
								mobEdu.enquire.f.submitLoginForm(inqUserName, inqPass);
								mobEdu.enquire.f.generateMessage('signup', inqUserName, inqPass);
								//                                mobEdu.util.showMainView();
								//                                mobEdu.enquire.f.showLeadLogin();

							}
						}
					});
				}
			};
			inquiryStore.load();

		},
		contactRecruiterSubmit: function() {
			var tofield = Ext.getCmp('to');
			var toValue = tofield.getValue();
			var msgField = Ext.getCmp('message');
			var msgValue = msgField.getValue();
			if (toValue != '' && msgValue != '') {
				Ext.Msg.show({
					id: 'rsubmitsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Thanks for your interest.<br>We will get back to you shortly</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.resetContactRecruiter();
							mobEdu.enquire.f.showSubMenu();
						}
					}

				});
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		onContctNoKeyup: function(cntNofield) {
			var value = cntNofield.getValue().toString() + '';
			var length = value.length;
			if (length > 10) {
				cntNofield.setValue(value.substring(0, 10));
				return false;
			}
			return true;

		},

		scheduleVisitSubmit: function() {
			var fNameField = Ext.getCmp('fullName');
			var fNameValue = fNameField.getValue();
			var cPhone = Ext.getCmp('contactNo');
			var cPhoneValue = cPhone.getValue();
			var vReason = Ext.getCmp('vReason');
			var reason = vReason.getValue();
			if (fNameValue != null && cPhoneValue != null && reason != null && fNameValue != '' && cPhoneValue != '' && reason != '') {
				var phone = cPhoneValue.toString();
				if (phone.length < 10) {
					Ext.Msg.alert(null, '<p>Invalid Contact Number</p>');
				} else {
					Ext.Msg.show({
						id: 'svsubmitsuccess',
						title: null,
						message: '<p>Thanks for your interest.<br>Your visit has been scheduled</p>',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox',
						fn: function(btn) {
							if (btn == 'ok') {
								mobEdu.enquire.f.resetScheduleVisit();
								mobEdu.enquire.f.showSubMenu();
							}
						}

					});
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},

		loadFinalSummary: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showInformationForm);
			mobEdu.util.show('mobEdu.enquire.view.finalSummary');
			var firstName = Ext.getCmp('firstname').getValue();
			var lastName = Ext.getCmp('lastname').getValue();
			var gender = Ext.getCmp('gender').getValue();
			var changeGender;
			if (gender == 'M') {
				changeGender = 'Male';
			} else {
				changeGender = 'Female';
			}
			var dob = Ext.getCmp('dateofbirth').getValue();
			var dobFormatted;
			if (dob == null) {
				dobFormatted = '';
			} else {
				dobFormatted = (dob.getFullYear() + '-' + [dob.getMonth() + 1] + '-' + dob.getDate());
			}
			var middleValue;
			middleValue = firstName + ' ' + lastName;


			var address = Ext.getCmp('address').getValue();
			var addre2 = Ext.getCmp('add2').getValue();
			//            var addre3 = Ext.getCmp('add3').getValue();
			var cityValue = Ext.getCmp('city').getValue();
			var stateValue = Ext.getCmp('stList').getValue();
			var zipCode = Ext.getCmp('zip').getValue();
			var countryName = Ext.getCmp('country').getValue();
			var phoneNo1 = Ext.getCmp('phone1').getValue().toString();
			var emailId1 = Ext.getCmp('email').getValue();
			var emailId2 = Ext.getCmp('email2').getValue();
			var userName = Ext.getCmp('userName').getValue();
			var addressValue;
			if (addre2 == '') {
				addressValue = address;
			} else {
				addressValue = address + ',' + addre2;
			}
			var classificationText = Ext.getCmp('class').getValue();
			if (classificationText == 'select') {
				classificationText = '';
			}
			var intendedMajorText = Ext.getCmp('intMaj').getValue();
			if (intendedMajorText == 'select') {
				intendedMajorText = '';
			}
			if (intendedMajorText == 'others') {
				intendedMajorText = Ext.getCmp('intendText').getValue();
			}

			var highSchoolText = Ext.getCmp('highS').getValue();
			var whatTypeInformation = Ext.getCmp('inf').getValue();
			if (whatTypeInformation == 'select') {
				whatTypeInformation = '';
			}
			var actSatScore = Ext.getCmp('asScore').getValue();
			var requestInfo = Ext.getCmp('reqInf').getValue();
			if (requestInfo == 'select') {
				requestInfo = '';
			}

			var finalSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Gender:</h2></td>' + '<td align="left"><h3>' + changeGender + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Date of Birth:</h2></td>' + '<td align="left"><h3>' + dobFormatted + '</h3></td></tr>' +
				'<tr><td valign="top" align="right" width="50%"><h2>Address:</h2></td>' + '<td align="left"><h3>' + addressValue + '<br />' + cityValue + ',' + stateValue + '<br />' + zipCode + ',' + countryName + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Phone:</h2></td>' + '<td align="left"><h3>' + phoneNo1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + emailId1 + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>User Name:</h2></td>' + '<td align="left"><h3>' + userName + '</h3></td></tr>';
			if (classificationText != '') {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Classification:</h2></td>' + '<td align="left"><h3>' + classificationText + '</h3></td></tr>';
			}
			finalSummaryHtml += '<tr><td align="right" width="50%"><h2>Intended Major:</h2></td>' + '<td align="left"><h3>' + intendedMajorText + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>High School:</h2></td>' + '<td align="left"><h3>' + highSchoolText + '</h3></td></tr>';
			if (whatTypeInformation != '') {
				finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>What type of information <br/> are you looking for?:</h2></td>' + '<td align="left"><h3>' + whatTypeInformation + '</h3></td></tr>';
			}
			if (actSatScore != '') {
				finalSummaryHtml += '<tr><td align="right" width="50%"><h2>ACT/SAT Score:</h2></td>' + '<td align="left"><h3>' + actSatScore + '</h3></td></tr>';
			}
			if (requestInfo != '') {
				finalSummaryHtml += '<tr><td align="right" width="50%" valign="top"><h2>Who is requesting the <br/> information?:</h2></td>' + '<td align="left"><h3>' + requestInfo + '</h3></td></tr></table>';
			}

			Ext.getCmp('finalsummary').setHtml(finalSummaryHtml);

		},

		getViewApplication: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enquire.store.getApplication');

			applicationStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			applicationStore.getProxy().setUrl(enquireWebserver + 'enquire/lead/getapplication');
			applicationStore.getProxy().setExtraParams({
				companyId: companyId
			});
			applicationStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.getViewApplicationResponseHandler();
			};
			applicationStore.load();
		},
		getViewApplicationResponseHandler: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enquire.store.getApplication');
			var applicationStatus = applicationStore.getProxy().getReader().rawData;
			if (applicationStatus.status != 'success') {
				Ext.Msg.show({
					id: 'appSuccess',
					title: null,
					message: applicationStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				mobEdu.enquire.f.loadViewApplication();
			}
		},

		onGradYearKeyup: function(gradYearField) {
			if (gradYearField.getValue() != null) {
				var gradYear = (gradYearField.getValue()).toString() + '';
				var length = gradYear.length;
				if (length > 4) {
					gradYearField.setValue(gradYear.substring(0, 4));
					return false;
				}
			}
			return true;
		},
		onGPAKeyup: function(gpaField) {
			if (gpaField.getValue() != null) {
				var gpa = (gpaField.getValue()).toString() + '';
				var length = gpa.length;
				if (length > 3) {
					gpaField.setValue(gpa.substring(0, 3));
					return false;
				}
			}
			return true;
		},

		loadViewApplication: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enquire.store.getApplication');
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showProfileDetails);
			mobEdu.util.show('mobEdu.enquire.view.viewApplication');
			//            Ext.getCmp('profDetail').setTitle('<h1>'+leadTitle+'</h1>');
			var profileData = applicationStore.data.all;
			var appSisInfo = profileData[0].data.sisInfo;
			var appIndicator;
			var applicationStatus;
			var appComments;
			var appBannerId;
			if (appSisInfo != 0 && appSisInfo != null && appSisInfo != '' && appSisInfo != undefined) {
				appIndicator = appSisInfo.applIndicator;
				applicationStatus = appSisInfo.apstDescription;
				appComments = appSisInfo.comments;
				appBannerId = appSisInfo.bannerID;
			} else {
				appIndicator = '';
				applicationStatus = '';
				appComments = '';
				appBannerId = '';
			}
			//            if (appIndicator == "Y" && appIndicator != '') {
			//                Ext.getCmp('pushApp').hide();
			//            } else {
			//                Ext.getCmp('pushApp').show();
			//            }
			var applicationId = profileData[0].data.applicationID;
			var appLeadId = profileData[0].data.leadID;
			var appStatus = profileData[0].data.status;
			var appVersionNo = profileData[0].data.versionNo;
			var appVersionDate = profileData[0].data.versionDate;
			var appVersionUser = profileData[0].data.versionUser;
			var appAdTermCode = profileData[0].data.termCode;
			var appAdTermCodeDes = profileData[0].data.termCodeDescr;
			var appAdLevelCode = profileData[0].data.levelCode;
			var appAdLevelCodeDes = profileData[0].data.levelCodeDescr;
			var appAdMajorCode = profileData[0].data.majorCode;
			var appAdMajorCodeDes = profileData[0].data.majorCodeDescr;
			var appAdStudentType = profileData[0].data.studentType;
			var appAdStudentTypeDes = profileData[0].data.studentTypeDescr;
			var appAdAdmissionType = profileData[0].data.admissionType;
			var appAdAdmissionTypeDes = profileData[0].data.admissionTypeDescr;
			var appAdResidenceCode = profileData[0].data.residenceCode;
			var appAdResidenceCodeDes = profileData[0].data.residenceCodeDescr;
			var appAdCollegeCode = profileData[0].data.collegeCode;
			var appAdCollegeCodeDes = profileData[0].data.collegeCodeDescr;
			var appAdDegreeCode = profileData[0].data.degreeCode;
			var appAdDegreeCodeDes = profileData[0].data.degreeCodeDescr;
			var appAdDepartment = profileData[0].data.department;
			var appAdDepartmentDes = profileData[0].data.departmentDescr;
			var appAdCampus = profileData[0].data.campus;
			var appAdCampusDes = profileData[0].data.campusDescr;
			var appAdEduGoal = profileData[0].data.educationGoal;
			var appAdEduGoalDes = profileData[0].data.educationGoalDescr;
			var middleValue;
			var appPerFirstName = profileData[0].data.firstName;
			var appPerLastName = profileData[0].data.lastName;
			//            middleValue = appPerFirstName + ' ' + appPerLastName;
			var appPerMiddleName = profileData[0].data.middleName;
			if (appPerMiddleName != " " && appPerMiddleName != null) {
				middleValue = appPerFirstName + ' ' + appPerMiddleName + ' ' + appPerLastName;
			} else {
				middleValue = appPerFirstName + ' ' + appPerLastName;
			}
			// middleValue = appPerFirstName + ' ' + appPerLastName;
			//            }
			var appPerGender = profileData[0].data.gender;
			if (appPerGender == "F") {
				appPerGender = 'Female';
			} else {
				appPerGender = 'Male';
			}
			var appPerRace = profileData[0].data.race;
			var appPerRaceDes = profileData[0].data.raceDescr;
			if (appPerRaceDes == 'select') {
				appPerRaceDes = '';
			}
			var appPerEthnicity = profileData[0].data.ethnicity;
			var appPerEthnicityDes = profileData[0].data.ethnicityDescr;
			var appPerDateOfBirth = profileData[0].data.dob;
			var appPerSsn = profileData[0].data.ssn;
			var address;
			var zipAndCountry;
			var appContAddress1 = profileData[0].data.address1;
			var appContAddress2 = profileData[0].data.address2;
			var appContAddress3 = profileData[0].data.address3;
			var appContCity = profileData[0].data.city;
			var appContState = profileData[0].data.state;
			var appContStateDes = profileData[0].data.stateDescr;
			var appContCounty = profileData[0].data.county;
			var appContZip = profileData[0].data.zip;
			var appContCountry = profileData[0].data.country;
			if (appContAddress2 != '' && appContAddress3) {
				address = appContAddress1 + ',' + appContAddress2 + ',' + appContAddress3;
			} else {
				address = appContAddress1;
			}
			if (appContCountry != '') {
				zipAndCountry = appContZip + ',' + appContCountry
			} else {
				zipAndCountry = appContZip;
			}

			var appContEmailId = profileData[0].data.email;
			var appContPhoneNo1 = profileData[0].data.phone1;
			var appContPhoneNo2 = profileData[0].data.phone2;
			var visaDetails;
			var appVisaType = profileData[0].data.visaType;
			var appVisaNationality = profileData[0].data.nationality;
			var appVisaNationalityDes = profileData[0].data.nationalityDescr;
			var appVisaNo = profileData[0].data.visaNumber;
			if (appVisaType != '' && appVisaNationality != '' && appVisaNo != '') {
				visaDetails = appVisaType + ',' + appVisaNationality + ',' + appVisaNo;
			}
			var parent1Details;
			var parent2Details;
			var appParRelation1 = profileData[0].data.parent1Relation;
			var appParRelation1Des = profileData[0].data.parent1RelationDescr;
			var appParFName1 = profileData[0].data.parent1FirstName;
			var appParLName1 = profileData[0].data.parent1LastName;
			var appParMName1 = profileData[0].data.parent1MiddleName;
			if (appParFName1 != '' && appParLName1 != '') {
				parent1Details = appParFName1 + ',' + appParLName1;
			} else if (appParLName1 != '') {
				parent1Details = appParLName1;
			} else {
				parent1Details = appParFName1;
			}
			var appParRelation2 = profileData[0].data.parent2Relation;
			var appParRelation2Des = profileData[0].data.parent2RelationDescr;
			var appParFName2 = profileData[0].data.parent2FirstName;
			var appParLName2 = profileData[0].data.parent2LastName;
			var appParMName2 = profileData[0].data.parent2MiddleName;
			if (appParFName2 != '' && appParLName2 != '') {
				parent2Details = appParFName2 + ',' + appParLName2;
			} else if (appParLName2 != '') {
				parent2Details = appParLName2;
			} else {
				parent2Details = appParFName2;
			}
			var appSchoolName = profileData[0].data.schoolName;
			var appSchoolCode = profileData[0].data.schoolCode;
			var appSchoolCodeDes = profileData[0].data.schoolCodeDescr;
			var appSchoolGpa = profileData[0].data.schoolGpa;
			//            var appSchoolCity = profileData[0].data.schoolCity;
			//            var appSchoolState = profileData[0].data.schoolState;
			var appSchoolGYear = profileData[0].data.schoolGradYear;
			var testCode;
			var testDate;
			var testScore;
			var appTsTestScores = profileData[0].data.testScores;
			if (appTsTestScores.length != 0) {
				for (var i = 0; i < appTsTestScores.length; i++) {
					testCode = appTsTestScores[i].testCode;
					testDate = appTsTestScores[i].testDate;
					testScore = appTsTestScores[i].testScore;
				}
			} else {
				testCode = "";
				testDate = "";
				testScore = "";
			}
			var collegeCode;
			var degree;
			var grdDate;
			var collegeCity;
			var collegeState;
			var appCInfoColleges = profileData[0].data.colleges;
			if (appCInfoColleges.length != 0) {
				for (var c = 0; c < appCInfoColleges.length; c++) {
					collegeCode = appCInfoColleges[c].collegeCode;
					degree = appCInfoColleges[c].degree;
					collegeCity = appCInfoColleges[c].city;
					collegeState = appCInfoColleges[c].state;
					grdDate = appCInfoColleges[c].gradDate;
				}
			} else {
				collegeCode = "";
				degree = "";
				grdDate = "";
				collegeCity = "";
				collegeState = "";
			}
			var appInterestPN = profileData[0].data.primaryInterest;
			var appInterestSN = profileData[0].data.secondaryInterest;
			var appInterestLevelN = profileData[0].data.levelOfInterest;
			var appInterestFactor = profileData[0].data.factorForChoosing;
			var appInterestOtherN = profileData[0].data.otherInterests;

			var applicationSummaryHtml = '<table align="center" class=".td,th"><tr><td align="right" width="50%"><h2>Name:</h2></td>' + '<td align="left"><h3>' + middleValue + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Birth Date:</h2></td>' + '<td align="left"><h3>' + appPerDateOfBirth + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Gender:</h2></td>' + '<td align="left"><h3>' + appPerGender + '</h3></td></tr>';
			if (appPerRaceDes != '' && appPerRaceDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Race:</h2></td>' + '<td align="left"><h3>' + appPerRace + '</h3></td></tr>';
			}
			if (appPerEthnicityDes != '' && appPerEthnicityDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Ethnicity:</h2></td>' + '<td align="left"><h3>' + appPerEthnicityDes + '</h3></td></tr>';
			}
			if (appPerSsn != '' && appPerSsn != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>SSN:</h2></td>' + '<td align="left"><h3>' + appPerSsn + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Address:</h2></td>' + '<td align="left"><h3>' + address + '<br />' + appContCity + ',' + appContStateDes + '<br />' + zipAndCountry + '</h3></td></tr>';
			if (appContEmailId != '' && appContEmailId != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Email:</h2></td>' + '<td align="left"><h3>' + appContEmailId + '</h3></td></tr>';
			}
			if (appContPhoneNo1 != '' && appContPhoneNo1 != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone1:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo1 + '</h3></td></tr>';
			}
			if (appContPhoneNo2 != '' && appContPhoneNo2 != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Phone2:</h2></td>' + '<td align="left"><h3>' + appContPhoneNo2 + '</h3></td></tr>';
			}
			if (appParRelation1Des != '' && appParRelation1Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 1 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation1Des + '<br />' + parent1Details + '</h3></td></tr>';
			}
			if (appParRelation2Des != '' && appParRelation2Des != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Parent 2 Details:</h2></td>' + '<td align="left"><h3>' + appParRelation2Des + '<br />' + parent2Details + '</h3></td></tr>';
			}
			if (appVisaType != '' && appVisaNationalityDes != '' && appVisaNo != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Visa Details:</h2></td>' + '<td align="left"><h3>' + appVisaType + '<br />' + appVisaNationalityDes + ',' + appVisaNo + '</h3></td></tr>';
			}
			if (appSchoolName != '' && appSchoolCodeDes != '' && appSchoolGpa != '' && appSchoolGYear != '') {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>School Details:</h2></td>' + '<td align="left"><h3>' + appSchoolName + '<br />' + appSchoolCodeDes + ',' + appSchoolGpa + ',' + appSchoolGYear + '</h3></td></tr>';
			}
			applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Term Code:</h2></td>' + '<td align="left"><h3>' + appAdTermCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Level Code:</h2></td>' + '<td align="left"><h3>' + appAdLevelCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Major Code:</h2></td>' + '<td align="left"><h3>' + appAdMajorCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Student Type:</h2></td>' + '<td align="left"><h3>' + appAdStudentTypeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Residence Code:</h2></td>' + '<td align="left"><h3>' + appAdResidenceCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + appAdCollegeCodeDes + '</h3></td></tr>' +
				'<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + appAdDegreeCodeDes + '</h3></td></tr>';
			if (appAdAdmissionTypeDes != '' && appAdAdmissionTypeDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Admission Type:</h2></td>' + '<td align="left"><h3>' + appAdAdmissionTypeDes + '</h3></td></tr>';
			}
			if (appAdDepartmentDes != '' && appAdDepartmentDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Department:</h2></td>' + '<td align="left"><h3>' + appAdDepartmentDes + '</h3></td></tr>';
			}
			if (appAdCampusDes != '' && appAdCampusDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Campus:</h2></td>' + '<td align="left"><h3>' + appAdCampusDes + '</h3></td></tr>';
			}
			if (appAdEduGoalDes != '' && appAdEduGoalDes != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Education Goal:</h2></td>' + '<td align="left"><h3>' + appAdEduGoalDes + '</h3></td></tr>';
			}
			if (testCode != '' && testCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Code:</h2></td>' + '<td align="left"><h3>' + testCode + '</h3></td></tr>';
			}
			if (testDate != '' && testDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Date:</h2></td>' + '<td align="left"><h3>' + testDate + '</h3></td></tr>';
			}
			if (testScore != '' && testScore != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Test Score:</h2></td>' + '<td align="left"><h3>' + testScore + '</h3></td></tr>';
			}
			if (collegeCode != '' && collegeCode != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Code:</h2></td>' + '<td align="left"><h3>' + collegeCode + '</h3></td></tr>';
			}
			if (collegeCity != '' && collegeState != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>College Address:</h2></td>' + '<td align="left"><h3>' + collegeCity + ',' + collegeState + '</h3></td></tr>';
			}
			if (degree != '' && degree != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Degree Code:</h2></td>' + '<td align="left"><h3>' + degree + '</h3></td></tr>';
			}
			if (grdDate != '' && grdDate != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Graduation Date:</h2></td>' + '<td align="left"><h3>' + grdDate + '</h3></td></tr>';
			}
			if (appInterestPN != '' && appInterestPN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Primary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestPN + '</h3></td></tr>';
			}
			if (appInterestSN != '' && appInterestSN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Secondary Interest:</h2></td>' + '<td align="left"><h3>' + appInterestSN + '</h3></td></tr>';
			}
			if (appInterestLevelN != '' && appInterestLevelN != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Level Of Interest:</h2></td>' + '<td align="left"><h3>' + appInterestLevelN + '</h3></td></tr>';
			}
			if (appInterestFactor != '' && appInterestFactor != '') {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Factor For Choosing:</h2></td>' + '<td align="left"><h3>' + appInterestFactor + '</h3></td></tr>';
			}
			if (appInterestOtherN != '' && appInterestOtherN == null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Other Interests:</h2></td>' + '<td align="left"><h3>' + appInterestOtherN + '</h3></td></tr>';
			}
			if (appBannerId != '' && appBannerId != undefined && appBannerId != null) {
				applicationSummaryHtml += '<tr><td align="right" width="50%"><h2>Banner ID:</h2></td>' + '<td align="left"><h3>' + appBannerId + '</h3></td></tr>';
			}
			if (applicationStatus != '' && applicationStatus != undefined && applicationStatus != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Application Status:</h2></td>' + '<td align="left"><h3>' + applicationStatus + '</h3></td></tr>';
			}
			if (appComments != '' && appComments != undefined && appComments != null) {
				applicationSummaryHtml += '<tr><td valign="top" align="right" width="50%"><h2>Comments:</h2></td>' + '<td align="left"><h3>' + appComments + '</h3></td></tr></table>';
			}

			Ext.getCmp('viewApp').setHtml(applicationSummaryHtml);
		},

		loadPushApplication: function() {
			var applicationStore = mobEdu.util.getStore('mobEdu.enquire.store.getApplication');
			var profileData = applicationStore.data.all;
			var applicationId = profileData[0].data.applicationID;
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.enumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'enroute/lead/pushtosis');
			enuStore.getProxy().setExtraParams({
				applicationID: applicationId,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadPushApplicationResponseHandler();
			};
			enuStore.load();
		},
		loadPushApplicationResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.enumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				Ext.Msg.show({
					id: 'pushmsg',
					title: null,
					message: '<p>Application pushed to banner successfully.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.showProfileDetails();
						}
					}
				});
			}
		},

		checkLocalDataBasicInfo: function() {
			var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var storeData = profileStore.data.all;
			mobEdu.enquire.f.loadRaceEnumerations();
			mobEdu.util.get('mobEdu.enquire.view.createAppBasicInfo');
			Ext.getCmp('appFName').setValue(storeData[0].data.firstName);
			Ext.getCmp('appLName').setValue(storeData[0].data.lastName);
			var gender = storeData[0].data.gender;
			if (gender == "M") {
				gender = 'Male'
			} else {
				gender = 'Female'
			}
			if (storeData[0].data.gender != '' && storeData[0].data.gender != undefined && storeData[0].data.gender != null) {
				Ext.getCmp('appGender').setValue(gender);
			}
			if (storeData[0].data.birthDate != '' && storeData[0].data.birthDate != undefined && storeData[0].data.birthDate != null) {
				Ext.getCmp('appDateofbirth').setValue(new Date(storeData[0].data.birthDate));
			}
			mobEdu.enquire.f.loadStateEnumerations();
			mobEdu.enquire.f.showCreateAppBasicInfo();

		},

		showCreateAppBasicInfo: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showProfileDetails);
			mobEdu.util.show('mobEdu.enquire.view.createAppBasicInfo');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			var record = localAppStore.getAt(0);
			if (record != undefined) {
				Ext.getCmp('appFName').setValue(record.data.localAppFirstName);
				Ext.getCmp('appLName').setValue(record.data.localAppLastName);
				//Ext.getCmp('appMName').setValue(record.data.localAppMiddleName);
				if (record.data.localAppGender != '' && record.data.localAppGender != undefined && record.data.localAppGender != null) {
					Ext.getCmp('appGender').setValue(record.data.localAppGender);
				}
				if (record.data.localAppDateOfBirth != '' && record.data.localAppDateOfBirth != undefined && record.data.localAppDateOfBirth != null) {
					Ext.getCmp('appDateofbirth').setValue(new Date(record.data.localAppDateOfBirth));
				}
				if (record.data.localAppRace != '' && record.data.localAppRace != undefined && record.data.localAppRace != null) {
					Ext.getCmp('appRace').setValue(record.data.localAppRace);
				}

				Ext.getCmp('appssn').setValue(record.data.localAppSSN);
			}
		},

		resetCreateAppBasicInfo: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppBasicInfo');
		},

		loadRaceEnumerations: function() {
			var enumType = 'GORRACE';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.raceEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadRaceEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadRaceEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.raceEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadEthnicityEnumerations: function() {
			var enumType = 'STVETHN';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.ethnicityEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadEthnicityEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadEthnicityEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.ethnicityEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCreateAppContactInfo: function() {
			mobEdu.enquire.f.loadStateEnumerations();
			//            mobEdu.enquire.f.loadCountyEnumerations();
			mobEdu.enquire.f.loadCountryEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppBasicInfo);
			mobEdu.util.show('mobEdu.enquire.view.createAppContactAddress');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			var record = localAppStore.getAt(0);
			if (record.data.localAppCAddress1 === null) {
				var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
				var record = profileStore.getAt(0);
				Ext.getCmp('appAddress1').setValue(record.data.address1);
				Ext.getCmp('appAddress2').setValue(record.data.address2);
				if (record.data.state != '' && record.data.state != undefined && record.data.state != null) {
					Ext.getCmp('stAppList').setValue(record.data.state);
					mobEdu.enquire.f.cAppStateCode = record.data.state;
				}
				mobEdu.util.get('mobEdu.enquire.view.appState');
				var stateStore = Ext.getCmp('stateAppList').getStore();
				var stateData = stateStore.data.all;
				for (var j = 0; j < stateData.length; j++) {
					if (stateData[j].data.enumValue == record.data.state) {
						Ext.getCmp('stAppList').setValue(stateData[j].data.displayValue);
					}
				}
				Ext.getCmp('appCity').setValue(record.data.city);
				if (record.data.country != '' && record.data.country != undefined && record.data.country != null) {
					Ext.getCmp('appCountry').setValue(record.data.country);
				}
				Ext.getCmp('appPhone1').setValue(record.data.phone2);
				Ext.getCmp('appPhone2').setValue(record.data.phone1);
				Ext.getCmp('appEmail').setValue(record.data.email);
				Ext.getCmp('appZip').setValue(record.data.zip);
			} else if (record != undefined) {
				Ext.getCmp('appAddress1').setValue(record.data.localAppCAddress1);
				Ext.getCmp('appAddress2').setValue(record.data.localAppCAddress2);
				//                Ext.getCmp('appAddress3').setValue(record.data.localAppCAddress3);
				if (record.data.localAppCState != '' && record.data.localAppCState != undefined && record.data.localAppCState != null) {
					Ext.getCmp('stAppList').setValue(record.data.localAppCState);
					mobEdu.enquire.f.cAppStateCode = record.data.localAppCState;
				}
				Ext.getCmp('appCity').setValue(record.data.localAppCCity);
				//                if(record.data.localAppCCounty != '' && record.data.localAppCCounty != undefined && record.data.localAppCCounty != null){
				//                    Ext.getCmp('appCounty').setValue(record.data.localAppCCounty);
				//                }
				Ext.getCmp('appZip').setValue(record.data.localAppCZip);
				if (record.data.localAppCCountry != '' && record.data.localAppCCountry != undefined && record.data.localAppCCountry != null) {
					Ext.getCmp('appCountry').setValue(record.data.localAppCCountry);
				}
				Ext.getCmp('appPhone1').setValue(record.data.localAppCPhone1);
				Ext.getCmp('appPhone2').setValue(record.data.localAppCPhone2);
				Ext.getCmp('appEmail').setValue(record.data.localAppCEmail);
				//var stateStore = Ext.getCmp('appState').getStore();
				mobEdu.util.get('mobEdu.enquire.view.appState');
				var stateStore = Ext.getCmp('stateAppList').getStore();
				var stateData = stateStore.data.all;
				for (var j = 0; j < stateData.length; j++) {
					if (stateData[j].data.enumValue == record.data.localAppCState) {
						Ext.getCmp('stAppList').setValue(stateData[j].data.displayValue);
					}
				}
			}
		},

		resetCreateAppContactInfo: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppContactAddress');
		},

		loadStateEnumerations: function() {
			var enumType = 'STVSTAT';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadStateEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadStateEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadCountryEnumerations: function() {
			var enumType = 'STVNATN';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.countryEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadCountryEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadCountryEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.countryEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadCountyEnumerations: function() {
			var enumType = 'STVCNTY';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.countyEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadCountyEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadCountyEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.countyEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCreateAppVisaDetail: function() {
			mobEdu.enquire.f.loadVisaTypeEnumerations();
			mobEdu.enquire.f.loadVisaNationEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppParentDetails);
			mobEdu.util.show('mobEdu.enquire.view.createAppVisaDetails');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            localAppStore.load();
			var record = localAppStore.getAt(0);
			if (record != undefined) {
				if (record.data.localAppVType != '' && record.data.localAppVType != undefined && record.data.localAppVType != null) {
					Ext.getCmp('appVType').setValue(record.data.localAppVType);
				}
				//                Ext.getCmp('appVType').setValue(record.data.localAppVType);
				if (record.data.localAppVNation != '' && record.data.localAppVNation != undefined && record.data.localAppVNation != null) {
					Ext.getCmp('appCitizenship').setValue(record.data.localAppVNation);
				}
				//                Ext.getCmp('appCitizenship').setValue(record.data.localAppVNation);
				Ext.getCmp('appVNo').setValue(record.data.localAppVNo);

			}
		},

		resetCreateAppVisaDetail: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppVisaDetails');
		},

		loadVisaTypeEnumerations: function() {
			var enumType = 'STVVTYP';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.visaTypeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadVisaTypeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadVisaTypeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.visaTypeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadVisaNationEnumerations: function() {
			var enumType = 'STVNATN';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.visaNationEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadVisaNationEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadVisaNationEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.visaNationEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCreateAppParentDetails: function() {
			mobEdu.enquire.f.loadParentEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppContactInfo);
			mobEdu.util.show('mobEdu.enquire.view.createAppParentDetails');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			var record = localAppStore.getAt(0);
			if (record != undefined) {
				if (record.data.localAppParRel1 != '' && record.data.localAppParRel1 != undefined && record.data.localAppParRel1 != null) {
					Ext.getCmp('appPRel1').setValue(record.data.localAppParRel1);
				}
				//                Ext.getCmp('appPRel1').setValue(record.data.localAppParRel1);
				Ext.getCmp('appPFName1').setValue(record.data.localAppParFN1);
				Ext.getCmp('appPLName1').setValue(record.data.localAppParLN1);
				Ext.getCmp('appPMName1').setValue(record.data.localAppParMN1);
				if (record.data.localAppParRel2 != '' && record.data.localAppParRel2 != undefined && record.data.localAppParRel2 != null) {
					Ext.getCmp('appPRel2').setValue(record.data.localAppParRel2);
				}
				//                Ext.getCmp('appPRel2').setValue(record.data.localAppParRel2);
				Ext.getCmp('appPFName2').setValue(record.data.localAppParFN2);
				Ext.getCmp('appPLName2').setValue(record.data.localAppParLN2);
				Ext.getCmp('appPMName2').setValue(record.data.localAppParMN2);
			}
		},

		resetCreateAppParentDetails: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppParentDetails');
		},

		loadParentEnumerations: function() {
			var enumType = 'STVRELT';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.parentEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadParentEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadParentEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.parentEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCreateAppSchoolDetails: function() {
			mobEdu.enquire.f.loadSchoolCodeEnumerations();
			mobEdu.enquire.f.loadStateEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppVisaDetail);
			mobEdu.util.show('mobEdu.enquire.view.createAppHighSchoolInfo');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            localAppStore.load();
			var record = localAppStore.getAt(0);
			if (record.data.localAppHsName === null) {
				var profileStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
				var record = profileStore.getAt(0);
				Ext.getCmp('appSName').setValue(record.data.highSchool);
			} else if (record != undefined) {
				Ext.getCmp('appSName').setValue(record.data.localAppHsName);
				if (record.data.localAppHsCode != '' && record.data.localAppHsCode != undefined && record.data.localAppHsCode != null) {
					Ext.getCmp('appSCode').setValue(record.data.localAppHsCode);
				}
				//                Ext.getCmp('appSCode').setValue(record.data.localAppHsCode);
				Ext.getCmp('appSGpa').setValue(record.data.localAppHsGpa);
				Ext.getCmp('appSCity').setValue(record.data.localAppHsCity);
				if (record.data.localAppHsState != '' && record.data.localAppHsState != undefined && record.data.localAppHsState != null) {
					Ext.getCmp('appSState').setValue(record.data.localAppHsState);
				}
				//                Ext.getCmp('appSState').setValue(record.data.localAppHsState);
				Ext.getCmp('appSZipCode').setValue(record.data.localAppHsZip);
				Ext.getCmp('appSGYear').setValue(record.data.localAppHsGYear);
			}
		},

		resetCreateAppSchoolDetails: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppHighSchoolInfo');
		},

		loadSchoolCodeEnumerations: function() {
			var enumType = 'STVSBGI';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.schoolCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.schoolCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		schoolCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.schoolCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCreateAdmissionDetails: function() {
			mobEdu.enquire.f.loadTermEnumerations();
			mobEdu.enquire.f.levelCodeEnumerations();
			mobEdu.enquire.f.majorCodeEnumerations();
			mobEdu.enquire.f.loadStudentTypeEnumerations();
			mobEdu.enquire.f.loadAdmissionTypeEnumerations();
			mobEdu.enquire.f.loadResidenceCodeEnumerations();
			mobEdu.enquire.f.loadCollegeCodeEnumerations();
			mobEdu.enquire.f.loadDegreeCodeEnumerations();
			mobEdu.enquire.f.loadDepartmentEnumerations();
			mobEdu.enquire.f.loadCampusEnumerations();
			mobEdu.enquire.f.loadEduGoalEnumerations();
			mobEdu.enquire.f.loadAppStatusEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppSchoolDetails);
			mobEdu.util.show('mobEdu.enquire.view.createAppAdmissionDetails');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            localAppStore.load();
			var record = localAppStore.getAt(0);
			if (record != undefined) {
				if (record.data.localAppStartTerm != '' && record.data.localAppStartTerm != undefined && record.data.localAppStartTerm != null) {
					Ext.getCmp('appStartTerm').setValue(record.data.localAppStartTerm);
				}
				if (record.data.localAppLevelCode != '' && record.data.localAppLevelCode != undefined && record.data.localAppLevelCode != null) {
					Ext.getCmp('appLCode').setValue(record.data.localAppLevelCode);
				}
				//                Ext.getCmp('appLCode').setValue(record.data.localAppLevelCode);
				if (record.data.localAppMajorCode != '' && record.data.localAppMajorCode != undefined && record.data.localAppMajorCode != null) {
					Ext.getCmp('appMCode').setValue(record.data.localAppMajorCode);
				}
				//                Ext.getCmp('appMCode').setValue(record.data.localAppMajorCode);
				if (record.data.localAppSType != '' && record.data.localAppSType != undefined && record.data.localAppSType != null) {
					Ext.getCmp('appSType').setValue(record.data.localAppSType);
				}
				//                Ext.getCmp('appSType').setValue(record.data.localAppSType);
				if (record.data.localAppAdType != '' && record.data.localAppAdType != undefined && record.data.localAppAdType != null) {
					Ext.getCmp('appAType').setValue(record.data.localAppAdType);
				}
				//                Ext.getCmp('appAType').setValue(record.data.localAppAdType);
				if (record.data.localAppResCode != '' && record.data.localAppResCode != undefined && record.data.localAppResCode != null) {
					Ext.getCmp('appRCode').setValue(record.data.localAppResCode);
				}
				//                Ext.getCmp('appRCode').setValue(record.data.localAppResCode);
				if (record.data.localAppCollegeCode != '' && record.data.localAppCollegeCode != undefined && record.data.localAppCollegeCode != null) {
					Ext.getCmp('appAdCCode').setValue(record.data.localAppCollegeCode);
				}
				//                Ext.getCmp('appAdCCode').setValue(record.data.localAppCollegeCode);
				if (record.data.localAppDegreeCode != '' && record.data.localAppDegreeCode != undefined && record.data.localAppDegreeCode != null) {
					Ext.getCmp('appDCode').setValue(record.data.localAppDegreeCode);
				}
				//                Ext.getCmp('appDCode').setValue(record.data.localAppDegreeCode);
				if (record.data.localAppCampus != '' && record.data.localAppCampus != undefined && record.data.localAppCampus != null) {
					Ext.getCmp('appCampus').setValue(record.data.localAppCampus);
				}
				//                Ext.getCmp('appCampus').setValue(record.data.localAppCampus);
				if (record.data.localAppEduGoal != '' && record.data.localAppEduGoal != undefined && record.data.localAppEduGoal != null) {
					Ext.getCmp('appEGoal').setValue(record.data.localAppEduGoal);
				}
				//                Ext.getCmp('appEGoal').setValue(record.data.localAppEduGoal);
				if (record.data.localAppDep != '' && record.data.localAppDep != undefined && record.data.localAppDep != null) {
					Ext.getCmp('appDep').setValue(record.data.localAppDep);
				}
				//                Ext.getCmp('appDep').setValue(record.data.localAppDep);
				if (record.data.localAppStatus != '' && record.data.localAppStatus != undefined && record.data.localAppStatus != null) {
					Ext.getCmp('appAppStatus').setValue(record.data.localAppStatus);
				}
				//                Ext.getCmp('appFTime').setValue(record.data.localAppFullTime);
				//                Ext.getCmp('appAppStatus').setValue(record.data.localAppStatus);
			}
		},

		resetCreateAdmissionDetails: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppAdmissionDetails');
		},

		loadTermEnumerations: function() {
			var enumType = 'STVTERM';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.termEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.loadTermEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		loadTermEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.termEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		levelCodeEnumerations: function() {
			var enumType = 'STVLEVL';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.levelCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.levelCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		levelCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.levelCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		majorCodeEnumerations: function() {
			var enumType = 'STVMAJR';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.majorCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.majorCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		majorCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.majorCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadStudentTypeEnumerations: function() {
			var enumType = 'STVSTYP';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.studentTypeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.studentTypeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		studentTypeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.studentTypeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadAdmissionTypeEnumerations: function() {
			var enumType = 'STVADMT';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.admissionTypeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.admissionTypeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		admissionTypeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.admissionTypeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadResidenceCodeEnumerations: function() {
			var enumType = 'STVRESD';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.residenceCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.residenceCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		residenceCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.residenceCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadCollegeCodeEnumerations: function() {
			var enumType = 'STVCOLL';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.collegeCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.collegeCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		collegeCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.collegeCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadDegreeCodeEnumerations: function() {
			var enumType = 'STVDEGC';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.degreeCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.degreeCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		degreeCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.degreeCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadDepartmentEnumerations: function() {
			var enumType = 'STVDEPT';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.departmentEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.departmentEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		departmentEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.departmentEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadCampusEnumerations: function() {
			var enumType = 'STVCAMP';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.campusEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.campusEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		campusEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.campusEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadEduGoalEnumerations: function() {
			var enumType = 'STVEGOL';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.eduGoalEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.eduGoalEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		eduGoalEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.eduGoalEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadAppStatusEnumerations: function() {
			var enumType = 'STVAPST';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.appStatusEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.appStatusEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		appStatusEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.appStatusEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showCreateAppTestScores: function() {
			var dateStore = mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid');
			dateStore.load();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppInterests);
			mobEdu.util.show('mobEdu.enquire.view.testScores');
		},

		resetCreateAppTestScores: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppTestScores');
		},

		showCreateAppCollegeDetails: function() {
			var collegeInfoStore = mobEdu.util.getStore('mobEdu.enquire.store.collegesJsonString');
			collegeInfoStore.load();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppTestScores);
			mobEdu.util.show('mobEdu.enquire.view.createAppCollegeDetails');
		},

		resetCreateAppCollegeDetails: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppCollegeDetails');
		},

		showCreateAppInterests: function() {
			mobEdu.enquire.f.loadPInterestEnumerations();
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAdmissionDetails);
			mobEdu.util.show('mobEdu.enquire.view.createAppInterests');
			var localAppStore = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            localAppStore.load();
			var record = localAppStore.getAt(0);
			if (record != undefined) {
				if (record.data.localAppPInterest != '' && record.data.localAppPInterest != undefined && record.data.localAppPInterest != null) {
					Ext.getCmp('appPrimary').setValue(record.data.localAppPInterest);
				}
				if (record.data.localAppSInterest != '' && record.data.localAppSInterest != undefined && record.data.localAppSInterest != null) {
					Ext.getCmp('appSecondary').setValue(record.data.localAppSInterest);
				}
				Ext.getCmp('appLevel').setValue(record.data.localAppLInterest);
				Ext.getCmp('appFactor').setValue(record.data.localAppFactor);
				if (record.data.localAppOtherIn != '' && record.data.localAppOtherIn != undefined && record.data.localAppOtherIn != null) {
					Ext.getCmp('appOtherI').setValue(record.data.localAppOtherIn);
				}
				//                Ext.getCmp('appOtherI').setValue(record.data.localAppOtherIn);

			}
		},

		resetCreateAppInterests: function() {
			mobEdu.util.get('mobEdu.enquire.view.createAppInterests');
		},

		loadPInterestEnumerations: function() {
			var enumType = 'STVINTS';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.pInterestEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.pInterestEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		pInterestEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.pInterestEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadCreateBasicInfo: function() {
			var appFirstN = Ext.getCmp('appFName').getValue();
			var appLastN = Ext.getCmp('appLName').getValue();
			var appGen = Ext.getCmp('appGender').getValue();
			var appRaceValue = Ext.getCmp('appRace').getValue();
			if (appRaceValue == "select ") {
				appRaceValue = '';
			}
			//            var appEthCity = Ext.getCmp('appECity').getValue();
			//            if(appEthCity == "select "){
			//                appEthCity = '';
			//            }
			var appSSN = Ext.getCmp('appssn').getValue();
			if (appGen == 'select') {
				appGen = '';
			}
			var appDob = Ext.getCmp('appDateofbirth').getValue();
			var dobFormatted;
			if (appDob == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([appDob.getMonth() + 1] + '/' + appDob.getDate() + '/' + appDob.getFullYear());
			}
			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.localAppFirstName = appFirstN;
				record.data.localAppLastName = appLastN;
				record.data.localAppGender = appGen;
				record.data.localAppRace = appRaceValue;
				record.data.localAppDateOfBirth = dobFormatted;
				//                record.data.localAppEthnicity=appEthCity;
				record.data.localAppSSN = appSSN;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					localAppFirstName: appFirstN,
					localAppLastName: appLastN,
					localAppGender: appGen,
					localAppDateOfBirth: dobFormatted,
					localAppRace: appRaceValue,
					//                    localAppEthnicity:appEthCity,
					localAppSSN: appSSN
				});
				store.sync();
			}
			if (appFirstN != null && appFirstN != '' && appLastN != null && appLastN != '' && appGen != null && appGen != '' && appDob != null && appDob != '') {

				var firstName = Ext.getCmp('appFName');
				var regExp = new RegExp("^[a-zA-Z ]+$");
				if (!regExp.test(appFirstN)) {
					Ext.Msg.show({
						id: 'firstNmae',
						name: 'firstNmae',
						cls: 'msgbox',
						title: null,
						message: '<p>Invalid First Name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								firstName.focus(true);
							}
						}
					});
				} else {
					var lastName = Ext.getCmp('appLName');
					var lastNRegExp = new RegExp("^[a-zA-Z ]+$");
					if (!lastNRegExp.test(appLastN)) {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							cls: 'msgbox',
							title: null,
							message: '<p>Invalid Last Name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									lastName.focus(true);
								}
							}
						});
					} else {
						var lastName = Ext.getCmp('appLName');
						var lName = lastName.getValue();
						if (lName.charAt(0) == ' ') {
							Ext.Msg.show({
								id: 'lastNmae',
								name: 'lastNmae',
								cls: 'msgbox',
								title: null,
								message: '<p>LastName must starts with alphabet</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										lastName.focus(true);
									}
								}
							});
						} else {
							var firstName = Ext.getCmp('appFName');
							var fName = firstName.getValue();
							if (fName.charAt(0) == ' ') {
								Ext.Msg.show({
									id: 'lastNmae',
									name: 'lastNmae',
									cls: 'msgbox',
									title: null,
									message: '<p>FirstName must starts with alphabet</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											firstName.focus(true);
										}
									}
								});
							} else {
								var ssnNo = Ext.getCmp('appssn');
								var ssNValue = ssnNo.getValue();
								if (ssNValue == null && ssNValue == '') {
									mobEdu.enquire.f.showCreateAppContactInfo();
								} else {
									ssNValue = ssNValue.replace(/-|\(|\)/g, "");
									var length = ssNValue.length;
									if (ssNValue.length != 9 && ssNValue.length != 0) {
										Ext.Msg.show({
											id: 'ssnMsg',
											name: 'ssnMsg',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid SSN Number.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													ssnNo.focus(true);
												}
											}
										});
									} else {
										mobEdu.enquire.f.showCreateAppContactInfo();
									}
								}
							}
						}
					}
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadCreateContactInfo: function() {
			var appAdd1 = Ext.getCmp('appAddress1').getValue();
			var appAdd2 = Ext.getCmp('appAddress2').getValue();
			//            var appAdd3 = Ext.getCmp('appAddress3').getValue();
			var appCityName = Ext.getCmp('appCity').getValue();
			var appStateName = mobEdu.enquire.f.cAppStateCode;
			if (appStateName == "select ") {
				appStateName = '';
			}
			//            var appCountyValue = Ext.getCmp('appCounty').getValue();
			//            if(appCountyValue == "select "){
			//                appCountyValue = '';
			//            }
			var appZipCode = Ext.getCmp('appZip').getValue();
			var appCountryName = Ext.getCmp('appCountry').getValue();
			if (appCountryName == "select ") {
				appCountryName = '';
			}
			var appPh1 = Ext.getCmp('appPhone1').getValue().toString();
			appPh1 = appPh1.replace(/\D/g, '');
			var appPh2 = Ext.getCmp('appPhone2').getValue().toString();
			appPh2 = appPh2.replace(/\D/g, '');
			var appMail = Ext.getCmp('appEmail').getValue();
			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			store.removeAll();
			store.sync();
			if (record != undefined) {
				record.data.localAppCAddress1 = appAdd1;
				record.data.localAppCAddress2 = appAdd2;
				//                record.data.localAppCAddress3=appAdd3;
				record.data.localAppCCity = appCityName;
				//                record.data.localAppCCounty=appCountyValue;
				record.data.localAppCState = appStateName;
				record.data.localAppCZip = appZipCode;
				record.data.localAppCCountry = appCountryName;
				record.data.localAppCPhone1 = appPh1;
				record.data.localAppCPhone2 = appPh2;
				record.data.localAppCEmail = appMail;
				store.add(record.copy());
				store.sync();
			}
			if (appAdd1 != '' && appAdd1 != null && appCityName != '' && appCityName != null &&
				appStateName != '' && appStateName != null && appZipCode != '' && appZipCode != null && appCountryName != '' && appCountryName != null) {
				var zipCode = Ext.getCmp('appZip');
				var zipValue = zipCode.getValue().toString();
				if (zipValue.length != 5) {
					console.log('loaded');
					Ext.Msg.show({
						id: 'zipcodevalue',
						name: 'zipcodevalue',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid Zip Code.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								zipCode.focus(true);
							}
						}
					});
				} else {
					var cStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
					cStore.getProxy().setUrl(enquireWebserver + 'enquire/common/validatelocation?city=' + appCityName + '&state=' + appStateName + '&zip=' + appZipCode);
					cStore.getProxy().setExtraParams({
						companyId: companyId
					});
					cStore.getProxy().afterRequest = function() {
						var cityStatus = cStore.getProxy().getReader().rawData;
						if (cityStatus.status != 'success') {
							Ext.Msg.show({
								title: null,
								message: '<p>Invalid City,State and Zip code combination.</p>',
								buttons: Ext.MessageBox.OK,
								cls: 'msgbox'
							});
						} else {
							var cityField = Ext.getCmp('appCity');
							var cityRegExp1 = new RegExp("^[a-zA-Z ]+$");
							var cityRegExp2 = new RegExp("^[a-zA-Z]+$");

							if (!cityRegExp1.test(appCityName)) {
								if (!cityRegExp2.test(appCityName)) {
									Ext.Msg.show({
										id: 'cityName',
										name: 'cityName',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid City.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												cityField.focus(true);
											}
										}
									});
								}
							} else {
								var country = Ext.getCmp('appCountry');
								var countryRegExp = new RegExp("^[a-zA-Z]+$");
								//                        if(appCountryName != '' && (!countryRegExp.test(appCountryName))) {
								if (appCountryName == '') {
									Ext.Msg.show({
										id: 'countryNmae',
										name: 'countryNmae',
										title: null,
										cls: 'msgbox',
										message: '<p>Invalid Country Name.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												country.focus(true);
											}
										}
									});
								} else {
									var phone1 = Ext.getCmp('appPhone1');
									var phone2 = Ext.getCmp('appPhone2');
									var phone1Value = phone1.getValue();
									phone1Value = phone1Value.replace(/-|\(|\)/g, "");
									var phone2Value = phone2.getValue();
									phone2Value = phone2Value.replace(/-|\(|\)/g, "");

									var length = phone1Value.length;
									if (phone1Value.length != 10 && phone1Value.length != 0) {
										Ext.Msg.show({
											id: 'phone1msg',
											name: 'phone1msg',
											title: null,
											cls: 'msgbox',
											message: '<p>Invalid Phone Number.</p>',
											buttons: Ext.MessageBox.OK,
											fn: function(btn) {
												if (btn == 'ok') {
													phone1.focus(true);
												}
											}
										});
									} else {
										var a = phone1Value.replace(/\D/g, '');
										//                                var newValue = a.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
										//                                phone1.setValue(newValue);
										length = phone2Value.length;
										if (phone2Value.length != 10 && phone2Value.length != 0) {
											Ext.Msg.show({
												id: 'phone2msg',
												name: 'phone1msg',
												title: null,
												cls: 'msgbox',
												message: '<p>Invalid Phone Number.</p>',
												buttons: Ext.MessageBox.OK,
												fn: function(btn) {
													if (btn == 'ok') {
														phone2.focus(true);
													}
												}
											});
										} else {
											var b = phone2Value.replace(/\D/g, '');
											//                                   var newValue = b.replace(/^(\d{3})(\d{3})(\d{4})$/, '($1)$2-$3');
											//                                    phone2.setValue(newValue);
											var mail = Ext.getCmp('appEmail');
											var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
											if (appMail != '' && regMail.test(appMail) == false) {
												Ext.Msg.show({
													id: 'email',
													name: 'email',
													title: null,
													cls: 'msgbox',
													message: '<p>Invalid Email address.</p>',
													buttons: Ext.MessageBox.OK,
													fn: function(btn) {
														if (btn == 'ok') {
															mail.focus(true);
														}
													}
												});
											} else {
												mobEdu.enquire.f.showCreateAppParentDetails();
											}
										}
									}
								}
							}
						}
					}
					cStore.load();
				}
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},
		loadCreateParentDetails: function() {
			var appParentRel1 = Ext.getCmp('appPRel1').getValue();
			if (appParentRel1 == "select ") {
				appParentRel1 = '';
			}
			var appParentFN1 = Ext.getCmp('appPFName1').getValue();
			var appParentLN1 = Ext.getCmp('appPLName1').getValue();
			var appParentMN1 = Ext.getCmp('appPMName1').getValue();
			var appParentRel2 = Ext.getCmp('appPRel2').getValue();
			if (appParentRel2 == "select ") {
				appParentRel1 = '';
			}
			var appParentFN2 = Ext.getCmp('appPFName2').getValue();
			var appParentLN2 = Ext.getCmp('appPLName2').getValue();
			var appParentMN2 = Ext.getCmp('appPMName2').getValue();

			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.localAppParRel1 = appParentRel1;
				record.data.localAppParFN1 = appParentFN1;
				record.data.localAppParLN1 = appParentLN1;
				record.data.localAppParMN1 = appParentMN1;
				record.data.localAppParRel2 = appParentRel2;
				record.data.localAppParFN2 = appParentFN2;
				record.data.localAppParLN2 = appParentLN2;
				record.data.localAppParMN2 = appParentMN2;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					localAppParRel1: appParentRel1,
					localAppParFN1: appParentFN1,
					localAppParLN1: appParentLN1,
					localAppParMN1: appParentMN1,
					localAppParRel2: appParentRel2,
					localAppParFN2: appParentFN2,
					localAppParLN2: appParentLN2,
					localAppParMN2: appParentMN2
				});
				store.sync();
			}
			//            if (appParentRel1 == null && appParentRel1 == '' && appParentFN1 == null && appParentFN1 == ''
			//                && appParentLN1 == null && appParentLN1 == '') {

			//            var parentRel1 = Ext.getCmp('appPRel1');
			//            var regParRel = new RegExp("^[a-zA-Z]+$");
			////            if(appParentRel1 != '' && (!regParRel.test(appParentRel1))) {
			//                if(appParentRel1 == ''){
			//                Ext.Msg.show({
			//                    id:'parentRel',
			//                    name:'parentRel',
			//                    cls:'msgbox',
			//                    title:null,
			//                    message:'<p>Invalid parent relation.</p>',
			//                    buttons:Ext.MessageBox.OK,
			//                    fn:function (btn) {
			//                        if (btn == 'ok') {
			//                            parentRel1.focus(true);
			//                        }
			//                    }
			//                });
			//            } else{
			var firstName = Ext.getCmp('appPFName1');
			var regExp = new RegExp("^[a-zA-Z]+$");
			if (appParentFN1 != '' && (!regExp.test(appParentFN1))) {
				Ext.Msg.show({
					id: 'firstNmae',
					name: 'firstNmae',
					cls: 'msgbox',
					title: null,
					message: '<p>Invalid First Name.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							firstName.focus(true);
						}
					}
				});
			} else {
				var lastName = Ext.getCmp('appPLName1');
				var lastNRegExp = new RegExp("^[a-zA-Z]+$");
				if (appParentLN1 != '' && !lastNRegExp.test(appParentLN1)) {
					Ext.Msg.show({
						id: 'lastNmae',
						name: 'lastNmae',
						cls: 'msgbox',
						title: null,
						message: '<p>Invalid Last Name.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								lastName.focus(true);
							}
						}
					});
				} else {
					var middleName = Ext.getCmp('appPMName1');
					var middleNRegExp = new RegExp("^[a-zA-Z]+$");
					if (appParentMN1 != '' && !middleNRegExp.test(appParentMN1)) {
						Ext.Msg.show({
							id: 'lastNmae',
							name: 'lastNmae',
							cls: 'msgbox',
							title: null,
							message: '<p>Invalid Middle Name.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									middleName.focus(true);
								}
							}
						});
					} else {
						//                            var parentRel2 = Ext.getCmp('appPRel2');
						//                            var regParRel2 = new RegExp("^[a-zA-Z]+$");
						////                            if(appParentRel2 != '' && (!regParRel2.test(appParentRel2))) {
						//                                if(appParentRel2 == ''){
						//                                Ext.Msg.show({
						//                                    id:'parentRel',
						//                                    name:'parentRel',
						//                                    cls:'msgbox',
						//                                    title:null,
						//                                    message:'<p>Invalid parent relation.</p>',
						//                                    buttons:Ext.MessageBox.OK,
						//                                    fn:function (btn) {
						//                                        if (btn == 'ok') {
						//                                            parentRel2.focus(true);
						//                                        }
						//                                    }
						//                                });
						//                            } else{
						var firstN2 = Ext.getCmp('appPFName2');
						var regExp2 = new RegExp("^[a-zA-Z]+$");
						if (appParentFN2 != '' && (!regExp2.test(appParentFN2))) {
							Ext.Msg.show({
								id: 'firstNmae',
								name: 'firstNmae',
								cls: 'msgbox',
								title: null,
								message: '<p>Invalid First Name.</p>',
								buttons: Ext.MessageBox.OK,
								fn: function(btn) {
									if (btn == 'ok') {
										firstN2.focus(true);
									}
								}
							});
						} else {
							var lastN2 = Ext.getCmp('appPLName2');
							var lastN2RegExp = new RegExp("^[a-zA-Z]+$");
							if (appParentLN2 != '' && !lastN2RegExp.test(appParentLN2)) {
								Ext.Msg.show({
									id: 'lastNmae',
									name: 'lastNmae',
									cls: 'msgbox',
									title: null,
									message: '<p>Invalid Last Name.</p>',
									buttons: Ext.MessageBox.OK,
									fn: function(btn) {
										if (btn == 'ok') {
											lastN2.focus(true);
										}
									}
								});
							} else {
								var middleN2 = Ext.getCmp('appPMName2');
								var middleN2RegExp = new RegExp("^[a-zA-Z]+$");
								if (appParentMN2 != '' && !middleN2RegExp.test(appParentMN2)) {
									Ext.Msg.show({
										id: 'lastNmae',
										name: 'lastNmae',
										cls: 'msgbox',
										title: null,
										message: '<p>Invalid Middle Name.</p>',
										buttons: Ext.MessageBox.OK,
										fn: function(btn) {
											if (btn == 'ok') {
												middleN2.focus(true);
											}
										}
									});
								} else {
									mobEdu.enquire.f.showCreateAppVisaDetail();
								}
							}
						}
						//                            }
					}
				}
			}
			//            }
			//            } else {
			//                mobEdu.enquire.f.showCreateAppVisaDetail();
			//                Ext.Msg.show({
			//                    title:null,
			//                    message:'<p>Please enter all mandatory fields.</p>',
			//                    buttons:Ext.MessageBox.OK,
			//                    cls:'msgbox'
			//                });
			//            }
		},

		loadCreateVisaDetails: function() {
			var appVisaType = Ext.getCmp('appVType').getValue();
			if (appVisaType == "select ") {
				appVisaType = '';
			}
			var appVisaCitizen = Ext.getCmp('appCitizenship').getValue();
			if (appVisaCitizen == "select ") {
				appVisaCitizen = '';
			}
			var appVisaNo = Ext.getCmp('appVNo').getValue();
			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.localAppVType = appVisaType;
				record.data.localAppVNation = appVisaCitizen;
				record.data.localAppVNo = appVisaNo;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					localAppVType: appVisaType,
					localAppVNation: appVisaCitizen,
					localAppVNo: appVisaNo
				});
				store.sync();
			}
			var visaNO = Ext.getCmp('appVNo');
			var visaNOValue = visaNO.getValue();
			if (visaNOValue != '' && visaNOValue != 0 && visaNOValue != null) {
				visaNOValue = visaNOValue.replace(/-|\(|\)/g, "");
				var length = visaNOValue.length;
				if (visaNOValue.length != 16 && visaNOValue.length != 0) {
					Ext.Msg.show({
						id: 'visaNOMsg',
						name: 'visaNOMsg',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid Visa Number.</p>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								visaNO.focus(true);
							}
						}
					});
				} else {
					//                    var a = visaNOValue.replace(/\D/g, '');
					mobEdu.enquire.f.showCreateAppSchoolDetails();
				}
			} else {
				mobEdu.enquire.f.showCreateAppSchoolDetails();
			}

		},

		loadCreateSchoolDetails: function() {
			var appSchoolName = Ext.getCmp('appSName').getValue();
			var appSchoolCode = Ext.getCmp('appSCode').getValue();
			if (appSchoolCode == "select ") {
				appSchoolCode = '';
			}
			var appSchoolCity = Ext.getCmp('appSCity').getValue();
			var appSchoolSate = mobEdu.enquire.f.highSchoolStateCode;
			if (appSchoolSate == "select ") {
				appSchoolSate = '';
			}
			var appSchoolPostalCode = Ext.getCmp('appSZipCode').getValue();
			var appSchoolYear = Ext.getCmp('appSGYear').getValue();
			var appSchoolGpa = Ext.getCmp('appSGpa').getValue();
			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			//            store.removeAll();
			//            store.sync();
			if (record != undefined) {
				record.data.localAppHsName = appSchoolName;
				record.data.localAppHsCode = appSchoolCode;
				record.data.localAppHsCity = appSchoolCity;
				record.data.localAppHsState = appSchoolSate;
				record.data.localAppHsZip = appSchoolPostalCode;
				record.data.localAppHsGYear = appSchoolYear;
				record.data.localAppHsGpa = appSchoolGpa;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					localAppHsName: appSchoolName,
					localAppHsCode: appSchoolCode,
					localAppHsCity: appSchoolCity,
					localAppHsState: appSchoolSate,
					localAppHsZip: appSchoolPostalCode,
					localAppHsGYear: appSchoolYear,
					localAppHsGpa: appSchoolGpa
				});
				store.sync();
			}
			//            if (appSchoolName == '' && appSchoolName == null && appSchoolCode == '' && appSchoolCode ==null && appSchoolCity == '' && appSchoolCity == null && appSchoolSate == '' && appSchoolSate == null &&
			//                appSchoolPostalCode == '' && appSchoolPostalCode == null && appSchoolYear == '' && appSchoolYear == null && appSchoolGpa == '' && appSchoolGpa == null) {

			var greYear = Ext.getCmp('appSGYear');
			var yearValue = greYear.getValue();
			var year;
			if (yearValue != null) {
				year = yearValue.toString();
			}
			var gpaValue = Ext.getCmp('appSGpa');
			var valueGpa = gpaValue.getValue();
			var gpaNo;
			if (valueGpa != null) {
				gpaNo = valueGpa.toString();
				gpaNo = gpaNo.replace(/-|\(|\)/g, "");
			}
			if (greYear.getValue() != null && greYear.getValue() != 0 && year.length != 4 && year.length != 0) {

				//                     if (yearValue.length != 4 && yearValue.length != 0) {
				Ext.Msg.show({
					id: 'yearmsg',
					name: 'yearmsg',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid Year.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							greYear.focus(true);
						}
					}
				});
				//                     }
			} else {
				if (valueGpa != null && valueGpa != '' && valueGpa != 0 && (valueGpa > 4 || valueGpa < 1)) {

					//                             if () {
					Ext.Msg.show({
						id: 'gpamsg',
						name: 'gpamsg',
						title: null,
						cls: 'msgbox',
						message: '<p>Invalid GPA Value<br>Please enter a value between 1.0 and 4.0</P>',
						buttons: Ext.MessageBox.OK,
						fn: function(btn) {
							if (btn == 'ok') {
								gpaValue.focus(true);
							}
						}
					});
					//                             }
				} else {
					var zip = Ext.getCmp('appSZipCode');
					var zipValue = zip.getValue();
					var zipText;
					if (zipValue != null) {
						zipText = zipValue.toString();
					}
					if (zip.getValue() != null && zip.getValue() != 0 && zipText.length != 5 && zipText.length != 0) {
						//                                     if (zipValue.length != 5 && zipValue.length != 0) {
						Ext.Msg.show({
							id: 'zipcodevalue',
							name: 'zipcodevalue',
							title: null,
							cls: 'msgbox',
							message: '<p>Invalid Zip Code.</p>',
							buttons: Ext.MessageBox.OK,
							fn: function(btn) {
								if (btn == 'ok') {
									zip.focus(true);
								}
							}
						});
						//                                     }else{
						//                                         mobEdu.enquire.f.showCreateAdmissionDetails();

					} else {
						if (appSchoolCity != null && appSchoolCity != '' && appSchoolSate != null && appSchoolSate != '' && appSchoolPostalCode != null && appSchoolPostalCode != 0) {
							var cStore = mobEdu.util.getStore('mobEdu.enquire.store.application');
							cStore.getProxy().setHeaders({
								companyID: companyId
							});
							cStore.getProxy().setUrl(enquireWebserver + 'enquire/common/validatelocation?city=' + appSchoolCity + '&state=' + appSchoolSate + '&zip=' + appSchoolPostalCode);
							cStore.getProxy().setExtraParams({
								companyId: companyId
							});
							cStore.getProxy().afterRequest = function() {
								var cityStatus = cStore.getProxy().getReader().rawData;
								if (cityStatus.status != 'success') {
									Ext.Msg.show({
										title: null,
										message: '<p>Invalid City,State and Zip code combination.</p>',
										buttons: Ext.MessageBox.OK,
										cls: 'msgbox'
									});
								} else {
									mobEdu.enquire.f.showCreateAdmissionDetails();
								}
							}
							cStore.load();
						} else {
							mobEdu.enquire.f.showCreateAdmissionDetails();
						}

						//                             }
						//                                     mobEdu.enquire.f.showCreateAdmissionDetails();
					}
				}
			}

		},

		loadCreateAdmissionDetails: function() {
			var appSTerm = Ext.getCmp('appStartTerm').getValue();
			if (appSTerm == "select ") {
				appSTerm = '';
			}
			var appLevelCode = Ext.getCmp('appLCode').getValue();
			if (appLevelCode == "select ") {
				appLevelCode = '';
			}
			var appMajorC = Ext.getCmp('appMCode').getValue();
			if (appMajorC == "select ") {
				appMajorC = '';
			}
			var appStudentType = Ext.getCmp('appSType').getValue();
			if (appStudentType == "select ") {
				appStudentType = '';
			}
			var appAdmissionType = Ext.getCmp('appAType').getValue();
			if (appAdmissionType == "select ") {
				appAdmissionType = '';
			}
			var appResidenceCode = Ext.getCmp('appRCode').getValue();
			if (appResidenceCode == "select ") {
				appResidenceCode = '';
			}
			var appDegreeCode = Ext.getCmp('appDCode').getValue();
			if (appDegreeCode == "select ") {
				appDegreeCode = '';
			}
			var appCollegeCode = Ext.getCmp('appAdCCode').getValue();
			if (appCollegeCode == "select ") {
				appCollegeCode = '';
			}
			var appCampus = Ext.getCmp('appCampus').getValue();
			if (appCampus == "select ") {
				appCampus = '';
			}
			var appEduGoal = Ext.getCmp('appEGoal').getValue();
			if (appEduGoal == "select ") {
				appEduGoal = '';
			}
			var appApplicationStatus = Ext.getCmp('appAppStatus').getValue();
			if (appApplicationStatus == "select ") {
				appApplicationStatus = '';
			}
			var appDepartment = Ext.getCmp('appDep').getValue();
			if (appDepartment == "select ") {
				appDepartment = '';
			}
			//            var appFullTime = Ext.getCmp('appFTime').getValue()
			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.localAppStartTerm = appSTerm;
				record.data.localAppLevelCode = appLevelCode;
				record.data.localAppMajorCode = appMajorC;
				record.data.localAppSType = appStudentType;
				record.data.localAppAdType = appAdmissionType;
				record.data.localAppResCode = appResidenceCode;
				record.data.localAppCollegeCode = appCollegeCode;
				record.data.localAppDegreeCode = appDegreeCode;
				record.data.localAppCampus = appCampus;
				record.data.localAppEduGoal = appEduGoal;
				record.data.localAppStatus = appApplicationStatus;
				record.data.localAppDep = appDepartment;
				//                record.data.localAppFullTime = appFullTime;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					localAppStartTerm: appSTerm,
					localAppLevelCode: appLevelCode,
					localAppMajorCode: appMajorC,
					localAppSType: appStudentType,
					localAppAdType: appAdmissionType,
					localAppResCode: appResidenceCode,
					localAppCollegeCode: appCollegeCode,
					localAppDegreeCode: appDegreeCode,
					localAppCampus: appCampus,
					localAppEduGoal: appEduGoal,
					localAppStatus: appApplicationStatus,
					localAppDep: appDepartment
						//                    localAppFullTime:appFullTime
				});
				store.sync();
			}
			if (appSTerm != null && appSTerm != '' && appLevelCode != null && appLevelCode != '' && appMajorC != null && appMajorC != '' && appStudentType != null && appStudentType != '' && appResidenceCode != null && appResidenceCode != '' && appDegreeCode != null && appDegreeCode != '' && appApplicationStatus != null && appApplicationStatus != '') {
				mobEdu.enquire.f.showCreateAppInterests();
			} else {
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		loadCreateAppInterests: function() {
			var apPrimarySelect = Ext.getCmp('appPrimary').getValue();
			if (apPrimarySelect == "select ") {
				apPrimarySelect = '';
			}
			var appSecondarySelect = Ext.getCmp('appSecondary').getValue();
			if (appSecondarySelect == "select ") {
				appSecondarySelect = '';
			}
			var appLevelInterest = Ext.getCmp('appLevel').getValue();
			var appFactorCh = Ext.getCmp('appFactor').getValue();
			var appOtherInterest = Ext.getCmp('appOtherI').getValue();
			if (appOtherInterest == "select ") {
				appOtherInterest = '';
			}

			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			if (record != undefined) {
				store.removeAll();
				store.sync();
				record.data.localAppPInterest = apPrimarySelect;
				record.data.localAppSInterest = appSecondarySelect;
				record.data.localAppLInterest = appLevelInterest;
				record.data.localAppFactor = appFactorCh;
				record.data.localAppOtherIn = appOtherInterest;
				store.add(record.copy());
				store.sync();
			} else {
				store.add({
					localAppPInterest: apPrimarySelect,
					localAppSInterest: appSecondarySelect,
					localAppLInterest: appLevelInterest,
					localAppFactor: appFactorCh,
					localAppOtherIn: appOtherInterest
				});
				store.sync();
			}
			//            if (apPrimarySelect != null && apPrimarySelect != '' && appLevelInterest != null && appLevelInterest != '' && appFactorCh != null && appFactorCh != ''
			//                && appOtherInterest != null && appOtherInterest !='') {
			mobEdu.enquire.f.showCreateAppTestScores();
			//            } else {
			//                Ext.Msg.show({
			//                    title:null,
			//                    message:'<p>Please enter all mandatory fields.</p>',
			//                    buttons:Ext.MessageBox.OK,
			//                    cls:'msgbox'
			//                });
			//            }
		},
		loadCreateAppTestScores: function() {
			mobEdu.enquire.f.showCreateAppCollegeDetails();
		},

		loadCreateCollegeDetails: function() {
			var appCollegeName = Ext.getCmp('appCName').getValue();
			var appCollegeCode = Ext.getCmp('appCCode').getValue();
			var appCollegeCity = Ext.getCmp('appCCity').getValue();
			var appCollegeState = Ext.getCmp('appCState').getValue();
			var appCollegeDegreeCode = Ext.getCmp('appCDCode').getValue();
			if (appCollegeState == 'select') {
				appCollegeState = '';
			}
			var appGraDate = Ext.getCmp('appCGDate').getValue();
			var appAttendFrom = Ext.getCmp('appCAtFrom').getValue();
			var appAttendTo = Ext.getCmp('appCAtTo').getValue();

			var store = mobEdu.util.getStore('mobEdu.enquire.store.localCreateApplication');
			//            store.load();
			var record = store.getAt(0);
			store.removeAll();
			store.sync();
			if (record != undefined) {
				record.data.localAppCollName = appCollegeName;
				record.data.localAppCollCode = appCollegeCode;
				record.data.localAppCollCity = appCollegeCity;
				record.data.localAppCollState = appCollegeState;
				record.data.localAppCollDegree = appCollegeDegreeCode;
				record.data.localAppCollGDate = appGraDate;
				record.data.localAppCollAtFrom = appAttendFrom;
				record.data.localAppCollAtTo = appAttendTo;
				store.add(record.copy());
				store.sync();
			}
			//            if (appCollegeName == '' && appCollegeName == null && appCollegeCode != '' && appCollegeCode != null && appCollegeCity != '' && appCollegeCity != null &&
			//                appCollegeState != '' && appCollegeState != null && appCollegeDegreeCode != '' && appCollegeDegreeCode != null && appGraDate != '' && appGraDate != null &&
			//                appAttendFrom != '' && appAttendFrom != null && appAttendTo != '' && appAttendTo != null) {

			var cityField = Ext.getCmp('appCCity');
			var cityRegExp = new RegExp("^[a-zA-Z]+$");
			if (appCollegeCity != '' && !cityRegExp.test(appCollegeCity)) {
				Ext.Msg.show({
					id: 'cityName',
					name: 'cityName',
					title: null,
					cls: 'msgbox',
					message: '<p>Invalid City.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							cityField.focus(true);
						}
					}
				});
			} else {
				mobEdu.enquire.f.submitCreateApplication();
			}
			//            }
			//            else {
			//                Ext.Msg.show({
			//                    title:null,
			//                    message:'<p>Please enter all mandatory fields.</p>',
			//                    buttons:Ext.MessageBox.OK,
			//                    cls:'msgbox'
			//                });
			//            }
		},
		showCollegesGrid: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppTestScores);
			mobEdu.util.show('mobEdu.enquire.view.createAppCollegeDetails');
		},
		showTestScoreGrid: function() {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.showCreateAppInterests);
			mobEdu.util.show('mobEdu.enquire.view.testScores');
		},

		showTestScoresPopup: function(tsPopupView) {
			//            mobEdu.util.updatePrevView(mobEdu.enquire.f.hidePopup);
			if (!tsPopupView) {
				mobEdu.enquire.f.loadTestCodeEnumerations();
				var popup = tsPopupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enquire.view.testScoresPopup'));
				Ext.Viewport.add(tsPopupView);
			}
			tsPopupView.show();

		},
		loadTestCodeEnumerations: function() {
			var enumType = 'STVTESC';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.testCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.testCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		testCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.testCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		hideTestPopup: function() {
			//            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			var popUp = Ext.getCmp('tsPopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		hideCollegePopup: function() {
			//            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			var popUp = Ext.getCmp('collegePopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		resetTsPopup: function() {
			//            mobEdu.util.get('mobEdu.enquire.view.testScoresPopup').reset();
			Ext.getCmp('appTCode').reset();
			Ext.getCmp('appTDate').reset();
			Ext.getCmp('appTScore').reset();
		},

		testScoresAddToGrid: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.testCodeEnumerations');
			var appTestCode = Ext.getCmp('appTCode').getValue();
			if (appTestCode == "select ") {
				appTestCode = '';
			}
			for (var i = 0; i < enuStore.data.length; i++) {
				var testCode = enuStore.data.all[i].data.enumValue;
				if (appTestCode == testCode) {
					var testCodeDisplayValue = enuStore.data.all[i].data.enumValue;
					appTestCode = testCodeDisplayValue;
				}
			}
			var appTestDate = Ext.getCmp('appTDate').getValue();
			var testDateFormatted;
			if (appTestDate == null) {
				testDateFormatted = '';
			} else {
				testDateFormatted = ([appTestDate.getMonth() + 1] + '/' + appTestDate.getDate() + '/' + appTestDate.getFullYear());
			}
			var appTestScore = Ext.getCmp('appTScore').getValue();
			if (appTestScore == 0) {
				appTestScore = '';
			}
			//            var testValue = appTestScore.getValue();
			//            var testScores ;
			//            if(testValue != null && testValue != 0){
			//                testScores = testValue.toString();
			//            }

			if (appTestCode != '' && appTestCode != null && testDateFormatted != '' && appTestDate != null && appTestScore != '' && appTestScore != null) {
				mobEdu.enquire.f.hideTestPopup();

				var store = mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid');
				store.load();
				var record = store.getAt(0);
				//            store.removeAll();
				store.sync();
				if (record != undefined) {
					record.data.testCode = appTestCode;
					record.data.testDate = testDateFormatted;
					record.data.testScore = appTestScore;
					store.add(record.copy());
					store.sync();
				} else {
					store.add({
						testCode: appTestCode,
						testDate: testDateFormatted,
						testScore: appTestScore
					});
					store.sync();
				}
				//                var gridView = Ext.getCmp('testScoreDetails');
				//                gridView.refresh();
				Ext.getCmp('appTCode').reset();
				Ext.getCmp('appTDate').reset();
				Ext.getCmp('appTScore').reset();
				mobEdu.enquire.f.showTestScoreGrid();
			} else {
				mobEdu.enquire.f.hideTestPopup();
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.showTestScoresPopup();
						}
					}
				});
			}

		},

		onTestScoresItemTap: function(selectedIndex, viewRef, record, item) {
			if (item.target.name == "testScoreDrop") {
				mobEdu.enquire.f.testScoresDrop(record);
			}
		},

		testScoresDrop: function(record) {
			var testScoresInfoStore = mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid');
			testScoresInfoStore.remove(record);
			testScoresInfoStore.sync();
		},

		showCollegesPopup: function(collegePopup) {
			//            mobEdu.util.updatePrevView(mobEdu.enquire.f.collegesHidePopup);
			if (!collegePopup) {
				mobEdu.enquire.f.loadPriorCCodeEnumerations();
				mobEdu.enquire.f.loadPriorDegreeEnumerations();
				var popup = collegePopup = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enquire.view.collegesPopup'));
				Ext.Viewport.add(collegePopup);
			}
			collegePopup.show();
		},
		collegesHidePopup: function() {
			//            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			var popUp = Ext.getCmp('collegePopup');
			popUp.hide();
			Ext.Viewport.unmask();
		},

		loadPriorCCodeEnumerations: function() {
			var enumType = 'STVPCOL';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.priorCCodeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.priorCCodeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		priorCCodeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.priorCCodeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}

		},
		loadPriorDegreeEnumerations: function() {
			var enumType = 'STVDEGC';
			//            var roleId = mobEdu.util.getRoleId();
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.priorDegreeEnumerations');

			enuStore.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			enuStore.getProxy().setUrl(enquireWebserver + 'config/getroleenums');
			enuStore.getProxy().setExtraParams({
				enumType: enumType,
				companyId: companyId
			});
			enuStore.getProxy().afterRequest = function() {
				mobEdu.enquire.f.priorDegreeEnumerationsResponseHandler();
			};
			enuStore.load();
		},
		priorDegreeEnumerationsResponseHandler: function() {
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.priorDegreeEnumerations');
			var enumStatus = enuStore.getProxy().getReader().rawData;
			if (enumStatus.status != 'success') {
				Ext.Msg.show({
					id: 'enum',
					title: null,
					message: enumStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		collegesAddToList: function() {
			var stateStore = mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations');
			var cState = Ext.getCmp('appCState').getValue();
			if (cState == "select ") {
				cState = '';
			}
			for (var s = 0; s < stateStore.data.length; s++) {
				var stateValue = stateStore.data.all[s].data.enumValue;
				if (cState == stateValue) {
					var stateValueDisplayValue = stateStore.data.all[s].data.enumValue;
					cState = stateValueDisplayValue;
				}
			}
			var enuStore = mobEdu.util.getStore('mobEdu.enquire.store.priorCCodeEnumerations');
			var cCode = Ext.getCmp('appCCode').getValue();
			if (cCode == "select ") {
				cCode = '';
			}
			for (var i = 0; i < enuStore.data.length; i++) {
				var CollegeCode = enuStore.data.all[i].data.enumValue;
				if (cCode == CollegeCode) {
					var collegeCodeDisplayValue = enuStore.data.all[i].data.enumValue;
					cCode = collegeCodeDisplayValue;
				}
			}
			var degreeStore = mobEdu.util.getStore('mobEdu.enquire.store.priorDegreeEnumerations');
			var cCity = Ext.getCmp('appCCity').getValue();
			var cDCode = Ext.getCmp('appCDCode').getValue();
			if (cDCode == "select ") {
				cDCode = '';
			}
			for (var d = 0; d < degreeStore.data.length; d++) {
				var degreeCode = degreeStore.data.all[d].data.enumValue;
				if (cDCode == degreeCode) {
					var degreeCodeDisplayValue = degreeStore.data.all[d].data.enumValue;
					cDCode = degreeCodeDisplayValue;
				}
			}
			var cGradDate = Ext.getCmp('appCGDate').getValue();
			var gradDateFormatted = '';
			if (cGradDate == null) {
				gradDateFormatted = '';
			} else {
				gradDateFormatted = ([cGradDate.getMonth() + 1] + '/' + cGradDate.getDate() + '/' + cGradDate.getFullYear());
			}

			if (cState != '' && cState != null && gradDateFormatted != '' && gradDateFormatted != null && cCode != '' && cCode != null &&
				cCity != '' && cCity != null && cDCode != '' && cDCode != null) {
				mobEdu.enquire.f.collegesHidePopup();

				var collegeInfoStore = mobEdu.util.getStore('mobEdu.enquire.store.collegesJsonString');
				collegeInfoStore.load();

				var record = collegeInfoStore.getAt(0);
				if (record != undefined) {
					record.data.collegeCode = cCode;
					record.data.state = cState;
					record.data.city = cCity;
					record.data.degree = cDCode;
					record.data.gradDate = gradDateFormatted;
					collegeInfoStore.add(record.copy());
					collegeInfoStore.sync();
				} else {
					collegeInfoStore.add({
						collegeCode: cCode,
						state: cState,
						city: cCity,
						degree: cDCode,
						gradDate: gradDateFormatted
					});
					collegeInfoStore.sync();
				}
				Ext.getCmp('appCCode').reset();
				Ext.getCmp('appCCity').reset();
				Ext.getCmp('appCDCode').reset();
				Ext.getCmp('appCState').reset();
				Ext.getCmp('appCGDate').reset();
				mobEdu.enquire.f.showCollegesGrid();
			} else {
				mobEdu.enquire.f.collegesHidePopup();
				Ext.Msg.show({
					title: null,
					message: '<p>Please enter all mandatory fields.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox',
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.showCollegesPopup();
						}
					}
				});
			}
		},

		resetCollegesList: function() {
			Ext.getCmp('appCCode').reset();
			Ext.getCmp('appCCity').reset();
			Ext.getCmp('appCDCode').reset();
			Ext.getCmp('appCState').reset();
			Ext.getCmp('appCGDate').reset();
		},

		onCollegesItemTap: function(selectedIndex, viewRef, record, item) {
			if (item.target.name == "collegeDrop") {
				mobEdu.enquire.f.collegeListDrop(record);
			}
		},
		collegeListDrop: function(record) {
			var collegeInfoStore = mobEdu.util.getStore('mobEdu.enquire.store.collegesJsonString');
			collegeInfoStore.remove(record);
			collegeInfoStore.sync();
		},

		submitCreateApplication: function() {
			var appDob = Ext.getCmp('appDateofbirth').getValue();
			var dobFormatted;
			if (appDob == null) {
				dobFormatted = '';
			} else {
				dobFormatted = ([appDob.getMonth() + 1] + '/' + appDob.getDate() + '/' + appDob.getFullYear());
			}
			var pInterest = Ext.getCmp('appPrimary').getValue();
			if (pInterest == "select ") {
				pInterest = "";
			}
			var sInterest = Ext.getCmp('appSecondary').getValue();
			if (sInterest == "select ") {
				sInterest = "";
			}

			var otherInterest = Ext.getCmp('appOtherI').getValue();
			var otherIData;
			if (otherInterest != '' && otherInterest != null && otherInterest == "select ") {
				//                otherIData =  otherInterest.match(/.{1,4}/g);
				otherIData = [''];
			} else {
				otherIData = [otherInterest];
			}

			var ssnValue = Ext.getCmp('appssn').getValue();
			if (ssnValue == null && ssnValue == "0") {
				ssnValue = "";
			}
			//                 else{
			//                     ssnValue = '"'+ssnValue+'"';
			//                 }
			var visaNo = Ext.getCmp('appVNo').getValue();
			if (visaNo == null) {
				visaNo = "";
			}
			//                else{
			//                    visaNo = '"'+visaNo+'"';
			//                }
			var sGpaValue = Ext.getCmp('appSGpa').getValue();
			if (sGpaValue == null && sGpaValue == "0") {
				sGpaValue = "";
			}
			//            else{
			//                sGpaValue = '"'+sGpaValue+'"';
			//            }
			var sGYear = Ext.getCmp('appSGYear').getValue();
			if (sGYear == null && sGYear == "0") {
				sGYear = "";
			}
			//            else{
			//                sGYear = '"'+sGYear+'"';
			//            }
			var detailStore = mobEdu.util.getStore('mobEdu.enquire.store.myProfile');
			var profileData = detailStore.data.all;
			var viewLeadId = profileData[0].data.leadID;
			var localTestScores = mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid');
			var testScoresData = '';
			var reqTestScoreData = localTestScores.getRange();
			for (var t = 0; t < reqTestScoreData.length; t++) {
				if (testScoresData == '') {
					testScoresData = Ext.encode(reqTestScoreData[t].data);
				} else {
					testScoresData = testScoresData + ',' + Ext.encode(reqTestScoreData[t].data);
				}
			}
			testScoresData = '[' + testScoresData + ']';

			var collegeInfoStore = mobEdu.util.getStore('mobEdu.enquire.store.collegesJsonString');
			var collegesData = '';
			var reqCollegesData = collegeInfoStore.getRange();
			for (var c = 0; c < reqCollegesData.length; c++) {
				if (collegesData == '') {
					collegesData = Ext.encode(reqCollegesData[c].data);
				} else {
					collegesData = collegesData + ',' + Ext.encode(reqCollegesData[c].data);
				}
			}
			collegesData = '[' + collegesData + ']';

			//            var ethnicityValue = Ext.getCmp('appECity').getValue();
			//            if(ethnicityValue == "select "){
			//                ethnicityValue = '' ;
			//            }
			//            var countyValue = Ext.getCmp('appCounty').getValue();
			//            if(countyValue == "select "){
			//                countyValue = '' ;
			//            }
			var parentRelation1 = Ext.getCmp('appPRel1').getValue();
			if (parentRelation1 == "select ") {
				parentRelation1 = '';
			}
			var parentRelation2 = Ext.getCmp('appPRel2').getValue();
			if (parentRelation2 == "select ") {
				parentRelation2 = '';
			}
			var visaTypeValue = Ext.getCmp('appVType').getValue();
			if (visaTypeValue == "select ") {
				visaTypeValue = '';
			}
			var visaNationality = Ext.getCmp('appCitizenship').getValue();
			if (visaNationality == "select ") {
				visaNationality = '';
			}
			var schoolCode = Ext.getCmp('appSCode').getValue();
			if (schoolCode == "select ") {
				schoolCode = '';
			}
			var schoolSate = mobEdu.enquire.f.highSchoolStateCode;
			if (schoolSate == "select ") {
				schoolSate = '';
			}
			var admissionTypeValue = Ext.getCmp('appAType').getValue();
			if (admissionTypeValue == "select ") {
				admissionTypeValue = '';
			}
			var derpartmentName = Ext.getCmp('appDep').getValue();
			if (derpartmentName == "select ") {
				derpartmentName = '';
			}
			var campusName = Ext.getCmp('appCampus').getValue();
			if (campusName == "select ") {
				campusName = '';
			}
			var eduGoal = Ext.getCmp('appEGoal').getValue();
			if (eduGoal == "select ") {
				eduGoal = '';
			}
			var raceValue = Ext.getCmp('appRace').getValue();
			if (raceValue == "select ") {
				raceValue = '';
			}

			var applicationStore = mobEdu.util.getStore('mobEdu.enquire.store.applicationJsonString');
			var record = applicationStore.getAt(0);
			applicationStore.removeAll();
			applicationStore.sync();
			applicationStore.add({
				leadID: viewLeadId,
				termCode: Ext.getCmp('appStartTerm').getValue(),
				levelCode: Ext.getCmp('appLCode').getValue(),
				majorCode: Ext.getCmp('appMCode').getValue(),
				studentType: Ext.getCmp('appSType').getValue(),
				admissionType: admissionTypeValue,
				residenceCode: Ext.getCmp('appRCode').getValue(),
				collegeCode: Ext.getCmp('appAdCCode').getValue(),
				degreeCode: Ext.getCmp('appDCode').getValue(),
				department: derpartmentName,
				campus: campusName,
				educationGoal: eduGoal,
				firstName: Ext.getCmp('appFName').getValue(),
				lastName: Ext.getCmp('appLName').getValue(),
				//                middleName:Ext.getCmp('appMName').getValue(),
				gender: Ext.getCmp('appGender').getValue(),
				race: raceValue,
				//                ethnicity:ethnicityValue,
				dob: dobFormatted,
				ssn: ssnValue,
				address1: Ext.getCmp('appAddress1').getValue(),
				address2: Ext.getCmp('appAddress2').getValue(),
				//                address3:Ext.getCmp('appAddress3').getValue(),
				city: Ext.getCmp('appCity').getValue(),
				state: mobEdu.enquire.f.cAppStateCode,
				//                county:countyValue,
				zip: Ext.getCmp('appZip').getValue(),
				country: Ext.getCmp('appCountry').getValue(),
				phone1: Ext.getCmp('appPhone1').getValue().toString(),
				phone2: Ext.getCmp('appPhone2').getValue().toString(),
				email: Ext.getCmp('appEmail').getValue(),
				visaType: visaTypeValue,
				nationality: visaNationality,
				visaNumber: visaNo,
				parent1Relation: parentRelation1,
				parent1FirstName: Ext.getCmp('appPFName1').getValue(),
				parent1LastName: Ext.getCmp('appPLName1').getValue(),
				parent1MiddleName: Ext.getCmp('appPMName1').getValue(),
				parent2Relation: parentRelation2,
				parent2FirstName: Ext.getCmp('appPFName2').getValue(),
				parent2LastName: Ext.getCmp('appPLName2').getValue(),
				parent2MiddleName: Ext.getCmp('appPMName2').getValue(),
				schoolName: Ext.getCmp('appSName').getValue(),
				schoolGpa: sGpaValue,
				schoolCode: schoolCode,
				schoolCity: Ext.getCmp('appSCity').getValue(),
				schoolState: schoolSate,
				schoolZip: Ext.getCmp('appSZipCode').getValue(),
				schoolGradYear: sGYear,
				primaryInterest: pInterest,
				secondaryInterest: sInterest,
				levelOfInterest: Ext.getCmp('appLevel').getValue(),
				factorForChoosing: Ext.getCmp('appFactor').getValue(),
				testScores: Ext.decode(testScoresData, 'mobEdu.enquire.model.testScoreJsonString'),
				colleges: Ext.decode(collegesData, 'mobEdu.enquire.model.collegesJsonString'),
				otherInterests: otherIData
			});
			applicationStore.sync();

			var reqData = applicationStore.getRange();
			var data = Ext.encode(reqData[0].data);

			var store = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			store.getProxy().setHeaders({
				Authorization: mobEdu.util.getLeadAuthString(),
				companyID: companyId
			});
			store.getProxy().setUrl(enquireWebserver + 'enquire/lead/createapplication');
			store.getProxy().setExtraParams({
				application: data,
				companyId: companyId
			});

			store.getProxy().afterRequest = function() {
				mobEdu.enquire.f.submitCreateApplicationResponseHandler();
			};
			store.load();
		},

		submitCreateApplicationResponseHandler: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.appointmentDetail');
			var createAppStatus = store.getProxy().getReader().rawData;
			if (createAppStatus.status == 'success') {
				Ext.Msg.show({
					id: 'appsuccess',
					title: null,
					cls: 'msgbox',
					message: '<p>Application created successfully.</p>',
					buttons: Ext.MessageBox.OK,
					fn: function(btn) {
						if (btn == 'ok') {
							mobEdu.enquire.f.resetCreateAppBasicInfo();
							mobEdu.enquire.f.resetCreateAdmissionDetails();
							mobEdu.enquire.f.resetCreateAppContactInfo();
							mobEdu.enquire.f.resetCreateAppInterests();
							mobEdu.enquire.f.resetCreateAppVisaDetail();
							mobEdu.enquire.f.resetCreateAppParentDetails();
							mobEdu.enquire.f.resetCreateAppSchoolDetails();
							var tsStore = mobEdu.util.getStore('mobEdu.enquire.store.testScoresGrid');
							tsStore.removeAll();
							tsStore.sync();
							var cStore = mobEdu.util.getStore('mobEdu.enquire.store.collegesJsonString');
							cStore.removeAll();
							cStore.sync();
							mobEdu.enquire.f.showLeadMainView();
						}
					}

				});
			} else {
				Ext.Msg.show({
					title: null,
					message: createAppStatus.status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
		},

		showAboutSubr: function() {
			var leadid = mobEdu.util.getLeadId();
			mobEdu.enquire.f.hidePopup();
			if (leadid != null) {
				mobEdu.util.updatePrevView(mobEdu.enquire.f.showSubMenu);
				mobEdu.util.show('mobEdu.enquire.view.subrMenu');
				// Ext.getCmp('customHome').show();
			} else {
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
				mobEdu.util.show('mobEdu.enquire.view.subrMenu');
				// Ext.getCmp('customHome').hide();
			}
			if (Ext.os.is.Phone) {
				Ext.getCmp('aboutSubr').setTitle('<h1>Southern University</h1>');
			}
		},
		initializeN2nMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'N2N Mobile',
				action: mobEdu.enquire.f.loadN2nMobile
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'N2N Portal',
				action: mobEdu.enquire.f.loadN2nPortal
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'N2N LMP',
				action: mobEdu.enquire.f.loadN2nLmp
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Events',
				action: mobEdu.enquire.f.loadN2nEvents
			});
			store.sync();

		},

		initializeUNGMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Degree Offerings',
				action: mobEdu.enquire.f.loadDegreeOffering
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Costs of Attendance',
				action: mobEdu.enquire.f.loadCOA
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Financial Aid & Scholarships',
				action: mobEdu.enquire.f.loadFAS
			});
			store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Events',
			// 	action: mobEdu.enquire.f.loadUNGEvents
			// });
			// store.sync();

		},
		initializeFVSUMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadFVSUAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About FVSU',
				action: mobEdu.enquire.f.loadAboutFVSU
			});
			store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Financial Aid & Scholarships',
			// 	action: mobEdu.enquire.f.loadFAS
			// });
			// store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Events',
			// 	action: mobEdu.enquire.f.loadUNGEvents
			// });
			// store.sync();

		},
		initializeWSSUMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadWSSUAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About WSSU',
				action: mobEdu.enquire.f.loadAboutWSSU
			});
			store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Financial Aid & Scholarships',
			// 	action: mobEdu.enquire.f.loadFAS
			// });
			// store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Events',
			// 	action: mobEdu.enquire.f.loadUNGEvents
			// });
			// store.sync();

		},
		initializeSCFMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadSCFAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About SCF',
				action: mobEdu.enquire.f.loadAboutSCF
			});
			store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Financial Aid & Scholarships',
			// 	action: mobEdu.enquire.f.loadFAS
			// });
			// store.sync();

			// store.add({
			// 	img: mobEdu.util.getResourcePath() + 'images/arrow.png',
			// 	title: 'Events',
			// 	action: mobEdu.enquire.f.loadUNGEvents
			// });
			// store.sync();

		},
		initializeOCMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadOCAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About OC',
				action: mobEdu.enquire.f.loadAboutOC
			});
			store.sync();
		},
		initializeRSVMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadRSVAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About OC',
				action: mobEdu.enquire.f.loadAboutRSV
			});
			store.sync();
		},
		initializeRSVMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadBBEAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About Bainbridge',
				action: mobEdu.enquire.f.loadAboutBBE
			});
			store.sync();
		},
		initializeUNCFSUMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadUNCFSUAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About FSU',
				action: mobEdu.enquire.f.loadAboutUNCFSU
			});
			store.sync();
		},
		initializeLCCMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadLCCAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About FSU',
				action: mobEdu.enquire.f.loadAboutLCC
			});
			store.sync();
		},
		initializeCSCCMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadCSCCAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About CSCC',
				action: mobEdu.enquire.f.loadAboutCSCC
			});
			store.sync();
		},
		initializeBBEMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Admissions',
				action: mobEdu.enquire.f.loadBBEAdmissions
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'About Bainbridge',
				action: mobEdu.enquire.f.loadAboutBBE
			});
			store.sync();
		},
		initializeSubrMenu: function() {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.subrMenu');
			store.removeAll();
			store.removed = [];

			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Adult Students Age 25 and over',
				action: mobEdu.enquire.f.loadAdultStudents
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Immunization Form',
				action: mobEdu.enquire.f.loadImmunizationForm
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Scholarship Application',
				action: mobEdu.enquire.f.loadScholarshipApp
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Honor Students',
				action: mobEdu.enquire.f.loadHonorStudents
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'Residency Requirements',
				action: mobEdu.enquire.f.loadResidencyRequirements
			});
			store.sync();

			store.add({
				img: mobEdu.util.getResourcePath() + 'images/arrow.png',
				title: 'SU Recruitment Material for Alumni',
				action: mobEdu.enquire.f.loadSURequirement
			});
			store.sync();

		},
		loadN2nMobile: function() {
			mobEdu.util.loadExternalUrl('http://n2nservices.com/N2NMobile/index.html')
		},
		loadN2nPortal: function() {
			mobEdu.util.loadExternalUrl('http://n2nservices.com/N2NMobile/index.html')

		},
		loadN2nLmp: function() {
			mobEdu.util.loadExternalUrl('http://n2nservices.com/LMP/index.html')
		},
		loadWSSUAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://wssu.edu/admissions/default.aspx')
		},
		loadSCFAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://scf.edu/StudentServices/Admissions/default.asp')
		},
		loadOCAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.odessa.edu/dept/admissions/')
		},
		loadRSVAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.roosevelt.edu/Admission.aspx')
		},
		loadRSVAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.roosevelt.edu/Admission.aspx')
		},
		loadBBEAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.bainbridge.edu/admissions/')
		},
		loadUNCFSUAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.uncfsu.edu/admissions')
		},
		loadLCCAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.lanecc.edu/counseling/steps-enroll')

		},
		loadCSCCAdmissions: function() {
			mobEdu.util.loadExternalUrl('http://www.chattanoogastate.edu/admissions')
		},
		loadAboutLCC: function() {
			mobEdu.util.loadExternalUrl('http://www.lanecc.edu/about')

		},
		loadAboutCSCC: function() {
			mobEdu.util.loadExternalUrl('http://www.chattanoogastate.edu/our-campus/about-us')
		},
		loadAboutFVSU: function() {
			mobEdu.util.loadExternalUrl('http://www.fvsu.edu/about_fvsu')
		},
		loadAboutWSSU: function() {
			mobEdu.util.loadExternalUrl('http://wssu.edu/about/default.aspx')
		},
		loadAboutSCF: function() {
			mobEdu.util.loadExternalUrl('http://scf.edu/AboutSCF')
		},
		loadAboutOC: function() {
			mobEdu.util.loadExternalUrl('http://www.odessa.edu/about/')
		},
		loadAboutRSV: function() {
			mobEdu.util.loadExternalUrl('http://www.roosevelt.edu/About')
		},
		loadAboutBBE: function() {
			mobEdu.util.loadExternalUrl('http://www.bainbridge.edu/about/')
		},
		loadAboutUNCFSU: function() {
			mobEdu.util.loadExternalUrl('http://www.uncfsu.edu/pr/about-fsu')
		},
		loadDegreeOffering: function() {
			mobEdu.util.loadExternalUrl('http://ung.edu/academics/degree-programs.php')
		},
		loadUNG: function() {
			mobEdu.util.loadExternalUrl('http://ung.edu/index.php')
		},
		loadCOA: function() {
			mobEdu.util.loadExternalUrl('http://ung.edu/financial-aid/costs.php')

		},
		loadFAS: function() {
			mobEdu.util.loadExternalUrl('http://ung.edu/financial-aid/explore-opportunities/georgia-financial-aid-programs.php')
		},
		loadUNGEvents: function() {
			mobEdu.util.loadExternalUrl('http://ung.edu/bbt-center-ethical-business-leadership/news-and-events.php')
		},
		loadN2nEvents: function() {
			mobEdu.util.loadExternalUrl('http://n2nservices.com/category/events/')
		},
		loadQuickFacts: function() {
			if (campusCode == 'N2N') {
				mobEdu.util.loadExternalUrl('http://www.n2nservices.com/features/mobile/registration.html')
			} else {
				mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/1204/n/916')
			}
		},

		loadAdmissionRequirements: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/325')
		},

		loadAdultStudents: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/326')
		},

		loadApplyingAdmissions: function() {
			if (Ext.os.is.Android) {
				var url = mobEdu.util.convertPdfUrl('http://www.subr.edu/assets/enrollmentservices/application.pdf');
				mobEdu.util.loadExternalUrl(url)
			} else {
				mobEdu.util.loadExternalUrl('http://www.subr.edu/assets/enrollmentservices/application.pdf')
			}
		},

		loadBeginningFreshman: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/329')
		},

		loadHonorStudents: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/326')
		},

		loadResidencyRequirements: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/1478')
		},

		loadSURequirement: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/41')
		},

		loadImmunizationForm: function() {
			if (Ext.os.is.Android) {
				var url = mobEdu.util.convertPdfUrl('http://www.subr.edu/assets/docs/Admissions/immunization09.pdf');
				mobEdu.util.loadExternalUrl(url)
			} else {
				mobEdu.util.loadExternalUrl('http://www.subr.edu/assets/docs/Admissions/immunization09.pdf')
			}

		},

		loadScholarshipApp: function() {
			mobEdu.util.loadExternalUrl('http://www.subr.edu/index.cfm/page/307')
		},
		loadStatePopup: function(popupView) {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('stList').blur();
			if (mobEdu.enquire.view.state.clearIconTapped) {
				mobEdu.enquire.view.state.clearIconTapped = false;
				return;
			}
			var popup = mobEdu.util.get('mobEdu.enquire.view.state');
			Ext.Viewport.add(popup);
		},

		hideStatePopup: function(record) {
			var popUp = Ext.getCmp('statePopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
				mobEdu.util.destroyView('mobEdu.enquire.view.state');
			}
			Ext.getCmp('stList').blur();
			mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations').clearFilter();
		},

		loadAppStatePopup: function(popupView) {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('stAppList').blur();
			if (mobEdu.enquire.view.appState.clearIconTapped) {
				mobEdu.enquire.view.appState.clearIconTapped = false;
				return;
			}
			mobEdu.util.updatePrevView(mobEdu.enquire.f.hideStatePopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enquire.view.appState')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},
		loadSchStatePopup: function(popupView) {
			mobEdu.util.hideKeyboard();
			Ext.getCmp('schStList').blur();
			if (mobEdu.enquire.view.schoolState.clearIconTapped) {
				mobEdu.enquire.view.schoolState.clearIconTapped = false;
				return;
			}
			mobEdu.util.updatePrevView(mobEdu.enquire.f.hideStatePopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enquire.view.schoolState')
				);
				Ext.Viewport.add(popupView);
			}
			popupView.show();
		},
		onStateKeyup: function(textfield, e) {
			//Restricting TAB key
			if (e.browserEvent.keyCode != 9) {
				mobEdu.enquire.f.doCheckBeforeSubjectSearch(textfield);
			}
		},

		doCheckBeforeSubjectSearch: function(textfield) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			var store = mobEdu.util.getStore('mobEdu.enquire.store.stateEnumerations');
			if (length > 1) {
				setTimeout(function() {
					var searchString = '';
					searchString = textfield.getValue();
					if (searchString == searchValue) {
						store.clearFilter();
						searchString = searchString.toLowerCase();
						store.filter(function(record) {
							var subStr = (record.get('displayValue')).toLowerCase();
							if (subStr.indexOf(searchString) !== -1) {
								return true;
							}
							return false;
						});
					}
				}, 1000);
			} else {
				store.clearFilter();
			}
		},
		onStateItemClear: function(textfield, e) {
			var searchValue = textfield.getValue();
			var length = searchValue.length;
			if (length == 0 || length == '') {
				mobEdu.enquire.f.doCheckBeforeSubjectSearch(textfield);
			}
		},
		onStateItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
			mobEdu.enquire.f.rinfstateCode = record.get('enumValue');
			Ext.getCmp('stList').setValue(record.get('displayValue'));
			mobEdu.enquire.f.hideStatePopup(record);
		},
		onAppStateItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
			mobEdu.enquire.f.cAppStateCode = record.get('enumValue');
			Ext.getCmp('stAppList').setValue(record.get('displayValue'));
			mobEdu.enquire.f.hideAppStatePopup();
		},
		onSchStateItemTap: function(viewRef, selectedItemIndex, target, record, e, eOpts) {
			mobEdu.util.deselectSelectedItem(selectedItemIndex, viewRef);
			mobEdu.enquire.f.highSchoolStateCode = record.get('enumValue');
			Ext.getCmp('schStList').setValue(record.get('displayValue'));
			mobEdu.enquire.f.hideSchStatePopup();
		},

		hideAppStatePopup: function() {
			//mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			var popUp = Ext.getCmp('stateAppPopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
			}
			Ext.Viewport.unmask();
			//Ext.getCmp('stList').blur();
		},
		hideSchStatePopup: function() {
			//mobEdu.util.updatePrevView(mobEdu.reg.f.showSearch);
			var popUp = Ext.getCmp('schstatePopup');
			if (typeof popUp != "undefined") {
				popUp.hide();
			}
			Ext.Viewport.unmask();
			//Ext.getCmp('stList').blur();
		},

		showLeadLogin: function(popupView) {
			mobEdu.util.updatePrevView(mobEdu.enquire.f.hidePopup);
			if (!popupView) {
				var popup = popupView = Ext.Viewport.add(
					mobEdu.util.get('mobEdu.enquire.view.login'));
				Ext.Viewport.add(popupView);
			}
			mobEdu.util.onOrientationChange(Ext.Viewport.getOrientation(), popupView, mobEdu.enquire.view.login);
			popupView.show();
		},
		hidePopup: function() {
			var popUp = Ext.getCmp('enquireLoginPopup');
			if (popUp != null) {
				popUp.hide();
			}
			Ext.Viewport.unmask();
		},
		submitLoginForm: function(uname, pwd) {
			var form_values = this.getFormValues();
			var username = form_values['enquireName'];
			var password = form_values['enquirePassword'];

			if (uname != null && uname != '' && pwd != null && pwd != '') {
				username = uname;
				password = pwd;
			} else {
				username = username.toUpperCase();
			}

			if (username == '' || password == '') {
				Ext.Msg.show({
					title: 'Login Error',
					message: '<p>Please provide Username and Password.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				var serviceText = 'login';
				var operationText = 'leadlogin';

				var cred = username + ':' + password;
				var encCred = Base64.encode(cred);
				var leadAuthString = 'Basic ' + encCred;
				// mobEdu.util.updateLeadAuthString(leadAuthString);
				var store = mobEdu.util.getStore('mobEdu.enquire.store.login');

				store.getProxy().setHeaders({
					Authorization: leadAuthString,
					companyID: companyId
				});
				store.getProxy().setUrl(loginserver + 'login');
				store.getProxy().setExtraParams({
					companyId: companyId
				});
				store.getProxy().afterRequest = function() {
					mobEdu.enquire.f.loginResponseHandler(leadAuthString);
				}
				store.load();
			}
		},

		resetLoginForm: function() {
			mobEdu.util.get('mobEdu.enquire.view.login').reset();
		},

		loginResponseHandler: function(leadAuthString) {
			var store = mobEdu.util.getStore('mobEdu.enquire.store.login');
			var response = store.getProxy().getReader().rawData;
			if (response.status == "success" && response.privileges.length === 0) {
				Ext.Msg.show({
					message: 'No privileges defined for your role.',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
				return;
			} else if (response.status == "success") {
				var hasModuleAccess = false;
				var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');
				// By default we will show all modules
				// Based on the ROLE privileges, we remove modules that are not returned
				// in Login Response ROLE Privileges

				// Do not remove directly in this loop.
				// Collect all items to be removed and remove at once.
				var mLength = modulesLocalStore.data.all.length;
				var removeItems = new Array();
				for (var mi = mLength - 1; mi >= 0; mi--) {

					var localCode = modulesLocalStore.data.all[mi].data.code;
					var localCodeFound = false;

					for (var i = 0; i < response.privileges.length; i++) {
						var type = response.privileges[i].type;
						if (type == 'MODULE_ACCESS') {

							if (response.privileges[i].name === localCode) {
								localCodeFound = true;
								if (mobEdu.util.getLoginModuleCode() == response.privileges[i].name) {
									hasModuleAccess = true;
								}
								break;
							}
						}
					}
					if (!localCodeFound) {
						removeItems.push(localCode);
					}
				}

				for (var mi = removeItems.length - 1; mi >= 0; mi--) {
					var index = modulesLocalStore.findExact('code', removeItems[mi]);
					var record = modulesLocalStore.getAt(index);
					modulesLocalStore.remove(record);
				};
				modulesLocalStore.sync();

				// Update the remaining modules auth required flag
				modulesLocalStore.each(function(item, index, length) {
					item.set('authRequired', 'N');
				});
				if (mainMenuLayout == 'GRID') {
					Ext.getCmp('logBtn').setHidden(false);
				}

				modulesLocalStore.sync();

				// Check proxy privileges now
				for (var i = 0; i < response.privileges.length; i++) {
					if (response.privileges[i].type === 'PROXY_REG') {
						mobEdu.util.updateProxyReg(response.privileges[i].value);
						mobEdu.util.updateProxyAccessFlag(response.privileges[i].accessFlag);
						break;
					}
				}
				if (modulesLocalStore.data.all.length == 0) {
					Ext.Msg.show({
						message: 'You do not have access to any of the modules.',
						buttons: Ext.MessageBox.OK,
						cls: 'msgbox'
					});
					return;
				}
				var leadId = response.userInfo.applicationId;
				var role = response.userInfo.roleCode;
				var roleId = response.userInfo.roleID;
				var companyId = response.userInfo.companyID;
				var privileges = response.privileges;
				var form_values = this.getFormValues();
				var username = form_values['enquireName'];
				var password = form_values['enquirePassword'];
				var mobile = 'MOBILE';
				var cred = username + ':' + password;
				var encCred = Base64.encode(cred);
				var modulesLocalStore = mobEdu.util.getStore('mobEdu.main.store.modulesLocal');

				if (modulesLocalStore.data.length > 0) {
					var localRecord = modulesLocalStore.findRecord('code', 'ENQ', null, null, null, true);
					if (localRecord != null) {
						localRecord.set('authRequired', false);
						modulesLocalStore.sync();
					}
				}
				mobEdu.enquire.f.hidePopup();
				mobEdu.main.f.loadModulesList();
				mobEdu.util.updateStudentId(leadId);
				mobEdu.util.updateLeadId(leadId);
				mobEdu.util.updateLeadAuthString(leadAuthString);
				mobEdu.util.updateAuthString(leadAuthString);
				mobEdu.util.updatePrivileges(privileges);
				mobEdu.util.updateRoleId(roleId);
				mobEdu.util.updatePrevView(mobEdu.util.showMainView);
				mobEdu.util.updateUsername(store.data.first().raw.userInfo.userName);
				mobEdu.enquire.f.showNextView();
				Ext.getCmp('logBtn').setCls(mobEdu.util.getLeadId() == null && mobEdu.util.getAuthString() == null ? 'login' : 'logout');
				// mobEdu.main.f.displayModules();
				//                Ext.getCmp('logBtn').setHidden(false);
				//Dumping cart
				//                cartFlag='login';
				//                mobEdu.reg.f.loadViewCart();

				if (mainMenuLayout === 'AP') {
					mobEdu.main.f.updateSlidingMenu(mobEdu.util.getStore('mobEdu.main.store.modulesLocal'));
				} else if (mainMenuLayout === 'GRID') {
					mobEdu.main.f.displayGridMenu();
				} else if (mainMenuLayout === 'SGRID') {
					mobEdu.main.f.displaySMenu();
				} else if (mainMenuLayout === 'BGRID') {
					mobEdu.main.f.displayBevelGridMenu();
				}

				if (mobEdu.main.f.isNotificationEnabled()) {
					//Load notifications for FirstTime
					mobEdu.notif.f.loadNotifications(true);
					mobEdu.notif.f.scheduleNotificationsJob();
				}

			} else if (status.match(/expire/gi)) {
				Ext.Msg.show({
					title: 'Login error',
					message: status,
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			} else {
				leadAuthString = '';
				Ext.Msg.show({
					title: 'Login Error',
					message: '<p>Invalid Username or Password.</p>',
					buttons: Ext.MessageBox.OK,
					cls: 'msgbox'
				});
			}
			mobEdu.enquire.f.getLeadRecruiterID();
		},
		getFormValues: function() {
			return (mobEdu.util.get('mobEdu.enquire.view.login').getValues());
		},
		showNextView: function() {
			if (mobEdu.util.getNextView() == null) {
				mobEdu.util.showEnquireMainView();
			} else {
				Ext.getCmp("logoutBtn").show();
				// if (mobEdu.util.getNextView() == mobEdu.enquire.notif.f.enquireLoadNotifPopup) {
				//     mobEdu.util.showLeadMainView();
				// }
				(mobEdu.util.getNextView())();
				//                setTimeout(function () {            
				//Load notifications for FirstTime
				// mobEdu.enquire.notif.f.enquireLoadNotifications();
				//Load notifications for every minute
				// var interval = setInterval(function() {
				//     mobEdu.enquire.notif.f.enquireLoadNotifications();
				// }, 60000);

				mobEdu.util.updateNextView(null);
			}
		},

		onUsernameKeyup: function(view, e, eOpts) {
			if (e.browserEvent.keyCode == 13) {
				Ext.getCmp('enquirePassword').focus();
			}
		},

		onPasswordKeyup: function(view, e, eOpts) {
			if (e.browserEvent.keyCode == 13) {
				mobEdu.enquire.f.submitLoginForm();
			}
		},

		showSignUp: function() {
			mobEdu.enquire.f.hidePopup();
			mobEdu.util.updatePrevView(mobEdu.util.showMainView);
			mobEdu.enquire.f.showRequestInformationFrom();
		},

		onOrientationChange: function(orientation) {
			var popupView = Ext.getCmp('enquireLoginPopup');
			if (orientation == 'portrait') {
				if (Ext.os.is.Phone) {
					popupView.setWidth('75%');
					popupView.setHeight('50%');
				} else if (Ext.os.is.Tablet) {
					popupView.setWidth('45%');
					popupView.setHeight('25%');
				} else {
					popupView.setWidth('30%');
					popupView.setHeight('30%');
				}

			} else {
				if (Ext.os.is.Phone) {
					popupView.setWidth('75%');
					popupView.setHeight('75%');
				} else if (Ext.os.is.Tablet) {
					popupView.setWidth('35%');
					popupView.setHeight('33%');
				} else {
					popupView.setWidth('30%');
					popupView.setHeight('30%');
				}
			}
		},

		generateMessage: function(value, username, password) {
			var serviceText = 'login';
			var operationText = 'leadlogin';

			var cred = username + ':' + password;
			var encCred = Base64.encode(cred);
			var leadAuthString = 'Basic ' + encCred;
			var eStore = mobEdu.util.getStore('mobEdu.enquire.store.emailsList');
			eStore.getProxy().setHeaders({
				Authorization: leadAuthString,
				companyID: companyId
			});
			eStore.getProxy().setUrl(commonwebserver + 'message/leadSystemGenerate');
			eStore.getProxy().setExtraParams({
				companyId: companyId,
				informatoion: value
			});
			eStore.getProxy().afterRequest = function() {
				//mobEdu.enquire.f.loadEmailResponseHandler();
			};
			eStore.load();
		}
	}
});