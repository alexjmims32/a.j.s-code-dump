Ext.define('mobEdu.enquire.model.myProfile', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name: 'leadID',
                type:'string'
            },
            {
                name:'firstName',
                type:'string'
            },
            {
                name:'lastName',
                type: 'string'
            },
            {
                name:'email',
                type: 'string'
            },
            {
                name:'gender',
                type:'string'
            },
            {
                name:'birthDate',
                type:'string'
            },
            {
                name:'phone1',
                type:'string'
            },
            {
                name:'recruiterID',
                type:'string'
            },
            {
              name:'recruiterName',
              type:'string'
            },
            {
                name : 'leadStatus',
                type : 'string'
            },
            {
                name:'addressID',
                type:'string'
            },
            {
                name:'addressType',
                type:'string'
            },
            {
                name:'address1',
                type:'string'
            },
            {
                name:'address2',
                type:'string'
            },
            {
                name:'address3',
                type:'string'
            },{
                name:'city',
                type:'string'
            },
            {
                name:'state',
                type:'string'
            } ,{
                name:'country',
                type:'string'
            },{
                name:'versionDate',
                type:'string'
            },{
                name:'versionNo',
                type:'string'
            },{
                name:'versionUser',
                type:'string'
            },{
                name:'applicationID',
                type:'string'
            },{
                name:'classification',
                type:'string'
            },{
                name:'highSchool',
                type:'string'
            },{
                name:'intendedMajor',
                type:'string'
            },{
                name:'actScore',
                type:'string'
            },{
                name:'requestInfo',
                type:'string'
            },{
                name:'lookInfo',
                type:'string'
            },{
                name:'zip',
                type:'string'
            }

        ]
    }
});

