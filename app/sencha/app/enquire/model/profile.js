Ext.define("mobEdu.enquire.model.profile", {
    extend:'Ext.data.Model',

  config:{
    fields : [ 
        {
        name : 'firstName',
        type : 'string'
        },
        {
        name : 'middleName',
        type : 'string'
        },
        {
        name : 'lastName',
        type : 'string'
        },
        {
        name : 'gender',
        type : 'string'
        },
        {
        name : 'race',
        type : 'string'
        },
        {
        name : 'dateOfBirth',
        type : 'string'
        },
        {
        name : 'cAddress',
        type : 'string'
        },
        {
            name : 'cAddress2',
            type : 'string'
        },
        {
            name : 'cAddress3',
            type : 'string'
        },
        {
        name : 'cCity',
        type : 'string'
        },
        {
        name : 'county',
        type : 'string'
        },
        {
        name : 'cState',
        type : 'string'
        },
        {
        name : 'cStateDesc',
        type : 'string'
        },
        {
        name : 'cZip',
        type : 'string'
        },
        {
        name : 'cCountry',
        type : 'string'
        },
        {
        name : 'phone1',
        type : 'string'
        },
        {
        name : 'phone2',
        type : 'string'
        },
        {
        name : 'email',
        type : 'string'
        },
        {
        name : 'vemail',
        type : 'string'
        },
        {
          name:'userName',
          type:'string'
        },
        {
        name : 'hName',
        type : 'string'
        },
        {
        name : 'hCity',
        type : 'string'
        },
        {
        name : 'hState',
        type : 'string'
        },
        {
        name : 'hPostal',
        type : 'string'
        },
        {
        name : 'graduationYear',
        type : 'string'
        },
        {
        name : 'gpa',
        type : 'string'
        },
        {
        name : 'satScore',
        type : 'string'
        },
        {
            name : 'satMScore',
            type : 'string'
        },
        {
        name : 'actScore',
        type : 'string'
        },
        {
        name : 'startTerm',
        type : 'string'
        },
        {
        name : 'startYear',
        type : 'string'
        },
        {
        name : 'pInterest',
        type : 'string'
        },
        {
        name : 'sInterest',
        type : 'string'
        },
        {
        name : 'level',
        type : 'string'
        },
        {
        name : 'impfactor',
        type : 'string'
        },
        {
            name : 'prefEmail',
            type : 'string'
        },
        {
            name : 'prefText',
            type : 'string'
        },
        {
            name : 'prefPhone',
            type : 'string'
        },
        {
            name:'password',
            type:'string'
        },
        {
            name:'vPassword',
            type:'string'
        },{
            name:'classification',
            type:'string'
        },{
            name:'intendedMajor',
            type:'string'
        },{
            name:'highSchool',
            type:'string'
        },{
            name:'whatTypeInf',
            type:'string'
        },{
            name:'actScore',
            type:'string'
        },{
            name:'reqInfo',
            type:'string'
        }

    ]
  }
});