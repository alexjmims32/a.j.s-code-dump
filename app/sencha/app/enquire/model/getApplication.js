Ext.define('mobEdu.enquire.model.getApplication', {
    extend:'Ext.data.Model',

    config:{
        fields:[
            {
                name:'applicationID',
                type:'string'
            },
            {
                name:'status',
                type:'string'
            },
            {
                name : 'leadID',
                type : 'string'
            },
            {
              name:'versionUser',
                type:'string'
            },
            {
                name:'versionDate',
                type:'string'
            },
            {
                name : 'versionNo',
                type : 'string'
            },
            {
                name : 'termCode',
                type : 'string'
            },{
                name:'termCodeDescr',
                type:'string'
            },
            {
                name : 'levelCode',
                type : 'string'
            },
            {
              name:'levelCodeDescr',
                type:'string'
            },
            {
                name : 'majorCode',
                type : 'string'
            },{
                name:'majorCodeDescr',
                type:'string'
            },
            {
                name : 'studentType',
                type : 'string'
            },{
              name:'studentTypeDescr',
                tyep:'string'
            },
            {
                name : 'admissionType',
                type : 'string'
            },
            {
                name:'admissionTypeDescr',
                type:'string'
            },
            {
                name : 'residenceCode',
                type : 'string'
            },{
                name:'residenceCodeDescr',
                type:'string'
            },
            {
                name : 'collegeCode',
                type : 'string'
            },
            {
              name:'collegeCodeDescr',
                type:'string'
            },
            {
                name : 'degreeCode',
                type : 'string'
            },
            {
              name:'degreeCodeDescr',
                type:'string'
            },
            {
                name : 'department',
                type : 'string'
            },
            {
              name:'departmentDescr',
                type:'string'
            },
//            {
//                name : 'isFullTime',
//                type : 'string'
//            },
            {
                name : 'campus',
                type : 'string'
            },
            {
              name:'campusDescr',
                type:'string'
            },
            {
                name : 'educationGoal',
                type : 'string'
            },
            {
              name:'educationGoalDescr',
                type:'string'
            },
            {
                name : 'firstName',
                type : 'string'
            },
            {
                name : 'lastName',
                type : 'string'
            },
            {
                name : 'middleName',
                type : 'string'
            },
            {
                name : 'gender',
                type : 'string'
            },
            {
                name : 'race',
                type : 'string'
            },{
                name:'raceDescr',
                type:'string'
            },
            {
                name : 'ethnicity',
                type : 'string'
            },
            {
              name:'ethnicityDescr',
                type:'string'
            },
            {
                name:'dob',
                type:'string'
            },
            {
                name:'ssn',
                type:'string'
            },
            {
                name:'address1',
                type:'string'
            },
            {
                name:'address2',
                type:'string'
            },
            {
                name:'address3',
                type:'string'
            },
            {
                name:'city',
                type:'string'
            },
            {
                name:'state',
                type:'string'
            },{
                name:'stateDescr',
                type:'string'
            },
            {
                name:'county',
                type:'string'
            },
            {
                name:'zip',
                type:'string'
            },
            {
                name:'country',
                type:'string'
            },
            {
                name:'phone1',
                type:'string'
            },
            {
                name : 'phone2',
                type : 'string'
            },
            {
                name : 'email',
                type : 'string'
            },
            {
                name : 'visaType',
                type : 'string'
            },
            {
                name : 'nationality',
                type : 'string'
            },{
                name:'nationalityDescr',
                type:'string'
            },
            {
                name : 'visaNumber',
                type : 'string'
            },
            {
                name : 'parent1Relation',
                type : 'string'
            },{
                name:'parent1RelationDescr',
                type:'string'
            },
            {
                name : 'parent1FirstName',
                type : 'string'
            },
            {
                name : 'parent1LastName',
                type : 'string'
            },
            {
                name : 'parent1MiddleName',
                type : 'string'
            },
            {
                name : 'parent2Relation',
                type : 'string'
            },{
                name:'parent2RelationDescr',
                type:'string'
            },
            {
                name:'parent2FirstName',
                type:'string'
            },
            {
                name:'parent2LastName',
                type:'string'
            },
            {
                name:'parent2MiddleName',
                type:'string'
            },
            {
                name:'schoolName',
                type:'string'
            },{
                name:'schoolCode',
                type:'string'
            },{
                name:'schoolCodeDescr',
                type:'string'
            },
            {
                name:'schoolGpa',
                type:'string'
            },
            {
                name:'schoolGradYear',
                type:'string'
            },
            {
                name:'testScores',
                type:'array'
            },
//            {
//                name:'testCode',
//                type:'string'
////                mapping:'application.testScores'
//            },
//            {
//                name:'testDate',
//                type:'string'
////                mapping:'application.testScores'
//            },
//            {
//                name : 'testScore',
//                type : 'string'
////                mapping:'application.testScores'
//            },
//            {
//                name : 'collegeCode',
//                type : 'string'
////                mapping:'application.colleges'
//            },
//            {
//                name : 'state',
//                type : 'string'
////                mapping:'application.colleges'
//            },
//            {
//                name : 'city',
//                type : 'string'
////                mapping:'application.colleges'
//            },
//            {
//                name : 'degree',
//                type : 'string'
////                mapping:'application.colleges'
//            },
//            {
//                name : 'gradDate',
//                type : 'string'
////                mapping:'application.colleges'
//            },
            {
                name:'colleges',
                type:'array'
            },
            {
                name : 'primaryInterest',
                type : 'string'
            },
            {
                name : 'secondaryInterest',
                type : 'string'
            },
            {
                name : 'levelOfInterest',
                type : 'string'
            },
            {
                name : 'factorForChoosing',
                type : 'string'
            },
            {
                name:'otherInterests',
                type:'array'
            },{
              name:'otherInterestsDescr',
                type:'array'
            },
            {
              name:'sisInfo',
              type:'array'
            }
//            {
//                name:'applIndicator',
//                type:'application.sisInfo'
//            }
//            {
//                name : 'INT1',
//                type : 'string'
////                mapping:'application.otherInterests'
//            },
//            {
//                name : 'INT2',
//                type : 'string'
////                mapping:'application.otherInterests'
//            },
//            {
//                name : 'INT3',
//                type : 'string'
////                mapping:'application.otherInterests'
//            }

        ]
    }
});