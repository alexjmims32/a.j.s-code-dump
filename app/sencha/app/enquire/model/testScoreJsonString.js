Ext.define("mobEdu.enquire.model.testScoreJsonString", {
    extend:'Ext.data.Model',

    config:{
        fields : [
            {
                name : 'testCode',
                type : 'string'
            },
            {
                name : 'testDate',
                type : 'string'
            },
            {
                name : 'testScore',
                type : 'string'
            }
        ]
    }
});