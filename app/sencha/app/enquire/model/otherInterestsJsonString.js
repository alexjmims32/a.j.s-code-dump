Ext.define("mobEdu.enquire.model.otherInterestsJsonString", {
    extend:'Ext.data.Model',

    config:{
        fields : [
            {
                name : 'INT1',
                type : 'string'
            },
            {
                name : 'INT2',
                type : 'string'
            },
            {
                name : 'INT3',
                type : 'string'
            }
        ]
    }
});
