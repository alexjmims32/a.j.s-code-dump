Ext.define("mobEdu.enquire.model.localCreateApplication", {
    extend:'Ext.data.Model',

    config:{
        fields : [
            {
                name : 'localAppFirstName',
                type : 'string'
            },
            {
                name : 'localAppMiddleName',
                type : 'string'
            },
            {
                name : 'localAppLastName',
                type : 'string'
            },
            {
                name : 'localAppGender',
                type : 'string'
            },
            {
                name : 'localAppRace',
                type : 'string'
            },
            {
                name : 'localAppDateOfBirth',
                type : 'string'
            },
            {
                name : 'localAppEthnicity',
                type : 'string'
            },
            {
                name : 'localAppSSN',
                type : 'string'
            },
            {
                name : 'localAppCAddress1',
                type : 'string'
            },
            {
                name : 'localAppCAddress2',
                type : 'string'
            },
            {
                name : 'localAppCAddress3',
                type : 'string'
            },
            {
                name : 'localAppCCity',
                type : 'string'
            },
            {
                name : 'localAppCCounty',
                type : 'string'
            },
            {
                name : 'localAppCState',
                type : 'string'
            },
            {
                name : 'localAppCZip',
                type : 'string'
            },
            {
                name : 'localAppCCountry',
                type : 'string'
            },
            {
                name : 'localAppCPhone1',
                type : 'string'
            },
            {
                name : 'localAppCPhone2',
                type : 'string'
            },
            {
                name : 'localAppCEmail',
                type : 'string'
            },
            {
                name : 'vemail',
                type : 'string'
            },
            {
                name:'localAppParRel1',
                type:'string'
            },
            {
                name:'localAppParFN1',
                type:'string'
            },
            {
                name:'localAppParLN1',
                type:'string'
            },
            {
                name:'localAppParMN1',
                type:'string'
            },
            {
                name:'localAppParRel2',
                type:'string'
            },
            {
                name:'localAppParFN2',
                type:'string'
            },
            {
                name:'localAppParLN2',
                type:'string'
            },
            {
                name:'localAppParMN2',
                type:'string'
            },
            {
                name:'localAppVType',
                type:'string'
            },
            {
                name:'localAppVNation',
                type:'string'
            },
            {
                name:'localAppVNo',
                type:'string'
            },
            {
                name : 'localAppHsName',
                type : 'string'
            },
            {
                name : 'localAppHsCode',
                type : 'string'
            },
            {
                name : 'localAppHsCity',
                type : 'string'
            },
            {
                name : 'localAppHsState',
                type : 'string'
            },
            {
                name : 'localAppHsZip',
                type : 'string'
            },
            {
                name : 'localAppHsGYear',
                type : 'string'
            },
            {
                name : 'localAppHsGpa',
                type : 'string'
            },
            {
                name : 'localAppTestCode',
                type : 'string'
            },
            {
                name : 'localAppTestDate',
                type : 'string'
            },
            {
                name : 'localAppTestScore',
                type : 'string'
            },
            {
                name:'localAppCollName',
                type:'string'
            },
            {
                name:'localAppCollCode',
                type:'string'
            },
            {
              name:'localAppCollState',
                type:'string'
            },
            {
                name:'localAppCollCity',
                type:'string'
            },
            {
                name:'localAppCollDegree',
                type:'string'
            },
            {
                name:'localAppCollGDate',
                type:'string'
            },
            {
                name:'localAppCollAtFrom',
                type:'string'
            },
            {
                name:'localAppCollAtTo',
                type:'string'
            },
            {
                name : 'localAppStartTerm',
                type : 'string'
            },
            {
                name : 'localAppLevelCode',
                type : 'string'
            },
            {
                name : 'localAppMajorCode',
                type : 'string'
            },
            {
                name : 'localAppAdType',
                type : 'string'
            },
            {
                name : 'localAppSType',
                type : 'string'
            },
            {
                name : 'localAppResCode',
                type : 'string'
            },
            {
                name: 'localAppCollegeCode',
                type: 'string'
            },
            {
                name : 'localAppDegreeCode',
                type : 'string'
            },
            {
                name : 'localAppCampus',
                type : 'string'
            },
            {
                name : 'localAppEduGoal',
                type : 'string'
            },
            {
                name : 'localAppDep',
                type : 'string'
            },
            {
                name : 'localAppFullTime',
                type : 'string'
            },
            {
                name : 'localAppStatus',
                type : 'string'
            },
            {
                name : 'localAppStartYear',
                type : 'string'
            },
            {
                name : 'localAppPInterest',
                type : 'string'
            },
            {
                name : 'localAppSInterest',
                type : 'string'
            },
            {
                name : 'localAppLInterest',
                type : 'string'
            },
            {
                name : 'localAppFactor',
                type : 'string'
            },
            {
                name : 'localAppOtherIn',
                type : 'string'
            }

        ]
    }
});