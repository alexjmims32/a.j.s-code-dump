Ext.define("mobEdu.enquire.model.application", {
    extend:'Ext.data.Model',

  config:{
    fields : [ {
        name : 'status',
        type : 'string'
    },{
        name:'leadID',
        type:'string'
    },
        {
            name:'applicationID',
            type:'string'
        }
    ]
  }
});