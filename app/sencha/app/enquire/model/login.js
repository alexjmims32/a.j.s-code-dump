Ext.define('mobEdu.enquire.model.login', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: "status",
            type: 'string'
        }, {
            name: "applicationId",
            mapping: 'userInfo.applicationId'
        }, {
            name: 'role'
        }, {
            name: 'firstName',
            mapping: 'userInfo.firstName'
        }, {
            name: 'lastName',
            mapping: 'userInfo.lastName'
        }, {
            name: 'roleCode',
            mapping: 'userInfo.roleCode'
        }, {
            name: 'companyID',
            mapping: 'userInfo.companyID'
        }, {
            name: 'roleID',
            mapping: 'userInfo.roleID'
        }, {
            name: 'privileges',
            type: 'array',
            mapping: 'privileges'
        }, {
            name: 'recruiterId',
            mapping: 'userInfo.recruiterId'


        }]
    }
});