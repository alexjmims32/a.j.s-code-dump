Ext.define("mobEdu.enquire.model.checkValues", {
    extend:'Ext.data.Model',

    config:{
        fields : [
            {
                name : 'CITY',
                type : 'string'
            },
            {
                name : 'STATE',
                type : 'string'
            },
            {
                name : 'ZIP',
                type : 'string'
            },
            {
                name : 'AREA_CODE',
                type : 'string'
            },
            {
                name : 'TIME_ZONE',
                type : 'string'
            }
        ]
    }
});
