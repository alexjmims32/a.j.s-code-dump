Ext.define('mobEdu.enquire.model.subrMenu', {
    extend: 'Ext.data.Model',

    config: {
        fields: [{
            name: 'img'
        }, {
            name: 'title'
        }, {
            name: 'action',
            type: 'function'
        }]
    }
});