Ext.define("mobEdu.enquire.model.email", {
    extend:'Ext.data.Model',

    config:{
        fields : [
            {
                name : 'email',
                type : 'string'
            },
            {
                name : 'vemail',
                type : 'string'
            },
            {
                name : 'leadID',
                type : 'string'
            },
            {
                name : 'status',
                type : 'string'
            }
        ]
    }
});