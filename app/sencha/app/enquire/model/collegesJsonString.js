Ext.define("mobEdu.enquire.model.collegesJsonString", {
    extend:'Ext.data.Model',

    config:{
        fields : [
//            {
//                name : 'collegeName',
//                type : 'string'
//            },
            {
                name : 'collegeCode',
                type : 'string'
            },
            {
                name : 'state',
                type : 'string'
            },
            {
                name : 'city',
                type : 'string'
            },
            {
                name : 'degree',
                type : 'string'
            },
            {
                name : 'gradDate',
                type : 'string'
            }
//            {
//                name:'attndFrom',
//                type:'string'
//            },
//            {
//                name : 'attndTo',
//                type : 'string'
//            }
        ]
    }
});