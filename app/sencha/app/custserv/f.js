Ext.define('mobEdu.custserv.f', {
    requires: [
        'mobEdu.custserv.store.areas',
        'mobEdu.custserv.store.courseList',
        'mobEdu.custserv.store.feedback',
        'mobEdu.custserv.store.subAreas'
    ],
    statics: {
        feedbackPrevView: null,

        loadAreas: function(from, courseId) {
            if (from == undefined) {
                from = 'customerService';
            }
            mobEdu.custserv.f.feedbackPrevView = from;
            mobEdu.custserv.f.courseId = courseId;
            if (from == 'customerCourse') {
                mobEdu.courses.f.loadRegTermList('feedback');
            }
            mobEdu.util.get('mobEdu.custserv.view.feedback');
            var areasStore = mobEdu.util.getStore('mobEdu.custserv.store.areas');
            areasStore.getProxy().setHeaders({
                companyID: companyId
            });
            areasStore.getProxy().setUrl(encorewebserver + 'open/areas');
            areasStore.getProxy().setExtraParams({
                companyId: companyId
            });
            areasStore.getProxy().afterRequest = function() {
                mobEdu.custserv.f.areasResponseHandler();
            }
            areasStore.load();
            mobEdu.util.gaTrackEvent('encore', 'serviceareas');
        },

        areasResponseHandler: function() {
            var areasStore = mobEdu.util.getStore('mobEdu.custserv.store.areas');
            if (areasStore.data.length > 0) {
                var areas = new Array();
                var index = 1;
                areas[0] = ({
                    text: 'Select Area',
                    value: '0'
                });
                areasStore.each(function(record) {
                    areas[index] = ({
                        text: record.data.area,
                        value: record.data.areaId
                    });
                    index = index + 1;
                });
                Ext.getCmp('areas').suspendEvents();
                Ext.getCmp('areas').setOptions(areas);
                Ext.getCmp('areas').setValue('0');
                Ext.getCmp('areas').resumeEvents();

                Ext.getCmp('subAreas').setOptions([{
                    text: 'Select Sub Area',
                    value: '0'
                }]);
                Ext.getCmp('subAreas').select(0);
                mobEdu.custserv.f.showFeedback();
            }
        },

        onAreaSelectionChange: function(selectfield, newValue, oldValue, eOpts) {
            if (newValue != null && newValue != '' && newValue != '0') {
                var subAreasStore = mobEdu.util.getStore('mobEdu.custserv.store.subAreas');
                subAreasStore.getProxy().setHeaders({
                    companyID: companyId
                });
                subAreasStore.getProxy().setUrl(encorewebserver + 'open/subAreas');
                subAreasStore.getProxy().setExtraParams({
                    areaId: newValue
                });
                subAreasStore.getProxy().afterRequest = function() {
                    mobEdu.custserv.f.subAreasResponseHandler();
                }
                subAreasStore.load();
                mobEdu.util.gaTrackEvent('encore', 'subAreas');
            }
        },

        subAreasResponseHandler: function() {
            var subAreasStore = mobEdu.util.getStore('mobEdu.custserv.store.subAreas');
            if (subAreasStore.data.length > 0) {
                var subAreas = new Array();
                var index = 1;
                subAreas[0] = ({
                    text: 'Select Sub Area',
                    value: '0'
                });
                subAreasStore.each(function(record) {
                    subAreas[index] = ({
                        text: record.data.subArea,
                        value: record.data.email
                    });
                    index = index + 1;
                });
                Ext.getCmp('subAreas').setOptions(subAreas);
                Ext.getCmp('subAreas').select(0);
            }
        },

        onSubmitButtonTap: function() {
            var area = Ext.getCmp('areas').getValue();
            var subArea = Ext.getCmp('subAreas').getValue();
            var fullName = Ext.getCmp('fedFullName').getValue();
            var email = Ext.getCmp('fedEmail').getValue();
            var comments = Ext.getCmp('fedComments').getValue();
            var conf = Ext.getCmp('fedConf');
            var crseImprComments = Ext.getCmp('crseImprComments').getValue();
            var crseExpComments = Ext.getCmp('crseExpComments').getValue();
            var course = Ext.getCmp('feedbackCourseId').getValue();

            var courseId = null;
            var title = null;
            var description = null;
            var instructor = null;
            var rec = null;
            var areaName = '';
            var subAreaName = '';
            var termCode = null;
            var crn = null;
            var toAddress = null;

            if (conf.getChecked() == true) {
                conf = 'Y';
            } else {
                conf = 'N';
            }

            if (mobEdu.custserv.f.feedbackPrevView == 'customerCourse') {
                if ((course == null || course == '' || course == '0') || (crseExpComments == null || crseExpComments == '')) {
                    Ext.Msg.show({
                        message: 'Please provide all mandatory fields.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                } else {

                    comments = crseExpComments;
                    var coursesStore = mobEdu.util.getStore('mobEdu.custserv.store.courseList');
                    coursesStore.each(function(record) {
                        if (record.get('courseId') == course) {
                            rec = record;
                        }
                    });
                    if (rec != null) {
                        courseId = rec.get('courseId');
                        title = rec.get('courseTitle');
                        description = rec.get('description');
                        instructor = rec.get('faculty');
                        termCode = rec.get('termCode');
                        crn = rec.get('crn');
                    }
                    mobEdu.custserv.f.feedbackSubmission(null, fullName, email, comments, crseImprComments, conf, areaName, subAreaName, toAddress, courseId, title, description, instructor, termCode, crn);
                }
            } else if (mobEdu.custserv.f.feedbackPrevView == 'courses') {
                var store = mobEdu.util.getStore('mobEdu.courses.store.courseDetail');
                rec = store.getAt(0);
                if (rec != null) {
                    courseId = rec.get('courseId');
                    title = rec.get('courseTitle');
                    description = rec.get('description');
                    instructor = rec.get('faculty');
                    termCode = rec.get('termCode');
                    crn = rec.get('crn');
                }
                if ((crseExpComments == null || crseExpComments == '')) {
                    Ext.Msg.show({
                        message: 'Please provide all mandatory fields.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                } else {
                    comments = crseExpComments;
                    fullName = mobEdu.util.getFirstName() + ' ' + mobEdu.util.getLastName();
                    mobEdu.custserv.f.feedbackSubmission(area, fullName, email, comments, crseImprComments, conf, areaName, subAreaName, toAddress, courseId, title, description, instructor, termCode, crn);
                }
            } else if (mobEdu.custserv.f.feedbackPrevView == 'customerService') {
                areaName = Ext.getCmp('areas').getRecord().get('text');
                subAreaName = Ext.getCmp('subAreas').getRecord().get('text');
                toAddress = Ext.getCmp('subAreas').getValue();
                if (subAreaName == 'Select Sub Area') {
                    subAreaName = '';
                }
                if ((area == null || area === '' || area == '0') || (comments == null || comments == '')) {
                    Ext.Msg.show({
                        message: 'Please provide all mandatory fields.',
                        buttons: Ext.MessageBox.OK,
                        cls: 'msgbox'
                    });
                } else {
                    var areasStore = mobEdu.util.getStore('mobEdu.custserv.store.areas');
                    if (areasStore.data.length > 0) {
                        var hasSubareas = false;
                        var subAreas = new Array();
                        areasStore.each(function(record) {
                            if (record.get('id') == area) {
                                subAreas = record.get('subAreas');
                                if (subAreas != null)
                                    hasSubareas = true;
                            }
                        });
                    }
                    if (subArea == '0') {
                        if (hasSubareas == false) {
                            subArea = area;
                            mobEdu.custserv.f.feedbackSubmission(area, fullName, email, comments, crseImprComments, conf, areaName, subAreaName, toAddress);
                        } else {
                            Ext.Msg.show({
                                message: 'Please Select Sub Area.',
                                buttons: Ext.MessageBox.OK,
                                cls: 'msgbox'
                            });
                        }
                    } else {
                        mobEdu.custserv.f.feedbackSubmission(area, fullName, email, comments, crseImprComments, conf, areaName, subAreaName, toAddress);
                    }
                }
            }
        },
        feedbackSubmission: function(area, fullName, email, comments, crseImprComments, conf, areaName, subAreaName, toAddress, courseId, title, description, instructor, termCode, crn) {
            var flag = 0;
            //Validating name
            if (fullName != null && fullName != '') {
                var regExp = new RegExp("^[a-zA-Z ]+$");
                if (!regExp.test(fullName)) {
                    flag = 1;
                    Ext.Msg.show({
                        cls: 'msgbox',
                        title: null,
                        message: 'Invalid full name!!',
                        buttons: Ext.MessageBox.OK
                    });
                }

            }
            //Validating email address
            if (email != null && email != '') {
                var regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
                if (regMail.test(email) == false) {
                    flag = 1;
                    Ext.Msg.show({
                        title: null,
                        cls: 'msgbox',
                        message: 'Invalid email address!!',
                        buttons: Ext.MessageBox.OK
                    });
                }
            }
            if (flag == 0) {
                var feedbackStore = mobEdu.util.getStore('mobEdu.custserv.store.feedback');
                if (mobEdu.custserv.f.feedbackPrevView == 'courses' || mobEdu.custserv.f.feedbackPrevView == 'customerCourse') {
                    feedbackStore.getProxy().setHeaders({
                        Authorization: mobEdu.util.getAuthString(),
                        companyID: companyId
                    });
                } else {
                    feedbackStore.getProxy().setHeaders({
                        companyID: companyId
                    });
                }
                feedbackStore.getProxy().setUrl(webserver + 'submitFeedback');
                feedbackStore.getProxy().setExtraParams({
                    studentId: mobEdu.main.f.getStudentId(),
                    fromName: fullName,
                    fromAddress: email,
                    comments: comments,
                    comments2: crseImprComments,
                    areaId: area,
                    areaName: areaName,
                    subAreaName: subAreaName,
                    confidential: conf,
                    courseId: courseId,
                    title: title,
                    description: description,
                    instructor: instructor,
                    termCode: termCode,
                    crn: crn,
                    recipients: toAddress,
                    companyId: companyId
                });
                feedbackStore.getProxy().afterRequest = function() {
                    mobEdu.custserv.f.feedbackResponseHandler();
                }
                feedbackStore.load();
                mobEdu.util.gaTrackEvent('encore', 'feedback');
            }
        },
        feedbackResponseHandler: function() {
            var feedbackStore = mobEdu.util.getStore('mobEdu.custserv.store.feedback');
            var status = feedbackStore.getProxy().getReader().rawData;
            if (status == 'SUCCESS') {
                Ext.Msg.show({
                    cls: 'msgbox',
                    title: null,
                    message: 'Thank you for your feedback.',
                    buttons: Ext.MessageBox.OK,
                    fn: function(btn) {
                        if (btn == 'ok') {
                            mobEdu.custserv.f.onFeedbackBackTap();
                        }
                    }
                });
            } else {
                Ext.Msg.show({
                    cls: 'msgbox',
                    title: null,
                    message: status,
                    buttons: Ext.MessageBox.OK
                });
            }
        },

        onFeedbackBackTap: function() {
            mobEdu.custserv.f.resetFeedbackView();
            if (mobEdu.custserv.f.feedbackPrevView == 'courses') {
                if (Ext.os.is.Phone) {
                    mobEdu.courses.f.showCourseDetails();
                } else {
                    mobEdu.courses.f.showCourseList();
                }
            } else {
                mobEdu.util.showMainView();
            }
        },

        resetFeedbackView: function() {
            Ext.getCmp('areas').setOptions([]);
            Ext.getCmp('subAreas').setOptions([]);
            Ext.getCmp('fedFullName').setValue('');
            Ext.getCmp('fedEmail').setValue('');
            Ext.getCmp('fedComments').setValue('');


            Ext.getCmp('feedbackCourseId').setOptions([]);
            Ext.getCmp('feedbackCourseId').setValue('0');
            Ext.getCmp('crseExpComments').setValue('');
            Ext.getCmp('crseImprComments').setValue('');
        },

        onMenuItemTap: function(view, index, target, record, e, eOpts) {
            mobEdu.util.deselectSelectedItem(index, view);
            var title = record.get('title');
            if (title != null && title != '') {
                if (title == 'Customer Service') {
                    mobEdu.custserv.f.loadAreas('customerService');
                } else if (title == 'Course Evaluation') {
                    mobEdu.util.doLoginCheck(true, 'mobEdu.custserv.f.loadCourseEvaluation', 'CUSTSERV');
                }
            }
        },

        loadCourseEvaluation: function() {
            mobEdu.custserv.f.feedbackPrevView = 'customerCourse';
            mobEdu.custserv.f.loadCurrentTermCourses();
        },

        loadCurrentTermCourses: function() {
            var coursesStore = mobEdu.util.getStore('mobEdu.custserv.store.courseList');
            coursesStore.getProxy().setHeaders({
                Authorization: mobEdu.util.getAuthString(),
                companyID: companyId
            });
            coursesStore.getProxy().setUrl(webserver + 'getCurrentTermCourses');
            coursesStore.getProxy().setExtraParams({
                studentId: mobEdu.main.f.getStudentId(),
                companyId: companyId
            });
            coursesStore.getProxy().afterRequest = function() {
                mobEdu.custserv.f.updateCoursesDropdown();
            };
            coursesStore.load();
            mobEdu.util.gaTrackEvent('encore', 'currenttermcourses');
        },

        updateCoursesDropdown: function() {
            var coursesStore = mobEdu.util.getStore('mobEdu.custserv.store.courseList');
            mobEdu.util.get('mobEdu.custserv.view.feedback');
            if (coursesStore.data.length > 0) {
                var courses = new Array();
                var index = 1;
                courses[0] = ({
                    text: 'Select a course',
                    value: '0'
                });
                coursesStore.each(function(record) {
                    courses[index] = ({
                        text: record.data.courseTitle + '  (' + record.data.crn + ')',
                        value: record.data.courseId
                    });
                    index = index + 1;
                });
                Ext.getCmp('feedbackCourseId').setOptions(courses);
                Ext.getCmp('feedbackCourseId').select(0);
                mobEdu.custserv.f.showFeedback();
            } else {
                mobEdu.custserv.f.showMenu();
                Ext.Msg.show({
                    message: 'No courses for evaluation.',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }

        },

        showMenu: function() {
            mobEdu.util.updatePrevView(mobEdu.util.showMainView);
            mobEdu.util.show('mobEdu.custserv.view.menu');
            mobEdu.util.setTitle('CUSTSERV', Ext.getCmp('customerSTitle'));
        },

        showFeedback: function() {
            mobEdu.util.updatePrevView(mobEdu.custserv.f.onFeedbackBackTap);
            mobEdu.util.show('mobEdu.custserv.view.feedback');
            var title = 'Customer Service';
            if (mobEdu.custserv.f.feedbackPrevView == 'customerCourse' || mobEdu.custserv.f.feedbackPrevView == 'courses') {
                title = 'Course Evaluation';
            }
            mobEdu.util.setTitle(null, Ext.getCmp('feedbackTitleBar'), title);

            if (mobEdu.custserv.f.feedbackPrevView == 'courses') {
                Ext.getCmp('custService').hide();
                Ext.getCmp('crseEvaluation').show();
                Ext.getCmp('feedbackCourseId').hide();
            } else if (mobEdu.custserv.f.feedbackPrevView == 'customerService') {
                Ext.getCmp('crseEvaluation').hide();
                Ext.getCmp('custService').show();
            } else if (mobEdu.custserv.f.feedbackPrevView == 'customerCourse') {
                Ext.getCmp('crseEvaluation').show();
                Ext.getCmp('custService').hide();
                Ext.getCmp('feedbackCourseId').show();
            }
        }
    }
});