Ext.define('mobEdu.custserv.model.areas',{
   extend:'Ext.data.Model',

   config:{
       fields:[{
               name:'areaId'
           },
           {
               name : 'area'
           },{
               name:'subAreaId'
           },
           {
               name:'email'
           },
           {
               name : 'subArea'
           },{
               name : 'supervisor'
           },{
               name : 'assocDean'
           },{
               name : 'dean'
           },{
               name : 'hr'
           },{
               name : 'provost'
           },{
            name : 'subAreas',
            type : 'array'
        }

       ]
   }

});