Ext.define('mobEdu.custserv.store.subAreas',{
extend: 'mobEdu.data.store',
     requires:[
       'mobEdu.custserv.model.areas'
   ],
   config:{
       storeId:'mobEdu.custserv.store.subAreas',
        autoLoad: false,

        model: 'mobEdu.custserv.model.areas'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'subAreas'        
        return proxy;
    }
})