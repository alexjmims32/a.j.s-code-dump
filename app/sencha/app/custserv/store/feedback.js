Ext.define('mobEdu.custserv.store.feedback',{
extend: 'mobEdu.data.store',
     requires:[
       'mobEdu.custserv.model.areas'
   ],
   config:{
       storeId:'mobEdu.custserv.store.feedback',
        autoLoad: false,

        model: 'mobEdu.custserv.model.areas'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= ''        
        return proxy;
    }
})