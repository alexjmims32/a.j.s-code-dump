Ext.define('mobEdu.custserv.store.areas',{
extend: 'mobEdu.data.store',
     requires:[
       'mobEdu.custserv.model.areas'
   ],
   config:{
       storeId:'mobEdu.custserv.store.areas',
        autoLoad: false,

        model: 'mobEdu.custserv.model.areas'
   },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'areas'        
        return proxy;
    }
})