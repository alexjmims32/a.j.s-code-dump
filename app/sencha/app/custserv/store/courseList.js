Ext.define('mobEdu.custserv.store.courseList', {
extend: 'mobEdu.data.store',

    requires: [
        'mobEdu.custserv.model.courseList'
    ],

    config: {
        storeId: 'mobEdu.custserv.store.courseList',

        autoLoad: false,

        model: 'mobEdu.custserv.model.courseList'
    },
    initProxy: function() {
        var proxy = this.callParent();
        proxy.reader.rootProperty= 'registeredCourseList.registeredCourse'        
        return proxy;
    }
});
