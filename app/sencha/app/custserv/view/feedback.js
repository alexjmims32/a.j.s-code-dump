Ext.define('mobEdu.custserv.view.feedback', {
    extend: 'Ext.form.Panel',
    config: {
        scroll: 'vertical',
        fullscreen: true,
        cls: 'logo',
        layout: {
            type: 'vbox'
        },
        items: [{
            xtype: 'customToolbar',
            title: 'feedback',
            id: 'feedbackTitleBar'
        }, {
            xtype: 'container',
            id: 'custService',
            name: 'custService',
            items: [{
                xtype: 'selectfield',
                name: 'areas',
                id: 'areas',
                required: true,
                placeHolder: 'Select Area',
                listeners: {
                    change: function(selectfield, newValue, oldValue, eOpts) {
                        mobEdu.custserv.f.onAreaSelectionChange(selectfield, newValue, oldValue, eOpts);
                    }
                }
            }, {
                xtype: 'selectfield',
                name: 'subAreas',
                required: true,
                placeHolder: 'Select sub Area',
                id: 'subAreas'
            }, {
                xtype: 'textfield',
                name: 'fedFullName',
                placeHolder: 'Enter Full name(optional)',
                id: 'fedFullName',
                useClearIcon: true
            }, {
                xtype: 'emailfield',
                name: 'fedEmail',
                placeHolder: 'Enter email(optional)',
                id: 'fedEmail',
                useClearIcon: true,
                initialize: function() {
                            var me    = this,
                                input = me.getInput().element.down('input');

                            input.set({
                                pattern : '[a-z A-Z 0-9 "." "_"]* @ [a-z 0-9]*"."[a-z][a-z][a-z]'
                            });

                            me.callParent(arguments);
                        }
            }, {
                xtype: 'textareafield',
                placeHolder: 'Enter Comments/Complaints/Complements(*)',
                maxRows: 4,
                name: 'fedComments',
                id: 'fedComments',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'checkboxfield',
                id: 'fedConf',
                name: 'fedConf',
                label: 'Confidentiality',
                labelWidth: '50%'
            }, {
                xtype: 'fieldset',
                instructions: 'Your comments/complaints/complements will be treated as confidential and anonymous if you check applicable box.'
            }]
        }, {
            xtype: 'container',
            id: 'crseEvaluation',
            name: 'crseEvaluation',
            items: [{
                xtype: 'selectfield',
                name: 'feedbackCourseId',
                placeHolder: 'Select a Course',
                id: 'feedbackCourseId'
            }, {
                xtype: 'label',
                padding: '5 0 0 5',
                cls:'labeltextcolor',
                html: '<h4><b class=>How would you describe the instruction and your classroom experience?(*)</b></h4>'
            }, {
                xtype: 'textareafield',
                maxRows: 4,
                name: 'crseExpComments',
                id: 'crseExpComments',
                required: true,
                useClearIcon: true
            }, {
                xtype: 'label',
                padding: '5 0 0 5',
                cls:'labeltextcolor',
                html: '<h4><b>Please provide any other comments on this course for improvement</b></h4>'
            }, {
                xtype: 'textareafield',
                maxRows: 4,
                name: 'crseImprComments',
                id: 'crseImprComments',
                useClearIcon: true
            }, {
                xtype: 'fieldset',
                baseCls: 'crsefeedback',
                instructions: '<h5><br/><center>We appreciate and value your feedback. Your feedback will be used to improve this course classroom experience for all students.</center></h5>'
            }]
        }, {
            xtype: 'toolbar',
            docked: 'bottom',
            layout: {
                pack: 'right'
            },
            items: [{
                text: 'Submit',
                align: 'right',
                handler: function() {
                    mobEdu.custserv.f.onSubmitButtonTap();
                }
            }]
        }]
    }
});