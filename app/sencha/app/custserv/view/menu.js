Ext.define('mobEdu.custserv.view.menu', {
	extend: 'Ext.Panel',
	config: {
		scroll: 'vertical',
		fullscreen: true,
		layout: 'fit',
		items: [{
				xtype: 'list',
				cls: 'logo',
				itemTpl: '<table width="100%"><tr><td width="90%" align="left"><h3>{title}</h3></td><td width="10%" align="right"><div class="arrow" /></td></tr></table>',
				data: customerService,
				singleSelect: true,
				loadingText: '',
				listeners: {
					itemtap: function(view, index, target, record, e, eOpts) {
						mobEdu.custserv.f.onMenuItemTap(view, index, target, record, e, eOpts);
					}
				}
			}, {
				xtype: 'customToolbar',
				id: 'customerSTitle'
			}],
		flex: 1
	}
});