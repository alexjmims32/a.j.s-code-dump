/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".

 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src',
    'mobEdu': 'app'
});
//</debug>

Ext.namespace('Ext.ux');

Ext.ux.Loader = Ext.apply({}, {
    load: function(fileList, callback, scope, preserveOrder) {
        var scope = scope || this,
            head = document.getElementsByTagName("head")[0],
            fragment = document.createDocumentFragment(),
            numFiles = fileList.length,
            loadedFiles = 0,
            me = this;

        // Loads a particular file from the fileList by index. This is used when preserving order
        var loadFileIndex = function(index) {
            head.appendChild(
                me.buildScriptTag(fileList[index], onFileLoaded)
            );
        };

        /**
         * Callback function which is called after each file has been loaded. This calls the callback
         * passed to load once the final file in the fileList has been loaded
         */
        var onFileLoaded = function() {
            loadedFiles++;

            //if this was the last file, call the callback, otherwise load the next file
            if (numFiles == loadedFiles && typeof callback == 'function') {
                callback.call(scope);
            } else {
                if (preserveOrder === true) {
                    loadFileIndex(loadedFiles);
                }
            }
        };

        if (preserveOrder === true) {
            loadFileIndex.call(this, 0);
        } else {
            //load each file (most browsers will do this in parallel)
            Ext.each(fileList, function(file, index) {
                fragment.appendChild(
                    this.buildScriptTag(file, onFileLoaded)
                );
            }, this);

            head.appendChild(fragment);
        }
    },

    buildScriptTag: function(filename, callback) {
        var exten = filename.substr(filename.lastIndexOf('.') + 1);
        //console.log('Loader.buildScriptTag: filename=[%s], exten=[%s]', filename, exten);
        if (exten == 'js') {
            var script = document.createElement('script');
            script.type = "text/javascript";
            script.src = filename;

            //IE has a different way of handling <script> loads, so we need to check for it here
            if (script.readyState) {
                script.onreadystatechange = function() {
                    if (script.readyState == "loaded" || script.readyState == "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else {
                script.onload = callback;
            }
            return script;
        }
        if (exten == 'css') {
            var style = document.createElement('link');
            style.rel = 'stylesheet';
            style.type = 'text/css';
            style.href = filename;
            callback();
            return style;
        }
    }
});


Ext.application({
    name: 'mobEdu',

    eventPublishers: {
        touchGesture: {
            recognizers: {
                tap: {
                    xclass: 'Ext.event.recognizer.Tap',
                    moveDistance: 20 //This was adjusted from the default 8
                }
            }
        }
    },

    requires: [
        'mobEdu.globals',
        'Ext.ux.touch.grid.feature.Abstract',
        'Ext.ux.touch.grid.feature.CheckboxSelection',
        'Ext.ux.touch.grid.feature.Editable',
        'Ext.ux.touch.grid.feature.Feature',
        'Ext.ux.touch.grid.feature.Paging',
        'Ext.ux.touch.grid.feature.Sorter',
        'Ext.ux.touch.grid.View',
        'Ext.ux.IframeComponent',
        'Ext.ux.PinchZoomImage',
        'Ext.ux.TouchCalendarView',
        'Ext.ux.Transform',
        'Ext.ux.PanelAction',
        'mobEdu.util',
        'mobEdu.storeUtil',
        'mobEdu.acc.f',
        'mobEdu.acc.view.makePayment',
        'mobEdu.acc.view.menu',
        'mobEdu.acc.view.phone.charges',
        'mobEdu.acc.view.phone.due',
        'mobEdu.acc.view.phone.grid',
        'mobEdu.acc.view.phone.payments',
        'mobEdu.acc.view.phone.summary',
        'mobEdu.acc.view.phone.summaryDetails',
        'mobEdu.acc.view.phone.termList',
        'mobEdu.acc.view.tablet.grid',
        'mobEdu.acc.view.tablet.summary',
        'mobEdu.acc.view.tablet.summaryDetails',
        'mobEdu.acc.view.tablet.termList',
        //'mobEdu.alumni.f',
        //'mobEdu.alumni.view.details',
        //'mobEdu.alumni.view.list',
        //'mobEdu.alumni.view.rssList',
        'mobEdu.catlogue.f',
        'mobEdu.catlogue.view.phone.search',
        'mobEdu.catlogue.view.searchCourseDetail',
        'mobEdu.catlogue.view.tablet.search',
        'mobEdu.Cookies',
        'mobEdu.finaid.f',
        'mobEdu.finaid.view.phone.menu',
        'mobEdu.finaid.view.phone.resources',
        'mobEdu.finaid.view.resources',
        'mobEdu.finaid.view.phone.appointments',
        'mobEdu.finaid.view.phone.messages',
        'mobEdu.finaid.view.coa',
        'mobEdu.finaid.view.req',
        'mobEdu.finaid.view.holds',
        'mobEdu.finaid.view.phone.holdsInfo',
        'mobEdu.finaid.view.tablet.menu',
        'mobEdu.finaid.view.award',
        'mobEdu.finaid.view.phone.awards',
        'mobEdu.finaid.view.phone.costAttendance',
        'mobEdu.finaid.view.phone.requirements',
        'mobEdu.finaid.view.loanHistory',
        'mobEdu.finaid.view.phone.loanHistory',
        'mobEdu.finaid.view.paymentSchedule',
        'mobEdu.finaid.view.phone.paymentSchedule',
        'mobEdu.finaid.view.links',
        'mobEdu.finaid.view.emailDetail',
        'mobEdu.finaid.view.tablet.msgPopup',
        'mobEdu.finaid.view.phone.links',
        'mobEdu.finaid.view.appointmentsCalendar',
        'mobEdu.counselor.f',
        'mobEdu.counselor.view.search',
        'mobEdu.counselor.view.advSearch',
        'mobEdu.counselor.view.phone.advSearch',
        'mobEdu.counselor.view.phone.advSearchList',
        'mobEdu.counselor.view.details',
        'mobEdu.courses.f',
        'mobEdu.courses.view.calendarView',
        'mobEdu.courses.view.coursesMap',
        'mobEdu.courses.view.dropStatus',
        'mobEdu.courses.view.mapDirectionInst',
        'mobEdu.courses.view.phone.courseDetail',
        'mobEdu.courses.view.phone.courseList',
        'mobEdu.courses.view.phone.termList',
        'mobEdu.courses.view.tablet.courseList',
        'mobEdu.courses.view.tablet.termList',
        'mobEdu.courses.view.attCrsePopup',
        'mobEdu.advising.f',
        'mobEdu.advising.view.details',
        'mobEdu.advising.view.menu',
        'mobEdu.advising.view.phone.students',
        'mobEdu.advising.view.tablet.students',
        'mobEdu.advising.view.tablet.newApptPopup',
        'mobEdu.advising.view.tablet.newMsgPopup',
        'mobEdu.advising.view.tablet.msgPopup',
        'mobEdu.advising.view.courses',
        'mobEdu.advising.view.currInfo',
        'mobEdu.advising.view.holds',
        'mobEdu.advising.view.newAppt',
        'mobEdu.advising.view.newMsg',
        'mobEdu.advising.view.subMenu',
        'mobEdu.advising.view.viewAppts',
        'mobEdu.advising.view.viewMsgs',
        'mobEdu.advising.view.emailDetail',
        'mobEdu.advising.view.appointmentCalendar',
        'mobEdu.crl.f',
        'mobEdu.crl.view.curriculumInfo',
        'mobEdu.crl.view.studentHolds',
        'mobEdu.crl.view.studentHoldsGrid',
        'mobEdu.crl.view.billingInfo',
        'mobEdu.custserv.f',
        'mobEdu.custserv.view.feedback',
        'mobEdu.custserv.view.menu',
        'mobEdu.data.store',
        'mobEdu.directory.f',
        'mobEdu.directory.view.dirSearchList',
        'mobEdu.directory.view.dirSearch',
        'mobEdu.directory.view.emerCntList',
        'mobEdu.directory.view.impNumCntList',
        'mobEdu.directory.view.paging',
        'mobEdu.directory.view.peopleDetails',
        'mobEdu.help.f',
        'mobEdu.help.view.about',
        'mobEdu.help.view.aboutEntourage',
        'mobEdu.help.view.aboutApp',
        'mobEdu.help.view.faqs',
        'mobEdu.help.view.menu',
        'mobEdu.hist.f',
        'mobEdu.hist.view.studentHistory',
        'mobEdu.main.f',
        'mobEdu.main.view.login',
        'mobEdu.main.view.campus',
        'mobEdu.main.view.dashboard',
        'mobEdu.main.view.menu',
        'mobEdu.main.view.smenu',
        'mobEdu.main.view.signUp',
        'mobEdu.main.view.preMenu',
        'mobEdu.main.view.bevelmenu',
        'mobEdu.main.view.modules',
        'mobEdu.main.view.splash',
        'mobEdu.main.view.studentFeedback',
        'mobEdu.main.view.studentDetailsChecking',
        'mobEdu.maps.f',
        'mobEdu.maps.view.browse',
        'mobEdu.maps.view.campus',
        'mobEdu.maps.view.popup',
        'mobEdu.maps.view.campusList',
        'mobEdu.maps.view.directionDetails',
        'mobEdu.maps.view.directionMap',
        'mobEdu.maps.view.filter',
        'mobEdu.maps.view.googleMap',
        'mobEdu.maps.view.locationDetails',
        'mobEdu.maps.view.search',
        'mobEdu.maps.view.select',
        'mobEdu.feeds.f',
        'mobEdu.feeds.view.details',
        'mobEdu.feeds.view.calendar',
        'mobEdu.feeds.view.list',
        'mobEdu.feeds.view.rssList',
        'mobEdu.feeds.view.socialMedia',
        'mobEdu.feeds.view.videoView',
        'mobEdu.notif.f',
        'mobEdu.notif.view.alertPopup',
        'mobEdu.notif.view.details',
        'mobEdu.notif.view.makePayment',
        'mobEdu.notif.view.popup',
        'mobEdu.profile.f',
        'mobEdu.profile.view.myInfo',
        'mobEdu.profile.view.studentInfo',
        'mobEdu.profile.view.studentProfile',
        'mobEdu.profile.view.directoryOptOut',
        'mobEdu.profile.view.bioInfo',
        'mobEdu.profile.view.updateInfo',
        'mobEdu.people.view.advisors',
        'mobEdu.people.view.faculty',
        'mobEdu.people.view.classmates',
        'mobEdu.people.view.peopleList',
        'mobEdu.people.f',
        'mobEdu.reg.f',
        'mobEdu.reg.view.cartSchedule',
        'mobEdu.reg.view.advSearch',
        'mobEdu.reg.view.advSearchResult',
        'mobEdu.reg.view.paging',
        'mobEdu.reg.view.advsearchpaging',
        'mobEdu.reg.view.phone.search',
        'mobEdu.reg.view.phone.termList',
        'mobEdu.reg.view.phone.viewCart',
        'mobEdu.reg.view.registerStatus',
        'mobEdu.reg.view.searchCourseDetail',
        'mobEdu.reg.view.tablet.search',
        'mobEdu.reg.view.tablet.termList',
        'mobEdu.reg.view.tablet.viewCart',
        'mobEdu.reg.view.tips',
        'mobEdu.reg.view.subject',
        'mobEdu.reg.view.immunization',
        'mobEdu.reg.view.termsAndConditions',
        'mobEdu.reg.view.olrPopup',
        'mobEdu.reg.view.finResAgreement',
        'mobEdu.settings.f',
        'mobEdu.settings.view.feeds',
        'mobEdu.settings.view.menu',
        'mobEdu.settings.view.preferences',
        'mobEdu.settings.view.proxySession',
        'mobEdu.sports.f',
        'mobEdu.sports.view.details',
        'mobEdu.sports.view.list',
        'mobEdu.sports.view.media',
        'mobEdu.sports.view.news',
        'mobEdu.sports.view.photos',
        'mobEdu.sports.view.results',
        'mobEdu.sports.view.rssList',
        'mobEdu.sports.view.schedules',
        'mobEdu.enradms.leads.view.adminAppList',
        'mobEdu.enradms.leads.view.admissionApp',
        'mobEdu.enradms.leads.view.appointmentDetail',
        'mobEdu.enradms.leads.view.appointmentsList',
        'mobEdu.enradms.leads.view.documentList',
        'mobEdu.enradms.leads.view.documentView',
        'mobEdu.enradms.leads.view.emailDetail',
        'mobEdu.enradms.leads.view.emailList',
        'mobEdu.enradms.leads.view.financialApp',
        'mobEdu.enradms.leads.view.leadNewEmail',
        'mobEdu.enradms.leads.view.newAppointment',
        'mobEdu.enradms.leads.view.newMail',
        'mobEdu.enradms.leads.view.notificationDetail',
        'mobEdu.enradms.leads.view.notificationsList',
        'mobEdu.enradms.leads.view.profileDetail',
        'mobEdu.enradms.leads.view.profileList',
        'mobEdu.enradms.leads.view.updateAppointment',
        'mobEdu.enradms.leads.view.updateProfile',
        'mobEdu.enradms.leads.view.viewProfileList',
        'mobEdu.enradms.main.view.allLeadAssignPopup',
        'mobEdu.enradms.main.view.allLeadDetail',
        'mobEdu.enradms.main.view.allLeadsList',
        'mobEdu.enradms.main.view.allLeadsRecruiterSearch',
        'mobEdu.enradms.main.view.email',
        'mobEdu.enradms.main.view.leadAssignPopup',
        'mobEdu.enradms.main.view.leadProfileUpdate',
        'mobEdu.enradms.main.view.menu',
        'mobEdu.enradms.main.view.rEmailDetail',
        'mobEdu.enradms.main.view.recruiterAppointmentCalendar',
        'mobEdu.enradms.main.view.recruiterEmailsList',
        'mobEdu.enradms.main.view.recruiterSearch',
        'mobEdu.enradms.main.view.reqAppointmentDetail',
        'mobEdu.enradms.main.view.reqAppointmentsList',
        'mobEdu.enradms.main.view.reqNewAppointment',
        'mobEdu.enradms.main.view.reqNewEmail',
        'mobEdu.enradms.main.view.respondEmail',
        'mobEdu.enradms.main.view.supLeadDetail',
        'mobEdu.enradms.main.view.supLeadsList',
        'mobEdu.enradms.main.view.updateLeadProfile',
        'mobEdu.enradms.main.view.updateReqAppointment',
        'mobEdu.enroute.leads.view.adminAppList',
        'mobEdu.enroute.leads.view.admissionApp',
        'mobEdu.enroute.leads.view.appointmentDetail',
        'mobEdu.enroute.leads.view.appointmentsList',
        'mobEdu.enroute.leads.view.emailDetail',
        'mobEdu.enroute.leads.view.emailList',
        'mobEdu.enroute.leads.view.financialApp',
        'mobEdu.enroute.leads.view.leadNewEmail',
        'mobEdu.enroute.leads.view.leadViewApplication',
        'mobEdu.enroute.leads.view.newAppointment',
        'mobEdu.enroute.leads.view.newMail',
        'mobEdu.enroute.leads.view.notificationDetail',
        'mobEdu.enroute.leads.view.notificationsList',
        'mobEdu.enroute.leads.view.profileDetail',
        'mobEdu.enroute.leads.view.profileList',
        'mobEdu.enroute.leads.view.updateAppointment',
        'mobEdu.enroute.leads.view.updateProfile',
        'mobEdu.enroute.leads.view.viewProfileList',
        'mobEdu.enroute.leads.view.rAppointmentCalendar',
        'mobEdu.enroute.main.view.allLeadAssignPopup',
        'mobEdu.enroute.main.view.allLeadDetail',
        'mobEdu.enroute.main.view.allLeadsList',
        'mobEdu.enroute.main.view.allLeadsRecruiterSearch',
        'mobEdu.enroute.main.view.email',
        'mobEdu.enroute.main.view.leadAssignPopup',
        'mobEdu.enroute.main.view.leadProfileUpdate',
        'mobEdu.enroute.main.view.menu',
        'mobEdu.enroute.main.view.rEmailDetail',
        'mobEdu.enroute.main.view.recruiterAppointmentCalendar',
        'mobEdu.enroute.main.view.recruiterEmailsList',
        'mobEdu.enroute.main.view.recruiterSearch',
        'mobEdu.enroute.main.view.reqAppointmentDetail',
        'mobEdu.enroute.main.view.reqAppointmentsList',
        'mobEdu.enroute.main.view.reqNewAppointment',
        'mobEdu.enroute.main.view.reqNewEmail',
        'mobEdu.enroute.main.view.respondEmail',
        'mobEdu.enroute.main.view.supLeadDetail',
        'mobEdu.enroute.main.view.supLeadsList',
        'mobEdu.enroute.main.view.updateLeadProfile',
        'mobEdu.enroute.main.view.updateReqAppointment',
        'mobEdu.enroute.main.view.viewApplication',
        'mobEdu.enroute.main.view.searchPopup',
        'mobEdu.enroute.main.view.searchAppPopup',
        'mobEdu.enroute.main.view.viewProfileList',
        'mobEdu.enroute.main.view.allLeadProfileList',
        'mobEdu.enter.view.campusMap',
        'mobEdu.enter.view.capturePic',
        'mobEdu.enter.view.cityMap',
        'mobEdu.enter.view.detailedProfile',
        'mobEdu.enter.view.directory',
        'mobEdu.enter.view.eNterMenu',
        'mobEdu.enter.view.healthInsu',
        'mobEdu.enter.view.hr',
        'mobEdu.enter.view.mobileClsRoom',
        'mobEdu.enter.view.myChecklist',
        'mobEdu.enter.view.myFiles',
        'mobEdu.enter.view.myProfile',
        'mobEdu.enter.view.myStatus',
        'mobEdu.enter.view.parking',
        'mobEdu.enter.view.pendingApprovals',
        'mobEdu.enter.view.welcome',
        //'mobEdu.dinserv.f',
        //'mobEdu.dinserv.view.list',
        'mobEdu.acc.view.tablet.ebillSummary',
        'mobEdu.acc.view.tablet.ebillGrid',
        'mobEdu.acc.view.tablet.ebillTermList',
        'mobEdu.acc.view.phone.ebillSummary',
        'mobEdu.acc.view.phone.ebillGrid',
        'mobEdu.acc.view.phone.ebillTermList',
        'mobEdu.help.view.videoList',
        'mobEdu.help.view.videoView',
        'mobEdu.att.view.menu',
        'mobEdu.att.view.studentList',
        'mobEdu.att.view.sendMessage',
        'mobEdu.att.view.sendMessageToClass',
        'mobEdu.att.view.codeGenerator',
        'mobEdu.att.f',
        'mobEdu.att.view.submitCodePopUp',
        'mobEdu.att.view.attendance',
        'mobEdu.att.view.roleSelectionPopup',
        'mobEdu.reg.view.updateCurriculumInfo',
        'mobEdu.catalogue.view.tips',
        'mobEdu.messages.view.rEmailDetail',
        'mobEdu.messages.view.recruiterEmailsList',
        'mobEdu.messages.view.respondEmail',
        'mobEdu.messages.view.searchPopup',
        'mobEdu.messages.view.paging',
        'mobEdu.messages.view.menu',
        'mobEdu.appointments.view.appointmentDetail',
        'mobEdu.appointments.view.appointmentsList',
        'mobEdu.appointments.view.mainAppointmentCalendar',
        'mobEdu.appointments.view.newAppointment',
        'mobEdu.appointments.view.updateAppointment',
        'mobEdu.appointments.view.menu',
        'mobEdu.appointments.view.paging',
        'mobEdu.enroute.main.view.leadSearchMenu',
        'mobEdu.enroute.main.view.leadSearchByState',
        'mobEdu.enroute.main.view.leadSearchBySchool',
        'mobEdu.enroute.main.view.leadSearchByRank',
        'mobEdu.enquire.view.admissions',
        'mobEdu.enquire.view.appointmentScheduleCalendar',
        'mobEdu.enquire.view.basicInfo',
        'mobEdu.enquire.view.collegesPopup',
        'mobEdu.enquire.view.contactAddressInfo',
        'mobEdu.enquire.view.contactPhoneInfo',
        'mobEdu.enquire.view.contactRecruiter',
        'mobEdu.enquire.view.createAppAdmissionDetails',
        'mobEdu.enquire.view.createAppBasicInfo',
        'mobEdu.enquire.view.createAppCollegeDetails',
        'mobEdu.enquire.view.createAppContactAddress',
        'mobEdu.enquire.view.createAppHighSchoolInfo',
        'mobEdu.enquire.view.createAppInterests',
        'mobEdu.enquire.view.createAppParentDetails',
        'mobEdu.enquire.view.createAppTestScores',
        'mobEdu.enquire.view.createAppVisaDetails',
        'mobEdu.enquire.view.enrollInfo',
        'mobEdu.enquire.view.finalSummary',
        'mobEdu.enquire.view.highSchoolInfo',
        'mobEdu.enquire.view.loginInfo',
        'mobEdu.enquire.view.myProfile',
        'mobEdu.enquire.view.newAppointment',
        'mobEdu.enquire.view.newEmail',
        'mobEdu.enquire.view.otherInfo',
        'mobEdu.enquire.view.profileUpdate',
        'mobEdu.enquire.view.replyMessage',
        'mobEdu.enquire.view.requestForInformation',
        'mobEdu.enquire.view.resetPassword',
        'mobEdu.enquire.view.scheduleVisit',
        'mobEdu.enquire.view.subMenu',
        'mobEdu.enquire.view.testScores',
        'mobEdu.enquire.view.testScoresGrid',
        'mobEdu.enquire.view.testScoresPopup',
        'mobEdu.enquire.view.updateAppointment',
        'mobEdu.enquire.view.viewApplication',
        'mobEdu.enquire.view.welcome',
        'mobEdu.enquire.view.whyEnroll',
        'mobEdu.enquire.view.phone.appointmentDetail',
        'mobEdu.enquire.view.phone.appointmentsList',
        'mobEdu.enquire.view.phone.emailDetail',
        'mobEdu.enquire.view.phone.emailsList',
        'mobEdu.enquire.view.tablet.appointmentsList',
        'mobEdu.enquire.view.tablet.emailsList',
        'mobEdu.enquire.view.login',
        'mobEdu.enquire.view.subrMenu',
        'mobEdu.enquire.view.state',
        'mobEdu.enquire.view.appState',
        'mobEdu.enquire.view.schoolState',
        'mobEdu.main.view.contactUS',
        'mobEdu.enroute.leads.view.paging',
        'mobEdu.report.f',
        'mobEdu.housing.view.housingInfo',
        'mobEdu.housing.view.studentInfo',
        'mobEdu.housing.view.housingForm',
        'mobEdu.housing.f',
        'mobEdu.report.view.report',
        'mobEdu.report.view.reporterDetails',
        'mobEdu.report.view.viewReports',
        'mobEdu.report.view.reportDetails',
        'mobEdu.acc.view.billingCounselorInfo',
        'mobEdu.transcripts.view.transcripts',
        'mobEdu.transcripts.f',
        'mobEdu.crl.view.allHoldsInfo',
        'mobEdu.main.view.subMenu',
        'mobEdu.counselor.view.cnslrDetails'
    ],
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },
    isIconPrecomposed: true,
    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },
    launch: function() {

        // All orientation changes should be executed here
        function orientationChangeHandler(orientation) {
                // mobEdu.main.f.displaySignUpForm();
                // Redraw the main menu grid when orientation changes
                if (mainMenuLayout === "GRID") {
                    mobEdu.main.f.displayGridMenu();
                } else if (mainMenuLayout === "BGRID") {
                    if (showPremenu != null && showPremenu != '') {
                        mobEdu.main.f.loadPreMenu();
                    }
                    mobEdu.main.f.updateBevelGridMenu();


                } else if (mainMenuLayout === "SGRID") {
                    mobEdu.main.f.displaySMenu();
                }

                if (Ext.os.version.major >= 7) {
                    mobEdu.util.adjustViewportIfIOS7();
                }
            }
            // window.addEventListener("resize", orientationChangeHandler, false);
        window.addEventListener("orientationchange", orientationChangeHandler, false);

        //To fix the bug in sencha while hiding messagebox
        Ext.Msg.defaultAllowedConfig.showAnimation = false;

        if (isSessionTimeoutEnabled == 'true') {
            if (mobEdu.util.getAuthString() != null && mobEdu.util.getStudentId() != null) {
                mobEdu.util.loadTask();
                task = Ext.create('Ext.util.DelayedTask', function() {
                    mobEdu.util.loadTask();
                }, this);
            }
        }

        // invokes before each ajax request
        Ext.Ajax.on('beforerequest', function(ref, reqObj) {
            if (isSessionTimeoutEnabled == 'true') {
                //Session Expiring if more than 5mins as idle - without firing any request
                if (mobEdu.util.getAuthString() != null && mobEdu.util.getStudentId() != null && task != null) {
                    var url = reqObj.url;
                    if (url != null) {
                        if (url.indexOf("notificationList") == -1 && url.indexOf("getConfig") == -1 && url.indexOf("getMainModules") == -1) {
                            mobEdu.util.loadTask();
                            mobEdu.util.masked = true;
                        }
                    }
                }
            }
            // showing the loading mask
            Ext.Viewport.setMasked({
                xtype: 'loadmask',
                zIndex: 10000,
                message: 'Please wait...'
            });
        });

        // invokes after request completed
        Ext.Ajax.on('requestcomplete', function(conn, response) {
            mobEdu.util.masked = false;
            var url = response.request.options.url;
            if (url.indexOf("notificationList") > -1) {
                return;
            }
            // hiding the loading mask
            Ext.Viewport.unmask();

        });

        // invokes if exception occured
        Ext.Ajax.on('requestexception', function(conn, response, options, eOpts) {
            mobEdu.util.masked = false;

            Ext.Viewport.unmask();

            // Do not show error for notification list
            var url = response.request.options.url;
            if (url.indexOf("notificationList") > -1) {
                return;
            }

            var labelName = loginLabel + ' or ' + passwordLabel;
            var userName = 'Username or ' + passwordLabel;
            var message = passwordLabel != '' ? 'Invalid ' + (loginLabel != '' ? labelName : userName) : 'Invalid Username or Password';
            if (response.status == 401) {
                Ext.Msg.show({
                    message: message,
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else if (Ext.device.Connection.isOnline() === false) {
                Ext.Msg.show({
                    message: 'No Internet Connection<br> Please try again',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            } else {
                Ext.Msg.show({
                    message: 'Server error has occurred.<br> Please try again',
                    buttons: Ext.MessageBox.OK,
                    cls: 'msgbox'
                });
            }

            delete options.failure;
            delete options.callback;
        });

        // Initialize the main view
        //Ext.Viewport.add(Ext.create('mobEdu.view.Main'));
        Ext.Viewport.setLayout({
            type: 'card'
        });

        if (primaryModule == 'campus') {
            // Campus selection is the start screen
            if (mobEdu.util.getResourcePath() == resourcesPath || mobEdu.util.getResourcePath() == null) {
                // No campus selected, show the campus selection screen
                mobEdu.main.f.showCampus();
            } else {
                // Campus is already selected; update that campus's theme and show the main menu
                mobEdu.util.updateTheme();

                // Do loadViews with a delay
                // This is a problem when we force close the app on android and try to open it again
                // In that case, the main menu is being loaded before the themes are updated
                // Hence loading the views with a delay of 2 sec
                task = Ext.create('Ext.util.DelayedTask', mobEdu.main.f.loadViews);
                task.delay(2000);
            }
        } else if (primaryModule == 'login') {
            if (mobEdu.util.getAuthString() == null && mobEdu.util.getStudentId() == null) {
                mobEdu.main.f.showLogin();
            } else {
                mobEdu.main.f.loadViews();
            }
        } else if (primaryModule == 'main') {
            mobEdu.main.f.loadViews();
        }

        document.addEventListener("deviceready", onDeviceReady, false);

        // result contains any message sent from the plugin call

        function successHandler(result) {}
            // result contains any error description text returned from the plugin call

        function errorHandler(error) {}

        function tokenHandler(result) {
            // Your iOS push server needs to know the token before it can push to this device
            // here is where you might want to send it the token for later use.
            //            alert('device token = '+result)
            deviceToken = result;
        }

        function onDeviceReady() {
            // Register the event listener
            document.addEventListener("backbutton", onBackKeyDown, false);
            document.addEventListener("menubutton", onMenuKeyDown, false);
            if (Ext.os.is.ios) {

                pushNotification = window.plugins.pushNotification;
                pushNotification.register(tokenHandler, errorHandler, {
                    "badge": "true",
                    "sound": "true",
                    "alert": "true",
                    "ecb": "onNotificationAPN"
                });

                // Decrese the vieport height by 20px is using ios7
                mobEdu.util.adjustViewportIfIOS7();

                // Add one time style to the body if using ios 7
                if (Ext.os.version.major >= 7) {
                    document.body.style.marginTop = "20px";
                }
            }

            if (Ext.os.is.Android || Ext.os.is.iOS) {
                mobEdu.util.analyticsPlugin = window.plugins.analytics;

                // Hit a page view that the app has been opened.
                mobEdu.util.gaTrackPageView("/index.html");
            }
        }

        // Handle the back button

        function onBackKeyDown() {
            // Call your back key code here and navigate to wherever you want to go
            // to
            mobEdu.util.hideKeyboard();
            if (mobEdu.util.getPrevView() == null) {
                device.exitApp();
            } else {
                (mobEdu.util.getPrevView())();
            }
        }

        // Handle the menu button

        function onMenuKeyDown() {
            mobEdu.util.hideKeyboard();
        }

        // Hide the loading mask initially. We will show it while loading the views
        Ext.select('#loading-mask').hide();

        mobEdu.util.createCustomToolbar();
    },
    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            });
    }
});