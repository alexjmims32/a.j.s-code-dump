package com.n2n.entourage.CLIENTNAME;

import org.apache.cordova.DroidGap;

import android.webkit.WebView;

public class CustomNativeAccess {

	private WebView mAppView;
	private DroidGap mGap;

	public CustomNativeAccess(DroidGap gap, WebView view) {
		mAppView = view;
		mGap = gap;
                mAppView.getSettings().setSavePassword(false);
	}

	public String getGCMRegisterId() {
		return ServerUtilities.regId;
	}

    public static int getNewNotifCount(){
    	return GCMIntentService.getNewNotifCount();
    }

    public static void resetNotifCount(){
    	GCMIntentService.resetNotifCount();
    }

   public static boolean isNotifJobEnabled(){
    	return GCMIntentService.isNotifJobEnabled();
    }

    public static void setNotifJobEnabled(){
    	GCMIntentService.setNotifJobEnabled();
    }	
}
