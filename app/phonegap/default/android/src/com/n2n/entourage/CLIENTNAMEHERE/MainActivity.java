/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.n2n.entourage.CLIENTNAME;

import org.apache.cordova.Config;
import org.apache.cordova.DroidGap;
import org.apache.http.cookie.Cookie;
import android.util.DisplayMetrics;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.Display;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.CookieManager;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends DroidGap {

	CustomNativeAccess cna;

	@Override
	public void onCreate(Bundle savedInstanceState) {
			EasyTracker.getInstance().activityStart(this);

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			final String regId = GCMRegistrar.getRegistrationId(this);
			if (regId.equals("")) {
				GCMRegistrar.register(this, CommonUtilities.SENDER_ID);
			}
			
			super.onCreate(savedInstanceState);
            if(isTablet()==false) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
			super.init();

			// Enable cookies
			CookieManager.getInstance().setAcceptCookie(true);

			Log.i(TAG, "Creating cna with " + regId);
			cna = new CustomNativeAccess(this, appView); 
			appView.addJavascriptInterface(cna, "CustomNativeAccess");       

			super.setIntegerProperty("splashscreen", R.drawable.splash);
			if(isNetworkConnected()){	
				// Set by <content src="index.html" /> in config.xml
				super.setIntegerProperty("loadUrlTimeoutValue", 10000000);
				super.loadUrl(Config.getStartUrl(), 10000000);
			}else{
				Context context = this;
				AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(context);
				dlgAlert.setMessage("No Internet Connectivity");
				dlgAlert.setPositiveButton("Ok",
					    new DialogInterface.OnClickListener() {
					        public void onClick(DialogInterface dialog, int which) {
					        	MainActivity.this.finish();
					        	
					        }
					    });
				// create alert dialog
				AlertDialog alertDialog = dlgAlert.create();	
				// show it
				alertDialog.show();
				super.onDestroy();
				
			}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options, menu);
		return true;
	}

    public boolean onOptionsItemSelected(MenuItem item) {
      // respond to menu item selection
      super.loadUrl(Config.getStartUrl());
      return true;
    }
	
	private boolean isNetworkConnected() {
	  ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	  NetworkInfo ni = cm.getActiveNetworkInfo();
	  if (ni == null) {
	   // There are no active networks.
	   return false;
	  } else
	   return true;
	 }

	@Override
	public void onDestroy() {
		super.onDestroy();
		EasyTracker.getInstance().dispatch();
		EasyTracker.getInstance().activityStop(this);
	}
    private boolean isTablet()
    {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        
        int width = displayMetrics.widthPixels / displayMetrics.densityDpi;
        int height = displayMetrics.heightPixels / displayMetrics.densityDpi;
        
        double screenDiagonal = Math.sqrt( width * width + height * height );
        return (screenDiagonal >= 7.0 );
    }
}

