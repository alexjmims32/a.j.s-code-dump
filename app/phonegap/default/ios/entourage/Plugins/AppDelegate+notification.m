//
//  AppDelegate+notification.m
//  pushtest
//
//  Created by Robert Easterday on 10/26/12.
//
//

#import "AppDelegate+notification.h"
#import "PushPlugin.h"
#import <objc/runtime.h>

static char launchNotificationKey;

@implementation AppDelegate (notification)

- (id) getCommandInstance:(NSString*)className
{
	return [self.viewController getCommandInstance:className];
}

// its dangerous to override a method from within a category.
// Instead we will use method swizzling. we set this up in the load call.
+ (void)load
{
    Method original, swizzled;

    original = class_getInstanceMethod(self, @selector(init));
    swizzled = class_getInstanceMethod(self, @selector(swizzled_init));
    method_exchangeImplementations(original, swizzled);
}

- (AppDelegate *)swizzled_init
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createNotificationChecker:)
               name:@"UIApplicationDidFinishLaunchingNotification" object:nil];

	// This actually calls the original init method over in AppDelegate. Equivilent to calling super
	// on an overrided method, this is not recursive, although it appears that way. neat huh?
	return [self swizzled_init];
}

// This code will be called immediately after application:didFinishLaunchingWithOptions:. We need
// to process notifications in cold-start situations
- (void)createNotificationChecker:(NSNotification *)notification
{
	if (notification)
	{
		NSDictionary *launchOptions = [notification userInfo];
		if (launchOptions)
			self.launchNotification = [launchOptions objectForKey: @"UIApplicationLaunchOptionsRemoteNotificationKey"];
	}
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

        PushPlugin *pushHandler = [self getCommandInstance:@"PushPlugin"];
        [pushHandler didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];

        NSMutableString *tokenString = [NSMutableString stringWithString:
                                        [[deviceToken description] mutableCopy]];
        [tokenString replaceOccurrencesOfString:@"<"
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, tokenString.length)];
        [tokenString replaceOccurrencesOfString:@">"
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, tokenString.length)];
        [tokenString replaceOccurrencesOfString:@" "
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, tokenString.length)];

        // Create the NSURL for the request
        NSString *urlFormat = @"WEBSERVERregisterDevice?regId=%@&platform=IOS";
        NSURL *registrationURL = [NSURL URLWithString:[NSString stringWithFormat:
                                                       urlFormat, tokenString]];

        // Create the registration request
        NSMutableURLRequest *registrationRequest = [[NSMutableURLRequest alloc]
                                                    initWithURL:registrationURL];
        [registrationRequest setHTTPMethod:@"POST"];
        NSString *companyId=@"COMPANYID";
        [registrationRequest addValue: companyId forHTTPHeaderField:@"companyId"];
        NSLog(@"%@", registrationRequest.allHTTPHeaderFields);

        // And fire it off
        NSURLConnection *connection = [NSURLConnection connectionWithRequest:registrationRequest
                                                                    delegate:self];
        [connection start];
        //
        //
        // Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.

        NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];

        // Set the defaults to disabled unless we find otherwise...

        NSString *pushBadge = @"disabled";

        NSString *pushAlert = @"disabled";

        NSString *pushSound = @"disabled";

        if(rntypes == UIRemoteNotificationTypeBadge){

            pushBadge = @"enabled";

        }

        else if(rntypes == UIRemoteNotificationTypeAlert){

            pushAlert = @"enabled";

        }

        else if(rntypes == UIRemoteNotificationTypeSound){

            pushSound = @"enabled";

        }

        else if(rntypes == ( UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert)){

            pushBadge = @"enabled";

            pushAlert = @"enabled";

        }

        else if(rntypes == ( UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)){

            pushBadge = @"enabled";

            pushSound = @"enabled";

        }

        else if(rntypes == ( UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)){

            pushAlert = @"enabled";

            pushSound = @"enabled";

        }

        else if(rntypes == ( UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)){

            pushBadge = @"enabled";

            pushAlert = @"enabled";

            pushSound = @"enabled";
        }

}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    PushPlugin *pushHandler = [self getCommandInstance:@"PushPlugin"];
    [pushHandler didFailToRegisterForRemoteNotificationsWithError:error];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"didReceiveNotification");

    // Get application state for iOS4.x+ devices, otherwise assume active
    UIApplicationState appState = UIApplicationStateActive;
    if ([application respondsToSelector:@selector(applicationState)]) {
        appState = application.applicationState;
    }

    if (appState == UIApplicationStateActive) {
        PushPlugin *pushHandler = [self getCommandInstance:@"PushPlugin"];
        pushHandler.notificationMessage = userInfo;
        pushHandler.isInline = YES;
        [pushHandler notificationReceived];
    } else {
        //save it for later
        self.launchNotification = userInfo;
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {

    NSLog(@"active");

    //zero badge
    application.applicationIconBadgeNumber = 0;

    if (![self.viewController.webView isLoading] && self.launchNotification) {
        PushPlugin *pushHandler = [self getCommandInstance:@"PushPlugin"];

        pushHandler.notificationMessage = self.launchNotification;
        self.launchNotification = nil;
        [pushHandler performSelectorOnMainThread:@selector(notificationReceived) withObject:pushHandler waitUntilDone:NO];
    }
}

// The accessors use an Associative Reference since you can't define a iVar in a category
// http://developer.apple.com/library/ios/#documentation/cocoa/conceptual/objectivec/Chapters/ocAssociativeReferences.html
- (NSMutableArray *)launchNotification
{
   return objc_getAssociatedObject(self, &launchNotificationKey);
}

- (void)setLaunchNotification:(NSDictionary *)aDictionary
{
    objc_setAssociatedObject(self, &launchNotificationKey, aDictionary, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)dealloc
{
    self.launchNotification	= nil; // clear the association and release the object
    [super dealloc];
}

@end
