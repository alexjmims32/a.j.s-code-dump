//
//  GoogleAnalyticsPlugin.m
//  Google Analytics plugin for PhoneGap
//
//  Created by Jesse MacFadyen on 11-04-21.
//  Updated to 1.x by Olivier Louvignes on 11-11-24.
//  Updated to 1.5 by Chris Kihneman on 12-04-09.
//  MIT Licensed
//

#import "GoogleAnalyticsPlugin.h"
#import "GAI.h"

@implementation GoogleAnalyticsPlugin

- (void) trackEvent:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
{

	NSString* category = [options valueForKey:@"category"];
	NSString* action = [options valueForKey:@"action"];
	NSString* label = [options valueForKey:@"label"];
    
//  TODO - Convert value param into proper NSNumber object. Will use 0 for now
//	int value = [[options valueForKey:@"value"] intValue];

    if (![[GAI sharedInstance].defaultTracker sendEventWithCategory:category withAction:action withLabel:label withValue: 0]) {
        NSLog(@"GA: Error sending event: %@-%@", category, action);
    }

}

- (void) trackPageView:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options
{
	NSString* pageUri = [arguments objectAtIndex:0];

    if(![[GAI sharedInstance].defaultTracker sendView:pageUri]){
        NSLog(@"GA: Error sending page view: %@", pageUri);
    }
}
@end
