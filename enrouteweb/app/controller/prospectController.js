Ext.define('rcrm.controller.prospectController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'prospectListStore',
    'statusStore',
    'recruiterStore',
    'prospectViewStore',
    'prospectUpdateStore',
    'leadStore',
    'prospMsgSummaryStore',
    'prospCallSummaryStore',
    'prospEmailSummaryStore',
    'prospLetterSummaryStore'
    ],
    models: [],

    views: [
    'prospectListView',
    'prospectListGridView',
    'prospectView',
    'updateProspectView',
    'addLeadView',
    'importLeadView'
    ],

    init: function() {
        controller=this;
        this.control({
            
            'prospectListGridView':{
                itemdblclick:this.loadUpdateProspectView
            },
            'prospectListView button[action=search]': {
                click: this.prospectSearch
            },
            'prospectListView button[action=reset]': {
                click: this.resetProspectList
            },
            'updateProspectView button[action=save]': {
                click: this.updateProspect
            },
            'updateProspectView button[action=cancel]': {
                click: this.loadProspectList
            },
            'addLeadView button[action=add]': {
                click: this.addLead
            },
            'addLeadView button[action=reset]': {
                click: this.resetAddLead
            },
            'prospectView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'prospectList'){
                            var list = Ext.getCmp('prospectList');
                            list.setVisible(true);
                        }else if(evt.currentTarget.name == 'addLead'){
                            var addLead = Ext.getCmp('addLead');
                            addLead.setVisible(true);
                        }else if(evt.currentTarget.name == 'importLead'){
                            var importLead=Ext.widget('importLeadView');
                            importLead.show();
                        }
                        
                    });
                }
            }
        });
    },
    
    prospectSearch: function(){
        var prospectId=Ext.getCmp('prospsId').getValue();
        var recruiterId=Ext.getCmp('prospRecruiter').getValue();
        if(recruiterId == null)
            recruiterId='';
        var startDate=Ext.getCmp('prospsDate').getValue();
        if(startDate == null)
            startDate='';
        else
            startDate=Ext.Date.format(startDate, 'm/d/Y');
        var endDate=Ext.getCmp('prospeDate').getValue();
        if(endDate == null)
            endDate='';
        else
            endDate=Ext.Date.format(endDate, 'm/d/Y');
        var eduGoal='';
        var status=Ext.getCmp('prospStatus').getValue();
        if(status == null)
            status='';
        var fName=Ext.getCmp('prospfName').getValue();
        var lName=Ext.getCmp('prosplName').getValue();
    //        var store=Ext.getStore('prospectListStore');
    //        store.getProxy().url = baseUrl +'/prospect?operation=list&prospectId='+prospectId+'&recruiterId='+recruiterId+'&startDate='+startDate+'&endDate='+endDate+'&eduGoal='+eduGoal+'&status='+status+'&firstName='+fName+'&lastName='+lName;
    //        store.getProxy().afterRequest = function(){
    //            //            console.log('email Summary loaded');
    //            console.log(store);
    //        }
    //        store.load();
    },

    resetProspectList: function(){
        Ext.getCmp('prospsId').reset();
        Ext.getCmp('prospRecruiter').reset();
        Ext.getCmp('prospsDate').reset();
        Ext.getCmp('prospeDate').reset();
        Ext.getCmp('prospStatus').reset();
        Ext.getCmp('prospfName').reset();
        Ext.getCmp('prosplName').reset();
    },
    
    loadUpdateProspectView: function(grid, record){
        console.log('double clicked'+record);  
        var store=Ext.getStore('prospectViewStore');
        
        //        store.getProxy().url = baseUrl +'/prospect?operation=view&prospectId='+record.data.prospectId;
        //        store.getProxy().afterRequest = function(){
        //            console.log(store);
        var prospect=store.getRange(0);
        var data=prospect[0].data;
        Ext.getCmp('updateProspName').setText(data.firstName + ' ' +data.lastName + ' ( Id: '+data.prospectId +' )');
        Ext.getCmp('upProspectId').setValue(data.prospectId);
        Ext.getCmp('upfName').setValue(data.firstName);
        Ext.getCmp('uplName').setValue(data.lastName);
        Ext.getCmp('upPhoneNumber').setValue(data.homeNumber);
        Ext.getCmp('upEmail').setValue(data.email);
        Ext.getCmp('upStatus').setValue(data.status);
        Ext.getCmp('upRecruiterId').setValue(data.recruiterId);
        Ext.getCmp('upRecruiterName').setValue(data.recruiterName);
        
        var list = Ext.getCmp('prospectList');
        list.setVisible(false);
        var view = Ext.getCmp('prospectView');
        view.setVisible(true);
    //        }
    //        store.load();
    },   
    
    loadUpdateProspectViewResponse:function(){
        var store=Ext.getStore('prospectViewStore');
        var prospect=store.getRange(0);
        var data=prospect[0].data;
        Ext.getCmp('updateProspName').setText(data.firstName + ' ' +data.lastName + ' ( Id: '+data.prospectId +' )');
        Ext.getCmp('upProspectId').setValue(data.prospectId);
        Ext.getCmp('upfName').setValue(data.firstName);
        Ext.getCmp('uplName').setValue(data.lastName);
        Ext.getCmp('upPhoneNumber').setValue(data.homeNumber);
        Ext.getCmp('upEmail').setValue(data.email);
        Ext.getCmp('upStatus').setValue(data.status);
        Ext.getCmp('upRecruiterId').setValue(data.recruiterId);
        Ext.getCmp('upRecruiterName').setValue(data.recruiterName);
        
        var list = Ext.getCmp('prospectList');
        list.setVisible(false);
        var view = Ext.getCmp('prospectView');
        view.setVisible(true);
    },
    
    loadProspectList: function(){
        var list = Ext.getCmp('prospectList');
        list.setVisible(true);
        var view = Ext.getCmp('prospectView');
        view.setVisible(false);
    },
    updateProspect: function(){
        var prospectId=Ext.getCmp('upProspectId').getValue();
        var status=Ext.getCmp('upStatus').getValue();
        if(status == null)
            status='';
        var recruiterId=Ext.getCmp('upRecruiterId').getValue();
        if(recruiterId == null)
            recruiterId='';        
        //        var store=Ext.getStore('prospectUpdateStore');
        //        store.getProxy().url = baseUrl +'/prospect?operation=update&prospectId='+prospectId+'&status='+status+'&recruiterId='+recruiterId+'&loggedInUser='+loginUserId;
        //        store.getProxy().afterRequest = function(){
        //            console.log(store);
        //        }
        //        store.load();
        var list = Ext.getCmp('prospectList');
        list.setVisible(true);
        var view = Ext.getCmp('prospectView');
        view.setVisible(false);
    },
    
    addLead: function(){
        
        //Basic Info
        var firstName=Ext.getCmp('alfName').getValue();
        var lastName=Ext.getCmp('allName').getValue();
        var middleName=Ext.getCmp('almName').getValue();
        var gender=Ext.getCmp('alGender').getValue();
        if(gender == null)
            gender='';
        var dateofbirth=Ext.getCmp('alDateofBirth').getValue();
        if(dateofbirth == null)
            dateofbirth='';
        else
            dateofbirth=Ext.Date.format(dateofbirth, 'm/d/Y');
        var race=Ext.getCmp('alRace').getValue();
        var ethnnicity=Ext.getCmp('alEthnicity').getValue();
        
        //High School Info
        var hschoolNmae=Ext.getCmp('alhSchoolName').getValue();
        var hCity=Ext.getCmp('alhCity').getValue();
        var hState=Ext.getCmp('alhStateCode').getValue();
        var gpa=Ext.getCmp('alGpa').getValue();
        var lvlCode=Ext.getCmp('alhLvlCode').getValue();
        var degCode=Ext.getCmp('alhDegCode').getValue();
        var gradYear=Ext.getCmp('algradYear').getValue();
        if(gradYear == null)
            gradYear='';
        //Contact Info
        var hNumber=Ext.getCmp('alcnthNumber').getValue();
        var stLine1=Ext.getCmp('alcntStLine1').getValue();
        var stLine2=Ext.getCmp('alcntStLine2').getValue();
        var stLine3=Ext.getCmp('alcntStLine3').getValue();
        var city=Ext.getCmp('alcntCity').getValue();
        var state=Ext.getCmp('alcntStateCode').getValue();
        var county=Ext.getCmp('alcntCounty').getValue();
        var zip=Ext.getCmp('alcntZip').getValue();
        var country=Ext.getCmp('alcntCountry').getValue();
        var phone1=Ext.getCmp('alcntPhone1').getValue();
        var phone2=Ext.getCmp('alcntPhone2').getValue();
        var email=Ext.getCmp('alcntEmail').getValue();
        var vemail=Ext.getCmp('alcntVEmail').getValue();
        
        //Enrollment Info
        var startTerm=Ext.getCmp('alStartTerm').getValue();
        var academicInterest1=Ext.getCmp('alAcademicInterest1').getValue();
        var academicInterest2=Ext.getCmp('alAcademicInterest2').getValue();
        
        //Other Info
        var lvelInterest=Ext.getCmp('alLvelInterest').getValue();
        var importantFactor=Ext.getCmp('alImportantFactor').getValue();
        if(firstName != '' && lastName != '' && email != ''){
            if(email == vemail){
                var lead=Ext.getStore('leadStore');
                lead.add({
                    email:email,
                    gender:gender,
                    birthDate:dateofbirth,
                    ethnicity:ethnnicity,
                    raceCode:race,
                    prelCode:'',
                    name:{
                        firstName:firstName,
                        middleName:middleName,
                        lastName:lastName
                    },
                    address:{
                        houseNumber:hNumber,
                        streetLine1:stLine1,
                        streetLine2:stLine2,
                        streetLine3:stLine3,
                        city:city,
                        state:state,
                        country:country,
                        county:county,
                        zipCode:zip
                    },
                    phones:{
                        phone:[
                        {
                            number:phone1,
                            extension:''
                        },
                        {
                            number:phone2,
                            extension:''
                        }
                        ]
                    },
                    highSchoolInfo:{
                        city:hCity,
                        stateCode:hState,
                        gpa:gpa,
                        levlCode:lvlCode,
                        degcCode:degCode,
                        graduationDate:gradYear
                
                    },
                    enrollmentInfo:{
                        startTerm:startTerm,
                        academicAreaOfInterest:{
                            primary:academicInterest1,
                            secondary:academicInterest2
                        }
                    },
                    otherInfo:{
                        levelInterest:lvelInterest,
                        importantFactor:importantFactor
                    },
                    credentials:{
                        username:'',
                        password:''
                    }
            
                });
                var leadData=lead.getRange();
                var data=Ext.encode(leadData[0].data);
                console.log(data);
                //                var store=Ext.getStore('prospectUpdateStore');
                //                store.getProxy().url = baseUrl +'/prospect?operation=add&lead='+data;
                //                store.getProxy().afterRequest = function(){
                //                    var response=store.getRange();
                //                    if(response.length > 0 && response[0].data.status == 'SUCCESS'){
                Ext.getCmp('alfName').reset();
                Ext.getCmp('allName').reset();
                Ext.getCmp('almName').reset();
                Ext.getCmp('alGender').reset();
                Ext.getCmp('alDateofBirth').reset();
                Ext.getCmp('alRace').reset();
                Ext.getCmp('alEthnicity').reset();
        
                Ext.getCmp('alhSchoolName').reset();
                Ext.getCmp('alhCity').reset();
                Ext.getCmp('alhStateCode').reset();
                Ext.getCmp('alGpa').reset();
                Ext.getCmp('alhLvlCode').reset();
                Ext.getCmp('alhDegCode').reset();
                Ext.getCmp('algradYear').reset();
        
                Ext.getCmp('alcnthNumber').reset();
                Ext.getCmp('alcntStLine1').reset();
                Ext.getCmp('alcntStLine2').reset();
                Ext.getCmp('alcntStLine3').reset();
                Ext.getCmp('alcntCity').reset();
                Ext.getCmp('alcntStateCode').reset();
                Ext.getCmp('alcntCounty').reset();
                Ext.getCmp('alcntZip').reset();
                Ext.getCmp('alcntCountry').reset();
                Ext.getCmp('alcntPhone1').reset();
                Ext.getCmp('alcntPhone2').reset();
                Ext.getCmp('alcntEmail').reset();
                Ext.getCmp('alcntVEmail').reset();
        
                Ext.getCmp('alStartTerm').reset();
                Ext.getCmp('alAcademicInterest1').reset();
                Ext.getCmp('alAcademicInterest2').reset();
        
                Ext.getCmp('alLvelInterest').reset();
                Ext.getCmp('alImportantFactor').reset();
            //                    }
            //                }
            //                store.load();
            }else{
                Ext.Msg.alert('Alert','Email and Verify email must be same');
            }
        }else{
            Ext.Msg.alert('Alert','First Name, Last Name and Email are mandatory');
        }
    },
    
    resetAddLead: function(){
        Ext.getCmp('alfName').reset();
        Ext.getCmp('allName').reset();
        Ext.getCmp('almName').reset();
        Ext.getCmp('alGender').reset();
        Ext.getCmp('alDateofBirth').reset();
        Ext.getCmp('alRace').reset();
        Ext.getCmp('alEthnicity').reset();
        
        Ext.getCmp('alhSchoolName').reset();
        Ext.getCmp('alhCity').reset();
        Ext.getCmp('alhStateCode').reset();
        Ext.getCmp('alGpa').reset();
        Ext.getCmp('alhLvlCode').reset();
        Ext.getCmp('alhDegCode').reset();
        Ext.getCmp('algradYear').reset();
        
        Ext.getCmp('alcnthNumber').reset();
        Ext.getCmp('alcntStLine1').reset();
        Ext.getCmp('alcntStLine2').reset();
        Ext.getCmp('alcntStLine3').reset();
        Ext.getCmp('alcntCity').reset();
        Ext.getCmp('alcntStateCode').reset();
        Ext.getCmp('alcntCounty').reset();
        Ext.getCmp('alcntZip').reset();
        Ext.getCmp('alcntCountry').reset();
        Ext.getCmp('alcntPhone1').reset();
        Ext.getCmp('alcntPhone2').reset();
        Ext.getCmp('alcntEmail').reset();
        Ext.getCmp('alcntVEmail').reset();
        
        Ext.getCmp('alStartTerm').reset();
        Ext.getCmp('alAcademicInterest1').reset();
        Ext.getCmp('alAcademicInterest2').reset();
        
        Ext.getCmp('alLvelInterest').reset();
        Ext.getCmp('alImportantFactor').reset();
    }
    
}); 