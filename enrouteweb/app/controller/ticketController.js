Ext.define('rcrm.controller.ticketController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    ],
    models: [],

    views: [
    'ticketListView',
    'ticketListGridView',
    'ticketView'
    ],

    init: function() {
        controller=this;
        this.control({
            

        });
    }
    
    
}); 