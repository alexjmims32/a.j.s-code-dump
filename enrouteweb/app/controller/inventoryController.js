Ext.define('rcrm.controller.inventoryController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'inventoryListStore',
    'inventoryStore',
    'inventoryEventStore',
    'inventoryViewStore'
    ],
    models: [],

    views: [
    'inventoryListView',
    'inventoryListGridView',
    'inventoryView',
    'addInventoryView',
    'updateInventoryView'
    ],

    init: function() {
        this.control({
            'inventoryListView button[action=search]': {
                click: this.loadInventoryList
            },
            'inventoryListGridView':{
                itemdblclick:this.loadUpdateInventoryView
            },
            'updateInventoryView button[action=save]': {
                click: this.updateInventory
            },
            'updateInventoryView button[action=cancel]': {
                click: this.loadInventoryList
            },
            'addInventoryView button[action=save]': {
                click: this.addInventory
            },
            'addInventoryView button[action=reset]': {
                click: this.resetAddInventory
            },
            'inventoryView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'inventoryList'){
                            var inventoryList = Ext.getCmp('inventoryList');
                            inventoryList.setVisible(true);
                        }else if(evt.currentTarget.name == 'addInventory'){
                            var addInventory = Ext.getCmp('addInventory');
                            addInventory.setVisible(true);
                        }
                        
                    });
                }
            }
        });
    },
    
    loadInventoryList: function(){
        var startDate=Ext.getCmp('invStartDate').getValue();
        if(startDate == null)
            startDate='';
        else
            startDate=Ext.Date.format(startDate, 'm/d/Y');
        var endDate=Ext.getCmp('invEndDate').getValue();
        if(endDate == null)
            endDate='';
        else
            endDate=Ext.Date.format(endDate, 'm/d/Y');
        var title=Ext.getCmp('invTitle').getValue();
        var status=Ext.getCmp('invStatus').getValue();
        if(status == null)
            status='';
        var inventory=Ext.getStore('inventoryStore');
        inventory.removeAll();
        inventory.add({
            inventoryTitle:title,
            status:status
        });
        var inventoryData=inventory.getRange();
        var data=Ext.encode(inventoryData[0].data);
//        var invStore=Ext.getStore('inventoryListStore');
//        invStore.getProxy().url = baseUrl +'/inventory?operation=list&startDate='+startDate+'&endDate='+endDate+'&inventory='+data;
//        invStore.getProxy().afterRequest = function(){
//            console.log('inventory list was loaded');
//        //            console.log(store);
//        }
//        invStore.load();
        var list = Ext.getCmp('inventoryList');
        list.setVisible(true);
        var view = Ext.getCmp('inventoryView');
        view.setVisible(false);
    },
    
    loadUpdateInventoryView:function(grid, record){
        
        var inventory=Ext.getStore('inventoryStore');
        inventory.removeAll();
        inventory.add({
            inventoryId:record.data.inventoryId
        });
        var inventoryData=inventory.getRange();
        var data=Ext.encode(inventoryData[0].data);
        console.log(data);
        var invStore=Ext.getStore('inventoryViewStore');
//        invStore.getProxy().url = baseUrl +'/inventory?operation=view&inventory='+data;
//        invStore.getProxy().afterRequest = function(){
//            console.log('inventory View was loaded');
//            console.log(invStore);
            var response=invStore.getRange();
            
            Ext.getCmp('ivnUpTitle').setText(response[0].data.inventoryTitle + ' (Inventory Id: '+response[0].data.inventoryId +' )');
            Ext.getCmp('invUpStatus').setValue(response[0].data.status);
            Ext.getCmp('invUpDesc').setValue(response[0].data.description);
            Ext.getCmp('invUpId').setValue(response[0].data.inventoryId);
            Ext.getCmp('invUpRemarks').setValue(response[0].data.remarks);
            Ext.getCmp('invUpType').setValue(response[0].data.inventoryType);
            Ext.getCmp('invUpAvailabel').setValue(response[0].data.count);
            
            var list = Ext.getCmp('inventoryList');
            list.setVisible(false);
            var view = Ext.getCmp('inventoryView');
            view.setVisible(true);
//        }
//        invStore.load();
        
    },
    
    updateInventory: function(){
        var invId=Ext.getCmp('invUpId').getValue();
        var status=Ext.getCmp('invUpStatus').getValue();
        var desc=Ext.getCmp('invUpDesc').getValue();
        var remarks=Ext.getCmp('invUpRemarks').getValue();
        var type=Ext.getCmp('invUpType').getValue();
        var count=Ext.getCmp('invUpAvailabel').getValue();
        var inventory=Ext.getStore('inventoryStore');
        inventory.removeAll();
        inventory.add({
            inventoryId:invId,
            description:desc,
            inventoryType:type,
            status:status,
            count:count,
            remarks:remarks
        });
        var inventoryData=inventory.getRange();
        var data=Ext.encode(inventoryData[0].data);
//        var uStore=Ext.getStore('updateStore');
//        uStore.getProxy().url = baseUrl +'/inventory?operation=update&loggedInUser='+loginUserId+'&inventory='+data;
//        uStore.getProxy().afterRequest = function(){
//            console.log('inventory is saved');
//        //            console.log(store);
//        }
//        uStore.load();
        var list = Ext.getCmp('inventoryList');
        list.setVisible(true);
        var view = Ext.getCmp('inventoryView');
        view.setVisible(false);
    },
    
    addInventory: function(){
        var title=Ext.getCmp('addInvItemName').getValue();
        var status=Ext.getCmp('addInvStatus').getValue();
        var desc=Ext.getCmp('addInvDesc').getValue();
        var type=Ext.getCmp('addInvType').getValue();
        var count=Ext.getCmp('addInvQuantity').getValue();
        var inventory=Ext.getStore('inventoryStore');
        inventory.removeAll();
        inventory.add({
            inventoryTitle:title,
            description:desc,
            inventoryType:type,
            status:status,
            count:count
        });
        var inventoryData=inventory.getRange();
        var data=Ext.encode(inventoryData[0].data);
//        var uStore=Ext.getStore('updateStore');
//        uStore.getProxy().url = baseUrl +'/inventory?operation=add&loggedInUser='+loginUserId+'&inventory='+data;
//        uStore.getProxy().afterRequest = function(){
//            console.log('inventory is saved');
            //            console.log(store);
            Ext.getCmp('addInvItemName').reset();
            Ext.getCmp('addInvStatus').reset();
            Ext.getCmp('addInvDesc').reset();
            Ext.getCmp('addInvType').reset();
            Ext.getCmp('addInvQuantity').reset();
//        }
//        uStore.load();
    },
    
    resetAddInventory:function(){
        Ext.getCmp('addInvItemName').reset();
        Ext.getCmp('addInvStatus').reset();
        Ext.getCmp('addInvDesc').reset();
        Ext.getCmp('addInvType').reset();
        Ext.getCmp('addInvQuantity').reset();        
    }
    
}); 