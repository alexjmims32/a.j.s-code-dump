Ext.define('rcrm.controller.letterController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'letterListStore'
    ],
    models: [
    'letterListModel'
    ],

    views: [
    'letterListView',
    'letterListGridView',
    'letterView',
    'updateLetterView',
    ],

    init: function() {
        this.control({
            'letterListGridView':{
                itemdblclick:this.loadUpdateLetterView
            },
            'letterListView button[action=search]': {
                click: this.letterSearch
            },
            'updateLetterView button[action=save]': {
                click: this.updateLetter
            },
            'updateLetterView button[action=cancel]': {
                click: this.loadLetterList
            },
            'letterView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'letterList'){
                            var list = Ext.getCmp('letterList');
                            list.setVisible(true);
                        }                        
                    });
                }
            }
        });
    },

    letterSearch: function(){
        var msgToDate=Ext.getCmp('ltreDate').getValue();
        if(msgToDate == null)
            msgToDate='';
        else
            msgToDate=Ext.Date.format(msgToDate, 'm/d/Y');
        var msgFromDate=Ext.getCmp('ltrsDate').getValue();
        if(msgFromDate == null)
            msgFromDate='';
        else
            msgFromDate=Ext.Date.format(msgFromDate, 'm/d/Y');
        var printToDate=Ext.getCmp('ltrptDate').getValue();
        if(printToDate== null)
            printToDate='';
        else
            printToDate=Ext.Date.format(printToDate, 'm/d/Y');
        var printFromDate=Ext.getCmp('ltrpfDate').getValue();
        if(printFromDate == null)
            printFromDate='';
        else
            printFromDate=Ext.Date.format(printFromDate, 'm/d/Y');
        var studentId=Ext.getCmp('ltrstudentId').getValue();
        var fName=Ext.getCmp('ltrfName').getValue();
        var lName=Ext.getCmp('ltrlName').getValue();
        var status=Ext.getCmp('ltrStatus').getValue();
        if(status == null)
            status='';
        var ltrType='';
        var store=Ext.getStore('letterListStore');
        store.getProxy().url = baseUrl +'/letter?operation=list&messageToDate='+msgToDate+'&messageFromDate='+msgFromDate+'&printToDate='+printToDate+'&printFromDate='+printFromDate+'&studentId='+studentId+'&firstName='+fName+'&lastName='+lName+'&status='+status+'&letterType='+ltrType;
        store.getProxy().afterRequest = function(){
            console.log('Letter List loaded');
            console.log(store);
        }
        store.load();
    },
    
    loadUpdateLetterView: function(grid, record){
        console.log('double clicked'+record);  
        
        Ext.getCmp('ltrvTitle').setText(record.data.firstName +' '+record.data.lastName +' (ID: '+record.data.studentId+' )');
        Ext.getCmp('ltrvCategory').setValue(record.data.messageCategory);
        Ext.getCmp('ltrvAddedDate').setValue(record.data.messageDate);
        Ext.getCmp('ltrvStatus').setValue(record.data.status);
        Ext.getCmp('ltrvpDate').setValue(record.data.printDate);
        Ext.getCmp('ltrvMessage').setValue(record.data.message);
        
        var list = Ext.getCmp('letterList');
        list.setVisible(false);
        var view = Ext.getCmp('letterView');
        view.setVisible(true);
    },
    
    loadLetterList: function(){
        var list = Ext.getCmp('letterList');
        list.setVisible(true);
        var view = Ext.getCmp('letterView');
        view.setVisible(false);
    }
    
}); 