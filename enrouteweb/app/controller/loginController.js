Ext.define('rcrm.controller.loginController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'loginStore',
    'emailSummaryStore',
    'enumerationListStore',
    'messageStatusStore',
    'callReasonStore',
    'callStatusStore',
    'eduGoalStore',
    'emailStatusStore',
    'eventStatusStore',
    'inventoryTypeStore',
    'letterStatusStore',
    'letterTypeStore',
    'messageStatusStore',
    'prospectStatusStore',
    'templateCategoryStore',
    'userActivityDropdownStore',
    'userListStore'
    ],
    
    models: [
    'loginModel',
    'privilegeListModel',
    'privilegeModel',
    'enumerationListModel',
    'dropDownModel',
    'userListModel'
    ],

    views: [
    'loginView',
    'mainView',
    'dashboardView',
    'emailSummaryView',
    'welcomeView',
    ],

    init: function() {
        this.control({
            'loginView button[action=signin]': {
                click: this.loginSubmit
            }
        });
    },

    onPanelRendered: function() {
        console.log('The panel was rendered');
    },
    
    loginSubmit: function(){
        var username = Ext.getCmp('username').getValue();
        var password=Ext.getCmp('password').getValue();
        if(username != '' && password != ''){
            var welcomeView = Ext.widget('welcomeView');
            var menu=Ext.getCmp('mainView');
            if(username == 'crmdir' && password == 'crmdir'){
                menu.add({
                    title: 'Dashboard',  
                    id:'dashboardTab',
                    xtype:'dashboardView'
                });
                menu.add({
                    title: 'Tickets',  
                    xtype:'ticketView'
                });
                menu.add({
                    title: 'Calls',        
                    xtype:'callView'
                });
                menu.add({
                    title: 'Letters',        
                    xtype:'letterView'
                });
                menu.add({
                    title: 'Verification',        
                    xtype:'verificationView'
                });
                menu.add({
                    title: 'Applications',        
                    xtype:'applicationView'
                });
                menu.add({
                    title: 'Documents',        
                    xtype:'documentView'
                });
                menu.add({
                    title: 'Activity',        
                    xtype:'activityView'
                });
                viewport.add(welcomeView);
                viewport.layout.setActiveItem(welcomeView);
            } else if(username == 'rcmdir' && password == 'rcmdir'){
                menu.add({
                    title: 'Dashboard',  
                    id:'dashboardTab',
                    xtype:'dashboardView'
                });
                menu.add({
                    title: 'Prospects',        
                    xtype:'prospectView'
                });
                menu.add({
                    title: 'Campaigns',        
                    xtype:'campaignView'
                });
                menu.add({
                    title: 'Mesages',        
                    xtype:'messageView'
                });
                menu.add({
                    title: 'Calls',        
                    xtype:'callView'
                });
                menu.add({
                    title: 'Activity',        
                    xtype:'activityView'
                });
                menu.add({
                    title: 'Events',        
                    xtype:'eventView'
                });
                menu.add({
                    title: 'Inventory',        
                    xtype:'inventoryView'
                });
                viewport.add(welcomeView);
                viewport.layout.setActiveItem(welcomeView);
            } else{
                Ext.Msg.alert('Sign In Error','Invalid Username or password');
            }
        }else{
            Ext.Msg.alert('Sign In Error','Enter Username or password');
        }
    }
}); 