Ext.define('rcrm.controller.verificationController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    ],
    models: [],

    views: [
    'creditListView',
    'creditListGridView',
    'verificationView',
    'cbcListView',
    'cbcListGridView',

    ],

    init: function() {
        controller=this;
        this.control({
            'verificationView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'creditHours'){
                            var creditlist = Ext.getCmp('creditList');
                            creditlist.setVisible(true);
                        }else if(evt.currentTarget.name == 'cbcReview'){
                            var cbcList= Ext.getCmp('cbcList');
                            cbcList.setVisible(true);
                        }
                        
                    });
                }
            }
        });
    }
    
}); 