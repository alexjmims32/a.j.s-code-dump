Ext.define('rcrm.controller.eventController', {
    extend: 'Ext.app.Controller',
    alias:'eventController',
    
    stores: [
    'eventStore',
    'eventListStore',
    'eventViewStore',
    'evtInventoryStore',
    'eventInventoryStore',
    'recruiterListStore',
    'attendeeListStore',
    'addressStore',
    'actInvStore',
    'evtrecruiterListStore'
    ],
    models: [],

    views: [
    'eventListView',
    'eventListGridView',
    'eventView',
    'updateEventView',
    'addEventView',
    'addEventInventoryView',
    'addEventRecruiterView',
    'addEventAttendeeView'
    ],
    
     refs:[{
        ref: 'eventListGridView',
        selector: 'CalendarEvent'   
    }],

    init: function() {
        this.control({
            'eventListGridView':{
                itemdblclick:this.loadUpdateEventView
            },
            'eventListView button[action=search]': {
                click: this.loadEventList
            },
            'updateEventView button[action=save]': {
                click: this.updateEvent
            },
            'updateEventView button[action=cancel]': {
                click: this.loadEventList
            },
            'updateEventView button[action=addInv]': {
                click: this.addInventory
            },
            'updateEventView button[action=addRect]': {
                click: this.addRecruiter
            },
            'updateEventView button[action=addAttendee]': {
                click: this.addAttendee
            },
            'updateEventView button[action=save]': {
                click: this.updateEvent
            },
            'addEventView button[action=addInv]': {
                click: this.addInventory
            },
            'addEventView button[action=addRect]': {
                click: this.addRecruiter
            },
            'addEventView button[action=save]': {
                click: this.addEvent
            },
            'addEventView button[action=reset]': {
                click: this.resetAddEventView
            },
            'addEventInventoryView button[action=close]': {
                click: this.closeAddEventInventoryView
            },
            'addEventInventoryView button[action=add]': {
                click: this.addEventInventory
            },
            'addEventRecruiterView button[action=add]': {
                click: this.addEventRecruiter
            },
            'addEventRecruiterView button[action=close]': {
                click: this.closeAddEventRectView
            },
            'addEventAttendeeView button[action=addAttendee]': {
                click: this.addEventAttendeeView
            },
            'addEventAttendeeView button[action=close]': {
                click: this.closeAddEventAttendeeView
            },
            'eventView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'eventList'){
                            var eventlist = Ext.getCmp('eventList');
                            eventlist.setVisible(true);
                        }else if(evt.currentTarget.name == 'addEvent'){
                            var invStore=Ext.getStore('eventInventoryStore');
                            invStore.removeAll();
                            var rectStore=Ext.getStore('recruiterListStore');
                            rectStore.removeAll();
                            var addEvent = Ext.getCmp('addEvent');
                            addEvent.setVisible(true);
                        }
                        
                    });
                }
            }
        });
    },

    
    loadUpdateEventView: function(grid, record){
        
        var event=Ext.getStore('eventStore');
        var address=Ext.getStore('addressStore');
        address.removeAll();
        address.add({
            address1:''
        });
        var inventoryList=Ext.getStore('eventInventoryStore');
        inventoryList.removeAll();
        inventoryList.add({
            inventoryId:''
        });
        var rectList=Ext.getStore('recruiterListStore');
        rectList.removeAll();
        rectList.add({
            recruiterId:''
        });
        event.removeAll();
        event.add({
            eventId:record.data.eventId,
            location:address.getRange()[0].data,
            inventoryList:inventoryList.getRange()[0].data,
            recruiters:rectList.getRange()[0].data
        });
        var eventData=event.getRange();
        var data=Ext.encode(eventData[0].data);
        console.log(data);
        var evtStore=Ext.getStore('eventViewStore');
//        evtStore.getProxy().url = baseUrl +'/event?operation=view&event='+data;
//        evtStore.getProxy().afterRequest = function(){
//            console.log('inventory View was loaded');
//            console.log(evtStore);
            var response=evtStore.getRange();
            
            Ext.getCmp('uevntTitle').setText(response[0].data.eventTitle+' (Event Id: '+response[0].data.eventId+' )');
            Ext.getCmp('uevntId').setValue(response[0].data.eventId);
            Ext.getCmp('uevntStatus').setValue(response[0].data.status);
            Ext.getCmp('uevntStartDate').setValue(response[0].data.eventStartDate);
            Ext.getCmp('uevntEndDate').setValue(response[0].data.eventEndDate);
            Ext.getCmp('uevntCreatedOn').setValue(response[0].data.createdDate);
            Ext.getCmp('uevntCreatedBy').setValue(response[0].data.createdBy);
            Ext.getCmp('uevntModifiedOn').setValue(response[0].data.modifiedDate);
            Ext.getCmp('uevntModifiedBy').setValue(response[0].data.modifiedBy);
            Ext.getCmp('uevntAddress1').setValue(response[0].data.location.address1);
            Ext.getCmp('uevntAddress2').setValue(response[0].data.location.address2);
            Ext.getCmp('uevntCity').setValue(response[0].data.location.city);
            Ext.getCmp('uevntState').setValue(response[0].data.location.state);
            Ext.getCmp('uevntZip').setValue(response[0].data.location.zip);
            Ext.getCmp('uevntCountry').setValue(response[0].data.location.country);
            
            
            var list = Ext.getCmp('eventList');
            list.setVisible(false);
            var view = Ext.getCmp('eventView');
            view.setVisible(true);
//        }
//        evtStore.load();
        console.log('double clicked'+record);  
    },   
    loadEventList: function(){
        var startDate=Ext.getCmp('evntStartDate').getValue();
        if(startDate == null)
            startDate='';
        else
            startDate=Ext.Date.format(startDate, 'm/d/Y');
        var endDate=Ext.getCmp('evntEndDate').getValue();
        if(endDate == null)
            endDate='';
        else
            endDate=Ext.Date.format(endDate, 'm/d/Y');
        var title=Ext.getCmp('evntTitle').getValue();
        var status=Ext.getCmp('evntStatus').getValue();
        if(status == null)
            status='';
        var event=Ext.getStore('eventStore');
        var address=Ext.getStore('addressStore');
        address.removeAll();
        address.add({
            address1:''
        });
        event.removeAll();
        var inventoryList=Ext.getStore('eventInventoryStore');
        inventoryList.removeAll();
        inventoryList.add({
            inventoryId:''
        });
        var rectList=Ext.getStore('recruiterListStore');
        rectList.removeAll();
        rectList.add({
            recruiterId:''
        });
        event.removeAll();
        event.add({
            eventTitle:title,
            status:status,
            location:address.getRange()[0].data,
            inventoryList:inventoryList.getRange()[0].data,
            recruiters:rectList.getRange()[0].data
        });
        var eventData=event.getRange();
        var data=Ext.encode(eventData[0].data);
        console.log(data);
//        var evtStore=Ext.getStore('eventListStore');
//        
//        evtStore.getProxy().url = baseUrl +'/event?operation=list&startDate='+startDate+'&endDate='+endDate+'&event='+data;
//        evtStore.getProxy().afterRequest = function(){
//            console.log('event list was loaded');
//        //            console.log(store);
//        }
//        evtStore.load();
        
        var list = Ext.getCmp('eventList');
        list.setVisible(true);
        var view = Ext.getCmp('eventView');
        view.setVisible(false);
    },
    updateEvent: function(){
        var status=Ext.getCmp('uevntStatus').getValue();
        if(status == null)
            status='';
        var eventId=Ext.getCmp('uevntId').getValue();
        
        var event=Ext.getStore('eventStore');
        event.removeAll();
        var address=Ext.getStore('addressStore');
        address.removeAll();
        address.add({
            address1:''
        });
        var invStore=Ext.getStore('eventInventoryStore');
        var inventory=invStore.getRange();
        var evtInv='';
        for(var i=0;i < inventory.length;i++){
            if(evtInv == ''){
                evtInv=Ext.encode(inventory[i].data);
            } else{
                evtInv=evtInv+ ','+ Ext.encode(inventory[i].data);
            }
        }
        evtInv='{'+ 'inventory: ['+evtInv+']'+'}';
        
        var rectStore=Ext.getStore('recruiterListStore');
        var recruiter=rectStore.getRange();
        var evtRect='';
        for(var i=0;i < recruiter.length;i++){
            if(evtRect == ''){
                evtRect=Ext.encode(recruiter[i].data);
            } else{
                evtRect=evtRect+ ','+ Ext.encode(recruiter[i].data);
            }
        }
        evtRect='{'+ 'recruiter: ['+evtRect+']'+'}';
        
        event.add({
            eventId:eventId,
            status:status,
            location:address.getRange()[0].data,
            inventoryList:Ext.decode(evtInv,'rcrm.model.evtInvListModel'),
            recruiters:Ext.decode(evtRect,'rcrm.model.evtRecruiterListModel')
        });
        
        var eventData=event.getRange();
        var data=Ext.encode(eventData[0].data);
        console.log('data:\n'+data);
//        var evtStore=Ext.getStore('updateStore');
//        evtStore.getProxy().url = baseUrl +'/event?operation=update&event='+data+'&loggedInUser='+loginUserId;
//        evtStore.getProxy().afterRequest = function(){
//            console.log('event is saved successfully');
            var list = Ext.getCmp('eventList');
            list.setVisible(true);
            var view = Ext.getCmp('eventView');
            view.setVisible(false);
//        }
//        evtStore.load();
    },
    
     addEvent: function(){
        var evtName=Ext.getCmp('addevtName').getValue();
        var evtStDate=Ext.getCmp('addevtStDate').getValue();
        if(evtStDate == null)
            evtStDate='';
        else
            evtStDate=Ext.Date.format(evtStDate, 'm/d/Y');
        var evtEndDate=Ext.getCmp('addevtendDate').getValue();
        if(evtEndDate == null)
            evtEndDate='';
        else
            evtEndDate=Ext.Date.format(evtEndDate, 'm/d/Y');
        var status=Ext.getCmp('addevtStatus').getValue();
        if(status == null)
            status='';
        var remarks=Ext.getCmp('addevtRemarks').getValue();
        var add1=Ext.getCmp('addevtAdd1').getValue();
        var add2=Ext.getCmp('addevtAdd2').getValue();
        var city=Ext.getCmp('addevtCity').getValue();
        var state=Ext.getCmp('addevtState').getValue();
        var zip=Ext.getCmp('addevtZip').getValue();
        var country=Ext.getCmp('addevtCountry').getValue();
        var event=Ext.getStore('eventStore');
        event.removeAll();
        var address=Ext.getStore('addressStore');
        address.removeAll();
        address.add({
            address1:add1,
            address2:add2,
            city:city,
            state:state,
            zip:zip,
            country:country
        });
        var invStore=Ext.getStore('eventInventoryStore');
        var inventory=invStore.getRange();
        var evtInv='';
        for(var i=0;i < inventory.length;i++){
            if(evtInv == ''){
                evtInv=Ext.encode(inventory[i].data);
            } else{
                evtInv=evtInv+ ','+ Ext.encode(inventory[i].data);
            }
        }
        evtInv='{'+ 'inventory: ['+evtInv+']'+'}';
        
        var rectStore=Ext.getStore('recruiterListStore');
        var recruiter=rectStore.getRange();
        var evtRect='';
        for(var i=0;i < recruiter.length;i++){
            if(evtRect == ''){
                evtRect=Ext.encode(recruiter[i].data);
            } else{
                evtRect=evtRect+ ','+ Ext.encode(recruiter[i].data);
            }
        }
        evtRect='{'+ 'recruiter: ['+evtRect+']'+'}';
        
        event.add({
            eventTitle:evtName,
            eventStartDate:evtStDate,
            eventEndDate:evtEndDate,
            remarks:remarks,
            status:status,
            location:address.getRange()[0].data,
            inventoryList:Ext.decode(evtInv,'rcrm.model.evtInvListModel'),
            recruiters:Ext.decode(evtRect,'rcrm.model.evtRecruiterListModel')
        });
        
        var eventData=event.getRange();
        var data=Ext.encode(eventData[0].data);
//        console.log('data:\n'+data);
//        var evtStore=Ext.getStore('updateStore');
//        evtStore.getProxy().url = baseUrl +'/event?operation=add&event='+data+'&loggedInUser='+loginUserId;
//        evtStore.getProxy().afterRequest = function(){
//            console.log('event is added successfully');
//        }
//        evtStore.load();
    },
    
    
    addInventory:function(){
        var inventory=Ext.getStore('inventoryStore');
        inventory.removeAll();
        inventory.add({
            inventoryTitle:''
        });
        var inventoryData=inventory.getRange();
        var data=Ext.encode(inventoryData[0].data);
//        var invStore=Ext.getStore('actInvStore');
//        invStore.getProxy().url = baseUrl +'/inventory?operation=activeinventorylist';
//        invStore.getProxy().afterRequest = function(){
//            console.log('active inventory list was loaded');
//        //            console.log(store);
//        }
//        invStore.load();
        
        
        addpopup=Ext.widget('addEventInventoryView');
        addpopup.show();
        viewport.mask();
        console.log('inventory add is clicked');
    },
    
    addEventInventory: function(){
        var id=Ext.getCmp('addEvtInvId').getValue();
        var title=Ext.getCmp('addEvtInvTitle').getValue();
        var type=Ext.getCmp('addEvtInvType').getValue();
        var count=Ext.getCmp('addEvtInvCount').getValue();
        var store=Ext.getStore('eventInventoryStore');
        store.add({
            'inventoryId':id,
            'inventoryTitle':title,
            'inventoryType':type,
            'count':count
        });
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    closeAddEventInventoryView: function(){
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    addRecruiter:function(){
        addpopup=Ext.widget('addEventRecruiterView');
        addpopup.show();
        viewport.mask();
        console.log('Recruiter add is clicked');
    },
    
    addEventRecruiter: function(){
        var id=Ext.getCmp('addEvtRectId').getValue();
        var name=Ext.getCmp('addEvtRectName').getValue();
        var store=Ext.getStore('recruiterListStore');
        store.add({
            'recruiterId':id,
            'recruiterName':name
        });
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    closeAddEventRectView: function(){
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    remove: function(view, rowIndex){
        console.log('remove');
    },
    addAttendee:function(){
        addpopup=Ext.widget('addEventAttendeeView');
        addpopup.show();
        viewport.mask();
        console.log('Attendee add is clicked');
    },
    
    addEventAttendeeView: function(){
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    closeAddEventAttendeeView: function(){
        addpopup.close();
        viewport.unmask();
        viewport.show();
    }
    
}); 