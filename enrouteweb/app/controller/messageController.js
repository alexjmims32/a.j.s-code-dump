Ext.define('rcrm.controller.messageController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'messageListStore',
    'messageViewStore',
    'updateStore'
    ],
    models: [
    'messageModel',
    'updateModel'
    ],

    views: [
    'messageListView',
    'messageListGridView',
    'messageView',
    'updateMessageView'
    ],

    init: function() {
        this.control({
            'messageListGridView':{
                itemdblclick:this.loadUpdateMessageView
            },
            'messageListView button[action=search]': {
                click: this.messageSearch
            },
            'messageListView button[action=reset]': {
                click: this.messageViewReset
            },
            'updateMessageView button[action=save]': {
                click: this.updateMessage
            },
            'updateMessageView button[action=close]': {
                click: this.loadMessageList
            },
            'messageView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'messageList'){
                            var list = Ext.getCmp('messageList');
                            list.setVisible(true);
                        }
                    });
                }
            }
        });
    },

    messageViewReset: function(){
        Ext.getCmp('msgsDate').reset();
        Ext.getCmp('msgeDate').reset();
        Ext.getCmp('msgStatus').reset();
        Ext.getCmp('msgStudentId').reset();
        Ext.getCmp('msgfName').reset();
        Ext.getCmp('msglName').reset();
       
    },
    
    messageSearch: function(){
        var startDate=Ext.getCmp('msgsDate').getValue();
        if(startDate == null)
            startDate='';
        else
            startDate=Ext.Date.format(startDate, 'm/d/Y');
        var endDate=Ext.getCmp('msgeDate').getValue();
        if(endDate == null)
            endDate='';
        else
            endDate=Ext.Date.format(endDate, 'm/d/Y');
        var status=Ext.getCmp('msgStatus').getValue();
        if(status == null)
            status='';
        var prospectId=Ext.getCmp('msgStudentId').getValue();
        var fName=Ext.getCmp('msgfName').getValue();
        var lName=Ext.getCmp('msglName').getValue();
        var msgId='';
         
//        var store=Ext.getStore('messageListStore');
//        store.getProxy().url = baseUrl +'/message?operation=list&startDate='+startDate+'&endDate='+endDate+'&status='+status+'&prospectId='+prospectId+'&firstName='+fName+'&lastName='+lName+'&messageId='+msgId;
//        store.getProxy().afterRequest = function(){
//            console.log('message List loaded');
//            console.log(store);
//        }
//        store.load();
    },
    
    loadUpdateMessageView: function(grid, record){
        console.log('double clicked'+record);  
        console.log(record.data.messageId);
        var store=Ext.getStore('messageViewStore');
//        store.getProxy().url = baseUrl +'/message?operation=view&messageId='+record.data.messageId;
//        store.getProxy().afterRequest = function(){
//            if(store.getProxy().getReader().rawData.status== 'SUCCESS'){
                var message = store.getRange();
                Ext.getCmp('titlename').setText(message[0].data.firstName+' '+message[0].data.lastName);
                Ext.getCmp('titlemsgId').setText(message[0].data.messageId);
                Ext.getCmp('msgViewMsgId').setValue(message[0].data.messageId);
                Ext.getCmp('msgViewfName').setValue(message[0].data.firstName);
                Ext.getCmp('msgViewlName').setValue(message[0].data.lastName);
                Ext.getCmp('msgViewhNo').setValue(message[0].data.homeNumber);
                Ext.getCmp('msgViewEmail').setValue(message[0].data.email);
                Ext.getCmp('msgViewRId').setValue(message[0].data.recruiterId);
                Ext.getCmp('msgViewRName').setValue(message[0].data.recruiterName);
                Ext.getCmp('msgViewComments').setValue(message[0].data.comments);
                Ext.getCmp('msgViewMsgStatus').setValue(message[0].data.messageStatus);
                Ext.getCmp('msgViewCreatedDate').setValue(message[0].data.messageDate);
                Ext.getCmp('msgViewPId').setValue(message[0].data.prospectId);
                Ext.getCmp('msgViewPStatus').setValue(message[0].data.prospectStatus);
                Ext.getCmp('msgViewCmpName').setValue(message[0].data.campaignName);
                Ext.getCmp('msgViewIntComments').setValue(message[0].data.internalComments);
                
                var list = Ext.getCmp('messageList');
                list.setVisible(false);
                var view = Ext.getCmp('messageView');
                view.setVisible(true);
//            }else{
//                Ext.Msg.alert('alert',store.getProxy().getReader().rawData.status);
//            }
//        }
//        store.load();
    },   
    loadMessageList: function(){
        var list = Ext.getCmp('messageList');
        list.setVisible(true);
        var view = Ext.getCmp('messageView');
        view.setVisible(false);
    },
    updateMessage: function(){
        var messageId= Ext.getCmp('msgViewMsgId').getValue();
        var comment=Ext.getCmp('msgViewIntComments').getValue();
        var status=Ext.getCmp('msgViewMsgStatus').getValue();
        if(status == null)
            status='';
//        var store=Ext.getStore('updateStore');
//        store.getProxy().url = baseUrl +'/message?operation=updatemessage&messageId='+messageId+'&comment='+comment+'&status='+status+'&loggedInUser='+loginUserId;
//        store.getProxy().afterRequest = function(){
//            if(store.getProxy().getReader().rawData.status != 'SUCCESS'){
                Ext.Msg.alert('alert','Message is updated Successfully');
//                var msgListStore=Ext.getStore('messageListStore');
//                msgListStore.load();
//            }
//        }
//        store.load();
        
        var list = Ext.getCmp('messageList');
        list.setVisible(true);
        var view = Ext.getCmp('messageView');
        view.setVisible(false);
    }
    
}); 