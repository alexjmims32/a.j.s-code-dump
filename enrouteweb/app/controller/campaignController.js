Ext.define('rcrm.controller.campaignController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'emailListStore',
    'populationListStore',
    'populationViewStore',
    'activePopListStore',
    'populationStore',
    'popQueryStore',
    'popPersonStore',
    'updatePopStore',
    'templateListStore',
    'activeTempListStore',
    'templateViewStore',
    'templateStore',
    'emailViewStore',
    'emailPopStore',
    'emailQueryStore',
    'emailStudentStore',
    'emailStore'
    ],
    models: [],

    views: [
    'campaignView',
    'emailListView',
    'updateEmailView',
    'templateSelectionView',
    'populationSelectionView',
    'emailScheduleView',
    'populationListView',
    'updatePopulationView',
    'addPopulationView',
    'addPopulationQueryView',
    'addPopulationContactView',
    'templateListView',
    'updateTemplateView',
    'addTemplateView'
    ],
    

    init: function() {
        emailContrller=this;
        this.control({
            'campaignView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'email'){
                            var emailList = Ext.getCmp('emailList');
                            emailList.setVisible(true);
                            emailContrller.loadEmailList();
                        }else if(evt.currentTarget.name == 'template'){
                            var templateList = Ext.getCmp('templateList');
                            templateList.setVisible(true);
                            
//                            var tstore=Ext.getStore('templateListStore');
//                            tstore.getProxy().url = baseUrl +'/template?operation=list';
//                            tstore.getProxy().afterRequest = function(){
//                                console.log('template list is loaded');
//                                console.log(tstore);
//                            }
//                            tstore.load();
        
                        }else if(evt.currentTarget.name == 'population'){
                            var populationList = Ext.getCmp('populationList');
                            populationList.setVisible(true);
                            
//                            var pstore=Ext.getStore('populationListStore');
//                            pstore.getProxy().url = baseUrl +'/population?operation=list';
//                            pstore.getProxy().afterRequest = function(){
//                                console.log('population list is loaded');
//                                console.log(pstore);
//                            }
//                            pstore.load();
                        }
                        
                    });
                }
            },
            'emailListView':{
                itemdblclick:this.loadUpdateEmailView
            },
            'emailListView button[action=addEmail]': {
                click: this.showAddEmail
            },
            'updateEmailView button[action=edit]': {
                click: this.showEditEmail
            },
            'updateEmailView button[action=close]': {
                click: this.loadEmailList
            },
            'updateEmailView gridpanel[ref=ueaction]':{
                itemclick: function(view, model, row, rowIndex, event){
                    console.log('view clicked');
                }
            },
            'templateSelectionView button[action=next]': {
                click: this.loadPopSelctionView
            },
            'templateSelectionView button[action=cancel]': {
                click: this.showPreviousView
            },
            'populationSelectionView button[action=next]': {
                click: this.loadScheduleView
            },
            'populationSelectionView button[action=prev]': {
                click: this.showTempSelectionView
            },
            'populationSelectionView button[action=cancel]': {
                click: this.showPreviousView
            },
            'emailScheduleView button[action=cancel]': {
                click: this.showPreviousView
            },
            'emailScheduleView button[action=prev]': {
                click: this.loadPopSelctionView
            },
            'populationListView':{
                itemdblclick:this.loadUpdatePopulationView
            },
            'populationListView button[action=addPop]': {
                click: this.showAddPopView
            },
            'addPopulationView button[action=addDynamicCnt]': {
                click: this.showAddPopQueryView
            },
            'addPopulationView button[action=addFixedCnt]': {
                click: this.showAddPopCntView
            },
            'addPopulationView button[action=save]': {
                click: this.addPopulation
            },
            'addPopulationView button[action=close]': {
                click: this.loadPopulationList
            },
            'updatePopulationView button[action=addDynamicCnt]': {
                click: this.showAddPopQueryView
            },
            'updatePopulationView button[action=addFixedCnt]': {
                click: this.showAddPopCntView
            },
            'updatePopulationView button[action=save]': {
                click: this.updatePopulation
            },
            'updatePopulationView button[action=close]': {
                click: this.loadPopulationList
            },
            'addPopulationQueryView button[action=close]': {
                click: this.closeAddPopQueryView
            },
            'addPopulationQueryView button[action=add]': {
                click: this.AddPopQuery
            },
            'addPopulationContactView button[action=close]': {
                click: this.closeAddPopCntView
            },
            'addPopulationContactView button[action=add]': {
                click: this.AddPopContact
            },
            'templateListView':{
                itemdblclick:this.loadUpdateTemplateView
            },
            'templateListView button[action=addTemp]': {
                click: this.showAddTempView
            },
            'updateTemplateView button[action=save]': {
                click: this.updateTemplate
            },
            'updateTemplateView button[action=close]': {
                click: this.loadTemplateList
            },
            'addTemplateView button[action=save]': {
                click: this.addTemplate
            },
            'addTemplateView button[action=close]': {
                click: this.loadTemplateList
            }
        });
    },
    
    showPreviousView: function(){
        if(previousView == 'emailList'){
            var addEmail = Ext.getCmp('editEmailView');
            addEmail.setVisible(false);
            var list = Ext.getCmp('emailList');
            list.setVisible(true);
        }
        if(previousView == 'emailView'){
            var editEmail = Ext.getCmp('editEmailView');
            editEmail.setVisible(false);
            var view = Ext.getCmp('emailView');
            view.setVisible(true);
        }
         
          
    },
    
    
    
    loadEmailList: function(){
//        var store=Ext.getStore('emailListStore');
//        store.getProxy().url = baseUrl +'/email?operation=list';
//        store.getProxy().afterRequest = function(){
//            console.log('email list is loaded');
//        //            console.log(store);
//        }
//        store.load();
        var list = Ext.getCmp('emailList');
        list.setVisible(true);
        var view = Ext.getCmp('emailView');
        view.setVisible(false);
    },

    loadUpdateEmailView: function(grid, record){
//        var email=Ext.getStore('emailStore');
//        email.removeAll();
//        email.add({
//            campaignId:record.data.campaignId
//        });
//        var emailData=email.getRange();
//        var data=Ext.encode(emailData[0].data);
//        console.log(data);
        var estore=Ext.getStore('emailViewStore');
//        estore.getProxy().url = baseUrl +'/email?operation=view&email='+data;
//        estore.getProxy().afterRequest = function(){
            console.log('email VIew is loaded');
            console.log(estore);
            var response=estore.getRange();
            Ext.getCmp('evCmpId').setValue(response[0].data.campaignId);
            Ext.getCmp('evCmpTitle').setValue(response[0].data.campaignTitle);
            Ext.getCmp('evEmailSubject').setValue(response[0].data.emailSubject);
            Ext.getCmp('evTempName').setValue(response[0].data.templateName);
            Ext.getCmp('evSchDate').setValue(response[0].data.scheduleDate);
            Ext.getCmp('evCreatedBy').setValue(response[0].data.createdBy);
            Ext.getCmp('evCreatedDate').setValue(response[0].data.createdOn);
            Ext.getCmp('evStatus').setValue(response[0].data.status);
            Ext.getCmp('evTotalEmails').setValue(response[0].data.count);
            Ext.getCmp('evSent').setValue(response[0].data.sentCount);
            Ext.getCmp('evFailed').setValue(response[0].data.failCount);
            Ext.getCmp('evInprogress').setValue(response[0].data.scheduleCount);

            var list = Ext.getCmp('emailList');
            list.setVisible(false);
            var view = Ext.getCmp('emailView');
            view.setVisible(true);
//        }
//        estore.load();
        
        
    },  
    
    showAddEmail: function(){
        previousView="emailList";
        
//        var tempStore=Ext.getStore('activeTempListStore');
//        tempStore.getProxy().url = baseUrl +'/template?operation=activelist';
//        tempStore.load();
//        
//        var popStore=Ext.getStore('activePopListStore');
//        popStore.getProxy().url = baseUrl +'/population?operation=activelist';
//        popStore.load();
        
        var edit = Ext.getCmp('editEmailView');
        Ext.getCmp('editEmailTitleES').setVisible(false);
        Ext.getCmp('editEmailTitleTS').setVisible(false);
        Ext.getCmp('editEmailTitlePS').setVisible(false);
        Ext.getCmp('addEmailTitleES').setVisible(true);
        Ext.getCmp('addEmailTitleTS').setVisible(true);
        Ext.getCmp('addEmailTitlePS').setVisible(true);
        edit.setVisible(true);
        var list = Ext.getCmp('emailList');
        list.setVisible(false);
    },
    
    showEditEmail: function(){
        previousView="emailView";
        var edit = Ext.getCmp('editEmailView');
        
//        var tempStore=Ext.getStore('activeTempListStore');
//        tempStore.getProxy().url = baseUrl +'/template?operation=activelist';
//        tempStore.load();
//        
//        var popStore=Ext.getStore('activePopListStore');
//        popStore.getProxy().url = baseUrl +'/population?operation=activelist';
//        popStore.load();
        
        var estore=Ext.getStore('emailViewStore');
        var email=estore.getRange();
        Ext.getCmp('tempSelTitle').setValue(email[0].data.campaignTitle);
        Ext.getCmp('tempSelChooseTemp').setValue(email[0].data.templateName);
        Ext.getCmp('emailTempHtml').setValue(email[0].data.htmlText);
        
        
        Ext.getCmp('popESubject').setValue(email[0].data.emailSubject);
        Ext.getCmp('popReplyEmail').setValue(email[0].data.emailReply);
        Ext.getCmp('emailPopList').setValue(Ext.getStore('emailPopStore').getRange());
        
//        Ext.getCmp('scheduleDate').setValue(email[0].data.htmlText);
//        Ext.getCmp('scheduleTime').setValue('13:07');
            
        Ext.getCmp('addEmailTitleES').setVisible(false);
        Ext.getCmp('addEmailTitleTS').setVisible(false);
        Ext.getCmp('addEmailTitlePS').setVisible(false);
        Ext.getCmp('editEmailTitleES').setVisible(true);
        Ext.getCmp('editEmailTitleTS').setVisible(true);
        Ext.getCmp('editEmailTitlePS').setVisible(true);
        edit.setVisible(true);
        var view = Ext.getCmp('emailView');
        view.setVisible(false);
    },
    
    showTempSelectionView: function(){
        var popSelection = Ext.getCmp('popSelectView');
        popSelection.setVisible(false);
        var edit = Ext.getCmp('editEmailView');
        edit.setVisible(true);
    },
    
    loadPopSelctionView: function(){
        
        var edit = Ext.getCmp('editEmailView');
        edit.setVisible(false);
        var popSelection = Ext.getCmp('popSelectView');
        popSelection.setVisible(true);
    },
    
    loadScheduleView: function(){
        var edit = Ext.getCmp('emailSchView');
        edit.setVisible(true);
        var popSelection = Ext.getCmp('popSelectView');
        popSelection.setVisible(false);
    },
    
    updateEmail: function(){
        var list = Ext.getCmp('emailList');
        list.setVisible(true);
        var view = Ext.getCmp('emailView');
        view.setVisible(false);
    },
    
    loadTemplateList: function(){
//        var store=Ext.getStore('templateListStore');
//        store.getProxy().url = baseUrl +'/template?operation=list';
//        store.getProxy().afterRequest = function(){
//            console.log('template list is loaded');
//        //            console.log(store);
//        }
//        store.load();
        var list = Ext.getCmp('templateList');
        list.setVisible(true);
        var view = Ext.getCmp('templateView');
        view.setVisible(false);
    },
    
    loadUpdateTemplateView: function(grid, record){
        
//        var template=Ext.getStore('templateStore');
//        template.removeAll();
//        template.add({
//            templateId:record.data.templateId
//        });
//        var templateData=template.getRange();
//        var data=Ext.encode(templateData[0].data);
//        console.log(data);
        var tstore=Ext.getStore('templateViewStore');
//        tstore.getProxy().url = baseUrl +'/template?operation=view&template='+data;
//        tstore.getProxy().afterRequest = function(){
            console.log('template View was loaded');
            console.log(tstore);
            var response=tstore.getRange();
            if(response[0].data.templateStatus == 'A')
                Ext.getCmp('utStatus').setValue(true);
            else
                Ext.getCmp('utStatus').setValue(false);
            Ext.getCmp('utCreatedBy').setValue(response[0].data.createdBy);
            Ext.getCmp('utCreatedOn').setValue(response[0].data.createdOn);
            Ext.getCmp('utModifiedBy').setValue(response[0].data.lastModifiedBy);
            Ext.getCmp('utModifiedOn').setValue(response[0].data.lastModifiedOn);
            Ext.getCmp('utCategory').setValue(response[0].data.templateCategory);
            Ext.getCmp('utTemplateName').setValue(response[0].data.templateName);
            Ext.getCmp('utTemplateId').setValue(response[0].data.templateId);
            Ext.getCmp('utEmailTemplate').setValue(response[0].data.htmlText);
            
            var list = Ext.getCmp('templateList');
            list.setVisible(false);
            var view = Ext.getCmp('templateView');
            view.setVisible(true);
//        }
//        tstore.load();
        
    },   
    
    showAddTempView: function(){
        var list = Ext.getCmp('templateList');
        list.setVisible(false);
        var addTemp = Ext.getCmp('addTempView');
        addTemp.setVisible(true);
    },
    
    updateTemplate: function(){
        var tempId=Ext.getCmp('utTemplateId').getValue();
        var tempName=Ext.getCmp('utTemplateName').getValue();
        var tempCategory=Ext.getCmp('utCategory').getValue();
        var tempStatus=Ext.getCmp('utStatus').getValue();
        if(tempStatus == true)
            tempStatus='A';
        else
            tempStatus='I';
        var htmlText=Ext.getCmp('utEmailTemplate').getValue();
        
        var template=Ext.getStore('templateStore');
        template.removeAll();
        template.add({
            templateId:tempId,
            templateCategory:tempCategory,
            templateName:tempName,
            templateStatus:tempStatus,
            htmlText:htmlText,
            loggedInUser:loginUserId
        });
        var templateData=template.getRange();
        var data=Ext.encode(templateData[0].data);

        console.log(data);
//        var tstore=Ext.getStore('updateStore');
//        tstore.getProxy().url = baseUrl +'/template?operation=save&template='+data;
//        tstore.getProxy().afterRequest = function(){
            console.log('template saved');
            console.log(tstore);
//            var response=tstore.getRange();
//            if(response[0].data.status == 'SUCCESS'){
                Ext.Msg.alert('Alert','Template Updated Successfully');
//                var store=Ext.getStore('templateListStore');
//                store.getProxy().url = baseUrl +'/template?operation=list';
//                store.getProxy().afterRequest = function(){
//                    console.log('template list is loaded');
//                //            console.log(store);
////                }
//                store.load();
                
//            }else{
//                Ext.Msg.alert('Alert',response[0].data.status);
//            }
//        }
//        tstore.load();     
        var list = Ext.getCmp('templateList');
        list.setVisible(true);
        var view = Ext.getCmp('templateView');
        view.setVisible(false);
    },
    
    addTemplate: function(){
        var tempName=Ext.getCmp('addTempName').getValue();
        var tempCategory=Ext.getCmp('addTempCategory').getValue();
        var tempStatus='A';
        var htmlText=Ext.getCmp('addTempHtml').getValue();
        
        var template=Ext.getStore('templateStore');
        template.removeAll();
        template.add({
            templateCategory:tempCategory,
            templateName:tempName,
            templateStatus:tempStatus,
            htmlText:htmlText,
            loggedInUser:loginUserId
        });
        var templateData=template.getRange();
        var data=Ext.encode(templateData[0].data);

//        var tstore=Ext.getStore('updateStore');
//        tstore.getProxy().url = baseUrl +'/template?operation=create&template='+data;
//        tstore.getProxy().afterRequest = function(){
//            var response=tstore.getRange();
//            if(response[0].data.status == 'SUCCESS'){
                Ext.Msg.alert('Alert','Template Added Successfully');
//                var store=Ext.getStore('templateListStore');
//                store.getProxy().url = baseUrl +'/template?operation=list';
//                store.load();
//            }else{
//                Ext.Msg.alert('Alert',response[0].data.status);
//            }
//        }
//        tstore.load(); 
        
        var addTemp = Ext.getCmp('addTempView');
        addTemp.setVisible(false);
        var list = Ext.getCmp('templateList');
        list.setVisible(true);
    },
    
    
    loadPopulationList: function(){
//        var store=Ext.getStore('populationListStore');
//        store.getProxy().url = baseUrl +'/population?operation=list';
//        store.getProxy().afterRequest = function(){
//            console.log('population list is loaded');
//        //            console.log(store);
//        }
//        store.load();
        var list = Ext.getCmp('populationList');
        list.setVisible(true);
        var view = Ext.getCmp('populationView');
        view.setVisible(false);
    },
    
    loadUpdatePopulationView: function(grid, record){
        
//        var population=Ext.getStore('populationStore');
//        population.removeAll();
//        population.add({
//            populationId:record.data.populationId
//        });
//        var populationData=population.getRange();
//        var data=Ext.encode(populationData[0].data);
//        console.log(data);
        var pstore=Ext.getStore('populationViewStore');
//        pstore.getProxy().url = baseUrl +'/population?operation=view&population='+data;
//        pstore.getProxy().afterRequest = function(){
            console.log('population View was loaded');
            console.log(pstore);
            var response=pstore.getRange();
            if(response[0].data.status == 'A')
                Ext.getCmp('upPopStatus').setValue(true);
            else
                Ext.getCmp('upPopStatus').setValue(false);
            Ext.getCmp('upPopId').setValue(response[0].data.populationId);
            Ext.getCmp('upPopName').setValue(response[0].data.populationTitle);
            Ext.getCmp('upCreatedBy').setValue(response[0].data.createdBy);
            Ext.getCmp('upCreatedOn').setValue(response[0].data.createdOn);
            Ext.getCmp('upModifiedBy').setValue(response[0].data.lastModifiedBy);
            Ext.getCmp('upModifiedOn').setValue(response[0].data.lastModifiedOn);
            
            var list = Ext.getCmp('populationList');
            list.setVisible(false);
            var view = Ext.getCmp('populationView');
            view.setVisible(true);
//        }
//        pstore.load();
        
    },
    
    showAddPopView: function(){
        var list = Ext.getCmp('populationList');
        list.setVisible(false);
        var queryStore=Ext.getStore('popQueryStore');
        queryStore.removeAll();
        var contStore=Ext.getStore('popPersonStore');
        contStore.removeAll();
        var addPop=Ext.getCmp('addPopView');
        addPop.setVisible(true);
        
    },
    
    updatePopulation: function(){
        var popId=Ext.getCmp('upPopId').getValue();
        var status='I';
        if( Ext.getCmp('upPopStatus').getValue() == true )
            status='A';
        var popName=Ext.getCmp('upPopName').getValue();
        var store=Ext.getStore('updatePopStore');
        store.removeAll();
        var personStore=Ext.getStore('popPersonStore');
        var person=personStore.getRange();
        var personList='';
        for(var i=0;i < person.length;i++){
            if(personList == ''){
                personList=Ext.encode(person[i].data);
            } else{
                personList=personList+ ','+ Ext.encode(person[i].data);
            }
        }
        personList='{'+ 'person: ['+personList+']'+'}';
        
        var queryStore=Ext.getStore('popQueryStore');
        var query=queryStore.getRange();
        var queryList='';
        for(var i=0;i < query.length;i++){
            if(queryList == ''){
                queryList=Ext.encode(query[i].data);
            } else{
                queryList=queryList+ ','+ Ext.encode(query[i].data);
            }
        }
        queryList='{'+ 'query: ['+queryList+']'+'}';
        
        store.add({
            populationId : popId,
            populationTitle : popName,
            status : status,
            loggedInUser : loginUserId,
            personList:Ext.decode(personList,'rcrm.model.campaign.studentModel'),
            queryList:Ext.decode(queryList,'rcrm.model.campaign.queryModel')
        });
        
        var storeData=store.getRange();
        var data=Ext.encode(storeData[0].data);
        
//        var updateStore=Ext.getStore('updateStore');
//        updateStore.getProxy().url = baseUrl +'/population?operation=save&population='+data;
//        updateStore.getProxy().afterRequest = function(){
//            var response=updateStore.getRange();
//            if(response[0].data.status == 'SUCCESS')
                Ext.Msg.alert('Alert','Population updated Successfully');
//            else
//                Ext.Msg.alert('Alert',response[0].data.status);
//            var store=Ext.getStore('populationListStore');
//            store.getProxy().url = baseUrl +'/population?operation=list';
//            store.getProxy().afterRequest = function(){
//                console.log('population list is loaded');
//            //            console.log(store);
//            }
//            store.load();
            var list = Ext.getCmp('populationList');
            list.setVisible(true);
            var view = Ext.getCmp('populationView');
            view.setVisible(false);
//        }
//        updateStore.load();
        
        
    },
    
    showAddPopQueryView:function(){
        addpopup=Ext.widget('addPopulationQueryView');
        addpopup.show();
        viewport.mask();
        console.log('Query add is clicked');
    },
        
    AddPopQuery: function(){
        var query=Ext.getCmp('addPopDbQuery').getValue();
        var name=Ext.getCmp('addPopQueryName').getValue();
        var store=Ext.getStore('popQueryStore');
        store.add({
            'queryName':name,
            'query':query
        });
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    closeAddPopQueryView: function(){
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    showAddPopCntView:function(){
        addpopup=Ext.widget('addPopulationContactView');
        addpopup.show();
        viewport.mask();
        console.log('Contact add is clicked');
    },
        
    AddPopContact: function(){
        //        var query=Ext.getCmp('addPopDbQuery').getValue();
        //        var name=Ext.getCmp('addPopQueryName').getValue();
        //        var store=Ext.getStore('popQueryStore');
        //        store.add({
        //            'queryName':name,
        //            'query':query
        //        });
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    closeAddPopCntView: function(){
        addpopup.close();
        viewport.unmask();
        viewport.show();
    },
    
    addPopulation: function(){
        var status = 'A';
        var popName=Ext.getCmp('addPopName').getValue();
        var store=Ext.getStore('updatePopStore');
        store.removeAll();
        var personStore=Ext.getStore('popPersonStore');
        var person=personStore.getRange();
        var personList='';
        for(var i=0;i < person.length;i++){
            if(personList == ''){
                personList=Ext.encode(person[i].data);
            } else{
                personList=personList+ ','+ Ext.encode(person[i].data);
            }
        }
        personList='{'+ 'person: ['+personList+']'+'}';
        
        var queryStore=Ext.getStore('popQueryStore');
        var query=queryStore.getRange();
        var queryList='';
        for(var i=0;i < query.length;i++){
            if(queryList == ''){
                queryList=Ext.encode(query[i].data);
            } else{
                queryList=queryList+ ','+ Ext.encode(query[i].data);
            }
        }
        queryList='{'+ 'query: ['+queryList+']'+'}';
        
        store.add({
            populationTitle : popName,
            status : status,
            loggedInUser : loginUserId,
            personList:Ext.decode(personList,'rcrm.model.campaign.studentModel'),
            queryList:Ext.decode(queryList,'rcrm.model.campaign.queryModel')
        });
        
        var storeData=store.getRange();
        var data=Ext.encode(storeData[0].data);
        
//        var updateStore=Ext.getStore('updateStore');
//        updateStore.getProxy().url = baseUrl +'/population?operation=create&population='+data;
//        updateStore.getProxy().afterRequest = function(){
//            if(response[0].data.status == 'SUCCESS')
                Ext.Msg.alert('Alert','Population Added Successfully');
//            else
//                Ext.Msg.alert('Alert',response[0].data.status);
//            var queryStore=Ext.getStore('popQueryStore');
//            queryStore.removeAll();
//            var contStore=Ext.getStore('popPersonStore');
//            contStore.removeAll();
//            
//            
//            var store=Ext.getStore('populationListStore');
//            store.getProxy().url = baseUrl +'/population?operation=list';
//            store.getProxy().afterRequest = function(){
//                console.log('population list is loaded');
//            //            console.log(store);
//            }
//            store.load();
            
            
            var addPop=Ext.getCmp('addPopView');
            addPop.setVisible(false);
            var list = Ext.getCmp('populationList');
            list.setVisible(true);
//        }
//        updateStore.load();
    },
    
    showAddEmailView: function(){
        
    },
    
    getHtml: function(){
        console.log('get html is fired');
    }
    
    
}); 