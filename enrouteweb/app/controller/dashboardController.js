Ext.define('rcrm.controller.dashboardController', {
    extend: 'Ext.app.Controller',
    stores: [
    'emailSummaryStore',
    'topRecruiterStore',
    'messageSummaryStore',
    'callSummaryStore',
    'performanceStore'
    ],
    models: [],

    views: [
    'dashboardView',
    'topRecruiterView',
    'messageSummaryView',
    'emailSummaryView',
    'callSummaryView',
    'performanceView',
    'todayScorecardView'
    ],
    init: function() {
        this.control({
            'emailSummaryView': {
                render: this.onLoadEmailSummary
            },
            'messageSummaryView': {
                render: this.onLoadMessageSummary
            }
        });
    },
    
    onLoadEmailSummary: function(){
        var store=Ext.getStore('emailSummaryStore');
//            store.getProxy().url = baseUrl +'/email?operation=summary';
//            store.getProxy().afterRequest = function(){
//                console.log('email Summary loaded');
//                console.log(store);
//            }
//            store.load();
    },
    onLoadMessageSummary: function(){
        var store=Ext.getStore('messageSummaryStore');
//            store.getProxy().url = baseUrl +'/message?operation=summary';
//            store.getProxy().afterRequest = function(){
//                console.log('email Summary loaded');
//                console.log(store);
//            }
//            store.load();
    },
     onLoadSummary: function(){
        var store=Ext.getStore('messageSummaryStore');
//            store.getProxy().url = baseUrl +'/prospect?operation=recruitperformance';
//            store.getProxy().afterRequest = function(){
//                console.log('email Summary loaded');
//                console.log(store);
//            }
//            store.load();
    }
    
    
    
//    onChartRendered: function(storeItem, item){
//        var total = 0;
//        var store=Ext.getStore('topRecruiterStore');
//        store.each(function(rec) {
//            total += rec.get('points');
//        });
//        this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('points') / total * 100)+ '%'+'('+storeItem.get('points')+')');
//    }

});