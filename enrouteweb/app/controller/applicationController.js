Ext.define('rcrm.controller.applicationController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    ],
    models: [
    ],

    views: [
    'applicationListView',
    'applicationListGridView',
    'applicationView',
    ],

    init: function() {
        this.control({
            'callView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'callList'){
                            var list = Ext.getCmp('callList');
                            list.setVisible(true);
                        }else if(evt.currentTarget.name == 'navigateCall'){
                            var navigateCall = Ext.getCmp('navigateCall');
                            navigateCall.setVisible(true);
                        }else if(evt.currentTarget.name == 'importCall'){
                            
                        }
                        
                    });
                }
            }
        });
    }
    
}); 