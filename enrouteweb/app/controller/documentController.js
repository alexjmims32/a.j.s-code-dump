Ext.define('rcrm.controller.documentController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    ],
    models: [
    ],

    views: [
    'documentListView',
    'documentListGridView',
    'documentView',
    ],

    init: function() {
        this.control({
            'letterView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'letterList'){
                            var list = Ext.getCmp('letterList');
                            list.setVisible(true);
                        }                        
                    });
                }
            }
        });
    }

    
}); 