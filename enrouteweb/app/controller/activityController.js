Ext.define('rcrm.controller.activityController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'userScorecardStore',
    'scorecardStandingsStore',
    'userActivityStore',
    'addPointStore'
    ],
    models: [
    'userScorecardModel',
    'scorecardStandingsModel',
    'userActivityModel',
    'addPointModel'
    ],

    views: [
    'activityView',
    'userScorecardView',
    'userScorecardListView',
    'userScorecardStandingsView',
    'userScorecardStandingsListView',
    'userActivityView',
    'userActivityListView',
    'addPointsView'
    ],

    init: function() {
        this.control({
            //            'userScorecardListView':{
            //                itemdblclick:this.loadUpdateProspectView
            //            },
            'userScorecardView button[action=search]': {
                click: this.loadUserScorecard
            },
            'userScorecardView button[action=reset]': {
                click: this.resetUserScoreCardView
            },
            'userScorecardStandingsView button[action=search]': {
                click: this.loadUserScorecardStandings
            },
            'userScorecardStandingsView button[action=reset]': {
                click: this.resetUserScorecardStandingsView
            },
            'userActivityView button[action=search]': {
                click: this.loadUserActivity
            },
            'userActivityView button[action=reset]': {
                click: this.resetUserActivityView
            },
            'addPointsView button[action=add]': {
                click: this.addActivityPoints
            },
            'addPointsView button[action=reset]': {
                click: this.resetAddPointsView
            },
            'activityView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'userScorecard'){
                            var userScorecard = Ext.getCmp('userScorecard');
                            userScorecard.setVisible(true);
                        }else if(evt.currentTarget.name == 'scorecardStandings'){
                            var scorecardStandings = Ext.getCmp('scorecardStandings');
                            scorecardStandings.setVisible(true);
                        }else if(evt.currentTarget.name == 'userActivity'){
                            var userActivity = Ext.getCmp('userActivity');
                            userActivity.setVisible(true);
                        }else if(evt.currentTarget.name == 'addPoints'){
                            var addPoints = Ext.getCmp('addPoints');
                            addPoints.setVisible(true);
                        }
                        
                    });
                }
            }
        });
    },

    loadUserScorecard: function(){
        var userId=Ext.getCmp('usrScrUserId').getValue();
        if(userId == null)
            userId='';
        var activityType=Ext.getCmp('usrScrActivity').getValue();
        if(activityType == null)
            activityType='';
        var sDate=Ext.getCmp('usrScrsDate').getValue();
        //        Ext.Date.format(sDate, 'm/d/Y');
        if(sDate != null)
            sDate= Ext.Date.format(sDate, 'm/d/Y');
        
        var eDate=Ext.getCmp('usrScreDate').getValue();
        if(eDate != null)
            eDate= Ext.Date.format(eDate, 'm/d/Y');

        if(sDate == null || eDate == null)
            Ext.Msg.alert('Activity Start Date and Activity End Date are mandatory');
        else{
//            var store=Ext.getStore('userScorecardStore');
//            store.getProxy().url = baseUrl +'/useractivity?operation=userscorecard&userId='+userId+'&activityType='+activityType+'&startDate='+sDate+'&endDate='+eDate;
//            store.getProxy().afterRequest = function(){
//                console.log('user Scorecard loaded');
//                console.log(store);
//            }
//            store.load();
        }
    },
    
    resetUserScoreCardView: function(){
        Ext.getCmp('usrScrUserId').reset();
        Ext.getCmp('usrScrActivity').reset();
        Ext.getCmp('usrScrsDate').reset();
        Ext.getCmp('usrScreDate').reset();
    },
    
    
    
    loadUserScorecardStandings: function(){
        var sDate=Ext.getCmp('scrstdsDate').getValue();
        if(sDate != null)
            sDate= Ext.Date.format(sDate, 'm/d/Y');
        
        var eDate=Ext.getCmp('scrstdeDate').getValue();
        if(eDate != null)
            eDate= Ext.Date.format(eDate, 'm/d/Y');
        
        var userId=Ext.getCmp('scrstdUserId').getValue();
        if(userId == null)
            userId='';
        var activityType=Ext.getCmp('scrstdActivity').getValue();
        if(activityType == null)
            activityType='';
        var groupByUser=Ext.getCmp('scrstdGbyUser').getValue();
        if(groupByUser == 'YES')
            groupByUser='Y';
        else
            groupByUser='N';
        var groupByDate=Ext.getCmp('scrstdGbyDate').getValue();
        if(groupByDate == 'YES')
            groupByDate='Y';
        else
            groupByDate='N';
        var groupByActivity=Ext.getCmp('scrstdGbyActivity').getValue();
        if(groupByActivity == 'YES')
            groupByActivity='Y';
        else
            groupByActivity='N';
        if(sDate == null || eDate == null)
            Ext.Msg.alert('Activity Start Date and Activity End Date are mandatory');
//        else{
//            var store=Ext.getStore('scorecardStandingsStore');
//            store.getProxy().url = baseUrl +'/useractivity?operation=userscorecardstandings&userId='+userId+'&startDate='+sDate+'&endDate='+eDate+'&activityType='+activityType+'&groupByUser='+groupByUser+'&groupByDate='+groupByDate+'&groupByActivityType='+groupByActivity;
//            store.getProxy().afterRequest = function(){
//                console.log('Scorecard Standings loaded');
//                console.log(store);
//            }
//            store.load();
//        }
    },
    
    resetUserScorecardStandingsView: function(){
        Ext.getCmp('scrstdsDate').reset();
        Ext.getCmp('scrstdeDate').reset();
        Ext.getCmp('scrstdUserId').reset();
        Ext.getCmp('scrstdActivity').reset();
        Ext.getCmp('scrstdGbyUser').reset();
        Ext.getCmp('scrstdGbyDate').reset();
        Ext.getCmp('scrstdGbyActivity').reset();
    },
    
    loadUserActivity: function(){
        var userId=Ext.getCmp('usrActUserId').getValue();
        if(userId == null)
            userId='';
        var sDate=Ext.getCmp('usrActsDate').getValue();
        if(sDate != null)
            sDate= Ext.Date.format(sDate, 'm/d/Y');
        
        var eDate=Ext.getCmp('usrActeDate').getValue();
        if(eDate != null)
            eDate= Ext.Date.format(eDate, 'm/d/Y');
        
        var activityType=Ext.getCmp('usrActivity').getValue();
        if(activityType == null)
            activityType='';
        var studentId=Ext.getCmp('usrActStudentId').getValue();
        if(sDate == null || eDate == null)
            Ext.Msg.alert('Activity Start Date and Activity End Date are mandatory');
//        else{
//            var store=Ext.getStore('userActivityStore');
//            store.getProxy().url = baseUrl +'/useractivity?operation=useractivity&userId='+userId+'&startDate='+sDate+'&endDate='+eDate+'&activityType='+activityType+'&studentId='+studentId;
//            store.getProxy().afterRequest = function(){
//                console.log('user Activity loaded');
//                console.log(store);
//            }
//            store.load();
//        }
    },
    
    resetUserActivityView: function(){
        Ext.getCmp('usrActUserId').reset();
        Ext.getCmp('usrActsDate').reset();
        Ext.getCmp('usrActeDate').reset(); 
        Ext.getCmp('usrActivity').reset();
        Ext.getCmp('usrActStudentId').reset();
    },
    
    addActivityPoints: function(){
        var userId=Ext.getCmp('addPointActUser').getValue();
        var actType=Ext.getCmp('addPointActType').getValue();
        var noofPoints=Ext.getCmp('addPointActPoints').getValue();
        var actDate=Ext.getCmp('addPointActDate').getValue();
        if(actDate != null )
            actDate=Ext.Date.format(actDate, 'm/d/Y');
        if(userId == null || actType == null || noofPoints == '' || actDate == null)
            Ext.Msg.alert('All fields are mandatory');
        else{
//            var store=Ext.getStore('addPointStore');
//            store.getProxy().url = baseUrl +'/useractivity?operation=addactivitypoint&userId='+userId+'&activityType='+actType+'&numberOfPoints='+noofPoints+'&addedBy='+loginUserId+'&activityDate='+actDate;
//            store.getProxy().afterRequest = function(){
//                var record = store.getProxy().getReader().rawData;
//                if(record.status == 'SUCCESS'){
                    Ext.Msg.alert('alert','The special activity has been successfully saved');
//                }else{
//                    Ext.Msg.alert('alert',record.status);
//                }
//                console.log('email Summary loaded');
//                console.log(store);
//            }
//            store.load();
        }
    },
    resetAddPointsView: function(){
        Ext.getCmp('addPointActUser').reset();
        Ext.getCmp('addPointActType').reset();
        Ext.getCmp('addPointActPoints').reset();
        Ext.getCmp('addPointActDate').reset();
    }
    
}); 