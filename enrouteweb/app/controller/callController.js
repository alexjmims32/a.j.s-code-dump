Ext.define('rcrm.controller.callController', {
    extend: 'Ext.app.Controller',
    
    stores: [
    'callListStore',
    'callViewStore',
    'callUpdateStore'
    ],
    models: [
    'callModel'
    ],

    views: [
    'callListView',
    'callListGridView',
    'callView',
    'updateCallView',
    'navigateCallView'
    ],

    init: function() {
        this.control({
            'callListGridView':{
                itemdblclick:this.loadUpdateCallView
            },
            'callListView button[action=search]': {
                click: this.callSearch
            },
            'callListView button[action=reset]': {
                click: this.resetCallSearchView
            },
            'updateCallView button[action=save]': {
                click: this.updateCall
            },
            'updateCallView button[action=cancel]': {
                click: this.loadPriviousView
            },
            'navigateCallView button[action=getNextCall]': {
                click: this.loadNextCallView
            },
            'callView':{
                afterrender: function(cmp){
                    var links = cmp.getEl().select('a'); 
                    links.on('click', function(evt, el, o){ 
                        if(evt.currentTarget.name == 'callList'){
                            var list = Ext.getCmp('callList');
                            list.setVisible(true);
                        }else if(evt.currentTarget.name == 'navigateCall'){
                            var navigateCall = Ext.getCmp('navigateCall');
                            navigateCall.setVisible(true);
                        }else if(evt.currentTarget.name == 'importCall'){
                            
                        }
                        
                    });
                }
            }
        });
    },

    callSearch: function(){
        var studentId=Ext.getCmp('callStudentId').getValue();
        var crmTicketNumber='';
        var callReason='';
        var status=Ext.getCmp('callStatus').getValue();
        if(status == null)
            status='';
        var fName=Ext.getCmp('callfName').getValue();
        var lName=Ext.getCmp('calllName').getValue();
        var startDate=Ext.getCmp('callsDate').getValue();
        if(startDate == null)
            startDate='';
        else
            startDate=Ext.Date.format(startDate, 'm/d/Y');
        var endDate=Ext.getCmp('calleDate').getValue();
        if(endDate == null)
            endDate='';
        else
            endDate=Ext.Date.format(endDate, 'm/d/Y');
//        var store=Ext.getStore('callListStore');
//        store.getProxy().url = baseUrl +'/call?operation=list&studentId='+studentId+'&crmTicketNumber='+crmTicketNumber+'&callReason='+callReason+'&status='+status+'&firstName='+fName+'&lastName='+lName+'&startDate='+startDate+'&endDate='+endDate;
//        store.getProxy().afterRequest = function(){
//            console.log('email Summary loaded');
//            console.log(store);
//        }
//        store.load();
    },
    
    resetCallSearchView: function(){
        Ext.getCmp('callStudentId').reset();
        Ext.getCmp('callStatus').reset();
        Ext.getCmp('callfName').reset();
        Ext.getCmp('calllName').reset();
        Ext.getCmp('callsDate').reset();
        Ext.getCmp('calleDate').reset();
    },
    
    loadUpdateCallView: function(grid, record){
        priviousCallView='list';
        console.log('double clicked'+record);  
        var store=Ext.getStore('callViewStore');
//        store.getProxy().url = baseUrl +'/call?operation=view&callId='+record.data.callID+'&loggedInUser=';
//        store.getProxy().afterRequest = function(){
            var call = store.getRange();
            
            Ext.getCmp('callViewtitle').setText(call[0].data.studentName +' ( Id: '+call[0].data.studentID+ ' )');
            Ext.getCmp('cvCReason').setValue(call[0].data.callReason);
            Ext.getCmp('cvCallId').setValue(call[0].data.callID);
            Ext.getCmp('cvHomeNumber').setValue(call[0].data.homeNumber);
            Ext.getCmp('cvEmail').setValue(call[0].data.email);
            Ext.getCmp('cvStatus').setValue(call[0].data.status);
            Ext.getCmp('cvOwner').setValue(call[0].data.callOwner);
            Ext.getCmp('cvContactDate').setValue(call[0].data.callDate);
            Ext.getCmp('cvInfStudent').setValue(call[0].data.contactInstructions);
            Ext.getCmp('cvComment').setValue(call[0].data.comment);
            if(call[0].data.contactedByPhone == 'Y')
                Ext.getCmp('cvCntByPhone').setValue(true);
            else
                Ext.getCmp('cvCntByPhone').setValue(false);
            if(call[0].data.contactedByEmail == 'Y')
                Ext.getCmp('cvCntByEmail').setValue(true);
            else
                Ext.getCmp('cvCntByEmail').setValue(false);
            if(call[0].data.leftMessage == 'Y')
                Ext.getCmp('cvLeftMsg').setValue(true);
            else
                Ext.getCmp('cvLeftMsg').setValue(false);
            if(call[0].data.noContactPossible == 'Y')
                Ext.getCmp('cvNoCntPossible').setValue(true);
            else
                Ext.getCmp('cvNoCntPossible').setValue(false);
            console.log('email Summary loaded');
            console.log(store);
            var navigateCall = Ext.getCmp('navigateCall');
            navigateCall.setVisible(false);
            var view = Ext.getCmp('callView');
            view.setVisible(true);
//        }
//        store.load();
    },   

    loadNextCallView: function(grid, record){
        priviousCallView='navigateCall';
        console.log('double clicked'+record);  
        var store=Ext.getStore('callViewStore');
//        store.getProxy().url = baseUrl +'/call?operation=nextcall&loggedInUser='+loginUserId;
//        store.getProxy().afterRequest = function(){
            var call = store.getRange();
            
            Ext.getCmp('callViewtitle').setText(call[0].data.studentName +' ( Id: '+call[0].data.studentID+ ' )');
            Ext.getCmp('cvCReason').setValue(call[0].data.callReason);
            Ext.getCmp('cvCallId').setValue(call[0].data.callID);
            Ext.getCmp('cvHomeNumber').setValue(call[0].data.homeNumber);
            Ext.getCmp('cvEmail').setValue(call[0].data.email);
            Ext.getCmp('cvStatus').setValue(call[0].data.status);
            Ext.getCmp('cvOwner').setValue(call[0].data.callOwner);
            Ext.getCmp('cvContactDate').setValue(call[0].data.callDate);
            Ext.getCmp('cvInfStudent').setValue(call[0].data.contactInstructions);
            Ext.getCmp('cvComment').setValue(call[0].data.comment);
            if(call[0].data.contactedByPhone == 'Y')
                Ext.getCmp('cvCntByPhone').setValue(true);
            else
                Ext.getCmp('cvCntByPhone').setValue(false);
            if(call[0].data.contactedByEmail == 'Y')
                Ext.getCmp('cvCntByEmail').setValue(true);
            else
                Ext.getCmp('cvCntByEmail').setValue(false);
            if(call[0].data.leftMessage == 'Y')
                Ext.getCmp('cvLeftMsg').setValue(true);
            else
                Ext.getCmp('cvLeftMsg').setValue(false);
            if(call[0].data.noContactPossible == 'Y')
                Ext.getCmp('cvNoCntPossible').setValue(true);
            else
                Ext.getCmp('cvNoCntPossible').setValue(false);
            console.log('email Summary loaded');
            console.log(store);
            var list = Ext.getCmp('callList');
            list.setVisible(false);
            var view = Ext.getCmp('callView');
            view.setVisible(true);
//        }
//        store.load();
    },   


    
    loadPriviousView: function(){
        var view = Ext.getCmp('callView');
        view.setVisible(false);
        if(priviousCallView == 'list'){
            var list = Ext.getCmp('callList');
            list.setVisible(true);
        }else if(priviousCallView == 'navigateCall' ){
            var navigateCall = Ext.getCmp('navigateCall');
            navigateCall.setVisible(true);
        }
    },
    updateCall: function(){
        var callId=Ext.getCmp('cvCallId').getValue();
        var status=Ext.getCmp('cvStatus').getValue();
        if(status == null)
            status='';
        var owner=Ext.getCmp('cvOwner').getValue();
        if(owner == null)
            owner='';
        var comment=Ext.getCmp('cvComment').getValue();
        var cntByPhone=Ext.getCmp('cvCntByPhone').getValue();
        if(cntByPhone == true)
            cntByPhone='Y';
        else
            cntByPhone='N';
        var cntByEmail=Ext.getCmp('cvCntByEmail').getValue();
        if(cntByEmail == true)
            cntByEmail='Y';
        else
            cntByEmail='N';
        var noCnt=Ext.getCmp('cvNoCntPossible').getValue();
        if(noCnt == true)
            noCnt='Y';
        else
            noCnt='N';
        var leftMsg=Ext.getCmp('cvLeftMsg').getValue();
        if(leftMsg == true)
            leftMsg='Y';
        else
            leftMsg='N';
//        var store=Ext.getStore('callUpdateStore');
//        store.getProxy().url = baseUrl +'/call?operation=save&callId='+callId+'&status='+status+'&owner='+owner+'&comment='+comment+'&contactedByPhone='+cntByPhone+'&contactedByEmail='+cntByEmail+'&leftMessage='+leftMsg+'&noContactPossible='+noCnt+'&loggedInUser='+loginUserId+'&notAttendingGPC=&requestTermChange=';
//        store.getProxy().afterRequest = function(){
//            if(store.getProxy().getReader().rawData.status != 'SUCCESS'){
                Ext.Msg.alert('alert','Call updated Successfully');
//            }
//            var callListStore=Ext.getStore('callListStore');
//            callListStore.load();
            var view = Ext.getCmp('callView');
            view.setVisible(false);
            if(priviousCallView == 'list'){
                var list = Ext.getCmp('callList');
                list.setVisible(true);
            }else if(priviousCallView == 'navigateCall' ){
                var navigateCall = Ext.getCmp('navigateCall');
                navigateCall.setVisible(true);
            }
//        }
//        store.load();
    }
    
}); 