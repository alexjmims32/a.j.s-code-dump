Ext.define('rcrm.view.userScorecardView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.userScorecardView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#00427C'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 2,
            tableAttrs:{
                style: {
//                    width: '50%',
//                    height:'100%'
                },
                //                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Activity Start Date',
            name: 'startDate',
            id:'usrScrsDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'User',
            displayField: 'userId',
            valueField: 'userId',
            value:'ANY',
            width:300,
            editable: false,
            id:'usrScrUserId',
            queryMode: 'local',
            store:'userListStore'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Activity End Date',
            name: 'endDate',
            id:'usrScreDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Activity',
            displayField: 'name',
            valueField: 'value',
            value:'ANY',
            editable: false, 
            width:300,
            id:'usrScrActivity',
            queryMode: 'local',
            store:'userActivityDropdownStore'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'userScorecardListView'
    }            
        
    ]

});