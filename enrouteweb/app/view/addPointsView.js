Ext.define('rcrm.view.addPointsView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addPointsView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 10',
        items:[
        {
            xtype:'label',
            padding:'20 5 10 0',
            html:'<b><u>Add Special Credit</b></u>'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            width:320,
            fieldLabel: 'Activity Date',
            name: 'activityDate',
            id:'addPointActDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Activity User',
            id:'addPointActUser',
            displayField: 'userId',
            valueField: 'userId',
            frame:true,
            editable: false,
            width:320,
            store:'userListStore',
            queryMode: 'local',
            allowBlank:false    
        },
        {
            xtype:'combobox',
            fieldLabel:'Activity Type',
            id:'addPointActType',
            displayField: 'name',
            valueField: 'value',
            frame:true,
            editable: false,
            width:320,
            store:'userActivityDropdownStore',
            queryMode: 'local',
            allowBlank:false    
        },

        {
            xtype:'textfield',
            fieldLabel:'Points',
            width:320,
            id:'addPointActPoints',
            allowBlank:false           
        },
        {
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Save',
                name:'add',
                padding:'2 60',
                action:'add'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                text:'Reset',
                name:'reset',
                padding:'2 60',
                action:'reset'
            }
            ]     
        }
       
        ]
    }
    ]
});