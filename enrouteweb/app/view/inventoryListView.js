Ext.define('rcrm.view.inventoryListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.inventoryListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 2,
            tableAttrs:{
                style: {
//                    width: '50%',
//                    height:'100%'
                },
//                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'invStartDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Title',
            name:'title',
            id:'invTitle'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            padding:'5',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'invEndDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            id:'invStatus',
            fieldLabel:'Inventory Status',
            padding:'5',
            displayField: 'name',
            valueField: 'value',
            value:'ANY',
            editable: false,
            queryMode: 'local',
            store:['ANY','ACTIVE','INACTIVE']
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'inventoryListGridView'
    }            
        
    ]

});