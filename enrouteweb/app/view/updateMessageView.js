Ext.define('rcrm.view.updateMessageView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateMessageView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'label',
                id:'titlename',
                padding:'0 0 20 10'
            },
            {
                xtype:'label',
                text:'( Message Id: ',
                padding:'0 0 20 0'
            },
            {
                xtype:'label',
                id:'titlemsgId',
                padding:'0 0 20 0'
            },
            {
                xtype:'label',
                text:' )',
                padding:'0 0 20 0'
            }
            ]
            
        },
        {
            xtype:'textfield',
            name:'firstName',
            width:'250',
            fieldLabel:'First Name',
            id:'msgViewfName',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'lastName',
            width:'250',
            fieldLabel:'Last Name',
            value:'',
            id:'msgViewlName',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'homeNumber',
            width:'250',
            fieldLabel:'Home Number',
            id:'msgViewhNo',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'email',
            width:'250',
            fieldLabel:'Email',
            id:'msgViewEmail',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'recruiterId',
            width:'250',
            fieldLabel:'Recruiter Id',
            id:'msgViewRId',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'recruiterName',
            width:'250',
            fieldLabel:'Recruiter Name',
            id:'msgViewRName',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'recruiterName',
            width:'250',
            fieldLabel:'Recruiter Name',
            id:'msgViewMsgId',
            value:'',
            padding:'2',
            readOnly:true,
            border:false,
            hidden:true
        },
        {
            xtype:'textareafield',
            name:'comments',
            grow:true,
            fieldLabel:'Comments',
            id:'msgViewComments',
            labelAlign:'top',
            rows:'10',
            width:'350',
            readOnly:true
        },
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'50',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Close',
                action:'close',
                name:'close'
            }
            ]
        }
        ],
        flex:3
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'left'
        },
        border:false,
        padding:'50 300 0 0',
        items:[
        {
            xtype:'combobox',
            fieldLabel:'Message Status',
            displayField: 'name',
            valueField: 'value',
            id:'msgViewMsgStatus',
            editable: false,
            queryMode: 'local',
            store:'messageStatusStore'
        },
        {
            xtype:'textfield',
            name:'messageCreatedDate',
            width:'250',
            fieldLabel:'Message Created Date',
            id:'msgViewCreatedDate',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'prospectId',
            width:'250',
            fieldLabel:'Prospect Id',
            id:'msgViewPId',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'prospectStatus',
            width:'250',
            fieldLabel:'Prospect Status',
            id:'msgViewPStatus',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'campaignName',
            width:'250',
            fieldLabel:'Campaign Name',
            id:'msgViewCmpName',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'textareafield',
            name:'internalComments',
            id:'msgViewIntComments',
            padding:'35 0 0 0',
            grow:true,
            fieldLabel:'Internal Comments',
            labelAlign:'top',
            rows:'10',
            width:'350'
        }
        ]
    }
    ]
});