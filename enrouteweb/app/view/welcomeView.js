Ext.define('rcrm.view.welcomeView',{
    extend:'Ext.panel.Panel',
    alias:'widget.welcomeView',
    
    autoScroll:true,
    //        frame:true,
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle: {
        padding:10
    },
    items:[
    {
        frame:true,
        xtype:'panel',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
                style: {
                    width: '100%',
                    height:'100%'
                }
                //                align: 'left',
//                valign: 'middle'
            },
            trAttrs:{
        //                align: 'right'
        }
        },
        //        layout: {
        //            type: 'hbox',
        //            align: 'stretch'   
        //        },
        bodyStyle: {
            //            background: '#7F3D17'
            background: '#00427C'
        },
        items:[
//        {
//            xtype:'image',
//            src:'resources/images/clientLogo.png',
//            align:'left',        
//            padding:'5 0 0 10',
//            flex:3
//            
//        },
        {
            xtype:'label',
            html:'<center><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;eNroute RCM<b></center>',
            style:'color:#FEB210;font-size:20pt;',
//            width:1000
        },
        {
            xtype: 'component',
            itemId: 'logout',
//            padding:'60 5 0 0',
            autoEl: {
                tag: 'a',
                href: '',
                html: '<font size="2" color="#FEB210"><center><b style="TEXT-DECORATION:none"><br />Logout</b></center></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right',
            flex:1
        },
        ],
        flex: 1
    },
    {
        xtype:'mainView',
        flex: 8
    }
    ]
})