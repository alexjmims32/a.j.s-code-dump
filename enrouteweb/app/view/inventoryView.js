Ext.define('rcrm.view.inventoryView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.inventoryView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C'
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'inventoryList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'inventoryList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Inventory List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'addInventory',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'addInventory',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Add Inventory Item</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'inventory',
        activeTab:'inventoryList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'inventoryList',
            xtype:'inventoryListView'
        },
        {
            id:'addInventory',
            xtype:'addInventoryView'
        },
        {
            id:'inventoryView',
            xtype:'updateInventoryView'
        }
        ]
    } 
    ]

});