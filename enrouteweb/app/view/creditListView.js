Ext.define('rcrm.view.creditListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.creditListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        id:'creditSearch',
        frame:'true',
        layout: {
            type: 'table',
            columns: 3,
            tableAttrs:{
                style: {
                    width: '75%',
                    height:'100%'
                },
                align: 'left',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'csDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            fieldLabel:'Student Id',
            id:'cStdId'
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'creStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'OPEN', 'INPROGRESS', 'APPROVED', 'REJECTED']
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'creDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            fieldLabel:'Term',
            id:'creTerm'
        },
        {
            xtype:'label'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ],
        height:100
    },{
        xtype:'creditListGridView'
    }            
        
    ]

});