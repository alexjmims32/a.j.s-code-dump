Ext.define('rcrm.view.documentListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.documentListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
                style: {
                //                    width: '100%',
                //                    height:'100%'
                },
                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'ltrsDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Document Type',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:['ANY', 'HIGH SCHOOL TRANSCRIPT', 'COLLEGE TRANSCRIPT', 'IMMUNIZATION', 'TRANSIENT LETTER'],
            queryMode: 'local'
        },
        {
            xtype:'combobox',
            fieldLabel:'Supervisor Refferal',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:['ANY', 'YES', 'NO'],
            queryMode: 'local'
        },
        {
            xtype:'textfield',
            fieldLabel:'Student Id'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Document Status',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:['ANY', 'NEW TRANSCRIPT', 'PROCESSED'],
            queryMode: 'local'
        },
        {
            xtype:'combobox',
            fieldLabel:'Owner',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:['ANY', 'CRMDIR'],
            queryMode: 'local'
        },
        {
            xtype:'label',
                    
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'documentListGridView'
    }            
        
    ]

});