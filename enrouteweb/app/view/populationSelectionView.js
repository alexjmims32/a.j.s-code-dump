Ext.define('rcrm.view.populationSelectionView',{
    extend:'Ext.panel.Panel',
    alias:'widget.populationSelectionView',
    
    autoScroll:true,
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
//    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
//        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 20',
        items:[
        {
            xtype:'label',
            id:'editEmailTitlePS',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Edit Email</u>'
        },{
            xtype:'label',
            id:'addEmailTitlePS',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Add Email</u>'

        },{
            xtype:'textfield',
            id:'popESubject',
            fieldLabel:'Subject',
            width:400,
            allowBlank:false           
        },{
            xtype:'textfield',
            id:'popReplyEmail',
            fieldLabel:'Reply Email',
            width:400,
            allowBlank:false       
        },{
//            xtype: 'multiselectfield',
            xtype:'combobox',
            id:'emailPopList',
            fieldLabel:'Choose Population',
            multiSelect:true,
            //            minHeight:'100',
            //            minWidth:'100',
            store:'activePopListStore',
            queryMode: 'local',
            displayField:'populationTitle',
            valueField:'populationId',
            frame:true,
            editable: false
        },{
//            
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Next',
                name:'next',
                padding:'2 60',
                action:'next'
            },{
                xtype:'tbspacer',
                width:'10'
            },{
                xtype:'button',
                text:'Prev',
                name:'prev',
                padding:'2 60',
                action:'prev'
            },{
                xtype:'tbspacer',
                width:'10'
            },{
                xtype:'button',
                text:'Cancel',
                name:'cancel',
                padding:'2 60',
                action:'cancel'
            }
            ]     
        }
       
        ]
    }
    ]
});