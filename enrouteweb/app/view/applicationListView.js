Ext.define('rcrm.view.applicationListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.applicationListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
                style: {
                //                    width: '75%',
                //                    height:'100%'
                },
                //                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'appsDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Student Type',
            id:'appStdType',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'FIRST TIME FRESHMAN', 'TRANSFER', 'CONTINUING', 'ENROLLMENT', 'NON DEGREE', 'POST', 'SPECIAL STUDENT', 'TRANSIENT']
        },
        {
            xtype:'combobox',
            fieldLabel:'Hot',
            id:'appHot',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'YES', 'NO']
        },
        {
            xtype:'combobox',
            fieldLabel:'Admission Requested',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'YES', 'NO'] 
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'appeDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'appStatus',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'UNDER REVIEW', 'PENDING', 'DECISION MADE', 'WITHDRAWN', 'CONDITIONAL ADMIT']
        },
        {
            xtype:'combobox',
            fieldLabel:'OWNER',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'RCMDIR']
        },
        {
            xtype:'combobox',
            fieldLabel:'Group Code',
            id:'callStatus',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'DOMESTIC', 'INTERNATIONAL']
        },
        {
            xtype:'textfield',
            fieldLabel:'Student Id'
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'callStatus',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:[]
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'applicationListGridView'
    }            
        
    ]

});