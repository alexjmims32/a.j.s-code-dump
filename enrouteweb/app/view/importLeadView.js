Ext.define('rcrm.view.importLeadView',{
    extend:'Ext.window.Window',
    alias:'widget.importLeadView',
    
    items:[
        {
        xtype: 'filefield',
        name: 'importLeadFile',
        fieldLabel: 'Leads File',
        labelWidth: 50,
        msgTarget: 'side',
        allowBlank: false,
        anchor: '100%',
        buttonText: 'Browse'
    }
    ]
});