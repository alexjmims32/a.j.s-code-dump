Ext.define('rcrm.view.eventListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.eventListGridView',
    store: 'eventListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Title',  
        dataIndex: 'eventTitle',  
        flex: 3
    },

    {
        header: 'Start Date',  
        dataIndex: 'eventStartDate',  
        flex: 2
    },

    {
        header: 'End Date',  
        dataIndex: 'eventEndDate',  
        flex: 2
    },

    {
        header: 'Status', 
        dataIndex: 'status',  
        flex: 2
    },
    {
        header: 'Last Modified By', 
        dataIndex: 'modifiedBy',  
        flex: 2
    },
    {
        header: 'Last Modified On', 
        dataIndex: 'modifiedDate',  
        flex: 2
    },
    {
        xtype: 'actioncolumn',
                header:'Action',
                items: [
                {
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var store=Ext.getStore('eventListStore');
                        var records=store.getRange();
                        var record=records[rowIndex];
                        console.log('remove');
                        store.remove(record);
                    },
                    align:'center',
                    icon:'resources/images/delete2.png'
                }
                ],
        flex: 1
    }
    ]
});