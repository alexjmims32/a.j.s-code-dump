Ext.define('rcrm.view.eventListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.eventListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 2,
            tableAttrs:{
                style: {
//                    width: '50%',
//                    height:'100%'
                },
//                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'evntStartDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Title',
            id:'evntTitle'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'evntEndDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Event Status',
            id:'evntStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:'eventStatusStore'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'eventListGridView'
    }            
        
    ]

});