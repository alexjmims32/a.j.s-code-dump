Ext.define('rcrm.view.addLeadView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addLeadView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 10',
        items:[
        {
            xtype:'label',
            padding:'20 5 10 0',
            html:'<b><u>Basic Information</b></u>'
        },
        {
            xtype:'textfield',
            id:'alfName',
            fieldLabel:'First Name',
            allowBlank:false           
        },
        {
            xtype:'textfield',
            id:'allName',
            fieldLabel:'Last Name',
            allowBlank:false           
        },       
        {
            xtype:'textfield',
            id:'almName',
            fieldLabel:'Middle Name'
        },
        {
            xtype:'combobox',
            fieldLabel:'Gender',
            id:'alGender',
            frame:true,
            editable: false, 
            store:['Male', 'Female']
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Date Of Birth',
            id:'alDateofBirth',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            id:'alRace',
            fieldLabel:'Race'
        },
        {
            xtype:'textfield',
            fieldLabel:'Ethnicity',
            id:'alEthnicity'
        },
        {
            xtype:'label',
            html:'<b><u>High School Information</b></u>',
            padding:'20 5 10 0'
        },
        {
            xtype:'textfield',
            id:'alhSchoolName',
            fieldLabel:'School Name'
        },
        {
            xtype:'textfield',
            id:'alhCity',
            fieldLabel:'City'
        },
        {
            xtype:'textfield',
            id:'alhStateCode',
            fieldLabel:'State Code'
        },
        {
            xtype:'textfield',
            id:'alGpa',
            fieldLabel:'GPA'
        },
        {
            xtype:'textfield',
            id:'alhLvlCode',
            fieldLabel:'Level Code'
        },
        {
            xtype:'textfield',
            id:'alhDegCode',
            fieldLabel:'Degree Code'
        },
        {
            xtype:'combobox',
            fieldLabel:'Graduation Year',
            id:'algradYear',
            editable: false, 
            store:['2012','2011','2010','2009','2008','2007']
        },
        {
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Add',
                name:'add',
                padding:'2 60',
                action:'add'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                text:'Reset',
                name:'reset',
                padding:'2 60',
                action:'reset'
            }
            ]     
        }
        ],
        flex:1
    },
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        padding:'5 5 5 10',
        items:[
        {
            xtype:'label',
            html:'<b><u>Contact Information</b></u>',
            padding:'20 5 10 0'
        },
        {
            xtype:'textfield',
            id:'alcnthNumber',
            fieldLabel:'House Number'
        },
        {
            xtype:'textfield',
            id:'alcntStLine1',
            fieldLabel:'Line 1'
        },
        {
            xtype:'textfield',
            id:'alcntStLine2',
            fieldLabel:'Line 2'
        },
        {
            xtype:'textfield',
            id:'alcntStLine3',
            fieldLabel:'Line 3'
        },
        {
            xtype:'textfield',
            id:'alcntCity',
            fieldLabel:'City'
        },
        {
            xtype:'textfield',
            id:'alcntStateCode',
            fieldLabel:'State Code'
        },
        {
            xtype:'textfield',
            id:'alcntCounty',
            fieldLabel:'County'
        },
        {
            xtype:'textfield',
            id:'alcntZip',
            fieldLabel:'ZIP Code'
        },
        {
            xtype:'textfield',
            id:'alcntCountry',
            fieldLabel:'Country'
        },
        {
            xtype:'textfield',
            id:'alcntPhone1',
            fieldLabel:'Phone 1'
        },
        {
            xtype:'textfield',
            id:'alcntPhone2',
            fieldLabel:'Phone 2'
        },
        {
            xtype:'textfield',
            inputType:'email',
            id:'alcntEmail',
            fieldLabel:'Email',
             allowBlank:false
        },
        {
            xtype:'textfield',
            inputType:'email',
            id:'alcntVEmail',
            fieldLabel:'Verify Email',
             allowBlank:false
        }
        ],
        flex:1
    },
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'5 5 5 10',
        border: false, 
        items:[
        {
            xtype:'label',
            html:'<b><u>Enrollment Information</u></b>',
            padding:'20 5 10 0'
        },
        {
            xtype:'textfield',
            id:'alStartTerm',
            fieldLabel:'Start Term'
        },
        {
            xtype:'textfield',
            id:'alAcademicInterest1',
            fieldLabel:'Primary Academic Area of Interest'
        },
        {
            xtype:'textfield',
            id:'alAcademicInterest2',
            fieldLabel:'Secondary Academic Area of Interest'
        },
        {
            xtype:'label',
            html:'<b><u>Other Information</b></u>',
            padding:'20 5 10 0'
        },
        {
            xtype:'textfield',
            id:'alLvelInterest',
            fieldLabel:'Level of Interest'
        },
        {
            xtype:'textfield',
            id:'alImportantFactor',
            fieldLabel:'Important Factor for choosing University'
        }
        ],
        flex:1
    }
    ]
});