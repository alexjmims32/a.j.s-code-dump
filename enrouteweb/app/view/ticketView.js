Ext.define('rcrm.view.ticketView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.ticketView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'ticketList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'ticketList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Tickets List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'importTickets',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'importTickets',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Import Tickets</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }


        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'ticket',
        activeTab:'ticketList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'ticketList',
            xtype:'ticketListView'
        }        
        ]
    } 
    ]

});