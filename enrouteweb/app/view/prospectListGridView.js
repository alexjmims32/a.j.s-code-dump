Ext.define('rcrm.view.prospectListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.prospectListGridView',
    store: 'prospectListStore',
    autoScroll:true,
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Id',  
        dataIndex: 'prospectId',  
        flex: 1
    },

    {
        header: 'First Name',  
        dataIndex: 'firstName',  
        flex: 1
    },

    {
        header: 'Last Name',  
        dataIndex: 'lastName',  
        flex: 1
    },

    {
        header: 'Recruiter Id', 
        dataIndex: 'recruiterId',  
        flex: 1
    },
    {
        header: 'Recruiter Name', 
        dataIndex: 'recruiterName',  
        flex: 1
    },
    {
        header: 'Status', 
        dataIndex: 'status',  
        flex: 1
    },
    {
        header: 'Added Date', 
        dataIndex: 'addedDate',  
        flex: 1
    }
    ]
});