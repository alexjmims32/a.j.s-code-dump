Ext.define('rcrm.view.populationListView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.populationListView',
    store: 'populationListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Name',  
        dataIndex: 'populationTitle',  
        flex: 1
    },

    {
        header: 'Status',  
        dataIndex: 'status',  
        flex: 1
    },

    {
        header: 'Last Modified By',  
        dataIndex: 'lastModifiedBy',  
        flex: 1
    },

    {
        header: 'Last Modified On', 
        dataIndex: 'lastModifiedOn',  
        flex: 1
    },
    {
        xtype: 'actioncolumn',
        header:'Action',
        items: [
        {
            handler: function(view, rowIndex, colIndex, item, e) {
                var store=Ext.getStore('populationListStore');
                var records=store.getRange();
                var record=records[rowIndex];
                console.log('remove');
                store.remove(record);
            },
            align:'center',
            icon:'resources/images/delete2.png'
        }
        ],
        flex: 1
    }
    ],
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            text: '<b>Population List</b>',
            padding:'0 1020 0 0',
            disabled: true,
            align:'center'
        },
        {
            tooltip:'Add',
            icon:'resources/images/add2.png',
            action:'addPop'
        }]
    }]    
});