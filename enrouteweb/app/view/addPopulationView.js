Ext.define('rcrm.view.addPopulationView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addPopulationView',
    layout:{
        type:'vbox',
        align:'strech'
    },
    bodyStyle:{
      padding:'10 10 10 50'  
    },
    items:[
    {
       
        xtype:'label',
        //                text:'Samuel David (Id : 508)'
        padding:'0 0 20 0',
        html:'<b>Add Population</b>'
    },
    {
        xtype:'textfield',
        name:'populationName',
        id:'addPopName',
        width:'250',
        fieldLabel:'Population Name',
        value:'',
        padding:'5 0 0 0',
        border:false
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        border:false,
        padding:'5 0 0 0',
        items:[
        {
            xtype:'gridpanel',
            border:true,
            minHeight:200,
            //            title:'Dynamic Contacts',
            padding:'5',
            maxHeight:200,
            maxWidth:600,
            store:'popQueryStore',
            columns: [
            {
                header: 'Query Name',
                dataIndex:'queryName',
                flex:2
            },
            {
                header: 'Query',
                dataIndex:'query',
                flex:7
            },
            {
                xtype: 'actioncolumn',
                header:'Action',
                items: [
                {
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var store=Ext.getStore('popQueryStore');
                        var records=store.getRange();
                        var record=records[rowIndex];
                        console.log('remove');
                        store.remove(record);
                    },
                    align:'center',
                    icon:'resources/images/delete2.png'
                }
                ],
                flex:1
            }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Dynamic Contacts</b>',
                    padding:'0 435 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add',
                    icon:'resources/images/add2.png',
                    action:'addDynamicCnt'
                }]
            }]
        },
        {
            xtype:'gridpanel',
            border:true,
            //            title:'Fixed Contacts',
            padding:'5',
            minHeight:150,
            maxHeight:150,
            maxWidth:600,
            store:'popPersonStore',
            columns: [
            {
                header: 'ID',
                dataIndex:'id',
                flex:3
            },
            {
                header: 'First Name',
                dataIndex:'firstName',
                flex:4
            },
            {
                header: 'Last Name',
                dataIndex:'lastName',
                flex:4
            },
            {
                header: 'Email Address',
                dataIndex:'emailAddress',
                flex:15
            },
            {
                xtype: 'actioncolumn',
                header:'Action',
                items: [
                {
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var store=Ext.getStore('popPersonStore');
                        var records=store.getRange();
                        var record=records[rowIndex];
                        console.log('remove');
                        store.remove(record);
                    },
                    align:'center',
                    icon:'resources/images/delete2.png'
                }
                ],
                flex:1
            }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Fixed Contacts</b>',
                    padding:'0 450 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add',
                    icon:'resources/images/add2.png',
                    action:'addFixedCnt'
                }]
            }]
        }       
        ],
        flex:5
    },
       {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        padding:'10 20 20 20',
        border:false,
        items:[
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Save',
            action:'save',
            name:'save'
        },
        {
            xtype:'tbspacer',
            width:'10'
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Close',
            action:'close',
            name:'close'
        }
        ]
    }
    ]
});