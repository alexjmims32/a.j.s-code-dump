Ext.define('rcrm.view.verificationView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.verificationView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'creditHours',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'creditHours',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Credit Hours</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'cbcReview',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'cbcReview',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>CBC Review</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'verification',
        activeTab:'creditList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'creditList',
            xtype:'creditListView'
        },
        {
            id:'cbcList',
            xtype:'cbcListView'
        }
        ]
    } 
    ]

});