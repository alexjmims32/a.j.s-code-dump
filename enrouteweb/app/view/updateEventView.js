Ext.define('rcrm.view.updateEventView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateEventView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'15 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            id:'uevntTitle',
            //                text:'Samuel David (Id : 508)'
            padding:'0 0 30 10'
//            html:'<b>Colorado Open House (Event Id : 508)</b>'
        },
        {
            xtype:'textfield',
            name:'eventId',
            id:'uevntId',
            width:'250',
            fieldLabel:'Event Id',
            padding:'2',
            readOnly:true,
            border:false,
            hidden:true
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'uevntStatus',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            frame:true,
            queryMode: 'local',
            store:'eventStatusStore'
        },
        {
            xtype:'textfield',
            name:'eventStartDate',
            id:'uevntStartDate',
            width:'250',
            fieldLabel:'Event Start Date',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'eventEndDate',
            id:'uevntEndDate',
            width:'250',
            fieldLabel:'Event End Date',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'createdOn',
            id:'uevntCreatedOn',
            width:'250',
            fieldLabel:'Created On',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'createdBy',
            id:'uevntCreatedBy',
            width:'250',
            fieldLabel:'Created By',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'lastModifiedOn',
            id:'uevntModifiedOn',
            width:'250',
            fieldLabel:'Last Modified On',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'lastModifiedBy',
            id:'uevntModifiedBy',
            width:'250',
            fieldLabel:'Last Modified By',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'address1',
            id:'uevntAddress1',
            width:'250',
            fieldLabel:'Address Line 1',
            //            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'address2',
            id:'uevntAddress2',
            align:'right',
            width:'250',
            fieldLabel:'Address Line 2',
            //            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'city',
            id:'uevntCity',
            width:'250',
            fieldLabel:'City',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'state',
            id:'uevntState',
            width:'250',
            fieldLabel:'State',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'zipCode',
            id:'uevntZip',
            width:'250',
            fieldLabel:'Zip Code',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'country',
            id:'uevntCountry',
            width:'250',
            fieldLabel:'Country',
            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'10',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Cancel',
                action:'cancel',
                name:'cancel'
            }
            ]
        }
        ],
        flex:3
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'70 0 0 0',
        border:false,
        items:[
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false, 
            items:[
            {
                xtype:'gridpanel',
//                title:'Inventory List',
                border:true,
                minHeight:150,
                maxHeight:150,
                maxWidth:350,
                store:'evtInventoryStore',
                columns: [
                {
                    header: 'Title',
                    dataIndex:'inventoryTitle',
                    flex:2
                },
                {
                    header: 'Type',
                    dataIndex:'inventoryType',
                    flex:1
                },
                {
                    header: 'Count',
                    dataIndex:'count',
                    flex:1
                },
                {
                    xtype: 'actioncolumn',
                    header:'Action',
                    items: [
                    {
                        handler: function(view, rowIndex, colIndex, item, e) {
                            var store=Ext.getStore('eventInventoryStore');
                            var records=store.getRange();
                            var record=records[rowIndex];
                            console.log('remove');
                            store.remove(record);
                        },
                        align:'center',
                        icon:'resources/images/delete2.png'
                    }
                    ],
                    flex:1
                }
            
                ],
                dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Inventory List</b>',
                    padding:'0 230 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add a Inventory',
                    icon:'resources/images/add2.png',
                    action:'addInv'
                }]
            }]
            },
            {
                xtype:'gridpanel',
//                title:'Recruiters List',
                border:true,
                minHeight:150,
                maxHeight:150,
                padding:'0 0 0 10',
                maxWidth:300,
                store:'evtrecruiterListStore',
                columns: [
                {
                    header: 'Id',
                    dataIndex:'recruiterId',
                    flex:2
                },
                {
                    header: 'Name',
                    dataIndex:'recruiterName',
                    flex:4
                },
                {
                    xtype: 'actioncolumn',
                    header:'Action',
                    items: [
                    {
                        handler: function(view, rowIndex, colIndex, item, e) {
                            var store=Ext.getStore('recruiterListStore');
                            var records=store.getRange();
                            var record=records[rowIndex];
                            console.log('remove');
                            store.remove(record);
                        },
                        align:'center',
                        icon:'resources/images/delete2.png'
                    }
                    ],
                    flex:1
                }
                ],
                dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Recruiter List</b>',
                    padding:'0 170 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add a Recruiter',
                    icon:'resources/images/add2.png',
                    action:'addRect'
                }]
            }]
            }
            ]
        },
        {
            xtype:'gridpanel',
//            title:'Attendees List',
            border:true,
            minHeight:180,
            maxHeight:180,
            maxWidth:650,
            padding:'10 0 0 0',
            store:'attendeeListStore',
            columns: [
            {
                header: 'First Name',
                dataIndex:'firstName',
                flex:1
            },
            {
                header: 'Last Name',
                dataIndex:'lastName',
                flex:1
            },
            {
                header: 'Email',
                dataIndex:'email',
                flex:3
            },
            {
                header: 'Status',
                dataIndex:'status',
                flex:1
            },
            {
                xtype: 'actioncolumn',
                header:'Action',
                items: [
                {
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var store=Ext.getStore('attendeeListStore');
                        var records=store.getRange();
                        var record=records[rowIndex];
                        console.log('remove');
                        store.remove(record);
                    },
                    align:'center',
                    icon:'resources/images/delete2.png'
                }
                ],
                flex:1
            }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Attendees List</b>',
                    padding:'0 530 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add Attendee',
                    icon:'resources/images/add2.png',
                    action:'addAttendee'
                }]
            }]
        }
        ],
        flex:5
    }    
    ]
});