Ext.define('rcrm.view.addEventInventoryView',{
    extend:'Ext.window.Window',
    alias:'widget.addEventInventoryView',
    title:'Inventoty Item List',
    closable:false,
    bodyStyle:{
        padding:'20',
        background: '#FFFFFF'
    },
    items:[
        {
        xtype:'textfield',
        padding:'5',
        id:'addEvtInvId',
        fieldLabel:'Inventory Id',
        readOnly:true,
        hidden:true
    },
    {
        xtype:'combobox',
        id:'addEvtInvTitle',
        fieldLabel:'Inventory Item',
        padding:'5',
        displayField: 'inventoryTitle',
        valueField: 'inventoryTitle',
        editable: false,
        store:'actInvStore',
        listeners: {
            select: function(combo, record, index) {
                Ext.getCmp('addEvtInvType').setValue(record[0].data.inventoryType);
                Ext.getCmp('addEvtInvCount').setMaxValue(record[0].data.count);
                Ext.getCmp('addEvtInvCount').setVisible(true);
                Ext.getCmp('addEvtInvCount').setValue(1);
                Ext.getCmp('addEvtInvId').setValue(record[0].data.inventoryId);
                
            }
        }
    },
    {
        xtype:'textfield',
        padding:'5',
        id:'addEvtInvType',
        fieldLabel:'Inventory Type',
        readOnly:true,
        name:'title'
    },
    {
        xtype:'numberfield',
        padding:'5',
        fieldLabel:'Quantity',
        minValue:1,
//        value:1,
        name:'count',
        id:'addEvtInvCount'
    },
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'button',
            text:'Add',
            name:'add',
            padding:'2 30',
            action:'add'
        },
        {
            xtype:'tbspacer',
            width:'20'
        },
        {
            xtype:'button',
            text:'Close',
            name:'close',
            padding:'2 30',
            action:'close'
        }
        ]     
    }
    ]
});