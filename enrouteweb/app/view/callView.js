Ext.define('rcrm.view.callView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.callView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'callList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'callList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Call List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'navigateCall',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'navigateCall',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Navigate Calls</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'importCall',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'importCall',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Import Calls</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }


        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'call',
        activeTab:'callList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'callList',
            xtype:'callListView'
        },
        {
            id:'navigateCall',
            xtype:'navigateCallView'
        },
        {
            id:'callView',
            xtype:'updateCallView'
        }
        ]
    } 
    ]

});