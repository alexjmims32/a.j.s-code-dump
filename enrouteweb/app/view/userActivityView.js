Ext.define('rcrm.view.userActivityView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.userActivityView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#00427C'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 3,
            tableAttrs:{
                style: {
//                    width: '75%',
//                    height:'100%'
                },
                //                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'2',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Activity Start Date',
            name: 'startDate',
            id:'usrActsDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Activity',
            width:320,
            displayField: 'name',
            valueField: 'value',
            value:'ANY',
            id:'usrActivity',
            store:'userActivityDropdownStore',
            queryMode: 'local',
            editable: false 
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Id',
            id:'usrActStudentId'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Activity End Date',
            name: 'endDate',
            id:'usrActeDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'User',
            padding:'5',
            displayField: 'userId',
            valueField: 'userId',
            id:'usrActUserId',
            value:'ANY',
            width:320,
            editable: false,
            queryMode: 'local',
            store:'userListStore'
        },
        {
            xtype:'label'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'userActivityListView'
    }            
        
    ]

});