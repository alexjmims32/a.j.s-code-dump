Ext.define('rcrm.view.letterListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.letterListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
                style: {
                //                    width: '100%',
                //                    height:'100%'
                },
                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            padding:'5',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'ltrsDate',
            maxValue: new Date()
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Print From',
            name: 'printFrom',
            id:'ltrpfDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Message Status',
            id:'ltrStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:'letterStatusStore',
            queryMode: 'local'
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Student Id',
            id:'ltrstudentId'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'ltreDate',
            maxValue: new Date()
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Print To',
            name: 'printTo',
            id:'ltrptDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Letter Type',
            id:'ltrType',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:['ADMISSION LETTER', 'STUDENT POSTCARD', 'VLWP'],
            queryMode: 'local'
        },
        {
            xtype:'label',
            padding:'5'
                    
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'letterListGridView'
    }            
        
    ]

});