Ext.define('rcrm.view.todayScorecardView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.todayScorecardView',
    store: 'userScorecardStore',
    title: 'Today\'s Scorecard',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Date',  
        dataIndex: 'activityDate',  
        flex: 2
    },

    {
        header: 'Activity',  
        dataIndex: 'activityType',  
        flex: 4
    },

    {
        header: 'Count',
        dataIndex: 'activityCount',  
        flex: 1
    },

    {
        header: 'Score',
        dataIndex: 'activityScore',  
        flex: 1
    }    
    ]
});