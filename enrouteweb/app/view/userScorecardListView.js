Ext.define('rcrm.view.userScorecardListView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.userScorecardListView',
    store: 'userScorecardStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
        {
        header: 'User',  
        dataIndex: 'userId',  
        flex: 1
    },
    {
        header: 'Date',  
        dataIndex: 'activityDate',  
        flex: 2
    },

    {
        header: 'Activity',  
        dataIndex: 'activityType',  
        flex: 4
    },

    {
        header: 'Count',
        dataIndex: 'activityCount',  
        flex: 1
    },

    {
        header: 'Score',
        dataIndex: 'activityScore',  
        flex: 1
    }    
    ]
});