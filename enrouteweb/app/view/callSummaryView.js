Ext.define('rcrm.view.callSummaryView',{
    extend:'Ext.chart.Chart',
    alias:'widget.callSummaryView',
    animate:true,
    store:'callSummaryStore',
//    theme:'Base:gradients',
    title:'Call Summary',
    series:[{
        type:'pie',
        field:'count',
        showInLegend:true,
        tips: {
            trackMouse: true,
            width: 140,
            height: 28,
            renderer: function(storeItem, item) {
                var store=Ext.getStore('callSummaryStore');
                var total = 0;
                store.each(function(rec) {
                    total += parseInt(rec.get('count'));
                });
                this.setTitle(storeItem.get('status') + ': ' + Math.round(parseInt(storeItem.get('count')) / total * 100)+ '%'+'('+storeItem.get('count')+')');
                
            }
        },
        highlight: {
            segment: {
                margin: 20
            }
        },
        label: {
            field: 'status',
            display: 'rotate',
            contrast: true,
            font: '12px Arial'
        }
    }]
});