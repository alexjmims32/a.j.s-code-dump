Ext.define('rcrm.view.updateTemplateView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateTemplateView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            //                text:'Samuel David (Id : 508)'
            padding:'0 0 20 10',
            html:'<b>Template Summary</b>'
        },
        {
           xtype:'checkboxfield',
           fieldLabel:'Status',
           labelAlign:'left',
           name:'status',
           id:'utStatus'
        },
        {
            xtype:'textfield',
            name:'createdBy',
            id:'utCreatedBy',
            width:'250',
            fieldLabel:'Created By',
            padding:'2',
            readOnly:true,
            border:false
        },
         {
            xtype:'textfield',
            name:'createdOn',
            id:'utCreatedOn',
            width:'250',
            fieldLabel:'Created On',
            padding:'2',
            readOnly:true,
            border:false
        },
         {
            xtype:'textfield',
            name:'modifiedBy',
            id:'utModifiedBy',
            width:'250',
            fieldLabel:'Modified By',
            padding:'2',
            readOnly:true,
            border:false
        },
         {
            xtype:'textfield',
            name:'modifeidOn',
            id:'utModifiedOn',
            width:'250',
            fieldLabel:'Modified On',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'50',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Close',
                action:'close',
                name:'close'
            }
            ]
        }
        ],
        flex:3
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'left'
        },
        border:false,
        padding:'50 350 0 0',
        items:[
        {
            xtype:'combobox',
            fieldLabel:'Category',
            id:'utCategory',
            displayField: 'name',
            valueField: 'value',
            editable: false,
            queryMode: 'local',
            store:'templateCategoryStore'
        },
        {
            xtype:'textfield',
            name:'templateId',
            id:'utTemplateId',
            width:'250',
            fieldLabel:'Template Id',
            readOnly:true,
            hidden:true,
            padding:'2'
        },
         {
            xtype:'textfield',
            name:'templateName',
            id:'utTemplateName',
            width:'250',
            fieldLabel:'Template Name',
            padding:'2'
        },
         {
            xtype:'textareafield',
            name:'emailTemplate',
            id:'utEmailTemplate',
            grow:true,
            fieldLabel:'Email Template',
            rows:'50',
            height:120,
            width:'400'
        }
    ]
    }
    ]
});