Ext.define('rcrm.view.topRecruiterView',{
    extend:'Ext.chart.Chart',
    alias:'widget.topRecruiterView',
    animate:true,
    store:'topRecruiterStore',
//    theme:'Base:gradients',
    title:'Top Recruiters',
    series:[{
        type:'pie',
        field:'points',
        showInLegend:true,
        tips: {
            trackMouse: true,
            width: 140,
            height: 28,
            renderer: function(storeItem, item) {
                var store=Ext.getStore('topRecruiterStore');
                var total = 0;
                store.each(function(rec) {
                    total += parseInt(rec.get('points'));
                });
                this.setTitle(storeItem.get('name') + ': ' + Math.round(parseInt(storeItem.get('points')) / total * 100)+ '%'+'('+storeItem.get('points')+')');
                
            }
        },
        highlight: {
            segment: {
                margin: 20
            }
        },
        label: {
            field: 'name',
            display: 'rotate',
            contrast: true,
            font: '12px Arial'
        }
    }]
});