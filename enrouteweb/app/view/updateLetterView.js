Ext.define('rcrm.view.updateLetterView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateLetterView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            id:'ltrvTitle',
            //                text:'Samuel David (Id : 508)'
            padding:'0 0 20 10'
//            html:'<b>Walter Biel (Message Id: 204)</b>'
        },
        {
            xtype:'textfield',
            name:'category',
            id:'ltrvCategory',
            width:'250',
            fieldLabel:'Category',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'addedDate',
            id:'ltrvAddedDate',
            width:'250',
            fieldLabel:'Added Date',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        
        {
            xtype:'textfield',
            name:'printStatus',
            id:'ltrvStatus',
            width:'250',
            fieldLabel:'Print Status',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'printDate',
            id:'ltrvpDate',
            width:'250',
            fieldLabel:'Print Date',
            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textareafield',
            name:'message',
            id:'ltrvMessage',
            grow:true,
            fieldLabel:'Message',
            labelAlign:'top',
            rows:'10',
            width:'350',
            readOnly:true
        },
        {
            xtype:'button',
            frame:true,
            padding:'2 50',
            text:'Cancel',
            action:'cancel',
            name:'cancel'
        }
        ]
    }
    ]
});



