Ext.define('rcrm.view.updateCallView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateCallView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            id:'callViewtitle',
            //                text:'Samuel David (Id : 508)'
            padding:'0 0 40 10'
//            html:'<b>Samule David (Id: 101)</b>'
        },
        {
            xtype:'textfield',
            name:'callId',
            width:'250',
            id:'cvCallId',
            fieldLabel:'Call Id',
            readOnly:true,
            hidden:true,
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'callReason',
            width:'250',
            id:'cvCReason',
            fieldLabel:'Call Reason',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'homeNumber',
            width:'250',
            id:'cvHomeNumber',
            fieldLabel:'Home Number',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'email',
            id:'cvEmail',
            width:'250',
            fieldLabel:'Email',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'cvStatus',
            displayField: 'name',
            valueField: 'value',
            editable: false,
            queryMode: 'local',
            store:'callStatusStore'
        },
        {
            xtype:'combobox',
            fieldLabel:'Owner',
            id:'cvOwner',
            displayField: 'userId',
            valueField: 'userId',
            editable: false, 
            queryMode: 'local',
            store:'userListStore'
        },
        {
            xtype:'textfield',
            name:'contactDate',
            id:'cvContactDate',
            width:'250',
            fieldLabel:'Contact Date',
            readOnly:true,
            padding:'2'
        },
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'10',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Cancel',
                action:'cancel',
                name:'cancel'
            }
            ]
        }
        ],
        flex:2
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        border:false,
        padding:'50',
        items:[
        {
            xtype:'textareafield',
            name:'information',
            grow:true,
            id:'cvInfStudent',
            fieldLabel:'Information For Student',
            rows:'5',
            width:'400',
            readOnly:true
        },
        {
            xtype:'textareafield',
            grow:true,
            name:'comment',
            id:'cvComment',
            fieldLabel:'Comment',
            rows:'5',
            width:'400'
        },
        {
            xtype:'label',
            padding:'30 0 5 0',
            html:'<b>Contact OutCome</b>'
        },
        {
            xtype:'form',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            padding:'2 2',
            items:[
            {
                xtype:'checkboxfield',
                id:'cvCntByPhone',
                boxLabel:'Contact By Phone'
            },
            {
                xtype:'checkboxfield',
                id:'cvCntByEmail',
                boxLabel:'Contact By Email'
            },
            {
                xtype:'checkboxfield',
                id:'cvLeftMsg',
                boxLabel:'Left Message'
            },
            {
                xtype:'checkboxfield',
                id:'cvNoCntPossible',
                boxLabel:'No Contact Possible'
            }
        ]
        }
        ],
        flex:4
    }
    ]
});