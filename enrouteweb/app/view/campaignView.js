Ext.define('rcrm.view.campaignView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.campaignView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C'
        //            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        {
            xtype: 'component',
            itemId: 'email',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'email',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Email Campaign</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'templates',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'template',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Templates</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'population',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'population',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Population</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }


        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        activeTab:'emailList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'emailList',
            xtype:'emailListView'
        },
        {
            id:'emailView',
            xtype:'updateEmailView'
        },
        {
            id:'editEmailView',
            xtype:'templateSelectionView'
        },
        {
            id:'popSelectView',
            xtype:'populationSelectionView'
        },
         {
            id:'emailSchView',
            xtype:'emailScheduleView'
        },
        {
            id:'templateList',
            xtype:'templateListView'
        },
        {
            id:'addTempView',
            xtype:'addTemplateView'
        },
        {
            id:'templateView',
            xtype:'updateTemplateView'
        },
        {
            id:'populationList',
            xtype:'populationListView'
        },
        {
            id:'populationView',
            xtype:'updatePopulationView'
        },
        {
            id:'addPopView',
            xtype:'addPopulationView'
        }
        ]
    } 
    ]

});