Ext.define('rcrm.view.addEventAttendeeView',{
    extend:'Ext.window.Window',
    alias:'widget.addEventAttendeeView',
    title:'Add Attendee',
    closable:false,
    bodyStyle:{
        padding:'20',
        background: '#FFFFFF'
    },
    items:[
    {
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            layout:{
                type:'vbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendFName',
                fieldLabel:'First Name'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendLName',
                fieldLabel:'Last Name'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendMName',
                fieldLabel:'Middle Name'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendType',
                fieldLabel:'Type'
            },
            {
                xtype:'combobox',
                id:'addEvtAtendStatus',
                fieldLabel:'Status',
                padding:'5',
                displayField: 'name',
                valueField: 'value',
                editable: false,
                store:['YES', 'NO']
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendEmail',
                fieldLabel:'Email'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendMobile',
                fieldLabel:'Mobile'
            }      
            ]
        },
        {
            layout:{
                type:'vbox',
                align:'strech'
            },
            border:false,
            items:[
                        
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendAddr1',
                fieldLabel:'Address 1'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendAddr2',
                fieldLabel:'Address 2'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendCity',
                fieldLabel:'City'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendState',
                fieldLabel:'State'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendZip',
                fieldLabel:'ZIP'
            },
            {
                xtype:'textfield',
                padding:'5',
                id:'addEvtAtendCountry',
                fieldLabel:'Country'
            }
            ]
        }
        ]
    },
        
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'button',
            text:'Add',
            name:'add',
            padding:'2 30',
            action:'addAttendee'
        },
        {
            xtype:'tbspacer',
            width:'20'
        },
        {
            xtype:'button',
            text:'Close',
            name:'close',
            padding:'2 30',
            action:'close'
        }
        ]     
    }
    ]
});