Ext.define('rcrm.view.messageView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.messageView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'messageList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'messageList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Message List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        activeTab:'messageList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'messageList',
            xtype:'messageListView'
        },
        {
            id:'messageView',
            xtype:'updateMessageView'
        }
        ]
    } 
    ]

});