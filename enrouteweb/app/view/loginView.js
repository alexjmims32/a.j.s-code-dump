Ext.define('rcrm.view.loginView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.loginView',
    activeItem: 'loginView',
    layout: {
        type: 'table',
        columns: 1,
        tableAttrs:{
            style: {
                width: '100%',
                height:'100%'
            },
            align: 'center',
            valign: 'middle'
        },
        trAttrs:{
            align: 'center'
        //            valign: 'middle'
        }
    },
    //    frame:true,
//    defaults: {
//        bodyStyle: 'padding:20px'
//    },    
    items:[{
        frame:true,
        width:'300',
        formId: 'loginForm',
        xtype: 'form',
        items:[
        {
            xtype:'panel',
            bodyStyle: {
                background: '#00427C',
                padding: '5 5 5 5'
            },
            items:[
            {
                xtype:'label',
                style:'color:#FEB210;font-size:15pt',
                html:'<b>eNroute RCM</b>'
            }
            ]
        },{
            xtype:'form',
//            frame:true,
            bodyStyle: {
                              background: '#8A0829'
//                padding: '5px'
            },
            items:[{
                //        frame:true,
                bodyStyle: {
                //                background: '#81BEF7',
                padding: '15 5 5 0'
            },
                width:'300',
                formId: 'admin-loginform',
                xtype: 'form',
                items:[{
                    xtype:'textfield',
                    fieldLabel:'User Name',
                    value:'rcmdir',
                    id:'username',
                    allowBlank:false           
                },
                {
                    xtype:'textfield',
                    inputType:'password',
                    fieldLabel:'Password',
                    value:'rcmdir',
                    id:'password', 
                    name:'password',
                    allowBlank:false
                },
                {
                    xtype:'button',
                    frame:true,
                    padding:'2 10',
                    text:'Sign In',
                    action:'signin',
                    name:'signin'
                }
                ]
            }
            ]
            }
        ]

    }]
});