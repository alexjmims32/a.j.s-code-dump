Ext.define('rcrm.view.prospectListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.prospectListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        id:'prospectSearch',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
//                style: {
//                    width: '100%',
//                    height:'100%'
//                },
                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Start Date',
            padding:'5',
            name: 'startDate',
            id:'prospsDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            fieldLabel:'First Name',
            padding:'5',
            id:'prospfName'
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            padding:'5',
            id:'prospStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:'statusStore'
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Id',
            id:'prospsId'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'prospeDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Last Name',
            id:'prosplName'
        },
        {
            xtype:'combobox',
            fieldLabel:'Recruiter',
            editable: false, 
            padding:'5',
            id:'prospRecruiter',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            queryMode: 'local',
            store:'recruiterStore'
        },
        {
            xtype:'label'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'prospectListGridView'
    }            
        
    ]

});