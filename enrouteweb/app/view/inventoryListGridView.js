Ext.define('rcrm.view.inventoryListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.inventoryListGridView',
    store: 'inventoryListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Title',  
        dataIndex: 'inventoryTitle',  
        flex: 1
    },{
        header: 'Type',  
        dataIndex: 'inventoryType',  
        flex: 1
    },{
        header: 'Count',
        dataIndex: 'count',  
        flex: 1
    },{
        header: 'Status',
        dataIndex: 'status',  
        flex: 1
    },{
        header: 'Added Date', 
        dataIndex: 'createdDate',  
        flex: 1
    },{
        header: 'Last Modified By', 
        dataIndex: 'modifiedBy',  
        flex: 1
    },{
        header: 'Last Modified On', 
        dataIndex: 'modifiedDate',  
        flex: 1
    },{
        header: 'Action', 
        flex: 1
    }
    ]
});