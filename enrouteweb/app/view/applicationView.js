Ext.define('rcrm.view.applicationView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.applicationView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'appList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'appList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Application List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'navigateapp',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'navigateapp',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Navigate Application</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'application',
        activeTab:'applicationList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'applicationList',
            xtype:'applicationListView'
        },
        {
            id:'navigateApplication',
//            xtype:'navigateApplicationView'
        }
        ]
    } 
    ]

});