Ext.define('rcrm.view.addEventView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addEventView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            padding:'0 0 20 10',
            html:'<b>Add Event</b>'
        },
        {
            xtype:'textfield',
            name:'eventName',
            id:'addevtName',
            width:'250',
            fieldLabel:'Event Name',
            padding:'2'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Event Start Date',
            name: 'estartDate',
            id:'addevtStDate',
            minValue: new Date()
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Event End Date',
            name: 'eendDate',
            id:'addevtendDate',
            minValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            displayField: 'name',
            valueField: 'value',
            editable: false,
            store:'eventStatusStore',
            queryMode: 'local',
            id:'addevtStatus'
        },
        {
            xtype:'textfield',
            name:'rmarks',
            id:'addevtRemarks',
            width:'250',
            fieldLabel:'Remarks',
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'address1',
            id:'addevtAdd1',
            width:'250',
            fieldLabel:'Address Line 1',
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'address2',
            id:'addevtAdd2',
            width:'250',
            fieldLabel:'Address Line 2',
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'city',
            id:'addevtCity',
            width:'250',
            fieldLabel:'City',
            padding:'2'
        },{
            xtype:'textfield',
            name:'state',
            id:'addevtState',
            width:'250',
            fieldLabel:'State',
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'zipCode',
            id:'addevtZip',
            width:'250',
            fieldLabel:'Zip Code',
            padding:'2'
        },
        {
            xtype:'textfield',
            name:'country',
            id:'addevtCountry',
            width:'250',
            fieldLabel:'Country',
            padding:'2'
        },      
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'10',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Reset',
                action:'reset',
                name:'reset'
            }
            ]
        }
        ],
        flex:3
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'gridpanel',
            border:true,
            minHeight:180,
            padding:'50 0 5 0',
            maxHeight:180,
            maxWidth:600,
            store:'eventInventoryStore',
            columns: [
            {
                header: 'Title',
                dataIndex:'inventoryTitle',
                flex:2
            },
            {
                header: 'Type',
                dataIndex:'inventoryType',
                flex:3
            },
            {
                header: 'Count',
                dataIndex:'count',
                flex:1
            },
            {
                xtype: 'actioncolumn',
                header:'Action',
                items: [
                {
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var store=Ext.getStore('eventInventoryStore');
                        var records=store.getRange();
                        var record=records[rowIndex];
                        console.log('remove');
                        store.remove(record);
                    },
                    align:'center',
                    icon:'resources/images/delete2.png'
                }
                ],
                flex:1
            }
            ],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Inventory List</b>',
                    padding:'0 480 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add a Inventory',
                    icon:'resources/images/add2.png',
                    action:'addInv'
                }]
            }]
        },
        {
            xtype:'gridpanel',
            border:true,
            minHeight:150,
            maxHeight:150,
            maxWidth:600,
            store:'recruiterListStore',
            columns: [
            {
                header: 'Id',
                dataIndex:'recruiterId',
                flex:2
            },
            {
                header: 'Name',
                dataIndex:'recruiterName',
                flex:4
            },
            {
                xtype: 'actioncolumn',
                header:'Action',
                items: [
                {
                    handler: function(view, rowIndex, colIndex, item, e) {
                        var store=Ext.getStore('recruiterListStore');
                        var records=store.getRange();
                        var record=records[rowIndex];
                        console.log('remove');
                        store.remove(record);
                    },
                    align:'center',
                    icon:'resources/images/delete2.png'
                }
                ],
                flex:1
            }            
            ],
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    text: '<b>Recruiter List</b>',
                    padding:'0 480 0 0',
                    disabled: true,
                    align:'center'
                },
                {
                    tooltip:'Add a Recruiter',
                    icon:'resources/images/add2.png',
                    action:'addRect'
                }]
            }]

        }        
        ],
        flex:5
    }
    ]
});