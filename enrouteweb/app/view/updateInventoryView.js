Ext.define('rcrm.view.updateInventoryView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateInventoryView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            id:'ivnUpTitle',
            padding:'0 0 40 10'
//            html:'<b>University Pens</b>'
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'invUpStatus',
            width:'350',
//            displayField: 'name',
//            valueField: 'value',
            editable: false ,
            queryMode: 'local',
            store:['ACTIVE','INACTIVE']
            
        },
        {
            xtype:'textfield',
            name:'description',
            id:'invUpDesc',
            width:'350',
            fieldLabel:'Description',
            value:'',
            padding:'2',
            border:false
        },
        {
            xtype:'textfield',
            name:'inventoryId',
            id:'invUpId',
            width:'350',
            fieldLabel:'Inventory Id',
            hidden:true,
            value:'',
            padding:'2',
            border:false
        },
         {
            xtype:'textareafield',
            name:'remarks',
            id:'invUpRemarks',
            grow:true,
            fieldLabel:'Remarks',
            rows:'10',
            width:'350',
            minHeight:'100',
            readOnly:true
        },
        {
            xtype:'combobox',
            fieldLabel:'Inventory Type',
            id:'invUpType',
            displayField: 'name',
            valueField: 'value',
            editable: false,
            queryMode: 'local',
            store:'inventoryTypeStore'
        },
        {
            xtype:'textfield',
            name:'availableItems',
            id:'invUpAvailabel',
//            width:'350',
            fieldLabel:'Available Items',
            value:'',
            padding:'2',
            border:false
        },
        
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'20 20 20 20',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Cancel',
                action:'cancel',
                name:'cancel'
            }
            ]
        }
        ],
        flex:2
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        border:false,
        padding:'50 0 0 0',
        items:[
            {
              xtype:'label',
              html:'Events Where Inventory is Included:'
//              padding:'100 250 0 0'
            },            
        {
            xtype:'gridpanel',
            border:true,
            minHeight:200,
            padding:'10 50 0 0',
            maxHeight:200,
            maxWidth:650,
            store:'inventoryEventStore',
            columns: [
            {
                header: 'Title',
                dataIndex:'eventTitle',
                flex:1
            },
            {
                header: 'Count',
                dataIndex:'inventoryCount',
                flex:1
            },
            {
                header: 'Start Date',
                dataIndex:'eventStartDate',
                flex:1
            },
            {
                header: 'End Date',
                dataIndex:'eventEndDate',
                flex:1
            },
            {
                header: 'Status',
                dataIndex:'status',
                flex:3
            }
            ]
        }
        ]
    }
    ]
});