Ext.define('rcrm.view.updateEmailView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateEmailView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
        {
            xtype:'panel',
            layout:{
                type:'vbox',
                align:'strech'
            },
            padding:'25 5 5 20',
            border:false,
            items:[
                {
                    xtype:'label',
                    //                text:'Samuel David (Id : 508)'
                    padding:'0 0 20 10',
                    html:'<b>Campaign Summary</b>'
                },
                {
                    xtype:'textfield',
                    name:'title',
                    width:'250',
                    fieldLabel:'Campaign Id',
                    id:'evCmpId',
                    padding:'2',
                    readOnly:true,
                    hidden:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'title',
                    width:'250',
                    fieldLabel:'Campaign Title',
                    id:'evCmpTitle',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'subject',
                    width:'250',
                    fieldLabel:'Email Subject',
                    id:'evEmailSubject',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'templateName',
                    id:'evTempName',
                    width:'250',
                    fieldLabel:'Template Name',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'scheduleDate',
                    id:'evSchDate',
                    width:'250',
                    fieldLabel:'Scheduled Date',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'createdBy',
                    id:'evCreatedBy',
                    width:'250',
                    fieldLabel:'Created By',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'created Date',
                    id:'evCreatedDate',
                    width:'250',
                    fieldLabel:'Created Date',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'status',
                    id:'evStatus',
                    width:'250',
                    fieldLabel:'Status',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
        
                {
                    xtype:'label',
                    padding:'35 0 5 0',
                    html:'<b>Email Summary</b>'
                },
                {
                    xtype:'textfield',
                    name:'totalEmails',
                    id:'evTotalEmails',
                    width:'250',
                    fieldLabel:'Total Emails',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'sent',
                    id:'evSent',
                    width:'250',
                    fieldLabel:'Sent',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'failed',
                    id:'evFailed',
                    width:'250',
                    fieldLabel:'Failed',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'textfield',
                    name:'inProgress',
                    id:'evInprogress',
                    width:'250',
                    fieldLabel:'In Progress',
                    padding:'2',
                    readOnly:true,
                    border:false
                },
                {
                    xtype:'panel',
                    layout:{
                        type:'hbox',
                        align:'strech'
                    },
                    padding:'10',
                    border:false,
                    items:[
//                        {
//                            xtype:'button',
//                            frame:true,
//                            padding:'2 50',
//                            text:'Edit',
//                            action:'edit',
//                            name:'edit'
//                        },
//                        {
//                            xtype:'tbspacer',
//                            width:'10'
//                        },
                        {
                            xtype:'button',
                            frame:true,
                            padding:'2 50',
                            text:'Close',
                            action:'close',
                            name:'close'
                        }
                    ]
                }
            ],
            flex:3
        },
        {
            xtype:'panel',
            layout:{
                type:'vbox',
                align:'left'
            },
            border:false,
            padding:'50',
            items:[
                {
                    xtype:'gridpanel',
                    border:true,
                    title:'Queries',
                    minHeight:150,
                    maxHeight:150,
                    maxWidth:600,
                    id:'upEmailQueries',
                    name:'upEmailQueries',
                    store:'emailQueryStore',
                    columns: [
                        {
                            header: 'Query Name',
                            dataIndex:'queryName',
                            flex:1
                        },
                        {
                            header: 'Query',
                            dataIndex:'query',
                            flex:3
                        },
                        {
                            xtype:'gridcolumn',
                            id:'ueaction',
//                            dataIndex:'query',
                            header: 'Action',
                            renderer: function() {
                                return '<a href="#">'+ 'VIEW'  +'</a>';
                            },
                            flex:1
                        }
                    ]
                },
                {
                    xtype:'label',
                    padding:'10 0 5 0'
                },
                {
                    xtype:'gridpanel',
                    border:true,
                    title:'Recipients',
                    minHeight:250,
                    maxHeight:250,
                    maxWidth:600,
                    store:'emailStudentStore',
                    columns: [
                        {
                            header: 'First Name',
                            dataIndex:'firstName',
                            flex:1
                        },
                        {
                            header: 'Last Name',
                            dataIndex:'lastName',
                            flex:1
                        },
                        {
                            header: 'Email Address',
                            dataIndex:'emailAddress',
                            flex:2
                        },
                        {
                            header: 'Status',
                            dataIndex:'status',
                            flex:1
                        }
                    ]
                }
            ]
        }
    ]
});