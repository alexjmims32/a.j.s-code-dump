Ext.define('rcrm.view.messageListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.messageListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 3,
            tableAttrs:{
                style: {
//                    width: '75%',
//                    height:'100%'
                },
                //                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'2',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            padding:'5',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'msgsDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Id',
            id:'msgStudentId',
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'First Name',
            id:'msgfName'
        //            id:'firstName'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'msgeDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Message Status',
            id:'msgStatus',
            displayField: 'name',
            valueField: 'value',
            value:'ANY',
            editable: false,
            store:'messageStatusStore',
            queryMode: 'local'
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Last Name',
            id:'msglName'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'messageListGridView'
    }            
        
    ]

});