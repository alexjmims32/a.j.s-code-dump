Ext.define('rcrm.view.ticketListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.ticketListGridView',
    //    store: 'ticketListStore',
    autoScroll:true,
    minHeight:420,
    bodyBorder:false,
    titleAlign:'center',
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Ticket#',  
        //        dataIndex: 'prospectId',  
        flex: 1
    },

    {
        header: 'Student Id',  
        flex: 1
    },

    {
        header: 'Student Name',  
        flex: 1
    },

    {
        header: 'Status', 
        flex: 1
    },
    {
        header: 'Created On', 
        flex: 1
    },
    {
        header: 'Email', 
        flex: 1
    },
    {
        header: 'Owner', 
        flex: 1
    },
    {
        header: 'Application Status', 
        flex: 1
    },
    {
        header: 'Summary', 
        flex: 1
    }
    ]
});