Ext.define('rcrm.view.documentView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.documentView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'documentList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'documentList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Document List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
         {
            xtype: 'component',
            itemId: 'documentNavigate',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'docNavigate',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Navigate Document</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
         {
            xtype: 'component',
            itemId: 'docErrors',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'docErrors',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Document Errors</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        activeTab:'documentList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'documentList',
            xtype:'documentListView'
        },
        {
            id:'docNavigate',
//            xtype:'NavigateDocView'
        },
        {
            id:'docError',
//            xtype:'documentErrorView'
        }
        ]
    } 
    ]

});