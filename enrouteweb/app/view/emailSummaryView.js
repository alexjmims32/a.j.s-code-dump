Ext.define('rcrm.view.emailSummaryView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.emailSummaryView',
    id:'emailSummary',
    title: 'Email Summary',
    store: 'emailSummaryStore',

    columns: [
    {
        header: 'Email Title',  
        dataIndex: 'emailTitle',  
        flex: 4
    },

    {
        header: 'In Progress',  
        dataIndex: 'inProgressCount',  
        flex: 1
    },

    {
        header: 'Sent',  
        dataIndex: 'sentCount',  
        flex: 1
    },

    {
        header: 'Failed', 
        dataIndex: 'failedCount', 
        flex: 1
    }
    ]
});