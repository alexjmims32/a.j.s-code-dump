Ext.define('rcrm.view.callListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.callListGridView',
    store: 'callListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Id',  
        dataIndex: 'studentID',  
        flex: 1
    },

    {
        header: 'Name', 
        dataIndex: 'studentName',  
        flex: 1
    },

    {
        header: 'Call Reason',  
        dataIndex: 'callReason',  
        flex: 3
    },

    {
        header: 'Message From', 
        dataIndex: 'messageFrom',  
        flex: 1
    },
    {
        header: 'Message Date', 
        dataIndex: 'messageRequestedDate',  
        flex: 1
    },
    {
        header: 'Status', 
        dataIndex: 'status',  
        flex: 1
    },
    {
        header: 'Owner',
        dataIndex: 'callOwner',  
        flex: 1
    },
    {
        header: 'Call Date', 
        dataIndex: 'callDate',  
        flex: 1
    }

    ]
});