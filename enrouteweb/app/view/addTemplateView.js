Ext.define('rcrm.view.addTemplateView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addTemplateView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 20',
        items:[
        {
            xtype:'label',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Create New Template</u>'
        },{
            xtype:'combobox',
            width:300,
            fieldLabel:'Template Category',
            frame:true,
            editable: false,
            id:'addTempCategory',
            displayField: 'name',
            valueField: 'value',
            queryMode: 'local',
            store:'templateCategoryStore'
        },{
            xtype:'textfield',
            fieldLabel:'Template Name',
            id:'addTempName',
            width:300,
            allowBlank:false           
        },{
            xtype:'textareafield',
            name:'emailTemplate',
            id:'addTempHtml',
            grow:true,
            fieldLabel:'Email Template',
            rows:'50',
            height:200,
            width:'600'
        },{
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Save',
                name:'save',
                padding:'2 60',
                action:'save'
            },{
                xtype:'tbspacer',
                width:'10'
            },{
                xtype:'button',
                text:'Cancel',
                name:'cancel',
                padding:'2 60',
                action:'close'
            }
            ]     
        }
       
        ]
    }
    ]
});