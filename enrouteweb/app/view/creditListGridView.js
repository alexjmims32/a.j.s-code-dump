Ext.define('rcrm.view.creditListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.creditListGridView',
    //    store: 'prospectListStore',
    autoScroll:true,
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Student Id',  
        flex: 1
    },

    {
        header: 'Student Name',  
        flex: 1
    },

    {
        header: 'Credit Hours',  
        flex: 1
    },

    {
        header: 'Has 30', 
        flex: 1
    },
    {
        header: 'Term', 
        flex: 1
    },
    {
        header: 'Status', 
        flex: 1
    },
    {
        header: 'Action By', 
        flex: 1
    },
    {
        header: 'Action Date', 
        flex: 1
    }

    ]
});