Ext.define('rcrm.view.addPopulationContactView',{
    extend:'Ext.window.Window',
    alias:'widget.addPopulationContactView',
    title:'Add Contact',
    closable:false,
    bodyStyle:{
        padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'label',
            text:'Select Date:'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'From',
            name: 'fromDate',
            id:'addPopCntFrom',
            maxValue: new Date()
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'To',
            name: 'startDate',
            id:'addPopCntTo',
            maxValue: new Date()
        }
        ]
    },
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'textfield',
            id:'addPopCntId',
            fieldLabel:'ID'
        },
        {
            xtype:'textfield',
            id:'addPopCntFName',
            fieldLabel:'First Name'
        },
        {
            xtype:'textfield',
            id:'addPopCntLName',
            fieldLabel:'Last Name'
        },
        {
            xtype:'tbspacer',
            width:'40'
        },
        {
            xtype:'button',
            text:'Search',
            name:'search',
            padding:'2 30',
            action:'search'
        },
        {
            xtype:'tbspacer',
            width:'5'
        },
        {
            xtype:'button',
            text:'Reset',
            name:'reset',
            padding:'2 30',
            action:'reset'
        }
        ]
    },
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:true,
        items:[
        {
            xtype:'gridpanel',
            border:true,
            //            title:'contacts',
            minHeight:120,
            //            maxHeight:120,
            maxWidth:950,
            columns: [
            {
                header: 'ID',
                flex:2
            },
            {
                header: 'First Name',
                flex:4
            },
            {
                header: 'Last Name',
                flex:4
            },
//            {
//                xtype: 'checkcolumn',
////                text: 'Indoor?',
////                dataIndex: 'indoor',
//                width: 55
//            }
            ],
        //            flex:3
        }
        ]
    },
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'button',
            text:'Add',
            name:'add',
            padding:'2 30',
            action:'add'
        },
        {
            xtype:'tbspacer',
            width:'20'
        },
        {
            xtype:'button',
            text:'Close',
            name:'close',
            padding:'2 30',
            action:'close'
        }
        ]     
    }
    ]
});