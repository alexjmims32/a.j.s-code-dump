Ext.define('rcrm.view.emailScheduleView',{
    extend:'Ext.panel.Panel',
    alias:'widget.emailScheduleView',
    
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 20',
        items:[
        {
            xtype:'label',
            id:'editEmailTitleES',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Edit Email</u>'
        },{
            xtype:'label',
            id:'addEmailTitleES',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Add Email</u>'

        },{
            xtype: 'radiogroup',
            columns: 1,
            vertical: true,
            items: [
            {
                boxLabel: 'Draft', 
                id:'emailDraft',
                name: 'rb', 
                inputValue: 'draft'
            },{
                boxLabel: 'Send Now', 
                id:'emailSendNow',
                name: 'rb', 
                inputValue: 'sendNo'
            },{
                boxLabel: 'Schedule for', 
                id:'emailSchedule',
                name: 'rb', 
                inputValue: 'schedule'
            }         
            ]
        },{
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Date',
            id:'scheduleDate',
            name: 'scheduleDate'       
        },{
            xtype:'timefield',
            padding:'5',
            anchor: '100%',
            pickerMaxHeight:100,
            name:'time',
            id:'scheduleTime',
            fieldLabel:'Time',
            increment: 1
        },{
            
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Send',
                name:'send',
                padding:'2 60',
                action:'send'
            },{
                xtype:'tbspacer',
                width:'10'
            },{
                xtype:'button',
                text:'Prev',
                name:'prev',
                padding:'2 60',
                action:'prev'
            },{
                xtype:'tbspacer',
                width:'10'
            },{
                xtype:'button',
                text:'Cancel',
                name:'cancel',
                padding:'2 60',
                action:'cancel'
            }
            ]     
        }
       
        ]
    }
    ]
});