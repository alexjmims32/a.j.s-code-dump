Ext.define('rcrm.view.performanceView',{
    extend:'Ext.chart.Chart',
    alias:'widget.performanceView',
//    animate: true,
    store: 'performanceStore',
    axes: [
        {
            type: 'Numeric',
            position: 'left',
            fields: ['lead'],
            label: {
                renderer: Ext.util.Format.numberRenderer('0,0')
            },
            grid: true,
            minimum: 0
        },
        {
            type: 'Category',
            position: 'bottom',
            fields: ['month']
        }
    ],
    series: [
        {
            type: 'line',
            highlight: {
                size: 7,
                radius: 7
            },
            axis: 'left',
            xField: 'month',
            yField: 'lead',
            markerConfig: {
                type: 'cross',
                size: 2,
                radius: 4,
                'stroke-width': 0
            }
        }
//        {
//            type: 'line',
//            highlight: {
//                size: 7,
//                radius: 7
//            },
//            axis: 'left',
//            xField: 'month',
//            yField: 'prospect',
//            markerConfig: {
//                type: 'cross',
//                size: 4,
//                radius: 4,
//                'stroke-width': 0
//            }
//        }
//        {
//            type: 'line',
//            highlight: {
//                size: 7,
//                radius: 7
//            },
//            axis: 'left',
//            xField: 'month',
//            yField: 'applicant',
//            markerConfig: {
//                type: 'cross',
//                size: 4,
//                radius: 4,
//                'stroke-width': 0
//            }
//        },
//        {
//            type: 'line',
//            highlight: {
//                size: 7,
//                radius: 7
//            },
//            axis: 'left',
//            xField: 'month',
//            yField: 'student',
//            markerConfig: {
//                type: 'cross',
//                size: 4,
//                radius: 4,
//                'stroke-width': 0
//            }
//        }
    ]
});