Ext.define('rcrm.view.applicationListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.applicationListGridView',
//    store: 'callListStore',
//    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Student Id',  
        flex: 1
    },

    {
        header: 'Student Name', 
        flex: 1
    },

    {
        header: 'Student Type',  
        flex: 1
    },

    {
        header: 'Admission Type', 
        flex: 1
    },
    {
        header: 'Term', 
        flex: 1
    },
    {
        header: 'Group', 
        flex: 1
    },
    {
        header: 'Status',
        flex: 1
    },
    {
        header: 'Hot', 
        flex: 1
    },
    {
        header: 'Owner', 
        flex: 1
    }

    ]
});