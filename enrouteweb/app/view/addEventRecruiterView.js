Ext.define('rcrm.view.addEventRecruiterView',{
    extend:'Ext.window.Window',
    alias:'widget.addEventRecruiterView',
    title:'Add Recruiter',
    closable:false,
    bodyStyle:{
        padding:'20',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'combobox',
        id:'addEvtRectId',
        fieldLabel:'Recruiter ID',
        padding:'5',
        mode: 'local',
        displayField: 'userId',
        valueField: 'userId',
        editable: false,
        store:'userListStore',
        listeners: {
            select: function(combo, record, index) {
                Ext.getCmp('addEvtRectName').setValue(record[0].data.firstName + ' '+ record[0].data.lastName);
            }
        }
    },
    {
        xtype:'textfield',
        padding:'5',
        id:'addEvtRectName',
        fieldLabel:'Recruiter Name',
        readOnly:true,
        name:'title'
    },
    {
        xtype:'checkboxfield',
        id:'intimatorFlag',
        boxLabel:'Intimate Recruiter'
    },
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'button',
            text:'Add',
            name:'add',
            padding:'2 30',
            action:'add'
        },
        {
            xtype:'tbspacer',
            width:'20'
        },
        {
            xtype:'button',
            text:'Close',
            name:'close',
            padding:'2 30',
            action:'close'
        }
        ]     
    }
    ]
});