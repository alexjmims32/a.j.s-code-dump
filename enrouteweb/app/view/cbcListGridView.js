Ext.define('rcrm.view.cbcListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.cbcListGridView',
//    store: 'prospectListStore',
    autoScroll:true,
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Student Id',  
        flex: 1
    },

    {
        header: 'Student Name',  
        flex: 1
    },

    {
        header: 'Added Date',  
        flex: 1
    },

    {
        header: 'Cleared Date', 
        flex: 1
    },
    {
        header: 'Sent Date', 
        flex: 1
    },
    {
        header: 'Received Date', 
        flex: 1
    },
    {
        header: 'Reviewed Date', 
        flex: 1
    }
    ]
});