Ext.define('rcrm.view.callListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.callListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 3,
            tableAttrs:{
                style: {
                //                    width: '75%',
                //                    height:'100%'
                },
                //                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'callsDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Id',
            id:'callStudentId'
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'First Name',
            id:'callfName'
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'calleDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Status',
            id:'callStatus',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:'callStatusStore'
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Last Name',
            id:'calllName'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'callListGridView'
    }            
        
    ]

});