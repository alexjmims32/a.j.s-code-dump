Ext.define('rcrm.view.userActivityListView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.userActivityListView',
    store: 'userActivityStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'User Id',  
        dataIndex: 'userId',  
        flex: 1
    },
    {
        header: 'User Name',  
        dataIndex: 'userName',  
        flex: 1
    },

    {
        header: 'Activity Date',  
        dataIndex: 'activityDate',  
        flex: 1
    },
    {
        header: 'Activity', 
        dataIndex: 'activityType',  
        flex: 1
    },
    {
        header: 'Id', 
        dataIndex: 'studentID',  
        flex: 1
    },
    {
        header: 'Student Name', 
        dataIndex: 'studentName',  
        flex: 1
    }
    ]
});