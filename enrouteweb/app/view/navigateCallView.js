Ext.define('rcrm.view.navigateCallView',{
    extend:'Ext.panel.Panel',
    alias:'widget.navigateCallView',
    padding:'30 0 2 500',
    items:[
    {
        xtype:'button',
        frame:true,
        align:'center',
        padding:'2 50',
        text:'Get Next Call',
        action:'getNextCall',
        name:'save'
    }
    ]
});