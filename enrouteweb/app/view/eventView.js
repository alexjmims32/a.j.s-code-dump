Ext.define('rcrm.view.eventView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.eventView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'eventList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'eventList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Events List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'addEvent',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'addEvent',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Add Event</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'event',
        activeTab:'eventList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'eventList',
            xtype:'eventListView'
        },
        {
            id:'eventView',
            xtype:'updateEventView'
        },
        {
            id:'addEvent',
            xtype:'addEventView'
        }
        ]
    } 
    ]

});