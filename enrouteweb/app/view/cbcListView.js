Ext.define('rcrm.view.cbcListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.cbcListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        id:'cbcSearch',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
//                style: {
//                    width: '100%',
//                    height:'100%'
//                },
                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            id:'cbcsDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Clear Status',
            id:'clearStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'YES', 'NO']
        },
        {
            xtype:'combobox',
            fieldLabel:'Letter Status',
            id:'ltrStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'YES', 'NO']
        },
        {
            xtype:'textfield',
            fieldLabel:'Student Id',
            id:'cbcsId'
        },
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'cbceDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            fieldLabel:'Received Status',
            id:'rcdStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY', 'YES', 'NO']
        },
        {
            xtype:'combobox',
            fieldLabel:'Committee Review',
            editable: false, 
            id:'comtReview',
            displayField: 'name',
            value:'ANY',
            valueField: 'value',
            queryMode: 'local',
            store:['ANY', 'APPROVED', 'DENIED', 'PENDING']
        },
        {
            xtype:'label'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'cbcListGridView'
    }            
        
    ]

});