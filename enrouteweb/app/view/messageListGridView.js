Ext.define('rcrm.view.messageListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.messageListGridView',
    store: 'messageListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Campaign',  
        dataIndex:'campaignName',
        flex: 1
    },
    {
        header: 'Id',  
        dataIndex:'prospectId',  
        flex: 1
    },

    {
        header: 'First Name',  
        dataIndex:'firstName',
        flex: 1
    },

    {
        header: 'Last Name',  
        dataIndex:'lastName',
        flex: 1
    },
    {
        header: 'Status', 
        dataIndex:'messageStatus',
        flex: 1
    },
    {
        header: 'Message Date', 
        dataIndex:'messageDate',
        flex: 1
    }
    ]
});