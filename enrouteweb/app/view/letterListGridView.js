Ext.define('rcrm.view.letterListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.letterListGridView',
    store: 'letterListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Id',  
        dataIndex: 'studentId',  
        flex: 1
    },

    {
        header: 'Name',  
        dataIndex: 'studentName',
        flex: 1
    },

    {
        header: 'Type',
        dataIndex: 'letterType',
        flex: 1
    },
    {
        header: 'Status', 
        dataIndex: 'status',
        flex: 1
    },
    {
        header: 'Message Date', 
        dataIndex: 'messageDate',
        flex: 1
    },
    {
        header: 'Print Date', 
        dataIndex: 'printDate',
        flex: 1
    }
    ]
});