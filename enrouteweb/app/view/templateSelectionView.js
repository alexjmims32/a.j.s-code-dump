Ext.define('rcrm.view.templateSelectionView',{
    extend:'Ext.panel.Panel',
    alias:'widget.templateSelectionView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 20',
        items:[
        {
            xtype:'label',
            id:'editEmailTitleTS',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Edit Email</u>'
        },{
            xtype:'label',
            id:'addEmailTitleTS',
            padding:'10 5 30 00',
            frame:true,
            html:'<b>Add Email</u>'
        },{
            xtype:'textfield',
            fieldLabel:'Title',
            id:'tempSelTitle',
            width:300,
            allowBlank:false           
        },{
            xtype:'combobox',
            width:300,
            fieldLabel:'Choose Template',
            id:'tempSelChooseTemp',
            frame:true,
            editable: false,
            displayField: 'templateName',
            valueField: 'templateId',
            queryMode: 'local',
            store:'activeTempListStore',
            listeners: {
                select: function(combo, record, index) {
                    var template=Ext.getStore('templateStore');
                    template.removeAll();
                    template.add({
                        templateId:record[0].data.templateId
                    });
                    var templateData=template.getRange();
                    var data=Ext.encode(templateData[0].data);
                    console.log(data);
                    var tstore=Ext.getStore('templateViewStore');
                    tstore.getProxy().url = baseUrl +'/template?operation=view&template='+data;
                    tstore.getProxy().afterRequest = function(){
                        var template=tstore.getRange();
                        Ext.getCmp('emailTempHtml').setValue(template[0].data.htmlText);
                    }
                    tstore.load();
                }
            }
        },{
            xtype:'textareafield',
            name:'emailTemplate',
            id:'emailTempHtml',
            grow:true,
            fieldLabel:'Email Template',
            rows:'50',
            height:200,
            width:'600'
        },{
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Next',
                name:'next',
                padding:'2 60',
                action:'next'
            },{
                xtype:'tbspacer',
                width:'10'
            },{
                xtype:'button',
                text:'Cancel',
                name:'cancel',
                padding:'2 60',
                action:'cancel'
            }
            ]     
        }
       
        ]
    }
    ]
});