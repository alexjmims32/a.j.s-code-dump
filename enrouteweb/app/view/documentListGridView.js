Ext.define('rcrm.view.documentListGridView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.documentListGridView',
//    store: 'letterListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Student Id',  
        flex: 1
    },

    {
        header: 'Student Name',  
        flex: 1
    },

    {
        header: 'Active Date',
        flex: 1
    },
    {
        header: 'Type', 
        flex: 1
    },
    {
        header: 'Status', 
        flex: 1
    },
    {
        header: 'Owner', 
        flex: 1
    }
    ]
});