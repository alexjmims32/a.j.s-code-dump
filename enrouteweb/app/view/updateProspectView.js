Ext.define('rcrm.view.updateProspectView',{
    extend:'Ext.panel.Panel',
    alias:'widget.updateProspectView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    items:[
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        padding:'25 5 5 20',
        border:false,
        items:[
        {
            xtype:'label',
            id:'updateProspName',
            padding:'0 0 40 10',
//            html:'Samuel David ( Id : 503 )</b>'
        }, 
        {
            xtype:'textfield',
            name:'firstname',
            width:'250',
            fieldLabel:'First Name',
            id:'upfName',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'prospectId',
            width:'250',
            fieldLabel:'Prospect Id',
            id:'upProspectId',
            value:'503',
            padding:'2',
            hidden:true,
            readOnly:true,
            border:false
        },

        {
            xtype:'textfield',
            name:'lastname',
            width:'250',
            fieldLabel:'Last Name',
            id:'uplName',
            //            value:'David',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'phoneNumber',
            id:'upPhoneNumber',
            width:'250',
            fieldLabel:'Phone Number',
            //            value:'',
            padding:'2',
            readOnly:true,
            border:false
        },
        {
            xtype:'textfield',
            name:'email',
            id:'upEmail',
            width:'250',
            fieldLabel:'Email',
            //            value:'satish@n2nservices.com',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            id:'upStatus',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            store:'statusStore'
        },
        {
            xtype:'combobox',
            fieldLabel:'Recruiter',
            id:'upRecruiterId',
            editable: false, 
            minwidth:'250',
            displayField: 'name',
            valueField: 'value',
            store:'recruiterStore'
        },
        {
            xtype:'textfield',
            name:'recruiterName',
            id:'upRecruiterName',
            width:'250',
            fieldLabel:'Recruiter Name',
            //            value:'',
            readOnly:true,
            padding:'2',
            border:false
        },
        {
            xtype:'label',
            padding:'35 0 5 0',
            html:'<b>Letter Summary</b>'
        },
        {
            xtype:'gridpanel',
            border:true,
            minHeight:120,
            maxHeight:120,
            maxWidth:350,
            store:'prospLetterSummaryStore',
            columns: [
            {
                header: 'Message',
                dataIndex:'message',
                flex:2
            },
            {
                header: 'Status',
                dataIndex:'status',
                flex:1
            },
            {
                header: 'Message Date',
                dataIndex:'messageDate',
                flex:1
            }
            ]
        },
        {
            xtype:'panel',
            layout:{
                type:'hbox',
                align:'strech'
            },
            padding:'10',
            border:false,
            items:[
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Save',
                action:'save',
                name:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                frame:true,
                padding:'2 50',
                text:'Cancel',
                action:'cancel',
                name:'cancel'
            }
            ]
        }
        ],
        flex:3
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'label',
            padding:'25 0 5 0',
            html:'<b>Message Summary</b>'
        },
        {
            xtype:'gridpanel',
            border:true,
            minHeight:120,
            maxHeight:120,
            maxWidth:600,
            store:'prospMsgSummaryStore',
            columns: [
            {
                header: 'Message',
                dataIndex: 'comments',  
                flex:2
            },
            {
                header: 'Status',
                dataIndex: 'messageStatus',  
                flex:1
            },
            {
                header: 'Message Date',
                dataIndex: 'messageDate',  
                flex:1
            }
            ]
        },
        {
            xtype:'label',
            padding:'10 0 5 0',
            html:'<b>Call Summary</b>'
        },
        {
            xtype:'gridpanel',
            border:true,
            minHeight:120,
            maxHeight:120,
            maxWidth:600,
            store:'prospCallSummaryStore',
            columns: [
            {
                header: 'Message From',
                dataIndex:'messageFrom',
                flex:1
            },
            {
                header: 'Message',
                dataIndex:'message',
                flex:2
            },
            {
                header: 'Status',
                dataIndex:'status',
                flex:1
            },
            {
                header: 'Message Date',
                dataIndex:'messageRequestedDate',
                flex:1
            }
            ]
        },
        {
            xtype:'label',
            padding:'10 0 5 0',
            html:'<b>Email Summary</b>'
        },
        {
            xtype:'gridpanel',
            border:true,
            minHeight:120,
            maxHeight:120,
            maxWidth:600,
            store:'prospEmailSummaryStore',
            columns: [
            {
                header: 'Campaign Title',
                dataIndex:'campaignTitle',
                flex:1
            },
            {
                header: 'HTML Text',
                dataIndex:'htmlText',
                flex:2
            },
            {
                header: 'Template Name',
                dataIndex:'templateName',
                flex:1
            },
            {
                header: 'Status',
                dataIndex:'status',
                flex:1
            }
            ]
        }
        ],
        flex:5
    },
    {
        xtype:'panel',
        layout:{
            type:'vbox',
            align:'strech'
        },
        border:false,
        items:[
                
        ],
        flex:1
    }
    ]
});