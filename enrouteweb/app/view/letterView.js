Ext.define('rcrm.view.letterView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.letterView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'letterList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'letterList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Letter List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        activeTab:'letterList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'letterList',
            xtype:'letterListView'
        },
        {
            id:'letterView',
            xtype:'updateLetterView'
        }
        ]
    } 
    ]

});