Ext.define('rcrm.view.emailListView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.emailListView',
    store: 'emailListStore',
    //    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Title',  
        dataIndex: 'campaignTitle',  
        flex: 1
    },

    {
        header: 'Template',  
        dataIndex: 'templateName',  
        flex: 1
    },

    {
        header: 'Scheduled On',  
        dataIndex: 'scheduleDate',  
        flex: 1
    },

    {
        header: 'Status', 
        dataIndex: 'status',  
        flex: 1
    },
    {
        header: 'Last Modified By', 
        dataIndex: 'lastModifiedBy',  
        flex: 1
    },
    {
        header: 'Last Modified On', 
        dataIndex: 'lastModifiedOn',  
        flex: 1
    }
    ],
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            text: '<b>Email List</b>',
            padding:'0 1050 0 0',
            disabled: true,
            align:'center'
        },
        {
            tooltip:'Add',
            icon:'resources/images/add2.png',
            action:'addEmail'
        }]
    }]
});