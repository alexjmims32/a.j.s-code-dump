Ext.define('rcrm.view.prospectView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.prospectView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
//            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'prospectList',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'prospectList',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Prospect List</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'addLead',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'addLead',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Add Lead</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'importLead',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'importLead',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Import Lead</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }


        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'prospect',
        activeTab:'prospectList',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'prospectList',
            xtype:'prospectListView'
        },
        {
            id:'prospectView',
            xtype:'updateProspectView'
        },
        {
            id:'addLead',
            xtype:'addLeadView'
        }
        ]
    } 
    ]

});