Ext.define('rcrm.view.userScorecardStandingsView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.userScorecardStandingsView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
                style: {
//                    width: '100%',
//                    height:'100%'
                },
                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'Start Date',
            name: 'startDate',
            format:'m/d/Y',
            id:'scrstdsDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Activity',
            displayField: 'name',
            valueField: 'value',
            value:'ANY',
            width:300,
            id:'scrstdActivity',
            editable: false ,
            queryMode: 'local',
            store:'userActivityDropdownStore'
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Group by Date',
//            width:100,
            editable: false,
            value:'YES',
            queryMode: 'local',
            id:'scrstdGbyDate',
            store:['YES', 'No']
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Group by Activity',
            value:'YES',
//            width:100,
            id:'scrstdGbyActivity',
            store:['YES', 'No'],
            queryMode: 'local',
            editable: false
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            format:'m/d/Y',
            name: 'endDate',
            id:'scrstdeDate',
            maxValue: new Date()
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'User',
            displayField: 'userId',
            valueField: 'userId',
            value:'ANY',
            id:'scrstdUserId',
            queryMode: 'local',
            width:300,
            editable: false,
            store:'userListStore'
        },
        {
            xtype:'combobox',
            padding:'5',
            fieldLabel:'Group by User',
            editable: false, 
//            width:100,
            value:'YES',
            id:'scrstdGbyUser',
            queryMode: 'local',
            store:['YES', 'No']
        },
        {
            xtype:'label'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'userScorecardStandingsListView'
    }            
        
    ]

});