Ext.define('rcrm.view.activityView',{
    //    extend:'Ext.panel.Panel',
    extend:'Ext.container.Container',
    alias: 'widget.activityView',
    layout: {
        type: 'hbox',       
        align: 'stretch'   
    },
    items:[
    {
        xtype:'panel',
        bodyStyle: {
            background: '#00427C',
        //            padding: '25 5 5 10'
        },
        layout: {
            type: 'vbox',       
            align: 'stretch'   
        },

        items:[
        //        {
        //            xtype:'label',
        //            style:'color:#FFFFFF;font-size:10pt',
        //            html:'<b><br />Prospect List<b>'
        //        },
        
        {
            xtype: 'component',
            itemId: 'userScorecard',
            padding:'30 0 2 15',
            autoEl: {
                tag: 'a',
                name:'userScorecard',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>User ScoreCard</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'scorecardStandings',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'scorecardStandings',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Scorecard Standings</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'userActivity',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'userActivity',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>User Activity</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        },
        {
            xtype: 'component',
            itemId: 'addPoints',
            padding:'5 0 2 15',
            autoEl: {
                tag: 'a',
                name:'addPoints',
                href: '#',
                html: '<font size="2" color="#FEB210" style="TEXT-DECORATION:none"><b>Add Special Project Credit</b></font>'
            },
            style:'font-weight:bold;TEXT-DECORATION:NONE;align:right'
        }
        ],
        flex:1
    },
    {
        xtype: 'splitter'   
    },
    {
        xtype:'tabpanel',
        id:'activity',
        activeTab:'userScorecard',
        tabBar :{
            hidden:'true'  
        },
        flex:6,
        items:[
        {
            id:'userScorecard',
            xtype:'userScorecardView'
        },
        {
            id:'scorecardStandings',
            xtype:'userScorecardStandingsView'
        },
        {
            id:'userActivity',
            xtype:'userActivityView'
        },
        {
            id:'addPoints',
            xtype:'addPointsView'
        }
        ]
    } 
    ]

});