Ext.define('rcrm.view.dashboardView',{
    extend:'Ext.panel.Panel',
    alias:'widget.dashboardView',
    width: '600',
    height: '400',
    //    renderTo: Ext.getBody(),
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    items: [{
        xtype: 'panel',
        layout: {
            type: 'hbox',       
            align: 'stretch'   
        },
        items:[
        {
            xtype:'todayScorecardView',
//            title:'Today\'s Scorecard' ,
            flex:3
        },{
            xtype: 'splitter'   
        },{
            xtype:'emailSummaryView',
            //            title:'Email Summary',
            flex:2
        }  
        ],
        flex: 4                                            
    }, {
        xtype: 'splitter'   
    }, {                    
        xtype: 'panel',
        height:'400',
        layout: {
            type: 'hbox',       
            align: 'stretch'   
        },
        items:[
        {
            xtype:'topRecruiterView',
//            collapsed :true,
            flex:1
//        },{
//            xtype: 'splitter' 
        },{
            xtype:'performanceView',
            collapsed :true,
            flex:1
        },{
//            xtype: 'splitter' 
//        },{
            xtype:'messageSummaryView',
//            collapsed :true,
            flex:1
        },{
//            xtype: 'splitter' 
//        },{
            xtype:'callSummaryView',
//            collapsed :true,
            flex:1
        }
        ],
        flex: 3     
    }]
});