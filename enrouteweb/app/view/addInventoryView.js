Ext.define('rcrm.view.addInventoryView',{
    extend:'Ext.panel.Panel',
    alias:'widget.addInventoryView',
    layout:{
        type:'hbox',
        align:'strech'
    },
    border: false, 
    //    padding:10,
    items:[
    {
        layout:{
            type:'vbox',
            align:'strech'
        },
        border: false, 
        bodyCssClass: 'x-panel-mc',
        padding:'5 5 5 10',
        items:[
        {
            xtype:'label',
            padding:'20 5 10 0',
            html:'<b><u>Add Inventory Item</b></u>'
        },
        {
            xtype:'textfield',
            fieldLabel:'Item Name',
            id:'addInvItemName',
            allowBlank:false           
        },
        {
            xtype:'textfield',
            fieldLabel:'Description',
            id:'addInvDesc',
            allowBlank:false           
        },
        {
            xtype:'textfield',
            fieldLabel:'Stock Quantity',
            id:'addInvQuantity',
            allowBlank:false           
        },
        {
            xtype:'combobox',
            fieldLabel:'Inventory Type',
            id:'addInvType',
            displayField: 'name',
            valueField: 'value',
            frame:true,
            editable: false,
            queryMode: 'local',
            store:'inventoryTypeStore'
        },
        {
            xtype:'combobox',
            fieldLabel:'Inventory Status',
            id:'addInvStatus',
            frame:true,
            editable: false,
            queryMode: 'local',
            store:['ACTIVE','INACTIVE'],
            value:'ACTIVE'
        },
        {
            xtype:'panel',
            padding:'5 0 0 0 0',
            layout:{
                type:'hbox',
                align:'strech'
            },
            border:false,
            items:[
            {
                xtype:'button',
                text:'Save',
                name:'save',
                padding:'2 60',
                action:'save'
            },
            {
                xtype:'tbspacer',
                width:'10'
            },
            {
                xtype:'button',
                text:'Reset',
                name:'reset',
                padding:'2 60',
                action:'reset'
            }
            ]     
        }
       
        ]
    }
    ]
});