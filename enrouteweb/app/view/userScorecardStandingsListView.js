Ext.define('rcrm.view.userScorecardStandingsListView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.userScorecardStandingsListView',
    store: 'scorecardStandingsStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'User Id',  
        dataIndex: 'userId',  
        flex: 1
    },

    {
        header: 'User Name',  
        dataIndex: 'userName',  
        flex: 1
    },

    {
        header: 'Activity Date',  
        dataIndex: 'activityDate',  
        flex: 1
    },

    {
        header: 'Activity', 
        dataIndex: 'activityType',  
        flex: 3
    },
    {
        header: 'Activity Count', 
        dataIndex: 'activityCount',  
        flex: 1
    },
    {
        header: 'Total Points', 
        dataIndex: 'activityScore',  
        flex: 1
    }
    ]
});