Ext.define('rcrm.view.templateListView' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.templateListView',
    store: 'templateListStore',
    minHeight:420,
    bodyBorder:false,
    //            margin:'5 5 5 5',
    columns: [
    {
        header: 'Category',  
        dataIndex: 'templateCategory',  
        flex: 1
    },

    {
        header: 'Name',  
        dataIndex: 'templateName',  
        flex: 1
    },
    {
        header: 'Status',
        dataIndex: 'templateStatus',  
        flex: 1
    },
    {
        header: 'Last Modified By', 
        dataIndex: 'lastModifiedBy',  
        flex: 1
    },
    {
        header: 'Last Modified On', 
        dataIndex: 'lastModifiedOn',  
        flex: 1
    },
    {
        header: 'Action', 
        xtype: 'actioncolumn',
        items: [
        {
            handler: function(view, rowIndex, colIndex, item, e) {
                var store=Ext.getStore('templateListStore');
                var records=store.getRange();
                var record=records[rowIndex];
                console.log('remove');
                store.remove(record);
            },
            align:'center',
            icon:'resources/images/delete2.png'
        }
        ],
        flex: 1
    }
    ],
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            text: '<b>Template List</b>',
            padding:'0 1027 0 0',
            disabled: true,
            align:'center'
        },
        {
            tooltip:'Add',
            icon:'resources/images/add2.png',
            action:'addTemp'
        }]
    }]
});