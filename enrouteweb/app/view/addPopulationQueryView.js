Ext.define('rcrm.view.addPopulationQueryView',{
    extend:'Ext.window.Window',
    alias:'widget.addPopulationQueryView',
    title:'Add Dynamic Contact Query',
    closable:false,
    bodyStyle:{
        padding:'20',
        background: '#FFFFFF'
    },
    items:[
        {
        xtype:'textfield',
        id:'addPopQueryName',
        fieldLabel:'Name'
    },
    {
            xtype:'textareafield',
            name:'addPopQuery',
            grow:true,
            height:100,
            fieldLabel:'Database Query',
            id:'addPopDbQuery',
            rows:'10',
            width:'350'
        },
    {
        xtype:'panel',
        padding:'5 0 0 0 0',
        layout:{
            type:'hbox',
            align:'strech'
        },
        border:false,
        items:[
        {
            xtype:'button',
            text:'Add',
            name:'add',
            padding:'2 30',
            action:'add'
        },
        {
            xtype:'tbspacer',
            width:'20'
        },
        {
            xtype:'button',
            text:'Close',
            name:'close',
            padding:'2 30',
            action:'close'
        }
        ]     
    }
    ]
});