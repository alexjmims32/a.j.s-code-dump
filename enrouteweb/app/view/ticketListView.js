Ext.define('rcrm.view.ticketListView',{
    extend:'Ext.panel.Panel',
    alias: 'widget.ticketListView',
    
    layout: {
        type: 'vbox',       
        align: 'stretch'   
    },
    bodyStyle:{
        //              padding:'5',
        background: '#FFFFFF'
    },
    items:[
    {
        xtype:'form',
        id:'ticketSearch',
        frame:'true',
        layout: {
            type: 'table',
            columns: 4,
            tableAttrs:{
//                style: {
//                    width: '100%',
//                    height:'100%'
//                },
                align: 'center',
                valign: 'middle'
            },
            trAttrs:{
                align: 'center'
            //            valign: 'middle'
            }
        },
        bodyStyle:{
            padding:'5',
            background: '#FFFFFF'
        },
        items:[
        {
            xtype: 'datefield',
            anchor: '100%',
            fieldLabel: 'Start Date',
            padding:'5',
            name: 'startDate',
            id:'tcktsDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            fieldLabel:'CRM Ticket Number',
            padding:'5',
            id:'tckNo'
        },
        {
            xtype:'combobox',
            fieldLabel:'Status',
            padding:'5',
            id:'tcktStatus',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY','ALL OPEN', 'NEW', 'INPROGRESS', 'ESCALATED' , 'CLOSED']
        },
        {
            xtype:'combobox',
            fieldLabel:'Owner',
            padding:'5',
            id:'tcktowner',
            value:'ANY',
            displayField: 'name',
            valueField: 'value',
            editable: false, 
            queryMode: 'local',
            store:['ANY','RCMDIR']
        },
        {
            xtype: 'datefield',
            padding:'5',
            anchor: '100%',
            fieldLabel: 'End Date',
            name: 'endDate',
            id:'tckteDate',
            maxValue: new Date()
        },
        {
            xtype:'textfield',
            padding:'5',
            fieldLabel:'Student Id',
            id:'tcktStdId'
        },
        {
            xtype:'combobox',
            fieldLabel:'Group Code',
            editable: false, 
            padding:'5',
            id:'tcktgCode',
            displayField: 'name',
            value:'DOMESTIC',
            valueField: 'value',
            queryMode: 'local',
            store:['DOMESTIC']
        },
        {
            xtype:'label'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Search',
            action:'search',
            name:'search'
        },
        {
            xtype:'button',
            padding:'2 75',
            text:'Reset',
            action:'reset',
            name:'reset'
        }
        ]
    },{
        xtype:'ticketListGridView'
    }            
        
    ]

});