Ext.define('rcrm.store.inventoryViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.inventoryModel',
    alias: 'inventoryViewStore',
    
    autoLoad: false,
    
    data:[
        {
            'inventoryId':'205',
            'description':'Long sleeve',
             'inventoryTitle':'Banners',
            'inventoryType':'INV TYPE1',
            'modifiedDate':'',
            'modifiedBy':'',
            'count':'500',
            'status':'ACTIVE',
            'createdDate':'09/10/2012'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'inventory',
//            totalProperty: 'numrows'
//        }
//    },
//    listeners: {
//        load: function(store, records, successful) {
//          Ext.getStore('inventoryEventStore').loadRawData(store.proxy.reader.jsonData);
//         
//        }
//    }
    
});