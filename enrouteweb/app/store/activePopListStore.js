Ext.define('rcrm.store.activePopListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.populationListModel',
    alias: 'activePopListStore',
    
    autoLoad: false,
    
     data:[
    {
        'populationTitle':'IT IS MY OPEN HOUSE',
        'lastModifiedBy':'',
        'lastModifiedOn':'',
        'status':'ACTIVE'
    },
    {
        'populationTitle':'Georgia Population',
        'lastModifiedBy':'RCMPROSP',
        'lastModifiedOn':'10/12/2012',
        'status':'ACTIVE'
    }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'populationList.population',
//            totalProperty: 'numrows'
//        }
//    }
    
});