Ext.define('rcrm.store.inventoryListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.inventoryModel',
    alias: 'inventoryListStore',
    
    autoLoad: false,
    
    data:[
        {
            'inventoryTitle':'Banners',
            'inventoryType':'INV TYPE1',
            'modifiedDate':'',
            'modifiedBy':'',
            'count':'500',
            'status':'ACTIVE',
            'createdDate':'09/10/2012'
        },
        {
            'inventoryTitle':'Poster',
            'inventoryType':'INV TYPE2',
            'modifiedDate':'10/10/2012',
            'modifiedBy':'RCMDIR',
            'count':'500',
            'status':'ACTIVE',
            'createdDate':'09/10/2012'
        },
        {
            'inventoryTitle':'Bags',
            'inventoryType':'INV TYPE1',
            'modifiedDate':'',
            'modifiedBy':'',
            'count':'100',
            'status':'ACTIVE',
            'createdDate':'10/10/2012'
        }
        
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'inventoryList.inventory',
//            totalProperty: 'numrows'
//        }
//    }
    
});