Ext.define('rcrm.store.popPersonStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.campaign.studentModel',
    alias: 'popPersonStore',
    
    autoLoad: false,
    data:[
    {
        'id':'1',
        'firstName':'Araon',
        'lastName':'jones',
        'emailAddress':'araon@n2n.com',
        'status':'SENT'
    },
    {
        'id':'2',
        'firstName':'Walter',
        'lastName':'biel',
        'emailAddress':'walter@n2n.com',
        'status':'SENT'
    },
    {
        'id':'3',
        'firstName':'Vin',
        'lastName':'Myers',
        'emailAddress':'vin@n2n.com',
        'status':'FAILED'
    }
    ]
//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'population.personList.person',
//            totalProperty: 'numrows'
//        }
//    }
    
});