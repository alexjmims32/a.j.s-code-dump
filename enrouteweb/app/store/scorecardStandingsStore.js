Ext.define('rcrm.store.scorecardStandingsStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.scorecardStandingsModel',
    alias: 'scorecardStandingsStore',
    
    autoLoad: false,
    
    data:[
        {
            'userId':'RCMDIR',
            'userName':'Cedric Clarke',
            'activityDate':'10/08/2012',
            'activityType':'PROSPECT-STUDENT',
            'activityCount':'5',
            'activityScore':'5'
        },
        {
            'userId':'RCMDIR',
            'userName':'Cedric Clarke',
            'activityDate':'10/09/2012',
            'activityType':'PROSPECT-APPLICANT',
            'activityCount':'10',
            'activityScore':'10'
        },
        {
            'userId':'RCMDIR',
            'userName':'Cedric Clarke',
            'activityDate':'10/08/2012',
            'activityType':'CALLS-CLOSED-MANUAL',
            'activityCount':'2',
            'activityScore':'5'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'scorecardlist.scorecard',
//            totalProperty: 'numrows'
//        }
//    }
    
});