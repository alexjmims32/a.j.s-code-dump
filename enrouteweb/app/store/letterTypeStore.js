Ext.define('rcrm.store.letterTypeStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'letterTypeStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'letterTypeStore'
    }
    
});