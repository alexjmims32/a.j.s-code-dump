Ext.define('rcrm.store.eventViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.eventViewModel',
    alias: 'eventViewStore',
    
    autoLoad: false,
    
    data:[
        {
        'eventId':'1',
        'eventTitle':'Sample Event',
        'eventStartDate':'10/05/2012',
        'eventEndDate':'10/10/2012',
        'status':'COMPLETED',
        'modifiedBy':'RCMDIR',
        'modifiedDate':'10/10/2012',
        'createdBy':'RCMDIR',
        'createdDate':'10/01/2012',
        'remarks':'No Action',
        'location':{'address1':'texas street1', 'address2':'texas', 'city':'new york', 'state':'texas', 'zip':'258963', 'country':'US'}
    }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'event',
//            totalProperty: 'numrows'
//        }
//    },
//    listeners: {
//        load: function(store, records, successful) {
//          Ext.getStore('eventInventoryStore').loadRawData(store.proxy.reader.jsonData);
//          Ext.getStore('recruiterListStore').loadRawData(store.proxy.reader.jsonData);
//          Ext.getStore('attendeeListStore').loadRawData(store.proxy.reader.jsonData);
//         
//        }
//    }
    
});