Ext.define('rcrm.store.recruiterListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.recruiterModel',
    alias: 'recruiterListStore',
    
    autoLoad: false,
    
    data:[
        {
            'recruiterId':'RCMDIR',
            'recruiterName':'Cedric Clarke'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'event.recruiters.recruiter',
//            totalProperty: 'numrows'
//        }
//    }
    
});