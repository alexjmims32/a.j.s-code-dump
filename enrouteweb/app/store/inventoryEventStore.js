Ext.define('rcrm.store.inventoryEventStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.eventModel',
    alias: 'inventoryEventStore',
    
    autoLoad: false,
    
    data:[
       {
        'eventTitle':'Sample Event',
        'eventStartDate':'10/05/2012',
        'eventEndDate':'10/10/2012',
        'status':'COMPLETED',
        'inventoryCount':'100'
    },
    {
        'eventTitle':'Carolina Event',
        'eventStartDate':'10/25/2012',
        'eventEndDate':'10/26/2012',
        'status':'SCHEDULED',
        'inventoryCount':'50'
    },
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'inventory.eventList.event',
//            totalProperty: 'numrows'
//        }
//    }
    
});