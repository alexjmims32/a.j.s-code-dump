Ext.define('rcrm.store.actInvStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.inventoryModel',
    alias: 'actInvStore',
    
    autoLoad: false,
    
    data:[
        {
            'inventoryTitle':'Banners',
            'inventoryType':'INV TYPE1',
            'count':'500'
        },
        {
            'inventoryTitle':'Poster',
            'inventoryType':'INV TYPE2',
            'count':'500'
        },
        {
            'inventoryTitle':'Bags',
            'inventoryType':'INV TYPE1',
            'count':'100'
        }
        
    ]


//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'inventoryList.inventory',
//            totalProperty: 'numrows'
//        }
//    }
    
});