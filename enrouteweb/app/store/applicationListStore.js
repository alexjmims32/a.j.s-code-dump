Ext.define('rcrm.store.applicationListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.applicationListModel',
    alias: 'applicationListStore',
    
    autoLoad: false,

    data:[
        {
            'studentId':'',
            'studentName':'',
            'studentType':'',
            'admissionType':'',
            'term':'',
            'group':'',
            'status':'',
            'hot':'',
            'owner':''
        }
    ]
    
});