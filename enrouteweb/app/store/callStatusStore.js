Ext.define('rcrm.store.callStatusStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'callStatusStore',
    
    autoLoad: false,
    
    data:[
        {
            'name':'ANY',
            'value':'ANY'
        },
        {
            'name':'NEW',
            'value':'NEW'
        },
        {
            'name':'IN PROGRESS',
            'value':'IN PROGRESS'
        },
        {
            'name':'CLOSED',
            'value':'CLOSED'
        }
    ]

//    proxy: {
//        type: 'localstorage',
//        id:'callStatusStore'
//    }
    
});