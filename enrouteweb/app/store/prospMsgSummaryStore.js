Ext.define('rcrm.store.prospMsgSummaryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.messageSummaryModel',
    alias: 'prospMsgSummaryStore',
    
    autoLoad: false,
    
    data:[
        {
            'status':'CLOSED',
            'messageDate':'10/22/2012',
            'comments':'I am interested in this. Please do call me back on my number 1-618-400-9283.'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'prospectSummary.messageList.message',
//            totalProperty: 'numrows'
//        }
//    }
    
});