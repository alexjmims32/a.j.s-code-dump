Ext.define('rcrm.store.emailQueryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.campaign.queryModel',
    alias: 'emailQueryStore',
    
    autoLoad: false,
    
    data:[
        {
            'queryName':'test',
            'query':'select recrstg_int_id from recrstg where recrstg_int_id=411'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'email.queryList.query',
//            totalProperty: 'numrows'
//        }
//    }
    
});