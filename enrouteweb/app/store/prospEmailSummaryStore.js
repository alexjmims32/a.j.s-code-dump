Ext.define('rcrm.store.prospEmailSummaryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.emailSummaryModel',
    alias: 'prospEmailSummaryStore',
    
    autoLoad: false,
    
    data:[
        {
            'campaignTitle':'n2n Campaign',
            'templateName':'Blank Template',
            'htmlText':'<html><body></body></html>',
            'status':'COMPLETED'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'prospectSummary.emailList.email',
//            totalProperty: 'numrows'
//        }
//    }
    
});