Ext.define('rcrm.store.letterListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.letterListModel',
    alias: 'letterListStore',
    
    autoLoad: false,
    
    data:[
        
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'letterList.letter',
//            totalProperty: 'numrows'
//        }
//    }
    
});