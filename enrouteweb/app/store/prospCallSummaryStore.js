Ext.define('rcrm.store.prospCallSummaryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.callSummaryModel',
    alias: 'prospCallSummaryStore',
    
    autoLoad: true,
    
    data:[
        {
        'message':'Event will be Conducted 10/23/2012, please attend the Event.',
        'messageRequestedDate':'10/22/2012',
        'status':'CLOSED',
        'messageFrom':'RCMPROSP'
        },
        {
        'message':'We need to speak about your records',
        'messageRequestedDate':'10/23/2012',
        'status':'IN PROGRESS',
        'messageFrom':'RCMPROSP'
        }        
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'prospectSummary.callList.call',
//            totalProperty: 'numrows'
//        }
//    }
    
});