Ext.define('rcrm.store.userScorecardStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.userScorecardModel',
    alias: 'userScorecardStore',
    
    autoLoad: false,
    
    data:[
        {
            'userId':'RCMDIR',
            'activityDate':'10/20/2012',
            'activityType':'PROSPECT-PROSPECT',
            'activityCount':'5',
            'activityScore':'5'
        },
        {
            'userId':'RCMDIR',
            'activityDate':'10/20/2012',
            'activityType':'PROSPECT-APPLICANT',
            'activityCount':'4',
            'activityScore':'4'
        },
        {
            'userId':'RCMDIR',
            'activityDate':'10/20/2012',
            'activityType':'PROSPECT-STUDENT',
            'activityCount':'1',
            'activityScore':'1'
        },
        {
            'userId':'RCMDIR',
            'activityDate':'10/20/2012',
            'activityType':'CALLS-CLOSED',
            'activityCount':'6',
            'activityScore':'6'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'scorecardlist.scorecard',
//            totalProperty: 'numrows'
//        }
//    }
    
});