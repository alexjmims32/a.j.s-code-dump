Ext.define('rcrm.store.prospectListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.prospectListModel',
    alias: 'prospectListStore',
    
    autoLoad: false,
    
    data:[
        {
            'prospectId':'1',
            'firstName':'Araon',
            'lastName':'Jones',
            'recruiterId':'RCMDIR',
            'recruiterName':'Cedric Clarke',
            'status':'LEAD',
            'addedDate':'10/22/2012'
        },
        {
            'prospectId':'2',
            'firstName':'Walter',
            'lastName':'biel',
            'recruiterId':'RCMDIR',
            'recruiterName':'Cedric Clarke',
            'status':'STUDENT',
            'addedDate':'09/22/2012'
        },
        {
            'prospectId':'3',
            'firstName':'Vin',
            'lastName':'Myers',
            'recruiterId':'',
            'recruiterName':'',
            'status':'LEAD',
            'addedDate':'10/23/2012'
        },
        {
            'prospectId':'4',
            'firstName':'Stan',
            'lastName':'Man',
            'recruiterId':'RCMDIR',
            'recruiterName':'Cedric Clarke',
            'status':'LEAD',
            'addedDate':'10/22/2012'
        }
    ]
//     proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'prospectList.prospect',
//            totalProperty: 'numrows'
//        }
//    }
    
});