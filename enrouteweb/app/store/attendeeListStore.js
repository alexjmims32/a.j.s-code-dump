Ext.define('rcrm.store.attendeeListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.attendeeModel',
    alias: 'attendeeListStore',
    
    autoLoad: false,
    
    data:[
        {
            'firstName':'Araon',            
            'lastName':'Jones',
            'email':'araon@n2n.com',
            'status':'YES'
        },
        {
            'firstName':'Walter',            
            'lastName':'Biel',
            'email':'walter@n2n.com',
            'status':'YES'
        },
        {
            'firstName':'Vin',            
            'lastName':'Myers',
            'email':'vin@n2n.com',
            'status':'NO'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'event.attendees.attender',
//            totalProperty: 'numrows'
//        }
//    }
});