Ext.define('rcrm.store.templateViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.templateListModel',
    alias: 'templateViewStore',
    
    autoLoad: false,
    
    data:[
        {
            'templateId':'1',
            'templateCategory':'GENERAL',
            'templateName':'BLANK TEMPLATE',
            'lastModifiedOn':'',
            'lastModifiedBy':'',
            'templateStatus':'A',
            'createdBy':'RCMDIR',
            'createdOn':'10/01/2012',
            'htmlText':'<html><body></body><html'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'template'
////            totalProperty: 'numrows'
//        }
//    }
    
});