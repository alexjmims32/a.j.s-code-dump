Ext.define('rcrm.store.userActivityDropdownStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'userActivityDropdownStore',
    
    autoLoad: false,
    
    data:[
        {
            'name':'ANY',
            'value':'ANY'
        },
        {
            'name':'MESSAGE-CLOSED',
            'value':'MESSAGE-CLOSED'
        },
        {
            'name':'CALLS-CLOSED',
            'value':'CALLS-CLOSED'
        },
        {
            'name':'PROSPECT-PROSPECT',
            'value':'PROSPECT-PROSPECT'
        },
        {
            'name':'PROSPECT-APPLICANT',
            'value':'PROSPECT-APPLICANT'
        },
        {
            'name':'PROSPECT-STUDENT',
            'value':'PROSPECT-STUDENT'
        }
    ]

//    proxy: {
//        type: 'localstorage',
//        id:'userActivityDropdownStore'
//    }
    
});