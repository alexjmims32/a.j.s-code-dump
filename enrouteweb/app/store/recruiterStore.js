Ext.define('rcrm.store.recruiterStore', {
    extend: 'Ext.data.Store',
    alias: 'recruiterStore',
    fields:['name', 'value'],
    
    autoLoad: true,
     data:[
                  {'name':'ANY', 'value':'ANY'},
                  {'name':'RCMDIR', 'value':'RCMDIR'},
                  {'name':'RCMPROS', 'value':'RCMPROS'},
                  {'name':'RCMCALL', 'value':'RCMCALL'}
              ]
});