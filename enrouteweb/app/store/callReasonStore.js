Ext.define('rcrm.store.callReasonStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'callReasonStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'callReasonStore'
    }
    
});