Ext.define('rcrm.store.userListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.userListModel',
    alias: 'userListStore',
    
    autoLoad: false,
    
    data:[
        {
            'userId':'RCMDIR'
        },
        {
            'userId':'RCMCALL'
        },
        {
            'userId':'RCMPROSP'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'usersList.user',
//            totalProperty: 'numrows'
//        }
//    }
    
});