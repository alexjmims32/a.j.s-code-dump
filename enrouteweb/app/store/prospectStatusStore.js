Ext.define('rcrm.store.prospectStatusStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'prospectStatusStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'prospectStatusStore'
    }
    
});