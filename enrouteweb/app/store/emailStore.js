Ext.define('rcrm.store.emailStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.emailModel',
    alias: 'emailStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'emailStore'
    }
    
});