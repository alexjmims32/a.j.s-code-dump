Ext.define('rcrm.store.performanceStore',{
    extend:'Ext.data.JsonStore',
    model: 'rcrm.model.performanceModel',
    alias: 'performanceStore',
    
    autoLoad: true,
    data:[
        {month:'Mar', lead:'20', prospect:'7', applicant:'3', student:'0'},
        {month:'Apr', lead:'10', prospect:'10', applicant:'7', student:'3'},
        {month:'May', lead:'5', prospect:'8', applicant:'9', student:'8'},
        {month:'Jun', lead:'0', prospect:'2', applicant:'10', student:'18'}
    ]
    
});