Ext.define('rcrm.store.populationViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.populationListModel',
    alias: 'populationViewStore',
    
    autoLoad: false,
    
    data:[
    {
        'populationId':'1',
        'populationTitle':'IT IS MY OPEN HOUSE',
        'lastModifiedBy':'',
        'lastModifiedOn':'',
        'status':'I',
        'createdBy':'RCMDIR',
        'createdOn':'09/20/2012'
    }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'population',
//            totalProperty: 'numrows'
//        }
//    },
//     listeners: {
//        load: function(store, records, successful) {
//          Ext.getStore('popQueryStore').loadRawData(store.proxy.reader.jsonData);
//          Ext.getStore('popPersonStore').loadRawData(store.proxy.reader.jsonData);
//        }
//    }
    
});