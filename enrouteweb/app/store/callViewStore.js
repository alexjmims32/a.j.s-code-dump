Ext.define('rcrm.store.callViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.callModel',
    alias: 'callViewStore',
    
    autoLoad: false,
    
    data:[
         {
            'studentID':'1',
            'studentName':'Aaron Jones',
            'callReason':'MSG FROM PROSPECTES',
            'messageFrom':'RCMDIR',
            'messageRequestedDate':'10/10/2012',
            'status':'CLOSED',
            'callOwner':'RCMCALL',
            'callDate':'10/15/2012',
            'homeNumber':'1-618-400-9283',
            'email':'aaron@n2n.com',
            'contactInstructions':'We need to speak about your records',
            'comment':'Student is sending updated info',
            'contactedByPhone':'Y',
            'contactedByEmail':'N',
            'leftMessage':'N',
            'noContactPossible':'N'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'call'
//        }
//    }
    
});