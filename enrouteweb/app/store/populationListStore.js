Ext.define('rcrm.store.populationListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.populationListModel',
    alias: 'populationListStore',
    
    autoLoad: false,
    
    data:[
    {
        'populationTitle':'N2N Group',
        'lastModifiedBy':'RCMDIR',
        'lastModifiedOn':'10/03/2012',
        'status':'INACITVE'
    },
    {
        'populationTitle':'IT IS MY OPEN HOUSE',
        'lastModifiedBy':'',
        'lastModifiedOn':'',
        'status':'ACTIVE'
    },
    {
        'populationTitle':'Georgia Population',
        'lastModifiedBy':'RCMPROSP',
        'lastModifiedOn':'10/12/2012',
        'status':'ACTIVE'
    }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'populationList.population',
//            totalProperty: 'numrows'
//        }
//    }
    
});