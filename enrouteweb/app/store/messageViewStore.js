Ext.define('rcrm.store.messageViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.messageModel',
    alias: 'messageViewStore',
    
    autoLoad: false,
    
    data:[
         {
             'messageId':'204',
            'campaignName':'N2N Campaign',
            'prospectId':'1',
            'firstName':'Araon',
            'lastName':'jones',
            'messageStatus':'CLOSED',
            'messageDate':'10/16/2012',
            'campaignName':'N2N Campaign',
            'comments':'I am interested in this. Please do call me back on my number 1-618-400-9283',
            'internalComments':'',
            'prospectStatus':'LEAD',
            'campaignId':'1',
            'homeNumber':'1-618-400-9283',
            'cellNumber':'1-618-400-9283',
            'email':'araon@n2n.com',
            'recruiterId':'RCMDIR',
            'recruiterName':'Cedric Clarke'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'message'
////            totalProperty: 'numrows'
//        }
//    }
    
});