Ext.define('rcrm.store.eventStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.eventModel',
    alias: 'eventStore',
    
    autoLoad: false,

    
    proxy: {
        type: 'localstorage',
        id:'eventStore'
    }
});