Ext.define('rcrm.store.emailStatusStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'emailStatusStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'emailStatusStore'
    }
    
});