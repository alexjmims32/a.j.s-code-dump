Ext.define('rcrm.store.letterStatusStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'letterStatusStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'letterStatusStore'
    }
    
});