Ext.define('rcrm.store.templateCategoryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'templateCategoryStore',
    
    autoLoad: false,
    
    data:[
        {
            'name':'GENERAL',
            'value':'GENERAL'
        },
        {
            'name':'STANDARD',
            'value':'STANDARD'
        }
    ]

//    proxy: {
//        type: 'localstorage',
//        id:'templateCategoryStore'
//    }
    
});