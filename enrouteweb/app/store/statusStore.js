Ext.define('rcrm.store.statusStore', {
    extend: 'Ext.data.Store',
    alias: 'statusStore',
    fields:['name', 'value'],
    
    autoLoad: true,
    data:[
    {
        'name':'ANY', 
        'value':'ANY'
    },

    {
        'name':'LEAD', 
        'value':'LEAD'
    },

    {
        'name':'INQUIRER', 
        'value':'INQUIRER'
    },

    {
        'name':'PROSPECT', 
        'value':'PROSPECT'
    },

    {
        'name':'APPLICANT', 
        'value':'APPLICANT'
    },

    {
        'name':'STUDENT', 
        'value':'STUDENT'
    }
    ]
});