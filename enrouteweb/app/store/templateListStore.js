Ext.define('rcrm.store.templateListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.templateListModel',
    alias: 'templateListStore',
    
    autoLoad: false,
    
    data:[
        {
            'templateId':'1',
            'templateCategory':'GENERAL',
            'templateName':'BLANK TEMPLATE',
            'lastModifiedOn':'',
            'lastModifiedBy':'',
            'templateStatus':'ACTIVE',
            'htmlText':'<html><body></body></html>'
        },
        {
            'templateId':'2',
            'templateCategory':'GENERAL',
            'templateName':'GEORGIA TEMPLATE',
            'lastModifiedOn':'10/10/2012',
            'lastModifiedBy':'RCMDIR',
            'templateStatus':'ACTIVE',
            'htmlText':'<html><body></body></html>'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'templateList.template',
//            totalProperty: 'numrows'
//        }
//    }
    
});