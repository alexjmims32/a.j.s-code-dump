Ext.define('rcrm.store.updatePopStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.updatePopModel',
    alias: 'updatePopStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: ''
        }
    }
    
});