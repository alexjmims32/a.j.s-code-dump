Ext.define('rcrm.store.eventListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.eventModel',
    alias: 'eventListStore',
    
    autoLoad: false,
    
    data:[
    {
        'eventTitle':'Sample Event',
        'eventStartDate':'10/05/2012',
        'eventEndDate':'10/10/2012',
        'status':'COMPLETED',
        'modifiedBy':'RCMDIR',
        'modifiedDate':'10/10/2012'
    },
    {
        'eventTitle':'Carolina Event',
        'eventStartDate':'10/25/2012',
        'eventEndDate':'10/26/2012',
        'status':'SCHEDULED',
        'modifiedBy':'',
        'modifiedDate':''
    },
    {
        'eventTitle':'Sample Event2',
        'eventStartDate':'10/30/2012',
        'eventEndDate':'10/31/2012',
        'status':'NOT SCHEDULED',
        'modifiedBy':'RCMDIR',
        'modifiedDate':'10/22/2012'
    }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'eventList.event',
//            totalProperty: 'numrows'
//        }
//    }
    
});