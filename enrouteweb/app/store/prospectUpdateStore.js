Ext.define('rcrm.store.prospectUpdateStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.updateModel',
    alias: 'prospectUpdateStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: ''
        }
    }
    
});