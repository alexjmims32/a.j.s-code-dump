Ext.define('rcrm.store.messageStatusStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'messageStatusStore',
    
    autoLoad: false,
    
    data:[
        {
            'name':'ANY',
            'value':'ANY'
        },
        {
            'name':'NEW',
            'value':'NEW'
        },
        {
            'name':'IN PROGRESS',
            'value':'IN PROGRESS'
        },
        {
            'name':'CLOSED',
            'value':'CLOSED'
        }
    ]

//    proxy: {
//        type: 'localstorage',
//        id:'messageStatusStore'
//    }
    
});