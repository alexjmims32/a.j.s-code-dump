Ext.define('rcrm.store.topRecruiterStore',{
    extend:'Ext.data.JsonStore',
    model: 'rcrm.model.topRecruiterModel',
    alias: 'topRecruiterStore',
    
    //    proxy: {
    //        type: 'ajax',
    //        reader: {
    //            type: 'json',
    //            root: 'emailSummaryList.emailSummary',
    //            totalProperty: 'numrows'
    //        }
    //    }

    autoload:true,
    data:[
    {
        name:'RCMDIR', 
        points:'15'
    },

    {
        name:'RCMPROSP', 
        points:'12'
    },
    {
        name:'RCMCALL', 
        points:'10'
    }
    ]
    
});