Ext.define('rcrm.store.leadStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.leadModel',
    alias: 'leadStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'leadStore'
    }
    
});