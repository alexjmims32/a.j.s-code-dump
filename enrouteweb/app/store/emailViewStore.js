Ext.define('rcrm.store.emailViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.emailListModel',
    alias: 'emailViewStore',
    
    autoLoad: false,
    
    data:[
         {
            'campaignTitle':'n2n Campaign',
            'templateName':'BLANK TEMPLATE',
            'scheduleDate':'10/15/2012',
            'status':'COMPLETED',
            'lastModifiedBy':'CRMDIR',
            'lastModifiedOn':'10/09/2012',
            'emailSubject':'Welcome to eNroute RCM',
            'emailReply':'rcm@n2n.com',
            'htmlText':'<html><body>Welcome to eNroute RCM</body></html>',
            'count':'3',
            'sentCount':'2',
            'failCount':'1',
            'scheduleCount':'0',
            'createdOn':'10/01/2012',
            'createdBy':'CRMDIR'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'email'
////            totalProperty: 'numrows'
//        }
//    },
//    listeners: {
//        LOAD: FUNCTION(STORE, RECORDS, SUCCESSFUL) {
//          EXT.GETSTORE('EMAILQUERYSTORE').LOADRAWDATA(STORE.PROXY.READER.JSONDATA);
//          EXT.GETSTORE('EMAILSTUDENTSTORE').LOADRAWDATA(STORE.PROXY.READER.JSONDATA);
//          EXT.GETSTORE('EMAILPOPSTORE').LOADRAWDATA(STORE.PROXY.READER.JSONDATA);
////          EXT.GETSTORE('PROSPLETTERSUMMARYSTORE').LOADRAWDATA(STORE.PROXY.READER.JSONDATA);
//          
//        }
//    }
    
});