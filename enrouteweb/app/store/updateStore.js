Ext.define('rcrm.store.updateStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.updateModel',
    alias: 'updateStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: ''
        }
    }
    
});