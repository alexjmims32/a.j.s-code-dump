Ext.define('rcrm.store.messageListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.messageModel',
    alias: 'messageListStore',
    
    autoLoad: false,
    
    data:[
        {
            'campaignName':'N2N Campaign',
            'prospectId':'1',
            'firstName':'Araon',
            'lastName':'jones',
            'messageStatus':'CLOSED',
            'messageDate':'10/16/2012'
        },
        {
            'campaignName':'N2N Campaign',
            'prospectId':'2',
            'firstName':'Walter',
            'lastName':'biel',
            'messageStatus':'NEW',
            'messageDate':'10/20/2012'
        },
         {
            'campaignName':'N2N Campaign',
            'prospectId':'3',
            'firstName':'Vin',
            'lastName':'Myers',
            'messageStatus':'IN PROGRESS',
            'messageDate':'10/18/2012'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'messageList.message',
//            totalProperty: 'numrows'
//        }
//    }
    
});