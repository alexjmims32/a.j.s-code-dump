Ext.define('rcrm.store.callSummaryStore',{
    extend:'Ext.data.JsonStore',
    model: 'rcrm.model.callSummaryModel',
    alias: 'callSummaryStore',
    
    autoLoad: true,
    data:[
        {status:'NEW', count:'20'},
        {status:'IN PROGRESS', count:'12'}
    ]
    
});