Ext.define('rcrm.store.callUpdateStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.updateModel',
    alias: 'callUpdateStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: ''
        }
    }
    
});