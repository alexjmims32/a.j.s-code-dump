Ext.define('rcrm.store.messageSummaryStore',{
    extend:'Ext.data.JsonStore',
    model: 'rcrm.model.messageSummaryModel',
    alias: 'messageSummaryStore',
    
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'messageSummaryList.messageSummary',
            totalProperty: 'numrows'
        }
    }
    
    
});