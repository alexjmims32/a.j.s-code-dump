Ext.define('rcrm.store.evtrecruiterListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.recruiterModel',
    alias: 'evtrecruiterListStore',
    
    autoLoad: false,
    
    data:[
        {
            'recruiterId':'RCMDIR',
            'recruiterName':'Cedric Clarke'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'event.recruiters.recruiter',
//            totalProperty: 'numrows'
//        }
//    }
    
});