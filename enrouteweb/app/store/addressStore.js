Ext.define('rcrm.store.addressStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.addressModel',
    alias: 'addressStore',
    
    autoLoad: false,

    
    proxy: {
        type: 'localstorage',
        id:'addressStore'
    }
});