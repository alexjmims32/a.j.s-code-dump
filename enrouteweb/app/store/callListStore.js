Ext.define('rcrm.store.callListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.callModel',
    alias: 'callListStore',
    
    autoLoad: false,
    
    data:[
        {
            'studentID':'1',
            'studentName':'Aaron Jones',
            'callReason':'MSG FROM PROSPECTES',
            'messageFrom':'RCMDIR',
            'messageRequestedDate':'10/10/2012',
            'status':'CLOSED',
            'callOwner':'RCMCALL',
            'callDate':'10/15/2012'
        },
        {
            'studentID':'2',
            'studentName':'Walter Biel',
            'callReason':'MSG FROM PROSPECTES',
            'messageFrom':'RCMDIR',
            'messageRequestedDate':'10/16/2012',
            'status':'INPROGRESS',
            'callOwner':'RCMCALL',
            'callDate':''
        },
        {
            'studentID':'3',
            'studentName':'Vin Myers',
            'callReason':'MSG FROM PROSPECTES',
            'messageFrom':'RCMDIR',
            'messageRequestedDate':'10/20/2012',
            'status':'NEW',
            'callOwner':'',
            'callDate':''
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'callList.call',
//            totalProperty: 'numrows'
//        }
//    }
    
});