Ext.define('rcrm.store.inventoryTypeStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'inventoryTypeStore',
    
    autoLoad: false,
    
    data:[
        {
            'name':'INV TYPE1',
            'value':'INV TYPE1'
        },
        {
            'name':'INV TYPE2',
            'value':'INV TYPE2'
        }
    ]

//    proxy: {
//        type: 'localstorage',
//        id:'inventoryTypeStore'
//    }
    
});