Ext.define('rcrm.store.eduGoalStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'eduGoalStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'eduGoalStore'
    }
    
});