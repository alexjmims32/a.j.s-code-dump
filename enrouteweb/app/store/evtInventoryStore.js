Ext.define('rcrm.store.evtInventoryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.inventoryModel',
    alias: 'evtInventoryStore',
    
    autoLoad: true,
    
    data:[
        {
            'inventoryTitle':'Banners',
            'inventoryType':'INV TYPE1',
            'count':'20'
        },
        {
            'inventoryTitle':'Posters',
            'inventoryType':'INV TYPE2',
            'count':'50'
        },
        {
            'inventoryTitle':'Bags',
            'inventoryType':'INV TYPE2',
            'count':'100'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'event.inventoryList.inventory',
//            totalProperty: 'numrows'
//        }
//    }
    
});