Ext.define('rcrm.store.templateStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.templateListModel',
    alias: 'templateStore',
    
    autoLoad: false,

    proxy: {
        type: 'localstorage',
        id:'templateStore'
    }
    
});