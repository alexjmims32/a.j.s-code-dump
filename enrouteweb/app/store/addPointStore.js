Ext.define('rcrm.store.addPointStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.addPointModel',
    alias: 'addPointStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: ''
        }
    }
    
});