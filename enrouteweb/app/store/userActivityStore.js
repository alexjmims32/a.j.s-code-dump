Ext.define('rcrm.store.userActivityStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.userActivityModel',
    alias: 'userActivityStore',
    
    autoLoad: false,
    
    data:[
        {
        'userId':'RCMDIR',
        'userName':'Cedric Clarke',
        'activityDate':'10/15/2012',
        'activityType':'CALLS-CLOSED',
        'studentID':'1',
        'studentName':'Araon Jones'
        },
        {
        'userId':'RCMDIR',
        'userName':'Cedric Clarke',
        'activityDate':'10/21/2012',
        'activityType':'PROSPECT-STUDENT',
        'studentID':'2',
        'studentName':'Walter Biel'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'userActivityList.userActivity ',
//            totalProperty: 'numrows'
//        }
//    }
    
});