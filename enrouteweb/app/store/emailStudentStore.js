Ext.define('rcrm.store.emailStudentStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.campaign.studentModel',
    alias: 'emailStudentStore',
    
    autoLoad: false,
    
    data:[
        {
            'firstName':'Araon',
            'lastName':'jones',
            'emailAddress':'araon@n2n.com',
            'status':'SENT'
        },
        {
            'firstName':'Walter',
            'lastName':'biel',
            'emailAddress':'walter@n2n.com',
            'status':'SENT'
        },
        {
            'firstName':'Vin',
            'lastName':'Myers',
            'emailAddress':'vin@n2n.com',
            'status':'FAILED'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'email.studentList.student',
//            totalProperty: 'numrows'
//        }
//    }
    
});