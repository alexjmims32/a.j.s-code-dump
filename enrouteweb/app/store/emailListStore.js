Ext.define('rcrm.store.emailListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.emailListModel',
    alias: 'emailListStore',
    
    autoLoad: false,
    
    data:[
        {
            'campaignTitle':'n2n Campaign',
            'templateName':'BLANK TEMPLATE',
            'scheduleDate':'10/15/2012',
            'status':'COMPLETED',
            'lastModifiedBy':'CRMDIR',
            'lastModifiedOn':'10/09/2012'
        },
        {
            'campaignTitle':'OPEN HOUSE',
            'templateName':'GEORGIA TEMPLATE',
            'scheduleDate':'09/20/2012',
            'status':'COMPLETED',
            'lastModifiedBy':'CRMDIR',
            'lastModifiedOn':'10/09/2012'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'emailList.email',
//            totalProperty: 'numrows'
//        }
//    }
    
});