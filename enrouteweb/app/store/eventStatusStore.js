Ext.define('rcrm.store.eventStatusStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.dropDownModel',
    alias: 'eventStatusStore',
    
    autoLoad: false,
    
    data:[
        {
            'name':'ANY',
            'value':'ANY'
        },
        {
            'name':'NOT SCHEDULED',
            'value':'NOT SCHEDULED'
        },
        {
            'name':'SCHEDULED',
            'value':'SCHEDULED'
        },
        {
            'name':'COMPLETED',
            'value':'COMPLETED'
        }
    ]

//    proxy: {
//        type: 'localstorage',
//        id:'eventStatusStore'
//    }
    
});