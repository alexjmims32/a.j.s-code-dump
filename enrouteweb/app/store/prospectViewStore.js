Ext.define('rcrm.store.prospectViewStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.prospectViewModel',
    alias: 'prospectViewStore',
    
    autoLoad: true,
    
    data:[
        {
        'prospectId':'1',
        'firstName':'Araon',
        'lastName':'Jones',
        'recruiterId':'RCMDIR',
        'recruiterName':'Cedric Clarke',
        'status':'LEAD',
        'homeNumber':'6356689527',
        'email':'araon@sample.com'
    }
]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'prospectSummary'
////            totalProperty: 'numrows'
//        }        
//    },
//    listeners: {
//        load: function(store, records, successful) {
//          Ext.getStore('prospMsgSummaryStore').loadRawData(store.proxy.reader.jsonData);
//          Ext.getStore('prospCallSummaryStore').loadRawData(store.proxy.reader.jsonData);
//          Ext.getStore('prospEmailSummaryStore').loadRawData(store.proxy.reader.jsonData);
//          Ext.getStore('prospLetterSummaryStore').loadRawData(store.proxy.reader.jsonData);
//          
//        }
//    }
    
});