Ext.define('rcrm.store.loginStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.loginModel',
    alias: 'loginStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: ''
        }
    }
});