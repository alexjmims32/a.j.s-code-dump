Ext.define('rcrm.store.prospLetterSummaryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.letterSummaryModel',
    alias: 'prospLetterSummaryStore',
    
    autoLoad: false,
    
    data:[
        {
            'message':'You have to submit more documents',
            'status':'PRINTED',
            'messageDate':'10/22/2012'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'prospectSummary.letterList.letter',
//            totalProperty: 'numrows'
//        }
//    }
    
});