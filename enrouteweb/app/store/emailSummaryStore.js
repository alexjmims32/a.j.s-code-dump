Ext.define('rcrm.store.emailSummaryStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.emailSummaryModel',
    alias: 'emailSummaryStore',
    
    autoLoad: true,
    
    data:[
    {
        'emailTitle':'n2n Campaign', 
        'inProgressCount':'0', 
        'sentCount':'1', 
        'failedCount':'2'
    },
    {
        'emailTitle':'Open House', 
        'inProgressCount':'0', 
        'sentCount':'5', 
        'failedCount':'0'
    }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'emailSummaryList.emailSummary',
//            totalProperty: 'numrows'
//        }
//    }
    
});