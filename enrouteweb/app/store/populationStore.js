Ext.define('rcrm.store.populationStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.populationListModel',
    alias: 'populationStore',
    
    autoLoad: false,

     proxy: {
        type: 'localstorage',
        id:'populationStore'
    }
    
});