Ext.define('rcrm.store.activeTempListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.templateListModel',
    alias: 'activeTempListStore',
    
    autoLoad: false,
    
     data:[
        {
            'templateId':'1',
            'templateCategory':'GENERAL',
            'templateName':'BLANK TEMPLATE',
            'lastModifiedOn':'',
            'lastModifiedBy':'',
            'templateStatus':'ACTIVE'
        },
        {
            'templateId':'2',
            'templateCategory':'GENERAL',
            'templateName':'GEORGIA TEMPLATE',
            'lastModifiedOn':'10/10/2012',
            'lastModifiedBy':'RCMDIR',
            'templateStatus':'ACTIVE'
        }
    ]

//    proxy: {
//        type: 'ajax',
//        reader: {
//            type: 'json',
//            root: 'templateList.template',
//            totalProperty: 'numrows'
//        }
//    }
    
});