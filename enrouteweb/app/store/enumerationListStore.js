Ext.define('rcrm.store.enumerationListStore', {
    extend: 'Ext.data.Store',
    model: 'rcrm.model.enumerationListModel',
    alias: 'enumerationListStore',
    
    autoLoad: false,

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'enumerationList.enumeration',
            totalProperty: 'numrows'
        }
    }
    
});