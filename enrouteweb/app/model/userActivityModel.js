Ext.define('rcrm.model.userActivityModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'activityId',
        type : 'string'
    },{
        name : 'userId',
        type : 'string'
    },{
        name : 'userName',
        type : 'string'
    },{
        name : 'activityDate',
        type : 'string'
    },{
        name : 'activityType',
        type : 'string'
    },{
        name : 'studentID',
        type : 'string'
    },{
        name : 'studentName',
        type : 'string'
    },{
        name : 'activeDate',
        type : 'string'
    },{
        name : 'institutionName',
        type : 'string'
    }
    ]
})