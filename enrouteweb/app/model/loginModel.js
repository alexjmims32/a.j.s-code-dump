Ext.define('rcrm.model.loginModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'status',
        type : 'string'
    },
    {
        name : 'operation',
        type : 'string'
    },
    {
        name : 'displayName',
        type : 'string'
    },
    {
        name : 'role',
        type : 'string'
    },
    {
        name : 'groupName',
        type : 'string'
    },
    {
        name : 'statusMessage',
        type : 'string'
    },
    {
        name:'privilegeList',
        model:'rcm.model.privilegeListModel'
    }
    
    ]
})