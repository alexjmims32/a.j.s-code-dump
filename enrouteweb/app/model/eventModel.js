Ext.define('rcrm.model.eventModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'eventId',
        type : 'string'
    },{
        name : 'eventTitle',
        type : 'string'
    },{
        name : 'eventStartDate',
        type : 'string'
    },{
        name : 'eventEndDate',
        type : 'string'
    },{
        name : 'inventoryCount',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'eventDetails',
        type : 'string'
    },{
        name : 'remarks',
        type : 'string'
    },{
        name : 'createdBy',
        type : 'string'
    },{
        name : 'createdDate',
        type : 'string'
    },{
        name : 'modifiedBy',
        type : 'string'
    },{
        name : 'modifiedDate',
        type : 'string'
    },{
        name : 'inventoryCount',
        type : 'string'
    },{
        name:'location',
        model:'rcrm.model.addressModel'
    },{
        name:'inventoryList',
        model:'rcrm.model.event.evtInvListModel'
    },{
        name:'recruiters',
        type:'rcrm.model.event.evtRecruiterListModel'
    }
    ]
})