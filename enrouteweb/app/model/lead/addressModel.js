Ext.define('rcrm.model.lead.addressModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'houseNumber',
        type : 'string'
    },
    {
        name : 'streetLine1',
        type : 'string'
    },
    {
        name : 'streetLine2',
        type : 'string'
    },
    {
        name : 'streetLine3',
        type : 'string'
    },
    {
        name : 'city',
        type : 'string'
    },
    {
        name : 'state',
        type : 'string'
    },
    {
        name : 'country',
        type : 'string'
    },
    {
        name : 'county',
        type : 'string'
    },
    {
        name : 'zipCode',
        type : 'string'
    }
    ]
});