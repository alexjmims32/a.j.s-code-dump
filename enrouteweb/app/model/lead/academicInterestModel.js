Ext.define('rcrm.model.lead.academicInterestModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'primary',
        type : 'string'
    },
    {
        name : 'secondary',
        type : 'string'
    }
    ]
});