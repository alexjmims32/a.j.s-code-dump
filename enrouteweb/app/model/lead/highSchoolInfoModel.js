Ext.define('rcrm.model.lead.highSchoolInfoModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'sbgiCode',
        type : 'string'
    },
    {
        name : 'sbgiDescription',
        type : 'string'
    },
    {
        name : 'streetLine1',
        type : 'string'
    },
    {
        name : 'streetLine2',
        type : 'string'
    },
    {
        name : 'streetLine3',
        type : 'string'
    },
    {
        name : 'city',
        type : 'string'
    },
    {
        name : 'stateCode',
        type : 'string'
    },
    {
        name : 'country',
        type : 'string'
    },
    {
        name : 'county',
        type : 'string'
    },
    {
        name : 'zip',
        type : 'string'
    },
    {
        name : 'graduationDate',
        type : 'string'
    },
    {
        name : 'gpa',
        type : 'string'
    },
    {
        name : 'levlCode',
        type : 'string'
    },
    {
        name : 'degcCode',
        type : 'string'
    },
    {
        name : 'classRank',
        type : 'string'
    },
    {
        name : 'classSize',
        type : 'string'
    },
    {
        name : 'percentile',
        type : 'string'
    }
    ]
});