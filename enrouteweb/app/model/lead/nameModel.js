Ext.define('rcrm.model.lead.nameModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'firstName',
        type : 'string'
    },
    {
        name : 'middleName',
        type : 'string'
    },
    {
        name : 'lastName',
        type : 'string'
    }
    ]
});