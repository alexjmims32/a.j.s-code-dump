Ext.define('rcrm.model.lead.enrolmentInfoModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'startTerm',
        type : 'string'
    },
    {
        name : 'academicAreaOfInterest',
        model:'rcrm.model.lead.academicInterestModel'
    }    
    ]
});