Ext.define('rcrm.model.lead.phoneModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'number',
        type : 'string'
    },
    {
        name : 'extension',
        type : 'string'
    }
    ]
});