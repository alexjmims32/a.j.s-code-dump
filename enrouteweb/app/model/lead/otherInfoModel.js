Ext.define('rcrm.model.lead.otherInfoModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'levelInterest',
        type : 'string'
    },
    {
        name : 'importantFactor',
        type : 'string'
    }
    ]
});