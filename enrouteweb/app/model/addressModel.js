Ext.define('rcrm.model.addressModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'address1',
        type : 'string'
    },
    {
        name : 'address2',
        type : 'string'
    },
    {
        name : 'city',
        type : 'string'
    },
    {
        name : 'state',
        type : 'string'
    },
    {
        name : 'zip',
        type : 'string'
    },
    {
        name : 'country',
        type : 'string'
    }
    ]
});