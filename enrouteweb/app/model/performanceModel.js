Ext.define('rcrm.model.performanceModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'month',
        type : 'string'
    },{
        name : 'lead',
        type : 'string'
    },{
        name : 'prospect',
        type : 'string'
    },{
        name : 'applicant',
        type : 'string'
    },{
        name : 'student',
        type : 'string'
    }
    ]
});