Ext.define('rcrm.model.letterListModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'messageId',
        type : 'string'
    },{
        name : 'letterType',
        type : 'string'
    },{
        name : 'printDate',
        type : 'string'
    },{
        name : 'studentId',
        type : 'string'
    },{
        name : 'studentName',
        type : 'string'
    },{
        name : 'messageCategory',
        type : 'string'
    },{
        name : 'messageDate',
        type : 'string'
    },{
        name : 'message',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'firstName',
        type : 'string'
    },{
        name : 'lastName',
        type : 'string'
    }
    ]
})