Ext.define('rcrm.model.attendeeModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'attenderId',
        type : 'string'
    },{
        name : 'attenderType',
        type : 'string'
    },{
        name : 'firstName',
        type : 'string'
    },{
        name : 'lastName',
        type : 'string'
    },{
        name : 'middleName',
        type : 'string'
    },{
        name : 'email',
        type : 'string'
    },{
        name : 'cellnumber',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    
    }
    ]
});