Ext.define('rcrm.model.userScorecardModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'userId',
        type : 'string'
    },{
        name : 'userName',
        type : 'string'
    },{
        name : 'activityDate',
        type : 'string'
    },{
        name : 'activityType',
        type : 'string'
    },{
        name : 'activityCount',
        type : 'string'
    },{
        name : 'activityScore',
        type : 'string'
    }
    ]
})