Ext.define('rcrm.model.inventoryModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'inventoryId',
        type : 'string'
    },{
        name : 'description',
        type : 'string'
    },{
        name : 'inventoryTitle',
        type : 'string'
    },{
        name : 'inventoryType',
        type : 'string'
    },{
        name : 'createdDate',
        type : 'string'
    },{
        name : 'createdBy',
        type : 'string'
    },{
        name : 'modifiedDate',
        type : 'string'
    },{
        name : 'modifiedBy',
        type : 'string'
    },{
        name : 'count',
        type : 'string'
    },{
        name : 'remarks',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    }
    ]
})