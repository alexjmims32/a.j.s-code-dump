Ext.define('rcrm.model.dropDownModel',{
    extend:'Ext.data.Model',
    
    fields:[
        {
            name : 'name',
            type : 'string'
        },{
            name : 'value',
            type : 'string'
        }
    ]
})