Ext.define('rcrm.model.privilegeModel',{
    extend:'Ext.data.Model',
    fields:[
        {
            name:'functionality',
            type:'string'
        },
        {
            name:'privilegeLevel',
            type:'string'
        }
    ]
    
});