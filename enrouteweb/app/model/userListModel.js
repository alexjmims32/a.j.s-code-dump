Ext.define('rcrm.model.userListModel',{
    extend:'Ext.data.Model',
    
    fields:[
        {
            name : 'spridenId',
            type : 'string'
        },
        {
            name : 'spridenPidm',
            type : 'string'
        },
        {
            name : 'firstName',
            type : 'string'
        },
        {
            name : 'lastName',
            type : 'string'
        },
        {
            name : 'middleName',
            type : 'string'
        },
        {
            name : 'email',
            type : 'string'
        },
        {
            name : 'userId',
            type : 'string'
        }
    ]
})