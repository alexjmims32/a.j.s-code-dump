Ext.define('rcrm.model.emailSummaryModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'emailTitle',
        type : 'string'
    },{
        name : 'inProgressCount',
        type : 'string'
    },{
        name : 'sentCount',
        type : 'string'
    },{
        name : 'failedCount',
        type : 'string'
    },{
        name : 'campaignTitle',
        type : 'string'
    },{
        name : 'templateName',
        type : 'string'
    },{
        name : 'htmlText',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    }
    ]
})