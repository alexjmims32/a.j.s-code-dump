Ext.define('rcrm.model.callModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'callID',
        type : 'string'
    },{
        name : 'studentID',
        type : 'string'
    },{
        name : 'studentName',
        type : 'string'
    },{
        name : 'callReason',
        type : 'string'
    },{
        name : 'messageFrom',
        type : 'string'
    },{
        name : 'message',
        type : 'string'
    },{
        name : 'messageRequestedDate',
        type : 'string'
    },{
        name : 'priority',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'callOwner',
        type : 'string'
    },{
        name : 'callDate',
        type : 'string'
    },{
        name : 'isHot',
        type : 'string'
    },{
        name : 'homeNumber',
        type : 'string'
    },{
        name : 'email',
        type : 'string'
    },{
        name : 'contactInstructions',
        type : 'string'
    },{
        name : 'comment',
        type : 'string'
    },{
        name : 'contactedByPhone',
        type : 'string'
    },{
        name : 'contactedByEmail',
        type : 'string'
    },{
        name : 'leftMessage',
        type : 'string'
    },{
        name : 'noContactPossible',
        type : 'string'
    }
    ]
})