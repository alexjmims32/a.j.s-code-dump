Ext.define('rcrm.model.prospectListModel',{
    extend:'Ext.data.Model',
    
    fields:[
        {
            name : 'prospectId',
            type : 'string'
        },{
            name : 'firstName',
            type : 'string'
        },{
            name : 'lastName',
            type : 'string'
        },{
            name : 'recruiterId',
            type : 'string'
        },
        {
            name : 'recruiterName',
            type : 'string'
        },
        {
            name : 'status',
            type : 'string'
        },
        {
            name : 'addedDate',
            type : 'string'
        }
    ]
})