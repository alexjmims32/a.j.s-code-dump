Ext.define('rcrm.model.populationListModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'populationId',
        type : 'string'
    },{
        name : 'populationTitle',
        type : 'string'
    },{
        name : 'description',
        type : 'string'
    },{
        name : 'lastModifiedBy',
        type : 'string'
    },{
        name : 'lastModifiedOn',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'createdBy',
        type : 'string'
    },{
        name : 'createdOn',
        type : 'string'
    }
    ]
})