Ext.define('rcrm.model.recruiterModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'recruiterId',
        type : 'string'
    },{
        name : 'recruiterName',
        type : 'string'
    },{
        name : 'recruiterType',
        type : 'string'
    }
    ]
});