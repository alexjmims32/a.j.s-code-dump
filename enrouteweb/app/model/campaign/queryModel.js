Ext.define('rcrm.model.campaign.queryModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'queryId',
        type : 'string'
    },{
        name : 'queryName',
        type : 'string'
    },{
        name : 'query',
        type : 'string'
    }
    ]
});