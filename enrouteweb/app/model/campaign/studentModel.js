Ext.define('rcrm.model.campaign.studentModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'id',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'firstName',
        type : 'string'
    },{
        name : 'lastName',
        type : 'string'
    },{
        name : 'emailAddress',
        type : 'string'
    }
    ]
});