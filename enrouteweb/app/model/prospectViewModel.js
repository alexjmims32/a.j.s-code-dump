Ext.define('rcrm.model.prospectViewModel',{
    extend:'Ext.data.Model',
    
    fields:[
        {
            name : 'prospectId',
            mapping:'prospect.prospectId',
            type : 'string'
        },{
            name : 'firstName',
            mapping:'person.studentBasicInfo[0].firstName',
            type : 'string'
        },{
            name : 'lastName',
            mapping:'person.studentBasicInfo[0].lastName',
            type : 'string'
        },{
            name : 'recruiterId',
            mapping:'prospect.recruiterId',
            type : 'string'
        },{
            name : 'recruiterName',
            mapping:'prospect.recruiterName',
            type : 'string'
        },{
            name : 'status',
            mapping:'prospect.status',
            type : 'string'
        },{
            name : 'homeNumber',
            mapping:'person.telephoneInfo[0].phoneNumber',
            type : 'string'
        },{
            name:'email',
            mapping:'person.personal[0].emailAddress',
            type:'string'
//        }
//        {
//            name:'emailSummary',
//            mapping:''
//        },
//        {
//            name:'callSummary',
//            mapping:''
//        },
//        {
//            name:'letterSummary',
//            mapping:''
//        },
//        {
//            name:'messageSummary',
//            mapping:''
        }
    ]
})