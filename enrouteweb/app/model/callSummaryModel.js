Ext.define('rcrm.model.callSummaryModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'status',
        type : 'string'
    },{
        name : 'count',
        type : 'string'
    },{
        name : 'messageFrom',
        type : 'string'
    },{
        name : 'message',
        type : 'string'
    },{
        name : 'messageRequestedDate',
        type : 'string'
    }
    ]
})