Ext.define('rcrm.model.templateListModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'templateId',
        type : 'string'
    },{
        name : 'templateCategory',
        type : 'string'
    },{
        name : 'templateName',
        type : 'string'
    },{
        name : 'lastModifiedOn',
        type : 'string'
    },{
        name : 'lastModifiedBy',
        type : 'string'
    },{
        name : 'templateStatus',
        type : 'string'
    },{
        name : 'createdBy',
        type : 'string'
    },{
        name : 'createdOn',
        type : 'string'
    },{
        name : 'htmlText',
        type : 'string'
    },{
        name : 'loggedInUser',
        type : 'string'
    }
    ]
})