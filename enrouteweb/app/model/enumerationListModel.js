Ext.define('rcrm.model.enumerationListModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'enumType',
        type : 'string'
    },{
        name : 'enumValue',
        type : 'string'
    },{
        name : 'displayValue',
        type : 'string'
    },{
        name : 'sequence',
        type : 'string'
    }
    ]
})