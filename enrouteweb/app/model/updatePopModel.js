Ext.define('rcrm.model.updatePopModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'populationId',
        type : 'string'
    },{
        name : 'populationTitle',
        type : 'string'
    },{
        name : 'description',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'loggedInUser',
        type : 'string'
    },{
        name:'personList',
        model:'rcrm.model.campaign.studentModel'
    },{
        name:'queryList',
        model:'rcrm.model.campaign.queryModel'
    }
    ]
});