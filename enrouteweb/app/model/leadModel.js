Ext.define('rcrm.model.leadModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'email',
        type : 'string'
    },
    {
        name : 'gender',
        type : 'string'
    },
    {
        name : 'birthDate',
        type : 'string'
    },
    {
        name : 'ethnicity',
        type : 'string'
    },
    {
        name : 'raceCode',
        type : 'string'
    },
    {
        name : 'prelCode',
        type : 'string'
    },
    {
        name:'name',
        model:'rcm.model.lead.nameModel'
    },
    {
        name:'address',
        model:'rcm.model.lead.addressModel'
    },
    {
        name:'phones',
        model:'rcm.model.lead.phoneListModel'
    },
    {
        name:'highSchoolInfo',
        model:'rcm.model.lead.highSchoolInfoModel'
    },
    {
        name:'enrollmentInfo',
        model:'rcm.model.lead.enrolmentInfoModel'
    },
    {
        name:'otherInfo',
        model:'rcm.model.lead.otherInfoModel'
    },
    {
        name:'credentials',
        model:'rcm.model.lead.credentialModel'
    }
    
    ]
})