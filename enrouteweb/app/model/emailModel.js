Ext.define('rcrm.model.emailModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'campaignId',
        type : 'string'
    },{
        name : 'campaignTitle',
        type : 'string'
    },{
        name : 'templateId',
        type : 'string'
    },{
        name : 'templateName',
        type : 'string'
    },{
        name : 'scheduleDate',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'lastModifiedOn',
        type : 'string'
    },{
        name : 'lastModifiedBy',
        type : 'string'
    },{
        name : 'emailSubject',
        type : 'string'
    },{
        name : 'emailReply',
        type : 'string'
    },{
        name : 'htmlText',
        type : 'string'
    },{
        name : 'count',
        type : 'string'
    },{
        name : 'sentCount',
        type : 'string'
    },{
        name : 'failCount',
        type : 'string'
    },{
        name : 'scheduleCount',
        type : 'string'
    },{
        name : 'createdOn',
        type : 'string'
    },{
        name : 'createdBy',
        type : 'string'
    }
    ]
})