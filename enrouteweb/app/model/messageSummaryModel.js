Ext.define('rcrm.model.messageSummaryModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'status',
        type : 'string'
    },{
        name : 'count',
        type : 'string'
    },{
        name : 'comments',
        type : 'string'
    },{
        name : 'messageDate',
        type : 'string'
        },{
        name : 'messageStatus',
        type : 'string'
    }
    ]
});