Ext.define('rcrm.model.messageModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'messageId',
        type : 'string'
    },{
        name : 'firstName',
        type : 'string'
    },{
        name : 'lastName',
        type : 'string'
    },{
        name : 'prospectId',
        type : 'string'
    },{
        name : 'messageStatus',
        type : 'string'
    },{
        name : 'messageDate',
        type : 'string'
    },{
        name : 'campaignName',
        type : 'string'
    },{
        name : 'comments',
        type : 'string'
    },{
        name : 'internalComments',
        type : 'string'
    },{
        name : 'prospectStatus',
        type : 'string'
    },{
        name : 'campaignId',
        type : 'string'
    },{
        name : 'homeNumber',
        type : 'string'
    },{
        name : 'cellNumber',
        type : 'string'
    },{
        name : 'email',
        type : 'string'
    },{
        name : 'recruiterId',
        type : 'string'
    },{
        name : 'recruiterName',
        type : 'string'
    }
    
    ]
})