Ext.define('rcrm.model.letterSummaryModel',{
    extend:'Ext.data.Model',
    
    fields:[
    {
        name : 'message',
        type : 'string'
    },{
        name : 'status',
        type : 'string'
    },{
        name : 'messageDate',
        type : 'string'
    }
    ]
})