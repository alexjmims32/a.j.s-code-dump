Ext.define('rcrm.model.topRecruiterModel',{
    extend:'Ext.data.Model',
    
    fields:[
        {
            name : 'name',
            type : 'string'
        },{
            name : 'points',
            type : 'string'
        }
    ]
})