Ext.define('rcrm.model.applicationListModel',{
    extend:'Ext.data.Model',
    
    fields:[
        {
            name : 'studentId',
            type : 'string'
        },
        {
            name : 'studentName',
            type : 'string'
        },
        {
            name : 'studentType',
            type : 'string'
        },
        {
            name : 'admissionType',
            type : 'string'
        },
        {
            name : 'term',
            type : 'string'
        },
        {
            name : 'group',
            type : 'string'
        },
        {
            name : 'status',
            type : 'string'
        },
        {
            name : 'hot',
            type : 'string'
        },
        {
            name : 'owner',
            type : 'string'
        }
    ]
})