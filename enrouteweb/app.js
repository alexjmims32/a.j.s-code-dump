var viewport;
var loginUserId;
var addpopup;
var priviousCallView;
var previousView;
var prospectController;
var emailContrller;
var loadingMask;
var baseUrl = 'http://localhost:8090/recruitcrmservices/json';
var chart;

Ext.Loader.setConfig({enabled:true});

Ext.application({
    //    requires: ['Ext.container.Viewport'],
    name: 'rcrm',
    appFolder: 'app',
    
    views:[
    'loginView',
    'mainView'
    ],
    
    controllers: [
    'loginController',
    'mainController',
    'dashboardController',
    'prospectController',
    'campaignController',
    'messageController',
    'callController',
    'letterController',
    'activityController',
    'eventController',
    'inventoryController',
    'ticketController',
    'verificationController',
    'applicationController',
    'documentController'
    ],

    launch: function() {

        // Create the vieport and store the reference in global variable
        viewport = Ext.create('Ext.Viewport', {
            alias:'viewPort',
            layout: 'card'
        });
        
        // Add loginView (first one) to viewport
        viewport.add(Ext.widget('loginView'));
        Ext.Ajax.timeout = 60000;
        
            loadingMask = new Ext.LoadMask(viewport, {
                msg:"Loading..."
            });
        Ext.Ajax.on('beforerequest', function () {
            // showing the loading mask
            if(!loadingMask.isVisible())
                loadingMask.show();
        });
       
        Ext.Ajax.on('requestcomplete', function () {
            // hidding the loading mask
            loadingMask.hide();
        });
        
        Ext.Ajax.on('requestexception', function (conn, response, options, eOpts) {

            if (response.status == 401) {
                Ext.Msg.alert(' ', 'Invalid username or password');
            }
            else {
                Ext.Msg.alert(' ', '<center>Server error has occured.Please try again</center>');
            }

            loadingMask.hide();
        });
    }   
});

