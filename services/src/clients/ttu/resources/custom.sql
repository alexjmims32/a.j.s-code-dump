P_GENERATE_CODE=me_attendence_objects_new.pz_create_update_attn_code(?,?,?,?,?,?,?,?,?)

Q_GET_CODE_DETAILS=SELECT INSTRUCTOR_ID,TERM_CODE,CRN,ATTENDENCE_DATE,CODE_EXPIRY_DATETIME FROM ATT_INSTRUCTOR_CODE_REF WHERE attendence_code='%s'
Q_ADD_ATT_NOTIFICATION= insert into noticelog (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE) values (noticelog_seq.NEXTVAL, %s, '%s', '%s', TRUNC(LAST_DAY(SYSDATE)+1), 'PUSH', 0, 0, '%s', sysdate, 0, '%s', '%s', sysdate)

#Courses
Q_GET_FACULTY_COURSES=SELECT sys_guid() as id,FACULTY_ID,FACULTY_NAME,TERM_CODE,CRN,CRSE_NUMB,SUBJECT,SUBJECT_DESC,COURSE_TITLE,CAMPUS,COURSE_LEVEL,DEPARTMENT,COLLEGE,TERM_DESC,'' AS CREDITS FROM ATT_FACULTY_CRSE WHERE FACULTY_ID = '%s' and TERM_CODE =  '%s'
#MEETING
Q_GET_MEETING=SELECT sys_guid() as id,term_code,crn,begin_time,end_time,bldg_code,latitude,longitude,room_code,start_date,end_date,sunday,monday,tueday,wedday,thuday,friday,satday,mtyp_code,SCHEDULE_CODE,SCHEDULE_DESC,' ' as period FROM CRSE_MEET_VW A LEFT OUTER JOIN MAPS B ON A.BLDG_CODE = B.BUILDING_CODE WHERE A.TERM_CODE='%s' AND A.CRN='%s'

Q_ABSENT_BY_CLASS=select count(*),'Total Students','1' as rowcount from ATT_REGISTERED_STUDENTS WHERE term_code= '%s' and crn= '%s' UNION SELECT COUNT(*) as Count, case when ATTENDENCE_IND='Y' Then 'Present Students' when ATTENDENCE_IND='N' Then 'Absent Students' End as Name,'2' as rowcount from ATT_ATTENDENCE_SHEET WHERE instructor_id = '%s' and term_code= '%s' and crn= '%s'  and attendence_date = TO_DATE('%s','MM/DD/YYYY') and attn_class_start_time = '%s' and attn_class_end_time = '%s' group by ATTENDENCE_IND order by rowcount

Q_ATTENDENCE_BY_DATE=SELECT name from ATT_ATTENDENCE_SHEET WHERE instructor_id = '%s' and term_code= '%s' and crn= '%s'  and attendence_date = TO_DATE('%s','MM/DD/YYYY') and attn_class_start_time = '%s' and attn_class_end_time = '%s' and ATTENDENCE_IND = '%s'


Q_VIEW_CLASSROASTER=select sys_guid() as id,student_id, NAME, TERM_CODE, CRN, EMAIL, ATTENDENCE_IND,ATTENDENCE_ID from ATT_ATTENDENCE_SHEET_NEW  where term_code='%s' and crn='%s' and attn_class_start_time = '%s' and attn_class_end_time = '%s'
Q_VIEW_ATTENDENCE=SELECT attendence_date,name,attendence_ind  from ATT_ATTENDENCE_SHEET WHERE student_id = '%s' and attendence_date between TO_DATE('%s','MM/DD/YYYY') and TO_DATE('%s','MM/DD/YYYY') and term_code= '%s' and crn= '%s' 
Q_GET_INDICATOR=select attendence_date,name,attendence_ind,crn,term_code from att_attendence_sheet where STUDENT_ID = '%s' and ATTENDENCE_DATE = To_DATE('%s', 'MM/DD/YYYY') and term_code = '%s'

Q_GET_REGISTERED_COURSES=SELECT sys_guid() as id,ACTION_DESC,ACTION_CODE,LINK_IDENT,REGISTERED_DATE,REGISTERED_USER,COURSE_NUMBER,COURSE_TITLE,SUBJECT,SUBJECT_DESCRIPTION,CREDITS,COURSE_LEVEL,LECTURE_HOURS,SCHEDULE_TYPE,DEPARTMENT,CRSE_LOCATION,PREREQUISITE,CRN,FACULTY,TERM_CODE,COLLEGE,TERM_DESC,STATUS_MESSAGE,REGISTRATION_STATUS,FLAG,ERROR_MESSAGE,WAIT_LIST_PRIORITY,AVAILABLE_WAIT_LIST,FLAG_DESC,VAR_CREDIT,VAR_CREDIT_HRS,FACULTY_EMAIL,SECT_SEQ_NUMB,CAST(CASE WHEN term_code in (select term_code from terms_to_register_vw) THEN 'Y' ELSE 'N' END as char) as dropFlag FROM ME_STUDENT_CRSE_REGSTRN_VW A WHERE A.STUDENT_ID = '%s' AND A.TERM_CODE = '%s'
Q_GET_MAPS=SELECT id,campus_code,building_code,building_name,building_desc,phone,email,img_url,longitude,latitude,TITLE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,address,website,principalname, center_flag,category FROM maps A JOIN campus B ON a.campus_code=b.code


P_TERMS_CONDITIONS_TO_REGISTER= me_warning.p_displayawardeligable_new(?,?,?,?,?)