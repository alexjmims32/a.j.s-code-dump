package com.n2n.enroll.proxy.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class ProxySessionAuditBo implements java.io.Serializable{

		@Id
		private String id;

		@Column(name = "USERNAME")
		public String username;
		@Column(name = "STUDENTID")
		public String studentid;
		@Column(name = "REQUESTTIMESTAMP")
		public String requestTimestamp;
		@Column(name = "LOGINTIME")
		public String loginTime;
		@Column(name = "IPADDRESS")
		public String ipAddress;
		@Column(name = "LOGOUTTIME")
		public String logoutTime;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getStudentid() {
			return studentid;
		}
		public void setStudentid(String studentid) {
			this.studentid = studentid;
		}
		public String getRequestTimestamp() {
			return requestTimestamp;
		}
		public void setRequestTimestamp(String requestTimestamp) {
			this.requestTimestamp = requestTimestamp;
		}
		public String getLoginTime() {
			return loginTime;
		}
		public void setLoginTime(String loginTime) {
			this.loginTime = loginTime;
		}
		public String getIpAddress() {
			return ipAddress;
		}
		public void setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
		}
		public String getLogoutTime() {
			return logoutTime;
		}
		public void setLogoutTime(String logoutTime) {
			this.logoutTime = logoutTime;
		}		
	
}
