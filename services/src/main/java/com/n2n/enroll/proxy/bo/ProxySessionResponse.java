package com.n2n.enroll.proxy.bo;

public class ProxySessionResponse {
	private String status;
	private String studentId;
		
	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
