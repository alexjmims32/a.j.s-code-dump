package com.n2n.enroll.proxy.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.contact.service.ContactService;
import com.n2n.enroll.ldap.PersonDao;
import com.n2n.enroll.proxy.bo.ProxySessionAuditBo;
import com.n2n.enroll.proxy.bo.ProxySessionResponse;
import com.n2n.enroll.util.AppUtil;

@Service
public class ProxyService {
	
	@Resource
	SessionFactory sessionFactory;
	
	@Resource
	PersonDao personDao;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	UserService userService;

	protected final Log log = LogFactory.getLog(getClass());

	public ProxySessionResponse startSession(String username, String studentId,String ipAddress) {
		ProxySessionResponse proxySessionResponse = new ProxySessionResponse();
		try {
			boolean userExists = userService.checkUserExists(studentId);
			if(userExists){
				saveProxyRecord(username,studentId, ipAddress);
				proxySessionResponse.setStatus("SUCCESS");
				proxySessionResponse.setStudentId(studentId);
			}else{
				proxySessionResponse.setStatus("Invalid UTAD Username");
			}
		} catch (Exception e) {
			log.error("Error in starting proxy session......"
					+ e.getMessage());
			proxySessionResponse.setStatus("Error in starting proxy session......"
					+ e.getMessage());
		}
		return proxySessionResponse;
	}
	
	public void saveProxyRecord(String username, String studentId, String ipAddress) {

		try {
			String sql = AppUtil.getProperty("Q_INSERT_PROXY_DATA");
			
			sql =AppUtil.format(sql, username,studentId,"sysdate",ipAddress,null);
			

			AppUtil.runADeleteQuery(sql);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
	}
	
	public ProxySessionResponse stopSession(String username, String studentId,String ipAddress) {
		ProxySessionResponse proxySessionResponse = new ProxySessionResponse();
		try {
			proxySessionResponse.setStatus("SUCCESS");
			String sql = AppUtil.getProperty("Q_GET_PROXY_DATA");
			
			sql = AppUtil.format(sql, username,studentId,ipAddress);
			

			List<ProxySessionAuditBo> l = (List<ProxySessionAuditBo>) AppUtil.runAQueryCurrentSession(sql, ProxySessionAuditBo.class);
			for (ProxySessionAuditBo proxySessionAuditBo : l) {
				sql = AppUtil.getProperty("Q_UPDATE_PROXY_DATA");
				
				sql = AppUtil.format(sql, proxySessionAuditBo.getUsername(),proxySessionAuditBo.getStudentid(),proxySessionAuditBo.getIpAddress());
				
				AppUtil.runADeleteQuery(sql);
			}
			

		} catch (Exception e) {
			log.error("Error in retriveing proxy stop session......"
					+ e.getMessage());
			proxySessionResponse.setStatus("Error in retriveing proxy stop session......"
					+ e.getMessage());
		}

		return proxySessionResponse;
	}

}