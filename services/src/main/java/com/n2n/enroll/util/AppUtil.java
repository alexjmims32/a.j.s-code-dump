package com.n2n.enroll.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message.RecipientType;

import oracle.sql.ARRAY;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.gson.Gson;
import com.n2n.enroll.util.hibernate.HibernateSessionFactory;

@SuppressWarnings("rawtypes")
public class AppUtil {
	protected static final Log log = LogFactory.getLog(AppUtil.class);
	private static Properties properties;
	private static Properties instSearchProperties;

	private static Gson gson = new Gson();

	public static String substring(String inputStr, int maxLength) {
		if (inputStr == null || inputStr.length() <= maxLength)
			return inputStr;

		return inputStr.substring(0, maxLength);
	}

	public static String getProperty(String key) throws Exception {
		if (properties == null) {
			InputStream stream = AppUtil.class
					.getResourceAsStream("/banner.sql");
			properties = new Properties();
			properties.load(stream);
			stream.close();

			InputStream stream1 = AppUtil.class
					.getResourceAsStream("/mobedu.sql");
			properties.load(stream1);
			stream1.close();

			InputStream stream2 = AppUtil.class
					.getResourceAsStream("/settings.properties");
			properties.load(stream2);
			stream2.close();

			try {
				InputStream stream3 = AppUtil.class
						.getResourceAsStream("/custom.sql");
				properties.load(stream3);
				stream3.close();
			} catch (Exception e) {
				// Do Nothing
			}
		}
		return properties.getProperty(key);
	}

	public static String getInstSearchProperty(String key) throws Exception {
		if (instSearchProperties == null) {
			InputStream stream = AppUtil.class
					.getResourceAsStream("/instantSearch.config.xml");
			instSearchProperties = new Properties();
			instSearchProperties.loadFromXML(stream);
			stream.close();
		}
		return instSearchProperties.getProperty(key);
	}

	public static ARRAY getVArray(String key) throws Exception {
		if (properties == null) {
			InputStream stream = AppUtil.class
					.getResourceAsStream("/ApplicationResources.properties");
			properties = new Properties();
			properties.load(stream);
			stream.close();
		}
		return getVArray(key);
	}

	public static String readFileAsString(String filePath) throws IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}

	public static Date makeDate(Object dateString) {
		if (dateString == null)
			return null;
		return makeDate(dateString.toString());
	}

	public static Date makeDate(String dateString) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		try {
			Date theDate = df.parse(dateString);
			return theDate;
		} catch (ParseException e) {
			log.error("StackTrace", e);
		}
		return null;
	}

	public static String getUsername() {
		String username = "";
		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			username = "anonymous";
		} else {
			Object principal = SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();
			username = ((UserDetails) principal).getUsername();
		}
		return username;
	}

	public static List runAQuery(String strQuery) {
		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.getCurrentSession();

		try {
			log.debug(strQuery);
			Query query = session.createSQLQuery(strQuery);
			List l = query.list();
			return l;
		} catch (RuntimeException e) {
			throw e;
		} finally {
		}
	}

	public static List runAOpenSessionQuery(String strQuery) {
		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.openSession();

		try {
			log.debug(strQuery);
			Query query = session.createSQLQuery(strQuery);
			List l = query.list();
			return l;
		} catch (RuntimeException e) {
			throw e;
		} finally {
			session.close();
		}
	}

	public static List runAQuery(String strQuery, int pageOffset, int pageSize) {

		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.getCurrentSession();

		try {
			log.debug(strQuery);
			Query query = session.createSQLQuery(strQuery);
			query.setFirstResult(pageSize * (pageOffset - 1));
			query.setMaxResults(pageSize);
			List l = query.list();
			return l;
		} catch (RuntimeException e) {
			throw e;
		} finally {
		}
	}

	public static List runAQueryCurrentSession(String strQuery, Class className) {

		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.getCurrentSession();

		try {
			log.debug(strQuery);
			List l = session.createSQLQuery(strQuery).addEntity(className)
					.list();
			;
			return l;
		} catch (RuntimeException e) {
			throw e;
		}
	}

	public static List runAQueryOpenSession(String strQuery, int pageOffset,
			int pageSize, Class className) {

		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.getCurrentSession();

		try {
			log.debug(strQuery);
			SQLQuery query = session.createSQLQuery(strQuery).addEntity(
					className);
			query.setFirstResult(pageSize * (pageOffset - 1));
			query.setMaxResults(pageSize);
			// List l = query.list();

			return query.list();
		} catch (RuntimeException e) {
			throw e;
		}
	}

	public static BigDecimal runACountQuery(String strQuery) {

		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.getCurrentSession();

		try {
			log.debug(strQuery);
			Query query = session.createSQLQuery(strQuery);
			BigDecimal l = BigDecimal.valueOf(AppUtil.getDoubleValue(query
					.uniqueResult()));
			return l;
		} catch (RuntimeException e) {
			throw e;
		} finally {
		}
	}

	public static int getIntValue(Object inputValue) {
		if (inputValue == null)
			return 0;

		return Integer.valueOf(inputValue.toString());
	}

	public static String getFloatValue(Object inputValue) {
		if (inputValue == null)
			return "0.00";
		DecimalFormat form = new DecimalFormat("0.00");
		return form.format(Double.valueOf(removeComma(inputValue.toString())));

	}

	public static String removeComma(String input) {
		if (input == null)
			return "";
		return input.replace(",", "");

	}

	public static String phoneNumberFormat(String phoneNumber) {

		return phoneNumber.replaceFirst("(\\d{3})(\\d{3})(\\d{4})", "$1-$2-$3");
	}

	public static double getDoubleValue(Object inputValue) {
		if (inputValue == null)
			return 0.00;
		return (Double.valueOf(inputValue.toString()));

	}

	public static long getLongValue(Object inputValue) {
		if (inputValue == null)
			return 0;

		return Long.valueOf(inputValue.toString());
	}

	public static String getStringValue(Object inputValue) {
		if (inputValue == null)
			return "NA";

		return inputValue.toString();
	}

	public static Date getDateValue(Object inputValue) {
		if (inputValue == null)
			return null;

		return AppUtil.makeDate(inputValue);
	}

	public static Timestamp getCurrentTimeStamp() {
		return new Timestamp((new Date()).getTime());
	}

	public static Boolean IsNumber(String number) {
		try {
			Double.parseDouble(number);
			return (true);
		} catch (Exception e) {
			return (false);
		}
	}

	public static String getJSONFrom(Object o) {
		String jsonResult = gson.toJson(o);
		return jsonResult;
	}
	
	public static <T> T getJsonFormString(String json,  Class<T> type) {
		return (T) gson.fromJson(json, type);
	}

	public static String getTime(String time) {
		String formatedTime = "";
		if (time == null || time.equals("")) {
			formatedTime = "NA";
			return formatedTime;
		}
		if (time != null || !(time.equals(""))) {
			if (Integer.parseInt(time.substring(0, time.length() - 2)) < 12) {
				if ((time.substring(0, time.length() - 2).length()) != 2) {
					formatedTime = "0" + time.substring(0, time.length() - 2)
							+ ":" + time.substring(time.length() - 2) + "AM";
				} else {
					formatedTime = time.substring(0, time.length() - 2) + ":"
							+ time.substring(time.length() - 2) + "AM";
				}
			} else if (Integer.parseInt(time.substring(0, time.length() - 2)) == 12) {
				formatedTime = time.substring(0, time.length() - 2) + ":"
						+ time.substring(time.length() - 2) + "PM";
			} else if (Integer.parseInt(time.substring(0, time.length() - 2)) > 12
					&& Integer.parseInt(time.substring(0, time.length() - 2)) < 24) {

				int hours = Integer.parseInt(time.substring(0,
						time.length() - 2)) - 12;
				if (hours < 10) {
					formatedTime = "0" + hours + ":"
							+ time.substring(time.length() - 2) + "PM";
				} else {
					formatedTime = hours + ":"
							+ time.substring(time.length() - 2) + "PM";
				}
			} else if (Integer.parseInt(time.substring(0, time.length() - 2)) == 24) {
				formatedTime = "00:" + time.substring(time.length() - 2) + "PM";
			} else {
				formatedTime = "00:00AM";
			}

		}
		return formatedTime;
	}

	public static int runADeleteQuery(String strQuery) {

		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.getCurrentSession();
		int l = 0;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			tx.begin();
			log.debug(strQuery);
			Query query = session.createSQLQuery(strQuery);
			l = query.executeUpdate();
			tx.commit();
			return l;
		} catch (RuntimeException e) {
			l = 0;
			tx.rollback();
			throw e;
		} finally {
			// session.close();
		}
	}

	public static int runADeleteQueryNewSession(String strQuery) {

		SessionFactory sessionFactory = (SessionFactory) HibernateSessionFactory
				.getBean("sessionFactory");
		Session session = sessionFactory.openSession();
		int l = 0;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			tx.begin();
			log.debug(strQuery);
			Query query = session.createSQLQuery(strQuery);
			l = query.executeUpdate();
			tx.commit();
			return l;
		} catch (RuntimeException e) {
			l = 0;
			tx.rollback();
			throw e;
		} finally {
			 session.close();
		}
	}
	
	public static String getSqlFmtStringValue(Object inputValue) {
		if (inputValue == null)
			return "";

		String stringValue = inputValue.toString();

		stringValue = stringValue.replace("'", "''");
		stringValue = stringValue.replace("\"", "\"\"");

		return stringValue;
	}

	public String sendEmail(String fromAddress, String fromName,
			String toAddress, String toName, String subject, String body) {
		String status = "";
		try {
			Email email = new Email();
			String emailHost = AppUtil.getProperty("EMAIL_HOST");
			String emailPort = AppUtil.getProperty("EMAIL_PORT");
			email.setFromAddress(fromName, fromAddress);
			email.setSubject(subject);

			String[] toAdd = toAddress.split(",");

			// Add To address
			for (int i = 0; i < toAdd.length; i++) {
				email.addRecipient(toAdd[i].toString(), toAdd[i].toString(),
						RecipientType.TO);

				// email.setTextHTML(body);
				email.setTextHTML("<html> <body>" + body + "</body> </html>");

				String mailboxUserName = AppUtil
						.getProperty("EMAIL_MAILBOX_LOGIN");
				String mailboxPassword = AppUtil
						.getProperty("EMAIL_MAILBOX_PASSWORD");

				Mailer mailer = new Mailer(emailHost,
						Integer.parseInt(emailPort), mailboxUserName,
						mailboxPassword, TransportStrategy.SMTP_SSL);

				mailer.setDebug(true);
				mailer.validate(email);
				mailer.sendMail(email);
			}
			status = "SENT";
		} catch (Exception ex) {
			log.error("StackTrace", ex);
		}
		return status;
	}
	
	public static String format(String sourceStr, Object ... args) {
		String formattedStr = sourceStr;
		
		try{
			formattedStr = String.format(sourceStr, args);
		}
		catch(Exception e){
			log.error("StackTrace", e);
		}
		return formattedStr;
	}
	
	public static String getTimeStamp(){
		long unixTime = System.currentTimeMillis() / 1000L;
		String ut = String.valueOf(unixTime);
		return ut;
	}
}
