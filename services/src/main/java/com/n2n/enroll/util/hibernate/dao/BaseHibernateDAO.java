package com.n2n.enroll.util.hibernate.dao;

import org.hibernate.Session;

import com.n2n.enroll.util.hibernate.HibernateSessionFactory;

/**
 * Data access object (DAO) for domain model
 * 
 * @author MyEclipse Persistence Tools
 */
public class BaseHibernateDAO implements IBaseHibernateDAO {

	public Session getSession() {
		return HibernateSessionFactory.getSession();
	}

}