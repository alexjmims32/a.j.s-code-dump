package com.n2n.enroll.util.hibernate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.mapping.PersistentClass;
import org.hibernate.mapping.Table;

/**
 * This class allows the user to override the schema of hibernate domain objects dynamically
 * at run-time.  To use this class you must set this class as the configuration class 
 * for the session factory by setting the "configurationClass" property of the AnnotationSessionFactoryBean.
 *   Example: <property name="configurationClass" value="my.package.util.DynamicSchemaHibernateConfiguration"/>
 * Users can specify a schema override for one or more domain packages by
 * setting the Hibernate configuration properties hibernate.schema_override and
 * hibernate.schema_override_classes.  If only hibernate.schema_override is specified, then
 * all hibernate domain objects will be given this schema.  The
 * hibernate.schema_override_classes is a semi-colon delimited list of 
 * classes (regex wild-cards allowed) which, if matched, will be given the override
 * schema.
 * 
 * Examples:
 *   hibernate.schema_override=JBPM_SCHEMA
 *   hibernate.schema_override_classes=org.jbpm.*
 *   
 *   hibernate.schema_override=JBPM_SCHEMA;SALESFORCE_SCHEMA
 *   hibernate.schema_override_classes=org.jbpm.*;com.salesforce.*
 * 
 */
@SuppressWarnings({ "rawtypes", "deprecation" })
public class DynamicSchemaHibernateConfiguration extends AnnotationConfiguration
{
	private static final String HIBERNATE_SCHEMA_OVERRIDE_CLASSES = "hibernate.schema_override_classes";
	private static final String HIBERNATE_SCHEMA_OVERRIDE = "hibernate.schema_override";
	private static final String OVERRIDE_LIST_SEPARATOR = ";";

	private static final long serialVersionUID = 7870319066075076884L;
	private Logger log = Logger.getLogger(DynamicSchemaHibernateConfiguration.class);
	
	/**
	 * @see org.hibernate.cfg.Configuration#buildSessionFactory()
	 */
	
	@Override
	public SessionFactory buildSessionFactory() throws HibernateException
	{
		/*
		 * First get the schema overrides from the hibernate configuration properties
		 * hibernate.schema_override and hibernate.schema_override_packages
		 */
		Map<String,String> overrides = getSchemaOverrides();
		
		/*
		 * The AnnotationConfiguration's buildMappings()
		 * must be called first to build up the class mappings.  This allows us to iterate
		 * over all class mappings to reset the schemas.  Finally we must call
		 * buildSessionFactory().
		 * 
		 */

		/*
		 * First generate class mappings so we can iterate over them
		 */
		super.buildMappings(); 

		/*
		 * Now iterate over all hibernate domain classes and change the schema
		 * for any that match our packages.
		 */
		
		Iterator i = getClassMappings();
		while (i.hasNext())
		{
			PersistentClass pc = (PersistentClass) i.next();
			String className = pc.getClassName();
			Table table = pc.getTable();
			String schema = table.getSchema();			
			String schemaOverride = findSchemaOverride(className,overrides);
			if (schemaOverride != null)
			{
				table.setSchema(schemaOverride);
				this.log.info("Overriding schema from " + schema + " to " + schemaOverride + " on hibernate class " + className);
			}
			else
			{
				this.log.debug("Leaving schema as " + schema + " on hibernate class " + className);
			}
		}

		/*
		 * Now rebuild the session factory and return it.
		 */
		return super.buildSessionFactory();
	}

	/**
	 * Finds the schema override value for the supplied class name.
	 * 
	 * @param className the class name to match
	 * @param overrides a map of class name regular expression and schema override
	 * @return schema override name if found, null otherwise
	 */
	private String findSchemaOverride(String className, Map<String, String> overrides)
	{
		for (Entry<String, String> override : overrides.entrySet())
		{
			if (className.matches(override.getKey()))
				return override.getValue();
		}
		
		return null;
	}

	/**
	 * @return a hash map of override package regexp's and their schema overrides.
	 */
	private Map<String, String> getSchemaOverrides()
	{
		Map<String,String> overrides = new HashMap<String, String>();
		
		String overrideProperty = this.getProperty(HIBERNATE_SCHEMA_OVERRIDE);
		String packagesProperty = this.getProperty(HIBERNATE_SCHEMA_OVERRIDE_CLASSES);

		if (overrideProperty != null) 
		{
			// a schema override was supplied.  now determine which packages should be overridden.
			
			if (packagesProperty == null)
				packagesProperty = "*"; // default to all packages if none are supplied
			
			String[] schemaList = overrideProperty.split(OVERRIDE_LIST_SEPARATOR);
			String[] packageList = packagesProperty.split(OVERRIDE_LIST_SEPARATOR);

			if ((schemaList.length > 1) && (packageList.length != schemaList.length))
				throw new IllegalArgumentException("When multiple schema overrides are supplied, the same number of packages must be supplied");
			
			// for each package supplied, find the schema override
			for (int i=0; i<packageList.length; i++)
			{
				String packageStr = packageList[i];
				String schemaStr = schemaList[i % schemaList.length];
				overrides.put(packageStr, schemaStr);
			}
		}
				
		return overrides;
	}
}
