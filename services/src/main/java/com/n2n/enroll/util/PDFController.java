package com.n2n.enroll.util;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PDFController {
	protected final Log log = LogFactory.getLog(getClass());

	@RequestMapping(value = "/pdf")
	public @ResponseBody
	void convert(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		int i = 0;
		int k = 0;
		int maxLength = request.getContentLength();
		byte[] bytes = new byte[maxLength];
		String method = request.getParameter("method");
		String name = request.getParameter("name");

		log.info("Method : " + method + "\nName : " + name);

		ServletInputStream si = request.getInputStream();
		while (true) {
			k = si.read(bytes, i, maxLength);
			i += k;
			if (k <= 0)
				break;
		}
		if (bytes != null) {
			ServletOutputStream stream = response.getOutputStream();
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);
			response.setHeader("Content-Disposition", method + ";filename="
					+ name);
			stream.write(bytes);
			stream.flush();
			stream.close();
		}

	}

}