package com.n2n.enroll.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.n2n.enroll.registration.bo.CourseList;
import com.n2n.enroll.registration.bo.JCourse;

@SuppressWarnings({ "rawtypes", "resource" })
public class CourseCache {
	public static Connection conn;
	public static Statement stmt;
	public static String cacheStatus;
	protected final static Log log = LogFactory.getLog(CourseCache.class);

	CourseCache() {
		log.info("Started Course Cache "+ new Date().toString());
		try {
			cacheStatus="Creating";
			getConnection();
			// Creating courseList table
			String createSql = AppUtil.getInstSearchProperty("createCourse");
			stmt.executeUpdate(createSql);
			log.info("Created courseList table in memory H2DB");
			// Creating Index for courseList table
			String createIndex = AppUtil.getInstSearchProperty("createIndex");
			stmt.executeUpdate(createIndex);
			log.info("Created Index for courseList table in memory H2DB");
			insertCourses();
		} catch (Exception e) {
			log.error("StackTrace", e);
			cacheStatus=null;
			log.info("Completed Course Cache "+ new Date().toString());
		}
		cacheStatus=null;
		log.info("Completed Course Cache "+ new Date().toString());

	}
	
	public static void getConnection() throws Exception{
		// Retrieving H2DB Connection Driver Class
		Class.forName(AppUtil
				.getInstSearchProperty("h2db.connection.driver_class"));
		log.info("Retrieved H2DB Connection Driver Class");
		// Retrieving Connection from H2DB
		conn = DriverManager.getConnection(
				AppUtil.getInstSearchProperty("connection.url"),
				AppUtil.getInstSearchProperty("connection.username"),
				AppUtil.getInstSearchProperty("connection.password"));
		log.info("Retrieved H2DB connection");
		stmt = conn.createStatement();
	}
	
	public static void insertCourses() throws Exception{
		// Retrieving all the courses info from database
		String sql = AppUtil.getInstSearchProperty("getCourses");
		List l = AppUtil.runAOpenSessionQuery(sql);
		// Inserting all the courses into H2DB courseList table
		for (Object obj : l) {
			Object[] course = (Object[]) obj;

			String insertQuery = AppUtil
					.getInstSearchProperty("insertCourse");
			
			String searchText = AppUtil.getSqlFmtStringValue(course[0]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[1]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[2]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[3]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[4]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[5]) + ' ' + 
					AppUtil.getFloatValue(course[6]) + ' ' + 
					AppUtil.getFloatValue(course[7]) + ' ' + 
					AppUtil.getFloatValue(course[8]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[9]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[10]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[11]) + ' ' + 
					AppUtil.getFloatValue(course[12]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[13]) + ' ' + 
					AppUtil.getIntValue(course[14]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[15]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[16]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[17]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[18]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[19]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[20]) + ' ' + 
					AppUtil.getSqlFmtStringValue(course[21]);
			insertQuery = (AppUtil.format(insertQuery,
					AppUtil.getSqlFmtStringValue(course[0]),
					AppUtil.getSqlFmtStringValue(course[1]),
					AppUtil.getSqlFmtStringValue(course[2]),
					AppUtil.getSqlFmtStringValue(course[3]),
					AppUtil.getSqlFmtStringValue(course[4]),
					AppUtil.getSqlFmtStringValue(course[5]),
					AppUtil.getFloatValue(course[6]),
					AppUtil.getFloatValue(course[7]),
					AppUtil.getFloatValue(course[8]),
					AppUtil.getSqlFmtStringValue(course[9]),
					AppUtil.getSqlFmtStringValue(course[10]),
					AppUtil.getSqlFmtStringValue(course[11]),
					AppUtil.getFloatValue(course[12]),
					AppUtil.getSqlFmtStringValue(course[13]),
					AppUtil.getIntValue(course[14]),
					AppUtil.getSqlFmtStringValue(course[15]),
					AppUtil.getSqlFmtStringValue(course[16]),
					AppUtil.getSqlFmtStringValue(course[17]),
					AppUtil.getSqlFmtStringValue(course[18]),
					AppUtil.getSqlFmtStringValue(course[19]),
					AppUtil.getSqlFmtStringValue(course[20]),
					AppUtil.getSqlFmtStringValue(course[21]),searchText));
			stmt.execute(insertQuery);
		}
		log.info("Inserted courses into H2DB courseList table");
	}
	
	public static void updatingCourses(){
		log.info("Started updating Course Cache "+ new Date().toString());
		try {
			cacheStatus="Updating"; 
			String deleteQuery = AppUtil
					.getInstSearchProperty("deleteCourses");
			stmt.executeUpdate(deleteQuery);
			insertCourses();
		} catch (Exception e) {
			log.error("StackTrace", e);
			cacheStatus=null;
			log.info("Completed updating Course Cache "+ new Date().toString());
		}
		cacheStatus=null;
		log.info("Completed updating Course Cache "+ new Date().toString());

	}

	public static CourseList search(String termCode,String searchString,int start,int limit,int page) throws Exception {
		CourseList xCourseList = new CourseList();
		if(cacheStatus==null){
			xCourseList.setStatus("SUCCESS");
			if (!(searchString.equals(""))) {
				String[] values;
				String condition="";
			    /* delimiter */
			    String delimiter = " ";
			    /* given string will be split by the argument delimiter provided. */
			    values = searchString.split(delimiter);
			    for(String value:values){
			    	condition = condition + " AND LOWER(SEARCH_TEXT) LIKE LOWER('%" + value + "%')";
			    }
				String query = AppUtil
						.getInstSearchProperty("searchCourses");
				query = (AppUtil.format(query,termCode,condition));
				// Searching the courses in H2DB
				ResultSet r = stmt.executeQuery(query + " Limit " + (page-1) * limit + ", " + limit);
				// Converting the resultset to course object
				while (r.next()) {
					JCourse xCourse = new JCourse();
					xCourse.setTermCode(AppUtil.getStringValue(r.getObject(1)));
					xCourse.setCourseNumber(AppUtil.getStringValue(r.getObject(2)));
					xCourse.setCrn(AppUtil.getStringValue(r.getObject(3)));
					xCourse.setSubject(AppUtil.getStringValue(r.getObject(4)));
					xCourse.setCourseTitle(AppUtil.getStringValue(r.getObject(6)));
					xCourse.setCredits(AppUtil.getStringValue(r.getObject(7)));
					xCourse.setCampus(AppUtil.getStringValue(r.getObject(10)));
					xCourse.setLevel(AppUtil.getStringValue(r.getObject(12)));
					xCourse.setLectureHours(AppUtil.getStringValue(r.getObject(13)));
					xCourse.setSeatsAvailable(AppUtil.getStringValue(r
							.getObject(14)));
					xCourse.setWaitedList(AppUtil.getStringValue(r.getObject(15)));
					xCourse.setDepartment(AppUtil.getStringValue(r.getObject(18)));
					xCourse.setFaculty(AppUtil.getStringValue(r.getObject(19)));
					xCourse.setCollege(AppUtil.getStringValue(r.getObject(21)));
					xCourse.setDescription(AppUtil.getStringValue(r.getObject(5)));
					xCourse.setTermDescription(AppUtil.getStringValue(r
							.getObject(22)));
					xCourseList.getCourse().add(xCourse);
				}
			}
		}else{
			xCourseList.setStatus("Updating course cache, Please try again in few seconds");
		}
		return xCourseList;
	}
}
