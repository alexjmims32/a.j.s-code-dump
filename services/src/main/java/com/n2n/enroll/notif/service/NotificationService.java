package com.n2n.enroll.notif.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javapns.Push;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.h2.util.IOUtils;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.n2n.enroll.notif.bo.JNotificationList;
import com.n2n.enroll.notif.bo.JNotificationLog;
import com.n2n.enroll.notif.bo.JNotificationSearch;
import com.n2n.enroll.notif.bo.JNotificationSearchList;
import com.n2n.enroll.notif.bo.JSearchByName;
import com.n2n.enroll.notif.bo.JSearchByNameList;
import com.n2n.enroll.notif.bo.NotificationBo;
import com.n2n.enroll.notif.bo.NotificationLog;
import com.n2n.enroll.notif.bo.SearchByNameBo;
import com.n2n.enroll.notif.bo.StudentInfoBo;
import com.n2n.enroll.notif.bo.XNotificationList;
import com.n2n.enroll.util.AppUtil;

@SuppressWarnings("unchecked")
@Service
public class NotificationService {

	public static final String NO_USERNAME = "NO_USERNAME";
	private static Sender sender;
	private static int MULTICAST_SIZE = 0;
	private static final Executor threadPool = Executors.newFixedThreadPool(5);
	private static byte[] keyStoreContents = null;

	@Resource
	SessionFactory sessionFactory;

	protected final Log log = LogFactory.getLog(getClass());

	public XNotificationList list(String userId, String maxId) {
		String status = "";
		XNotificationList response = new XNotificationList();
		JNotificationList jNotificationList = null;
		List<NotificationLog> notifications = null;
		String sql = "";
		try {

			sql = AppUtil.getProperty("Q_GET_NOTIFICATIONS");

			sql = AppUtil.format(sql, userId);
			if (maxId != null && !maxId.equals("")) {
				sql = sql + " AND N.id >" + maxId + " order by N.id";
			}
			notifications = (List<NotificationLog>) AppUtil
					.runAQueryCurrentSession(sql, NotificationLog.class);

			jNotificationList = new JNotificationList();
			for (NotificationLog notification : notifications) {
				JNotificationLog jNotification = new JNotificationLog();
				jNotification
						.setId(AppUtil.getStringValue(notification.getId()));
				jNotification.setNoticeId(AppUtil.getStringValue(notification
						.getNoticeid()));
				jNotification.setUserName(AppUtil.getStringValue(notification
						.getUsername()));
				jNotification.setMessage(AppUtil.getStringValue(notification
						.getMessage()));
				jNotification.setExpiryDate(AppUtil.getStringValue(notification
						.getExpirydate()));
				jNotification.setDeliveryMethod(AppUtil
						.getStringValue(notification.getDeliverymethod()));
				jNotification.setDelivered(AppUtil.getStringValue(notification
						.getDelivered()));
				jNotification.setReadFlag(AppUtil.getStringValue(notification
						.getReadflag()));
				jNotification.setLastModifiedBy(AppUtil
						.getStringValue(notification.getLastmodifiedby()));
				jNotification.setLastModifiedOn(AppUtil
						.getStringValue(notification.getLastmodifiedon()));
				jNotification.setDeleted(AppUtil.getStringValue(notification
						.getDeleted()));
				jNotification.setType(AppUtil.getStringValue(notification
						.getType()));
				jNotification.setTitle(AppUtil.getStringValue(notification
						.getTitle()));

				jNotification.setDueDate(AppUtil.getStringValue(notification
						.getDueDate()));

				jNotificationList.getNotification().add(jNotification);
			}

			response.setNotifications(jNotificationList);
			status = "SUCCESS";
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return response;
	}

	public XNotificationList adminNotificationList(String userId, String maxId) {
		String status = "";
		XNotificationList response = new XNotificationList();
		JNotificationList jNotificationList = null;
		List<NotificationLog> notifications = null;
		String sql = "";
		try {

			sql = AppUtil.getProperty("Q_GET_ADMIN_NOTIFICATIONS");

			sql = AppUtil.format(sql);
			if (maxId != null && !maxId.equals("")) {
				sql = sql + " AND N.id >" + maxId + " order by N.id";
			}

			notifications = (List<NotificationLog>) AppUtil
					.runAQueryCurrentSession(sql, NotificationLog.class);

			jNotificationList = new JNotificationList();
			for (NotificationLog notification : notifications) {
				JNotificationLog jNotification = new JNotificationLog();
				jNotification
						.setId(AppUtil.getStringValue(notification.getId()));
				jNotification.setNoticeId(AppUtil.getStringValue(notification
						.getNoticeid()));
				jNotification.setUserName(AppUtil.getStringValue(notification
						.getUsername()));
				jNotification.setMessage(AppUtil.getStringValue(notification
						.getMessage()));
				jNotification.setExpiryDate(AppUtil.getStringValue(notification
						.getExpirydate()));
				jNotification.setDeliveryMethod(AppUtil
						.getStringValue(notification.getDeliverymethod()));
				jNotification.setDelivered(AppUtil.getStringValue(notification
						.getDelivered()));
				jNotification.setReadFlag(AppUtil.getStringValue(notification
						.getReadflag()));
				jNotification.setLastModifiedBy(AppUtil
						.getStringValue(notification.getLastmodifiedby()));
				jNotification.setLastModifiedOn(AppUtil
						.getStringValue(notification.getLastmodifiedon()));
				jNotification.setDeleted(AppUtil.getStringValue(notification
						.getDeleted()));
				jNotification.setType(AppUtil.getStringValue(notification
						.getType()));
				jNotification.setTitle(AppUtil.getStringValue(notification
						.getTitle()));
				// if (notification.getDueDate() != null) {
				// dueDate = notification.getDueDate().toString();
				// }
				// if (dueDate == null || dueDate.equals("")
				// || dueDate.equals("NA")) {
				// dueDate = "";
				// } else {
				// dueDate = AppUtil.makeAdminDate(notification.getDueDate()
				// .toString(), "yyyy-MM-dd HH:mm:ss.s");
				// }

				jNotification.setDueDate(AppUtil.getStringValue(notification
						.getDueDate()));

				jNotificationList.getNotification().add(jNotification);
			}

			response.setNotifications(jNotificationList);
			status = "SUCCESS";
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return response;
	}

	public String updateReadFlag(String id, int readUnreadStatus) {

		String status = "";

		if (id == null || id.equals("")) {
			status = "Invalid Notice";
		} else {
			try {

				String query = AppUtil.getProperty("Q_UPDATE_READ_FLAG");
				query = (AppUtil.format(query, readUnreadStatus, id));

				AppUtil.runADeleteQuery(query);
				status = "SUCCESS";

			} catch (Exception e) {
				log.error("Error updating read flag on message "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return status;
	}

	public String updeDeletedFlag(String id, String deleted) {

		String status = "";

		if (id == null || id.equals("")) {
			status = "Invalid Notice Id";
		} else {
			try {

				String query = AppUtil.getProperty("Q_UPDATE_DELETED_FLAG");
				query = (AppUtil.format(query, deleted, id));

				AppUtil.runADeleteQuery(query);
				status = "SUCCESS";

			} catch (Exception e) {
				log.error("Error updating deleted flag on message "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return status;
	}

	public String addNotification(JNotificationLog notifLog) {

		String status = "";

		if (notifLog == null || notifLog.equals("")) {
			status = "Invalid Notification Details";
		} else {
			try {
				log.debug("add notification object validated successfully");
				BigDecimal maxNoticeId;
				long noticeIdVal = 0;
				try {
					String qry = "select max(noticeId) from noticelog";
					maxNoticeId = AppUtil.runACountQuery(qry);
					if (maxNoticeId == null) {
						noticeIdVal = 0;
					}
					noticeIdVal = maxNoticeId.longValue();
					noticeIdVal = noticeIdVal + 1;
					log.debug("executed maxNoticeId");
				} catch (Exception e) {
					log.error("StackTrace", e);
				}
				String[] to = notifLog.getUserName().split(",");

				for (int i = 0; i < to.length; i++) {

					String query = AppUtil.getProperty("Q_ADD_NOTIFICATION");
					query = (AppUtil.format(query, noticeIdVal, to[i],
							notifLog.getMessage(), notifLog.getExpiryDate(),
							notifLog.getDeliveryMethod(),
							notifLog.getDelivered(), notifLog.getReadFlag(),
							notifLog.getLastModifiedBy(),
							notifLog.getLastModifiedOn(),
							notifLog.getDeleted(), notifLog.getType(),
							notifLog.getTitle(), notifLog.getDueDate()));

					noticeIdVal++;
					AppUtil.runADeleteQuery(query);
				}
				status = "SUCCESS";

			} catch (Exception e) {
				log.error("Error updating deleted flag on message "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return status;
	}

	public String addAdminNotification(JNotificationLog notifLog) {

		String status = "";

		if (notifLog == null || notifLog.equals("")) {
			status = "Invalid Notification Details";
		} else {
			try {
				log.debug("add notification object validated successfully");
				BigDecimal maxNoticeId;
				long noticeIdVal = 0;
				try {
					String qry = "select max(noticeId) from noticelog";
					maxNoticeId = (BigDecimal) AppUtil.runACountQuery(qry);
					noticeIdVal = maxNoticeId.longValue();
					noticeIdVal = noticeIdVal + 1;
					log.debug("executed maxNoticeId");
				} catch (Exception e) {
					log.error("StackTrace", e);
				}
				String[] to = notifLog.getUserName().split(",");

				for (int i = 0; i < to.length; i++) {

					String query = AppUtil
							.getProperty("Q_ADD_ADMIN_NOTIFICATION");
					query = (AppUtil.format(query, noticeIdVal, to[i], notifLog
							.getMessage().replaceAll("'", "''"), notifLog
							.getExpiryDate(), notifLog.getDeliveryMethod(),
							notifLog.getDelivered(), notifLog.getReadFlag(),
							notifLog.getLastModifiedBy(),
							notifLog.getDeleted(), notifLog.getType(), notifLog
									.getTitle(), notifLog.getDueDate()));

					noticeIdVal++;
					AppUtil.runADeleteQuery(query);
				}
				status = "SUCCESS";

			} catch (Exception e) {
				log.error("Error updating deleted flag on message "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return status;
	}

	public JNotificationSearchList searchBanner(String firstName,
			String lastName, String pageOffset, String pageSize, String category) {

		if ((firstName == null || "".equals(firstName))
				&& (lastName == null || "".equals(lastName))) {
			return new JNotificationSearchList();
		}

		// Paging Params
		if (pageOffset == null || "".equals(pageOffset)) {
			pageOffset = "1";
		}

		if (pageSize == null || "".equals(pageSize)) {
			pageSize = "40";
		}

		int pageOffsetInt = 1;
		try {
			pageOffsetInt = Integer.parseInt(pageOffset);
		} catch (Exception e) {
			// Do nothing. Default is set to 1
		}
		int pageSizeInt = 20;
		try {
			pageSizeInt = Integer.parseInt(pageSize);
		} catch (Exception e) {
			// Do nothing. Default is set to 20
		}

		JNotificationSearchList notifList = new JNotificationSearchList();
		try {
			String personSql = AppUtil
					.getProperty("Q_NOTIFICATION_SEARCH_BY_NAME");

			String condition = "";
			if (firstName == null || "".equals(firstName)) {
				condition = String.format("( lower(lst_name) LIKE '%%%s%%' )",
						lastName);
			} else if (lastName == null || "".equals(lastName)) {
				condition = String.format(
						"( lower(first_name) LIKE '%%%s%%' )", firstName);
			} else {
				condition = String
						.format("( Lower(first_name) LIKE lower('%%%s%%') or lower(lst_name) LIKE lower('%%%s%%') )",
								firstName, lastName);
			}

			personSql = (AppUtil.format(personSql, condition, category));

			List<NotificationBo> pel = (List<NotificationBo>) AppUtil
					.runAQueryOpenSession(personSql, pageOffsetInt,
							pageSizeInt, NotificationBo.class);
			for (NotificationBo notifBo : pel) {
				JNotificationSearch search = new JNotificationSearch();
				search.setId(AppUtil.getStringValue(notifBo.getId()));
				search.setLastName(notifBo.getLastName());
				search.setFirstName(notifBo.getFirstName());
				search.setMiddleName(notifBo.getMiddleName());
				if (search.getFirstName() == null
						|| search.getFirstName().equals("")) {
					search.setFirstName("");
				}
				if (search.getLastName() == null
						|| search.getLastName().equals("")) {
					search.setLastName("");
				}
				if (search.getMiddleName() == null
						|| search.getMiddleName().equals("")) {
					search.setMiddleName("");
				}
				notifList.getNotifications().add(search);
			}

		} catch (Exception e) {
			log.error("Error in Searching notification id's List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}

		return notifList;
	}

	public JNotificationSearchList searchByQuery(String query,
			String pageOffset, String pageSize) {

		// Paging Params
		if (pageOffset == null || "".equals(pageOffset)) {
			pageOffset = "1";
		}

		if (pageSize == null || "".equals(pageSize)) {
			pageSize = "20";
		}

		int pageOffsetInt = 1;
		try {
			pageOffsetInt = Integer.parseInt(pageOffset);
		} catch (Exception e) {
			// Do nothing. Default is set to 1
		}
		int pageSizeInt = 20;
		try {
			pageSizeInt = Integer.parseInt(pageSize);
		} catch (Exception e) {
			// Do nothing. Default is set to 20
		}

		JNotificationSearchList notifList = new JNotificationSearchList();
		try {
			if (query.contains("*")) {

			}
			List<StudentInfoBo> pel = (List<StudentInfoBo>) AppUtil
					.runAQueryOpenSession(query, pageOffsetInt, pageSizeInt,
							StudentInfoBo.class);
			for (StudentInfoBo serBo : pel) {
				JNotificationSearch search = new JNotificationSearch();
				search.setId(AppUtil.getStringValue(serBo.getId()));
				notifList.getNotifications().add(search);
			}

		} catch (Exception e) {
			log.error("Error in Searching notification id's List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}

		return notifList;
	}

	@SuppressWarnings("unused")
	public JSearchByNameList searchAll(String name, String pageOffset,
			String pageSize, String campusCode, String firstName,
			String lastName) {

		// Paging Params
		if (pageOffset == null || "".equals(pageOffset)) {
			pageOffset = "1";
		}

		if (pageSize == null || "".equals(pageSize)) {
			pageSize = "20";
		}

		int pageOffsetInt = 1;
		try {
			pageOffsetInt = Integer.parseInt(pageOffset);
		} catch (Exception e) {
			// Do nothing. Default is set to 1
		}
		int pageSizeInt = 20;
		try {
			pageSizeInt = Integer.parseInt(pageSize);
		} catch (Exception e) {
			// Do nothing. Default is set to 20
		}

		String status = "SUCCESS";
		JSearchByNameList notifList = new JSearchByNameList();
		try {
			String personSql = AppUtil
					.getProperty("Q_GET_NOTIFICATION_BY_CLIENT");

			String countSql = AppUtil
					.getProperty("Q_GET_COUNT_NOTIFICATION_BY_CLIENT");

			String condition = "";
			
			if (name.equalsIgnoreCase("FACULTY")) {
				condition = " Where CATEGORY like '%" + name + "%'";
				String searchCondition = getCondition(firstName, lastName);
				condition = condition + searchCondition;
			} else if (name.equalsIgnoreCase("EMPLOYEE")) {
				condition = " Where CATEGORY like '%" + name + "%'";
				String searchCondition = getCondition(firstName, lastName);
				condition = condition + searchCondition;
			} else if (name.equalsIgnoreCase("STUDENT")) {
				condition = " Where CATEGORY like '%" + name + "%'";
				String searchCondition = getCondition(firstName, lastName);
				condition = condition + searchCondition;
			} else if (name.equalsIgnoreCase("ALL")) {
				if ((firstName != null && !firstName.equals(""))
						&& (lastName != null) && !lastName.equals("")) {
					condition = " Where "
							+ String.format(
									"(UPPER(first_name) LIKE UPPER('%s%%') and UPPER(lst_name) LIKE UPPER('%s%%') )",
									firstName, lastName);

				} else if ((lastName != null && !lastName.equals(""))
						&& (firstName.equals(""))) {
					condition = " Where "
							+ String.format(
									"(UPPER(first_name) LIKE UPPER('%s%%') or UPPER(lst_name) LIKE UPPER('%s%%') )",
									lastName, lastName);
				} else if ((firstName != null && !firstName.equals(""))
						&& (lastName.equals(""))) {
					condition = " Where "
							+ String.format(
									"(UPPER(first_name) LIKE UPPER('%s%%') or UPPER(lst_name) LIKE UPPER('%s%%') )",
									firstName, firstName);
				}

			}

			personSql = (AppUtil.format(personSql, condition));
			log.debug(personSql);

			countSql = (AppUtil.format(countSql, condition));

			BigDecimal count = AppUtil.runACountQuery(countSql);

			List<SearchByNameBo> pel = (List<SearchByNameBo>) AppUtil
					.runAQueryOpenSession(personSql, pageOffsetInt,
							pageSizeInt, SearchByNameBo.class);
			 
			for (SearchByNameBo notifBo : pel) {
				JSearchByName search = new JSearchByName();
				search.setId(AppUtil.getStringValue(notifBo.getId()));
				search.setLastName(notifBo.getLastName());
				search.setFirstName(notifBo.getFirstName());
				search.setMiddleName(notifBo.getMiddleName());
				if (search.getFirstName() == null
						|| search.getFirstName().equals("")) {
					search.setFirstName("");
				}
				if (search.getLastName() == null
						|| search.getLastName().equals("")) {
					search.setLastName("");
				}
				if (search.getMiddleName() == null
						|| search.getMiddleName().equals("")) {
					search.setMiddleName("");
				}
				notifList.getNotifications().add(search);
				notifList.setTotalRecords(count);
			}

		} catch (Exception e) {
			log.error("Error in Searching notification id's List : "
					+ e.getMessage());
			log.error("StackTrace", e);
			status = e.getMessage();
		}

		return notifList;
	}

	public String getCondition(String firstName, String lastName) {

		String condition = "";

		if ((firstName != null && !firstName.equals(""))
				&& (lastName.equals(""))) {
			condition = condition
					+ String.format(
							" and (UPPER(first_name) LIKE UPPER('%s%%') or UPPER(lst_name) LIKE UPPER('%s%%') )",
							firstName, firstName);
		} else if ((lastName != null && !lastName.equals(""))
				&& (firstName.equals(""))) {
			condition = condition
					+ String.format(
							" and (UPPER(first_name) LIKE UPPER('%s%%') or UPPER(lst_name) LIKE UPPER('%s%%') )",
							lastName, lastName);
		} else if ((firstName != null && !firstName.equals(""))
				&& !(lastName.equals("")) && !lastName.equals("")) {
			condition = condition
					+ String.format(
							" and (UPPER(first_name) LIKE UPPER('%s%%') and UPPER(lst_name) LIKE UPPER('%s%%') )",
							firstName, lastName);
		}
		return condition;

	}

	public boolean registerDevice(String username, String deviceId,
			String platform) {
		boolean result = true;

		if (username == null || username.trim().isEmpty()) {
			username = NO_USERNAME;
		}

		try {
			// Check if we already have this device in our list
			String sql1 = AppUtil.getProperty("Q_CHECK_DEVICES");
			sql1 = AppUtil.format(sql1, deviceId);
			BigDecimal count = AppUtil.runACountQuery(sql1);
			int countInt = count.intValue();

			String sql2 = "";
			if (countInt == 1) {
				// Device already registered, update username
				sql2 = AppUtil.getProperty("Q_UPDATE_USERNAME");
				sql2 = AppUtil.format(sql2, username, NO_USERNAME, deviceId);
			} else {
				// Device not registered yet, add it to our list
				sql2 = AppUtil.getProperty("Q_REG_DEVICE");
				sql2 = AppUtil.format(sql2, username, deviceId, platform);
			}
			AppUtil.runADeleteQuery(sql2);
		} catch (Exception e) {
			log.error("Error in registering device : " + e.getMessage());
			log.error("StackTrace", e);
			result = false;
		}

		return result;
	}

	public void push(String id, String title) {
		pushToAndroid(id, title);
		pushToIos(id, title);
		markAsTransmitted(id);
	}

	private void pushToIos(String noticeId, String title) {
		final String platform = "Ios";

		List<String> devices = null;
		try {
			// Check if we already have this device in our list
			String sql1 = AppUtil.getProperty("Q_GET_USER_DEVICES");
			sql1 = AppUtil.format(sql1, noticeId, platform.toUpperCase());
			devices = AppUtil.runAOpenSessionQuery(sql1);
		} catch (Exception e) {
			log.error("Error in retreiving push devices list: "
					+ e.getMessage());
			log.error("StackTrace", e);
		}

		if (devices == null || devices.size() == 0) {
			log.info("No devices registered");
			return;
		}

		try {
			String passPhrase = AppUtil.getProperty("IOS_KEYSTORE_PASSPHRASE");
			Push.alert(title, getKeyStoreStream(), passPhrase, false, devices);
		} catch (Exception e) {
			log.error("Unable to push ios notifications: " + e.getMessage());
			log.error("StackTrace", e);
		}
	}

	private byte[] getKeyStoreStream() throws URISyntaxException, IOException {
		if (keyStoreContents == null) {
			InputStream input = NotificationService.class
					.getResourceAsStream("/eNtouragePush.p12");
			keyStoreContents = IOUtils.readBytesAndClose(input, 0);
		}
		return keyStoreContents;
	}

	private boolean pushToAndroid(String noticeId, String title) {
		final String platform = "Android";

		if (sender == null) {
			String key = null;
			try {
				key = AppUtil.getProperty("GCM_API_KEY");
			} catch (Exception e) {
				log.error("Unable to retrieve api key: " + e.getMessage());
				log.error("StackTrace", e);
				return false;
			}
			sender = new Sender(key);
		}

		List<String> devices = null;
		try {
			// Check if we already have this device in our list
			String sql1 = AppUtil.getProperty("Q_GET_USER_DEVICES");
			sql1 = AppUtil.format(sql1, noticeId, platform.toUpperCase());
			devices = AppUtil.runAOpenSessionQuery(sql1);
		} catch (Exception e) {
			log.error("Error in retreiving push devices list: "
					+ e.getMessage());
			log.error("StackTrace", e);
		}

		if (devices == null || devices.size() == 0) {
			log.info("No devices registered");
			return true;
		}

		if (devices.size() == 1) {
			// send a single message using plain post
			String registrationId = devices.get(0);
			Message message = new Message.Builder().addData("Subject", title)
					.build();
			Result result = null;
			try {
				result = sender.send(message, registrationId, 5);
			} catch (IOException e) {
				log.error("Error pushing to device:" + registrationId);
				log.error("StackTrace", e);
				return false;
			}
			log.debug("Sent message to one device: " + result);
		} else {
			// send a multicast message using JSON
			setMulticastSize();
			int total = devices.size();
			List<String> partialDevices = new ArrayList<String>(total);
			int counter = 0;
			for (String device : devices) {
				counter++;
				partialDevices.add(device);
				int partialSize = partialDevices.size();
				if (partialSize == MULTICAST_SIZE || counter == total) {
					asyncSend(partialDevices);
					partialDevices.clear();
				}
			}
		}
		return true;
	}

	private void asyncSend(List<String> partialDevices) {
		// make a copy
		final List<String> devices = new ArrayList<String>(partialDevices);
		threadPool.execute(new Runnable() {

			public void run() {
				Message message = new Message.Builder().build();
				MulticastResult multicastResult;
				try {
					multicastResult = sender.send(message, devices, 5);
				} catch (IOException e) {
					log.debug("Error posting messages: " + e.getMessage());
					return;
				}
				List<Result> results = multicastResult.getResults();
				// analyze the results
				for (int i = 0; i < devices.size(); i++) {
					String regId = devices.get(i);
					Result result = results.get(i);
					String messageId = result.getMessageId();
					if (messageId != null) {
						log.debug("Succesfully sent message to device: "
								+ regId + "; messageId = " + messageId);
						String canonicalRegId = result
								.getCanonicalRegistrationId();
						if (canonicalRegId != null) {
							// same device has more than on registration id:
							// update it
							log.info("canonicalRegId " + canonicalRegId);
							// Datastore.updateRegistration(regId,
							// canonicalRegId);
						}
					} else {
						String error = result.getErrorCodeName();
						if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
							// application has been removed from device -
							// unregister it
							log.info("Unregistered device: " + regId);
							// Unregister device
						} else {
							log.error("Error sending message to " + regId
									+ ": " + error);
						}
					}
				}
			}
		});
	}

	private void setMulticastSize() {
		if (MULTICAST_SIZE == 0) {
			try {
				MULTICAST_SIZE = AppUtil.getIntValue(AppUtil
						.getProperty("MULTICAST_SIZE"));
			} catch (Exception e) {
				MULTICAST_SIZE = 1000;
			}
		}
	}

	private void markAsTransmitted(String noticeId) {
		log.debug("Marking this notice as sent out. Notice ID: " + noticeId);
		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_MARK_AS_SENT");
			sql = AppUtil.format(sql, noticeId);
			AppUtil.runADeleteQueryNewSession(sql);
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("");
		}
	}
}