package com.n2n.enroll.notif.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class JSearchByNameList {
	private BigDecimal totalRecords;

	public BigDecimal getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(BigDecimal totalRecords) {
		this.totalRecords = totalRecords;
	}

	protected List<JSearchByName> notifications = new ArrayList<JSearchByName>(
			0);

	public List<JSearchByName> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<JSearchByName> notifications) {
		this.notifications = notifications;
	}
}
