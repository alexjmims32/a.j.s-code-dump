package com.n2n.enroll.notif.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
public class StudentInfoBo implements java.io.Serializable {
	@Id
	@Column(name = "STUDENTID")
	private String id;
	@Column(name = "PIDM")
	private String pidm;

	public String getPidm() {
		return pidm;
	}

	public void setPidm(String pidm) {
		this.pidm = pidm;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
