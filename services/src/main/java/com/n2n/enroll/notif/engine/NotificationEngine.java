package com.n2n.enroll.notif.engine;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.n2n.enroll.notif.bo.NotificationLog;
import com.n2n.enroll.notif.service.NotificationService;
import com.n2n.enroll.util.AppUtil;

@Service
public class NotificationEngine {
	
	@Resource
	SessionFactory sessionFactory;
	
	@Resource
	NotificationService notificationService;
	
	private static final Logger log = LoggerFactory
			.getLogger(NotificationEngine.class);

	
	public List<NotificationLog> getNewNotifications() throws Exception {
		
		String sql = AppUtil.getProperty("Q_GET_NEW_NOTICES");
		
		@SuppressWarnings("rawtypes")
		List dbList = AppUtil.runAOpenSessionQuery(sql);
		List<NotificationLog> notices = new ArrayList<NotificationLog>();
		for(Object o: dbList){
			Object[] details = (Object[])o;
			
			NotificationLog notice = new NotificationLog();
			notice.setNoticeid(Long.valueOf(details[0].toString()));
			notice.setTitle(details[1].toString());
			
			notices.add(notice);
		}
		return notices;
	}
	
	public void transmitNotifications() {
		
		List <NotificationLog> notices = null;
		try {
			log.debug("Retreiving new notifications");
			notices = getNewNotifications();
		} catch (Exception e) {
			log.error("Error retreiving new notifications: " + e.getMessage());
			log.error("StackTrace", e);
		}
		
		for(NotificationLog n: notices){
			log.debug("Trying to push notification: " + n.getNoticeid());
			notificationService.push(String.valueOf(n.getNoticeid()), n.getTitle());
		}
	}
}
