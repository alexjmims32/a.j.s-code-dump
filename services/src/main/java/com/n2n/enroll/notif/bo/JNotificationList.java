package com.n2n.enroll.notif.bo;

import java.util.ArrayList;
import java.util.List;

public class JNotificationList {
	
	protected List<JNotificationLog> notification = new ArrayList<JNotificationLog>(0);

	public List<JNotificationLog> getNotification() {
		return notification;
	}

	public void setNotification(List<JNotificationLog> notification) {
		this.notification = notification;
	}

}
