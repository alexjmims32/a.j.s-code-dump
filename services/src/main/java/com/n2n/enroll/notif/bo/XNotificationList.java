package com.n2n.enroll.notif.bo;

public class XNotificationList {
private String status;
private JNotificationList notificationList;
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public JNotificationList getNotifications() {
	return notificationList;
}
public void setNotifications(JNotificationList notificationList) {
	this.notificationList = notificationList;
}
}
