package com.n2n.enroll.notif.engine;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class NotificationEngineScheduler {
	protected final Log log = LogFactory.getLog(getClass());
	
	private static final int sleepIntervalInSeconds = 6000;

	@Resource
	NotificationEngine notificationEngine;
	
	@Scheduled(fixedDelay = sleepIntervalInSeconds)
	public void runService() {
		try {
			if(notificationEngine == null){
				log.error("Notification Engine not initialized");
			}
			else{
				notificationEngine.transmitNotifications();
			}
		} catch (Exception ex) {
			log.info("Unable to invoke the engine");
			log.error("StackTrace", ex);
		}
	}
}
