package com.n2n.enroll.notif.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class JNotificationSearchList {
	private BigDecimal totalRecords;

	public BigDecimal getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(BigDecimal totalRecords) {
		this.totalRecords = totalRecords;
	}

	protected List<JNotificationSearch> notifications = new ArrayList<JNotificationSearch>(
			0);

	public List<JNotificationSearch> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<JNotificationSearch> notifications) {
		this.notifications = notifications;
	}
}
