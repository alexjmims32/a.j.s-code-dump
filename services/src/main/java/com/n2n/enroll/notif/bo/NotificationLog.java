package com.n2n.enroll.notif.bo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="NOTICELOG")
public class NotificationLog implements Serializable {
	
	@Id
	@Column(name="ID")
	private long id;
	@Column(name="NOTICEID")
	private long noticeid;
	@Column(name="USERNAME")
	private String username;
	@Column(name="MESSAGE")
	private String	message;
	@Column(name="EXPIRYDATE")
	private Date expirydate;
	@Column(name="DELIVERYMETHOD")
	private String deliverymethod;
	@Column(name="DELIVERED")
	private long delivered;
	@Column(name="READFLAG")
	private long readflag;
	@Column(name="LASTMODIFIEDBY")
	private String lastmodifiedby;
	@Column(name="LASTMODIFIEDON")
	private Date lastmodifiedon;
	@Column(name="DELETED")
	private long deleted;
	@Column(name="TYPE")
	private String type;
	@Column(name="TITLE")
	private String title;
	@Column(name="DUEDATE")
	private Date dueDate;
	
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getNoticeid() {
		return noticeid;
	}
	public void setNoticeid(long noticeid) {
		this.noticeid = noticeid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getExpirydate() {
		return expirydate;
	}
	public void setExpirydate(Date expirydate) {
		this.expirydate = expirydate;
	}
	public String getDeliverymethod() {
		return deliverymethod;
	}
	public void setDeliverymethod(String deliverymethod) {
		this.deliverymethod = deliverymethod;
	}
	public long getDelivered() {
		return delivered;
	}
	public void setDelivered(long delivered) {
		this.delivered = delivered;
	}
	public long getReadflag() {
		return readflag;
	}
	public void setReadflag(long readflag) {
		this.readflag = readflag;
	}
	public String getLastmodifiedby() {
		return lastmodifiedby;
	}
	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}
	public Date getLastmodifiedon() {
		return lastmodifiedon;
	}
	public void setLastmodifiedon(Date lastmodifiedon) {
		this.lastmodifiedon = lastmodifiedon;
	}
	public long getDeleted() {
		return deleted;
	}
	public void setDeleted(long deleted) {
		this.deleted = deleted;
	}
}
