package com.n2n.enroll.contact.bo;

import java.util.ArrayList;
import java.util.List;

public class ContactList {
	protected String status;

	protected List<JContact> contact = new ArrayList<JContact>(0);

	public List<JContact> getContact() {
		return contact;
	}

	public void setContact(List<JContact> contact) {
		this.contact = contact;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
