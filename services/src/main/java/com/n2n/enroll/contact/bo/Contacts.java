package com.n2n.enroll.contact.bo;

import java.util.ArrayList;
import java.util.List;

public class Contacts {
	
	protected List<Contact> emergencyContacts = new ArrayList<Contact>(0);

	public List<Contact> getEmergencyContacts() {
		return emergencyContacts;
	}

	public void setEmergencyContacts(List<Contact> emergencyContacts) {
		this.emergencyContacts = emergencyContacts;
	}
}
