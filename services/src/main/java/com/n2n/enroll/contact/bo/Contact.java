package com.n2n.enroll.contact.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emergency_contacts")
public class Contact implements Serializable {
	@Id
	@Column(name = "id")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "phone")
	private String phone;
	@Column(name = "address")
	private String address;
	@Column(name = "email")
	private String email;
	@Column(name = "picture_url")
	private String picture_url;
	@Column(name = "campus")
	private String campus;
	@Column(name = "category")
	private String category;
	@Column(name = "comments")
	private String comments;
	@Column(name = "seq_num")
	private String sequenceNumber;

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Column(name = "VERSION_NO")
	private String versionNO;
	@Column(name = "LASTMODIFIEDBY")
	private String lastModifiedBY;
	@Column(name = "LASTMODIFIEDON")
	private String lastModifiedON;

	public String getVersionNO() {
		return versionNO;
	}

	public void setVersionNO(String versionNO) {
		this.versionNO = versionNO;
	}

	public String getLastModifiedBY() {
		return lastModifiedBY;
	}

	public void setLastModifiedBY(String lastModifiedBY) {
		this.lastModifiedBY = lastModifiedBY;
	}

	public String getLastModifiedON() {
		return lastModifiedON;
	}

	public void setLastModifiedON(String lastModifiedON) {
		this.lastModifiedON = lastModifiedON;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPicture_url() {
		return picture_url;
	}

	public void setPicture_url(String picture_url) {
		this.picture_url = picture_url;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}
}
