package com.n2n.enroll.contact.bo;

import java.util.ArrayList;
import java.util.List;

public class PersonList {
	public String status;
	public List<JPersonDetails> personDetails = new ArrayList<JPersonDetails>(0);

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<JPersonDetails> getPersonDetails() {
		return personDetails;
	}

	public void setPersonDetails(List<JPersonDetails> personDetails) {
		this.personDetails = personDetails;
	}

}
