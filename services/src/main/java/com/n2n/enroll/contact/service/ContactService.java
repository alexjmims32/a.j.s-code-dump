package com.n2n.enroll.contact.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.n2n.enroll.contact.bo.Contact;
import com.n2n.enroll.contact.bo.ContactBo;
import com.n2n.enroll.contact.bo.ContactList;
import com.n2n.enroll.contact.bo.Contacts;
import com.n2n.enroll.contact.bo.EmergencyContactResponse;
import com.n2n.enroll.contact.bo.EnumListBo;
import com.n2n.enroll.contact.bo.EnumListResponse;
import com.n2n.enroll.contact.bo.JConfirmBioInfoResponse;
import com.n2n.enroll.contact.bo.JContact;
import com.n2n.enroll.contact.bo.JPersonDetails;
import com.n2n.enroll.contact.bo.JUpdateInfo;
import com.n2n.enroll.contact.bo.PersonBo;
import com.n2n.enroll.contact.bo.PersonList;
import com.n2n.enroll.ldap.LdapContact;
import com.n2n.enroll.ldap.PersonDao;
import com.n2n.enroll.util.AppUtil;

@SuppressWarnings("unchecked")
@Service
public class ContactService {
	protected final Log log = LogFactory.getLog(getClass());

	@Resource
	PersonDao personDao;

	@Resource
	SessionFactory sessionFactory;

	public PersonList search(String firstName, String lastName,
			String category, String pageOffset, String pageSize, String start) {

		String dirSource = null;

		PersonList result = new PersonList();
		try {
			dirSource = AppUtil.getProperty("DIRECTORY_SOURCE");
		} catch (Exception e) {
			log.error("Unable to retreive directory source");
			return result;
		}

		if (dirSource == null || dirSource.equals("")) {
			return result;
		}

		if (dirSource.equals("BANNER")) {
			result = searchBanner(firstName, lastName, category, pageOffset,
					pageSize);
		} else if (dirSource.equals("LDAP")) {
			result = searchLdap(firstName, lastName, category, pageOffset,
					pageSize, start);
		} else if (dirSource.equals("ONLINE")) {
			result = searchOnline(firstName, lastName, category, pageOffset,
					pageSize, start);
		}
		return result;
	}

	public PersonList searchLdap(String firstName, String lastName,
			String category, String pageOffset, String pageSize, String start) {

		List<LdapContact> result = personDao.findByName(firstName, lastName,
				category);
		List<LdapContact> contacts = new ArrayList<LdapContact>();
		Integer size = result.size();

		if (size >= (Integer.parseInt(start) + Integer.parseInt(pageSize))) {
			contacts = result.subList(Integer.parseInt(start),
					(Integer.parseInt(start) + Integer.parseInt(pageSize)));
		} else if (size >= Integer.parseInt(start)
				&& size < (Integer.parseInt(start) + Integer.parseInt(pageSize))) {
			contacts = result.subList(Integer.parseInt(start), result.size());
		}
		String defaultFaxType = "FAX";
		String defaultEmailType = "EMAIL";
		String defaultPhoneType = "PHONE";
		String showFax = "Y";
		String showEmail = "Y";
		String showPhone = "Y";

		try {
			defaultFaxType = AppUtil.getProperty("DEFAULT_FAX_TYPE");
		} catch (Exception e) {

		}
		try {
			defaultEmailType = AppUtil.getProperty("DEFAULT_EMAIL_TYPE");
		} catch (Exception e) {

		}
		try {
			showFax = AppUtil.getProperty("SHOW_FAX");
		} catch (Exception e) {

		}
		try {
			showEmail = AppUtil.getProperty("SHOW_EMAIL");
		} catch (Exception e) {

		}
		try {
			showPhone = AppUtil.getProperty("SHOW_PHONE");
		} catch (Exception e) {

		}

		try {
			defaultPhoneType = AppUtil.getProperty("DEFAULT_PHONE_TYPE");
		} catch (Exception e) {

		}

		PersonList list = new PersonList();
		for (LdapContact c : contacts) {
			JPersonDetails jP = new JPersonDetails();

			jP.setFirstName(c.getFirstName());
			jP.setLastName(c.getLastName());
			jP.setFullName(c.getFullName());
			jP.setStatus(c.getOrganizationalStatus());
			jP.setPidm(c.getBannerId());
			jP.setDepartment(c.getDepartment());
			jP.setTitle(c.getTitle());
			jP.setId(c.getuId());

			ContactList cList = new ContactList();
			if (showFax.equals("Y")) {
				JContact jFC = new JContact();
				jFC.setGrp("FAX");
				jFC.setAddress(c.getFax());
				jFC.setType(defaultFaxType);
				cList.getContact().add(jFC);
			}
			if (showEmail.equals("Y")) {
				JContact jC = new JContact();
				jC.setGrp("EMAIL");
				jC.setAddress(c.getEmail());
				jC.setType(defaultEmailType);
				cList.getContact().add(jC);
			}
			// System.out.print(c.getPhone());
			if (showPhone.equals("Y") && c.getPhone() != ""
					&& c.getPhone() != null) {
				JContact jPC = new JContact();
				jPC.setGrp("PHONE");
				jPC.setAddress(c.getPhone());
				jPC.setType(defaultPhoneType);
				cList.getContact().add(jPC);
			}

			jP.setContactList(cList);

			list.getPersonDetails().add(jP);
		}

		list.setStatus("SUCCESS");
		return list;
	}

	public PersonList searchBanner(String firstName, String lastName,
			String category, String pageOffset, String pageSize) {

		if ((firstName == null || "".equals(firstName))
				&& (lastName == null || "".equals(lastName))) {
			return new PersonList();
		}

		// Paging Params
		if (pageOffset == null || "".equals(pageOffset)) {
			pageOffset = "1";
		}

		if (pageSize == null || "".equals(pageSize)) {
			pageSize = "20";
		}

		int pageOffsetInt = 1;
		try {
			pageOffsetInt = Integer.parseInt(pageOffset);
		} catch (Exception e) {
			// Do nothing. Default is set to 1
		}
		int pageSizeInt = 20;
		try {
			pageSizeInt = Integer.parseInt(pageSize);
		} catch (Exception e) {
			// Do nothing. Default is set to 20
		}

		String banCategory = "";

		try {
			if (category.equals("all")) {
				banCategory = "";
			} else if (category.equals("student")) {
				banCategory = AppUtil.getProperty("DIRECTORY_CATAGORY_STUDENT");
			} else {
				banCategory = AppUtil.getProperty("DIRECTORY_CATAGORY_FACULTY");
			}
		} catch (Exception e) {
		}

		PersonList pList = new PersonList();
		try {

			// firstName = firstName.replaceAll("(\\s+)", ":").toLowerCase();
			// lastName = lastName.replaceAll("(\\s+)", ":").toLowerCase();

			String condition = "";
			if (banCategory == null || "".equals(banCategory)) {
				if (firstName == null || "".equals(firstName)) {
					condition = String.format(
							"( lower(lst_name) LIKE '%%%s%%' )",
							lastName.toLowerCase());
				} else if (lastName == null || "".equals(lastName)) {
					condition = String.format(
							"( lower(first_name) LIKE '%%%s%%' )",
							firstName.toLowerCase());
				} else {
					condition = String
							.format("( Lower(first_name) LIKE '%%%s%%' and lower(lst_name) LIKE '%%%s%%' )",
									firstName.toLowerCase(),
									lastName.toLowerCase());
				}
			} else {
				if (firstName == null || "".equals(firstName)) {
					condition = String
							.format("( lower(lst_name) LIKE '%%%s%%' and category = '%s' )",
									lastName.toLowerCase(), banCategory);
				} else if (lastName == null || "".equals(lastName)) {
					condition = String
							.format("( lower(first_name) LIKE '%%%s%%' and category = '%s' )",
									firstName.toLowerCase(), banCategory);
				} else {
					condition = String
							.format("( Lower(first_name) LIKE '%%%s%%' and lower(lst_name) LIKE '%%%s%%' and category  = '%s' )",
									firstName.toLowerCase(),
									lastName.toLowerCase(), banCategory);
				}
			}

			pList = getPersonInfo(condition, pageOffsetInt, pageSizeInt);

		} catch (Exception e) {
			log.error("Error in Searching contact List : " + e.getMessage());
			log.error("StackTrace", e);
			pList.setStatus("Error in Searching ");
		}

		return pList;
	}

	public PersonList getPersonInfo(String condition, int pageOffsetInt,
			int pageSizeInt) throws Exception {
		PersonList pList = new PersonList();
		String status = "SUCCESS";
		String personSql = AppUtil.getProperty("Q_GET_SEARCH_BY_NAME");

		personSql = (AppUtil.format(personSql, condition));

		List<PersonBo> pel = (List<PersonBo>) AppUtil.runAQueryOpenSession(
				personSql, pageOffsetInt, pageSizeInt, PersonBo.class);

		if (pel.size() > 0) {
			String pidms = "";
			for (PersonBo perBo : pel) {
				if (pidms.equals("")) {
					pidms = pidms + "'" + perBo.getPidm() + "'";
				} else {
					pidms = pidms + "," + "'" + perBo.getPidm() + "'";

				}

			}

			String sql = AppUtil.getProperty("Q_GET_CONTACT_DETAILS");

			sql = (AppUtil.format(sql, pidms));

			List<ContactBo> l = (List<ContactBo>) AppUtil
					.runAQueryCurrentSession(sql, ContactBo.class);

			String showFax = AppUtil.getProperty("SHOW_FAX");
			String showEmail = AppUtil.getProperty("SHOW_EMAIL");
			String showPhone = AppUtil.getProperty("SHOW_PHONE");
			String showAddress = AppUtil.getProperty("SHOW_ADDRESS");
			String showEmergencyContact = AppUtil
					.getProperty("SHOW_EMERGENCY_CONTACT");
			for (PersonBo personBo : pel) {
				JPersonDetails details = new JPersonDetails();
				details.setId(AppUtil.getStringValue(personBo.getId()));
				details.setPidm(personBo.getPidm());
				details.setLastName(personBo.getLastName());
				details.setFirstName(personBo.getFirstName());
				details.setMiddleName(personBo.getMiddleName());
				if (details.getFirstName() == null
						|| details.getFirstName().equals("")) {
					details.setFirstName("");
				}
				if (details.getLastName() == null
						|| details.getLastName().equals("")) {
					details.setLastName("");
				}
				if (details.getMiddleName() == null
						|| details.getMiddleName().equals("")) {
					details.setMiddleName("");
				}
				details.setFullName(details.getFirstName() + " "
						+ details.getMiddleName() + " " + details.getLastName());

				details.setSsn(AppUtil.getStringValue(personBo.getSsn()));
				details.setDob(AppUtil.getStringValue(personBo.getDob()));
				details.setPrefix(AppUtil.getStringValue(personBo.getPrefix()));
				details.setCategory(AppUtil.getStringValue(personBo
						.getCategory()));
				details.setPublished(AppUtil.getStringValue(personBo
						.getPublished()));
				details.setUpdated(AppUtil.getStringValue(personBo.getUpdated()));

				ContactList contactList = new ContactList();

				for (ContactBo contactBo : l) {

					if (personBo.getPidm().equals(contactBo.getPidm())) {

						boolean canSend = false;

						if (contactBo.getGrp().equals("EMAIL")
								&& showEmail.equals("Y")) {
							canSend = true;
						} else if (contactBo.getGrp().equals("FAX")
								&& showFax.equals("Y")) {
							canSend = true;

						} else if (contactBo.getGrp().equals("PHONE")
								&& showPhone.equals("Y")) {
							canSend = true;

						} else if (contactBo.getGrp().equals("ADDRESS")
								&& showAddress.equals("Y")) {
							canSend = true;
						} else if (contactBo.getGrp().equals(
								"EMERGENCY CONTACT")
								&& showEmergencyContact.equals("Y")) {
							canSend = true;
						}

						if (canSend) {
							JContact contact = new JContact();
							contact.setGrp(AppUtil.getStringValue(contactBo
									.getGrp()));
							contact.setPidm(AppUtil.getStringValue(contactBo
									.getPidm()));
							contact.setType(AppUtil.getStringValue(contactBo
									.getType()));
							contact.setPri(AppUtil.getStringValue(contactBo
									.getPri()));
							if (contact.getGrp().equalsIgnoreCase("PHONE")) {
								contact.setAddress(AppUtil
										.getStringValue(AppUtil
												.phoneNumberFormat(contactBo
														.getAddress())));
							} else {
								contact.setAddress(AppUtil
										.getStringValue(contactBo.getAddress()));
							}
							contactList.getContact().add(contact);
						}
					}
				}
				details.setContactList(contactList);
				pList.getPersonDetails().add(details);
			}
		}
		pList.setStatus(status);
		return pList;
	}

	public PersonList getContact(String id) {
		PersonList pList = new PersonList();
		try {

			String condition = "id='" + id + "'";
			pList = getPersonInfo(condition, 1, 20);

		} catch (Exception e) {
			log.error("Error in getting contact List : " + e.getMessage());
			log.error("StackTrace", e);
		}
		return pList;
	}

	public Contacts getUniversityContacts(String campusCode, String category,
			String searchString) {

		String query = null;
		Contacts contacts = new Contacts();
		try {
			query = AppUtil.getProperty("Q_GET_EMERGENCYCONTACTS");
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting getAll query " + e.getMessage());
		}

		// try{
		// query = (fmt.format(query));
		// }catch(Exception e){
		// log.error("StackTrace", e);
		// log.error("Error in formating  getAll query " + e.getMessage());
		// }
		String condition = "";
		try {

			if (campusCode != null && !campusCode.equals("")) {
				condition = condition + "WHERE  campus = '" + campusCode + "'";
				if (category != null && !category.equals("")) {
					condition = condition + " and category = '" + category
							+ "'";
				}
				if (searchString != null && !searchString.equals("")) {
					condition = condition
							+ " and "
							+ String.format("upper(name) LIKE Upper('%%%s%%')",
									searchString);
				}
			} else {
				if (category != null && !category.equals("")
						&& searchString != null && !searchString.equals("")) {
					condition = condition
							+ "WHERE category = '"
							+ category
							+ "' and "
							+ String.format("upper(name) LIKE Upper('%%%s%%')",
									searchString);
				} else if (category != null && !category.equals("")) {
					condition = condition + "WHERE category = '" + category
							+ "'";
				} else if (searchString != null && !searchString.equals("")) {
					condition = condition + "WHERE "
							+ String.format("name LIKE '%%%s%%'", searchString);
				}
			}
			condition = condition + "order by seq_num";

			query = (AppUtil.format(query, condition));

			List<Contact> contactsList = (List<Contact>) AppUtil
					.runAQueryCurrentSession(query, Contact.class);
			contacts.setEmergencyContacts(contactsList);
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving contacts " + e.getMessage());
		}

		return contacts;
	}

	public EmergencyContactResponse addOrEditEmergecyContact(String id,
			String name, String phone, String campusCode, String address,
			String email, String pictureUrl, String lastModifiedBy,
			String category, String comments) {
		String status = "";
		String query = null;
		EmergencyContactResponse emergencyContactResponse = new EmergencyContactResponse();
		int verssionId = 1;
		String replaceName = name.replaceAll("'", "''");
		String replaceAdd = address.replaceAll("'", "''");

		try {
			if (id != null && !(id.equals(""))) {

				String versionQuery = AppUtil
						.getProperty("Q_GET_TRACKING_CONTACTS");
				versionQuery = (AppUtil.format(versionQuery, id));

				List<Contact> contacts = (List<Contact>) AppUtil
						.runAQueryCurrentSession(versionQuery, Contact.class);
				for (Contact con : contacts) {
					String insertTrck = AppUtil
							.getProperty("Q_INSERT_TRACKING_CONTACTS");

					insertTrck = (AppUtil.format(insertTrck, con.getId(),
							con.getCampus(), con.getName(), con.getPhone(),
							con.getAddress(), con.getEmail(),
							con.getPicture_url(), con.getVersionNO(),
							con.getLastModifiedBY(), con.getCategory(),
							con.getComments()));

					log.debug("    :::    " + insertTrck);
					AppUtil.runADeleteQuery(insertTrck);
				}

				verssionId = verssionId + 1;
				query = AppUtil.getProperty("Q_UPDATE_EMERGENCYCONTACTS");
				query = (AppUtil.format(query, replaceName, phone, replaceAdd,
						email, pictureUrl, campusCode, verssionId,
						lastModifiedBy, category, comments, id));

				AppUtil.runADeleteQuery(query);

			} else if (id == null || id.equals("")) {

				String verQry = "select max(VERSION_NO) from emergency_contacts";
				BigDecimal maxVersion;
				long idVersion = 0;
				maxVersion = AppUtil.runACountQuery(verQry);
				idVersion = maxVersion.longValue();
				idVersion = idVersion + 1;

				query = AppUtil.getProperty("Q_ADD_EMERGENCYCONTACTS");
				String qry = "select max(id) from emergency_contacts";
				BigDecimal maxId;
				long idVal = 0;
				maxId = AppUtil.runACountQuery(qry);
				idVal = maxId.longValue();
				idVal = idVal + 1;

				String seqQry = "select max(seq_num) from emergency_contacts";
				BigDecimal seqId;
				long seqVal = 0;
				seqId = AppUtil.runACountQuery(seqQry);
				seqVal = seqId.longValue();
				seqVal = seqVal + 1;

				query = (AppUtil.format(query, seqVal, idVal, campusCode,
						replaceName, phone, replaceAdd, email, pictureUrl,
						category, idVersion, lastModifiedBy, comments));

				AppUtil.runADeleteQuery(query);
			}
			status = "SUCCESS";
			emergencyContactResponse.setStatus(status);

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in add or update query " + e.getMessage());
			emergencyContactResponse.setStatus(e.getMessage());
		}
		return emergencyContactResponse;
	}

	public PersonList searchOnline(String firstName, String lastName,
			String category, String pageOffset, String pageSize, String start) {
		PersonList pList = new PersonList();
		try {
			String uri = AppUtil.getProperty("DIRECTORY_SEARCH_URL");

			if ((category.equals(null) || category.equals(""))
					&& (lastName.equals(null) || lastName.equals(""))) {
				uri = AppUtil.format(uri, firstName);
			} else {
				uri = AppUtil.format(uri, category.toLowerCase(), lastName,
						firstName);
			}
			URL url = new URL(uri);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/xml");

			InputStream xml = connection.getInputStream();

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(xml);
			NodeList nodeLst = doc.getElementsByTagName("Directory");
			if (nodeLst.item(0) == null) {
				NodeList nodeLst1 = doc.getElementsByTagName("feed");
				if (nodeLst1.item(0) != null) {
					NodeList entryList1 = ((Element) nodeLst1.item(0))
							.getElementsByTagName("item");
					pList = parseXmltoPersonList(entryList1, start, pageSize);
				}
			} else {
				NodeList entryList = ((Element) nodeLst.item(0))
						.getElementsByTagName("entry");
				pList = parseXmltoPersonList(entryList, start, pageSize);
			}
			pList.setStatus("SUCCESS");
		} catch (Exception e) {
			log.error("Error in Searching contact List : " + e.getMessage());
			log.error("StackTrace", e);
			pList.setStatus("Error in Searching ");
		}

		return pList;
	}

	private PersonList parseXmltoPersonList(NodeList entryList, String start,
			String pageSize) {
		PersonList pList = new PersonList();
		int s = Integer.parseInt(start);

		for (; s < entryList.getLength()
				&& s < (Integer.parseInt(start) + Integer.parseInt(pageSize)); s++) {

			Node fstNode = entryList.item(s);
			if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

				Element fstElmnt = (Element) fstNode;
				pList.getPersonDetails().add(parseXmltoPerson(fstElmnt));
			}
		}

		return pList;
	}

	private JPersonDetails parseXmltoPerson(Element entry) {
		JPersonDetails person = new JPersonDetails();
		if (getProperty(entry, "name_first") != null) {
			String full_name = getProperty(entry, "name_first") + " "
					+ getProperty(entry, "name_last");
			person.setFullName(full_name);
		} else {
			person.setFullName(getProperty(entry, "full_name"));
		}
		person.setImage(getProperty(entry, "image"));
		person.setTitle(getProperty(entry, "title"));
		person.setDepartment(getProperty(entry, "department"));
		person.setCampus(getProperty(entry, "campus"));
		person.setOffice(getProperty(entry, "office"));
		person.setMailstop(getProperty(entry, "mail_stop"));
		String defaultEmailType = "EMAIL";
		String defaultPhoneType = "PHONE";
		String defaultFaxType = "FAX";
		String defaultAddressType = "ADDRESS";
		try {
			defaultEmailType = AppUtil.getProperty("DEFAULT_EMAIL_TYPE");
		} catch (Exception e) {

		}
		try {
			defaultPhoneType = AppUtil.getProperty("DEFAULT_PHONE_TYPE");
		} catch (Exception e) {

		}
		try {
			defaultFaxType = AppUtil.getProperty("DEFAULT_FAX_TYPE");
		} catch (Exception e) {

		}
		try {
			defaultAddressType = AppUtil.getProperty("DEFAULT_ADDRESS_TYPE");
		} catch (Exception e) {

		}

		ContactList cList = new ContactList();
		String email = getProperty(entry, "email");
		if (email != "" && email != null) {
			JContact jeC = new JContact();
			jeC.setGrp("EMAIL");
			jeC.setAddress(getProperty(entry, "email"));
			jeC.setType(defaultEmailType);
			cList.getContact().add(jeC);
		}
		String phone = getProperty(entry, "phone");
		if (phone != "" && phone != null) {
			JContact jPC = new JContact();
			jPC.setGrp("PHONE");
			jPC.setAddress(phone);
			jPC.setType(defaultPhoneType);
			cList.getContact().add(jPC);
		}
		String fax = getProperty(entry, "fax");
		if (fax != "" && fax != null) {
			JContact jFC = new JContact();
			jFC.setGrp("FAX");
			jFC.setAddress(fax);
			jFC.setType(defaultFaxType);
			cList.getContact().add(jFC);
		}

		String address = getProperty(entry, "address");
		String csz = getProperty(entry, "city_state_zip");
		if ((address != "" && address != null) || (csz != "" && csz != null)) {
			JContact jAC = new JContact();
			jAC.setGrp("ADDRESS");
			jAC.setAddress(address + " " + csz);
			jAC.setType(defaultAddressType);
			cList.getContact().add(jAC);
		}

		person.setContactList(cList);
		return person;
	}

	public EmergencyContactResponse deleteAdminContact(String sequenceNumber,
			String id) {

		EmergencyContactResponse response = new EmergencyContactResponse();
		String sql = "";
		String status = "SUCCESS";
		try {

			String sqlSequence = AppUtil
					.getProperty("Q_DELETE_UPDATE_SEQUENCE");

			sqlSequence = AppUtil.format(sqlSequence, sequenceNumber);

			AppUtil.runADeleteQuery(sqlSequence);

			sql = AppUtil.getProperty("Q_DELETE_EMERGENCY_CONTACT");

			sql = AppUtil.format(sql, id);

			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
		}
		return response;
	}

	private String getProperty(Element e, String property) {

		String value = null;
		try {
			NodeList fstmElmntLst = e.getElementsByTagName(property);

			Element fstNmElmnt = (Element) fstmElmntLst.item(0);
			NodeList fstNm = fstNmElmnt.getChildNodes();
			value = ((Node) fstNm.item(0)).getNodeValue();
		} catch (Exception excep) {

		}
		return value;

	}

	public EmergencyContactResponse moveContactUp(String id,
			String sequenceNumber) {

		EmergencyContactResponse response = new EmergencyContactResponse();
		String sql = "";
		String status = "SUCCESS";

		try {

			String sqlSequence = AppUtil.getProperty("Q_UPDATE_SEQUENCE");

			sqlSequence = AppUtil.format(sqlSequence,
					Integer.parseInt(sequenceNumber) + 1, sequenceNumber);

			AppUtil.runADeleteQuery(sqlSequence);

			sql = AppUtil.getProperty("Q_CONTACT_CHANGE_DIRECTION");

			sql = AppUtil.format(sql, sequenceNumber, id);

			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}

	public EmergencyContactResponse moveContactDown(String id,
			String sequenceNumber) {

		EmergencyContactResponse response = new EmergencyContactResponse();
		String sql = "";
		String status = "SUCCESS";
		try {
			String sqlSequence = AppUtil.getProperty("Q_UPDATE_SEQUENCE");

			sqlSequence = AppUtil.format(sqlSequence,
					Integer.parseInt(sequenceNumber) - 1, sequenceNumber);

			AppUtil.runADeleteQuery(sqlSequence);

			sql = AppUtil.getProperty("Q_CONTACT_CHANGE_DIRECTION");

			sql = AppUtil.format(sql, sequenceNumber, id);

			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}

	public EnumListResponse getEnums(String enumType) throws Exception {
		EnumListResponse enumList = new EnumListResponse();
		try {
			String sql = AppUtil.getProperty("Q_GET_ENUM_LIST");
			// log.debug("Relations list started ------" + new Date());
			sql = AppUtil.format(sql, enumType);
			log.info(sql);
			List<EnumListBo> enums = (List<EnumListBo>) AppUtil
					.runAQueryCurrentSession(sql, EnumListBo.class);
			// log.debug("Relations list completed ------" + new Date());
			enumList.setEnums(enums);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return enumList;
	}

	public EnumListResponse getCountyList(String enumType, String stateCode)
			throws Exception {
		EnumListResponse enumList = new EnumListResponse();
		try {
			String sql = AppUtil.getProperty("Q_GET_COUNTY_LIST");
			// log.debug("Relations list started ------" + new Date());
			sql = AppUtil.format(sql, stateCode, enumType);
			log.info(sql);
			List<EnumListBo> enums = (List<EnumListBo>) AppUtil
					.runAQueryCurrentSession(sql, EnumListBo.class);
			// log.debug("Relations list completed ------" + new Date());
			enumList.setEnums(enums);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return enumList;
	}

	public String bioContact(String studentId, String role)
			throws HibernateException, SQLException, Exception {
		Clob data;
		String response = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": bioContact procedure started ------");
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_BIO_INFO") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.CLOB);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, role);
			callableStatement.execute();

			log.debug(studentId + ": bioContact procedure completed ------");
			data = callableStatement.getClob(3);
			log.debug(data);
			callableStatement.close();
			if (!(data == null)) {
				response = getClobData(data);
				log.debug(response);
			}
		} catch (Exception e) {
			log.error("Error in BioContact..." + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response = "Server error has occured.Please try again";
			} else {
				response = e.getMessage();
			}
		}

		return response;
	}

	public JUpdateInfo updateBioInfo(String studentId, String role,
			String type, String sequenceNo, String delAddr, String dateFmt,
			String tDate, String houseNo, String str1, String str2,
			String str3, String str4, String city, String state, String zip,
			String county, String nation, String country, String area,
			String primaryNo, String primaryExt, String pAccess, String unlist,
			String seqList, String phCodeList, String countryList,
			String areaList, String phList, String phExtList, String intAccess,
			String unlistInd, String delInd, String pRelation, String order,
			String lastNamePfx, String lastName, String firstName, String pmi)
			throws HibernateException, Exception {
		JUpdateInfo response = new JUpdateInfo();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String status = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			log.debug(studentId + ": update procedure started---" + new Date());
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_UPDATE_INFO") + "}");

			callableStatement.registerOutParameter(39, java.sql.Types.VARCHAR);

			callableStatement.setString(1, studentId);
			log.info("studentId:" + studentId);
			callableStatement.setString(2, "STUDENT");
			log.info("role:" + role);
			callableStatement.setString(3, type);
			log.info("type:" + type);
			if ("".equals(sequenceNo)) {
				callableStatement.setString(4, null);
			} else {
				callableStatement.setString(4, sequenceNo);
			}
			log.info("sequenceNo:" + sequenceNo);
			if ("".equals(pRelation)) {
				callableStatement.setString(5, null);
			} else {
				callableStatement.setString(5, pRelation);
			}
			log.info("pRelation:" + pRelation);
			if ("".equals(order)) {
				callableStatement.setString(6, null);
			} else {
				callableStatement.setString(6, order);
			}
			log.info("order:" + order);
			if ("".equals(delAddr)) {
				callableStatement.setString(7, null);
			} else {
				callableStatement.setString(7, delAddr);
			}
			log.info("delAddr:" + delAddr);
			callableStatement.setString(8, dateFormat.format(date));
			log.info("dateFmt:" + dateFormat.format(date));
			if ("".equals(tDate)) {
				callableStatement.setString(9, null);
			} else {
				callableStatement.setString(9, tDate);
			}
			log.info("tDate:" + tDate);
			if ("".equals(lastNamePfx)) {
				callableStatement.setString(10, null);
			} else {
				callableStatement.setString(10, lastNamePfx);
			}
			log.info("lastNamePfx:" + lastNamePfx);
			if ("".equals(lastName)) {
				callableStatement.setString(11, null);
			} else {
				callableStatement.setString(11, lastName);
			}
			log.info("lastName:" + lastName);
			if ("".equals(firstName)) {
				callableStatement.setString(12, null);
			} else {
				callableStatement.setString(12, firstName);
			}
			log.info("firstName:" + firstName);
			if ("".equals(pmi)) {
				callableStatement.setString(13, null);
			} else {
				callableStatement.setString(13, pmi);
			}
			log.info("pmi:" + pmi);
			if ("".equals(houseNo)) {
				callableStatement.setString(14, null);
			} else {
				callableStatement.setString(14, houseNo);
			}
			log.info("houseNo:" + houseNo);
			if ("".equals(str1)) {
				callableStatement.setString(15, null);
			} else {
				callableStatement.setString(15, str1);
			}
			log.info("str1:" + str1);
			if ("".equals(str2)) {
				callableStatement.setString(16, null);
			} else {
				callableStatement.setString(16, str2);
			}
			log.info("str2:" + str2);
			if ("".equals(str3)) {
				callableStatement.setString(17, null);
			} else {
				callableStatement.setString(17, str3);
			}
			log.info("str3:" + str3);
			if ("".equals(str4)) {
				callableStatement.setString(18, null);
			} else {
				callableStatement.setString(18, str4);
			}
			log.info("str4:" + str4);
			if ("".equals(city)) {
				callableStatement.setString(19, null);
			} else {
				callableStatement.setString(19, city);
			}
			log.info("city:" + city);
			if ("".equals(state)) {
				callableStatement.setString(20, null);
			} else {
				callableStatement.setString(20, state);
			}
			log.info("state:" + state);
			if ("".equals(zip)) {
				callableStatement.setString(21, null);
			} else {
				callableStatement.setString(21, zip);
			}
			log.info("zip:" + zip);
			if ("".equals(county)) {
				callableStatement.setString(22, null);
			} else {
				callableStatement.setString(22, county);
			}
			log.info("county:" + county);
			if ("".equals(nation)) {
				callableStatement.setString(23, null);
			} else {
				callableStatement.setString(23, nation);
			}
			log.info("nation:" + nation);
			if ("".equals(country)) {
				callableStatement.setString(24, null);
			} else {
				callableStatement.setString(24, country);
			}
			log.info("country:" + country);
			if ("".equals(area)) {
				callableStatement.setString(25, null);
			} else {
				callableStatement.setString(25, area);
			}
			log.info("area:" + area);
			if ("".equals(primaryNo)) {
				callableStatement.setString(26, null);
			} else {
				callableStatement.setString(26, primaryNo);
			}
			log.info("primaryNo:" + primaryNo);
			if ("".equals(primaryExt)) {
				callableStatement.setString(27, null);
			} else {
				callableStatement.setString(27, primaryExt);
			}
			log.info("primaryExt:" + primaryExt);
			if ("".equals(pAccess)) {
				callableStatement.setString(28, null);
			} else {
				callableStatement.setString(28, pAccess);
			}
			log.info("pAccess:" + pAccess);
			if ("".equals(unlist)) {
				callableStatement.setString(29, null);
			} else {
				callableStatement.setString(29, unlist);
			}
			log.info("unlist:" + unlist);
			if ("".equals(seqList)) {
				callableStatement.setString(30, null);
			} else {
				callableStatement.setString(30, seqList);
			}
			log.info("seqList:" + seqList);
			if ("".equals(phCodeList)) {
				callableStatement.setString(31, null);
			} else {
				callableStatement.setString(31, phCodeList);
			}
			log.info("phCodeList:" + phCodeList);
			callableStatement.setString(32, ",,,,");
			if ("".equals(areaList)) {
				callableStatement.setString(33, null);
			} else {
				callableStatement.setString(33, areaList);
			}
			log.info("areaList:" + areaList);
			if ("".equals(phList)) {
				callableStatement.setString(34, null);
			} else {
				callableStatement.setString(34, phList);
			}
			log.info("phList:" + phList);
			if ("".equals(phExtList)) {
				callableStatement.setString(35, null);
			} else {
				callableStatement.setString(35, phExtList);
			}
			log.info("phExtList:" + phExtList);
			if ("".equals(intAccess)) {
				callableStatement.setString(36, null);
			} else {
				callableStatement.setString(36, intAccess);
			}
			log.info("intAccess:" + intAccess);
			if ("".equals(unlistInd)) {
				callableStatement.setString(37, null);
			} else {
				callableStatement.setString(37, unlistInd);
			}
			log.info("unlistInd:" + unlistInd);
			if ("".equals(delInd)) {
				callableStatement.setString(38, null);
			} else {
				callableStatement.setString(38, delInd);
			}
			log.info("delInd:" + delInd);

			callableStatement.execute();
			log.debug(callableStatement);
			log.debug(studentId + ": update BioInfo procedure completed---"
					+ new Date());
			String errorMsg = callableStatement.getString(39);
			callableStatement.close();
			log.debug(errorMsg);
			if (errorMsg != null) {
				status = errorMsg;
				response.setStatus(status);
			} else {
				status = "SUCCESS";
				response.setStatus(status);
			}

		} catch (Exception e) {
			log.error("Error in update BioInfo:" + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")
					|| e.getMessage().contains("ORA-06508")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				status = "Server error has occured.Please try again";
				log.debug(status);
			} else {
				status = e.getMessage();
				response.setStatus(status);
			}
			log.error("StackTrace", e);
		}
		return response;
	}

	public JConfirmBioInfoResponse confirmBioInfo(String studentId, String type)
			throws HibernateException, Exception {
		JConfirmBioInfoResponse response = new JConfirmBioInfoResponse();
		String status = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			log.debug(studentId + ": Confirm procedure started---" + new Date());
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_CONFIRM_INFO") + "}");

			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);

			callableStatement.setString(1, studentId);
			callableStatement.setString(2, type);
			callableStatement.execute();
			log.debug(studentId + ": confirm BioInfo procedure completed---"
					+ new Date());
			String errorMsg = callableStatement.getString(3);
			callableStatement.close();
			if (errorMsg != null) {
				status = errorMsg;
				response.setStatus(status);
			} else {
				status = "SUCCESS";
				response.setStatus(status);
			}

		} catch (Exception e) {
			log.error("Error in confirm BioInfo:" + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				status = "Server error has occured.Please try again";
				response.setStatus(status);
			} else {
				status = e.getMessage();
				response.setStatus(status);
			}
			log.error("StackTrace", e);
		}

		return response;
	}

	public String getClobData(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();

	}
}
