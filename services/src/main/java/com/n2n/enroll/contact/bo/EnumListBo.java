package com.n2n.enroll.contact.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
public class EnumListBo implements java.io.Serializable {
	@Id
	private String id;

	@Column(name = "enum_value")
	private String value;
	
	@Column(name = "enum_desc")
	private String description;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
