package com.n2n.enroll.contact.bo;

import java.util.ArrayList;
import java.util.List;

public class EnumListResponse {
	protected List<EnumListBo> enums = new ArrayList<EnumListBo>(0);

	public List<EnumListBo> getEnums() {
		return enums;
	}

	public void setEnums(List<EnumListBo> enums) {
		this.enums = enums;
	}
}
