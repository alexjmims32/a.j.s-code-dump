package com.n2n.enroll.bursar.bo;

import java.util.ArrayList;
import java.util.List;

public class BursarTermList {
	protected List<Term> bursarTermList = new ArrayList<Term>(0);

	public List<Term> getBursarTermList() {
		return bursarTermList;
	}

	public void setBursarTermList(List<Term> bursarTermList) {
		this.bursarTermList = bursarTermList;
	}

}
