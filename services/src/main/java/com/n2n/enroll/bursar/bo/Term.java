package com.n2n.enroll.bursar.bo;

import javax.persistence.Column;
import javax.persistence.Id;

public class Term {

	@Id
	private String id;
	@Column(name = "TERM_CODE")
	private String code;
	@Column(name = "TERM_DESC")
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
