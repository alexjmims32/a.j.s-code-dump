package com.n2n.enroll.bursar.bo;

import java.util.ArrayList;
import java.util.List;

public class SummaryDetailsList {
	private List<SummaryDetails> summaryDetails = new ArrayList<SummaryDetails>(0);

	public List<SummaryDetails> getSummaryDetails() {
		return summaryDetails;
	}

	public void setSummaryDetails(List<SummaryDetails> summaryDetails) {
		this.summaryDetails = summaryDetails;
	}
	
}
