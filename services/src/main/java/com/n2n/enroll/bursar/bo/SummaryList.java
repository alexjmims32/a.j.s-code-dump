package com.n2n.enroll.bursar.bo;

import java.util.ArrayList;
import java.util.List;

public class SummaryList {
	private List<Summary> payments = new ArrayList<Summary>(0);
	private List<Summary> charges = new ArrayList<Summary>(0);
	private List<Summary> due = new ArrayList<Summary>(0);
	public List<Summary> getPayments() {
		return payments;
	}
	public void setPayments(List<Summary> payments) {
		this.payments = payments;
	}
	public List<Summary> getCharges() {
		return charges;
	}
	public void setCharges(List<Summary> charges) {
		this.charges = charges;
	}
	public List<Summary> getDue() {
		return due;
	}
	public void setDue(List<Summary> due) {
		this.due = due;
	}
	
}
