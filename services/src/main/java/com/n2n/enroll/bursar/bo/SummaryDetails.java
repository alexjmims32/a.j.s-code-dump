package com.n2n.enroll.bursar.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class SummaryDetails {
	@Id
	private String id;
	@Column(name = "term_code")
	private String termCode;
	@Column(name = "amount")
	private String amount;
	@Column(name = "balance")
	private String balance;
	@Column(name = "bill_date")
	private String billDate;
	@Column(name = "due_date")
	private String dueDate;
	@Column(name = "term_desc")
	private String termDescription;
	@Column(name = "category")
	private String category;
	@Column(name = "description")
	private String categoryDescription;	
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategoryDescription() {
		return categoryDescription;
	}
	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}
	public String getTermDescription() {
		return termDescription;
	}
	public void setTermDescription(String termDescription) {
		this.termDescription = termDescription;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTermCode() {
		return termCode;
	}
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
}
