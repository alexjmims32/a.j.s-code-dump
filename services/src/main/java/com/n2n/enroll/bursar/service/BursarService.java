package com.n2n.enroll.bursar.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.n2n.enroll.bursar.bo.BursarTermList;
import com.n2n.enroll.bursar.bo.EbillSummary;
import com.n2n.enroll.bursar.bo.EbillSummaryList;
import com.n2n.enroll.bursar.bo.Summary;
import com.n2n.enroll.bursar.bo.SummaryDetails;
import com.n2n.enroll.bursar.bo.SummaryDetailsList;
import com.n2n.enroll.bursar.bo.SummaryList;
import com.n2n.enroll.bursar.bo.SummaryView;
import com.n2n.enroll.bursar.bo.Term;
import com.n2n.enroll.registration.bo.CurrentTerm;
import com.n2n.enroll.registration.service.RegistrationService;
import com.n2n.enroll.util.AppUtil;

@SuppressWarnings({ "rawtypes", "unchecked", "resource" })
@Service
public class BursarService {
	@Resource
	SessionFactory sessionFactory;

	@Autowired
	RegistrationService registrationService;

	protected final Log log = LogFactory.getLog(getClass());

	public BursarTermList getBursarTerms(String studentId) {
		BursarTermList xBursarTermList = new BursarTermList();
		try {

			String sql = AppUtil.getProperty("Q_GET_BURSAR_TERMS");

			sql = (AppUtil.format(sql, studentId));
			List l = AppUtil.runAQuery(sql);

			for (Object obj : l) {
				Object[] termFields = (Object[]) obj;

				Term xTerm = new Term();

				xTerm.setCode(AppUtil.getStringValue(termFields[0]));
				xTerm.setDescription(AppUtil.getStringValue(termFields[1]));
				xBursarTermList.getBursarTermList().add(xTerm);
			}

		} catch (Exception e) {
			log.error(" Error in retriveing Bursar terms......"
					+ e.getMessage());
		}

		return xBursarTermList;
	}

	public SummaryList getSummaryList(String studentId) throws Exception {
		SummaryList summaryList = new SummaryList();

		String summaryListSql = AppUtil.getProperty("Q_GET_SUMMARY_LIST");
		summaryListSql = (AppUtil.format(summaryListSql, studentId));
		List<SummaryView> sumList = (List<SummaryView>) AppUtil
				.runAQueryCurrentSession(summaryListSql, SummaryView.class);
		summaryList = getSummaryListObject(sumList);
		return summaryList;
	}

	public SummaryList getSummaryListByTerm(String studentId, String termCode)
			throws Exception {
		if (termCode == null || termCode.equals("")) {
			CurrentTerm term = getCurrentTerm(studentId);
			termCode = term.getTermCode();
		}
		SummaryList summaryList = new SummaryList();
		String summaryListSql = AppUtil
				.getProperty("Q_GET_SUMMARY_LIST_BY_TERM");

		summaryListSql = (AppUtil.format(summaryListSql, studentId, termCode,
				termCode));
		List<SummaryView> sumList = (List<SummaryView>) AppUtil
				.runAQueryCurrentSession(summaryListSql, SummaryView.class);
		summaryList = getSummaryListObject(sumList);
		return summaryList;
	}

	public SummaryList getSummaryListObject(List<SummaryView> sumDbList) {
		SummaryList summaryList = new SummaryList();
		List<Summary> chargesList = new ArrayList<Summary>(0);
		List<Summary> paymentList = new ArrayList<Summary>(0);
		List<Summary> dueList = new ArrayList<Summary>(0);
		double paymentTotal = 0.00;
		double chargesTotal = 0.00;

		for (SummaryView sumObj : sumDbList) {
			if (sumObj.getCharge() != "0" && sumObj.getCharge() != null
					&& !(sumObj.getCharge().equals(""))) {
				Summary charges = new Summary();
				charges.setAmount(AppUtil.getFloatValue(sumObj.getCharge()));
				charges.setDescription(sumObj.getCategoryDescription());
				charges.setCategory(sumObj.getCategory());
				chargesList.add(charges);
				chargesTotal = chargesTotal
						+ AppUtil.getDoubleValue(sumObj.getCharge());
			}

			if (sumObj.getPayment() != "0" && sumObj.getPayment() != null
					&& !(sumObj.getPayment().equals(""))) {
				Summary payment = new Summary();
				payment.setAmount(AppUtil.getFloatValue(sumObj.getPayment()));
				payment.setDescription(sumObj.getCategoryDescription());
				payment.setCategory(sumObj.getCategory());
				paymentList.add(payment);
				paymentTotal = paymentTotal
						+ AppUtil.getDoubleValue(sumObj.getPayment());
			}
		}
		if (chargesTotal > 0) {
			Summary charges = new Summary();
			charges.setAmount(AppUtil.getFloatValue(chargesTotal));
			charges.setDescription("Total");
			chargesList.add(charges);
		}
		if (paymentTotal > 0) {
			Summary payment = new Summary();
			payment.setAmount(AppUtil.getFloatValue(paymentTotal));
			payment.setDescription("Total");
			paymentList.add(payment);
		}
		summaryList.setCharges(chargesList);
		summaryList.setPayments(paymentList);

		if (Math.abs(chargesTotal - paymentTotal) > 0) {
			Summary due = new Summary();
			due.setDescription("Total amount due");
			due.setAmount(AppUtil.getFloatValue((chargesTotal - paymentTotal)));
			dueList.add(due);
			summaryList.setDue(dueList);
		}

		return summaryList;

	}

	public SummaryDetailsList getSummaryDetails(String studentId,
			String category) throws Exception {
		SummaryDetailsList sumDetailsList = new SummaryDetailsList();
		String summaryDetailsSql = AppUtil.getProperty("Q_GET_SUMMARY_DETAILS");

		summaryDetailsSql = (AppUtil.format(summaryDetailsSql, studentId,
				category));
		List<SummaryDetails> sumDetails = (List<SummaryDetails>) AppUtil
				.runAQueryCurrentSession(summaryDetailsSql,
						SummaryDetails.class);
		sumDetails = convetTimestampToDate(sumDetails);
		sumDetailsList.setSummaryDetails(sumDetails);
		return sumDetailsList;
	}

	public SummaryDetailsList getSummaryDetailsByTerm(String studentId,
			String category, String termCode) throws Exception {
		SummaryDetailsList sumDetailsList = new SummaryDetailsList();
		String summaryDetailsSql = AppUtil.getProperty("Q_GET_SUMMARY_DETAILS");

		summaryDetailsSql = (AppUtil.format(summaryDetailsSql, studentId,
				category));
		summaryDetailsSql = summaryDetailsSql + " and v.term_code='" + termCode
				+ "'";
		List<SummaryDetails> sumDetails = (List<SummaryDetails>) AppUtil
				.runAQueryCurrentSession(summaryDetailsSql,
						SummaryDetails.class);
		sumDetails = convetTimestampToDate(sumDetails);
		sumDetailsList.setSummaryDetails(sumDetails);
		return sumDetailsList;
	}

	public List<SummaryDetails> convetTimestampToDate(
			List<SummaryDetails> sumDetails) throws Exception {
		List<SummaryDetails> summarydetails = new ArrayList<SummaryDetails>(0);
		for (SummaryDetails summary : sumDetails) {
			if (!(AppUtil.getStringValue(summary.getDueDate()).equals("NA"))) {
				String ts = summary.getDueDate();
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
						.parse(ts);
				String dueDate = new SimpleDateFormat("yyyy-MM-dd")
						.format(date);
				summary.setDueDate(dueDate);
			}
			if (!(AppUtil.getStringValue(summary.getBillDate()).equals("NA"))) {
				String ts = summary.getBillDate();
				Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
						.parse(ts);
				String billDate = new SimpleDateFormat("yyyy-MM-dd")
						.format(date);
				summary.setBillDate(billDate);
			}

			summary.setAmount(AppUtil.getFloatValue(summary.getAmount()));
			summary.setBalance(AppUtil.getFloatValue(summary.getBalance()));

			summarydetails.add(summary);
		}
		return summarydetails;
	}

	public EbillSummaryList formatEbillSummary(SummaryList summaryList) {
		double paymentTotal = 0.00;
		double chargesTotal = 0.00;
		EbillSummary ebillSum = null;
		List<EbillSummary> listEbill = new ArrayList<EbillSummary>();
		EbillSummaryList ebillSumList = new EbillSummaryList();
		List<Summary> chargesList = new ArrayList<Summary>(0);
		List<Summary> paymentList = new ArrayList<Summary>(0);
		chargesList = summaryList.getCharges();
		paymentList = summaryList.getPayments();
		for (Summary chargeSummary : chargesList) {
			ebillSum = new EbillSummary();
			ebillSum.setCharges(chargeSummary.getAmount());
			ebillSum.setDescription(chargeSummary.getDescription());
			if (!(chargeSummary.getDescription().equals("Total"))) {
				listEbill.add(ebillSum);
				chargesTotal = chargesTotal
						+ AppUtil.getDoubleValue(chargeSummary.getAmount());
			}
		}
		for (Summary paySummary : paymentList) {
			ebillSum = new EbillSummary();
			ebillSum.setPayments(paySummary.getAmount());
			ebillSum.setDescription(paySummary.getDescription());
			if (!(paySummary.getDescription().equals("Total"))) {
				listEbill.add(ebillSum);
				paymentTotal = paymentTotal
						+ AppUtil.getDoubleValue(paySummary.getAmount());
			}
		}
		ebillSum = new EbillSummary();
		ebillSum.setPayments(AppUtil.getFloatValue(paymentTotal));
		ebillSum.setDescription("Total");
		ebillSum.setCharges(AppUtil.getFloatValue(chargesTotal));
		listEbill.add(ebillSum);
		if (Math.abs(chargesTotal - paymentTotal) > 0) {
			EbillSummary due = new EbillSummary();
			due.setDescription("Total amount due");
			due.setDue(AppUtil.getFloatValue((chargesTotal - paymentTotal)));
			listEbill.add(due);
		}
		ebillSumList.setSummaryList(listEbill);
		return ebillSumList;
	}

	public CurrentTerm getCurrentTerm(String studentId) {
		CurrentTerm xTerm = new CurrentTerm();
		try {

			String sql = AppUtil.getProperty("Q_GET_BURSAR_CURRNT_TERM");

			sql = (AppUtil.format(sql, studentId));
			List l = AppUtil.runAQuery(sql);

			for (Object obj : l) {
				Object[] termFields = (Object[]) obj;
				xTerm.setTermCode(AppUtil.getStringValue(termFields[0]));
				xTerm.setDescription(AppUtil.getStringValue(termFields[1]));
			}

		} catch (Exception e) {
			log.error(" Error in retriveing Bursar Current term......"
					+ e.getMessage());
		}

		return xTerm;
	}

	public String getEbillStatement(String studentId, String termCode)
			throws HibernateException, SQLException, Exception {
		Clob data;
		String response = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": EbillStatement procedure started ------");
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_CLOB_DATA") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.CLOB);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.execute();
			log.debug(studentId + ": EbillStatement procedure completed ------");
			data = callableStatement.getClob(3);
			callableStatement.close();
			if (!(data == null)) {
				response = getClobData(data);
			}
		} catch (Exception e) {
			log.error("Error in EbillStatement..." + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response = "Server error has occured.Please try again";
			} else {
				response = e.getMessage();
			}
		}

		return response;
	}

	public BursarTermList getBillintInfoTerms() {
		BursarTermList termList = new BursarTermList();
		try {

			String sql = AppUtil.getProperty("Q_GET_BILLING_INFO_TERMS");

			sql = (AppUtil.format(sql));
			List l = AppUtil.runAQuery(sql);

			for (Object obj : l) {
				Object[] termFields = (Object[]) obj;
				Term xTerm = new Term();
				xTerm.setCode(AppUtil.getStringValue(termFields[0]));
				xTerm.setDescription(AppUtil.getStringValue(termFields[1]));
				termList.getBursarTermList().add(xTerm);
			}

		} catch (Exception e) {
			log.error(" Error in retriveing Bursar terms......"
					+ e.getMessage());
		}

		return termList;
	}

	public String getClobData(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();

	}
}