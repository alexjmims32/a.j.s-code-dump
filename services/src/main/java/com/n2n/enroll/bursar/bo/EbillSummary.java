package com.n2n.enroll.bursar.bo;



public class EbillSummary{
	private String description;
	private String payments;
	private String charges;
	private String due;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPayments() {
		return payments;
	}
	public void setPayments(String payments) {
		this.payments = payments;
	}
	public String getCharges() {
		return charges;
	}
	public void setCharges(String charges) {
		this.charges = charges;
	}	
	public String getDue() {
		return due;
	}
	public void setDue(String due) {
		this.due = due;
	}
}
