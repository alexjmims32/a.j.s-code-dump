package com.n2n.enroll.bursar.bo;

import java.util.ArrayList;
import java.util.List;

public class EbillSummaryList {
	private List<EbillSummary> summaryList = new ArrayList<EbillSummary>(0);

	public List<EbillSummary> getSummaryList() {
		return summaryList;
	}

	public void setSummaryList(List<EbillSummary> summaryList) {
		this.summaryList = summaryList;
	}	
}
