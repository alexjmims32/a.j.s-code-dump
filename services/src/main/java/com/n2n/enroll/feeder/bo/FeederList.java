package com.n2n.enroll.feeder.bo;

import java.util.ArrayList;
import java.util.List;

public class FeederList {
	private List<Feeder> feederList = new ArrayList<Feeder>(0);

	public List<Feeder> getFeederList() {
		return feederList;
	}

	public void setFeederList(List<Feeder> feederList) {
		this.feederList = feederList;
	}
	
}
