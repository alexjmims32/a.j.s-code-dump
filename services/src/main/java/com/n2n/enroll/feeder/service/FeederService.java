package com.n2n.enroll.feeder.service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.n2n.enroll.auth.bo.StatusResponse;
import com.n2n.enroll.bursar.service.BursarService;
import com.n2n.enroll.feeder.bo.AboutAreaText;
import com.n2n.enroll.feeder.bo.AdminFeedResponse;
import com.n2n.enroll.feeder.bo.FaqBo;
import com.n2n.enroll.feeder.bo.Feeder;
import com.n2n.enroll.feeder.bo.FeederList;
import com.n2n.enroll.feeder.bo.JAbouttextandFaq;
import com.n2n.enroll.feeder.bo.JAdminFeeder;
import com.n2n.enroll.feeder.bo.JAdminFeederList;
import com.n2n.enroll.feeder.bo.JFaq;
import com.n2n.enroll.feeder.bo.JFaqList;
import com.n2n.enroll.util.AppUtil;
import com.n2n.enroll.util.MyCalendarBuilder;

enum SupportedFeedTypes {
	LINK("LINK"), GDATA("GDATA");

	private final String abbreviation;
	// Reverse-lookup map for getting a day from an abbreviation
	private static final Map<String, SupportedFeedTypes> lookup = new HashMap<String, SupportedFeedTypes>();
	static {
		for (SupportedFeedTypes d : SupportedFeedTypes.values())
			lookup.put(d.getAbbreviation(), d);
	}

	private SupportedFeedTypes(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public static SupportedFeedTypes get(String abbreviation) {
		return lookup.get(abbreviation);
	}
}



@Service
public class FeederService {
	
	@Resource
	SessionFactory sessionFactory;
	
	@Resource
	BursarService bursarService;
	
	protected final Log log = LogFactory.getLog(getClass());

	public FeederList getConfig(int moduleID, int parentID, String campusCode, String moduleCode) {

		FeederList feederList = new FeederList();

		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_FEEDCONFIG");

			sql = AppUtil.format(sql, parentID, moduleID);

			if (moduleID == 0) {
				sql = "SELECT * FROM FEEDS WHERE campuscode = '" + campusCode
						+ "' order by id";
			} else {
				if (campusCode != null && !campusCode.equals("")) {
					sql = sql + " AND campuscode = '" + campusCode + "'";
				}
			}
			
			if(moduleCode != null && !moduleCode.equals("")){
				if (campusCode != null && !campusCode.equals("")) {
					sql = "SELECT * FROM FEEDS WHERE campuscode = '"+ campusCode +"' and moduleid in (select ID from module where code = '" + moduleCode + "' AND CAMPUSCODE = '"+campusCode +"') AND TYPE != 'MENU'";
				}else{
					sql = "SELECT * FROM FEEDS WHERE moduleid in (select ID from module where code = '" + moduleCode + "') AND TYPE != 'MENU'";
				}
			}

			@SuppressWarnings("unchecked")
			List<Feeder> feeders = (List<Feeder>) AppUtil
					.runAQueryCurrentSession(sql, Feeder.class);
			feederList.setFeederList(feeders);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return feederList;

	}

	public JAdminFeederList getAdminConfig(int moduleID, int parentID,
			String campusCode) {

		JAdminFeederList feederList = new JAdminFeederList();
		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_ADMIN_FEEDCONFIG");

			sql = AppUtil.format(sql, parentID, moduleID);

			List<Feeder> feeders = (List<Feeder>) AppUtil
					.runAQueryCurrentSession(sql, Feeder.class);

			for (Feeder feeder : feeders) {
				// Feeder feeder =(Feeder)feeders.get(i);
				JAdminFeeder jFeeder = new JAdminFeeder();
				jFeeder.setId(feeder.getId());
				jFeeder.setParentID(feeder.getParentID());
				jFeeder.setType(feeder.getType());
				jFeeder.setLink(feeder.getLink());
				jFeeder.setFormat(feeder.getFormat());
				jFeeder.setModuleID(feeder.getModuleID());
				jFeeder.setName(feeder.getName());
				jFeeder.setImage(feeder.getIcon());
				jFeeder.setCampusCode(feeder.getCampusCode());
				jFeeder.setLeaf(feeder.getType());
				feederList.getFeederList().add(jFeeder);
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return feederList;
	}

	public String getFeedContent(String feedId, String pageNo, String pageSize) {
		if (pageNo == null || "".equals(pageNo)) {
			pageNo = "1";
		}
		Document doc = null;
		if (pageSize == null || "".equals(pageSize)) {
			try {
				pageSize = AppUtil.getProperty("GDATA_PAGING_PAGESIZE");
			} catch (Exception e) {
				pageSize = "25";
			}
		}

		String sql = null;
		String UrlString = null;
		String output = null;
		String type = null;
		String name = null;
		try {
			sql = AppUtil.getProperty("Q_GET_FEED");
			String displayPastItems = "true";
			Boolean dateFilterFlag = false;
			try {
				displayPastItems = AppUtil.getProperty("DISPLAY_PAST_ITEMS");
			} catch (Exception e) {
				log.error("StackTrace", e);
			}

			sql = AppUtil.format(sql, feedId);

			List<Feeder> l = (List<Feeder>) AppUtil.runAQueryCurrentSession(
					sql, Feeder.class);

			if (l.size() == 0) {
				return null;
			}

			Feeder feeder = (Feeder) l.get(0);
			UrlString = feeder.getLink();
			type = feeder.getType();
			name = feeder.getName();
			if (feeder.getFormat().equalsIgnoreCase("ical")) {
				int startIndex = 0;
				startIndex = Integer.parseInt(pageSize)
						* (Integer.parseInt(pageNo) - 1);
				doc = getICALContent(feedId, startIndex, pageSize);
			} else {
				if (SupportedFeedTypes.get(type) == null) {
					return "";
				}
				if (name != null && name.equalsIgnoreCase("Events")
						&& displayPastItems.equalsIgnoreCase("false")) {
					dateFilterFlag = true;
				}
				int startIndex = 0;
				if (type.equalsIgnoreCase("GDATA")) {
					String gDataOptions = AppUtil
							.getProperty("GDATA_PAGING_OPTIONS");

					startIndex = Integer.parseInt(pageSize)
							* (Integer.parseInt(pageNo) - 1) + 1;

					gDataOptions = AppUtil.format(gDataOptions, startIndex,
							pageSize);

					if (UrlString.contains("?")) {
						UrlString += "&" + gDataOptions;
					} else {
						UrlString += "?" + gDataOptions;
					}
				}
				log.debug(UrlString);
				// open url connection
				URL url = new URL(UrlString);
				HttpURLConnection con = (HttpURLConnection) url
						.openConnection();

				// set up url connection to get retrieve information back
				con.setRequestMethod("GET");
				con.setDoInput(true);

				// pull the information back from the URL
				InputStream is = con.getInputStream();
				StringBuffer buf = new StringBuffer();
				int c;
				while ((c = is.read()) != -1) {
					if (c > 127)
						continue;
					buf.append((char) c);
				}
				con.disconnect();

				output = buf.toString();
				// Replace html sensitive characters
				output.replace("&", "&amp;");

				doc = parseXMLFromString(output, dateFilterFlag, type,
						pageSize, pageNo, startIndex); // parseXMLFromString
			}
			// function removed the
			// empty 'item' nodes in
			// xml content
			// set up a transformer
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();
			output = xmlString;
			// create string from xml tree
			String findPattern = "(\\s+)\\<\\!\\[CDATA\\[";
			String replacePattern = "\\<\\!\\[CDATA\\[";
			output = output.replaceAll(findPattern, replacePattern);
			return output;

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in retriveing feed data" + e.getMessage());
			return "";
		}
	}

	public Document parseXMLFromString(String xml, Boolean dateFilterFlag,
			String type, String pageSize, String pageNo, int startIndex) {
		Document doc = null;
		try {
			// Create a Reader Object
			Reader xmlReader = new StringReader(xml);

			// Create an InputSource
			InputSource is = new InputSource(xmlReader);
			// Get the DocumentBuilder
			DocumentBuilder docBuild = (DocumentBuilder) ((DocumentBuilderFactory) DocumentBuilderFactory
					.newInstance()).newDocumentBuilder();

			doc = (Document) docBuild.parse(is);
			doc.getDocumentElement().normalize();

			String dateFormat = "dd MMM yyyy hh:mm:ss GG";
			try {
				String format = AppUtil.getProperty("PUBLISH_DATE_FORMAT");
				if (!(format == null || format.equals(""))) {
					dateFormat = format;
				}
			} catch (Exception e) {
			}

			SimpleDateFormat fmt = new SimpleDateFormat(dateFormat);

			// This is for processing video list feed from youtube api
			// The total videos count in the rss is held in the property
			// rss.channel.openSearch:totalResults
			// In the sencha touch paging, we are unable to map totalProperty to
			// the above
			// Hence changed the name of the property to total, just eliminating
			// : in the property name
			if (type.equalsIgnoreCase(SupportedFeedTypes.GDATA.toString())) {
				NodeList channelNodes = doc.getElementsByTagName("channel");
				Node totalNode = findNode(channelNodes.item(0),
						"openSearch:totalResults", true, true);
				if (totalNode != null) {
					Node newTotalNode = doc.createElement("total");
					newTotalNode.setTextContent(totalNode.getTextContent());
					channelNodes.item(0).removeChild(totalNode);
					channelNodes.item(0).appendChild(newTotalNode);
				}
			}
			if (type.equalsIgnoreCase(SupportedFeedTypes.GDATA.toString())) {
				NodeList channelNodes = doc.getElementsByTagName("feed");
				Node totalNode = findNode(channelNodes.item(0),
						"openSearch:totalResults", true, true);
				if (totalNode != null) {
					Node newTotalNode = doc.createElement("total");
					newTotalNode.setTextContent(totalNode.getTextContent());
					channelNodes.item(0).removeChild(totalNode);
					channelNodes.item(0).appendChild(newTotalNode);
				}
			}

			NodeList rootNodes = doc.getElementsByTagName("feed");
			for (int i = 0; i < rootNodes.getLength(); i++) {
				Node feedNode = rootNodes.item(i);
				if (feedNode.getNodeType() == Node.ELEMENT_NODE) {
					doc.renameNode(feedNode, feedNode.getNamespaceURI(), "rss");
				}
			}

			NodeList desNodes = doc.getElementsByTagName("content");
			for (int i = 0; i < desNodes.getLength(); i++) {
				Node conNode = desNodes.item(i);
				if (conNode.getNodeType() == Node.ELEMENT_NODE) {
					doc.renameNode(conNode, conNode.getNamespaceURI(),
							"description");
				}
			}

			NodeList entryNodes = doc.getElementsByTagName("entry");
			for (int i = 0; i < entryNodes.getLength(); i++) {
				Node entNode = entryNodes.item(i);
				if (entNode.getNodeType() == Node.ELEMENT_NODE) {
					doc.renameNode(entNode, entNode.getNamespaceURI(), "item");
				}
			}

			NodeList pubNodes = doc.getElementsByTagName("published");
			for (int i = 0; i < pubNodes.getLength(); i++) {
				Node pubNode = pubNodes.item(i);
				if (pubNode.getNodeType() == Node.ELEMENT_NODE) {
					doc.renameNode(pubNode, pubNode.getNamespaceURI(),
							"pubDate");
				}
			}

			NodeList nodes = doc.getElementsByTagName("item");

			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);

				if (!node.hasChildNodes() && "".equals(node.getTextContent())) {
					node.getParentNode().removeChild(node);
					// Decrement the loop counter by 1 as 1 item has been
					// removed
					i--;
					continue;
				}

				// Remove the node from output if there is no title node or
				// title is empty
				Node titleNode = findNode(node, "title", true, true);
				if (titleNode == null) {
					node.getParentNode().removeChild(node);
					// Decrement the loop counter by 1 as 1 item has been
					// removed
					i--;
					continue;
				} else if ("".equals(titleNode.getTextContent().trim())) {
					node.getParentNode().removeChild(node);
					// Decrement the loop counter by 1 as 1 item has been
					// removed
					i--;
					continue;
				}

				if (dateFilterFlag) {
					Node pubDate = findNode(node, "pubDate", true, true);
					if (pubDate != null) {
						String pdate = pubDate.getFirstChild().getTextContent();
						if (pdate.contains("UT")) {
							pdate = pdate + "C";
						}
						try {
							Date pDate = fmt.parse(pdate);
							Date curDate = new Date();
							if (pDate.compareTo(curDate) < 0) {
								node.getParentNode().removeChild(node);
								// Decrement the loop counter by 1 as 1 item has
								// been
								// removed
								i--;
								continue;
							}
						} catch (Exception e) {
							log.error("invalid format");
							log.error("StackTrace", e);
						}
					}
				}
			}

			int limit = Integer.parseInt(pageSize);
			int pageNumber = Integer.parseInt(pageNo);
			int start = (limit * (pageNumber - 1)) + 1;

			int totalRecords = nodes.getLength();
			if (!type.equalsIgnoreCase(SupportedFeedTypes.GDATA.toString())) {
				for (int i = 0; i < start - 1; i++) {
					Node node = nodes.item(0);
					node.getParentNode().removeChild(node);
				}
			}
			for (int i = limit; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				node.getParentNode().removeChild(node);
				i--;
			}

			Node newTotalNode = doc.createElement("total");
			newTotalNode.setTextContent(String.valueOf(totalRecords));
			NodeList channelNodes = doc.getElementsByTagName("channel");
			if(channelNodes.getLength()<1){
				channelNodes = doc.getElementsByTagName("rss");
			}
			channelNodes.item(0).appendChild(newTotalNode);

		} catch (Exception ex) {
			log.error("StackTrace", ex);
		}

		return doc;
	}

	public static Node findNode(Node root, String elementName, boolean deep,
			boolean elementsOnly) {
		// Check to see if root has any children if not return null
		if (!(root.hasChildNodes()))
			return null;

		// Root has children, so continue searching for them
		Node matchingNode = null;
		String nodeName = null;
		Node child = null;

		NodeList childNodes = root.getChildNodes();
		int noChildren = childNodes.getLength();
		for (int i = 0; i < noChildren; i++) {
			if (matchingNode == null) {
				child = childNodes.item(i);
				nodeName = child.getNodeName();
				if ((nodeName != null) & (nodeName.equals(elementName)))
					return child;
				if (deep)
					matchingNode = findNode(child, elementName, deep,
							elementsOnly);
			} else
				break;
		}

		if (!elementsOnly) {
			NamedNodeMap childAttrs = root.getAttributes();
			noChildren = childAttrs.getLength();
			for (int i = 0; i < noChildren; i++) {
				if (matchingNode == null) {
					child = childAttrs.item(i);
					nodeName = child.getNodeName();
					if ((nodeName != null) & (nodeName.equals(elementName)))
						return child;
				} else
					break;
			}
		}
		return matchingNode;
	}

	public JAdminFeederList getFeedModules(String campusCode) {

		JAdminFeederList feederList = new JAdminFeederList();

		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_GET_FEEDMOULES");
			sql = AppUtil.format(sql, campusCode);
			List<Feeder> feeders = (List<Feeder>) AppUtil
					.runAQueryCurrentSession(sql, Feeder.class);
			for (Feeder feeder : feeders) {
				// Feeder feeder =(Feeder)feeders.get(i);
				JAdminFeeder jFeeder = new JAdminFeeder();
				jFeeder.setId(feeder.getId());
				jFeeder.setParentID(feeder.getParentID());
				jFeeder.setType(feeder.getType());
				jFeeder.setLink(feeder.getLink());
				jFeeder.setFormat(feeder.getFormat());
				jFeeder.setModuleID(feeder.getModuleID());
				jFeeder.setName(feeder.getName());
				jFeeder.setImage(feeder.getIcon());
				jFeeder.setCampusCode(feeder.getCampusCode());
				jFeeder.setLeaf(feeder.getType());
				feederList.getFeederList().add(jFeeder);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return feederList;
	}

	public AdminFeedResponse deleteFeed(String id) {

		AdminFeedResponse response = new AdminFeedResponse();
		String sql = "";
		String status = "SUCCESS";
		try {
			sql = AppUtil.getProperty("Q_DELETE_FEEDS");
			sql = AppUtil.format(sql, id);
			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}

	public AdminFeedResponse addOrEditFeeder(String id, String parentID,
			String type, String link, String format, String moduleID,
			String name, String icon, String campusCode, String lastModifiedBy) {

		String status = "SUCCESS";
		String query = "";

		AdminFeedResponse response = new AdminFeedResponse();
		int verssionId = 1;
		try {
			if (!"".equals(id) && id != null) {
				String versionQuery = AppUtil.getProperty("Q_TRACKING_FEEDS");
				versionQuery = AppUtil.format(versionQuery, id);
				List<Feeder> feeders = (List<Feeder>) AppUtil
						.runAQueryCurrentSession(versionQuery, Feeder.class);
				for (Feeder feeder : feeders) {
					String insertTrck = AppUtil
							.getProperty("Q_INSERT_TRACKING_FEEDS");
					insertTrck = AppUtil.format(insertTrck, feeder.getId(),
							feeder.getParentID(), feeder.getType(),
							feeder.getLink(), feeder.getFormat(),
							feeder.getModuleID(),
							feeder.getName().replaceAll("'", "''"),
							feeder.getIcon(), feeder.getCampusCode(),
							feeder.getVersionNo(), feeder.getLastModifiedBy());
					AppUtil.runADeleteQuery(insertTrck);
				}
				verssionId = verssionId + 1;
				query = AppUtil.getProperty("Q_UPDATE_FEED");
				query = AppUtil.format(query, parentID, type, link, format,
						moduleID, name.replaceAll("'", "''"), icon, campusCode,
						verssionId, lastModifiedBy, id);
				response.setStatus(status);
				AppUtil.runADeleteQuery(query);

			} else if (id == null || "".equals(id)) {
				log.debug("add feeder object validated successfully");
				BigDecimal maxFeedId, maxModuleId;

				long feedIdVal = 0, maxModuleIdVal = 0;
				try {
					String qry = "select max(id) from feeds";
					maxFeedId = AppUtil.runACountQuery(qry);
					feedIdVal = maxFeedId.longValue();
					feedIdVal = feedIdVal + 1;
				} catch (Exception e) {
					log.error("StackTrace", e);
				}
				boolean nameCheck = isExist(name);
				if (nameCheck == true) {
					if (moduleID == "" || moduleID.equals("0")) {
						String maxModuleQuery = "select max(moduleid) from feeds";
						maxModuleId = AppUtil.runACountQuery(maxModuleQuery);
						maxModuleIdVal = maxModuleId.longValue();
						maxModuleIdVal = maxModuleIdVal + 1;
						query = AppUtil.getProperty("Q_ADD_FEED");
						query = AppUtil.format(query, feedIdVal, parentID,
								type, link, format, maxModuleIdVal,
								name.replaceAll("'", "''"), icon, campusCode,
								verssionId, lastModifiedBy);
						AppUtil.runADeleteQuery(query);
						response.setStatus(status);
					} else {
						query = AppUtil.getProperty("Q_ADD_FEED");
						query = AppUtil.format(query, feedIdVal, parentID,
								type, link, format, moduleID,
								name.replaceAll("'", "''"), icon, campusCode,
								verssionId, lastModifiedBy);
						AppUtil.runADeleteQuery(query);
						response.setStatus(status);
					}
				} else {
					status = "The feed with this name is already exist";
					response.setStatus(status);
				}
			}

		} catch (Exception e) {
			log.error("Error on adding or editing feed the feed "
					+ e.getMessage());
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public boolean isExist(String title) {
		try {
			String sql = AppUtil.getProperty("Q_GET_EXISTING_FEED");
			sql = AppUtil.format(sql, title);
			List<Feeder> feeders = (List<Feeder>) AppUtil
					.runAQueryCurrentSession(sql, Feeder.class);
			if (feeders.size() == 0) {
				return true;
			}
		} catch (Exception e) {
			log.error("Error in checking isExist function " + e.getMessage());
			log.error("StackTrace", e);
		}
		return false;
	}

	public JAdminFeederList getVersion() {

		JAdminFeederList feederList = new JAdminFeederList();

		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_GET_TRACKING");

			List<Feeder> feeders = (List<Feeder>) AppUtil
					.runAQueryCurrentSession(sql, Feeder.class);
			for (Feeder feeder : feeders) {
				// Feeder feeder =(Feeder)feeders.get(i);
				JAdminFeeder jFeeder = new JAdminFeeder();
				jFeeder.setId(feeder.getId());
				jFeeder.setParentID(feeder.getParentID());
				jFeeder.setType(feeder.getType());
				jFeeder.setLink(feeder.getLink());
				jFeeder.setFormat(feeder.getFormat());
				jFeeder.setModuleID(feeder.getModuleID());
				jFeeder.setName(feeder.getName());
				jFeeder.setImage(feeder.getIcon());
				jFeeder.setCampusCode(feeder.getCampusCode());
				jFeeder.setVersionNo(feeder.getVersionNo());
				jFeeder.setLastModifiedBy(feeder.getLastModifiedBy());
				jFeeder.setLastModifiedOn(feeder.getLastModifiedOn());
				feederList.getFeederList().add(jFeeder);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return feederList;
	}

	public StatusResponse updateModule(String privilegesValues,
			String lastModifiedBy) {
		String status = "";
		StatusResponse statusResponse = new StatusResponse();
		if ((privilegesValues == null || privilegesValues.equals(""))) {
			status = "Invalid update privilege values";
		} else {
			try {

				// String privilegesValues="ENQ,Y,Y;ENTER,Y,N";
				String[] privilegesList = privilegesValues.split("\\;");
				for (String privilegeValue : privilegesList) {

					String[] privilege = privilegeValue.split("\\,");
					String moduleCode = privilege[0];
					String show = privilege[1];
					String authRequired = privilege[2];

					System.out.println(moduleCode + ' ' + show + ' '
							+ authRequired);
					String query = AppUtil.getProperty("Q_UPDATE_MODULE");
					query = AppUtil.format(query, authRequired, show,
							moduleCode);

					AppUtil.runADeleteQuery(query);
				}
				status = "SUCCESS";
				statusResponse.setStatus(status);

			} catch (Exception e) {
				log.error("Error updating modules " + e.getMessage());
				statusResponse.setStatus(e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return statusResponse;
	}

	public StatusResponse createHelpDoc(String campusCode, String areaText,
			String lastModifiedBy) {
		StatusResponse statusResponse = new StatusResponse();
		int versionId = 1;
		String status = "SUCCESS";
		String replaceAreaContent = areaText.replaceAll("'", "''");
		try {

			if (campusCode != null && !(campusCode.equals(""))) {
				String query = AppUtil.getProperty("Q_GET_HELP_CONTENT");
				query = AppUtil.format(query, campusCode);
				List l = AppUtil.runAQuery(query);
				if (l.size() != 0) {
					versionId = versionId + 1;
					String updateQuery = AppUtil
							.getProperty("Q_UPDATE_HELP_CONTENT");
					updateQuery = AppUtil.format(updateQuery,
							replaceAreaContent, campusCode);

					AppUtil.runADeleteQuery(updateQuery);
					statusResponse.setStatus(status);
				} else {

					String verQry = "select max(VERSION_NO) from help";
					BigDecimal maxVersion;
					long idVersion = 0;
					maxVersion = AppUtil.runACountQuery(verQry);
					idVersion = maxVersion.longValue();
					idVersion = idVersion + 1;

					String addQuery = AppUtil.getProperty("Q_INSERT_HELP_TEXT");

					addQuery = AppUtil.format(addQuery, campusCode,
							replaceAreaContent, idVersion, lastModifiedBy);
					AppUtil.runADeleteQuery(addQuery);
					statusResponse.setStatus(status);

				}

			}
		} catch (Exception e) {
			log.error("Error In creating help " + e.getMessage());
			log.error("StackTrace", e);
			status = "FAIL";
		}
		return statusResponse;
	}

	public StatusResponse createTermsAndConditions(String campusCode,
			String conditionText, String lastModifiedBy) {
		StatusResponse statusResponse = new StatusResponse();
		int versionId = 1;
		String status = "SUCCESS";
		String replaceconditionText = conditionText.replaceAll("'", "''");
		try {

			if (campusCode != null && !(campusCode.equals(""))) {
				String query = AppUtil.getProperty("Q_GET_TERM_CONDITIONS");
				query = AppUtil.format(query, campusCode);
				List l = AppUtil.runAQuery(query);
				if (l.size() != 0) {
					versionId = versionId + 1;
					String updateQuery = AppUtil
							.getProperty("Q_UPDATE_TERM_CONDITIONS");
					updateQuery = AppUtil.format(updateQuery,
							replaceconditionText, campusCode);

					AppUtil.runADeleteQuery(updateQuery);
					statusResponse.setStatus(status);
				} else {

					String verQry = "select max(VERSION_NO) from help";
					BigDecimal maxVersion;
					long idVersion = 0;
					maxVersion = AppUtil.runACountQuery(verQry);
					idVersion = maxVersion.longValue();
					idVersion = idVersion + 1;

					String addQuery = AppUtil
							.getProperty("Q_INSERT_TERM_CONDITIONS");

					addQuery = AppUtil.format(addQuery, campusCode,
							replaceconditionText, idVersion, lastModifiedBy);
					AppUtil.runADeleteQuery(addQuery);
					statusResponse.setStatus(status);

				}

			}
		} catch (Exception e) {
			log.error("Error In TermsAndConditions " + e.getMessage());
			log.error("StackTrace", e);
			status = "FAIL";
		}
		return statusResponse;
	}

	
	public StatusResponse createFaq(String id, String campusCode,
			String question, String answer, String lastModifiedBy) {
		StatusResponse statusResponse = new StatusResponse();
		int versionId = 1;
		String status = "SUCCESS";

		String replaceQuestion = question.replaceAll("'", "''");
		String replaceAnswer = answer.replaceAll("'", "''");

		try {

			if (id != null && !(id.equals(""))) {
				versionId = versionId + 1;
				String updateQuery = AppUtil.getProperty("Q_UPDATE_FAQ");
				updateQuery = AppUtil.format(updateQuery, replaceQuestion,
						replaceAnswer, id);
				AppUtil.runADeleteQuery(updateQuery);
				statusResponse.setStatus(status);
			} else {
				String idQry = "select max(id) from faq";
				BigDecimal maxValue;
				long idVal = 0;
				maxValue = AppUtil.runACountQuery(idQry);
				idVal = maxValue.longValue();
				idVal = idVal + 1;

				String verQry = "select max(VERSION_NO) from faq";
				BigDecimal maxVersion;
				long idVersion = 0;
				maxVersion = AppUtil.runACountQuery(verQry);
				idVersion = maxVersion.longValue();
				idVersion = idVersion + 1;

				String addQuery = AppUtil.getProperty("Q_INSERT_FAQ");

				addQuery = AppUtil.format(addQuery, idVal, campusCode,
						replaceQuestion, replaceAnswer, idVersion,
						lastModifiedBy);
				AppUtil.runADeleteQuery(addQuery);
				statusResponse.setStatus(status);
			}
		} catch (Exception e) {
			log.error("Error In creating help " + e.getMessage());
			log.error("StackTrace", e);
			status = "FAIL";
		}
		return statusResponse;
	}

	public JAbouttextandFaq getHelpContent(String campusCode) {
		JAbouttextandFaq areaTextandFaq = new JAbouttextandFaq();
		try {
			String query = AppUtil.getProperty("Q_GET_HELP_CONTENT");
			query = AppUtil.format(query, campusCode);
			List<AboutAreaText> areaTextAndFaq = (List<AboutAreaText>) AppUtil
					.runAQueryCurrentSession(query, AboutAreaText.class);
			for (AboutAreaText about : areaTextAndFaq) {
				areaTextandFaq.setCampusCode(about.getCampusCode());
				areaTextandFaq.setAreaText(about.getAreaText());
			}

		} catch (Exception e) {
			log.error("Error In creating help " + e.getMessage());
			log.error("StackTrace", e);
		}
		return areaTextandFaq;
	}

	public JFaqList getFaqContent(String campusCode) {
		JFaqList list = new JFaqList();
		try {
			String query = AppUtil.getProperty("Q_GET_FAQ_CONTENT");
			query = AppUtil.format(query, campusCode);
			List<FaqBo> faqList = (List<FaqBo>) AppUtil
					.runAQueryCurrentSession(query, FaqBo.class);
			for (FaqBo about : faqList) {
				JFaq faq = new JFaq();
				faq.setCampusCode(about.getCampusCode());
				faq.setQuestion(about.getQuestion());
				faq.setAnswer(about.getAnswer());
				faq.setId(about.getId());
				list.getFaqList().add(faq);
			}

		} catch (Exception e) {
			log.error("Error In creating help " + e.getMessage());
			log.error("StackTrace", e);
		}
		return list;
	}

	public AdminFeedResponse deleteFaq(String id) {
		AdminFeedResponse response = new AdminFeedResponse();
		String sql = "";
		String status = "SUCCESS";
		try {
			sql = AppUtil.getProperty("Q_DELETE_FAQ");
			sql = AppUtil.format(sql, id);
			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
		}
		return response;
	}

	public String getHtmlContent(String UrlString) {

		String output = null;
		try {
			log.debug(UrlString);
			// open url connection
			URL url = new URL(UrlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// set up url connection to get retrieve information back
			con.setRequestMethod("GET");
			con.setDoInput(true);

			// pull the information back from the URL
			InputStream is = con.getInputStream();
			StringBuffer buf = new StringBuffer();
			int c;
			while ((c = is.read()) != -1) {
				if (c > 127)
					continue;
				buf.append((char) c);
			}
			con.disconnect();

			output = buf.toString();
			return output;

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in retriveing feed data" + e.getMessage());
			return "";
		}
	}

	public Document getICALContent(String feedId, int startIndex,
			String pageSize) {
		String output = null;
		Document doc = null;
		try {
			int limit = Integer.parseInt(pageSize) + startIndex;
			SimpleDateFormat dateParse = new SimpleDateFormat(
					"yyyyMMdd'T'HHmmss'Z'");
			dateParse.setTimeZone(TimeZone.getTimeZone("GMT"));
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"EEE, d MMM yyyy HH:mm:ss z");
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
			Element element = doc.createElement("rss");
			doc.appendChild(element);
			Element rootElement = doc.createElement("channel");
			element.appendChild(rootElement);
			String sql = null;
			String UrlString = null;
			sql = AppUtil.getProperty("Q_GET_FEED");
			sql = AppUtil.format(sql, feedId);
			List<Feeder> l = (List<Feeder>) AppUtil.runAQueryCurrentSession(
					sql, Feeder.class);
			if (l.size() == 0) {
				return null;
			}
			Feeder feeder = (Feeder) l.get(0);
			UrlString = feeder.getLink();
			URL url = new URL(UrlString);
			InputStreamReader inputStream = new InputStreamReader(
					url.openStream());
			StringBuffer buf = new StringBuffer();
			int c;
			while ((c = inputStream.read()) != -1) {
				if (c > 127)
					continue;
				buf.append((char) c);
			}
			output = buf.toString();
			output.replace("&", "&amp;");
			MyCalendarBuilder builder = new MyCalendarBuilder();
			Calendar calendar = builder.build(new StringReader(output));
			int count = 1;
			for (Iterator i = calendar.getComponents().iterator(); i.hasNext();) {
				Component component = (Component) i.next();
				Boolean isPastEvent = false;
				Element node = doc.createElement("item");
				rootElement.appendChild(node);
				for (Iterator j = component.getProperties().iterator(); j
						.hasNext();) {
					Property property = (Property) j.next();
					String atrValue = "";
					String atrName = property.getName().toString();
					atrName = atrName.toLowerCase();
					if (atrName.equalsIgnoreCase("DTSTAMP")
							|| atrName.equalsIgnoreCase("DTSTART")
							|| atrName.equalsIgnoreCase("DTEND")) {
						if (atrName.equalsIgnoreCase("DTSTART")) {
							atrName = "pubDate";
						}
						try {
							Date date = dateParse.parse(property.getValue()
									.toString());
							if (atrName == "pubDate" && new Date().after(date)) {
								isPastEvent = true;
							}
							atrValue = dateFormat.format(date).toString();
						} catch (Exception e) {
							atrValue = property.getValue();
						}
					} else {
						atrValue = property.getValue();
					}
					if (atrName.equalsIgnoreCase("CATEGORIES")) {
						atrName = "category";
					} else if (atrName.equalsIgnoreCase("SUMMARY")) {
						atrName = "title";
					} else if (atrName.equalsIgnoreCase("UID")) {
						atrName = "guid";
					}
					atrValue = atrValue.trim();
					Element elementNode = doc.createElement(atrName);
					elementNode.appendChild(doc.createTextNode(atrValue));
					node.appendChild(elementNode);
				}
				if (isPastEvent) {
					node.getParentNode().removeChild(node);
					count--;
				}
				if (count <= startIndex && !isPastEvent) {
					node.getParentNode().removeChild(node);
				}
				count++;
				if (count > limit) {
					return doc;
				}
			}
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return doc;
	}

	public String getLatestFeedContent(String category, String pageNo,
			String pageSize) {

		pageNo = "1";

		if (pageSize == null || "".equals(pageSize)) {
			pageSize = "4";
		}

		String sql = null;
		String UrlString = null;
		String output = null;
		String type = null;
		String name = null;
		try {
			String displayPastItems = "true";
			Boolean dateFilterFlag = false;
			if (category.equals("NEWS")) {
				UrlString = "http://www.spsu.edu/_resources/rss/news.xml";
			} else if (category.equals("EVENTS")) {
				UrlString = "http://calendar.spsu.edu/webcache/v1.0/rssDays/7/list-rss/no--filter.rss";
			} else if (category.equals("JOBS")) {
				UrlString = "https://employment.spsu.edu/all_jobs.atom";
			} else if (category.equals("SPORTS")) {
				UrlString = "http://www.spsuhornets.com/rss.php?categoryID=1";
			}

			log.debug(UrlString);
			// open url connection
			URL url = new URL(UrlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// set up url connection to get retrieve information back
			con.setRequestMethod("GET");
			con.setDoInput(true);

			// pull the information back from the URL
			InputStream is = con.getInputStream();
			StringBuffer buf = new StringBuffer();
			int c;
			while ((c = is.read()) != -1) {
				if (c > 127)
					continue;
				buf.append((char) c);
			}
			con.disconnect();

			output = buf.toString();

			// Replace html sensitive characters
			output.replace("&", "&amp;");
			int startIndex = 0;
			pageNo = "1";
			if (pageSize == "" || pageSize == null) {
				pageSize = "4";
			}
			Document doc = parseXMLFromString(output, dateFilterFlag, type,
					pageSize, pageNo, startIndex); // parseXMLFromString
			// function removed the
			// empty 'item' nodes in
			// xml content
			// set up a transformer
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();
			output = xmlString;

			String findPattern = "(\\s+)\\<\\!\\[CDATA\\[";
			String replacePattern = "\\<\\!\\[CDATA\\[";
			output = output.replaceAll(findPattern, replacePattern);

			// Just throw the output from the URL
			return output;

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in retriveing feed data" + e.getMessage());
			return "";
		}
	}
}