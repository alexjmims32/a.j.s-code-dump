package com.n2n.enroll.feeder.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "AboutAreaText")
public class AboutAreaText implements Serializable {
	@Column(name = "CAMPUSCODE")
	private String campusCode;
	@Id
	@Column(name = "ABOUTTEXT")
	private String areaText;
	@Column(name = "LASTMODIFIEDBY")
	private String lastModifiedBy;
	@Column(name = "LASTMODIFIEDON")
	private String lastModifiedON;
	@Column(name = "VERSION_NO")
	private String versionNo;

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedON() {
		return lastModifiedON;
	}

	public void setLastModifiedON(String lastModifiedON) {
		this.lastModifiedON = lastModifiedON;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public String getAreaText() {
		return areaText;
	}

	public void setAreaText(String areaText) {
		this.areaText = areaText;
	}

}
