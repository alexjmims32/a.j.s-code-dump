package com.n2n.enroll.feeder.bo;

import java.util.ArrayList;
import java.util.List;

public class JAdminFeederList {
	private List<JAdminFeeder> feederList = new ArrayList<JAdminFeeder>(0);

	public List<JAdminFeeder> getFeederList() {
		return feederList;
	}

	public void setFeederList(List<JAdminFeeder> feederList) {
		this.feederList = feederList;
	}
	
}
