package com.n2n.enroll.feeder.bo;

import java.util.ArrayList;
import java.util.List;

public class JFaqList {
	private List<JFaq> faqList = new ArrayList<JFaq>(0);

	public List<JFaq> getFaqList() {
		return faqList;
	}

	public void setFaqList(List<JFaq> faqList) {
		this.faqList = faqList;
	}
}
