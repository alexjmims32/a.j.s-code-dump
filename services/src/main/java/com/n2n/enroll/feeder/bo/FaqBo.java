package com.n2n.enroll.feeder.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "FaqBo")
public class FaqBo implements Serializable {
	@Id
	@Column(name = "ID")
	private String id;
	@Column(name = "CAMPUSCODE")
	private String campusCode;

	@Column(name = "QUESTION")
	private String question;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ANSWER")
	private String answer;
	@Column(name = "LASTMODIFIEDBY")
	private String lastModifiedBy;
	@Column(name = "LASTMODIFIEDON")
	private String lastModifiedON;
	@Column(name = "VERSION_NO")
	private String versionNo;

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedON() {
		return lastModifiedON;
	}

	public void setLastModifiedON(String lastModifiedON) {
		this.lastModifiedON = lastModifiedON;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

}
