package com.n2n.enroll.feeder.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Feeder {

	@Id

	private int id;
	@Column(name = "PARENTID")
	private int parentID;
	@Column(name = "TYPE")
	private String type;
	@Column(name = "LINK")
	private String link;
	@Column(name = "FORMAT")
	private String format;
	@Column(name = "MODULEID")
	private int moduleID;
	@Column(name = "NAME")
	private String name;
	@Column(name = "ICON")
	private String icon;
	@Column(name = "CAMPUSCODE")
	private String campusCode;
	@Column(name = "VERSION_NO")
	private String versionNo;
	@Column(name = "LASTMODIFIEDBY")
	private String lastModifiedBy;
	@Column(name = "LASTMODIFIEDON")
	private String lastModifiedOn;

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getParentID() {
		return parentID;
	}

	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getModuleID() {
		return moduleID;
	}

	public void setModuleID(int moduleID) {
		this.moduleID = moduleID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
