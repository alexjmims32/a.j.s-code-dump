package com.n2n.enroll.feedback.bo;

import java.util.List;

public class JServiceArea {

	private String id;
	private String area;
	private String supervisor;
	private String assocDean;
	private String dean;
	private String hr;
	private String provost;

	private List<JServiceArea> subAreas;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getAssocDean() {
		return assocDean;
	}

	public void setAssocDean(String assocDean) {
		this.assocDean = assocDean;
	}

	public String getDean() {
		return dean;
	}

	public void setDean(String dean) {
		this.dean = dean;
	}

	public String getHr() {
		return hr;
	}

	public void setHr(String hr) {
		this.hr = hr;
	}

	public String getProvost() {
		return provost;
	}

	public void setProvost(String provost) {
		this.provost = provost;
	}

	public List<JServiceArea> getSubAreas() {
		return subAreas;
	}

	public void setSubAreas(List<JServiceArea> subAreas) {
		this.subAreas = subAreas;
	}
}
