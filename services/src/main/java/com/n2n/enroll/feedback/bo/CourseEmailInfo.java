package com.n2n.enroll.feedback.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CourseEmailInfo implements Serializable {

	private static final long serialVersionUID = -2444840817790957417L;
	
	@Id
	@Column(name = "PIDM")
	private String id;
	
	@Column(name = "FACULTY_NAME")
	private String facultyName;
	
	@Column(name = "FACULTY_EMAIL")
	private String facultyEmail;
	
	@Column(name = "TERM_CODE")
	private String termcode;

	@Column(name = "CRN")
	private String crn;

	@Column(name = "SUBJECT")
	private String subject;

	@Column(name = "COURSE_NUMB")
	private String crseNumber;

	@Column(name = "PRIMARY_IND")
	private String primaryInd;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFacultyName() {
		return facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

	public String getFacultyEmail() {
		return facultyEmail;
	}

	public void setFacultyEmail(String facultyEmail) {
		this.facultyEmail = facultyEmail;
	}

	public String getTermcode() {
		return termcode;
	}

	public void setTermcode(String termcode) {
		this.termcode = termcode;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCrseNumber() {
		return crseNumber;
	}

	public void setCrseNumber(String crseNumber) {
		this.crseNumber = crseNumber;
	}

	public String getPrimaryInd() {
		return primaryInd;
	}

	public void setPrimaryInd(String primaryInd) {
		this.primaryInd = primaryInd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
