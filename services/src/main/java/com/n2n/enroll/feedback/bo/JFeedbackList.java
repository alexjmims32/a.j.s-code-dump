package com.n2n.enroll.feedback.bo;

import java.util.ArrayList;
import java.util.List;

public class JFeedbackList {
	private List<JFeedback> feedback = new ArrayList<JFeedback>(0);

	public List<JFeedback> getFeedback() {
		return feedback;
	}

	public void setFeedback(List<JFeedback> feedback) {
		this.feedback = feedback;
	}

}
