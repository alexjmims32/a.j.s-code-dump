package com.n2n.enroll.feedback.bo;

public class FeedbackResponse {
	private String status = "";

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
