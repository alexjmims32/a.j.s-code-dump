package com.n2n.enroll.feedback.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ServiceArea implements Serializable {

	private static final long serialVersionUID = -2444840817790957417L;
	
	@Id
	@Column(name = "ID")
	private String id;
	
	@Column(name = "area")
	private String area;
	
	@Column(name = "subarea")
	private String subArea;
	
	@Column(name = "supervisor")
	private String supervisor;

	@Column(name = "assocdean")
	private String assocDean;

	@Column(name = "dean")
	private String dean;

	@Column(name = "hr")
	private String hr;

	@Column(name = "provost")
	private String provost;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSubArea() {
		return subArea;
	}

	public void setSubArea(String subArea) {
		this.subArea = subArea;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getAssocDean() {
		return assocDean;
	}

	public void setAssocDean(String assocDean) {
		this.assocDean = assocDean;
	}

	public String getDean() {
		return dean;
	}

	public void setDean(String dean) {
		this.dean = dean;
	}

	public String getHr() {
		return hr;
	}

	public void setHr(String hr) {
		this.hr = hr;
	}

	public String getProvost() {
		return provost;
	}

	public void setProvost(String provost) {
		this.provost = provost;
	}

}
