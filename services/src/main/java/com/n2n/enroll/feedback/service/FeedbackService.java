package com.n2n.enroll.feedback.service;

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import javax.mail.Message.RecipientType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;
import org.springframework.stereotype.Service;

import com.n2n.enroll.feedback.bo.AdminFeedback;
import com.n2n.enroll.feedback.bo.CourseEmailInfo;
import com.n2n.enroll.feedback.bo.FeedbackResponse;
import com.n2n.enroll.feedback.bo.JFeedback;
import com.n2n.enroll.feedback.bo.JFeedbackList;
import com.n2n.enroll.feedback.bo.JServiceArea;
import com.n2n.enroll.feedback.bo.ServiceArea;
import com.n2n.enroll.util.AppUtil;

@Service
public class FeedbackService {

	protected final Log log = LogFactory.getLog(getClass());

	@SuppressWarnings("unchecked")
	public String getServiceAreaContacts(String areaId) {

		String email = "";

		try {
			String sql = AppUtil.getProperty("Q_GET_SERVICE_AREA_EMAILS");
			
			sql = AppUtil.format(sql, areaId);
			
			List<ServiceArea> sList = (List<ServiceArea>) AppUtil
					.runAQueryCurrentSession(sql, ServiceArea.class);

			if (sList.size() > 0) {
				ServiceArea area = sList.get(0);
				if(area.getAssocDean()!=null && !((area.getAssocDean()).equals(""))){
					email = area.getAssocDean();
				}
				
				if(area.getDean()!=null && !((area.getDean()).equals(""))){
					if(email!=null && !(email.equals(""))){
						email = email + ",";
					}
					
					email = email + area.getDean();
				}
				
				if(area.getHr()!=null && !((area.getHr()).equals(""))){
					if(email!=null && !(email.equals(""))){
						email = email + ",";
					}
					
					email = email + area.getHr();
				}
				
				if(area.getProvost()!=null && !((area.getProvost()).equals(""))){
					if(email!=null && !(email.equals(""))){
						email = email + ",";
					}
					
					email = email + area.getProvost();
				}
				
				if(area.getSupervisor()!=null && !((area.getSupervisor()).equals(""))){
					if(email!=null && !(email.equals(""))){
						email = email + ",";
					}
					
					email = email + area.getSupervisor();
				}
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
		}

		return email;
	}
	
	public String getCourseContacts(String termCode,String crn) {

		String email = "";

		try {
			String sql = AppUtil.getProperty("Q_GET_COURSE_EMAILS");
			
			sql = AppUtil.format(sql, termCode,crn);
			
			List<CourseEmailInfo> sList = (List<CourseEmailInfo>) AppUtil
					.runAQueryCurrentSession(sql, CourseEmailInfo.class);

			if (sList.size() > 0) {
				CourseEmailInfo area = sList.get(0);
				email = area.getFacultyEmail();
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
		}

		return email;
	}

	public void saveFeedbackRecord(String areaId, String courseId,
			String comments1, String comments2, String confidential,
			String senderName, String senderEmail) {

		try {
			String sql = AppUtil.getProperty("Q_INSERT_FEEDBACK_DATA");
			
			sql = AppUtil.format(sql, areaId, courseId, comments1, comments2,
					confidential, senderName, senderEmail);
			
                        

			AppUtil.runADeleteQuery(sql);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
	}

	public String sendEmail(String fromAddress, String fromName,
			String toAddress, String toName, String subject, String body1,
			String body2) {
		String status = "";
		try {
			Email email = new Email();
			String emailHost = AppUtil.getProperty("EMAIL_HOST");
			String emailPort = AppUtil.getProperty("EMAIL_PORT");

			email.setFromAddress(fromName, fromAddress);
			email.setReplyToAddress(fromName, fromAddress);
			email.setSubject(subject);

			String[] toAdd = toAddress.split(",");

			String mailboxUserName = AppUtil.getProperty("EMAIL_MAILBOX_LOGIN");
			String mailboxPassword = AppUtil
					.getProperty("EMAIL_MAILBOX_PASSWORD");

			Mailer mailer = new Mailer(emailHost, Integer.parseInt(emailPort),
					mailboxUserName, mailboxPassword,
					TransportStrategy.SMTP_TLS);

			email.setTextHTML("<html> <body>" + body1 + "</body> </html>");

			// Add To address
			for (int i = 0; i < toAdd.length; i++) {

				// Email Will be of the form Name:Email
				String nameAndEmail = toAdd[i].toString();
				String[] temp = nameAndEmail.split(":");
				String name = "";
				String emailId = "";
				if (temp.length > 1) {
					name = temp[0];
					emailId = temp[1];
				} else {
					name = temp[0];
					emailId = temp[0];
				}
				email.addRecipient(name, emailId, RecipientType.TO);

				mailer.validate(email);
			}

			mailer.sendMail(email);

			status = "SUCCESS";

		} catch (Exception ex) {
			status = "Unable to send email";
			log.error("StackTrace", ex);
		}
		return status;
	}

	@SuppressWarnings("unchecked")
	public JFeedbackList getList() {
		JFeedbackList feedbackList = new JFeedbackList();
		try {
			String sql = AppUtil.getProperty("Q_GET_FEEDBACK");
			sql = AppUtil.format(sql);
			List<AdminFeedback> feedbacks = (List<AdminFeedback>) AppUtil
					.runAQueryCurrentSession(sql, AdminFeedback.class);
			for (AdminFeedback role : feedbacks) {
				JFeedback feedback = new JFeedback();
				feedback.setId(role.getId());
				feedback.setTitle(role.getTitle());
				feedback.setEmail(role.getEmail());
				feedback.setDescription(role.getDescription());
				feedbackList.getFeedback().add(feedback);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Registration terms......"
					+ e.getMessage());
		}
		return feedbackList;
	}

	public FeedbackResponse addOrEditFeedback(String id, String title,
			String email, String description) {
		String status = "";
		String query = null;
		FeedbackResponse feedbackResponse = new FeedbackResponse();
		

		String replaceName = title.replaceAll("'", "''");
		try {
			if (id != null && !(id.equals(""))) {
				query = AppUtil.getProperty("Q_UPDATE_USER_FEEDBACK");
				query = (AppUtil.format(query, replaceName, email, description, id))
						;
				AppUtil.runADeleteQuery(query);

			} else if (id == null || id.equals("")) {
				query = AppUtil.getProperty("Q_INSERT_FEEDBACK");
				String qry = "select max(id) from feedback";
				BigDecimal maxId;
				long idVal = 0;
				maxId = AppUtil.runACountQuery(qry);
				idVal = maxId.longValue();
				idVal = idVal + 1;
				query = (AppUtil.format(query, replaceName, email, description))
						;
				

				AppUtil.runADeleteQuery(query);
			}
			status = "SUCCESS";
			feedbackResponse.setStatus(status);

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in add or update query " + e.getMessage());
			feedbackResponse.setStatus(e.getMessage());
		}
		return feedbackResponse;
	}

	public FeedbackResponse deleteAdminContact(String id) {

		FeedbackResponse response = new FeedbackResponse();
		String sql = "";
		String status = "SUCCESS";
		try {
			sql = AppUtil.getProperty("Q_DELETE_FEEDBACK");
			sql = AppUtil.format(sql, id);
			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	public List<JServiceArea> getServiceAreas() {

		List<JServiceArea> response = null;

		try {
			String sql = AppUtil.getProperty("Q_GET_SERVICE_AREAS");
			List<ServiceArea> dbAreas = AppUtil.runAQueryCurrentSession(sql,
					ServiceArea.class);

			HashMap<String, List<JServiceArea>> hList = new HashMap<String, List<JServiceArea>>();

			List<JServiceArea> areaList = new ArrayList<JServiceArea>();

			for (ServiceArea sa : dbAreas) {

				// If Sub Area field is null, its the main area
				// Add them into areasList
				if (sa.getSubArea() == null) {
					JServiceArea jsa = new JServiceArea();
					jsa.setId(sa.getId());
					jsa.setArea(sa.getArea());
					jsa.setAssocDean(sa.getAssocDean());
					jsa.setDean(sa.getDean());
					jsa.setHr(sa.getHr());
					jsa.setProvost(sa.getProvost());

					areaList.add(jsa);
					hList.put(sa.getArea(), null);
				} else {
					// If SubArea field is not null, its a sub area
					// Add it into the HashMap, main area name will be the key
					// for this item
					JServiceArea jsa = new JServiceArea();
					jsa.setId(sa.getId());
					jsa.setArea(sa.getSubArea());
					jsa.setAssocDean(sa.getAssocDean());
					jsa.setDean(sa.getDean());
					jsa.setHr(sa.getHr());
					jsa.setProvost(sa.getProvost());

					List<JServiceArea> subAreaList = hList.get(sa.getArea());
					// If this is the first sub area under this area,
					// instantiate the list and then add sub area
					if (subAreaList == null) {
						subAreaList = new ArrayList<JServiceArea>();
					}
					subAreaList.add(jsa);

					hList.put(sa.getArea(), subAreaList);
				}
			}

			// Now we have the list of areas in areasList and
			// list of sub areas in a hash map hList with main area name as key
			// Loop through these and add the sub areas list into the main area
			// object
			response = new ArrayList<JServiceArea>();
			for (JServiceArea jsa : areaList) {
				for (String area : hList.keySet()) {
					if (jsa.getArea().equals(area)) {
						jsa.setSubAreas(hList.get(area));
						response.add(jsa);
					}
				}
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return response;
	}

	public String getDefaultFeedbackToEmail() {

		try {
			return AppUtil.getProperty("DEFAULT_FEEDBACK_TO_EMAIL");
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return "";
	}
}