package com.n2n.enroll.ldap;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.directory.SearchControls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ldap.control.SortControlDirContextProcessor;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.LikeFilter;
import org.springframework.ldap.filter.NotFilter;
import org.springframework.ldap.filter.OrFilter;

import com.n2n.enroll.util.AppUtil;

public class PersonDao {

	@Resource
	LdapTemplate ldapTemplate;

	protected final Log log = LogFactory.getLog(getClass());

	private static HashMap<String, String> roleMap = null;

	public ContextMapper getContextMapper() {
		return new PersonContextMapper();
	}

	public List<LdapContact> findByName(String firstName, String lastName,
			String category) {

		AndFilter filter = new AndFilter();
		String objectclass = "";
		try {
			objectclass = AppUtil.getProperty("LDAP_OBJECT_CLASS");
		} catch (Exception e) {

		}
		if (!(objectclass == null || "".equals(objectclass))) {
			filter.and(new EqualsFilter("objectclass", objectclass));
		}
		String ldapCatergory = "";
		try {
			ldapCatergory = AppUtil.getProperty("LDAP_CATAGORY_PROPERTY");
		} catch (Exception e) {

		}
		if (!(category == null || "".equals(category) || category.equals("all")
				|| ldapCatergory == null || "".equals(ldapCatergory))) {
			if (category.equals("student")) {
				String stdProperty = "";
				try {
					stdProperty = AppUtil.getProperty("LDAP_CATAGORY_STUDENT");
				} catch (Exception e) {

				}
				if (!(stdProperty == null || stdProperty.equals(""))) {
					String[] stdPropertis = stdProperty.split(",");
					if (stdPropertis.length == 1) {
						filter.and(new EqualsFilter(ldapCatergory, stdProperty));
					} else {
						OrFilter or = new OrFilter();
						for (int i = 0; i < stdPropertis.length; i++) {
							or.or(new EqualsFilter(ldapCatergory,
									stdPropertis[i]));
						}
						filter.and(or);
					}
				}

			} else {
				String notfactProperty = "", factProperty = "";
				try {
					factProperty = AppUtil.getProperty("LDAP_CATAGORY_FACULTY");
				} catch (Exception e) {

				}
				if (factProperty == null || factProperty.equals("")) {
					try {
						notfactProperty = AppUtil
								.getProperty("LDAP_CATAGORY_NOT_FACULTY");
					} catch (Exception e) {

					}
					if (!(notfactProperty == null || notfactProperty.equals(""))) {
						String[] stdPropertis = notfactProperty.split(",");
						if (stdPropertis.length == 1) {
							NotFilter not = new NotFilter(new EqualsFilter(
									ldapCatergory, notfactProperty));
							filter.and(not);
						} else {
							OrFilter or = new OrFilter();
							for (int i = 0; i < stdPropertis.length; i++) {
								or.or(new EqualsFilter(ldapCatergory,
										stdPropertis[i]));
							}
							NotFilter not = new NotFilter(or);
							filter.and(not);
						}
					}
				} else {
					String[] factProperties = factProperty.split(",");
					if (factProperties.length == 1) {
						filter.and(new EqualsFilter(ldapCatergory, factProperty));
					} else {
						OrFilter or = new OrFilter();
						for (int i = 0; i < factProperties.length; i++) {
							or.or(new EqualsFilter(ldapCatergory,
									factProperties[i]));
						}
						filter.and(or);
					}
				}
			}
		}

		AndFilter and = new AndFilter();
		OrFilter or = new OrFilter();
		if (firstName == null || "".equals(firstName)) {
			and.and(new LikeFilter("sn", "*" + lastName + "*"));
		} else if (lastName == null || "".equals(lastName)) {
			firstName = firstName.trim();
			String[] resName = firstName.split("\\s+");
			if (resName.length == 1) {
				String nameSearchType = "";
				try {
					nameSearchType = AppUtil.getProperty("NAME_SEARCH_TYPE");
				} catch (Exception e) {

				}
				if ("ALL".equalsIgnoreCase(nameSearchType)) {
					lastName = firstName;
					firstName = lastName;
					or.or(new LikeFilter("givenName", "*" + firstName + "*"));
					or.or(new LikeFilter("sn", "*" + lastName + "*"));
					filter.and(or);
				} else
					and.and(new LikeFilter("givenName", "*" + firstName + "*"));
			} else {
				String firstNameParam = resName[0];
				String lastNameParam = resName[1];
				firstName = firstNameParam;
				lastName = lastNameParam;
				// Split first name
				// Take first 2 words
				// now firstnameparam = 1st word
				// lastnamenameparam = 2nd word
				//
				// ( AND FILTER
				// 1 - firstname like firstnameparam
				// 2 - lastname like lastnameparam
				// )
				// OR
				// ( AND FILTER
				// 1 - lastname like firstnameparam
				// 2 - firstname like lastnameparam
				// )
				and.and(new LikeFilter("givenName", "*" + firstName + "*"));
				and.and(new LikeFilter("sn", "*" + lastName + "*"));
			}
		} else {
			and.and(new LikeFilter("givenName", "*" + firstName + "*"));
			and.and(new LikeFilter("sn", "*" + lastName + "*"));
		}
		filter.and(and);

		String ldapGroupSearch = "false";
		try {
			ldapGroupSearch = AppUtil.getProperty("LDAP_SEARCH_GROUP");
		} catch (Exception e) {
			log.error("StackTrace", e.fillInStackTrace());
		}
		DistinguishedName searchInGroup = DistinguishedName.EMPTY_PATH;

		if ("true".equals(ldapGroupSearch)) {

			String searchInGroupStr = "";
			try {
				searchInGroupStr = AppUtil.getProperty("LDAP_FIRSTGROUP_TYPE")
						+ "=";

				if (category.equals("student")) {
					searchInGroupStr += AppUtil
							.getProperty("LDAP_GROUP_STUDENT");
				} else {
					searchInGroupStr += AppUtil
							.getProperty("LDAP_GROUP_FACULTY");
				}

				String firstGrp = AppUtil.getProperty("LDAP_FIRSTGROUP_TYPE");
				if (!(firstGrp == null || "".equals(firstGrp))) {
					searchInGroupStr += ","
							+ AppUtil.getProperty("LDAP_FIRSTGROUP_TYPE") + "=";
					searchInGroupStr += AppUtil
							.getProperty("LDAP_FIRSTGROUP_NAME");
				}

				searchInGroup = new DistinguishedName(searchInGroupStr);
				ldapTemplate.setIgnorePartialResultException(true);
				List<LdapContact> l = ldapTemplate.search(searchInGroup,
						filter.encode(), getContextMapper());

				String secondGrp = AppUtil.getProperty("LDAP_SECONDGROUP_TYPE");
				if (!(secondGrp == null || "".equals(secondGrp))
						&& !category.equals("student")) {
					searchInGroupStr = AppUtil
							.getProperty("LDAP_SECONDGROUP_TYPE") + "=";
					searchInGroupStr += AppUtil
							.getProperty("LDAP_SECONDGROUP_NAME");
					searchInGroup = new DistinguishedName(searchInGroupStr);
					l.addAll(ldapTemplate.search(searchInGroup,
							filter.encode(), getContextMapper()));
				}
				return l;

			} catch (Exception e) {
				log.error("StackTrace", e.fillInStackTrace());
			}
		}
		ldapTemplate.setIgnorePartialResultException(true);

		String additionalFilters = null;
		try {
			additionalFilters = AppUtil.getProperty("LDAP_ADDITIONAL_FILTERS");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String finalFilter = filter.encode();
		if (additionalFilters != null && !("".equals(additionalFilters))) {
			finalFilter = "(&(" + finalFilter + ")(" + additionalFilters + "))";
		}

		SortControlDirContextProcessor requestControl;

		// Prepare for first search
		requestControl = new SortControlDirContextProcessor("cn");

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        
		List<LdapContact> l = ldapTemplate.search(searchInGroup, finalFilter, searchControls,
				getContextMapper(), requestControl);

		return l;
	}

	public LdapContact getUser(String uid) {
		String userProp = "uid";
		try {
			userProp = AppUtil.getProperty("LDAP_USERID_PROPERTY");
		} catch (Exception e) {
		}
		if (userProp == null || "".equals(userProp)) {
			userProp = "uid";
		}
		EqualsFilter f = new EqualsFilter(userProp, uid);
		List result = ldapTemplate.search(DistinguishedName.EMPTY_PATH,
				f.encode(), getContextMapper());

		if (result.size() != 1) {
			throw new RuntimeException("User not found or not unique");
		}

		return (LdapContact) result.get(0);
	}

	private static class PersonContextMapper implements ContextMapper {

		public Object mapFromContext(Object ctx) {

			DirContextAdapter context = (DirContextAdapter) ctx;
			LdapContact c = new LdapContact();
			c.setFullDn(context.getStringAttribute("distinguishedName"));
			String fullName = "";
			try {
				fullName = AppUtil.getProperty("LDAP_FULL_NAME");
			} catch (Exception e) {

			}
			if (!(fullName == null || "".equals(fullName))) {
				c.setFullName(context.getStringAttribute(fullName));
			} else {
				c.setFullName(context.getStringAttribute("cn"));
			}
			c.setFirstName(context.getStringAttribute("givenName"));
			c.setLastName(context.getStringAttribute("sn"));
			c.setEmail(context.getStringAttribute("mail"));
			c.setOrganizationalStatus(context
					.getStringAttribute("organizationalStatus"));
			c.setPhone(context.getStringAttribute("telephoneNumber"));
			c.setMemberOf(context.getStringAttributes("memberOf"));
			String bannerIdProp = "pdsBannerID";
			try {
				bannerIdProp = AppUtil.getProperty("LDAP_BANNERPIDM_PROPERTY");
			} catch (Exception e) {
			}
			String pdsBannerID = context.getStringAttribute(bannerIdProp);
			if (pdsBannerID != null) {
				String[] arr = pdsBannerID.split(":", 2);
				c.setBannerId(arr[0]);
			}
			c.setDepartment(context.getStringAttribute("department"));
			c.setTitle(context.getStringAttribute("title"));
			c.setuId(context.getStringAttribute("uid"));

			String roleProperty = "organizationalStatus";
			try {
				roleProperty = AppUtil.getProperty("LDAP_ROLE_PROPERTY");
			} catch (Exception e) {
			}

			String roles[] = context.getStringAttributes(roleProperty);
			String role = "";

			HashMap<String, String> roleMap = PersonDao.getRoleMap();
			if (roles != null) {
				for (int i = 0; i < roles.length; i++) {
					String ldapRole = roles[i];
					if (roleMap.containsKey(ldapRole)) {
						role = role + "," + roleMap.get(ldapRole);
					}
				}
			}
			role = role.replaceFirst(",", "");

			if (role != null) {
				role = role.toUpperCase();
				String[] temp = role.split("\\.");
				if (temp.length > 1) {
					role = temp[1];
				} else {
					role = temp[0];
				}
			}

			if (role == null || "".equals(role)) {
				try {
					role = AppUtil.getProperty("LDAP_DEFAULT_ROLE");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			c.setRole(role);

			return c;
		}
	}

	private static HashMap<String, String> getRoleMap() {
		if (PersonDao.roleMap == null) {
			PersonDao.roleMap = new HashMap<String, String>();
			String roleMapStr = "";
			try {
				roleMapStr = AppUtil.getProperty("LDAP_ROLE_MAPPING");
			} catch (Exception e) {
				e.printStackTrace();
			}
			String roleMapArr[] = roleMapStr.split(",");
			for (int i = 0; i < roleMapArr.length; i++) {
				String ldapRole = roleMapArr[i].split(":")[0];
				String appRole = roleMapArr[i].split(":")[1];
				PersonDao.roleMap.put(ldapRole, appRole);
			}
			return PersonDao.roleMap;
		} else {
			return PersonDao.roleMap;
		}
	}
}