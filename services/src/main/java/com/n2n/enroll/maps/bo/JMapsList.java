package com.n2n.enroll.maps.bo;

import java.util.ArrayList;
import java.util.List;

public class JMapsList {
	
	private String status;
	private List<JMap> map = new ArrayList<JMap>(0);
	private List<JFilter> filter= new ArrayList<JFilter>(0);
	
	public List<JFilter> getFilter() {
		return filter;
	}

	public void setFilter(List<JFilter> filter) {
		this.filter = filter;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<JMap> getMap() {
		return map;
	}

	public void setMap(List<JMap> map) {
		this.map = map;
	}
}
