package com.n2n.enroll.maps.bo;

import java.util.ArrayList;
import java.util.List;

public class RoleList {
	private List<RoleDropDown> campuses = new ArrayList<RoleDropDown>(0);

	public List<RoleDropDown> getCampuses() {
		return campuses;
	}

	public void setCampuses(List<RoleDropDown> campuses) {
		this.campuses = campuses;
	}

}
