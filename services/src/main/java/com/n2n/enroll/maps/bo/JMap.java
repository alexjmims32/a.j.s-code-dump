package com.n2n.enroll.maps.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "maps")
public class JMap implements Serializable {

	private static final long serialVersionUID = 5066046790850914589L;

	@Id
	@Column(name = "id")
	private long id;
	@GeneratedValue
	@Column(name = "CAMPUS_CODE")
	private String campusCode;
	@Column(name = "BUILDING_CODE")
	private String buildingCode;
	@Column(name = "BUILDING_NAME")
	private String buildingName;
	@Column(name = "BUILDING_DESC")
	private String buildingDesc;
	@Column(name = "PHONE")
	private String phone;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "IMG_URL")
	private String imgUrl;
	@Column(name = "LONGITUDE")
	private String longitude;
	@Column(name = "LATITUDE")
	private String latitude;
	@Column(name = "TITLE")
	private String title;
	@Column(name = "VERSION_NO")
	private String versionNo;
	@Column(name = "LASTMODIFIEDBY")
	private String lastModifiedBy;
	@Column(name = "LASTMODIFIEDON")
	private String lastModifiedOn;

	@Column(name = "PRINCIPALNAME")
	private String principal;
	@Column(name = "WEBSITE")
	private String website;
	@Column(name = "ADDRESS")
	private String address;
	@Column(name = "CENTER_FLAG")
	private String centerFlag;
	@Column(name = "CATEGORY")
	private String category;
	

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCenterFlag() {
		return centerFlag;
	}

	public void setCenterFlag(String centerFlag) {
		this.centerFlag = centerFlag;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedOn() {
		return lastModifiedOn;
	}

	public void setLastModifiedOn(String lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getBuildingCode() {
		return buildingCode;
	}

	public void setBuildingCode(String buildingCode) {
		this.buildingCode = buildingCode;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getBuildingDesc() {
		return buildingDesc;
	}

	public void setBuildingDesc(String buildingDesc) {
		this.buildingDesc = buildingDesc;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
}
