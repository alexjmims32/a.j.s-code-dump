package com.n2n.enroll.maps.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "category_filter")
public class JFilter implements Serializable {

	@Id
	@Column(name = "id")
	private long id;
	@Column(name = "CATEGORY")
	private String category;
	@Column(name = "SHOW_BY_DEFAULT")
	private String showByDefault;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getShowByDefault() {
		return showByDefault;
	}

	public void setShowByDefault(String showByDefault) {
		this.showByDefault = showByDefault;
	}

}
