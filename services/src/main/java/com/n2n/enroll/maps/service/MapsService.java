package com.n2n.enroll.maps.service;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.n2n.enroll.maps.bo.JAdminMap;
import com.n2n.enroll.maps.bo.JFilter;
import com.n2n.enroll.maps.bo.JMap;
import com.n2n.enroll.maps.bo.MapsResponse;
import com.n2n.enroll.maps.bo.RoleDropDown;
import com.n2n.enroll.maps.bo.RoleList;
import com.n2n.enroll.maps.bo.XMap;
import com.n2n.enroll.util.AppUtil;

@Service
@SuppressWarnings("unchecked")
public class MapsService {

	@Resource
	SessionFactory sessionFactory;

	protected final Log log = LogFactory.getLog(getClass());

	public List<JMap> getAll(String campusCode) {

		String query = null;
		List<JMap> l = null;
		try {
			query = AppUtil.getProperty("Q_GET_MAPS");
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting getAll query " + e.getMessage());
		}

		if ((campusCode != null || "".equals(campusCode))) {
			query += " WHERE campus_code = '" + campusCode + "'";
		}

		query += " order by id";

		try {
			l = AppUtil.runAQueryCurrentSession(query, JMap.class);
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving mapsList " + e.getMessage());
		}

		return l;
	}

	public MapsResponse updateOrCreate(XMap xMap) {
		String status = "SUCCESS";
		String query = null;
		MapsResponse response = new MapsResponse();
		int versionId = 1;

		String repalceBuildingName = xMap.getBuildingName().replaceAll("'",
				"''");
		String repalceBuildingDesc = xMap.getBuildingDesc().replaceAll("'",
				"''");

		try {
			if (!(xMap.getId().equals(""))) {
				String versionQuery = AppUtil
						.getProperty("Q_GET_TRACKING_MAPS");
				versionQuery = AppUtil.format(versionQuery, xMap.getId());

				List<JAdminMap> maps = (List<JAdminMap>) AppUtil
						.runAQueryCurrentSession(versionQuery, JAdminMap.class);
				for (JAdminMap map : maps) {
					if (map.getBuildingDesc() == null) {
						map.setBuildingDesc("");
					}
					if (map.getBuildingCode() == null) {
						map.setBuildingCode("");
					}
					String insertTrck = AppUtil
							.getProperty("Q_INSERT_TRACKING_MAPS");
					insertTrck = AppUtil.format(insertTrck, String.valueOf(map
							.getId()), map.getCampusCode(), map
							.getBuildingCode(), map.getBuildingName()
							.replaceAll("'", "''"), map.getBuildingDesc()
							.replaceAll("'", "''"), map.getPhone(), map
							.getEmail(), map.getImgUrl(), map.getLongitude(),
							map.getLatitude(), map.getVersionNo(), map
									.getLastModifiedBy(), map.getAddress(), map
									.getCategory());
					AppUtil.runADeleteQuery(insertTrck);
				}
				versionId = versionId + 1;
				query = AppUtil.getProperty("Q_UPDATE_MAPS");
				query = AppUtil.format(query, xMap.getCampusCode(),
						xMap.getBuildingCode(), repalceBuildingName,
						repalceBuildingDesc, xMap.getPhone(), xMap.getEmail(),
						xMap.getImgUrl(), xMap.getLongitude(),
						xMap.getLatitude(), String.valueOf(versionId),
						xMap.getLastModifiedBy(), xMap.getAddress(),
						xMap.getCategory(), xMap.getId());
				AppUtil.runADeleteQuery(query);
				response.setStatus(status);
			} else if (xMap.getId().equals("")) {

				query = AppUtil.getProperty("Q_ADD_MAPS");
				String qry = "select max(id) from maps";
				BigDecimal maxId;
				long idVal = 0;
				maxId = (BigDecimal) AppUtil.runACountQuery(qry);
				idVal = maxId.longValue();
				idVal = idVal + 1;
				log.info("Address:   " + xMap.getAddress());
				query = AppUtil.format(query, String.valueOf(idVal),
						xMap.getCampusCode(), xMap.getBuildingCode(),
						repalceBuildingName, repalceBuildingDesc,
						xMap.getPhone(), xMap.getEmail(), xMap.getImgUrl(),
						xMap.getLongitude(), xMap.getLatitude(),
						xMap.getAddress(), xMap.getCategory());
				log.info("inser query:   " + query);

				AppUtil.runADeleteQuery(query);
				response.setStatus(status);
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in add or update maps query " + e.getMessage());
			response.setStatus(e.getMessage());
		}
		return response;
	}

	public MapsResponse deleteMap(String id) {

		MapsResponse response = new MapsResponse();
		String sql = "";
		String status = "SUCCESS";
		try {
			sql = AppUtil.getProperty("Q_DELETE_MAP");
			sql = AppUtil.format(sql, id);
			AppUtil.runADeleteQuery(sql);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}

	public RoleList getRoles(String campusCode) {
		RoleList roles = new RoleList();
		try {
			String sql = AppUtil.getProperty("Q_GET_CAMPUS_CODES");
			sql = AppUtil.format(sql, campusCode);
			List l = AppUtil.runAQuery(sql);
			for (Object obj : l) {
				Object[] termfields = (Object[]) obj;
				RoleDropDown role = new RoleDropDown();
				role.setCode(AppUtil.getStringValue(termfields[0]));
				role.setDescription(AppUtil.getStringValue(termfields[1]));
				roles.getCampuses().add(role);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Registration terms......"
					+ e.getMessage());
		}
		return roles;
	}

	public List<JFilter> getCategoryFilters() {

		String query = null;
		List<JFilter> l = null;
		try {
			query = AppUtil.getProperty("Q_GET_CATEGORY_FILTERS");
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting category filter query "
					+ e.getMessage());
		}

		try {
			l = AppUtil.runAQueryCurrentSession(query, JFilter.class);
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving category filter list "
					+ e.getMessage());
		}

		return l;
	}
}