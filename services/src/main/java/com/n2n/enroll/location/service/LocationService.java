package com.n2n.enroll.location.service;

import java.net.URL;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
@Service
public class LocationService {
	@Resource
	SessionFactory sessionFactory;

	protected final Log log = LogFactory.getLog(getClass());

	public String validateAddress(String city,String state,String zip) throws Exception{
		String status="Failed";
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new URL("http://www.webservicex.net/uszip.asmx/GetInfoByCity?USCity=" + city).openStream());
        NodeList nodeLst = doc.getElementsByTagName("Table");
        for (int s = 0; s < nodeLst.getLength(); s++) {

            Node fstNode = nodeLst.item(s);
            
            if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
          
              Element fstElmnt = (Element) fstNode;
              NodeList fstNmElmntLst = fstElmnt.getElementsByTagName("STATE");
              Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
              NodeList fstNm = fstNmElmnt.getChildNodes();
              String stateElm = ((Node) fstNm.item(0)).getNodeValue();
              NodeList lstNmElmntLst = fstElmnt.getElementsByTagName("ZIP");
              Element lstNmElmnt = (Element) lstNmElmntLst.item(0);
              NodeList lstNm = lstNmElmnt.getChildNodes();
              String zipElm =((Node) lstNm.item(0)).getNodeValue();
              if(state.equalsIgnoreCase(stateElm) && zip.equalsIgnoreCase(zipElm)){
            	  status="Success";
            	  return status;
              }
            }

          }
		
		return status;
	}
}