package com.n2n.enroll.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.notif.bo.JNotificationLog;
import com.n2n.enroll.notif.bo.JNotificationSearchList;
import com.n2n.enroll.notif.bo.JSearchByNameList;
import com.n2n.enroll.notif.bo.XNotificationList;
import com.n2n.enroll.notif.service.NotificationService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class NotificationController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	NotificationService notificationService;

	@RequestMapping("/notificationList")
	public ResponseEntity<String> notificationList(
			@RequestParam(value = "maxId", required = false, defaultValue = "") String maxId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.debug("Notification List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("Retrieving notificationsList for user:" + username);
			try {
				username = userService.getNotificationsIdentifier(username);
				XNotificationList notifications = notificationService.list(
						username, maxId);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(notifications));

			} catch (Exception e) {
				log.error("Error In Retrieving Notificationlist : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/adminNotificationList")
	public ResponseEntity<String> adminNotificationList(
			@RequestParam(value = "maxId", required = false, defaultValue = "") String maxId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.debug("Notification List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("Retrieving notificationsList for user:" + username);
			try {

				XNotificationList notifications = notificationService
						.adminNotificationList(username, maxId);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(notifications));

			} catch (Exception e) {
				log.error("Error In Retrieving Notificationlist : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/notification/markAsRead")
	public ResponseEntity<String> markAsRead(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Notifications mark as read Request from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			try {

				String id = request.getParameter("id");

				if (id != null && !id.equals("")) {

					String status = notificationService.updateReadFlag(id, 1);

					nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
				}

			} catch (Exception e) {
				log.error("Error In updating read flag: " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/notification/markAsUnread")
	public ResponseEntity<String> markAsUnread(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Notifications mark as unread Request from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			try {

				String id = request.getParameter("id");

				if (id != null && !id.equals("")) {

					String status = notificationService.updateReadFlag(id, 0);

					nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
				}

			} catch (Exception e) {
				log.error("Error In updating read flag: " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/notification/delete")
	public ResponseEntity<String> notificationDelete(
			@RequestParam(value = "id", required = true) String id,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Notifications delete Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			try {
				String status = notificationService.updeDeletedFlag(id, "1");
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In retrieving deleted flag : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/addNotification")
	public ResponseEntity<String> addNotification(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Notifications add Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		JNotificationLog jNotificationLog = new JNotificationLog();
		jNotificationLog.setUserName(username);
		jNotificationLog.setMessage(request.getParameter("message"));
		jNotificationLog.setExpiryDate(request.getParameter("expirydate"));
		jNotificationLog.setDeliveryMethod("PUSH");
		jNotificationLog.setDelivered("0");
		jNotificationLog.setReadFlag("0");
		jNotificationLog.setLastModifiedBy(username);
		jNotificationLog.setLastModifiedOn("");
		jNotificationLog.setDeleted("0");
		jNotificationLog.setType(request.getParameter("type"));
		jNotificationLog.setTitle(request.getParameter("title"));
		jNotificationLog.setDueDate(request.getParameter("dueDate"));

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				String status = notificationService
						.addNotification(jNotificationLog);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In retrieving add notification request : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/addAdminNotification")
	public ResponseEntity<String> addAdminNotification(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Notifications add Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		JNotificationLog jNotificationLog = new JNotificationLog();
		jNotificationLog.setUserName(request.getParameter("to"));
		jNotificationLog.setMessage(request.getParameter("message"));
		jNotificationLog.setExpiryDate(request.getParameter("expirydate"));
		jNotificationLog.setDeliveryMethod("PUSH");
		jNotificationLog.setDelivered("0");
		jNotificationLog.setReadFlag("0");
		jNotificationLog.setLastModifiedBy(username);
		jNotificationLog.setDeleted("0");
		jNotificationLog.setType(request.getParameter("type"));
		jNotificationLog.setTitle(request.getParameter("title"));
		jNotificationLog.setDueDate(request.getParameter("dueDate"));

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				String status = notificationService
						.addAdminNotification(jNotificationLog);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In retrieving add notification request : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/searchByName")
	public ResponseEntity<String> search(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("notification Search Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String category = "STUDENT";
		String pageOffset = request.getParameter("page");
		String pageSize = request.getParameter("limit");

		try {
			JNotificationSearchList searhContacts = notificationService
					.searchBanner(firstName, lastName, pageOffset, pageSize,
							category);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(searhContacts));
		} catch (Exception e) {
			log.error("Error in Retrieving notification Search "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		// }
		return nResponse;
	}

	@RequestMapping("/searchByAll")
	public ResponseEntity<String> searchAll(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("notification Search Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String name = request.getParameter("name");
		String pageOffset = request.getParameter("page");
		String pageSize = request.getParameter("limit");
		String campusCode = request.getParameter("campusCode");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");

		try {
			JSearchByNameList searhContacts = notificationService
					.searchAll(name, pageOffset, pageSize, campusCode,
							firstName, lastName);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(searhContacts));
		} catch (Exception e) {
			log.error("Error in Retrieving notification Search "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		// }
		return nResponse;
	}

	
	@RequestMapping("/searchByQuery")
	public ResponseEntity<String> searchByQuery(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("notification Search Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String query = request.getParameter("query");
		String pageOffset = request.getParameter("page");
		String pageSize = request.getParameter("limit");

		try {
			JNotificationSearchList searhContacts = notificationService
					.searchByQuery(query, pageOffset, pageSize);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(searhContacts));
		} catch (Exception e) {
			log.error("Error in Retrieving notification Search "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		// }
		return nResponse;
	}

	@RequestMapping("/registerDevice")
	public ResponseEntity<String> registerDevice(
			@RequestParam(value = "regId", required = true) String deviceId,
			@RequestParam(value = "platform", required = true) String platform,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Register Device Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		ResponseEntity<String> nResponse = createJSONResponse("");

		if (deviceId == null || deviceId.isEmpty() || platform == null
				|| platform.isEmpty()) {
			nResponse = createErrorResponse("Invalid request data");
		} else {
			try {
				if (username != null) {
					username = userService.getNotificationsIdentifier(username);
				}
				boolean regStatus = notificationService.registerDevice(
						username, deviceId, platform);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(regStatus));
			} catch (Exception e) {
				log.error("Error Registering device " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

}
