package com.n2n.enroll.json;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.contact.bo.Contacts;
import com.n2n.enroll.contact.bo.EmergencyContactResponse;
import com.n2n.enroll.contact.bo.EnumListResponse;
import com.n2n.enroll.contact.bo.JConfirmBioInfoResponse;
import com.n2n.enroll.contact.bo.JUpdateInfo;
import com.n2n.enroll.contact.bo.PersonList;
import com.n2n.enroll.contact.service.ContactService;
import com.n2n.enroll.ldap.PersonDao;
import com.n2n.enroll.util.AppUtil;

@Controller
public class ContactsController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	ContactService contactService;

	@Resource
	PersonDao personDao;

	@RequestMapping("/universityContacts")
	public ResponseEntity<String> get(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("EmergencyContacts Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		// String authHeader = request.getHeader("Authorization");

		String campusCode = request.getParameter("campusCode");
		String category = request.getParameter("category");
		String searchString = request.getParameter("searchString");

		try {
			Contacts contacts = contactService.getUniversityContacts(
					campusCode, category, searchString);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(contacts));
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving EmergencyContacts " + e.getMessage());
		}
		return nResponse;
	}

	@RequestMapping("/editEmergencyContacts")
	public ResponseEntity<String> editEmergencyContacts(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info(" Edit EmergencyContacts Request from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		// String authHeader = request.getHeader("Authorization");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		String address = request.getParameter("address");
		String email = request.getParameter("email");
		String pictureUrl = request.getParameter("pictureUrl");
		String campusCode = request.getParameter("campusCode");
		String lastModifiedBy = request.getParameter("lastModifiedBy");
		String category = request.getParameter("category");
		String comments = request.getParameter("comments");
		try {
			EmergencyContactResponse status = contactService
					.addOrEditEmergecyContact(id, name, phone, campusCode,
							address, email, pictureUrl, lastModifiedBy,
							category, comments);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in add or edit emergency contact "
					+ e.getMessage());
		}
		return nResponse;
	}

	@RequestMapping("/searchContact")
	public ResponseEntity<String> search(
			@RequestParam(value = "firstName", required = true) String firstName,
			@RequestParam(value = "lastName", required = true) String lastName,
			@RequestParam(value = "category", defaultValue = "STUDENT", required = false) String category,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("PersonContacts Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String pageOffset = request.getParameter("page");
		String pageSize = request.getParameter("limit");
		String start = request.getParameter("start");
		try {
			PersonList searhContacts = contactService.search(firstName,
					lastName, category, pageOffset, pageSize, start);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(searhContacts));
		} catch (Exception e) {
			log.error("Error in Retrieving PersonContacts " + e.getMessage());
			log.error("StackTrace", e);
		}
		// }
		return nResponse;
	}

	@RequestMapping("/getContact")
	public ResponseEntity<String> get(
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "studentId", required = false) String studentId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("PersonContacts Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				if(id == null || id == ""){
					id = studentId;
				}
				else
				id = userService.getBannerId(id);
				PersonList personContacts = contactService.getContact(id);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(personContacts));
				// PersonBo s = personDao.findByDn(id);
				// nResponse = createJSONResponse(AppUtil
				// .getJSONFrom(s));
			} catch (Exception e) {
				log.error("Error in Retrieving PersonContacts "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/deleteContact")
	public ResponseEntity<String> deleteAdminContact(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("contact deletion Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String id = request.getParameter("id");
		String sequence = request.getParameter("sequenceNumber");

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				EmergencyContactResponse status = contactService
						.deleteAdminContact(sequence, id);

				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In deleting feed request : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/contactUp")
	public ResponseEntity<String> contactUp(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("contactUp Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		String id = request.getParameter("id");
		String sequence = request.getParameter("sequenceNumber");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			EmergencyContactResponse status = contactService.moveContactUp(id,
					sequence);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
			try {
			} catch (Exception e) {
				log.error("Error in Retrieving PersonContacts "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/contactDown")
	public ResponseEntity<String> contactDown(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("contactDown Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		String id = request.getParameter("id");
		String sequence = request.getParameter("sequenceNumber");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			EmergencyContactResponse status = contactService.moveContactDown(
					id, sequence);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
			try {
			} catch (Exception e) {
				log.error("Error in Retrieving PersonContacts "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}
	
	@RequestMapping("/getCountyList")
	public ResponseEntity<String> getEnums(
			@RequestParam(value = "enumType", required = false) String enumType,
			@RequestParam(value = "stateCode", required = false) String stateCode,
			HttpServletRequest request, HttpServletResponse response)	
			throws Exception {

		log.info("Search Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String username = "";
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null && !(authHeader.equals(""))) {
			username = userService.authenticateUser(authHeader);

			if (username == null) {
				nResponse = unAuthorized();
				return nResponse;
			}
		}

		try {
			nResponse = createJSONResponse("");

			EnumListResponse enumList = contactService.getCountyList(enumType,stateCode);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(enumList));

		} catch (Exception e) {
			log.error("Error executing query: " + e.getMessage());
			log.error("StackTrace", e);
			nResponse = createErrorResponse(e.getMessage());
		}

		return nResponse;
	}
	
	@RequestMapping("/getEnums")
	public ResponseEntity<String> getEnums(
			@RequestParam(value = "enumType", required = false) String enumType,
			HttpServletRequest request, HttpServletResponse response)	
			throws Exception {

		log.info("Search Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String username = "";
		String authHeader = request.getHeader("Authorization");
		if (authHeader != null && !(authHeader.equals(""))) {
			username = userService.authenticateUser(authHeader);

			if (username == null) {
				nResponse = unAuthorized();
				return nResponse;
			}
		}

		try {
			nResponse = createJSONResponse("");

			EnumListResponse enumList = contactService.getEnums(enumType);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(enumList));

		} catch (Exception e) {
			log.error("Error executing query: " + e.getMessage());
			log.error("StackTrace", e);
			nResponse = createErrorResponse(e.getMessage());
		}

		return nResponse;
	}

	@RequestMapping(value = "/bioContact")
	public ResponseEntity<String> bioContact(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "role", required = true) String role,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("biographic Info request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				studentId = userService.getBannerId(studentId);
//				studentId = "900019523";
				String bioContact = contactService.bioContact(studentId, role);
				nResponse = createXMLResponse(bioContact);
			} catch (Exception e) {
				log.error("Error in retrieving biographic Info : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/updateBioInfo")
	public ResponseEntity<String> updateBioInfo(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "role", required = true) String role,
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "sequenceNo", required = false) String sequenceNo,
			@RequestParam(value = "delAddr", required = false) String delAddr,
			@RequestParam(value = "dateFmt", required = false) String dateFmt,
			@RequestParam(value = "tDate", required = false) String tDate,
			@RequestParam(value = "houseNo", required = false) String houseNo,
			@RequestParam(value = "str1", required = false) String str1,
			@RequestParam(value = "str2", required = false) String str2,
			@RequestParam(value = "str3", required = false) String str3,
			@RequestParam(value = "str4", required = false) String str4,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "zip", required = false) String zip,
			@RequestParam(value = "county", required = false) String county,
			@RequestParam(value = "nation", required = false) String nation,
			@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "area", required = false) String area,
			@RequestParam(value = "primaryNo", required = false) String primaryNo,
			@RequestParam(value = "primaryExt", required = false) String primaryExt,
			@RequestParam(value = "pAccess", required = false) String pAccess,
			@RequestParam(value = "unlist", required = false) String unlist,
			@RequestParam(value = "seqList", required = false) String seqList,
			@RequestParam(value = "phCodeList", required = false) String phCodeList,
			@RequestParam(value = "countryList", required = false) String countryList,
			@RequestParam(value = "areaList", required = false) String areaList,
			@RequestParam(value = "phList", required = false) String phList,
			@RequestParam(value = "phExtList", required = false) String phExtList,
			@RequestParam(value = "intAccess", required = false) String intAccess,
			@RequestParam(value = "unlistInd", required = false) String unlistInd,
			@RequestParam(value = "delInd", required = false) String delInd,
			@RequestParam(value = "pRelation", required = false) String pRelation,
			@RequestParam(value = "order", required = false) String order,
			@RequestParam(value = "lastNamePfx", required = false) String lastNamePfx,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "pmi", required = false) String pmi,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("update biographic Contacts Request from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				studentId = userService.getBannerId(studentId);
				JUpdateInfo updateInfo = contactService.updateBioInfo(
						studentId, role, type, sequenceNo, delAddr, dateFmt,
						tDate, houseNo, str1, str2, str3, str4, city, state,
						zip, county, nation, country, area, primaryNo,
						primaryExt, pAccess, unlist, seqList, phCodeList,
						countryList, areaList, phList, phExtList, intAccess,
						unlistInd, delInd, pRelation, order, lastNamePfx,
						lastName, firstName, pmi);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(updateInfo));
			} catch (Exception e) {
				log.error("Error in updating biographic info : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping(value = "/confirmBioInfo")
	public ResponseEntity<String> confirmBioInfo(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "type", required = true) String type,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Confirm biographic Info request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				studentId = userService.getBannerId(studentId);
				JConfirmBioInfoResponse confirmBioInfo = contactService
						.confirmBioInfo(studentId, type);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(confirmBioInfo));
			} catch (Exception e) {
				log.error("Error in confirming BioInfo : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}
}
