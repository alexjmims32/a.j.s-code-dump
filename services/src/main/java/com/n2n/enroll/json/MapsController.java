package com.n2n.enroll.json;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.maps.bo.JFilter;
import com.n2n.enroll.maps.bo.JMap;
import com.n2n.enroll.maps.bo.JMapsList;
import com.n2n.enroll.maps.bo.MapsResponse;
import com.n2n.enroll.maps.bo.RoleList;
import com.n2n.enroll.maps.bo.XMap;
import com.n2n.enroll.maps.service.MapsService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class MapsController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	MapsService mapsService;

	protected final Log log = LogFactory.getLog(getClass());

	@RequestMapping("/maps")
	public ResponseEntity<String> get(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");

		log.info("Maps List Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String campusCode = request.getParameter("campusCode");
		String status = "";
		try {
			List<JMap> jMapList = mapsService.getAll(campusCode);
			List<JFilter> jFilterList = mapsService.getCategoryFilters();
			JMapsList mapsList = new JMapsList();
			mapsList.setMap(jMapList);
			mapsList.setFilter(jFilterList);
			status = "success";
			mapsList.setStatus(status);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(mapsList));

		} catch (Exception e) {
			log.error("Error in Retrieving mapsList " + e.getMessage());
			log.error("StackTrace", e);
		}
		return nResponse;
	}

	@RequestMapping("/updateMaps")
	public ResponseEntity<String> update(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");

		log.info("addorUpdateMaps Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		XMap xMap = new XMap();
		xMap.setCampusCode(request.getParameter("campusCode"));
		xMap.setBuildingCode(request.getParameter("buildingCode"));
		xMap.setBuildingName(request.getParameter("buildingName"));
		xMap.setBuildingDesc(request.getParameter("buildingDesc"));
		xMap.setPhone(request.getParameter("phone"));
		xMap.setEmail(request.getParameter("email"));
		xMap.setImgUrl(request.getParameter("imgUrl"));
		xMap.setLongitude(request.getParameter("longitude"));
		xMap.setLatitude(request.getParameter("latitude"));
		xMap.setLastModifiedBy(request.getParameter("lastModifiedBy"));
		xMap.setId(request.getParameter("id"));
		xMap.setAddress(request.getParameter("address"));
		xMap.setCategory(request.getParameter("category"));
		try {
			MapsResponse status = mapsService.updateOrCreate(xMap);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

		} catch (Exception e) {
			log.error("Error in Retrieving mapsList " + e.getMessage());
			log.error("StackTrace", e);
		}
		return nResponse;
	}

	@RequestMapping("/deleteMap")
	public ResponseEntity<String> deleteMap(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("contact deletion Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String id = request.getParameter("id");

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				MapsResponse status = mapsService.deleteMap(id);

				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In deleting feed request : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping(value = "/getCampusDropDown")
	public ResponseEntity<String> getRoleDropDown(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("CampusDropDown Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String campusCode = request.getParameter("campusCode");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				RoleList xRoles = mapsService.getRoles(campusCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(xRoles));
			} catch (Exception e) {
				log.error("Error in retrieving CampusDropDown List : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		log.info("Completed CampusDropDown request");
		return nResponse;
	}

	@RequestMapping(value = "/getCategory")
	public ResponseEntity<String> getCategory(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("getCategory Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				List<JFilter> xRoles = mapsService.getCategoryFilters();
				nResponse = createJSONResponse(AppUtil.getJSONFrom(xRoles));
			} catch (Exception e) {
				log.error("Error in retrieving getCategory List : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		log.info("Completed getCategory request");
		return nResponse;
	}
}
