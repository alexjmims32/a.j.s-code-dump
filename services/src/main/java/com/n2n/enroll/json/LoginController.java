package com.n2n.enroll.json;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.n2n.enroll.auth.bo.JAdminUser;
import com.n2n.enroll.auth.bo.JModule;
import com.n2n.enroll.auth.bo.JUserList;
import com.n2n.enroll.auth.bo.LoginServiceResponse;
import com.n2n.enroll.auth.bo.LoginStatus;
import com.n2n.enroll.auth.bo.PrivilegesResponse;
import com.n2n.enroll.auth.bo.RoleAndPrivilegeResponse;
import com.n2n.enroll.auth.bo.StatusResponse;
import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.notif.service.NotificationService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class LoginController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	NotificationService notificationService;

	@RequestMapping("/adminlogin")
	public ResponseEntity<String> adminLogin(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Login Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");

		String username = userService.authenticateAdminUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			log.info("Login through " + userService.getAuthSource()
					+ " successful: " + username);

			try {

				LoginServiceResponse loginresponse = userService
						.loadUserByUsername(username);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(loginresponse));

			} catch (Exception e) {
				log.error("Error Retrieving User Details: " + e.getMessage());
				nResponse = unAuthorized();
			}
		}
		log.debug("Completed login request");
		return nResponse;
	}

	@RequestMapping("/login")
	public ResponseEntity<String> login(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Login Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		LoginStatus status = userService.authenticateUser(authHeader, true);
		String username=status.getUserName();
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			log.info("Login through " + userService.getAuthSource()
					+ " successful: " + username);

			try {
				LoginServiceResponse loginresponse = userService
						.loadUserByUsername(username, status.getRole());
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(loginresponse));

				// If the login request is from Phonegap wrapped native app
				// Register the device for push notifications
				String regId = request.getParameter("regId");
				String platform = request.getParameter("platform");
				if (regId != null && !regId.trim().isEmpty()
						&& platform != null && !platform.trim().isEmpty()) {
					username = userService.getNotificationsIdentifier(username);
					notificationService.registerDevice(username, regId,
							platform);
				}

			} catch (Exception e) {
				log.error("Error Retrieving User Details: " + e.getMessage());
				log.error("StackTrace", e);
				nResponse = unAuthorized();
			}
		}
		log.debug("Completed login request");
		return nResponse;
	}

	@RequestMapping("/getMainModules")
	public ResponseEntity<String> getAll(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Modules Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		try {

			String campusCode = request.getParameter("campusCode");

			List<JModule> l = userService.getMainModules(campusCode);
			HashMap<String, Object> map = new HashMap<String, Object>(0);
			map.put("status", "SUCCESS");
			map.put("modules", l);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(map));

		} catch (Exception e) {
			log.error("Error Retrieving module details: " + e.getMessage());
			nResponse = createErrorResponse(e.getMessage());
		}
		return nResponse;
	}

	@RequestMapping("/getModules")
	public ResponseEntity<String> getModules(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("modules Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		try {

			String campusCode = request.getParameter("campusCode");

			List<JModule> l = userService.getModules(campusCode);
			HashMap<String, Object> map = new HashMap<String, Object>(0);
			map.put("status", "SUCCESS");
			map.put("modules", l);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(map));

		} catch (Exception e) {
			log.error("Error Retrieving module details: " + e.getMessage());
			nResponse = createErrorResponse(e.getMessage());
		}
		return nResponse;
	}

	@RequestMapping("/getPrivileges")
	public ResponseEntity<String> getPrivilege(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("privileges Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		String roleId = request.getParameter("roleId");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("Login through " + userService.getAuthSource()
					+ " successful: " + username);
			try {

				PrivilegesResponse pResponse = userService
						.getPrivileges(roleId);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(pResponse));

			} catch (Exception e) {
				log.error("Error Retrieving get privilege details: "
						+ e.getMessage());
				nResponse = unAuthorized();
			}
		}
		log.info("Completed get privilege details");
		return nResponse;

	}

	@RequestMapping("/getPrivilege")
	public ResponseEntity<String> getAdminPrivilege(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("privileges Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);
		String roleId = request.getParameter("roleId");
		String campusCode = request.getParameter("campusCode");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("Login through " + userService.getAuthSource()
					+ " successful: " + username);
			try {

				RoleAndPrivilegeResponse pResponse = userService
						.getAdminPrivilege(roleId, campusCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(pResponse));

			} catch (Exception e) {
				log.error("Error Retrieving get privilege details: "
						+ e.getMessage());
				nResponse = unAuthorized();
			}
		}
		log.info("Completed get privilege details");
		return nResponse;

	}

	@RequestMapping("/updatePrivilege")
	public ResponseEntity<String> updatePrivilege(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("update privileges Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String updateValue = request.getParameter("updateValue");
		String lastModifiedBy = request.getParameter("lastModifiedBy");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("Login through " + userService.getAuthSource()
					+ " successful: " + username);
			try {

				StatusResponse pResponse = userService.updatePrivilege(
						updateValue, lastModifiedBy);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(pResponse));

			} catch (Exception e) {
				log.error("Error Retrieving update privilege details: "
						+ e.getMessage());
				nResponse = unAuthorized();
			}
		}
		log.info("Completed update privilege details");
		return nResponse;
	}

	@RequestMapping("/addUser")
	public ResponseEntity<String> addUser(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Users add Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		JAdminUser user = new JAdminUser();
		user.setFirstName(request.getParameter("firstName"));
		user.setLastName(request.getParameter("lastName"));
		user.setUsername(request.getParameter("username"));
		user.setRole(request.getParameter("role"));
		user.setActive(request.getParameter("active"));
		user.setPassword(request.getParameter("password"));

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("user validation completed.");
			try {
				StatusResponse status = userService.addUser(user);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In retrieving add user request : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		log.info("Completed Add user request ");

		return nResponse;
	}

	@RequestMapping("/userList")
	public @ResponseBody
	ResponseEntity<String> getUsers(HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		log.info("userList Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		try {
			String authenticationType = AppUtil
					.getProperty("ADMIN_AUTHENTICATION_TYPE");
			JUserList userList = null;
			if ("DB".equalsIgnoreCase(authenticationType)) {
				userList = userService.getUserListFromDB();
			} else {
				userList = userService.getUserListFromPriv();
			}
			nResponse = createJSONResponse(AppUtil.getJSONFrom(userList));

		} catch (Exception e) {
			log.error("Error Retrieving update privilege details: "
					+ e.getMessage());
			nResponse = unAuthorized();
		}

		return nResponse;

	}

	@RequestMapping("/updateUser")
	public ResponseEntity<String> updateUser(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Users add Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		JAdminUser user = new JAdminUser();
		user.setFirstName(request.getParameter("firstName"));
		user.setLastName(request.getParameter("lastName"));
		user.setUsername(request.getParameter("username"));
		user.setRole(request.getParameter("role"));
		user.setActive(request.getParameter("active"));
		// user.setPassword(request.getParameter("password"));

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("user validation completed.");
			try {
				StatusResponse status = userService.updateUser(user);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In retrieving add user request : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		log.info("Completed Add user request ");

		return nResponse;
	}

	@RequestMapping(value = "/changePassword")
	public ResponseEntity<String> changePassword(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("changePassword Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String userId = request.getParameter("userId");
		String password = request.getParameter("password");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {

				StatusResponse passwordChange = userService.changePassword(
						userId, password);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(passwordChange));

			} catch (Exception e) {
				log.error("Error in retrieving Roles List : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		log.info("Completed changePassword request");
		return nResponse;
	}

	@RequestMapping("/logout")
	public @ResponseBody
	String logout(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		log.info("Logout Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		return "SESSION_COMPLETE";
	}

}
