package com.n2n.enroll.json;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.contact.bo.JContact;
import com.n2n.enroll.contact.bo.PersonList;
import com.n2n.enroll.contact.service.ContactService;
import com.n2n.enroll.feedback.bo.FeedbackResponse;
import com.n2n.enroll.feedback.bo.JFeedbackList;
import com.n2n.enroll.feedback.bo.JServiceArea;
import com.n2n.enroll.feedback.service.FeedbackService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class FeedbackController extends BaseController {

	@Autowired
	FeedbackService feedbackService;

	@Autowired
	UserService userService;
	
	@Autowired
	ContactService contactService;

	@RequestMapping("/getFeedback")
	public ResponseEntity<String> getFeedback(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");
		log.info("Feedback Emails Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		String fromAddress =request.getParameter("fromAddress");
		username =request.getParameter("studentId");
		if (username != null) {
			String studentId = userService.getBannerId(username);
	 		PersonList plist = contactService.getContact(studentId);
	 		List<JContact> cl = plist.getPersonDetails().get(0).getContactList().getContact();
	 		for(JContact cbo: cl){
	 			
					if (cbo.getGrp().equals("EMAIL")
							&& cbo.getType().equals(AppUtil.getProperty("studentEmailType"))) {
						fromAddress = cbo.getAddress();
					}
	 		}
		}

		String fromName = request.getParameter("fromName");
		String toAddress = request.getParameter("recipients");
		String toName = "";
		String subject = request.getParameter("subject");
		String comments1 = request.getParameter("comments");
		String comments2 = request.getParameter("comments2");
		String formEmailId = request.getParameter("fromEmailId");

		String areaId = request.getParameter("areaId");
		String areaName = request.getParameter("areaName");
		String subAreaName = request.getParameter("subAreaName");
		String courseId = request.getParameter("courseId");
		String confidential = request.getParameter("confidential");
		String title = request.getParameter("title");
		String instructor = request.getParameter("instructor");
		String description = request.getParameter("description");
		String termCode = request.getParameter("termCode");
		String crn = request.getParameter("crn");

		String feedbackType = "";
		try {
			feedbackType = AppUtil.getProperty("FEEDBACK_TYPE");
		} catch (Exception e) {
		}
		if (feedbackType != null && feedbackType.equals("GENERAL")) {
			if (subject == null || subject.equals("")) {
				subject = "eNtourage Feedback";
			}
			if (toAddress == null || toAddress.equals("")) {
				toAddress = feedbackService.getDefaultFeedbackToEmail();
			}
			if (fromName == null || fromName.equals("")) {
				fromAddress = AppUtil.getProperty("DEFAULT_FEEDBACK_FROM_ID");
				fromName = AppUtil.getProperty("DEFAULT_FEEDBACK_FROM_NAME");
			}

		} else {

			if (areaId != null && !areaId.equals("")) {
				toAddress = feedbackService.getServiceAreaContacts(areaId);
			} else {
				areaId = null;
				if (toAddress == null || toAddress.equals("")) {
					toAddress = feedbackService.getDefaultFeedbackToEmail();
				}

				String courseContacts = feedbackService.getCourseContacts(termCode,crn);
				if(!(courseContacts == null || "".equals(courseContacts))){
					toAddress= toAddress + "," +  courseContacts;
				}
			}

			if (courseId == null || courseId.equals("")) {
				subject = "Customer Service Feedback";
				if (areaName != null && !(areaName.equals(""))) {
					subject = subject + " for " + areaName;
					if (subAreaName != null && !(subAreaName.equals(""))) {
						subject = subject + ", " + subAreaName;
					}
				} else if (subAreaName != null && !(subAreaName.equals(""))) {
					subject = subject + " for " + subAreaName;
				}

				String body = "";
				if (fromName != null && !(fromName.equals(""))) {
					body = body + "Full Name: " + fromName + "<br />";
				}
				if (fromAddress != null && !(fromAddress.equals(""))) {
					body = body + "Email: " + fromAddress + "<br />";
				}

				body = body + "<p>Message: " + comments1 + "</p>";

				if (comments2 != null && !(comments2.equals(""))) {
					body = body + "<p>" + comments2 + "</p>";
				}

				if (confidential.equals("Y")) {
					body = body + "<br /> Confidentiality/Anonymity: Checked";
				} else {
					body = body + "<br /> Confidentiality/Anonymity: Unchecked";
				}

				comments1 = body;

				fromAddress = AppUtil.getProperty("CUSTSERV_FEEDBACK_FROM_ID");
				fromName = AppUtil.getProperty("CUSTSERV_FEEDBACK_FROM_NAME");

			} else {
				subject = "Course Evaluation for " + title;
				

				fromAddress = AppUtil.getProperty("DEFAULT_FEEDBACK_FROM_ID");
				fromName = AppUtil.getProperty("DEFAULT_FEEDBACK_FROM_NAME");

				String body = "";
				body = body + "Message : <br />Course - ";

				if (courseId != null && !(courseId.equals(""))) {
					body = body + courseId;
				}

				if (description != null && !(description.equals(""))) {
					body = body + " - " + description;
				}

				body = body + "<br /><br />";

				if (instructor != null && !(instructor.equals(""))) {
					body = body + "Instructor - " + instructor + "<br /><br />";
				}

				if (comments1 != null && !(comments1.equals(""))) {
					body = body
							+ "How would you describe the instruction and your classroom experience?<br />"
							+ comments1 + "<br /><br />";
				}
				if (comments2 != null && !(comments2.equals(""))) {
					body = body
							+ "Please provide any other comments on this course for improvement<br />"
							+ comments2 + "<br /><br />";
				}
				comments1 = body;
			}
		}

		feedbackService.saveFeedbackRecord(areaId, courseId, comments1,
				comments2, confidential, fromName, fromAddress);
		String status = feedbackService.sendEmail(fromAddress, fromName,
				toAddress, toName, subject, comments1, comments2);

		nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
		return nResponse;
	}

	@RequestMapping("/getFeedbackList")
	public ResponseEntity<String> getFeedbackList(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");
		log.info("Feedback Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		JFeedbackList feedbackList = feedbackService.getList();
		nResponse = createJSONResponse(AppUtil.getJSONFrom(feedbackList));
		return nResponse;
	}

	@RequestMapping("/addFeedback")
	public ResponseEntity<String> editEmergencyContacts(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info(" Edit addFeedback Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String id = request.getParameter("id");
		String title = request.getParameter("title");
		String email = request.getParameter("email");
		String description = request.getParameter("description");
		try {
			FeedbackResponse status = feedbackService.addOrEditFeedback(id,
					title, email, description);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in add or edit Feedback " + e.getMessage());
		}
		return nResponse;
	}

	@RequestMapping("/deleteFeedback")
	public ResponseEntity<String> deleteFeedback(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("contact deletion Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String id = request.getParameter("id");

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				FeedbackResponse status = feedbackService
						.deleteAdminContact(id);

				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In deleting feed request : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/getServiceAreas")
	public ResponseEntity<String> getServiceAreas(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Service areas Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		try {
			List<JServiceArea> areas = feedbackService.getServiceAreas();

			nResponse = createJSONResponse(AppUtil.getJSONFrom(areas));

		} catch (Exception e) {
			log.error("Error retreiving service areas: " + e.getMessage());
			log.error("StackTrace", e);
		}
		return nResponse;
	}
}
