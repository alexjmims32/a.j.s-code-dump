package com.n2n.enroll.json;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.n2n.enroll.util.AppUtil;

@Controller
public class StatusController extends BaseController {
	@Resource
	SessionFactory sessionFactory;

	@RequestMapping("/status")
	public String login(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Status Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String appStatus = "";

		try {

			AppUtil.runAQuery("select 1 from dual");
			appStatus = "Database connection successful";

		} catch (Exception e) {
			log.error("Error executing test query: " + e.getMessage());
			appStatus = "Database connection Failed. Please contact support@n2nservices.com";
		}

		request.getSession(true).setAttribute("APP_STATUS", appStatus);
		return "redirect:/appstatus.jsp";
	}
}
