package com.n2n.enroll.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.bo.StatusResponse;
import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.feeder.bo.AdminFeedResponse;
import com.n2n.enroll.feeder.bo.FeederList;
import com.n2n.enroll.feeder.bo.JAbouttextandFaq;
import com.n2n.enroll.feeder.bo.JAdminFeederList;
import com.n2n.enroll.feeder.bo.JFaqList;
import com.n2n.enroll.feeder.service.FeederService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class FeederController extends BaseController {

	@Autowired
	FeederService feederService;

	@Autowired
	UserService userService;

	@RequestMapping("/getConfig")
	public ResponseEntity<String> configurations(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");
		log.info("Config request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		int moduleID = AppUtil.getIntValue(request.getParameter("moduleID"));
		int parentID = AppUtil.getIntValue(request.getParameter("parentID"));

		String campusCode = request.getParameter("campusCode");
		String moduleCode = request.getParameter("moduleCode");

		FeederList feeders = feederService.getConfig(moduleID, parentID,
				campusCode, moduleCode);

		nResponse = createJSONResponse(AppUtil.getJSONFrom(feeders));
		return nResponse;
	}

	@RequestMapping("/getAdminConfig")
	public ResponseEntity<String> getAdminConfig(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");
		log.info("Feed config request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		int moduleID = AppUtil.getIntValue(request.getParameter("moduleID"));
		int parentID = AppUtil.getIntValue(request.getParameter("parentID"));

		String campusCode = request.getParameter("campusCode");

		JAdminFeederList feeders = feederService.getAdminConfig(moduleID,
				parentID, campusCode);

		nResponse = createJSONResponse(AppUtil.getJSONFrom(feeders));
		return nResponse;
	}

	@RequestMapping("/getFeedContent")
	public ResponseEntity<String> feedContent(
			@RequestParam(value = "page", required = false) String pageNo,
			@RequestParam(value = "limit", required = false) String pageSize,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Feed content request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		String feedId = request.getParameter("feedId");

		String feedData = "";

		HttpHeaders responseHeaders = new HttpHeaders();
		if (feedId != null && !feedId.equals("")) {

			try {

				feedData = feederService.getFeedContent(feedId, pageNo,
						pageSize);
				responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in retrieving get feed content : "
						+ e.getMessage());
			}
		}
		return new ResponseEntity<String>(feedData, responseHeaders,
				HttpStatus.CREATED);
	}

	@RequestMapping("/getFeedModules")
	public ResponseEntity<String> feedModules(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");
		log.info("feed modules Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		// int moduleID = AppUtil.getIntValue(request.getParameter("moduleID"));
		String campusCode = request.getParameter("campusCode");

		JAdminFeederList feeders = feederService.getFeedModules(campusCode);

		log.info("Completed Configurations request");
		nResponse = createJSONResponse(AppUtil.getJSONFrom(feeders));
		return nResponse;
	}

	@RequestMapping("/addEditFeed")
	public ResponseEntity<String> addFeed(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("feed add Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String id = request.getParameter("id");
		String parentID = request.getParameter("parentID");
		String type = request.getParameter("type");
		String link = request.getParameter("link");
		String format = request.getParameter("format");
		String moduleID = request.getParameter("moduleID");
		String name = request.getParameter("name");
		String icon = request.getParameter("icon");
		String campusCode = request.getParameter("campusCode");
		String lastModifiedBy = request.getParameter("lastModifiedBy");
		if (campusCode.equals("") || campusCode == null) {
			campusCode = "UT";
		}

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("user validation completed.");
			try {
				AdminFeedResponse status = feederService.addOrEditFeeder(id,
						parentID, type, link, format, moduleID, name, icon,
						campusCode, lastModifiedBy);

				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In retrieving add notification request : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		log.info("Completed Add feed request ");

		return nResponse;
	}

	@RequestMapping("/deleteFeed")
	public ResponseEntity<String> deleteFeed(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("feed deletion Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String id = request.getParameter("id");

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				AdminFeedResponse status = feederService.deleteFeed(id);

				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));

			} catch (Exception e) {
				log.error("Error In deleting feed request : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/versionTracking")
	public ResponseEntity<String> versionTracking(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ResponseEntity<String> nResponse = createJSONResponse("");
		log.info("versionTracking request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		JAdminFeederList feeders = feederService.getVersion();

		nResponse = createJSONResponse(AppUtil.getJSONFrom(feeders));
		return nResponse;
	}

	@RequestMapping("/updateModule")
	public ResponseEntity<String> updatePrivilege(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("update privileges Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String updateValue = request.getParameter("updateValue");
		String lastModifiedBy = request.getParameter("lastModifiedBy");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			log.info("Login through " + userService.getAuthSource()
					+ " successful: " + username);
			try {

				StatusResponse pResponse = feederService.updateModule(
						updateValue, lastModifiedBy);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(pResponse));

			} catch (Exception e) {
				log.error("Error Retrieving update privilege details: "
						+ e.getMessage());
				nResponse = unAuthorized();
			}
		}
		log.info("Completed update module details");
		return nResponse;
	}

	@RequestMapping(value = "/createHelpDoc")
	public ResponseEntity<String> createHelpDoc(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("createHelpDoc Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String campusCode = request.getParameter("campusCode");
		String areaText = request.getParameter("areaText");
		String lastModifiedBy = request.getParameter("lastModifiedBy");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {

				StatusResponse createHelpDoc = feederService.createHelpDoc(
						campusCode, areaText, lastModifiedBy);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(createHelpDoc));

			} catch (Exception e) {
				log.error("Error in creating HelpDoc: " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		log.info("Completed Help request");
		return nResponse;
	}

	@RequestMapping(value = "/updateTermsConditions")
	public ResponseEntity<String> createTermsDoc(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("createTermsDoc Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		String campusCode = request.getParameter("campusCode");
		String conditionText = request.getParameter("termsConditions");
		String lastModifiedBy = request.getParameter("lastModifiedBy");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {

				StatusResponse createTermsDoc = feederService
						.createTermsAndConditions(campusCode, conditionText,
								lastModifiedBy);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(createTermsDoc));

			} catch (Exception e) {
				log.error("Error in createTermsDoc : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		log.info("Completed createTermsDoc request");
		return nResponse;
	}

	
	@RequestMapping(value = "/getHelpContent")
	public ResponseEntity<String> getHelpContent(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("getHelpContent Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String campusCode = request.getParameter("campusCode");
		try {

			JAbouttextandFaq aboutAreaText = feederService
					.getHelpContent(campusCode);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(aboutAreaText));

		} catch (Exception e) {
			log.error("Error in getHelpContent: " + e.getMessage());
			log.error("StackTrace", e);
		}
		// }
		log.info("Completed getHelpContent request");
		return nResponse;
	}

	@RequestMapping(value = "/createFaq")
	public ResponseEntity<String> createFaq(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("createFaq Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String campusCode = request.getParameter("campusCode");
		String question = request.getParameter("question");
		String answer = request.getParameter("answer");
		String lastModifiedBy = request.getParameter("lastModifiedBy");
		String id = request.getParameter("id");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {

				StatusResponse createFaq = feederService.createFaq(id,
						campusCode, question, answer, lastModifiedBy);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(createFaq));

			} catch (Exception e) {
				log.error("Error in creating Faq: " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		log.info("Completed Faq request");
		return nResponse;
	}

	@RequestMapping(value = "/getFaqContent")
	public ResponseEntity<String> getFaqContent(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("getFaqContent Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String campusCode = request.getParameter("campusCode");
		try {

			JFaqList faqList = feederService.getFaqContent(campusCode);

			nResponse = createJSONResponse(AppUtil.getJSONFrom(faqList));

		} catch (Exception e) {
			log.error("Error in getFaqContent: " + e.getMessage());
			log.error("StackTrace", e);
		}
		// }
		log.info("Completed getFaqContent request");
		return nResponse;
	}

	@RequestMapping("/deleteFaq")
	public ResponseEntity<String> deleteFaq(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("Faq deletion Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateAdminUser(authHeader);

		String id = request.getParameter("id");

		ResponseEntity<String> nResponse = createJSONResponse("");
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				AdminFeedResponse deleteFaq = feederService.deleteFaq(id);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(deleteFaq));
			} catch (Exception e) {
				log.error("Error In deleting faq request : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/getHTMLContent")
	public ResponseEntity<String> htmlContent(
			@RequestParam(value = "page", required = false) String pageNo,
			@RequestParam(value = "limit", required = false) String pageSize,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("HTML content request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		String url = request.getParameter("url");

		String feedData = "";
		if (url != null && !url.equals("")) {
			try {
				feedData = feederService.getHtmlContent(url);
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in retrieving get html content : "
						+ e.getMessage());
			}
		}
		return createJSONResponse(AppUtil.getJSONFrom(feedData));
	}

	@RequestMapping("/getlatestFeedContent")
	public ResponseEntity<String> lastestFeedContent(
			@RequestParam(value = "category", required = true) String category,
			@RequestParam(value = "page", required = false) String pageNo,
			@RequestParam(value = "limit", required = false) String pageSize,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("latest Feed content request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		String feedData = "";

		HttpHeaders responseHeaders = new HttpHeaders();

		try {

			feedData = feederService.getLatestFeedContent(category, pageNo,
					pageSize);
			responseHeaders.add("Content-Type", "text/xml; charset=utf-8");

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in retrieving get feed content : "
					+ e.getMessage());
		}
		return new ResponseEntity<String>(feedData, responseHeaders,
				HttpStatus.CREATED);
	}

}
