package com.n2n.enroll.json;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.registration.bo.Cart;
import com.n2n.enroll.registration.bo.CourseLevelList;
import com.n2n.enroll.registration.bo.CourseList;
import com.n2n.enroll.registration.bo.PartOfTermList;
import com.n2n.enroll.registration.bo.ProgramList;
import com.n2n.enroll.registration.bo.ScheduleTypeList;
import com.n2n.enroll.registration.bo.SearchCourseResponse;
import com.n2n.enroll.registration.bo.SubjectList;
import com.n2n.enroll.registration.service.RegistrationService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class CourseSearchController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	RegistrationService registrationService;

	@RequestMapping("/search")
	public ResponseEntity<String> search(
			@RequestParam(value = "query", required = false) String searchString,
			@RequestParam(value = "termCode", required = false, defaultValue = "") String termCode,
			@RequestParam(value = "subCode", required = false, defaultValue = "") String subCode,
			@RequestParam(value = "start", required = false, defaultValue = "1") int start,
			@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Search Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String username = "";
		String authHeader = request.getHeader("Authorization");
		if(authHeader!=null && !(authHeader.equals(""))){
			username = userService.authenticateUser(authHeader);
			log.debug(username+": Entered into search Controller --"+new Date());
			if (username == null) {
				nResponse = unAuthorized();
				return nResponse;
			}
		}

		try {
			nResponse = createJSONResponse("");
			// We will now use a materialized view instead of cacheing
			// course
			// data in memory
			// CourseList xCourseList =
			// CourseCache.search(termCode,searchQuery,start,limit,page);

			username = userService.getBannerId(username);
			CourseList xCourseList = registrationService.search(username, termCode,subCode,
					searchString, start, limit, page);
			SearchCourseResponse courseList = new SearchCourseResponse();
			courseList.setCourseList(xCourseList);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(courseList));
			
		} catch (Exception e) {
			log.error("Error executing query: " + e.getMessage());
			log.error("StackTrace", e);
			nResponse = createErrorResponse(e.getMessage());
		}
		log.debug(username+": Return from search Controller --"+new Date());
		return nResponse;
	}
	
	
	@RequestMapping("/advSearch")
	public ResponseEntity<String> advSearch(
			@RequestParam(value = "crn", required = false) String crn,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "crseNum", required = false) String crseNum,
			@RequestParam(value = "crseLevel", required = false) String crseLevel,
			@RequestParam(value = "crseAttr", required = false) String crseAttr,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "term", required = false, defaultValue = "") String termCode,
			@RequestParam(value = "partOfTerm", required = false, defaultValue = "") String partOfTerm,
			@RequestParam(value = "scheduleType", required = false, defaultValue = "") String scheduleType,
			@RequestParam(value = "start", required = false, defaultValue = "1") int start,
			@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Search Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String username = "";
		String authHeader = request.getHeader("Authorization");
		if(authHeader!=null && !(authHeader.equals(""))){
			username = userService.authenticateUser(authHeader);
			
			if (username == null) {
				nResponse = unAuthorized();
				return nResponse;
			} 
		}

		try {
			nResponse = createJSONResponse("");
			// We will now use a materialized view instead of cacheing
			// course
			// data in memory
			// CourseList xCourseList =
			// CourseCache.search(termCode,searchQuery,start,limit,page);

			CourseList xCourseList = registrationService.advSearch(username, termCode,partOfTerm,scheduleType,crseAttr,
					crn,subject,crseNum,crseLevel,title, start, limit, page);
			SearchCourseResponse courseList = new SearchCourseResponse();
			courseList.setCourseList(xCourseList);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(courseList));
			
		} catch (Exception e) {
			log.error("Error executing query: " + e.getMessage());
			log.error("StackTrace", e);
			nResponse = createErrorResponse(e.getMessage());
		}

		return nResponse;
	}
	
	@RequestMapping(value = "/getProgramList")
	public ResponseEntity<String> getProgramList(
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "termCode", required = true) String termCode)
			throws Exception {

		log.info("Program List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		log.debug("Entered into getProgramList Controller ------"+new Date());
		try {
			ProgramList xList = registrationService
					.getProgramList(termCode);
			nResponse = createJSONResponse(AppUtil
					.getJSONFrom(xList));
		} catch (Exception e) {
			log.error("Error in retrieving Program List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		log.debug("Return from getProgramList Controller ------"+new Date());
		return nResponse;
	}
	
	@RequestMapping(value = "/getSubjectList")
	public ResponseEntity<String> getSubjectList(
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "termCode", required = true) String termCode)
			throws Exception {

		log.info("Subject List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		log.debug("Entered into getSubjectList Controller ------"+new Date());
		try {
			SubjectList xList = registrationService
					.getSubjectList(termCode);
			nResponse = createJSONResponse(AppUtil
					.getJSONFrom(xList));
		} catch (Exception e) {
			log.error("Error in retrieving Subject List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		log.debug("Return from getSubjectList Controller ------"+new Date());
		return nResponse;
	}
	
	@RequestMapping(value = "/getCourseLevelList")
	public ResponseEntity<String> getCourseLevelList(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Course level List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		log.debug("Entered into getCourseLevelList Controller ------"+new Date());
		try {
			CourseLevelList xList = registrationService
					.getCourseLevelList();
			nResponse = createJSONResponse(AppUtil
					.getJSONFrom(xList));
		} catch (Exception e) {
			log.error("Error in retrieving Course Level List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		log.debug("Return from getCourseLevelList Controller ------"+new Date());
		return nResponse;
	}
	
	@RequestMapping(value = "/getPartOfTermList")
	public ResponseEntity<String> getPartOfTermList(
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "termCode", required = true) String termCode)
			throws Exception {

		log.info("Part of term List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		log.debug("Entered into getPartOfTermList Controller ------"+new Date());
		try {
			PartOfTermList xList = registrationService
					.getPartOfTermList(termCode);
			nResponse = createJSONResponse(AppUtil
					.getJSONFrom(xList));
		} catch (Exception e) {
			log.error("Error in retrieving Part of term List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		log.debug("Return from getPartOfTermList Controller ------"+new Date());
		return nResponse;
	}
	
	@RequestMapping(value = "/getScheduleTypeList")
	public ResponseEntity<String> getScheduleTypeList(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Schedule type List Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		log.debug("Entered into getScheduleTypeList Controller ------"+new Date());
		try {
			ScheduleTypeList xList = registrationService
					.getScheduleTypeList();
			nResponse = createJSONResponse(AppUtil
					.getJSONFrom(xList));
		} catch (Exception e) {
			log.error("Error in retrieving Schedule type List : "
					+ e.getMessage());
			log.error("StackTrace", e);
		}
		log.debug("Return from getScheduleTypeList Controller ------"+new Date());
		return nResponse;
	}
	
	@RequestMapping("/getOlrDates")
	public ResponseEntity<String> getOlrDates(
			@RequestParam(value = "termCode", required = true) String termCode,
			@RequestParam(value = "crn", required = true) String crn, 
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("OLR Dates Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		try {
			Cart olrDates = registrationService.getOlrDates(termCode,
					crn);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(olrDates));
		} catch (Exception e) {
			log.error("Error in OlrDates : " + e.getMessage());
			e.printStackTrace();
		}
		return nResponse;
	}
}
