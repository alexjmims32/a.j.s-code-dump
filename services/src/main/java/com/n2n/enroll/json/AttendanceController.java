package com.n2n.enroll.json;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.n2n.enroll.attendance.bo.ClassRoasterList;
import com.n2n.enroll.attendance.bo.Code;
import com.n2n.enroll.attendance.bo.CodeResponse;
import com.n2n.enroll.attendance.service.AttendanceService;
import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.notif.bo.JNotificationLog;
import com.n2n.enroll.registration.bo.RegisteredCoursesResponse;
import com.n2n.enroll.util.AppUtil;

@Controller
public class AttendanceController extends BaseController {
	@Resource
	SessionFactory sessionFactory;

	@Autowired
	UserService userService;

	@Resource
	AttendanceService attendanceService;

	private static final String defaultCodeValidityTime = "120";
	private static final String defaultCodeExtendTime = "30";

	@RequestMapping("/genarateCode")
	public ResponseEntity<String> genarateCode(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("genarateCode Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			Code xCode = new Code();
			xCode.setInstructorId(request.getParameter("instructorId"));
			xCode.setTermCode(request.getParameter("termCode"));
			xCode.setCrn(request.getParameter("crn"));
			xCode.setAttDate(request.getParameter("attDate"));
			xCode.setStartTime(request.getParameter("startTime"));
			xCode.setEndTime(request.getParameter("endTime"));
			String codeValidityTime = AttendanceController.defaultCodeValidityTime; // This
																					// is
																					// the
																					// default

			codeValidityTime = AppUtil.getProperty("ATT_CODE_VALIDITY_TIME");

			if (codeValidityTime == null || "".equals(codeValidityTime)) {
				codeValidityTime = AttendanceController.defaultCodeValidityTime;
			}

			xCode.setCodeExpiryDteTime(codeValidityTime);
			try {
				CodeResponse code = attendanceService.genarateCode(xCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(code));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in genarateCode " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/extendTime")
	public ResponseEntity<String> extendTime(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("extendTime Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			Code xCode = new Code();
			xCode.setInstructorId(request.getParameter("instructorId"));
			xCode.setTermCode(request.getParameter("termCode"));
			xCode.setCrn(request.getParameter("crn"));
			xCode.setAttCode(request.getParameter("attCode"));
			xCode.setAttDate(request.getParameter("attDate"));
			xCode.setStartTime(request.getParameter("startTime"));
			xCode.setEndTime(request.getParameter("endTime"));

			String codeExtendTime = AttendanceController.defaultCodeExtendTime; // This
																				// is
																				// the
																				// default

			codeExtendTime = AppUtil.getProperty("ATT_CODE_EXTEND_TIME");
			if (codeExtendTime == null || "".equals(codeExtendTime)) {
				codeExtendTime = AttendanceController.defaultCodeExtendTime;
			}

			xCode.setCodeExpiryDteTime(codeExtendTime);

			try {
				CodeResponse status = attendanceService.timeExtend(xCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in extendTime " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/attendnceSubmitByStudent")
	public ResponseEntity<String> attendnceSubmitbyStudent(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("attendnceSubmitbyStudent Request from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			Code xCode = new Code();
			xCode.setStudentId(request.getParameter("studentId"));
			xCode.setInstructorId(request.getParameter("instructorId"));
			xCode.setTermCode(request.getParameter("termCode"));
			xCode.setCrn(request.getParameter("crn"));
			xCode.setAttCode(request.getParameter("attendanceCode"));
			xCode.setAttSubmitTime(request.getParameter("codeSubmitDateTime"));
			xCode.setValidindicator(request.getParameter("indicator"));
			xCode.setOverWrite(request.getParameter("overWrite"));
			xCode.setOverComments(request.getParameter("overComments"));
			xCode.setOverWriteBy(request.getParameter("overWriteBy"));
			xCode.setGpsLocationMatch(request.getParameter("gpsLocationMatch"));
			xCode.setLastModifiedDate(request.getParameter("lastModifiedDate"));
			String values = request.getParameter("values");

			try {
				CodeResponse attResponse = attendanceService
						.submitAttendance(xCode, values);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(attResponse));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in attendnceSubmitByStudent " + e.getMessage());
			}
		}
		return nResponse;
	}
	
	@RequestMapping("/updateAttendance")
	public ResponseEntity<String> updateAttendance(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("attendnceSubmitbyStudent Request from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			Code xCode = new Code();
			xCode.setInstructorId(request.getParameter("instructorId"));
			xCode.setTermCode(request.getParameter("termCode"));
			xCode.setCrn(request.getParameter("crn"));
			xCode.setOverWrite(request.getParameter("overWrite"));
			xCode.setOverComments(request.getParameter("overComments"));
			xCode.setOverWriteBy(request.getParameter("overWriteBy"));
			String values = request.getParameter("values");

			try {
				CodeResponse attResponse = attendanceService
						.updateAttendance(xCode, values);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(attResponse));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in update Attendance " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/sendMessage")
	public ResponseEntity<String> sendMail(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("sendMessage Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			String to = request.getParameter("to");
			String crn = request.getParameter("crn");
			String termCode = request.getParameter("termCode");
			username = request.getParameter("userName");

			JNotificationLog jNotificationLog = new JNotificationLog();
			jNotificationLog.setUserName(to);
			jNotificationLog.setMessage(request.getParameter("comments"));
			jNotificationLog.setExpiryDate(request.getParameter("expirydate"));
			jNotificationLog.setDeliveryMethod("PUSH");
			jNotificationLog.setDelivered("0");
			jNotificationLog.setReadFlag("0");
			jNotificationLog.setLastModifiedBy(username);
			jNotificationLog.setLastModifiedOn("");
			jNotificationLog.setDeleted("0");
			jNotificationLog.setType("Academics");
			jNotificationLog.setTitle(request.getParameter("subject"));
			jNotificationLog.setDueDate("");
			try {
				CodeResponse status = attendanceService.sendMessage(
						jNotificationLog, crn, termCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in sendMessage " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/facultyCourses")
	public ResponseEntity<String> facultyCourse(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("facultyCourses Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			String termCode = request.getParameter("termCode");
			String facultyId = request.getParameter("studentId");
			try {
				RegisteredCoursesResponse courseListResponse = attendanceService
						.getFacultyCourses(termCode, facultyId);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(courseListResponse));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in facultyCourses " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/getAttendenceDetails")
	public ResponseEntity<String> getAttendenceDetails(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("getAttendenceDetails Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			String instructorId = request.getParameter("instructorId");
			String termCode = request.getParameter("termCode");
			String crn = request.getParameter("crn");
			String attDate = request.getParameter("attDate");
			String classStartTime = request.getParameter("classStartTime");
			String classEndTime = request.getParameter("classEndTime");
			try {
				ClassRoasterList attendenceResponse = attendanceService
						.getAttendenceDetails(instructorId, termCode, crn,
								attDate, classStartTime, classEndTime);

				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(attendenceResponse));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in getAttendenceDetails " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/classRoaster")
	public ResponseEntity<String> classRoaster(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("classRoaster Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			Code xCode = new Code();
			xCode.setInstructorId(request.getParameter("instructorId"));
			xCode.setTermCode(request.getParameter("termCode"));
			xCode.setCrn(request.getParameter("crn"));
			xCode.setAttDate(request.getParameter("attDate"));
			xCode.setStartTime(request.getParameter("startTime"));
			xCode.setEndTime(request.getParameter("endTime"));
			xCode.setCodeExpiryDteTime("30");

			try {
				ClassRoasterList roasterList = attendanceService
						.viewClassRoaster(xCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(roasterList));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in classRoaster " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/viewAttendence")
	public ResponseEntity<String> viewAttendence(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("viewAttendence Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String studentId = request.getParameter("studentId");
			String termCode = request.getParameter("termCode");
			String crn = request.getParameter("crn");

			try {
				ClassRoasterList roastList = attendanceService.viewAttendence(
						studentId, startDate, endDate, termCode, crn);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(roastList));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in viewAttendence " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/attendenceHistory")
	public ResponseEntity<String> getAttendenceHistory(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("attendenceHistory Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			String instructorId = request.getParameter("instructorId");
			String termCode = request.getParameter("termCode");
			String crn = request.getParameter("crn");
			String attDate = request.getParameter("attDate");
			String classStartTime = request.getParameter("classStartTime");
			String classEndTime = request.getParameter("classEndTime");
			String indicator = request.getParameter("indicator");

			try {
				ClassRoasterList names = attendanceService.attendenceHistory(
						instructorId, termCode, crn, attDate, classStartTime,
						classEndTime, indicator);

				nResponse = createJSONResponse(AppUtil.getJSONFrom(names));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in attendenceHistory " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/getIndicatorByDate")
	public ResponseEntity<String> getIndicatorByDate(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("getIndicatorByDate Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			String studentId = request.getParameter("studentId");
			String termCode = request.getParameter("termCode");
			String attDate = request.getParameter("attDate");

			try {
				ClassRoasterList roastList = attendanceService
						.getIndicatorByDate(studentId, attDate, termCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(roastList));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in getIndicatorByDate " + e.getMessage());
			}
		}
		return nResponse;
	}

	@RequestMapping("/getAttandenceReport")
	public ResponseEntity<String> getAttandenceList(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		log.info("getAttandenceReport Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {

			String studentId = request.getParameter("studentId");
			String termCode = request.getParameter("termCode");
			String crn = request.getParameter("crn");
			String startTime = request.getParameter("classStartTime");
			String endTime = request.getParameter("classEndTime");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			try {
				ClassRoasterList roastList = attendanceService
						.getAttandenceReport(studentId, crn, termCode,
								startTime, endTime, startDate, endDate, username);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(roastList));
			} catch (Exception e) {
				log.error("StackTrace", e);
				log.error("Error in getAttandenceReport " + e.getMessage());
			}
		}
		return nResponse;
	}
}
