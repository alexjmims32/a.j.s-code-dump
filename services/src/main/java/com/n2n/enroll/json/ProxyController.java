package com.n2n.enroll.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.proxy.bo.ProxySessionResponse;
import com.n2n.enroll.proxy.service.ProxyService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class ProxyController extends BaseController {

	@Autowired
	UserService userService;

	@Autowired
	ProxyService proxyService;

	@RequestMapping(value = "/start")
	public ResponseEntity<String> startProxySession(
			@RequestParam(value = "studentId", required = true) String studentId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Start proxy session Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				String ipAddress = request.getRemoteHost();				
				ProxySessionResponse proxySessionResponse = proxyService
						.startSession(username,studentId, ipAddress);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(proxySessionResponse));
			} catch (Exception e) {
				log.error("Error in retrieving start proxy session : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping(value = "/stop")
	public ResponseEntity<String> stopProxySession(
			@RequestParam(value = "studentId", required = true) String studentId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Stop proxy session Request from " + request.getRemoteHost()
				+ "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				String ipAddress = request.getRemoteHost();
				ProxySessionResponse proxySessionResponse = proxyService
						.stopSession(username,studentId, ipAddress);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(proxySessionResponse));
			} catch (Exception e) {
				log.error("Error in retrieving stop proxy session : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}
}
