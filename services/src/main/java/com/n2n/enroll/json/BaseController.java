package com.n2n.enroll.json;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.n2n.enroll.util.AppUtil;

public class BaseController {

	public static final String SUCCESS = "SUCCESS";
	public static final String HTMLContentType = "text/html; charset=utf-8";
	public static final String JSONContentType = "application/json; charset=utf-8";
	public static final String XMLContentType = "text/xml; charset=utf-8";
	protected final Log log = LogFactory.getLog(getClass());

	public ResponseEntity<String> createResponse(String contentType,
			HashMap<String, String> moreHeaders, String responseBody,
			HttpStatus responseStatus) {

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", contentType);
		for (String key : moreHeaders.keySet()) {
			responseHeaders.add(key, moreHeaders.get(key));
		}

		return new ResponseEntity<String>(responseBody, responseHeaders,
				responseStatus);
	}

	public ResponseEntity<String> createResponse(String contentType,
			String responseBody, HttpStatus responseStatus) {
		return createResponse(contentType, new HashMap<String, String>(0),
				responseBody, responseStatus);
	}

	public ResponseEntity<String> createResponse(String responseBody,
			HttpStatus responseStatus) {
		return createResponse(HTMLContentType, responseBody, responseStatus);
	}

	public ResponseEntity<String> createResponse(String responseBody) {
		return createResponse(HTMLContentType, responseBody, HttpStatus.OK);
	}

	public ResponseEntity<String> createJSONResponse(String responseBody,
			HttpStatus responseStatus) {
		return createResponse(JSONContentType, responseBody, responseStatus);
	}

	public ResponseEntity<String> createJSONResponse(String responseBody) {
		return createResponse(JSONContentType, responseBody, HttpStatus.OK);
	}

	public ResponseEntity<String> createXMLResponse(String responseBody) {
		return createResponse(XMLContentType, responseBody, HttpStatus.OK);
	}

	public ResponseEntity<String> unAuthorized() {
		return createResponse(JSONContentType, "UNAUTHORIZED",
				HttpStatus.UNAUTHORIZED);
	}

	public ResponseEntity<String> createErrorResponse(String message) {
		HashMap<String, Object> map = new HashMap<String, Object>(0);
		map.put("status", message);
		return createResponse(JSONContentType, AppUtil.getJSONFrom(map),
				HttpStatus.OK);
	}
}
