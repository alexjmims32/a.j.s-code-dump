package com.n2n.enroll.json;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FileUploadController extends BaseController {
	protected final Log log = LogFactory.getLog(getClass());

	private boolean isMultipart;
	private String filePath = "E:\\";
	private int maxFileSize = 1000 * 1024;
	private int maxMemSize = 4 * 1024;
	private File file;

	@Autowired
	private ServletContext servletContext;

	@SuppressWarnings({ "rawtypes", "unused" })
	@RequestMapping(value = "/fileUpload")
	public ResponseEntity<String> upload(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		BufferedInputStream bis = null;
		BufferedImage bufimg = null;
		BufferedImage resized = null;
		ByteArrayOutputStream bytestream = null;
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;

		String test8 = System.getProperty("catalina.base") + "/webapps/images/";

		// String test1 = request.getSession().getServletContext()
		// .getRealPath("images");
		// String test2 = request.getSession().getServletContext()
		// .getRealPath("/images/");
		// String test5 = request.getSession().getServletContext()
		// .getRealPath(request.getServletPath());
		// String test6 = request.getSession().getServletContext()
		// .getRealPath("\\image");
		filePath = test8;

		// log.info("test 1:  " + test1);
		// log.info("test 2:  " + test2);
		// log.info("test 5:  " + test5);
		log.info("filePath:  " + filePath);

		ResponseEntity<String> nResponse = createJSONResponse("");
		String status = "success";
		System.out.println(">>>>>>>>file upload >>>>>>>>>>>> ");
		isMultipart = ServletFileUpload.isMultipartContent(request);
		response.setContentType("txt/html");
		// java.io.PrintWriter out = response.getWriter();
		if (!isMultipart) {
			// out.println("<html>");
			// out.println("<head>");
			// out.println("<title>Servlet upload</title>");
			// out.println("</head>");
			// out.println("<body>");
			// out.println("<p>No file uploaded</p>");
			// out.println("</body>");
			// out.println("</html>");
			// return;
		}
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);
		// Location to save data that is larger than maxMemSize.
		// factory.setRepository(new File("E:\\"));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		try {
			// Parse the request to get file items.
			List fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();

			// out.println("<html>");
			// out.println("<head>");
			// out.println("<title>Servlet upload</title>");
			// out.println("</head>");
			// out.println("<body>");
			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				if (!fi.isFormField()) {
					bis = new BufferedInputStream(fi.getInputStream());

					// BufferedImage
					bufimg = ImageIO.read(bis);

					// check size of image
					int img_width = bufimg.getWidth();
					int img_height = bufimg.getHeight();
					log.info("Height   :" + img_height + "Width   :"
							+ img_width);
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					String fileName = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();
					log.info("File Size: " + sizeInBytes);
					// Write the file
					if (sizeInBytes < maxFileSize) {
						if (fileName.lastIndexOf("\\") >= 0) {
							file = new File(filePath + "mainmenu.png");
						} else {
							file = new File(filePath + "mainmenu.png");
						}
						fi.write(file);

						log.info("Uploaded Filename: " + fileName + "<br>");
						nResponse = createJSONResponse(status);
					}
				} else {
					status = "Image size must be below 4MB";
					nResponse = createJSONResponse(status);

				}
			}

		} catch (Exception ex) {
			System.out.println(ex);
			nResponse = createJSONResponse("Fail");
		}
		return nResponse;

	}

}
