package com.n2n.enroll.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.location.service.LocationService;
import com.n2n.enroll.util.AppUtil;

@Controller
public class LocationController extends BaseController {
	@Autowired
	UserService userService;

	@Autowired
	LocationService locationService;

	@RequestMapping(value = "/validateAddress")
	public ResponseEntity<String> validateAddress(
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state,
			@RequestParam(value = "zip", required = true) String zip,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ResponseEntity<String> nResponse = null;
		log.info("Validate address request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());
		nResponse = createJSONResponse("");

		try {
			String status = locationService.validateAddress(city, state, zip);
			nResponse = createJSONResponse(AppUtil.getJSONFrom(status));
			
		} catch (Exception e) {
			log.error("Error in retrieving summary list : " + e.getMessage());
			log.error("StackTrace", e);
		}
		return nResponse;
	}

}
