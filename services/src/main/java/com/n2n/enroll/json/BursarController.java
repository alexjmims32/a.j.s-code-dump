package com.n2n.enroll.json;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.n2n.enroll.auth.service.UserService;
import com.n2n.enroll.bursar.bo.BursarTermList;
import com.n2n.enroll.bursar.bo.EbillSummary;
import com.n2n.enroll.bursar.bo.EbillSummaryList;
import com.n2n.enroll.bursar.bo.Summary;
import com.n2n.enroll.bursar.bo.SummaryDetailsList;
import com.n2n.enroll.bursar.bo.SummaryList;
import com.n2n.enroll.bursar.service.BursarService;
import com.n2n.enroll.registration.bo.CurrentTerm;
import com.n2n.enroll.util.AppUtil;

@Controller
public class BursarController extends BaseController {
	@Autowired
	UserService userService;

	@Autowired
	BursarService bursarService;

	@RequestMapping(value = "/getSummaryList")
	public ResponseEntity<String> getSummaryList(
			@RequestParam(value = "studentId", required = true) String studentId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Summary list request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			studentId = userService.getBannerId(studentId);
			try {
				SummaryList summaryList = new SummaryList();
				summaryList = bursarService.getSummaryList(studentId);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(summaryList));

			} catch (Exception e) {
				log.error("Error in retrieving summary list : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping(value = "/getSummaryListByTerm")
	public ResponseEntity<String> getSummaryListByTerm(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "termCode", required = true) String termCode,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Summary list by term request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				studentId = userService.getBannerId(studentId);
				SummaryList summaryList = new SummaryList();
				summaryList = bursarService.getSummaryListByTerm(studentId,
						termCode);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(summaryList));
			} catch (Exception e) {
				log.error("Error in retrieving summary list by term : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping(value = "/getSummaryDetails")
	public ResponseEntity<String> getSummaryDetails(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "category", required = true) String category,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Summary details request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			studentId = userService.getBannerId(studentId);
			try {
				SummaryDetailsList summaryDetailsList = new SummaryDetailsList();
				summaryDetailsList = bursarService.getSummaryDetails(studentId,
						category);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(summaryDetailsList));
			} catch (Exception e) {
				log.error("Error in retrieving summary details : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;

	}

	@RequestMapping(value = "/getSummaryDetailsByTerm")
	public ResponseEntity<String> getSummaryDetailsByTerm(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "category", required = true) String category,
			@RequestParam(value = "termCode", required = true) String termCode,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Summary details by term request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			studentId = userService.getBannerId(studentId);
			try {
				SummaryDetailsList summaryDetailsList = new SummaryDetailsList();
				summaryDetailsList = bursarService.getSummaryDetailsByTerm(
						studentId, category, termCode);
				nResponse = createJSONResponse(AppUtil
						.getJSONFrom(summaryDetailsList));

			} catch (Exception e) {
				log.error("Error in retrieving summary details by term : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}

		}
		return nResponse;
	}

	@RequestMapping(value = "/getBursarTerms")
	public ResponseEntity<String> getTerms(
			@RequestParam(value = "studentId", required = false) String studentId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Terms Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);

		ResponseEntity<String> nResponse = createJSONResponse("");

		if (username == null) {
			nResponse = unAuthorized();
		} else {
			studentId = userService.getBannerId(studentId);
			try {
				BursarTermList termList = bursarService
						.getBursarTerms(studentId);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(termList));
			} catch (Exception e) {
				log.error("Error in Retrieving Terms " + e.getMessage());
				log.error("StackTrace", e);
			}

		}
		return nResponse;
	}

	@RequestMapping(value = "/getEbillSummary")
	public ResponseEntity<String> getEbillSummary(
			@RequestParam(value = "studentId", required = true) String studentId,
			@RequestParam(value = "termCode", required = true) String termCode,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		log.info("Summary list by term request received from "
				+ request.getRemoteHost() + "\\" + request.getRemoteUser());

		ResponseEntity<String> nResponse = createJSONResponse("");
		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				studentId = userService.getBannerId(studentId);
				String ebillSummary = bursarService.getEbillStatement(
						studentId, termCode);
				nResponse = createXMLResponse(ebillSummary);
			} catch (Exception e) {
				log.error("Error in retrieving summary list by term : "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}

		return nResponse;
	}

	@RequestMapping("/getBursarCurrentTerm")
	public ResponseEntity<String> getCurrentTerm(
			@RequestParam(value = "studentId", required = true) String studentId,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Current Term Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				studentId = userService.getBannerId(studentId);
				CurrentTerm term = bursarService.getCurrentTerm(studentId);
				nResponse = createJSONResponse(AppUtil.getJSONFrom(term));
			} catch (Exception e) {
				log.error("Error in getCurrentTerm : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

	@RequestMapping("/getBillingInfoTerms")
	public ResponseEntity<String> getBillingInfoTerms(
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		log.info("Current Term Request from " + request.getRemoteHost() + "\\"
				+ request.getRemoteUser());
		ResponseEntity<String> nResponse = createJSONResponse("");

		String authHeader = request.getHeader("Authorization");
		String username = userService.authenticateUser(authHeader);
		if (username == null) {
			nResponse = unAuthorized();
		} else {
			try {
				BursarTermList terms = bursarService.getBillintInfoTerms();
				nResponse = createJSONResponse(AppUtil.getJSONFrom(terms));
			} catch (Exception e) {
				log.error("Error in getCurrentTerm : " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return nResponse;
	}

}
