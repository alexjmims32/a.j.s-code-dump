package com.n2n.enroll.attendance.bo;

import java.util.ArrayList;
import java.util.List;

public class ClassRoasterList {
	protected List<JClassRoaster> roaster = new ArrayList<JClassRoaster>(0);

	public List<JClassRoaster> getRoaster() {
		return roaster;
	}

	public void setRoaster(List<JClassRoaster> roaster) {
		this.roaster = roaster;
	}

	protected String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
