package com.n2n.enroll.attendance.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
public class CodeBo implements Serializable {

	@Column(name = "INSTRUCTOR_ID")
	private String instructorId;
	@Column(name = "TERM_CODE")
	private String termCode;
	@Column(name = "CRN")
	private String crn;
	@Id
	@Column(name = "ATTENDENCE_CODE")
	private String attCode;
	@Column(name = "ATTENDENCE_DATE")
	private String attDate;
	@Column(name = "CODE_EXPIRY_DATETIME")
	private String expiryTime;

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(String instructorId) {
		this.instructorId = instructorId;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getAttCode() {
		return attCode;
	}

	public void setAttCode(String attCode) {
		this.attCode = attCode;
	}

	public String getAttDate() {
		return attDate;
	}

	public void setAttDate(String attDate) {
		this.attDate = attDate;
	}

	public String getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(String expiryTime) {
		this.expiryTime = expiryTime;
	}

}