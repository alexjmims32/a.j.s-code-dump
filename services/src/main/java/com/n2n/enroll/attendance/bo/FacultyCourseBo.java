package com.n2n.enroll.attendance.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class FacultyCourseBo implements Serializable {
	@Id
	private String id;
	
	@Column(name = "FACULTY_ID")
	private String facultyId;

	@Column(name = "FACULTY_NAME")
	private String facultyName;

	@Column(name = "TERM_CODE")
	private String termCode;

	@Column(name = "CRN")
	private String crn;
	
	@Column(name = "CRSE_NUMB")
	private String courseNumber;

	@Column(name = "SUBJECT")
	private String subject;

	@Column(name = "SUBJECT_DESC")
	private String subjectDesc;

	@Column(name = "COURSE_TITLE")
	private String crseTitle;

	@Column(name = "CAMPUS")
	private String campus;

	@Column(name = "COURSE_LEVEL")
	private String courseLevel;

	@Column(name = "DEPARTMENT")
	private String department;
	
	@Column(name = "COLLEGE")
	private String college;
	
	@Column(name = "TERM_DESC")
	private String termDesc;
	
	@Column(name = "CREDITS")
	private String credits;
	
	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getCollege() {
		return college;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getTermDesc() {
		return termDesc;
	}

	public void setTermDesc(String termDesc) {
		this.termDesc = termDesc;
	}

	public String getFacultyId() {
		return facultyId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setFacultyId(String facultyId) {
		this.facultyId = facultyId;
	}

	public String getFacultyName() {
		return facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getCourseNumber() {
		return courseNumber;
	}

	public void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubjectDesc() {
		return subjectDesc;
	}

	public void setSubjectDesc(String subjectDesc) {
		this.subjectDesc = subjectDesc;
	}

	public String getCrseTitle() {
		return crseTitle;
	}

	public void setCrseTitle(String crseTitle) {
		this.crseTitle = crseTitle;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getCourseLevel() {
		return courseLevel;
	}

	public void setCourseLevel(String courseLevel) {
		this.courseLevel = courseLevel;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

}
