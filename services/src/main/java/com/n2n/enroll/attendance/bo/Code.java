package com.n2n.enroll.attendance.bo;

import java.math.BigDecimal;
import java.sql.Date;

public class Code {
	private String studentId;
	private String attSubmitTime;
	private String validindicator;
	private String overWrite;
	private String overComments;
	private String overWriteBy;
	private String gpsLocationMatch;
	private String lastModifiedDate;
	private String instructorId;
	private String termCode;
	private String crn;
	private String attCode;
	private String attDate;

	public String getAttDate() {
		return attDate;
	}

	public void setAttDate(String attDate) {
		this.attDate = attDate;
	}

	private String startTime;
	private String endTime;
	private String codeExpiryDteTime;
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getAttSubmitTime() {
		return attSubmitTime;
	}

	public void setAttSubmitTime(String attSubmitTime) {
		this.attSubmitTime = attSubmitTime;
	}

	public String getValidindicator() {
		return validindicator;
	}

	public void setValidindicator(String validindicator) {
		this.validindicator = validindicator;
	}

	public String getOverWrite() {
		return overWrite;
	}

	public void setOverWrite(String overWrite) {
		this.overWrite = overWrite;
	}

	public String getOverComments() {
		return overComments;
	}

	public void setOverComments(String overComments) {
		this.overComments = overComments;
	}

	public String getOverWriteBy() {
		return overWriteBy;
	}

	public void setOverWriteBy(String overWriteBy) {
		this.overWriteBy = overWriteBy;
	}

	public String getGpsLocationMatch() {
		return gpsLocationMatch;
	}

	public void setGpsLocationMatch(String gpsLocationMatch) {
		this.gpsLocationMatch = gpsLocationMatch;
	}

	public String getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(String instructorId) {
		this.instructorId = instructorId;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getAttCode() {
		return attCode;
	}

	public void setAttCode(String attCode) {
		this.attCode = attCode;
	}

	public String getCodeExpiryDteTime() {
		return codeExpiryDteTime;
	}

	public void setCodeExpiryDteTime(String codeExpiryDteTime) {
		this.codeExpiryDteTime = codeExpiryDteTime;
	}
}
