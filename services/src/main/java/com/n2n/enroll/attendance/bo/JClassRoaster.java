package com.n2n.enroll.attendance.bo;

public class JClassRoaster {
	private String studentId;
	private String name;
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	private String termCode;
	private String crn;
	private String email;
	private String attIndicator;
	private String code;
	private String count;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getStudentId() {
		return studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAttIndicator() {
		return attIndicator;
	}

	public void setAttIndicator(String attIndicator) {
		this.attIndicator = attIndicator;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
}
