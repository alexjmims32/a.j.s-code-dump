package com.n2n.enroll.attendance.service;

import java.sql.CallableStatement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.n2n.enroll.attendance.bo.ClassRoasterBo;
import com.n2n.enroll.attendance.bo.ClassRoasterList;
import com.n2n.enroll.attendance.bo.Code;
import com.n2n.enroll.attendance.bo.CodeResponse;
import com.n2n.enroll.attendance.bo.FacultyCourseBo;
import com.n2n.enroll.attendance.bo.JClassRoaster;
import com.n2n.enroll.json.AttendanceController;
import com.n2n.enroll.notif.bo.JNotificationLog;
import com.n2n.enroll.notif.service.NotificationService;
import com.n2n.enroll.registration.bo.JCourse;
import com.n2n.enroll.registration.bo.JMeetingList;
import com.n2n.enroll.registration.bo.JPeople;
import com.n2n.enroll.registration.bo.JPeopleList;
import com.n2n.enroll.registration.bo.JRegisteredCourse;
import com.n2n.enroll.registration.bo.RegisteredCourseList;
import com.n2n.enroll.registration.bo.RegisteredCoursesResponse;
import com.n2n.enroll.registration.service.RegistrationService;
import com.n2n.enroll.util.AppUtil;

@Service
@SuppressWarnings("unchecked")
public class AttendanceService {
	@Resource
	SessionFactory sessionFactory;

	@Resource
	RegistrationService registrationService;

	@Resource
	NotificationService notificationService;

	protected final Log log = LogFactory.getLog(getClass());

	public CodeResponse genarateCode(Code xCode) throws Exception {
		CodeResponse response = new CodeResponse();
		String status = "SUCCESS";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		String code = "";
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_GENERATE_CODE") + "}");
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(9, java.sql.Types.VARCHAR);
			callableStatement.setString(1, xCode.getInstructorId());
			callableStatement.setString(2, xCode.getTermCode());
			callableStatement.setString(3, xCode.getCrn());
			callableStatement.setString(5, xCode.getAttDate().toString());
			callableStatement.setString(6, xCode.getStartTime());
			callableStatement.setString(7, xCode.getEndTime());
			callableStatement.setString(8, xCode.getCodeExpiryDteTime());
			callableStatement.execute();
			code = callableStatement.getString(4);
			status = callableStatement.getString(9);
			callableStatement.close();
			response.setCode(code);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;

	}

	public CodeResponse timeExtend(Code xCode) throws Exception {
		CodeResponse response = new CodeResponse();
		String status = "Success";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {

			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_GENERATE_CODE") + "}");
			callableStatement.registerOutParameter(9, java.sql.Types.VARCHAR);
			callableStatement.setString(1, xCode.getInstructorId());
			callableStatement.setString(2, xCode.getTermCode());
			callableStatement.setString(3, xCode.getCrn());
			callableStatement.setString(4, xCode.getAttCode());
			callableStatement.setString(5, xCode.getAttDate());
			callableStatement.setString(6, xCode.getStartTime());
			callableStatement.setString(7, xCode.getEndTime());
			callableStatement.setString(8, xCode.getCodeExpiryDteTime());
			callableStatement.execute();
			status = callableStatement.getString(9);
			callableStatement.close();
			response.setStatus(status);
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;

	}

	public CodeResponse submitAttendance(Code xCode, String values)
			throws Exception {
		CodeResponse response = new CodeResponse();
		String status = "Success";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			callableStatement = session.connection()
					.prepareCall(
							"{call "
									+ AppUtil
											.getProperty("P_SUBMIT_ATTENDANCE")
									+ "}");
			String[] attValues = values.split(";");
			for (int i = 0; i < attValues.length; i++) {
				String[] studentValue = attValues[i].split(",");
				String studentId = studentValue[0];
				String indicator = studentValue[1];
				callableStatement.setString(1, studentId);
				callableStatement.setString(2, xCode.getAttCode());
				callableStatement.setString(3, studentId);
				callableStatement.setString(4, xCode.getOverComments());
				callableStatement.setString(5, indicator);
				callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);
				callableStatement.execute();
				status = callableStatement.getString(6);
				if(!("SUCCESS".equals(status))){
					throw new Exception(status);
				}
			}
			callableStatement.close();
			response.setStatus(status.toLowerCase());

		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}
	
	public CodeResponse updateAttendance(Code xCode, String values)
			throws Exception {
		CodeResponse response = new CodeResponse();
		String status = "Success";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			callableStatement = session.connection()
					.prepareCall(
							"{call "
									+ AppUtil
											.getProperty("P_UPDATE_ATTENDANCE")
									+ "}");
			String[] attValues = values.split(";");
			for (int i = 0; i < attValues.length; i++) {
				String[] studentValue = attValues[i].split(",");
				String studentId = studentValue[0];
				String attId = studentValue[1];
				callableStatement.setString(1, studentId);
				callableStatement.setString(2, attId);
				callableStatement.setString(3, xCode.getOverWriteBy());
				callableStatement.setString(4, xCode.getOverComments());
				callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);
				callableStatement.execute();
				status = callableStatement.getString(5);
				if(!("SUCCESS".equals(status))){
					throw new Exception(status);
				}
			}
			callableStatement.close();
			response.setStatus(status.toLowerCase());

		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			response.setStatus(status);
			return response;
		}
		return response;
	}

	public ClassRoasterList viewClassRoaster(Code xCode) throws Exception {
		ClassRoasterList studentList = new ClassRoasterList();
		String status = "SUCCESS";
		try {

			CodeResponse response = genarateCode(xCode);
			String sql = AppUtil.getProperty("Q_VIEW_CLASSROASTER");

			sql = (AppUtil.format(sql, xCode.getTermCode(), xCode.getCrn(),
					xCode.getStartTime(), xCode.getEndTime()));
			List<ClassRoasterBo> l = (List<ClassRoasterBo>) AppUtil
					.runAQueryCurrentSession(sql, ClassRoasterBo.class);
			for (ClassRoasterBo classBo : l) {
				JClassRoaster classView = new JClassRoaster();
				classView.setStudentId(classBo.getStudentId());
				classView.setName(classBo.getName());
				classView.setTermCode(classBo.getTermCode());
				classView.setCrn(classBo.getCrn());
				classView.setEmail(classBo.getEmail());
				classView.setAttIndicator(classBo.getAttIndicator());
				classView.setCode(classBo.getAttendenceCode());
				studentList.getRoaster().add(classView);
				studentList.setStatus(status);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
			status = e.getMessage();
			studentList.setStatus(status);
			return studentList;
		}
		return studentList;

	}

	public CodeResponse sendMessage(JNotificationLog jNotificationLog,
			String crn, String termCode) {
		CodeResponse response = new CodeResponse();
		String status = "Success";
		JPeopleList peopleList = null;
		try {
			if (!crn.equals("")) {
				peopleList = registrationService.getClassmates(termCode, crn);

				List<JPeople> people = peopleList.getFacultyList();
				String ids = "";
				for (int i = 0; i < people.size(); i++) {
					String pepId = people.get(i).id;
					if (ids.equals("")) {
						ids = pepId;
					} else {
						ids = ids + "," + pepId;
					}
				}
				Calendar calendar = Calendar.getInstance();
				DateFormat formatter = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm:ss");
				String str = formatter.format(calendar.getTime());
				calendar.add(Calendar.MONTH, 1);
				String strAfterAddMonth = formatter.format(calendar.getTime());
				jNotificationLog.setUserName(ids);
				jNotificationLog.setDueDate(str);
				jNotificationLog.setExpiryDate(strAfterAddMonth);
				notificationService.addAdminNotification(jNotificationLog);
			} else {
				Calendar calendar = Calendar.getInstance();
				DateFormat formatter = new SimpleDateFormat(
						"dd-MM-yyyy HH:mm:ss");
				String str = formatter.format(calendar.getTime());
				calendar.add(Calendar.MONTH, 1);
				String strAfterAddMonth = formatter.format(calendar.getTime());
				jNotificationLog.setDueDate(str);
				jNotificationLog.setExpiryDate(strAfterAddMonth);
				notificationService.addAdminNotification(jNotificationLog);
			}
			response.setStatus(status);
		} catch (Exception ex) {
			status = ex.getMessage();
			response.setStatus(status);
			log.error("StackTrace", ex);
			return response;
		}
		return response;
	}

	public RegisteredCoursesResponse getFacultyCourses(String termCode,
			String facultyId) {
		RegisteredCoursesResponse response = new RegisteredCoursesResponse();
		RegisteredCourseList xRegisteredCourseList = new RegisteredCourseList();
		String status = "SUCCESS";
		try {
			if (termCode == null || termCode.equals("")) {
				// CurrentTerm term = getCurrentTerm();
				// termCode = term.getTermCode();

				String sql = "";
				try {
					sql = AppUtil
							.getProperty("Q_GET_FACULTY_COURSES_DEFAULT_TERM");
				} catch (Exception e) {
					log.error("StackTrace", e);
				}

				sql = (AppUtil.format(sql, facultyId));

				List l = AppUtil.runAQuery(sql);

				termCode = (String) l.get(0);
			}

			String sql = AppUtil.getProperty("Q_GET_FACULTY_COURSES");

			sql = (AppUtil.format(sql, facultyId, termCode));
			List<FacultyCourseBo> l = (List<FacultyCourseBo>) AppUtil
					.runAQueryCurrentSession(sql, FacultyCourseBo.class);
			for (FacultyCourseBo courseBo : l) {
				JRegisteredCourse xRegisteredCourse = new JRegisteredCourse();
				JCourse courseDetails = new JCourse();
				courseDetails.setFaculty(courseBo.getFacultyName());
				courseDetails.setTermCode(courseBo.getTermCode());
				courseDetails.setCrn(courseBo.getCrn());
				courseDetails.setSubject(courseBo.getSubject());
				courseDetails.setDescription(courseBo.getSubjectDesc());
				courseDetails.setCourseTitle(courseBo.getCrseTitle());
				courseDetails.setCampus(courseBo.getCampus());
				courseDetails.setLevel(courseBo.getCourseLevel());
				courseDetails.setDepartment(courseBo.getDepartment());
				courseDetails.setCollege(courseBo.getCollege());
				courseDetails.setTermDescription(courseBo.getTermDesc());
				courseDetails.setCredits(courseBo.getCredits());
				courseDetails.setCourseNumber(courseBo.getCourseNumber());

				JMeetingList xMeetingList = registrationService
						.getMeetingList(courseDetails);
				courseDetails.setMeetingList(xMeetingList);

				xRegisteredCourse.setCourse(courseDetails);
				xRegisteredCourse.setStatusMessage(status);
				xRegisteredCourseList.getRegisteredCourse().add(
						xRegisteredCourse);
			}
			response.setRegisteredCourseList(xRegisteredCourseList);
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in retrieving getFacultyCourses...."
					+ e.getMessage());
		}
		return response;

	}

	public ClassRoasterList viewAttendence(String studentId, String startDate,
			String endDate, String termCode, String crn) {
		ClassRoasterList roastList = new ClassRoasterList();
		try {
			String sql = AppUtil.getProperty("Q_VIEW_ATTENDENCE");
			sql = (AppUtil.format(sql, studentId, startDate, endDate, termCode,
					crn));
			List l = AppUtil.runAQuery(sql);
			for (Object obj : l) {
				Object[] roster = (Object[]) obj;
				JClassRoaster jClass = new JClassRoaster();
				jClass.setDate(AppUtil.getStringValue(roster[0]));
				jClass.setName(AppUtil.getStringValue(roster[1]));
				jClass.setAttIndicator(AppUtil.getStringValue(roster[2]));
				roastList.getRoaster().add(jClass);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in viewAttendence......" + e.getMessage());
		}
		return roastList;
	}

	public ClassRoasterList attendenceHistory(String instructorId,
			String termCode, String crn, String attDate, String classStartTime,
			String classEndTime, String indicator) {
		ClassRoasterList roastList = new ClassRoasterList();
		try {
			String sql = AppUtil.getProperty("Q_ATTENDENCE_BY_DATE");
			sql = (AppUtil.format(sql, instructorId, termCode, crn, attDate,
					classStartTime, classEndTime, indicator));
			List l = AppUtil.runAQuery(sql);
			for (Object obj : l) {
				String roster = (String) obj;
				JClassRoaster jClass = new JClassRoaster();
				jClass.setName(AppUtil.getStringValue(roster));
				roastList.getRoaster().add(jClass);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in retriveing attendence History......"
					+ e.getMessage());
		}
		return roastList;
	}

	public ClassRoasterList getAttendenceDetails(String instructorId,
			String termCode, String crn, String attDate, String classStartTime,
			String classEndTime) throws ParseException {
		ClassRoasterList list = new ClassRoasterList();
		boolean presentFlag = false;
		boolean absentFlag = false;
		try {
			String sql = AppUtil.getProperty("Q_ABSENT_BY_CLASS");
			sql = (AppUtil.format(sql, termCode, crn, instructorId, termCode,
					crn, attDate, classStartTime, classEndTime));
			List l = AppUtil.runAQuery(sql);
			if (l.size() == 3) {
				for (Object obj : l) {
					Object[] details = (Object[]) obj;
					JClassRoaster roster = new JClassRoaster();
					roster.setName(AppUtil.getStringValue(details[1]));
					roster.setCount(AppUtil.getStringValue(details[0]));
					String presentStudents = AppUtil.getStringValue(details[1]);
					if (presentStudents.equalsIgnoreCase("Present Students")) {
						list.getRoaster().add(1, roster);
					}
					if (presentStudents.equalsIgnoreCase("Absent Students")) {
						list.getRoaster().add(roster);
					}
					if (presentStudents.equalsIgnoreCase("Total Students")) {
						list.getRoaster().add(0, roster);
					}
				}
			} else {

				for (Object obj : l) {
					Object[] details = (Object[]) obj;
					JClassRoaster roster = new JClassRoaster();
					String presentStudents = AppUtil.getStringValue(details[1]);
					if (presentStudents.equalsIgnoreCase("Present Students")) {
						presentFlag = true;
					}
					if (presentStudents.equalsIgnoreCase("Absent Students")) {
						absentFlag = true;
					}
					roster.setName(AppUtil.getStringValue(details[1]));
					roster.setCount(AppUtil.getStringValue(details[0]));
					list.getRoaster().add(roster);
				}
				if (presentFlag == false) {
					JClassRoaster roster = new JClassRoaster();
					roster.setName("Present Students");
					roster.setCount("0");
					list.getRoaster().add(1, roster);
				}
				if (absentFlag == false) {
					JClassRoaster roster = new JClassRoaster();
					roster.setName("Absent Students");
					roster.setCount("0");
					list.getRoaster().add(roster);
				}
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in get attendence details" + e.getMessage());

		}
		return list;

	}

	public ClassRoasterList getIndicatorByDate(String studentId,
			String attDate, String termCode) {
		ClassRoasterList roastList = new ClassRoasterList();

		try {
			String sql = AppUtil.getProperty("Q_GET_INDICATOR");
			sql = (AppUtil.format(sql, studentId, attDate, termCode));
			List l = AppUtil.runAQuery(sql);
			for (Object obj : l) {
				Object[] roster = (Object[]) obj;
				JClassRoaster jClass = new JClassRoaster();
				jClass.setDate(AppUtil.getStringValue(roster[0]));
				jClass.setName(AppUtil.getStringValue(roster[1]));
				jClass.setAttIndicator(AppUtil.getStringValue(roster[2]));
				jClass.setCrn(AppUtil.getStringValue(roster[3]));
				jClass.setTermCode(AppUtil.getStringValue(roster[4]));
				roastList.getRoaster().add(jClass);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in getIndicatorByDate......" + e.getMessage());
		}
		return roastList;
	}

	public ClassRoasterList getAttandenceReport(String studentId, String crn,
			String termCode, String startTime, String endTime,
			String startDate, String endDate,String username) {
		ClassRoasterList roastList = new ClassRoasterList();
		try {
			if (termCode == null || termCode.equals("")) {
				// CurrentTerm term = getCurrentTerm();
				// termCode = term.getTermCode();

				String sql = "";
				try {
					sql = AppUtil
							.getProperty("Q_GET_MY_COURSES_DEFAULT_TERM");
				} catch (Exception e) {
					log.error("StackTrace", e);
				}

				sql = (AppUtil.format(sql, studentId));

				List l = AppUtil.runAQuery(sql);

				termCode = (String) l.get(0);
			}
			String sql = AppUtil.getProperty("Q_GET_ATTENDANCE_REPORT");

			String condition = "";
			if (studentId != null && !studentId.equals("")) {
				String searchCondition = String.format("student_id = '%s'",
						studentId);
				condition = condition + " and " + searchCondition;
			}
			if (startDate != null && !startDate.equals("") && endDate != null
					&& !endDate.equals("")) {
				String searchCondition = String
						.format("attendence_date between TO_DATE('%s','MM/DD/YYYY') and TO_DATE('%s','MM/DD/YYYY')",
								startDate, endDate);
				condition = condition + " and " + searchCondition;
			}else{
				Code xCode = new Code();
				SimpleDateFormat fmt=new SimpleDateFormat("MM/dd/yyyy");
				xCode.setInstructorId(username);
				xCode.setTermCode(termCode);
				xCode.setCrn(crn);
				xCode.setAttDate(fmt.format(new Date()));
				xCode.setStartTime(startTime);
				xCode.setEndTime(endTime);
				xCode.setCodeExpiryDteTime("0");
				genarateCode(xCode);
				String searchCondition = String
						.format("attendence_date between TO_DATE('%s','MM/DD/YYYY') and TO_DATE('%s','MM/DD/YYYY')",
								fmt.format(new Date()), fmt.format(new Date()));
				condition = condition + " and " + searchCondition;
			}
			condition=condition + " order by NAME ";
			
			sql = (AppUtil.format(sql, termCode, crn, startTime, endTime,condition));
			List l = AppUtil.runAQuery(sql);
			for (Object obj : l) {
				Object[] roster = (Object[]) obj;
				JClassRoaster jClass = new JClassRoaster();
				jClass.setStudentId(AppUtil.getStringValue(roster[0]));
				jClass.setName(AppUtil.getStringValue(roster[1]));
				jClass.setAttIndicator(AppUtil.getStringValue(roster[2]));
				jClass.setDate(AppUtil.getStringValue(roster[3]));
				jClass.setCode(AppUtil.getStringValue(roster[4]));
				roastList.getRoaster().add(jClass);
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error(" Error in getAttandenceReport......" + e.getMessage());
		}
		return roastList;
	}

}
