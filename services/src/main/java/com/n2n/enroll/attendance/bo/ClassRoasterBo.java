package com.n2n.enroll.attendance.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity
public class ClassRoasterBo implements Serializable {
	@Id
	private String id;
	@Column(name = "STUDENT_ID")
	private String studentId;
	@Column(name = "NAME")
	private String name;
	@Column(name = "TERM_CODE")
	private String termCode;
	@Column(name = "CRN")
	private String crn;

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	@Column(name = "EMAIL")
	private String email;
	@Column(name = "ATTENDENCE_IND")
	private String attIndicator;
	@Column(name = "ATTENDENCE_CODE")
	private String attendenceCode;

	public String getAttendenceCode() {
		return attendenceCode;
	}

	public void setAttendenceCode(String attendenceCode) {
		this.attendenceCode = attendenceCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAttIndicator() {
		return attIndicator;
	}

	public void setAttIndicator(String attIndicator) {
		this.attIndicator = attIndicator;
	}

}
