package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

public class JUserList {

	List<JAdminUser> admin = new ArrayList<JAdminUser>(0);

	public List<JAdminUser> getAdmin() {
		return admin;
	}

	public void setAdmin(List<JAdminUser> admin) {
		this.admin = admin;
	}

}
