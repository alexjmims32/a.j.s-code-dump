package com.n2n.enroll.auth.bo;

public class PrivilegesResponse {

	private String status = "";
	private PrivilegesList privilegesList = new PrivilegesList();
	private AdminPrivilegesList privileges = new AdminPrivilegesList();

	public AdminPrivilegesList getPrivileges() {
		return privileges;
	}

	public void setPrivileges(AdminPrivilegesList privileges) {
		this.privileges = privileges;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PrivilegesList getPrivilegesList() {
		return privilegesList;
	}

	public void setPrivilegesList(PrivilegesList privilegesList) {
		this.privilegesList = privilegesList;
	}
}
