//package com.n2n.enroll.auth.dao;
//
//import java.util.List;
//
//import org.hibernate.LockMode;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.ApplicationContext;
//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
//
//import com.n2n.enroll.auth.bo.MobeduServiceConfig;
//
///**
// * A data access object (DAO) providing persistence and search support for
// * MobeduServiceConfig entities. Transaction control of the save(), update() and
// * delete() operations can directly support Spring container-managed
// * transactions or they can be augmented to handle user-managed Spring
// * transactions. Each of these methods provides additional information for how
// * to configure it for the desired type of transaction control.
// * 
// * @see net.n2n.mobedu.config.MobeduServiceConfig
// * @author MyEclipse Persistence Tools
// */
//@SuppressWarnings({"unchecked","rawtypes"})
//public class MobeduServiceConfigDAO extends HibernateDaoSupport {
//	private static final Logger log = LoggerFactory
//			.getLogger(MobeduServiceConfigDAO.class);
//	// property constants
//	public static final String CONFIG_VALUE = "configValue";
//	public static final String CONFIG_CONTEXT = "configContext";
//	public static final String CONFIG_DESCRIPTION = "configDescription";
//
//	protected void initDao() {
//		// do nothing
//	}
//
//	public void save(MobeduServiceConfig transientInstance) {
//		log.debug("saving MobeduServiceConfig instance");
//		try {
//			getHibernateTemplate().save(transientInstance);
//			log.debug("save successful");
//		} catch (RuntimeException re) {
//			log.error("save failed", re);
//			throw re;
//		}
//	}
//
//	public void delete(MobeduServiceConfig persistentInstance) {
//		log.debug("deleting MobeduServiceConfig instance");
//		try {
//			getHibernateTemplate().delete(persistentInstance);
//			log.debug("delete successful");
//		} catch (RuntimeException re) {
//			log.error("delete failed", re);
//			throw re;
//		}
//	}
//
//	public MobeduServiceConfig findById(java.lang.String id) {
//		log.debug("getting MobeduServiceConfig instance with id: " + id);
//		try {
//			MobeduServiceConfig instance = (MobeduServiceConfig) getHibernateTemplate()
//					.get("com.n2n.n2nea.config.MobeduServiceConfig", id);
//			return instance;
//		} catch (RuntimeException re) {
//			log.error("get failed", re);
//			throw re;
//		}
//	}
//
//	public List<MobeduServiceConfig> findByExample(MobeduServiceConfig instance) {
//		log.debug("finding MobeduServiceConfig instance by example");
//		try {
//			List<MobeduServiceConfig> results = (List<MobeduServiceConfig>) getHibernateTemplate()
//					.findByExample(instance);
//			log.debug("find by example successful, result size: "
//					+ results.size());
//			return results;
//		} catch (RuntimeException re) {
//			log.error("find by example failed", re);
//			throw re;
//		}
//	}
//
//	public List findByProperty(String propertyName, Object value) {
//		log.debug("finding MobeduServiceConfig instance with property: "
//				+ propertyName + ", value: " + value);
//		try {
//			String queryString = "from MobeduServiceConfig as model where model."
//					+ propertyName + "= ?";
//			return getHibernateTemplate().find(queryString, value);
//		} catch (RuntimeException re) {
//			log.error("find by property name failed", re);
//			throw re;
//		}
//	}
//
//	
//	public List<MobeduServiceConfig> findByConfigValue(Object configValue) {
//		return findByProperty(CONFIG_VALUE, configValue);
//	}
//
//	public List<MobeduServiceConfig> findByConfigContext(Object configContext) {
//		return findByProperty(CONFIG_CONTEXT, configContext);
//	}
//
//	public List<MobeduServiceConfig> findByConfigDescription(
//			Object configDescription) {
//		return findByProperty(CONFIG_DESCRIPTION, configDescription);
//	}
//
//	public List findAll() {
//		log.debug("finding all MobeduServiceConfig instances");
//		try {
//			String queryString = "from MobeduServiceConfig";
//			return getHibernateTemplate().find(queryString);
//		} catch (RuntimeException re) {
//			log.error("find all failed", re);
//			throw re;
//		}
//	}
//
//	public MobeduServiceConfig merge(MobeduServiceConfig detachedInstance) {
//		log.debug("merging MobeduServiceConfig instance");
//		try {
//			MobeduServiceConfig result = (MobeduServiceConfig) getHibernateTemplate()
//					.merge(detachedInstance);
//			log.debug("merge successful");
//			return result;
//		} catch (RuntimeException re) {
//			log.error("merge failed", re);
//			throw re;
//		}
//	}
//
//	public void attachDirty(MobeduServiceConfig instance) {
//		log.debug("attaching dirty MobeduServiceConfig instance");
//		try {
//			getHibernateTemplate().saveOrUpdate(instance);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public void attachClean(MobeduServiceConfig instance) {
//		log.debug("attaching clean MobeduServiceConfig instance");
//		try {
//			getHibernateTemplate().lock(instance, LockMode.NONE);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public static MobeduServiceConfigDAO getFromApplicationContext(
//			ApplicationContext ctx) {
//		return (MobeduServiceConfigDAO) ctx.getBean("MobeduServiceConfigDAO");
//	}
//}