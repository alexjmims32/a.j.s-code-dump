package com.n2n.enroll.auth.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "PRIVILEGE")
public class Privilege implements Serializable {
	@Id
	@Column(name = "CODE")
	private String moduleCode;
	@Column(name = "ACCESSFLAG")
	private String accessFlag;
	@Column(name = "AUTHREQUIRED")
	private String authRequired;
	@Column(name = "ICON")
	private String icon;
	@Column(name = "POSITION")
	private String position;
	@Column(name = "DESCRIPTION")
	private String moduledescription;

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getModuledescription() {
		return moduledescription;
	}

	public void setModuledescription(String moduledescription) {
		this.moduledescription = moduledescription;
	}

	public String getAuthRequired() {
		return authRequired;
	}

	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getAccessFlag() {
		return accessFlag;
	}

	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}

}
