package com.n2n.enroll.auth.bo;

public class JModule {
	private String id;
	private String code;
	private String description;
	private String authRequired;
	private String icon;
	private String position;
	private String showByDefault;

	public String getShowByDefault() {
		return showByDefault;
	}

	public void setShowByDefault(String showByDefault) {
		this.showByDefault = showByDefault;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthRequired() {
		return authRequired;
	}

	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

}
