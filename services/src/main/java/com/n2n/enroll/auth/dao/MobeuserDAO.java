//package com.n2n.enroll.auth.dao;
//
//import java.util.List;
//
//import org.hibernate.LockMode;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.ApplicationContext;
//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
//
//import com.n2n.enroll.auth.bo.Mobeuser;
//
///**
// * A data access object (DAO) providing persistence and search support for
// * Mobeuser entities. Transaction control of the save(), update() and delete()
// * operations can directly support Spring container-managed transactions or they
// * can be augmented to handle user-managed Spring transactions. Each of these
// * methods provides additional information for how to configure it for the
// * desired type of transaction control.
// * 
// * @see net.n2n.mobedu.auth.Mobeuser
// * @author MyEclipse Persistence Tools
// */
//@SuppressWarnings({"unchecked","rawtypes"})
//public class MobeuserDAO extends HibernateDaoSupport {
//	private static final Logger log = LoggerFactory
//			.getLogger(MobeuserDAO.class);
//	// property constants
//	public static final String USERNAME = "username";
//	public static final String AUTHSOURCE = "authsource";
//	public static final String PASSWORD = "password";
//	public static final String EMAIL = "email";
//	public static final String SMS = "sms";
//	public static final String ACTIVE = "active";
//
//	protected void initDao() {
//		// do nothing
//	}
//
//	public void save(Mobeuser transientInstance) {
//		log.debug("saving Mobeuser instance");
//		try {
//			getHibernateTemplate().save(transientInstance);
//			log.debug("save successful");
//		} catch (RuntimeException re) {
//			log.error("save failed", re);
//			throw re;
//		}
//	}
//
//	public void delete(Mobeuser persistentInstance) {
//		log.debug("deleting Mobeuser instance");
//		try {
//			getHibernateTemplate().delete(persistentInstance);
//			log.debug("delete successful");
//		} catch (RuntimeException re) {
//			log.error("delete failed", re);
//			throw re;
//		}
//	}
//
//	public Mobeuser findById(Long id) {
//		log.debug("getting Mobeuser instance with id: " + id);
//		try {
//			Mobeuser instance = (Mobeuser) getHibernateTemplate().get(
//					"com.n2n.ea.auth.Mobeuser", id);
//			return instance;
//		} catch (RuntimeException re) {
//			log.error("get failed", re);
//			throw re;
//		}
//	}
//
//	public List<Mobeuser> findByExample(Mobeuser instance) {
//		log.debug("finding Mobeuser instance by example");
//		try {
//			List<Mobeuser> results = (List<Mobeuser>) getHibernateTemplate()
//					.findByExample(instance);
//			log.debug("find by example successful, result size: "
//					+ results.size());
//			return results;
//		} catch (RuntimeException re) {
//			log.error("find by example failed", re);
//			throw re;
//		}
//	}
//
//	public List findByProperty(String propertyName, Object value) {
//		log.debug("finding Mobeuser instance with property: " + propertyName
//				+ ", value: " + value);
//		try {
//			String queryString = "from Mobeuser as model where model."
//					+ propertyName + "= ?";
//			return getHibernateTemplate().find(queryString, value);
//		} catch (RuntimeException re) {
//			log.error("find by property name failed", re);
//			throw re;
//		}
//	}
//
//	
//	public List<Mobeuser> findByUsername(Object username) {
//		return findByProperty(USERNAME, username);
//	}
//
//	public List<Mobeuser> findByAuthsource(Object authsource) {
//		return findByProperty(AUTHSOURCE, authsource);
//	}
//
//	public List<Mobeuser> findByPassword(Object password) {
//		return findByProperty(PASSWORD, password);
//	}
//
//	public List<Mobeuser> findByEmail(Object email) {
//		return findByProperty(EMAIL, email);
//	}
//
//	public List<Mobeuser> findBySms(Object sms) {
//		return findByProperty(SMS, sms);
//	}
//
//	public List<Mobeuser> findByActive(Object active) {
//		return findByProperty(ACTIVE, active);
//	}
//
//	public List findAll() {
//		log.debug("finding all Mobeuser instances");
//		try {
//			String queryString = "from Mobeuser";
//			return getHibernateTemplate().find(queryString);
//		} catch (RuntimeException re) {
//			log.error("find all failed", re);
//			throw re;
//		}
//	}
//
//	public Mobeuser merge(Mobeuser detachedInstance) {
//		log.debug("merging Mobeuser instance");
//		try {
//			Mobeuser result = (Mobeuser) getHibernateTemplate().merge(
//					detachedInstance);
//			log.debug("merge successful");
//			return result;
//		} catch (RuntimeException re) {
//			log.error("merge failed", re);
//			throw re;
//		}
//	}
//
//	public void attachDirty(Mobeuser instance) {
//		log.debug("attaching dirty Mobeuser instance");
//		try {
//			getHibernateTemplate().saveOrUpdate(instance);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public void attachClean(Mobeuser instance) {
//		log.debug("attaching clean Mobeuser instance");
//		try {
//			getHibernateTemplate().lock(instance, LockMode.NONE);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public static MobeuserDAO getFromApplicationContext(ApplicationContext ctx) {
//		return (MobeuserDAO) ctx.getBean("MobeuserDAO");
//	}
//}