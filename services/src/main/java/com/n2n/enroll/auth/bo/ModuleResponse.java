package com.n2n.enroll.auth.bo;

public class ModuleResponse {
	private String status;
	private ModulesList modulesList = new ModulesList();

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ModulesList getModulesList() {
		return modulesList;
	}
	public void setModulesList(ModulesList modulesList) {
		this.modulesList = modulesList;
	}

}
