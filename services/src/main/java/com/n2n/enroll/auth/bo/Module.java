package com.n2n.enroll.auth.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="MODULE")
public class Module implements Serializable{

	private static final long serialVersionUID = 6252168011650887816L;

	@Id
	@Column(name="ID")
	private long id;
	
	@Column(name="CODE")
	private String code;
	
	@Column(name="DESCRIPTION")
	private String description;

	@Column(name="AUTHREQUIRED")
	private String authRequired;

	@Column(name="ICON")
	private String icon;

	@Column(name="SHOWBYDEFAULT")
	private String showByDefault;
	
	@Column(name="CAMPUSCODE")
	private String campusCode;

	@Column(name="POSITION")
	private String position;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthRequired() {
		return authRequired;
	}

	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getShowByDefault() {
		return showByDefault;
	}

	public void setShowByDefault(String showByDefault) {
		this.showByDefault = showByDefault;
	}

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}
	
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
}
