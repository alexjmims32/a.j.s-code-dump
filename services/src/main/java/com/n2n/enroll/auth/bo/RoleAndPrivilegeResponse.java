package com.n2n.enroll.auth.bo;

public class RoleAndPrivilegeResponse {
	private RoleAndPrivilegeList roleList = new RoleAndPrivilegeList();

	public RoleAndPrivilegeList getRoleList() {
		return roleList;
	}

	public void setRoleList(RoleAndPrivilegeList roleList) {
		this.roleList = roleList;
	}
}
