package com.n2n.enroll.auth.bo;


public class AdminLoginServiceResponse {

	protected boolean success;
	protected LoginStatus loginStatus;
	protected EumerationList enumerationList;
	protected AdminPrivilegesList privilegesList;

	protected UserSettingsList settingsList;
	protected MappingsList mappingsList;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public AdminPrivilegesList getPrivilegesList() {
		return privilegesList;
	}

	public void setPrivilegesList(AdminPrivilegesList privilegesList) {
		this.privilegesList = privilegesList;
	}

	public LoginStatus getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(LoginStatus loginStatus) {
		this.loginStatus = loginStatus;
	}

	public EumerationList getEnumerationList() {
		return enumerationList;
	}

	public void setEnumerationList(EumerationList enumerationList) {
		this.enumerationList = enumerationList;
	}

	public UserSettingsList getSettingsList() {
		return settingsList;
	}

	public void setSettingsList(UserSettingsList settingsList) {
		this.settingsList = settingsList;
	}

	public MappingsList getMappingsList() {
		return mappingsList;
	}

	public void setMappingsList(MappingsList mappingsList) {
		this.mappingsList = mappingsList;
	}
}
