package com.n2n.enroll.auth.bo;

public class LoginStatus {

	protected String status;
	protected String detailedStatus;
	protected String sessionKey1;
	protected String sessionKey2;
	protected String role;
	protected String firstName;
	protected String lastName;
	protected String userName;
	protected String applicationId;

	public String getStatus() {
		return status;
	}

	public void setStatus(String value) {
		this.status = value;
	}

	public String getDetailedStatus() {
		return detailedStatus;
	}

	public void setDetailedStatus(String value) {
		this.detailedStatus = value;
	}

	public String getSessionKey1() {
		return sessionKey1;
	}

	public void setSessionKey1(String value) {
		this.sessionKey1 = value;
	}

	public String getSessionKey2() {
		return sessionKey2;
	}

	public void setSessionKey2(String value) {
		this.sessionKey2 = value;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String value) {
		this.role = value;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String value) {
		this.firstName = value;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String value) {
		this.lastName = value;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String value) {
		this.userName = value;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String value) {
		this.applicationId = value;
	}

}
