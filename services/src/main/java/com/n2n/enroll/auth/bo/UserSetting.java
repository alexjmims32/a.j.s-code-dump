package com.n2n.enroll.auth.bo;

public class UserSetting {

	protected String settingType;
	protected String settingValue;

	/**
	 * Gets the value of the settingType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSettingType() {
		return settingType;
	}

	/**
	 * Sets the value of the settingType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSettingType(String value) {
		this.settingType = value;
	}

	/**
	 * Gets the value of the settingValue property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getSettingValue() {
		return settingValue;
	}

	/**
	 * Sets the value of the settingValue property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setSettingValue(String value) {
		this.settingValue = value;
	}

}
