package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

public class RoleAndPrivilegeList {
	private List<JRoleAndPrivilege> role = new ArrayList<JRoleAndPrivilege>(0);

	public List<JRoleAndPrivilege> getRole() {
		return role;
	}

	public void setRole(List<JRoleAndPrivilege> role) {
		this.role = role;
	}
}
