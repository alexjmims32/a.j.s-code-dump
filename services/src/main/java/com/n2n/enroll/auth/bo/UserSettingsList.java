package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

public class UserSettingsList {

	protected List<UserSetting> userSetting;

	/**
	 * Gets the value of the userSetting property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a
	 * snapshot. Therefore any modification you make to the returned list will
	 * be present inside the JAXB object. This is why there is not a
	 * <CODE>set</CODE> method for the userSetting property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getUserSetting().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link XUserSetting }
	 * 
	 * 
	 */
	public List<UserSetting> getUserSetting() {
		if (userSetting == null) {
			userSetting = new ArrayList<UserSetting>();
		}
		return this.userSetting;
	}

}
