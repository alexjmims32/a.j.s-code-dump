package com.n2n.enroll.auth.bo;

public class StatusResponse {

	private String status="";

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
