package com.n2n.enroll.auth.bo;

public class Enumeration {

	protected String enumType;
	protected String enumValue;
	protected String displayValue;
	protected String sequence;

	public String getEnumType() {
		return enumType;
	}

	public void setEnumType(String value) {
		this.enumType = value;
	}

	public String getEnumValue() {
		return enumValue;
	}

	public void setEnumValue(String value) {
		this.enumValue = value;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String value) {
		this.displayValue = value;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String value) {
		this.sequence = value;
	}

}
