package com.n2n.enroll.auth.bo;

public class Mapping {

	protected String mappingType;

	protected String mappingFrom;

	protected String mappingTo;

	/**
	 * Gets the value of the mappingType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMappingType() {
		return mappingType;
	}

	/**
	 * Sets the value of the mappingType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMappingType(String value) {
		this.mappingType = value;
	}

	/**
	 * Gets the value of the mappingFrom property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMappingFrom() {
		return mappingFrom;
	}

	/**
	 * Sets the value of the mappingFrom property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMappingFrom(String value) {
		this.mappingFrom = value;
	}

	/**
	 * Gets the value of the mappingTo property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMappingTo() {
		return mappingTo;
	}

	/**
	 * Sets the value of the mappingTo property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMappingTo(String value) {
		this.mappingTo = value;
	}

}
