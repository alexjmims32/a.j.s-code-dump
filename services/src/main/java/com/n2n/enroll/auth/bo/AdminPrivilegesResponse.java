package com.n2n.enroll.auth.bo;

public class AdminPrivilegesResponse {

	private String status = "";
	private AdminPrivilegesList privilegesList = new AdminPrivilegesList();

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public AdminPrivilegesList getPrivilegesList() {
		return privilegesList;
	}

	public void setPrivilegesList(AdminPrivilegesList privilegesList) {
		this.privilegesList = privilegesList;
	}
}
