package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

public class PrivilegesList {
		private List<Privilege> privilege = new ArrayList<Privilege>(0);

		public List<Privilege> getPrivilege() {
			return privilege;
		}

		public void setPrivilege(List<Privilege> privilege) {
			this.privilege = privilege;
		}
}
