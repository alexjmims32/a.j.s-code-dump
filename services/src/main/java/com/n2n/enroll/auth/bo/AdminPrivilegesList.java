package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

public class AdminPrivilegesList {
	private List<JPrivilegeAdmin> privilege = new ArrayList<JPrivilegeAdmin>(0);
	private String activeFlag;

	public String getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public List<JPrivilegeAdmin> getPrivilege() {
		return privilege;
	}

	public void setPrivilege(List<JPrivilegeAdmin> privilege) {
		this.privilege = privilege;
	}

}
