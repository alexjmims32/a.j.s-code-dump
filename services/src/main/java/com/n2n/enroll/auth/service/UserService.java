package com.n2n.enroll.auth.service;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.directory.DirContext;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.AbstractContextMapper;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.stereotype.Service;

import com.n2n.enroll.auth.bo.AdminLoginServiceResponse;
import com.n2n.enroll.auth.bo.AdminPrivilege;
import com.n2n.enroll.auth.bo.AdminPrivilegesList;
import com.n2n.enroll.auth.bo.AdminUser;
import com.n2n.enroll.auth.bo.Enumeration;
import com.n2n.enroll.auth.bo.EumerationList;
import com.n2n.enroll.auth.bo.JAdminUser;
import com.n2n.enroll.auth.bo.JModule;
import com.n2n.enroll.auth.bo.JPrivilege;
import com.n2n.enroll.auth.bo.JPrivilegeAdmin;
import com.n2n.enroll.auth.bo.JRoleAndPrivilege;
import com.n2n.enroll.auth.bo.JUserList;
import com.n2n.enroll.auth.bo.LoginServiceResponse;
import com.n2n.enroll.auth.bo.LoginStatus;
import com.n2n.enroll.auth.bo.Mapping;
import com.n2n.enroll.auth.bo.MappingsList;
import com.n2n.enroll.auth.bo.Module;
import com.n2n.enroll.auth.bo.ModuleResponse;
import com.n2n.enroll.auth.bo.Modules;
import com.n2n.enroll.auth.bo.ModulesList;
import com.n2n.enroll.auth.bo.Privilege;
import com.n2n.enroll.auth.bo.PrivilegesList;
import com.n2n.enroll.auth.bo.PrivilegesResponse;
import com.n2n.enroll.auth.bo.RoleAndPrivilege;
import com.n2n.enroll.auth.bo.RoleAndPrivilegeList;
import com.n2n.enroll.auth.bo.RoleAndPrivilegeResponse;
import com.n2n.enroll.auth.bo.StatusResponse;
import com.n2n.enroll.auth.bo.UpdateRoleBo;
import com.n2n.enroll.contact.bo.JContact;
import com.n2n.enroll.contact.bo.JPersonDetails;
import com.n2n.enroll.contact.bo.PersonList;
import com.n2n.enroll.contact.service.ContactService;
import com.n2n.enroll.ldap.LdapContact;
import com.n2n.enroll.ldap.PersonDao;
import com.n2n.enroll.util.AppUtil;

@SuppressWarnings({ "unchecked", "deprecation" })
@Service
public class UserService {

	@Resource
	SessionFactory sessionFactory;

	@Resource
	ContextSource contextSource;

	@Resource
	LdapTemplate ldapTemplate;

	@Resource
	PersonDao personDao;

	@Resource
	ContactService contactService;

	public static final String AUTH_LDAP = "LDAP";
	public static final String AUTH_BANNER = "BANNER";

	private String CURR_AUTH_SOURCE = null;

	protected final Log log = LogFactory.getLog(getClass());

	public UserService() {
		super();
	}
	
	public LoginServiceResponse loadUserByUsername(String username) {
		return loadUserByUsername(username, "");
	}

	public LoginServiceResponse loadUserByUsername(String username, String role) {
		LoginServiceResponse loginServiceResponse = new LoginServiceResponse();
		try {

			LoginStatus xLoginStatus = new LoginStatus();

			xLoginStatus.setStatus("SUCCESS");
			xLoginStatus.setDetailedStatus("");
			xLoginStatus.setSessionKey1("");
			xLoginStatus.setSessionKey2("");
			xLoginStatus.setRole(role);
			xLoginStatus.setFirstName("");
			xLoginStatus.setLastName("");
			xLoginStatus.setUserName("");
			xLoginStatus.setApplicationId(username);

			loginServiceResponse.setLoginStatus(xLoginStatus);

			EumerationList xEumerationList = new EumerationList();
			Enumeration xEnumeration = new Enumeration();

			xEnumeration.setEnumType("");
			xEnumeration.setEnumType("");
			xEnumeration.setDisplayValue("");
			xEnumeration.setSequence("");

			xEumerationList.getEnumeration().add(xEnumeration);
			loginServiceResponse.setEnumerationList(xEumerationList);

			JPrivilege jPrivilege = new JPrivilege();
			AdminPrivilegesList adminprivList = adminPriv(username);

			// Role ID 1 for student
			PrivilegesResponse pr = getPrivileges(role);

			jPrivilege.setType("MODULE_ACCESS");
			jPrivilege.setValue(pr.getPrivilegesList().getPrivilege());

			loginServiceResponse.getPrivilegesList().add(jPrivilege);

			jPrivilege = new JPrivilege();
			jPrivilege.setType("ADMIN_ACCESS");
			jPrivilege.setValue(adminprivList);

			loginServiceResponse.getPrivilegesList().add(jPrivilege);

			if (isProxyRegEnabled()) {
				String proxyAccessFlag = getProxyAccessFlag(username);
				if (proxyAccessFlag != null) {
					JPrivilege proxyRegPriv = new JPrivilege();
					proxyRegPriv.setType("PROXY_REG");
					proxyRegPriv.setValue(true);
					proxyRegPriv.setAccessFlag(proxyAccessFlag);
					loginServiceResponse.getPrivilegesList().add(proxyRegPriv);
				}
			}

			MappingsList xMappingsList = new MappingsList();
			Mapping xMapping = new Mapping();
			xMapping.setMappingType("");
			xMapping.setMappingFrom("");
			xMapping.setMappingTo("");
			xMappingsList.getMapping().add(xMapping);

			loginServiceResponse.setMappingsList(xMappingsList);
			loginServiceResponse.setSuccess(true);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return loginServiceResponse;

		// Collection<GrantedAuthority> authorities = new
		// ArrayList<GrantedAuthority>(
		// 0);
		//
		// User userReturn = null;
		//
		// List<Mobeuser> users = mobeuserDAO.findByUsername(username);
		//
		// if (users.size() > 0) {
		// Mobeuser user = users.get(0);
		// authorities.add(new GrantedAuthorityImpl(user.getMoberole()
		// .getTitle()));
		// userReturn = new User(user.getUsername(), user.getPassword(), true,
		// true, true, true, authorities);
		// } else {
		// throw new UsernameNotFoundException("Invalid username or password");
		// }
		//
		// return userReturn;
	}

	public List<Privilege> getPrivileges() {
		List<Privilege> privileges = null;
		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_GET_PRIVILEGES");

			sql = AppUtil.format(sql);
			privileges = (List<Privilege>) AppUtil.runAQueryCurrentSession(sql,
					Privilege.class);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return privileges;
	}

	public String getListToString(List<Privilege> privilegesList) {
		String value = "";

		for (Iterator i = privilegesList.iterator(); i.hasNext();) {
			Privilege privilege = (Privilege) i.next();
			String moduleCode = privilege.getModuleCode().toString() + ",";
			value += moduleCode;
		}
		value = value.substring(0, value.length() - 1);
		return value;
	}

	// Method to authenticate a user
	// Authentication details sent in Auth Header
	// Returns username if authentication is successful
	public String authenticateUser(String authHeader) throws Exception {

		try {
			LoginStatus status = authenticateUser(authHeader, false);
			if ("ACCESS SUCCESSFUL".equalsIgnoreCase(status.getStatus())) {
				return status.getUserName();
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("Authentication Error: " + e.getMessage());
			log.error("StackTrace", e);
		}
		return null;
	}

	public LoginStatus authenticateUser(String authHeader, Boolean isReturnRole)
			throws Exception {

		log.debug("Authorization header: " + authHeader);
		LoginStatus status = new LoginStatus();
		String username = "";
		String password = "";
		String role = "";
		try {
			if (authHeader == null || authHeader.equals("")) {
				log.debug("Not using auth sending 401 not authorized");
				status.setStatus("UNAUTHORIZED");
				return status;
			} else if (authHeader.startsWith("Basic")) {
				String userInfo = authHeader.substring(6).trim();

				String nameAndPassword;

				byte[] nameAndPasswordBytes = DatatypeConverter
						.parseBase64Binary(userInfo);
				nameAndPassword = new String(nameAndPasswordBytes);

				int index = nameAndPassword.indexOf(":");
				username = nameAndPassword.substring(0, index);
				password = nameAndPassword.substring(index + 1);
			} else {
				log.info("Using unsupported authentication type");
				status.setStatus("UNAUTHORIZED");
				return status;
			}

			if (getAuthSource().equals(UserService.AUTH_LDAP)) {
				status = validateLDAPUser(username, password);
			} else {
				status = validateBannerUser(username, password);
			}

		} catch (Exception e) {
			log.error("Authentication Error: " + e.getMessage());
			log.error("StackTrace", e);
			status.setStatus("ERROR IN LOGIN");
		}
		return status;
	}

	public String authenticateAdminUser(String authHeader) throws Exception {

		log.debug("Authorization header: " + authHeader);

		String authenticationType = AppUtil
				.getProperty("ADMIN_AUTHENTICATION_TYPE");
		try {
			String username = null;
			if ("DB".equalsIgnoreCase(authenticationType)) {
				username = authenticateAdminUserInDB(authHeader);
				return username;
			} else {
				username = authenticateUser(authHeader);
				String adminUserGroup = AppUtil
						.getProperty("LDAP_ADMIN_USER_GROUP_NAME");

				// If we need to check an LDAP group for admin console access
				if (username != null && adminUserGroup != null) {
					LdapContact c = personDao.getUser(username);
					for (String group : c.getMemberOf()) {
						if (group.contains(adminUserGroup)) {
							initializeAdminUserPrivileges(username);
							return username;
						}
					}
					// User is valid in LDAP but not part of the required group
					// So send UNAUTHORIZED
					return null;
				}
			}

			return username;

		} catch (Exception e) {
			log.error("Login authentication Error");
			log.error("StackTrace", e);
		}
		return null;
	}

	public String authenticateAdminUserInDB(String authHeader) {

		String credentials = authHeader.substring(6).trim();

		String nameAndPassword;

		byte[] nameAndPasswordBytes = DatatypeConverter
				.parseBase64Binary(credentials);
		nameAndPassword = new String(nameAndPasswordBytes);

		int index = nameAndPassword.indexOf(":");
		String username = nameAndPassword.substring(0, index);

		try {
			String sql = AppUtil.getProperty("Q_ADMIN_LOGIN");

			sql = (AppUtil.format(sql, username, credentials));

			List l = AppUtil.runAQuery(sql);
			if (l.size() != 0) {
				return username;
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
		}

		return null;
	}

	public LoginStatus validateBannerUser(String username, String p_pin)
			throws Exception {

		String status = "";
		String role = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		LoginStatus xLoginStatus = new LoginStatus();
		try {

			callableStatement = session.connection().prepareCall(
					"call " + AppUtil.getProperty("P_LOGIN"));
			callableStatement.setString(1, username.toUpperCase());
			callableStatement.setString(2, p_pin);
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.execute();
			// log.debug("Valid Login: " + callableStatement.getString(5));
			// log.debug("reg: " + callableStatement.getString(3));
			// log.debug("bursur: " + callableStatement.getString(4));
			status = callableStatement.getString(4);
			role = callableStatement.getString(3);
			callableStatement.close();
			xLoginStatus.setStatus(status);
			if("ACCESS SUCCESSFUL".equals(status)){
				xLoginStatus.setRole(role);
				xLoginStatus.setUserName(username.toUpperCase());
			}

		} catch (SQLException e) {
			log.error("Error validating banner user:" + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				xLoginStatus.setStatus("Error");
			}
			log.error("StackTrace", e);
		}

		return xLoginStatus;
	}

	private LoginStatus validateLDAPUser(String userDn, String credentials) {
		LoginStatus status = new LoginStatus();
		DirContext ctx = null;
		try {
			LdapContact c = personDao.getUser(userDn);
			String userDnFull = c.getFullDn();
			if (userDnFull == null) {
				userDnFull = getFullDnForUser(userDn);
			}
			log.debug("DN for user: " + userDn + " : " + userDnFull);
			ctx = contextSource.getContext(userDnFull, credentials);
			status.setUserName(userDn);
			status.setRole(c.getRole());
			status.setStatus("ACCESS SUCCESSFUL");
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			// Context creation failed - authentication did not succeed
			log.error("Login failed: " + e.getMessage());
			status.setUserName(null);
			return status;
		} finally {
			// It is imperative that the created DirContext instance is always
			// closed
			LdapUtils.closeContext(ctx);
		}
	}

	// Method to get the configured Authenticaton Source
	// returns BANNER or LDAP
	public String getAuthSource() {

		if (CURR_AUTH_SOURCE == null) {

			// Considering banner as default.
			String authSource = AUTH_BANNER;

			try {
				authSource = AppUtil.getProperty("AUTHENTICATION_TYPE");
			} catch (Exception e) {
				log.error("Error retrieving Authentication source from application properties");
				log.error("StackTrace", e);
			}

			CURR_AUTH_SOURCE = authSource;
		}

		return CURR_AUTH_SOURCE;

		// We will now read authentication source from app properties file.

		// if (CURR_AUTH_SOURCE == null) {
		// try {
		// Session session = sessionFactory.getCurrentSession();
		//
		// Criteria criteria = session
		// .createCriteria(MobeduServiceConfig.class);
		// criteria.add(Restrictions.eq("configKey", "AUTHENTICATION"));
		// criteria.add(Restrictions.eq("configContext", "AUTH"));
		// MobeduServiceConfig mobeduServiceConfig = (MobeduServiceConfig)
		// criteria
		// .uniqueResult();
		// if (mobeduServiceConfig != null) {
		// CURR_AUTH_SOURCE = mobeduServiceConfig.getConfigValue();
		// }
		// else{
		// throw new
		// RuntimeException("No config found in the SERVICE_CONFIG table for AUTHENTICATION");
		// }
		//
		// } catch (RuntimeException e) {
		// log.error("Error retrieving Authentication Mechanism:" +
		// e.getMessage());
		// }
		// }
		// return CURR_AUTH_SOURCE;

		// return AUTH_LDAP;
	}

	// Method to check the current authentication type
	// If Auth Source is LDAP then Return BannerID from LDAP
	// If Auth Source is Banner then just return the username provided.

	public ModuleResponse getFeedModules() {

		String query = null;
		List<Modules> l = null;
		ModulesList moduleList = new ModulesList();
		ModuleResponse response = new ModuleResponse();
		try {
			query = AppUtil.getProperty("Q_GET_MODULES");
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting modules " + e.getMessage());
		}
		try {
			l = AppUtil.runAQueryCurrentSession(query, Modules.class);
			moduleList.setModule(l);
			response.setStatus("SUCCESS");
			response.setModulesList(moduleList);
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving modules " + e.getMessage());
		}

		return response;
	}

	public List<JModule> getMainModules(String campusCode) {

		String query = null;
		List<Module> l = null;
		List<JModule> result = new ArrayList<JModule>(0);

		try {
			query = AppUtil.getProperty("Q_GET_MAIN_MODULES");

			if (campusCode != null && !campusCode.equals("")) {
				query += " AND CAMPUSCODE = '" + campusCode + "'";
			}

			query += " order by ID";

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting modules " + e.getMessage());
		}

		try {
			l = AppUtil.runAQueryCurrentSession(query, Module.class);

			for (Module m : l) {
				JModule jM = new JModule();
				jM.setId(AppUtil.getStringValue(m.getId()));
				jM.setCode(m.getCode());
				jM.setDescription(m.getDescription());
				jM.setIcon(m.getIcon());
				jM.setAuthRequired(m.getAuthRequired());
				jM.setPosition(m.getPosition());
				result.add(jM);
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving modules " + e.getMessage());
		}

		return result;
	}

	public List<JModule> getModules(String campusCode) {

		String query = null;
		List<Module> l = null;
		List<JModule> result = new ArrayList<JModule>(0);

		try {
			query = AppUtil.getProperty("Q_GET_ADMIN_MODULES");

			if (campusCode != null && !campusCode.equals("")) {
				query += " where CAMPUSCODE = '" + campusCode + "'";
			}

			query += " order by ID";

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting modules " + e.getMessage());
		}

		try {
			l = AppUtil.runAQueryCurrentSession(query, Module.class);

			for (Module m : l) {
				JModule jM = new JModule();
				jM.setId(AppUtil.getStringValue(m.getId()));
				jM.setCode(m.getCode());
				jM.setDescription(m.getDescription());
				jM.setIcon(m.getIcon());
				jM.setAuthRequired(m.getAuthRequired());
				jM.setPosition(m.getPosition());
				jM.setShowByDefault(m.getShowByDefault());
				result.add(jM);
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving modules " + e.getMessage());
		}

		return result;
	}

	public PrivilegesResponse getPrivileges(String role) {

		String query = null;
		List<Privilege> l = null;
		PrivilegesList privilegesList = new PrivilegesList();
		PrivilegesResponse response = new PrivilegesResponse();

		try {
			query = AppUtil.getProperty("Q_GET_PRIVILEGES_DETAILS");
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in getting privilegs " + e.getMessage());
		}
		try {
			String[] roleString =role.split(",");
			String condition="";
			for(int i = 0; i<roleString.length; i++){
				if(condition.equals("")){
					condition="'"+roleString[i]+"'";
				}else{
					condition=condition+","+"'"+roleString[i]+"'";
				}
			}
			query = (AppUtil.format(query, condition));
			l = AppUtil.runAQueryCurrentSession(query, Privilege.class);
			privilegesList.setPrivilege(l);
			response.setPrivilegesList(privilegesList);
			response.setStatus("SUCCESS");
		} catch (Exception e) {
			log.error("StackTrace", e);
			log.error("Error in Retrieving privilegs " + e.getMessage());
		}

		return response;
	}

	public StatusResponse updatePrivilege(String privilegesValues,
			String lastModifiedBy) {
		String status = "";
		StatusResponse statusResponse = new StatusResponse();
		int version = 1;
		if ((privilegesValues == null || privilegesValues.equals(""))) {
			status = "Invalid update privilege values";
		} else {
			try {

				// String privilegesValues="ENQ,Y,Y;ENTER,Y,N";
				String[] privilegesList = privilegesValues.split("\\;");
				for (String privilegeValue : privilegesList) {

					String[] privilege = privilegeValue.split("\\,");
					String moduleCode = privilege[0];
					String accessFlag = privilege[1];
					String authRequired = privilege[2];

					String versionQuery = AppUtil
							.getProperty("Q_GET_TRACKING_PRIV");
					versionQuery = (AppUtil.format(versionQuery, moduleCode));

					List<Privilege> privs = (List<Privilege>) AppUtil
							.runAQueryCurrentSession(versionQuery,
									Privilege.class);
//					for (Privilege pri : privs) {
//						String insertTrck = AppUtil
//								.getProperty("Q_INSERT_TRACKING_PRIV");
//
//						insertTrck = (AppUtil.format(insertTrck,
//								pri.getRoleId(), pri.getModuleCode(),
//								pri.getAccessFlag(), pri.getAuthRequired(),
//								pri.getVersionNo(), pri.getLastModifiedBy()));
//
//						log.info("    :::    " + insertTrck);
//						AppUtil.runADeleteQuery(insertTrck);
//					}
					System.out.println(moduleCode + ' ' + accessFlag + ' '
							+ authRequired);
					version = version + 1;

					String query = AppUtil.getProperty("Q_UPDATE_PRIVILEGES");
					query = (AppUtil.format(query, accessFlag, authRequired,
							version, lastModifiedBy, moduleCode));

					AppUtil.runADeleteQuery(query);
				}
				status = "SUCCESS";
				statusResponse.setStatus(status);

			} catch (Exception e) {
				log.error("Error updating deleted flag on message "
						+ e.getMessage());
				statusResponse.setStatus(e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return statusResponse;
	}

	public AdminLoginServiceResponse adminLogin(String username, String password) {
		AdminLoginServiceResponse response = new AdminLoginServiceResponse();
		AdminPrivilegesList privileges = new AdminPrivilegesList();
		try {

			LoginStatus xLoginStatus = new LoginStatus();
			xLoginStatus.setStatus("SUCCESS");
			response.setLoginStatus(xLoginStatus);
			String sql = AppUtil.getProperty("Q_ADMIN_LOGIN");

			sql = (AppUtil.format(sql, username, password));

			List<AdminPrivilege> l = (List<AdminPrivilege>) AppUtil
					.runAQueryCurrentSession(sql, AdminPrivilege.class);
			for (AdminPrivilege privilege : l) {
				JPrivilegeAdmin jPrivilege = new JPrivilegeAdmin();
				jPrivilege.setUserName(privilege.getUserName());
				jPrivilege.setPrivilegeCode(privilege.getPrivilegeCode());
				jPrivilege.setAccessFlag(privilege.getAccessFlag());
				jPrivilege.setDescription(privilege.getDescription());
				privileges.getPrivilege().add(jPrivilege);
				privileges.setActiveFlag(privilege.getActiveFlag());
			}
			response.setPrivilegesList(privileges);

		} catch (Exception e) {

		}
		return response;
	}

	public JUserList getUserListFromDB() {
		JUserList users = new JUserList();
		try {

			String sql = AppUtil.getProperty("Q_GET_USERS");

			sql = (AppUtil.format(sql));

			List<AdminUser> l = (List<AdminUser>) AppUtil
					.runAQueryCurrentSession(sql, AdminUser.class);
			for (AdminUser user : l) {
				JAdminUser jAdminUser = new JAdminUser();
				jAdminUser.setFirstName(user.getFirstName());
				jAdminUser.setLastName(user.getLastName());
				jAdminUser.setActive(user.getActive());
				jAdminUser.setUsername(user.getUsername());
				jAdminUser.setPassword(user.getPassword());
				AdminPrivilegesList jAdminPrivilegeList = getPrivilegeList(user
						.getUsername());
				jAdminUser.setPrivilegeList(jAdminPrivilegeList);
				users.getAdmin().add(jAdminUser);
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return users;
	}

	public AdminPrivilegesList getPrivilegeList(String username) {
		AdminPrivilegesList privList = new AdminPrivilegesList();
		try {

			String sql = AppUtil.getProperty("Q_GET_ROLE_FOR_UPDATE");

			sql = (AppUtil.format(sql, username));

			List<UpdateRoleBo> l = (List<UpdateRoleBo>) AppUtil
					.runAQueryCurrentSession(sql, UpdateRoleBo.class);
			for (UpdateRoleBo user : l) {
				JPrivilegeAdmin privilege = new JPrivilegeAdmin();
				privilege.setAccessFlag(user.getAccessFlag());
				privilege.setPrivilegeCode(user.getPrivilegeCode());
				privList.getPrivilege().add(privilege);
			}

		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return privList;
	}

	public StatusResponse addUser(JAdminUser adminUser) {
		StatusResponse response = new StatusResponse();
		String status = "SUCCESS";

		if (adminUser == null || adminUser.equals("")) {
			status = "Invalid user Details";
		} else {
			try {

				// String checkQuery = AppUtil.getProperty("Q_ISEXISTS_USER");
				// checkQuery = (checkfmt.format(checkQuery,
				// adminUser.getUsername())).toString();
				// checkfmt.close();
				// List l = AppUtil.runAQuery(checkQuery);
				// if (l.size() > 0) {
				// status = "User is alredy Exist";
				// response.setStatus(status);
				// } else {
				log.info("user object validated successfully For adding");

				String query = AppUtil.getProperty("Q_INSERT_USER");
				query = (AppUtil.format(query, adminUser.getFirstName(),
						adminUser.getLastName(), adminUser.getUsername(),
						adminUser.getActive(), adminUser.getPassword()));

				AppUtil.runADeleteQuery(query);
				response.setStatus(status);
				response = insertRoles(adminUser.getUsername(),
						adminUser.getRole());
				// }
			} catch (Exception e) {
				log.error("Error In inserting user " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return response;
	}

	public StatusResponse updateUser(JAdminUser adminUser) {

		StatusResponse response = new StatusResponse();
		String status = "SUCCESS";

		if (adminUser == null || adminUser.equals("")) {
			status = "Invalid user Details";
			response.setStatus(status);
		} else {
			try {
				log.info("user object validated successfully For adding");

				String query = AppUtil.getProperty("Q_UPDATE_USER");
				query = (AppUtil.format(query, adminUser.getFirstName(),
						adminUser.getLastName(), adminUser.getActive(),
						adminUser.getUsername()));

				AppUtil.runADeleteQuery(query);
				response.setStatus(status);
				response = deleteRoles(adminUser.getUsername());
				if (response.getStatus().equalsIgnoreCase("SUCCESS")) {
					response = insertRoles(adminUser.getUsername(),
							adminUser.getRole());
				}

			} catch (Exception e) {
				log.error("Error In Update the user " + e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return response;
	}

	public StatusResponse insertRoles(String userName, String privileges)
			throws Exception {
		StatusResponse response = new StatusResponse();
		String status = "SUCCESS";
		String[] privilegesList = privileges.split(";");
		for (String roleValue : privilegesList) {

			String[] privilege = roleValue.split(",");
			String privcode = privilege[0];
			String seq_num = privilege[1];
			String access = privilege[2];

			String roleQuery = AppUtil.getProperty("Q_INSERT_ROLE");
			roleQuery = (AppUtil.format(roleQuery, userName, privcode, access,
					privcode, seq_num));

			AppUtil.runADeleteQuery(roleQuery);
			response.setStatus(status);
		}
		return response;
	}

	public StatusResponse deleteRoles(String userName) throws Exception {
		String status = "SUCCESS";
		StatusResponse response = new StatusResponse();
		try {

			String roleQuery = AppUtil.getProperty("Q_DELETE_ROLE");
			roleQuery = (AppUtil.format(roleQuery, userName));

			AppUtil.runADeleteQuery(roleQuery);
			response.setStatus(status);
		} catch (Exception e) {
			log.error("Error In delete roles for the user " + e.getMessage());
			log.error("StackTrace", e);
		}
		return response;

	}

	public RoleAndPrivilegeResponse getAdminPrivilege(String roleId,
			String campusCode) {
		RoleAndPrivilegeResponse response = new RoleAndPrivilegeResponse();
		RoleAndPrivilegeList privilegesList = new RoleAndPrivilegeList();
		String sql = "";
		try {
			sql = AppUtil.getProperty("Q_GET_ADMIN_ROLES");

			sql = AppUtil.format(sql, roleId, campusCode);

			List<RoleAndPrivilege> privileges = (List<RoleAndPrivilege>) AppUtil
					.runAQueryCurrentSession(sql, RoleAndPrivilege.class);
			for (RoleAndPrivilege role : privileges) {
				JRoleAndPrivilege roleAndPriv = new JRoleAndPrivilege();
				roleAndPriv.setRoleId(role.getRoleId());
				roleAndPriv.setModuleCode(role.getModuleCode());
				roleAndPriv.setAccessFlag(role.getAccessFlag());
				roleAndPriv.setAuthRequired(role.getAuthRequired());
				roleAndPriv.setCampusCode(role.getCampusCode());
				roleAndPriv.setDescription(role.getDescription());
				privilegesList.getRole().add(roleAndPriv);
			}
			response.setRoleList(privilegesList);
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return response;
	}

	public StatusResponse changePassword(String username, String password) {

		StatusResponse response = new StatusResponse();
		String status = "SUCCESS";

		if (username == null || username.equals("") || password == null
				|| password.equals("")) {
			status = "Invalid user Details";
		} else {
			try {

				String query = AppUtil.getProperty("Q_CHANGE_PASSWORD");
				query = (AppUtil.format(query, password, username));

				AppUtil.runADeleteQuery(query);
				response.setStatus(status);

			} catch (Exception e) {
				log.error("Error In inserting deleted flag on message "
						+ e.getMessage());
				log.error("StackTrace", e);
			}
		}
		return response;
	}

	public boolean isProxyRegEnabled() {
		boolean result = false;

		try {
			String proxyRegFlag = AppUtil.getProperty("PROXY_REG_ENABLED");

			if ("true".equals(proxyRegFlag)) {
				result = true;
			}

		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	public String getProxyAccessFlag(String username) throws Exception {
		String accessFlag = null;

		log.debug("Pulling user " + username + " from Ldap");
		LdapContact c = personDao.getUser(username);

		String proxyUsers = AppUtil.getProperty("PROXY_USERS");
		String testUsers = AppUtil.getProperty("PROXY_TEST_USERS");

		String[] memberOf = c.getMemberOf();
		if (memberOf.length > 0) {
			for (String member : memberOf) {
				if (member.contains(proxyUsers)) {
					accessFlag = "1";
					break;
				} else if (member.contains(testUsers)) {
					accessFlag = "0";
				}
			}
		}

		return accessFlag;
	}

	public String getBannerId(final String username) {
		String applicationId = null;
		if (getAuthSource().equals(UserService.AUTH_LDAP)) {
			LdapContact c = personDao.getUser(username);
			applicationId = c.getBannerId();
		} else if (getAuthSource().equals(UserService.AUTH_BANNER)) {
			applicationId = username;
		}
		log.debug("Banner id for " + username + " : " + applicationId);
		return applicationId;
	}

	public boolean checkUserExists(String username) {
		boolean userExists = false;
		if (getAuthSource().equals(UserService.AUTH_BANNER)) {
			PersonList personContacts = contactService.getContact(username);
			if (personContacts.getPersonDetails().size() > 0) {
				userExists = true;
			} else {
				userExists = false;
			}
		} else if (getAuthSource().equals(UserService.AUTH_LDAP)) {
			if (getBannerId(username) != null) {
				userExists = true;
			} else {
				userExists = false;
			}
		}
		return userExists;
	}

	public AdminPrivilegesList adminPriv(String username) {
		AdminPrivilegesList privileges = new AdminPrivilegesList();
		try {

			LoginStatus xLoginStatus = new LoginStatus();
			xLoginStatus.setStatus("SUCCESS");
			String sql = AppUtil.getProperty("Q_ADMIN_PRIVILEGE");
			sql = AppUtil.format(sql, username);
			List<AdminPrivilege> l = (List<AdminPrivilege>) AppUtil
					.runAQueryCurrentSession(sql, AdminPrivilege.class);
			for (AdminPrivilege privilege : l) {
				JPrivilegeAdmin jPrivilege = new JPrivilegeAdmin();
				jPrivilege.setUserName(privilege.getUserName());
				jPrivilege.setPrivilegeCode(privilege.getPrivilegeCode());
				jPrivilege.setAccessFlag(privilege.getAccessFlag());
				jPrivilege.setDescription(privilege.getDescription());
				privileges.getPrivilege().add(jPrivilege);
				// privileges.setActiveFlag(privilege.getActiveFlag());
			}

		} catch (Exception e) {

		}
		return privileges;
	}

	public String initializeAdminUserPrivileges(String username) {
		String status = "";
		try {
			String checkQuery = AppUtil.getProperty("Q_ISEXISTS_USER");
			checkQuery = AppUtil.format(checkQuery, username);
			List l = AppUtil.runAQuery(checkQuery);
			if (l.size() == 0) {
				String query = "";
				username = username.toLowerCase();

				String privGrantType = AppUtil
						.getProperty("ADMIN_PRIV_GRANT_TYPE");

				if ("ALL".equalsIgnoreCase(privGrantType)) {
					query = AppUtil.getProperty("Q_INSERT_ROLE_ALL");
					query = AppUtil.format(query, username);
				} else if ("LIMITED".equalsIgnoreCase(privGrantType)) {
					query = AppUtil.getProperty("Q_INSERT_ROLE");
					query = AppUtil.format(query, username, "NOTIFICATIONS",
							"N", "NOTIFICATIONS", 1);
				}

				AppUtil.runADeleteQuery(query);
			} else {
				status = "";
			}
		} catch (Exception e) {
			log.error("Unable to initialize user privileges: " + e.getMessage());
			log.error("StackTrace", e);
		}
		return status;
	}

	public JUserList getUserListFromPriv() {
		JUserList users = new JUserList();
		try {

			String sql = AppUtil.getProperty("Q_DISTINCT_USER");

			List<AdminUser> l = (List<AdminUser>) AppUtil
					.runAQueryCurrentSession(sql, AdminUser.class);
			for (AdminUser privilege : l) {
				JAdminUser jAdminUser = new JAdminUser();
				jAdminUser.setUsername(privilege.getUsername());
				jAdminUser.setFirstName(privilege.getFirstName());
				jAdminUser.setLastName(privilege.getLastName());
				jAdminUser.setActive(privilege.getActive());
				AdminPrivilegesList jAdminPrivilegeList = getPrivilegeList(privilege
						.getUsername());
				jAdminUser.setPrivilegeList(jAdminPrivilegeList);
				users.getAdmin().add(jAdminUser);
			}

		} catch (Exception e) {

		}
		return users;
	}

	// Method to retrieve the ID to be used for notifications
	// This is required to easily switch between system IDs to be used for
	// notifications
	public String getNotificationsIdentifier(final String username) {
		String nUsername = username;

		String nIdentifierType = null;
		try {
			nIdentifierType = AppUtil
					.getProperty("NOTIFICATION_USER_IDENTIFIER");
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		if (nIdentifierType != null) {
			if (nIdentifierType.equals(AUTH_LDAP)) {
				if (getAuthSource().equals(AUTH_LDAP)) {
					nUsername = username;
				} else if (getAuthSource().equals(AUTH_BANNER)) {
					// TODO - As of now we do not have this scenario for any of
					// the clients
					// nUsername = getLdapIdFromBannerId(username);
				}
			} else if (nIdentifierType.equals(AUTH_BANNER)) {
				if (getAuthSource().equals(AUTH_LDAP)) {
					nUsername = getBannerId(username);
				} else if (getAuthSource().equals(AUTH_BANNER)) {
					nUsername = username;
				}
			}
		}
		return nUsername;
	}

	private String getFullDnForUser(String uid) {
		String userProp = "uid";
		try {
			userProp = AppUtil.getProperty("LDAP_USERID_PROPERTY");
		} catch (Exception e) {
		}
		if (userProp == null || "".equals(userProp)) {
			userProp = "uid";
		}
		EqualsFilter f = new EqualsFilter(userProp, uid);
		List result = ldapTemplate.search(DistinguishedName.EMPTY_PATH,
				f.encode(), new AbstractContextMapper() {
					protected Object doMapFromContext(DirContextOperations ctx) {
						return ctx.getNameInNamespace();
					}
				});

		if (result.size() != 1) {
			throw new RuntimeException("User not found or not unique");
		}

		return (String) result.get(0);
	}
}
