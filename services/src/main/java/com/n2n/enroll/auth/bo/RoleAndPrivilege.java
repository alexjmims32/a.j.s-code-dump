package com.n2n.enroll.auth.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@SuppressWarnings("serial")
@Entity(name = "RoleAndPrivilege")
public class RoleAndPrivilege implements Serializable {
	@Id
	private String Id;
	@Column(name = "ROLEID")
	private String roleId;
	@Column(name = "MODULECODE")
	private String moduleCode;
	@Column(name = "ACCESSFLAG")
	private String accessFlag;
	@Column(name = "AUTHREQUIRED")
	private String authRequired;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "CAMPUSCODE")
	private String campusCode;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getAccessFlag() {
		return accessFlag;
	}

	public void setAccessFlag(String accessFlag) {
		this.accessFlag = accessFlag;
	}

	public String getAuthRequired() {
		return authRequired;
	}

	public void setAuthRequired(String authRequired) {
		this.authRequired = authRequired;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	
}
