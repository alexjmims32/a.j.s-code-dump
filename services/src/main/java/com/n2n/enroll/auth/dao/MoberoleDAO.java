//package com.n2n.enroll.auth.dao;
//
//import java.util.List;
//
//import org.hibernate.LockMode;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.ApplicationContext;
//import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
//
//import com.n2n.enroll.auth.bo.Moberole;
//
///**
// * A data access object (DAO) providing persistence and search support for
// * Moberole entities. Transaction control of the save(), update() and delete()
// * operations can directly support Spring container-managed transactions or they
// * can be augmented to handle user-managed Spring transactions. Each of these
// * methods provides additional information for how to configure it for the
// * desired type of transaction control.
// * 
// * @see net.n2n.mobedu.auth.Moberole
// * @author MyEclipse Persistence Tools
// */
//@SuppressWarnings({"unchecked","rawtypes"})
//public class MoberoleDAO extends HibernateDaoSupport {
//	private static final Logger log = LoggerFactory
//			.getLogger(MoberoleDAO.class);
//	// property constants
//	public static final String TITLE = "title";
//	public static final String DESCRIPTION = "description";
//
//	protected void initDao() {
//		// do nothing
//	}
//
//	public void save(Moberole transientInstance) {
//		log.debug("saving Moberole instance");
//		try {
//			getHibernateTemplate().save(transientInstance);
//			log.debug("save successful");
//		} catch (RuntimeException re) {
//			log.error("save failed", re);
//			throw re;
//		}
//	}
//
//	public void delete(Moberole persistentInstance) {
//		log.debug("deleting Moberole instance");
//		try {
//			getHibernateTemplate().delete(persistentInstance);
//			log.debug("delete successful");
//		} catch (RuntimeException re) {
//			log.error("delete failed", re);
//			throw re;
//		}
//	}
//
//	public Moberole findById(Long id) {
//		log.debug("getting Moberole instance with id: " + id);
//		try {
//			Moberole instance = (Moberole) getHibernateTemplate().get(
//					"net.n2n.mobedu.auth.Moberole", id);
//			return instance;
//		} catch (RuntimeException re) {
//			log.error("get failed", re);
//			throw re;
//		}
//	}
//
//	public List<Moberole> findByExample(Moberole instance) {
//		log.debug("finding Moberole instance by example");
//		try {
//			List<Moberole> results = (List<Moberole>) getHibernateTemplate()
//					.findByExample(instance);
//			log.debug("find by example successful, result size: "
//					+ results.size());
//			return results;
//		} catch (RuntimeException re) {
//			log.error("find by example failed", re);
//			throw re;
//		}
//	}
//
//	public List findByProperty(String propertyName, Object value) {
//		log.debug("finding Moberole instance with property: " + propertyName
//				+ ", value: " + value);
//		try {
//			String queryString = "from Moberole as model where model."
//					+ propertyName + "= ?";
//			return getHibernateTemplate().find(queryString, value);
//		} catch (RuntimeException re) {
//			log.error("find by property name failed", re);
//			throw re;
//		}
//	}
//
//	public List<Moberole> findByTitle(Object title) {
//		return findByProperty(TITLE, title);
//	}
//
//	
//	public List<Moberole> findByDescription(Object description) {
//		return findByProperty(DESCRIPTION, description);
//	}
//
//	public List findAll() {
//		log.debug("finding all Moberole instances");
//		try {
//			String queryString = "from Moberole";
//			return getHibernateTemplate().find(queryString);
//		} catch (RuntimeException re) {
//			log.error("find all failed", re);
//			throw re;
//		}
//	}
//
//	public Moberole merge(Moberole detachedInstance) {
//		log.debug("merging Moberole instance");
//		try {
//			Moberole result = (Moberole) getHibernateTemplate().merge(
//					detachedInstance);
//			log.debug("merge successful");
//			return result;
//		} catch (RuntimeException re) {
//			log.error("merge failed", re);
//			throw re;
//		}
//	}
//
//	public void attachDirty(Moberole instance) {
//		log.debug("attaching dirty Moberole instance");
//		try {
//			getHibernateTemplate().saveOrUpdate(instance);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public void attachClean(Moberole instance) {
//		log.debug("attaching clean Moberole instance");
//		try {
//			getHibernateTemplate().lock(instance, LockMode.NONE);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public static MoberoleDAO getFromApplicationContext(ApplicationContext ctx) {
//		return (MoberoleDAO) ctx.getBean("MoberoleDAO");
//	}
//}