package com.n2n.enroll.auth.bo;

public class JAdminUser {
	private String username;
	private String role;

	private String firstName;
	private String lastName;
	private String password;
	private AdminPrivilegesList privilegeList;

	public AdminPrivilegesList getPrivilegeList() {
		return privilegeList;
	}

	public void setPrivilegeList(AdminPrivilegesList privilegeList) {
		this.privilegeList = privilegeList;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	private String active;
}
