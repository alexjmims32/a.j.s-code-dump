package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

public class ModulesList {
		private List<Modules> module = new ArrayList<Modules>(0);

		public List<Modules> getModule() {
			return module;
		}

		public void setModule(List<Modules> module) {
			this.module = module;
		}
}
