package com.n2n.enroll.auth.bo;

import java.util.ArrayList;
import java.util.List;

import com.n2n.enroll.contact.bo.PersonList;


public class LoginServiceResponse {

	protected boolean success;
	protected LoginStatus loginStatus;
	protected EumerationList enumerationList;
	protected List<JPrivilege> privilegesList = new ArrayList<JPrivilege>();
	protected UserSettingsList settingsList;
	protected MappingsList mappingsList;

	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public List<JPrivilege> getPrivilegesList() {
		return privilegesList;
	}

	public void setPrivilegesList(List<JPrivilege> privilegesList) {
		this.privilegesList = privilegesList;
	}
	
	public LoginStatus getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(LoginStatus loginStatus) {
		this.loginStatus = loginStatus;
	}

	public EumerationList getEnumerationList() {
		return enumerationList;
	}

	public void setEnumerationList(EumerationList enumerationList) {
		this.enumerationList = enumerationList;
	}
	public UserSettingsList getSettingsList() {
		return settingsList;
	}

	public void setSettingsList(UserSettingsList settingsList) {
		this.settingsList = settingsList;
	}

	public MappingsList getMappingsList() {
		return mappingsList;
	}

	public void setMappingsList(MappingsList mappingsList) {
		this.mappingsList = mappingsList;
	}
}
