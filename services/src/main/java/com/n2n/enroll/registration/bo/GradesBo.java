package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class GradesBo implements java.io.Serializable {

	@Id
	private String id;

	@Column(name = "STU_ID")
	public String studentId;
	@Column(name = "TERM_CODE")
	public String termCode;
	@Column(name = "SUBJ_CODE")
	public String subject;
	@Column(name = "CRN")
	public String crn;
	@Column(name = "CRSE_NUMB")
	public String courseNumber;
	@Column(name = "CRSE_TITLE")
	public String title;
	@Column(name = "MID_GRADE")
	public String midGrade;
	@Column(name = "FINAL_GRADE")
	public String finalGrade;
	@Column(name = "GRADE_DATE")
	public String date;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getTermCode() {
		return termCode;
	}
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCrn() {
		return crn;
	}
	public void setCrn(String crn) {
		this.crn = crn;
	}
	public String getCourseNumber() {
		return courseNumber;
	}
	public void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMidGrade() {
		return midGrade;
	}
	public void setMidGrade(String midGrade) {
		this.midGrade = midGrade;
	}
	public String getFinalGrade() {
		return finalGrade;
	}
	public void setFinalGrade(String finalGrade) {
		this.finalGrade = finalGrade;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
