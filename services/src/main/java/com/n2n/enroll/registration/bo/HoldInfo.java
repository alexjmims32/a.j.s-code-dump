package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class HoldInfo implements java.io.Serializable {
	@Id
	private String id;
	@Column(name = "HOLD_DESC")
	public String holdsDescription;
	@Column(name = "HOLD_AMOUNT")
	public String holdAmount;
	@Column(name = "HOLD_RELEASE_IND")
	public String holdReleaseIndicator;
	@Column(name = "REASON")
	public String holdReason;
	@Column(name = "HOLD_ORIGINATING_OFFICE_DESC")
	public String holdOriginOffice;
	@Column(name = "HOLD_FROM_DATE")
	public String holdFromDate;
	@Column(name = "HOLD_TO_DATE")
	public String holdToDate;
	@Column(name="PROCESSES_AFFECTED")
	public String processesAffected;
	
	public String getHoldReason() {
		return holdReason;
	}

	public void setHoldReason(String holdReason) {
		this.holdReason = holdReason;
	}

	public String getProcessesAffected() {
		return processesAffected;
	}

	public void setProcessesAffected(String processesAffected) {
		this.processesAffected = processesAffected;
	}

	public String getHoldFromDate() {
		return holdFromDate;
	}

	public void setHoldFromDate(String holdFromDate) {
		this.holdFromDate = holdFromDate;
	}

	public String getHoldToDate() {
		return holdToDate;
	}

	public void setHoldToDate(String holdToDate) {
		this.holdToDate = holdToDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHoldsDescription() {
		return holdsDescription;
	}

	public void setHoldsDescription(String holdsDescription) {
		this.holdsDescription = holdsDescription;
	}

	public String getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(String holdAmount) {
		this.holdAmount = holdAmount;
	}

	public String getHoldReleaseIndicator() {
		return holdReleaseIndicator;
	}

	public void setHoldReleaseIndicator(String holdReleaseIndicator) {
		this.holdReleaseIndicator = holdReleaseIndicator;
	}

	public String getHoldOriginOffice() {
		return holdOriginOffice;
	}

	public void setHoldOriginOffice(String holdOriginOffice) {
		this.holdOriginOffice = holdOriginOffice;
	}

}
