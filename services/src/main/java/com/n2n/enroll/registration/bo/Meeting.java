package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class Meeting implements java.io.Serializable {
	@Id
	private String id;
	@Column(name = "TERM_CODE")
	public String termCode;
	@Column(name = "CRN")
	public String crn;
	@Column(name = "BEGIN_TIME")
	public String beginTime;
	@Column(name = "END_TIME")
	public String endTime;
	@Column(name = "BLDG_CODE")
	public String bldgCode;
	@Column(name = "ROOM_CODE")
	public String roomCode;
	@Column(name = "START_DATE")
	public String startDate;
	@Column(name = "END_DATE")
	public String endDate;
	@Column(name = "SUNDAY")
	public String sunDay;
	@Column(name = "MONDAY")
	public String monDay;
	@Column(name = "TUEDAY")
	public String tuesDay;
	@Column(name = "WEDDAY")
	public String wednesDay;
	@Column(name = "THUDAY")
	public String thursDay;
	@Column(name = "FRIDAY")
	public String friDay;
	@Column(name = "SATDAY")
	public String satDay;
	@Column(name = "MTYP_CODE")
	public String mtypCode;
	@Column(name = "PERIOD")
	public String period;
	@Column(name = "LATITUDE")
	public String latitude;
	@Column(name = "LONGITUDE")
	public String longitude;

	@Column(name = "SCHEDULE_CODE")
	public String scheduleCode;
	@Column(name = "SCHEDULE_DESC")
	public String scheduleDesc;

	public String getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}

	public String getScheduleDesc() {
		return scheduleDesc;
	}

	public void setScheduleDesc(String scheduleDesc) {
		this.scheduleDesc = scheduleDesc;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getSatDay() {
		return satDay;
	}

	public void setSatDay(String satDay) {
		this.satDay = satDay;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getBldgCode() {
		return bldgCode;
	}

	public void setBldgCode(String bldgCode) {
		this.bldgCode = bldgCode;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSunDay() {
		return sunDay;
	}

	public void setSunDay(String sunDay) {
		this.sunDay = sunDay;
	}

	public String getMonDay() {
		return monDay;
	}

	public void setMonDay(String monDay) {
		this.monDay = monDay;
	}

	public String getTuesDay() {
		return tuesDay;
	}

	public void setTuesDay(String tuesDay) {
		this.tuesDay = tuesDay;
	}

	public String getWednesDay() {
		return wednesDay;
	}

	public void setWednesDay(String wednesDay) {
		this.wednesDay = wednesDay;
	}

	public String getThursDay() {
		return thursDay;
	}

	public void setThursDay(String thursDay) {
		this.thursDay = thursDay;
	}

	public String getFriDay() {
		return friDay;
	}

	public void setFriDay(String friDay) {
		this.friDay = friDay;
	}

	public String getMtypCode() {
		return mtypCode;
	}

	public void setMtypCode(String mtypCode) {
		this.mtypCode = mtypCode;
	}

}
