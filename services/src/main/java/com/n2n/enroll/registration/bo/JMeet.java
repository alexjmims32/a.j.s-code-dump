package com.n2n.enroll.registration.bo;

public class JMeet {

	public String beginTime;
	public String endTime;
	public String bldgCode;
	public String latitude;
	public String longitude;

	public String scheduleCode;
	public String scheduleDesc;

	public String getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}

	public String getScheduleDesc() {
		return scheduleDesc;
	}

	public void setScheduleDesc(String scheduleDesc) {
		this.scheduleDesc = scheduleDesc;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String roomCode;

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getBldgCode() {
		return bldgCode;
	}

	public void setBldgCode(String bldgCode) {
		this.bldgCode = bldgCode;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String roomCode) {
		this.roomCode = roomCode;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSunDay() {
		return sunDay;
	}

	public void setSunDay(String sunDay) {
		this.sunDay = sunDay;
	}

	public String getMonDay() {
		return monDay;
	}

	public void setMonDay(String monDay) {
		this.monDay = monDay;
	}

	public String getTuesDay() {
		return tuesDay;
	}

	public void setTuesDay(String tuesDay) {
		this.tuesDay = tuesDay;
	}

	public String getWednesDay() {
		return wednesDay;
	}

	public void setWednesDay(String wednesDay) {
		this.wednesDay = wednesDay;
	}

	public String getThursDay() {
		return thursDay;
	}

	public void setThursDay(String thursDay) {
		this.thursDay = thursDay;
	}

	public String getFriDay() {
		return friDay;
	}

	public void setFriDay(String friDay) {
		this.friDay = friDay;
	}

	public String getSatDay() {
		return satDay;
	}

	public void setSatDay(String satDay) {
		this.satDay = satDay;
	}

	public String getMtypCode() {
		return mtypCode;
	}

	public void setMtypCode(String mtypCode) {
		this.mtypCode = mtypCode;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String startDate;
	public String endDate;
	public String sunDay;
	public String monDay;
	public String tuesDay;
	public String wednesDay;
	public String thursDay;
	public String friDay;
	public String satDay;
	public String mtypCode;
	public String period;
}
