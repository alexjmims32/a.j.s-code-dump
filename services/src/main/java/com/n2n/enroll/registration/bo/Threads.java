package com.n2n.enroll.registration.bo;

public class Threads {
	protected String forumId;
	protected String threadId;
	protected String forumTitle;
	protected String forumDescription;
	protected String isForumAvailable;
	protected String forumStartDate;
	protected String forumEndDate;
	protected String threadSubject;
	protected String threadBody;
	protected String threadParentId;
	protected String threadPostDate;
	protected String threadEditDate;
	protected String threadResponses;
	protected String threadPostedName;
	public String getForumId() {
		return forumId;
	}
	public void setForumId(String forumId) {
		this.forumId = forumId;
	}
	public String getThreadId() {
		return threadId;
	}
	public void setThreadId(String threadId) {
		this.threadId = threadId;
	}
	public String getForumTitle() {
		return forumTitle;
	}
	public void setForumTitle(String forumTitle) {
		this.forumTitle = forumTitle;
	}
	public String getForumDescription() {
		return forumDescription;
	}
	public void setForumDescription(String forumDescription) {
		this.forumDescription = forumDescription;
	}
	public String getIsForumAvailable() {
		return isForumAvailable;
	}
	public void setIsForumAvailable(String isForumAvailable) {
		this.isForumAvailable = isForumAvailable;
	}
	public String getForumStartDate() {
		return forumStartDate;
	}
	public void setForumStartDate(String forumStartDate) {
		this.forumStartDate = forumStartDate;
	}
	public String getForumEndDate() {
		return forumEndDate;
	}
	public void setForumEndDate(String forumEndDate) {
		this.forumEndDate = forumEndDate;
	}
	public String getThreadSubject() {
		return threadSubject;
	}
	public void setThreadSubject(String threadSubject) {
		this.threadSubject = threadSubject;
	}
	public String getThreadBody() {
		return threadBody;
	}
	public void setThreadBody(String threadBody) {
		this.threadBody = threadBody;
	}
	public String getThreadParentId() {
		return threadParentId;
	}
	public void setThreadParentId(String threadParentId) {
		this.threadParentId = threadParentId;
	}
	public String getThreadPostDate() {
		return threadPostDate;
	}
	public void setThreadPostDate(String threadPostDate) {
		this.threadPostDate = threadPostDate;
	}
	public String getThreadEditDate() {
		return threadEditDate;
	}
	public void setThreadEditDate(String threadEditDate) {
		this.threadEditDate = threadEditDate;
	}
	public String getThreadResponses() {
		return threadResponses;
	}
	public void setThreadResponses(String threadResponses) {
		this.threadResponses = threadResponses;
	}
	public String getThreadPostedName() {
		return threadPostedName;
	}
	public void setThreadPostedName(String threadPostedName) {
		this.threadPostedName = threadPostedName;
	}
	
}
