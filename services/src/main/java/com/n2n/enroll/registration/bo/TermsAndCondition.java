package com.n2n.enroll.registration.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "TermsAndCondition")
public class TermsAndCondition implements Serializable {
	@Column(name = "CAMPUSCODE")
	private String campusCode;
	@Id
	@Column(name = "TERMS_CONDITIONS")
	private String termConditions;
	@Column(name = "LASTMODIFIEDBY")
	private String lastModifiedBy;
	@Column(name = "LASTMODIFIEDON")
	private String lastModifiedON;

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getTermConditions() {
		return termConditions;
	}

	public void setTermConditions(String termConditions) {
		this.termConditions = termConditions;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedON() {
		return lastModifiedON;
	}

	public void setLastModifiedON(String lastModifiedON) {
		this.lastModifiedON = lastModifiedON;
	}

	public String getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(String versionNo) {
		this.versionNo = versionNo;
	}

	@Column(name = "VERSION_NO")
	private String versionNo;
}
