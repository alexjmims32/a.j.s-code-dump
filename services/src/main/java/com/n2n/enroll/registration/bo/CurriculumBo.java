package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class CurriculumBo implements java.io.Serializable {
	@Id
	private String id;
	@Column(name = "STUDENT_ID")
	public String studentId;
	@Column(name = "ADMIT_TERM")
	public String termCode;
	@Column(name = "LEVL")
	public String level;
	@Column(name = "ADMIT_TYPE")
	public String admitType;
	@Column(name = "COLLEGE")
	public String college;
	@Column(name = "MAJOR")
	public String major;
	@Column(name = "DEPARTMENT")
	public String department;
	@Column(name = "PROGRAM")
	public String program;
	@Column(name = "PROGRAM_DESC")
	public String programDesc;
	@Column(name = "CATALOG_TERM")
	public String catalogTerm;
	@Column(name = "CAMPUS")
	public String campus;
	@Column(name = "CONCENTRATION")
	public String concentration;

	public String getConcentration() {
		return concentration;
	}

	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAdmitType() {
		return admitType;
	}

	public void setAdmitType(String admitType) {
		this.admitType = admitType;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public String getProgramDesc() {
		return programDesc;
	}

	public void setProgramDesc(String programDesc) {
		this.programDesc = programDesc;
	}

	public String getCatalogTerm() {
		return catalogTerm;
	}

	public void setCatalogTerm(String catalogTerm) {
		this.catalogTerm = catalogTerm;
	}

}
