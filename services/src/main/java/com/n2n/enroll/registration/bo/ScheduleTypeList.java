package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class ScheduleTypeList {

	protected List<JItem> scheduleTypeList = new ArrayList<JItem>(0);

	public List<JItem> getScheduleTypeList() {
		return scheduleTypeList;
	}

	public void setScheduleTypeList(List<JItem> scheduleTypeList) {
		this.scheduleTypeList = scheduleTypeList;
	}

}
