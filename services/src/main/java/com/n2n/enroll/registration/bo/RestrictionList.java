package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class RestrictionList {
	protected List<JRestrictions> crseRestrictions = new ArrayList<JRestrictions>(0);

	public List<JRestrictions> getCrseRestrictions() {
		return crseRestrictions;
	}

	public void setCrseRestrictions(List<JRestrictions> crseRestrictions) {
		this.crseRestrictions = crseRestrictions;
	}

	
}
