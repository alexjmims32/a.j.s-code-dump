package com.n2n.enroll.registration.bo;

public class JCourse {

	private String courseNumber;
	private String courseTitle;
	private String subject;
	private String description;
	private String credits;
	private String level;
	private String college;
	private String period;
	private String roomCode;
	private String termCode;
	private String termDescription;
	private String lectureHours;
	private String seatsAvailable;
	private String noOfSeatsAvailable;
	private String totalSeats;
	private String waitedList;
	private String flagDescription;
	private String scheduleType;
	private String department;
	private String campus;
	private String campusCode;
	private String prerequisite;
	private String crn;
	private String scheduleDate;
	private String scheduleTime;
	private String faculty;
	private String courseIndicator;
	private String status;
	private String checkCart;
	private String varCredit;
	private String varCreditHrs;
	private String minCredit;
	private String maxCredit;
	private String seqNo;
	private String linkId;
	private String facultyEmail;
	private String courseComments;
	private String prereqcheck;
	private String ssn;
	private String dropFlag;
	private String actionDesc;
	private String actionCode;
	private String statusMessage;
	private String statusCode;
	private String crseAttr;
	protected JMeetingList meetingList;
	private String courseDates;
	private String courseMeetings;
	private String olrInd;

	public String getOlrInd() {
		return olrInd;
	}

	public void setOlrInd(String olrInd) {
		this.olrInd = olrInd;
	}

	public String getCrseAttr() {
		return crseAttr;
	}

	public void setCrseAttr(String crseAttr) {
		this.crseAttr = crseAttr;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionDesc() {
		return actionDesc;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	public String getDropFlag() {
		return dropFlag;
	}

	public void setDropFlag(String dropFlag) {
		this.dropFlag = dropFlag;
	}

	public String getCourseDates() {
		return courseDates;
	}

	public void setCourseDates(String courseDates) {
		this.courseDates = courseDates;
	}

	public String getCourseMeetings() {
		return courseMeetings;
	}

	public void setCourseMeetings(String courseMeetings) {
		this.courseMeetings = courseMeetings;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getPrereqcheck() {
		return prereqcheck;
	}

	public void setPrereqcheck(String prereqcheck) {
		this.prereqcheck = prereqcheck;
	}

	public String getCourseComments() {
		return courseComments;
	}

	public void setCourseComments(String courseComments) {
		this.courseComments = courseComments;
	}

	public String getFacultyEmail() {
		return facultyEmail;
	}

	public void setFacultyEmail(String facultyEmail) {
		this.facultyEmail = facultyEmail;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public String getMinCredit() {
		return minCredit;
	}

	public void setMinCredit(String minCredit) {
		this.minCredit = minCredit;
	}

	public String getMaxCredit() {
		return maxCredit;
	}

	public void setMaxCredit(String maxCredit) {
		this.maxCredit = maxCredit;
	}

	public String getVarCredit() {
		return varCredit;
	}

	public void setVarCredit(String varCredit) {
		this.varCredit = varCredit;
	}

	public String getVarCreditHrs() {
		return varCreditHrs;
	}

	public void setVarCreditHrs(String varCreditHrs) {
		this.varCreditHrs = varCreditHrs;
	}

	public String getCheckCart() {
		return checkCart;
	}

	public void setCheckCart(String checkCart) {
		this.checkCart = checkCart;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFlagDescription() {
		return flagDescription;
	}

	public void setFlagDescription(String flagDescription) {
		this.flagDescription = flagDescription;
	}

	public String getCourseIndicator() {
		return courseIndicator;
	}

	public void setCourseIndicator(String courseIndicator) {
		this.courseIndicator = courseIndicator;
	}

	public String getWaitedList() {
		return waitedList;
	}

	public void setWaitedList(String waitedList) {
		this.waitedList = waitedList;
	}

	public String getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(String seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public JMeetingList getMeetingList() {
		return meetingList;
	}

	public void setMeetingList(JMeetingList meetingList) {
		this.meetingList = meetingList;
	}

	public String getCourseNumber() {
		return courseNumber;
	}

	public void setCourseNumber(String value) {
		this.courseNumber = value;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String value) {
		this.courseTitle = value;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String value) {
		this.subject = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String value) {
		this.description = value;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String value) {
		this.credits = value;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String value) {
		this.level = value;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String value) {
		this.college = value;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String value) {
		this.period = value;
	}

	public String getRoomCode() {
		return roomCode;
	}

	public void setRoomCode(String value) {
		this.roomCode = value;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String value) {
		this.termCode = value;
	}

	public String getTermDescription() {
		return termDescription;
	}

	public void setTermDescription(String value) {
		this.termDescription = value;
	}

	public String getLectureHours() {
		return lectureHours;
	}

	public void setLectureHours(String value) {
		this.lectureHours = value;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String value) {
		this.scheduleType = value;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String value) {
		this.department = value;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String value) {
		this.campus = value;
	}

	public String getPrerequisite() {
		return prerequisite;
	}

	public void setPrerequisite(String value) {
		this.prerequisite = value;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String value) {
		this.crn = value;
	}

	public String getScheduleDate() {
		return scheduleDate;
	}

	public void setScheduleDate(String value) {
		this.scheduleDate = value;
	}

	public String getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(String value) {
		this.scheduleTime = value;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String value) {
		this.faculty = value;
	}

	public String getCampusCode() {
		return campusCode;
	}

	public void setCampusCode(String campusCode) {
		this.campusCode = campusCode;
	}

	public String getNoOfSeatsAvailable() {
		return noOfSeatsAvailable;
	}

	public void setNoOfSeatsAvailable(String noOfSeatsAvailable) {
		this.noOfSeatsAvailable = noOfSeatsAvailable;
	}

	public String getTotalSeats() {
		return totalSeats;
	}

	public void setTotalSeats(String totalSeats) {
		this.totalSeats = totalSeats;
	}

}
