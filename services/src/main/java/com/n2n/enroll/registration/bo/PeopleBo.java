package com.n2n.enroll.registration.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class PeopleBo implements Serializable {
	@Id
	private String id;
	@Column(name = "EMAIL")
	public String email;
	@Column(name = "PHONE_AREA")
	public String phoneArea;
	@Column(name = "PHONE_NUMB")
	public String phoneNumber;
	@Column(name = "PHONE_EXT")
	public String phoneExt;
	@Column(name = "NAME")
	public String name;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneArea() {
		return phoneArea;
	}

	public void setPhoneArea(String phoneArea) {
		this.phoneArea = phoneArea;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneExt() {
		return phoneExt;
	}

	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
