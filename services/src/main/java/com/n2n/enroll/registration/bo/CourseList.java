package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class CourseList {

	protected List<JCourse> course = new ArrayList<JCourse>(0);
	protected String status;
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<JCourse> getCourse() {
		return course;
	}

	public void setCourse(List<JCourse> course) {
		this.course = course;
	}

}
