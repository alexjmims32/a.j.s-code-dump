package com.n2n.enroll.registration;

public interface CourseSearchType {

	public static final String USE_MATERIALIZED_VIEW = "1";
	public static final String USE_DIRECT_VIEW = "2";
	
}
