package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class CartList {
	protected String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	protected List<Cart> course = new ArrayList<Cart>(0);

	public List<Cart> getCourse() {
		return course;
	}

	public void setCourse(List<Cart> course) {
		this.course = course;
	}
}
