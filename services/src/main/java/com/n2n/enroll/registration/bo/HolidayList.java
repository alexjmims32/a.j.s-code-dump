package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class HolidayList {
	
	
	public List<HolidayInfo> holidays=new ArrayList<HolidayInfo>(0);
	public List<HolidayInfo> getHolidays() {
		return holidays;
	}
	public void setHolidays(List<HolidayInfo> holidays) {
		this.holidays = holidays;
	}
	
	

}
