package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class ProgramList {

	protected List<JItem> programList = new ArrayList<JItem>(0);

	public List<JItem> getProgramList() {
		return programList;
	}

	public void setProgramList(List<JItem> programList) {
		this.programList = programList;
	}
}