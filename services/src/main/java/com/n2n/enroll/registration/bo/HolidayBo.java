package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
@SuppressWarnings({ "serial" })
public class HolidayBo  implements java.io.Serializable{
	@Id
	private String id;
	@Column(name="HOLIDAY_YEAR")
	public  String year;
	@Column(name="HOLIDAY_DATE")
	public String date;
	@Column(name="HOLIDAY_DESCRIPTION")
	public String holidayDesc;
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHolidayDesc() {
		return holidayDesc;
	}
	public void setHolidayDesc(String holidayDesc) {
		this.holidayDesc = holidayDesc;
	}
	
	
}
