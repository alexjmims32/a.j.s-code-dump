package com.n2n.enroll.registration.bo;

public class JDropResponse {

	protected String status;
	protected DropList dropList;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DropList getDropList() {
		return dropList;
	}
	public void setDropList(DropList dropList) {
		this.dropList = dropList;
	}
	
}
