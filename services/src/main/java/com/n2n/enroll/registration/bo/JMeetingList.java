package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class JMeetingList {
	protected List<JMeet> meeting = new ArrayList<JMeet>(0);

	public List<JMeet> getMeeting() {
		return meeting;
	}

	public void setMeeting(List<JMeet> meeting) {
		this.meeting = meeting;
	}

}
