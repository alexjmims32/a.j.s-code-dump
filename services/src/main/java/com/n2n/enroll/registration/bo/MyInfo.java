package com.n2n.enroll.registration.bo;

import com.n2n.enroll.contact.bo.PersonList;

public class MyInfo {
	
	protected JStudentEffectiveInfo studentInfo;

	public JStudentEffectiveInfo getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(JStudentEffectiveInfo studentInfo) {
		this.studentInfo = studentInfo;
	}
	
	protected CurriculumInfo cirinfo;
	
	public CurriculumInfo getCirinfo() {
		return cirinfo;
	}
	
	public void setCirinfo(CurriculumInfo cirinfo) {
		this.cirinfo = cirinfo;
	}
	
	protected PersonList plist;
	
	public PersonList getPlist() {
		return plist;
	}
	
	public void setPlist(PersonList plist) {
		this.plist = plist;
	}
}
