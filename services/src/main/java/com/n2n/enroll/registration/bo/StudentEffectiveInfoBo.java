package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class StudentEffectiveInfoBo implements java.io.Serializable {
	
	@Id
	private String id;
	@Column(name = "STUDENT_ID")
	public String studentId;
	@Column(name ="STUDENT_INF")
	private String studentInf;
	@Column(name = "FROM_TERM")
	public String fromTerm;
	@Column(name = "TO_TERM")
	public String toTerm;
	@Column(name = "IS_REGISTERED")
	public String isRegistered;
	@Column(name = "FIRST_TERM")
	public String firstTerm;
	@Column(name = "LAST_TERM")
	public String lastTerm;
	@Column(name = "STUDENT_STATUS")
	public String studentStatus;
	@Column(name = "TERM_CODE_MATRIC")
	public String termCodeMatric;
	@Column(name = "RESIDENCE")
	public String residence;
	@Column(name = "CITIZEN")
	public String citizen;
	@Column(name = "STUDNET_TYPE")
	public String studentType;
	@Column(name = "EXP_GRAD_DATE")
	public String expGradeDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudentInf() {
		return studentInf;
	}
	public void setStudentInf(String studentInf) {
		this.studentInf = studentInf;
	}
	public String getFromTerm() {
		return fromTerm;
	}
	public void setFromTerm(String fromTerm) {
		this.fromTerm = fromTerm;
	}
	public String getToTerm() {
		return toTerm;
	}
	public void setToTerm(String toTerm) {
		this.toTerm = toTerm;
	}
	public String getIsRegistered() {
		return isRegistered;
	}
	public void setIsRegistered(String isRegistered) {
		this.isRegistered = isRegistered;
	}
	public String getFirstTerm() {
		return firstTerm;
	}
	public void setFirstTerm(String firstTerm) {
		this.firstTerm = firstTerm;
	}
	public String getLastTerm() {
		return lastTerm;
	}
	public void setLastTerm(String lastTerm) {
		this.lastTerm = lastTerm;
	}
	public String getStudentStatus() {
		return studentStatus;
	}
	public void setStudentStatus(String studentStatus) {
		this.studentStatus = studentStatus;
	}
	public String getTermCodeMatric() {
		return termCodeMatric;
	}
	public void setTermCodeMatric(String termCodeMatric) {
		this.termCodeMatric = termCodeMatric;
	}
	public String getResidence() {
		return residence;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public String getCitizen() {
		return citizen;
	}
	public void setCitizen(String citizen) {
		this.citizen = citizen;
	}
	public String getStudentType() {
		return studentType;
	}
	public void setStudentType(String studentType) {
		this.studentType = studentType;
	}
	public String getExpGradeDate() {
		return expGradeDate;
	}
	public void setExpGradeDate(String expGradeDate) {
		this.expGradeDate = expGradeDate;
	}
}
