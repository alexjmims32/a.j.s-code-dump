package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

import com.n2n.enroll.contact.bo.JPersonDetails;
import com.n2n.enroll.contact.bo.PersonList;

public class JStudentEffectiveInfo {

	public String studentId;
	public String studentInf;
	public String fromTerm;
	public String toTerm;
	public String isRegistered;
	public String firstTerm;
	public String lastTerm;
	public String studentStatus;
	public String termCodeMatric;
	public String residence;
	public String citizen;
	public String studentType;
	public String expGradeDate;
	protected CurriculumInfo curriculumInfo;
	protected PersonList plist;

	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public PersonList getPlist() {
		return plist;
	}

	public CurriculumInfo getCurriculumInfo() {
		return curriculumInfo;
	}

	public void setCurriculumInfo(CurriculumInfo curriculumInfo) {
		this.curriculumInfo = curriculumInfo;
	}

	public void setPlist(PersonList plist) {
		this.plist = plist;
	}

	public String getStudentInf() {
		return studentInf;
	}
	public void setStudentInf(String studentInf) {
		this.studentInf = studentInf;
	}
	public String getFromTerm() {
		return fromTerm;
	}
	public void setFromTerm(String fromTerm) {
		this.fromTerm = fromTerm;
	}
	public String getToTerm() {
		return toTerm;
	}
	public void setToTerm(String toTerm) {
		this.toTerm = toTerm;
	}
	public String getIsRegistered() {
		return isRegistered;
	}
	public void setIsRegistered(String isRegistered) {
		this.isRegistered = isRegistered;
	}
	public String getFirstTerm() {
		return firstTerm;
	}
	public void setFirstTerm(String firstTerm) {
		this.firstTerm = firstTerm;
	}
	public String getLastTerm() {
		return lastTerm;
	}
	public void setLastTerm(String lastTerm) {
		this.lastTerm = lastTerm;
	}
	public String getStudentStatus() {
		return studentStatus;
	}
	public void setStudentStatus(String studentStatus) {
		this.studentStatus = studentStatus;
	}
	public String getTermCodeMatric() {
		return termCodeMatric;
	}
	public void setTermCodeMatric(String termCodeMatric) {
		this.termCodeMatric = termCodeMatric;
	}
	public String getResidence() {
		return residence;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public String getCitizen() {
		return citizen;
	}
	public void setCitizen(String citizen) {
		this.citizen = citizen;
	}
	public String getStudentType() {
		return studentType;
	}
	public void setStudentType(String studentType) {
		this.studentType = studentType;
	}
	public String getExpGradeDate() {
		return expGradeDate;
	}
	public void setExpGradeDate(String expGradeDate) {
		this.expGradeDate = expGradeDate;
	}
}

