package com.n2n.enroll.registration.bo;

public class JStatus {
	protected String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
