package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class SubjectList {

	protected List<JItem> subjectList = new ArrayList<JItem>(0);

	public List<JItem> getSubjectList() {
		return subjectList;
	}

	public void setSubjectList(List<JItem> subjectList) {
		this.subjectList = subjectList;
	}

	

}
