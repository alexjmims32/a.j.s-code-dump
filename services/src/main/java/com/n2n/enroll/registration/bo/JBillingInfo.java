package com.n2n.enroll.registration.bo;


public class JBillingInfo{
	
	private String id;
	public String billHours;
	public String creditHours;
	public String maxHours;
	public String minHours;
	public String termCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBillHours() {
		return billHours;
	}
	public void setBillHours(String billHours) {
		this.billHours = billHours;
	}
	public String getCreditHours() {
		return creditHours;
	}
	public void setCreditHours(String creditHours) {
		this.creditHours = creditHours;
	}
	public String getMaxHours() {
		return maxHours;
	}
	public void setMaxHours(String maxHours) {
		this.maxHours = maxHours;
	}
	public String getMinHours() {
		return minHours;
	}
	public void setMinHours(String minHours) {
		this.minHours = minHours;
	}
	public String getTermCode() {
		return termCode;
	}
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}
	
	
}
