package com.n2n.enroll.registration.service;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

class BlackboardAuthenticator extends Authenticator {

	  private final String username;
	  private final char[] password;

	  public BlackboardAuthenticator(final String username, final String password) {
	    super();
	    this.username = username;
	    this.password = password.toCharArray(); 
	  }

	  public PasswordAuthentication getPasswordAuthentication() {
	    return (new PasswordAuthentication (username, password));
	  }
	}