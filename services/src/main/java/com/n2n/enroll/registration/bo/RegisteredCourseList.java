package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class RegisteredCourseList {

	protected List<JRegisteredCourse> registeredCourse = new ArrayList<JRegisteredCourse>(0);

	public List<JRegisteredCourse> getRegisteredCourse() {
		return registeredCourse;
	}

	public void setRegisteredCourse(List<JRegisteredCourse> registeredCourse) {
		this.registeredCourse = registeredCourse;
	}

}
