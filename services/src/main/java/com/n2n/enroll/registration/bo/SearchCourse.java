package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class SearchCourse implements java.io.Serializable {
	@Id
	private String id;
	@Column(name = "TERM_CODE")
	public String termCode;
	@Column(name = "TERM_DESC")
	public String termDescription;
	@Column(name = "COURSE_NUMBER")
	public String courseNumber;
	@Column(name = "COURSE_TITLE")
	public String courseTitle;
	@Column(name = "SUBJECT")
	public String subject;
	@Column(name = "SUBJECT_DESC")
	public String description;
	@Column(name = "CREDIT_HRS")
	public String credits;
	@Column(name = "COURSE_LEVEL")
	public String level;
	@Column(name = "COLLEGE")
	public String college;
	@Column(name = "CRN")
	public String crn;
	@Column(name = "FACULTY")
	public String faculty;
	@Column(name = "DEPARTMENT")
	public String department;
	@Column(name = "CAMPUS")
	public String campus;
	@Column(name = "LECTURE_HOURS")
	public String lectureHours;
	@Column(name = "SEATS_AVAILABLE")
	public String seatsAvailable;
	@Column(name = "WAITLIST_AVAIL")
	public String waitList;
	@Column(name = "course_cart_exist_ind")
	public String cartExistIndicator;

	// @Column(name = "WAIT_LIST_PRIORITY")
	// public String waitListPriority;
	//
	// public String getWaitListPriority() {
	// return waitListPriority;
	// }
	//
	// public void setWaitListPriority(String waitListPriority) {
	// this.waitListPriority = waitListPriority;
	// }

	public String getCartExistIndicator() {
		return cartExistIndicator;
	}

	public void setCartExistIndicator(String cartExistIndicator) {
		this.cartExistIndicator = cartExistIndicator;
	}

	public String getWaitList() {
		return waitList;
	}

	public void setWaitList(String waitList) {
		this.waitList = waitList;
	}

	public String getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(String seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCourseNumber() {
		return courseNumber;
	}

	public void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getTermDescription() {
		return termDescription;
	}

	public void setTermDescription(String termDescription) {
		this.termDescription = termDescription;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getLectureHours() {
		return lectureHours;
	}

	public void setLectureHours(String lectureHours) {
		this.lectureHours = lectureHours;
	}
}
