package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class HoldsList {
	protected List<JHoldsInfo> holds = new ArrayList<JHoldsInfo>(0);

	public List<JHoldsInfo> getHolds() {
		return holds;
	}

	public void setHolds(List<JHoldsInfo> holds) {
		this.holds = holds;
	}

}
