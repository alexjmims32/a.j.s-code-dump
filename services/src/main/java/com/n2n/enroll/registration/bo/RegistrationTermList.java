package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class RegistrationTermList {

	protected List<JTerm> registerTermList = new ArrayList<JTerm>(0);

	public List<JTerm> getRegisterTermList() {
		return registerTermList;
	}

	public void setRegisterTermList(List<JTerm> registerTermList) {
		this.registerTermList = registerTermList;
	}

}
