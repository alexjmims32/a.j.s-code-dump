package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class PartOfTermList {

	protected List<JItem> partOfTermList = new ArrayList<JItem>(0);

	public List<JItem> getPartOfTermList() {
		return partOfTermList;
	}

	public void setPartOfTermList(List<JItem> partOfTermList) {
		this.partOfTermList = partOfTermList;
	}

}
