package com.n2n.enroll.registration.bo;

public class SearchCourseResponse {

	protected JCourse course;
	protected CourseList courseList;
	protected String status;
	public JCourse getCourse() {
		return course;
	}

	public void setCourse(JCourse course) {
		this.course = course;
	}
	
	
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public CourseList getCourseList() {
		return courseList;
	}

	public void setCourseList(CourseList value) {
		this.courseList = value;
	}

}