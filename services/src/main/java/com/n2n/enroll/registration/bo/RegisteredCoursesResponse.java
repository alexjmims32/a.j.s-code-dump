package com.n2n.enroll.registration.bo;

public class RegisteredCoursesResponse {
	protected RegisteredCourseList registeredCourseList;

	public RegisteredCourseList getRegisteredCourseList() {
		return registeredCourseList;
	}

	public void setRegisteredCourseList(RegisteredCourseList value) {
		this.registeredCourseList = value;
	}

}
