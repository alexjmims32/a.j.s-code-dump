package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class CartBo implements java.io.Serializable {
	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	// @Column(name = "STUDENT_ID")
	// private String studentId;
	@Column(name = "CRN")
	private String crn;
	@Column(name = "SUBJECT")
	private String subject;
	@Column(name = "COURSE_LEVEL")
	private String courseLevel;
	@Column(name = "DEPARTMENT")
	private String department;
	@Column(name = "COLLEGE")
	private String college;
	@Column(name = "COURSE_TITLE")
	private String courseName;
	@Column(name = "TERM_CODE")
	private String termCode;
	@Column(name = "FACULTY")
	private String faculty;
	@Column(name = "COURSE_NUMBER")
	private String courseNumber;

	public String getCourseNumber() {
		return courseNumber;
	}

	public void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseLevel() {
		return courseLevel;
	}

	public void setCourseLevel(String courseLevel) {
		this.courseLevel = courseLevel;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

}
