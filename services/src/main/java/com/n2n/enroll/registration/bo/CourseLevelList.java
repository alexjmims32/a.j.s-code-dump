package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class CourseLevelList {

	protected List<JItem> courseLevelList = new ArrayList<JItem>(0);

	public List<JItem> getCourseLevelList() {
		return courseLevelList;
	}

	public void setCourseLevelList(List<JItem> courseLevelList) {
		this.courseLevelList = courseLevelList;
	}

}
