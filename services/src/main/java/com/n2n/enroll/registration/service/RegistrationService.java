package com.n2n.enroll.registration.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.n2n.enroll.bursar.service.BursarService;
import com.n2n.enroll.contact.bo.PersonList;
import com.n2n.enroll.contact.service.ContactService;
import com.n2n.enroll.registration.CourseSearchType;
import com.n2n.enroll.registration.bo.BillingInfoBo;
import com.n2n.enroll.registration.bo.Cart;
import com.n2n.enroll.registration.bo.CartBo;
import com.n2n.enroll.registration.bo.CartList;
import com.n2n.enroll.registration.bo.CourseBo;
import com.n2n.enroll.registration.bo.CourseLevelList;
import com.n2n.enroll.registration.bo.CourseList;
import com.n2n.enroll.registration.bo.CurrentTerm;
import com.n2n.enroll.registration.bo.CurriculumBo;
import com.n2n.enroll.registration.bo.CurriculumInfo;
import com.n2n.enroll.registration.bo.DropBo;
import com.n2n.enroll.registration.bo.DropList;
import com.n2n.enroll.registration.bo.Eligibility;
import com.n2n.enroll.registration.bo.Forums;
import com.n2n.enroll.registration.bo.GradesBo;
import com.n2n.enroll.registration.bo.HoldInfo;
import com.n2n.enroll.registration.bo.HoldsList;
import com.n2n.enroll.registration.bo.JBillingInfo;
import com.n2n.enroll.registration.bo.JCourse;
import com.n2n.enroll.registration.bo.JCurriculum;
import com.n2n.enroll.registration.bo.JDrop;
import com.n2n.enroll.registration.bo.JDropResponse;
import com.n2n.enroll.registration.bo.JHoldsInfo;
import com.n2n.enroll.registration.bo.JItem;
import com.n2n.enroll.registration.bo.JMeet;
import com.n2n.enroll.registration.bo.JMeetingList;
import com.n2n.enroll.registration.bo.JPeople;
import com.n2n.enroll.registration.bo.JPeopleList;
import com.n2n.enroll.registration.bo.JRegister;
import com.n2n.enroll.registration.bo.JRegisterResponse;
import com.n2n.enroll.registration.bo.JRegisteredCourse;
import com.n2n.enroll.registration.bo.JStudentEffectiveInfo;
import com.n2n.enroll.registration.bo.JTerm;
import com.n2n.enroll.registration.bo.JTermsConditions;
import com.n2n.enroll.registration.bo.Meeting;
import com.n2n.enroll.registration.bo.MyInfo;
import com.n2n.enroll.registration.bo.PartOfTermList;
import com.n2n.enroll.registration.bo.PaymentResponse;
import com.n2n.enroll.registration.bo.PeopleBo;
import com.n2n.enroll.registration.bo.PinStatus;
import com.n2n.enroll.registration.bo.ProgramList;
import com.n2n.enroll.registration.bo.RegisterBo;
import com.n2n.enroll.registration.bo.RegisterList;
import com.n2n.enroll.registration.bo.RegisteredCourseList;
import com.n2n.enroll.registration.bo.RegisteredCoursesResponse;
import com.n2n.enroll.registration.bo.RegistrationTermList;
import com.n2n.enroll.registration.bo.ScheduleTypeList;
import com.n2n.enroll.registration.bo.SearchCourseResponse;
import com.n2n.enroll.registration.bo.StudentEffectiveInfo;
import com.n2n.enroll.registration.bo.StudentEffectiveInfoBo;
import com.n2n.enroll.registration.bo.SubjectList;
import com.n2n.enroll.registration.bo.TermsAndCondition;
import com.n2n.enroll.registration.bo.Threads;
import com.n2n.enroll.util.AppUtil;

@SuppressWarnings({ "deprecation", "rawtypes", "unchecked", "resource" })
@Service
public class RegistrationService {
	@Resource
	SessionFactory sessionFactory;

	@Resource
	ContactService contactService;

	@Resource
	BursarService bursarService;

	protected final Log log = LogFactory.getLog(getClass());

	public RegisteredCoursesResponse getRegisteredCourse(String studentId,
			String termCode) {
		if (termCode == null || termCode.equals("")) {
			// CurrentTerm term = getCurrentTerm();
			// termCode = term.getTermCode();

			String sql = "";
			try {
				sql = AppUtil.getProperty("Q_GET_MY_COURSES_DEFAULT_TERM");
			} catch (Exception e) {
				log.error("StackTrace", e);
			}

			sql = (AppUtil.format(sql, studentId));

			List l = AppUtil.runAQuery(sql);

			termCode = (String) l.get(0);
		}

		RegisteredCourseList xRegisteredCourseList = new RegisteredCourseList();
		RegisteredCoursesResponse response = new RegisteredCoursesResponse();
		try {
			String sql = AppUtil.getProperty("Q_GET_REGISTERED_COURSES");

			sql = (AppUtil.format(sql, studentId, termCode));
			log.debug(studentId + ":RegisteredCourses list started ------");
			List<CourseBo> l = (List<CourseBo>) AppUtil
					.runAQueryCurrentSession(sql, CourseBo.class);
			log.debug(studentId + ":RegisteredCourses list completed ------");
			for (CourseBo courseBo : l) {
				JRegisteredCourse xRegisteredCourse = new JRegisteredCourse();
				JCourse xCourse = new JCourse();
				xCourse.setCourseNumber(AppUtil.getStringValue(courseBo
						.getCourseNumber()));
				xCourse.setCourseTitle(AppUtil.getStringValue(courseBo
						.getCourseTitle()));
				xCourse.setSubject(AppUtil.getStringValue(courseBo.getSubject()));
				xCourse.setDescription(AppUtil.getStringValue(courseBo
						.getDescription()));
				xCourse.setCredits(AppUtil.getStringValue(courseBo.getCredits()));
				xCourse.setLevel(AppUtil.getStringValue(courseBo.getLevel()));
				xCourse.setLectureHours(AppUtil.getStringValue(courseBo
						.getLectureHours()));
				xCourse.setScheduleType(AppUtil.getStringValue(courseBo
						.getScheduleType()));
				xCourse.setDepartment(AppUtil.getStringValue(courseBo
						.getDepartment()));
				xCourse.setCampus(AppUtil.getStringValue(courseBo.getCampus()));
				xCourse.setPrerequisite(AppUtil.getStringValue(courseBo
						.getPrerequisite()));
				xCourse.setCrn(AppUtil.getStringValue(courseBo.getCrn()));
				xCourse.setFaculty(AppUtil.getStringValue(courseBo.getFaculty()));
				xCourse.setCollege(AppUtil.getStringValue(courseBo.getCollege()));
				xCourse.setTermDescription(AppUtil.getStringValue(courseBo
						.getTermDescription()));
				xCourse.setTermCode(AppUtil.getStringValue(courseBo
						.getTermCode()));
				xCourse.setSeqNo(AppUtil.getStringValue(courseBo.getSeqNo()));
				xCourse.setActionDesc(AppUtil.getStringValue(courseBo
						.getActionDesc()));
				xCourse.setActionCode(AppUtil.getStringValue(courseBo
						.getActionCode()));
				xCourse.setStatusCode(AppUtil.getStringValue(courseBo
						.getStatusCode()));
				xCourse.setStatusMessage(AppUtil.getStringValue(courseBo
						.getStatusMessage()));
				xCourse.setVarCredit(AppUtil.getStringValue(courseBo
						.getVarCredit()));
				xCourse.setFacultyEmail(AppUtil.getStringValue(courseBo
						.getFacultyEmail()));
				xCourse.setDropFlag(AppUtil.getStringValue(courseBo
						.getDropFlag()));
				xCourse.setLinkId(AppUtil.getStringValue(courseBo
						.getLinkident()));
				if (courseBo.getVarCredit().equals("Y")) {

					String s = (String) courseBo.getVarCreditHrs();

					if (s != null && !s.equals("")) {
						String[] splitArray = s.split("\\s+");

						if (splitArray.length > 0) {
							if (splitArray[1].equals("TO")) {
								xCourse.setMinCredit(AppUtil
										.getStringValue(splitArray[0]));
								xCourse.setMaxCredit(AppUtil
										.getStringValue(splitArray[2]));

							} else if (splitArray[1].equals("OR")) {
								xCourse.setVarCreditHrs((AppUtil
										.getStringValue(splitArray[0] + ","
												+ splitArray[2])));
							}
						}
					}
				}

				JMeetingList xMeetingList = getMeetingList(xCourse);
				xCourse.setMeetingList(xMeetingList);
				xRegisteredCourse.setCourse(xCourse);
				xRegisteredCourse.setRegisteredBy(courseBo.getRegisteredBy());
				xRegisteredCourse.setRegisteredDate(courseBo
						.getRegisteredDate());
				if (courseBo.getFlag() != null
						&& courseBo.getFlag().equalsIgnoreCase("F")) {

					xRegisteredCourse.setStatusCode("FAIL");
					xRegisteredCourse.setStatusMessage(AppUtil
							.getStringValue(courseBo.getErrorMessage()));

				} else {
					xRegisteredCourse.setStatusCode(AppUtil
							.getStringValue(courseBo.getStatusCode()));
					xRegisteredCourse.setStatusMessage(AppUtil
							.getStringValue(courseBo.getStatusMessage()));
				}
				if (courseBo.getStatusCode().equals("WL")) {
					xRegisteredCourse.setStatusCode(AppUtil
							.getStringValue(courseBo.getStatusCode()));
					xRegisteredCourse.setStatusMessage("Wait Listed "
							+ courseBo.getWaitListPriority() + "/"
							+ courseBo.getAvailableWaitList());
				}

				if (courseBo.getFlagDescription().equalsIgnoreCase("Success")) {
					xRegisteredCourse.setStatusCode(courseBo.getStatusCode());
					xRegisteredCourse.setStatusMessage("");
				}
				xRegisteredCourseList.getRegisteredCourse().add(
						xRegisteredCourse);
			}
			response.setRegisteredCourseList(xRegisteredCourseList);
		} catch (Exception e) {
			log.error("Error in retrieving RegisteredCourses...."
					+ e.getMessage());
		}
		return response;
	}

	public RegistrationTermList getRegistrationTerms() {
		RegistrationTermList termList = new RegistrationTermList();
		try {
			String sql = AppUtil.getProperty("Q_GET_REGISTER_TERMS");
			log.debug("RegistrationTerms list started ------" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug("RegistrationTerms list completed ------" + new Date());
			for (Object obj : l) {
				Object[] termfields = (Object[]) obj;
				JTerm xTerm = new JTerm();
				xTerm.setCode(AppUtil.getStringValue(termfields[0]));
				xTerm.setDescription(AppUtil.getStringValue(termfields[1]));
				termList.getRegisterTermList().add(xTerm);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Registration terms......"
					+ e.getMessage());
		}
		return termList;
	}

	public RegistrationTermList getMyCoursesTerms(String studentId) {
		RegistrationTermList termList = new RegistrationTermList();
		try {
			String sql = AppUtil.getProperty("Q_GET_MY_COURSES_DEFAULT_TERM");

			sql = (AppUtil.format(sql, studentId));

			List dl = AppUtil.runAQuery(sql);

			String defaultTermCode = (String) dl.get(0);

			sql = AppUtil.getProperty("Q_GET_MY_COURSES_TERMS");

			sql = (AppUtil.format(sql, studentId));
			log.debug(studentId + ": MyCoursesTerms list started ------");
			List l = AppUtil.runAQuery(sql);
			log.debug(studentId + ": MyCoursesTerms list completed ------");
			for (Object obj : l) {
				Object[] termfields = (Object[]) obj;
				JTerm xTerm = new JTerm();
				xTerm.setCode(AppUtil.getStringValue(termfields[0]));
				xTerm.setDescription(AppUtil.getStringValue(termfields[1]));
				if (defaultTermCode.equals(AppUtil
						.getStringValue(termfields[0]))) {
					xTerm.setDefaultTerm("true");
				}
				termList.getRegisterTermList().add(xTerm);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing My Courses terms......"
					+ e.getMessage());
		}
		return termList;
	}

	public JRegisterResponse register(String studentId, String termCode)
			throws HibernateException, SQLException, Exception {
		JRegisterResponse response = new JRegisterResponse();
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": register procedure started ------");
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_DO_REGISTER") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);

			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.execute();
			log.debug(studentId + ": register procedure completed ------");
			String status = callableStatement.getString(3);
			callableStatement.close();

			if (status.equalsIgnoreCase("Registration Completed.")) {
				response = getRegistrationResponse(studentId, termCode);
				response.setStatus("SUCCESS");

			} else
				response.setStatus(status);

		} catch (Exception e) {
			log.error("In catch Block with error before refreshing");
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				log.error("In catch Block with error after refreshed");
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}

			log.error("Error in  registration ..." + e.getMessage());
		}
		return response;
	}

	public Eligibility getEligibility(String studentId, String termCode)
			throws HibernateException, SQLException, Exception {
		Eligibility response = new Eligibility();
		String errorMsg = "";
		int x = 0;
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": Eligibility procedure started ------");
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_GET_ELIGIBILITY") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.execute();
			log.debug(studentId + ": Eligibility procedure completed ------");
			errorMsg = callableStatement.getString(3);
			callableStatement.close();
			if (!(errorMsg == null)) {

				x = errorMsg.length();
				response.setStatus(errorMsg.substring(0, 1).toUpperCase()
						.concat(errorMsg.substring(1, x).toLowerCase()));
			}
		} catch (Exception e) {
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}
			log.error("Error in Course Removing..." + e.getMessage());
			log.error("StackTrace", e);
		}

		return response;
	}

	public JDropResponse dropCourse(String studentId, String termCode,
			String crn) throws HibernateException, SQLException, Exception {
		JDropResponse response = new JDropResponse();
		String errorMsg = "";
		String status = "SUCCESS";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ":  DropCourse procedure started ------");
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_DROP_COURSE") + "}");
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.setString(3, crn);
			callableStatement.execute();
			log.debug(studentId + ":    DropCourse procedure completed ------");
			errorMsg = callableStatement.getString(4);
			callableStatement.close();
			if (!(errorMsg == null)) {
				response.setStatus(errorMsg);

			} else {
				response = getDropResponse(studentId, termCode);
				response.setStatus(status);

			}
		} catch (Exception e) {
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}
			log.error("Error in Course Removing..." + e.getMessage());
			log.error("StackTrace", e);
		}
		return response;
	}

	public CurriculumInfo myCurriculumInfo(String studentId) {
		CurriculumInfo curriculumInfo = new CurriculumInfo();
		try {
			String sql = AppUtil.getProperty("Q_GET_CURRICULUM_INFO");

			sql = (AppUtil.format(sql, studentId));
			log.debug(studentId + ": CurriculumInfo list started ------");
			List<CurriculumBo> cl = (List<CurriculumBo>) AppUtil
					.runAQueryCurrentSession(sql, CurriculumBo.class);
			log.debug(studentId + ": CurriculumInfo list completed ------");
			for (CurriculumBo curriculumBo : cl) {
				JCurriculum curriculum = new JCurriculum();
				curriculum.setTermCode(AppUtil.getStringValue(curriculumBo
						.getTermCode()));
				curriculum.setLevel(AppUtil.getStringValue(curriculumBo
						.getLevel()));
				curriculum.setAdmitType(AppUtil.getStringValue(curriculumBo
						.getAdmitType()));
				curriculum.setCollege(AppUtil.getStringValue(curriculumBo
						.getCollege()));
				curriculum.setMajor(AppUtil.getStringValue(curriculumBo
						.getMajor()));
				curriculum.setDepartment(AppUtil.getStringValue(curriculumBo
						.getDepartment()));
				curriculum.setProgram(AppUtil.getStringValue(curriculumBo
						.getProgram()));
				curriculum.setProgramDesc(AppUtil.getStringValue(curriculumBo
						.getProgramDesc()));
				curriculum.setCatalogTerm(AppUtil.getStringValue(curriculumBo
						.getCatalogTerm()));
				curriculum.setCampus(AppUtil.getStringValue(curriculumBo
						.getCampus()));
				curriculum.setConcentration(AppUtil.getStringValue(curriculumBo
						.getConcentration()));
				// HoldsList holdsList = new HoldsList();
				// String hSql = AppUtil.getProperty("Q_GET_HOLDS_INFO");

				// hSql = (hFmt.format(hSql, studentId)).toString();
				// List<HoldInfo> hl = (List<HoldInfo>) AppUtil
				// .runAQueryCurrentSession(hSql, HoldInfo.class);
				// for (HoldInfo holdInfoBo : hl) {
				// JHoldsInfo holdsInfo = new JHoldsInfo();
				//
				// holdsInfo.setHoldsDescription(AppUtil
				// .getStringValue(holdInfoBo.getHoldsDescription()));
				// holdsInfo.setHoldAmount(AppUtil.getStringValue(holdInfoBo
				// .getHoldAmount()));
				// holdsInfo.setHoldReleaseIndicator(AppUtil
				// .getStringValue(holdInfoBo
				// .getHoldReleaseIndicator()));
				// holdsInfo.setHoldOriginOffice(AppUtil
				// .getStringValue(holdInfoBo.getHoldOriginOffice()));
				// holdsList.getHolds().add(holdsInfo);
				//
				// }
				// curriculum.setHoldList(holdsList);
				curriculumInfo.setCurriculumInfo(curriculum);
			}

		} catch (Exception e) {
			log.error(" Error in retriveing CurriculumInfo......"
					+ e.getMessage());
		}
		return curriculumInfo;
	}

	public StudentEffectiveInfo myStudentInfo(String studentId, String termCode) {
		StudentEffectiveInfo studentEffectiveInfo = new StudentEffectiveInfo();
		try {
			if (termCode == null) {
				String sql1 = AppUtil
						.getProperty("Q_GET_MY_COURSES_DEFAULT_TERM");
				sql1 = AppUtil.format(sql1, studentId);
				List<String> l = AppUtil.runAQuery(sql1);
				termCode = (String) l.get(0);
			}
			String sql = AppUtil.getProperty("Q_GET_STUDENT_INFO");

			sql = (AppUtil.format(sql, studentId, termCode));
			List<StudentEffectiveInfoBo> sl = (List<StudentEffectiveInfoBo>) AppUtil
					.runAQueryCurrentSession(sql, StudentEffectiveInfoBo.class);
			for (StudentEffectiveInfoBo studentInfoBo : sl) {
				JStudentEffectiveInfo studentInfo = new JStudentEffectiveInfo();
				studentInfo.setStudentId(AppUtil.getStringValue(studentInfoBo
						.getStudentId()));
				studentInfo.setStudentInf(AppUtil.getStringValue(studentInfoBo
						.getStudentInf()));
				studentInfo.setFromTerm(AppUtil.getStringValue(studentInfoBo
						.getFromTerm()));
				studentInfo.setToTerm(AppUtil.getStringValue(studentInfoBo
						.getToTerm()));
				studentInfo.setIsRegistered(AppUtil
						.getStringValue(studentInfoBo.getIsRegistered()));
				studentInfo.setFirstTerm(AppUtil.getStringValue(studentInfoBo
						.getFirstTerm()));
				studentInfo.setLastTerm(AppUtil.getStringValue(studentInfoBo
						.getLastTerm()));
				studentInfo.setStudentStatus(AppUtil
						.getStringValue(studentInfoBo.getStudentStatus()));
				studentInfo.setTermCodeMatric(AppUtil
						.getStringValue(studentInfoBo.getTermCodeMatric()));
				studentInfo.setResidence(AppUtil.getStringValue(studentInfoBo
						.getResidence()));
				studentInfo.setCitizen(AppUtil.getStringValue(studentInfoBo
						.getCitizen()));
				studentInfo.setStudentType(AppUtil.getStringValue(studentInfoBo
						.getStudentType()));
				studentInfo.setExpGradeDate(AppUtil
						.getStringValue(studentInfoBo.getExpGradeDate()));
				studentEffectiveInfo.setStudentInfo(studentInfo);
			}

		} catch (Exception e) {
			log.error(" Error in retriveing CurriculumInfo......"
					+ e.getMessage());
		}
		return studentEffectiveInfo;
	}

	public MyInfo myInfo(String studentId, String termCode) {
		MyInfo studentEffectiveInfo = new MyInfo();
		try {
			if (termCode == null) {
				String sql1 = AppUtil
						.getProperty("Q_GET_MY_COURSES_DEFAULT_TERM");
				sql1 = AppUtil.format(sql1, studentId);
				List<String> l = AppUtil.runAQuery(sql1);
				termCode = (String) l.get(0);
			}
			String sql = AppUtil.getProperty("Q_GET_STUDENT_INFO");

			sql = (AppUtil.format(sql, studentId, termCode));
			List<StudentEffectiveInfoBo> sl = (List<StudentEffectiveInfoBo>) AppUtil
					.runAQueryCurrentSession(sql, StudentEffectiveInfoBo.class);
			for (StudentEffectiveInfoBo studentInfoBo : sl) {
				JStudentEffectiveInfo studentInfo = new JStudentEffectiveInfo();
				studentInfo.setStudentId(AppUtil.getStringValue(studentInfoBo
						.getStudentId()));
				studentInfo.setStudentInf(AppUtil.getStringValue(studentInfoBo
						.getStudentInf()));
				studentInfo.setFromTerm(AppUtil.getStringValue(studentInfoBo
						.getFromTerm()));
				studentInfo.setToTerm(AppUtil.getStringValue(studentInfoBo
						.getToTerm()));
				studentInfo.setIsRegistered(AppUtil
						.getStringValue(studentInfoBo.getIsRegistered()));
				studentInfo.setFirstTerm(AppUtil.getStringValue(studentInfoBo
						.getFirstTerm()));
				studentInfo.setLastTerm(AppUtil.getStringValue(studentInfoBo
						.getLastTerm()));
				studentInfo.setStudentStatus(AppUtil
						.getStringValue(studentInfoBo.getStudentStatus()));
				studentInfo.setTermCodeMatric(AppUtil
						.getStringValue(studentInfoBo.getTermCodeMatric()));
				studentInfo.setResidence(AppUtil.getStringValue(studentInfoBo
						.getResidence()));
				studentInfo.setCitizen(AppUtil.getStringValue(studentInfoBo
						.getCitizen()));
				studentInfo.setStudentType(AppUtil.getStringValue(studentInfoBo
						.getStudentType()));
				studentInfo.setExpGradeDate(AppUtil
						.getStringValue(studentInfoBo.getExpGradeDate()));
				CurriculumInfo curInfo = myCurriculumInfo(studentId);
				PersonList pList = contactService.getContact(studentId);

				studentInfo.setCurriculumInfo(curInfo);
				studentInfo.setPlist(pList);
				studentEffectiveInfo.setStudentInfo(studentInfo);
			}

		} catch (Exception e) {
			log.error(" Error in retriveing CurriculumInfo......"
					+ e.getMessage());
		}
		return studentEffectiveInfo;
	}

	public CurrentTerm getCurrentTerm() {
		CurrentTerm response = new CurrentTerm();
		String termCode = "";
		String description = "";
		Session session = sessionFactory.getCurrentSession();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		log.debug(" CurrentTerm procedure started------" + new Date());
		try {
			CallableStatement callableStatement = session.connection()
					.prepareCall(
							"{call " + AppUtil.getProperty("F_GET_TERMCODE")
									+ "}");
			callableStatement.setString(1, dateFormat.format(date));
			callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.execute();
			log.debug(" CurrentTerm procedure completed------" + new Date());
			String errMsg = callableStatement.getString(4);
			if (errMsg == null || errMsg.equals("")) {
				termCode = callableStatement.getString(2);
				description = callableStatement.getString(3);
			} else {
				response.setErrMsg(errMsg);
			}
		} catch (Exception e) {
			log.error("Error in getting Current Term" + e.getMessage());
		}
		response.setTermCode(termCode);
		response.setDescription(description);
		return response;

	}

	public Cart addToCart(String studentId, String crn, String termCode,
			String rstsCode,String startDate,String endDate) throws HibernateException, SQLException, Exception {
		Cart response = new Cart();
		String errorMsg = "";
		String status = "SUCCESS";

		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": addToCart procedure started------"
				+ new Date());
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_ADD_TO_CART") + "}");
			callableStatement.registerOutParameter(7, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, crn);
			callableStatement.setString(3, termCode);
			callableStatement.setString(4, rstsCode);
			callableStatement.setString(5, startDate);
			callableStatement.setString(6, endDate);
			callableStatement.execute();
			log.debug(studentId + ": addToCart procedure completed------"
					+ new Date());
			errorMsg = callableStatement.getString(7);
			callableStatement.close();
			if (!(errorMsg == null || errorMsg.equals(""))) {
				response.setStatus(errorMsg);
			} else
				response.setStatus(status);
		} catch (Exception e) {
			log.error("Error in course for add to Cart..." + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}

		}
		return response;
	}

	public Cart removeFromCart(String studentId, String crn, String termCode)
			throws HibernateException, SQLException, Exception {
		Cart response = new Cart();
		String errorMsg = "";
		String status = "SUCCESS";

		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": removeFromCart procedure started------"
				+ new Date());
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_REMOVE_FROM_CART") + "}");
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, crn);
			callableStatement.setString(3, termCode);
			callableStatement.execute();
			log.debug(studentId + ": removeFromCart procedure completed------"
					+ new Date());
			errorMsg = callableStatement.getString(4);
			callableStatement.close();
			if (!(errorMsg == null || errorMsg.equals(""))) {
				response.setStatus(errorMsg);
			} else
				response.setStatus(status);
		} catch (Exception e) {
			log.error("Error in Removing course from Cart..." + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}
		}
		return response;
	}

	public Cart clearCart(String studentId, String termCode)
			throws HibernateException, SQLException, Exception {
		Cart response = new Cart();
		String errorMsg = "";
		String status = "SUCCESS";

		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": clearCart procedure started-----" + new Date());
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_CLEAR_CART") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.execute();
			log.debug(studentId + ": clearCart procedure completed-----"
					+ new Date());
			errorMsg = callableStatement.getString(3);
			callableStatement.close();
			if (!(errorMsg == null || errorMsg.equals(""))) {
				response.setStatus(errorMsg);
			} else
				response.setStatus(status);
		} catch (Exception e) {
			log.error("Error in clear Cart..." + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}
		}
		return response;
	}

	public Eligibility checkAltPinStatus(String studentId, String termCode)
			throws HibernateException, SQLException, Exception {
		Eligibility response = new Eligibility();
		String errorMsg = "";
		String pin = "";

		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_CHECK_PIN_STATUS") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.execute();
			pin = callableStatement.getString(3);
			errorMsg = callableStatement.getString(4);
			if (!(errorMsg == null)) {
				response.setStatus(errorMsg);
			} else {
				response.setStatus(pin);
			}

			callableStatement.close();
		} catch (Exception e) {
			log.error("Error in clear Cart..." + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				response.setStatus("Server error has occured.Please try again");
			} else {
				response.setStatus(e.getMessage());
			}
		}
		return response;
	}

	public CartList getCartView(String studentId, String termCode) {
		CartList cartList = new CartList();
		try {
			String sql = AppUtil.getProperty("Q_GET_CART_ITEMS");

			sql = (AppUtil.format(sql, studentId));

			if (termCode != null && !termCode.equals("")) {
				sql += " AND TERM_CODE = " + termCode;
			}
			log.debug(studentId + ": viewCart list started----" + new Date());
			List<CartBo> l = (List<CartBo>) AppUtil.runAQueryCurrentSession(
					sql, CartBo.class);
			log.debug(studentId + ": viewCart list completed----" + new Date());
			cartList = getAllCoursesInCart(l);

		} catch (Exception e) {
			log.error("Error in view Cart" + e.getMessage());
			log.error("StackTrace", e);
		}
		return cartList;
	}

	private CartList getAllCoursesInCart(List<CartBo> l) {
		CartList cartList = new CartList();
		try {

			if (l.size() > 0) {

				for (CartBo cartBo : l) {
					Cart jCart = new Cart();

					jCart.setCrn(AppUtil.getStringValue(cartBo.getCrn()));
					jCart.setSubject(AppUtil.getStringValue(cartBo.getSubject()));

					jCart.setCourseName(AppUtil.getStringValue(cartBo
							.getCourseName()));

					jCart.setCourseLevel(AppUtil.getStringValue(cartBo
							.getCourseLevel()));

					jCart.setDepartment(AppUtil.getStringValue(cartBo
							.getDepartment()));

					jCart.setCollege(AppUtil.getStringValue(cartBo.getCollege()));
					jCart.setTermCode(AppUtil.getStringValue(cartBo
							.getTermCode()));
					jCart.setFaculty(AppUtil.getStringValue(cartBo.getFaculty()));
					jCart.setCourseNumber(AppUtil.getStringValue(cartBo
							.getCourseNumber()));
					cartList.getCourse().add(jCart);

				}

			} else {
				cartList.setStatus("Cart is Empty");
			}
		} catch (Exception e) {
			log.error("StackTrace", e);
		}
		return cartList;
	}

	public JRegisterResponse getRegistrationResponse(String studentId,
			String termCode) {
		JRegisterResponse response = new JRegisterResponse();
		RegisterList rstsList = new RegisterList();
		try {
			String sql = AppUtil.getProperty("Q_GET_REGISTER_STATUS");

			sql = (AppUtil.format(sql, studentId, termCode));
			log.debug(studentId + ": register status started ------"
					+ new Date());
			List<RegisterBo> l = (List<RegisterBo>) AppUtil
					.runAQueryCurrentSession(sql, RegisterBo.class);
			log.debug(studentId + ": register status completed ------"
					+ new Date());

			if (l != null && l.size() > 0) {
				for (RegisterBo registerBo : l) {
					JRegister xRegister = new JRegister();
					xRegister
							.setCrn(AppUtil.getStringValue(registerBo.getCrn()));
					xRegister.setCourseTitle(AppUtil.getStringValue(registerBo
							.getCourseTitle()));
					xRegister.setLevel(AppUtil.getStringValue(registerBo
							.getLevel()));
					xRegister.setDepartment(AppUtil.getStringValue(registerBo
							.getDepartment()));
					xRegister.setCollege(AppUtil.getStringValue(registerBo
							.getCollege()));
					xRegister.setStatusCode(AppUtil.getStringValue(registerBo
							.getStatusCode()));
					xRegister.setStatusMessage(AppUtil
							.getStringValue(registerBo.getStatusMessage()));
					xRegister.setSubject(AppUtil.getStringValue(registerBo
							.getSubject()));

					xRegister.setCourseNumber(AppUtil.getStringValue(registerBo
							.getCourseNumber()));

					xRegister.setFaculty(AppUtil.getStringValue(registerBo
							.getFaculty()));
					xRegister.setErrorFlag(AppUtil.getStringValue(registerBo
							.getErrorFlag()));
					rstsList.getRegister().add(xRegister);
				}
				response.setRegisterList(rstsList);
				delete(studentId, termCode);
			}
		} catch (Exception e) {
			log.error("Error in registration " + e.getMessage());
			log.error("StackTrace", e);
		}
		return response;
	}

	public SearchCourseResponse getCourse(String studentId, String termCode,
			String crn) throws Exception {
		SearchCourseResponse response = new SearchCourseResponse();
		String sql = AppUtil.getProperty("Q_GET_COURSE");

		sql = AppUtil.format(sql, studentId, termCode, termCode, crn, crn);
		log.debug(studentId + ": getCourse list started----" + new Date());
		List l = AppUtil.runAQuery(sql);
		log.debug(studentId + ": getCourse list completed----" + new Date());
		Object crseObj = l.get(0);
		Object[] crseFields = (Object[]) crseObj;
		JCourse xCourse = new JCourse();
		xCourse.setTermCode(AppUtil.getStringValue(crseFields[1]));
		xCourse.setCourseNumber(AppUtil.getStringValue(crseFields[3]));
		xCourse.setCrn(AppUtil.getStringValue(crseFields[4]));
		xCourse.setSubject(AppUtil.getStringValue(crseFields[5]));
		xCourse.setCourseTitle(AppUtil.getStringValue(crseFields[6]));
		xCourse.setCredits(AppUtil.getStringValue(crseFields[7]));
		xCourse.setCampus(AppUtil.getStringValue(crseFields[8]));
		xCourse.setLevel(AppUtil.getStringValue(crseFields[9]));
		xCourse.setLectureHours(AppUtil.getStringValue(crseFields[10]));
		xCourse.setSeatsAvailable(AppUtil.getStringValue(crseFields[15]));
		xCourse.setWaitedList(AppUtil.getStringValue(crseFields[16]));
		xCourse.setDepartment(AppUtil.getStringValue(crseFields[11]));
		xCourse.setFaculty(AppUtil.getStringValue(crseFields[12]));
		xCourse.setCollege(AppUtil.getStringValue(crseFields[13]));
		xCourse.setDescription(AppUtil.getStringValue(crseFields[14]));
		xCourse.setTermDescription(AppUtil.getStringValue(crseFields[2]));
		xCourse.setCourseIndicator(AppUtil.getStringValue(crseFields[17]));

		xCourse.setNoOfSeatsAvailable(AppUtil.getStringValue(crseFields[18]));
		xCourse.setTotalSeats(AppUtil.getStringValue(crseFields[19]));
		xCourse.setFacultyEmail(AppUtil.getStringValue(crseFields[20]));
		xCourse.setCourseComments(AppUtil.getStringValue(crseFields[21]));
		xCourse.setSeqNo(AppUtil.getStringValue(crseFields[22]));
		xCourse.setLinkId(AppUtil.getStringValue(crseFields[23]));
		JMeetingList xMeetingList = getMeetingList(xCourse);
		xCourse.setMeetingList(xMeetingList);

		response.setCourse(xCourse);

		return response;
	}

	public void delete(String studentId, String termCode) {
		try {
			String sql = AppUtil.getProperty("Q_DELETE_CART_SUCCESS");

			sql = (AppUtil.format(sql, studentId, termCode));
			AppUtil.runADeleteQuery(sql);

		} catch (Exception e) {
			log.error("Error in view Cart" + e.getMessage());
		}
	}

	public JMeetingList getMeetingList(JCourse xCourse) {
		JMeetingList xMeetingList = new JMeetingList();
		try {
			String metQuery = AppUtil.getProperty("Q_GET_MEETING");

			metQuery = (AppUtil.format(metQuery, xCourse.getTermCode(),
					xCourse.getCrn()));
			List<Meeting> ml = (List<Meeting>) AppUtil.runAQueryCurrentSession(
					metQuery, Meeting.class);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outFormat = new SimpleDateFormat("MM-dd-yyyy");

			for (Meeting meetBo : ml) {
				JMeet xMeeting = new JMeet();
				xMeeting.setBeginTime(AppUtil.getTime(meetBo.getBeginTime()));
				xMeeting.setEndTime(AppUtil.getTime(meetBo.getEndTime()));
				xMeeting.setBldgCode(AppUtil.getStringValue(meetBo
						.getBldgCode()));
				xMeeting.setStartDate(outFormat.format(sdf.parse(AppUtil
						.getStringValue(meetBo.getStartDate().substring(0, 10)))));

				xMeeting.setEndDate(outFormat.format(sdf.parse(AppUtil
						.getStringValue(meetBo.getEndDate().substring(0, 10)))));
				xMeeting.setMtypCode(AppUtil.getStringValue(meetBo
						.getMtypCode()));
				xMeeting.setRoomCode(AppUtil.getStringValue(meetBo
						.getRoomCode()));
				xMeeting.setLatitude(AppUtil.getStringValue(meetBo
						.getLatitude()));
				xMeeting.setLongitude(AppUtil.getStringValue(meetBo
						.getLongitude()));
				xMeeting.setScheduleCode(AppUtil.getStringValue(meetBo
						.getScheduleCode()));
				xMeeting.setScheduleDesc(AppUtil.getStringValue(meetBo
						.getScheduleDesc()));
				String period = "";
				if (!(meetBo.getMonDay() == null || meetBo.getMonDay().equals(
						"")))
					period = period + "MON";
				if (!(meetBo.getTuesDay() == null || meetBo.getTuesDay()
						.equals(""))) {
					if (period.equals(""))
						period = period + "TUE";
					else
						period = period + ":TUE";
				}
				if (!(meetBo.getWednesDay() == null || meetBo.getWednesDay()
						.equals(""))) {
					if (period.equals(""))
						period = period + "WED";
					else
						period = period + ":WED";
				}
				if (!(meetBo.getThursDay() == null || meetBo.getThursDay()
						.equals(""))) {
					if (period.equals(""))
						period = period + "THU";
					else
						period = period + ":THU";
				}
				if (!(meetBo.getFriDay() == null || meetBo.getFriDay().equals(
						""))) {
					if (period.equals(""))
						period = period + "FRI";
					else
						period = period + ":FRI";
				}
				if (!(meetBo.getSatDay() == null || meetBo.getSatDay().equals(
						""))) {
					if (period.equals(""))
						period = period + "SAT";
					else
						period = period + ":SAT";
				}
				if (!(meetBo.getSunDay() == null || meetBo.getSunDay().equals(
						""))) {
					if (period.equals(""))
						period = period + "SUN";
					else
						period = period + ":SUN";
				}
				if (period.equals(""))
					period = "NA";
				xMeeting.setPeriod(period);
				xMeetingList.getMeeting().add(xMeeting);
			}
		} catch (Exception e) {
			log.error("Error in retrieving meetingList" + e.getMessage());
		}
		return xMeetingList;
	}

	public PinStatus checkPin(String studentId, String termCode)
			throws HibernateException, SQLException, Exception {
		PinStatus pinStatus = new PinStatus();
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": checkPin procedure started ------"
				+ new Date());
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_CHECK_PIN_STATUS") + "}");
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.execute();
			log.debug(studentId + ": checkPin procedure completed ------"
					+ new Date());
			String pin = callableStatement.getString(3);
			String errorMsg = callableStatement.getString(4);
			if (!(errorMsg == null)) {
				pinStatus.setStatusMsg(errorMsg);
			} else {
				pinStatus.setStatus(pin);
			}
			callableStatement.close();
		} catch (Exception e) {
			log.error("Error in check pin : " + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				pinStatus
						.setStatus("Server error has occured.Please try again");
			} else {
				pinStatus.setStatus(e.getMessage());
			}
			return pinStatus;
		}

		return pinStatus;
	}

	public PinStatus validatePin(String studentId, String termCode, String pin)
			throws HibernateException, Exception {
		PinStatus pinStatus = new PinStatus();
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": validatePin procedure started ------"
				+ new Date());
		try {
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("F_VALIDATE_PIN") + "}");
			callableStatement.registerOutParameter(1, java.sql.Types.VARCHAR);
			callableStatement.setString(2, studentId);
			callableStatement.setString(3, termCode);
			callableStatement.setString(4, "TREG");
			callableStatement.setString(5, pin);
			callableStatement.execute();
			log.debug(studentId + ": validatePin procedure completed ------"
					+ new Date());
			String pinValStatus = callableStatement.getString(1);
			callableStatement.close();
			if (pinValStatus.equalsIgnoreCase("N")) {
				pinStatus.setStatusMsg("Alternate Pin Validation Failed");
				pinStatus.setStatus("N");
				return pinStatus;
			} else {
				pinStatus.setStatus(pinValStatus);
			}
		} catch (Exception e) {
			log.error("Error in validate pin : " + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				pinStatus
						.setStatus("Server error has occured.Please try again");
			} else {
				pinStatus.setStatus(e.getMessage());
			}
			return pinStatus;
		}

		return pinStatus;
	}

	public CourseList search(String studentId, String termCode, String subCode,
			String searchString, int start, int limit, int page)
			throws Exception {

		CourseList xCourseList = new CourseList();
		xCourseList.setStatus("SUCCESS");
		if (searchString != null && !searchString.equals("")) {
			String courseSearchType = AppUtil.getProperty("COURSE_SEARCH_TYPE");
			String query = "";
			if (courseSearchType.equals(CourseSearchType.USE_MATERIALIZED_VIEW)) {
				query = AppUtil.getProperty("Q_SEARCH_COURSE_MV");

				query = (AppUtil.format(query, termCode, "%" + searchString
						+ "%", subCode));

			} else {
				query = AppUtil.getProperty("Q_SEARCH_COURSE_VW");

				String[] values;
				String condition = "";
				String delimiter = " ";
				/*
				 * given string will be split by the argument delimiter
				 * provided.
				 */
				if (subCode != null && !subCode.equals("")) {
					condition = condition + "     SUBJECT=" + "'" + subCode
							+ "'";
				}
				values = searchString.split(delimiter);
				for (String value : values) {
					condition = condition + " AND SEARCH_STRING LIKE UPPER('%"
							+ value + "%') ";
				}

				// Remove the AND at the beginning
				condition = condition.substring(5);

				query = (AppUtil.format(query, studentId, studentId, termCode,
						condition, subCode));
			}
			List l = AppUtil.runAQuery(query, page, limit);

			for (Object obj : l) {
				Object[] course = (Object[]) obj;
				JCourse xCourse = new JCourse();
				xCourse.setTermCode(AppUtil.getStringValue(course[0]));
				xCourse.setCourseNumber(AppUtil.getStringValue(course[1]));
				xCourse.setCrn(AppUtil.getStringValue(course[2]));
				xCourse.setSubject(AppUtil.getStringValue(course[3]));
				xCourse.setCourseTitle(AppUtil.getStringValue(course[5]));
				xCourse.setCredits(AppUtil.getStringValue(course[7]));
				xCourse.setCampus(AppUtil.getStringValue(course[9]));
				xCourse.setCampusCode(AppUtil.getStringValue(course[10]));
				xCourse.setLevel(AppUtil.getStringValue(course[11]));
				xCourse.setLectureHours(AppUtil.getStringValue(course[12]));
				xCourse.setSeatsAvailable(AppUtil.getStringValue(course[13]));
				xCourse.setNoOfSeatsAvailable(AppUtil
						.getStringValue(course[24]));
				xCourse.setTotalSeats(AppUtil.getStringValue(course[25]));
				xCourse.setWaitedList(AppUtil.getStringValue(course[14]));
				xCourse.setDepartment(AppUtil.getStringValue(course[17]));
				xCourse.setFaculty(AppUtil.getStringValue(course[18]));
				xCourse.setCollege(AppUtil.getStringValue(course[19]));
				xCourse.setDescription(AppUtil.getStringValue(course[4]));
				xCourse.setTermDescription(AppUtil.getStringValue(course[20]));
				xCourse.setCourseIndicator(AppUtil.getStringValue(course[21]));
				xCourse.setVarCredit(AppUtil.getStringValue(course[22]));
				xCourse.setSeqNo(AppUtil.getStringValue(course[26]));
				xCourse.setLinkId(AppUtil.getStringValue(course[27]));
				xCourse.setCourseComments(AppUtil.getStringValue(course[28]));
				xCourse.setPrereqcheck(AppUtil.getStringValue(course[29]));
				xCourse.setCourseMeetings(AppUtil.getStringValue(course[30]));
				xCourse.setCourseDates(AppUtil.getStringValue(course[31]));
				xCourse.setOlrInd(AppUtil.getStringValue(course[32]));
				if (course[22].equals("Y")) {

					String s = (String) course[23];

					if (s != null && !s.equals("")) {
						String[] splitArray = s.split("\\s+");

						if (splitArray.length > 0) {
							if (splitArray[1].equals("TO")) {
								xCourse.setMinCredit(AppUtil
										.getStringValue(splitArray[0]));
								xCourse.setMaxCredit(AppUtil
										.getStringValue(splitArray[2]));

							} else if (splitArray[1].equals("OR")) {
								xCourse.setVarCreditHrs((AppUtil
										.getStringValue(splitArray[0] + ","
												+ splitArray[2])));
							}
						}
					}
				}

				xCourseList.getCourse().add(xCourse);

			}
		}
		return xCourseList;
	}

	public CourseList advSearch(String studentId, String termCode,
			String partOfTerm, String scheduleType, String crseAttr,
			String crn, String subject, String crseNum, String crseLevel,
			String title, int start, int limit, int page) throws Exception {
		CourseList xCourseList = new CourseList();
		xCourseList.setStatus("SUCCESS");
		String condition = "";

		if (termCode != null && !termCode.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + " TERM_CODE=" + "'" + termCode + "'";
		}

		if (partOfTerm != null && !partOfTerm.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(PART_OF_TERM) LIKE UPPER('%"
					+ partOfTerm + "%')";
		}

		if (scheduleType != null && !scheduleType.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(SCHEDULE_CODE) LIKE UPPER('%"
					+ scheduleType + "%')";
		}

		if (crn != null && !crn.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(CRN) LIKE UPPER('%" + crn + "%')";
		}

		if (subject != null && !subject.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(SUBJECT) LIKE UPPER('%" + subject
					+ "%')";
		}

		if (crseNum != null && !crseNum.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(COURSE_NUMBER) LIKE UPPER('%"
					+ crseNum + "%')";
		}

		if (crseLevel != null && !crseLevel.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(COURSE_LEVEL) LIKE UPPER('%"
					+ crseLevel + "%')";
		}

		if (crseAttr != null && !crseAttr.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(COURSE_ATTRIBUTE) LIKE UPPER('%"
					+ crseAttr + "%')";
		}

		if (title != null && !title.equals("")) {
			if (!condition.equals("")) {
				condition = condition + " AND ";
			}
			condition = condition + "UPPER(COURSE_TITLE) LIKE UPPER('%" + title
					+ "%')";
		}

		// if (searchString != null && !searchString.equals("")) {
		String courseSearchType = AppUtil.getProperty("COURSE_SEARCH_TYPE");
		String query = "";
		if (courseSearchType.equals(CourseSearchType.USE_MATERIALIZED_VIEW)) {
			query = AppUtil.getProperty("Q_ADV_SEARCH_COURSE_MV");

			query = (AppUtil.format(query, condition));

		} else {
			query = AppUtil.getProperty("Q_ADV_SEARCH_COURSE_VW");

			query = (AppUtil.format(query, studentId, studentId, condition));
		}

		List l = AppUtil.runAQuery(query, page, limit);

		for (Object obj : l) {
			Object[] course = (Object[]) obj;
			JCourse xCourse = new JCourse();
			xCourse.setTermCode(AppUtil.getStringValue(course[0]));
			xCourse.setCourseNumber(AppUtil.getStringValue(course[1]));
			xCourse.setCrn(AppUtil.getStringValue(course[2]));
			xCourse.setSubject(AppUtil.getStringValue(course[3]));
			xCourse.setCourseTitle(AppUtil.getStringValue(course[5]));
			xCourse.setCredits(AppUtil.getStringValue(course[7]));
			xCourse.setCampus(AppUtil.getStringValue(course[9]));
			xCourse.setCampusCode(AppUtil.getStringValue(course[10]));
			xCourse.setLevel(AppUtil.getStringValue(course[11]));
			xCourse.setLectureHours(AppUtil.getStringValue(course[12]));
			xCourse.setSeatsAvailable(AppUtil.getStringValue(course[13]));
			xCourse.setNoOfSeatsAvailable(AppUtil.getStringValue(course[24]));
			xCourse.setTotalSeats(AppUtil.getStringValue(course[25]));
			xCourse.setWaitedList(AppUtil.getStringValue(course[14]));
			xCourse.setDepartment(AppUtil.getStringValue(course[17]));
			xCourse.setFaculty(AppUtil.getStringValue(course[18]));
			xCourse.setCollege(AppUtil.getStringValue(course[19]));
			xCourse.setDescription(AppUtil.getStringValue(course[4]));
			xCourse.setTermDescription(AppUtil.getStringValue(course[20]));
			xCourse.setCourseIndicator(AppUtil.getStringValue(course[21]));
			xCourse.setVarCredit(AppUtil.getStringValue(course[22]));
			xCourse.setSeqNo(AppUtil.getStringValue(course[26]));
			xCourse.setLinkId(AppUtil.getStringValue(course[27]));
			xCourse.setCourseComments(AppUtil.getStringValue(course[28]));
			xCourse.setPrereqcheck(AppUtil.getStringValue(course[29]));
			xCourse.setCourseMeetings(AppUtil.getStringValue(course[30]));
			xCourse.setCourseDates(AppUtil.getStringValue(course[31]));
			xCourse.setCrseAttr(AppUtil.getStringValue(course[32]));
			xCourse.setOlrInd(AppUtil.getStringValue(course[33]));
			if (course[22].equals("Y")) {

				String s = (String) course[23];

				if (s != null && !s.equals("")) {
					String[] splitArray = s.split("\\s+");

					if (splitArray.length > 0) {
						if (splitArray[1].equals("TO")) {
							xCourse.setMinCredit(AppUtil
									.getStringValue(splitArray[0]));
							xCourse.setMaxCredit(AppUtil
									.getStringValue(splitArray[2]));

						} else if (splitArray[1].equals("OR")) {
							xCourse.setVarCreditHrs((AppUtil
									.getStringValue(splitArray[0] + ","
											+ splitArray[2])));
						}
					}
				}
			}

			xCourseList.getCourse().add(xCourse);

		}
		// }
		return xCourseList;
	}

	public String updateCreditHrs(String studentId, String termCode,
			String crn, String creditHrs, String oldCreditHrs)
			throws HibernateException, Exception {
		String status = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			log.debug(studentId + ": updateCreditHrs procedure started---"
					+ new Date());
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_CHANGE_CREDITS") + "}");

			callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);

			callableStatement.setString(1, studentId);
			callableStatement.setString(2, termCode);
			callableStatement.setInt(3, Integer.valueOf(crn));
			callableStatement.setInt(4, Integer.valueOf(creditHrs));
			callableStatement.setInt(5, Integer.valueOf(oldCreditHrs));
			callableStatement.execute();
			log.debug(studentId + ": updateCreditHrs procedure completed---"
					+ new Date());
			String errorMsg = callableStatement.getString(6);
			callableStatement.close();
			if (errorMsg != null) {
				status = errorMsg;
			} else {
				status = "SUCCESS";
			}

		} catch (Exception e) {
			log.error("Error in Update credit Hrs:" + e.getMessage());
			if (e.getMessage().contains("ORA-04068")
					|| e.getMessage().contains("ORA-04061")
					|| e.getMessage().contains("ORA-04065")) {

				callableStatement = session.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_RESET_PACKAGE")
										+ "}");
				callableStatement.execute();
				callableStatement.close();
				status = "Server error has occured.Please try again";
			} else {
				status = e.getMessage();
			}
			log.error("StackTrace", e);
		}

		return status;
	}

	public JCurriculum myHoldInfo(String studentId) {
		HoldsList holdsList = new HoldsList();
		JCurriculum curriculum = new JCurriculum();
		try {

			String hSql = AppUtil.getProperty("Q_GET_HOLDS_INFO");

			hSql = (AppUtil.format(hSql, studentId));
			log.debug(studentId + ":HoldsInfo list started ------" + new Date());
			List<HoldInfo> hl = (List<HoldInfo>) AppUtil
					.runAQueryCurrentSession(hSql, HoldInfo.class);
			log.debug(studentId + ":HoldsInfo list completed ------"
					+ new Date());
			for (HoldInfo holdInfoBo : hl) {
				JHoldsInfo holdsInfo = new JHoldsInfo();

				holdsInfo.setHoldsDescription(AppUtil.getStringValue(holdInfoBo
						.getHoldsDescription()));
				holdsInfo.setHoldAmount(AppUtil.getStringValue(holdInfoBo
						.getHoldAmount()));
				holdsInfo.setHoldReleaseIndicator(AppUtil
						.getStringValue(holdInfoBo.getHoldReleaseIndicator()));
				holdsInfo.setProcessesAffected(AppUtil
						.getStringValue(holdInfoBo.getProcessesAffected()));
				holdsInfo.setHoldOriginOffice(AppUtil.getStringValue(holdInfoBo
						.getHoldOriginOffice()));
				holdsInfo.setHoldReason(AppUtil.getStringValue(holdInfoBo
						.getHoldReason()));
				if (!(AppUtil.getStringValue(holdInfoBo.getHoldFromDate())
						.equals("NA"))) {
					String ts = holdInfoBo.getHoldFromDate();
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
							.parse(ts);
					String fromDate = new SimpleDateFormat("yyyy-MM-dd")
							.format(date);
					holdsInfo.setHoldFromDate(fromDate);
				}
				if (!(AppUtil.getStringValue(holdInfoBo.getHoldToDate())
						.equals("NA"))) {
					String ts = holdInfoBo.getHoldToDate();
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")
							.parse(ts);
					String toDate = new SimpleDateFormat("yyyy-MM-dd")
							.format(date);
					holdsInfo.setHoldToDate(toDate);
				}

				holdsList.getHolds().add(holdsInfo);
			}
			curriculum.setHoldList(holdsList);
		} catch (Exception e) {
			log.error("Error in retriveing HoldInfo: " + e.getMessage());
		}
		return curriculum;
	}

	public SearchCourseResponse getCartSchedule(String studentId,
			String termCode) {
		SearchCourseResponse response = new SearchCourseResponse();
		CourseList courseList = new CourseList();

		String sql = "";
		String status = "SUCCESS";
		try {
			sql = AppUtil.getProperty("Q_GET_CRSE_LIST");
			sql = (AppUtil.format(sql, studentId, termCode));
			log.debug(studentId + ": CartSchedule list started---" + new Date());
			List<JCourse> l = AppUtil.runAQuery(sql);
			log.debug(studentId + ": CartSchedule list completed---"
					+ new Date());
			String sql2 = AppUtil.getProperty("Q_GET_MEETING_LIST");
			String metQuery = (AppUtil.format(sql2, studentId, termCode));
			log.debug(studentId + ": meeting list started---" + new Date());
			List<Meeting> ml = (List<Meeting>) AppUtil.runAQueryCurrentSession(
					metQuery, Meeting.class);
			log.debug(studentId + ": meeting list completed---" + new Date());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outFormat = new SimpleDateFormat("MM-dd-yyyy");

			for (int i = 0; i < l.size(); i++) {

				Object crseObj = l.get(i);
				Object[] crseFields = (Object[]) crseObj;
				JCourse xCourse = new JCourse();
				xCourse.setTermCode(AppUtil.getStringValue(crseFields[1]));
				xCourse.setTermDescription(AppUtil
						.getStringValue(crseFields[2]));
				xCourse.setCourseNumber(AppUtil.getStringValue(crseFields[3]));
				xCourse.setCrn(AppUtil.getStringValue(crseFields[4]));
				xCourse.setSubject(AppUtil.getStringValue(crseFields[5]));
				xCourse.setCourseTitle(AppUtil.getStringValue(crseFields[6]));
				xCourse.setCredits(AppUtil.getStringValue(crseFields[7]));
				xCourse.setCampus(AppUtil.getStringValue(crseFields[8]));
				xCourse.setLevel(AppUtil.getStringValue(crseFields[9]));
				xCourse.setLectureHours(AppUtil.getStringValue(crseFields[10]));
				xCourse.setSeatsAvailable(AppUtil
						.getStringValue(crseFields[15]));
				xCourse.setWaitedList(AppUtil.getStringValue(crseFields[16]));
				xCourse.setDepartment(AppUtil.getStringValue(crseFields[11]));
				xCourse.setFaculty(AppUtil.getStringValue(crseFields[12]));
				xCourse.setCollege(AppUtil.getStringValue(crseFields[13]));
				xCourse.setDescription(AppUtil.getStringValue(crseFields[14]));

				JMeetingList xMeetingList = new JMeetingList();
				for (Meeting meetBo : ml) {
					if (crseFields[4].equals(meetBo.getCrn())) {
						JMeet xMeeting = new JMeet();
						xMeeting.setBeginTime(AppUtil.getTime(meetBo
								.getBeginTime()));
						xMeeting.setEndTime(AppUtil.getTime(meetBo.getEndTime()));
						xMeeting.setBldgCode(AppUtil.getStringValue(meetBo
								.getBldgCode()));
						xMeeting.setStartDate(outFormat.format(sdf
								.parse(AppUtil.getStringValue(meetBo
										.getStartDate().substring(0, 10)))));

						xMeeting.setEndDate(outFormat.format(sdf.parse(AppUtil
								.getStringValue(meetBo.getEndDate().substring(
										0, 10)))));
						xMeeting.setMtypCode(AppUtil.getStringValue(meetBo
								.getMtypCode()));
						xMeeting.setRoomCode(AppUtil.getStringValue(meetBo
								.getRoomCode()));
						xMeeting.setLatitude(AppUtil.getStringValue(meetBo
								.getLatitude()));
						xMeeting.setLongitude(AppUtil.getStringValue(meetBo
								.getLongitude()));
						xMeeting.setScheduleCode(AppUtil.getStringValue(meetBo
								.getScheduleCode()));
						xMeeting.setScheduleDesc(AppUtil.getStringValue(meetBo
								.getScheduleDesc()));
						String period = "";
						if (!(meetBo.getMonDay() == null || meetBo.getMonDay()
								.equals("")))
							period = period + "MON";
						if (!(meetBo.getTuesDay() == null || meetBo
								.getTuesDay().equals(""))) {
							if (period.equals(""))
								period = period + "TUE";
							else
								period = period + ":TUE";
						}
						if (!(meetBo.getWednesDay() == null || meetBo
								.getWednesDay().equals(""))) {
							if (period.equals(""))
								period = period + "WED";
							else
								period = period + ":WED";
						}
						if (!(meetBo.getThursDay() == null || meetBo
								.getThursDay().equals(""))) {
							if (period.equals(""))
								period = period + "THU";
							else
								period = period + ":THU";
						}
						if (!(meetBo.getFriDay() == null || meetBo.getFriDay()
								.equals(""))) {
							if (period.equals(""))
								period = period + "FRI";
							else
								period = period + ":FRI";
						}
						if (!(meetBo.getSatDay() == null || meetBo.getSatDay()
								.equals(""))) {
							if (period.equals(""))
								period = period + "SAT";
							else
								period = period + ":SAT";
						}
						if (!(meetBo.getSunDay() == null || meetBo.getSunDay()
								.equals(""))) {
							if (period.equals(""))
								period = period + "SUN";
							else
								period = period + ":SUN";
						}
						if (period.equals(""))
							period = "NA";
						xMeeting.setPeriod(period);
						xMeetingList.getMeeting().add(xMeeting);
					}
				}
				xCourse.setMeetingList(xMeetingList);
				courseList.getCourse().add(xCourse);
				response.setCourseList(courseList);
			}
			response.setStatus(status);
		} catch (Exception e) {
			log.error("Error in getCartSchedule: " + e.getMessage());
			log.error("StackTrace", e);
			response.setStatus(e.getMessage());
		}

		return response;
	}

	public String getInfo(String url, String studentLdapId) throws IOException,
			Exception {
		String secret = AppUtil.getProperty("BB_SHARED_SECRET_KEY");
		String macarray[] = new String[2];
		macarray[0] = AppUtil.getTimeStamp();
		macarray[1] = studentLdapId;

		String mac = calculateSecureMAC(macarray, secret);
		url = url + "&timestamp=" + AppUtil.getTimeStamp() + "&user_id="
				+ studentLdapId + "&mac=" + mac;
		log.debug("The mac value is---" + mac);
		URL weburl;
		String result = null;
		weburl = new URL(url);
		InputStream ins = weburl.openConnection().getInputStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(ins,
				Charset.forName("UTF-8")));
		result = readAll(rd);
		return (result);
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			if (cp > 127) {
				// Skip all special characters
				continue;
			}
			sb.append((char) cp);
		}

		return sb.toString();
	}

	public Forums[] getThreads(String userId, String courseID, String forumID)
			throws IOException, Exception {

		String url = AppUtil.getProperty("SERVER_NAME");
		url = url + "get_forum_threads&course_id=" + courseID + "&forum_id="
				+ forumID;

		String res = getInfo(url, userId);

		Forums[] f = AppUtil.getJsonFormString(res, Forums[].class);

		return f;
	}

	public List<Threads> formatObject(Forums forum, Forums[] threads) {
		List<Threads> threadList = new ArrayList<Threads>(0);

		for (Forums forumThread : threads) {
			Threads thread = new Threads();
			thread.setForumId(forum.getId());
			thread.setForumTitle(forum.getTitle());
			thread.setForumDescription(forum.getDescription());
			thread.setForumStartDate(forum.getStartDate());
			thread.setForumEndDate(forum.getEndDate());
			thread.setIsForumAvailable(forum.getIsAvailable());
			thread.setThreadId(forumThread.getId());
			thread.setThreadSubject(forumThread.getSubject());
			thread.setThreadBody(forumThread.getBody());
			thread.setThreadParentId(forumThread.getParentId());
			thread.setThreadPostDate(forumThread.getPostDate());
			thread.setThreadPostedName(forumThread.getPostedName());
			thread.setThreadEditDate(forumThread.getEditDate());
			thread.setThreadResponses(forumThread.getResponses());

			threadList.add(thread);
		}

		return threadList;
	}

	public RegisteredCoursesResponse getCurrentTermCourses(String studentId) {

		RegisteredCourseList xRegisteredCourseList = new RegisteredCourseList();
		RegisteredCoursesResponse response = new RegisteredCoursesResponse();
		try {
			String sql = AppUtil.getProperty("Q_GET_CURRENT_TERM_COURSES");

			sql = (AppUtil.format(sql, studentId, "%5"));
			log.debug(studentId + ":CurrentTermCourses list started---"
					+ new Date());
			List<CourseBo> l = (List<CourseBo>) AppUtil
					.runAQueryCurrentSession(sql, CourseBo.class);
			log.debug(studentId + ":CurrentTermCourses list completed---"
					+ new Date());
			for (CourseBo courseBo : l) {
				JRegisteredCourse xRegisteredCourse = new JRegisteredCourse();
				JCourse xCourse = new JCourse();
				xCourse.setCourseNumber(AppUtil.getStringValue(courseBo
						.getCourseNumber()));
				xCourse.setCourseTitle(AppUtil.getStringValue(courseBo
						.getCourseTitle()));
				xCourse.setSubject(AppUtil.getStringValue(courseBo.getSubject()));
				xCourse.setDescription(AppUtil.getStringValue(courseBo
						.getDescription()));
				xCourse.setCredits(AppUtil.getStringValue(courseBo.getCredits()));
				xCourse.setLevel(AppUtil.getStringValue(courseBo.getLevel()));
				xCourse.setLectureHours(AppUtil.getStringValue(courseBo
						.getLectureHours()));
				xCourse.setScheduleType(AppUtil.getStringValue(courseBo
						.getScheduleType()));
				xCourse.setDepartment(AppUtil.getStringValue(courseBo
						.getDepartment()));
				xCourse.setCampus(AppUtil.getStringValue(courseBo.getCampus()));
				xCourse.setPrerequisite(AppUtil.getStringValue(courseBo
						.getPrerequisite()));
				xCourse.setCrn(AppUtil.getStringValue(courseBo.getCrn()));
				xCourse.setFaculty(AppUtil.getStringValue(courseBo.getFaculty()));
				xCourse.setCollege(AppUtil.getStringValue(courseBo.getCollege()));
				xCourse.setTermDescription(AppUtil.getStringValue(courseBo
						.getTermDescription()));
				xCourse.setTermCode(AppUtil.getStringValue(courseBo
						.getTermCode()));

				xCourse.setVarCredit(AppUtil.getStringValue(courseBo
						.getVarCredit()));
				xCourse.setFacultyEmail(AppUtil.getStringValue(courseBo
						.getFacultyEmail()));
				if (courseBo.getVarCredit().equals("Y")) {

					String s = (String) courseBo.getVarCreditHrs();

					if (s != null && !s.equals("")) {
						String[] splitArray = s.split("\\s+");

						if (splitArray.length > 0) {
							if (splitArray[1].equals("TO")) {
								xCourse.setMinCredit(AppUtil
										.getStringValue(splitArray[0]));
								xCourse.setMaxCredit(AppUtil
										.getStringValue(splitArray[2]));

							} else if (splitArray[1].equals("OR")) {
								xCourse.setVarCreditHrs((AppUtil
										.getStringValue(splitArray[0] + ","
												+ splitArray[2])));
							}
						}
					}
				}

				JMeetingList xMeetingList = getMeetingList(xCourse);
				xCourse.setMeetingList(xMeetingList);
				xRegisteredCourse.setCourse(xCourse);
				xRegisteredCourse.setRegisteredBy(courseBo.getRegisteredBy());
				xRegisteredCourse.setRegisteredDate(courseBo
						.getRegisteredDate());
				if (courseBo.getFlag() != null
						&& courseBo.getFlag().equalsIgnoreCase("F")) {

					xRegisteredCourse.setStatusCode("FAIL");
					xRegisteredCourse.setStatusMessage(AppUtil
							.getStringValue(courseBo.getErrorMessage()));

				} else {
					xRegisteredCourse.setStatusCode(AppUtil
							.getStringValue(courseBo.getStatusCode()));
					xRegisteredCourse.setStatusMessage(AppUtil
							.getStringValue(courseBo.getStatusMessage()));
				}
				if (courseBo.getStatusCode().equals("WL")) {
					xRegisteredCourse.setStatusCode(AppUtil
							.getStringValue(courseBo.getStatusCode()));
					xRegisteredCourse.setStatusMessage("Wait Listed "
							+ courseBo.getWaitListPriority() + "/"
							+ courseBo.getAvailableWaitList());
				}

				if (courseBo.getFlagDescription().equalsIgnoreCase("Success")) {
					xRegisteredCourse.setStatusCode(courseBo.getStatusCode());
					xRegisteredCourse.setStatusMessage("");
				}
				xRegisteredCourseList.getRegisteredCourse().add(
						xRegisteredCourse);
			}
			response.setRegisteredCourseList(xRegisteredCourseList);
		} catch (Exception e) {
			log.error("Error in retrieving current term courses...."
					+ e.getMessage());
		}
		return response;
	}

	public String getStudentId(String authHeader) {
		String userId = "";

		if (authHeader.startsWith("Basic")) {
			String userInfo = authHeader.substring(6).trim();

			String nameAndPassword;

			// BASE64Decoder decoder = new BASE64Decoder();
			// nameAndPassword = new String(decoder.decodeBuffer(userInfo));

			byte[] nameAndPasswordBytes = DatatypeConverter
					.parseBase64Binary(userInfo);
			nameAndPassword = new String(nameAndPasswordBytes);

			int index = nameAndPassword.indexOf(":");
			userId = nameAndPassword.substring(0, index);
		}

		return userId;
	}

	public List<GradesBo> getGrades(String studentId, String crn,
			String termCode) {
		List<GradesBo> grades = new ArrayList<GradesBo>(0);

		try {

			String sql = AppUtil.getProperty("Q_GET_GRADES");

			sql = (AppUtil.format(sql, studentId, crn, termCode));
			grades = (List<GradesBo>) AppUtil.runAQueryCurrentSession(sql,
					GradesBo.class);

		} catch (Exception e) {
			log.error("Error in grades" + e.getMessage());
			log.error("StackTrace", e);
		}

		return grades;
	}

	public JDropResponse getDropResponse(String studentId, String termCode) {
		JDropResponse response = new JDropResponse();
		DropList drpList = new DropList();
		try {
			String sql = AppUtil.getProperty("Q_GET_DROP_STATUS");
			sql = (AppUtil.format(sql, studentId, termCode));
			log.debug(studentId + ": DropCourse status started------"
					+ new Date());
			List<DropBo> l = (List<DropBo>) AppUtil.runAQueryCurrentSession(
					sql, DropBo.class);
			log.debug(studentId + ": DropCourse status completed ------"
					+ new Date());
			List<JDrop> dropList = new ArrayList<JDrop>();
			for (DropBo dropBo : l) {
				JDrop xDrop = new JDrop();
				xDrop.setCrn(AppUtil.getStringValue(dropBo.getCrn()));
				xDrop.setCourseTitle(AppUtil.getStringValue(dropBo
						.getCourseTitle()));
				xDrop.setLevel(AppUtil.getStringValue(dropBo.getLevel()));
				xDrop.setDepartment(AppUtil.getStringValue(dropBo
						.getDepartment()));
				xDrop.setCollege(AppUtil.getStringValue(dropBo.getCollege()));
				xDrop.setStatus(AppUtil.getStringValue(dropBo.getStatus()));
				xDrop.setErrorMessage(AppUtil.getStringValue(dropBo
						.getErrorMessage()));
				xDrop.setSubject(AppUtil.getStringValue(dropBo.getSubject()));

				xDrop.setCourseNumber(AppUtil.getStringValue(dropBo
						.getCourseNumber()));

				xDrop.setFaculty(AppUtil.getStringValue(dropBo.getFaculty()));

				dropList.add(xDrop);
			}
			drpList.setDrop(dropList);
			response.setDropList(drpList);
			// delete(studentId, termCode);
		} catch (Exception e) {
			log.error("Error in drop course " + e.getMessage());
			log.error("StackTrace", e);
		}
		return response;
	}

	public PaymentResponse getPaymentUrl(String studentId) {
		PaymentResponse response = new PaymentResponse();
		String url = "";
		try {
			String source = AppUtil.getProperty("PAYMENTURL_SOURCE");
			if (source.equals("BANNER_PROC")) {
				url = getBannerPaymentUrl(studentId);
			} else if (source.equals("LINK")) {
				url = getPaymentLink();
			}
			response.setUrl(url);
		} catch (Exception e) {
			log.error("Error in Getting Payment Url..." + e.getMessage());
			response.setStatus(e.getMessage());
		}

		return response;
	}

	public String getBannerPaymentUrl(String studentId) {
		String url = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		try {
			String source = AppUtil.getProperty("PAYMENTURL_SOURCE");
			if (source.equals("BANNER_PROC")) {

			} else if (source.equals("LINK")) {

			}
			log.debug(studentId + ": PaymentGateway procedure started--"
					+ new Date());
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_PAYMENT_GATEWAY") + "}");
			callableStatement.registerOutParameter(2, java.sql.Types.VARCHAR);
			callableStatement.setString(1, studentId);
			callableStatement.execute();
			log.debug(studentId + ": PaymentGateway procedure completed--"
					+ new Date());
			url = callableStatement.getString(2);
			callableStatement.close();
		} catch (Exception e) {
			log.error("Error in Getting Payment Url..." + e.getMessage());

			return e.getMessage();
		}
		return url;
	}

	public String getPaymentLink() {
		String url = "", sql = "";
		try {
			sql = AppUtil.getProperty("Q_GET_PAYMENT_LINK");
			log.debug(": Paymentlink  started--" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug(": Paymentlink  completed--" + new Date());
			Object obj = l.get(0);
			url = AppUtil.getStringValue(obj);
		} catch (Exception e) {
			log.error("Error in Getting Payment Url..." + e.getMessage());
			return e.getMessage();
		}
		return url;
	}

	public JPeopleList getClassmates(String termCode, String crn) {
		String sql = "";
		JPeopleList peopleList = new JPeopleList();
		try {
			sql = AppUtil.getProperty("Q_GET_CLASSMATES");
			sql = AppUtil.format(sql, termCode, crn);
			List<PeopleBo> classmates = (List<PeopleBo>) AppUtil
					.runAQueryCurrentSession(sql, PeopleBo.class);
			for (PeopleBo clsmate : classmates) {
				JPeople people = new JPeople();
				people.setId(clsmate.getId());
				people.setEmail(clsmate.getEmail());
				people.setPhoneArea(clsmate.getPhoneArea());
				people.setPhoneNumber(clsmate.getPhoneNumber());
				people.setPhoneExt(clsmate.getPhoneExt());
				people.setName(clsmate.getName());
				peopleList.getFacultyList().add(people);
			}
		} catch (Exception e) {
			log.error("Error in GetClassMates : " + e.getMessage());
			log.error("StackTrace", e);
		}
		return peopleList;
	}

	public JPeopleList getFaculty(String termCode, String studentId, String crn) {
		String sql = "";
		JPeopleList peopleList = new JPeopleList();
		try {
			sql = AppUtil.getProperty("Q_GET_FACULTY");
			sql = AppUtil.format(sql, termCode, studentId, crn);
			List<PeopleBo> faculty = (List<PeopleBo>) AppUtil
					.runAQueryCurrentSession(sql, PeopleBo.class);
			for (PeopleBo faclty : faculty) {
				JPeople people = new JPeople();
				people.setId(faclty.getId());
				people.setEmail(faclty.getEmail());
				people.setPhoneArea(faclty.getPhoneArea());
				people.setPhoneNumber(faclty.getPhoneNumber());
				people.setPhoneExt(faclty.getPhoneExt());
				people.setName(faclty.getName());
				peopleList.getFacultyList().add(people);
			}
		} catch (Exception e) {
			log.error("Error in GetFaculty : " + e.getMessage());
			log.error("StackTrace", e);
		}
		return peopleList;
	}

	public JPeopleList getAdvisors(String termCode, String studentId) {
		String sql = "";
		JPeopleList peopleList = new JPeopleList();
		try {
			sql = AppUtil.getProperty("Q_GET_ADVISORS");
			sql = AppUtil.format(sql, studentId, termCode);

			List<PeopleBo> advisors = (List<PeopleBo>) AppUtil
					.runAQueryCurrentSession(sql, PeopleBo.class);
			for (PeopleBo advsrs : advisors) {
				JPeople people = new JPeople();
				people.setId(advsrs.getId());
				people.setEmail(advsrs.getEmail());
				people.setPhoneArea(advsrs.getPhoneArea());
				people.setPhoneNumber(advsrs.getPhoneNumber());
				people.setPhoneExt(advsrs.getPhoneExt());
				people.setName(advsrs.getName());
				peopleList.getFacultyList().add(people);
			}
		} catch (Exception e) {
			log.error("Error in GetAdvisors : " + e.getMessage());
			log.error("StackTrace", e);
		}
		return peopleList;
	}

	public JBillingInfo getBillingInfo(String termCode, String studentId) {

		JBillingInfo billingInfo = new JBillingInfo();
		if (termCode == null || termCode.equals("")) {
			// CurrentTerm term = getCurrentTerm();
			// termCode = term.getTermCode();

			String sql = "";
			try {
				sql = AppUtil.getProperty("Q_GET_MY_COURSES_DEFAULT_TERM");
			} catch (Exception e) {
				log.error("StackTrace", e);
			}

			sql = (AppUtil.format(sql, studentId));

			List l = AppUtil.runAQuery(sql);

			termCode = (String) l.get(0);
		}
		try {
			String sql = "";
			sql = AppUtil.getProperty("Q_GET_BILLING_INFO");
			sql = AppUtil.format(sql, studentId, termCode);

			List<BillingInfoBo> billingInfoBo = (List<BillingInfoBo>) AppUtil
					.runAQueryCurrentSession(sql, BillingInfoBo.class);
			for (BillingInfoBo bilInfo : billingInfoBo) {
				billingInfo.setTermCode(bilInfo.getTermCode());
				billingInfo.setBillHours(bilInfo.getBillHours());
				billingInfo.setCreditHours(bilInfo.getCreditHours());
				billingInfo.setMaxHours(bilInfo.getMaxHours());
				billingInfo.setMinHours(bilInfo.getMinHours());
			}
		} catch (Exception e) {
			log.error("Error in GetAdvisors : " + e.getMessage());
			log.error("StackTrace", e);
		}
		return billingInfo;
	}

	public String calculateSecureMAC(final String[] values, final String secret)
			throws NoSuchAlgorithmException {
		final int size = values.length;
		System.out.println(size);
		String paramString = "";

		for (int i = 0; i < size; i++) {
			paramString += values[i];
		}

		System.out.println(paramString);

		final MessageDigest md = MessageDigest.getInstance("MD5");
		final byte[] hashBytes = md.digest((paramString + secret).getBytes());
		md.reset();

		String mac = "";
		String hexByte;

		for (int k = 0; k < hashBytes.length; k++) {
			hexByte = Integer.toHexString(hashBytes[k] < 0 ? hashBytes[k] + 256
					: hashBytes[k]);
			mac += (hexByte.length() == 1) ? "0" + hexByte : hexByte;
		}
		return mac;
	}

	public SubjectList getSubjectList(String termCode) {
		SubjectList list = new SubjectList();
		try {
			String sql = AppUtil.getProperty("Q_GET_SUBJECT_LIST");
			sql = AppUtil.format(sql, termCode);
			log.debug("Subject list started ------" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug("Subject list completed ------" + new Date());
			for (Object obj : l) {
				Object[] fields = (Object[]) obj;
				JItem item = new JItem();
				item.setCode(AppUtil.getStringValue(fields[0]));
				item.setDescription(AppUtil.getStringValue(fields[1]));
				list.getSubjectList().add(item);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Subject list......"
					+ e.getMessage());
		}
		return list;
	}

	public ProgramList getProgramList(String termCode) {
		ProgramList list = new ProgramList();
		try {
			String sql = AppUtil.getProperty("Q_GET_PROGRAM_LIST");
			sql = AppUtil.format(sql, termCode);
			log.debug("Program list started ------" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug("Program list completed ------" + new Date());
			for (Object obj : l) {
				Object[] fields = (Object[]) obj;
				JItem item = new JItem();
				item.setCode(AppUtil.getStringValue(fields[0]));
				item.setDescription(AppUtil.getStringValue(fields[1]));
				list.getProgramList().add(item);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Program list......"
					+ e.getMessage());
		}
		return list;
	}

	public CourseLevelList getCourseLevelList() {
		CourseLevelList list = new CourseLevelList();
		try {
			String sql = AppUtil.getProperty("Q_GET_COURSE_LEVEL");
			log.debug("Course level list started ------" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug("Course level list completed ------" + new Date());
			for (Object obj : l) {
				Object[] fields = (Object[]) obj;
				JItem item = new JItem();
				item.setCode(AppUtil.getStringValue(fields[0]));
				item.setDescription(AppUtil.getStringValue(fields[1]));
				list.getCourseLevelList().add(item);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Course level list......"
					+ e.getMessage());
		}
		return list;
	}

	public PartOfTermList getPartOfTermList(String termCode) {
		PartOfTermList list = new PartOfTermList();
		try {
			String sql = AppUtil.getProperty("Q_GET_PART_OF_TERM");
			sql = AppUtil.format(sql, termCode);
			log.debug("Part of term list started ------" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug("Part of term list completed ------" + new Date());
			for (Object obj : l) {
				Object[] fields = (Object[]) obj;
				JItem item = new JItem();
				item.setCode(AppUtil.getStringValue(fields[0]));
				item.setDescription(AppUtil.getStringValue(fields[1]));
				list.getPartOfTermList().add(item);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Part of term list......"
					+ e.getMessage());
		}
		return list;
	}

	public ScheduleTypeList getScheduleTypeList() {
		ScheduleTypeList list = new ScheduleTypeList();
		try {
			String sql = AppUtil.getProperty("Q_GET_SCHEDULE_TYPE");
			log.debug("Schedule type list started ------" + new Date());
			List l = AppUtil.runAQuery(sql);
			log.debug("Schedule type list completed ------" + new Date());
			for (Object obj : l) {
				Object[] fields = (Object[]) obj;
				JItem item = new JItem();
				item.setCode(AppUtil.getStringValue(fields[0]));
				item.setDescription(AppUtil.getStringValue(fields[1]));
				list.getScheduleTypeList().add(item);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing Schedule type list......"
					+ e.getMessage());
		}
		return list;
	}

	public RegistrationTermList getFacultyTerms(String facultyId) {
		RegistrationTermList termList = new RegistrationTermList();
		try {

			String sql = AppUtil
					.getProperty("Q_GET_FACULTY_COURSES_DEFAULT_TERM");

			sql = (AppUtil.format(sql, facultyId));

			List dl = AppUtil.runAQuery(sql);

			String defaultTermCode = (String) dl.get(0);

			sql = AppUtil.getProperty("Q_GET_FACULTY_TERMS");

			sql = (AppUtil.format(sql, facultyId));
			log.debug(facultyId + ": MyCoursesTerms list started ------");
			List l = AppUtil.runAQuery(sql);
			log.debug(facultyId + ": MyCoursesTerms list completed ------");
			for (Object obj : l) {
				Object[] termfields = (Object[]) obj;
				JTerm xTerm = new JTerm();
				xTerm.setCode(AppUtil.getStringValue(termfields[0]));
				xTerm.setDescription(AppUtil.getStringValue(termfields[1]));
				if (defaultTermCode.equals(AppUtil
						.getStringValue(termfields[0])))
					xTerm.setDefaultTerm("true");
				termList.getRegisterTermList().add(xTerm);
			}
		} catch (Exception e) {
			log.error(" Error in retriveing My Courses terms......"
					+ e.getMessage());
		}
		return termList;
	}

	public String getImmunizationContent(String studentId)
			throws HibernateException, SQLException, Exception {
		Clob data;
		String response = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId + ": ImmunizationContent procedure started ------");
		try {
			callableStatement = session.connection().prepareCall(
					"{call "
							+ AppUtil.getProperty("P_GET_IMMUNIZATION_CONTENT")
							+ "}");
			callableStatement.registerOutParameter(2, java.sql.Types.CLOB);
			callableStatement.setString(1, studentId);
			callableStatement.execute();
			log.debug(studentId
					+ ": ImmunizationContent procedure completed ------");
			data = callableStatement.getClob(2);
			callableStatement.close();
			if (!(data == null)) {
				response = bursarService.getClobData(data);
			}
		} catch (Exception e) {
			log.error("Error in get Immunization Content..." + e.getMessage());
		}

		return response;
	}

	public String submitImmunizationContent(String studentId, String aques,
			String bques) throws HibernateException, SQLException, Exception {
		String response = "";
		Session session = sessionFactory.getCurrentSession();
		CallableStatement callableStatement = null;
		log.debug(studentId
				+ ": Submit ImmunizationContent procedure started ------");
		try {
			callableStatement = session
					.connection()
					.prepareCall(
							"{call "
									+ AppUtil
											.getProperty("P_Submit_IMMUNIZATION_CONTENT")
									+ "}");
			callableStatement.setString(1, studentId);
			callableStatement.setString(2, aques);
			callableStatement.setString(3, bques);
			callableStatement.execute();
			log.debug(studentId
					+ ": Submit ImmunizationContent procedure completed ------");
			callableStatement.close();
		} catch (Exception e) {
			response = e.getMessage();
			log.error("Error in Submit Immunization Content..."
					+ e.getMessage());
		}

		return response;
	}

	public JTermsConditions getTermsAndConditions(String campusCode,
			String termCode, String studentId, String crn, String indicator) {
		JTermsConditions termConditions = new JTermsConditions();
		try {
			if (AppUtil.getProperty("TERMS_CONDITIONS_SOURCE")
					.equalsIgnoreCase("BANNER")) {
				Session session = sessionFactory.getCurrentSession();
				CallableStatement callableStatement = null;
				log.debug("Terms and Conditions procedure started ------");
				callableStatement = session
						.connection()
						.prepareCall(
								"{call "
										+ AppUtil
												.getProperty("P_TERMS_CONDITIONS_TO_REGISTER")
										+ "}");
				callableStatement.setString(1, termCode);
				callableStatement.setString(2, studentId);
				callableStatement.setString(3, indicator);
				callableStatement.setString(4, crn);
				callableStatement.registerOutParameter(5, java.sql.Types.CLOB);
				callableStatement.execute();
				log.debug(studentId
						+ ": Terms and Conditions procedure completed ------");
				Clob data = callableStatement.getClob(5);
				callableStatement.close();
				if (data != null) {
					termConditions.setTermAndConditions(bursarService
							.getClobData(data));
				} else {
					termConditions.setTermAndConditions(null);
				}

				termConditions.setCampusCode(campusCode);
			} else {
				String query = AppUtil.getProperty("Q_GET_TERM_CONDITIONS");
				query = AppUtil.format(query, campusCode);
				List<TermsAndCondition> areaTextAndFaq = (List<TermsAndCondition>) AppUtil
						.runAQueryCurrentSession(query, TermsAndCondition.class);
				for (TermsAndCondition tac : areaTextAndFaq) {
					termConditions
							.setTermAndConditions(tac.getTermConditions());
					termConditions.setCampusCode(tac.getCampusCode());
				}
			}

		} catch (Exception e) {
			log.error("Error In TermsAndConditions " + e.getMessage());
			log.error("StackTrace", e);
		}
		return termConditions;
	}

	public Cart getOlrDates(String termCode, String crn) {
		Cart response = new Cart();
		String duration = "";
		String startDate = "";
		String endDate = "";
		try {
			Session session = sessionFactory.getCurrentSession();
			CallableStatement callableStatement = null;
			log.debug("OLR COURSE procedure started ------");
			callableStatement = session.connection().prepareCall(
					"{call " + AppUtil.getProperty("P_OLR_DATES") + "}");
			callableStatement.setString(1, termCode);
			callableStatement.setString(2, crn);
			callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(4, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);
			callableStatement.execute();
			log.debug(crn + ": Olr course procedure completed ------");
			callableStatement.close();

			duration = callableStatement.getString(3);
			startDate = callableStatement.getString(4);
			endDate = callableStatement.getString(5);
			String errMsg = callableStatement.getString(6);
			
			if (errMsg == null || errMsg.equals("")) {
				response.setDuration(duration);
				response.setStartDates(startDate);
				response.setEndDates(endDate);
			} else {
				response.setErrMsg(errMsg);
			}

		} catch (Exception e) {
			log.error("Error In Olr course procedure " + e.getMessage());
			log.error("StackTrace", e);
		}
		return response;
	}
}
