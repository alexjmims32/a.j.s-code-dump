package com.n2n.enroll.registration.bo;

public class JHoldsInfo {
	public String holdsDescription;
	public String holdAmount;
	public String holdReleaseIndicator;
	public String holdReason;
	public String holdOriginOffice;
	public String holdFromDate;
	public String holdToDate;
	public String processesAffected;
	
	
	public String getHoldReason() {
		return holdReason;
	}

	public void setHoldReason(String holdReason) {
		this.holdReason = holdReason;
	}

	public String getProcessesAffected() {
		return processesAffected;
	}

	public void setProcessesAffected(String processesAffected) {
		this.processesAffected = processesAffected;
	}

	public String getHoldFromDate() {
		return holdFromDate;
	}

	public void setHoldFromDate(String holdFromDate) {
		this.holdFromDate = holdFromDate;
	}

	public String getHoldToDate() {
		return holdToDate;
	}

	public void setHoldToDate(String holdToDate) {
		this.holdToDate = holdToDate;
	}

	public String getHoldsDescription() {
		return holdsDescription;
	}

	public void setHoldsDescription(String holdsDescription) {
		this.holdsDescription = holdsDescription;
	}

	public String getHoldAmount() {
		return holdAmount;
	}

	public void setHoldAmount(String holdAmount) {
		this.holdAmount = holdAmount;
	}

	public String getHoldReleaseIndicator() {
		return holdReleaseIndicator;
	}

	public void setHoldReleaseIndicator(String holdReleaseIndicator) {
		this.holdReleaseIndicator = holdReleaseIndicator;
	}

	public String getHoldOriginOffice() {
		return holdOriginOffice;
	}

	public void setHoldOriginOffice(String holdOriginOffice) {
		this.holdOriginOffice = holdOriginOffice;
	}

}
