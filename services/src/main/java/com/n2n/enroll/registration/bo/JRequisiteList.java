package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class JRequisiteList {
	public String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	protected List<JPreRequisites> preRequisites = new ArrayList<JPreRequisites>(
			0);

	public List<JPreRequisites> getPreRequisites() {
		return preRequisites;
	}

	public void setPreRequisites(List<JPreRequisites> preRequisites) {
		this.preRequisites = preRequisites;
	}

}
