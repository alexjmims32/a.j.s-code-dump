package com.n2n.enroll.registration.bo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({"serial"})
public class BillingInfoBo implements Serializable {
	
	@Id
	private String id;
	@Column(name = "BILL_HOURS")
	public String billHours;
	@Column(name = "CREDIT_HOURS")
	public String creditHours;
	@Column(name = "MAX_HOURS")
	public String maxHours;
	@Column(name = "MIN_HOURS")
	public String minHours;
	@Column(name = "TERM_CODE")
	public String termCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBillHours() {
		return billHours;
	}
	public void setBillHours(String billHours) {
		this.billHours = billHours;
	}
	public String getCreditHours() {
		return creditHours;
	}
	public void setCreditHours(String creditHours) {
		this.creditHours = creditHours;
	}
	public String getMaxHours() {
		return maxHours;
	}
	public void setMaxHours(String maxHours) {
		this.maxHours = maxHours;
	}
	public String getMinHours() {
		return minHours;
	}
	public void setMinHours(String minHours) {
		this.minHours = minHours;
	}
	public String getTermCode() {
		return termCode;
	}
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}
	
	
}
