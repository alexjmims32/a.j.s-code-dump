package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class JPeopleList {
	private List<JPeople> facultyList = new ArrayList<JPeople>();

	public void setFacultyList(List<JPeople> facultyList) {
		this.facultyList = facultyList;
	}

	public List<JPeople> getFacultyList() {
		return facultyList;
	}
}
