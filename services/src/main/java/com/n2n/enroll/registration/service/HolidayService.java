package com.n2n.enroll.registration.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

import com.n2n.enroll.registration.bo.HolidayBo;
import com.n2n.enroll.registration.bo.HolidayInfo;
import com.n2n.enroll.registration.bo.HolidayList;
import com.n2n.enroll.registration.bo.HolidayResponse;
import com.n2n.enroll.util.AppUtil;

@SuppressWarnings({ "resource", "unchecked" })
@Service
public class HolidayService {

	@Resource
	SessionFactory sessionFactory;

	protected final Log log = LogFactory.getLog(getClass());

	public HolidayResponse getHolidayInfo(String year) {
		HolidayResponse response = new HolidayResponse();
		HolidayList holidayList = new HolidayList();

		if (year == null || year.equals("")) {
			year = AppUtil.getStringValue(Calendar.getInstance().get(
					Calendar.YEAR));
		}

		String sql = "";

		try {

			sql = AppUtil.getProperty("Q_GET_HOLIDAY_DETAILS");

		
			sql = (AppUtil.format(sql, year));
			List<HolidayBo> hList = (List<HolidayBo>) AppUtil
					.runAQueryCurrentSession(sql, HolidayBo.class);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outFormat = new SimpleDateFormat("MM-dd-yyyy");
			for (HolidayBo holidayBo : hList) {
				HolidayInfo holiday = new HolidayInfo();
				holiday.setYear(AppUtil.getStringValue(holidayBo.getYear()));
				holiday.setDate(outFormat.format(sdf.parse(AppUtil
						.getStringValue(holidayBo.getDate().substring(0, 10)))));
				holiday.setHolidayDesc(AppUtil.getStringValue(holidayBo
						.getHolidayDesc()));

				holidayList.getHolidays().add(holiday);
			}
			response.setHolidayList(holidayList);
			response.setStatus("SUCCESS");

		} catch (Exception e) {
			log.error("Error in getHolidaysInfo" + e.getMessage());
			log.error("StackTrace", e);
			response.setStatus(e.getMessage());
		}

		return response;
	}
}
