package com.n2n.enroll.registration.bo;

public class HolidayResponse {
	
	protected  String status;
	protected  HolidayList holidayList;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public HolidayList getHolidayList() {
		return holidayList;
	}
	public void setHolidayList(HolidayList holidayList) {
		this.holidayList = holidayList;
	}

}
