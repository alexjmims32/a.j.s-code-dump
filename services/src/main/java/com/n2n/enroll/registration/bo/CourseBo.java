package com.n2n.enroll.registration.bo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@SuppressWarnings({ "serial" })
public class CourseBo implements java.io.Serializable {

	@Id
	private String id;

	@Column(name = "COURSE_NUMBER")
	public String courseNumber;
	@Column(name = "COURSE_TITLE")
	public String courseTitle;
	@Column(name = "SUBJECT")
	public String subject;
	@Column(name = "SUBJECT_DESCRIPTION")
	public String description;
	@Column(name = "CREDITS")
	public String credits;
	@Column(name = "COURSE_LEVEL")
	public String level;
	@Column(name = "COLLEGE")
	public String college;
	@Column(name = "LECTURE_HOURS")
	public String lectureHours;
	@Column(name = "SCHEDULE_TYPE")
	public String scheduleType;
	@Column(name = "DEPARTMENT")
	public String department;
	@Column(name = "CRSE_LOCATION")
	public String campus;
	@Column(name = "PREREQUISITE")
	public String prerequisite;
	@Column(name = "CRN")
	public String crn;
	@Column(name = "FACULTY")
	public String faculty;
	@Column(name = "REGISTERED_DATE")
	public String registeredDate;
	@Column(name = "REGISTERED_USER")
	public String registeredBy;
	@Column(name = "TERM_CODE")
	public String termCode;
	@Column(name = "TERM_DESC")
	public String termDescription;
	@Column(name = "STATUS_MESSAGE")
	public String statusMessage;
	@Column(name = "REGISTRATION_STATUS")
	public String statusCode;

	@Column(name = "FLAG")
	public String flag;
	@Column(name = "FLAG_DESC")
	public String flagDescription;

	@Column(name = "VAR_CREDIT")
	public String varCredit;

	@Column(name = "VAR_CREDIT_HRS")
	public String varCreditHrs;
	@Column(name = "FACULTY_EMAIL")
	public String facultyEmail;
	@Column(name = "SECT_SEQ_NUMB")
	public String seqNo;
	@Column(name = "dropFlag")
	public String dropFlag;
	@Column(name = "action_code")
	public String actionCode;
	@Column(name = "action_desc")
	public String actionDesc;

	public String getActionDesc() {
		return actionDesc;
	}

	public void setActionDesc(String actionDesc) {
		this.actionDesc = actionDesc;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	
	public String getLinkident() {
		return linkident;
	}

	public void setLinkident(String linkident) {
		this.linkident = linkident;
	}


	@Column(name = "LINK_IDENT")
	public String linkident;
	

	public String getDropFlag() {
		return dropFlag;
	}

	public void setDropFlag(String dropFlag) {
		this.dropFlag = dropFlag;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSsn(String seqNo) {
		this.seqNo = seqNo;
	}

	public String getFacultyEmail() {
		return facultyEmail;
	}

	public void setFacultyEmail(String facultyEmail) {
		this.facultyEmail = facultyEmail;
	}

	public String getVarCredit() {
		return varCredit;
	}

	public void setVarCredit(String varCredit) {
		this.varCredit = varCredit;
	}

	public String getVarCreditHrs() {
		return varCreditHrs;
	}

	public void setVarCreditHrs(String varCreditHrs) {
		this.varCreditHrs = varCreditHrs;
	}

	public String getFlagDescription() {
		return flagDescription;
	}

	public void setFlagDescription(String flagDescription) {
		this.flagDescription = flagDescription;
	}

	@Column(name = "ERROR_MESSAGE")
	public String errorMessage;

	@Column(name = "WAIT_LIST_PRIORITY")
	public String waitListPriority;
	@Column(name = "AVAILABLE_WAIT_LIST")
	public String availableWaitList;

	public String getWaitListPriority() {
		return waitListPriority;
	}

	public void setWaitListPriority(String waitListPriority) {
		this.waitListPriority = waitListPriority;
	}

	public String getAvailableWaitList() {
		return availableWaitList;
	}

	public void setAvailableWaitList(String availableWaitList) {
		this.availableWaitList = availableWaitList;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(String registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTermCode() {
		return termCode;
	}

	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	public String getTermDescription() {
		return termDescription;
	}

	public void setTermDescription(String termDescription) {
		this.termDescription = termDescription;
	}

	public String getCourseNumber() {
		return courseNumber;
	}

	public void setCourseNumber(String courseNumber) {
		this.courseNumber = courseNumber;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCollege() {
		return college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getLectureHours() {
		return lectureHours;
	}

	public void setLectureHours(String lectureHours) {
		this.lectureHours = lectureHours;
	}

	public String getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCampus() {
		return campus;
	}

	public void setCampus(String campus) {
		this.campus = campus;
	}

	public String getPrerequisite() {
		return prerequisite;
	}

	public void setPrerequisite(String prerequisite) {
		this.prerequisite = prerequisite;
	}

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

}
