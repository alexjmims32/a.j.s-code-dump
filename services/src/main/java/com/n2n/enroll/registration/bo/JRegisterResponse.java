package com.n2n.enroll.registration.bo;

public class JRegisterResponse {

	protected String status;
	protected RegisterList registerList;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public RegisterList getRegisterList() {
		return registerList;
	}

	public void setRegisterList(RegisterList registerList) {
		this.registerList = registerList;
	}

}
