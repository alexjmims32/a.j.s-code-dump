package com.n2n.enroll.registration.bo;

import java.util.ArrayList;
import java.util.List;

public class RegisterList {

	protected List<JRegister> register;

	public List<JRegister> getRegister() {
		if (register == null) {
			register = new ArrayList<JRegister>();
		}
		return this.register;
	}

	public void setRegister(List<JRegister> register) {
		this.register = register;
	}

}
