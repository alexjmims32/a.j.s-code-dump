#Maps

Q_GET_MAPS=SELECT id,campus_code,building_code,building_name,building_desc,phone,email,img_url,longitude,latitude,TITLE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,address,website,principalname, '' as center_flag FROM maps A JOIN campus B ON a.campus_code=b.code
Q_UPDATE_MAPS=update maps set campus_code='%s',building_code='%s',building_name='%s',building_desc='%s',phone='%s',email='%s',img_url='%s',longitude='%s',latitude='%s',VERSION_NO='%s',LASTMODIFIEDBY='%s',LASTMODIFIEDON=sysdate where id='%s' 

Q_ADD_MAPS= insert into maps (ID, campus_code, building_code, building_name, building_desc, phone, email,img_url,longitude,latitude) values (%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')
Q_DELETE_MAP=delete from maps where id='%s'

#Contacts
Q_GET_EMERGENCYCONTACTS= SELECT id,name,phone,address,email,picture_url,campus,CATEGORY,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,comments, seq_num FROM emergency_contacts
Q_UPDATE_EMERGENCYCONTACTS= update emergency_contacts  SET name='%s' , phone='%s' ,address='%s' , email='%s',picture_url='%s',campus='%s',VERSION_NO='%s', LASTMODIFIEDBY='%s',LASTMODIFIEDON=sysdate,category='%s',comments='%s' WHERE id= '%s'
Q_ADD_EMERGENCYCONTACTS=insert into EMERGENCY_CONTACTS (seq_num,ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL,CATEGORY,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,COMMENTS) values (%s,%s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s',sysdate, '%s')
Q_DELETE_EMERGENCY_CONTACT=delete from EMERGENCY_CONTACTS where id='%s'

Q_GET_MODULES = select id,module_code,module_desc from modules
Q_GET_MAIN_MODULES = select * from "MODULE" where showbydefault = 'Y'
Q_GET_ADMIN_MODULES = select * from "MODULE"
Q_GET_PRIVILEGES_DETAILS=SELECT  rownum as id,p.roleId,p.modulecode,p.accessflag,p.authrequired,p.VERSION_NO,p.LASTMODIFIEDBY,p.LASTMODIFIEDON FROM privilege p, ROLE r WHERE p.roleId= r.roleId and r.roleId = %s

#Feeds
Q_FEEDCONFIG=SELECT * FROM FEEDS WHERE PARENTID=%s AND MODULEID=%s

#Notifications
Q_UPDATE_READ_FLAG=UPDATE NOTICELOG SET READFLAG = %s WHERE ID = %s
Q_UPDATE_DELETED_FLAG=UPDATE NOTICELOG SET DELETED = %s WHERE ID = %s 
Q_GET_NOTIFICATIONS=select N.*, P.FIRSTNAME || ' ' || P.LASTNAME as SENT_BY from noticelog N left join admin_user P on N.LASTMODIFIEDBY = P.username where N.username ='%s' and N.expirydate > sysdate and N.deleted != 1
Q_ADD_NOTIFICATION= insert into noticelog (NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE) values (%s, '%s', '%s', to_date('%s','dd-mm-yyyy'), '%s', %s, %s, '%s', '%s', %s, '%s', '%s', to_date('%s','dd-mm-yyyy'))



#Admin Queries

#AUTHENTICATION
Q_GET_PRIVILEGES = SELECT sys_guid() as id,roleid,modulecode,accessflag,authrequired,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON FROM privilege where accessflag = 'Y'
Q_GET_ADMIN_PRIVILEGES_DETAILS=SELECT sys_guid() as id,roleid,modulecode,accessflag,privilege.VERSION_NO,privilege.LASTMODIFIEDBY,privilege.LASTMODIFIEDON,privilege.authrequired,module.description,module.campuscode FROM privilege privilege,module module where privilege.modulecode=module.code  and privilege.roleid='%s' and campuscode='%s'
Q_UPDATE_PRIVILEGES = UPDATE PRIVILEGE SET accessflag='%s' , authrequired='%s',VERSION_NO='%s',LASTMODIFIEDBY='%s',LASTMODIFIEDON=sysdate WHERE modulecode = '%s'
Q_CHANGE_PASSWORD=update admin_user set password='%s' where username='%s'
Q_GET_USERS=select * from admin_user
Q_ADMIN_LOGIN=select sys_guid() as id,a.active,b.username, b.privcode, b.accessflag, b.description from admin_user a, admin_priv b where a.username = b.username and a.username = '%s' and a.password = '%s'
Q_INSERT_USER=INSERT INTO admin_user(FIRSTNAME,LASTNAME,USERNAME,ACTIVE,PASSWORD) VALUES('%s','%s','%s','%s','%s')
Q_UPDATE_USER=update admin_user set FIRSTNAME='%s',LASTNAME='%s',ACTIVE='%s' where username='%s'
Q_GET_ROLES=select code,description from admin_role
Q_INSERT_ROLE=INSERT INTO admin_priv(username,privcode,accessflag,description) VALUES ('%s','%s','%s','%s Module Access')
Q_GET_ROLES=select code,description from admin_role
Q_INSERT_ROLE=INSERT INTO admin_priv(username,privcode,accessflag,description) VALUES ('%s','%s','%s','%s Module Access')
Q_DELETE_ROLE=delete from admin_priv where username='%s'
Q_GET_ROLE_FOR_UPDATE=select sys_guid() as id,USERNAME,PRIVCODE,ACCESSFLAG,DESCRIPTION from admin_priv where username='%s'
Q_ISEXISTS_USER=select * from admin_user WHERE USERNAME='%s'

#FEEDS
Q_ADMIN_FEEDCONFIG=SELECT sys_guid() as pId,f.* FROM feeds f WHERE PARENTID='%s' AND MODULEID='%s'
Q_GET_FEED= SELECT * FROM feeds WHERE id = '%s'
Q_ADD_FEED=insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON,CAMPUSCODE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON) values (%s, %s, '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s', '%s',sysdate)
Q_UPDATE_FEED=update feeds set parentid=%s,type='%s',link='%s',format='%s',moduleid=%s,name='%s',icon='%s',campuscode='%s',VERSION_NO='%s',LASTMODIFIEDBY='%s',LASTMODIFIEDON=sysdate where id =%s
Q_UPDATE_MODULE=update module set authrequired='%s',SHOWBYDEFAULT='%s' where code='%s'
Q_GET_FEEDMOULES = select sys_guid() as pId, f.* from feeds f where parentid = 0 and campuscode='%s'
Q_DELETE_FEEDS=delete from feeds where id='%s'
Q_UPDATE_READ_FLAG=UPDATE NOTICELOG SET READFLAG = %s WHERE ID = %s
Q_UPDATE_DELETED_FLAG=UPDATE NOTICELOG SET DELETED = %s WHERE ID = %s


#NITIFICATIONS
Q_GET_ADMIN_NOTIFICATIONS=select N.*, P.FIRST_NAME || ' ' || P.LST_NAME as SENT_BY from noticelog N left join person_vw P on N.LASTMODIFIEDBY = P.ID where N.expirydate > sysdate and N.deleted != 1
Q_ADD_ADMIN_NOTIFICATION= insert into noticelog (NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, DELETED, TYPE, TITLE, DUEDATE,LASTMODIFIEDON) values (%s, '%s', '%s',to_date('%s','dd-MM-yyyy HH:mm:ss'),'%s', %s, %s, '%s',%s,'%s','%s',to_date('%s','dd-MM-yyyy HH:mm:ss'),sysdate)

#FEEDBACK
Q_INSERT_FEEDBACK=insert into FEEDBACK(title,email,description) values ('%s','%s','%s')
Q_GET_FEEDBACK=SELECT id,title,email,description FROM FEEDBACK
Q_DELETE_FEEDBACK=delete from FEEDBACK where id='%s'
Q_UPDATE_USER_FEEDBACK=update FEEDBACK set title='%s',email='%s',description='%s' where id='%s'
Q_GET_EXISTING_FEED=select * from feeds where name='%s'

#TRACKING
Q_GET_TRACKING_CONTACTS=select ID,CAMPUS,NAME,PHONE,ADDRESS,EMAIL,PICTURE_URL,CATEGORY,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,category,comments,seq_num from emergency_contacts where id='%s'
Q_INSERT_TRACKING_CONTACTS=insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,category,comments) values (%s, '%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', sysdate, '%s', '%s')

Q_GET_TRACKING_MAPS=select ID,CAMPUS_CODE,BUILDING_CODE,BUILDING_NAME,BUILDING_DESC,PHONE,EMAIL,IMG_URL,LONGITUDE,LATITUDE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON,'' as title from maps where id='%s'
Q_INSERT_TRACKING_MAPS=INSERT INTO maps_audit(ID,CAMPUS_CODE,BUILDING_CODE,BUILDING_NAME,BUILDING_DESC,PHONE,EMAIL,IMG_URL,LONGITUDE,LATITUDE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%s,'%s',sysdate)

Q_GET_TRACKING_PRIV=select sys_guid() as Id,ROLEID,MODULECODE,ACCESSFLAG,AUTHREQUIRED,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON from PRIVILEGE where MODULECODE='%s'
Q_INSERT_TRACKING_PRIV=INSERT INTO PRIVILEGE_AUDIT(ROLEID,MODULECODE,ACCESSFLAG,AUTHREQUIRED,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON) VALUES ('%s','%s','%s','%s',%s,'%s',sysdate)

Q_TRACKING_FEEDS=SELECT ID, PARENTID, TYPE, LINK,FORMAT,MODULEID,NAME,ICON,CAMPUSCODE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON FROM feeds WHERE ID='%s'
Q_INSERT_TRACKING_FEEDS=INSERT INTO FEEDS_AUDIT(ID, PARENTID, TYPE, LINK,FORMAT,MODULEID,NAME,ICON,CAMPUSCODE,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON) values('%s','%s','%s','%s','%s','%s','%s','%s','%s',%s,'%s',sysdate)

#HELP
Q_INSERT_HELP_TEXT=insert into help(campuscode,ABOUTTEXT,version_no,lastmodifiedby,lastmodifiedon) values ('%s','%s',%s,'%s',sysdate)
Q_TRACKING_HELP=SELECT campuscode,ABOUTTEXT,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON FROM help  WHERE ID='%s'
Q_INSERT_TRACKING_HELP=INSERT INTO help_AUDIT(campuscode,ABOUTTEXT,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON) values('%s','%s',%s,'%s',sysdate)
Q_GET_HELP_CONTENT=select campuscode,ABOUTTEXT,VERSION_NO,LASTMODIFIEDBY,LASTMODIFIEDON from help where campuscode='%s'
Q_UPDATE_HELP_CONTENT=update help set ABOUTTEXT='%s' where campusCode='%s'

#CUSTOMER SERVICE
Q_GET_SERVICE_AREAS=SELECT * FROM SERVICE_AREA
Q_GET_SERVICE_AREA_EMAILS=SELECT * FROM SERVICE_AREA WHERE ID = %s
Q_INSERT_FEEDBACK_DATA=INSERT INTO FEEDBACK_DATA VALUES(%s, '%s', '%s', '%s', '%s', '%s', '%s', sysdate)

Q_GET_CAMPUS_CODES=select code,description from campus where client='%s'

#FAQ
Q_INSERT_FAQ=insert into faq(id,campuscode,QUESTION,ANSWER,version_no,lastmodifiedby,lastmodifiedon) values ('%s','%s','%s','%s',%s,'%s',sysdate)
Q_UPDATE_FAQ=update faq set QUESTION='%s' ,ANSWER='%s' where id='%s'
Q_GET_FAQ_CONTENT=select id,campuscode,QUESTION,ANSWER,version_no,lastmodifiedby,lastmodifiedon from faq where campuscode='%s'
Q_DELETE_FAQ=delete from faq where id='%s'

Q_GET_NOTIFICATION_BY_CLIENT=SELECT ID,first_name,last_name,middle_name,campus FROM person_campus_vw %s
Q_GET_COUNT_NOTIFICATION_BY_CLIENT=SELECT count(*) FROM person_campus_vw %s

Q_CONTACT_CHANGE_DIRECTION=update emergency_contacts set seq_num=%s where id=%s
Q_UPDATE_SEQUENCE=update emergency_contacts set seq_num=%d where seq_num=%s
Q_DELETE_UPDATE_SEQUENCE=update emergency_contacts set seq_num=seq_num-1 where seq_num>%s

