PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start DO.sql"   
PROMPT ====================================================

spool DO.log

@@Synonyms.sql
@@Types.sql
@@Type\T_ARRAY_TYPE.sql
@@Table\ADMIN_PRIV.sql
@@Table\ADMIN_ROLE.sql
@@Table\ADMIN_USER.sql
@@Table\CAMPUS.sql
@@Table\EMERGENCY_CONTACTS.sql
@@Table\EMERGENCY_CONTACTS_AUDIT.sql
@@Table\FAQ.sql
@@Table\FEEDBACK.sql
@@Table\FEEDBACK_DATA.sql
@@Table\FEEDS.sql
@@Table\FEEDS_AUDIT.sql
@@Table\GENS.sql
@@Table\HELP.sql
@@Table\HELP_AUDIT.sql
@@Table\MAPS.sql
@@Table\MAPS_AUDIT.sql
@@Table\MOBEDU_CART.sql
@@Table\MODULE.sql
@@Table\MODULES.sql
@@Table\NOTICELOG.sql
@@Table\PERSONVW.sql
@@Table\PRIVILEGE.sql
@@Table\PRIVILEGE_AUDIT.sql
@@Table\ROLE.sql
@@Table\SERVICE_AREA.sql
@@Table\ADMIN_PRIV~INX.sql
@@Table\ADMIN_ROLE~INX.sql
@@Table\SERVICE_AREA~INX.sql
@@Table\SERVICE_AREA~UNQ.sql
@@Sequence\FEEDBACK_SEQ.sql
@@Sequence\NOTICELOG_SEQ.sql
@@Sequence\NOTICE_SEQ.sql
@@Procedure\PZ_DROP_COURSE.sql
@@Procedure\PZ_ME_AUTHENTICATION.sql
@@Package\ADD_COURSE_PKG_CORQ.sql
@@Package\CART_PKG.sql
@@Package\ME_ACCOUNT_OBJECTS.sql
@@Package\ME_ALT_PIN_PKG.sql
@@Package\ME_BWCKCOMS.sql
@@Package\ME_BWCKGENS.sql
@@Package\ME_BWCKREGS.sql
@@Package\ME_REG_UTILS.sql
@@Package\ME_VALID.sql
@@Package\ME_WITHDRAWL.sql
@@Package\Z_CM_MOBILE_CAMPUS.sql
@@View\ACCT_SUMM_SPSU.sql
@@View\ACCOUNT_SUMMARY_VIEW.sql
@@View\ACCT_SUMM_VW.sql
@@View\CART_DETAILS_VW.sql
@@View\CHARGE_PAY_DETAIL_SPSU.sql
@@View\CHARGE_PAY_DETAIL_VW.sql
@@View\CONTACT_VW.sql
@@View\TERMS_TO_REGISTER_VW.sql
@@View\COURSE_SEARCH_VW.sql
@@View\CRSE_COURSE_SECTION_DETAIL.sql
@@View\CRSE_MEET_VW.sql
@@View\GENERAL_PERSON_VW.sql
@@View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@View\MOBEDU_HOLIDAY_VW.sql
@@View\PERSON_CAMPUS_VW.sql
@@View\PERSON_VW.sql
@@View\STUDENT_ACCOUNT_DETAIL.sql
@@View\STU_CURCULAM_INFO.sql
@@View\STU_HOLD_INFO.sql
@@View\SUMMARY_BY_TERM_SPSU.sql
@@View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@View\SUMMARY_BY_TERM_VW.sql
@@View\TERMS_TO_BURSAR_VW.sql
@@View\TERM_COURSE_DETAILS_VW.sql
@@View\VW_ALL_TERMS_DUES.sql
@@View\VW_PREVTERMS_CHARGES.sql
@@View\VW_TERM_CHARGES.sql
@@View\VW_TERM_PAYMENTS.sql
@@PackageBody\ADD_COURSE_PKG_CORQ.sql
@@PackageBody\CART_PKG.sql
@@PackageBody\ME_ACCOUNT_OBJECTS.sql
@@PackageBody\ME_ALT_PIN_PKG.sql
@@PackageBody\ME_BWCKCOMS.sql
@@PackageBody\ME_BWCKGENS.sql
@@PackageBody\ME_BWCKREGS.sql
@@PackageBody\ME_REG_UTILS.sql
@@PackageBody\ME_VALID.sql
@@PackageBody\ME_WITHDRAWL.sql
@@PackageBody\Z_CM_MOBILE_CAMPUS.sql

EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
