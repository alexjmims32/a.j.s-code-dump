PROMPT CREATE TABLE mobedu.service_area
CREATE TABLE mobedu.service_area (
  id         INTEGER       NOT NULL,
  area       VARCHAR2(100) NOT NULL,
  subarea    VARCHAR2(100) NULL,
  supervisor VARCHAR2(100) NULL,
  assocdean  VARCHAR2(100) NULL,
  dean       VARCHAR2(100) NULL,
  hr         VARCHAR2(100) NULL,
  provost    VARCHAR2(100) NULL
)
/


