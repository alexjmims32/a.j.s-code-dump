PROMPT CREATE TABLE mobedu.admin_user
CREATE TABLE mobedu.admin_user (
  username  VARCHAR2(20)  NOT NULL,
  firstname VARCHAR2(100) NOT NULL,
  lastname  VARCHAR2(100) NOT NULL,
  password  VARCHAR2(100) NOT NULL,
  active    CHAR(1)       DEFAULT 'Y' NOT NULL
)
/


