PROMPT CREATE INDEX mobedu.admin_priv_idx
CREATE UNIQUE INDEX mobedu.admin_priv_idx
  ON mobedu.admin_priv (
    username,
    privcode
  )
/

