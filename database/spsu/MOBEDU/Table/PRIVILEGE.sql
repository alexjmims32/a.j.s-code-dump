PROMPT CREATE TABLE mobedu.privilege
CREATE TABLE mobedu.privilege (
  roleid         VARCHAR2(100) NOT NULL,
  modulecode     VARCHAR2(100) NOT NULL,
  accessflag     VARCHAR2(1)   NULL,
  authrequired   CHAR(1)       DEFAULT 'Y' NULL,
  version_no     NUMBER(5,0)   NULL,
  lastmodifiedby VARCHAR2(40)  NULL,
  lastmodifiedon TIMESTAMP(6)  NULL
)
/


