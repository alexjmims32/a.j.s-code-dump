PROMPT CREATE TABLE mobedu.faq
CREATE TABLE mobedu.faq (
  id             NUMBER         NOT NULL,
  campuscode     VARCHAR2(100)  NULL,
  question       VARCHAR2(4000) NULL,
  answer         VARCHAR2(4000) NULL,
  version_no     NUMBER(5,0)    NULL,
  lastmodifiedby VARCHAR2(40)   NULL,
  lastmodifiedon TIMESTAMP(6)   NULL
)
/


