PROMPT CREATE TABLE mobedu.personvw
CREATE TABLE mobedu.personvw (
  id          VARCHAR2(9 BYTE)    NOT NULL,
  pidm        NUMBER(8,0)         NULL,
  lst_name    VARCHAR2(60 BYTE)   NULL,
  first_name  VARCHAR2(60 BYTE)   NULL,
  middle_name VARCHAR2(60 BYTE)   NULL,
  ssn         CHAR(6 BYTE)        NULL,
  dob         DATE                NULL,
  gender      VARCHAR2(6 BYTE)    NULL,
  prefix      VARCHAR2(20 BYTE)   NULL,
  category    VARCHAR2(4000 BYTE) NULL,
  published   DATE                NULL,
  updated     DATE                NULL
)
/


