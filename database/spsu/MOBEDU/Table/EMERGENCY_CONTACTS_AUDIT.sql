PROMPT CREATE TABLE mobedu.emergency_contacts_audit
CREATE TABLE mobedu.emergency_contacts_audit (
  id             NUMBER              NOT NULL,
  campus         VARCHAR2(100)       NULL,
  name           VARCHAR2(50)        NULL,
  phone          VARCHAR2(13)        NULL,
  address        VARCHAR2(50)        NULL,
  email          VARCHAR2(30)        NULL,
  picture_url    VARCHAR2(150)       NULL,
  category       VARCHAR2(34)        NULL,
  version_no     NUMBER(5,0)         NULL,
  lastmodifiedby VARCHAR2(40)        NULL,
  lastmodifiedon TIMESTAMP(6)        NULL,
  comments       VARCHAR2(1000 BYTE) NULL
)
/


