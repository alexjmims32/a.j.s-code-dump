PROMPT CREATE TABLE mobedu.maps
CREATE TABLE mobedu.maps (
  id             VARCHAR2(100)  NULL,
  campus_code    VARCHAR2(6)    NULL,
  building_code  VARCHAR2(6)    NULL,
  building_name  VARCHAR2(30)   NULL,
  building_desc  VARCHAR2(30)   NULL,
  phone          VARCHAR2(15)   NULL,
  email          VARCHAR2(30)   NULL,
  img_url        VARCHAR2(30)   NULL,
  longitude      VARCHAR2(30)   NULL,
  latitude       VARCHAR2(30)   NULL,
  version_no     NUMBER(5,0)    NULL,
  lastmodifiedby VARCHAR2(40)   NULL,
  lastmodifiedon TIMESTAMP(6)   NULL,
  address        VARCHAR2(4000) NULL,
  center_flag    VARCHAR2(10) default 'false'
)
/


