PROMPT CREATE TABLE mobedu.feedback_data
CREATE TABLE mobedu.feedback_data (
  areaid       NUMBER(3,0)    NULL,
  courseid     VARCHAR2(10)   NULL,
  comments1    VARCHAR2(4000) NULL,
  comments2    VARCHAR2(4000) NULL,
  confidential CHAR(1)        NULL,
  sender_name  VARCHAR2(100)  NULL,
  sender_email VARCHAR2(100)  NULL,
  created_date TIMESTAMP(6)   NULL
)
/


