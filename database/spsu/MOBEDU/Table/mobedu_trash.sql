create table MOBEDU.MOBEDU_TRASH
(
  student_id   VARCHAR2(10),
  student_pidm NUMBER(8),
  term_code    VARCHAR2(6),
  crn          NUMBER,
  status       VARCHAR2(100),
  error_msg    VARCHAR2(4000),
  rsts_code    VARCHAR2(10)
)
;

create index MOBEDU_TRASH_IDX on MOBEDU_TRASH (STUDENT_ID, TERM_CODE);