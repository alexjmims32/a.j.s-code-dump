PROMPT CREATE TABLE mobedu.module
CREATE TABLE mobedu.module (
  campuscode    VARCHAR2(6 BYTE)   NULL,
  id            NUMBER             NULL,
  code          VARCHAR2(10 BYTE)  NULL,
  description   VARCHAR2(100 BYTE) NULL,
  authrequired  CHAR(1 BYTE)       DEFAULT 'Y' NULL,
  showbydefault CHAR(1 BYTE)       DEFAULT 'N' NULL,
  icon          VARCHAR2(100 BYTE) NULL,
  position      VARCHAR2(10 BYTE)  NULL
)
/


