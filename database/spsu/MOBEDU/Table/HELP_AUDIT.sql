PROMPT CREATE TABLE mobedu.help_audit
CREATE TABLE mobedu.help_audit (
  campuscode     VARCHAR2(100)  NULL,
  abouttext      VARCHAR2(4000) NULL,
  faq            VARCHAR2(4000) NULL,
  version_no     NUMBER(5,0)    NULL,
  lastmodifiedby VARCHAR2(40)   NULL,
  lastmodifiedon TIMESTAMP(6)   NULL
)
/


