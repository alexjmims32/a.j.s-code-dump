PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.cart_pkg
CREATE OR REPLACE package body mobedu.cart_pkg as
  function fz_check_cart(p_student_id in varchar2,
                         p_crn        in varchar2,
                         p_term       in varchar2) return varchar2 as
    v_cart_entry_exist varchar2(1) := 'N';
  begin
    select 'Y'
      into v_cart_entry_exist
      from mobedu.mobedu_cart
     where student_id = p_student_id
       and crn = p_crn
       and term = p_term;
    return v_cart_entry_exist;
  exception
    when others then
      return v_cart_entry_exist;
  end fz_check_cart;

  PROCEDURE pz_add_to_cart(p_student_id in varchar2,
                           p_crn        in varchar2,
                           p_term       in varchar2,
                           p_rsts_code  in varchar2,
                           p_error_msg  out varchar2) is
    cart_row        mobedu_cart%rowtype;
    v_cart_rowcount number := 0;
    cursor c_get_cart_entry(cv_student_id in varchar2,
                            cv_crn        in varchar2,
                            cv_term       in varchar2) is
      select *
        from mobedu.mobedu_cart
       where student_id = cv_student_id
         and crn = cv_crn
         and term = cv_term;

       V_CHECK varchar2(1);
  begin
    open c_get_cart_entry(p_student_id, p_crn, p_term);
    fetch c_get_cart_entry
      into cart_row;
    v_cart_rowcount := c_get_cart_entry%rowcount;
    close c_get_cart_entry;
    dbms_output.put_line('row count::'||v_cart_rowcount);
    if v_cart_rowcount <> 0 then
      p_error_msg := 'The Course you are trying to add is already in the cart.';
      goto quit_from_program;
    end if;
      V_CHECK:=me_valid.fz_check_sfrrsts(p_term,p_crn,'R');
    IF V_CHECK ='N' THEN
      p_error_msg := 'Registration not available at this time';
      goto quit_from_program;
    END if;
    insert into mobedu_cart
      (student_id, crn, term, status, processed_ind, error_flag,rsts_code)
    values
      (p_student_id, p_crn, p_term, '', 'N', 'N',p_rsts_code);
    commit;
    <<quit_from_program>>
    null;
  exception
    when others then
      p_error_msg := 'Failed to add to cart.';
  end PZ_ADD_TO_CART;

  procedure pz_remove_from_cart(p_student_id in varchar2,
                                p_crn        in varchar2,
                                p_term       in varchar2,
                                p_error_msg  out varchar2) is
    v_deleted_rows_no number;
  begin
    delete from mobedu.mobedu_cart
     where student_id = p_student_id
       and crn = p_crn
       and term = p_term;
    v_deleted_rows_no := sql%rowcount;
    commit;
    if v_deleted_rows_no = 0 then
      p_error_msg := 'There is no course in the cart.';
    end if;
  exception
    when others then
      p_error_msg := 'Failed to remove from the cart.';
  end pz_remove_from_cart;

  procedure pz_clear_cart(p_student_id in varchar2,
                          p_term       in varchar2,
                          p_error_msg  out varchar2) is
    mobedu_row mobedu_cart%rowtype;
    v_rowcount number := 0;
    cursor c_get_mobedu_entries(cv_student_id in varchar2,
                                cv_term       in varchar2) is
      select *
        from mobedu.mobedu_cart
       where student_id = cv_student_id
         and term = cv_term;
  begin
    open c_get_mobedu_entries(p_student_id, p_term);
    fetch c_get_mobedu_entries
      into mobedu_row;
    v_rowcount := c_get_mobedu_entries%rowcount;
    close c_get_mobedu_entries;
    if v_rowcount = 0 then
      p_error_msg := 'There are no courses in cart to clear.';
      goto quit_from_program;
    end if;
    delete from mobedu.mobedu_cart
     where student_id = p_student_id
       and term = p_term;
    commit;
    <<quit_from_program>>
    null;
  exception
    when others then
      rollback;
      p_error_msg := 'Failed to clear the cart';
  end pz_clear_cart;

begin
  null;
exception
  when others then
    null;
end cart_pkg;

/

