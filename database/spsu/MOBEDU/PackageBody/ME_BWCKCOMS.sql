PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.me_bwckcoms
CREATE OR REPLACE PACKAGE BODY mobedu.me_bwckcoms AS
  --AUDIT_TRAIL_MSGKEY_UPDATE
  -- PROJECT : MSGKEY
  -- MODULE  : BWCKCOM1
  -- SOURCE  : enUS
  -- TARGET  : I18N
  -- DATE    : Thu Jan 21 05:44:23 2010
  -- MSGSIGN : #713cec5280255ed8
  --TMI18N.ETR DO NOT CHANGE--
  --
  -- FILE NAME..: bwckcom1.sql
  -- RELEASE....: 8.3.0.2
  -- OBJECT NAME: BWCKCOMS
  -- PRODUCT....: SCOMWEB
  -- COPYRIGHT..: Copyright (C) Sungard 2002-2008. All rights reserved.
  --

  curr_release CONSTANT VARCHAR2(10) := '8.3.0.2';
  global_pidm SPRIDEN.SPRIDEN_PIDM%TYPE;
  genpidm     SPRIDEN.SPRIDEN_PIDM%TYPE;
  row_count   NUMBER;
  stvterm_rec stvterm%ROWTYPE;
  sorrtrm_rec sorrtrm%ROWTYPE;
  sfbetrm_rec sfbetrm%ROWTYPE;
  sftregs_rec sftregs%ROWTYPE;
  scbcrse_row scbcrse%ROWTYPE;
  sfrbtch_row sfrbtch%ROWTYPE;
  tbrcbrq_row tbrcbrq%ROWTYPE;
  menu_name   VARCHAR2(60);
  ssbsect_row ssbsect%ROWTYPE;
  stvrsts_row stvrsts%ROWTYPE;
  sobterm_row sobterm%ROWTYPE;
  sql_error   NUMBER;
  term_desc   stvterm%ROWTYPE;

  --------------------------------------------------------------------------

  -- =====================================================================
  -- This function calls the sfkfunc function of the same name.
  -- This checks to see if the access_id we have created is the same as
  -- the one stored in SFRRACL.
  -- If they are different, we know that some other session has taken
  -- 'possession' of sftregs, and we need to prevent the current session
  -- from also trying to use it.
  -- =====================================================================
  FUNCTION f_reg_access_still_good(pidm_in      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                                   term_in      IN STVTERM.STVTERM_CODE%TYPE,
                                   call_path_in IN VARCHAR2,
                                   proc_name_in IN VARCHAR2) RETURN BOOLEAN IS
    reg_sess_id  VARCHAR2(7) := '';
    cookie_value VARCHAR2(2000) := '';
    sessid       VARCHAR2(7);
    gtvsdax_row  gtvsdax%ROWTYPE;
    reg_seqno    NUMBER;

    CURSOR seqno_c(pidm_in SPRIDEN.SPRIDEN_PIDM%TYPE,
                   term_in STVTERM.STVTERM_CODE%TYPE) IS
      SELECT NVL(MAX(sfrstca_seq_number), 0)
        FROM sfrstca
       WHERE sfrstca_term_code = term_in
         AND sfrstca_pidm(+) = pidm_in;
  BEGIN
    IF NOT sfkfunc.f_reg_access_still_good(pidm_in, term_in, call_path_in) THEN
      bwckfrmt.p_open_doc(proc_name_in, term_in);
      twbkwbis.p_dispinfo(proc_name_in, 'SESSIONEXP');
      twbkwbis.p_closedoc(curr_release);
      RETURN FALSE;
    END IF;

    OPEN seqno_c(pidm_in, term_in);
    FETCH seqno_c
      INTO reg_seqno;
    CLOSE seqno_c;
    sfkcurs.p_get_gtvsdax('MAXREGNO', 'WEBREG', gtvsdax_row);

    IF reg_seqno > NVL(gtvsdax_row.gtvsdax_external_code, 9999) THEN
      /*bwckfrmt.p_open_doc(proc_name_in, term_in);
      twbkwbis.p_dispinfo(proc_name_in, 'MAXREGNO');
      twbkwbis.p_closedoc(curr_release);*/
      RETURN FALSE;
    END IF;

    RETURN TRUE;
  END f_reg_access_still_good;

  --------------------------------------------------------------------------
  FUNCTION f_find_associated_term(term_in IN OWA_UTIL.ident_arr,
                                  crn_in  IN VARCHAR2) RETURN VARCHAR2 IS
    CURSOR ssbsectc(term_in VARCHAR2, crn_in VARCHAR2) IS
      SELECT *
        FROM ssbsect
       WHERE ssbsect_term_code = term_in
         AND ssbsect_crn = crn_in;

    ssbsect_rec ssbsect%ROWTYPE;
  BEGIN
    FOR i IN 1 .. term_in.COUNT LOOP
      OPEN ssbsectc(term_in(i), crn_in);
      FETCH ssbsectc
        INTO ssbsect_rec;
      CLOSE ssbsectc;
    END LOOP;

    RETURN ssbsect_rec.ssbsect_term_code;
  END f_find_associated_term;

  --------------------------------------------------------------------------
  FUNCTION f_trim_sel_crn(sel_crn_in IN VARCHAR2) RETURN VARCHAR2 IS
    space_position NUMBER;
  BEGIN
    space_position := INSTR(sel_crn_in, ' ');

    IF space_position = 0 THEN
      RETURN sel_crn_in;
    ELSE
      RETURN SUBSTR(sel_crn_in, 1, space_position - 1);
    END IF;
  END f_trim_sel_crn;

  --------------------------------------------------------------------------
  FUNCTION f_trim_sel_crn_term(sel_crn_in IN VARCHAR2) RETURN VARCHAR2 IS
    space_position NUMBER;
  BEGIN
    space_position := INSTR(sel_crn_in, ' ');

    IF space_position = 0 THEN
      RETURN sel_crn_in;
    ELSE
      RETURN SUBSTR(sel_crn_in, space_position + 1);
    END IF;
  END f_trim_sel_crn_term;

  -- =====================================================================
  -- This procedure displays the add drop page after a course has been
  -- chosen from the lookup classes to add menu option (as opposed to the
  -- add/drop classes menu option).
  -- It is called from P_AddFromSearch.
  -- =====================================================================
  PROCEDURE p_adddrop(term       IN OWA_UTIL.ident_arr,
                      assoc_term IN OWA_UTIL.ident_arr,
                      sel_crn    IN OWA_UTIL.ident_arr) IS
    j                  INTEGER := 0;
    errs_count         NUMBER;
    regs_count         NUMBER;
    wait_count         NUMBER;
    reg_access_allowed BOOLEAN;
    multi_term         BOOLEAN := TRUE;
    drop_problems      sfkcurs.drop_problems_rec_tabtype;
    drop_failures      sfkcurs.drop_problems_rec_tabtype;
    capp_tech_error    VARCHAR2(4) := NULL;

  BEGIN
    /*  IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    IF term.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    --
    -- Display the current registration information.
    -- ======================================================
    bwcksams.p_regsresult(term,
                          errs_count,
                          regs_count,
                          wait_count,
                          NULL,
                          reg_access_allowed,
                          capp_tech_error,
                          NULL,
                          drop_problems,
                          drop_failures);

    IF capp_tech_error IS NOT NULL THEN
      RETURN;
    END IF;

    IF errs_count > 0 OR wait_count > 0 THEN
      twbkfrmt.p_tableclose;
      HTP.br;
    END IF;

    IF NOT multi_term THEN
      --
      -- Display the add table.
      -- ======================================================
      p_add_drop_crn1(1, sel_crn.COUNT);

      BEGIN
        j := 2;

        WHILE sel_crn(j) IS NOT NULL LOOP
          twbkfrmt.p_tabledataopen;
          twbkfrmt.P_FormHidden('rsts_in',
                                SUBSTR(f_stu_getwebregsrsts('R'), 1, 2));
          twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0000', 'SQL', 'CRN'),
                               visible => 'INVISIBLE',
                               idname => 'crn_id' || TO_CHAR(j - 1));
          twbkfrmt.p_formtext('crn_in',
                              '5',
                              '5',
                              cvalue      => f_trim_sel_crn(sel_crn(j)),
                              cattributes => 'ID="crn_id' || TO_CHAR(j - 1) || '"');
          twbkfrmt.P_FormHidden('assoc_term_in', assoc_term(j));
          twbkfrmt.P_FormHidden('start_date_in', NULL);
          twbkfrmt.P_FormHidden('end_date_in', NULL);
          twbkfrmt.p_tabledataclose;
          j := j + 1;
        END LOOP;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          FOR i IN j - 1 .. j + 8 LOOP
            twbkfrmt.p_tabledataopen;
            twbkfrmt.P_FormHidden('rsts_in',
                                  SUBSTR(f_stu_getwebregsrsts('R'), 1, 2));
            twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0001', 'SQL', 'CRN'),
                                 visible => 'INVISIBLE',
                                 idname => 'crn_id' || TO_CHAR(i));
            twbkfrmt.p_formtext('crn_in',
                                '5',
                                '5',
                                cattributes => 'ID="crn_id' || TO_CHAR(i) || '"');
            twbkfrmt.P_FormHidden('assoc_term_in', NULL);
            twbkfrmt.P_FormHidden('start_date_in', NULL);
            twbkfrmt.P_FormHidden('end_date_in', NULL);
            twbkfrmt.p_tabledataclose;
          END LOOP;
      END;

      twbkfrmt.p_tableclose;
    END IF;

    BEGIN
      IF sel_crn(2) IS NULL THEN
        j := 1;
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        j := 1;
    END;

    p_add_drop_crn2(TO_NUMBER(10 + j), regs_count, 0);
    HTP.formclose;
    twbkwbis.p_closedoc(curr_release);
  END p_adddrop;

  --------------------------------------------------------------------------

  -- =====================================================================
  -- This procedure displays the add drop page.
  -- It gets called from P_AddFromSearch1.
  -- =====================================================================
  PROCEDURE p_adddrop1(term_in       IN OWA_UTIL.ident_arr,
                       sel_crn       IN OWA_UTIL.ident_arr,
                       assoc_term_in IN OWA_UTIL.ident_arr,
                       crn_in        IN OWA_UTIL.ident_arr,
                       start_date_in IN OWA_UTIL.ident_arr,
                       end_date_in   IN OWA_UTIL.ident_arr,
                       rsts_in       IN OWA_UTIL.ident_arr,
                       subj          IN OWA_UTIL.ident_arr,
                       crse          IN OWA_UTIL.ident_arr,
                       sec           IN OWA_UTIL.ident_arr,
                       levl          IN OWA_UTIL.ident_arr,
                       cred          IN OWA_UTIL.ident_arr,
                       gmod          IN OWA_UTIL.ident_arr,
                       title         IN bwckcoms.varchar2_tabtype,
                       mesg          IN OWA_UTIL.ident_arr,
                       regs_row      NUMBER,
                       add_row       NUMBER,
                       wait_row      NUMBER) IS
    j                         INTEGER := 2;
    k                         INTEGER := 2;
    l                         NUMBER := 0;
    sdax_rsts_code            VARCHAR2(2);
    tot_credit_hr             SFTREGS.SFTREGS_CREDIT_HR%TYPE := 0;
    tot_bill_hr               SFTREGS.SFTREGS_BILL_HR%TYPE := 0;
    max_hr                    SFBETRM.SFBETRM_MHRS_OVER%TYPE := 0;
    min_hr                    SFBETRM.SFBETRM_MIN_HRS%TYPE := 0;
    tot_ceu                   SFTREGS.SFTREGS_CREDIT_HR%TYPE := 0;
    regs_count                NUMBER := 0;
    errs_count                NUMBER := 0;
    wait_count                NUMBER := 0;
    ssbxlst_count             NUMBER := 0;
    hold_rsts                 VARCHAR2(4);
    gmod_code                 STVGMOD.STVGMOD_CODE%TYPE;
    gmod_desc                 STVGMOD.STVGMOD_DESC%TYPE;
    ssbxlst_row               ssbxlst%ROWTYPE;
    term                      stvterm.stvterm_code%TYPE := NULL;
    multi_term                BOOLEAN := TRUE;
    crse_title                SSRSYLN.SSRSYLN_LONG_COURSE_TITLE%TYPE;
    change_class_options_proc VARCHAR2(100);
    heading_displayed         BOOLEAN := FALSE;
    row_count                 NUMBER := 0;
    crndirect                 VARCHAR2(1) := NVL(SUBSTR(bwcklibs.f_getgtvsdaxrule('CRNDIRECT',
                                                                                  'WEBREG'),
                                                        1,
                                                        1),
                                                 'N');
  BEGIN
    /*    IF NOT twbkwbis.f_validuser(global_pidm) THEN
          RETURN;
        END IF;
    */
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm                   := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                                 'STUPIDM'),
                                             '999999999');
      change_class_options_proc := 'bwlkfrad.P_FacChangeCrseOpt';
    ELSE
      genpidm                   := global_pidm;
      change_class_options_proc := 'bwskfreg.P_ChangeCrseOpt';
    END IF;

    p_add_drop_init(term_in);
    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    --
    -- Set the submit button to call p_regs.
    -- ======================================================
    HTP.formopen(twbkwbis.f_cgibin || 'bwckcoms.P_Regs',
                 cattributes => 'onSubmit="return checkSubmit()"');
    --
    -- Display instructions, pictures, etc.
    -- ======================================================

    twbkwbis.p_dispinfo('bwskfreg.P_AddDropCrse', 'DEFAULT');

    --
    -- Display the current registration information.
    -- ======================================================
    FOR i IN 1 .. term_in.COUNT LOOP
      twbkfrmt.P_FormHidden('term_in', term_in(i));
    END LOOP;

    twbkfrmt.P_FormHidden('RSTS_IN', 'DUMMY');
    twbkfrmt.P_FormHidden('assoc_term_in', 'DUMMY');
    twbkfrmt.P_FormHidden('crn_in', 'DUMMY');
    twbkfrmt.P_FormHidden('start_date_in', 'DUMMY');
    twbkfrmt.P_FormHidden('end_date_in', 'DUMMY');
    twbkfrmt.P_FormHidden('SUBJ', 'DUMMY');
    twbkfrmt.P_FormHidden('CRSE', 'DUMMY');
    twbkfrmt.P_FormHidden('SEC', 'DUMMY');
    twbkfrmt.P_FormHidden('LEVL', 'DUMMY');
    twbkfrmt.P_FormHidden('CRED', 'DUMMY');
    twbkfrmt.P_FormHidden('GMOD', 'DUMMY');
    twbkfrmt.P_FormHidden('MESG', 'DUMMY');
    twbkfrmt.P_FormHidden('TITLE', 'DUMMY');
    twbkfrmt.P_FormHidden('REG_BTN', 'DUMMY');
    bwcklibs.p_initvalue(global_pidm, term, '', '', '', '');
    bwckregs.p_calchrs(tot_credit_hr, tot_bill_hr, tot_ceu);
    regs_count := 0;

    FOR i IN 1 .. term_in.COUNT LOOP
      FOR sftregs_row IN sfkcurs.sftregsrowc(genpidm, term_in(i)) LOOP
        regs_count := regs_count + 1;
        p_curr_sched_heading(heading_displayed, term, multi_term);
        hold_rsts := '';

        BEGIN
          WHILE crn_in(k) IS NOT NULL LOOP
            IF crn_in(k) = sftregs_row.rec_sftregs_crn AND
               rsts_in(k) IS NOT NULL THEN
              hold_rsts := rsts_in(k);
              EXIT;
            END IF;

            k := k + 1;
          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            k := 2;
        END;

        twbkfrmt.p_tablerowopen;

        FOR stvrsts IN stkrsts.stvrstsc(sftregs_row.rec_sftregs_rsts_code) LOOP
          twbkfrmt.p_tabledata(g$_nls.get('BWCKCOM1-0002',
                                          'SQL',
                                          '%01%%02% on %03%',
                                          twbkfrmt.F_FormHidden('MESG',
                                                                'DUMMY'),
                                          stvrsts.stvrsts_desc,
                                          TO_CHAR(sftregs_row.rec_sftregs_rsts_date,
                                                  twbklibs.date_display_fmt)));
        END LOOP;

        sdax_rsts_code := SUBSTR(f_stu_getwebregsrsts('R'), 1, 2);

        IF NVL(sftregs_row.rec_sftregs_error_flag, '#') <> 'F' THEN
          IF sftregs_row.rec_sftregs_grde_date IS NOT NULL OR
             (sftregs_row.rec_ssbsect_voice_avail = 'N' AND crndirect = 'N') THEN
            twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('RSTS_IN', '') || '');
          ELSE
            bwckcoms.p_build_action_pulldown(term_in(i),
                                             genpidm,
                                             sftregs_row.rec_sftregs_crn,
                                             sftregs_row.rec_sftregs_start_date,
                                             sftregs_row.rec_sftregs_completion_date,
                                             trunc(SYSDATE),
                                             sftregs_row.rec_sftregs_dunt_code,
                                             regs_count,
                                             sftregs_row.rec_sftregs_rsts_code,
                                             sftregs_row.rec_ssbsect_ptrm_code,
                                             sdax_rsts_code,
                                             hold_rsts);
          END IF;
        ELSE
          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('RSTS_IN', '') || '');
        END IF;

        IF multi_term THEN
          twbkfrmt.p_tabledata(sftregs_row.rec_stvterm_desc);
        END IF;

        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('assoc_term_in',
                                                   sftregs_row.rec_sftregs_term_code) ||
                             twbkfrmt.F_FormHidden('crn_in',
                                                   sftregs_row.rec_sftregs_crn) ||
                             sftregs_row.rec_sftregs_crn ||
                             twbkfrmt.F_FormHidden('start_date_in',
                                                   TO_CHAR(sftregs_row.rec_sftregs_start_date,
                                                           twbklibs.date_input_fmt)) ||
                             twbkfrmt.F_FormHidden('end_date_in',
                                                   TO_CHAR(sftregs_row.rec_sftregs_completion_date,
                                                           twbklibs.date_input_fmt)));

        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('SUBJ',
                                                   sftregs_row.rec_ssbsect_subj_code) ||
                             sftregs_row.rec_ssbsect_subj_code);
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('CRSE',
                                                   sftregs_row.rec_ssbsect_crse_numb) ||
                             sftregs_row.rec_ssbsect_crse_numb);
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('SEC',
                                                   sftregs_row.rec_ssbsect_seq_numb) ||
                             sftregs_row.rec_ssbsect_seq_numb);

        FOR scrlevl IN scklibs.scrlevlc(sftregs_row.rec_ssbsect_subj_code,
                                        sftregs_row.rec_ssbsect_crse_numb,
                                        sftregs_row.rec_sftregs_term_code) LOOP
          row_count := scklibs.scrlevlc%rowcount;
        END LOOP;

        FOR stvlevl IN stklevl.stvlevlc(sftregs_row.rec_sftregs_levl_code) LOOP
          bwcklibs.p_getsobterm(sftregs_row.rec_sftregs_term_code,
                                sobterm_row);

          IF NVL(sobterm_row.sobterm_levl_web_upd_ind, 'N') = 'Y' AND
             sftregs_row.rec_sftregs_grde_date IS NULL AND row_count > 1 THEN
            twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('LEVL',
                                                       stvlevl.stvlevl_desc) ||
                                 twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(change_class_options_proc ||
                                                                             '?term_in=' ||
                                                                             sftregs_row.rec_sftregs_term_code),
                                                        stvlevl.stvlevl_desc,
                                                        cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKCOM1-0003',
                                                                                                          'SQL',
                                                                                                          'Change'))));
          ELSE
            twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('LEVL',
                                                       stvlevl.stvlevl_desc) ||
                                 stvlevl.stvlevl_desc);
          END IF;
        END LOOP;

        FOR scbcrse IN scklibs.scbcrsec(sftregs_row.rec_ssbsect_subj_code,
                                        sftregs_row.rec_ssbsect_crse_numb,
                                        term_in(i)) LOOP
          scbcrse_row := scbcrse;
        END LOOP;

        IF NVL(sobterm_row.sobterm_cred_web_upd_ind, 'N') = 'Y' AND
           sftregs_row.rec_sftregs_grde_date IS NULL AND
           sftregs_row.rec_scbcrse_credit_hr_ind IS NOT NULL AND
           sftregs_row.rec_ssbsect_credit_hrs IS NULL THEN
          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('CRED',
                                                     TO_CHAR(sftregs_row.rec_sftregs_credit_hr,
                                                             '9990D990')) ||
                               twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(change_class_options_proc ||
                                                                           '?term_in=' ||
                                                                           sftregs_row.rec_sftregs_term_code),
                                                      TO_CHAR(sftregs_row.rec_sftregs_credit_hr,
                                                              '9990D990'),
                                                      cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKCOM1-0004',
                                                                                                        'SQL',
                                                                                                        'Change'))));
        ELSE
          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('CRED',
                                                     TO_CHAR(sftregs_row.rec_sftregs_credit_hr,
                                                             '9990D990')) ||
                               TO_CHAR(sftregs_row.rec_sftregs_credit_hr,
                                       '9990D990'));
        END IF;

        row_count := 0;
        FOR scrgmod IN scklibs.scrgmodc(sftregs_row.rec_ssbsect_subj_code,
                                        sftregs_row.rec_ssbsect_crse_numb,
                                        sftregs_row.rec_sftregs_term_code) LOOP
          row_count := scklibs.scrgmodc%rowcount;
        END LOOP;

        IF NVL(sobterm_row.sobterm_gmod_web_upd_ind, 'N') = 'Y' AND
           sftregs_row.rec_sftregs_grde_date IS NULL AND row_count > 1 AND
           sftregs_row.rec_ssbsect_gmod_code IS NULL THEN
          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('GMOD',
                                                     sftregs_row.rec_stvgmod_desc) ||
                               twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(twbkwbis.f_cgibin ||
                                                                           change_class_options_proc ||
                                                                           '?term_in=' ||
                                                                           sftregs_row.rec_sftregs_term_code),
                                                      sftregs_row.rec_stvgmod_desc,
                                                      cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKCOM1-0005',
                                                                                                        'SQL',
                                                                                                        'Change'))));
        ELSE
          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('GMOD',
                                                     sftregs_row.rec_stvgmod_desc) ||
                               sftregs_row.rec_stvgmod_desc);
        END IF;

        crse_title := bwcklibs.f_course_title(term_in(i),
                                              sftregs_row.rec_sftregs_crn);
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('TITLE', crse_title) ||
                             crse_title);
        twbkfrmt.p_tablerowclose;
      END LOOP;
    END LOOP;

    twbkfrmt.p_tableclose;

    --
    -- Display the hours information.
    -- ===================================================
    IF NOT multi_term THEN
      bwckcoms.p_summary(term,
                         tot_credit_hr,
                         tot_bill_hr,
                         tot_ceu,
                         regs_count);
    END IF;

    --
    -- Display registration errors.
    -- ===================================================
    IF wait_row > 0 THEN
      FOR i IN regs_row + 2 .. regs_row + wait_row + 1 LOOP
        IF i = regs_row + 2 THEN
          p_reg_err_heading(1, multi_term);
        END IF;

        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('MESG', mesg(i)) ||
                             mesg(i));

        FOR ssbsect IN ssklibs.ssbsectc(crn_in(i), assoc_term_in(i)) LOOP
          --
          -- Build the Action pulldown - if waitlistable.
          -- ==================================================
          bwckcoms.p_build_wait_action_pulldown(assoc_term_in(i),
                                                genpidm,
                                                crn_in(i),
                                                i,
                                                ssbsect.ssbsect_ptrm_code,
                                                rsts_in(i),
                                                row_count);

          IF row_count = 0 THEN
            twbkfrmt.P_FormHidden('RSTS_IN', '');
          END IF;
        END LOOP;

        IF multi_term THEN
          twbkfrmt.p_tabledata(bwcklibs.f_term_desc(assoc_term_in(i)));
        END IF;

        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('assoc_term_in',
                                                   assoc_term_in(i)) ||
                             twbkfrmt.F_FormHidden('crn_in', crn_in(i)) ||
                             crn_in(i) ||
                             twbkfrmt.F_FormHidden('start_date_in',
                                                   start_date_in(i)) ||
                             twbkfrmt.F_FormHidden('end_date_in',
                                                   end_date_in(i)));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('SUBJ', subj(i)) ||
                             subj(i));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('CRSE', crse(i)) ||
                             crse(i));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('SEC', sec(i)) ||
                             sec(i));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('LEVL', levl(i)) ||
                             levl(i));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('CRED', cred(i)) ||
                             cred(i));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('GMOD', gmod(i)) ||
                             gmod(i));
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('TITLE', title(i)) ||
                             title(i));
      END LOOP;
    END IF;

    IF wait_row > 0 THEN
      twbkfrmt.p_tableclose;
      HTP.br;
    END IF;

    IF NOT multi_term THEN
      --
      -- Display the add table.
      -- ===================================================
      p_add_drop_crn1(1, sel_crn.COUNT);

      FOR k IN regs_row + wait_row + 2 .. regs_row + wait_row + add_row + 1 LOOP
        BEGIN
          IF crn_in(k) IS NOT NULL THEN
            l := l + 1;
            twbkfrmt.p_tabledataopen;
            twbkfrmt.P_FormHidden('RSTS_IN',
                                  SUBSTR(f_stu_getwebregsrsts('R'), 1, 2));
            twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0006', 'SQL', 'CRN'),
                                 visible => 'INVISIBLE',
                                 idname => 'crn_id' || TO_CHAR(l));
            twbkfrmt.p_formtext('crn_in',
                                '5',
                                '5',
                                cvalue      => crn_in(k),
                                cattributes => 'ID="crn_id' || TO_CHAR(l) || '"');
            twbkfrmt.P_FormHidden('assoc_term_in', assoc_term_in(k));
            twbkfrmt.P_FormHidden('start_date_in', NULL);
            twbkfrmt.P_FormHidden('end_date_in', NULL);
            twbkfrmt.p_tabledataclose;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      END LOOP;

      --
      -- ?????
      -- ===================================================
      BEGIN
        WHILE sel_crn(j) IS NOT NULL LOOP
          l := l + 1;
          twbkfrmt.p_tabledataopen;
          twbkfrmt.P_FormHidden('RSTS_IN',
                                SUBSTR(f_stu_getwebregsrsts('R'), 1, 2));
          twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0007', 'SQL', 'CRN'),
                               visible => 'INVISIBLE',
                               idname => 'crn_id' || TO_CHAR(l));
          twbkfrmt.p_formtext('crn_in',
                              '5',
                              '5',
                              cvalue      => f_trim_sel_crn(sel_crn(j)),
                              cattributes => 'ID="crn_id' || TO_CHAR(l) || '"');
          twbkfrmt.P_FormHidden('assoc_term_in', assoc_term_in(j));
          twbkfrmt.P_FormHidden('start_date_in', NULL);
          twbkfrmt.P_FormHidden('end_date_in', NULL);
          twbkfrmt.p_tabledataclose;
          j := j + 1;
        END LOOP;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l := l + 1;

          FOR i IN l .. l + 9 LOOP
            twbkfrmt.p_tabledataopen;
            twbkfrmt.P_FormHidden('RSTS_IN',
                                  SUBSTR(f_stu_getwebregsrsts('R'), 1, 2));
            twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0008', 'SQL', 'CRN'),
                                 visible => 'INVISIBLE',
                                 idname => 'crn_id' || TO_CHAR(i));
            twbkfrmt.p_formtext('crn_in',
                                '5',
                                '5',
                                cattributes => 'ID="crn_id' || TO_CHAR(i) || '"');
            twbkfrmt.P_FormHidden('assoc_term_in', assoc_term_in(l));
            twbkfrmt.P_FormHidden('start_date_in', NULL);
            twbkfrmt.P_FormHidden('end_date_in', NULL);
            twbkfrmt.p_tabledataclose;
          END LOOP;
      END;

      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tableclose;
    ELSE
      l := l + 1;
    END IF;

    p_add_drop_crn2(TO_NUMBER(9 + l), regs_count, wait_count);
    HTP.formclose;
    twbkwbis.p_closedoc(curr_release);
  END p_adddrop1;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- This procedure displays the add/drop page.
  -- 1. The p_adddropcrse procedure executes when "Add/Drop Classes" is selected
  --    from the registration menu. It displays the add/drop page.
  -- 2. The P_Regs procedure executes when the submit button is pressed on the
  --    add/drop page. It processes the registration changes.
  -- 3. This procedure executes after registration changes are processed by
  --    the P_Regs procedure. It redisplays the add/drop page.
  -- =====================================================================
  PROCEDURE p_adddrop2(term_in                IN OWA_UTIL.ident_arr,
                       err_term               IN OWA_UTIL.ident_arr,
                       err_crn                IN OWA_UTIL.ident_arr,
                       err_subj               IN OWA_UTIL.ident_arr,
                       err_crse               IN OWA_UTIL.ident_arr,
                       err_sec                IN OWA_UTIL.ident_arr,
                       err_code               IN OWA_UTIL.ident_arr,
                       err_levl               IN OWA_UTIL.ident_arr,
                       err_cred               IN OWA_UTIL.ident_arr,
                       err_gmod               IN OWA_UTIL.ident_arr,
                       capp_tech_error_in_out IN OUT VARCHAR2,
                       drop_result_label_in   IN twgrinfo.twgrinfo_label%TYPE DEFAULT NULL,
                       drop_problems_in       IN sfkcurs.drop_problems_rec_tabtype,
                       drop_failures_in       IN sfkcurs.drop_problems_rec_tabtype) IS
    i                  INTEGER := 2;
    errs_count         NUMBER;
    regs_count         NUMBER;
    wait_count         NUMBER;
    err_levl_desc      STVLEVL.STVLEVL_DESC%TYPE;
    err_crhrs          SSBSECT.SSBSECT_CREDIT_HRS%TYPE;
    err_gmod_desc      STVGMOD.STVGMOD_DESC%TYPE;
    reg_access_allowed BOOLEAN;
    term               stvterm.stvterm_code%TYPE := NULL;
    multi_term         BOOLEAN := TRUE;
  BEGIN
    /*   IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');
    ELSE
      genpidm := global_pidm;
    END IF;

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    --
    -- Display the current registration information.
    -- ===================================================
    bwcksams.p_regsresult(term_in,
                          errs_count,
                          regs_count,
                          wait_count,
                          NULL,
                          reg_access_allowed,
                          capp_tech_error_in_out,
                          drop_result_label_in,
                          drop_problems_in,
                          drop_failures_in);

    --
    -- only redisplay add/drop page if no capp technical problems.
    -- ===========================================================
    IF capp_tech_error_in_out IS NOT NULL THEN
      RETURN;
    END IF;

    --
    -- Display registration errors.
    -- ===================================================
    DECLARE
      --
      -- Cursor to get level description.
      -- =======================================================
      CURSOR level_desc_c(levl_in VARCHAR2) IS
        SELECT NVL(stvlevl_desc, ' ')
          FROM stvlevl
         WHERE stvlevl_code = levl_in;

      --
      -- Cursor to get grade mode description.
      -- =======================================================
      CURSOR gmod_desc_c(gmod_in VARCHAR2) IS
        SELECT NVL(stvgmod_desc, ' ')
          FROM stvgmod
         WHERE stvgmod_code = gmod_in;
    BEGIN
      WHILE err_crn(i) IS NOT NULL LOOP
        IF errs_count = 0 AND i = 2 AND wait_count = 0 THEN
          p_reg_err_heading(NULL, multi_term);
        END IF;

        --
        -- Start a row of error data in the error display table.
        -- ======================================================
        twbkfrmt.p_tablerowopen;

        BEGIN
          IF err_code(i) IS NOT NULL THEN
            twbkfrmt.p_tabledata(bwcklibs.error_msg_table(err_code(i)));
          ELSE
            twbkfrmt.p_tabledata(sb_registration_msg.f_get_message(p_cde   => 'SYST',
                                                                   p_seqno => 1));
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            twbkfrmt.p_tabledata(sb_registration_msg.f_get_message(p_cde   => 'SYST',
                                                                   p_seqno => 1));
        END;

        IF wait_count > 0 THEN
          twbkfrmt.p_tabledata;
        END IF;

        IF multi_term THEN
          twbkfrmt.p_tabledata(bwcklibs.f_term_desc(err_term(i)));
        END IF;

        twbkfrmt.p_tabledata(err_crn(i));

        BEGIN
          IF err_subj(i) IS NOT NULL THEN
            twbkfrmt.p_tabledata(err_subj(i));
            twbkfrmt.p_tabledata(err_crse(i));
            twbkfrmt.p_tabledata(err_sec(i));
            OPEN level_desc_c(err_levl(i));
            FETCH level_desc_c
              INTO err_levl_desc;
            twbkfrmt.p_tabledata(err_levl_desc);
            CLOSE level_desc_c;
            twbkfrmt.p_tabledata(err_cred(i));
            OPEN gmod_desc_c(err_gmod(i));
            FETCH gmod_desc_c
              INTO err_gmod_desc;
            twbkfrmt.p_tabledata(err_gmod_desc);
            CLOSE gmod_desc_c;
            twbkfrmt.p_tabledata(bwcklibs.f_course_title(err_term(i),
                                                         err_crn(i)));
          ELSE
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
            twbkfrmt.p_tabledata;
        END;

        twbkfrmt.p_tablerowclose;
        i := i + 1;
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        IF errs_count > 0 OR wait_count > 0 OR i > 2 THEN
          twbkfrmt.p_tableclose;
          HTP.br;
        END IF;
    END;
    IF NOT multi_term THEN
      p_add_drop_crn1;
    END IF;

    p_add_drop_crn2(10, regs_count, wait_count);

    HTP.formclose;
    twbkwbis.p_closedoc(curr_release);
  END p_adddrop2;

  --------------------------------------------------------------------
  --
  -- =====================================================================
  -- This procedure is called from 'lookup classes to add' search results
  -- =====================================================================
  PROCEDURE p_addfromsearch(term_in       IN OWA_UTIL.ident_arr,
                            assoc_term_in IN OWA_UTIL.ident_arr,
                            sel_crn       IN OWA_UTIL.ident_arr,
                            add_btn       IN OWA_UTIL.ident_arr) IS
    i               INTEGER := 2;
    j               INTEGER := 0;
    rsts            OWA_UTIL.ident_arr;
    subj            OWA_UTIL.ident_arr;
    crse            OWA_UTIL.ident_arr;
    sec             OWA_UTIL.ident_arr;
    levl            OWA_UTIL.ident_arr;
    cred            OWA_UTIL.ident_arr;
    gmod            OWA_UTIL.ident_arr;
    title           bwckcoms.varchar2_tabtype;
    mesg            OWA_UTIL.ident_arr;
    btn             OWA_UTIL.ident_arr;
    start_date_out  OWA_UTIL.ident_arr;
    end_date_out    OWA_UTIL.ident_arr;
    proc_called_by  VARCHAR2(50);
    call_path       VARCHAR2(1);
    term            stvterm.stvterm_code%TYPE := NULL;
    multi_term      BOOLEAN := TRUE;
    assoc_term_out  OWA_UTIL.ident_arr;
    crn_out         OWA_UTIL.ident_arr;
    msg             VARCHAR2(200);
    wmnu_rec        twgbwmnu%ROWTYPE;
    capp_tech_error VARCHAR2(4);
    lv_add_btn1     VARCHAR2(20);
    lv_add_btn2     VARCHAR2(20);

  BEGIN
    /*  IF NOT twbkwbis.f_validuser(global_pidm) THEN
          RETURN;
        END IF;
    */
    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm        := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                      'STUPIDM'),
                                  '999999999');
      call_path      := 'F';
      proc_called_by := 'bwlkfrad.P_FacAddDropCrse';
    ELSE
      genpidm        := global_pidm;
      call_path      := 'S';
      proc_called_by := 'bwskfreg.P_AddDropCrse';
    END IF;
    lv_add_btn1 := g$_nls.get('BWCKCOM1-0009', 'SQL', 'Submit Changes');
    lv_add_btn2 := g$_nls.get('BWCKCOM1-0010', 'SQL', 'Register');

    IF LTRIM(RTRIM(add_btn(2))) = lv_add_btn1 OR
       LTRIM(RTRIM(add_btn(2))) = lv_add_btn2 THEN
      --
      -- Check to see if anyone else 'owns' sftregs for this
      -- student. If not, remove any remnant sftregs records,
      -- and copy all sfrstcr rows into sftregs.
      -- ==================================================

      FOR i IN 1 .. term_in.COUNT LOOP
        IF NOT bwckregs.f_registration_access(genpidm,
                                              term_in(i),
                                              call_path || global_pidm) THEN
          capp_tech_error := sfkfunc.f_get_capp_tech_error(genpidm,
                                                           term_in(i));
          IF capp_tech_error IS NOT NULL THEN
            OPEN twbklibs.getmenuc(proc_called_by);
            FETCH twbklibs.getmenuc
              INTO wmnu_rec;
            CLOSE twbklibs.getmenuc;
            IF capp_tech_error = 'PIPE' THEN
              msg := 'PIPE_ERROR';
            ELSE
              msg := 'CAPP_ERROR';
            END IF;

            --
            -- Return to the menu.
            -- =================================================
            twbkwbis.p_genmenu(SUBSTR(wmnu_rec.twgbwmnu_back_url,
                                      INSTR(wmnu_rec.twgbwmnu_back_url,
                                            '/',
                                            -1) + 1),
                               msg,
                               message_type => 'ERROR');
            RETURN;
          END IF;

          bwckfrmt.p_open_doc(proc_called_by, term_in(i));
          twbkwbis.P_DispInfo(proc_called_by, 'SESSIONBLOCKED');
          twbkwbis.P_CloseDoc(curr_release);
          RETURN;
        END IF;
      END LOOP;

      rsts(1) := 'dummy';
      subj(1) := 'dummy';
      crse(1) := 'dummy';
      sec(1) := 'dummy';
      levl(1) := 'dummy';
      cred(1) := 'dummy';
      gmod(1) := 'dummy';
      mesg(1) := 'dummy';
      title(1) := 'dummy';
      assoc_term_out(1) := 'dummy';
      crn_out(1) := 'dummy';
      start_date_out(1) := 'dummy';
      end_date_out(1) := 'dummy';
      btn(1) := 'dummy';
      btn(2) := g$_nls.get('BWCKCOM1-0011', 'SQL', 'Submit Changes');

      FOR i IN 2 .. sel_crn.COUNT LOOP
        assoc_term_out(i) := f_trim_sel_crn_term(sel_crn(i));
        crn_out(i) := f_trim_sel_crn(sel_crn(i));
        rsts(i) := SUBSTR(f_stu_getwebregsrsts('R'), 1, 2);
        start_date_out(i) := NULL;
        end_date_out(i) := NULL;
        j := j + 1;
      END LOOP;

      bwckcoms.p_regs(term_in,
                      rsts,
                      assoc_term_out,
                      crn_out,
                      start_date_out,
                      end_date_out,
                      subj,
                      crse,
                      sec,
                      levl,
                      cred,
                      gmod,
                      title,
                      mesg,
                      btn,
                      0,
                      j,
                      0);
    ELSIF LTRIM(RTRIM(add_btn(2))) IN
          (g$_nls.get('BWCKCOM1-0012', 'SQL', 'Add to WorkSheet'),
           g$_nls.get('BWCKCOM1-0013', 'SQL', 'Back to WorkSheet')) THEN
      --
      -- Check to see if anyone else 'owns' sftregs for this
      -- student. If not, remove any remnant sftregs records,
      -- and copy all sfrstcr rows into sftregs.
      -- ==================================================

      FOR i IN 1 .. term_in.COUNT LOOP
        IF NOT bwckregs.f_registration_access(genpidm,
                                              term_in(i),
                                              call_path || global_pidm) THEN
          capp_tech_error := sfkfunc.f_get_capp_tech_error(genpidm,
                                                           term_in(i));
          IF capp_tech_error IS NOT NULL THEN
            OPEN twbklibs.getmenuc(proc_called_by);
            FETCH twbklibs.getmenuc
              INTO wmnu_rec;
            CLOSE twbklibs.getmenuc;
            IF capp_tech_error = 'PIPE' THEN
              msg := 'PIPE_ERROR';
            ELSE
              msg := 'CAPP_ERROR';
            END IF;

            --
            -- Return to the menu.
            -- =================================================
            twbkwbis.p_genmenu(SUBSTR(wmnu_rec.twgbwmnu_back_url,
                                      INSTR(wmnu_rec.twgbwmnu_back_url,
                                            '/',
                                            -1) + 1),
                               msg,
                               message_type => 'ERROR');
            RETURN;
          END IF;

          bwckfrmt.p_open_doc(proc_called_by, term_in(i));
          twbkwbis.P_DispInfo(proc_called_by, 'SESSIONBLOCKED');
          twbkwbis.P_CloseDoc(curr_release);
          RETURN;
        END IF;
      END LOOP;

      --
      bwckcoms.p_adddrop(term_in, assoc_term_in, sel_crn);
      --
      lv_add_btn1 := g$_nls.get('BWCKCOM1-0014', 'SQL', 'Class Search');
    ELSIF LTRIM(RTRIM(add_btn(2))) = lv_add_btn1 THEN
      /*   IF NOT twbkwbis.f_validuser(global_pidm) THEN
        RETURN;
      END IF;*/

      IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
        /* FACWEB */
        bwlkffcs.P_FacCrseSearch(term_in);
      ELSE
        /* STUWEB */
        bwskfcls.P_CrseSearch(term_in);
      END IF;
    END IF;
  END p_addfromsearch;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- This procedure is called upon submit from the class search results
  -- page - which is called from add/drop.
  -- =====================================================================
  PROCEDURE p_addfromsearch1(term_in       IN OWA_UTIL.ident_arr,
                             assoc_term_in IN OWA_UTIL.ident_arr,
                             sel_crn       IN OWA_UTIL.ident_arr,
                             rsts          IN OWA_UTIL.ident_arr,
                             crn           IN OWA_UTIL.ident_arr,
                             start_date_in IN OWA_UTIL.ident_arr,
                             end_date_in   IN OWA_UTIL.ident_arr,
                             subj          IN OWA_UTIL.ident_arr,
                             crse          IN OWA_UTIL.ident_arr,
                             sec           IN OWA_UTIL.ident_arr,
                             levl          IN OWA_UTIL.ident_arr,
                             cred          IN OWA_UTIL.ident_arr,
                             gmod          IN OWA_UTIL.ident_arr,
                             title         IN bwckcoms.varchar2_tabtype,
                             mesg          IN OWA_UTIL.ident_arr,
                             regs_row      NUMBER,
                             add_row       NUMBER,
                             wait_row      NUMBER,
                             add_btn       IN OWA_UTIL.ident_arr) IS
    i              INTEGER := 2;
    j              INTEGER := 2;
    add_ctr        NUMBER;
    assoc_term_out OWA_UTIL.ident_arr;
    crn_out        OWA_UTIL.ident_arr;
    rsts_out       OWA_UTIL.ident_arr;
    start_date_out OWA_UTIL.ident_arr;
    end_date_out   OWA_UTIL.ident_arr;
    btn            OWA_UTIL.ident_arr;
    term           stvterm.stvterm_code%TYPE := NULL;
    multi_term     BOOLEAN := TRUE;
    lv_add_btn1    VARCHAR2(20);
    lv_add_btn2    VARCHAR2(20);
    lv_add_btn3    VARCHAR2(20);
  BEGIN
    lv_add_btn1 := g$_nls.get('BWCKCOM1-0015', 'SQL', 'Submit Changes');
    lv_add_btn2 := g$_nls.get('BWCKCOM1-0016', 'SQL', 'Register');
    lv_add_btn3 := g$_nls.get('BWCKCOM1-0017', 'SQL', 'Class Search');
    /*  IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');

      FOR i IN 1 .. term_in.COUNT LOOP
        IF NOT
            bwckcoms.f_reg_access_still_good(genpidm,
                                             term_in(i),
                                             'F' || global_pidm,
                                             'bwlkfrad.P_FacAddDropCrse') THEN
          RETURN;
        END IF;
      END LOOP;
    ELSE
      genpidm := global_pidm;

      FOR i IN 1 .. term_in.COUNT LOOP
        IF NOT bwckcoms.f_reg_access_still_good(genpidm,
                                                term_in(i),
                                                'S' || global_pidm,
                                                'bwskfreg.P_AddDropCrse') THEN
          RETURN;
        END IF;
      END LOOP;
    END IF;

    IF LTRIM(RTRIM(add_btn(2))) = lv_add_btn1 OR
       LTRIM(RTRIM(add_btn(2))) = lv_add_btn2

     THEN
      add_ctr := add_row;
      assoc_term_out(1) := 'dummy';
      crn_out(1) := 'dummy';
      rsts_out(1) := 'dummy';
      start_date_out(1) := 'dummy';
      end_date_out(1) := 'dummy';
      btn(1) := 'dummy';
      btn(2) := g$_nls.get('BWCKCOM1-0018', 'SQL', 'Submit Changes');

      BEGIN
        WHILE crn(j) IS NOT NULL LOOP
          assoc_term_out(j) := assoc_term_in(j);
          crn_out(j) := crn(j);
          rsts_out(j) := rsts(j);
          start_date_out(j) := start_date_in(j);
          end_date_out(j) := end_date_in(j);
          j := j + 1;
        END LOOP;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;

      BEGIN
        WHILE sel_crn(i) IS NOT NULL LOOP
          assoc_term_out(j) := f_trim_sel_crn_term(sel_crn(i));
          crn_out(j) := f_trim_sel_crn(sel_crn(i));
          start_date_out(j) := NULL;
          end_date_out(j) := NULL;
          rsts_out(j) := SUBSTR(f_stu_getwebregsrsts('R'), 1, 2);
          i := i + 1;
          j := j + 1;
          add_ctr := add_ctr + 1;
        END LOOP;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          i := i - 1;
      END;

      bwckcoms.p_regs(term_in,
                      rsts_out,
                      assoc_term_out,
                      crn_out,
                      start_date_out,
                      end_date_out,
                      subj,
                      crse,
                      sec,
                      levl,
                      cred,
                      gmod,
                      title,
                      mesg,
                      btn,
                      regs_row,
                      add_ctr,
                      wait_row);
    ELSIF LTRIM(RTRIM(add_btn(2))) IN
          (g$_nls.get('BWCKCOM1-0019', 'SQL', 'Add to WorkSheet'),
           g$_nls.get('BWCKCOM1-0020', 'SQL', 'Back to WorkSheet')) THEN
      bwckcoms.p_adddrop1(term_in,
                          sel_crn,
                          assoc_term_in,
                          crn,
                          start_date_in,
                          end_date_in,
                          rsts,
                          subj,
                          crse,
                          sec,
                          levl,
                          cred,
                          gmod,
                          title,
                          mesg,
                          regs_row,
                          add_row,
                          wait_row);
    ELSIF LTRIM(RTRIM(add_btn(2))) = lv_add_btn3 THEN
      bwckgens.P_RegsCrseSearch(term_in,
                                rsts,
                                assoc_term_in,
                                crn,
                                start_date_in,
                                end_date_in,
                                subj,
                                crse,
                                sec,
                                levl,
                                cred,
                                gmod,
                                title,
                                mesg,
                                regs_row,
                                add_row,
                                wait_row);
    END IF;
  END p_addfromsearch1;

  --------------------------------------------------------------------------
  --
  -- Get the latest student and registration records.
  -- =====================================================================
  PROCEDURE p_regs_etrm_chk(pidm_in           NUMBER,
                            term_in           stvterm.stvterm_code%TYPE,
                            clas_code         IN OUT SGRCLSR.SGRCLSR_CLAS_CODE%TYPE,
                            multi_term_in     BOOLEAN DEFAULT FALSE,
                            create_sfbetrm_in BOOLEAN DEFAULT TRUE) IS
  BEGIN
    --
    -- Get the latest student record. Do some validation and
    -- retrieve more info based on the student record.
    -- ===================================================
    dbms_output.put_line('pidm and term' || pidm_in || term_in);
    FOR sgbstdn IN sgklibs.sgbstdnc(pidm_in, term_in) LOOP
      sgbstdn_rec := sgbstdn;
    END LOOP;
    --
    -- Get student's curriculum records
    --
    lcur_tab := sokccur.f_current_active_curriculum(p_pidm      => pidm_in,
                                                    p_lmod_code => sb_curriculum_str.f_learner,
                                                    p_eff_term  => term_in);

    me_bwckregs.p_regschk(sgbstdn_rec, multi_term_in);
    --
    -- Get the class code from another package (bwckregs).
    -- ===================================================
    clas_code := me_bwckregs.f_getstuclas;
    --
    -- Get the latest registration record. Do some validation
    -- and retrieve more info based on the registration record.
    -- ===================================================
    row_count := 0;

    FOR sfbetrm IN sfkcurs.sfbetrmc(pidm_in, term_in) LOOP
      sfbetrm_rec := sfbetrm;
      row_count   := sfkcurs.sfbetrmc%rowcount;
    END LOOP;

    IF row_count = 0 AND create_sfbetrm_in THEN
      /* in a multi term scenario, when no sfbetrm_rec is found, */
      /* the record will be for a different term to the one we */
      /* expect, so reset it */
      sfbetrm_rec.sfbetrm_term_code := term_in;
      me_bwckregs.p_regschk(sfbetrm_rec, 'Y', multi_term_in);
    ELSE
      me_bwckregs.p_regschk(sfbetrm_rec, NULL, multi_term_in);
    END IF;
  END p_regs_etrm_chk;

  --------------------------------------------------------------------------

  --
  -- P_REGS
  -- This procedure processes registration changes (add/drop/change status).
  -- =====================================================================
  PROCEDURE p_regs(term_in       IN OWA_UTIL.ident_arr,
                   rsts_in       IN OWA_UTIL.ident_arr,
                   assoc_term_in IN OWA_UTIL.ident_arr,
                   crn_in        IN OWA_UTIL.ident_arr,
                   start_date_in IN OWA_UTIL.ident_arr,
                   end_date_in   IN OWA_UTIL.ident_arr,
                   subj          IN OWA_UTIL.ident_arr,
                   crse          IN OWA_UTIL.ident_arr,
                   sec           IN OWA_UTIL.ident_arr,
                   levl          IN OWA_UTIL.ident_arr,
                   cred          IN OWA_UTIL.ident_arr,
                   gmod          IN OWA_UTIL.ident_arr,
                   title         IN bwckcoms.varchar2_tabtype,
                   mesg          IN OWA_UTIL.ident_arr,
                   reg_btn       IN OWA_UTIL.ident_arr,
                   regs_row      NUMBER,
                   add_row       NUMBER,
                   wait_row      NUMBER) IS
    i                     INTEGER := 2;
    k                     INTEGER := 1;
    err_term              OWA_UTIL.ident_arr;
    err_crn               OWA_UTIL.ident_arr;
    err_subj              OWA_UTIL.ident_arr;
    err_crse              OWA_UTIL.ident_arr;
    err_sec               OWA_UTIL.ident_arr;
    err_code              OWA_UTIL.ident_arr;
    err_levl              OWA_UTIL.ident_arr;
    err_cred              OWA_UTIL.ident_arr;
    err_gmod              OWA_UTIL.ident_arr;
    clas_code             SGRCLSR.SGRCLSR_CLAS_CODE%TYPE;
    stvterm_rec           stvterm%ROWTYPE;
    sorrtrm_rec           sorrtrm%ROWTYPE;
    stufac_ind            VARCHAR2(1);
    term                  stvterm.stvterm_code%TYPE := NULL;
    multi_term            BOOLEAN := TRUE;
    olr_course_selected   BOOLEAN := FALSE;
    capp_tech_error       VARCHAR2(4);
    assoc_term            OWA_UTIL.ident_arr := assoc_term_in;
    crn                   OWA_UTIL.ident_arr;
    crn_index             NUMBER;
    crn_in_index          NUMBER;
    start_date            OWA_UTIL.ident_arr := start_date_in;
    end_date              OWA_UTIL.ident_arr := end_date_in;
    etrm_done             BOOLEAN := FALSE;
    drop_problems         sfkcurs.drop_problems_rec_tabtype;
    drop_failures         sfkcurs.drop_problems_rec_tabtype;
    local_capp_tech_error VARCHAR2(30);
    called_by_proc_name   VARCHAR2(100);
    web_rsts_checkc_flag  VARCHAR2(1);
    lv_reg_btn1           VARCHAR2(20);
    V_ERROR               VARCHAR2(1000);
    CURSOR web_rsts_checkc(rsts_in VARCHAR2) IS
      SELECT 'Y'
        FROM stvrsts
       WHERE stvrsts_code = rsts_in
         AND stvrsts_web_ind = 'Y';

  BEGIN
    lv_reg_btn1 := g$_nls.get('BWCKCOM1-0021', 'SQL', 'Class Search');

    /*   IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    --
    -- Remove any null leading crns from the to-add list.
    -- ==================================================
    crn_index := 1;
    dbms_output.put_line('BEFORE CRN COUNT');
    FOR crn_in_index IN 1 .. crn_in.COUNT LOOP
      IF crn_in(crn_in_index) IS NOT NULL THEN
        crn(crn_index) := crn_in(crn_in_index);
        crn_index := crn_index + 1;
      END IF;
    END LOOP;
    --

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm             := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                           'STUPIDM'),
                                       '999999999');
      stufac_ind          := 'F';
      called_by_proc_name := 'bwlkfrad.P_FacAddDropCrse';
      dbms_output.put_line('BEFORE REGS ACCESS');
      FOR i IN 1 .. term_in.COUNT LOOP
        IF NOT bwckcoms.f_reg_access_still_good(genpidm,
                                                term_in(i),
                                                stufac_ind || global_pidm,
                                                called_by_proc_name) THEN
          RETURN;
        END IF;
      END LOOP;
    ELSE
      genpidm             := global_pidm;
      stufac_ind          := 'S';
      called_by_proc_name := 'bwskfreg.P_AddDropCrse';
      dbms_output.put_line('BEFORE REGS ACCESS');
      FOR i IN 1 .. term_in.COUNT LOOP
        dbms_output.put_line('BEFORE REGS ACCESS');
        /* IF NOT ME_bwckcoms.f_reg_access_still_good(genpidm,
                                                term_in(i),
                                                stufac_ind || global_pidm,
                                                called_by_proc_name) THEN
          RETURN;
        END IF;*/
      END LOOP;
    END IF;
    dbms_output.put_line('BEFORE RSTS CHECK');
    FOR i IN 2 .. rsts_in.COUNT LOOP
      IF rsts_in(i) IS NOT NULL THEN
        OPEN web_rsts_checkc(rsts_in(i));
        FETCH web_rsts_checkc
          INTO web_rsts_checkc_flag;

        IF web_rsts_checkc%NOTFOUND THEN
          CLOSE web_rsts_checkc;
          twbkwbis.p_dispinfo(called_by_proc_name, 'BADRSTS');
          RETURN;
        END IF;

        CLOSE web_rsts_checkc;
      END IF;
    END LOOP;

    IF NOT multi_term THEN
      -- This added for security reasons, in order to prevent students
      -- saving the add/droppage while registration is open, and
      -- re-using the saved page after it has closed
      IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
        twbkwbis.p_dispinfo('bwskflib.P_SelDefTerm', 'BADTERM');
        RETURN;
      END IF;
    END IF;
    dbms_output.put_line('BEFORE CLASS SEARCH');
    --
    -- If the class search button was pressed...
    -- ===================================================
    /* IF LTRIM(RTRIM(reg_btn(2))) = lv_reg_btn1 THEN
      bwckgens.P_RegsCrseSearch(term_in,
                                rsts_in,
                                assoc_term_in,
                                crn,
                                start_date_in,
                                end_date_in,
                                subj,
                                crse,
                                sec,
                                levl,
                                cred,
                                gmod,
                                title,
                                mesg,
                                regs_row,
                                add_row,
                                wait_row);
      RETURN;
    END IF;*/

    --
    -- If the Register, or Submit button was pressed.
    -- ===================================================
    --
    -- Setup globals.
    -- ===================================================
    bwcklibs.p_initvalue(global_pidm, term, '', SYSDATE, '', '');

    --
    -- Check if student is allowed to register - single term
    -- only, as multi term is handled in the search results
    -- page.
    -- ===================================================
    dbms_output.put_line('BEFORE RESSTU');
    /* IF NOT multi_term THEN
      IF NOT bwcksams.f_regsstu(genpidm, term, called_by_proc_name) THEN
        RETURN;
      END IF;
    END IF;*/

    --
    -- Check whether OLR courses have been chosen, and if so,
    -- confirm their start/end dates
    -- ======================================================
    dbms_output.put_line('BEFORE OLR');
    FOR i IN 2 .. crn.COUNT LOOP
      IF crn(i) IS NULL THEN
        EXIT;
      END IF;

      IF NOT multi_term AND
         (assoc_term(i) IS NULL OR assoc_term(i) = 'dummy') THEN
        assoc_term(i) := term;
      END IF;

      IF NOT start_date.EXISTS(i) THEN
        start_date(i) := NULL;
        end_date(i) := NULL;
      END IF;

      IF NOT olr_course_selected THEN
        IF sfkolrl.f_open_learning_course(assoc_term(i), crn(i)) THEN
          IF start_date(i) IS NULL THEN
            olr_course_selected := TRUE;
          END IF;
        END IF;
      END IF;
    END LOOP;

    IF olr_course_selected THEN
      bwckcoms.p_disp_start_date_confirm(term_in,
                                         rsts_in,
                                         assoc_term,
                                         crn,
                                         start_date,
                                         end_date,
                                         subj,
                                         crse,
                                         sec,
                                         levl,
                                         cred,
                                         gmod,
                                         title,
                                         mesg,
                                         reg_btn,
                                         regs_row,
                                         add_row,
                                         wait_row,
                                         NULL,
                                         NULL);
      RETURN;
    END IF;

    --
    -- Initialize arrays.
    -- ===================================================
    err_term(1) := 'dummy';
    err_crn(1) := 'dummy';
    err_code(1) := 'dummy';
    err_subj(1) := 'dummy';
    err_crse(1) := 'dummy';
    err_sec(1) := 'dummy';
    err_levl(1) := 'dummy';
    err_cred(1) := 'dummy';
    err_gmod(1) := 'dummy';

    --
    -- Check for admin errors that may have been introduced
    -- during this add/drop session.
    -- ====================================================
    dbms_output.put_line('BEFORE ADD/DROP');
    FOR i IN 1 .. term_in.COUNT LOOP

      /* Reset any linked courses that should be dropped. */
      /* This is so that they will be re-flagged as requiring drop confirmation. */
      /* This handles the scenario where a user presses the back button from */
      /* the drop confirmation page. */
      UPDATE sftregs a
         SET a.sftregs_error_flag     = a.sftregs_hold_error_flag,
             a.sftregs_rmsg_cde       = a.sftregs_hold_rmsg_cde,
             a.sftregs_message        = a.sftregs_hold_message,
             a.sftregs_rsts_code      = a.sftregs_hold_rsts_code,
             a.sftregs_rsts_date      = a.sftregs_hold_rsts_date,
             a.sftregs_vr_status_type = a.sftregs_hold_rsts_type,
             a.sftregs_grde_code      = a.sftregs_hold_grde_code
       WHERE a.sftregs_term_code = term_in(i)
         AND a.sftregs_pidm = genpidm
         AND a.sftregs_error_link IS NOT NULL
         AND a.sftregs_error_link =
             (SELECT b.sftregs_error_link
                FROM sftregs b
               WHERE b.sftregs_term_code = a.sftregs_term_code
                 AND b.sftregs_pidm = a.sftregs_pidm
                 AND b.sftregs_crn <> a.sftregs_crn
                 AND b.sftregs_error_flag = 'F'
                 AND ROWNUM = 1);

      UPDATE sftregs
         SET sftregs_error_flag     = sftregs_hold_error_flag,
             sftregs_rmsg_cde       = sftregs_hold_rmsg_cde,
             sftregs_message        = sftregs_hold_message,
             sftregs_rsts_code      = sftregs_hold_rsts_code,
             sftregs_rsts_date      = sftregs_hold_rsts_date,
             sftregs_vr_status_type = sftregs_hold_rsts_type,
             sftregs_grde_code      = sftregs_hold_grde_code
       WHERE sftregs_term_code = term_in(i)
         AND sftregs_pidm = genpidm
         AND sftregs_error_flag = 'F';

      sfkmods.p_admin_msgs(genpidm, term_in(i), stufac_ind || global_pidm);
      local_capp_tech_error := sfkfunc.f_get_capp_tech_error(genpidm,
                                                             term_in(i));
      IF local_capp_tech_error IS NOT NULL THEN
        bwckcoms.p_adddrop2(term_in,
                            err_term,
                            err_crn,
                            err_subj,
                            err_crse,
                            err_sec,
                            err_code,
                            err_levl,
                            err_cred,
                            err_gmod,
                            local_capp_tech_error,
                            NULL,
                            drop_problems,
                            drop_failures);
        RETURN;
      END IF;

      IF NOT bwckregs.f_finalize_admindrops(genpidm,
                                            term_in(i),
                                            stufac_ind || global_pidm) THEN
        bwckfrmt.p_open_doc(called_by_proc_name,
                            term,
                            NULL,
                            multi_term,
                            term_in(1));
        twbkwbis.p_dispinfo(called_by_proc_name, 'SESSIONBLOCKED');
        twbkwbis.p_closedoc(curr_release);
        RETURN;
      END IF;

    END LOOP;

    i := 2;
    dbms_output.put_line('BEFORE REGS MAIN LOOP');
    --
    -- Loop through the registration records on the page.
    -- ===================================================
    WHILE i <= regs_row + 1 LOOP
      BEGIN
        DBMS_OUTPUT.PUT_LINE('INSIDE REGS MAIN LOOP');
        IF LTRIM(RTRIM(rsts_in(i))) IS NOT NULL THEN
          sftregs_rec.sftregs_crn       := crn(i);
          sftregs_rec.sftregs_pidm      := genpidm;
          sftregs_rec.sftregs_term_code := assoc_term(i);
          sftregs_rec.sftregs_rsts_code := rsts_in(i);
          DBMS_OUTPUT.PUT_LINE('INSIDE RSTS IF');
          DBMS_OUTPUT.PUT_LINE(crn(i) || rsts_in(i));
          BEGIN
            IF multi_term THEN
              --
              -- Get the latest student and registration records.
              -- ===================================================
              bwckcoms.p_regs_etrm_chk(genpidm,
                                       assoc_term(i),
                                       clas_code,
                                       multi_term);
            ELSIF NOT etrm_done THEN
              DBMS_OUTPUT.PUT_LINE('ETRM CHK');
              /* bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);*/
              etrm_done := TRUE;
            END IF;
            DBMS_OUTPUT.PUT_LINE('INSIDE p_getsection');
            --
            -- Get the section information.
            -- ===================================================
            bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                                  sftregs_rec.sftregs_crn,
                                  sftregs_rec.sftregs_sect_subj_code,
                                  sftregs_rec.sftregs_sect_crse_numb,
                                  sftregs_rec.sftregs_sect_seq_numb);

            --
            -- If the action corresponds to gtvsdax web drop code,
            -- then call the procedure to drop a course.
            -- ===================================================
            DBMS_OUTPUT.PUT_LINE('BEFORE DROP IF');
            IF rsts_in(i) = SUBSTR(f_stu_getwebregsrsts('D'), 1, 2) THEN
              DBMS_OUTPUT.put_line('BEFORE DROPCRSE');
              /*ME_bwckregs.p_dropcrse(sftregs_rec.sftregs_term_code,
              sftregs_rec.sftregs_crn,
              sftregs_rec.sftregs_rsts_code,
              sftregs_rec.sftregs_reserved_key,
              rec_stat                         => 'D',
              del_ind                          => 'Y');*/

              me_bwckregs.p_dropcrse(term    => sftregs_rec.sftregs_term_code,
                                     crn     => sftregs_rec.sftregs_crn,
                                     rsts    => sftregs_rec.sftregs_rsts_code /*'DW'*/, --replaced DD with DW
                                     pidm    => sftregs_rec.sftregs_pidm,
                                     p_error => V_ERROR);
              --
              -- If the second character of the action is not D, then
              -- call the procedure to update a course.
              -- ===================================================
            ELSE
              DBMS_OUTPUT.put_line('BEFORE UPDCRSE' ||
                                   sftregs_rec.sftregs_crn ||
                                   sftregs_rec.sftregs_rsts_code);
              ME_bwckregs.p_updcrse(sftregs_rec);
            END IF;

            --
            -- Create a batch fee assessment record.
            -- ===================================================
            sfrbtch_row.sfrbtch_term_code     := assoc_term(i);
            sfrbtch_row.sfrbtch_pidm          := genpidm;
            sfrbtch_row.sfrbtch_clas_code     := clas_code;
            sfrbtch_row.sfrbtch_activity_date := SYSDATE;
            bwcklibs.p_add_sfrbtch(sfrbtch_row);
            tbrcbrq_row.tbrcbrq_term_code     := assoc_term(i);
            tbrcbrq_row.tbrcbrq_pidm          := genpidm;
            tbrcbrq_row.tbrcbrq_activity_date := SYSDATE;
            bwcklibs.p_add_tbrcbrq(tbrcbrq_row);
          EXCEPTION
            WHEN OTHERS THEN
              IF SQLCODE = gb_event.APP_ERROR THEN
                twbkfrmt.p_storeapimessages(SQLERRM);
                RAISE;
              END IF;
              k := k + 1;
              err_term(k) := assoc_term(i);
              err_crn(k) := crn(i);
              err_subj(k) := sftregs_rec.sftregs_sect_subj_code;
              err_crse(k) := sftregs_rec.sftregs_sect_crse_numb;
              err_sec(k) := sftregs_rec.sftregs_sect_seq_numb;
              err_code(k) := SQLCODE;
              err_levl(k) := lcur_tab(1).r_levl_code;
              err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
              err_gmod(k) := sftregs_rec.sftregs_gmod_code;
          END;
        END IF;

        i := i + 1;
      EXCEPTION
        WHEN OTHERS THEN
          IF SQLCODE = gb_event.APP_ERROR THEN
            twbkfrmt.p_storeapimessages(SQLERRM);
            RAISE;
          END IF;
          i := i + 1;
      END;
    END LOOP;
    dbms_output.put_line('BEFORE ADD LOOP');
    --
    -- Loop through the add table on the page.
    -- ===================================================

    WHILE i <= regs_row + wait_row + add_row + 1 LOOP
      DECLARE
      BEGIN
        IF crn(i) IS NOT NULL AND LTRIM(RTRIM(rsts_in(i))) IS NOT NULL THEN
          sftregs_rec.sftregs_crn       := crn(i);
          sftregs_rec.sftregs_rsts_code := rsts_in(i);
          sftregs_rec.sftregs_rsts_date := SYSDATE;
          sftregs_rec.sftregs_pidm      := genpidm;
          sftregs_rec.sftregs_term_code := assoc_term(i);

          BEGIN
            IF multi_term THEN
              --
              -- Get the latest student and registration records.
              -- ===================================================
              bwckcoms.p_regs_etrm_chk(genpidm,
                                       assoc_term(i),
                                       clas_code,
                                       multi_term);
            ELSIF NOT etrm_done THEN
              bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);
              etrm_done := TRUE;
            END IF;

            --
            -- Get the section information.
            -- ===================================================
            bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                                  sftregs_rec.sftregs_crn,
                                  sftregs_rec.sftregs_sect_subj_code,
                                  sftregs_rec.sftregs_sect_crse_numb,
                                  sftregs_rec.sftregs_sect_seq_numb);
            --
            -- Call the procedure to add a course.
            -- ===================================================
            bwckregs.p_addcrse(sftregs_rec,
                               sftregs_rec.sftregs_sect_subj_code,
                               sftregs_rec.sftregs_sect_crse_numb,
                               sftregs_rec.sftregs_sect_seq_numb,
                               start_date(i),
                               end_date(i));
            --
            -- Create a batch fee assessment record.
            -- ===================================================
            sfrbtch_row.sfrbtch_term_code     := assoc_term(i);
            sfrbtch_row.sfrbtch_pidm          := genpidm;
            sfrbtch_row.sfrbtch_clas_code     := clas_code;
            sfrbtch_row.sfrbtch_activity_date := SYSDATE;
            bwcklibs.p_add_sfrbtch(sfrbtch_row);
            tbrcbrq_row.tbrcbrq_term_code     := assoc_term(i);
            tbrcbrq_row.tbrcbrq_pidm          := genpidm;
            tbrcbrq_row.tbrcbrq_activity_date := SYSDATE;
            bwcklibs.p_add_tbrcbrq(tbrcbrq_row);
          EXCEPTION
            WHEN OTHERS THEN
              IF SQLCODE = gb_event.APP_ERROR THEN
                twbkfrmt.p_storeapimessages(SQLERRM);
                RAISE;
              END IF;
              k := k + 1;
              err_term(k) := assoc_term(i);
              err_crn(k) := crn(i);
              err_subj(k) := sftregs_rec.sftregs_sect_subj_code;
              err_crse(k) := sftregs_rec.sftregs_sect_crse_numb;
              err_sec(k) := sftregs_rec.sftregs_sect_seq_numb;
              err_code(k) := SQLCODE;
              err_levl(k) := lcur_tab(1).r_levl_code;
              err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
              err_gmod(k) := sftregs_rec.sftregs_gmod_code;
          END;
        ELSE
          BEGIN
            --
            -- ?????
            -- ===================================================
            IF subj(i) IS NOT NULL AND crse(i) IS NOT NULL AND
               sec(i) IS NOT NULL AND LTRIM(RTRIM(rsts_in(i))) IS NOT NULL THEN
              BEGIN
                sftregs_rec.sftregs_rsts_code := rsts_in(i);
                sftregs_rec.sftregs_rsts_date := SYSDATE;
                sftregs_rec.sftregs_pidm      := genpidm;
                sftregs_rec.sftregs_term_code := assoc_term(i);

                IF multi_term THEN
                  --
                  -- Get the latest student and registration records.
                  -- ===================================================
                  bwckcoms.p_regs_etrm_chk(genpidm,
                                           assoc_term(i),
                                           clas_code,
                                           multi_term);
                ELSIF NOT etrm_done THEN
                  bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);
                  etrm_done := TRUE;
                END IF;

                bwckregs.p_addcrse(sftregs_rec,
                                   subj(i),
                                   crse(i),
                                   sec(i),
                                   start_date(i),
                                   end_date(i));
                --                        COMMIT;
                --                        commit_flag := 'Y';
              EXCEPTION
                WHEN OTHERS THEN
                  IF SQLCODE = gb_event.APP_ERROR THEN
                    twbkfrmt.p_storeapimessages(SQLERRM);
                    RAISE;
                  END IF;
                  k := k + 1;
                  err_term(k) := assoc_term(i);
                  err_crn(k) := crn(i);
                  err_subj(k) := subj(i);
                  err_crse(k) := crse(i);
                  err_sec(k) := sec(i);
                  err_code(k) := SQLCODE;
                  err_levl(k) := lcur_tab(1).r_levl_code;
                  err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
                  err_gmod(k) := sftregs_rec.sftregs_gmod_code;
                  --                           ROLLBACK;
                --                           commit_flag := 'N';
              END;
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              IF SQLCODE = gb_event.APP_ERROR THEN
                twbkfrmt.p_storeapimessages(SQLERRM);
                RAISE;
              END IF;
              NULL;
          END;
        END IF;

        i := i + 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          BEGIN
            IF subj(i) IS NOT NULL AND crse(i) IS NOT NULL AND
               sec(i) IS NOT NULL THEN
              BEGIN
                sftregs_rec.sftregs_rsts_code := rsts_in(i);
                sftregs_rec.sftregs_rsts_date := SYSDATE;
                sftregs_rec.sftregs_pidm      := genpidm;
                sftregs_rec.sftregs_term_code := assoc_term(i);

                IF multi_term THEN
                  --
                  -- Get the latest student and registration records.
                  -- ===================================================
                  bwckcoms.p_regs_etrm_chk(genpidm,
                                           assoc_term(i),
                                           clas_code,
                                           multi_term);
                ELSIF NOT etrm_done THEN
                  bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);
                  etrm_done := TRUE;
                END IF;

                bwckregs.p_addcrse(sftregs_rec,
                                   subj(i),
                                   crse(i),
                                   sec(i),
                                   start_date(i),
                                   end_date(i));
                --                        COMMIT;
                --                        commit_flag := 'Y';
              EXCEPTION
                WHEN OTHERS THEN
                  IF SQLCODE = gb_event.APP_ERROR THEN
                    twbkfrmt.p_storeapimessages(SQLERRM);
                    RAISE;
                  END IF;
                  k := k + 1;
                  err_term(k) := assoc_term(i);
                  err_crn(k) := crn(i);
                  err_subj(k) := subj(i);
                  err_crse(k) := crse(i);
                  err_sec(k) := sec(i);
                  err_code(k) := SQLCODE;
                  err_levl(k) := lcur_tab(1).r_levl_code;
                  err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
                  err_gmod(k) := sftregs_rec.sftregs_gmod_code;
                  --                           ROLLBACK;
                --                           commit_flag := 'N';
              END;
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              IF SQLCODE = gb_event.APP_ERROR THEN
                twbkfrmt.p_storeapimessages(SQLERRM);
                RAISE;
              END IF;
              NULL;
          END;

          i := i + 1;
      END;
    END LOOP;

    --
    -- Do batch validation on all registration records.
    -- ===================================================
    ME_bwckcoms.p_group_edits(term_in,
                              genpidm,
                              etrm_done,
                              capp_tech_error,
                              drop_problems,
                              drop_failures);
    DBMS_OUTPUT.PUT_LINE('BEFORE p_problems');
    If capp_tech_error is null then
      DBMS_OUTPUT.PUT_LINE('IN p_problems');
      ME_bwckcoms.p_problems(term_in,
                             err_term,
                             err_crn,
                             err_subj,
                             err_crse,
                             err_sec,
                             err_code,
                             err_levl,
                             err_cred,
                             err_gmod,
                             drop_problems,
                             drop_failures);
      return;
    end if;

    --
    -- Redisplay the add/drop page (after a capp error)
    -- ===================================================
    bwckcoms.p_adddrop2(term_in,
                        err_term,
                        err_crn,
                        err_subj,
                        err_crse,
                        err_sec,
                        err_code,
                        err_levl,
                        err_cred,
                        err_gmod,
                        capp_tech_error,
                        NULL,
                        drop_problems,
                        drop_failures);
    /*EXCEPTION
    WHEN OTHERS THEN
      IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
        \*
        * FACWEB-specific code
        *\
        IF NOT bwlkilib.f_add_drp(term) THEN
          NULL;
        END IF;

        bwckfrmt.p_open_doc(called_by_proc_name,
                            term,
                            NULL,
                            multi_term,
                            term_in(1));
      ELSE
        \*
        * STUWEB-specific code
        *\
        IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
          NULL;
        END IF;

        \*\*   bwckfrmt.p_open_doc (
           called_by_proc_name,
           term,
           NULL,
           multi_term,
           term_in (1)*\
        );*\
      END IF; \* END stu/fac specific code sections *\

      IF SQLCODE = gb_event.APP_ERROR THEN
        twbkfrmt.p_storeapimessages(SQLERRM);
      ELSE
        IF SQLCODE <= -20000 AND SQLCODE >= -20999 THEN
          twbkfrmt.p_printmessage(SUBSTR(SQLERRM, INSTR(SQLERRM, ':') + 1),
                                  'ERROR');
        ELSE
          twbkfrmt.p_printmessage(G$_NLS.Get('BWCKCOM1-0022',
                                             'SQL',
                                             'Error occurred while processing registration changes.'),
                                  'ERROR');
        END IF;
      END IF;*/
  END p_regs;

  PROCEDURE p_disp_pin_prompt IS
    useridname VARCHAR2(500);
    useridlen  NUMBER;
    pinlen     NUMBER;
    pinname    VARCHAR2(500);
  BEGIN
    twbkfrmt.P_TableOpen('DATAENTRY',
                         cattributes => G$_NLS.Get('BWCKCOM1-0023',
                                                   'SQL',
                                                   'summary="This table allows the user to enter a student Personal Identification Number."'));

    IF SUBSTR(UPPER(twbklibs.twgbldap_rec.twgbldap_protocol), 1, 4) =
       'LDAP' THEN
      pinlen  := twbkwbis.F_FetchWTParam('LDAPPWDLENGTH');
      pinname := twbkwbis.F_FetchWTParam('PINNAME');
      IF twbkwbis.F_FetchWTParam('LDAPMAPUSER') = 'PROMPT' THEN
        useridlen  := twbkwbis.F_FetchWTParam('USERIDLENGTH');
        useridname := twbkwbis.F_FetchWTParam('USERIDNAME');

        twbkfrmt.P_TableRowOpen;
        twbkfrmt.P_TableDataLabel(twbkfrmt.F_FormLabel(twbkwbis.F_FetchWTParam('USERIDNAME'),
                                                       idname => 'userid_input'));
        twbkfrmt.P_TableData(twbkfrmt.f_formtext('ldap_userid',
                                                 useridlen + 2,
                                                 useridlen,
                                                 cattributes => 'ID="userid_input"'));
        twbkfrmt.P_TableRowClose;
      END IF;
      twbkfrmt.P_TableRowOpen;
      twbkfrmt.P_TableDataLabel(twbkfrmt.f_formlabel(pinname,
                                                     idname => 'student_pin'));
    ELSE
      pinlen := twbkwbis.F_FetchWTParam('PINLENGTH');
      twbkfrmt.P_TableRowOpen;
      twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(G$_NLS.Get('BWCKCOM1-0024',
                                                                'SQL',
                                                                'Student ') ||
                                                     '<ACRONYM title = "' ||
                                                     g$_nls.get('BWCKCOM1-0025',
                                                                'SQL',
                                                                'Personal Identification Number') || '">' ||
                                                     g$_nls.get('BWCKCOM1-0026',
                                                                'SQL',
                                                                'PIN') ||
                                                     '</ACRONYM>' || ':',
                                                     idname => 'student_pin'));
    END IF;
    twbkfrmt.P_TableData(HTF.formpassword('PIN_NUMB',
                                          pinlen + 1,
                                          pinlen,
                                          cattributes => 'ID="student_pin"'));
    twbkfrmt.P_TableRowClose;
    twbkfrmt.P_TableClose;
    htp.br;

  END p_disp_pin_prompt;

  FUNCTION f_validatestudentpin(pidm        SPRIDEN.SPRIDEN_PIDM%TYPE,
                                pin         GOBTPAC.GOBTPAC_PIN%TYPE,
                                ldap_userid IN VARCHAR2 DEFAULT NULL)
    RETURN BOOLEAN IS
    pin_valid    VARCHAR2(1);
    pin_expired  VARCHAR2(1);
    pin_disabled VARCHAR2(1);
    use_ldap     VARCHAR2(10);

    lv_id       VARCHAR2(30);
    ldapparam   VARCHAR2(255);
    iden_id_cv  SYS_REFCURSOR;
    iden_id_rec gb_identification.identification_rec;
  BEGIN

    IF SUBSTR(UPPER(twbklibs.twgbldap_rec.twgbldap_protocol), 1, 4) =
       'LDAP' THEN
      ldapparam := twbkwbis.F_FetchWTParam('LDAPMAPUSER');
      -- Use spriden id for the validation logic when using the DEFAULT option.
      IF nvl(ldapparam, ' ') = 'DEFAULT' THEN
        iden_id_cv := gb_identification.f_query_one(pidm, NULL);
        FETCH iden_id_cv
          INTO iden_id_rec;
        CLOSE iden_id_cv;
        lv_id := iden_id_rec.r_id;
      END IF;
    END IF;
    twbklogn.p_validate_pin(p_id           => nvl(ldap_userid, lv_id),
                            p_pidm         => pidm,
                            p_pin          => pin,
                            p_pin_valid    => pin_valid,
                            p_pin_expired  => pin_expired,
                            p_pin_disabled => pin_disabled,
                            p_use_ldap     => use_ldap);

    IF pin_valid = 'Y' THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;

  END f_validatestudentpin;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- This procedure displays the first part of the add/drop page.
  -- Called when coming into add/drop from lookup classes page.
  -- =====================================================================
  PROCEDURE p_add_drop_init(term_in IN OWA_UTIL.ident_arr) IS
    term       stvterm.stvterm_code%TYPE := NULL;
    multi_term BOOLEAN := TRUE;

    CURSOR reg_check_c(pidm_in SPRIDEN.SPRIDEN_PIDM%TYPE,
                       term_in stvterm.stvterm_code%TYPE) IS
      SELECT NVL(COUNT(*), 0)
        FROM sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in;

  BEGIN
    /*  IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');
    ELSE
      genpidm := global_pidm;
    END IF;

    /* FACWEB-specific code */
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      IF NOT multi_term THEN
        IF NOT bwcksams.f_regsstu(global_pidm,
                                  term,
                                  'bwlkfrad.P_FacAddDropCrse') THEN
          RETURN;
        END IF;

        /* Check to see if add/drop activity is allowed for selected term */
        IF NOT bwlkilib.f_add_drp(term) THEN
          bwckfrmt.p_open_doc('bwlkfrad.P_FacAddDropCrse',
                              term,
                              NULL,
                              multi_term,
                              term_in(1));
          twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0027',
                                             'SQL',
                                             'Registration is not allowed for this term at this time'),
                                  'ERROR');
          twbkwbis.p_closedoc(curr_release);
          RETURN;
        END IF;

        IF NOT sfkvars.add_allowed THEN
          OPEN reg_check_c(genpidm, term);
          FETCH reg_check_c
            INTO row_count;
          CLOSE reg_check_c;

          IF row_count = 0 THEN
            bwckfrmt.p_open_doc('bwlkfrad.P_FacAddDropCrse',
                                term,
                                NULL,
                                multi_term,
                                term_in(1));
            twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0028',
                                               'SQL',
                                               'Registration is not allowed at this time'),
                                    'ERROR');
            twbkwbis.p_closedoc(curr_release);
            RETURN;
          END IF;
        END IF;

        IF NOT sfkvars.regs_allowed THEN
          bwckfrmt.p_open_doc('bwlkfrad.P_FacAddDropCrse',
                              term,
                              NULL,
                              multi_term,
                              term_in(1));
          twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0029',
                                             'SQL',
                                             'Registration is not allowed at this time'),
                                  'ERROR');
          twbkwbis.p_closedoc(curr_release);
          RETURN;
        END IF;
      END IF; /* not multi_term */

      bwckfrmt.p_open_doc('bwlkfrad.P_FacAddDropCrse',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));

      /* STUWEB-specific code */
    ELSE
      IF NOT multi_term THEN
        --
        -- Validate the term. If not valid, prompt for a new one.
        -- ======================================================
        IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
          bwskflib.p_seldefterm(term, 'bwskfreg.P_AddDropCrse');
          RETURN;
        END IF;

        --
        -- Check if the current user is eligible to register for
        -- the current term.
        -- ===================================================
        IF NOT
            bwcksams.f_regsstu(global_pidm, term, 'bwskfreg.P_AddDropCrse') THEN
          RETURN;
        END IF;

        IF NOT sfkvars.regs_allowed THEN

          bwckfrmt.p_open_doc('bwskfreg.P_AddDropCrse',
                              term,
                              NULL,
                              multi_term,
                              term_in(1));

          twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0030',
                                             'SQL',
                                             'Registration is not allowed at this time'),
                                  'ERROR');

          twbkwbis.p_closedoc(curr_release);
          RETURN;
        END IF;

        --
        -- Check if registration is allowed at the current time.
        -- ===================================================
        IF NOT sfkvars.add_allowed THEN
          OPEN reg_check_c(genpidm, term);
          FETCH reg_check_c
            INTO row_count;
          CLOSE reg_check_c;

          IF row_count = 0 THEN

            bwckfrmt.p_open_doc('bwskfreg.P_AddDropCrse',
                                term,
                                NULL,
                                multi_term,
                                term_in(1));
            twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0031',
                                               'SQL',
                                               'Registration is not allowed at this time'),
                                    'ERROR');
            twbkwbis.p_closedoc(curr_release);
            RETURN;
          END IF;
        END IF;
      END IF; /* not multi_term */
      bwckfrmt.p_open_doc('bwskfreg.P_AddDropCrse',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));

    END IF; /* END stu/fac specific code sections */
  END p_add_drop_init;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- This procedure displays the current registration portion of the
  -- add/drop page.
  -- =====================================================================
  PROCEDURE p_curr_sched_heading(heading_displayed IN OUT BOOLEAN,
                                 term              IN STVTERM.STVTERM_CODE%TYPE DEFAULT NULL,
                                 multi_term_in     IN BOOLEAN DEFAULT FALSE) IS
  BEGIN
    /*   IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    IF NOT heading_displayed THEN
      IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
        genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                             '999999999');
      ELSE
        genpidm := global_pidm;
      END IF;

      --
      -- Print student name subtitle for facweb.
      -- ==================================================
      IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
        --
        -- Check if student is confidential.
        -- Print subtitle, with or without link to address page.
        -- ==================================================
        bwcklibs.P_ConfidStudInfo(genpidm, term);
      END IF;

      twbkfrmt.p_printheader('3',
                             g$_nls.get('BWCKCOM1-0032',
                                        'SQL',
                                        'Current Schedule'));
      twbkfrmt.p_tableopen('DATADISPLAY',
                           cattributes => 'SUMMARY="' ||
                                          g$_nls.get('BWCKCOM1-0033',
                                                     'SQL',
                                                     'Current Schedule') || '"');
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0034',
                                            'SQL',
                                            'Status'));
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0035',
                                            'SQL',
                                            'Action'));

      IF multi_term_in THEN
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0036',
                                              'SQL',
                                              'Associated Term'));
      END IF;

      twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                      g$_nls.get('BWCKCOM1-0037',
                                                                 'SQL',
                                                                 'Course Reference Number') || '">' ||
                                                      g$_nls.get('BWCKCOM1-0038',
                                                                 'SQL',
                                                                 'CRN') ||
                                                      '</ACRONYM>'));
      twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                      g$_nls.get('BWCKCOM1-0039',
                                                                 'SQL',
                                                                 'Subject') || '>' ||
                                                      g$_nls.get('BWCKCOM1-0040',
                                                                 'SQL',
                                                                 'Subj') ||
                                                      '</ABBR>'));
      twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                      g$_nls.get('BWCKCOM1-0041',
                                                                 'SQL',
                                                                 'Course') || '>' ||
                                                      g$_nls.get('BWCKCOM1-0042',
                                                                 'SQL',
                                                                 'Crse') ||
                                                      '</ABBR>'));
      twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                      g$_nls.get('BWCKCOM1-0043',
                                                                 'SQL',
                                                                 'Section') || '>' ||
                                                      g$_nls.get('BWCKCOM1-0044',
                                                                 'SQL',
                                                                 'Sec') ||
                                                      '</ABBR>'));
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0045',
                                            'SQL',
                                            'Level'));
      twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                      g$_nls.get('BWCKCOM1-0046',
                                                                 'SQL',
                                                                 'Credit Hours') || '">' ||
                                                      g$_nls.get('BWCKCOM1-0047',
                                                                 'SQL',
                                                                 'Cred') ||
                                                      '</ABBR>'));
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0048',
                                            'SQL',
                                            'Grade Mode'));
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0049',
                                            'SQL',
                                            'Title'));
      twbkfrmt.p_tablerowclose;
      heading_displayed := TRUE;
    END IF;
  END p_curr_sched_heading;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- After displaying the current schedule, display a few
  -- summary data lines for credit hours, billing hours, etc.
  -- ==================================================
  PROCEDURE p_summary(term          IN STVTERM.STVTERM_CODE%TYPE DEFAULT NULL,
                      tot_credit_hr IN SFTREGS.SFTREGS_CREDIT_HR%TYPE DEFAULT NULL,
                      tot_bill_hr   IN SFTREGS.SFTREGS_BILL_HR%TYPE DEFAULT NULL,
                      tot_ceu       IN SFTREGS.SFTREGS_CREDIT_HR%TYPE DEFAULT NULL,
                      regs_count    IN NUMBER DEFAULT NULL) IS
    max_hr SFBETRM.SFBETRM_MHRS_OVER%TYPE := 0;
    min_hr SFBETRM.SFBETRM_MIN_HRS%TYPE := 0;
  BEGIN
    /*IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    IF regs_count > 0 THEN
      IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
        genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                             '999999999');
      ELSE
        genpidm := global_pidm;
      END IF;

      HTP.br;
      twbkfrmt.p_tableopen('PLAIN',
                           cattributes => 'SUMMARY="' ||
                                          g$_nls.get('BWCKCOM1-0050',
                                                     'SQL',
                                                     'Schedule Summary') || '"');
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledata(g$_nls.get('BWCKCOM1-0051',
                                      'SQL',
                                      'Total Credit Hours') || ': ');
      twbkfrmt.p_tabledata(RPAD(TO_CHAR(NVL(tot_credit_hr, 0), '9990D990'),
                                9));
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledata(g$_nls.get('BWCKCOM1-0052',
                                      'SQL',
                                      'Billing Hours') || ':');
      twbkfrmt.p_tabledata(RPAD(TO_CHAR(NVL(tot_bill_hr, 0), '9990D990'),
                                9));
      twbkfrmt.p_tablerowclose;

      IF NVL(tot_ceu, 0) > 0 THEN
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                  g$_nls.get('BWCKCOM1-0053',
                                                             'SQL',
                                                             'Continuing Education Units') || '">' ||
                                                  g$_nls.get('BWCKCOM1-0054',
                                                             'SQL',
                                                             'CEU') ||
                                                  '</ACRONYM>:'));
        twbkfrmt.p_tabledata(RPAD(TO_CHAR(NVL(tot_ceu, 0), '9990D990'), 9));
        twbkfrmt.p_tablerowclose;
      END IF;

      FOR sfbetrm IN sfkcurs.sfbetrmc(genpidm, term) LOOP
        twbkfrmt.p_tablerowopen;
        min_hr := sfbetrm.sfbetrm_min_hrs;
        twbkfrmt.p_tabledata(g$_nls.get('BWCKCOM1-0055',
                                        'SQL',
                                        'Minimum Hours') || ':');
        twbkfrmt.p_tabledata(RPAD(TO_CHAR(min_hr, '999990D990'), 11));
        twbkfrmt.p_tablerowclose;
        --
        twbkfrmt.p_tablerowopen;
        max_hr := sfbetrm.sfbetrm_mhrs_over;
        twbkfrmt.p_tabledata(g$_nls.get('BWCKCOM1-0056',
                                        'SQL',
                                        'Maximum Hours') || ':');
        twbkfrmt.p_tabledata(RPAD(TO_CHAR(max_hr, '999990D990'), 11));
        twbkfrmt.p_tablerowclose;
      END LOOP;

      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledata(g$_nls.get('BWCKCOM1-0057', 'SQL', 'Date') || ':');
      twbkfrmt.p_tabledata(TO_CHAR(SYSDATE,
                                   twbklibs.date_display_fmt || ' ' ||
                                   twbklibs.twgbwrul_rec.twgbwrul_time_fmt));
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tableclose;
      HTP.br;
    END IF;
  END p_summary;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- This procedure displays headings for the registration errors portion
  -- of the add/drop page.
  -- =====================================================================
  PROCEDURE p_reg_err_heading(call_type     IN NUMBER DEFAULT NULL,
                              multi_term_in IN BOOLEAN DEFAULT FALSE) IS
    table_type VARCHAR2(11);
  BEGIN
    twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0058',
                                       'SQL',
                                       'Registration Add Errors'),
                            'ERROR');
    twbkfrmt.p_tableopen('DATADISPLAY',
                         cattributes => 'SUMMARY="' ||
                                        g$_nls.get('BWCKCOM1-0059',
                                                   'SQL',
                                                   'This layout table is used to present Registration Errors') || '."');
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0060', 'SQL', 'Status'));

    IF call_type = 1 THEN
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0061',
                                            'SQL',
                                            'Action'));
    END IF;

    IF multi_term_in THEN
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0062',
                                            'SQL',
                                            'Associated Term'));
    END IF;

    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                    g$_nls.get('BWCKCOM1-0063',
                                                               'SQL',
                                                               'Course Reference Number') || '">' ||
                                                    g$_nls.get('BWCKCOM1-0064',
                                                               'SQL',
                                                               'CRN') ||
                                                    '</ACRONYM>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0065',
                                                               'SQL',
                                                               'Subject') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0066',
                                                               'SQL',
                                                               'Subj') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0067',
                                                               'SQL',
                                                               'Course') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0068',
                                                               'SQL',
                                                               'Crse') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0069',
                                                               'SQL',
                                                               'Section') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0070',
                                                               'SQL',
                                                               'Sec') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0071', 'SQL', 'Level'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                    g$_nls.get('BWCKCOM1-0072',
                                                               'SQL',
                                                               'Credit Hours') || '">' ||
                                                    g$_nls.get('BWCKCOM1-0073',
                                                               'SQL',
                                                               'Cred') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0074',
                                          'SQL',
                                          'Grade Mode'));
    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0075', 'SQL', 'Title'));
    twbkfrmt.p_tablerowclose;
  END p_reg_err_heading;

  --------------------------------------------------------------------------

  --
  -- =====================================================================
  -- This procedure displays the new crn entry boxes on the add/drop page.
  -- =====================================================================
  PROCEDURE p_add_drop_crn1(call_type        IN NUMBER DEFAULT NULL,
                            sel_crn_count_in IN NUMBER DEFAULT 0) IS
    i NUMBER;
  BEGIN
    --
    -- Display the add table.
    -- ===================================================
    IF NOT sfkvars.add_allowed THEN
      RETURN;
    END IF;

    twbkfrmt.p_printheader('3',
                           g$_nls.get('BWCKCOM1-0076',
                                      'SQL',
                                      'Add Classes Worksheet'));
    twbkfrmt.p_tableopen('DATAENTRY',
                         cattributes => 'SUMMARY="' ||
                                        g$_nls.get('BWCKCOM1-0077',
                                                   'SQL',
                                                   'Add Classes Data Entry') ||
                                        '" WIDTH="100%"');
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tableheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                g$_nls.get('BWCKCOM1-0078',
                                                           'SQL',
                                                           'Course Reference Numbers') || '">' ||
                                                g$_nls.get('BWCKCOM1-0079',
                                                           'SQL',
                                                           'CRNs') ||
                                                '</ACRONYM>'),
                           ccolspan => 10 + sel_crn_count_in);
    twbkfrmt.p_tablerowclose;
    twbkfrmt.p_tablerowopen;

    IF call_type = 1 THEN
      RETURN;
    END IF;

    FOR i IN 1 .. 10 LOOP
      twbkfrmt.p_tabledataopen;
      twbkfrmt.P_FormHidden('RSTS_IN',
                            SUBSTR(f_stu_getwebregsrsts('R'), 1, 2));
      twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0080', 'SQL', 'CRN'),
                           visible => 'INVISIBLE',
                           idname => 'crn_id' || TO_CHAR(i));
      twbkfrmt.p_formtext('CRN_IN',
                          '5',
                          '5',
                          cattributes => 'ID="crn_id' || TO_CHAR(i) || '"');
      twbkfrmt.P_FormHidden('assoc_term_in', NULL);
      twbkfrmt.P_FormHidden('start_date_in', NULL);
      twbkfrmt.P_FormHidden('end_date_in', NULL);
      twbkfrmt.p_tabledataclose;
    END LOOP;

    twbkfrmt.p_tableclose;
  END p_add_drop_crn1;

  --
  -- =====================================================================
  -- This procedure displays the new crn entry boxes on the add/drop page.
  -- =====================================================================
  PROCEDURE p_add_drop_crn2(add_row_number IN NUMBER DEFAULT NULL,
                            regs_count     IN NUMBER DEFAULT NULL,
                            wait_count     IN NUMBER DEFAULT NULL) IS
  BEGIN
    twbkfrmt.P_FormHidden('regs_row', regs_count);
    twbkfrmt.P_FormHidden('wait_row', wait_count);
    twbkfrmt.P_FormHidden('add_row', add_row_number);
    twbkfrmt.p_printtext;
    HTP.br;
    HTP.formsubmit('REG_BTN',
                   g$_nls.get('BWCKCOM1-0081', 'SQL', 'Submit Changes'));

    IF sfkvars.add_allowed THEN
      HTP.formsubmit('REG_BTN',
                     g$_nls.get('BWCKCOM1-0082', 'SQL', 'Class Search'));
    END IF;

    HTP.formreset(g$_nls.get('BWCKCOM1-0083', 'SQL', 'Reset'));
  END p_add_drop_crn2;

  --------------------------------------------------------------------------

  PROCEDURE p_disp_start_date_confirm(term_in       IN OWA_UTIL.ident_arr,
                                      rsts_in       IN OWA_UTIL.ident_arr,
                                      assoc_term_in IN OWA_UTIL.ident_arr,
                                      crn_in        IN OWA_UTIL.ident_arr,
                                      start_date_in IN OWA_UTIL.ident_arr,
                                      end_date_in   IN OWA_UTIL.ident_arr,
                                      subj          IN OWA_UTIL.ident_arr,
                                      crse          IN OWA_UTIL.ident_arr,
                                      sec           IN OWA_UTIL.ident_arr,
                                      levl          IN OWA_UTIL.ident_arr,
                                      cred          IN OWA_UTIL.ident_arr,
                                      gmod          IN OWA_UTIL.ident_arr,
                                      title         IN bwckcoms.varchar2_tabtype,
                                      mesg          IN OWA_UTIL.ident_arr,
                                      reg_btn       IN OWA_UTIL.ident_arr,
                                      regs_row      NUMBER,
                                      add_row       NUMBER,
                                      wait_row      NUMBER,
                                      next_proc_in  IN VARCHAR2,
                                      msg_in        IN VARCHAR2 DEFAULT NULL) IS
    term              stvterm.stvterm_code%TYPE := NULL;
    multi_term        BOOLEAN := TRUE;
    local_ssbsect_row ssbsect%ROWTYPE;
    i                 INTEGER;
    temp_from_date    VARCHAR2(100);
    temp_to_date      VARCHAR2(100);
  BEGIN
    --
    -- Initialize genpidm based on faculty/student.
    -- =================================================

    /*   IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      --
      -- Start the web page for Faculty.
      -- =================================================
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');
      bwckfrmt.p_open_doc('bwlkfrad.p_fac_disp_start_date_confirm',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      twbkwbis.p_dispinfo('bwlkfrad.p_fac_disp_start_date_confirm');
    ELSE
      --
      -- Start the web page for Student.
      -- =================================================
      genpidm := global_pidm;
      bwckfrmt.p_open_doc('bwskfreg.p_disp_start_date_confirm',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      twbkwbis.p_dispinfo('bwskfreg.p_disp_start_date_confirm');
    END IF;

    --
    -- Initialize variables.
    -- =================================================
    bwcklibs.p_initvalue(global_pidm, term, NULL, SYSDATE, NULL, NULL);

    --
    -- If error message passed in, something was wrong with
    -- the data entered - display the message.
    -- ==================================================

    IF msg_in IS NOT NULL THEN
      CASE substr(msg_in, 1, 1)
        WHEN '1' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0084',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0085', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0086',
                                             'SQL',
                                             '%01%, date entered (%02%) is invalid.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2))),
                                             start_date_in(to_number(substr(msg_in,
                                                                            2)))),
                                  'ERROR');

        WHEN '2' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0087',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0088', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0089',
                                             'SQL',
                                             '%01%, date entered (%02%) is invalid.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2))),
                                             end_date_in(to_number(substr(msg_in,
                                                                          2)))),
                                  'ERROR');
        WHEN '3' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0090',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0091', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0092',
                                             'SQL',
                                             '%01%, only one of start or end dates may be entered.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2)))),
                                  'ERROR');
        WHEN '4' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0093',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0094', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0095',
                                             'SQL',
                                             '%01%, start date (%02%) not within the permitted range.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2))),
                                             start_date_in(to_number(substr(msg_in,
                                                                            2)))),
                                  'ERROR');
        WHEN '5' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0096',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0097', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0098',
                                             'SQL',
                                             '%01%, start date (%02%) cannot be prior to today.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2))),
                                             start_date_in(to_number(substr(msg_in,
                                                                            2)))),
                                  'ERROR');
        WHEN '6' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0099',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0100', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0101',
                                             'SQL',
                                             '%01%, end date (%02%) is not within the permitted range.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2))),
                                             end_date_in(to_number(substr(msg_in,
                                                                          2)))),
                                  'ERROR');
        WHEN '7' THEN
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0102',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0103', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0104',
                                             'SQL',
                                             '%01%, start date (%02%), calculated from end date (%03%), cannot be prior to today.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2))),
                                             TO_CHAR(bwcklibs.f_olr_date(assoc_term_in(to_number(substr(msg_in,
                                                                                                        2))),
                                                                         crn_in(to_number(substr(msg_in,
                                                                                                 2))),
                                                                         TO_DATE(end_date_in(to_number(substr(msg_in,
                                                                                                              2))),
                                                                                 twbklibs.date_input_fmt),
                                                                         'E'),
                                                     twbklibs.date_input_fmt),
                                             end_date_in(to_number(substr(msg_in,
                                                                          2)))),
                                  'ERROR');
        ELSE
          twbkfrmt.p_printmessage('<ACRONYM title = "' ||
                                  g$_nls.get('BWCKCOM1-0105',
                                             'SQL',
                                             'Course Reference Number') || '">' ||
                                  g$_nls.get('BWCKCOM1-0106', 'SQL', 'CRN') ||
                                  '</ACRONYM> ' ||
                                  G$_NLS.Get('BWCKCOM1-0107',
                                             'SQL',
                                             '%01%, start or end date must be entered.',
                                             crn_in(to_number(substr(msg_in,
                                                                     2)))),
                                  'ERROR');
      END CASE;

    END IF;

    --
    --  Open Form, initialize form variables
    -- ==================================================
    HTP.formopen(twbkwbis.f_cgibin || 'bwckcoms.p_proc_start_date_confirm',
                 cattributes => 'onSubmit="return checkSubmit()"');

    FOR i IN 1 .. term_in.COUNT LOOP
      twbkfrmt.P_FormHidden('term_in', term_in(i));
    END LOOP;

    twbkfrmt.P_FormHidden('rsts_in', 'dummy');
    twbkfrmt.P_FormHidden('assoc_term_in', 'dummy');
    twbkfrmt.P_FormHidden('crn_in', 'dummy');
    twbkfrmt.P_FormHidden('start_date_in', 'dummy');
    twbkfrmt.P_FormHidden('end_date_in', 'dummy');
    twbkfrmt.P_FormHidden('subj', 'dummy');
    twbkfrmt.P_FormHidden('crse', 'dummy');
    twbkfrmt.P_FormHidden('sec', 'dummy');
    twbkfrmt.P_FormHidden('levl', 'dummy');
    twbkfrmt.P_FormHidden('cred', 'dummy');
    twbkfrmt.P_FormHidden('gmod', 'dummy');
    twbkfrmt.P_FormHidden('title', 'dummy');
    twbkfrmt.P_FormHidden('mesg', 'dummy');
    twbkfrmt.P_FormHidden('reg_btn', 'dummy');
    twbkfrmt.P_FormHidden('reg_btn', reg_btn(2));
    twbkfrmt.P_FormHidden('regs_row', regs_row);
    twbkfrmt.P_FormHidden('add_row', add_row);
    twbkfrmt.P_FormHidden('wait_row', wait_row);
    twbkfrmt.P_FormHidden('start_date_from_in', 'dummy');
    twbkfrmt.P_FormHidden('start_date_to_in', 'dummy');
    twbkfrmt.P_FormHidden('end_date_from_in', 'dummy');
    twbkfrmt.P_FormHidden('end_date_to_in', 'dummy');
    twbkfrmt.P_FormHidden('next_proc_in', next_proc_in);

    FOR i IN 2 .. crn_in.COUNT LOOP
      EXIT WHEN crn_in(i) IS NULL;

      --
      -- Display course information.
      -- =================================================
      IF i = 2 THEN
        twbkfrmt.p_tableopen('DATAENTRY',
                             cattributes => 'WIDTH="100%" SUMMARY="' ||
                                            g$_nls.get('BWCKCOM1-0108',
                                                       'SQL',
                                                       'This table allows the user to enter a start or end date for Open Learning courses.') || '."');
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                        g$_nls.get('BWCKCOM1-0109',
                                                                   'SQL',
                                                                   'Course Reference Number') || '">' ||
                                                        g$_nls.get('BWCKCOM1-0110',
                                                                   'SQL',
                                                                   'CRN') ||
                                                        '</ACRONYM>'));

        IF multi_term THEN
          twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0111',
                                                'SQL',
                                                'Associated Term'));
        END IF;

        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0112',
                                              'SQL',
                                              'Course'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0113',
                                              'SQL',
                                              'Course Title'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0114',
                                              'SQL',
                                              'Duration'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0115',
                                              'SQL',
                                              'Start Date (%01%)',
                                              g$_date.translate_format(twbklibs.date_input_fmt)));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0116',
                                              'SQL',
                                              'End Date (%01%)',
                                              g$_date.translate_format(twbklibs.date_input_fmt)));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0117',
                                              'SQL',
                                              'Permitted Start Dates'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0118',
                                              'SQL',
                                              'Permitted End Dates'));
        twbkfrmt.p_tablerowclose;
      END IF;

      IF i < regs_row + 2 THEN
        --
        -- pre-existing courses.
        -- =================================================
        twbkfrmt.P_FormHidden('rsts_in', rsts_in(i));
        twbkfrmt.P_FormHidden('subj', subj(i));
        twbkfrmt.P_FormHidden('crse', crse(i));
        twbkfrmt.P_FormHidden('sec', sec(i));
        twbkfrmt.P_FormHidden('levl', levl(i));
        twbkfrmt.P_FormHidden('cred', cred(i));
        twbkfrmt.P_FormHidden('gmod', gmod(i));
        twbkfrmt.P_FormHidden('title', title(i));
        twbkfrmt.P_FormHidden('mesg', mesg(i));
        twbkfrmt.P_FormHidden('crn_in', crn_in(i));
        twbkfrmt.P_FormHidden('assoc_term_in', assoc_term_in(i));
        twbkfrmt.P_FormHidden('start_date_in', start_date_in(i));
        twbkfrmt.P_FormHidden('end_date_in', end_date_in(i));
        twbkfrmt.P_FormHidden('start_date_from_in', NULL);
        twbkfrmt.P_FormHidden('start_date_to_in', NULL);
        twbkfrmt.P_FormHidden('end_date_from_in', NULL);
        twbkfrmt.P_FormHidden('end_date_to_in', NULL);
      ELSE
        --
        -- newly added course.
        -- =================================================

        OPEN ssklibs.ssbsectc(crn_in(i), assoc_term_in(i));
        FETCH ssklibs.ssbsectc
          INTO local_ssbsect_row;
        CLOSE ssklibs.ssbsectc;
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('rsts_in', rsts_in(i)) ||
                             twbkfrmt.F_FormHidden('crn_in', crn_in(i)) ||
                             crn_in(i) ||
                             twbkfrmt.F_FormHidden('assoc_term_in',
                                                   assoc_term_in(i)));

        IF multi_term THEN
          twbkfrmt.p_tabledata(bwcklibs.f_term_desc(assoc_term_in(i)));
        END IF;

        twbkfrmt.p_tabledata(local_ssbsect_row.ssbsect_subj_code || ' ' ||
                             local_ssbsect_row.ssbsect_crse_numb);
        twbkfrmt.p_tabledata(bwcklibs.f_course_title(assoc_term_in(i),
                                                     crn_in(i)));

        IF sfkolrl.f_open_learning_course(assoc_term_in(i), crn_in(i)) THEN
          twbkfrmt.p_tabledata(local_ssbsect_row.ssbsect_number_of_units || ' ' ||
                               local_ssbsect_row.ssbsect_dunt_code);

          IF (TRUNC(SYSDATE) BETWEEN
             local_ssbsect_row.ssbsect_reg_from_date AND
             local_ssbsect_row.ssbsect_reg_to_date) AND
             (TRUNC(SYSDATE) <=
             local_ssbsect_row.ssbsect_learner_regstart_tdate) THEN
            --
            -- OLR course.
            -- =================================================
            twbkfrmt.p_tabledataopen;
            twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0119',
                                            'SQL',
                                            'Start'),
                                 visible => 'INVISIBLE',
                                 idname => 'start' || TO_CHAR(i - 1) ||
                                           '_id');

            IF local_ssbsect_row.ssbsect_learner_regstart_fdate =
               local_ssbsect_row.ssbsect_learner_regstart_tdate THEN
              twbkfrmt.p_formtext('start_date_in',
                                  '15',
                                  '15',
                                  cvalue         => TO_CHAR(local_ssbsect_row.ssbsect_learner_regstart_fdate,
                                                            twbklibs.date_input_fmt),
                                  cattributes    => 'ID="start' ||
                                                    TO_CHAR(i - 1) || '_id"');
            ELSE
              twbkfrmt.p_formtext('start_date_in',
                                  '15',
                                  '15',
                                  cvalue         => start_date_in(i),
                                  cattributes    => 'ID="start' ||
                                                    TO_CHAR(i - 1) || '_id"');
            END IF;

            twbkfrmt.p_tabledataclose;
            twbkfrmt.p_tabledataopen;
            twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0120', 'SQL', 'End'),
                                 visible => 'INVISIBLE',
                                 idname => 'end' || TO_CHAR(i - 1) || '_id');
            twbkfrmt.p_formtext('end_date_in',
                                '15',
                                '15',
                                cvalue       => end_date_in(i),
                                cattributes  => 'ID="end' || TO_CHAR(i - 1) ||
                                                '_id"');
            twbkfrmt.p_tabledataclose;
          ELSE
            --
            -- OLR course with expired valid date range.
            -- =================================================
            twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('start_date_in',
                                                       'NR') ||
                                 g$_nls.get('BWCKCOM1-0121',
                                            'SQL',
                                            'Not available'));
            twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('end_date_in', 'NR') ||
                                 g$_nls.get('BWCKCOM1-0122',
                                            'SQL',
                                            'Not available'));
          END IF;

          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('start_date_from_in',
                                                     TO_CHAR(local_ssbsect_row.ssbsect_learner_regstart_fdate,
                                                             twbklibs.date_input_fmt)) ||
                               TO_CHAR(local_ssbsect_row.ssbsect_learner_regstart_fdate,
                                       twbklibs.date_input_fmt) ||
                               g$_nls.get('BWCKCOM1-0123', 'SQL', ' to ') ||
                               twbkfrmt.F_FormHidden('start_date_to_in',
                                                     TO_CHAR(local_ssbsect_row.ssbsect_learner_regstart_tdate,
                                                             twbklibs.date_input_fmt)) ||
                               TO_CHAR(local_ssbsect_row.ssbsect_learner_regstart_tdate,
                                       twbklibs.date_input_fmt));
          temp_from_date := TO_CHAR(bwcklibs.f_olr_date(assoc_term_in(i),
                                                        crn_in(i),
                                                        local_ssbsect_row.ssbsect_learner_regstart_fdate,
                                                        'S'),
                                    twbklibs.date_input_fmt);
          temp_to_date   := TO_CHAR(bwcklibs.f_olr_date(assoc_term_in(i),
                                                        crn_in(i),
                                                        local_ssbsect_row.ssbsect_learner_regstart_tdate,
                                                        'S'),
                                    twbklibs.date_input_fmt);
          twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('end_date_from_in',
                                                     temp_from_date) ||
                               temp_from_date ||
                               g$_nls.get('BWCKCOM1-0124', 'SQL', ' to ') ||
                               twbkfrmt.F_FormHidden('end_date_to_in',
                                                     temp_to_date) ||
                               temp_to_date);
        ELSE
          --
          -- Traditional course.
          -- =================================================
          twbkfrmt.p_tabledatadead(twbkfrmt.F_FormHidden('start_date_in',
                                                         NULL));
          twbkfrmt.p_tabledatadead(twbkfrmt.F_FormHidden('end_date_in',
                                                         NULL));
          twbkfrmt.p_tabledatadead(twbkfrmt.F_FormHidden('start_date_from_in',
                                                         NULL));
          twbkfrmt.p_tabledatadead(twbkfrmt.F_FormHidden('start_date_to_in',
                                                         NULL));
          twbkfrmt.p_tabledatadead(twbkfrmt.F_FormHidden('end_date_from_in',
                                                         NULL) ||
                                   twbkfrmt.F_FormHidden('end_date_to_in',
                                                         NULL));
        END IF;

        twbkfrmt.p_tablerowclose;
      END IF;
    END LOOP;

    IF crn_in.COUNT > 1 THEN
      twbkfrmt.p_tableclose;
    END IF;

    --
    -- Submit Button.
    -- =================================================
    HTP.br;
    HTP.formsubmit(NULL,
                   g$_nls.get('BWCKCOM1-0125', 'SQL', 'Submit Changes'));
    HTP.formclose;
    twbkwbis.p_closedoc(curr_release);
  END p_disp_start_date_confirm;

  --------------------------------------------------------------------------

  PROCEDURE p_proc_start_date_confirm(term_in            IN OWA_UTIL.ident_arr,
                                      rsts_in            IN OWA_UTIL.ident_arr,
                                      assoc_term_in      IN OWA_UTIL.ident_arr,
                                      crn_in             IN OWA_UTIL.ident_arr,
                                      start_date_in      IN OWA_UTIL.ident_arr,
                                      end_date_in        IN OWA_UTIL.ident_arr,
                                      subj               IN OWA_UTIL.ident_arr,
                                      crse               IN OWA_UTIL.ident_arr,
                                      sec                IN OWA_UTIL.ident_arr,
                                      levl               IN OWA_UTIL.ident_arr,
                                      cred               IN OWA_UTIL.ident_arr,
                                      gmod               IN OWA_UTIL.ident_arr,
                                      title              IN bwckcoms.varchar2_tabtype,
                                      mesg               IN OWA_UTIL.ident_arr,
                                      reg_btn            IN OWA_UTIL.ident_arr,
                                      regs_row           NUMBER,
                                      add_row            NUMBER,
                                      wait_row           NUMBER,
                                      start_date_from_in IN OWA_UTIL.ident_arr,
                                      start_date_to_in   IN OWA_UTIL.ident_arr,
                                      end_date_from_in   IN OWA_UTIL.ident_arr,
                                      end_date_to_in     IN OWA_UTIL.ident_arr,
                                      next_proc_in       IN VARCHAR2) IS
    term              stvterm.stvterm_code%TYPE := NULL;
    multi_term        BOOLEAN := TRUE;
    local_ssbsect_row ssbsect%ROWTYPE;
    i                 INTEGER;
    test_date         DATE;
    start_date        OWA_UTIL.ident_arr := start_date_in;
    end_date          OWA_UTIL.ident_arr := end_date_in;
    msg               VARCHAR2(2);
  BEGIN
    /*  IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    FOR i IN 2 .. crn_in.COUNT LOOP
      BEGIN
        --
        -- validate entered start date.
        -- =================================================
        IF start_date_in(i) IS NOT NULL AND start_date_in(i) <> 'NR' THEN
          test_date := TO_DATE(start_date_in(i), twbklibs.date_input_fmt);
        END IF;
      EXCEPTION
        /* check others to catch all date errors */
        WHEN OTHERS THEN
          msg := '1' || to_char(i);
          EXIT;
      END;

      BEGIN
        --
        -- validate entered end date.
        -- =================================================
        IF end_date_in(i) IS NOT NULL AND
           nvl(start_date_in(i), 'XX') <> 'NR' THEN
          test_date := TO_DATE(end_date_in(i), twbklibs.date_input_fmt);
        END IF;
      EXCEPTION
        /* check others to catch all date errors */
        WHEN OTHERS THEN
          msg := '2' || to_char(i);
          EXIT;
      END;

      --
      -- calculate unentered start/end dates for new OLR course.
      -- =======================================================
      IF sfkolrl.f_open_learning_course(assoc_term_in(i), crn_in(i)) AND
         NOT subj.EXISTS(i) THEN
        IF start_date_in(i) = 'NR' THEN
          start_date(i) := start_date_from_in(i);
          end_date(i) := end_date_from_in(i);
        ELSIF start_date_in(i) IS NOT NULL AND end_date_in(i) IS NOT NULL THEN
          msg := '3' || to_char(i);
          EXIT;
        ELSIF start_date_in(i) IS NOT NULL THEN
          IF NOT TO_DATE(start_date_in(i), twbklibs.date_input_fmt) BETWEEN
              TO_DATE(start_date_from_in(i), twbklibs.date_input_fmt) AND
             TO_DATE(start_date_to_in(i), twbklibs.date_input_fmt) THEN
            msg := '4' || to_char(i);
            EXIT;
          ELSIF TO_DATE(start_date_in(i), twbklibs.date_input_fmt) <
                TRUNC(SYSDATE) THEN
            msg := '5' || to_char(i);
            EXIT;
          ELSE
            end_date(i) := TO_CHAR(bwcklibs.f_olr_date(assoc_term_in(i),
                                                       crn_in(i),
                                                       TO_DATE(start_date_in(i),
                                                               twbklibs.date_input_fmt),
                                                       'S'),
                                   twbklibs.date_input_fmt);
          END IF;
        ELSIF start_date_in(i) IS NULL AND end_date_in(i) IS NOT NULL THEN
          IF NOT TO_DATE(end_date_in(i), twbklibs.date_input_fmt) BETWEEN
              TO_DATE(end_date_from_in(i), twbklibs.date_input_fmt) AND
             TO_DATE(end_date_to_in(i), twbklibs.date_input_fmt) THEN
            msg := '6' || to_char(i);
            EXIT;
          ELSE
            start_date(i) := TO_CHAR(bwcklibs.f_olr_date(assoc_term_in(i),
                                                         crn_in(i),
                                                         TO_DATE(end_date_in(i),
                                                                 twbklibs.date_input_fmt),
                                                         'E'),
                                     twbklibs.date_input_fmt);

            IF TO_DATE(start_date(i), twbklibs.date_input_fmt) <
               TRUNC(SYSDATE) THEN
              msg := '7' || to_char(i);
              EXIT;
            END IF;
          END IF;
        ELSIF start_date_in(i) IS NULL AND end_date_in(i) IS NULL THEN
          msg := '8' || to_char(i);
          EXIT;
        END IF;
      END IF;
    END LOOP;

    IF msg IS NOT NULL THEN
      p_disp_start_date_confirm(term_in,
                                rsts_in,
                                assoc_term_in,
                                crn_in,
                                start_date_in,
                                end_date_in,
                                subj,
                                crse,
                                sec,
                                levl,
                                cred,
                                gmod,
                                title,
                                mesg,
                                reg_btn,
                                regs_row,
                                add_row,
                                wait_row,
                                next_proc_in,
                                msg);
      RETURN;
    END IF;

    --
    -- no date problems, so add/drop.
    -- =================================================
    bwckcoms.p_regs(term_in,
                    rsts_in,
                    assoc_term_in,
                    crn_in,
                    start_date,
                    end_date,
                    subj,
                    crse,
                    sec,
                    levl,
                    cred,
                    gmod,
                    title,
                    mesg,
                    reg_btn,
                    regs_row,
                    add_row,
                    wait_row);
  END p_proc_start_date_confirm;

  --------------------------------------------------------------------------
  PROCEDURE p_open_rstsrowc(rstsrowc          IN OUT rstsrowc_typ,
                            open_learning     OUT BOOLEAN,
                            term_in           VARCHAR2,
                            pidm_in           NUMBER,
                            crn_in            VARCHAR2,
                            rsts_in           VARCHAR2,
                            ptrm_in           VARCHAR2,
                            sdax_rsts_code_in VARCHAR2) IS
  BEGIN
    IF sfkolrl.f_open_learning_course(term_in, crn_in) THEN
      open_learning := TRUE;
      OPEN rstsrowc FOR
        SELECT stvrsts_code,
               stvrsts_desc,
               ssrrsts_usage_cutoff_pct_from,
               ssrrsts_usage_cutoff_pct_to,
               ssrrsts_usage_cutoff_dur_from,
               ssrrsts_usage_cutoff_dur_to
          FROM stvrsts, ssrrsts
         WHERE stvrsts_code <>
               DECODE(rsts_in, 'RE', sdax_rsts_code_in, rsts_in)
           AND (NVL(stvrsts_voice_type, 'R') not in ('D', 'W') OR
               NOT EXISTS
                (SELECT 'X'
                   FROM stvrsts b
                  WHERE b.stvrsts_code = rsts_in
                    AND b.stvrsts_voice_type in ('D', 'W')))
           AND stvrsts_web_ind = 'Y'
           AND stvrsts_extension_ind <> 'Y'
           AND stvrsts_wait_ind <> 'Y'
           AND stvrsts_code = ssrrsts_rsts_code
           AND term_in = ssrrsts_term_code
           AND crn_in = ssrrsts_crn;
    ELSE
      open_learning := FALSE;
      OPEN rstsrowc FOR
        SELECT stvrsts_code,
               stvrsts_desc,
               NULL         ssrrsts_usage_cutoff_pct_from,
               NULL         ssrrsts_usage_cutoff_pct_to,
               NULL         ssrrsts_usage_cutoff_dur_from,
               NULL         ssrrsts_usage_cutoff_dur_to
          FROM stvrsts
         WHERE stvrsts_code <>
               DECODE(rsts_in, 'RE', sdax_rsts_code_in, rsts_in)
           AND (NVL(stvrsts_voice_type, 'R') not in ('D', 'W') OR
               NOT EXISTS
                (SELECT 'X'
                   FROM stvrsts b
                  WHERE b.stvrsts_code = rsts_in
                    AND b.stvrsts_voice_type in ('D', 'W')))
           AND stvrsts_web_ind = 'Y'
           AND stvrsts_extension_ind <> 'Y'
           AND stvrsts_wait_ind <> 'Y'
           AND EXISTS
         (SELECT 'X'
                  FROM sfrrsts
                 WHERE sfrrsts_rsts_code = stvrsts_code
                   AND sfrrsts_term_code = term_in
                   AND sfrrsts_ptrm_code = ptrm_in
                   AND TRUNC(SYSDATE) BETWEEN sfrrsts_start_date AND
                       sfrrsts_end_date);
    END IF;
  END p_open_rstsrowc;

  --------------------------------------------------------------------------
  PROCEDURE p_open_waitrstsrowc(waitrstsrowc  IN OUT rstsrowc_typ,
                                open_learning OUT BOOLEAN,
                                term_in       VARCHAR2,
                                pidm_in       NUMBER,
                                crn_in        VARCHAR2,
                                ptrm_in       VARCHAR2) IS
  BEGIN
    IF sfkolrl.f_open_learning_course(term_in, crn_in) THEN
      open_learning := TRUE;
      OPEN waitrstsrowc FOR
        SELECT stvrsts_code, stvrsts_desc, NULL, NULL, NULL, NULL
          FROM stvrsts, ssrrsts
         WHERE stvrsts_wait_ind = 'Y'
           AND stvrsts_extension_ind <> 'Y'
           AND stvrsts_web_ind = 'Y'
           AND stvrsts_code = ssrrsts_rsts_code
           AND term_in = ssrrsts_term_code
           AND crn_in = ssrrsts_crn;
    ELSE
      open_learning := FALSE;
      OPEN waitrstsrowc FOR
        SELECT stvrsts_code, stvrsts_desc, NULL, NULL, NULL, NULL
          FROM stvrsts
         WHERE stvrsts_wait_ind = 'Y'
           AND stvrsts_extension_ind <> 'Y'
           AND stvrsts_web_ind = 'Y'
           AND EXISTS
         (SELECT 'X'
                  FROM sfrrsts
                 WHERE sfrrsts_rsts_code = stvrsts_code
                   AND sfrrsts_term_code = term_in
                   AND sfrrsts_ptrm_code = ptrm_in
                   AND TRUNC(SYSDATE) BETWEEN sfrrsts_start_date AND
                       sfrrsts_end_date);
    END IF;
  END p_open_waitrstsrowc;

  --------------------------------------------------------------------------
  PROCEDURE p_build_action_pulldown(term_in            stvterm.stvterm_code%TYPE,
                                    pidm_in            spriden.spriden_pidm%TYPE,
                                    crn_in             ssbsect.ssbsect_crn%TYPE,
                                    start_date_in      sftregs.sftregs_start_date%TYPE,
                                    completion_date_in sftregs.sftregs_completion_date%TYPE,
                                    regstatus_date_in  sftregs.sftregs_rsts_date%TYPE,
                                    dunt_in            sftregs.sftregs_dunt_code%TYPE,
                                    regs_count_in      NUMBER,
                                    rsts_code_in       stvrsts.stvrsts_code%TYPE,
                                    ptrm_code_in       ssbsect.ssbsect_ptrm_code%TYPE DEFAULT NULL,
                                    sdax_rsts_code_in  VARCHAR2 DEFAULT NULL,
                                    hold_rsts_in       VARCHAR2 DEFAULT NULL) IS
    gtvsdax_row         gtvsdax%ROWTYPE;
    current_stvrsts_row stvrsts%ROWTYPE;
    rstsrowc            rstsrowc_typ;
    rsts_row            rstsrowc_rec_typ;
    row_count           NUMBER := 0;
    open_learning       BOOLEAN := FALSE;
  BEGIN
    --
    -- Build the Action pulldown. Loop through the
    -- NOT waitlisted reg status records.
    -- ==========================================
    p_open_rstsrowc(rstsrowc,
                    open_learning,
                    term_in,
                    pidm_in,
                    crn_in,
                    rsts_code_in,
                    ptrm_code_in,
                    sdax_rsts_code_in);

    <<rstsrowc_loop>>
    LOOP
      FETCH rstsrowc
        INTO rsts_row;
      EXIT rstsrowc_loop WHEN rstsrowc%NOTFOUND;

      --
      -- Do not allow a "Web Registered" option in the Action
      -- pulldown in the Current Schedule area.
      -- ==================================================
      sfkcurs.p_get_gtvsdax('WEBRSTSREG', 'WEBREG', gtvsdax_row);
      -- Defect 1-2ZYBH7
      OPEN stkrsts.stvrstsc(rsts_code_in);
      FETCH stkrsts.stvrstsc
        INTO current_stvrsts_row;
      CLOSE stkrsts.stvrstsc;

      IF (rsts_row.stvrsts_code = gtvsdax_row.gtvsdax_external_code)
        -- Defect :1-2ZYBH7
         AND
         ((sfkwlat.f_wl_automation_active(p_term => term_in,
                                          p_crn  => crn_in) = 'Y' AND
         (sfkwlat.f_student_is_notified(p_term => term_in,
                                          p_pidm => pidm_in,
                                          p_crn  => crn_in) = 'N' OR
         sfkwlat.f_is_seats_available(p_term => term_in,
                                         p_crn  => crn_in,
                                         p_pidm => pidm_in) = 'N') AND
         current_stvrsts_row.stvrsts_wait_ind = 'Y') OR
         sfkwlat.f_wl_automation_active(p_term => term_in, p_crn => crn_in) = 'N') THEN
        GOTO next_row;
      END IF;

      --
      -- Web Drop is the only option displayed in Action pulldown
      -- for a class that is waitlisted.
      --
      -- This code removed for defect 94298.
      -- ==================================================
      --         OPEN stkrsts.stvrstsc (rsts_code_in);
      --         FETCH stkrsts.stvrstsc INTO current_stvrsts_row;
      --         CLOSE stkrsts.stvrstsc;
      --         sfkcurs.p_get_gtvsdax ('WEBRSTSDRP', 'WEBREG', gtvsdax_row);

      --         IF     (rsts_row.stvrsts_code <> gtvsdax_row.gtvsdax_external_code)
      --            AND (current_stvrsts_row.stvrsts_wait_ind = 'Y')
      --         THEN
      --            GOTO next_row;
      --         END IF;

      --
      -- For OLR courses, check for ssrrsts usage cutoffs, if any exist.
      -- ===============================================================

      IF open_learning THEN

        IF sfkolrl.f_cutoff_reached(start_date_in,
                                    completion_date_in,
                                    regstatus_date_in,
                                    dunt_in,
                                    rsts_row.ssrrsts_usage_cutoff_pct_from,
                                    rsts_row.ssrrsts_usage_cutoff_pct_to,
                                    rsts_row.ssrrsts_usage_cutoff_dur_from,
                                    rsts_row.ssrrsts_usage_cutoff_dur_to) THEN
          GOTO next_row;
        END IF;

      END IF;

      --
      -- Display the Action pulldown.
      -- ==================================================
      row_count := row_count + 1;

      IF row_count = 1 THEN
        twbkfrmt.p_tabledataopen;
        twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0126', 'SQL', 'Action'),
                             visible => 'INVISIBLE',
                             idname => 'action_id' || TO_CHAR(regs_count_in));
        HTP.formselectopen('RSTS_IN',
                           nsize       => 1,
                           cattributes => 'ID="action_id' ||
                                          TO_CHAR(regs_count_in) || '"');

        IF hold_rsts_in IS NULL THEN
          twbkwbis.p_formselectoption(g$_nls.get('BWCKCOM1-0127',
                                                 'SQL',
                                                 'None'),
                                      NULL,
                                      'SELECTED');
        ELSE
          twbkwbis.p_formselectoption(g$_nls.get('BWCKCOM1-0128',
                                                 'SQL',
                                                 'None'),
                                      NULL);
        END IF;
      END IF;

      IF NVL(hold_rsts_in, '#') <> rsts_row.stvrsts_code THEN
        twbkwbis.p_formselectoption(rsts_row.stvrsts_desc,
                                    rsts_row.stvrsts_code);
      ELSE
        twbkwbis.p_formselectoption(rsts_row.stvrsts_desc,
                                    rsts_row.stvrsts_code,
                                    'SELECTED');
      END IF;

      <<next_row>>
      NULL;
    END LOOP rstsrowc_loop;

    CLOSE rstsrowc;

    IF row_count = 0 THEN
      twbkfrmt.p_tabledata(twbkfrmt.F_FormHidden('RSTS_IN', NULL));
    ELSE
      HTP.formselectclose;
      twbkfrmt.p_tabledataclose;
    END IF;
  END p_build_action_pulldown;

  --------------------------------------------------------------------------
  PROCEDURE p_build_wait_action_pulldown(term_in       stvterm.stvterm_code%TYPE,
                                         pidm_in       spriden.spriden_pidm%TYPE,
                                         crn_in        ssbsect.ssbsect_crn%TYPE,
                                         wait_count_in NUMBER,
                                         ptrm_code_in  ssbsect.ssbsect_ptrm_code%TYPE DEFAULT NULL,
                                         rsts_in       VARCHAR2 DEFAULT NULL,
                                         row_count_out OUT NUMBER) IS
    waitrstsrowc        rstsrowc_typ;
    rsts_row            rstsrowc_rec_typ;
    open_learning       BOOLEAN := FALSE;
    gtvsdax_row         gtvsdax%ROWTYPE;
    current_stvrsts_row stvrsts%ROWTYPE;

  BEGIN
    row_count_out := 0;
    p_open_waitrstsrowc(waitrstsrowc,
                        open_learning,
                        term_in,
                        pidm_in,
                        crn_in,
                        ptrm_code_in);

    <<waitrstsrowc_loop>>
    LOOP
      FETCH waitrstsrowc
        INTO rsts_row;
      EXIT waitrstsrowc_loop WHEN waitrstsrowc%NOTFOUND;
      --
      -- Do not check for ssrrsts usage cutoffs, as the course has never
      -- been registered - so usage will always be zero.
      -- ===============================================================

      row_count_out := row_count_out + 1;

      IF row_count_out = 1 THEN
        twbkfrmt.p_tabledataopen;
        twbkfrmt.p_formlabel(g$_nls.get('BWCKCOM1-0129', 'SQL', 'Action'),
                             visible => 'INVISIBLE',
                             idname => 'waitaction_id' ||
                                       TO_CHAR(wait_count_in));
        HTP.formselectopen('RSTS_IN',
                           nsize       => 1,
                           cattributes => 'ID="waitaction_id' ||
                                          TO_CHAR(wait_count_in) || '"');

        IF rsts_in IS NULL THEN
          twbkwbis.p_formselectoption(g$_nls.get('BWCKCOM1-0130',
                                                 'SQL',
                                                 'None'),
                                      NULL,
                                      'SELECTED');
        ELSE
          twbkwbis.p_formselectoption(g$_nls.get('BWCKCOM1-0131',
                                                 'SQL',
                                                 'None'),
                                      NULL);
        END IF;
      END IF;

      IF rsts_in <> rsts_row.stvrsts_code OR rsts_in IS NULL THEN
        twbkwbis.p_formselectoption(rsts_row.stvrsts_desc,
                                    rsts_row.stvrsts_code);
      ELSE
        twbkwbis.p_formselectoption(rsts_row.stvrsts_desc,
                                    rsts_row.stvrsts_code,
                                    'SELECTED');
      END IF;

      <<next_row>>
      NULL;
    END LOOP waitrstsrowc_loop;

    CLOSE waitrstsrowc;

    IF row_count = 0 THEN
      twbkfrmt.p_tabledata;
    ELSE
      HTP.formselectclose;
      twbkfrmt.p_tabledataclose;
    END IF;
  END p_build_wait_action_pulldown;

  --
  -- This procedure performs group edits for all selected terms
  -- =====================================================================
  PROCEDURE p_group_edits(term_in                IN OWA_UTIL.ident_arr,
                          pidm_in                IN spriden.spriden_pidm%TYPE,
                          etrm_done_in_out       IN OUT BOOLEAN,
                          capp_tech_error_in_out IN OUT VARCHAR2,
                          drop_problems_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                          drop_failures_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype) IS
    multi_term BOOLEAN := TRUE;
    clas_code  sgrclsr.sgrclsr_clas_code%TYPE;
  BEGIN
    dbms_output.put_line('pidm' || pidm_in);
    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    --
    -- Do batch validation on all registration records.
    -- ===================================================

    FOR i IN 1 .. term_in.COUNT LOOP
      IF multi_term THEN
        --
        -- Get the latest student and registration records.
        -- ===================================================
        me_bwckcoms.p_regs_etrm_chk(pidm_in,
                                    term_in(i),
                                    clas_code,
                                    multi_term,
                                    create_sfbetrm_in => FALSE);
      ELSIF NOT etrm_done_in_out THEN
        me_bwckcoms.p_regs_etrm_chk(pidm_in,
                                    term_in(i),
                                    clas_code,
                                    create_sfbetrm_in => FALSE);
        etrm_done_in_out := TRUE;
      END IF;

      me_bwckregs.p_allcrsechk(term_in(i),
                               'ADD_DROP',
                               capp_tech_error_in_out,
                               drop_problems_in_out,
                               drop_failures_in_out,
                               term_in);

      IF capp_tech_error_in_out IS NOT NULL THEN
        RETURN;
      END IF;

      -- Check for minimum hours error
      IF NVL(twbkwbis.f_getparam(genpidm, 'ERROR_FLAG'), 'N') = 'M' THEN
        sfkmods.p_delete_sftregs_by_pidm_term(genpidm, term_in(i));
        sfkmods.p_insert_sftregs_from_stcr(genpidm, term_in(i), SYSDATE);
      END IF;
    END LOOP;
  END p_group_edits;

  --
  -- This procedure determines how to proceed, based on whether a problems
  -- list has been created, and the client defined rule for what to do with
  -- the problems list.
  -- The rule options are:
  --  'C'- The user can confirm/reject problems list related drops
  --  'Y' - problems list related drops are automatically dropped
  --  'N' - problems list related drops cannot be dropped.
  -- ======================================================================
  PROCEDURE p_problems(term_in          IN OWA_UTIL.ident_arr,
                       err_term         IN OWA_UTIL.ident_arr,
                       err_crn          IN OWA_UTIL.ident_arr,
                       err_subj         IN OWA_UTIL.ident_arr,
                       err_crse         IN OWA_UTIL.ident_arr,
                       err_sec          IN OWA_UTIL.ident_arr,
                       err_code         IN OWA_UTIL.ident_arr,
                       err_levl         IN OWA_UTIL.ident_arr,
                       err_cred         IN OWA_UTIL.ident_arr,
                       err_gmod         IN OWA_UTIL.ident_arr,
                       drop_problems_in IN sfkcurs.drop_problems_rec_tabtype,
                       drop_failures_in IN sfkcurs.drop_problems_rec_tabtype) IS
    unique_drop      BOOLEAN := FALSE;
    autodrop_setting VARCHAR2(1);
    drop_msg         VARCHAR2(15) := NULL;
  BEGIN

    --
    -- Initialize genpidm based on faculty/student.
    -- =================================================

    /* IF NOT twbkwbis.f_validuser(global_pidm) THEN
          RETURN;
        END IF;
    */
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');
    ELSE
      genpidm := global_pidm;
    END IF;

    IF drop_problems_in.EXISTS(1) THEN
      autodrop_setting := sfkfunc.f_autodrop_setting;

      --
      -- Autodrop rule set to 'Y' - problems list related drops are
      -- automatically dropped.
      -- ======================================================================
      IF autodrop_setting = 'Y' THEN
        bwcklibs.p_confirm_drops(genpidm, term_in, drop_problems_in);
        drop_msg := 'DROPCONFIRMED';
        --
        -- Autodrop rule set to 'C' - The user can confirm/reject problems list
        -- related drops.
        -- ======================================================================
      ELSIF autodrop_setting = 'C' THEN
        bwckcoms.p_disp_confirm_drops(term_in,
                                      err_term,
                                      err_crn,
                                      err_subj,
                                      err_crse,
                                      err_sec,
                                      err_code,
                                      err_levl,
                                      err_cred,
                                      err_gmod,
                                      drop_problems_in,
                                      drop_failures_in);
        RETURN;
        --
        -- Autodrop rule set to 'N' - problems list related drops cannot be
        -- dropped.
        -- ======================================================================
      ELSE
        bwcklibs.p_reset_drops(genpidm, drop_problems_in);
        drop_msg := 'DROPPROHIBITED';
      END IF;
    END IF;
    DBMS_OUTPUT.PUT_LINE('BEFORE p_final_updates');
    --
    -- Failures list (problems that can't be given a drop code) courses
    -- changes are rejected.
    -- ======================================================================
    bwcklibs.p_reset_failures(genpidm, drop_failures_in);
    --
    -- Transfer all changes in SFTREGS to SFRSTCR.
    -- =====================================================================
    ME_bwckregs.p_final_updates(term_in,
                                err_term,
                                err_crn,
                                err_subj,
                                err_crse,
                                err_sec,
                                err_code,
                                err_levl,
                                err_cred,
                                err_gmod,
                                drop_msg,
                                drop_problems_in,
                                drop_failures_in);
    RETURN;
  END p_problems;

  --
  -- This procedure displays the list of 'problems', that is, courses that
  -- will be dropped as a result of dropping courses requested by the user.
  -- The user can choose to reject or accept these changes.
  -- ======================================================================
  PROCEDURE p_disp_confirm_drops(term_in          IN OWA_UTIL.ident_arr,
                                 err_term_in      IN OWA_UTIL.ident_arr,
                                 err_crn_in       IN OWA_UTIL.ident_arr,
                                 err_subj_in      IN OWA_UTIL.ident_arr,
                                 err_crse_in      IN OWA_UTIL.ident_arr,
                                 err_sec_in       IN OWA_UTIL.ident_arr,
                                 err_code_in      IN OWA_UTIL.ident_arr,
                                 err_levl_in      IN OWA_UTIL.ident_arr,
                                 err_cred_in      IN OWA_UTIL.ident_arr,
                                 err_gmod_in      IN OWA_UTIL.ident_arr,
                                 drop_problems_in IN sfkcurs.drop_problems_rec_tabtype,
                                 drop_failures_in IN sfkcurs.drop_problems_rec_tabtype) IS
    term       stvterm.stvterm_code%TYPE := NULL;
    multi_term BOOLEAN := TRUE;
    i          NUMBER;
  BEGIN

    --
    -- Initialize genpidm based on faculty/student.
    -- =================================================

    /*  IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    term := term_in(term_in.COUNT);

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      --
      -- Start the web page for Faculty.
      -- =================================================
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');
      bwckfrmt.p_open_doc('bwlkfrad.p_fac_disp_confirm_drops',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      twbkwbis.p_dispinfo('bwlkfrad.p_fac_disp_confirm_drops', 'DEFAULT');
    ELSE
      --
      -- Start the web page for Student.
      -- =================================================
      genpidm := global_pidm;
      bwckfrmt.p_open_doc('bwskfreg.p_disp_confirm_drops',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      twbkwbis.p_dispinfo('bwskfreg.p_disp_confirm_drops', 'DEFAULT');
    END IF;

    --
    -- Initialize variables.
    -- =================================================
    bwcklibs.p_initvalue(global_pidm, term, NULL, SYSDATE, NULL, NULL);

    --
    --  Open Form
    -- ==================================================
    HTP.formopen(twbkwbis.f_cgibin || 'bwckcoms.p_proc_confirm_drops',
                 cattributes => 'onSubmit="return checkSubmit()"');

    --
    -- Initialize form variables.
    -- ==================================================
    FOR i IN 1 .. term_in.COUNT LOOP
      twbkfrmt.P_FormHidden('term_in', term_in(i));
    END LOOP;

    FOR i IN 1 .. err_term_in.COUNT LOOP
      twbkfrmt.P_FormHidden('err_term_in', err_term_in(i));
      twbkfrmt.P_FormHidden('err_crn_in', err_crn_in(i));
      twbkfrmt.P_FormHidden('err_subj_in', err_subj_in(i));
      twbkfrmt.P_FormHidden('err_crse_in', err_crse_in(i));
      twbkfrmt.P_FormHidden('err_sec_in', err_sec_in(i));
      twbkfrmt.P_FormHidden('err_code_in', err_code_in(i));
      twbkfrmt.P_FormHidden('err_levl_in', err_levl_in(i));
      twbkfrmt.P_FormHidden('err_cred_in', err_cred_in(i));
      twbkfrmt.P_FormHidden('err_gmod_in', err_gmod_in(i));
    END LOOP;

    IF NOT drop_problems_in.EXISTS(1) THEN
      twbkfrmt.P_FormHidden('d_p_term_code', 'dummy');
      twbkfrmt.P_FormHidden('d_p_crn', 'dummy');
      twbkfrmt.P_FormHidden('d_p_subj', 'dummy');
      twbkfrmt.P_FormHidden('d_p_crse', 'dummy');
      twbkfrmt.P_FormHidden('d_p_sec', 'dummy');
      twbkfrmt.P_FormHidden('d_p_ptrm_code', 'dummy');
      twbkfrmt.P_FormHidden('d_p_rmsg_cde', 'dummy');
      twbkfrmt.P_FormHidden('d_p_message', 'dummy');
      twbkfrmt.P_FormHidden('d_p_start_date', 'dummy');
      twbkfrmt.P_FormHidden('d_p_comp_date', 'dummy');
      twbkfrmt.P_FormHidden('d_p_rsts_date', 'dummy');
      twbkfrmt.P_FormHidden('d_p_dunt_code', 'dummy');
      twbkfrmt.P_FormHidden('d_p_drop_code', 'dummy');
      twbkfrmt.P_FormHidden('d_p_drop_conn', 'dummy');
    ELSE
      FOR i IN 1 .. drop_problems_in.COUNT LOOP
        twbkfrmt.P_FormHidden('d_p_term_code',
                              drop_problems_in(i).term_code);
        twbkfrmt.P_FormHidden('d_p_crn', drop_problems_in(i).crn);
        twbkfrmt.P_FormHidden('d_p_subj', drop_problems_in(i).subj);
        twbkfrmt.P_FormHidden('d_p_crse', drop_problems_in(i).crse);
        twbkfrmt.P_FormHidden('d_p_sec', drop_problems_in(i).sec);
        twbkfrmt.P_FormHidden('d_p_ptrm_code',
                              drop_problems_in(i).ptrm_code);
        twbkfrmt.P_FormHidden('d_p_rmsg_cde', drop_problems_in(i).rmsg_cde);
        twbkfrmt.P_FormHidden('d_p_message', drop_problems_in(i).message);
        twbkfrmt.P_FormHidden('d_p_start_date',
                              drop_problems_in(i).start_date);
        twbkfrmt.P_FormHidden('d_p_comp_date',
                              drop_problems_in(i).comp_date);
        twbkfrmt.P_FormHidden('d_p_rsts_date',
                              drop_problems_in(i).rsts_date);
        twbkfrmt.P_FormHidden('d_p_dunt_code',
                              drop_problems_in(i).dunt_code);
        twbkfrmt.P_FormHidden('d_p_drop_code',
                              drop_problems_in(i).drop_code);
        twbkfrmt.P_FormHidden('d_p_drop_conn',
                              drop_problems_in(i).connected_crns);
      END LOOP;
    END IF;

    IF NOT drop_failures_in.EXISTS(1) THEN
      twbkfrmt.P_FormHidden('d_f_term_code', 'dummy');
      twbkfrmt.P_FormHidden('d_f_crn', 'dummy');
      twbkfrmt.P_FormHidden('d_f_subj', 'dummy');
      twbkfrmt.P_FormHidden('d_f_crse', 'dummy');
      twbkfrmt.P_FormHidden('d_f_sec', 'dummy');
      twbkfrmt.P_FormHidden('d_f_ptrm_code', 'dummy');
      twbkfrmt.P_FormHidden('d_f_rmsg_cde', 'dummy');
      twbkfrmt.P_FormHidden('d_f_message', 'dummy');
      twbkfrmt.P_FormHidden('d_f_start_date', 'dummy');
      twbkfrmt.P_FormHidden('d_f_comp_date', 'dummy');
      twbkfrmt.P_FormHidden('d_f_rsts_date', 'dummy');
      twbkfrmt.P_FormHidden('d_f_dunt_code', 'dummy');
      twbkfrmt.P_FormHidden('d_f_drop_code', 'dummy');
      twbkfrmt.P_FormHidden('d_f_drop_conn', 'dummy');

    ELSE
      FOR i IN 1 .. drop_failures_in.COUNT LOOP
        twbkfrmt.P_FormHidden('d_f_term_code',
                              drop_failures_in(i).term_code);
        twbkfrmt.P_FormHidden('d_f_crn', drop_failures_in(i).crn);
        twbkfrmt.P_FormHidden('d_f_subj', drop_failures_in(i).subj);
        twbkfrmt.P_FormHidden('d_f_crse', drop_failures_in(i).crse);
        twbkfrmt.P_FormHidden('d_f_sec', drop_failures_in(i).sec);
        twbkfrmt.P_FormHidden('d_f_ptrm_code',
                              drop_failures_in(i).ptrm_code);
        twbkfrmt.P_FormHidden('d_f_rmsg_cde', drop_failures_in(i).rmsg_cde);
        twbkfrmt.P_FormHidden('d_f_message', drop_failures_in(i).message);
        twbkfrmt.P_FormHidden('d_f_start_date',
                              drop_failures_in(i).start_date);
        twbkfrmt.P_FormHidden('d_f_comp_date',
                              drop_failures_in(i).comp_date);
        twbkfrmt.P_FormHidden('d_f_rsts_date',
                              drop_failures_in(i).rsts_date);
        twbkfrmt.P_FormHidden('d_f_dunt_code',
                              drop_failures_in(i).dunt_code);
        twbkfrmt.P_FormHidden('d_f_drop_code',
                              drop_failures_in(i).drop_code);
        twbkfrmt.P_FormHidden('d_f_drop_conn',
                              drop_failures_in(i).connected_crns);
      END LOOP;
    END IF;

    twbkfrmt.p_tableopen('DATADISPLAY',
                         cattributes  => 'SUMMARY="' ||
                                         g$_nls.get('BWCKCOM1-0132',
                                                    'SQL',
                                                    'Class Connections') || '"',
                         ccaption     => g$_nls.get('BWCKCOM1-0133',
                                                    'SQL',
                                                    'Classes that will be Dropped'));
    twbkfrmt.p_tablerowopen;

    IF multi_term THEN
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0134',
                                            'SQL',
                                            'Associated Term'));
    END IF;

    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                    g$_nls.get('BWCKCOM1-0135',
                                                               'SQL',
                                                               'Course Reference Number') || '">' ||
                                                    g$_nls.get('BWCKCOM1-0136',
                                                               'SQL',
                                                               'CRN') ||
                                                    '</ACRONYM>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0137',
                                                               'SQL',
                                                               'Subject') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0138',
                                                               'SQL',
                                                               'Subj') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0139',
                                                               'SQL',
                                                               'Course') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0140',
                                                               'SQL',
                                                               'Crse') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0141',
                                                               'SQL',
                                                               'Section') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0142',
                                                               'SQL',
                                                               'Sec') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0143', 'SQL', 'Title'));

    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0144',
                                          'SQL',
                                          'Registration Issues'));

    twbkfrmt.p_tablerowclose;

    FOR i IN 1 .. drop_problems_in.COUNT LOOP
      twbkfrmt.p_tablerowopen;

      IF multi_term THEN
        twbkfrmt.p_tabledata(bwcklibs.f_term_desc(drop_problems_in(i)
                                                  .term_code));
      END IF;

      twbkfrmt.p_tabledata(drop_problems_in(i).crn);
      twbkfrmt.p_tabledata(drop_problems_in(i).subj);
      twbkfrmt.p_tabledata(drop_problems_in(i).crse);
      twbkfrmt.p_tabledata(drop_problems_in(i).sec);
      twbkfrmt.p_tabledata(bwcklibs.f_course_title(drop_problems_in(i)
                                                   .term_code,
                                                   drop_problems_in(i).crn));
      twbkfrmt.p_tabledata(bwcklibs.f_connected_crn_message(drop_problems_in(i)
                                                            .rmsg_cde,
                                                            drop_problems_in(i)
                                                            .message,
                                                            drop_problems_in(i)
                                                            .connected_crns));
      twbkfrmt.p_tablerowclose;
    END LOOP;

    twbkfrmt.p_tableclose;

    --
    --  Infotext at bottom of page to remind user to press a button
    -- ============================================================
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      twbkwbis.p_dispinfo('bwlkfrad.p_fac_disp_confirm_drops', 'REMIND');
    ELSE
      twbkwbis.p_dispinfo('bwskfreg.p_disp_confirm_drops', 'REMIND');
    END IF;

    --
    -- Submit Buttons.
    -- =================================================
    HTP.br;
    HTP.formsubmit('submit_btn',
                   g$_nls.get('BWCKCOM1-0145', 'SQL', 'Drop'));
    HTP.formsubmit('submit_btn',
                   g$_nls.get('BWCKCOM1-0146', 'SQL', 'Do not drop'));
    HTP.formclose;
    HTP.br;

    twbkwbis.p_closedoc(curr_release);
  END p_disp_confirm_drops;

  --
  -- This procedure interprets the choice made by the user on whether to
  -- reject or accept the 'problem list' drops.
  -- =====================================================================
  PROCEDURE p_proc_confirm_drops(term_in        IN OWA_UTIL.ident_arr,
                                 err_term_in    IN OWA_UTIL.ident_arr,
                                 err_crn_in     IN OWA_UTIL.ident_arr,
                                 err_subj_in    IN OWA_UTIL.ident_arr,
                                 err_crse_in    IN OWA_UTIL.ident_arr,
                                 err_sec_in     IN OWA_UTIL.ident_arr,
                                 err_code_in    IN OWA_UTIL.ident_arr,
                                 err_levl_in    IN OWA_UTIL.ident_arr,
                                 err_cred_in    IN OWA_UTIL.ident_arr,
                                 err_gmod_in    IN OWA_UTIL.ident_arr,
                                 d_p_term_code  IN OWA_UTIL.ident_arr,
                                 d_p_crn        IN OWA_UTIL.ident_arr,
                                 d_p_subj       IN OWA_UTIL.ident_arr,
                                 d_p_crse       IN OWA_UTIL.ident_arr,
                                 d_p_sec        IN OWA_UTIL.ident_arr,
                                 d_p_ptrm_code  IN OWA_UTIL.ident_arr,
                                 d_p_rmsg_cde   IN OWA_UTIL.ident_arr,
                                 d_p_message    IN OWA_UTIL.ident_arr,
                                 d_p_start_date IN OWA_UTIL.ident_arr,
                                 d_p_comp_date  IN OWA_UTIL.ident_arr,
                                 d_p_rsts_date  IN OWA_UTIL.ident_arr,
                                 d_p_dunt_code  IN OWA_UTIL.ident_arr,
                                 d_p_drop_code  IN OWA_UTIL.ident_arr,
                                 d_p_drop_conn  IN bwckcoms.varchar2_tabtype,
                                 d_f_term_code  IN OWA_UTIL.ident_arr,
                                 d_f_crn        IN OWA_UTIL.ident_arr,
                                 d_f_subj       IN OWA_UTIL.ident_arr,
                                 d_f_crse       IN OWA_UTIL.ident_arr,
                                 d_f_sec        IN OWA_UTIL.ident_arr,
                                 d_f_ptrm_code  IN OWA_UTIL.ident_arr,
                                 d_f_rmsg_cde   IN OWA_UTIL.ident_arr,
                                 d_f_message    IN OWA_UTIL.ident_arr,
                                 d_f_start_date IN OWA_UTIL.ident_arr,
                                 d_f_comp_date  IN OWA_UTIL.ident_arr,
                                 d_f_rsts_date  IN OWA_UTIL.ident_arr,
                                 d_f_dunt_code  IN OWA_UTIL.ident_arr,
                                 d_f_drop_code  IN OWA_UTIL.ident_arr,
                                 d_f_drop_conn  IN bwckcoms.varchar2_tabtype,
                                 submit_btn     IN VARCHAR2) IS
    drop_problems  sfkcurs.drop_problems_rec_tabtype;
    drop_failures  sfkcurs.drop_problems_rec_tabtype;
    last_term      stvterm.stvterm_code%type := 'dummy';
    lv_submit_btn1 VARCHAR(20);
  BEGIN
    --
    -- Initialize genpidm based on faculty/student.
    -- =================================================

    /*    IF NOT twbkwbis.f_validuser(global_pidm) THEN
      RETURN;
    END IF;*/

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                           '999999999');
    ELSE
      genpidm := global_pidm;
    END IF;

    --
    -- Regenerate the problems and failures list arrays from the repeating
    -- html form variables created on the display page.
    -- =====================================================================
    IF d_p_term_code(1) <> 'dummy' THEN
      FOR i IN 1 .. d_p_term_code.COUNT LOOP

        -- In order to prevent students saving the drop confirm page while
        -- registration is open, and re-using the saved page after it has closed
        -- =====================================================================
        IF last_term <> d_p_term_code(i) THEN
          IF NOT bwskflib.f_validterm(d_p_term_code(i),
                                      stvterm_rec,
                                      sorrtrm_rec) THEN
            twbkwbis.p_dispinfo('bwskflib.P_SelDefTerm', 'BADTERM');
            RETURN;
          END IF;
        END IF;
        last_term := d_p_term_code(i);

        drop_problems(i).term_code := d_p_term_code(i);
        drop_problems(i).crn := d_p_crn(i);
        drop_problems(i).subj := d_p_subj(i);
        drop_problems(i).crse := d_p_crse(i);
        drop_problems(i).sec := d_p_sec(i);
        drop_problems(i).ptrm_code := d_p_ptrm_code(i);
        drop_problems(i).rmsg_cde := d_p_rmsg_cde(i);
        drop_problems(i).message := d_p_message(i);
        drop_problems(i).start_date := d_p_start_date(i);
        drop_problems(i).comp_date := d_p_comp_date(i);
        drop_problems(i).rsts_date := d_p_rsts_date(i);
        drop_problems(i).dunt_code := d_p_dunt_code(i);
        drop_problems(i).drop_code := d_p_drop_code(i);
        drop_problems(i).connected_crns := d_p_drop_conn(i);
      END LOOP;
    END IF;

    IF d_f_term_code(1) <> 'dummy' THEN
      FOR i IN 1 .. d_f_term_code.COUNT LOOP
        drop_failures(i).term_code := d_f_term_code(i);
        drop_failures(i).crn := d_f_crn(i);
        drop_failures(i).subj := d_f_subj(i);
        drop_failures(i).crse := d_f_crse(i);
        drop_failures(i).sec := d_f_sec(i);
        drop_failures(i).ptrm_code := d_f_ptrm_code(i);
        drop_failures(i).rmsg_cde := d_f_rmsg_cde(i);
        drop_failures(i).message := d_f_message(i);
        drop_failures(i).start_date := d_f_start_date(i);
        drop_failures(i).comp_date := d_f_comp_date(i);
        drop_failures(i).rsts_date := d_f_rsts_date(i);
        drop_failures(i).dunt_code := d_f_dunt_code(i);
        drop_failures(i).drop_code := d_f_drop_code(i);
        drop_failures(i).connected_crns := d_f_drop_conn(i);
      END LOOP;
    END IF;

    --
    -- Failures list courses changes are always rejected.
    -- ======================================================================
    bwcklibs.p_reset_failures(genpidm, drop_failures);

    --
    -- User accepts problem list drops - so drop them, and then transfer all
    -- changes in SFTREGS to SFRSTCR.
    -- =====================================================================
    lv_submit_btn1 := g$_nls.get('BWCKCOM1-0147', 'SQL', 'Drop');
    IF submit_btn = lv_submit_btn1 THEN
      bwcklibs.p_confirm_drops(genpidm, term_in, drop_problems);
      bwckregs.p_final_updates(term_in,
                               err_term_in,
                               err_crn_in,
                               err_subj_in,
                               err_crse_in,
                               err_sec_in,
                               err_code_in,
                               err_levl_in,
                               err_cred_in,
                               err_gmod_in,
                               'DROPCONFIRMED',
                               drop_problems,
                               drop_failures);
      RETURN;
    ELSE
      --
      -- User rejects problem list drops - so cancel ALL drops related to
      -- them, and then transfer all remaining changes in SFTREGS to SFRSTCR.
      -- =====================================================================
      bwcklibs.p_reset_drops(genpidm, drop_problems);
      bwckregs.p_final_updates(term_in,
                               err_term_in,
                               err_crn_in,
                               err_subj_in,
                               err_crse_in,
                               err_sec_in,
                               err_code_in,
                               err_levl_in,
                               err_cred_in,
                               err_gmod_in,
                               'DROPREJECTED',
                               drop_problems,
                               drop_failures);
      RETURN;
    END IF;
  END p_proc_confirm_drops;

  --
  -- This procedure opens the html display table and generates the column
  -- headings, for the problems and/or failures list to be shown when the
  -- add/drop page is redisplayed.
  -- =====================================================================
  PROCEDURE p_drop_problems_table_open(multi_term_in IN BOOLEAN DEFAULT FALSE) IS
  BEGIN

    twbkfrmt.p_printmessage(g$_nls.get('BWCKCOM1-0148',
                                       'SQL',
                                       'Registration Update Errors'),
                            'ERROR');

    twbkfrmt.p_tableopen('DATADISPLAY',
                         cattributes => 'SUMMARY="' ||
                                        g$_nls.get('BWCKCOM1-0149',
                                                   'SQL',
                                                   'This layout table is used to present Drop or Withdrawal Errors') || '."');
    twbkfrmt.p_tablerowopen;

    IF multi_term_in THEN
      twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0150',
                                            'SQL',
                                            'Associated Term'));
    END IF;

    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                    g$_nls.get('BWCKCOM1-0151',
                                                               'SQL',
                                                               'Course Reference Number') || '">' ||
                                                    g$_nls.get('BWCKCOM1-0152',
                                                               'SQL',
                                                               'CRN') ||
                                                    '</ACRONYM>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0153',
                                                               'SQL',
                                                               'Subject') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0154',
                                                               'SQL',
                                                               'Subj') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0155',
                                                               'SQL',
                                                               'Course') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0156',
                                                               'SQL',
                                                               'Crse') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                    g$_nls.get('BWCKCOM1-0157',
                                                               'SQL',
                                                               'Section') || '>' ||
                                                    g$_nls.get('BWCKCOM1-0158',
                                                               'SQL',
                                                               'Sec') ||
                                                    '</ABBR>'));
    twbkfrmt.p_tabledataheader(g$_nls.get('BWCKCOM1-0159', 'SQL', 'Status'));
    twbkfrmt.p_tablerowclose;
  END p_drop_problems_table_open;

  --
  -- This procedure displays the problems list - shown when the add/drop
  -- page is redisplayed.
  -- =====================================================================
  PROCEDURE p_drop_problems_list(drop_problems_in  IN sfkcurs.drop_problems_rec_tabtype,
                                 multi_term_in     IN BOOLEAN DEFAULT FALSE,
                                 table_open_in_out IN OUT BOOLEAN) IS
  BEGIN
    IF drop_problems_in.EXISTS(1) THEN
      FOR i IN 1 .. drop_problems_in.COUNT LOOP
        IF i = 1 AND NOT table_open_in_out THEN
          bwckcoms.p_drop_problems_table_open(multi_term_in);
          table_open_in_out := TRUE;
        END IF;

        twbkfrmt.p_tablerowopen;

        IF multi_term_in THEN
          twbkfrmt.p_tabledata(bwcklibs.f_term_desc(drop_problems_in(i)
                                                    .term_code));
        END IF;

        twbkfrmt.p_tabledata(drop_problems_in(i).crn);
        twbkfrmt.p_tabledata(drop_problems_in(i).subj);
        twbkfrmt.p_tabledata(drop_problems_in(i).crse);
        twbkfrmt.p_tabledata(drop_problems_in(i).sec);
        twbkfrmt.p_tabledata(bwcklibs.f_connected_crn_message(drop_problems_in(i)
                                                              .rmsg_cde,
                                                              drop_problems_in(i)
                                                              .message,
                                                              drop_problems_in(i)
                                                              .connected_crns));
        twbkfrmt.p_tablerowclose;
      END LOOP;
    END IF;
  END p_drop_problems_list;

  --
  -- This procedure displays the failures list - shown when the add/drop
  -- page is redisplayed.
  -- =====================================================================
  PROCEDURE p_drop_failures_list(drop_failures_in  IN sfkcurs.drop_problems_rec_tabtype,
                                 multi_term_in     IN BOOLEAN DEFAULT FALSE,
                                 table_open_in_out IN OUT BOOLEAN) IS
  BEGIN
    IF drop_failures_in.EXISTS(1) THEN
      FOR i IN 1 .. drop_failures_in.COUNT LOOP
        IF i = 1 AND NOT table_open_in_out THEN
          bwckcoms.p_drop_problems_table_open(multi_term_in);
          table_open_in_out := TRUE;
        END IF;

        twbkfrmt.p_tablerowopen;

        IF multi_term_in THEN
          twbkfrmt.p_tabledata(bwcklibs.f_term_desc(drop_failures_in(i)
                                                    .term_code));
        END IF;

        twbkfrmt.p_tabledata(drop_failures_in(i).crn);
        twbkfrmt.p_tabledata(drop_failures_in(i).subj);
        twbkfrmt.p_tabledata(drop_failures_in(i).crse);
        twbkfrmt.p_tabledata(drop_failures_in(i).sec);
        twbkfrmt.p_tabledata(bwcklibs.f_connected_crn_message(drop_failures_in(i)
                                                              .rmsg_cde,
                                                              drop_failures_in(i)
                                                              .message,
                                                              drop_failures_in(i)
                                                              .connected_crns));
        twbkfrmt.p_tablerowclose;
      END LOOP;
    END IF;
  END p_drop_failures_list;
  procedure p_intglobal(p_pidm number /*,
                                                               p_term stvterm.stvterm_code%type*/) is

  begin
    global_pidm := p_pidm;
    /* term:=p_term;*/
  end;
BEGIN
  /* initialization starts here */
  NULL;
END ME_bwckcoms;
/

