PROMPT CREATE OR REPLACE VIEW mobedu.contact_vw
CREATE OR REPLACE FORCE VIEW mobedu.contact_vw (
  grp,
  pidm,
  type,
  address,
  pri
) AS
select 'EMAIL' GRP,
       G.GOREMAL_PIDM PIDM,
       GT.GTVEMAL_DESC TYPE,
       G.GOREMAL_EMAIL_ADDRESS ADDRESS,
       G.GOREMAL_PREFERRED_IND PRI
  FROM GOREMAL G
  JOIN GTVEMAL GT
    ON GT.GTVEMAL_CODE = G.GOREMAL_EMAL_CODE
   AND (G.GOREMAL_STATUS_IND = 'A' OR G.GOREMAL_STATUS_IND IS NULL)
UNION ALL
SELECT 'PHONE' GRP,
       T.SPRTELE_PIDM PIDM,
       ST.STVTELE_DESC TYPE,
       T.SPRTELE_PHONE_AREA || T.SPRTELE_PHONE_NUMBER ||
       T.SPRTELE_PHONE_EXT ADDRESS,
       T.SPRTELE_PRIMARY_IND PRI
  FROM SPRTELE T
  JOIN STVTELE ST
    ON ST.STVTELE_CODE = T.SPRTELE_TELE_CODE
   AND (T.SPRTELE_STATUS_IND = 'A' OR T.SPRTELE_STATUS_IND IS NULL)
   and t.sprtele_seqno =
       (select max(pr.sprtele_seqno)
          from sprtele pr
         where pr.sprtele_pidm = t.sprtele_pidm
           and pr.sprtele_tele_code = t.sprtele_tele_code)
UNION ALL
SELECT 'ADDRESS' GRP,
       D.SPRADDR_PIDM PIDM,
       DT.STVATYP_DESC TYPE,
       D.SPRADDR_STREET_LINE1 || ';' || D.SPRADDR_STREET_LINE2 || ';' ||
       D.SPRADDR_STREET_LINE3 || ';' || D.SPRADDR_CITY || ';' ||
       z_cm_mobile_campus.fz_get_state(D.SPRADDR_STAT_CODE) || '; ZIP ' ||
       D.SPRADDR_ZIP ADDRESS,
       '' PRI
  FROM SPRADDR D
  JOIN STVATYP DT
    ON DT.STVATYP_CODE = D.SPRADDR_ATYP_CODE
   and NVL(D.SPRADDR_STATUS_IND, 'A') = 'A'
   and d.spraddr_seqno =
       (select max(ad.spraddr_seqno)
          from spraddr ad
         where ad.spraddr_pidm = d.spraddr_pidm
           and ad.spraddr_atyp_code = ad.spraddr_atyp_code)
/

