PROMPT CREATE OR REPLACE VIEW mobedu.acct_summ_spsu
CREATE OR REPLACE FORCE VIEW mobedu.acct_summ_spsu (
  student_id,
  category,
  description,
  charge,
  payment
) AS
select p.spriden_id student_id,b.tbbdetc_detail_code category,b.tbbdetc_desc description,sum(t.tbraccd_amount) charge,0 payment from tbraccd t join tbbdetc b
on t.tbraccd_detail_code=b.tbbdetc_detail_code
and b.tbbdetc_type_ind='C'
join spriden p
on t.tbraccd_pidm=p.spriden_pidm
and p.spriden_change_ind is null
group by p.spriden_id,b.tbbdetc_detail_code,b.tbbdetc_desc
union
select p.spriden_id student_id,b.tbbdetc_detail_code category,b.tbbdetc_desc description,0 charge,sum(t.tbraccd_amount) payment from tbraccd t join tbbdetc b
on t.tbraccd_detail_code=b.tbbdetc_detail_code
and b.tbbdetc_type_ind='P'
join spriden p
on t.tbraccd_pidm=p.spriden_pidm
and p.spriden_change_ind is null
group by p.spriden_id,b.tbbdetc_detail_code,b.tbbdetc_desc
/

