PROMPT CREATE OR REPLACE VIEW mobedu.charge_pay_detail_spsu
CREATE OR REPLACE FORCE VIEW mobedu.charge_pay_detail_spsu (
  student_id,
  category,
  description,
  term_code,
  tbbdetc_type_ind,
  amount,
  balance,
  bill_date,
  due_date,
  term_desc
) AS
SELECT spriden_id          student_id,
       a.tbbdetc_detail_code      category,
       a.tbbdetc_desc      description,
       b.tbraccd_term_code term_code,
       a.tbbdetc_type_ind,
       b.tbraccd_amount    amount,
       b.tbraccd_balance   balance,
       b.tbraccd_bill_date bill_date,
       b.tbraccd_due_date  due_date,
       t.stvterm_desc      term_desc
  FROM tbbdetc a
  JOIN tbraccd b
    ON tbbdetc_detail_code = tbraccd_detail_code
  join spriden c
    on tbraccd_pidm = spriden_pidm
   and spriden_change_ind is null
   left join stvterm t
   on b.tbraccd_term_code=t.stvterm_code
/

