PROMPT CREATE OR REPLACE VIEW mobedu.vw_prevterms_charges
CREATE OR REPLACE FORCE VIEW mobedu.vw_prevterms_charges (
  pidm,
  term_code,
  calculated_balance,
  detail_code,
  amount_description,
  type_ind,
  charge_amount,
  payment_amount,
  student_id
) AS
SELECT tbraccd_pidm pidm , TBRACCD_TERM_CODE TERM_CODE ,
 SUM(DECODE( tbbdetc_type_ind ,
                 'C',tbraccd_amount,
                     tbraccd_amount * -1)) over(partition by tbraccd_pidm , tbraccd_term_code) Calculated_Balance
 , tbraccd_detail_code detail_code,
 tbbdetc_desc Amount_Description ,
 tbbdetc_type_ind type_ind ,
decode(tbbdetc_type_ind,'C',tbraccd_amount , 0) Charge_Amount ,
decode(tbbdetc_type_ind,'P',tbraccd_amount,0) Payment_Amount ,
 spriden_id student_id
        FROM TBRACCD JOIN
              TBBDETC ON
              TBRACCD_DETAIL_CODE = TBBDETC_DETAIL_CODE
              JOIN SPRIDEN
              ON TBRACCD_PIDM = SPRIDEN_PIDM
              AND SPRIDEN_CHANGE_IND IS NULL
/

