PROMPT CREATE TABLE mobedu_trash
CREATE TABLE mobedu_trash (
  student_id   VARCHAR2(10 BYTE)   NULL,
  student_pidm NUMBER(8,0)         NULL,
  term_code    VARCHAR2(6 BYTE)    NULL,
  crn          NUMBER              NULL,
  status       VARCHAR2(100 BYTE)  NULL,
  error_msg    VARCHAR2(4000 BYTE) NULL,
  rsts_code    VARCHAR2(10 BYTE)   NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


