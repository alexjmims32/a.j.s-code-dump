PROMPT CREATE TABLE feedback_data
CREATE TABLE feedback_data (
  areaid       NUMBER(3,0)         NULL,
  courseid     VARCHAR2(10 BYTE)   NULL,
  comments1    VARCHAR2(4000 BYTE) NULL,
  comments2    VARCHAR2(4000 BYTE) NULL,
  confidential CHAR(1 BYTE)        NULL,
  sender_name  VARCHAR2(100 BYTE)  NULL,
  sender_email VARCHAR2(100 BYTE)  NULL,
  created_date TIMESTAMP(6)        NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


