PROMPT ALTER TABLE noticelog ADD CONSTRAINT mobedu_notice_log_notice_id_fk FOREIGN KEY
ALTER TABLE noticelog
  ADD CONSTRAINT mobedu_notice_log_notice_id_fk FOREIGN KEY (
    noticeid
  ) REFERENCES notice (
    id
  ) ON DELETE CASCADE
  DISABLE
  NOVALIDATE
/

