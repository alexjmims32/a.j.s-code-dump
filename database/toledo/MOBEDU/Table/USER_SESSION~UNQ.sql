PROMPT ALTER TABLE user_session ADD UNIQUE
ALTER TABLE user_session
  ADD UNIQUE (
    username
  )
  USING INDEX
    STORAGE (
      NEXT       1024 K
    )
/

