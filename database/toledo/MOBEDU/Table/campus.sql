PROMPT CREATE TABLE campus
CREATE TABLE campus (
  code          VARCHAR2(4000 BYTE) NULL,
  title         VARCHAR2(4000 BYTE) NULL,
  description   VARCHAR2(4000 BYTE) NULL,
  client        VARCHAR2(100 BYTE)  NULL,
  principalname VARCHAR2(100 BYTE)  NULL,
  website       VARCHAR2(4000 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


