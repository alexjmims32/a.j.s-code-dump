PROMPT CREATE TABLE stu_housing_info
CREATE TABLE stu_housing_info (
  fullname           VARCHAR2(100 BYTE) NOT NULL,
  classof            VARCHAR2(100 BYTE) NULL,
  studenid           VARCHAR2(50 BYTE)  NOT NULL,
  gender             VARCHAR2(10 BYTE)  NULL,
  birthdate          DATE               NULL,
  address            VARCHAR2(500 BYTE) NULL,
  city               VARCHAR2(50 BYTE)  NULL,
  state              VARCHAR2(50 BYTE)  NULL,
  pincode            VARCHAR2(10 BYTE)  NULL,
  phone              VARCHAR2(20 BYTE)  NULL,
  email              VARCHAR2(50 BYTE)  NULL,
  beginterm          VARCHAR2(50 BYTE)  NULL,
  beginyear          VARCHAR2(50 BYTE)  NULL,
  applicationtype    VARCHAR2(50 BYTE)  NULL,
  hallsizepreference VARCHAR2(50 BYTE)  NULL,
  floorpreference    VARCHAR2(50 BYTE)  NULL,
  issmoker           VARCHAR2(50 BYTE)  NULL,
  livewithsmoker     VARCHAR2(50 BYTE)  NULL,
  studyprefer        VARCHAR2(50 BYTE)  NULL,
  wakeuppref         VARCHAR2(50 BYTE)  NULL,
  roompref           VARCHAR2(50 BYTE)  NULL,
  names              VARCHAR2(50 BYTE)  NULL,
  specialneeds       VARCHAR2(100 BYTE) NULL,
  mealplan           VARCHAR2(100 BYTE) NULL,
  comments           VARCHAR2(500 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


