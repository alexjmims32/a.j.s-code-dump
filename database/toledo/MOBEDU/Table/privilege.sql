PROMPT CREATE TABLE privilege
CREATE TABLE privilege (
  roleid         VARCHAR2(100 BYTE) NOT NULL,
  modulecode     VARCHAR2(100 BYTE) NOT NULL,
  accessflag     VARCHAR2(1 BYTE)   NULL,
  authrequired   CHAR(1 BYTE)       DEFAULT 'Y' NULL,
  version_no     NUMBER(5,0)        NULL,
  lastmodifiedby VARCHAR2(40 BYTE)  NULL,
  lastmodifiedon TIMESTAMP(6)       NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


