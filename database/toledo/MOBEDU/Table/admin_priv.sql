PROMPT CREATE TABLE admin_priv
CREATE TABLE admin_priv (
  username    VARCHAR2(20 BYTE)  NOT NULL,
  privcode    VARCHAR2(20 BYTE)  NOT NULL,
  accessflag  CHAR(1 BYTE)       DEFAULT 'Y' NOT NULL,
  description VARCHAR2(100 BYTE) NOT NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


