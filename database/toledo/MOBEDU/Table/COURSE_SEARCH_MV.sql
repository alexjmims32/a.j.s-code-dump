PROMPT CREATE TABLE course_search_mv
CREATE TABLE course_search_mv (
  crn             VARCHAR2(5)    NOT NULL,
  search_string   VARCHAR2(4000) NULL,
  term_code       VARCHAR2(6)    NOT NULL,
  part_of_term    VARCHAR2(3)    NULL,
  course_number   VARCHAR2(5)    NOT NULL,
  subject         VARCHAR2(4)    NOT NULL,
  subject_desc    VARCHAR2(30)   NULL,
  course_title    VARCHAR2(30)   NULL,
  credit_hrs      NUMBER(7,3)    NULL,
  tot_credit_hrs  NUMBER(9,3)    NULL,
  bill_hours      NUMBER(7,3)    NULL,
  campus          VARCHAR2(30)   NULL,
  campus_code     VARCHAR2(3)    NOT NULL,
  course_level    CHAR(1)        NULL,
  lecture_hours   NUMBER(7,3)    NULL,
  seats_available VARCHAR2(3)    NULL,
  waitlist_avail  NUMBER(4,0)    NULL,
  grade           VARCHAR2(30)   NULL,
  grade_code      VARCHAR2(1)    NULL,
  department      VARCHAR2(4000) NULL,
  faculty         VARCHAR2(4000) NULL,
  instructor_name VARCHAR2(4000) NULL,
  college         VARCHAR2(4000) NULL,
  term_desc       VARCHAR2(30)   NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


