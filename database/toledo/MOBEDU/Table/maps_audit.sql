PROMPT CREATE TABLE maps_audit
CREATE TABLE maps_audit (
  id             NUMBER              NOT NULL,
  campus_code    VARCHAR2(6 BYTE)    NULL,
  building_code  VARCHAR2(6 BYTE)    NULL,
  building_name  VARCHAR2(100 BYTE)  NULL,
  building_desc  VARCHAR2(2000 BYTE) NULL,
  phone          VARCHAR2(15 BYTE)   NULL,
  email          VARCHAR2(30 BYTE)   NULL,
  img_url        VARCHAR2(300 BYTE)  NULL,
  longitude      VARCHAR2(30 BYTE)   NULL,
  latitude       VARCHAR2(30 BYTE)   NULL,
  version_no     NUMBER(5,0)         NULL,
  lastmodifiedby VARCHAR2(40 BYTE)   NULL,
  lastmodifiedon TIMESTAMP(6)        NULL,
  address        VARCHAR2(4000 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


