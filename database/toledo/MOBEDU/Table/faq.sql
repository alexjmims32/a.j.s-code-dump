PROMPT CREATE TABLE faq
CREATE TABLE faq (
  id             NUMBER              NOT NULL,
  campuscode     VARCHAR2(100 BYTE)  NULL,
  question       VARCHAR2(4000 BYTE) NULL,
  answer         VARCHAR2(4000 BYTE) NULL,
  version_no     NUMBER(5,0)         NULL,
  lastmodifiedby VARCHAR2(40 BYTE)   NULL,
  lastmodifiedon TIMESTAMP(6)        NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


