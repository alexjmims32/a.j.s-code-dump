PROMPT CREATE TABLE admin_user
CREATE TABLE admin_user (
  username  VARCHAR2(20 BYTE)  NOT NULL,
  firstname VARCHAR2(100 BYTE) NOT NULL,
  lastname  VARCHAR2(100 BYTE) NOT NULL,
  password  VARCHAR2(100 BYTE) NOT NULL,
  active    CHAR(1 BYTE)       DEFAULT 'Y' NOT NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


