PROMPT CREATE TABLE notice
CREATE TABLE notice (
  id             NUMBER(11,0)        NOT NULL,
  typeid         NUMBER(11,0)        NOT NULL,
  title          VARCHAR2(50 BYTE)   NOT NULL,
  message        VARCHAR2(4000 BYTE) NOT NULL,
  duedate        DATE                NULL,
  expirydate     DATE                NULL,
  posted         NUMBER(1,0)         DEFAULT 0 NOT NULL,
  lastmodifiedby VARCHAR2(50 BYTE)   NOT NULL,
  lastmodifiedon TIMESTAMP(6)        NOT NULL,
  url            VARCHAR2(100 BYTE)  NULL
)
  STORAGE (
    NEXT       1024 K
  )
/

COMMENT ON COLUMN notice.typeid IS 'refers to ID in noticetypes table';

