PROMPT CREATE TABLE privilege_audit
CREATE TABLE privilege_audit (
  roleid         NUMBER             NOT NULL,
  modulecode     VARCHAR2(100 BYTE) NOT NULL,
  accessflag     VARCHAR2(1 BYTE)   NULL,
  authrequired   CHAR(1 BYTE)       DEFAULT 'Y' NULL,
  lastmodifiedon TIMESTAMP(6)       NULL,
  lastmodifiedby VARCHAR2(40 BYTE)  NULL,
  version_no     NUMBER(5,0)        NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


