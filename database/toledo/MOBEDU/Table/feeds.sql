PROMPT CREATE TABLE feeds
CREATE TABLE feeds (
  id             NUMBER(5,0)        NULL,
  parentid       NUMBER(5,0)        NULL,
  type           VARCHAR2(20 BYTE)  NULL,
  link           VARCHAR2(200 BYTE) NULL,
  format         VARCHAR2(100 BYTE) NULL,
  moduleid       NUMBER(5,0)        NULL,
  name           VARCHAR2(100 BYTE) NULL,
  icon           VARCHAR2(100 BYTE) NULL,
  campuscode     VARCHAR2(6 BYTE)   NULL,
  version_no     NUMBER(5,0)        NULL,
  lastmodifiedby VARCHAR2(40 BYTE)  NULL,
  lastmodifiedon TIMESTAMP(6)       NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


