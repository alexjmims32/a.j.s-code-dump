PROMPT ALTER TABLE ent_users ADD UNIQUE
ALTER TABLE ent_users
  ADD UNIQUE (
    entid
  )
  USING INDEX
    STORAGE (
      NEXT       1024 K
    )
/

