PROMPT ALTER TABLE noticepopulation ADD CONSTRAINT mobedu_notice_pop_pk PRIMARY KEY
ALTER TABLE noticepopulation
  ADD CONSTRAINT mobedu_notice_pop_pk PRIMARY KEY (
    noticeid,
    populationtypeid,
    paramname
  )
  USING INDEX
    STORAGE (
      NEXT       1024 K
    )
/

