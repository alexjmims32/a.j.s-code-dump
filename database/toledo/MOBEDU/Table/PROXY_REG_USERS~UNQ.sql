PROMPT ALTER TABLE proxy_reg_users ADD UNIQUE
ALTER TABLE proxy_reg_users
  ADD UNIQUE (
    username
  )
  USING INDEX
    STORAGE (
      NEXT       1024 K
    )
/

