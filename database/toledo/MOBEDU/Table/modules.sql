PROMPT CREATE TABLE modules
CREATE TABLE modules (
  id          NUMBER             NULL,
  module_code VARCHAR2(10 BYTE)  NULL,
  module_desc VARCHAR2(100 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


