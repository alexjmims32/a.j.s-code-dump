PROMPT CREATE TABLE error_table_temp
CREATE TABLE error_table_temp (
  student_id    VARCHAR2(10)  NULL,
  crn           VARCHAR2(50)  NULL,
  term          NUMBER        NULL,
  error_msg     VARCHAR2(500) NULL,
  activity_date DATE          NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


