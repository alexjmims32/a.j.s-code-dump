PROMPT CREATE TABLE help_audit
CREATE TABLE help_audit (
  campuscode     VARCHAR2(100 BYTE)  NULL,
  abouttext      VARCHAR2(4000 BYTE) NULL,
  faq            VARCHAR2(4000 BYTE) NULL,
  version_no     NUMBER(5,0)         NULL,
  lastmodifiedby VARCHAR2(40 BYTE)   NULL,
  lastmodifiedon TIMESTAMP(6)        NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


