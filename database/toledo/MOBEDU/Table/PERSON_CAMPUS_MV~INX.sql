PROMPT CREATE INDEX category_idx
CREATE INDEX category_idx
  ON person_campus_mv (
    category
  )
  MAXTRANS 167
  STORAGE (
    NEXT       1024 K
  )
/

PROMPT CREATE INDEX fname_idx
CREATE INDEX fname_idx
  ON person_campus_mv (
    first_name
  )
  STORAGE (
    NEXT       1024 K
  )
/

PROMPT CREATE INDEX lname_idx
CREATE INDEX lname_idx
  ON person_campus_mv (
    last_name
  )
  STORAGE (
    NEXT       1024 K
  )
/

