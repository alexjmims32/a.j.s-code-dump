PROMPT CREATE TABLE user_devices
CREATE TABLE user_devices (
  entid    VARCHAR2(100 BYTE)  NOT NULL,
  deviceid VARCHAR2(1000 BYTE) NOT NULL,
  platform VARCHAR2(100 BYTE)  NOT NULL,
  active   VARCHAR2(1 BYTE)    DEFAULT 'Y' NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


