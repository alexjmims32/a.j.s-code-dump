PROMPT ALTER TABLE noticepopulation ADD CONSTRAINT mobedu_notice_id_fk FOREIGN KEY
ALTER TABLE noticepopulation
  ADD CONSTRAINT mobedu_notice_id_fk FOREIGN KEY (
    noticeid
  ) REFERENCES notice (
    id
  ) ON DELETE CASCADE
/

PROMPT ALTER TABLE noticepopulation ADD CONSTRAINT mobedu_pop_type_id_fk FOREIGN KEY
ALTER TABLE noticepopulation
  ADD CONSTRAINT mobedu_pop_type_id_fk FOREIGN KEY (
    populationtypeid
  ) REFERENCES populationtype (
    id
  )
/

