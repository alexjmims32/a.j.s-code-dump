PROMPT CREATE TABLE feedback
CREATE TABLE feedback (
  id          NUMBER(5,0)        NOT NULL,
  title       VARCHAR2(50 BYTE)  NULL,
  email       VARCHAR2(100 BYTE) NULL,
  description VARCHAR2(100 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


