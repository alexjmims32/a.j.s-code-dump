PROMPT ALTER TABLE service_area ADD PRIMARY KEY
ALTER TABLE service_area
  ADD PRIMARY KEY (
    id
  )
  USING INDEX
    STORAGE (
      NEXT       1024 K
    )
/

