PROMPT ALTER TABLE noticelog ADD CONSTRAINT mobedu_notice_log_pk PRIMARY KEY
ALTER TABLE noticelog
  ADD CONSTRAINT mobedu_notice_log_pk PRIMARY KEY (
    id
  )
  USING INDEX
    STORAGE (
      NEXT       1024 K
    )
/

