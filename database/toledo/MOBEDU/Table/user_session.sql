PROMPT CREATE TABLE user_session
CREATE TABLE user_session (
  username     VARCHAR2(100 BYTE) NOT NULL,
  last_request TIMESTAMP(6)       NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


