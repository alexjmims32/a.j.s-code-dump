PROMPT CREATE TABLE admin_role
CREATE TABLE admin_role (
  code        VARCHAR2(20 BYTE)  NOT NULL,
  description VARCHAR2(100 BYTE) NOT NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


