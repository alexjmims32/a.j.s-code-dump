PROMPT CREATE INDEX admin_user_idx
CREATE UNIQUE INDEX admin_user_idx
  ON admin_user (
    username
  )
  STORAGE (
    NEXT       1024 K
  )
/

