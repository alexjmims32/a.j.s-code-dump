PROMPT CREATE TABLE populationtype
CREATE TABLE populationtype (
  id             NUMBER(11,0)        NOT NULL,
  type           VARCHAR2(20 BYTE)   NOT NULL,
  title          VARCHAR2(50 BYTE)   NOT NULL,
  description    VARCHAR2(100 BYTE)  NOT NULL,
  query          VARCHAR2(1000 BYTE) NULL,
  lastmodifiedby VARCHAR2(50 BYTE)   NOT NULL,
  lastmodifiedon TIMESTAMP(6)        NOT NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


