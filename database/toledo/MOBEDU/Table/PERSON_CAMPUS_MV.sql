PROMPT CREATE TABLE person_campus_mv
CREATE TABLE person_campus_mv (
  id          VARCHAR2(9)    NOT NULL,
  first_name  VARCHAR2(60)   NULL,
  last_name   VARCHAR2(60)   NULL,
  middle_name VARCHAR2(60)   NULL,
  full_name   VARCHAR2(121)  NULL,
  category    VARCHAR2(4000) NULL,
  campus      VARCHAR2(4000) NULL,
  college     VARCHAR2(4000) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


