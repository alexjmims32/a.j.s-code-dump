PROMPT CREATE TABLE emergency_contacts
CREATE TABLE emergency_contacts (
  id             NUMBER              NOT NULL,
  campus         VARCHAR2(100 BYTE)  NULL,
  name           VARCHAR2(60 BYTE)   NULL,
  phone          VARCHAR2(13 BYTE)   NULL,
  address        VARCHAR2(50 BYTE)   NULL,
  email          VARCHAR2(30 BYTE)   NULL,
  picture_url    VARCHAR2(150 BYTE)  NULL,
  category       VARCHAR2(34 BYTE)   NULL,
  version_no     NUMBER(5,0)         NULL,
  lastmodifiedby VARCHAR2(40 BYTE)   NULL,
  lastmodifiedon TIMESTAMP(6)        NULL,
  comments       VARCHAR2(1000 BYTE) NULL,
  seq_num        NUMBER              NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


