PROMPT CREATE TABLE noticepopulation
CREATE TABLE noticepopulation (
  noticeid         NUMBER(11,0)        NOT NULL,
  populationtypeid NUMBER(11,0)        NOT NULL,
  paramname        VARCHAR2(100 BYTE)  NOT NULL,
  paramvalue       VARCHAR2(1000 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


