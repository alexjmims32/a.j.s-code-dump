PROMPT CREATE TABLE ent_users
CREATE TABLE ent_users (
  entid     VARCHAR2(100 BYTE) NOT NULL,
  firstname VARCHAR2(100 BYTE) NOT NULL,
  lastname  VARCHAR2(100 BYTE) NULL,
  active    VARCHAR2(1 BYTE)   DEFAULT 'Y' NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


