PROMPT CREATE SEQUENCE noticelog_seq
CREATE SEQUENCE noticelog_seq
  MINVALUE 0
  MAXVALUE 99999999
  INCREMENT BY 1
  START WITH 1321
  NOCYCLE
  NOORDER
  CACHE 20
/

