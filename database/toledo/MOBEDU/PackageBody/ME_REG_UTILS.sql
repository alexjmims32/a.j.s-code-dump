PROMPT CREATE OR REPLACE PACKAGE BODY me_reg_utils
CREATE OR REPLACE PACKAGE BODY me_reg_utils is
  FUNCTION FZ_ME_REG_SEQ(P_CRN       IN SSBSECT.SSBSECT_CRN%TYPE,
                         P_TERM_CODE IN SSBSECT.SSBSECT_TERM_CODE%TYPE)
    RETURN NUMBER IS
    CURSOR C_MAX_SEQ IS
      SELECT MAX(SEC.SSBSECT_REG_ONEUP) MAX_SEQ
        FROM SSBSECT SEC
       WHERE SEC.SSBSECT_CRN = P_CRN
         AND SEC.SSBSECT_TERM_CODE = P_TERM_CODE;
    SEQ NUMBER;

  BEGIN
    OPEN C_MAX_SEQ;
    FETCH C_MAX_SEQ
      INTO SEQ;
    CLOSE C_MAX_SEQ;
    SEQ := SEQ + 1;
    RETURN SEQ;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  end FZ_ME_REG_SEQ;
  FUNCTION FZ_ME_SORT_SEQ(P_CRN       IN SSBSECT.SSBSECT_CRN%TYPE,
                          P_TERM_CODE IN SSBSECT.SSBSECT_TERM_CODE%TYPE)
    RETURN NUMBER IS

    SEQ NUMBER;

  BEGIN
    SELECT NVL(MAX(SFRSTCR_CLASS_SORT_KEY), 0)
      INTO SEQ
      FROM SFRSTCR
     WHERE SFRSTCR_CRN = P_CRN
       AND SFRSTCR_TERM_CODE = P_TERM_CODE;

    SEQ := SEQ + 1;
    RETURN SEQ;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  end FZ_ME_SORT_SEQ;
  function FZ_ADDRESS(PIDM  in SATURN.SPRIDEN.SPRIDEN_PIDM%type,
                      FIELD varchar2) return varchar2 as
    result       varchar2(32767);
    V_ENTITY_IND SATURN.SPRIDEN.SPRIDEN_ENTITY_IND%type;
  begin
    select SPRIDEN_ENTITY_IND
      into V_ENTITY_IND
      from SPRIDEN
     where SPRIDEN_CHANGE_IND is null
       and SPRIDEN_PIDM = PIDM;
    if V_ENTITY_IND = 'P' then
      select case
               when FIELD = 'STREET1' then
                (select a.spraddr_street_line1

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
               when FIELD = 'STREET2' then
                (select a.spraddr_street_line2

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
               when FIELD = 'CITY' then
                (select a.spraddr_city

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
               when FIELD = 'STATE' then
                (select a.spraddr_stat_code

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
               when FIELD = 'COUNTY' then
                (select a.spraddr_cnty_code

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
               when FIELD = 'NATION' then
                (select a.spraddr_natn_code

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
               when FIELD = 'ZIP' then
                (select a.spraddr_zip

                   from spraddr a
                  where a.spraddr_pidm = PIDM
                    and a.spraddr_atyp_code = 'MA'
                    and a.spraddr_seqno =
                        (SELECT MAX(a1.spraddr_seqno)
                           FROM spraddr a1
                          WHERE a1.spraddr_pidm = a.spraddr_pidm
                            AND a1.spraddr_atyp_code = a.spraddr_atyp_code
                            AND nvl(a1.spraddr_to_date, SYSDATE + 1) > SYSDATE
                            AND nvl(a1.spraddr_status_ind, 'A') = 'A'))
             end
        into result
        from DUAL;

    end if;
    return result;
  end FZ_ADDRESS;
  function FZ_PHONE(PIDM  in SATURN.SPRIDEN.SPRIDEN_PIDM%type,
                    FIELD in varchar2) return varchar2 as
    result varchar2(32767);
  begin
    select case
             when FIELD = 'AREA_CODE' then
              (SELECT *
                 FROM (select Z.SPRTELE_PHONE_AREA

                         from sprtele z
                        where z.sprtele_tele_code = 'MA'
                          and z.sprtele_pidm = PIDM
                          and Z.sprtele_primary_ind = 'Y'
                          and Z.sprtele_status_ind is null) A
                WHERE ROWNUM < 2)
             when FIELD = 'PHONE' then
              (SELECT *
                 FROM (select Z.SPRTELE_PHONE_NUMBER

                         from sprtele z
                        where z.sprtele_tele_code = 'MA'
                          and z.sprtele_pidm = PIDM
                          and Z.sprtele_primary_ind = 'Y'
                          and Z.sprtele_status_ind is null)
                WHERE ROWNUM < 2)
             when FIELD = 'EXT' then
              (SELECT *
                 FROM (select Z.SPRTELE_PHONE_EXT

                         from sprtele z
                        where z.sprtele_tele_code = 'MA'
                          and z.sprtele_pidm = PIDM
                          and Z.sprtele_primary_ind = 'Y'
                          and Z.sprtele_status_ind is null)
                WHERE ROWNUM < 2)
           end
      into result
      from DUAL;
    return result;
  end FZ_PHONE;
  FUNCTION FZ_GET_EMAIL_ADDRESS(P_PIDM NUMBER) RETURN VARCHAR2 IS
    V_COUNT      NUMBER(2);
    V_EMAIL      VARCHAR2(180);
    V_EMAIL_CODE VARCHAR2(4);
  BEGIN
    SELECT COUNT(*)
      INTO V_COUNT
      FROM GOREMAL
     WHERE GOREMAL_PIDM = P_PIDM
       AND GOREMAL_STATUS_IND = 'A'
       AND GOREMAL_PREFERRED_IND = 'Y';
    IF V_COUNT = 1 THEN
      SELECT GOREMAL_EMAIL_ADDRESS
        INTO V_EMAIL
        FROM GOREMAL
       WHERE GOREMAL_PIDM = P_PIDM
         AND GOREMAL_STATUS_IND = 'A'
         AND GOREMAL_PREFERRED_IND = 'Y';
      RETURN V_EMAIL;
    ELSIF V_COUNT = 0 THEN
      RETURN NULL;
    ELSE
      SELECT GTVSDAX_EXTERNAL_CODE
        INTO V_EMAIL_CODE
        FROM GTVSDAX
       WHERE GTVSDAX_INTERNAL_CODE_SEQNO =
             (SELECT MIN(GTVSDAX_INTERNAL_CODE_SEQNO)
                FROM GTVSDAX, GOREMAL
               WHERE GOREMAL_PIDM = P_PIDM
                 AND GOREMAL_STATUS_IND = 'A'
                 AND GOREMAL_PREFERRED_IND = 'Y'
                 AND GTVSDAX_EXTERNAL_CODE = GOREMAL_EMAL_CODE
                 AND GTVSDAX_INTERNAL_CODE_GROUP = 'EMAIL'
                 AND GTVSDAX_INTERNAL_CODE = 'GENEMAIL')
         AND GTVSDAX_INTERNAL_CODE_GROUP = 'EMAIL'
         AND GTVSDAX_INTERNAL_CODE = 'GENEMAIL';

      FOR I IN (SELECT GOREMAL_EMAIL_ADDRESS
                  INTO V_EMAIL
                  FROM GOREMAL
                 WHERE GOREMAL_PIDM = P_PIDM
                   AND GOREMAL_STATUS_IND = 'A'
                   AND GOREMAL_PREFERRED_IND = 'Y'
                   AND GOREMAL_EMAL_CODE = V_EMAIL_CODE) LOOP
        RETURN I.GOREMAL_EMAIL_ADDRESS;
      END LOOP;
    END IF;

  END;
  FUNCTION FZ_FLAG_DESC(P_FLAG VARCHAR2) RETURN VARCHAR2 AS

  BEGIN
    CASE P_FLAG
      WHEN 'F' THEN
        RETURN 'Fatal Error';
      WHEN 'D' THEN
        RETURN 'Do not count in enrollment';
      WHEN 'L' THEN
        RETURN 'WaitListed';
      WHEN 'O' THEN
        RETURN 'Override';
      WHEN 'W' THEN
        RETURN 'Warning';
      WHEN 'X' THEN
        RETURN 'Delete';
      ELSE
        RETURN NULL;
    END CASE;
    RETURN NULL;
  END;

  /*FUNCTION FZ_GET_MAX_APPL_NO(P_PIDM NUMBER) RETURN NUMBER IS
    V_APPL_NO NUMBER;
  BEGIN
    SELECT MAX(SARADAP_APPL_NO) INTO V_APPL_NO FROM SARADAP WHERE SARADAP_PIDM=P_PIDM;
    RETURN V_APPL_NO;
    EXCEPTION WHEN OTHERS THEN
      RETURN V_APPL_NO;
  END FZ_GET_MAX_APPL_NO;*/
  FUNCTION FZ_GET_CAMP(P_PIDM NUMBER) RETURN VARCHAR2 IS
    V_EMP NUMBER;

    V_CAMP VARCHAR2(100);
  BEGIN
    /*BEGIN
      SELECT B.STVCAMP_DESC  INTO V_CAMP FROM PEBEMPL A LEFT JOIN STVCAMP B ON A.PEBEMPL_CAMP_CODE=B.STVCAMP_CODE
       AND A.PEBEMPL_PIDM=P_PIDM AND A.PEBEMPL_EMPL_STATUS<>'T' ;
       RETURN V_CAMP;
    EXCEPTION WHEN OTHERS THEN
      V_CAMP:=NULL;
    END;*/

    BEGIN
      FOR I IN (SELECT C.STVCAMP_DESC
                  FROM SGBSTDN B
                  JOIN STVCAMP C
                    ON B.SGBSTDN_CAMP_CODE = C.STVCAMP_CODE
                   AND B.SGBSTDN_PIDM = P_PIDM
                 ORDER BY B.SGBSTDN_ACTIVITY_DATE) LOOP
        V_CAMP := I.STVCAMP_DESC;
        RETURN V_CAMP;
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        V_CAMP := NULL;
    END;

    RETURN NULL;

  END;

  FUNCTION FZ_GET_INSTRUCTOR(P_TERM_CODE VARCHAR2, P_CRN NUMBER)
    RETURN VARCHAR2 IS
    V_INST VARCHAR2(200);
  BEGIN
    SELECT B.SPRIDEN_LAST_NAME || ' ' || B.SPRIDEN_FIRST_NAME
      INTO V_INST
      FROM SIRASGN A
      JOIN SPRIDEN B
        ON A.SIRASGN_PIDM = B.SPRIDEN_PIDM
       AND B.SPRIDEN_CHANGE_IND IS NULL
       AND A.SIRASGN_TERM_CODE = P_TERM_CODE
       AND A.SIRASGN_CRN = P_CRN
       AND NVL(A.SIRASGN_PRIMARY_IND, 'N') = 'Y';
    RETURN V_INST;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END FZ_GET_INSTRUCTOR;

  FUNCTION FZ_GET_COURSE_LEVEL(P_CRSE_NUMB VARCHAR2,
                               P_TERM      VARCHAR2,
                               P_SUBJ_CODE VARCHAR2) RETURN VARCHAR2 IS

    V_RES VARCHAR2(1000) := NULL;

    CURSOR GET_LEVL IS
      SELECT B.STVLEVL_DESC
        FROM SCRLEVL A
        JOIN STVLEVL B
          ON A.SCRLEVL_LEVL_CODE = B.STVLEVL_CODE
         AND A.SCRLEVL_CRSE_NUMB = P_CRSE_NUMB
         AND A.SCRLEVL_SUBJ_CODE = P_SUBJ_CODE
         AND A.SCRLEVL_EFF_TERM <= P_TERM;

  BEGIN

    FOR R_LEVL IN GET_LEVL LOOP
      -- DBMS_OUTPUT.PUT_LINE('L_COUNTER:' || L_COUNTER);
      --DBMS_OUTPUT.PUT_LINE('ROWNUM:' || R_LEVL.ROWNUM);
      IF V_RES IS NOT NULL THEN

        V_RES := V_RES || ',' || R_LEVL.STVLEVL_DESC;
      ELSE

        V_RES := V_RES || R_LEVL.STVLEVL_DESC;

      END IF;

    END LOOP;
    RETURN V_RES;
  EXCEPTION
    WHEN OTHERS THEN
      V_RES := NULL;
      RETURN V_RES;
  END;

  FUNCTION FZ_GET_EMP_EMAIL(P_PIDM VARCHAR2) RETURN VARCHAR2 AS
    V_EMAIL VARCHAR2(200);

  BEGIN
    V_EMAIL := FZ_GET_EMAIL_ADDRESS(P_PIDM);

    RETURN V_EMAIL;

  EXCEPTION
    WHEN OTHERS THEN
      V_EMAIL := NULL;

      RETURN V_EMAIL;
  END;
  FUNCTION wf_course_comment(term  VARCHAR2,
                             crn   NUMBER,
                             delim VARCHAR2 DEFAULT ' ') RETURN VARCHAR2 AS

    -- -----------------------------------------------
    -- Get Course Comments  -- allow max of 6 comments
    -- -----------------------------------------------
    CURSOR comment_cur(term_in VARCHAR2, crn_in VARCHAR2) IS
      SELECT ssrtext_text
        FROM ssrtext
       WHERE ssrtext_crn = crn_in
         AND ssrtext_term_code = term_in
         AND ssrtext_seqno <= 6
       ORDER BY ssrtext_seqno;

    l_comment  VARCHAR2(4000) := '';
    l_temp     VARCHAR2(60) := '';
    loop_count NUMBER(3) := 0;

  BEGIN

    OPEN comment_cur(term, crn);
    <<comment_loop>>
    LOOP
      FETCH comment_cur
        INTO l_temp;
      EXIT WHEN comment_cur%NOTFOUND;

      IF loop_count = 0 THEN
        l_comment  := l_temp;
        loop_count := loop_count + 1;
      ELSE
        l_comment := l_comment || delim || l_temp;
      END IF;

    END LOOP comment_loop;
    CLOSE comment_cur;

    RETURN l_comment;

  END;
FUNCTION fz_course_meeting_time(term VARCHAR2, crn NUMBER) RETURN VARCHAR2 AS

    -- -----------------------------------------------
    -- Get Course Meeting Times
    -- -----------------------------------------------

    cursor meetingTimeCursor(term_in VARCHAR2, crn_in VARCHAR2) is
      select nvl(ssrmeet_mon_day, ' ') || nvl(ssrmeet_tue_day, ' ') ||
             nvl(ssrmeet_wed_day, ' ') || nvl(ssrmeet_thu_day, ' ') ||
             nvl(ssrmeet_fri_day, ' ') || nvl(ssrmeet_sat_day, ' ') || ' ' ||
             to_char(to_date(ssrmeet_begin_time, 'HH24MI'), 'HH:MI') || '-' ||
             to_char(to_date(ssrmeet_end_time, 'HH24MI'), 'HH:MIam')
        from ssrmeet
       where ssrmeet_term_code = term_in
         and ssrmeet_crn = crn_in
         and -- course needs to be meet at least one day a week
             (nvl(ssrmeet_mon_day, ' ') || nvl(ssrmeet_tue_day, ' ') ||
             nvl(ssrmeet_wed_day, ' ') || nvl(ssrmeet_thu_day, ' ') ||
             nvl(ssrmeet_fri_day, ' ') || nvl(ssrmeet_sat_day, ' ')) <>
             '      '
         and ssrmeet_begin_time is not null
         and ssrmeet_end_time is not null;

    meetingTime varchar2(100) := '';

    meetingCount number(3) := 0;

  begin

    select count(*)
      into meetingCount
      from ssrmeet
     where ssrmeet_term_code = term
       and ssrmeet_crn = crn
       and nvl(ssrmeet_mtyp_code, 'x') <> 'FIN' -- filter out final exams
    ;

    if meetingCount > 1 then

      meetingTime := 'See details for schedule';

      return meetingTime;

    end if;

    /*if wf_course_online(term, crn) = 'Y' then

      meetingTime := 'Course meets online';

      return meetingTime;

    end if;
  */
    if meetingCount = 0 then

      meetingTime := 'No schedule specified';

      return meetingTime;

    end if;

    open meetingTimeCursor(term, crn);

    fetch meetingTimeCursor
      into meetingTime;

    close meetingTimeCursor;

    if meetingTime is null then

      meetingTime := 'No schedule specified';

      return meetingTime;

    end if;

    return meetingTime;

    exception when others then

     meetingTime := 'No schedule specified';
      return meetingTime;

  end;
  function fz_get_desc(p_which_table varchar2,p_code varchar2) return varchar2 as
    description varchar2(120);
    cursor get_stvterm is
      select stvterm_desc from stvterm where stvterm_code = p_code;
    cursor get_stvstst is
      select stvstst_desc from stvstst where stvstst_code = p_code;
    cursor get_stvresd is
      select stvresd_desc from stvresd where stvresd_code = p_code;
    cursor get_stvcitz is
      select stvcitz_desc from stvcitz where stvcitz_code = p_code;
    cursor get_stvstyp is
      select stvstyp_desc from stvstyp where stvstyp_code = p_code;
    begin
      if p_which_table = 'STVTERM' then
      open get_stvterm;
      fetch get_stvterm
        into description;
      close get_stvterm;
    elsif p_which_table = 'STVSTST' then
      open get_stvstst;
      fetch get_stvstst
        into description;
      close get_stvstst;
    elsif p_which_table = 'STVRESD' then
      open get_stvresd;
      fetch get_stvresd
        into description;
      close get_stvresd;
    elsif p_which_table = 'STVCITZ' then
      open get_stvcitz;
      fetch get_stvcitz
        into description;
      close get_stvcitz;
    elsif p_which_table = 'STVSTYP' then
      open get_stvstyp;
      fetch get_stvstyp
        into description;
      close get_stvstyp;
    end if;
    return description;
    exception when others then
      return null;
    end fz_get_desc;
    FUNCTION fz_gettermfirst(p_stupidm   IN NUMBER,
                            p_levl_code IN VARCHAR2,
                            p_term varchar2) RETURN VARCHAR2 IS
      /* This procedure gets the First Term code Attended */
      /* for the student.                                 */
      --Local Cursor
      v_term varchar2(30);
      CURSOR c_gettermfirstc IS
         SELECT nvl(MIN(shrtgpa_term_code), '000000')
           FROM shrtgpa
          WHERE shrtgpa_pidm = p_stupidm AND
                shrtgpa_levl_code = p_levl_code AND
                shrtgpa_gpa_type_ind = 'I';

      CURSOR sgbstdnfirstterm IS
         SELECT sgbstdn_term_code_eff
           FROM sgbstdn x
          WHERE sgbstdn_pidm = p_stupidm AND
                sgbstdn_term_code_eff =
                (SELECT MIN(sgbstdn_term_code_eff)
                   FROM sgbstdn
                  WHERE sgbstdn_pidm = x.sgbstdn_pidm AND
                        sgbstdn_term_code_eff <= p_term);
   BEGIN
     begin
      OPEN c_gettermfirstc;
      FETCH c_gettermfirstc
         INTO v_term;
      CLOSE c_gettermfirstc;
      exception when others then
        null;
      end;
      IF v_term = '000000' THEN
         OPEN sgbstdnfirstterm;
         FETCH sgbstdnfirstterm
            INTO v_term;
         CLOSE sgbstdnfirstterm;
      END IF;
      SELECT STVTERM_DESC INTO v_term FROM STVTERM WHERE STVTERM_CODE=v_term;
      return v_term;
      exception when others then
        return null;
   END fz_gettermfirst;
end me_reg_utils;
/

