PROMPT CREATE OR REPLACE PACKAGE BODY payment_gateway
CREATE OR REPLACE PACKAGE BODY payment_gateway AS
  PROCEDURE PZ_CASHNET_URL(P_STUDENT_ID VARCHAR2, P_URL OUT VARCHAR2) IS
    v_url        VARCHAR2(250);
    v_encstring  VARCHAR2(250);
    v_keystring  VARCHAR2(32);
    v_expseconds NUMBER;
  BEGIN

    SELECT ztvcshc_cfg_val
      INTO v_keystring
      FROM ban2cash.ztvcshc
     WHERE ztvcshc_cfg_name = 'KEY';

    SELECT TO_NUMBER(ztvcshc_cfg_val)
      INTO v_expseconds
      FROM ban2cash.ztvcshc
     WHERE ztvcshc_cfg_name = 'EXPSECONDS';

    SELECT ztvcshc_cfg_val
      INTO v_url
      FROM ban2cash.ztvcshc
     WHERE ztvcshc_cfg_name = 'URL';

    v_encstring := BAN2CASH.Z_CASHNET_SSO.f_getencrypted (P_STUDENT_ID, v_expseconds, v_keystring);
  P_URL:=v_url||v_encstring;
  END PZ_CASHNET_URL;
END PAYMENT_GATEWAY;
/

