PROMPT CREATE OR REPLACE VIEW general_person_vw
CREATE OR REPLACE FORCE VIEW general_person_vw (
  pidm,
  student_id,
  last_name,
  first_name,
  mi,
  ssn,
  birth_date,
  ethn_code,
  gender,
  street1,
  street2,
  city,
  state,
  county,
  nation,
  zip,
  phone_area,
  phone_numb,
  phone_ext,
  email
) AS
select A.SPRIDEN_PIDM PIDM,
A.SPRIDEN_ID STUDENT_ID,
A.SPRIDEN_LAST_NAME LAST_NAME,
A.SPRIDEN_FIRST_NAME FIRST_NAME,
A.SPRIDEN_MI MI,
'' SSN,
B.SPBPERS_BIRTH_DATE BIRTH_DATE,
B.SPBPERS_ETHN_CODE ETHN_CODE,
B.SPBPERS_SEX GENDER,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'STREET1') STREET1,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'STREET2') STREET2,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'CITY')    CITY,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'STATE')   STATE,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'COUNTY')  COUNTY,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'NATION')  NATION,
MOBILE_APPS.ME_REG_UTILS.FZ_ADDRESS(A.SPRIDEN_PIDM,'ZIP')     ZIP,
MOBILE_APPS.ME_REG_UTILS.FZ_PHONE(A.SPRIDEN_PIDM,'AREA_CODE') PHONE_AREA,
MOBILE_APPS.ME_REG_UTILS.FZ_PHONE(A.SPRIDEN_PIDM,'PHONE')     PHONE_NUMB,
MOBILE_APPS.ME_REG_UTILS.FZ_PHONE(A.SPRIDEN_PIDM,'EXT')       PHONE_EXT,
MOBILE_APPS.ME_REG_UTILS.FZ_GET_EMAIL_ADDRESS(A.SPRIDEN_PIDM) EMAIL
 FROM SPRIDEN A JOIN spbpers B ON A.SPRIDEN_PIDM=B.SPBPERS_PIDM
 WHERE A.SPRIDEN_CHANGE_IND IS NULL AND A.SPRIDEN_ENTITY_IND='P'
/

