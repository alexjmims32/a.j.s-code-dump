PROMPT CREATE OR REPLACE VIEW faculty_feedback_vw
CREATE OR REPLACE FORCE VIEW faculty_feedback_vw (
  pidm,
  faculty_name,
  faculty_email,
  term_code,
  crn,
  subject,
  course_numb,
  primary_ind
) AS
select sirasgn_pidm pidm,
       case
         when BANINST1.F_FORMAT_NAME(SIRASGN_PIDM, 'FL') like 'NAME NOT%' then
          ' '
         else
          BANINST1.F_FORMAT_NAME(SIRASGN_PIDM, 'FL')
       end FACULTY_NAME,
       ME_REG_UTILS.FZ_GET_EMP_EMAIL(SIRASGN_PIDM) FACULTY_EMAIL,
       ssbsect_term_code term_code,
       ssbsect_crn crn,
       ssbsect_subj_code subject,
       ssbsect_crse_numb course_numb,
       sirasgn_primary_ind primary_ind
  from sirasgn
  join ssbsect
    on sirasgn_term_code = ssbsect_term_code
   and sirasgn_crn = ssbsect_crn
  join scbcrse
    on scbcrse_subj_code = ssbsect_subj_code
   and scbcrse_crse_numb = ssbsect_crse_numb
   and scbcrse_eff_term =
       (select max(a.scbcrse_eff_term)
          from scbcrse a
         where a.scbcrse_subj_code = ssbsect_subj_code
           and a.scbcrse_crse_numb = ssbsect_crse_numb
           and a.scbcrse_eff_term <= ssbsect_term_code)
   and scbcrse_aprv_code <> 'P'
/

