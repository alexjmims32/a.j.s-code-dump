create table MOBEDU.POPULATIONTYPE
(
  id             NUMBER(11) not null,
  type           VARCHAR2(20) not null,
  title          VARCHAR2(50) not null,
  description    VARCHAR2(100) not null,
  query          VARCHAR2(1000),
  lastmodifiedby VARCHAR2(50) not null,
  lastmodifiedon TIMESTAMP(6) not null
)
;
alter table MOBEDU.POPULATIONTYPE
  add constraint MOBEDU_POP_TYPE_PK primary key (ID);

