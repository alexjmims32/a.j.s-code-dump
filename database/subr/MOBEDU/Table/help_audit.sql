create table MOBEDU.HELP_AUDIT
(
  abouttext      VARCHAR2(4000),
  faq            VARCHAR2(4000),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6),
  campuscode     VARCHAR2(100)
)
;

