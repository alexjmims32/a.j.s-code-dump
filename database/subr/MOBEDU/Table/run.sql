----------------------------------------------
-- Export file for user MOBEDU              --
-- Created by user on 3/22/2013, 3:27:30 PM --
----------------------------------------------

spool run.log

prompt
prompt Creating table ADMIN_PRIV
prompt =========================
prompt
@@admin_priv.sql
prompt
prompt Creating table ADMIN_ROLE
prompt =========================
prompt
@@admin_role.sql
prompt
prompt Creating table ADMIN_USER
prompt =========================
prompt
@@admin_user.sql
prompt
prompt Creating table CAMPUS
prompt =====================
prompt
@@campus.sql
prompt
prompt Creating table COURSE_SEARCH_MV
prompt ===============================
prompt
@@course_search_mv.sql
prompt
prompt Creating table EMERGENCY_CONTACTS
prompt =================================
prompt
@@emergency_contacts.sql
prompt
prompt Creating table EMERGENCY_CONTACTS_AUDIT
prompt =======================================
prompt
@@emergency_contacts_audit.sql
prompt
prompt Creating table FAQ
prompt ==================
prompt
@@faq.sql
prompt
prompt Creating table FEEDBACK
prompt =======================
prompt
@@feedback.sql
prompt
prompt Creating table FEEDBACK_DATA
prompt ============================
prompt
@@feedback_data.sql
prompt
prompt Creating table FEEDS
prompt ====================
prompt
@@feeds.sql
prompt
prompt Creating table FEEDS_AUDIT
prompt ==========================
prompt
@@feeds_audit.sql
prompt
prompt Creating table HELP
prompt ===================
prompt
@@help.sql
prompt
prompt Creating table HELP_AUDIT
prompt =========================
prompt
@@help_audit.sql
prompt
prompt Creating table MAPS
prompt ===================
prompt
@@maps.sql
prompt
prompt Creating table MAPS_AUDIT
prompt =========================
prompt
@@maps_audit.sql
prompt
prompt Creating table MOBEDU_CART
prompt ==========================
prompt
@@mobedu_cart.sql
prompt
prompt Creating table MODULE
prompt =====================
prompt
@@module.sql
prompt
prompt Creating table MODULES
prompt ======================
prompt
@@modules.sql
prompt
prompt Creating table NOTICETYPE
prompt =========================
prompt
@@noticetype.sql
prompt
prompt Creating table NOTICE
prompt =====================
prompt
@@notice.sql
prompt
prompt Creating table NOTICELOG
prompt ========================
prompt
@@noticelog.sql
prompt
prompt Creating table POPULATIONTYPE
prompt =============================
prompt
@@populationtype.sql
prompt
prompt Creating table NOTICEPOPULATION
prompt ===============================
prompt
@@noticepopulation.sql
prompt
prompt Creating table PRIVILEGE
prompt ========================
prompt
@@privilege.sql
prompt
prompt Creating table PRIVILEGE_AUDIT
prompt ==============================
prompt
@@privilege_audit.sql
prompt
prompt Creating table ROLE
prompt ===================
prompt
@@role.sql
prompt
prompt Creating table SERVICE_AREA
prompt ===========================
prompt
@@service_area.sql
prompt
prompt Creating table STU_HOUSING_INFO
prompt ===============================
prompt
@@stu_housing_info.sql

spool off
