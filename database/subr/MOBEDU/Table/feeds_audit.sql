create table MOBEDU.FEEDS_AUDIT
(
  id             NUMBER(5) not null,
  parentid       NUMBER(5) not null,
  type           VARCHAR2(20 BYTE) not null,
  link           VARCHAR2(200 BYTE),
  format         VARCHAR2(100 BYTE),
  moduleid       NUMBER(5) not null,
  name           VARCHAR2(100 BYTE),
  icon           VARCHAR2(100 BYTE),
  campuscode     VARCHAR2(6 BYTE),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40 BYTE),
  lastmodifiedon TIMESTAMP(6)
)
;

