create table MOBEDU.PRIVILEGE_AUDIT
(
  roleid         NUMBER not null,
  modulecode     VARCHAR2(100 BYTE) not null,
  accessflag     VARCHAR2(1 BYTE),
  authrequired   CHAR(1 BYTE) default 'Y',
  lastmodifiedon TIMESTAMP(6),
  lastmodifiedby VARCHAR2(40 BYTE),
  version_no     NUMBER(5)
)
;

