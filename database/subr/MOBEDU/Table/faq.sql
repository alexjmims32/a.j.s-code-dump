create table MOBEDU.FAQ
(
  id             NUMBER not null,
  campuscode     VARCHAR2(100 BYTE),
  question       VARCHAR2(4000 BYTE),
  answer         VARCHAR2(4000 BYTE),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40 BYTE),
  lastmodifiedon TIMESTAMP(6)
)
;

