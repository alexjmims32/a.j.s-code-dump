create table MOBEDU.MODULE
(
  campuscode    VARCHAR2(6),
  id            NUMBER,
  code          VARCHAR2(10),
  description   VARCHAR2(100),
  authrequired  CHAR(1) default 'Y',
  showbydefault CHAR(1) default 'N',
  icon          VARCHAR2(100),
  position      VARCHAR2(10 BYTE)
)
;

