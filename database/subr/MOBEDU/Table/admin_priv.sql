create table MOBEDU.ADMIN_PRIV
(
  username    VARCHAR2(20 BYTE) not null,
  privcode    VARCHAR2(20 BYTE) not null,
  accessflag  CHAR(1 BYTE) default 'Y' not null,
  description VARCHAR2(100 BYTE) not null
)
;

