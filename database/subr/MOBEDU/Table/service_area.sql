create table MOBEDU.SERVICE_AREA
(
  id         INTEGER not null,
  area       VARCHAR2(100) not null,
  subarea    VARCHAR2(100),
  supervisor VARCHAR2(100),
  assocdean  VARCHAR2(100),
  dean       VARCHAR2(100),
  hr         VARCHAR2(100),
  provost    VARCHAR2(100)
)
;
create unique index MOBEDU.SYS_C00152142 on MOBEDU.SERVICE_AREA (ID);
alter table MOBEDU.SERVICE_AREA
  add primary key (ID);

