create table MOBEDU.STU_HOUSING_INFO
(
  fullname           VARCHAR2(100) not null,
  classof            VARCHAR2(100),
  studenid           VARCHAR2(50) not null,
  gender             VARCHAR2(10),
  birthdate          DATE,
  address            VARCHAR2(500),
  city               VARCHAR2(50),
  state              VARCHAR2(50),
  pincode            VARCHAR2(10),
  phone              VARCHAR2(20),
  email              VARCHAR2(50),
  beginterm          VARCHAR2(50),
  beginyear          VARCHAR2(50),
  applicationtype    VARCHAR2(50),
  hallsizepreference VARCHAR2(50),
  floorpreference    VARCHAR2(50),
  issmoker           VARCHAR2(50),
  livewithsmoker     VARCHAR2(50),
  studyprefer        VARCHAR2(50),
  wakeuppref         VARCHAR2(50),
  roompref           VARCHAR2(50),
  names              VARCHAR2(50),
  specialneeds       VARCHAR2(100),
  mealplan           VARCHAR2(100),
  comments           VARCHAR2(500)
)
;

