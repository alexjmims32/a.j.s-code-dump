create table MOBEDU.MOBEDU_CART
(
  student_id        VARCHAR2(10),
  crn               VARCHAR2(10),
  term              VARCHAR2(10),
  status            VARCHAR2(500),
  processed_ind     VARCHAR2(1),
  error_flag        VARCHAR2(1),
  rsts_code         VARCHAR2(2),
  var_credit_hr_ind VARCHAR2(2),
  var_credit_hrs    NUMBER(7,3)
)
;

