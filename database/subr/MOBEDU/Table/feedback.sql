create table MOBEDU.FEEDBACK
(
  id          NUMBER(5) not null,
  title       VARCHAR2(50 BYTE),
  email       VARCHAR2(100 BYTE),
  description VARCHAR2(100 BYTE)
)
;

