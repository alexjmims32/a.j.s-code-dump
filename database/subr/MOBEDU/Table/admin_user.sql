create table MOBEDU.ADMIN_USER
(
  username  VARCHAR2(20 BYTE) not null,
  firstname VARCHAR2(100 BYTE) not null,
  lastname  VARCHAR2(100 BYTE) not null,
  password  VARCHAR2(100 BYTE) not null,
  active    CHAR(1 BYTE) default 'Y' not null
)
;
create unique index MOBEDU.ADMIN_USER_IDX on MOBEDU.ADMIN_USER (USERNAME);

