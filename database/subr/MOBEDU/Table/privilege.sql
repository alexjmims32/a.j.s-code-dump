create table MOBEDU.PRIVILEGE
(
  roleid         VARCHAR2(100) not null,
  modulecode     VARCHAR2(100) not null,
  accessflag     VARCHAR2(1),
  authrequired   CHAR(1) default 'Y',
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6)
)
;

