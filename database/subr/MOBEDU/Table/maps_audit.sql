create table MOBEDU.MAPS_AUDIT
(
  id             NUMBER not null,
  campus_code    VARCHAR2(6 BYTE),
  building_code  VARCHAR2(6 BYTE),
  building_name  VARCHAR2(100 BYTE),
  building_desc  VARCHAR2(100 BYTE),
  phone          VARCHAR2(15 BYTE),
  email          VARCHAR2(30 BYTE),
  img_url        VARCHAR2(30 BYTE),
  longitude      VARCHAR2(30 BYTE),
  latitude       VARCHAR2(30 BYTE),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40 BYTE),
  lastmodifiedon TIMESTAMP(6),
  address        VARCHAR2(4000)
)
;

