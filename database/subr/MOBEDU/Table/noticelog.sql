create table MOBEDU.NOTICELOG
(
  id             NUMBER(11) not null,
  noticeid       NUMBER(11) not null,
  username       VARCHAR2(20) not null,
  message        VARCHAR2(4000) not null,
  expirydate     DATE,
  deliverymethod VARCHAR2(20),
  delivered      NUMBER(1) default 0 not null,
  lastmodifiedby VARCHAR2(50) not null,
  lastmodifiedon TIMESTAMP(6),
  deleted        NUMBER(1) default 0,
  type           VARCHAR2(100),
  title          VARCHAR2(100),
  duedate        DATE,
  readflag       NUMBER(1) default 0 not null
)
;
alter table MOBEDU.NOTICELOG
  add constraint MOBEDU_NOTICE_LOG_PK primary key (ID);
alter table MOBEDU.NOTICELOG
  add constraint MOBEDU_NOTICE_LOG_NOTICE_ID_FK foreign key (NOTICEID)
  references MOBEDU.NOTICE (ID) on delete cascade
  disable;

