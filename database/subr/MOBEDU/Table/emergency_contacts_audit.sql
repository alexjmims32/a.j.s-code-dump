create table MOBEDU.EMERGENCY_CONTACTS_AUDIT
(
  id             NUMBER not null,
  campus         VARCHAR2(100 BYTE),
  name           VARCHAR2(50 BYTE),
  phone          VARCHAR2(13 BYTE),
  address        VARCHAR2(50 BYTE),
  email          VARCHAR2(30 BYTE),
  picture_url    VARCHAR2(150 BYTE),
  category       VARCHAR2(34 BYTE),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40 BYTE),
  lastmodifiedon TIMESTAMP(6),
  comments       VARCHAR2(1000 BYTE)
)
;

