create table MOBEDU.COURSE_SEARCH_MV
(
  crn             VARCHAR2(5) not null,
  search_string   VARCHAR2(4000),
  term_code       VARCHAR2(6) not null,
  course_number   VARCHAR2(5) not null,
  subject         VARCHAR2(4) not null,
  subject_desc    VARCHAR2(30),
  course_title    VARCHAR2(30),
  credit_hrs      NUMBER(7,3),
  tot_credit_hrs  NUMBER(9,3),
  bill_hours      NUMBER(7,3),
  campus          VARCHAR2(30),
  campus_code     VARCHAR2(3) not null,
  course_level    CHAR(1),
  lecture_hours   NUMBER(7,3),
  seats_available VARCHAR2(3),
  waitlist_avail  NUMBER(4),
  grade           VARCHAR2(30),
  grade_code      VARCHAR2(1),
  department      VARCHAR2(4000),
  faculty         VARCHAR2(4000),
  instructor_name VARCHAR2(4000),
  college         VARCHAR2(4000),
  term_desc       VARCHAR2(30)
)
;

