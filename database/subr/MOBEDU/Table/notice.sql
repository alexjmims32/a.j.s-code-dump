create table MOBEDU.NOTICE
(
  id             NUMBER(11) not null,
  typeid         NUMBER(11) not null,
  title          VARCHAR2(50) not null,
  message        VARCHAR2(4000) not null,
  duedate        DATE,
  expirydate     DATE,
  posted         NUMBER(1) default 0 not null,
  lastmodifiedby VARCHAR2(50) not null,
  lastmodifiedon TIMESTAMP(6) not null,
  url            VARCHAR2(100)
)
;
comment on column MOBEDU.NOTICE.typeid
  is 'refers to ID in noticetypes table';
alter table MOBEDU.NOTICE
  add constraint MOBEDU_NOTICE_PK primary key (ID);
alter table MOBEDU.NOTICE
  add constraint MOBEDU_NOTICE_TYPE_FK foreign key (TYPEID)
  references MOBEDU.NOTICETYPE (ID);

