PROMPT CREATE OR REPLACE FUNCTION mobedu.fz_get_var_ind
CREATE OR REPLACE function mobedu.fz_get_var_ind(p_term varchar2,
                        p_crn  number)return varchar2 is
cursor c_var is
select * from ssbsect a join sobterm c
on a.ssbsect_term_code=c.sobterm_term_code
and nvl(c.sobterm_cred_web_upd_ind,'N')='Y'
AND A.SSBSECT_TERM_CODE=p_term
AND A.SSBSECT_CRN=P_CRN
join scbcrse b on
a.ssbsect_subj_code=b.scbcrse_subj_code
and a.ssbsect_crse_numb=b.scbcrse_crse_numb
and b.scbcrse_eff_term=(select max(t.scbcrse_eff_term) from scbcrse t where t.scbcrse_subj_code=a.ssbsect_subj_code
and t.scbcrse_crse_numb=a.ssbsect_crse_numb and t.scbcrse_eff_term<=a.ssbsect_term_code)
AND B.SCBCRSE_CREDIT_HR_IND IS NOT NULL
AND A.SSBSECT_CREDIT_HRS IS NULL
AND A.SSBSECT_VOICE_AVAIL='Y';
begin

FOR I IN C_VAR LOOP
  RETURN 'Y';
END LOOP;
RETURN 'N';
EXCEPTION WHEN OTHERS THEN
  RETURN 'N';
end;

/

