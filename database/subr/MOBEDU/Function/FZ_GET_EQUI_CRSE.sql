PROMPT CREATE OR REPLACE FUNCTION mobedu.fz_get_equi_crse
CREATE OR REPLACE function mobedu.fz_get_equi_crse(p_crse_num      varchar2,
                                            p_subj_code     varchar2,
                                            p_transfer_inst VARCHAR2)
  return varchar2 as
  return varchar2(1000) := ' shrtatc.shrtatc_crse_numb_inst || ||shrtatc.shrtatc_subj_code_inst';
  /* V_shrtatc_connector varchar2(30);*/
  V_RESULT VARCHAR2(500);
  cursor get_details is
    select shrtatc.shrtatc_crse_numb_inst,
           shrtatc.shrtatc_subj_code_inst,
           SHRTATC.SHRTATC_CONNECTOR,
           SHRTATC_INST_LPAREN_CONN,
           SHRTATC_INST_RPAREN
      from sobsbgi
      join shrtatc
        on sobsbgi.sobsbgi_sbgi_code = shrtatc.shrtatc_sbgi_code
       and shrtatc.shrtatc_crse_numb_trns = p_crse_num
       and shrtatc.shrtatc_subj_code_trns = p_subj_code
       and shrtatc.shrtatc_sbgi_code = p_transfer_inst;

  /*cursor O_details is
  select shrtatc.shrtatc_crse_numb_inst, shrtatc.shrtatc_subj_code_inst
    from sobsbgi
    join shrtatc
      on sobsbgi.sobsbgi_sbgi_code = shrtatc.shrtatc_sbgi_code
     and shrtatc.shrtatc_crse_numb_trns = p_crse_num
     and shrtatc.shrtatc_subj_code_trns = p_subj_code
     and shrtatc.shrtatc_sbgi_code = p_transfer_inst
     AND SHRTATC.SHRTATC_CONNECTOR='O';*/

begin
  /*BEGIN
  select 'Y' INTO V_shrtatc_connector
      from sobsbgi
      join shrtatc
        on sobsbgi.sobsbgi_sbgi_code = shrtatc.shrtatc_sbgi_code
       and shrtatc.shrtatc_crse_numb_trns = p_crse_num
       and shrtatc.shrtatc_subj_code_trns = p_subj_code
       and shrtatc.shrtatc_sbgi_code = p_transfer_inst
       AND SHRTATC.SHRTATC_CONNECTOR='A';
  EXCEPTION WHEN OTHERS THEN
    V_shrtatc_connector:='N';
  END;*/
  FOR R_DET IN get_details LOOP
    IF R_DET.SHRTATC_CONNECTOR = 'A' THEN
      V_RESULT := V_RESULT || ' AND ' ||R_DET.SHRTATC_INST_LPAREN_CONN || R_DET.SHRTATC_CRSE_NUMB_INST || ' ' ||
                  R_DET.SHRTATC_SUBJ_CODE_INST||R_DET.SHRTATC_INST_RPAREN;
    ELSIF R_DET.SHRTATC_CONNECTOR = 'O' THEN
      V_RESULT := V_RESULT || ' OR ' ||R_DET.SHRTATC_INST_LPAREN_CONN || R_DET.SHRTATC_CRSE_NUMB_INST || ' ' ||
                  R_DET.SHRTATC_SUBJ_CODE_INST||R_DET.SHRTATC_INST_RPAREN;
    ELSIF R_DET.SHRTATC_CONNECTOR IS NULL THEN
      V_RESULT := R_DET.SHRTATC_INST_LPAREN_CONN ||R_DET.SHRTATC_CRSE_NUMB_INST || ' ' ||
                  R_DET.SHRTATC_SUBJ_CODE_INST||R_DET.SHRTATC_INST_RPAREN;
    END IF;

  END LOOP;

  /*FOR R_DETO IN O_details LOOP
  IF V_RESULT IS NOT NULL THEN
    V_RESULT:=V_RESULT ||' OR '|| R_DETO.SHRTATC_CRSE_NUMB_INST||' '||R_DETO.SHRTATC_SUBJ_CODE_INST;
  ELSE
    V_RESULT:= R_DETO.SHRTATC_CRSE_NUMB_INST||' '||R_DETO.SHRTATC_SUBJ_CODE_INST;
  END IF;
    END LOOP;    */

  RETURN V_RESULT;
end fz_get_equi_crse;

/

