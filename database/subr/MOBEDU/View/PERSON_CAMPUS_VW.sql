PROMPT CREATE OR REPLACE VIEW mobedu.person_campus_vw
CREATE OR REPLACE VIEW PERSON_CAMPUS_VW AS
SELECT
a.spriden_id Id,
a.spriden_first_name First_name,
a.spriden_last_name Last_name,
a.spriden_mi Middle_name,
a.spriden_first_name||' '||a.spriden_last_name Full_name,
z_cm_mobile_campus.fz_category(a.spriden_pidm) category,
z_cm_mobile_campus.fz_get_campus(a.spriden_pidm) campus,
z_cm_mobile_campus.f_get_college(a.spriden_pidm) college
FROM spriden a
WHERE a.spriden_change_ind IS NULL;

