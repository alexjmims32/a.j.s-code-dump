PROMPT CREATE OR REPLACE VIEW mobedu.terms_to_bursar_vw
CREATE OR REPLACE FORCE VIEW mobedu.terms_to_bursar_vw (
  term_code,
  student_id,
  term_desc
) AS
SELECT DISTINCT A.TBRACCD_TERM_CODE TERM_CODE,
                B.SPRIDEN_ID        STUDENT_ID,
                C.STVTERM_DESC      TERM_DESC/*,
                a.tbraccd_due_date  due_date*/
  FROM TBRACCD A, SPRIDEN B, STVTERM C
 WHERE A.TBRACCD_PIDM = B.SPRIDEN_PIDM
   AND B.SPRIDEN_CHANGE_IND IS NULL
   AND C.STVTERM_CODE = A.TBRACCD_TERM_CODE
/

