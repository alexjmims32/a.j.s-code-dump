PROMPT CREATE OR REPLACE VIEW mobedu.me_student_crse_grde
CREATE OR REPLACE FORCE VIEW mobedu.me_student_crse_grde (
  pidm,
  stu_id,
  term_code,
  crn,
  subj_code,
  crse_numb,
  crse_title,
  mid_grade,
  final_grade,
  grade_date
) AS
SELECT SFRSTCR_PIDM pidm,
       gb_common.f_get_id(SFRSTCR_PIDM) stu_id,
       SFRSTCR_TERM_CODE term_code,
       SFRSTCR_CRN crn,
       SSBSECT_SUBJ_CODE subj_code,
       SSBSECT_CRSE_NUMB crse_numb,
       NVL(SSBSECT_CRSE_TITLE, SCBCRSE_TITLE) crse_title,
       SFRSTCR_GRDE_CODE_MID mid_grade,
       SFRSTCR_GRDE_CODE final_grade,
       DECODE(SFRSTCR_GRDE_DATE, NULL, 'N', 'Y') grade_date
  FROM SSBSECT, SCBCRSE, SFRSTCR
 WHERE SFRSTCR_TERM_CODE = SSBSECT_TERM_CODE
   AND SFRSTCR_CRN = SSBSECT_CRN
   AND SCBCRSE_SUBJ_CODE = SSBSECT_SUBJ_CODE
   AND SCBCRSE_CRSE_NUMB = SSBSECT_CRSE_NUMB
   AND SCBCRSE_EFF_TERM =
       (SELECT MAX(X.SCBCRSE_EFF_TERM)
          FROM SCBCRSE X
         WHERE X.SCBCRSE_SUBJ_CODE = SSBSECT_SUBJ_CODE
           AND X.SCBCRSE_CRSE_NUMB = SSBSECT_CRSE_NUMB
           AND X.SCBCRSE_EFF_TERM <= SSBSECT_TERM_CODE)
/

