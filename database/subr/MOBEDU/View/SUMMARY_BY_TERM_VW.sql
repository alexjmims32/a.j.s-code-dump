PROMPT CREATE OR REPLACE VIEW mobedu.summary_by_term_vw
CREATE OR REPLACE FORCE VIEW mobedu.summary_by_term_vw (
  term_code,
  term_description,
  category,
  cat_description,
  charge,
  payment,
  balance,
  student_id
) AS
select a1.term_code,
       a1.term_description,
       A1.CATEGORY,
       A1.CAT_DESCRIPTION,
       A1.AMOUNT           charge,
       B1.AMOUNT           payment,
       A1.balance          balance,
       a1.student_id       student_id
  from account_summary_by_term_vw A1
  left join account_summary_by_term_vw B1
    on A1.student_id = B1.student_id
   and B1.tbbdetc_type_ind = 'P'
   and A1.CATEGORY = B1.CATEGORY
 where A1.tbbdetc_type_ind = 'C'
UNION all
select a1.term_code term_code,
       a1.term_description,
       A1.CATEGORY,
       A1.CAT_DESCRIPTION  N,
       B1.AMOUNT           CHARGE,
       A1.AMOUNT           PAYMENT,
       B1.balance          balance,
       a1.student_id       student_id
  from account_summary_by_term_vw A1
  left join account_summary_by_term_vw B1
    on A1.student_id = B1.student_id
   and B1.tbbdetc_type_ind = 'C'
   and A1.CATEGORY = B1.CATEGORY
 where A1.tbbdetc_type_ind = 'P'
 order by term_code desc
/

