PROMPT CREATE OR REPLACE PACKAGE mobedu.me_bwckgens
CREATE OR REPLACE PACKAGE me_bwckgens
AS
--
-- FILE NAME..: bwckgens.sql
-- RELEASE....: 8.0
-- OBJECT NAME: BWCKGENS
-- PRODUCT....: SCOMWEB
-- USAGE......:
-- COPYRIGHT..: Copyright (C) SCTCorporation 2002.  All rights reserved.

-- DESCRIPTION:
-- DESCRIPTION END
   sobterm_row   sobterm%ROWTYPE;

   TYPE search_reg_allowed_tabtype IS TABLE OF BOOLEAN
      INDEX BY BINARY_INTEGER;

---------------------------------------------------------------------------

   PROCEDURE P_DispCrseSchdDetl (
      crn       IN   VARCHAR2 DEFAULT NULL,
      term_in   IN   stvterm.stvterm_code%TYPE
   );

   -- Paints the student schedule, without opening or closing the web page
   -- PIDM - Banner personal ID master
   -- CRN - course reference number
   -- TERM_IN - Selected term code
---------------------------------------------------------------------------

   PROCEDURE P_ListCrse (
      term_in         IN   OWA_UTIL.ident_arr,
      sel_subj        IN   OWA_UTIL.ident_arr,
      sel_crse        IN   VARCHAR2,
      sel_title       IN   VARCHAR2,
      begin_hh        IN   VARCHAR2,
      begin_mi        IN   VARCHAR2,
      begin_ap        IN   VARCHAR2,
      sel_day         IN   OWA_UTIL.ident_arr,
      sel_ptrm        IN   OWA_UTIL.ident_arr,
      end_hh          IN   VARCHAR2,
      end_mi          IN   VARCHAR2,
      end_ap          IN   VARCHAR2,
      sel_camp        IN   OWA_UTIL.ident_arr,
      sel_schd        IN   OWA_UTIL.ident_arr,
      sel_sess        IN   OWA_UTIL.ident_arr,
      sel_instr       IN   OWA_UTIL.ident_arr,
      sel_attr        IN   OWA_UTIL.ident_arr,
      crn             IN   OWA_UTIL.ident_arr,
      rsts            IN   OWA_UTIL.ident_arr,
      sel_dunt_code   IN   VARCHAR2 DEFAULT NULL,
      sel_dunt_unit   IN   VARCHAR2 DEFAULT NULL,
      sel_levl        IN   OWA_UTIL.ident_arr,
      sel_from_cred   IN   VARCHAR2 DEFAULT NULL,
      sel_to_cred     IN   VARCHAR2 DEFAULT NULL,
      sel_insm        IN   OWA_UTIL.ident_arr,
      call_value_in   IN   VARCHAR2 DEFAULT NULL
   );

---------------------------------------------------------------------------


   PROCEDURE P_RegsCrseSearch (
      term_in            IN   OWA_UTIL.ident_arr,
      rsts               IN   OWA_UTIL.ident_arr,
      assoc_term_in      IN   OWA_UTIL.ident_arr,
      crn                IN   OWA_UTIL.ident_arr,
      start_date_in      IN   OWA_UTIL.ident_arr,
      end_date_in        IN   OWA_UTIL.ident_arr,
      subj               IN   OWA_UTIL.ident_arr,
      crse               IN   OWA_UTIL.ident_arr,
      sec                IN   OWA_UTIL.ident_arr,
      levl               IN   OWA_UTIL.ident_arr,
      cred               IN   OWA_UTIL.ident_arr,
      gmod               IN   OWA_UTIL.ident_arr,
      title              IN   bwckcoms.varchar2_tabtype,
      mesg               IN   OWA_UTIL.ident_arr,
      regs_row                NUMBER,
      add_row                 NUMBER,
      wait_row                NUMBER,
      subject_error_in   IN   BOOLEAN DEFAULT FALSE
   );

---------------------------------------------------------------------------

   PROCEDURE P_RegsGetCrse (
      term_in         IN   OWA_UTIL.ident_arr,
      sel_subj        IN   OWA_UTIL.ident_arr,
      sel_crse        IN   VARCHAR2,
      sel_title       IN   VARCHAR2,
      begin_hh        IN   VARCHAR2,
      begin_mi        IN   VARCHAR2,
      begin_ap        IN   VARCHAR2,
      sel_day         IN   OWA_UTIL.ident_arr,
      sel_ptrm        IN   OWA_UTIL.ident_arr,
      end_hh          IN   VARCHAR2,
      end_mi               VARCHAR2,
      end_ap          IN   VARCHAR2,
      sel_camp        IN   OWA_UTIL.ident_arr,
      sel_schd        IN   OWA_UTIL.ident_arr,
      sel_sess        IN   OWA_UTIL.ident_arr,
      sel_instr       IN   OWA_UTIL.ident_arr,
      sel_attr        IN   OWA_UTIL.ident_arr,
      rsts            IN   OWA_UTIL.ident_arr,
      assoc_term_in   IN   OWA_UTIL.ident_arr,
      crn             IN   OWA_UTIL.ident_arr,
      start_date_in   IN   OWA_UTIL.ident_arr,
      end_date_in     IN   OWA_UTIL.ident_arr,
      subj            IN   OWA_UTIL.ident_arr,
      crse            IN   OWA_UTIL.ident_arr,
      sec             IN   OWA_UTIL.ident_arr,
      levl            IN   OWA_UTIL.ident_arr,
      cred            IN   OWA_UTIL.ident_arr,
      gmod            IN   OWA_UTIL.ident_arr,
      title           IN   bwckcoms.varchar2_tabtype,
      mesg            IN   OWA_UTIL.ident_arr,
      regs_row             NUMBER,
      add_row              NUMBER,
      wait_row             NUMBER,
      sel_dunt_code   IN   VARCHAR2 DEFAULT NULL,
      sel_dunt_unit   IN   VARCHAR2 DEFAULT NULL,
      sel_levl        IN   OWA_UTIL.ident_arr,
      sel_from_cred   IN   VARCHAR2 DEFAULT NULL,
      sel_to_cred     IN   VARCHAR2 DEFAULT NULL,
      sel_insm        IN   OWA_UTIL.ident_arr
   );

---------------------------------------------------------------------------

   PROCEDURE P_RegsUpd (
      term       IN   stvterm.stvterm_code%TYPE DEFAULT NULL,
      crn        IN   OWA_UTIL.ident_arr,
      cred       IN   OWA_UTIL.ident_arr,
      gmod       IN   OWA_UTIL.ident_arr,
      levl       IN   OWA_UTIL.ident_arr,
      cred_old   IN   OWA_UTIL.ident_arr,
      gmod_old   IN   OWA_UTIL.ident_arr,
      levl_old   IN   OWA_UTIL.ident_arr,
      P_ERROR    OUT  VARCHAR2
   );

---------------------------------------------------------------------------

   PROCEDURE P_Search (term_in       IN OWA_UTIL.ident_arr,
                       call_value_in IN VARCHAR2 DEFAULT NULL
                       );

---------------------------------------------------------------------------

   PROCEDURE p_disp_active_regs (
      term_in   IN   STVTERM.STVTERM_CODE%TYPE DEFAULT NULL
   );

---------------------------------------------------------------------------

   PROCEDURE p_disp_reg_hist;

------------------------------------------------------------------------

   PROCEDURE P_SearchChk (
      stdn_row_in                 sgbstdn%ROWTYPE,
      term_in                     stvterm.stvterm_code%TYPE,
      pidm_in                     spriden.spriden_pidm%TYPE,
      search_reg_allow   IN OUT   VARCHAR2
   );

   PROCEDURE p_disp_term_date (
      p_calling_proc VARCHAR2 DEFAULT NULL,
      p_term         VARCHAR2 DEFAULT NULL,
      p_from_date    VARCHAR2 DEFAULT NULL,
      p_to_date      VARCHAR2 DEFAULT NULL,
      p_error_msg    VARCHAR2 DEFAULT NULL
   );
   PROCEDURE p_proc_term_date (
      p_calling_proc VARCHAR2 DEFAULT NULL,
      p_term         VARCHAR2 DEFAULT NULL,
      p_by_date      VARCHAR2 DEFAULT NULL,
      p_from_date    VARCHAR2 DEFAULT NULL,
      p_to_date      VARCHAR2 DEFAULT NULL
   );
   PROCEDURE p_return_to_disp_term_date (
      p_calling_proc VARCHAR2 DEFAULT NULL,
      p_term         VARCHAR2 DEFAULT NULL,
      p_from_date    VARCHAR2 DEFAULT NULL,
      p_to_date      VARCHAR2 DEFAULT NULL,
      p_error_msg    VARCHAR2 DEFAULT NULL);
   procedure p_intglobal(p_pidm number,
               p_term stvterm.stvterm_code%type);

   procedure p_disp;
   PROCEDURE PZ_CHANGE_CREDITS(p_student_id VARCHAR2,
                               P_TERM_CODE VARCHAR2,
                               P_CRN NUMBER,
                               P_cred NUMBER,
                               P_CRED_OLD NUMBER,
                               P_ERROR OUT VARCHAR2) ;

   function fz_get_var_ind(p_term varchar2,
                        p_crn  number)return varchar2;

   PROCEDURE PZ_withdrawl(p_student_id VARCHAR2,
                               P_TERM_CODE VARCHAR2,
                               P_CRN NUMBER,
                               P_RSTS VARCHAR2,
                               P_ERROR OUT VARCHAR2);
---------------------------------------------------------------------------
END ME_BWCKGENS;
/

