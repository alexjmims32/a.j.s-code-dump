REM INSERTING into MOBEDU.MODULE
SET DEFINE OFF;
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',22,'VIDEOS','Videos','N','Y','video.png','BOT');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',12,'ALUMNI','Alumni','Y','N','alumni.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',10,'HISTORY','Student History','Y','N','student-history.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',2,'ATHLETICS','Athletics','N','Y','athletics.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',3,'EVENTS','Events','N','Y','events.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',4,'MAPS','Campus Maps','N','Y','campus-map.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',5,'COURSES','My Schedule','Y','Y','courses.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',6,'DIRECTORY','Directory','N','Y','dir.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',7,'PROFILE','Student Profile','Y','Y','student-profile.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',8,'ACCOUNTS','Student Accounts','Y','Y','student-accounts.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',9,'REG','Registration','Y','Y','registration.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',11,'JOBS','Jobs','N','Y','jobs.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',17,'HELP','Help','N','Y','help.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',18,'FEEDBACK','Feedback','N','Y','feedback.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',1,'NEWS','Campus News','N','Y','campus-news.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',13,'ENTER','eNter','N','N','n2n.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',14,'ENQ','eNquire','Y','Y','n2n.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',15,'ENROUTE','eNroute','Y','Y','n2n.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',16,'EMER','Emergency Contacts','N','Y','dir.png','MAIN');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',19,'NOTIF','Notifications','Y','Y','notification_white.png','TOPLEFT');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',20,'FACEBOOK','Facebook','N','Y','facebook.png','BOT');
Insert into MOBEDU.MODULE (CAMPUSCODE,ID,CODE,DESCRIPTION,AUTHREQUIRED,SHOWBYDEFAULT,ICON,POSITION) values ('SUBR',21,'TWITTER','Twitter','N','Y','twitter-white.png','BOT');
