REM INSERTING into MOBEDU.MODULES
SET DEFINE OFF;
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'ATHLETICS','Athletics');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'EVENTS','Events');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'MAPS','Maps');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'COURSES','Courses');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'DIRECTORY','Directory');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'PROFILE','Profile');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'ACCOUNTS','Accounts');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'REG','Registration');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'JOBS','Jobs');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'HELP','Help');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'FEEDBACK','Feedback');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'NEWS','News');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'ENTER','Enter');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'ENQ','Enquire');
Insert into MOBEDU.MODULES (ID,MODULE_CODE,MODULE_DESC) values (1,'ENROUTE','Enroute');
