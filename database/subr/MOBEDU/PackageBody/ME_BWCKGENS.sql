PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.me_bwckgens
CREATE OR REPLACE PACKAGE BODY me_bwckgens AS
  --AUDIT_TRAIL_MSGKEY_UPDATE
  -- PROJECT : MSGKEY
  -- MODULE  : BWCKGEN1
  -- SOURCE  : enUS
  -- TARGET  : I18N
  -- DATE    : Wed Apr 15 10:57:30 2009
  -- MSGSIGN : #c0a90f1418d69c2f
  --TMI18N.ETR DO NOT CHANGE--
  --
  -- FILE NAME..: bwckgen1.sql
  -- RELEASE....: 8.2
  -- OBJECT NAME: BWCKGENS
  -- PRODUCT....: SCOMWEB
  -- COPYRIGHT..: Copyright (C) SunGard Higher Education Corporation 2002-2009. All rights reserved.
  --
  -- variables visible only in this package or any items which must
  -- be maintined throughoutb a session or across transactions.

  curr_release CONSTANT VARCHAR2(10) := '8.2';
  -- InfoText entered in webtailor to be displayed as the labels for the search criteria on the
  -- class schedule search criteria page?
  SUBJECT_LABEL      CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'SUBJECT');
  COURSE_LABEL       CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'COURSE');
  TITLE_LABEL        CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'TITLE');
  SCHEDULE_LABEL     CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'SCHEDULE');
  INST_MTH_LABEL     CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'INST_MTH');
  CREDITS_LABEL      CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'CREDITS');
  CREDITS_FROM_LABEL CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'CREDITS_FROM');
  CREDITS_TO_LABEL   CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'CREDITS_TO');
  CAMPUS_LABEL       CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'CAMPUS');
  LEVEL_LABEL        CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'LEVEL');
  TERM_PART1_LABEL   CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'TERM_PART1');
  TERM_PART2_LABEL   CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'TERM_PART2');
  DURATION_LABEL     CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'DURATION');
  INSTRUCTOR_LABEL   CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'INSTRUCTOR');
  SESSION_LABEL      CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'SESSION');
  ATTRIBUTE_LABEL    CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'ATTRIBUTE');
  START_TIME_LABEL   CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'START_TIME');
  END_TIME_LABEL     CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'END_TIME');
  DAYS_LABEL         CONSTANT VARCHAR2(100) := bwckfrmt.f_dispinfo('bwckschd.schedule_search_label_text',
                                                                   'DAYS');

  row_count            NUMBER;
  global_pidm          spriden.spriden_pidm%TYPE;
  term                 stvterm.stvterm_code%TYPE;
  student_name         VARCHAR2(185);
  stvterm_rec          stvterm%ROWTYPE;
  sorrtrm_rec          sorrtrm%ROWTYPE;
  sfbetrm_rec          sfbetrm%ROWTYPE;
  sgbstdn_rec          sgbstdn%ROWTYPE;
  scbcrse_row          scbcrse%ROWTYPE;
  term_desc            stvterm%ROWTYPE;
  sfrbtch_row          sfrbtch%ROWTYPE;
  tbrcbrq_row          tbrcbrq%ROWTYPE;
  sftregs_rec          sftregs%ROWTYPE;
  astd_prevent_reg_ind STVASTD.STVASTD_PREVENT_REG_IND%TYPE;
  mhrs_max_hrs         SFRMHRS.SFRMHRS_MAX_HRS%TYPE;
  mhrs_min_hrs         SFRMHRS.SFRMHRS_MIN_HRS%TYPE;
  minh_srce            sfbetrm.sfbetrm_minh_srce_cde%TYPE;
  maxh_srce            sfbetrm.sfbetrm_maxh_srce_cde%TYPE;
  cast_prevent_reg_ind STVCAST.STVCAST_PREVENT_REG_IND%TYPE;
  STST_REG_IND         STVSTST.STVSTST_REG_IND%TYPE;
  sgrclsr_clas_code    STVCLAS.STVCLAS_CODE%TYPE;
  clas_desc            STVCLAS.STVCLAS_DESC%TYPE;
  advr_row             SOKLIBS.Advr_Rec;
  regs_date            DATE := TRUNC(SYSDATE);
  use_man_control      VARCHAR2(1);
  internal_code        gtvsdax.gtvsdax_internal_code%TYPE;
  gtvsdax_group        gtvsdax.gtvsdax_internal_code_group%TYPE;
  tmp_hist_ind         VARCHAR2(1) DEFAULT NULL;
  tmp_hyper_ind        VARCHAR2(1) DEFAULT NULL;
  g_from_date          DATE;
  g_to_date            DATE;
  lcur_levl            sorlcur.sorlcur_levl_code%TYPE;
  lcur_term_admit      sorlcur.sorlcur_term_code_admit%TYPE;

  CURSOR regcrsec(pidm_in     spriden.spriden_pidm%TYPE,
                  term_in     stvterm.stvterm_code%TYPE DEFAULT NULL,
                  crn_in      ssbsect.ssbsect_crn%TYPE DEFAULT NULL,
                  hist_ind_in VARCHAR2 DEFAULT NULL) IS
    SELECT *
      FROM stvschd,
           stvcamp,
           stvrsts,
           ssbsect,
           sfrstcr,
           stvterm,
           scbcrse,
           sobterm
     WHERE sfrstcr_term_code = sobterm_term_code
       AND sobterm_dynamic_sched_term_ind = 'Y'
       AND sfrstcr_pidm = pidm_in
       AND sfrstcr_term_code = NVL(term_in, sfrstcr_term_code)
       AND sfrstcr_crn = NVL(crn_in, sfrstcr_crn)
       AND sfrstcr_crn = ssbsect_crn
       AND NVL(sfrstcr_error_flag, 'N') <> 'F'
       AND ssbsect_term_code = sfrstcr_term_code
       AND scbcrse_subj_code = ssbsect_subj_code
       AND scbcrse_crse_numb = ssbsect_crse_numb
       AND scbcrse_eff_term =
           (SELECT MAX(scbcrse_eff_term)
              FROM scbcrse x
             WHERE x.scbcrse_subj_code = ssbsect_subj_code
               AND x.scbcrse_crse_numb = ssbsect_crse_numb
               AND x.scbcrse_eff_term <= sfrstcr_term_code)
       AND stvcamp_code = sfrstcr_camp_code
       AND stvschd_code = ssbsect_schd_code
       AND stvrsts_code = sfrstcr_rsts_code
       AND stvterm_code = sfrstcr_term_code
       AND stvrsts_sb_print_ind =
           DECODE(hist_ind_in, 'Y', stvrsts_sb_print_ind, 'Y')
     ORDER BY ssbsect_term_code DESC,
              ssbsect_subj_code,
              ssbsect_crse_numb,
              ssbsect_seq_numb;

  CURSOR insmcrsec(code_in gtvinsm.gtvinsm_code%TYPE DEFAULT NULL) IS
    SELECT * FROM gtvinsm WHERE gtvinsm_code = code_in;

  insmcrsec_rec insmcrsec%ROWTYPE;

  CURSOR sylncrsec(term_in stvterm.stvterm_code%TYPE DEFAULT NULL,
                   crn_in  ssbsect.ssbsect_crn%TYPE DEFAULT NULL) IS
    SELECT *
      FROM ssrsyln
     WHERE ssrsyln_term_code = term_in
       AND ssrsyln_crn = crn_in;

  sylncrsec_rec sylncrsec%ROWTYPE;

  CURSOR aregcrsec(pidm_in spriden.spriden_pidm%TYPE,
                   term_in stvterm.stvterm_code%TYPE DEFAULT NULL,
                   crn_in  ssbsect.ssbsect_crn%TYPE DEFAULT NULL) IS
    SELECT *
      FROM sfrareg a
     WHERE sfrareg_term_code = term_in
       AND sfrareg_pidm = pidm_in
       AND sfrareg_crn = crn_in
       AND sfrareg_extension_number =
           (SELECT MAX(sfrareg_extension_number)
              FROM sfrareg x
             WHERE x.sfrareg_pidm = a.sfrareg_pidm
               AND x.sfrareg_crn = a.sfrareg_crn
               AND x.sfrareg_term_code = a.sfrareg_term_code);

  aregcrsec_rec aregcrsec%ROWTYPE;

  CURSOR course_hyperlink_c(term_in stvterm.stvterm_code%TYPE,
                            crn_in  ssbsect.ssbsect_crn%TYPE) IS
    SELECT 'Y'
      FROM ssbsect, stvsubj
     WHERE ssbsect_term_code = term_in
       AND ssbsect_crn = crn_in
       AND ssbsect_voice_avail = 'Y'
       AND stvsubj_code = ssbsect_subj_code
       AND stvsubj_disp_web_ind = 'Y';
  --

  FUNCTION f_grade_hold(pidm_in spriden.spriden_pidm%TYPE) RETURN BOOLEAN IS
  
    hold_ind VARCHAR2(1);
  
    CURSOR hold_ind_c(pidm_in spriden.spriden_pidm%TYPE) IS
      SELECT DISTINCT 'Y'
        FROM stvhldd
       WHERE stvhldd_grade_hold_ind = 'Y'
         AND stvhldd_code IN
             (SELECT sprhold_hldd_code
                FROM sprhold
               WHERE (TRUNC(SYSDATE) >= TRUNC(sprhold_from_date) AND
                     TRUNC(SYSDATE) <= TRUNC(sprhold_to_date))
                 AND sprhold_pidm = pidm_in);
  
  BEGIN
  
    OPEN hold_ind_c(pidm_in);
    FETCH hold_ind_c
      INTO hold_ind;
    IF hold_ind_c%FOUND THEN
      CLOSE hold_ind_c;
      RETURN TRUE;
    END IF;
    CLOSE hold_ind_c;
  
    RETURN FALSE;
  
  END f_grade_hold;

  --

  FUNCTION f_local_format_name(pidm_in NUMBER, name_type_in VARCHAR2)
    RETURN VARCHAR2 IS
    hold_name VARCHAR2(120) := NULL;
  BEGIN
    hold_name := f_format_name(pidm_in, name_type_in);
  
    IF hold_name LIKE '%NAME NOT FOUND%' THEN
      RETURN NULL;
    ELSE
      RETURN hold_name;
    END IF;
  END f_local_format_name;

  --

  FUNCTION f_24_hour_clock RETURN BOOLEAN IS
    CURSOR clock_type_c IS
      SELECT *
        FROM nls_session_parameters
       WHERE parameter = 'NLS_TIME_FORMAT';
  
    clock_type_rec clock_type_c%ROWTYPE;
  
  BEGIN
    OPEN clock_type_c;
    FETCH clock_type_c
      INTO clock_type_rec;
    CLOSE clock_type_c;
  
    IF clock_type_rec.VALUE LIKE 'HH24%' THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  
  END f_24_hour_clock;

  PROCEDURE P_DispCrseSchdDetl(crn     IN VARCHAR2 DEFAULT NULL,
                               term_in IN stvterm.stvterm_code%TYPE) IS
    row_count              NUMBER;
    scbcrse_row            scbcrse%ROWTYPE;
    name1                  VARCHAR2(60);
    cpinuse                VARCHAR2(1);
    webctinuse             VARCHAR2(1);
    makewebctlink          VARCHAR2(1);
    webctlogin             VARCHAR2(200);
    webctlink              VARCHAR2(1000);
    tot_credit_hr          NUMBER;
    tot_bill_hr            NUMBER;
    tot_ceu                NUMBER;
    term                   stvterm.stvterm_code%TYPE;
    hold_beg_time          VARCHAR2(30);
    hold_end_time          VARCHAR2(30);
    not_registered_message VARCHAR2(60);
    genpidm                spriden.spriden_pidm%TYPE;
    table_opened           BOOLEAN := FALSE;
    hld_stvgmod_desc       STVGMOD.STVGMOD_DESC%TYPE;
    hld_stvlevl_desc       STVLEVL.STVLEVL_DESC%TYPE;
    call_path              VARCHAR2(1);
    lv_wl_notification_ref sb_wl_notification.wl_notification_ref;
    lv_wl_notification_rec sb_wl_notification.wl_notification_rec;
    lv_wl_section_ctrl_ref sb_wl_section_ctrl.wl_section_ctrl_ref;
    lv_wl_section_ctrl_rec sb_wl_section_ctrl.wl_section_ctrl_rec;
  
    CURSOR tot_credit_hr_c(pidm_in spriden.spriden_pidm%TYPE,
                           term_in stvterm.stvterm_code%TYPE) IS
      SELECT SUM(DECODE(stvlevl_ceu_ind, 'Y', 0, NVL(sfrstcr_credit_hr, 0)))
        FROM stvlevl, sfrstcr
       WHERE stvlevl_code = sfrstcr_levl_code
         AND sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND ((sfrstcr_error_flag <> 'F' OR sfrstcr_error_flag IS NULL) OR
             (sfrstcr_error_flag = 'F' AND sfrstcr_rmsg_cde = 'MAXI'));
  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
  
    IF NOT bwskflib.f_validviewterm(term_in, stvterm_rec, sorrtrm_rec) THEN
      NULL;
    END IF;
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm                := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                              'STUPIDM'),
                                          '999999999');
      call_path              := 'F';
      not_registered_message := g$_nls.get('BWCKGEN1-0000',
                                           'SQL',
                                           'No schedule available for selected term.');
    ELSE
      genpidm                := global_pidm;
      call_path              := 'S';
      not_registered_message := g$_nls.get('BWCKGEN1-0001',
                                           'SQL',
                                           'You are not currently registered for the term.');
    END IF;
  
    term      := term_in;
    row_count := 0;
  
    IF call_path = 'S' THEN
      bwckfrmt.p_open_doc('bwskfshd.P_CrseSchdDetl', term);
    END IF;
  
    twbkwbis.p_dispinfo('bwskfshd.P_CrseSchdDetl', 'DEFAULT');
    cpinuse := twbkwbis.f_fetchwtparam('cpinuse');
  
    IF cpinuse = 'Y' THEN
      makewebctlink := 'N';
    ELSE
      webctinuse := twbkwbis.f_fetchwtparam('WEBCTINUSE');
    
      IF webctinuse = 'Y' THEN
        makewebctlink := 'Y';
        webctlogin    := twbkwbis.f_fetchwtparam('WEBCTLOGIN');
      
        IF webctlogin IS NULL THEN
          makewebctlink := 'N';
        END IF;
      ELSE
        makewebctlink := 'N';
      END IF;
    END IF;
  
    FOR regcrse IN regcrsec(genpidm, term, crn, tmp_hist_ind) LOOP
      row_count := regcrsec%rowcount;
    
      sylncrsec_rec := NULL;
      OPEN sylncrsec(term, crn);
      FETCH sylncrsec
        INTO sylncrsec_rec;
      CLOSE sylncrsec;
    
      aregcrsec_rec := NULL;
      OPEN aregcrsec(genpidm, term, crn);
      FETCH aregcrsec
        INTO aregcrsec_rec;
      CLOSE aregcrsec;
    
      IF row_count = 1 THEN
        /*  Calculate total credit hours for Schedule By Day and Time */
        OPEN tot_credit_hr_c(genpidm, term);
        FETCH tot_credit_hr_c
          INTO tot_credit_hr;
        CLOSE tot_credit_hr_c;
        -- Prints header:
        --------
      
        twbkfrmt.p_printtext(g$_nls.get('BWCKGEN1-0002',
                                        'SQL',
                                        'Total Credit Hours') || ': ' ||
                             LTRIM(TO_CHAR(tot_credit_hr, '99990D990')));
        HTP.br;
        HTP.br;
      END IF;
    
      FOR scbcrse IN scklibs.scbcrsec(regcrse.ssbsect_subj_code,
                                      regcrse.ssbsect_crse_numb,
                                      term) LOOP
        scbcrse_row := scbcrse;
      END LOOP;
    
      IF makewebctlink = 'N' OR regcrse.ssbsect_intg_cde IS NULL THEN
        twbkfrmt.p_tableopen('DATADISPLAY',
                             cattributes  => 'SUMMARY="' ||
                                             g$_nls.get('BWCKGEN1-0003',
                                                        'SQL',
                                                        'This layout table is used to present the schedule course detail') || '"',
                             ccaption     => bwcklibs.f_course_title(term_in,
                                                                     regcrse.ssbsect_crn) ||
                                             ' - ' ||
                                             regcrse.ssbsect_subj_code || ' ' ||
                                             regcrse.ssbsect_crse_numb ||
                                             ' - ' ||
                                             regcrse.ssbsect_seq_numb);
      ELSE
        twbkfrmt.p_tableopen('DATADISPLAY',
                             cattributes => 'SUMMARY="' ||
                                            g$_nls.get('BWCKGEN1-0004',
                                                       'SQL',
                                                       'This layout table is used to present the schedule course detail') || '"');
        webctlink := bwcklibs.f_course_title(term_in, regcrse.ssbsect_crn) ||
                     ' - ' || regcrse.ssbsect_subj_code || ' ' ||
                     regcrse.ssbsect_crse_numb || ' - ' ||
                     regcrse.ssbsect_seq_numb;
        webctlink := twbkfrmt.f_printanchor(webctlogin,
                                            webctlink,
                                            '',
                                            '',
                                            bwckfrmt.f_anchor_focus(webctlogin));
        twbkfrmt.p_tableheader(twbkfrmt.f_printtext(webctlink),
                               ccolspan => 3);
      END IF;
    
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0005',
                                           'SQL',
                                           'Associated Term:'),
                                ccolspan => 2);
      twbkfrmt.p_tabledata(bwcklibs.f_term_desc(term_in));
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                     g$_nls.get('BWCKGEN1-0006',
                                                                'SQL',
                                                                'Course Reference Number') || '">' ||
                                                     g$_nls.get('BWCKGEN1-0007',
                                                                'SQL',
                                                                'CRN') ||
                                                     '</ACRONYM>') || ':',
                                ccolspan => 2);
      twbkfrmt.p_tabledata(regcrse.ssbsect_crn);
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0008',
                                           'SQL',
                                           'Status:'),
                                ccolspan => 2);
      twbkfrmt.p_tabledata(g$_nls.get('BWCKGEN1-0009',
                                      'SQL',
                                      '%01% on %02%',
                                      regcrse.stvrsts_desc,
                                      TO_CHAR(regcrse.sfrstcr_rsts_date,
                                              twbklibs.date_display_fmt)));
      twbkfrmt.p_tablerowclose;
    
      -- WaitList automation enhancement begins.
      IF sfkwlat.f_display_position(term, regcrse.ssbsect_crn) = 'Y' AND
         sfkwlat.f_wl_automation_active(term, regcrse.ssbsect_crn) = 'Y' AND
         regcrse.stvrsts_voice_type = 'L' THEN
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0010',
                                             'SQL',
                                             'Waitlist Position:'),
                                  ccolspan => 2);
        twbkfrmt.p_tabledata(sfkwlat.f_get_wl_pos(p_pidm => genpidm,
                                                  p_term => term_in,
                                                  p_crn  => regcrse.ssbsect_crn));
        twbkfrmt.p_tablerowclose;
        lv_wl_notification_ref := sb_wl_notification.f_query_one(p_term_code => term_in,
                                                                 p_crn       => regcrse.ssbsect_crn,
                                                                 p_pidm      => genpidm);
        FETCH lv_wl_notification_ref
          INTO lv_wl_notification_rec;
        IF lv_wl_notification_ref%NOTFOUND THEN
          lv_wl_notification_rec.r_end_date := NULL;
        END IF;
        CLOSE lv_wl_notification_ref;
      
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0011',
                                             'SQL',
                                             'Notification Expires:'),
                                  ccolspan => 2);
        twbkfrmt.p_tabledata(TO_CHAR(lv_wl_notification_rec.r_end_date,
                                     twbklibs.twgbwrul_rec.twgbwrul_date_fmt) || ' ' ||
                             TO_CHAR(lv_wl_notification_rec.r_end_date,
                                     twbklibs.twgbwrul_rec.twgbwrul_time_fmt));
        twbkfrmt.p_tablerowclose;
      END IF;
      -- Waitlist automation enhancement ends.
    
      IF sfkolrl.f_open_learning_course(term_in, regcrse.ssbsect_crn) THEN
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0012',
                                             'SQL',
                                             'Class Start Date:'),
                                  ccolspan => 2);
        twbkfrmt.p_tabledata(TO_CHAR(aregcrsec_rec.sfrareg_start_date,
                                     twbklibs.date_display_fmt));
        twbkfrmt.p_tablerowclose;
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0013',
                                             'SQL',
                                             'Expected Completion Date:'),
                                  ccolspan => 2);
        twbkfrmt.p_tabledata(TO_CHAR(aregcrsec_rec.sfrareg_completion_date,
                                     twbklibs.date_display_fmt));
        twbkfrmt.p_tablerowclose;
      END IF;
    
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0014',
                                           'SQL',
                                           'Assigned Instructor:'),
                                ccolspan => 2);
    
      bwckfrmt.p_instructor_links(regcrse.sfrstcr_term_code,
                                  regcrse.sfrstcr_crn,
                                  regcrse.ssbsect_ptrm_code,
                                  aregcrsec_rec.sfrareg_instructor_pidm,
                                  call_path);
    
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0015',
                                           'SQL',
                                           'Grade Mode:'),
                                ccolspan => 2);
      bwckfrmt.p_disp_grade_mode(term_in, regcrse.ssbsect_crn);
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0016',
                                           'SQL',
                                           'Credits:'),
                                ccolspan => 2);
      bwckfrmt.p_disp_credit_hours(term_in, regcrse.ssbsect_crn);
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0017',
                                           'SQL',
                                           'Level:'),
                                ccolspan => 2);
      bwckfrmt.p_disp_level(term_in, regcrse.ssbsect_crn);
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0018',
                                           'SQL',
                                           'Campus:'),
                                ccolspan => 2);
      twbkfrmt.p_tabledata(regcrse.stvcamp_desc);
      twbkfrmt.p_tablerowclose;
    
      IF sylncrsec_rec.ssrsyln_section_url IS NOT NULL THEN
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledatalabel(twbkfrmt.f_printtext(g$_nls.get('BWCKGEN1-0019',
                                                                  'SQL',
                                                                  'Course ') ||
                                                       '<ACRONYM title = "' ||
                                                       g$_nls.get('BWCKGEN1-0020',
                                                                  'SQL',
                                                                  'Uniform Resource Locator') || '">' ||
                                                       g$_nls.get('BWCKGEN1-0021',
                                                                  'SQL',
                                                                  'URL') ||
                                                       '</ACRONYM>' || ':'),
                                  ccolspan => 2);
        twbkfrmt.p_tabledata(twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(sylncrsec_rec.ssrsyln_section_url),
                                                    sylncrsec_rec.ssrsyln_section_url));
        twbkfrmt.p_tablerowclose;
      END IF;
    
      twbkfrmt.p_tableclose;
      bwckfrmt.p_disp_meeting_times(term_in, regcrse.ssbsect_crn);
    END LOOP;
  
    IF row_count = 0 THEN
      twbkfrmt.p_printmessage(not_registered_message);
    END IF;
  
    bwckfrmt.p_disp_back_anchor;
  END P_DispCrseSchdDetl;

  --------------------------------------------------------------------------

  PROCEDURE P_RegsCrseSearch(term_in          IN OWA_UTIL.ident_arr,
                             rsts             IN OWA_UTIL.ident_arr,
                             assoc_term_in    IN OWA_UTIL.ident_arr,
                             crn              IN OWA_UTIL.ident_arr,
                             start_date_in    IN OWA_UTIL.ident_arr,
                             end_date_in      IN OWA_UTIL.ident_arr,
                             subj             IN OWA_UTIL.ident_arr,
                             crse             IN OWA_UTIL.ident_arr,
                             sec              IN OWA_UTIL.ident_arr,
                             levl             IN OWA_UTIL.ident_arr,
                             cred             IN OWA_UTIL.ident_arr,
                             gmod             IN OWA_UTIL.ident_arr,
                             title            IN bwckcoms.varchar2_tabtype,
                             mesg             IN OWA_UTIL.ident_arr,
                             regs_row         NUMBER,
                             add_row          NUMBER,
                             wait_row         NUMBER,
                             subject_error_in IN BOOLEAN DEFAULT FALSE) IS
    subject_error BOOLEAN;
    i             INTEGER;
    term          stvterm.stvterm_code%TYPE := NULL;
    multi_term    BOOLEAN := TRUE;
  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
  
    subject_error := subject_error_in;
    term          := term_in(term_in.COUNT);
  
    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      /*  FACWEB-specific code */
    
      IF NOT multi_term THEN
        IF NOT bwlkilib.f_add_drp(term) THEN
          bwlkostm.p_facselterm(term, 'bwlkffcs.P_FacCrseSearch');
          RETURN;
        END IF;
      
        IF NOT
            bwcksams.f_regsstu(global_pidm, term, 'bwlkffcs.P_FacCrseSearch') THEN
          RETURN;
        END IF;
      
        IF NOT sfkvars.add_allowed THEN
          bwckfrmt.p_open_doc('bwlkffcs.P_FacCrseSearch',
                              term,
                              NULL,
                              multi_term,
                              term_in(1));
          twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0022',
                                             'SQL',
                                             'Registration is not allowed at this time'),
                                  'ERROR');
          twbkwbis.p_closedoc(curr_release);
          RETURN;
        END IF;
      END IF; /* multi_term check */
    
      bwckfrmt.p_open_doc('bwlkffcs.P_FacCrseSearch',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      HTP.formopen(twbkwbis.f_cgibin || 'bwckgens.P_RegsGetCrse',
                   cattributes => 'onSubmit="return checkSubmit()"');
      twbkwbis.p_dispinfo('bwlkffcs.P_FacCrseSearch', 'DEFAULT');
    
      IF subject_error = TRUE THEN
        twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0023',
                                           'SQL',
                                           'You must select at least ONE subject'),
                                'ERROR');
        subject_error := FALSE;
      END IF;
    ELSE
      /* STUWEB-specific code  */
    
      IF NOT multi_term THEN
        IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
          bwskflib.p_seldefterm(term, 'bwskfcls.P_CrseSearch');
          RETURN;
        END IF;
      
        IF NOT
            bwcksams.F_RegsStu(global_pidm, term, 'bwskfcls.P_CrseSearch') THEN
          RETURN;
        END IF;
      
        IF NOT sfkvars.add_allowed THEN
          bwckfrmt.p_open_doc('bwskfcls.P_CrseSearch',
                              term,
                              NULL,
                              multi_term,
                              term_in(1));
          twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0024',
                                             'SQL',
                                             'Registration is not allowed at this time'),
                                  'ERROR');
          twbkwbis.p_closedoc(curr_release);
          RETURN;
        END IF;
      END IF; /* multi_term check */
    
      bwckfrmt.p_open_doc('bwskfcls.P_CrseSearch',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      HTP.formopen(twbkwbis.f_cgibin || 'bwckgens.P_RegsGetCrse',
                   cattributes => 'onSubmit="return checkSubmit()"');
      twbkwbis.p_dispinfo('bwskfcls.P_CrseSearch', 'DEFAULT');
    
      IF subject_error = TRUE THEN
        twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0025',
                                           'SQL',
                                           'You must select at least ONE subject'),
                                'ERROR');
        subject_error := FALSE;
      END IF;
    END IF; /* END stu fac specific code sections */
  
    HTP.br;
    HTP.br;
    twbkfrmt.P_FormHidden('rsts', 'dummy');
    twbkfrmt.P_FormHidden('assoc_term_in', 'dummy');
    twbkfrmt.P_FormHidden('crn', 'dummy');
    twbkfrmt.P_FormHidden('start_date_in', 'dummy');
    twbkfrmt.P_FormHidden('end_date_in', 'dummy');
    twbkfrmt.P_FormHidden('subj', 'dummy');
    twbkfrmt.P_FormHidden('crse', 'dummy');
    twbkfrmt.P_FormHidden('sec', 'dummy');
    twbkfrmt.P_FormHidden('levl', 'dummy');
    twbkfrmt.P_FormHidden('gmod', 'dummy');
    twbkfrmt.P_FormHidden('cred', 'dummy');
    twbkfrmt.P_FormHidden('title', 'dummy');
    twbkfrmt.P_FormHidden('mesg', 'dummy');
    twbkfrmt.P_FormHidden('regs_row', regs_row);
    twbkfrmt.P_FormHidden('wait_row', wait_row);
    twbkfrmt.P_FormHidden('add_row', add_row);
  
    BEGIN
      i := 2;
    
      WHILE crn(i) IS NOT NULL LOOP
        BEGIN
          twbkfrmt.P_FormHidden('rsts', rsts(i));
          twbkfrmt.P_FormHidden('assoc_term_in', assoc_term_in(i));
          twbkfrmt.P_FormHidden('crn', crn(i));
          twbkfrmt.P_FormHidden('start_date_in', start_date_in(i));
          twbkfrmt.P_FormHidden('end_date_in', end_date_in(i));
          twbkfrmt.P_FormHidden('subj', subj(i));
          twbkfrmt.P_FormHidden('crse', crse(i));
          twbkfrmt.P_FormHidden('sec', sec(i));
          twbkfrmt.P_FormHidden('levl', levl(i));
          twbkfrmt.P_FormHidden('gmod', gmod(i));
          twbkfrmt.P_FormHidden('cred', cred(i));
          twbkfrmt.P_FormHidden('title', title(i));
          twbkfrmt.P_FormHidden('mesg', mesg(i));
          i := i + 1;
        EXCEPTION
          WHEN OTHERS THEN
            i := i + 1;
        END;
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
    bwckgens.P_Search(term_in);
    HTP.br;
    HTP.formsubmit(NULL,
                   g$_nls.get('BWCKGEN1-0026', 'SQL', 'Class Search'));
    HTP.formreset(g$_nls.get('BWCKGEN1-0027', 'SQL', 'Reset'));
    HTP.formclose;
    twbkwbis.p_closedoc(curr_release);
  END P_RegsCrseSearch;

  --------------------------------------------------------------------------

  PROCEDURE P_RegsUpd(term     IN stvterm.stvterm_code%TYPE DEFAULT NULL,
                      crn      IN OWA_UTIL.ident_arr,
                      cred     IN OWA_UTIL.ident_arr,
                      gmod     IN OWA_UTIL.ident_arr,
                      levl     IN OWA_UTIL.ident_arr,
                      cred_old IN OWA_UTIL.ident_arr,
                      gmod_old IN OWA_UTIL.ident_arr,
                      levl_old IN OWA_UTIL.ident_arr,
                      P_ERROR  OUT VARCHAR2) IS
    i               INTEGER := 2;
    scbcrse_row     scbcrse%ROWTYPE;
    err_code        OWA_UTIL.ident_arr;
    err_msg         OWA_UTIL.ident_arr;
    clas_code       sgrclsr.sgrclsr_clas_code%TYPE;
    genpidm         spriden.spriden_pidm%TYPE;
    capp_tech_error VARCHAR2(4);
    call_path       VARCHAR2(1);
    proc_called_by  VARCHAR2(50);
    error_exists    BOOLEAN := FALSE;
    drop_problems   sfkcurs.drop_problems_rec_tabtype;
    drop_failures   sfkcurs.drop_problems_rec_tabtype;
    multi_term_list OWA_UTIL.ident_arr;
  
  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
    dbms_output.put_line('ME_BWCKGENS: 1T');
    multi_term_list(1) := NULL;
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm        := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                      'STUPIDM'),
                                  '999999999');
      call_path      := 'F';
      proc_called_by := 'bwlkfrad.P_FacChangeCrseOpt';
    ELSE
      dbms_output.put_line('ME_BWCKGENS: 2T');
      genpidm        := global_pidm;
      call_path      := 'S';
      proc_called_by := 'bwskfreg.P_ChangeCrseOpt';
    END IF;
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      --  FACWEB-specific code
      IF NOT bwckcoms.f_reg_access_still_good(genpidm,
                                              term,
                                              call_path || global_pidm,
                                              proc_called_by) THEN
        RETURN;
      END IF;
      dbms_output.put_line('ME_BWCKGENS: 3');
      --  Check to see if add/drop activity is allowed for selected term
      IF NOT bwlkilib.f_add_drp(term) THEN
        bwckfrmt.p_open_doc('bwlkfrad.P_FacAddDropCrse', term);
        twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0028',
                                           'SQL',
                                           'Registration is not allowed for this term at this time'),
                                'ERROR');
        twbkwbis.p_closedoc(curr_release);
        RETURN;
      END IF;
      dbms_output.put_line('ME_BWCKGENS: 4');
      IF NOT bwcksams.F_RegsStu(global_pidm,
                                term,
                                'bwlkfrad.P_FacChangeCrseOpt') THEN
        RETURN;
      END IF;
    ELSE
      -- STUWEB-specific code
      dbms_output.put_line('ME_BWCKGENS: 3TT' || TERM);
      begin
        IF NOT bwckcoms.f_reg_access_still_good(genpidm,
                                                term,
                                                call_path || global_pidm,
                                                proc_called_by) THEN
          RETURN;
        END IF;
      exception
        when others then
          P_ERROR := 'You have made too many attempts to register this term. Contact your registrar for assistance';
          return;
      end;
      IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
        bwskflib.p_seldefterm(term, 'bwskfreg.p_changecrseopt');
        RETURN;
      END IF;
    
      /*IF NOT bwcksams.F_RegsStu (
            global_pidm,
            term,
            'bwskfreg.p_changecrseopt'
         )
      THEN
         RETURN;
      END IF;*/
    END IF; /* END stu fac specific code sections */
  
    err_code(1) := 'dummy';
    err_msg(1) := 'dummy';
    dbms_output.put_line('ME_BWCKGENS: 4T');
    FOR i IN 2 .. crn.count LOOP
      dbms_output.put_line('ME_BWCKGENS: 4.1');
      BEGIN
        IF NOT goklibs.f_isnumber(cred(i)) THEN
          RAISE bwcklibs.inv_cred_hrs;
        END IF;
      
        IF nvl(TO_NUMBER(cred(i)), 0) = nvl(TO_NUMBER(cred_old(i)), 0) AND
           nvl(gmod(i), 'X') = nvl(gmod_old(i), 'X') AND
           nvl(levl(i), 'X') = nvl(levl_old(i), 'X') THEN
          NULL;
          dbms_output.put_line('ME_BWCKGENS: 5');
        ELSE
          dbms_output.put_line('ME_BWCKGENS: 5.1');
          IF TO_NUMBER(cred(i)) <> TO_NUMBER(cred_old(i)) THEN
            FOR ssbsect IN ssklibs.ssbsectc(crn(i), term) LOOP
              OPEN scklibs.scbcrsec(ssbsect.ssbsect_subj_code,
                                    ssbsect.ssbsect_crse_numb,
                                    term);
              FETCH scklibs.scbcrsec
                INTO scbcrse_row;
              dbms_output.put_line('ME_BWCKGENS: 6');
              IF scklibs.scbcrsec%NOTFOUND THEN
                scbcrse_row.scbcrse_credit_hr_ind  := NULL;
                scbcrse_row.scbcrse_credit_hr_low  := NULL;
                scbcrse_row.scbcrse_credit_hr_high := NULL;
                scbcrse_row.scbcrse_bill_hr_ind    := NULL;
                scbcrse_row.scbcrse_bill_hr_low    := NULL;
                scbcrse_row.scbcrse_bill_hr_high   := NULL;
              END IF;
            
              CLOSE scklibs.scbcrsec;
            END LOOP;
          
            IF scbcrse_row.scbcrse_credit_hr_ind = 'TO' THEN
              IF TO_NUMBER(cred(i)) <
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_low) OR
                 TO_NUMBER(cred(i)) >
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_high) THEN
                RAISE bwcklibs.inv_cred_hrs;
              END IF;
            ELSE
              IF TO_NUMBER(cred(i)) <>
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_low) AND
                 TO_NUMBER(cred(i)) <>
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_high) THEN
                RAISE bwcklibs.inv_cred_hrs;
              END IF;
            END IF;
          
            IF scbcrse_row.scbcrse_bill_hr_ind = 'TO' THEN
              IF TO_NUMBER(cred(i)) BETWEEN
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_low) AND
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_high) THEN
                sftregs_rec.sftregs_bill_hr := TO_NUMBER(cred(i));
              ELSE
                sftregs_rec.sftregs_bill_hr := NULL;
                dbms_output.put_line('ME_BWCKGENS: 7');
              END IF;
            ELSIF scbcrse_row.scbcrse_bill_hr_ind = 'OR' THEN
              IF TO_NUMBER(cred(i)) =
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_low) OR
                 TO_NUMBER(cred(i)) =
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_high) THEN
                sftregs_rec.sftregs_bill_hr := TO_NUMBER(cred(i));
              ELSE
                sftregs_rec.sftregs_bill_hr := NULL;
              END IF;
            END IF;
          
          ELSE
            sftregs_rec.sftregs_bill_hr := NULL;
          END IF;
          dbms_output.put_line('ME_BWCKGENS: 8');
          bwcklibs.p_initvalue(global_pidm, term, '', SYSDATE, '', '');
        
          FOR sgbstdn IN sgklibs.sgbstdnc(genpidm, term) LOOP
            dbms_output.put_line('ME_BWCKGENS: 9');
            sgbstdn_rec := sgbstdn;
          END LOOP;
        
          me_bwckregs.p_regschk(sgbstdn_rec);
          dbms_output.put_line('ME_BWCKGENS: 10');
          clas_code := me_bwckregs.f_getstuclas;
          dbms_output.put_line('ME_BWCKGENS: 11  ' || clas_code);
          FOR sfbetrm IN sfkcurs.sfbetrmc(genpidm, term) LOOP
            sfbetrm_rec := sfbetrm;
          END LOOP;
        
          me_bwckregs.p_regschk(sfbetrm_rec);
          dbms_output.put_line('ME_BWCKGENS: 12');
          sftregs_rec.sftregs_pidm      := genpidm;
          sftregs_rec.sftregs_term_code := term;
          sftregs_rec.sftregs_crn       := crn(i);
          sftregs_rec.sftregs_credit_hr := TO_NUMBER(cred(i));
          sftregs_rec.sftregs_gmod_code := gmod(i);
          sftregs_rec.sftregs_levl_code := levl(i);
          --
          -- Get the section information.
          -- ==============================
          me_bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                                   sftregs_rec.sftregs_crn,
                                   sftregs_rec.sftregs_sect_subj_code,
                                   sftregs_rec.sftregs_sect_crse_numb,
                                   sftregs_rec.sftregs_sect_seq_numb);
          --
          dbms_output.put_line('ME_BWCKGENS: 13');
          me_bwckregs.p_updcrse(sftregs_rec, sftregs_rec.sftregs_crn);
        END IF;
        dbms_output.put_line('ME_BWCKGENS: 14');
      EXCEPTION
        WHEN OTHERS THEN
          err_code(i) := SQLCODE;
          error_exists := TRUE;
          P_ERROR := BWCKLIBS.error_msg_table(SQLCODE);
      END;
    
    END LOOP;
  
    FOR i IN 2 .. crn.count LOOP
      FOR sftregs IN sfkcurs.sftregsc(genpidm, term) LOOP
        IF sftregs.sftregs_error_flag = 'F' AND
           sftregs.sftregs_crn = crn(i) THEN
          err_msg(i) := sftregs.sftregs_message;
          err_code(i) := '';
          sfkmods.p_reset_sftregs_fields(genpidm, term, crn(i), NULL);
          EXIT;
        END IF;
      END LOOP;
    END LOOP;
  
    me_bwckregs.p_allcrsechk(term,
                             'CHANGE',
                             capp_tech_error,
                             drop_problems,
                             drop_failures,
                             multi_term_list);
  
    -- Check for minimum hours error
    IF NVL(twbkwbis.f_getparam(genpidm, 'ERROR_FLAG'), 'N') = 'M' THEN
      -- don't pass other potential edit errors back to page for redisplay, as
      -- we are starting from scratch. sfrstcr was not updated in p_allcrsechk.
      bwcksams.P_DispChgCrseOpt2(term);
      RETURN;
    END IF;
  
    FOR i IN 2 .. crn.count LOOP
      FOR sftregs IN sfkcurs.sftregsc(genpidm, term) LOOP
        IF sftregs.sftregs_error_flag = 'F' AND
           sftregs.sftregs_crn = crn(i) THEN
          err_msg(i) := sftregs.sftregs_message;
          err_code(i) := '';
          error_exists := TRUE;
          EXIT;
        END IF;
      END LOOP;
    END LOOP;
  
    -- repopulate sftregs from sfrstcr if any errors found during
    -- registration checking
    -------------------------------------------------------------
    IF error_exists THEN
      IF NOT sfkfunc.f_registration_access(genpidm,
                                           term,
                                           'PROXY',
                                           call_path || global_pidm,
                                           bypass_admin_in => 'Y') THEN
        twbkwbis.p_dispinfo(proc_called_by, 'SESSIONBLOCKED');
        HTP.formclose;
        twbkwbis.p_closedoc(curr_release);
        RETURN;
      END IF;
    
    END IF;
    /*exception when others then
    P_ERROR:= BWCKLIBS.error_msg_table (SQLCODE);*/
    /* bwcksams.P_DispChgCrseOpt (
       term,
       crn,
       cred,
       gmod,
       levl,
       err_code,
       err_msg,
       FALSE,
       capp_tech_error
    );*/
  
  END P_RegsUpd;

  --------------------------------------------------------------------------
  PROCEDURE P_SearchChk(stdn_row_in      sgbstdn%ROWTYPE,
                        term_in          stvterm.stvterm_code%TYPE,
                        pidm_in          spriden.spriden_pidm%TYPE,
                        search_reg_allow IN OUT VARCHAR2) IS
    last_term_attended        STVTERM.STVTERM_CODE%TYPE;
    astd_code                 STVASTD.STVASTD_CODE%TYPE;
    cast_code                 STVCAST.STVCAST_CODE%TYPE;
    sql_error                 NUMBER := 0;
    SOBTERM_REC               SOBTERM%ROWTYPE;
    atmp_hrs                  VARCHAR2(1);
    restrict_time_ticket      VARCHAR2(1);
    restrict_time_ticket_code gtvsdax.gtvsdax_internal_code%TYPE := 'WEBRESTTKT';
    web_vr_ind                VARCHAR2(1) := 'W';
    internal_code             gtvsdax.gtvsdax_internal_code%TYPE;
    gtvsdax_group             gtvsdax.gtvsdax_internal_code_group%TYPE;
    sgbstdn_rec               sgbstdn%ROWTYPE;
    use_man_control           VARCHAR2(1);
    stvterm_rec               soklibs.update_term_select_c%ROWTYPE;
    sfbetrm_row               sfbetrm%ROWTYPE;
  
    CURSOR last_term_attended_c(term_in stvterm.stvterm_code%TYPE,
                                pidm_in spriden.spriden_pidm%TYPE) IS
      SELECT GREATEST(NVL(MAX(sfbetrm_term_code), '000000'),
                      NVL(last_term_attended, '000000'))
        FROM sfbetrm, stvests
       WHERE sfbetrm_term_code < term_in
         AND sfbetrm_pidm = pidm_in
         AND sfbetrm_ests_code = stvests_code
         AND stvests_eff_headcount = 'Y';
  
  BEGIN
    search_reg_allow := 'Y';
    sgbstdn_rec      := stdn_row_in;
    sql_error        := 0;
    --
    -- Check if person is deceased.
    -- =====================================================
    IF me_bwckregs.f_deceasedperscheck(sql_error, pidm_in) THEN
      search_reg_allow := 'N';
      RETURN;
    END IF;
  
    -- This is the mod to use the VR registration management
    -- controls in Web registration
    -- =====================================================
    internal_code := 'WEBMANCONT';
    gtvsdax_group := 'WEBREG';
    OPEN SFKCURS.get_gtvsdaxC(internal_code, gtvsdax_group);
    FETCH SFKCURS.get_gtvsdaxC
      INTO use_man_control;
    CLOSE SFKCURS.get_gtvsdaxC;
    OPEN sfkcurs.get_gtvsdaxc(restrict_time_ticket_code, gtvsdax_group);
    FETCH sfkcurs.get_gtvsdaxc
      INTO restrict_time_ticket;
    CLOSE sfkcurs.get_gtvsdaxc;
  
    IF NOT sfkrctl.f_check_reg_appointment(sgbstdn_rec.sgbstdn_pidm,
                                           term_in,
                                           use_man_control,
                                           restrict_time_ticket,
                                           web_vr_ind) THEN
      search_reg_allow := 'N';
      RETURN;
    END IF;
  
    --
  
    IF me_bwckregs.f_stuhld(sql_error, sgbstdn_rec.sgbstdn_pidm) THEN
      search_reg_allow := 'N';
      RETURN;
    END IF;
  
    row_count := 0;
  
    FOR stvstst IN stkstst.stvststc(sgbstdn_rec.sgbstdn_stst_code) LOOP
      stst_reg_ind := stvstst.stvstst_reg_ind;
    
      IF stst_reg_ind = 'Y' THEN
        row_count := row_count + 1;
      END IF;
    END LOOP;
  
    IF row_count <> 1 THEN
      search_reg_allow := 'N';
      RETURN;
    END IF;
  
    FOR SHRTTRM IN SHKLIBS.SHRTTRMC(SGBSTDN_REC.SGBSTDN_PIDM, term_in) LOOP
      last_term_attended := SHRTTRM.SHRTTRM_TERM_CODE;
      astd_code          := SHRTTRM.SHRTTRM_ASTD_CODE_END_OF_TERM;
      cast_code          := SHRTTRM.SHRTTRM_CAST_CODE;
    END LOOP;
  
    IF SGBSTDN_REC.SGBSTDN_ASTD_CODE IS NOT NULL AND
       SGBSTDN_REC.SGBSTDN_TERM_CODE_EFF = term_in AND
       SGBSTDN_REC.SGBSTDN_TERM_CODE_ASTD = term_in THEN
      astd_code := SGBSTDN_REC.SGBSTDN_ASTD_CODE;
    END IF;
  
    IF SGBSTDN_REC.SGBSTDN_CAST_CODE IS NOT NULL AND
       SGBSTDN_REC.SGBSTDN_TERM_CODE_EFF = term_in AND
       SGBSTDN_REC.SGBSTDN_TERM_CODE_CAST = term_in THEN
      cast_code := SGBSTDN_REC.SGBSTDN_CAST_CODE;
    END IF;
  
    IF cast_code IS NOT NULL THEN
      FOR STVCAST IN STKCAST.STVCASTC(cast_code) LOOP
        cast_prevent_reg_ind := STVCAST.STVCAST_PREVENT_REG_IND;
      END LOOP;
    
      IF cast_prevent_reg_ind = 'Y' THEN
        search_reg_allow := 'N';
        RETURN;
      END IF;
    ELSIF astd_code IS NOT NULL THEN
      FOR STVASTD IN STKASTD.STVASTDC(astd_code) LOOP
        astd_prevent_reg_ind := STVASTD.STVASTD_PREVENT_REG_IND;
      END LOOP;
    
      IF astd_prevent_reg_ind = 'Y' THEN
        search_reg_allow := 'N';
        RETURN;
      END IF;
    END IF;
  
    sfksels.p_get_min_max_by_curric_stand(p_term         => term_in,
                                          p_pidm         => SGBSTDN_REC.SGBSTDN_PIDM,
                                          p_seq_no       => NULL,
                                          p_astd_code    => astd_code,
                                          p_cast_code    => cast_code,
                                          p_min_hrs_out  => mhrs_min_hrs,
                                          p_max_hrs_out  => mhrs_max_hrs,
                                          p_min_srce_out => minh_srce,
                                          p_max_srce_out => maxh_srce);
  
    bwckgens.sobterm_row.sobterm_fee_assessmnt_eff_date := GREATEST(regs_date,
                                                                    NVL(bwckgens.sobterm_row.sobterm_fee_assessmnt_eff_date,
                                                                        regs_date));
  
    OPEN soklibs.sobterm_webc(term_in);
    FETCH soklibs.sobterm_webc
      INTO SOBTERM_REC;
  
    IF soklibs.sobterm_webc%NOTFOUND THEN
      atmp_hrs := '';
    ELSE
      IF sobterm_rec.sobterm_incl_attmpt_hrs_ind = 'Y' THEN
        atmp_hrs := 'A';
      ELSE
        atmp_hrs := '';
      END IF;
    END IF;
  
    CLOSE soklibs.sobterm_webc;
    --
    lcur_levl := sokccur.f_curriculum_value(p_pidm      => pidm_in,
                                            p_lmod_code => sb_curriculum_str.f_learner,
                                            p_term_code => term_in,
                                            p_key_seqno => 99,
                                            p_eff_term  => term_in,
                                            p_order     => 1,
                                            p_field     => 'LEVL');
    --
    soklibs.p_class_calc(pidm_in,
                         lcur_levl,
                         term_in,
                         atmp_hrs,
                         sgrclsr_clas_code,
                         clas_desc);
  
    FOR advr IN soklibs.advisorc(pidm_in, term_in) LOOP
      advr_row := advr;
    END LOOP;
  
    OPEN last_term_attended_c(term_in, pidm_in);
    FETCH last_term_attended_c
      INTO last_term_attended;
    CLOSE last_term_attended_c;
    --
    lcur_term_admit := sokccur.f_curriculum_value(p_pidm      => pidm_in,
                                                  p_lmod_code => sb_curriculum_str.f_learner,
                                                  p_term_code => term_in,
                                                  p_key_seqno => 99,
                                                  p_eff_term  => term_in,
                                                  p_order     => 1,
                                                  p_field     => 'TADMIT');
    --
    IF lcur_term_admit <> term_in AND
       sobterm_rec.sobterm_readm_req IS NOT NULL THEN
      IF NVL(last_term_attended, '0') < sobterm_rec.sobterm_readm_req THEN
        search_reg_allow := 'N';
        RETURN;
      END IF;
    END IF;
  
    IF search_reg_allow = 'N' THEN
      RETURN;
    END IF;
  
    --
    -- perform the etrm checks, if no reg restrictions have yet been found
    ----------------------------------------------------------------------
    BEGIN
      OPEN sfkcurs.sfbetrmc(pidm_in, term_in);
      FETCH sfkcurs.sfbetrmc
        INTO sfbetrm_row;
    
      IF sfkcurs.sfbetrmc%NOTFOUND THEN
        sfbetrm_row.sfbetrm_term_code := term_in;
      END IF;
    
      CLOSE sfkcurs.sfbetrmc;
      me_bwckregs.p_regschk(sfbetrm_row);
    EXCEPTION
      WHEN OTHERS THEN
        search_reg_allow := 'N';
    END;
  END P_SearchChk;

  --------------------------------------------------------------------------
  PROCEDURE p_searchchk_term(term_in           stvterm.stvterm_code%TYPE,
                             search_term_allow IN OUT VARCHAR2) IS
    stvterm_rec soklibs.update_term_select_c%ROWTYPE;
  BEGIN
    search_term_allow := 'Y';
    --
    -- Check if term is registerable or view only.
    -- And check if web registration open for current date.
    -- =====================================================
    OPEN soklibs.update_term_select_c(term_in);
    FETCH soklibs.update_term_select_c
      INTO stvterm_rec;
  
    IF soklibs.update_term_select_c%NOTFOUND THEN
      search_term_allow := 'N';
      CLOSE soklibs.update_term_select_c;
      RETURN;
    END IF;
  
    CLOSE soklibs.update_term_select_c;
  
    --
    -- Check if register code defined for the current term/date.
    -- =========================================================
    IF NOT bwcksams.F_ValidRegDate(term_in) THEN
      search_term_allow := 'N';
      RETURN;
    END IF;
  
  END p_searchchk_term;

  --
  -- P_ListCrse
  -- Procedure displays courses that are available for
  -- registration.
  -- ==================================================
  PROCEDURE P_ListCrse(term_in       IN OWA_UTIL.ident_arr,
                       sel_subj      IN OWA_UTIL.ident_arr,
                       sel_crse      IN VARCHAR2,
                       sel_title     IN VARCHAR2,
                       begin_hh      IN VARCHAR2,
                       begin_mi      IN VARCHAR2,
                       begin_ap      IN VARCHAR2,
                       sel_day       IN OWA_UTIL.ident_arr,
                       sel_ptrm      IN OWA_UTIL.ident_arr,
                       end_hh        IN VARCHAR2,
                       end_mi        IN VARCHAR2,
                       end_ap        IN VARCHAR2,
                       sel_camp      IN OWA_UTIL.ident_arr,
                       sel_schd      IN OWA_UTIL.ident_arr,
                       sel_sess      IN OWA_UTIL.ident_arr,
                       sel_instr     IN OWA_UTIL.ident_arr,
                       sel_attr      IN OWA_UTIL.ident_arr,
                       crn           IN OWA_UTIL.ident_arr,
                       rsts          IN OWA_UTIL.ident_arr,
                       sel_dunt_code IN VARCHAR2 DEFAULT NULL,
                       sel_dunt_unit IN VARCHAR2 DEFAULT NULL,
                       sel_levl      IN OWA_UTIL.ident_arr,
                       sel_from_cred IN VARCHAR2 DEFAULT NULL,
                       sel_to_cred   IN VARCHAR2 DEFAULT NULL,
                       sel_insm      IN OWA_UTIL.ident_arr,
                       call_value_in IN VARCHAR2 DEFAULT NULL) IS
    search_array_index     INTEGER := 2;
    crn_index              INTEGER := 2;
    crse                   VARCHAR2(6);
    title                  VARCHAR2(32);
    begin_time             VARCHAR2(4);
    end_time               VARCHAR2(4);
    mon                    VARCHAR2(1) := NULL;
    tue                    VARCHAR2(1) := NULL;
    wed                    VARCHAR2(1) := NULL;
    thu                    VARCHAR2(1) := NULL;
    fri                    VARCHAR2(1) := NULL;
    sat                    VARCHAR2(1) := NULL;
    sun                    VARCHAR2(1) := NULL;
    hold_subj              stvsubj.stvsubj_code%TYPE;
    hold_crse              scbcrse.scbcrse_crse_numb%TYPE;
    hold_crn               ssbsect.ssbsect_crn%TYPE;
    sobptrm_row            sobptrm%ROWTYPE;
    subj_code              VARCHAR2(2000);
    schd_code              VARCHAR2(1000);
    camp_code              VARCHAR2(1000);
    ptrm_code              VARCHAR2(1000);
    sess_code              VARCHAR2(1000);
    attr_code              VARCHAR2(1000);
    attr_desc              VARCHAR2(1000);
    ssrattr_rec            ssrattr%ROWTYPE;
    attr_tbl               OWA_UTIL.ident_arr;
    camp_tbl               OWA_UTIL.ident_arr;
    insm_tbl               OWA_UTIL.ident_arr;
    instr_tbl              OWA_UTIL.ident_arr;
    levl_tbl               OWA_UTIL.ident_arr;
    schd_tbl               OWA_UTIL.ident_arr;
    sess_tbl               OWA_UTIL.ident_arr;
    hold_term_tbl          soklibs.chartabtyp;
    hold_crn_tbl           soklibs.chartabtyp;
    nxt_row                BOOLEAN;
    l_found                BOOLEAN;
    ssbxlst_row            ssbxlst%ROWTYPE;
    seats_avail            NUMBER;
    xseats_avail           NUMBER;
    xseats_enrl            NUMBER;
    xseats_max_enrl        NUMBER;
    search_reg_ind         VARCHAR2(1);
    checklist_count        NUMBER := 0;
    instr_name2            VARCHAR2(1000);
    attr_found             BOOLEAN;
    first1                 BOOLEAN;
    schd_found             BOOLEAN;
    camp_found             BOOLEAN;
    cred_found             BOOLEAN;
    insm_found             BOOLEAN;
    levl_found             BOOLEAN;
    dunt_found             BOOLEAN;
    sess_found             BOOLEAN;
    term                   stvterm.stvterm_code%TYPE;
    date_found             BOOLEAN;
    term_count             NUMBER;
    term_loop              BOOLEAN;
    title_search           ssrsyln.ssrsyln_long_course_title%TYPE;
    multi_term             BOOLEAN;
    i                      INTEGER;
    search_reg_allowed     search_reg_allowed_tabtype;
    facweb                 BOOLEAN := FALSE;
    facweb_regterm         search_reg_allowed_tabtype;
    facweb_regterm_overall BOOLEAN := TRUE;
    sfrrsts_found          BOOLEAN := FALSE;
    sfrrsts_ind            VARCHAR2(1);
    dispenroll             BOOLEAN := FALSE;
    dispwl                 BOOLEAN := FALSE;
    dispxl                 BOOLEAN := FALSE;
    search_term_ind        VARCHAR2(1);
    search_term_allowed    search_reg_allowed_tabtype;
  
    CURSOR sirasgn_c(term_code_in stvterm.stvterm_code%TYPE,
                     crn_in       ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM sirasgn, spriden
       WHERE sirasgn_pidm = spriden_pidm
         AND sirasgn_term_code = term_code_in
         AND sirasgn_crn = crn_in
         AND sirasgn_primary_ind = 'Y'
         AND spriden_change_ind IS NULL;
  
    CURSOR ssrattr_c(term_code_in stvterm.stvterm_code%TYPE,
                     crn_in       ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM stvattr, ssrattr
       WHERE ssrattr_attr_code = stvattr_code
         AND ssrattr_term_code = term_code_in
         AND ssrattr_crn = crn_in;
  
    CURSOR scrlevl_c(term_code_in stvterm.stvterm_code%TYPE,
                     subj_in      stvsubj.stvsubj_code%TYPE,
                     crse_in      scrlevl.scrlevl_crse_numb%TYPE) IS
      SELECT *
        FROM stvlevl, scrlevl
       WHERE scrlevl_levl_code = stvlevl_code
         AND scrlevl_subj_code = subj_in
         AND scrlevl_crse_numb = crse_in
         AND scrlevl_eff_term =
             (SELECT MAX(scrlevl_eff_term)
                FROM scrlevl
               WHERE scrlevl_subj_code = subj_in
                 AND scrlevl_crse_numb = crse_in
                 AND scrlevl_eff_term <= term_code_in);
  
    CURSOR ssrmeet_exists_c(term_code_in ssrmeet.ssrmeet_term_code%TYPE,
                            crn_in       ssrmeet.ssrmeet_crn%TYPE) IS
      SELECT 1
        FROM ssrmeet
       WHERE ssrmeet_term_code = term_code_in
         AND ssrmeet_crn = crn_in;
  
    CURSOR ssbsect_camp_c(term_in stvterm.stvterm_code%TYPE,
                          crn_in  ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM stvcamp, ssbsect
       WHERE stvcamp_code = ssbsect_camp_code
         AND ssbsect_term_code = term_in
         AND ssbsect_crn = crn_in
         AND ssbsect_camp_code IS NOT NULL;
  
    CURSOR ssbsect_sess_c(term_in stvterm.stvterm_code%TYPE,
                          crn_in  ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM stvsess, ssbsect
       WHERE stvsess_code = ssbsect_sess_code
         AND ssbsect_term_code = term_in
         AND ssbsect_crn = crn_in
         AND ssbsect_sess_code IS NOT NULL;
  
    CURSOR ssbsect_schd_c(term_in stvterm.stvterm_code%TYPE,
                          crn_in  ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM stvschd, ssbsect
       WHERE stvschd_code = ssbsect_schd_code
         AND ssbsect_term_code = term_in
         AND ssbsect_crn = crn_in
         AND ssbsect_schd_code IS NOT NULL;
  
    CURSOR ssbsect_insm_c(term_in stvterm.stvterm_code%TYPE,
                          crn_in  ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM gtvinsm, ssbsect
       WHERE gtvinsm_code = ssbsect_insm_code
         AND ssbsect_term_code = term_in
         AND ssbsect_crn = crn_in
         AND ssbsect_insm_code IS NOT NULL;
  
    CURSOR ssbsect_dunt_c(term_in  stvterm.stvterm_code%TYPE,
                          crn_in   ssbsect.ssbsect_crn%TYPE,
                          dunt_in  gtvdunt.gtvdunt_code%TYPE,
                          units_in ssbsect.ssbsect_number_of_units%TYPE) IS
      SELECT *
        FROM gtvdunt, ssbsect
       WHERE gtvdunt_code = ssbsect_dunt_code
         AND ssbsect_term_code = term_in
         AND ssbsect_crn = crn_in
         AND ssbsect_dunt_code = dunt_in
         AND ssbsect_number_of_units = units_in;
  
    CURSOR ssrsyln_c(term_in stvterm.stvterm_code%TYPE,
                     crn_in  ssbsect.ssbsect_crn%TYPE) IS
      SELECT *
        FROM ssrsyln
       WHERE ssrsyln_crn = crn_in
         AND ssrsyln_term_code = term_in;
  
    CURSOR scbcrse_cred_c(term_code_in stvterm.stvterm_code%TYPE,
                          subj_in      stvsubj.stvsubj_code%TYPE,
                          crse_in      scrlevl.scrlevl_crse_numb%TYPE) IS
      SELECT *
        FROM scbcrse
       WHERE scbcrse_subj_code = subj_in
         AND scbcrse_crse_numb = crse_in
         AND scbcrse_eff_term =
             (SELECT MAX(scbcrse_eff_term)
                FROM scbcrse
               WHERE scbcrse_subj_code = subj_in
                 AND scbcrse_crse_numb = crse_in
                 AND scbcrse_eff_term <= term_code_in);
  
    CURSOR crsesectc(term  VARCHAR2,
                     subj  VARCHAR2,
                     crse  VARCHAR2,
                     ptrm  VARCHAR2,
                     btime VARCHAR2,
                     etime VARCHAR2,
                     mon   VARCHAR2,
                     tue   VARCHAR2,
                     wed   VARCHAR2,
                     thu   VARCHAR2,
                     fri   VARCHAR2,
                     sat   VARCHAR2,
                     sun   VARCHAR2) IS
      SELECT *
        FROM stvssts, stvsubj, ssrmeet, ssbsect, scbcrse
       WHERE ssbsect_term_code = term
         AND (subj IS NULL OR subj = '%' OR
             INSTR(subj, CHR(9) || ssbsect_subj_code || CHR(9)) > 0)
         AND UPPER(ssbsect_crse_numb) LIKE UPPER(crse)
         AND (ssbsect_ptrm_code LIKE ptrm OR ptrm = '%' OR
             INSTR(ptrm, CHR(9) || ssbsect_ptrm_code || CHR(9)) > 0)
         AND ssbsect_voice_avail = 'Y'
         AND ssrmeet_term_code(+) = ssbsect_term_code
         AND ssrmeet_crn(+) = ssbsect_crn
         AND NVL(ssrmeet_begin_time(+), '0000') >= btime
         AND NVL(ssrmeet_end_time(+), '0000') <= etime
         AND NVL(ssrmeet_mon_day(+), '%') LIKE NVL(mon, '%')
         AND NVL(ssrmeet_tue_day(+), '%') LIKE NVL(tue, '%')
         AND NVL(ssrmeet_wed_day(+), '%') LIKE NVL(wed, '%')
         AND NVL(ssrmeet_thu_day(+), '%') LIKE NVL(thu, '%')
         AND NVL(ssrmeet_fri_day(+), '%') LIKE NVL(fri, '%')
         AND NVL(ssrmeet_sat_day(+), '%') LIKE NVL(sat, '%')
         AND NVL(ssrmeet_sun_day(+), '%') LIKE NVL(sun, '%')
         AND stvsubj_code = ssbsect_subj_code
         AND stvsubj_disp_web_ind = 'Y'
         AND stvssts_code = ssbsect_ssts_code
         AND stvssts_reg_ind = 'Y'
         AND stvssts_active_ind = 'A'
         AND ssbsect_subj_code = scbcrse_subj_code
         AND ssbsect_crse_numb = scbcrse_crse_numb
         AND scbcrse_eff_term =
             (SELECT MAX(scbcrse_eff_term)
                FROM scbcrse x
               WHERE x.scbcrse_subj_code = ssbsect_subj_code
                 AND x.scbcrse_crse_numb = ssbsect_crse_numb
                 AND x.scbcrse_eff_term <= ssbsect_term_code)
       ORDER BY stvsubj_desc,
                ssbsect_crse_numb,
                ssbsect_seq_numb,
                ssbsect_crn;
  
    CURSOR sfrrstsc(term_in VARCHAR2, ptrm_in VARCHAR2) IS
      SELECT 'X'
        FROM sfrrsts
       WHERE sfrrsts_rsts_code = SUBSTR(f_stu_getwebregsrsts('R'), 1, 2)
         AND sfrrsts_term_code = term_in
         AND sfrrsts_ptrm_code = ptrm_in
         AND trunc(sysdate) BETWEEN TRUNC(sfrrsts_start_date) AND
             TRUNC(sfrrsts_end_date);
  
    CURSOR ssrrstsc(term VARCHAR2, crn VARCHAR2) IS
      SELECT *
        FROM ssrrsts
       WHERE ssrrsts_rsts_code = SUBSTR(f_stu_getwebregsrsts('R'), 1, 2)
         AND ssrrsts_term_code = term
         AND ssrrsts_crn = crn;
  
    genpidm             spriden.spriden_pidm%TYPE;
    sirasgn_rec         sirasgn_c%ROWTYPE;
    scbcrse_cred_rec    scbcrse_cred_c%ROWTYPE;
    scrlevl_rec         scrlevl_c%ROWTYPE;
    ssbsect_camp_rec    ssbsect_camp_c%ROWTYPE;
    ssbsect_dunt_rec    ssbsect_dunt_c%ROWTYPE;
    ssbsect_insm_rec    ssbsect_insm_c%ROWTYPE;
    ssbsect_schd_rec    ssbsect_schd_c%ROWTYPE;
    ssbsect_sess_rec    ssbsect_sess_c%ROWTYPE;
    ssrattr_rec         ssrattr_c%ROWTYPE;
    ssrsyln_rec         ssrsyln_c%ROWTYPE;
    schd_web_search     BOOLEAN := FALSE;
    insm_web_search     BOOLEAN := FALSE;
    camp_web_search     BOOLEAN := FALSE;
    levl_web_search     BOOLEAN := FALSE;
    duration_web_search BOOLEAN := FALSE;
    instr_web_search    BOOLEAN := FALSE;
    sess_web_search     BOOLEAN := FALSE;
    attr_web_search     BOOLEAN := FALSE;
    last_term           stvterm.stvterm_code%TYPE;
    display_cols        NUMBER := 17;
    olr_rw_defined      BOOLEAN := FALSE;
  BEGIN
    --
    -- Take term_array input and find the max term and make it the associated
    -- term.
    -- ======================================================
    term := term_in(term_in.COUNT);
  
    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    ELSE
      multi_term := TRUE;
    END IF;
  
    --
    -- Get basic term info.
    -- ======================================================
  
    bwcklibs.p_getsobterm(term, sobterm_row);
  
    --
    -- Validate the user.
    -- ======================================================
    IF call_value_in <> 'UNSECURED' OR call_value_in IS NULL THEN
      /*IF NOT twbkwbis.f_validuser (global_pidm)
      THEN
         RETURN;
      END IF;*/
    
      --
      -- Determine if we're running facweb or stuweb. Use
      -- the appropriate pidm.
      -- ======================================================
      IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
        facweb  := TRUE;
        genpidm := TO_NUMBER(twbkwbis.f_getparam(global_pidm, 'STUPIDM'),
                             '999999999');
      ELSE
        genpidm := global_pidm;
      END IF;
    
      --
      -- Reset global variables.
      -- ======================================================
      BEGIN
        bwcklibs.p_initvalue(global_pidm, term, NULL, SYSDATE, NULL, NULL);
        -- ignore invalid term exception, as lookup classes
        -- is valid in non-registerable terms
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    
      --
      -- Check if registration is allowed, based on holds, academic
      -- standing, etc...
      -- ======================================================
      sfkvars.search_reg_allowed := FALSE;
    
      IF facweb THEN
        facweb_regterm_overall := FALSE;
      END IF;
    
      FOR i IN 1 .. term_in.COUNT LOOP
        --
        -- Get the student record.
        -- ======================================================
        FOR sgbstdn IN sgklibs.sgbstdnc(genpidm, term_in(i)) LOOP
          sgbstdn_rec := sgbstdn;
        END LOOP;
      
        p_searchchk(sgbstdn_rec, term_in(i), genpidm, search_reg_ind);
        p_searchchk_term(term_in(i), search_term_ind);
      
        IF search_reg_ind = 'Y' THEN
          search_reg_allowed(i) := TRUE;
        ELSE
          search_reg_allowed(i) := FALSE;
        END IF;
      
        IF search_term_ind = 'Y' THEN
          search_term_allowed(i) := TRUE;
        ELSE
          search_term_allowed(i) := FALSE;
        END IF;
      
        IF search_reg_ind = 'Y' AND search_term_ind = 'Y' THEN
          sfkvars.search_reg_allowed := TRUE;
        END IF;
      
        facweb_regterm(i) := TRUE;
      
        IF facweb THEN
          OPEN sfkcurs.faculty_stvtermc(global_pidm, term_in(i));
          FETCH sfkcurs.faculty_stvtermc
            INTO stvterm_rec;
          IF sfkcurs.faculty_stvtermc%FOUND and
             sobterm_row.sobterm_add_drp_web_upd_ind = 'Y' THEN
            facweb_regterm_overall := TRUE;
          ELSE
            facweb_regterm(i) := FALSE;
          END IF;
        
          CLOSE sfkcurs.faculty_stvtermc;
        END IF;
      
        twbkfrmt.P_FormHidden('TERM_IN', term_in(i));
      END LOOP;
    END IF;
  
    --
    -- Get the course and title that we're searching for. Append with
    -- % to do a LIKE query, unless call is from the catalog page link.
    -- ================================================================
    IF nvl(instr(sel_crse, '.'), 0) = 0 THEN
      crse := sel_crse || '%';
    ELSE
      crse := substr(sel_crse, 1, instr(sel_crse, '.') - 1);
    END IF;
    title := '%' || sel_title || '%';
  
    --
    -- Get the begin and end times, concatenating the hour and
    -- minute entered by the user.
    -- ======================================================+
    IF f_24_hour_clock THEN
      /* 24 hour clock */
      begin_time := LPAD(begin_hh, 2, '0') || LPAD(begin_mi, 2, '0');
      end_time   := LPAD(end_hh, 2, '0') || LPAD(end_mi, 2, '0');
      IF end_time = '0000' THEN
        end_time := '2359';
      END IF;
    ELSE
      /* 12 hour clock */
      --
      -- If a begin hour was not specified, then default to start
      -- of day.
      -- ======================================================
      IF LPAD(begin_hh, 2, '0') = '00' THEN
        begin_time := '0000';
      ELSE
        --
        -- Convert the time to military time. Use a bogus date for
        -- the to_date function.
        -- ======================================================
        begin_time := LPAD(TO_CHAR(TO_DATE(G$_DATE.NORMALISE_GREG_DATE('01-01-1998',
                                                                       'DD-MM-YYYY') ||
                                           LPAD(begin_hh, 2, '0') ||
                                           LPAD(begin_mi, 2, '0') ||
                                           UPPER(begin_ap) || 'M',
                                           'HHMIAM',
                                           'NLS_DATE_LANGUAGE=''AMERICAN'''),
                                   'HH24MI'),
                           4,
                           '0');
      END IF;
    
      --
      -- If an end hour was not specified, then default to end
      -- of day.
      -- ======================================================
      IF LPAD(end_hh, 2, '0') || LPAD(end_mi, 2, '0') = '0000' THEN
        end_time := '2359';
      ELSE
        --
        -- Convert the time to military time. Use a bogus date for
        -- the to_date function.
        -- ======================================================
        end_time := LPAD(TO_CHAR(TO_DATE(G$_DATE.NORMALISE_GREG_DATE('01-01-1998',
                                                                     'DD-MM-YYYY') ||
                                         LPAD(end_hh, 2, '0') ||
                                         LPAD(end_mi, 2, '0') ||
                                         UPPER(end_ap) || 'M',
                                         'HHMIAM',
                                         'NLS_DATE_LANGUAGE=''AMERICAN'''),
                                 'HH24MI'),
                         4,
                         '0');
      END IF;
    END IF; /* 24 hour clock condition */
  
    --
    -- Get the day of week that we're searching for.
    -- ======================================================
    <<get_day_loop>>
    FOR day_index IN 2 .. 8 LOOP
      IF sel_day.EXISTS(day_index) THEN
        IF sel_day(day_index) IS NOT NULL THEN
          IF sel_day(day_index) = 'm' THEN
            mon := 'M';
          ELSIF sel_day(day_index) = 't' THEN
            tue := 'T';
          ELSIF sel_day(day_index) = 'w' THEN
            wed := 'W';
          ELSIF sel_day(day_index) = 'r' THEN
            thu := 'R';
          ELSIF sel_day(day_index) = 'f' THEN
            fri := 'F';
          ELSIF sel_day(day_index) = 's' THEN
            sat := 'S';
          ELSIF sel_day(day_index) = 'u' THEN
            sun := 'U';
          END IF;
        END IF;
      END IF;
    END LOOP get_day_loop;
  
    --
    -- Convert search criteria arrays to concatenated strings.
    -- No_data_found exception will detect when last element
    -- of array has been read, and will reset pointer.
    -- ======================================================
    subj_code := NULL;
  
    BEGIN
    
      <<get_subj_loop>>
      WHILE (sel_subj(search_array_index) IS NOT NULL) LOOP
        IF sel_subj(search_array_index) = '%' THEN
          subj_code := '%';
          RAISE NO_DATA_FOUND;
        END IF;
      
        IF length(subj_code) > 1994 THEN
          twbkfrmt.P_PrintMessage(g$_nls.get('BWCKGEN1-0029',
                                             'SQL',
                                             'You have selected too many subjects. Results for this search include only subjects %01% through %02%.',
                                             sel_subj(2),
                                             sel_subj(search_array_index - 1)),
                                  'WARNING');
        
          search_array_index := 2;
          EXIT;
        END IF;
      
        subj_code          := subj_code || CHR(9) ||
                              sel_subj(search_array_index) || CHR(9);
        search_array_index := search_array_index + 1;
      END LOOP get_subj_loop;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        search_array_index := 2;
    END;
  
    --
    --  Change part of term array into part of term code.
    -- ===================================================================
  
    --
    ptrm_code := NULL;
  
    BEGIN
      WHILE (sel_ptrm(search_array_index) IS NOT NULL) LOOP
        IF sel_ptrm(search_array_index) = '%' THEN
          ptrm_code := '%';
          RAISE NO_DATA_FOUND;
        END IF;
      
        ptrm_code          := ptrm_code || CHR(9) ||
                              sel_ptrm(search_array_index) || CHR(9);
        search_array_index := search_array_index + 1;
      END LOOP;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        IF search_array_index = 2 THEN
          ptrm_code := '%';
        END IF;
      
        search_array_index := 2;
    END;
  
    --
    --  Change session array into part of session code.
    -- ===================================================================
    sess_code := NULL;
  
    BEGIN
      WHILE (sel_sess(search_array_index) IS NOT NULL) LOOP
        IF sel_sess(search_array_index) = '%' THEN
          sess_code := '%';
          RAISE NO_DATA_FOUND;
        END IF;
      
        sess_code          := sess_code || CHR(9) ||
                              sel_sess(search_array_index) || CHR(9);
        search_array_index := search_array_index + 1;
      END LOOP;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        search_array_index := 2;
    END;
  
    --
    -- In this case we're converting a search criteria array
    -- (for instructors) to another search criteria array.
    -- ======================================================
  
    bwcklibs.p_getsobterm(term, sobterm_row);
  
    -- Loop through sobterm for all terms chosen, if any
    -- search indicators are found set to yes for any
    -- term, then overall indicator is set to true
    -- ======================================================
  
    FOR i IN 1 .. term_in.COUNT LOOP
      FOR sobterm IN soklibs.sobterm_webc(term_in(i)) LOOP
        sobterm_row := sobterm;
      END LOOP;
    
      IF NVL(sobterm_row.sobterm_schd_web_search_ind, 'N') = 'Y' THEN
        schd_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_insm_web_search_ind, 'N') = 'Y' THEN
        insm_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_camp_web_search_ind, 'N') = 'Y' THEN
        camp_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_levl_web_search_ind, 'N') = 'Y' THEN
        levl_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_duration_web_srch_ind, 'N') = 'Y' THEN
        duration_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_instr_web_search_ind, 'N') = 'Y' THEN
        instr_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_sess_web_search_ind, 'N') = 'Y' THEN
        sess_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_attr_web_search_ind, 'N') = 'Y' THEN
        attr_web_search := TRUE;
      END IF;
    END LOOP;
  
    IF instr_web_search THEN
      instr_tbl(1) := sel_instr(1);
    
      BEGIN
        IF sel_instr(2) <> '%' THEN
          WHILE (sel_instr(search_array_index) IS NOT NULL) LOOP
            instr_tbl(search_array_index) := sel_instr(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            instr_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    ELSE
      instr_tbl(1) := sel_instr(1);
      instr_tbl(2) := '%';
      search_array_index := 2;
    END IF;
  
    --
    --  Change attributes array input variable into local array.
    -- ===================================================================
  
    IF attr_web_search THEN
      attr_tbl(1) := sel_attr(1);
    
      BEGIN
        IF sel_attr(2) <> '%' THEN
          WHILE (sel_attr(search_array_index) IS NOT NULL) LOOP
            attr_tbl(search_array_index) := sel_attr(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            attr_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    END IF;
  
    --
    --  Change level array input variable into local array.
    -- ===================================================================
  
    IF levl_web_search THEN
      levl_tbl(1) := sel_levl(1);
    
      BEGIN
        IF sel_levl(2) <> '%' THEN
          WHILE (sel_levl(search_array_index) IS NOT NULL) LOOP
            levl_tbl(search_array_index) := sel_levl(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            levl_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    END IF;
  
    --
    --  Change campus array input variable into local array.
    -- ===================================================================
    search_array_index := 2;
  
    IF camp_web_search THEN
      camp_tbl(1) := sel_camp(1);
    
      BEGIN
        IF sel_camp(search_array_index) <> '%' THEN
          WHILE (sel_camp(search_array_index) IS NOT NULL) LOOP
            camp_tbl(search_array_index) := sel_camp(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            camp_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    END IF;
  
    --
    --  Change session array input variable into local array.
    -- ===================================================================
    search_array_index := 2;
  
    IF sess_web_search THEN
      sess_tbl(1) := sel_sess(1);
    
      BEGIN
        IF sel_sess(search_array_index) <> '%' THEN
          WHILE (sel_sess(search_array_index) IS NOT NULL) LOOP
            sess_tbl(search_array_index) := sel_sess(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            sess_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    END IF;
  
    --
    --  Change schedule type array input variable into local array.
    -- ===================================================================
  
    IF schd_web_search THEN
      schd_tbl(1) := sel_schd(1);
    
      BEGIN
        IF sel_schd(2) <> '%' THEN
          WHILE (sel_schd(search_array_index) IS NOT NULL) LOOP
            schd_tbl(search_array_index) := sel_schd(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            schd_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    END IF;
  
    --
    --  Change instruction method array input variable into local array.
    -- ===================================================================
    search_array_index := 2;
  
    IF insm_web_search THEN
      insm_tbl(1) := sel_insm(1);
    
      BEGIN
        IF sel_insm(2) <> '%' THEN
          WHILE (sel_insm(search_array_index) IS NOT NULL) LOOP
            insm_tbl(search_array_index) := sel_insm(search_array_index);
            search_array_index := search_array_index + 1;
          END LOOP;
        ELSE
          RAISE NO_DATA_FOUND;
        END IF;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          IF search_array_index = 2 THEN
            insm_tbl(2) := '%';
          ELSE
            search_array_index := 2;
          END IF;
      END;
    END IF;
  
    --
    --  Set hidden form variables and initialize others.
    -- ===================================================================
    twbkfrmt.P_FormHidden('sel_crn', 'dummy');
    twbkfrmt.P_FormHidden('assoc_term_in', 'dummy');
    twbkfrmt.P_FormHidden('ADD_BTN', 'dummy');
    hold_subj := NULL;
    hold_crse := NULL;
    hold_crn  := NULL;
    l_found   := FALSE;
  
    IF NVL(SUBSTR(bwcklibs.f_getgtvsdaxrule('DISPENROLL', 'WEBREG'), 1, 1),
           'N') = 'Y' THEN
      dispenroll   := TRUE;
      display_cols := display_cols + 3;
    END IF;
  
    IF NVL(SUBSTR(bwcklibs.f_getgtvsdaxrule('DISPWL', 'WEBREG'), 1, 1), 'N') = 'Y' THEN
      dispwl       := TRUE;
      display_cols := display_cols + 3;
    END IF;
  
    IF NVL(SUBSTR(bwcklibs.f_getgtvsdaxrule('DISPXL', 'WEBREG'), 1, 1), 'N') = 'Y' THEN
      dispxl       := TRUE;
      display_cols := display_cols + 3;
    END IF;
  
    --
    -- Loop through the courses that meet the search criteria.
    -- If there is a date range coming into this than loop
    -- through the terms in the term array, if a term came in
    -- skip that part.  Term_loop will be set to FALSE at the
    -- end of the loop so that if a start_date_in is NULL meaning
    -- a term was entered, the while loop will only be executed
    -- once.
    -- ======================================================
    last_term := NULL;
  
    FOR i IN 1 .. term_in.COUNT LOOP
      BEGIN
      
        --
        --  This loop will either be executed once for a term that was
        --  entered or multiple times if there is a date range for each
        --  term associated.
        -- ===================================================================
      
        <<display_sections_loop>>
        FOR sect_rec IN crsesectc(term_in(i),
                                  subj_code,
                                  crse,
                                  ptrm_code,
                                  begin_time,
                                  end_time,
                                  mon,
                                  tue,
                                  wed,
                                  thu,
                                  fri,
                                  sat,
                                  sun) LOOP
          instr_name2 := NULL;
          attr_desc   := NULL;
        
          --
          -- Don't display duplicates.
          -- ======================================================
        
          IF hold_crn IS NOT NULL AND sect_rec.ssrmeet_crn IS NOT NULL AND
             sect_rec.ssrmeet_crn = hold_crn THEN
            GOTO next_row;
          END IF;
        
          --
          -- Get the course record.
          -- ======================================================
          --               FOR scbcrse IN scklibs.scbcrsec (
          --                                 sect_rec.ssbsect_subj_code,
          --                                 sect_rec.ssbsect_crse_numb,
          --                                 term_in (i)
          --                              )
          --               LOOP
          --                  scbcrse_row := scbcrse;
          --               END LOOP;
        
          --
          -- Procedurally (can't be done in the query) eliminate
          -- sections where title does not meet the search criteria.
          -- ======================================================
          IF title <> '%%' THEN
            title_search := bwcklibs.f_course_title(term_in(i),
                                                    sect_rec.ssbsect_crn);
          
            IF LOWER(title_search) NOT LIKE LOWER(title) THEN
              GOTO next_row;
            END IF;
          
          END IF;
        
          --
          -- If no meeting times were found in the query, then check
          -- to see if it's because the meeting times did not match
          -- the search criteria or if no meetings have yet been created.
          -- If the former, then eliminate the section.
          -- ======================================================
          row_count := 0;
        
          IF sect_rec.ssrmeet_crn IS NULL THEN
            OPEN ssrmeet_exists_c(term_in(i), sect_rec.ssbsect_crn);
            FETCH ssrmeet_exists_c
              INTO row_count;
            CLOSE ssrmeet_exists_c;
          
            IF row_count > 0 THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          -- Check for cross list courses.
          -- ======================================================
          BEGIN
            xseats_max_enrl := NULL;
            xseats_enrl     := NULL;
            xseats_avail    := NULL;
          
            SELECT B.*
              INTO ssbxlst_row
              FROM SSBXLST B, SSRXLST
             WHERE SSRXLST_CRN = sect_rec.ssbsect_crn
               AND SSRXLST_TERM_CODE = term_in(i)
               AND SSBXLST_XLST_GROUP = SSRXLST_XLST_GROUP
               AND SSRXLST_TERM_CODE = SSBXLST_TERM_CODE
               AND ROWNUM = 1;
          
            xseats_max_enrl := ssbxlst_row.ssbxlst_max_enrl;
            xseats_enrl     := ssbxlst_row.ssbxlst_enrl;
            xseats_avail    := ssbxlst_row.ssbxlst_seats_avail;
          
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;
        
          --
          --
          -- ======================================================
          seats_avail := sect_rec.ssbsect_seats_avail;
        
          --
          --
          -- ======================================================
          --               IF call_value_in = 'UNSECURED'
          --               THEN
          --                  NULL;
          --               ELSE
          IF (seats_avail <= 0 OR xseats_avail <= 0) AND
             NVL(sobterm_row.sobterm_closect_web_disp_ind, 'N') = 'N' THEN
            GOTO next_row;
          END IF;
          --               END IF;
        
          --
          -- Procedurally (can't be done in the query) eliminate
          -- sections where instructor does not meet the search criteria.
          -- ======================================================
        
          IF instr_web_search AND sel_instr(2) <> '%' THEN
            FOR sirasgn_rec IN sirasgn_c(term_in(i), sect_rec.ssbsect_crn) LOOP
              IF sirasgn_c%NOTFOUND THEN
                NULL;
              ELSE
                BEGIN
                  FOR search_array_index IN 2 .. instr_tbl.COUNT LOOP
                    IF TO_NUMBER(instr_tbl(search_array_index)) =
                       sirasgn_rec.sirasgn_pidm THEN
                      instr_name2 := sirasgn_rec.spriden_first_name || ' ' ||
                                     sirasgn_rec.spriden_mi || ' ' ||
                                     sirasgn_rec.spriden_last_name;
                      EXIT;
                    END IF;
                  END LOOP;
                EXCEPTION
                  WHEN OTHERS THEN
                    GOTO next_row;
                END;
              END IF;
            END LOOP;
          
            IF instr_name2 IS NULL THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          --  Eliminate any attributes that don't match if attributes
          --  were specified.  The following criteria were removed
          --  from the crsesectc cursor to speed up that cursor and
          --  they will only be hit if the sobterm record is set up
          --  that way.
          -- ===================================================================
        
          IF attr_web_search AND sel_attr(2) <> '%' THEN
            FOR ssrattr_rec IN ssrattr_c(term_in(i), sect_rec.ssbsect_crn) LOOP
              IF ssrattr_c%NOTFOUND THEN
                NULL;
              ELSE
                BEGIN
                  FOR search_array_index IN 2 .. attr_tbl.COUNT LOOP
                    IF attr_tbl(search_array_index) =
                       ssrattr_rec.ssrattr_attr_code THEN
                      IF attr_desc IS NOT NULL THEN
                        attr_desc := attr_desc || ' ' ||
                                     g$_nls.get('BWCKGEN1-0030',
                                                'SQL',
                                                'and') || ' ';
                      END IF;
                    
                      attr_desc := attr_desc || ssrattr_rec.stvattr_desc;
                    END IF;
                  END LOOP;
                EXCEPTION
                  WHEN OTHERS THEN
                    search_array_index := 2;
                END;
              END IF;
            END LOOP;
          
            IF attr_desc IS NULL THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          --  Eliminate any levels that don't match if levels
          --  were specified.
          -- ===================================================================
          levl_found := FALSE;
        
          IF levl_web_search AND sel_levl(2) <> '%' THEN
            BEGIN
              FOR scrlevl_rec IN scrlevl_c(term_in(i),
                                           sect_rec.ssbsect_subj_code,
                                           sect_rec.ssbsect_crse_numb) LOOP
                IF scrlevl_c%NOTFOUND THEN
                  NULL;
                ELSE
                  FOR search_array_index IN 2 .. levl_tbl.COUNT LOOP
                    IF levl_tbl(search_array_index) =
                       scrlevl_rec.scrlevl_levl_code THEN
                      levl_found := TRUE;
                    END IF;
                  END LOOP;
                END IF;
              END LOOP;
            
              IF levl_found = FALSE THEN
                GOTO next_row;
              END IF;
            EXCEPTION
              WHEN OTHERS THEN
                search_array_index := 2;
            END;
          END IF;
        
          --
          --  Eliminate any campuses that don't match if campuses
          --  were specified.
          -- ===================================================================
          IF camp_web_search AND camp_tbl(2) <> '%' THEN
            BEGIN
              camp_found := FALSE;
              OPEN ssbsect_camp_c(term_in(i), sect_rec.ssbsect_crn);
              FETCH ssbsect_camp_c
                INTO ssbsect_camp_rec;
            
              IF ssbsect_camp_c%NOTFOUND THEN
                NULL;
              ELSE
                FOR search_array_index IN 2 .. camp_tbl.COUNT LOOP
                  IF camp_tbl(search_array_index) =
                     ssbsect_camp_rec.ssbsect_camp_code THEN
                    camp_found := TRUE;
                  END IF;
                END LOOP;
              END IF;
            
              CLOSE ssbsect_camp_c;
            EXCEPTION
              WHEN OTHERS THEN
                IF ssbsect_camp_c%ISOPEN THEN
                  CLOSE ssbsect_camp_c;
                END IF;
              
                search_array_index := 2;
            END;
          
            IF camp_found = FALSE THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          --  Eliminate any sessions that don't match if sessions
          --  were specified.
          -- ===================================================================
          IF sess_web_search AND sess_tbl(2) <> '%' THEN
            BEGIN
              sess_found := FALSE;
              OPEN ssbsect_sess_c(term_in(i), sect_rec.ssbsect_crn);
              FETCH ssbsect_sess_c
                INTO ssbsect_sess_rec;
            
              IF ssbsect_sess_c%NOTFOUND THEN
                NULL;
              ELSE
                FOR search_array_index IN 2 .. sess_tbl.COUNT LOOP
                  IF sess_tbl(search_array_index) =
                     ssbsect_sess_rec.ssbsect_sess_code THEN
                    sess_found := TRUE;
                  END IF;
                END LOOP;
              END IF;
            
              CLOSE ssbsect_sess_c;
            EXCEPTION
              WHEN OTHERS THEN
                IF ssbsect_sess_c%ISOPEN THEN
                  CLOSE ssbsect_sess_c;
                END IF;
              
                search_array_index := 2;
            END;
          
            IF sess_found = FALSE THEN
              GOTO next_row;
            END IF;
          
            NULL;
          END IF;
        
          --
          --  Eliminate any schedule types that don't match if schedule types
          --  were specified.
          -- ===================================================================
        
          IF schd_web_search AND schd_tbl(2) <> '%' THEN
            BEGIN
              schd_found := FALSE;
              OPEN ssbsect_schd_c(term_in(i), sect_rec.ssbsect_crn);
              FETCH ssbsect_schd_c
                INTO ssbsect_schd_rec;
            
              IF ssbsect_schd_c%NOTFOUND THEN
                NULL;
              ELSE
                FOR search_array_index IN 2 .. schd_tbl.COUNT LOOP
                  IF schd_tbl(search_array_index) =
                     ssbsect_schd_rec.ssbsect_schd_code THEN
                    schd_found := TRUE;
                  END IF;
                END LOOP;
              END IF;
            
              CLOSE ssbsect_schd_c;
            EXCEPTION
              WHEN OTHERS THEN
                IF ssbsect_schd_c%ISOPEN THEN
                  CLOSE ssbsect_schd_c;
                END IF;
              
                search_array_index := 2;
            END;
          
            IF schd_found = FALSE THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          --  Eliminate any instructional methods that don't match if they
          --  were specified.
          -- ===================================================================
        
          IF insm_web_search AND insm_tbl(2) <> '%' THEN
            BEGIN
              insm_found := FALSE;
              OPEN ssbsect_insm_c(term_in(i), sect_rec.ssbsect_crn);
              FETCH ssbsect_insm_c
                INTO ssbsect_insm_rec;
            
              IF ssbsect_insm_c%NOTFOUND THEN
                NULL;
              ELSE
                FOR search_array_index IN 2 .. insm_tbl.COUNT LOOP
                  IF insm_tbl(search_array_index) =
                     ssbsect_insm_rec.ssbsect_insm_code THEN
                    insm_found := TRUE;
                  END IF;
                END LOOP;
              END IF;
            
              CLOSE ssbsect_insm_c;
            EXCEPTION
              WHEN OTHERS THEN
                IF ssbsect_insm_c%ISOPEN THEN
                  CLOSE ssbsect_insm_c;
                END IF;
              
                search_array_index := 2;
            END;
          
            IF insm_found = FALSE THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          --  Eliminate any credits that don't fall into any range that was
          --  specified.
          -- ===================================================================
        
          cred_found := FALSE;
        
          IF (sel_from_cred IS NOT NULL) AND (sel_to_cred IS NOT NULL) THEN
            OPEN scbcrse_cred_c(term_in(i),
                                sect_rec.ssbsect_subj_code,
                                sect_rec.ssbsect_crse_numb);
            FETCH scbcrse_cred_c
              INTO scbcrse_cred_rec;
          
            IF scbcrse_cred_c%NOTFOUND THEN
              cred_found := FALSE;
            ELSE
              IF (scbcrse_cred_rec.scbcrse_credit_hr_ind IS NULL AND
                 scbcrse_cred_rec.scbcrse_credit_hr_low <= sel_to_cred AND
                 scbcrse_cred_rec.scbcrse_credit_hr_low >= sel_from_cred) OR
                 (scbcrse_cred_rec.scbcrse_credit_hr_ind = 'OR' AND
                 ((scbcrse_cred_rec.scbcrse_credit_hr_low <= sel_to_cred AND
                 scbcrse_cred_rec.scbcrse_credit_hr_low >= sel_from_cred) OR
                 (scbcrse_cred_rec.scbcrse_credit_hr_high <= sel_to_cred AND
                 scbcrse_cred_rec.scbcrse_credit_hr_high >=
                 sel_from_cred))) THEN
                cred_found := TRUE;
              END IF;
            END IF;
          
            CLOSE scbcrse_cred_c;
          
            IF cred_found = FALSE THEN
              GOTO next_row;
            END IF;
          END IF;
        
          --
          --  Eliminate any duration codes/units that don't match if they
          --  were specified.
          -- ===================================================================
          IF duration_web_search THEN
            dunt_found := FALSE;
          
            IF (sel_dunt_code IS NOT NULL) AND (sel_dunt_unit IS NOT NULL) THEN
              BEGIN
                FOR ssbsect_dunt_rec IN ssbsect_dunt_c(term_in(i),
                                                       sect_rec.ssbsect_crn,
                                                       sel_dunt_code,
                                                       TO_NUMBER(sel_dunt_unit)) LOOP
                  IF ssbsect_dunt_c%NOTFOUND THEN
                    dunt_found := FALSE;
                  ELSE
                    dunt_found := TRUE;
                  END IF;
                END LOOP;
              EXCEPTION
                WHEN OTHERS THEN
                  NULL;
              END;
            
              IF dunt_found = FALSE THEN
                GOTO next_row;
              END IF;
            END IF;
          END IF;
        
          --
          -- Eliminate duplicate CRNs
          -- ======================================================
          search_array_index := 1;
        
          BEGIN
            WHILE (hold_crn_tbl(search_array_index) IS NOT NULL) LOOP
              IF hold_crn_tbl(search_array_index) = sect_rec.ssbsect_crn AND
                 hold_term_tbl(search_array_index) =
                 sect_rec.ssbsect_term_code THEN
                nxt_row := TRUE;
                EXIT;
              END IF;
            
              search_array_index := search_array_index + 1;
            END LOOP;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              nxt_row := FALSE;
              hold_crn_tbl(search_array_index) := sect_rec.ssbsect_crn;
              hold_term_tbl(search_array_index) := sect_rec.ssbsect_term_code;
          END;
        
          IF nxt_row THEN
            GOTO next_row;
          END IF;
        
          --
          --  If coming from unsecured dynamic schedule, then goto those
          --  display pages.
          -- ===================================================================
          IF call_value_in = 'UNSECURED' THEN
            IF hold_subj IS NULL THEN
              first1    := TRUE;
              last_term := NULL;
            ELSE
              first1 := FALSE;
            END IF;
          
            IF term_in.COUNT = 1 THEN
              last_term := term_in(i);
            END IF;
          
            l_found   := TRUE;
            hold_subj := sect_rec.ssbsect_subj_code;
            bwckschd.p_disp_course_unsec(last_term,
                                         first1,
                                         term_in(i),
                                         sect_rec.ssbsect_ptrm_code,
                                         crn,
                                         sect_rec.ssbsect_crn,
                                         sect_rec.ssbsect_subj_code,
                                         sect_rec.ssbsect_crse_numb,
                                         sect_rec.ssbsect_seq_numb,
                                         sect_rec.ssbsect_learner_regstart_fdate,
                                         sect_rec.ssbsect_learner_regstart_tdate);
            last_term := term_in(i);
          ELSE
            --
            -- Start the html table to display sections.
            -- ======================================================
            IF hold_subj IS NULL THEN
              twbkfrmt.p_tableopen('DATADISPLAY',
                                   cattributes  => 'SUMMARY="' ||
                                                   g$_nls.get('BWCKGEN1-0031',
                                                              'SQL',
                                                              'This layout table is used to present the sections found') ||
                                                   '" width="100%"',
                                   ccaption     => g$_nls.get('BWCKGEN1-0032',
                                                              'SQL',
                                                              'Sections Found'));
            END IF;
          
            --
            -- Display column headers when subject changes.
            -- ======================================================
            IF sect_rec.ssbsect_subj_code <> NVL(hold_subj, '#') OR
               (NVL(last_term, 0) <> term_in(i) AND term_in.COUNT > 1) THEN
              IF NVL(last_term, 0) <> term_in(i) AND term_in.COUNT > 1 THEN
                twbkfrmt.p_tablerowopen;
                twbkfrmt.p_tabledatalabel(twbkfrmt.f_printtext(bwcklibs.f_term_desc(sect_rec.ssbsect_term_code),
                                                               class_in => 'fieldOrangetextbold'),
                                          ccolspan => display_cols);
                twbkfrmt.p_tablerowclose;
              END IF;
            
              last_term := term_in(i);
              twbkfrmt.p_tablerowopen;
              twbkfrmt.p_tableheader(sect_rec.stvsubj_desc,
                                     ccolspan => display_cols);
              twbkfrmt.p_tablerowclose;
              twbkfrmt.p_tablerowopen;
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0033',
                                                    'SQL',
                                                    'Select'));
            
              IF multi_term THEN
                twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0034',
                                                      'SQL',
                                                      'Associated Term'));
              END IF;
            
              twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                              g$_nls.get('BWCKGEN1-0035',
                                                                         'SQL',
                                                                         'Course Reference Number') || '">' ||
                                                              g$_nls.get('BWCKGEN1-0036',
                                                                         'SQL',
                                                                         'CRN') ||
                                                              '</ACRONYM>'));
              twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                              g$_nls.get('BWCKGEN1-0037',
                                                                         'SQL',
                                                                         'Subject') || '>' ||
                                                              g$_nls.get('BWCKGEN1-0038',
                                                                         'SQL',
                                                                         'Subj') ||
                                                              '</ABBR>'));
              twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                              g$_nls.get('BWCKGEN1-0039',
                                                                         'SQL',
                                                                         'Course') || '>' ||
                                                              g$_nls.get('BWCKGEN1-0040',
                                                                         'SQL',
                                                                         'Crse') ||
                                                              '</ABBR>'));
              twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                              g$_nls.get('BWCKGEN1-0041',
                                                                         'SQL',
                                                                         'Section') || '>' ||
                                                              g$_nls.get('BWCKGEN1-0042',
                                                                         'SQL',
                                                                         'Sec') ||
                                                              '</ABBR>'));
              twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                              g$_nls.get('BWCKGEN1-0043',
                                                                         'SQL',
                                                                         'Campus') || '>' ||
                                                              g$_nls.get('BWCKGEN1-0044',
                                                                         'SQL',
                                                                         'Cmp') ||
                                                              '</ABBR>'));
              twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                              g$_nls.get('BWCKGEN1-0045',
                                                                         'SQL',
                                                                         'Credit Hours') || '">' ||
                                                              g$_nls.get('BWCKGEN1-0046',
                                                                         'SQL',
                                                                         'Cred') ||
                                                              '</ABBR>'));
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0047',
                                                    'SQL',
                                                    'Title'));
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0048',
                                                    'SQL',
                                                    'Days'));
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0049',
                                                    'SQL',
                                                    'Time'));
            
              IF dispenroll THEN
              
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0050',
                                                                           'SQL',
                                                                           'Section Capacity') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0051',
                                                                           'SQL',
                                                                           'Cap') ||
                                                                '</ABBR>'));
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0052',
                                                                           'SQL',
                                                                           'Section Actual') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0053',
                                                                           'SQL',
                                                                           'Act') ||
                                                                '</ABBR>'));
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0054',
                                                                           'SQL',
                                                                           'Section Remaining') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0055',
                                                                           'SQL',
                                                                           'Rem') ||
                                                                '</ABBR>'));
              END IF;
              IF dispwl THEN
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0056',
                                                                           'SQL',
                                                                           'Waitlist Capacity') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0057',
                                                                           'SQL',
                                                                           'WL Cap') ||
                                                                '</ABBR>'));
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0058',
                                                                           'SQL',
                                                                           'Waitlist Actual') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0059',
                                                                           'SQL',
                                                                           'WL Act') ||
                                                                '</ABBR>'));
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0060',
                                                                           'SQL',
                                                                           'Waitlist Remaining') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0061',
                                                                           'SQL',
                                                                           'WL Rem') ||
                                                                '</ABBR>'));
              END IF;
              IF dispxl THEN
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0062',
                                                                           'SQL',
                                                                           'Crosslist Capacity') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0063',
                                                                           'SQL',
                                                                           'XL Cap') ||
                                                                '</ABBR>'));
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0064',
                                                                           'SQL',
                                                                           'Crosslist Actual') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0065',
                                                                           'SQL',
                                                                           'XL Act') ||
                                                                '</ABBR>'));
                twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                G$_NLS.GET('BWCKGEN1-0066',
                                                                           'SQL',
                                                                           'Crosslist Remaining') || '">' ||
                                                                G$_NLS.GET('BWCKGEN1-0067',
                                                                           'SQL',
                                                                           'XL Rem') ||
                                                                '</ABBR>'));
              END IF;
            
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0068',
                                                    'SQL',
                                                    'Instructor'));
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0069',
                                                    'SQL',
                                                    'Date') || ' (' ||
                                         twbkfrmt.f_printtext('<ABBR title = "' ||
                                                              g$_nls.get('BWCKGEN1-0070',
                                                                         'SQL',
                                                                         'month month') || '">' ||
                                                              g$_nls.get('BWCKGEN1-0071',
                                                                         'SQL',
                                                                         'MM') ||
                                                              '</ABBR>') || '/' ||
                                         twbkfrmt.f_printtext('<ABBR title = "' ||
                                                              g$_nls.get('BWCKGEN1-0072',
                                                                         'SQL',
                                                                         'day day') || '">' ||
                                                              g$_nls.get('BWCKGEN1-0073',
                                                                         'SQL',
                                                                         'DD') ||
                                                              '</ABBR>') || ')');
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0074',
                                                    'SQL',
                                                    'Location'));
              twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0075',
                                                    'SQL',
                                                    'Attribute'));
              twbkfrmt.p_tablerowclose;
            END IF;
          
            hold_subj := sect_rec.ssbsect_subj_code;
            hold_crse := sect_rec.ssbsect_crse_numb;
            --
            --
            -- ======================================================
            l_found := TRUE;
            twbkfrmt.p_tablerowopen;
            row_count       := 0;
            checklist_count := checklist_count + 1;
          
            --
            -- Procedurally (can't be done in the query) eliminate
            -- sections where crn does not meet the search criteria.
            -- ======================================================
            BEGIN
              WHILE crn(crn_index) IS NOT NULL LOOP
                IF sect_rec.ssbsect_crn = crn(crn_index) THEN
                  row_count := 1;
                  EXIT;
                END IF;
              
                crn_index := crn_index + 1;
              END LOOP;
            EXCEPTION
              WHEN OTHERS THEN
                crn_index := 2;
            END;
          
            --
            -- If a section does not meet the search criteria for crn,
            -- but the student is registered for the section, show it
            -- anyway.
            -- ======================================================
            IF row_count = 0 THEN
              FOR sfrstcr IN sfkcurs.sfrstcrc(genpidm, term_in(i)) LOOP
                IF sfrstcr.sfrstcr_crn = sect_rec.ssbsect_crn THEN
                  row_count := 1;
                  EXIT;
                END IF;
              END LOOP;
            END IF;
          
            --
            -- For Traditional courses, check whether ptrm rsts_code RE/RW's date
            -- range excludes today
            -- ==================================================================
            sfrrsts_found := FALSE;
            IF sect_rec.ssbsect_ptrm_code IS NOT NULL THEN
              OPEN sfrrstsc(term_in(i), sect_rec.ssbsect_ptrm_code);
              FETCH sfrrstsc
                INTO sfrrsts_ind;
              IF sfrrstsc%found THEN
                sfrrsts_found := TRUE;
              END IF;
              CLOSE sfrrstsc;
            END IF;
          
            --
            -- For OLR courses, check whether rsts_code RW is defined for the term
            -- ===================================================================
            olr_rw_defined := FALSE;
            IF sect_rec.ssbsect_reg_from_date IS NOT NULL AND
               sect_rec.ssbsect_ptrm_code IS NULL THEN
              FOR ssrrsts IN ssrrstsc(term_in(i), sect_rec.ssbsect_crn) LOOP
                olr_rw_defined := TRUE;
                EXIT;
              END LOOP;
            END IF;
          
            --
            -- Display the checkbox/restriction indicator/blankspace, and crn.
            -- ===============================================================
          
            --
            -- Display blankspace if section is already registered.
            -- ===============================================================
            IF row_count > 0 THEN
              twbkfrmt.p_tabledata;
              --
              -- Display C if section is closed.
              -- ======================================================
            ELSIF (seats_avail <= 0 OR xseats_avail <= 0) AND
                  NVL(sobterm_row.sobterm_capc_severity, 'N') = 'F' THEN
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = ' ||
                                                        g$_nls.get('BWCKGEN1-0076',
                                                                   'SQL',
                                                                   'Closed') || '>' ||
                                                        g$_nls.get('BWCKGEN1-0077',
                                                                   'SQL',
                                                                   'C') ||
                                                        '</ABBR>'));
            ELSIF sect_rec.ssbsect_reg_from_date IS NOT NULL AND
                  sect_rec.ssbsect_ptrm_code IS NULL AND
                  ((TRUNC(SYSDATE) NOT BETWEEN
                  sect_rec.ssbsect_reg_from_date AND
                  sect_rec.ssbsect_reg_to_date) OR
                  (TRUNC(SYSDATE) >
                  sect_rec.ssbsect_learner_regstart_tdate) OR
                  NOT olr_rw_defined) THEN
              --
              -- OLR course, whose reg dates range excludes today
              -- =================================================
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0078',
                                                                   'SQL',
                                                                   'Not available for registration') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0079',
                                                                   'SQL',
                                                                   'NR') ||
                                                        '</ABBR>'));
            
            ELSIF sect_rec.ssbsect_ptrm_code IS NOT NULL AND
                  NOT sfrrsts_found THEN
              --
              -- Traditional course, and ptrm rsts_code date range excludes today
              -- ================================================================
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0080',
                                                                   'SQL',
                                                                   'Not available for registration') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0081',
                                                                   'SQL',
                                                                   'NR') ||
                                                        '</ABBR>'));
            ELSIF NOT search_term_allowed(i) THEN
              --
              -- Registration restricted for the current term/date
              -- ================================================================
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0082',
                                                                   'SQL',
                                                                   'Not available for registration') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0083',
                                                                   'SQL',
                                                                   'NR') ||
                                                        '</ABBR>'));
            ELSIF NOT search_reg_allowed(i) THEN
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0084',
                                                                   'SQL',
                                                                   'Student Restrictions') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0085',
                                                                   'SQL',
                                                                   'SR') ||
                                                        '</ABBR>'));
            ELSIF NOT facweb_regterm(i) THEN
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0086',
                                                                   'SQL',
                                                                   'Faculty Restrictions') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0087',
                                                                   'SQL',
                                                                   'FR') ||
                                                        '</ABBR>'));
            ELSE
              --
              -- Display checkbox if no restrictions are found.
              -- ===============================================================
              twbkfrmt.p_tabledataopen;
              HTP.formcheckbox('sel_crn',
                               sect_rec.ssbsect_crn || ' ' ||
                               sect_rec.ssbsect_term_code,
                               cattributes => 'ID="action_id' ||
                                              TO_CHAR(checklist_count) || '"');
              twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0088',
                                              'SQL',
                                              'add to worksheet'),
                                   visible => 'INVISIBLE',
                                   idname => 'action_id' ||
                                             TO_CHAR(checklist_count));
              twbkfrmt.P_FormHidden('assoc_term_in',
                                    sect_rec.ssbsect_term_code);
              twbkfrmt.p_tabledataclose;
              checklist_count := checklist_count + 1;
            END IF;
          
            --
            -- Display associated term, if applicable.
            -- ======================================================
            IF multi_term THEN
              twbkfrmt.p_tabledata(bwcklibs.f_term_desc(sect_rec.ssbsect_term_code));
            END IF;
          
            --
            -- Display crn as an anchor.
            -- ======================================================
            twbkfrmt.p_tabledata(twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(twbkwbis.f_cgibin ||
                                                                             'bwckschd.p_disp_listcrse' ||
                                                                             '?term_in=' ||
                                                                             twbkfrmt.f_encode(sect_rec.ssbsect_term_code) ||
                                                                             '&subj_in=' ||
                                                                             twbkfrmt.f_encode(sect_rec.ssbsect_subj_code) ||
                                                                             '&crse_in=' ||
                                                                             twbkfrmt.f_encode(sect_rec.ssbsect_crse_numb) ||
                                                                             '&crn_in=' ||
                                                                             twbkfrmt.f_encode(sect_rec.ssbsect_crn)),
                                                        sect_rec.ssbsect_crn,
                                                        cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0089',
                                                                                                          'SQL',
                                                                                                          'Detail'))));
            --
            -- Display subject, course, section, campus.
            -- ======================================================
            twbkfrmt.p_tabledata(sect_rec.ssbsect_subj_code);
            twbkfrmt.p_tabledata(sect_rec.ssbsect_crse_numb);
            twbkfrmt.p_tabledata(sect_rec.ssbsect_seq_numb);
            twbkfrmt.p_tabledata(sect_rec.ssbsect_camp_code);
          
            --
            -- Display credit hours.
            -- ======================================================
            IF sect_rec.ssbsect_credit_hrs IS NOT NULL THEN
              twbkfrmt.p_tabledata(LTRIM(TO_CHAR(sect_rec.ssbsect_credit_hrs,
                                                 '9990D990')));
            ELSE
              IF sect_rec.scbcrse_credit_hr_ind = 'TO' THEN
                twbkfrmt.p_tabledata(NVL(LTRIM(TO_CHAR(sect_rec.scbcrse_credit_hr_low,
                                                       '9990D990')),
                                         TO_CHAR(0, '0D99')) || '-' ||
                                     LTRIM(TO_CHAR(sect_rec.scbcrse_credit_hr_high,
                                                   '9990D990')));
              ELSIF sect_rec.scbcrse_credit_hr_ind = 'OR' THEN
                twbkfrmt.p_tabledata(NVL(LTRIM(TO_CHAR(sect_rec.scbcrse_credit_hr_low,
                                                       '9990D990')),
                                         TO_CHAR(0, '0D99')) || '/' ||
                                     LTRIM(TO_CHAR(sect_rec.scbcrse_credit_hr_high,
                                                   '9990D990')));
              ELSE
                twbkfrmt.p_tabledata(LTRIM(TO_CHAR(sect_rec.scbcrse_credit_hr_low,
                                                   '9990D990')));
              END IF;
            END IF;
          
            --
            -- Display course title.
            -- ======================================================
            twbkfrmt.p_tabledata(bwcklibs.f_course_title(term_in(i),
                                                         sect_rec.ssbsect_crn));
          
            --
            -- If meeting times have not been assigned, then show TBA
            -- for day and time. Show capacity numbers.
            -- ======================================================
            IF sect_rec.ssrmeet_crn IS NULL THEN
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0090',
                                                                   'SQL',
                                                                   'To Be Announced') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0091',
                                                                   'SQL',
                                                                   'TBA') ||
                                                        '</ABBR>'),
                                   ccolspan => '2');
            
              IF dispenroll THEN
                twbkfrmt.p_tabledata(sect_rec.ssbsect_max_enrl);
                twbkfrmt.p_tabledata(sect_rec.ssbsect_enrl);
                twbkfrmt.p_tabledata(seats_avail);
              END IF;
              IF dispwl THEN
                twbkfrmt.p_tabledata(sect_rec.ssbsect_wait_capacity);
                twbkfrmt.p_tabledata(sect_rec.ssbsect_wait_count);
                twbkfrmt.p_tabledata(sect_rec.ssbsect_wait_avail);
              END IF;
              IF dispxl THEN
                twbkfrmt.p_tabledata(nvl(xseats_max_enrl, 0));
                twbkfrmt.p_tabledata(nvl(xseats_enrl, 0));
                twbkfrmt.p_tabledata(nvl(xseats_avail, 0));
              END IF;
            
              --
              -- If an instructor is assigned, show their name. Else show TBA.
              -- =============================================================
            
              BEGIN
                IF instr_web_search OR instr_name2 IS NULL OR
                   instr_tbl(2) = '%' THEN
                  bwckfrmt.p_instructor_list(term_in(i),
                                             sect_rec.ssbsect_crn,
                                             NULL,
                                             p_display_email => FALSE);
                ELSE
                  twbkfrmt.p_tabledata(instr_name2);
                END IF;
              
              EXCEPTION
                WHEN OTHERS THEN
                  twbkfrmt.p_tabledata(nvl(instr_name2,
                                           twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                g$_nls.get('BWCKGEN1-0092',
                                                                           'SQL',
                                                                           'To Be Announced') || '">' ||
                                                                g$_nls.get('BWCKGEN1-0093',
                                                                           'SQL',
                                                                           'TBA') ||
                                                                '</ABBR>')));
              END;
            
              --
              -- Show part-of-term begin and end dates.
              -- ======================================================
              twbkfrmt.p_tabledata(TO_CHAR(sect_rec.ssbsect_ptrm_start_date,
                                           'MM/DD') || '-' ||
                                   TO_CHAR(sect_rec.ssbsect_ptrm_end_date,
                                           'MM/DD'));
              twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                        g$_nls.get('BWCKGEN1-0094',
                                                                   'SQL',
                                                                   'To Be Announced') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0095',
                                                                   'SQL',
                                                                   'TBA') ||
                                                        '</ABBR>'));
            
              --
              -- Show attributes.
              -- ======================================================
              IF attr_desc IS NULL THEN
                FOR ssrattr_rec IN ssrattr_c(term_in(i),
                                             sect_rec.ssbsect_crn) LOOP
                  IF ssrattr_c%NOTFOUND THEN
                    EXIT;
                  ELSE
                    IF attr_desc IS NOT NULL THEN
                      attr_desc := attr_desc || ' ' ||
                                   g$_nls.get('BWCKGEN1-0096', 'SQL', 'and') || ' ';
                    END IF;
                  
                    attr_desc := attr_desc || ssrattr_rec.stvattr_desc;
                  END IF;
                END LOOP;
              END IF;
            
              IF attr_desc IS NOT NULL THEN
                twbkfrmt.p_tabledata(attr_desc);
              ELSE
                twbkfrmt.p_tabledata;
              END IF;
            
              twbkfrmt.p_tablerowclose;
              GOTO next_row;
            END IF;
          
            --
            -- If meeting times DO exist, loop through the meeting times.
            -- ======================================================
            FOR ssrmeet IN ssklibs.ssrmeetc(sect_rec.ssrmeet_crn,
                                            sect_rec.ssrmeet_term_code) LOOP
              --
              -- The first meeting record should be displayed on the
              -- same row as the section info. Subsequent records should
              -- be displayed on new rows.
              -- ======================================================
              IF ssklibs.ssrmeetc%rowcount > 1 THEN
                twbkfrmt.p_tablerowopen;
              
                --
                -- leave space for associated term column, if applicable.
                -- ======================================================
                IF multi_term THEN
                  twbkfrmt.p_tabledata;
                END IF;
              
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
                twbkfrmt.p_tabledata;
              END IF;
            
              --
              -- Display the meeting day and time.
              -- ======================================================
              twbkfrmt.p_tabledata(g$_date.nls_abv_day(ssrmeet.ssrmeet_mon_day) ||
                                   g$_date.nls_abv_day(ssrmeet.ssrmeet_tue_day) ||
                                   g$_date.nls_abv_day(ssrmeet.ssrmeet_wed_day) ||
                                   g$_date.nls_abv_day(ssrmeet.ssrmeet_thu_day) ||
                                   g$_date.nls_abv_day(ssrmeet.ssrmeet_fri_day) ||
                                   g$_date.nls_abv_day(ssrmeet.ssrmeet_sat_day) ||
                                   g$_date.nls_abv_day(ssrmeet.ssrmeet_sun_day));
            
              IF ssrmeet.ssrmeet_begin_time || ssrmeet.ssrmeet_end_time IS NULL THEN
                twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                          g$_nls.get('BWCKGEN1-0097',
                                                                     'SQL',
                                                                     'To Be Announced') || '">' ||
                                                          g$_nls.get('BWCKGEN1-0098',
                                                                     'SQL',
                                                                     'TBA') ||
                                                          '</ABBR>'));
              ELSE
                twbkfrmt.p_tabledata(TO_CHAR(TO_DATE(ssrmeet.ssrmeet_begin_time,
                                                     'HH24MI'),
                                             twbklibs.twgbwrul_rec.twgbwrul_time_fmt) || '-' ||
                                     TO_CHAR(TO_DATE(ssrmeet.ssrmeet_end_time,
                                                     'HH24MI'),
                                             twbklibs.twgbwrul_rec.twgbwrul_time_fmt));
              END IF;
            
              --
              -- Display capacity numbers.
              -- ======================================================
              IF ssklibs.ssrmeetc%rowcount = 1 THEN
              
                IF dispenroll THEN
                  twbkfrmt.p_tabledata(sect_rec.ssbsect_max_enrl);
                  twbkfrmt.p_tabledata(sect_rec.ssbsect_enrl);
                  twbkfrmt.p_tabledata(seats_avail);
                END IF;
                IF dispwl THEN
                  twbkfrmt.p_tabledata(sect_rec.ssbsect_wait_capacity);
                  twbkfrmt.p_tabledata(sect_rec.ssbsect_wait_count);
                  twbkfrmt.p_tabledata(sect_rec.ssbsect_wait_avail);
                END IF;
                IF dispxl THEN
                  twbkfrmt.p_tabledata(nvl(xseats_max_enrl, 0));
                  twbkfrmt.p_tabledata(nvl(xseats_enrl, 0));
                  twbkfrmt.p_tabledata(nvl(xseats_avail, 0));
                END IF;
              
              ELSE
                IF dispenroll THEN
                  twbkfrmt.p_tabledata;
                  twbkfrmt.p_tabledata;
                  twbkfrmt.p_tabledata;
                END IF;
                IF dispwl THEN
                  twbkfrmt.p_tabledata;
                  twbkfrmt.p_tabledata;
                  twbkfrmt.p_tabledata;
                END IF;
                IF dispxl THEN
                  twbkfrmt.p_tabledata;
                  twbkfrmt.p_tabledata;
                  twbkfrmt.p_tabledata;
                END IF;
              END IF;
            
              --
              -- If an instructor is assigned, show their name. Else show TBA.
              -- ======================================================
            
              BEGIN
              
                IF instr_web_search OR instr_name2 IS NULL OR
                   instr_tbl(2) = '%' THEN
                  bwckfrmt.p_instructor_list(term_in(i),
                                             sect_rec.ssbsect_crn,
                                             ssrmeet.ssrmeet_catagory,
                                             p_display_email => FALSE);
                ELSE
                  twbkfrmt.p_tabledata(instr_name2);
                END IF;
              
              EXCEPTION
                WHEN OTHERS THEN
                  twbkfrmt.p_tabledata(nvl(instr_name2,
                                           twbkfrmt.f_printtext('<ABBR title = "' ||
                                                                g$_nls.get('BWCKGEN1-0099',
                                                                           'SQL',
                                                                           'To Be Announced') || '">' ||
                                                                g$_nls.get('BWCKGEN1-0100',
                                                                           'SQL',
                                                                           'TBA') ||
                                                                '</ABBR>')));
              END;
            
              --
              -- Show part of term begin and end dates.
              -- ======================================================
              IF ssrmeet.ssrmeet_start_date <>
                 NVL(sobptrm_row.sobptrm_start_date, SYSDATE) OR
                 ssrmeet.ssrmeet_end_date <>
                 NVL(sobptrm_row.sobptrm_end_date, SYSDATE) THEN
                twbkfrmt.p_tabledata(TO_CHAR(ssrmeet.ssrmeet_start_date,
                                             'MM/DD') || '-' ||
                                     TO_CHAR(ssrmeet.ssrmeet_end_date,
                                             'MM/DD'));
              ELSE
                twbkfrmt.p_tabledata(TO_CHAR(sobptrm_row.sobptrm_start_date,
                                             'MM/DD') || '-' ||
                                     TO_CHAR(sobptrm_row.sobptrm_end_date,
                                             'MM/DD'));
              END IF;
            
              --
              -- Show building and room.
              -- ======================================================
              IF (ssrmeet.ssrmeet_bldg_code IS NOT NULL) OR
                 (ssrmeet.ssrmeet_room_code IS NOT NULL) THEN
                twbkfrmt.p_tabledata(ssrmeet.ssrmeet_bldg_code || ' ' ||
                                     ssrmeet.ssrmeet_room_code);
              ELSE
                twbkfrmt.p_tabledata(twbkfrmt.f_printtext('<ABBR title = "' ||
                                                          g$_nls.get('BWCKGEN1-0101',
                                                                     'SQL',
                                                                     'To Be Announced') || '">' ||
                                                          g$_nls.get('BWCKGEN1-0102',
                                                                     'SQL',
                                                                     'TBA') ||
                                                          '</ABBR>'));
              END IF;
            
              --
              -- Show attributes.
              -- ======================================================
              IF attr_desc IS NULL THEN
                FOR ssrattr_rec IN ssrattr_c(term_in(i),
                                             sect_rec.ssbsect_crn) LOOP
                  IF ssrattr_c%NOTFOUND THEN
                    EXIT;
                  ELSE
                    IF attr_desc IS NOT NULL THEN
                      attr_desc := attr_desc || ' ' ||
                                   g$_nls.get('BWCKGEN1-0103', 'SQL', 'and') || ' ';
                    END IF;
                  
                    attr_desc := attr_desc || ssrattr_rec.stvattr_desc;
                  END IF;
                END LOOP;
              END IF;
            
              IF attr_desc IS NOT NULL THEN
                twbkfrmt.p_tabledata(attr_desc);
              ELSE
                twbkfrmt.p_tabledata;
              END IF;
            
              twbkfrmt.p_tablerowclose;
            END LOOP;
          END IF;
        
          <<next_row>>
          hold_crn := sect_rec.ssbsect_crn;
          NULL;
        END LOOP display_sections_loop;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END LOOP;
  
    <<end_label>>
    twbkfrmt.p_tableclose;
    HTP.br;
  
    IF call_value_in <> 'UNSECURED' OR call_value_in IS NULL THEN
      --
      --
      -- ======================================================
      IF NOT l_found THEN
        twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0104',
                                           'SQL',
                                           'No classes were found that meet your search criteria'));
        HTP.br;
      
        BEGIN
          IF crn(2) IS NOT NULL THEN
            HTP.formsubmit('ADD_BTN',
                           g$_nls.get('BWCKGEN1-0105',
                                      'SQL',
                                      'Back to WorkSheet'));
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      
        HTP.formsubmit('ADD_BTN',
                       g$_nls.get('BWCKGEN1-0106', 'SQL', 'Class Search'));
      ELSE
        IF sfkvars.search_reg_allowed AND facweb_regterm_overall THEN
          HTP.formsubmit('ADD_BTN',
                         g$_nls.get('BWCKGEN1-0107', 'SQL', 'Register'));
        
          IF NOT multi_term THEN
            HTP.formsubmit('ADD_BTN',
                           g$_nls.get('BWCKGEN1-0108',
                                      'SQL',
                                      'Add to WorkSheet'));
          END IF;
        
          HTP.formsubmit('ADD_BTN',
                         g$_nls.get('BWCKGEN1-0109', 'SQL', 'Class Search'));
        ELSE
          HTP.formsubmit('ADD_BTN',
                         g$_nls.get('BWCKGEN1-0110', 'SQL', 'Class Search'));
        END IF;
      END IF;
    ELSE
      IF NOT l_found THEN
        twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0111',
                                           'SQL',
                                           'No classes were found that meet your search criteria'));
        HTP.br;
      END IF;
    END IF;
  END P_ListCrse;

  --------------------------------------------------------------------------

  PROCEDURE P_RegsGetCrse(term_in       IN OWA_UTIL.ident_arr,
                          sel_subj      IN OWA_UTIL.ident_arr,
                          sel_crse      IN VARCHAR2,
                          sel_title     IN VARCHAR2,
                          begin_hh      IN VARCHAR2,
                          begin_mi      IN VARCHAR2,
                          begin_ap      IN VARCHAR2,
                          sel_day       IN OWA_UTIL.ident_arr,
                          sel_ptrm      IN OWA_UTIL.ident_arr,
                          end_hh        IN VARCHAR2,
                          end_mi        VARCHAR2,
                          end_ap        IN VARCHAR2,
                          sel_camp      IN OWA_UTIL.ident_arr,
                          sel_schd      IN OWA_UTIL.ident_arr,
                          sel_sess      IN OWA_UTIL.ident_arr,
                          sel_instr     IN OWA_UTIL.ident_arr,
                          sel_attr      IN OWA_UTIL.ident_arr,
                          rsts          IN OWA_UTIL.ident_arr,
                          assoc_term_in IN OWA_UTIL.ident_arr,
                          crn           IN OWA_UTIL.ident_arr,
                          start_date_in IN OWA_UTIL.ident_arr,
                          end_date_in   IN OWA_UTIL.ident_arr,
                          subj          IN OWA_UTIL.ident_arr,
                          crse          IN OWA_UTIL.ident_arr,
                          sec           IN OWA_UTIL.ident_arr,
                          levl          IN OWA_UTIL.ident_arr,
                          cred          IN OWA_UTIL.ident_arr,
                          gmod          IN OWA_UTIL.ident_arr,
                          title         IN bwckcoms.varchar2_tabtype,
                          mesg          IN OWA_UTIL.ident_arr,
                          regs_row      NUMBER,
                          add_row       NUMBER,
                          wait_row      NUMBER,
                          sel_dunt_code IN VARCHAR2 DEFAULT NULL,
                          sel_dunt_unit IN VARCHAR2 DEFAULT NULL,
                          sel_levl      IN OWA_UTIL.ident_arr,
                          sel_from_cred IN VARCHAR2 DEFAULT NULL,
                          sel_to_cred   IN VARCHAR2 DEFAULT NULL,
                          sel_insm      IN OWA_UTIL.ident_arr) IS
    i          INTEGER;
    count_row  NUMBER;
    term       stvterm.stvterm_code%TYPE := NULL;
    multi_term BOOLEAN := TRUE;
  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
  
    term := term_in(term_in.COUNT);
  
    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      /* FACWEB */
      IF NOT multi_term THEN
        IF NOT bwlkilib.f_add_drp(term) THEN
          bwlkostm.p_facselterm(term, 'bwlkffcs.P_FacCrseSearch');
          RETURN;
        END IF;
      END IF;
    ELSE
      /* STUWEB */
      IF NOT multi_term THEN
        IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
          bwskflib.p_seldefterm(term, 'bwskfcls.P_CrseSearch');
          RETURN;
        END IF;
      END IF;
    END IF;
  
    BEGIN
      IF sel_subj(2) IS NULL THEN
        RAISE NO_DATA_FOUND;
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        --
        bwckgens.P_RegsCrseSearch(term_in,
                                  rsts,
                                  assoc_term_in,
                                  crn,
                                  start_date_in,
                                  end_date_in,
                                  subj,
                                  crse,
                                  sec,
                                  levl,
                                  cred,
                                  gmod,
                                  title,
                                  mesg,
                                  regs_row,
                                  add_row,
                                  wait_row,
                                  TRUE);
        RETURN;
        --
    END;
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      /* FACWEB */
      bwckfrmt.p_open_doc('bwlkffcs.P_FacGetCrse',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      HTP.formopen(twbkwbis.f_cgibin || 'bwckcoms.P_AddFromSearch1',
                   cattributes => 'onSubmit="return checkSubmit()"');
      twbkwbis.p_dispinfo('bwlkffcs.P_FacGetCrse', 'DEFAULT');
    ELSE
      /* STUWEB */
      bwckfrmt.p_open_doc('bwskfcls.P_GetCrse',
                          term,
                          NULL,
                          multi_term,
                          term_in(1));
      HTP.formopen(twbkwbis.f_cgibin || 'bwckcoms.P_AddFromSearch1',
                   cattributes => 'onSubmit="return checkSubmit()"');
      twbkwbis.p_dispinfo('bwskfcls.P_GetCrse', 'DEFAULT');
    END IF; /* END stu fac specific code sections */
  
    HTP.br;
    twbkfrmt.P_FormHidden('assoc_term_in', 'dummy');
    twbkfrmt.P_FormHidden('crn', 'dummy');
    twbkfrmt.P_FormHidden('start_date_in', 'dummy');
    twbkfrmt.P_FormHidden('end_date_in', 'dummy');
    twbkfrmt.P_FormHidden('rsts', 'dummy');
    twbkfrmt.P_FormHidden('subj', 'dummy');
    twbkfrmt.P_FormHidden('crse', 'dummy');
    twbkfrmt.P_FormHidden('sec', 'dummy');
    twbkfrmt.P_FormHidden('levl', 'dummy');
    twbkfrmt.P_FormHidden('gmod', 'dummy');
    twbkfrmt.P_FormHidden('cred', 'dummy');
    twbkfrmt.P_FormHidden('title', 'dummy');
    twbkfrmt.P_FormHidden('mesg', 'dummy');
    twbkfrmt.P_FormHidden('regs_row', regs_row);
    twbkfrmt.P_FormHidden('wait_row', wait_row);
    twbkfrmt.P_FormHidden('add_row', add_row);
    i := 2;
  
    BEGIN
      WHILE crn(i) IS NOT NULL LOOP
        BEGIN
          twbkfrmt.P_FormHidden('rsts', rsts(i));
          twbkfrmt.P_FormHidden('assoc_term_in', assoc_term_in(i));
          twbkfrmt.P_FormHidden('crn', crn(i));
          twbkfrmt.P_FormHidden('start_date_in', start_date_in(i));
          twbkfrmt.P_FormHidden('end_date_in', end_date_in(i));
          twbkfrmt.P_FormHidden('subj', subj(i));
          twbkfrmt.P_FormHidden('crse', crse(i));
          twbkfrmt.P_FormHidden('sec', sec(i));
          twbkfrmt.P_FormHidden('levl', levl(i));
          twbkfrmt.P_FormHidden('gmod', gmod(i));
          twbkfrmt.P_FormHidden('cred', cred(i));
          twbkfrmt.P_FormHidden('title', title(i));
          twbkfrmt.P_FormHidden('mesg', mesg(i));
          i := i + 1;
        EXCEPTION
          WHEN OTHERS THEN
            i := i + 1;
        END;
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        i := 2;
    END;
  
    bwckgens.P_ListCrse(term_in,
                        sel_subj,
                        sel_crse,
                        sel_title,
                        begin_hh,
                        begin_mi,
                        begin_ap,
                        sel_day,
                        sel_ptrm,
                        end_hh,
                        end_mi,
                        end_ap,
                        sel_camp,
                        sel_schd,
                        sel_sess,
                        sel_instr,
                        sel_attr,
                        crn,
                        rsts,
                        sel_dunt_code,
                        sel_dunt_unit,
                        sel_levl,
                        sel_from_cred,
                        sel_to_cred,
                        sel_insm);
    HTP.formclose;
    twbkwbis.p_closedoc(curr_release);
  END P_RegsGetCrse;

  --------------------------------------------------------------------------

  PROCEDURE p_display_instructor_selectbox(p_last_name  spriden.spriden_last_name%TYPE,
                                           p_first_name spriden.spriden_first_name%TYPE,
                                           p_mi         spriden.spriden_mi%TYPE,
                                           p_pidm       spriden.spriden_pidm%TYPE,
                                           p_count_row  IN OUT NUMBER) IS
  BEGIN
  
    IF p_count_row = 1 THEN
      twbkfrmt.p_tabledataopen(ccolspan => '7');
      HTP.formselectopen('sel_instr',
                         nsize       => 3,
                         cattributes => 'MULTIPLE ID="instr_id"');
      twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0112', 'SQL', 'All'),
                                  '%',
                                  'SELECTED');
    END IF;
  
    p_count_row := p_count_row + 1;
  
    twbkwbis.p_formselectoption(f_format_name(p_pidm, 'LFMI'), -- As a part of I18N
                                p_pidm);
  
  END p_display_instructor_selectbox;

  --------------------------------------------------------------------------

  PROCEDURE P_Search(term_in       IN OWA_UTIL.ident_arr,
                     call_value_in IN VARCHAR2 DEFAULT NULL) IS
    CURSOR stvattrc IS
      SELECT * FROM stvattr ORDER BY stvattr_desc;
  
    CURSOR gtvdunt_c IS
      SELECT * FROM gtvdunt ORDER BY gtvdunt_desc;
  
    CURSOR stvptrm_wdsp_single_c(term VARCHAR2) IS
      SELECT stvptrm.*
        FROM stvptrm, sorwdsp
       WHERE sorwdsp_code = stvptrm_code
         AND sorwdsp_table_name = 'STVPTRM'
         AND EXISTS (SELECT '1'
                FROM ssbsect
               WHERE ssbsect_term_code = term
                 AND ssbsect_ptrm_code = stvptrm_code)
       ORDER BY stvptrm_desc;
  
    i                          NUMBER := 0;
    col_span                   NUMBER;
    count_row                  NUMBER;
    gtvdunt_rec                gtvdunt%ROWTYPE;
    gtvinsm_rec                bwcklibs.gtvinsm_wdsp_c%ROWTYPE;
    row_count                  NUMBER;
    sobterm_row                sobterm%ROWTYPE;
    stvcamp_rec                bwcklibs.stvcamp_wdsp_c%ROWTYPE;
    stvlevl_rec                bwcklibs.stvlevl_wdsp_c%ROWTYPE;
    ptrm_drop_down_initialized BOOLEAN;
    term                       stvterm.stvterm_code%TYPE;
    multi_term                 BOOLEAN;
    schd_web_search            BOOLEAN := FALSE;
    insm_web_search            BOOLEAN := FALSE;
    camp_web_search            BOOLEAN := FALSE;
    levl_web_search            BOOLEAN := FALSE;
    duration_web_search        BOOLEAN := FALSE;
    instr_web_search           BOOLEAN := FALSE;
    sess_web_search            BOOLEAN := FALSE;
    attr_web_search            BOOLEAN := FALSE;
    valid_pot                  BOOLEAN := FALSE;
    j                          NUMBER;
    max_hour                   NUMBER;
  BEGIN
    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    ELSE
      multi_term := TRUE;
    END IF;
  
    term := term_in(term_in.COUNT);
  
    FOR i IN 1 .. term_in.COUNT LOOP
      twbkfrmt.P_FormHidden('term_in', term_in(i));
    END LOOP;
  
    twbkfrmt.P_FormHidden('sel_subj', 'dummy');
    twbkfrmt.P_FormHidden('sel_day', 'dummy');
    twbkfrmt.P_FormHidden('sel_schd', 'dummy');
    twbkfrmt.P_FormHidden('sel_insm', 'dummy');
    twbkfrmt.P_FormHidden('sel_camp', 'dummy');
    twbkfrmt.P_FormHidden('sel_levl', 'dummy');
    twbkfrmt.P_FormHidden('sel_sess', 'dummy');
    twbkfrmt.P_FormHidden('sel_instr', 'dummy');
    twbkfrmt.P_FormHidden('sel_ptrm', 'dummy');
    twbkfrmt.P_FormHidden('sel_attr', 'dummy');
    twbkfrmt.p_tableopen('DATAENTRY',
                         cattributes => 'SUMMARY="' ||
                                        g$_nls.get('BWCKGEN1-0113',
                                                   'SQL',
                                                   'Table is used to present the course search criteria') || '"');
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(SUBJECT_LABEL,
                                                   idname => 'subj_id'));
    count_row := 1;
  
    FOR stvsubj IN sfkcurs.stvsubjc(term_in(1),
                                    term_in(term_in.COUNT),
                                    g_from_date,
                                    g_to_date) LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_subj',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="subj_id"');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(stvsubj.stvsubj_desc,
                                  stvsubj.stvsubj_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
    --
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(COURSE_LABEL,
                                                   idname => 'crse_id'));
    twbkfrmt.p_tabledata(twbkfrmt.f_formtext('sel_crse',
                                             '5',
                                             '5',
                                             cattributes => 'ID="crse_id"'),
                         ccolspan => '7');
    twbkfrmt.p_tablerowclose;
    --
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(TITLE_LABEL,
                                                   idname => 'title_id'));
    twbkfrmt.p_tabledata(twbkfrmt.f_formtext('sel_title',
                                             '30',
                                             '30',
                                             cattributes => 'ID="title_id"'),
                         ccolspan => '7');
    twbkfrmt.p_tablerowclose;
  
    --
    -- Loop through sobterm for all terms chosen, if any
    -- search indicators are found set to yes for any
    -- term, then overall indicator is set to true
    -- ======================================================
    FOR i IN 1 .. term_in.COUNT LOOP
      FOR sobterm IN soklibs.sobterm_webc(term_in(i)) LOOP
        sobterm_row := sobterm;
      END LOOP;
    
      IF NVL(sobterm_row.sobterm_schd_web_search_ind, 'N') = 'Y' THEN
        schd_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_insm_web_search_ind, 'N') = 'Y' THEN
        insm_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_camp_web_search_ind, 'N') = 'Y' THEN
        camp_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_levl_web_search_ind, 'N') = 'Y' THEN
        levl_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_duration_web_srch_ind, 'N') = 'Y' THEN
        duration_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_instr_web_search_ind, 'N') = 'Y' THEN
        instr_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_sess_web_search_ind, 'N') = 'Y' THEN
        sess_web_search := TRUE;
      END IF;
    
      IF NVL(sobterm_row.sobterm_attr_web_search_ind, 'N') = 'Y' THEN
        attr_web_search := TRUE;
      END IF;
    END LOOP;
  
    --
    IF NOT schd_web_search THEN
      GOTO insm_search;
    END IF;
  
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(SCHEDULE_LABEL,
                                                   idname => 'schd_id'));
    count_row := 1;
  
    FOR stvschd IN bwcklibs.stvschd_wdsp_c LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_schd',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="schd_id"');
        twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0114',
                                               'SQL',
                                               'All'),
                                    '%',
                                    'SELECTED');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(stvschd.stvschd_desc,
                                  stvschd.stvschd_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
  
    --
    <<insm_search>>
    IF NOT insm_web_search THEN
      GOTO cred_search;
    END IF;
  
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(INST_MTH_LABEL,
                                                   idname => 'insm_id'));
    count_row := 1;
  
    FOR gtvinsm_rec IN bwcklibs.gtvinsm_wdsp_c LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_insm',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="insm_id"');
        twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0115',
                                               'SQL',
                                               'All'),
                                    '%',
                                    'SELECTED');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(gtvinsm_rec.gtvinsm_desc,
                                  gtvinsm_rec.gtvinsm_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
  
    --
  
    <<cred_search>>
    twbkfrmt.P_TableRowOpen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(CREDITS_LABEL,
                                                   idname => 'credit_id_from'));
    twbkfrmt.p_tabledataopen(ccolspan => '7');
    twbkfrmt.p_formtext('sel_from_cred',
                        '10',
                        '10',
                        cattributes => 'ID="credit_id_from"');
    twbkfrmt.p_printtext(' ' || CREDITS_FROM_LABEL || ' ');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0116',
                                    'SQL',
                                    'Credit Range To') || ':',
                         idname => 'credit_id_to',
                         visible => 'INVISIBLE');
    twbkfrmt.p_formtext('sel_to_cred',
                        '10',
                        '10',
                        cattributes => 'ID="credit_id_to"');
    twbkfrmt.p_printtext(' ' || CREDITS_TO_LABEL);
    twbkfrmt.P_TableDataClose;
    twbkfrmt.P_TableRowClose;
    --
  
    i := i + 1;
  
    <<camp_search>>
    IF NOT camp_web_search THEN
      GOTO levl_search;
    END IF;
  
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(CAMPUS_LABEL,
                                                   idname => 'camp_id'));
    count_row := 1;
  
    FOR stvcamp_rec IN bwcklibs.stvcamp_wdsp_c LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_camp',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="camp_id"');
        twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0117',
                                               'SQL',
                                               'All'),
                                    '%',
                                    'SELECTED');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(stvcamp_rec.stvcamp_desc,
                                  stvcamp_rec.stvcamp_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
  
    --
    <<levl_search>>
    IF NOT levl_web_search THEN
      GOTO part_search;
    END IF;
  
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(LEVEL_LABEL,
                                                   idname => 'levl_id'));
    count_row := 1;
  
    FOR stvlevl_rec IN bwcklibs.stvlevl_wdsp_c LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_levl',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="levl_id"');
        twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0118',
                                               'SQL',
                                               'All'),
                                    '%',
                                    'SELECTED');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(stvlevl_rec.stvlevl_desc,
                                  stvlevl_rec.stvlevl_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
  
    --
    <<part_search>>
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(TERM_PART1_LABEL,
                                                   idname => 'ptrm_id') ||
                              HTF.br || '     ' ||
                              twbkfrmt.f_printtext(TERM_PART2_LABEL,
                                                   class_in => 'fieldsmalltext'));
  
    count_row := 1;
    IF term_in.COUNT = 1 THEN
      FOR stvptrm_rec IN stvptrm_wdsp_single_c(term) LOOP
        IF count_row = 1 THEN
          twbkfrmt.p_tabledataopen(ccolspan => '7');
          HTP.formselectopen('sel_ptrm',
                             nsize       => 3,
                             cattributes => 'MULTIPLE ID="ptrm_id"');
          twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0119',
                                                 'SQL',
                                                 'All'),
                                      '%',
                                      'SELECTED');
        END IF;
      
        count_row := count_row + 1;
        twbkwbis.p_formselectoption(stvptrm_rec.stvptrm_desc,
                                    stvptrm_rec.stvptrm_code);
      END LOOP;
    ELSE
      FOR stvptrm_rec IN bwcklibs.stvptrm_wdsp_c(term_in(1),
                                                 term_in(term_in.COUNT)) LOOP
        IF count_row = 1 THEN
          twbkfrmt.p_tabledataopen(ccolspan => '7');
          HTP.formselectopen('sel_ptrm',
                             nsize       => 3,
                             cattributes => 'MULTIPLE ID="ptrm_id"');
          twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0120',
                                                 'SQL',
                                                 'All'),
                                      '%',
                                      'SELECTED');
        END IF;
      
        count_row := count_row + 1;
        twbkwbis.p_formselectoption(stvptrm_rec.stvptrm_desc,
                                    stvptrm_rec.stvptrm_code);
      END LOOP;
    END IF;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
    --
  
    i := i + 1;
  
    <<duration_search>>
    IF NOT duration_web_search THEN
      GOTO instr_search;
    END IF;
  
    twbkfrmt.P_TableRowOpen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(DURATION_LABEL,
                                                   idname => 'dunt_unit_id'));
    twbkfrmt.p_tabledataopen(ccolspan => '7');
    twbkfrmt.p_formtext('sel_dunt_unit',
                        '5',
                        '5',
                        cattributes => 'ID="dunt_unit_id"');
    twbkfrmt.P_formlabel(g$_nls.get('BWCKGEN1-0121',
                                    'SQL',
                                    'Duration Code'),
                         idname => 'dunt_code_id',
                         visible => 'INVISIBLE');
    HTP.formselectopen('sel_dunt_code',
                       nsize          => 1,
                       cattributes    => 'ID="dunt_code_id"');
  
    FOR gtvdunt_rec IN gtvdunt_c LOOP
      twbkwbis.p_formselectoption(gtvdunt_rec.gtvdunt_desc,
                                  gtvdunt_rec.gtvdunt_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.P_TableRowClose;
  
    <<instr_search>>
    IF NOT instr_web_search THEN
      GOTO sess_search;
    END IF;
  
    --
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(INSTRUCTOR_LABEL,
                                                   idname => 'instr_id'));
    row_count := 0;
    count_row := 1;
  
    IF multi_term THEN
      FOR instr_rec IN sfkcurs.multi_term_instrc(term_in(1),
                                                 term_in(term_in.COUNT)) LOOP
        row_count := sfkcurs.multi_term_instrc%ROWCOUNT;
        p_display_instructor_selectbox(instr_rec.spriden_last_name,
                                       instr_rec.spriden_first_name,
                                       instr_rec.spriden_mi,
                                       instr_rec.spriden_pidm,
                                       count_row);
      END LOOP;
    ELSE
      FOR instr_rec IN sfkcurs.single_term_instrc(term) LOOP
        row_count := sfkcurs.single_term_instrc%ROWCOUNT;
        p_display_instructor_selectbox(instr_rec.spriden_last_name,
                                       instr_rec.spriden_first_name,
                                       instr_rec.spriden_mi,
                                       instr_rec.spriden_pidm,
                                       count_row);
      END LOOP;
    END IF;
  
    IF row_count > 0 THEN
      HTP.formselectclose;
      twbkfrmt.p_tabledataclose;
    ELSE
      twbkfrmt.P_FormHidden('sel_instr', '%');
    END IF;
  
    twbkfrmt.p_tablerowclose;
    i := i + 1;
  
    <<sess_search>>
    IF NOT sess_web_search THEN
      GOTO attr_search;
    END IF;
  
    --
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(SESSION_LABEL,
                                                   idname => 'sess_id'));
    count_row := 1;
  
    FOR stvsess IN bwcklibs.stvsess_wdsp_c LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_sess',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="sess_id"');
        twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0122',
                                               'SQL',
                                               'All'),
                                    '%',
                                    'SELECTED');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(stvsess.stvsess_desc,
                                  stvsess.stvsess_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
    i := i + 1;
  
    --
    <<attr_search>>
    IF NOT attr_web_search THEN
      GOTO start_time;
    END IF;
  
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(ATTRIBUTE_LABEL,
                                                   idname => 'attr_id'));
    count_row := 1;
  
    FOR stvattr IN bwcklibs.stvattr_wdsp_c LOOP
      IF count_row = 1 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '7');
        HTP.formselectopen('sel_attr',
                           nsize       => 3,
                           cattributes => 'MULTIPLE ID="attr_id"');
        twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0123',
                                               'SQL',
                                               'All'),
                                    '%',
                                    'SELECTED');
      END IF;
    
      count_row := count_row + 1;
      twbkwbis.p_formselectoption(stvattr.stvattr_desc,
                                  stvattr.stvattr_code);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
    --
    i := i + 1;
  
    <<start_time>>
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(START_TIME_LABEL);
  
    IF f_24_hour_clock THEN
      max_hour := 23;
    ELSE
      max_hour := 12;
    END IF;
  
    FOR i IN 0 .. max_hour LOOP
      IF i = 0 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '2');
        twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0124', 'SQL', 'Hour'),
                             visible => 'INVISIBLE',
                             idname => 'begin_hh_id');
        HTP.formselectopen('begin_hh',
                           g$_nls.get('BWCKGEN1-0125', 'SQL', 'Hour') || ' ',
                           nsize => 1,
                           cattributes => 'ID="begin_hh_id"');
      END IF;
    
      twbkwbis.p_formselectoption(TO_CHAR(TO_NUMBER(i), '09'), i);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
  
    FOR i IN 0 .. 11 LOOP
      IF i = 0 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '2');
        twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0126', 'SQL', 'Minute'),
                             visible => 'INVISIBLE',
                             idname => 'begin_mi_id');
        HTP.formselectopen('begin_mi',
                           g$_nls.get('BWCKGEN1-0127', 'SQL', 'Minute') || ' ',
                           nsize => 1,
                           cattributes => 'ID="begin_mi_id"');
      END IF;
    
      twbkwbis.p_formselectoption(TO_CHAR(TO_NUMBER(i) * 5, '09'),
                                  TO_NUMBER(i) * 5);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
  
    IF max_hour = 12 THEN
      /* 12 hour clock, so display am/pm choice */
      FOR i IN 1 .. 2 LOOP
        IF i = 1 THEN
          twbkfrmt.p_tabledataopen(ccolspan => '3');
          twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0128', 'SQL', 'am/pm'),
                               visible => 'INVISIBLE',
                               idname => 'begin_ap_id');
          HTP.formselectopen('begin_ap',
                             g$_nls.get('BWCKGEN1-0129', 'SQL', 'am/pm') || ' ',
                             nsize => 1,
                             cattributes => 'ID="begin_ap_id"');
          twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0130',
                                                 'SQL',
                                                 'am'),
                                      'a');
        ELSE
          twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0131',
                                                 'SQL',
                                                 'pm'),
                                      'p');
        END IF;
      END LOOP;
    
      HTP.formselectclose;
      twbkfrmt.p_tabledataclose;
    
    ELSE
      /* 24 hour clock, so store a dummy value for the am/pm choice */
      twbkfrmt.P_FormHidden('begin_ap', 'x');
    END IF;
  
    twbkfrmt.p_tablerowclose;
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(END_TIME_LABEL);
  
    FOR i IN 0 .. max_hour LOOP
      IF i = 0 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '2');
        twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0132', 'SQL', 'Hour'),
                             visible => 'INVISIBLE',
                             idname => 'end_hh_id');
        HTP.formselectopen('end_hh',
                           g$_nls.get('BWCKGEN1-0133', 'SQL', 'Hour') || ' ',
                           nsize => 1,
                           cattributes => 'ID="end_hh_id"');
      END IF;
    
      twbkwbis.p_formselectoption(TO_CHAR(TO_NUMBER(i), '09'), i);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
  
    FOR i IN 0 .. 11 LOOP
      IF i = 0 THEN
        twbkfrmt.p_tabledataopen(ccolspan => '2');
        twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0134', 'SQL', 'Minute'),
                             visible => 'INVISIBLE',
                             idname => 'end_mi_id');
        HTP.formselectopen('end_mi',
                           g$_nls.get('BWCKGEN1-0135', 'SQL', 'Minute') || ' ',
                           nsize => 1,
                           cattributes => 'ID="end_mi_id"');
      END IF;
    
      twbkwbis.p_formselectoption(TO_CHAR(TO_NUMBER(i) * 5, '09'),
                                  TO_NUMBER(i) * 5);
    END LOOP;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
  
    IF max_hour = 12 THEN
      /* 12 hour clock, so display am/pm choice */
      FOR i IN 1 .. 2 LOOP
        IF i = 1 THEN
          twbkfrmt.p_tabledataopen(ccolspan => '3');
          twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0136', 'SQL', 'am/pm'),
                               visible => 'INVISIBLE',
                               idname => 'end_ap_id');
          HTP.formselectopen('end_ap',
                             g$_nls.get('BWCKGEN1-0137', 'SQL', 'am/pm') || ' ',
                             nsize => 1,
                             cattributes => 'ID="end_ap_id"');
          twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0138',
                                                 'SQL',
                                                 'am'),
                                      'a');
        ELSE
          twbkwbis.p_formselectoption(g$_nls.get('BWCKGEN1-0139',
                                                 'SQL',
                                                 'pm'),
                                      'p');
        END IF;
      END LOOP;
    
      HTP.formselectclose;
      twbkfrmt.p_tabledataclose;
    ELSE
      /* 24 hour clock, so store a dummy value for the am/pm choice */
      twbkfrmt.P_FormHidden('end_ap', 'x');
    END IF;
  
    twbkfrmt.p_tablerowclose;
    twbkfrmt.p_tablerowopen;
    twbkfrmt.p_tabledatalabel(DAYS_LABEL);
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 'm', cattributes => 'ID="sel_day_mon_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0140', 'SQL', 'Monday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_mon_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0141', 'SQL', 'Monday') || '>' ||
                         g$_nls.get('BWCKGEN1-0142', 'SQL', 'Mon') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 't', cattributes => 'ID="sel_day_tue_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0143', 'SQL', 'Tuesday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_tue_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0144', 'SQL', 'Tuesday') || '>' ||
                         g$_nls.get('BWCKGEN1-0145', 'SQL', 'Tue') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 'w', cattributes => 'ID="sel_day_wed_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0146', 'SQL', 'Wednesday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_wed_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0147', 'SQL', 'Wednesday') || '>' ||
                         g$_nls.get('BWCKGEN1-0148', 'SQL', 'Wed') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 'r', cattributes => 'ID="sel_day_thur_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0149', 'SQL', 'Thursday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_thur_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0150', 'SQL', 'Thursday') || '>' ||
                         g$_nls.get('BWCKGEN1-0151', 'SQL', 'Thur') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 'f', cattributes => 'ID="sel_day_fri_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0152', 'SQL', 'Friday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_fri_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0153', 'SQL', 'Friday') || '>' ||
                         g$_nls.get('BWCKGEN1-0154', 'SQL', 'Fri') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 's', cattributes => 'ID="sel_day_sat_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0155', 'SQL', 'Saturday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_sat_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0156', 'SQL', 'Saturday') || '>' ||
                         g$_nls.get('BWCKGEN1-0157', 'SQL', 'Sat') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tabledataopen(cattributes => 'WIDTH=11%');
    HTP.formcheckbox('sel_day', 'u', cattributes => 'ID="sel_day_sun_id"');
    twbkfrmt.p_formlabel(g$_nls.get('BWCKGEN1-0158', 'SQL', 'Sunday'),
                         visible => 'INVISIBLE',
                         idname => 'sel_day_sun_id');
    twbkfrmt.p_printtext('<ABBR title = ' ||
                         g$_nls.get('BWCKGEN1-0159', 'SQL', 'Sunday') || '>' ||
                         g$_nls.get('BWCKGEN1-0160', 'SQL', 'Sun') ||
                         '</ABBR>');
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
  
    <<end_search>>
    twbkfrmt.p_tableclose;
  END P_Search;

  ---------------------------------------------------------------------------------
  --
  -- This procedure displays a student active registration
  -- Information to the web.
  --
  PROCEDURE p_disp_active_regs(term_in IN STVTERM.STVTERM_CODE%TYPE DEFAULT NULL) IS
    global_pidm                SPRIDEN.SPRIDEN_PIDM%TYPE;
    pidm                       SPRIDEN.SPRIDEN_PIDM%TYPE;
    call_path                  VARCHAR2(1);
    student_name               VARCHAR2(185);
    tmp_crn                    SSBSECT.SSBSECT_CRN%TYPE DEFAULT NULL;
    sobterm_row                SOBTERM%ROWTYPE;
    row_count                  NUMBER DEFAULT 0;
    scbcrse_row                SCBCRSE%ROWTYPE;
    cpinuse                    VARCHAR2(1);
    webctinuse                 VARCHAR2(1);
    makewebctlink              VARCHAR2(1);
    webctlogin                 VARCHAR2(200);
    webctlink                  VARCHAR2(1000);
    not_registered_message     VARCHAR2(60);
    hld_stvgmod_desc           STVGMOD.STVGMOD_DESC%TYPE;
    detail_schedule_proc       VARCHAR2(23) DEFAULT NULL;
    midterm_display_c_rec      sfkcurs.midterm_display_c%ROWTYPE;
    grade_detail_exist_c_rec   sfkcurs.grade_detail_exist_c%ROWTYPE;
    final_grades_display_c_rec sfkcurs.final_grades_display_c%ROWTYPE;
  
    CURSOR active_regcrsec(pidm_in     spriden.spriden_pidm%TYPE,
                           term_in     stvterm.stvterm_code%TYPE DEFAULT NULL,
                           crn_in      ssbsect.ssbsect_crn%TYPE DEFAULT NULL,
                           hist_ind_in VARCHAR2 DEFAULT NULL) IS
      SELECT *
        FROM stvschd,
             stvcamp,
             stvrsts,
             ssbsect,
             sfrstcr,
             stvterm,
             scbcrse,
             sobterm
       WHERE sfrstcr_term_code = sobterm_term_code
         AND sobterm_dynamic_sched_term_ind = 'Y'
         AND sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = NVL(term_in, sfrstcr_term_code)
         AND sfrstcr_crn = NVL(crn_in, sfrstcr_crn)
         AND sfrstcr_crn = ssbsect_crn
         AND sfrstcr_grde_date IS NULL
         AND NVL(sfrstcr_error_flag, 'N') <> 'F'
         AND ssbsect_term_code = sfrstcr_term_code
         AND scbcrse_subj_code = ssbsect_subj_code
         AND scbcrse_crse_numb = ssbsect_crse_numb
         AND scbcrse_eff_term =
             (SELECT MAX(scbcrse_eff_term)
                FROM scbcrse x
               WHERE x.scbcrse_subj_code = ssbsect_subj_code
                 AND x.scbcrse_crse_numb = ssbsect_crse_numb
                 AND x.scbcrse_eff_term <= sfrstcr_term_code)
         AND stvcamp_code = sfrstcr_camp_code
         AND stvschd_code = ssbsect_schd_code
         AND stvrsts_code = sfrstcr_rsts_code
         AND stvterm_code = sfrstcr_term_code
         AND stvrsts_sb_print_ind =
             DECODE(hist_ind_in, 'Y', stvrsts_sb_print_ind, 'Y')
         AND stvrsts_wait_ind = 'N'
         AND stvrsts_withdraw_ind = 'N'
         AND stvrsts_voice_type = 'R'
       ORDER BY ssbsect_term_code,
                ssbsect_subj_code,
                ssbsect_crse_numb,
                ssbsect_seq_numb;
  
    CURSOR link_not_graded_c(pidm_in       spriden.spriden_pidm%TYPE,
                             term_in       sfrstcr.sfrstcr_term_code%TYPE,
                             crn_in        ssbsect.ssbsect_crn%TYPE,
                             subj_code_in  ssbsect.ssbsect_subj_code%TYPE,
                             crse_numb_in  ssbsect.ssbsect_crse_numb%TYPE,
                             link_ident_in ssbsect.ssbsect_link_ident%TYPE) IS
      SELECT 'Y'
        FROM sfrstcr, ssbsect, ssrlink
       WHERE sfrstcr_term_code = term_in
         AND sfrstcr_pidm = pidm_in
         AND sfrstcr_crn <> crn_in
         AND sfrstcr_grde_date IS NULL
         AND NVL(sfrstcr_error_flag, 'N') <> 'F'
         AND sfrstcr_term_code = ssbsect_term_code
         AND sfrstcr_crn = ssbsect_crn
         AND ssbsect_subj_code = subj_code_in
         AND ssbsect_crse_numb = crse_numb_in
         AND ssbsect_term_code = ssrlink_term_code
         AND ssbsect_crn = ssrlink_crn
         AND ssrlink_link_conn = link_ident_in;
  
    link_not_graded_ind VARCHAR2(1);
  
    FUNCTION f_disp_unrolled_final(term_in      sfrareg.sfrareg_term_code%TYPE,
                                   crn_in       sfrareg.sfrareg_crn%TYPE,
                                   ptrm_code    ssbsect.ssbsect_ptrm_code%TYPE,
                                   call_path_in VARCHAR2 DEFAULT NULL)
      RETURN BOOLEAN IS
      return_value BOOLEAN DEFAULT FALSE;
      tmp_1        VARCHAR2(1) DEFAULT 'N';
      tmp_2        VARCHAR2(1) DEFAULT 'N';
      tmp_3        VARCHAR2(1) DEFAULT 'N';
    
      CURSOR tmp_1_c(term_in sfrareg.sfrareg_term_code%TYPE) IS
        SELECT 'Y'
          FROM SOBTERM
         WHERE SOBTERM_TERM_CODE = term_in
           AND SOBTERM_MAILER_WEB_IND = 'Y';
    
      CURSOR tmp_2_c(term_in sfrareg.sfrareg_term_code%TYPE,
                     crn_in  sfrareg.sfrareg_crn%TYPE) IS
        SELECT 'Y'
          FROM SSBSSEC
         WHERE SSBSSEC_TERM_CODE = term_in
           AND SSBSSEC_CRN = crn_in
           AND SSBSSEC_FIN_GRD_DISP_IND = 'Y';
    
      CURSOR tmp_3_c(term_in sfrareg.sfrareg_term_code%TYPE,
                     crn_in  sfrareg.sfrareg_crn%TYPE) IS
        SELECT 'Y'
          FROM SSBFSEC
         WHERE SSBFSEC_TERM_CODE = term_in
           AND SSBFSEC_CRN = crn_in
           AND SSBFSEC_FIN_GRD_DISP_IND = 'Y';
    
    BEGIN
      OPEN tmp_1_c(term_in);
      FETCH tmp_1_c
        INTO tmp_1;
      CLOSE tmp_1_c;
    
      IF (call_path_in = 'S') THEN
        OPEN tmp_2_c(term_in, crn_in);
        FETCH tmp_2_c
          INTO tmp_2;
        CLOSE tmp_2_c;
      ELSE
        OPEN tmp_3_c(term_in, crn_in);
        FETCH tmp_3_c
          INTO tmp_3;
        CLOSE tmp_3_c;
      END IF;
    
      IF ptrm_code IS NULL THEN
        IF (tmp_2 = 'Y') OR (tmp_3 = 'Y') THEN
          return_value := TRUE;
        ELSE
          return_value := FALSE;
        END IF;
      ELSE
        IF (tmp_1 = 'Y') THEN
          return_value := TRUE;
        ELSE
          return_value := FALSE;
        END IF;
      END IF;
    
      RETURN return_value;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RETURN return_value;
    END f_disp_unrolled_final;
  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      pidm                   := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                              'STUPIDM'),
                                          '999999999');
      student_name           := f_format_name(pidm, 'FMIL');
      call_path              := 'F';
      detail_schedule_proc   := 'bwlkifac.P_FacSched';
      not_registered_message := g$_nls.get('BWCKGEN1-0161',
                                           'SQL',
                                           'No schedule available.');
      bwckfrmt.p_open_doc('bwlksreg.p_fac_active_regs',
                          header_text_in => student_name);
      bwcklibs.p_confidstudinfo(pidm, NULL);
    ELSE
      call_path              := 'S';
      pidm                   := global_pidm;
      detail_schedule_proc   := 'bwskfshd.P_CrseSchdDetl';
      not_registered_message := g$_nls.get('BWCKGEN1-0162',
                                           'SQL',
                                           'You are not currently registered.');
      bwckfrmt.p_open_doc('bwsksreg.p_active_regs');
    END IF;
  
    twbkwbis.p_dispinfo('bwckgens.p_disp_active_regs', 'DEFAULT');
    cpinuse := twbkwbis.f_fetchwtparam('cpinuse');
  
    IF cpinuse = 'Y' THEN
      makewebctlink := 'N';
    ELSE
      webctinuse := twbkwbis.f_fetchwtparam('WEBCTINUSE');
    
      IF webctinuse = 'Y' THEN
        makewebctlink := 'Y';
        webctlogin    := twbkwbis.f_fetchwtparam('WEBCTLOGIN');
      
        IF webctlogin IS NULL THEN
          makewebctlink := 'N';
        END IF;
      ELSE
        makewebctlink := 'N';
      END IF;
    END IF;
  
    row_count := 0;
  
    twbkfrmt.P_TableOpen('DATADISPLAY',
                         cattributes => 'SUMMARY="' ||
                                        g$_nls.get('BWCKGEN1-0163',
                                                   'SQL',
                                                   'This table represents active registrations, regardless of term.') || '."' ||
                                        'WIDTH="100%"');
  
    FOR regcrse IN active_regcrsec(pidm, term_in, tmp_crn, tmp_hist_ind) LOOP
    
      sylncrsec_rec := NULL;
      OPEN sylncrsec(regcrse.ssbsect_term_code, regcrse.ssbsect_crn);
      FETCH sylncrsec
        INTO sylncrsec_rec;
      CLOSE sylncrsec;
    
      insmcrsec_rec := NULL;
      OPEN insmcrsec(regcrse.ssbsect_insm_code);
      FETCH insmcrsec
        INTO insmcrsec_rec;
      CLOSE insmcrsec;
    
      aregcrsec_rec := NULL;
      OPEN aregcrsec(pidm, regcrse.ssbsect_term_code, regcrse.ssbsect_crn);
      FETCH aregcrsec
        INTO aregcrsec_rec;
      CLOSE aregcrsec;
    
      /* if non-gradable course, check to see if can still be considered active */
      IF nvl(regcrse.ssbsect_gradable_ind, 'N') = 'N' THEN
      
        /* check to see if olr course has 'elapsed' */
        IF regcrse.ssbsect_ptrm_code IS NULL THEN
          IF trunc(SYSDATE) > trunc(aregcrsec_rec.sfrareg_completion_date) THEN
            GOTO next_row_label;
          END IF;
        
          /* check to see if traditional course has 'elapsed' */
        ELSE
          IF trunc(SYSDATE) > trunc(regcrse.ssbsect_ptrm_end_date) THEN
            GOTO next_row_label;
          END IF;
        END IF;
      
        /* check to see if this non-gradable course is linked to */
        /* one that is graded */
        IF regcrse.ssbsect_link_ident IS NOT NULL THEN
          OPEN link_not_graded_c(pidm,
                                 regcrse.sfrstcr_term_code,
                                 regcrse.ssbsect_crn,
                                 regcrse.ssbsect_subj_code,
                                 regcrse.ssbsect_crse_numb,
                                 regcrse.ssbsect_link_ident);
          FETCH link_not_graded_c
            INTO link_not_graded_ind;
          IF link_not_graded_c%FOUND THEN
            CLOSE link_not_graded_c;
          ELSE
            CLOSE link_not_graded_c;
            GOTO next_row_label;
          END IF;
        END IF;
      
      END IF;
    
      row_count := row_count + 1;
      -- --------------------------------------------------------- --
      --                Format The Table Header                    --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
    
      OPEN course_hyperlink_c(regcrse.sfrstcr_term_code,
                              regcrse.ssbsect_crn);
      FETCH course_hyperlink_c
        INTO tmp_hyper_ind;
      IF course_hyperlink_c%FOUND THEN
        twbkfrmt.p_tabledatalabel(twbkfrmt.f_printanchor(curl        => twbkfrmt.f_encodeurl(twbkwbis.f_cgibin ||
                                                                                             'bwckschd.p_disp_listcrse' ||
                                                                                             '?term_in=' ||
                                                                                             twbkfrmt.f_encode(regcrse.sfrstcr_term_code) ||
                                                                                             '&subj_in=' ||
                                                                                             twbkfrmt.f_encode(regcrse.ssbsect_subj_code) ||
                                                                                             '&crse_in=' ||
                                                                                             twbkfrmt.f_encode(regcrse.ssbsect_crse_numb) ||
                                                                                             '&crn_in=' ||
                                                                                             twbkfrmt.f_encode(regcrse.ssbsect_crn)),
                                                         ctext       => bwcklibs.f_course_title(regcrse.sfrstcr_term_code,
                                                                                                regcrse.sfrstcr_crn) ||
                                                                        ' - ' ||
                                                                        regcrse.ssbsect_subj_code || ' ' ||
                                                                        regcrse.ssbsect_crse_numb ||
                                                                        ' - ' ||
                                                                        regcrse.ssbsect_seq_numb,
                                                         cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0164',
                                                                                                           'SQL',
                                                                                                           'Dynamic Schedule'))),
                                  ccolspan => 4);
      ELSE
        twbkfrmt.p_tabledatalabel(bwcklibs.f_course_title(regcrse.sfrstcr_term_code,
                                                          regcrse.sfrstcr_crn) ||
                                  ' - ' || regcrse.ssbsect_subj_code || ' ' ||
                                  regcrse.ssbsect_crse_numb || ' - ' ||
                                  regcrse.ssbsect_seq_numb,
                                  ccolspan => 4);
      END IF;
      CLOSE course_hyperlink_c;
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format First Row                           --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0165',
                                           'SQL',
                                           'Associated Term'));
      twbkfrmt.p_tabledata(regcrse.stvterm_desc);
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0166',
                                           'SQL',
                                           'Credits'));
      bwckfrmt.p_disp_credit_hours(regcrse.sfrstcr_term_code,
                                   regcrse.ssbsect_crn);
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Second Row                          --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                     g$_nls.get('BWCKGEN1-0167',
                                                                'SQL',
                                                                'Course Reference Number') || '">' ||
                                                     g$_nls.get('BWCKGEN1-0168',
                                                                'SQL',
                                                                'CRN') ||
                                                     '</ACRONYM>'));
    
      IF makewebctlink = 'N' OR regcrse.ssbsect_intg_cde IS NULL THEN
        -- twbkfrmt.p_tabledata (regcrse.ssbsect_crn);
        twbkfrmt.P_TableData(twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(detail_schedule_proc ||
                                                                         '?term_in=' ||
                                                                         regcrse.sfrstcr_term_code ||
                                                                         '&crn=' ||
                                                                         regcrse.sfrstcr_crn),
                                                    regcrse.ssbsect_crn,
                                                    cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0169',
                                                                                                      'SQL',
                                                                                                      'Detail'))));
      ELSE
        webctlink := twbkfrmt.f_printanchor(webctlogin,
                                            regcrse.ssbsect_crn,
                                            '',
                                            '',
                                            bwckfrmt.f_anchor_focus(webctlogin));
        twbkfrmt.p_tabledata(webctlink);
      END IF;
    
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0170',
                                           'SQL',
                                           'Grade Mode'));
      bwckfrmt.p_disp_grade_mode(regcrse.sfrstcr_term_code,
                                 regcrse.sfrstcr_crn);
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Third Row                           --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0171',
                                           'SQL',
                                           'Status'));
      twbkfrmt.P_TableData(regcrse.stvrsts_desc || ' ' ||
                           TO_CHAR(regcrse.sfrstcr_rsts_date,
                                   twbklibs.date_display_fmt));
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0172',
                                           'SQL',
                                           'Course Level'));
      bwckfrmt.p_disp_level(regcrse.sfrstcr_term_code, regcrse.ssbsect_crn);
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Fourth Row                          --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0173',
                                           'SQL',
                                           'Schedule Type'));
      twbkfrmt.P_TableData(regcrse.stvschd_desc);
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0174',
                                           'SQL',
                                           'Midterm Grade'));
    
      IF f_grade_hold(pidm) THEN
        twbkfrmt.p_tabledatadead;
      ELSE
        OPEN sfkcurs.midterm_display_c(pidm,
                                       regcrse.sfrstcr_term_code,
                                       regcrse.sfrstcr_crn);
        FETCH sfkcurs.midterm_display_c
          INTO midterm_display_c_rec;
      
        IF sfkcurs.midterm_display_c%FOUND THEN
          twbkfrmt.p_tabledata(regcrse.sfrstcr_grde_code_mid);
        ELSE
          twbkfrmt.p_tabledatadead;
        END IF;
      
        CLOSE sfkcurs.midterm_display_c;
      END IF;
    
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Fifth Row                           --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0175',
                                           'SQL',
                                           'Instructional Method'));
      twbkfrmt.p_tabledata(NVL(insmcrsec_rec.gtvinsm_desc,
                               g$_nls.get('BWCKGEN1-0176', 'SQL', 'N/A')));
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0177',
                                           'SQL',
                                           'Grade Detail'));
    
      IF f_grade_hold(pidm) THEN
        twbkfrmt.p_tabledatadead;
      ELSE
        OPEN sfkcurs.marked_reg_terms_c(pidm,
                                        regcrse.sfrstcr_term_code,
                                        regcrse.sfrstcr_crn);
        FETCH sfkcurs.marked_reg_terms_c
          INTO midterm_display_c_rec;
      
        IF sfkcurs.marked_reg_terms_c%FOUND THEN
          OPEN sfkcurs.grade_detail_exist_c(pidm,
                                            regcrse.sfrstcr_term_code,
                                            regcrse.sfrstcr_crn);
          FETCH sfkcurs.grade_detail_exist_c
            INTO grade_detail_exist_c_rec;
        
          IF sfkcurs.grade_detail_exist_c%FOUND THEN
            IF call_path = 'S' THEN
              twbkfrmt.p_tabledata(twbkfrmt.f_printanchor(curl        => twbkfrmt.f_encodeurl(twbkwbis.f_cgibin ||
                                                                                              'bwsksmrk.p_write_grade_detail' ||
                                                                                              '?term_in=' ||
                                                                                              twbkfrmt.f_encode(regcrse.sfrstcr_term_code) ||
                                                                                              '&crn_in=' ||
                                                                                              twbkfrmt.f_encode(regcrse.sfrstcr_crn) ||
                                                                                              '&calling_proc_in=' ||
                                                                                              twbkfrmt.f_encode('bwckgens.p_disp_active_regs')),
                                                          ctext       => g$_nls.get('BWCKGEN1-0178',
                                                                                    'SQL',
                                                                                    'Available'),
                                                          cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0179',
                                                                                                            'SQL',
                                                                                                            'Grade Detail Component'))));
            ELSE
              twbkfrmt.p_tabledata(twbkfrmt.f_printanchor(curl        => twbkfrmt.f_encodeurl(twbkwbis.f_cgibin ||
                                                                                              'bwlkegrb.P_FacGradeComponents' ||
                                                                                              '?term=' ||
                                                                                              twbkfrmt.f_encode(regcrse.sfrstcr_term_code) ||
                                                                                              '&crn=' ||
                                                                                              twbkfrmt.f_encode(regcrse.sfrstcr_crn)),
                                                          ctext       => g$_nls.get('BWCKGEN1-0180',
                                                                                    'SQL',
                                                                                    'Available'),
                                                          cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0181',
                                                                                                            'SQL',
                                                                                                            'Grade Detail Component'))));
            END IF;
          ELSE
            twbkfrmt.p_tabledata(g$_nls.get('BWCKGEN1-0182', 'SQL', 'None'));
          END IF;
        
          CLOSE sfkcurs.grade_detail_exist_c;
        ELSE
          twbkfrmt.p_tabledatadead;
        END IF;
      
        CLOSE sfkcurs.marked_reg_terms_c;
      END IF;
    
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Sixth Row                           --
      -- --------------------------------------------------------- --
    
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0183',
                                           'SQL',
                                           'Campus'));
      twbkfrmt.p_tabledata(regcrse.stvcamp_desc);
      twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0184',
                                           'SQL',
                                           'Associated Instructor'));
    
      bwckfrmt.p_instructor_links(regcrse.sfrstcr_term_code,
                                  regcrse.sfrstcr_crn,
                                  regcrse.ssbsect_ptrm_code,
                                  aregcrsec_rec.sfrareg_instructor_pidm,
                                  call_path);
    
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Seventh Row                         --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
    
      IF regcrse.ssbsect_ptrm_code IS NULL THEN
        twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0185',
                                             'SQL',
                                             'Class Start Date'));
      
        IF (aregcrsec_rec.sfrareg_start_date IS NOT NULL) AND
           (aregcrsec_rec.sfrareg_extension_number <> 0) THEN
          twbkfrmt.p_tabledata(TO_CHAR(bwcklibs.f_get_sfrareg_start_date(pidm,
                                                                         regcrse.sfrstcr_term_code,
                                                                         regcrse.ssbsect_crn),
                                       twbklibs.date_display_fmt));
        ELSE
          twbkfrmt.p_tabledata(TO_CHAR(aregcrsec_rec.sfrareg_start_date,
                                       twbklibs.date_display_fmt));
        END IF;
      ELSE
        twbkfrmt.p_tabledatadead(ccolspan => 2);
      END IF;
    
      twbkfrmt.p_tabledatalabel(twbkfrmt.f_printtext(g$_nls.get('BWCKGEN1-0186',
                                                                'SQL',
                                                                'Course ') ||
                                                     '<ACRONYM title = "' ||
                                                     g$_nls.get('BWCKGEN1-0187',
                                                                'SQL',
                                                                'Uniform Resource Locator') || '">' ||
                                                     g$_nls.get('BWCKGEN1-0188',
                                                                'SQL',
                                                                'URL') ||
                                                     '</ACRONYM>'));
    
      IF sylncrsec_rec.ssrsyln_section_url IS NOT NULL THEN
        twbkfrmt.p_tabledata(twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(sylncrsec_rec.ssrsyln_section_url),
                                                    sylncrsec_rec.ssrsyln_section_url));
      ELSE
        twbkfrmt.p_tabledatadead;
      END IF;
    
      twbkfrmt.p_tablerowclose;
      -- --------------------------------------------------------- --
      --                Format Eighth Row                          --
      -- --------------------------------------------------------- --
      twbkfrmt.p_tablerowopen;
    
      IF regcrse.ssbsect_ptrm_code IS NULL THEN
        twbkfrmt.p_tabledatalabel(g$_nls.get('BWCKGEN1-0189',
                                             'SQL',
                                             'Expected Completion'));
        twbkfrmt.p_tabledata(TO_CHAR(aregcrsec_rec.sfrareg_completion_date,
                                     twbklibs.date_display_fmt));
        twbkfrmt.p_tabledatadead(ccolspan => 2);
        twbkfrmt.p_tablerowclose;
      END IF;
    
      twbkfrmt.p_tablerowopen();
      twbkfrmt.p_tabledataseparator(ccolspan => 4);
      twbkfrmt.p_tablerowclose;
    
      <<next_row_label>>
      NULL;
    END LOOP;
  
    twbkfrmt.P_TableClose;
  
    IF row_count = 0 THEN
      twbkfrmt.p_printmessage(not_registered_message);
    END IF;
  END p_disp_active_regs;

  ---------------------------------------------------------------------------------
  --
  -- This procedure displays a student registration history
  -- Information to the web.
  --
  PROCEDURE p_disp_reg_hist IS
    global_pidm            SPRIDEN.SPRIDEN_PIDM%TYPE;
    pidm                   SPRIDEN.SPRIDEN_PIDM%TYPE;
    call_path              VARCHAR2(1);
    student_name           VARCHAR2(185);
    row_count              NUMBER DEFAULT 0;
    not_registered_message VARCHAR2(60);
    detail_schedule_proc   VARCHAR2(23) DEFAULT NULL;
    print_header           BOOLEAN DEFAULT TRUE;
  
    CURSOR shrtckg_c(pidm   NUMBER,
                     term   VARCHAR2,
                     crn_in VARCHAR2,
                     levl   VARCHAR2) IS
      SELECT shrtckg_grde_code_final,
             shrtckg_credit_hours,
             shrtckn_crse_title,
             shrtckl_levl_code
        FROM shrgrde a, shrtckg, shrtckl, shrtckn
       WHERE shrtckn_pidm = pidm
         AND shrtckn_term_code = term
         AND shrtckn_crn = crn_in
         AND shrtckn_pidm = shrtckg_pidm
         AND shrtckg_term_code = shrtckn_term_code
         AND shrtckg_tckn_seq_no = shrtckn_seq_no
         AND shrtckg_seq_no =
             (SELECT MAX(shrtckg_seq_no)
                FROM shrtckg
               WHERE shrtckg_pidm = shrtckn_pidm
                 AND shrtckg_term_code = shrtckn_term_code
                 AND shrtckg_tckn_seq_no = shrtckn_seq_no)
         AND shrtckl_pidm = shrtckn_pidm
         AND shrtckl_term_code = shrtckn_term_code
         AND shrtckl_tckn_seq_no = shrtckn_seq_no
         AND shrtckl_levl_code = levl
         AND a.shrgrde_code = shrtckg_grde_code_final
         AND a.shrgrde_levl_code = shrtckl_levl_code
         AND a.shrgrde_term_code_effective =
             (SELECT MAX(b.shrgrde_term_code_effective)
                FROM shrgrde b
               WHERE b.shrgrde_code = shrtckg_grde_code_final
                 AND b.shrgrde_levl_code = shrtckl_levl_code
                 AND b.shrgrde_term_code_effective <= shrtckn_term_code);
  
    shrtckg_c_rec              shrtckg_c%ROWTYPE;
    midterm_display_c_rec      sfkcurs.midterm_display_c%ROWTYPE;
    final_grades_display_c_rec sfkcurs.final_grades_display_c%ROWTYPE;
  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
  
    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      pidm                   := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                              'STUPIDM'),
                                          '999999999');
      student_name           := f_format_name(pidm, 'FMIL');
      call_path              := 'F';
      detail_schedule_proc   := 'bwlkifac.P_FacSched';
      not_registered_message := g$_nls.get('BWCKGEN1-0190',
                                           'SQL',
                                           'No Registration History Available');
      bwcklibs.p_confidstudinfo(pidm, NULL);
    ELSE
      call_path              := 'S';
      pidm                   := global_pidm;
      detail_schedule_proc   := 'bwskfshd.P_CrseSchdDetl';
      not_registered_message := g$_nls.get('BWCKGEN1-0191',
                                           'SQL',
                                           'No Registration History Available');
    END IF;
  
    twbkwbis.p_dispinfo('bwckgens.p_disp_reg_hist', 'DEFAULT');
    twbkfrmt.P_TableOpen('DATADISPLAY',
                         cattributes => 'SUMMARY="' ||
                                        g$_nls.get('BWCKGEN1-0192',
                                                   'SQL',
                                                   'summary="This table will display the registration activity, regardless of term."'));
  
    FOR regcrse IN regcrsec(pidm, NULL, NULL, 'Y') LOOP
      row_count := regcrsec%rowcount;
    
      IF row_count = 0 THEN
        print_header := FALSE;
      END IF;
    
      IF print_header THEN
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0193',
                                              'SQL',
                                              'Associated Term'));
        twbkfrmt.p_tabledataheader(twbkfrmt.f_printtext('<ACRONYM title = "' ||
                                                        g$_nls.get('BWCKGEN1-0194',
                                                                   'SQL',
                                                                   'Course Reference Number') || '">' ||
                                                        g$_nls.get('BWCKGEN1-0195',
                                                                   'SQL',
                                                                   'CRN') ||
                                                        '</ACRONYM>'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0196',
                                              'SQL',
                                              'Course'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0197',
                                              'SQL',
                                              'Course Title'),
                                   ccolspan => 3);
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0198',
                                              'SQL',
                                              'Credits'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0199',
                                              'SQL',
                                              'Level'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0200',
                                              'SQL',
                                              'Status'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0201',
                                              'SQL',
                                              'Midterm Grade'));
        twbkfrmt.p_tabledataheader(g$_nls.get('BWCKGEN1-0202',
                                              'SQL',
                                              'Final Grade'));
        twbkfrmt.p_tablerowclose;
        print_header := FALSE;
      END IF;
    
      twbkfrmt.P_TableRowOpen;
      twbkfrmt.P_TableData(regcrse.stvterm_desc);
      twbkfrmt.P_TableData(twbkfrmt.f_printanchor(twbkfrmt.f_encodeurl(detail_schedule_proc ||
                                                                       '?term_in=' ||
                                                                       regcrse.sfrstcr_term_code ||
                                                                       '&crn=' ||
                                                                       regcrse.sfrstcr_crn),
                                                  regcrse.ssbsect_crn,
                                                  cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0203',
                                                                                                    'SQL',
                                                                                                    'Detail'))));
      twbkfrmt.P_TableData(regcrse.ssbsect_subj_code || ' ' ||
                           regcrse.ssbsect_crse_numb);
      shrtckg_c_rec := NULL;
      OPEN shrtckg_c(pidm,
                     regcrse.sfrstcr_term_code,
                     regcrse.sfrstcr_crn,
                     regcrse.sfrstcr_levl_code);
      FETCH shrtckg_c
        INTO shrtckg_c_rec;
      CLOSE shrtckg_c;
    
      OPEN course_hyperlink_c(regcrse.sfrstcr_term_code,
                              regcrse.ssbsect_crn);
      FETCH course_hyperlink_c
        INTO tmp_hyper_ind;
      IF course_hyperlink_c%FOUND THEN
        twbkfrmt.p_tabledata(twbkfrmt.f_printanchor(curl        => twbkfrmt.f_encodeurl(twbkwbis.f_cgibin ||
                                                                                        'bwckschd.p_disp_listcrse' ||
                                                                                        '?term_in=' ||
                                                                                        twbkfrmt.f_encode(regcrse.sfrstcr_term_code) ||
                                                                                        '&subj_in=' ||
                                                                                        twbkfrmt.f_encode(regcrse.ssbsect_subj_code) ||
                                                                                        '&crse_in=' ||
                                                                                        twbkfrmt.f_encode(regcrse.ssbsect_crse_numb) ||
                                                                                        '&crn_in=' ||
                                                                                        twbkfrmt.f_encode(regcrse.ssbsect_crn)),
                                                    ctext       => NVL(shrtckg_c_rec.shrtckn_crse_title,
                                                                       bwcklibs.f_course_title(regcrse.sfrstcr_term_code,
                                                                                               regcrse.sfrstcr_crn)),
                                                    cattributes => bwckfrmt.f_anchor_focus(g$_nls.get('BWCKGEN1-0204',
                                                                                                      'SQL',
                                                                                                      'Dynamic Schedule'))),
                             ccolspan => 3);
      ELSE
        twbkfrmt.p_tabledata(NVL(shrtckg_c_rec.shrtckn_crse_title,
                                 bwcklibs.f_course_title(regcrse.sfrstcr_term_code,
                                                         regcrse.sfrstcr_crn)),
                             ccolspan => 3);
      END IF;
      CLOSE course_hyperlink_c;
    
      IF (regcrse.sfrstcr_grde_date IS NULL) THEN
        twbkfrmt.P_TableData(TO_CHAR(NVL(regcrse.sfrstcr_credit_hr,
                                         regcrse.scbcrse_credit_hr_low),
                                     '9990D990'));
      ELSE
        twbkfrmt.P_TableData(TO_CHAR(shrtckg_c_rec.shrtckg_credit_hours,
                                     '9990D990'));
      END IF;
    
      twbkfrmt.p_tableData(bwckcapp.get_levl_desc(NVL(shrtckg_c_rec.shrtckl_levl_code,
                                                      regcrse.sfrstcr_levl_code)));
      twbkfrmt.P_TableData(regcrse.stvrsts_desc || ' ' ||
                           TO_CHAR(regcrse.sfrstcr_rsts_date,
                                   twbklibs.date_display_fmt));
    
      IF f_grade_hold(pidm) THEN
        twbkfrmt.p_tabledatadead;
        twbkfrmt.p_tabledatadead;
      ELSE
        OPEN sfkcurs.midterm_display_c(pidm,
                                       regcrse.sfrstcr_term_code,
                                       regcrse.sfrstcr_crn);
        FETCH sfkcurs.midterm_display_c
          INTO midterm_display_c_rec;
      
        IF sfkcurs.midterm_display_c%FOUND THEN
          twbkfrmt.p_tabledata(regcrse.sfrstcr_grde_code_mid);
        ELSE
          twbkfrmt.p_tabledatadead;
        END IF;
      
        CLOSE sfkcurs.midterm_display_c;
      
        OPEN sfkcurs.final_grades_display_c(pidm,
                                            regcrse.sfrstcr_term_code,
                                            regcrse.sfrstcr_crn);
        FETCH sfkcurs.final_grades_display_c
          INTO final_grades_display_c_rec;
      
        IF sfkcurs.final_grades_display_c%FOUND THEN
          IF regcrse.sfrstcr_grde_date IS NULL THEN
            twbkfrmt.p_tabledatadead;
          ELSE
            twbkfrmt.p_tabledata(shrtckg_c_rec.shrtckg_grde_code_final);
          END IF;
        ELSE
          twbkfrmt.p_tabledatadead;
        END IF;
      
        CLOSE sfkcurs.final_grades_display_c;
      END IF;
    END LOOP;
  
    twbkfrmt.P_TableClose;
  
    IF row_count = 0 THEN
      twbkfrmt.p_printmessage(not_registered_message);
    END IF;
  END p_disp_reg_hist;

  --------------------------------------------------------------------------

  PROCEDURE p_disp_term_date(p_calling_proc VARCHAR2 DEFAULT NULL,
                             p_term         VARCHAR2 DEFAULT NULL,
                             p_from_date    VARCHAR2 DEFAULT NULL,
                             p_to_date      VARCHAR2 DEFAULT NULL,
                             p_error_msg    VARCHAR2 DEFAULT NULL) IS
    row_count NUMBER := 0;
  BEGIN
    IF p_calling_proc = 'P_CrseSearch' OR
       p_calling_proc = 'bwskfcls.p_disp_dyn_sched' OR
       p_calling_proc = 'P_FacCrseSearch' OR
       p_calling_proc = 'bwlkffcs.p_disp_dyn_sched' THEN
      /* IF NOT twbkwbis.F_ValidUser (global_pidm)
      THEN
         RETURN;
      END IF;*/
      NULL;
    END IF;
  
    IF p_error_msg IS NOT NULL THEN
      twbkwbis.P_DispInfo('bwckgens.p_disp_term_date', p_error_msg);
    END IF;
  
    HTP.formOpen(twbkwbis.f_cgibin || 'bwckgens.p_proc_term_date',
                 cattributes => 'onSubmit="return checkSubmit()"');
    twbkfrmt.P_FormHidden('p_calling_proc', p_calling_proc);
  
    -- Loop through soklibs.view_term_select_c and put terms
    -- into a drop down box
    FOR sobterm IN soklibs.view_term_select_c LOOP
      EXIT WHEN soklibs.view_term_select_c%NOTFOUND;
    
      IF soklibs.view_term_select_c%ROWCOUNT = 1 THEN
        twbkfrmt.p_tableopen('DATAENTRY',
                             cattributes => g$_nls.get('BWCKGEN1-0205',
                                                       'SQL',
                                                       'summary="This layout table is used for term selection."') ||
                                            'width="100%"',
                             ccaption    => g$_nls.get('BWCKGEN1-0206',
                                                       'SQL',
                                                       'Search by Term: '));
        twbkfrmt.p_tablerowopen;
        twbkfrmt.p_tabledataopen(twbkfrmt.f_formlabel(g$_nls.get('BWCKGEN1-0207',
                                                                 'SQL',
                                                                 'Term'),
                                                      visible => 'INVISIBLE',
                                                      idname => 'term_input_id'));
        HTP.formselectopen('p_term',
                           NULL,
                           1,
                           cattributes => ' ID="term_input_id"');
        twbkwbis.p_formselectoption(G$_NLS.Get('BWCKGEN1-0208',
                                               'SQL',
                                               'None'),
                                    NULL);
      END IF;
    
      IF sobterm.stvterm_code = p_term THEN
        twbkwbis.p_formselectoption(sobterm.descrip,
                                    sobterm.stvterm_code,
                                    'SELECTED');
      ELSE
        twbkwbis.p_formselectoption(sobterm.descrip, sobterm.stvterm_code);
      END IF;
    
      row_count := soklibs.view_term_select_c%ROWCOUNT;
    END LOOP;
  
    IF soklibs.view_term_select_c%ISOPEN THEN
      CLOSE soklibs.view_term_select_c;
    END IF;
  
    HTP.formselectclose;
    twbkfrmt.p_tabledataclose;
    twbkfrmt.p_tablerowclose;
    twbkfrmt.P_TableClose;
    HTP.br;
  
    -- If dates are allowed, allow input of dates
    IF NVL(SUBSTR(bwcklibs.f_getgtvsdaxrule('SCHBYDATE', 'WEBREG'), 1, 1),
           'N') = 'Y' THEN
      twbkfrmt.P_FormHidden('p_by_date', 'Y');
      twbkfrmt.P_PrintHeader('3', g$_nls.get('BWCKGEN1-0209', 'SQL', 'OR'));
      twbkfrmt.p_tableopen('DATAENTRY',
                           cattributes => g$_nls.get('BWCKGEN1-0210',
                                                     'SQL',
                                                     'summary="This layout table is used for date selection."') ||
                                          ' WIDTH="30%"',
                           ccaption    => g$_nls.get('BWCKGEN1-0211',
                                                     'SQL',
                                                     'Search by Date Range ( %01% ):',
                                                     g$_date.translate_format(twbklibs.date_input_fmt)));
      --
      twbkfrmt.p_tablerowopen;
      twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(g$_nls.get('BWCKGEN1-0212',
                                                                'SQL',
                                                                'From:'),
                                                     idname => 'from_id'));
      twbkfrmt.p_tabledataopen;
      twbkfrmt.p_formtext('p_from_date',
                          12,
                          12,
                          cvalue       => p_from_date,
                          cattributes  => ' ID="from_id"');
      twbkfrmt.p_tabledataclose;
      twbkfrmt.p_tabledatalabel(twbkfrmt.f_formlabel(g$_nls.get('BWCKGEN1-0213',
                                                                'SQL',
                                                                'To:'),
                                                     idname => 'to_id'));
      twbkfrmt.p_tabledataopen;
      twbkfrmt.p_formtext('p_to_date',
                          12,
                          12,
                          cvalue      => p_to_date,
                          cattributes => ' ID="to_id"');
      twbkfrmt.p_tabledataclose;
      twbkfrmt.p_tablerowclose;
      twbkfrmt.p_tableclose;
    END IF;
  
    -- If there are no terms return that message
    IF row_count = 0 THEN
      twbkfrmt.p_printtext(g$_nls.get('BWCKGEN1-0214',
                                      'SQL',
                                      'No term available'),
                           class_in => 'fieldlabeltext');
    ELSE
      HTP.br;
      HTP.formsubmit(NULL, g$_nls.get('BWCKGEN1-0215', 'SQL', 'Submit'));
      HTP.formreset(g$_nls.get('BWCKGEN1-0216', 'SQL', 'Reset'));
    END IF;
  
    HTP.formclose;
  END p_disp_term_date;

  PROCEDURE p_proc_term_date(p_calling_proc VARCHAR2 DEFAULT NULL,
                             p_term         VARCHAR2 DEFAULT NULL,
                             p_by_date      VARCHAR2 DEFAULT NULL,
                             p_from_date    VARCHAR2 DEFAULT NULL,
                             p_to_date      VARCHAR2 DEFAULT NULL) IS
    -- Cursor Declarations
  
    CURSOR term_c(search_start_date_in VARCHAR2,
                  search_end_date_in   VARCHAR2) IS
      SELECT a.sobterm_term_code
        FROM sobterm a
       WHERE a.sobterm_dynamic_sched_term_ind = 'Y'
         AND EXISTS
       (SELECT NULL
                FROM ssbsect
               WHERE ssbsect_term_code = a.sobterm_term_code
                 AND (NVL(ssbsect_ptrm_start_date, ssbsect_reg_from_date) BETWEEN
                     TO_DATE(search_start_date_in, twbklibs.date_input_fmt) AND
                     TO_DATE(search_end_date_in, twbklibs.date_input_fmt) OR
                     NVL(ssbsect_ptrm_end_date, ssbsect_reg_to_date) BETWEEN
                     TO_DATE(search_start_date_in, twbklibs.date_input_fmt) AND
                     TO_DATE(search_end_date_in, twbklibs.date_input_fmt) OR
                     TO_DATE(search_start_date_in, twbklibs.date_input_fmt) BETWEEN
                     NVL(ssbsect_ptrm_start_date, ssbsect_reg_from_date) AND
                     NVL(ssbsect_ptrm_end_date, ssbsect_reg_to_date)))
       ORDER BY 1;
  
    -- Variable declarations
    count_row     NUMBER;
    term_arr      OWA_UTIL.ident_arr;
    from_date_chk DATE;
    to_date_chk   DATE;
  BEGIN
    IF p_calling_proc = 'P_CrseSearch' OR
       p_calling_proc = 'bwskfcls.p_disp_dyn_sched' OR
       p_calling_proc = 'P_FacCrseSearch' OR
       p_calling_proc = 'bwlkffcs.p_disp_dyn_sched' THEN
      /*IF NOT twbkwbis.F_ValidUser (global_pidm)
      THEN
         RETURN;
      END IF;*/
      NULL;
    END IF;
  
    -- Open Form and Display Info text
    count_row   := 1;
    g_from_date := NULL;
    g_to_date   := NULL;
  
    IF p_by_date = 'Y' THEN
      IF p_from_date IS NOT NULL THEN
        BEGIN
          g_from_date := TO_DATE(p_from_date, twbklibs.date_input_fmt);
          g_to_date   := TO_DATE(p_to_date, twbklibs.date_input_fmt);
        
          FOR term_rec IN term_c(p_from_date, p_to_date) LOOP
            term_arr(count_row) := term_rec.sobterm_term_code;
            count_row := count_row + 1;
          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            NULL;
        END;
      
        IF term_c%ISOPEN THEN
          CLOSE term_c;
        END IF;
      ELSE
        term_arr(1) := p_term;
        count_row := count_row + 1;
      END IF;
    ELSE
      term_arr(1) := p_term;
      count_row := count_row + 1;
    END IF;
  
    IF count_row = 1 THEN
      term_arr(1) := NULL;
    END IF;
  
    -- if single term chosen
    IF p_term IS NOT NULL AND p_from_date IS NULL AND p_to_date IS NULL THEN
      CASE
        WHEN p_calling_proc = 'bwckschd.p_disp_dyn_sched' THEN
          bwckschd.p_crse_search_unsec(term_arr, FALSE, p_calling_proc);
        WHEN p_calling_proc = 'bwskfcls.p_disp_dyn_sched' THEN
          bwskfcls.p_disp_dyn_sched_search(term_arr, FALSE);
        WHEN p_calling_proc = 'P_CrseSearch' THEN
          bwskfcls.P_CrseSearch(term_arr, FALSE);
        WHEN p_calling_proc = 'bwlkffcs.p_disp_dyn_sched' THEN
          bwlkffcs.p_disp_dyn_sched_search(term_arr, FALSE);
        ELSE
          bwlkffcs.P_FacCrseSearch(term_arr, FALSE);
      END CASE;
      RETURN;
    ELSIF ( -- term chosen and date range entered
           p_term IS NOT NULL AND
           (p_from_date IS NOT NULL OR p_to_date IS NOT NULL)) OR
          ( -- neither term chosen nor date range entered
           p_term IS NULL AND (p_from_date IS NULL OR p_to_date IS NULL)) THEN
      p_return_to_disp_term_date(p_calling_proc,
                                 p_term,
                                 p_from_date,
                                 p_to_date,
                                 'NOTERM');
      RETURN;
    ELSIF ( -- just date range entered
           (p_from_date IS NOT NULL OR p_to_date IS NOT NULL) AND
           p_term IS NULL) THEN
      BEGIN
        IF TO_DATE(p_from_date, twbklibs.date_input_fmt) >
           TO_DATE(p_to_date, twbklibs.date_input_fmt) THEN
          p_return_to_disp_term_date(p_calling_proc,
                                     p_term,
                                     p_from_date,
                                     p_to_date,
                                     'BIGFROM');
          RETURN;
        END IF;
        from_date_chk := twbkwbis.F_FmtDate(p_from_date);
        to_date_chk   := twbkwbis.F_FmtDate(p_to_date);
      EXCEPTION
        WHEN OTHERS THEN
          p_return_to_disp_term_date(p_calling_proc,
                                     p_term,
                                     p_from_date,
                                     p_to_date,
                                     'FORMAT');
          RETURN;
      END;
    
      IF term_arr(1) IS NULL THEN
        p_return_to_disp_term_date(p_calling_proc,
                                   p_term,
                                   p_from_date,
                                   p_to_date,
                                   'BADTERM');
        RETURN;
      ELSE
        CASE
          WHEN p_calling_proc = 'bwckschd.p_disp_dyn_sched' THEN
            bwckschd.p_crse_search_unsec(term_arr, FALSE, p_calling_proc);
          WHEN p_calling_proc = 'bwskfcls.p_disp_dyn_sched' THEN
            bwskfcls.p_disp_dyn_sched_search(term_arr, FALSE);
          WHEN p_calling_proc = 'P_CrseSearch' THEN
            bwskfcls.P_CrseSearch(term_arr, FALSE);
          WHEN p_calling_proc = 'bwlkffcs.p_disp_dyn_sched' THEN
            bwlkffcs.p_disp_dyn_sched_search(term_arr, FALSE);
          ELSE
            bwlkffcs.P_FacCrseSearch(term_arr, FALSE);
        END CASE;
        RETURN;
      END IF;
    END IF;
  
  END p_proc_term_date;

  PROCEDURE p_return_to_disp_term_date(p_calling_proc VARCHAR2 DEFAULT NULL,
                                       p_term         VARCHAR2 DEFAULT NULL,
                                       p_from_date    VARCHAR2 DEFAULT NULL,
                                       p_to_date      VARCHAR2 DEFAULT NULL,
                                       p_error_msg    VARCHAR2 DEFAULT NULL) IS
  BEGIN
    CASE
      WHEN p_calling_proc = 'bwckschd.p_disp_dyn_sched' OR
           p_calling_proc = 'bwskfcls.p_disp_dyn_sched' OR
           p_calling_proc = 'bwlkffcs.p_disp_dyn_sched' THEN
        bwckschd.p_disp_term_date(p_calling_proc,
                                  p_term,
                                  p_from_date,
                                  p_to_date,
                                  p_error_msg);
      WHEN p_calling_proc = 'P_CrseSearch' THEN
        bwskfcls.p_disp_term_date(p_calling_proc,
                                  p_term,
                                  p_from_date,
                                  p_to_date,
                                  p_error_msg);
      ELSE
        -- p_calling_proc = 'P_FacCrseSearch'
        bwlkffcs.p_disp_term_date(p_calling_proc,
                                  p_term,
                                  p_from_date,
                                  p_to_date,
                                  p_error_msg);
    END CASE;
  
  END p_return_to_disp_term_date;
  procedure p_intglobal(p_pidm number, p_term stvterm.stvterm_code%type) is
  
  begin
    global_pidm := p_pidm;
    term        := p_term;
  end;
  procedure p_disp is
  
  begin
    dbms_output.put_line(global_pidm);
  end;
  PROCEDURE PZ_CHANGE_CREDITS(p_student_id VARCHAR2,
                              P_TERM_CODE  VARCHAR2,
                              P_CRN        NUMBER,
                              P_cred       NUMBER,
                              P_CRED_OLD   NUMBER,
                              P_ERROR      OUT VARCHAR2) IS
  
    crn              owa_util.ident_arr;
    cred             owa_util.ident_arr;
    gmod             owa_util.ident_arr;
    levl             owa_util.ident_arr;
    cred_old         owa_util.ident_arr;
    gmod_old         owa_util.ident_arr;
    levl_old         owa_util.ident_arr;
    term_in          stvterm.stvterm_code%type;
    global_pidm      spriden.spriden_pidm%type;
    save_act_date    varchar2(100);
    fa_return_status varchar2(100);
    V_LEVEL          STVLEVL.STVLEVL_CODE%TYPE;
    V_GMOD           VARCHAR2(10);
    V_CHECK          varchar2(10);
    sobterm_row      SOBTERM%ROWTYPE;
  begin
  
    global_pidm := baninst1.gb_common.f_get_pidm(p_student_id);
  
    if P_cred is null then
      P_ERROR := 'Credit hours can not be null';
      GOTO QUIT_TO_PROGRAM;
    end if;
    BEGIN
      SELECT A.SFRSTCR_LEVL_CODE, A.SFRSTCR_GMOD_CODE
        INTO V_LEVEL, V_GMOD
        FROM SFRSTCR A
       WHERE A.SFRSTCR_PIDM = global_pidm
         AND A.SFRSTCR_TERM_CODE = P_TERM_CODE
         AND A.SFRSTCR_CRN = P_CRN;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    V_CHECK := me_valid.fz_check_sfrrsts(P_TERM_CODE, p_crn, 'R');
    IF V_CHECK = 'N' THEN
      P_ERROR := 'Registration not available at this time';
      goto QUIT_TO_PROGRAM;
    END if;
    crn(1) := 'dummy';
    cred(1) := 'dummy';
    gmod(1) := 'dummy';
    levl(1) := 'dummy';
    cred_old(1) := 'dummy';
    gmod_old(1) := 'dummy';
    levl_old(1) := 'dummy';
    crn(2) := P_CRN;
    cred(2) := P_cred;
    gmod(2) := V_GMOD;
    levl(2) := V_LEVEL;
    cred_old(2) := P_CRED_OLD;
    gmod_old(2) := V_GMOD;
    levl_old(2) := V_LEVEL;
    -- Call the procedure
    me_bwckgens.p_intglobal(global_pidm, P_TERM_CODE);
    me_bwckgens.p_disp;
    bwcklibs.p_initvalue(global_pidm, P_TERM_CODE, '', '', '', '');
    -- Call update procedure
    twbkwbis.p_setparam(global_pidm, 'STUFAC_IND', 'STU');
    twbkwbis.p_setparam(global_pidm, 'TERM', P_TERM_CODE);
    me_bwckgens.p_regsupd(term     => P_TERM_CODE,
                          crn      => crn,
                          cred     => cred,
                          gmod     => gmod,
                          levl     => levl,
                          cred_old => cred_old,
                          gmod_old => gmod_old,
                          levl_old => levl_old,
                          P_ERROR  => P_ERROR);
  
    dbms_output.put_line('P_ERROR:=' || P_ERROR);
    IF P_ERROR IS NULL THEN
      BEGIN
        bwcklibs.p_getsobterm(p_term_code, sobterm_row);
        IF sobterm_row.sobterm_fee_assess_vr = 'Y' AND
           sobterm_row.sobterm_fee_assessment = 'Y' THEN
          SFKFEES.p_processfeeassessment(P_TERM_CODE,
                                         global_pidm,
                                         SYSDATE,
                                         SYSDATE,
                                         'R',
                                         'Y',
                                         'BWCKREGS',
                                         'Y',
                                         save_act_date,
                                         'N',
                                         fa_return_status);
        
          gb_common.p_commit;
        END IF;
      END;
    END IF;
    <<QUIT_TO_PROGRAM>>
    NULL;
    /*EXCEPTION WHEN OTHERS THEN
    P_ERROR:=SQLERRM;*/
  END PZ_CHANGE_CREDITS;

  function fz_get_var_ind(p_term varchar2, p_crn number) return varchar2 is
    cursor c_var is
      select *
        from ssbsect a
        join sobterm c
          on a.ssbsect_term_code = c.sobterm_term_code
         and nvl(c.sobterm_cred_web_upd_ind, 'N') = 'Y'
         AND A.SSBSECT_TERM_CODE = p_term
         AND A.SSBSECT_CRN = P_CRN
        join scbcrse b
          on a.ssbsect_subj_code = b.scbcrse_subj_code
         and a.ssbsect_crse_numb = b.scbcrse_crse_numb
         and b.scbcrse_eff_term =
             (select max(t.scbcrse_eff_term)
                from scbcrse t
               where t.scbcrse_subj_code = a.ssbsect_subj_code
                 and t.scbcrse_crse_numb = a.ssbsect_crse_numb
                 and t.scbcrse_eff_term <= a.ssbsect_term_code)
         AND B.SCBCRSE_CREDIT_HR_IND IS NOT NULL
         AND A.SSBSECT_CREDIT_HRS IS NULL
         AND A.SSBSECT_VOICE_AVAIL = 'Y';
  begin
  
    FOR I IN C_VAR LOOP
      RETURN 'Y';
    END LOOP;
    RETURN 'N';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'N';
  end;
  PROCEDURE PZ_withdrawl(p_student_id VARCHAR2,
                         P_TERM_CODE  VARCHAR2,
                         P_CRN        NUMBER,
                         P_RSTS       VARCHAR2,
                         P_ERROR      OUT VARCHAR2) IS
  
    global_pidm      spriden.spriden_pidm%type;
    save_act_date    varchar2(100);
    fa_return_status varchar2(100);
    V_LEVEL          STVLEVL.STVLEVL_CODE%TYPE;
    V_GMOD           VARCHAR2(10);
    V_CHECK          varchar2(10);
    v_sfrstcr        sfrstcr%rowtype;
    V_ROWID          ROWID;
  begin
  
    global_pidm := baninst1.gb_common.f_get_pidm(p_student_id);
    me_bwckgens.p_intglobal(global_pidm, P_TERM_CODE);
    me_bwckgens.p_disp;
    bwcklibs.p_initvalue(global_pidm, P_TERM_CODE, '', '', '', '');
  
    BEGIN
      select *
        into v_sfrstcr
        from sfrstcr
       where sfrstcr_pidm = global_pidm
         and sfrstcr_term_code = P_TERM_CODE
         and sfrstcr_crn = P_CRN;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
  
    sftregs_rec.sftregs_term_code := P_TERM_CODE;
    sftregs_rec.sftregs_pidm      := global_pidm;
    sftregs_rec.sftregs_crn       := P_CRN;
    /*sftregs_rec.sftregs_ptrm_code:=v_sfrstcr.sfrstcr_ptrm_code;*/
    sftregs_rec.sftregs_rsts_code := P_RSTS;
    /*sftregs_rec.sftregs_bill_hr:=v_sfrstcr.sfrstcr_bill_hr;
    sftregs_rec.sftregs_waiv_hr:=v_sfrstcr.sfrstcr_waiv_hr;
    sftregs_rec.sftregs_credit_hr:=v_sfrstcr.sfrstcr_credit_hr;*/
  
    /* BEGIN*/
    /* me_bwckregs.p_updcrse(sftregs_rec);*/
    add_course_pkg_corq.pz_student_course_register(p_pidm      => global_pidm,
                                                   p_term_in   => P_TERM_CODE,
                                                   p_crn_in    => P_CRN,
                                                   p_rsts_code => 'WW',
                                                   p_error_msg => P_ERROR,
                                                   p_rowid     => V_ROWID);
    /*me_bwckregs.p_updcrse(sftregs_rec,sftregs_rec.sftregs_crn);*/
  
    /*EXCEPTION WHEN OTHERS THEN
      P_ERROR:=SQLERRM;
    END;*/
  
    /*IF P_ERROR IS NULL THEN
    SFKFEES.p_processfeeassessment(P_TERM_CODE,
                                        global_pidm,
                                        SYSDATE,
                                        SYSDATE,
                                        'R',
                                        'Y',
                                        'BWCKREGS',
                                        'Y',
                                        save_act_date,
                                        'N',
                                        fa_return_status);
    
    
       gb_common.p_commit;
      END IF;*/
    <<QUIT_TO_PROGRAM>>
    NULL;
    /* EXCEPTION WHEN OTHERS THEN
    P_ERROR:=SQLERRM;*/
  END PZ_withdrawl;

---------------------------------------------------------------------------------
BEGIN
  /* initialization starts here */
  g_from_date := NULL;
  g_to_date   := NULL;
END ME_BWCKGENS;
/

