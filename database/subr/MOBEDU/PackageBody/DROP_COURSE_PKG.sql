PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.drop_course_pkg
CREATE OR REPLACE PACKAGE BODY drop_course_pkg AS

  procedure pz_drop_validations(p_pidm      in number,
                                p_term_code in varchar2,
                                p_crn       in ssbsect.ssbsect_crn%type,
                                p_error_msg out varchar2) is
  
    v_check    int;
    v_ptrm_chk varchar2(1);
    v_rsts     stvrsts.stvrsts_code%type;
  
  begin
  
    v_ptrm_chk := me_valid.fz_check_sfrrsts(p_term_code, p_crn, 'D');
  
    begin
      select count(*)
        into v_check
        from sftregs
       where sftregs_pidm = p_pidm
         and sftregs_term_code = p_term_code
         and sftregs_crn = p_crn;
    exception
      when no_data_found then
        v_check := 0;
    end;
  
    if not v_ptrm_chk <> 'N' then
      p_error_msg := 'Not permitted to drop at this time';
    end if;
  
    if p_error_msg is null then
    
      if v_check = 0 then
      
        p_error_msg := p_error_msg ||
                       'Course not registered, it cannot be dropped.';
      
      ELSE
        select a.sftregs_rsts_code
          into v_rsts
          from sftregs a
         where sftregs_pidm = p_pidm
           and sftregs_term_code = p_term_code
           and sftregs_crn = p_crn;
      
        if v_rsts not in ('RE', 'RW', 'WL') THEN
          p_error_msg := p_error_msg || 'Course cannot be dropped.';
        
        END IF;
      
      end if;
    
    end if;
  
  end;

  procedure pz_remove_drop_failures(p_drop_failures in sfkcurs.drop_problems_rec_tabtype,
                                    p_pidm          in spriden.spriden_pidm%type,
                                    p_term_code     in sftregs.sftregs_term_code%type) is
  
    v_failed_crn ssbsect.ssbsect_crn%type;
    v_error_link sftregs.sftregs_error_link%type;
    v_error_msg  varchar2(4000);
  
  begin
  
    FOR i IN 1 .. p_drop_failures.COUNT LOOP
    
      dbms_output.put_line('d_p_crn:' || p_drop_failures(i).crn);
    
      v_failed_crn := p_drop_failures(i).crn;
    
      begin
      
        select sftregs_error_link
          into v_error_link
          from sftregs
         where sftregs_pidm = p_pidm
           and sftregs_crn = p_drop_failures(i).crn
           and sftregs_term_code = p_term_code;
      
      exception
        when others then
        
          v_error_link := null;
        
      end;
    
      v_error_msg := bwcklibs.f_connected_crn_message(p_drop_failures(i)
                                                      .rmsg_cde,
                                                      p_drop_failures(i)
                                                      .message,
                                                      p_drop_failures(i)
                                                      .connected_crns);
      update sftregs
         set SFTREGS_ERROR_FLAG = 'F',
             SFTREGS_MESSAGE    = v_error_msg,
             SFTREGS_REC_STAT   = 'Q'
       where sftregs_pidm = p_pidm
         and sftregs_error_link = v_error_link
         and sftregs_term_code = p_term_code;
    
    END LOOP;
  
  end;

  procedure pz_drop_course(p_student_id in SPRIDEN.SPRIDEN_ID%TYPE,
                           p_term_code  in varchar2,
                           p_crn        in varchar2,
                           p_error_msg  out varchar2,
                           p_response   out varchar2) is
  
    V_ERROR varchar2(4000);
    v_pidm  number;
  
    v_styp_code   varchar2(10);
    v_tmst_code   varchar2(10);
    v_tmst_ind    varchar2(10);
    v_sftregs_rec sftregs%rowtype;
    v_error_out   varchar2(500);
    v_tmst_out    varchar2(500);
    v_add_date    sfrstcr.sfrstcr_add_date%type;
  
    fa_return_status number;
    save_act_date    VARCHAR2(200);
  
    drop_problems   sfkcurs.drop_problems_rec_tabtype;
    drop_failures   sfkcurs.drop_problems_rec_tabtype;
    capp_tech_error VARCHAR2(40);
    term_in         OWA_UTIL.ident_arr;
    sobterm_row     SOBTERM%ROWTYPE;
    CURSOR C_CRN IS
      SELECT TRIM(REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL)) CRN
        FROM DUAL
      CONNECT BY REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL) IS NOT NULL;
  
  BEGIN
  
    begin
      select baninst1.gb_common.f_get_pidm(p_student_id)
        into v_pidm
        from dual;
    exception
      when others then
        p_error_msg := sqlerrm || ': Unable to get student details';
    end;
  
    if p_error_msg is not null then
      return;
    end if;
    /*genpidm             := global_pidm;
    stufac_ind          := 'S';
    called_by_proc_name := 'bwskfreg.P_AddDropCrse';*/
    IF NOT
        add_course_pkg_corq.f_reg_access_still_good(v_pidm,
                                                    p_term_code,
                                                    'S' || v_pidm,
                                                    'bwskfreg.P_AddDropCrse') THEN
    
      p_error_msg := 'You have made too many attempts to register this term. Contact your registrar for assistance';
      return;
    else
      dbms_output.put_line('f_reg_access_still_good is true');
    
    END IF;
  
    sfkmods.p_delete_sftregs_by_pidm_term(v_pidm, p_term_code);
    dbms_output.put_line('delete1');
  
    sfkmods.p_insert_sftregs_from_stcr(v_pidm, p_term_code, SYSDATE);
    dbms_output.put_line('insert1');
  
    delete mobedu_trash
     where student_id = p_student_id
       and TERM_CODE = p_term_code;
  
    commit;
  
    dbms_output.put_line('Error:' || V_ERROR);
  
    bwcklibs.p_initvalue(v_pidm, p_term_code, '', '', '', '');
  
    FOR R_CRN IN C_CRN LOOP
    
      V_ERROR := NULL;
    
      insert into mobedu_trash
        (STUDENT_ID,
         STUDENT_PIDM,
         TERM_CODE,
         CRN,
         STATUS,
         ERROR_MSG,
         RSTS_CODE)
      values
        (p_student_id, v_pidm, p_term_code, R_CRN.CRN, null, null, 'DW');
    
      pz_drop_validations(v_pidm, p_term_code, R_CRN.CRN, V_ERROR);
    
      dbms_output.put_line('CRN:' || R_CRN.CRN || ';Error:' || V_ERROR);
    
      IF V_ERROR IS NULL THEN
        bwcklibs.p_del_sftregs(v_pidm, p_term_code, R_CRN.CRN, 'DW', 'C');
      ELSE
      
        update sftregs
           set SFTREGS_ERROR_FLAG = 'F',
               SFTREGS_MESSAGE    = V_ERROR,
               SFTREGS_REC_STAT   = 'Q'
         where sftregs_pidm = v_pidm
           and sftregs_crn = R_CRN.CRN
           and sftregs_term_code = p_term_code;
      
        update mobedu_trash
           set status = 'F', ERROR_MSG = V_ERROR
         where student_id = p_student_id
           and TERM_CODE = p_term_code
           and crn = R_CRN.CRN;
      
      END IF;
    
    END LOOP;
  
    term_in(1) := p_term_code;
  
    -- Validations start
    me_bwckregs.p_allcrsechk(term_in(1),
                             'ADD_DROP',
                             capp_tech_error,
                             drop_problems,
                             drop_failures,
                             term_in);
  
    FOR i IN 1 .. drop_problems.COUNT LOOP
      dbms_output.put_line('d_p_term_code:' || drop_problems(i).term_code);
      dbms_output.put_line('d_p_crn:' || drop_problems(i).crn);
      dbms_output.put_line('d_p_subj:' || drop_problems(i).subj);
      dbms_output.put_line('d_p_crse:' || drop_problems(i).crse);
      dbms_output.put_line('d_p_sec:' || drop_problems(i).sec);
      dbms_output.put_line('d_p_ptrm_code:' || drop_problems(i).ptrm_code);
      dbms_output.put_line('d_p_rmsg_cde:' || drop_problems(i).rmsg_cde);
      dbms_output.put_line('d_p_message:' || drop_problems(i).message);
      dbms_output.put_line('d_p_start_date:' || drop_problems(i)
                           .start_date);
      dbms_output.put_line('d_p_comp_date:' || drop_problems(i).comp_date);
      dbms_output.put_line('d_p_rsts_date:' || drop_problems(i).rsts_date);
      dbms_output.put_line('d_p_dunt_code:' || drop_problems(i).dunt_code);
      dbms_output.put_line('d_p_drop_code:' || drop_problems(i).drop_code);
      dbms_output.put_line('d_p_drop_conn:' || drop_problems(i)
                           .connected_crns);
      NULL;
    END LOOP;
  
    FOR i IN 1 .. drop_failures.COUNT LOOP
    
      dbms_output.put_line('d_p_term_code:' || drop_failures(i).term_code);
      dbms_output.put_line('d_p_crn:' || drop_failures(i).crn);
      dbms_output.put_line('d_p_subj:' || drop_failures(i).subj);
      dbms_output.put_line('d_p_crse:' || drop_failures(i).crse);
      dbms_output.put_line('d_p_sec:' || drop_failures(i).sec);
      dbms_output.put_line('d_p_ptrm_code:' || drop_failures(i).ptrm_code);
      dbms_output.put_line('d_p_rmsg_cde:' || drop_failures(i).rmsg_cde);
      dbms_output.put_line('d_p_message:' || drop_failures(i).message);
      dbms_output.put_line('d_p_start_date:' || drop_failures(i)
                           .start_date);
      dbms_output.put_line('d_p_comp_date:' || drop_failures(i).comp_date);
      dbms_output.put_line('d_p_rsts_date:' || drop_failures(i).rsts_date);
      dbms_output.put_line('d_p_dunt_code:' || drop_failures(i).dunt_code);
      dbms_output.put_line('d_p_drop_code:' || drop_failures(i).drop_code);
      dbms_output.put_line('d_p_drop_conn:' || drop_failures(i)
                           .connected_crns);
    
      dbms_output.put_line(bwcklibs.f_connected_crn_message(drop_failures(i)
                                                            .rmsg_cde,
                                                            drop_failures(i)
                                                            .message,
                                                            drop_failures(i)
                                                            .connected_crns));
    END LOOP;
  
    -- Validations end
  
    begin
    
      pz_remove_drop_failures(drop_failures, v_pidm, p_term_code);
    
    exception
      when others then
      
        dbms_output.put_line(SQLERRM);
        p_error_msg := 'Drop failures exist and failed to remove from the list;' ||
                       sqlerrm;
    end;
  
    if p_error_msg is not null then
    
      GB_COMMON.P_ROLLBACK;
    
      update mobedu_trash
         set status = 'F', ERROR_MSG = p_error_msg
       where student_id = p_student_id
         and TERM_CODE = p_term_code
         and status is null;
      GB_COMMON.P_COMMIT;
    
      return;
    
    end if;
  
    begin
    
      dbms_output.put_line('updating record in sfrstcr');
    
      baninst1.SFKEDIT.p_update_regs(pidm_in            => v_pidm,
                                     term_in            => p_term_code,
                                     reg_date_in        => v_add_date,
                                     clas_code_in       => '01',
                                     styp_code_in       => v_styp_code,
                                     capc_severity_in   => 'F',
                                     tmst_calc_ind_in   => 'Y',
                                     tmst_maint_ind_in  => v_tmst_ind,
                                     tmst_code_in       => v_tmst_code,
                                     drop_last_class_in => null,
                                     system_in          => 'WA',
                                     error_rec_out      => v_sftregs_rec,
                                     error_flag_out     => v_error_out,
                                     tmst_flag_out      => v_tmst_out);
    
      update (select t.status,
                     t.error_msg,
                     R.SFTREGS_ERROR_FLAG,
                     R.sftregs_message
                from mobedu_trash T, sftregs R
               where t.student_pidm = R.sftregs_pidm
                 and t.crn = sftregs_crn
                 and t.term_code = R.sftregs_term_code) UP
         set UP.status    = decode(SFTREGS_ERROR_FLAG, 'F', 'F', 'Y'),
             UP.error_msg = sftregs_message;
    
      fa_return_status := 0;
    
      /* Fee assessment after drop */
      BEGIN
        bwcklibs.p_getsobterm(p_term_code, sobterm_row);
        IF sobterm_row.sobterm_fee_assess_vr = 'Y' AND
           sobterm_row.sobterm_fee_assessment = 'Y' THEN
          SFKFEES.p_processfeeassessment(p_term_code,
                                         v_pidm,
                                         SYSDATE,
                                         SYSDATE,
                                         'R',
                                         'Y',
                                         'BWCKREGS',
                                         'Y',
                                         save_act_date,
                                         'N',
                                         fa_return_status);
        END IF;
      END;
      dbms_output.put_line('deleted and v_error value is ' || v_error_out);
      dbms_output.put_line('v_sftregs value is ' ||
                           v_sftregs_rec.sftregs_rsts_code);
      dbms_output.put_line('v_tmst value is ' || v_tmst_out);
    
    exception
      when others then
        p_error_msg := 'Exception in p_update_regs:' || sqlerrm;
    end;
  
    if p_error_msg is not null then
    
      GB_COMMON.P_ROLLBACK;
      update mobedu_trash
         set status = 'F', ERROR_MSG = p_error_msg
       where student_id = p_student_id
         and TERM_CODE = p_term_code
         and status is null;
      GB_COMMON.P_COMMIT;
      return;
    else
      GB_COMMON.P_COMMIT;
      P_RESPONSE := 'Course(s) dropped successfully.';
    
    end if;
  
  end;

begin

  null;

end drop_course_pkg;
/
