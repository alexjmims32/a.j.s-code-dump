PROMPT CREATE OR REPLACE PACKAGE BODY add_course_pkg_corq
CREATE OR REPLACE PACKAGE BODY add_course_pkg_corq AS


  P_ROWID_STCR      VARCHAR2(50);
  p_error_msg       varchar2(4000);
  preq_errors_found BOOLEAN;

  capp_tech_error_out  VARCHAR2(100);
  term_list_in         OWA_UTIL.ident_arr;
  sgbstdn_row          sgbstdn%rowtype;
  clas_desc            varchar2(400);
  cast_prevent_reg_ind stvcast.stvcast_prevent_reg_ind%type;
  astd_prevent_reg_ind stvastd.stvastd_prevent_reg_ind%type;
  p_student_id         varchar2(10);
  global_pidm          SPRIDEN.SPRIDEN_PIDM%TYPE;
  genpidm              SPRIDEN.SPRIDEN_PIDM%TYPE;
  row_count            NUMBER;
  seq                  ssbsect.ssbsect_seq_numb%type;
  stud_level           varchar2(40);
  stvterm_rec          stvterm%ROWTYPE;
  sorrtrm_rec          sorrtrm%ROWTYPE;
  sfbetrm_rec          sfbetrm%ROWTYPE;
  sftregs_rec          sftregs%ROWTYPE;
  scbcrse_row          scbcrse%ROWTYPE;
  sfrbtch_row          sfrbtch%ROWTYPE;
  tbrcbrq_row          tbrcbrq%ROWTYPE;
  menu_name            VARCHAR2(60);
  ssbsect_row          ssbsect%ROWTYPE;
  stvrsts_row          stvrsts%ROWTYPE;
  sobterm_row          sobterm%ROWTYPE;
  sql_error            NUMBER;
  term_desc            stvterm%ROWTYPE;
  regs_date            date;
  gv_term_in           STVTERM.STVTERM_CODE%TYPE;
  gv_crn_in            SSBSECT.SSBSECT_CRN%TYPE;
  lcur_tab             sokccur.curriculum_savedtab;
  use_man_control      VARCHAR2(1);
  internal_code        gtvsdax.gtvsdax_internal_code%TYPE;
  gtvsdax_group        gtvsdax.gtvsdax_internal_code_group%TYPE;
  sgbstdn_rec          sgbstdn%ROWTYPE;
  rsts                 varchar2(20);
  old_stvrsts_row      stvrsts%rowtype;
  old_sftregs_row      sftregs%rowtype;
  sftregs_row          sftregs%rowtype;
  sgrclsr_clas_code    varchar2(200);
  override             varchar2(50);
  term                 STVTERM.STVTERM_CODE%TYPE;
  id                   varchar2(10);
  hold_passwd          varchar2(40);
  samsys               varchar2(40);
  camp                 varchar2(40);
  levl                 varchar2(40);
  error_rec_out        sftregs%ROWTYPE;
  error_flag_out       VARCHAR2(40);
  tmst_flag_out        VARCHAR2(40);
  stst_reg_ind         stvstst.stvstst_reg_ind%TYPE;
  mhrs_min_hrs         sfrmhrs.sfrmhrs_min_hrs%TYPE;
  advr_row             soklibs.advr_rec;
  mhrs_max_hrs         sfrmhrs.sfrmhrs_max_hrs%TYPE;
  minh_srce            sfbetrm.sfbetrm_minh_srce_cde%TYPE;
  maxh_srce            sfbetrm.sfbetrm_maxh_srce_cde%TYPE;
  /*
  row_count               NUMBER;
  global_pidm             spriden.spriden_pidm%TYPE;
  term                    stvterm.stvterm_code%TYPE;
  stvterm_rec             stvterm%ROWTYPE;
  sorrtrm_rec             sorrtrm%ROWTYPE;
  sfbetrm_rec             sfbetrm%ROWTYPE;

  foo_rec                 sfbetrm%ROWTYPE;
  scbcrse_row             scbcrse%ROWTYPE;
  term_desc               stvterm%ROWTYPE;
  sobterm_row             sobterm%ROWTYPE;*/

  procedure pz_rect5_prev_errs_nd_ad_2_crt(p_student_id in varchar2,
                                           p_term_in    in varchar2,
                                           p_error      out varchar2) is
  begin

    update mobedu_cart
       set error_flag = 'N', processed_ind = 'N', Status = ''
     where error_flag = 'Y'
       and student_id = p_student_id
       and term = p_term_in;

    gb_common.p_commit;

  exception
    when others then
      gb_common.p_commit;
      p_error := 'Failed to rectify previous errors in the cart';
  end pz_rect5_prev_errs_nd_ad_2_crt;

  /*  procedure pz_check_cart_empty(p_student_id in varchar2,
                                p_term_in    in varchar2,
                                p_error      out varchar2) is
    do_i_quit varchar2(1);
    cursor c_is_cart_empty is
      select decode(count(*), 0, 'Y', 'N')
        from MOBILE_APPS.mobedu_cart
       where student_id = p_student_id
         and term = p_term_in
         and processed_ind = 'N';
  begin
    open c_is_cart_empty;
    fetch c_is_cart_empty
      into do_i_quit;
    if do_i_quit = 'Y' then
      p_error := 'Empty Cart!';
      goto quit_from_program;

    end if;
    close c_is_cart_empty;
  end pz_check_cart_empty;*/

  PROCEDURE p_regs(term_in       IN OWA_UTIL.ident_arr,
                   rsts_in       IN OWA_UTIL.ident_arr,
                   assoc_term_in IN OWA_UTIL.ident_arr,
                   crn_in        IN OWA_UTIL.ident_arr,
                   start_date_in IN OWA_UTIL.ident_arr,
                   end_date_in   IN OWA_UTIL.ident_arr,
                   subj          IN OWA_UTIL.ident_arr,
                   crse          IN OWA_UTIL.ident_arr,
                   sec           IN OWA_UTIL.ident_arr,
                   levl          IN OWA_UTIL.ident_arr,
                   cred          IN OWA_UTIL.ident_arr,
                   gmod          IN OWA_UTIL.ident_arr,
                   title         IN bwckcoms.varchar2_tabtype,
                   mesg          IN OWA_UTIL.ident_arr,
                   reg_btn       IN OWA_UTIL.ident_arr,
                   regs_row      NUMBER,
                   add_row       NUMBER,
                   wait_row      NUMBER,
                   error_msg     out varchar2) IS
    i                     INTEGER := 2;
    k                     INTEGER := 1;
    err_term              OWA_UTIL.ident_arr;
    err_crn               OWA_UTIL.ident_arr;
    err_subj              OWA_UTIL.ident_arr;
    err_crse              OWA_UTIL.ident_arr;
    err_sec               OWA_UTIL.ident_arr;
    err_code              OWA_UTIL.ident_arr;
    err_levl              OWA_UTIL.ident_arr;
    err_cred              OWA_UTIL.ident_arr;
    err_gmod              OWA_UTIL.ident_arr;
    clas_code             SGRCLSR.SGRCLSR_CLAS_CODE%TYPE;
    stvterm_rec           stvterm%ROWTYPE;
    sorrtrm_rec           sorrtrm%ROWTYPE;
    stufac_ind            VARCHAR2(1);
    term                  stvterm.stvterm_code%TYPE := NULL;
    multi_term            BOOLEAN := TRUE;
    olr_course_selected   BOOLEAN := FALSE;
    capp_tech_error       VARCHAR2(4);
    assoc_term            OWA_UTIL.ident_arr := assoc_term_in;
    crn                   OWA_UTIL.ident_arr;
    crn_index             NUMBER;
    crn_in_index          NUMBER;
    start_date            OWA_UTIL.ident_arr := start_date_in;
    end_date              OWA_UTIL.ident_arr := end_date_in;
    etrm_done             BOOLEAN := FALSE;
    drop_problems         sfkcurs.drop_problems_rec_tabtype;
    drop_failures         sfkcurs.drop_problems_rec_tabtype;
    local_capp_tech_error VARCHAR2(30);
    called_by_proc_name   VARCHAR2(100);
    web_rsts_checkc_flag  VARCHAR2(1);
    lv_reg_btn1           VARCHAR2(20);

    CURSOR web_rsts_checkc(rsts_in VARCHAR2) IS
      SELECT 'Y'
        FROM stvrsts
       WHERE stvrsts_code = rsts_in
         AND stvrsts_web_ind = 'Y';

  BEGIN
    dbms_output.put_line('Term Number .....');

    term := gv_term_in;
    /* term := TO_CHAR(term_in.count);*/
    dbms_output.put_line('TERM : ' || term);

    term := term_in(term_in.COUNT);

    /*for i in 1 .. term_in.count loop
      term := term_in(term_in.COUNT);
      dbms_output.put_line('Term : ' || term);

    end loop;*/

    dbms_output.put_line('Rsts Status.....');

    rsts := rsts_in(rsts_in.COUNT);
    dbms_output.put_line('Rsts : ' || rsts);

    /* for i in 1 .. rsts_in.count loop
      rsts := rsts_in(rsts_in.COUNT);
      dbms_output.put_line('Rsts : ' || rsts);

    end loop;*/

    IF term_in.COUNT = 1 THEN
      multi_term := FALSE;
    END IF;
    --remove this start here
    dbms_output.put_line('Multi Term Status ..... Check below');

    if multi_term then
      dbms_output.put_line('Multiterm : TRUE');

    else
      dbms_output.put_line('Multiterm : FALSE');

    end if;
    --remove this END here
    -- Remove any null leading crns from the to-add list.
    dbms_output.put_line('CRN Status ..... Check below');
    crn_index := 1;
    FOR crn_in_index IN 1 .. crn_in.COUNT LOOP
      IF crn_in(crn_in_index) IS NOT NULL THEN
        crn(crn_index) := crn_in(crn_in_index);
        DBMS_OUTPUT.PUT_LINE('CRN : ' || crn(crn_index));

        crn_index := crn_index + 1;
      END IF;
    END LOOP;
    --
    dbms_output.put_line('Student or Faculty Status ..... Check below');

    --------------------------------------------------------------------------------
    /* Assumption is that it is student  always */

    genpidm             := global_pidm;
    stufac_ind          := 'S';
    called_by_proc_name := 'bwskfreg.P_AddDropCrse';
    dbms_output.put_line('Student : ' || called_by_proc_name);

    --Need to check   f_reg_access_still_good if it is false then need to raise a exception

    FOR i IN 1 .. term_in.COUNT LOOP
      IF NOT f_reg_access_still_good(genpidm,
                                     term_in(i),
                                     stufac_ind || global_pidm,
                                     called_by_proc_name) THEN

        error_msg := 'You have made too many attempts to register this term. Contact your registrar for assistance';
        return;
      else
        dbms_output.put_line('f_reg_access_still_good is true');

      END IF;
    END LOOP;
    ---------------------------------------------------------------------------------
    dbms_output.put_line('Checking Registration Status Web Allowing Indicator ...');

    FOR i IN 1 .. rsts_in.COUNT LOOP
      dbms_output.put_line('Registration Status Value :' || rsts_in(i));

      IF rsts_in(i) IS NOT NULL THEN
        OPEN web_rsts_checkc(rsts_in(i));
        FETCH web_rsts_checkc
          INTO web_rsts_checkc_flag;
        IF web_rsts_checkc%NOTFOUND THEN
          error_msg := 'Registration through web is not allowed for this term.';
          return;
          dbms_output.put_line('Registration Status Web Indicator not allowed.');

          CLOSE web_rsts_checkc;
        ELSE
          dbms_output.put_line('Registration Status Web Indicator Allowed.');

        END IF;

        CLOSE web_rsts_checkc;
      END IF;
    END LOOP;

    IF NOT multi_term THEN
      -- This added for security reasons, in order to prevent students
      -- saving the add/droppage while registration is open, and
      -- re-using the saved page after it has closed
      dbms_output.put_line('Validating Term ...' || term);

      IF NOT f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
        error_msg := 'Registration is not allowed for this term.';
        return;
        dbms_output.put_line('f_validterm returned false.');

        --        error_msg := 'Term is not allowed for Registration.';
      ELSE
        dbms_output.put_line('f_validterm returned true.');

      END IF;
    END IF;

    -- Check if student is allowed to register - single term
    -- only, as multi term is handled in the search results
    -- page.
    -- ===================================================
    dbms_output.put_line('Calling bwcksams.f_regsstu Function...');

    i := term_in.count;
    DECLARE
    BEGIN
      dbms_output.put_line('CRN in Main : ' || crn_in(i));
      dbms_output.put_line('Rsts in Main : ' || rsts_in(i));
      dbms_output.put_line('LTRIM(RTRIM(rsts_in(i))) : ' ||
                           LTRIM(RTRIM(rsts_in(i))));

      IF crn_in(i) IS NOT NULL AND LTRIM(RTRIM(rsts_in(i))) IS NOT NULL THEN
        sftregs_rec.sftregs_crn       := crn_in(i);
        sftregs_rec.sftregs_rsts_code := rsts_in(i);
        sftregs_rec.sftregs_rsts_date := SYSDATE;
        sftregs_rec.sftregs_pidm      := genpidm;
        sftregs_rec.sftregs_term_code := term_in(i);

        dbms_output.put_line('Register based on crn code');

        BEGIN
          IF multi_term THEN
            --
            -- Get the latest student and registration records.
            -- ===================================================
            dbms_output.put_line('calling me_bwckcoms.p_regs_etrm_chk for multiterm');
            me_bwckcoms.p_regs_etrm_chk(genpidm,
                                        term_in(i),
                                        clas_code,
                                        multi_term);
          ELSIF NOT etrm_done THEN
            dbms_output.put_line('calling me_bwckcoms.p_regs_etrm_chk for single-term');
            me_bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);
            etrm_done := TRUE;
          END IF;

          --
          -- Get the section information.
          -- ===================================================
          begin
            dbms_output.put_line('calling me_bwckregs.p_getsection');
            me_bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                                     sftregs_rec.sftregs_crn,
                                     sftregs_rec.sftregs_sect_subj_code,
                                     sftregs_rec.sftregs_sect_crse_numb,
                                     sftregs_rec.sftregs_sect_seq_numb);
          exception
            when others then
              error_msg := 'Unable to get course section details';
              return;
          end;

          dbms_output.put_line('get section in Main passed.');
          dbms_output.put_line('SUBJ : ' ||
                               sftregs_rec.sftregs_sect_subj_code);
          dbms_output.put_line('CRSE : ' ||
                               sftregs_rec.sftregs_sect_crse_numb);
          dbms_output.put_line('SEQ : ' ||
                               sftregs_rec.sftregs_sect_seq_numb);
          dbms_output.put_line('CRN : ' || sftregs_rec.sftregs_crn);
          --          dbms_output.put_line('Start Date : ' || start_date(i));
          --          dbms_output.put_line('End Date : ' || end_date(i));

          sftregs_rec.sftregs_pidm      := genpidm;
          sftregs_rec.sftregs_term_code := term;
          sftregs_rec.sftregs_rsts_code := rsts;
          sftregs_rec.sftregs_ptrm_code := f_get_ptrm_code(sftregs_rec.sftregs_crn,
                                                           term);
          begin
            me_bwckregs.p_addcrse(sftregs_rec,
                                  sftregs_rec.sftregs_sect_subj_code,
                                  sftregs_rec.sftregs_sect_crse_numb,
                                  sftregs_rec.sftregs_sect_seq_numb,
                                  null,
                                  null);

          exception
            when others then
              error_msg := 'Exception while adding the course.';
              DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
              DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
              RAISE;
          end;

          dbms_output.put_line('Course is added to sftregs.');
          --
          -- Create a batch fee assessment record.
          -- ===================================================
          sfrbtch_row.sfrbtch_term_code     := term_in(1);
          sfrbtch_row.sfrbtch_pidm          := genpidm;
          sfrbtch_row.sfrbtch_clas_code     := clas_code;
          sfrbtch_row.sfrbtch_activity_date := SYSDATE;
          bwcklibs.p_add_sfrbtch(sfrbtch_row);
          dbms_output.put_line('Add sfrbtch passed.');
          tbrcbrq_row.tbrcbrq_term_code     := term_in(1);
          tbrcbrq_row.tbrcbrq_pidm          := genpidm;
          tbrcbrq_row.tbrcbrq_activity_date := SYSDATE;
          bwcklibs.p_add_tbrcbrq(tbrcbrq_row);
          dbms_output.put_line('Add tbrcbrq passed.');

        EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line('Exception Raised in Main Procedure' ||
                                 sqlerrm);
            DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
            DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            error_msg := sqlerrm;
            return;

            /*  IF SQLCODE = gb_event.APP_ERROR THEN
              twbkfrmt.p_storeapimessages(SQLERRM);
              RAISE;
            END IF; */

            k := k + 1;
            err_term(k) := term_in(i);
            err_crn(k) := crn_in(i);
            err_subj(k) := sftregs_rec.sftregs_sect_subj_code;
            err_crse(k) := sftregs_rec.sftregs_sect_crse_numb;
            err_sec(k) := sftregs_rec.sftregs_sect_seq_numb;
            err_code(k) := SQLCODE;
            err_levl(k) := lcur_tab(1).r_levl_code;
            err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
            err_gmod(k) := sftregs_rec.sftregs_gmod_code;
        END;

      ELSE
        BEGIN

          dbms_output.put_line('Register based on subject code');
          --
          -- ?????
          -- ===================================================
          IF subj(i) IS NOT NULL AND crse(i) IS NOT NULL AND
             sec(i) IS NOT NULL AND LTRIM(RTRIM(rsts_in(i))) IS NOT NULL THEN
            BEGIN
              sftregs_rec.sftregs_rsts_code := rsts_in(i);
              sftregs_rec.sftregs_rsts_date := SYSDATE;
              sftregs_rec.sftregs_pidm      := genpidm;
              sftregs_rec.sftregs_term_code := term_in(i);

              IF multi_term THEN
                --
                -- Get the latest student and registration records.
                -- ===================================================
                bwckcoms.p_regs_etrm_chk(genpidm,
                                         term_in(i),
                                         clas_code,
                                         multi_term);
              ELSIF NOT etrm_done THEN
                me_bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);
                etrm_done := TRUE;
              END IF;

              me_bwckregs.p_addcrse(sftregs_rec,
                                    subj(i),
                                    crse(i),
                                    sec(i),
                                    null,
                                    null);
              --                        COMMIT;
              --                        commit_flag := 'Y';
            EXCEPTION
              WHEN OTHERS THEN
                IF SQLCODE = gb_event.APP_ERROR THEN
                  twbkfrmt.p_storeapimessages(SQLERRM);
                  RAISE;
                END IF;
                k := k + 1;
                err_term(k) := term_in(i);
                err_crn(k) := crn_in(i);
                err_subj(k) := subj(i);
                err_crse(k) := crse(i);
                err_sec(k) := sec(i);
                err_code(k) := SQLCODE;
                err_levl(k) := lcur_tab(1).r_levl_code;
                err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
                err_gmod(k) := sftregs_rec.sftregs_gmod_code;
                --                           ROLLBACK;
              --                           commit_flag := 'N';
            END;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            IF SQLCODE = gb_event.APP_ERROR THEN
              twbkfrmt.p_storeapimessages(SQLERRM);
              RAISE;
            END IF;
            NULL;
        END;
      END IF;

      -- i := i + 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        BEGIN
          IF subj(i) IS NOT NULL AND crse(i) IS NOT NULL AND
             sec(i) IS NOT NULL THEN
            BEGIN
              sftregs_rec.sftregs_rsts_code := rsts_in(i);
              sftregs_rec.sftregs_rsts_date := SYSDATE;
              sftregs_rec.sftregs_pidm      := genpidm;
              sftregs_rec.sftregs_term_code := term_in(i);

              IF multi_term THEN
                --
                -- Get the latest student and registration records.
                -- ===================================================
                bwckcoms.p_regs_etrm_chk(genpidm,
                                         term_in(i),
                                         clas_code,
                                         multi_term);
              ELSIF NOT etrm_done THEN
                me_bwckcoms.p_regs_etrm_chk(genpidm, term, clas_code);
                etrm_done := TRUE;
              END IF;

              p_addcrse(sftregs_rec,
                        subj(i),
                        crse(i),
                        sec(i),
                        start_date(i),
                        end_date(i));
              --                        COMMIT;
              --                        commit_flag := 'Y';
            EXCEPTION
              WHEN OTHERS THEN
                IF SQLCODE = gb_event.APP_ERROR THEN
                  twbkfrmt.p_storeapimessages(SQLERRM);
                  RAISE;
                END IF;
                k := k + 1;
                err_term(k) := term_in(i);
                err_crn(k) := crn_in(i);
                err_subj(k) := subj(i);
                err_crse(k) := crse(i);
                err_sec(k) := sec(i);
                err_code(k) := SQLCODE;
                err_levl(k) := lcur_tab(1).r_levl_code;
                err_cred(k) := TO_CHAR(sftregs_rec.sftregs_credit_hr);
                err_gmod(k) := sftregs_rec.sftregs_gmod_code;
                --                           ROLLBACK;
              --                           commit_flag := 'N';
            END;
          END IF;
        EXCEPTION
          WHEN OTHERS THEN

            error_msg := sqlerrm;
            return;
            /* IF SQLCODE = gb_event.APP_ERROR THEN
              twbkfrmt.p_storeapimessages(SQLERRM);
              RAISE;
            END IF;*/

            NULL;

        END;

      --    i := i + 1;
    END;
    --END LOOP;

    --
    -- Do batch validation on all registration records.
    -- ===================================================
    dbms_output.put_line('Term : ' || term_in(term_in.count));
    dbms_output.put_line('PIDM : ' || sftregs_rec.sftregs_pidm);
    dbms_output.put_line('CAPP_TECH_ERROR : ' || capp_tech_error);

    /*   dbms_output.put_line('DROP PROBLEMS : ' || drop_problems);
    dbms_output.put_line('DROP FAILURES : ' || drop_failures);*/
    /*  begin
      me_bwckcoms.p_group_edits(term_in,
                                genpidm,
                                etrm_done,
                                capp_tech_error,
                                drop_problems,
                                drop_failures);
    exception
      when others then
        error_msg := 'Pre Checks Failed.';
    end;*/

    /*-- This call may not be required as required validations are already done
    begin
      dbms_output.put_line('Calling me_bwckcoms.p_group_edits....');
      dbms_output.put_line('global_pidm : ' || global_pidm);
      me_bwckcoms.p_group_edits(term_in,
                                global_pidm,
                                etrm_done,
                                capp_tech_error,
                                drop_problems,
                                drop_failures);
      dbms_output.put_line('me_bwckcoms.p_group_edits completed.');
    exception
      when others then
        error_msg := 'Failed to do Pre-checks.';
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        RAISE;
    end;*/

    /*  dbms_output.put_line('p_group_edits passed, and tech error is' ||
    capp_tech_error); */

    /* If capp_tech_error is null then

        me_bwckcoms.p_problems(term_in,
                               err_term,
                               err_crn,
                               err_subj,
                               err_crse,
                               err_sec,
                               err_code,
                               err_levl,
                               err_cred,
                               err_gmod,
                               drop_problems,
                               drop_failures);
      end if;

    EXCEPTION
      WHEN OTHERS THEN

        dbms_output.put_line('Error in me_bwckcoms.p_problems:' || sqlerrm);

        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        RAISE;

        IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN

          IF NOT bwlkilib.f_add_drp(term) THEN
            NULL;
          END IF;

        ELSE

          IF NOT f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
            NULL;

          END IF;

        END IF; */

  END p_regs;

  procedure pz_student_course_register(p_pidm      in number,
                                       p_term_in   in STVTERM.STVTERM_CODE%TYPE,
                                       p_crn_in    in ssbsect.ssbsect_crn%type,
                                       P_RSTS_CODE IN SFRSTCR.SFRSTCR_RSTS_CODE%type,
                                       p_error_msg out varchar2,
                                       p_rowid     out varchar2) is
    -- Non-scalar parameters require additional processing
    term_in        owa_util.ident_arr;
    rsts_in        owa_util.ident_arr;
    assoc_term_in  owa_util.ident_arr;
    crn_in         owa_util.ident_arr;
    start_date_in  owa_util.ident_arr;
    end_date_in    owa_util.ident_arr;
    subj           owa_util.ident_arr;
    crse           owa_util.ident_arr;
    sec            owa_util.ident_arr;
    levl           owa_util.ident_arr;
    cred           owa_util.ident_arr;
    gmod           owa_util.ident_arr;
    title          bwckcoms.varchar2_tabtype;
    mesg           owa_util.ident_arr;
    reg_btn        owa_util.ident_arr;
    regs_row       NUMBER;
    add_row        NUMBER;
    wait_row       NUMBER;
    id_in          VARCHAR2(10);
    regs_date_in   DATE;
    hold_passwd_in VARCHAR2(70);
    samsys_in      VARCHAR2(70);
    --   global_pidm    NUMBER;
    cursor c_get_course_details(cv_crn     in SSBSECT.SSBSECT_CRN%TYPE,
                                cv_term_in in STVTERM.STVTERM_CODE%TYPE) is
      select *
        from ssbsect
       where ssbsect_crn = cv_crn
         and ssbsect_term_code = cv_term_in;

    cursor c_get_id(cv_pidm in number) is
      select *
        from spriden
       where spriden_pidm = cv_pidm
         and spriden_change_ind is null;
  begin
    --set global variables here
    dbms_output.put_line('Initializing global variables.');

    global_pidm := p_pidm;
    gv_term_in  := p_term_in;
    gv_crn_in   := p_crn_in;
    dbms_output.put_line('global variables Initialized..');

    --Collect Parameters data here
    for r_get_course_details in c_get_course_details(p_crn_in, p_term_in) loop
      subj(1) := r_get_course_details.ssbsect_subj_code;
      crse(1) := r_get_course_details.ssbsect_crse_numb;
      term_in(1) := r_get_course_details.ssbsect_term_code;
      rsts_in(1) := nvl(P_RSTS_CODE, 'RW');
      gmod(1) := r_get_course_details.ssbsect_gmod_code;
      cred(1) := r_get_course_details.ssbsect_credit_hrs;
      crn_in(1) := r_get_course_details.ssbsect_crn;
      seq := r_get_course_details.ssbsect_seq_numb;
      camp := r_get_course_details.ssbsect_camp_code;
    end loop;
    id_in := baninst1.gb_common.f_Get_id(p_pidm);
    --set global variables here
    dbms_output.put_line('Values Passed .... Check below');

    dbms_output.put_line('PIDM : ' || global_pidm);

    dbms_output.put_line('TERM : ' || term_in(1));

    dbms_output.put_line('RSTS : ' || rsts_in(1));

    dbms_output.put_line('CRN : ' || crn_in(1));

    dbms_output.put_line('SUBJ : ' || subj(1));

    dbms_output.put_line('CRSE : ' || crse(1));

    dbms_output.put_line('GMOD : ' || gmod(1));

    dbms_output.put_line('CREDIT HOURS : ' || cred(1));
    dbms_output.put_line('CAMPUS CODE : ' || camp);
    dbms_output.put_line('Initializing Values : ');
    dbms_output.put_line('Passing Control to p_initvalue ...');

    bwcklibs.p_initvalue(global_pidm, gv_term_in, '', '', '', '');
    -- Call the main procedure here

    begin
      delete from sfrstcr
       where sfrstcr_pidm = p_pidm
         and sfrstcr_term_code = p_term_in
         and sfrstcr_error_flag = 'F'
         and sfrstcr_message is not null
         and sfrstcr_rsts_code = 'DW';
    end;

    dbms_output.put_line('Passing Control to p_regs ...');

    p_regs(term_in       => term_in,
           rsts_in       => rsts_in,
           assoc_term_in => assoc_term_in,
           crn_in        => crn_in,
           start_date_in => start_date_in,
           end_date_in   => end_date_in,
           subj          => subj,
           crse          => crse,
           sec           => sec,
           levl          => levl,
           cred          => cred,
           gmod          => gmod,
           title         => title,
           mesg          => mesg,
           reg_btn       => reg_btn,
           regs_row      => '0',
           add_row       => add_row,
           wait_row      => wait_row,
           error_msg     => p_error_msg);

    dbms_output.put_line('Control is in p_regs.');

    IF (sb_course_registration.f_exists(p_term_in, p_pidm, p_crn_in) = 'Y') THEN
      p_student_id := baninst1.gb_common.f_Get_id(p_pidm);
      p_error_msg  := 'Student already registered for this course.';

      return;
    END IF;

    sftregs_rec.sftregs_levl_code := sftregs_row.sftregs_levl_code;

    sftregs_rec.sftregs_camp_code := camp;

    dbms_output.put_line('pz_student_course_register procedure completed.');

    if p_error_msg is not null then
      p_student_id := baninst1.gb_common.f_Get_id(p_pidm);

      return;
    end if;
  exception
    when others then
      p_error_msg  := 'Failed to Register the Student due to . ' || sqlerrm;
      p_student_id := baninst1.gb_common.f_Get_id(p_pidm);

  end pz_student_course_register;

  FUNCTION f_reg_access_still_good(pidm_in      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                                   term_in      IN STVTERM.STVTERM_CODE%TYPE,
                                   call_path_in IN VARCHAR2,
                                   proc_name_in IN VARCHAR2) RETURN BOOLEAN IS
    reg_sess_id  VARCHAR2(7) := '';
    cookie_value VARCHAR2(2000) := '';
    sessid       VARCHAR2(7);
    gtvsdax_row  gtvsdax%ROWTYPE;
    reg_seqno    NUMBER;

    CURSOR seqno_c(pidm_in SPRIDEN.SPRIDEN_PIDM%TYPE,
                   term_in STVTERM.STVTERM_CODE%TYPE) IS
      SELECT NVL(MAX(sfrstca_seq_number), 0)
        FROM sfrstca
       WHERE sfrstca_term_code = term_in
         AND sfrstca_pidm(+) = pidm_in;
  BEGIN
    dbms_output.put_line('pidm'||pidm_in);
    dbms_output.put_line('pidm'||call_path_in);
    IF NOT sfkfunc.f_reg_access_still_good(pidm_in, term_in, call_path_in) THEN
      null;
       dbms_output.put_line('returning  false');
      RETURN FALSE;
    END IF;


    OPEN seqno_c(pidm_in, term_in);
    FETCH seqno_c
      INTO reg_seqno;
    CLOSE seqno_c;
    sfkcurs.p_get_gtvsdax('MAXREGNO', 'WEBREG', gtvsdax_row);

    IF reg_seqno > NVL(gtvsdax_row.gtvsdax_external_code, 9999) THEN
      null;
      RETURN FALSE;
    END IF;

    RETURN TRUE;
  END f_reg_access_still_good;

  FUNCTION F_ValidTerm(term     IN VARCHAR2 DEFAULT NULL,
                       term_rec OUT stvterm%ROWTYPE,
                       rtrm_rec OUT sorrtrm%ROWTYPE) RETURN BOOLEAN IS
    row_count NUMBER;
    cursor c_sorrtrmc(term in STVTERM.STVTERM_CODE%TYPE, date_in in date) is
      SELECT a.*
        FROM sorrtrm a, sobterm
       WHERE a.sorrtrm_term_code = term
         AND a.sorrtrm_term_code = sobterm_term_code
         AND sobterm_dynamic_sched_term_ind = 'Y'
         AND ((TRUNC(date_in) BETWEEN trunc(a.sorrtrm_start_date) AND
             trunc(a.sorrtrm_end_date)) OR (date_in IS NULL));
  BEGIN
    IF term IS NULL THEN
      RETURN FALSE;
    END IF;

    row_count := 0;
    dbms_output.put_line('Entering into Main Term Checking.');
    FOR c1 IN STKTERM.STVTERMC(term) LOOP
      term_rec := c1;
      dbms_output.put_line('Valid STVTERM');
      /*
       SELECT *
         FROM STVTERM
         WHERE STVTERM_CODE = 201010;
      */

      FOR c2 IN c_sorrtrmc(term, SYSDATE) LOOP
        rtrm_rec := c2;
        dbms_output.put_line('Valid  SORRTRMC');
        /*  --SOKLIBS.SORRTRMC
          SELECT a.*
          FROM sorrtrm a, sobterm
         WHERE a.sorrtrm_term_code = term
           AND a.sorrtrm_term_code = sobterm_term_code
           AND sobterm_dynamic_sched_term_ind = 'Y'
           AND (
                     (
                        TRUNC (date_in) BETWEEN a.sorrtrm_start_date
                            AND a.sorrtrm_end_date
                     )
                  OR (date_in IS NULL)
               );
        */
        FOR sobterm_rec IN soklibs.sobtermc(term) LOOP
          row_count := c_sorrtrmc%rowcount;
          dbms_output.put_line('Valid SOBTERM');
          /*  --soklibs.sobtermc
              SELECT *
           FROM sobterm
          WHERE sobterm_term_code = term
            AND sobterm_reg_allowed = 'Y';
             */
        END LOOP;

      END LOOP;
    END LOOP;

    IF row_count = 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
  END F_ValidTerm;

  FUNCTION F_RegsStu(pidm_in   IN spriden.spriden_pidm%TYPE,
                     term      IN stvterm.stvterm_code%TYPE DEFAULT NULL,
                     proc_name IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN IS
    sql_err                   NUMBER;
    error                     varchar2(200);
    gen_proc_name             VARCHAR2(80);
    genpidm                   spriden.spriden_pidm%TYPE;
    restrict_time_ticket      VARCHAR2(1);
    restrict_time_ticket_code gtvsdax.gtvsdax_internal_code%TYPE := 'WEBRESTTKT';
    web_vr_ind                VARCHAR2(1) := 'W';
  BEGIN
    --
    -- Establish the student pidm.
    -- ==================================================
    /*  IF NVL(twbkwbis.f_getparam(pidm_in, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm := TO_NUMBER(twbkwbis.f_getparam(pidm_in, 'STUPIDM'),
                           '999999999');
    ELSE
      genpidm := pidm_in;
    END IF; */

    genpidm := pidm_in;

    --
    -- Check if person is deceased.`
    -- ==================================================
    IF me_bwckregs.f_deceasedperscheck(sql_err, genpidm) THEN
      RAISE bwcklibs.deceased_person_err;
    END IF;

    --
    -- Do either registration management control checking
    -- or time ticket checking, based on a gtvsdax flag.
    -- ==================================================
    --
    -- Get the student information record.
    -- ==================================================
    dbms_output.put_line('Checking student records for pidm..' || genpidm ||
                         'and term ' || term);

    FOR sgbstdn IN sgklibs.sgbstdnc(genpidm, term) LOOP
      sgbstdn_rec := sgbstdn;
    END LOOP;

    dbms_output.put_line(sgbstdn_rec.sgbstdn_pidm || ':' ||
                         sgbstdn_rec.sgbstdn_term_code_eff || ':' ||
                         sgbstdn_rec.sgbstdn_stst_code);

    --
    -- Check the student record.
    -- ==================================================
    --  begin

    me_bwckregs.p_regschk(sgbstdn_rec);

    /* exception
      when others then
        error := sqlerrm;
        dbms_output.put_line('Exception in Registration check,' || error);
        --return;

    end;*/

    dbms_output.put_line('He is a proper student.');
    row_count := 0;

    /* Disabling this code

      --
      -- Get the registration header record.
      -- ==================================================
      dbms_output.put_line('Checking enrollment records..');
      FOR sfbetrm IN sfkcurs.sfbetrmc(genpidm, term) LOOP
        sfbetrm_rec := sfbetrm;
        row_count   := sfkcurs.sfbetrmc%rowcount;
        dbms_output.put_line('PIDM : ' || genpidm);
        dbms_output.put_line('Enrolled term in table : ' ||
                             sfbetrm_rec.sfbetrm_term_code);
      END LOOP;
      --
      -- Check the registration header record if it exists.
      -- If not it will be created elsewhere when required.
      -- ==================================================
      IF row_count <> 0 THEN
        me_bwckregs.p_regschk(sfbetrm_rec);
        dbms_output.put_line('He is Enrolled Properly.');
      END IF;
      --
      --
      -- ==================================================
      sfkvars.regs_allowed := bwcksams.f_validregdate(term);

    */

    RETURN TRUE;

  EXCEPTION
    WHEN OTHERS THEN
      p_student_id := baninst1.gb_common.f_get_id(pidm_in);
      dbms_output.put_line('Exception Raised in f_regsstu : ' || sqlerrm);
      error := sqlerrm;

      return FALSE;
  END F_RegsStu;

  PROCEDURE p_get_min_max_by_curric_stand(p_term         IN sfrmhrs.sfrmhrs_term_code%TYPE,
                                          p_pidm         IN spriden.spriden_pidm%TYPE,
                                          p_seq_no       IN sfrmhrs.sfrmhrs_seq_no%TYPE DEFAULT NULL,
                                          p_new_cast     IN VARCHAR DEFAULT 'N',
                                          p_new_astd     IN VARCHAR DEFAULT 'N',
                                          p_astd_code    IN stvastd.stvastd_code%TYPE,
                                          p_cast_code    IN stvcast.stvcast_code%TYPE,
                                          p_min_hrs_out  OUT sfrmhrs.sfrmhrs_min_hrs%TYPE,
                                          p_max_hrs_out  OUT sfrmhrs.sfrmhrs_max_hrs%TYPE,
                                          p_min_srce_out OUT sfbetrm.sfbetrm_minh_srce_cde%TYPE,
                                          p_max_srce_out OUT sfbetrm.sfbetrm_maxh_srce_cde%TYPE) IS
    lv_min_hours       sfrmhrs.sfrmhrs_min_hrs%TYPE := 999999.999;
    lv_max_hours       sfrmhrs.sfrmhrs_max_hrs%TYPE := 0.0;
    lv_over_cmin_hours sfrmhrs.sfrmhrs_min_hrs%TYPE := NULL;
    lv_over_cmax_hours sfrmhrs.sfrmhrs_max_hrs%TYPE := NULL;
    lv_over_amin_hours sfrmhrs.sfrmhrs_min_hrs%TYPE := NULL;
    lv_over_amax_hours sfrmhrs.sfrmhrs_max_hrs%TYPE := NULL;
    lv_hold_min_hours  sfrmhrs.sfrmhrs_min_hrs%TYPE := NULL;
    lv_hold_max_hours  sfrmhrs.sfrmhrs_max_hrs%TYPE := NULL;
    lv_min_srce        sfbetrm.sfbetrm_minh_srce_cde%TYPE := 'M';
    lv_max_srce        sfbetrm.sfbetrm_maxh_srce_cde%TYPE := 'M';

    CURSOR shrttrm_cast_cursor IS
      SELECT stvcast_min_reg_hours, stvcast_max_reg_hours
        FROM stvcast, shrttrm a
       WHERE a.shrttrm_pidm = p_pidm
         AND stvcast_code = a.shrttrm_cast_code
         AND stvcast_max_reg_hours IS NOT NULL
         AND a.shrttrm_term_code =
             (SELECT MAX(b.shrttrm_term_code)
                FROM shrttrm b
               WHERE b.shrttrm_pidm = p_pidm
                 AND b.shrttrm_term_code < p_term);

    CURSOR shrttrm_astd_cursor IS
      SELECT stvastd_min_reg_hours, stvastd_max_reg_hours
        FROM stvastd, shrttrm a
       WHERE a.shrttrm_pidm = p_pidm
         AND stvastd_code = a.shrttrm_astd_code_end_of_term
         AND stvastd_max_reg_hours IS NOT NULL
         AND a.shrttrm_term_code =
             (SELECT MAX(b.shrttrm_term_code)
                FROM shrttrm b
               WHERE b.shrttrm_pidm = p_pidm
                 AND b.shrttrm_term_code < p_term);

  BEGIN
    dbms_output.put_line('Calling p_get_min_max_by_curric....');
    p_get_min_max_by_curric(p_term        => p_term,
                            p_pidm        => p_pidm,
                            p_seq_no      => NULL,
                            p_min_hrs_out => lv_hold_min_hours,
                            p_max_hrs_out => lv_hold_max_hours);
    dbms_output.put_line('p_get_min_max_by_curric completed.');
    dbms_output.put_line('shrttrm cursor started..');
    OPEN shrttrm_cast_cursor;
    FETCH shrttrm_cast_cursor
      INTO lv_over_cmin_hours, lv_over_cmax_hours; -- CAST override values
    CLOSE shrttrm_cast_cursor;
    dbms_output.put_line('shrttrm cursor passed.');
    OPEN shrttrm_astd_cursor;
    FETCH shrttrm_astd_cursor
      INTO lv_over_amin_hours, lv_over_amax_hours; -- ASTD override values
    CLOSE shrttrm_astd_cursor;

    IF p_cast_code IS NOT NULL THEN
      FOR stvcast IN stkcast.stvcastc(p_cast_code) LOOP
        lv_min_hours := stvcast.stvcast_min_reg_hours;
        lv_max_hours := stvcast.stvcast_max_reg_hours;
        lv_min_srce  := 'C';
        lv_max_srce  := 'C';
      END LOOP;

      IF p_new_cast = 'Y' THEN
        IF lv_min_hours is NULL AND lv_over_cmin_hours IS NOT NULL THEN
          lv_min_hours := lv_over_cmin_hours;
          lv_min_srce  := 'C';
        END IF;
        IF lv_max_hours is NULL AND lv_over_cmax_hours IS NOT NULL THEN
          lv_max_hours := lv_over_cmax_hours;
          lv_max_srce  := 'C';
        END IF;
      END IF;

      IF lv_min_hours is NULL AND lv_max_hours IS NOT NULL THEN
        lv_min_hours := 0.0;
        lv_min_srce  := 'C';
      ELSIF lv_min_hours is NOT NULL AND lv_max_hours IS NULL THEN
        lv_max_hours := 999999.999;
        lv_max_srce  := 'C';
      END IF;

      IF lv_min_hours is NULL AND lv_over_cmin_hours IS NULL THEN
        lv_min_hours := 999999.999;
        lv_min_srce  := '';
      END IF;
      IF lv_max_hours is NULL AND lv_over_cmax_hours IS NULL THEN
        lv_max_hours := 0.0;
        lv_max_srce  := '';
      END IF;

    END IF;

    IF p_astd_code IS NOT NULL AND lv_max_hours = 0.0 THEN
      FOR stvastd IN stkastd.stvastdc(p_astd_code) LOOP
        lv_max_hours := stvastd.stvastd_max_reg_hours;
        lv_max_srce  := 'A';
      END LOOP;

      IF p_new_astd = 'Y' THEN
        IF lv_max_hours is NULL AND lv_over_amax_hours IS NOT NULL THEN
          lv_max_hours := lv_over_amax_hours;
          lv_max_srce  := 'A';
        END IF;
      END IF;
    END IF;

    IF p_astd_code IS NOT NULL AND lv_min_hours = 999999.999 THEN
      FOR stvastd IN stkastd.stvastdc(p_astd_code) LOOP
        lv_min_hours := stvastd.stvastd_min_reg_hours;
        lv_min_srce  := 'A';
      END LOOP;

      IF p_new_astd = 'Y' THEN
        IF lv_min_hours is NULL AND lv_over_amin_hours IS NOT NULL THEN
          lv_min_hours := lv_over_amin_hours;
          lv_min_srce  := 'A';
        END IF;
      END IF;
    END IF;

    IF p_astd_code IS NOT NULL THEN
      IF lv_min_hours is NULL AND lv_max_hours IS NOT NULL THEN
        lv_min_hours := 0.0;
        lv_min_srce  := 'A';
      ELSIF lv_min_hours is NOT NULL AND lv_max_hours IS NULL THEN
        lv_max_hours := 999999.999;
        lv_max_srce  := 'A';
      END IF;

      IF lv_min_hours is NULL AND lv_over_amin_hours IS NULL THEN
        lv_min_hours := 999999.999;
        lv_min_srce  := '';
      END IF;
      IF lv_max_hours is NULL AND lv_over_amax_hours IS NULL THEN
        lv_max_hours := 0.0;
        lv_max_srce  := '';
      END IF;

    END IF;

    IF lv_min_hours = 999999.999 THEN
      lv_min_hours := lv_hold_min_hours;
      lv_min_srce  := 'M';
    END IF;
    IF lv_max_hours = 0.0 THEN
      lv_max_hours := lv_hold_max_hours;
      lv_max_srce  := 'M';
    END IF;

    IF lv_min_hours > lv_max_hours THEN
      lv_min_hours := lv_max_hours;
    END IF;
    IF lv_min_hours = lv_max_hours AND lv_max_hours = 0.0 THEN
      lv_max_hours := 999999.999;
    END IF;

    IF lv_min_hours IS NULL THEN
      lv_min_hours := 0.0;
    END IF;
    IF lv_max_hours IS NULL THEN
      lv_max_hours := 999999.999;
    END IF;

    p_min_hrs_out  := lv_min_hours;
    p_max_hrs_out  := lv_max_hours;
    p_min_srce_out := lv_min_srce;
    p_max_srce_out := lv_max_srce;

  END p_get_min_max_by_curric_stand;

  PROCEDURE p_get_min_max_by_curric(p_term        IN sfrmhrs.sfrmhrs_term_code%TYPE,
                                    p_pidm        IN spriden.spriden_pidm%TYPE,
                                    p_seq_no      IN sfrmhrs.sfrmhrs_seq_no%TYPE DEFAULT NULL,
                                    p_min_hrs_out OUT sfrmhrs.sfrmhrs_min_hrs%TYPE,
                                    p_max_hrs_out OUT sfrmhrs.sfrmhrs_max_hrs%TYPE)

   IS

    i NUMBER;
    TYPE mhrs_tabtype IS TABLE OF sfrmhrs%ROWTYPE INDEX BY binary_integer;
    lv_mhrs_table mhrs_tabtype;
    lv_min_hours  sfrmhrs.sfrmhrs_min_hrs%TYPE := 0.0;
    lv_max_hours  sfrmhrs.sfrmhrs_max_hrs%TYPE := 999999.999;

    lv_learner_rec sb_learner.learner_rec;
    lv_learner_ref sb_learner.learner_ref;
    lv_lcur_table  sokccur.curriculum_savedtab;
    lv_clas_code   sfrmhrs.sfrmhrs_clas_code%TYPE;
    lv_clas_desc   stvclas.stvclas_desc%TYPE;

    lv_bid        spriden.spriden_id%type;
    lv_vtype_code stvvtyp.stvvtyp_code%type;

    lv_sobterm_row     sobterm%ROWTYPE;
    lv_include_attempt sobterm.sobterm_incl_attmpt_hrs_ind%TYPE := NULL;

    CURSOR sobterm_c IS
      SELECT * FROM sobterm WHERE sobterm_term_code = p_term;

    CURSOR sgrsatt_c IS
      SELECT sgrsatt_atts_code
        FROM sgrsatt
       WHERE sgrsatt_pidm = p_pidm
         AND sgrsatt_term_code_eff =
             (SELECT MAX(y.sgrsatt_term_code_eff)
                FROM sgrsatt y
               WHERE y.sgrsatt_pidm = p_pidm
                 AND y.sgrsatt_term_code_eff <= p_term);

    CURSOR all_pidms_sfrmhrs_c(pidm_in spriden.spriden_pidm%TYPE,
                               term_in sfrmhrs.sfrmhrs_term_code%TYPE,
                               styp_in sfrmhrs.sfrmhrs_styp_code%TYPE,
                               clas_in sfrmhrs.sfrmhrs_clas_code%TYPE,
                               resd_in sfrmhrs.sfrmhrs_resd_code%TYPE) IS
      SELECT *
        FROM sfrmhrs
       WHERE sfrmhrs_term_code = term_in
         AND sfrmhrs_term_code >=
             (SELECT MAX(y.SGBSTDN_TERM_CODE_EFF)
                FROM SGBSTDN y
               WHERE y.SGBSTDN_PIDM = pidm_in
                 AND y.SGBSTDN_STYP_CODE = styp_in
                 AND y.SGBSTDN_RESD_CODE = resd_in
                 AND y.SGBSTDN_TERM_CODE_EFF <= term_in)
         AND (sfrmhrs_styp_code = styp_in OR sfrmhrs_styp_code IS NULL)
         AND (sfrmhrs_resd_code = resd_in OR sfrmhrs_resd_code IS NULL)
         AND (sfrmhrs_clas_code = clas_in OR sfrmhrs_clas_code IS NULL)
         AND (sokccur.f_match_curriculum(pidm_in,
                                         sb_curriculum_str.f_learner,
                                         term_in,
                                         NULL,
                                         sfrmhrs_levl_code,
                                         sfrmhrs_coll_code,
                                         sfrmhrs_degc_code,
                                         sfrmhrs_camp_code,
                                         sfrmhrs_program,
                                         sfrmhrs_prim_sec_cde,
                                         sfrmhrs_lfst_code,
                                         sfrmhrs_majr_code,
                                         sfrmhrs_dept_code,
                                         sfrmhrs_admt_code,
                                         NULL,
                                         NULL) = 'Y');

  BEGIN
    dbms_output.put_line('Calling sokccur.f_current_active_curriculum...');
    lv_lcur_table := sokccur.f_current_active_curriculum(p_pidm      => p_pidm,
                                                         p_lmod_code => sb_curriculum_str.f_learner,
                                                         p_term_code => p_term,
                                                         p_eff_term  => p_term);

    dbms_output.put_line('sokccur.f_current_active_curriculum completed.');
    OPEN sobterm_c;
    FETCH sobterm_c
      INTO lv_sobterm_row;
    CLOSE sobterm_c;
    IF lv_sobterm_row.sobterm_incl_attmpt_hrs_ind = 'Y' THEN
      lv_include_attempt := 'A';
    ELSE
      lv_include_attempt := NULL;
    END IF;
    dbms_output.put_line('Calling soklibs.p_class_calc...');
    dbms_output.put_line('Sample');
    dbms_output.put_line('lv_lcur_table(1).r_levl_code:' || lv_lcur_table(1)
                         .r_levl_code);
    soklibs.p_class_calc(pidm            => p_pidm,
                         levl_code       => lv_lcur_table(1).r_levl_code,
                         term_code       => p_term,
                         in_progress_ind => lv_include_attempt,
                         clas_code       => lv_clas_code,
                         clas_desc       => lv_clas_desc);
    dbms_output.put_line('soklibs.p_class_calc completed.');
    lv_learner_ref := f_query_last_term(p_pidm          => p_pidm,
                                        p_term_code_eff => p_term);
    FETCH lv_learner_ref
      INTO lv_learner_rec;

    OPEN all_pidms_sfrmhrs_c(pidm_in => p_pidm,
                             term_in => p_term,
                             styp_in => lv_learner_rec.r_styp_code,
                             clas_in => lv_clas_code,
                             resd_in => lv_learner_rec.r_resd_code);
    FETCH all_pidms_sfrmhrs_c BULK COLLECT
      INTO lv_mhrs_table; -- Term mhrs records
    CLOSE all_pidms_sfrmhrs_c;
    CLOSE lv_learner_ref;

    lv_bid        := gb_common.f_get_id(p_pidm);
    lv_vtype_code := gokvisa.f_check_visa(lv_bid, sysdate);
    dbms_output.put_line('lv_mhrs_table.COUNT : ' || lv_mhrs_table.COUNT);
    FOR i IN 1 .. lv_mhrs_table.COUNT LOOP
      -- Loop through the term mhrs records that so far
      -- match the student curricula
      /*      if lv_mhrs_table(i) then
       dbms_output.put_line('lv_mhrs_table(i) : TRUE' );
      else
       dbms_output.put_line('lv_mhrs_table(i) : FALSE' );
      end if;*/
      dbms_output.put_line('lv_mhrs_table(i).sfrmhrs_chrt_code : ' || lv_mhrs_table(i)
                           .sfrmhrs_chrt_code);
      IF ((lv_mhrs_table(i)
         .sfrmhrs_chrt_code IS NOT NULL AND
          (sgksels.f_query_sgrchrt(p_pidm          => p_pidm,
                                    p_term_code_eff => p_term,
                                    p_chrt_code     => lv_mhrs_table(i)
                                                       .sfrmhrs_chrt_code,
                                    p_multiple_ind  => 'Y') = 'Y')) OR lv_mhrs_table(i)
         .sfrmhrs_chrt_code IS NULL) AND ((lv_mhrs_table(i)
         .sfrmhrs_atts_code IS NOT NULL AND
          (sgksels.f_query_sgrsatt(p_pidm          => p_pidm,
                                                                    p_term_code_eff => p_term,
                                                                    p_atts_code     => lv_mhrs_table(i)
                                                                                       .sfrmhrs_atts_code,
                                                                    p_multiple_ind  => 'Y') = 'Y')) OR lv_mhrs_table(i)
         .sfrmhrs_atts_code IS NULL) AND ((lv_mhrs_table(i)
         .sfrmhrs_actc_code IS NOT NULL AND
          (sgksels.f_query_sgrsprt(p_pidm         => p_pidm,
                                                                    p_term_code    => p_term,
                                                                    p_actc_code    => lv_mhrs_table(i)
                                                                                      .sfrmhrs_actc_code,
                                                                    p_multiple_ind => 'Y') = 'Y')) OR lv_mhrs_table(i)
         .sfrmhrs_actc_code IS NULL) AND
         ((lv_mhrs_table(i)
         .sfrmhrs_vtyp_code IS NOT NULL AND
          lv_vtype_code = lv_mhrs_table(i).sfrmhrs_vtyp_code) OR lv_mhrs_table(i)
         .sfrmhrs_vtyp_code is NULL)
        --        AND  ((lv_mhrs_table(i).sfrmhrs_vtyp_code IS NOT NULL
        --               AND (gb_visa.f_any_exists(p_pidm,NULL,
        --        lv_mhrs_table(i).sfrmhrs_vtyp_code, NULL)='Y'))
        --               OR lv_mhrs_table(i).sfrmhrs_vtyp_code IS NULL)
         AND
         (sokccur.f_match_curriculum(p_pidm             => p_pidm,
                                     p_lmod_code        => sb_curriculum_str.f_learner,
                                     p_term_code        => p_term,
                                     p_key_seqno        => 99,
                                     p_levl_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_levl_code,
                                     p_coll_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_coll_code,
                                     p_degc_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_degc_code,
                                     p_camp_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_camp_code,
                                     p_program          => lv_mhrs_table(i)
                                                           .sfrmhrs_program,
                                     p_prim_sec_cde     => lv_mhrs_table(i)
                                                           .sfrmhrs_prim_sec_cde,
                                     p_lfst_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_lfst_code,
                                     p_majr_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_majr_code,
                                     p_dept_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_dept_code,
                                     p_admt_code        => lv_mhrs_table(i)
                                                           .sfrmhrs_admt_code,
                                     p_term_code_admt   => NULL,
                                     p_term_code_matric => NULL) = 'Y') THEN
        IF lv_mhrs_table(i).sfrmhrs_min_hrs > lv_min_hours THEN
          lv_min_hours := lv_mhrs_table(i).sfrmhrs_min_hrs;
        ELSIF lv_mhrs_table(i).sfrmhrs_min_hrs IS NULL THEN
          lv_min_hours := 0.0;
        END IF;
        IF lv_mhrs_table(i).sfrmhrs_max_hrs < lv_max_hours THEN
          lv_max_hours := lv_mhrs_table(i).sfrmhrs_max_hrs;
        ELSIF lv_mhrs_table(i).sfrmhrs_max_hrs IS NULL THEN
          lv_min_hours := 999999.999;
        END IF;
      END IF;
    END LOOP; -- end mhrs loop
    IF lv_min_hours > lv_max_hours THEN
      lv_min_hours := lv_max_hours;
    END IF;
    IF lv_min_hours = lv_max_hours AND lv_max_hours = 0.0 THEN
      lv_max_hours := 999999.999;
    END IF;

    p_min_hrs_out := lv_min_hours;
    p_max_hrs_out := lv_max_hours;

  END p_get_min_max_by_curric;

  FUNCTION f_query_last_term(p_pidm          sgbstdn.sgbstdn_pidm%TYPE,
                             p_term_code_eff sgbstdn.sgbstdn_term_code_eff%TYPE)
    RETURN sb_learner.learner_ref IS

    learner_rc sb_learner.learner_ref;
  BEGIN
    OPEN learner_rc FOR
      SELECT SGBSTDN_PIDM,
             SGBSTDN_TERM_CODE_EFF,
             SGBSTDN_STST_CODE,
             SGBSTDN_LEVL_CODE,
             SGBSTDN_STYP_CODE,
             SGBSTDN_TERM_CODE_MATRIC,
             SGBSTDN_TERM_CODE_ADMIT,
             SGBSTDN_EXP_GRAD_DATE,
             SGBSTDN_CAMP_CODE,
             SGBSTDN_FULL_PART_IND,
             SGBSTDN_SESS_CODE,
             SGBSTDN_RESD_CODE,
             SGBSTDN_COLL_CODE_1,
             SGBSTDN_DEGC_CODE_1,
             SGBSTDN_MAJR_CODE_1,
             SGBSTDN_MAJR_CODE_MINR_1,
             SGBSTDN_MAJR_CODE_MINR_1_2,
             SGBSTDN_MAJR_CODE_CONC_1,
             SGBSTDN_MAJR_CODE_CONC_1_2,
             SGBSTDN_MAJR_CODE_CONC_1_3,
             SGBSTDN_COLL_CODE_2,
             SGBSTDN_DEGC_CODE_2,
             SGBSTDN_MAJR_CODE_2,
             SGBSTDN_MAJR_CODE_MINR_2,
             SGBSTDN_MAJR_CODE_MINR_2_2,
             SGBSTDN_MAJR_CODE_CONC_2,
             SGBSTDN_MAJR_CODE_CONC_2_2,
             SGBSTDN_MAJR_CODE_CONC_2_3,
             SGBSTDN_ORSN_CODE,
             SGBSTDN_PRAC_CODE,
             SGBSTDN_ADVR_PIDM,
             SGBSTDN_GRAD_CREDIT_APPR_IND,
             SGBSTDN_CAPL_CODE,
             SGBSTDN_LEAV_CODE,
             SGBSTDN_LEAV_FROM_DATE,
             SGBSTDN_LEAV_TO_DATE,
             SGBSTDN_ASTD_CODE,
             SGBSTDN_TERM_CODE_ASTD,
             SGBSTDN_RATE_CODE,
             SGBSTDN_MAJR_CODE_1_2,
             SGBSTDN_MAJR_CODE_2_2,
             SGBSTDN_EDLV_CODE,
             SGBSTDN_INCM_CODE,
             SGBSTDN_ADMT_CODE,
             SGBSTDN_EMEX_CODE,
             SGBSTDN_APRN_CODE,
             SGBSTDN_TRCN_CODE,
             SGBSTDN_GAIN_CODE,
             SGBSTDN_VOED_CODE,
             SGBSTDN_BLCK_CODE,
             SGBSTDN_TERM_CODE_GRAD,
             SGBSTDN_ACYR_CODE,
             SGBSTDN_DEPT_CODE,
             SGBSTDN_SITE_CODE,
             SGBSTDN_DEPT_CODE_2,
             SGBSTDN_EGOL_CODE,
             SGBSTDN_DEGC_CODE_DUAL,
             SGBSTDN_LEVL_CODE_DUAL,
             SGBSTDN_DEPT_CODE_DUAL,
             SGBSTDN_COLL_CODE_DUAL,
             SGBSTDN_MAJR_CODE_DUAL,
             SGBSTDN_BSKL_CODE,
             SGBSTDN_PRIM_ROLL_IND,
             SGBSTDN_PROGRAM_1,
             SGBSTDN_TERM_CODE_CTLG_1,
             SGBSTDN_DEPT_CODE_1_2,
             SGBSTDN_MAJR_CODE_CONC_121,
             SGBSTDN_MAJR_CODE_CONC_122,
             SGBSTDN_MAJR_CODE_CONC_123,
             SGBSTDN_SECD_ROLL_IND,
             SGBSTDN_TERM_CODE_ADMIT_2,
             SGBSTDN_ADMT_CODE_2,
             SGBSTDN_PROGRAM_2,
             SGBSTDN_TERM_CODE_CTLG_2,
             SGBSTDN_LEVL_CODE_2,
             SGBSTDN_CAMP_CODE_2,
             SGBSTDN_DEPT_CODE_2_2,
             SGBSTDN_MAJR_CODE_CONC_221,
             SGBSTDN_MAJR_CODE_CONC_222,
             SGBSTDN_MAJR_CODE_CONC_223,
             SGBSTDN_CURR_RULE_1,
             SGBSTDN_CMJR_RULE_1_1,
             SGBSTDN_CCON_RULE_11_1,
             SGBSTDN_CCON_RULE_11_2,
             SGBSTDN_CCON_RULE_11_3,
             SGBSTDN_CMJR_RULE_1_2,
             SGBSTDN_CCON_RULE_12_1,
             SGBSTDN_CCON_RULE_12_2,
             SGBSTDN_CCON_RULE_12_3,
             SGBSTDN_CMNR_RULE_1_1,
             SGBSTDN_CMNR_RULE_1_2,
             SGBSTDN_CURR_RULE_2,
             SGBSTDN_CMJR_RULE_2_1,
             SGBSTDN_CCON_RULE_21_1,
             SGBSTDN_CCON_RULE_21_2,
             SGBSTDN_CCON_RULE_21_3,
             SGBSTDN_CMJR_RULE_2_2,
             SGBSTDN_CCON_RULE_22_1,
             SGBSTDN_CCON_RULE_22_2,
             SGBSTDN_CCON_RULE_22_3,
             SGBSTDN_CMNR_RULE_2_1,
             SGBSTDN_CMNR_RULE_2_2,
             SGBSTDN_PREV_CODE,
             SGBSTDN_TERM_CODE_PREV,
             SGBSTDN_CAST_CODE,
             SGBSTDN_TERM_CODE_CAST,
             SGBSTDN_USER_ID,
             SGBSTDN_DATA_ORIGIN,
             SGBSTDN_SCPC_CODE,
             ROWID
        FROM SGBSTDN
       WHERE sgbstdn_pidm = p_pidm
         AND sgbstdn_term_code_eff =
             (select max(x.sgbstdn_term_code_eff)
                from sgbstdn x
               where x.sgbstdn_pidm = p_pidm
                 and x.sgbstdn_term_code_eff <= p_term_code_eff);
    RETURN learner_rc;
  END f_query_last_term;

  PROCEDURE p_addcrse(stcr_row      sftregs%ROWTYPE,
                      subj          ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse          ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq           ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                      start_date_in VARCHAR2 DEFAULT NULL,
                      end_date_in   VARCHAR2 DEFAULT NULL) IS
    sgbstdn_rec       sgbstdn%ROWTYPE;
    stcr_err_ind      sftregs.sftregs_error_flag%TYPE;
    appr_err          sftregs.sftregs_error_flag%TYPE;
    local_ssbsect_row ssbsect%ROWTYPE;

    CURSOR sobptrmc(term VARCHAR2, ptrm VARCHAR2) IS
      SELECT *
        FROM sobptrm
       WHERE sobptrm_term_code = term
         AND sobptrm_ptrm_code = ptrm;

    CURSOR stvssts_c(ssts_code_in ssbsect.ssbsect_ssts_code%type) IS
      SELECT 'STAT', sb_registration_msg.f_get_message('STAT', 1), 'F'
        FROM stvssts
       WHERE stvssts_code = ssts_code_in
         AND stvssts_reg_ind = 'N';

    local_sobptrm_row sobptrmc%ROWTYPE;
  BEGIN
    dbms_output.put_line('Entered into Add Course.');
    --      ssbsect_row.ssbsect_subj_code := subj;
    --      ssbsect_row.ssbsect_crse_numb := crse;
    --      ssbsect_row.ssbsect_seq_numb := seq;
    row_count   := 0;
    sgbstdn_rec := bwckcoms.sgbstdn_rec;

    me_bwckregs.p_init_final_update_vars(stcr_row.sftregs_pidm,
                                         stcr_row.sftregs_term_code);
    dbms_output.put_line('sftregs_rec.sftregs_rsts_code :' ||
                         sftregs_rec.sftregs_rsts_code);
    IF stcr_row.sftregs_crn IS NOT NULL THEN
      dbms_output.put_line(stcr_row.sftregs_pidm);
      dbms_output.put_line(stcr_row.sftregs_term_code);
      FOR stcr_rec IN sfkcurs.sftregsc(stcr_row.sftregs_pidm,
                                       stcr_row.sftregs_term_code) LOOP
        IF stcr_rec.sftregs_crn = stcr_row.sftregs_crn THEN
          row_count := sfkcurs.sftregsc%rowcount;
          EXIT;
        END IF;
      END LOOP;
      dbms_output.put_line('sftregs_rec.sftregs_rsts_code :' ||
                           sftregs_rec.sftregs_rsts_code);

      IF row_count <> 0 THEN
        null;
      END IF;
    END IF;
    old_stvrsts_row.stvrsts_incl_sect_enrl := 'N';
    old_stvrsts_row.stvrsts_wait_ind       := 'N';
    old_sftregs_row.sftregs_credit_hr      := 00.00;
    dbms_output.put_line(subj);
    dbms_output.put_line(crse);
    dbms_output.put_line(seq);
    dbms_output.put_line(stcr_row.sftregs_crn);
    dbms_output.put_line(stcr_row.sftregs_start_date);
    /*
    sftregs_start_date

    */
    dbms_output.put_line(stcr_row.sftregs_completion_date);
    me_bwckregs.p_regschk(stcr_row, subj, crse, seq, NULL, 'Y');
    dbms_output.put_line('bwckregs.p_regschk in add course passed.');
    --  sftregs_row.sftregs_vr_status_type := sfkfunc.f_get_rsts_type(sftregs_row.sftregs_rsts_code);

    --
    -- determine the sftregs start and end dates,
    -- if OLR, they will have been entered by the user and passed in,
    -- if trad course, they will be found in ssbsect, or sobptrm.
    -----------------------------------------------------------------
    dbms_output.put_line(stcr_row.sftregs_crn);
    dbms_output.put_line(stcr_row.sftregs_term_code);
    dbms_output.put_line('ssbsectc cursor started.');
    OPEN ssklibs.ssbsectc(stcr_row.sftregs_crn, stcr_row.sftregs_term_code);
    FETCH ssklibs.ssbsectc
      INTO local_ssbsect_row;
    CLOSE ssklibs.ssbsectc;
    dbms_output.put_line('ssbsectc cursor passed.');
    sftregs_row.sftregs_start_date      := NVL(TO_DATE(start_date_in,
                                                       twbklibs.date_input_fmt),
                                               local_ssbsect_row.ssbsect_ptrm_start_date);
    sftregs_row.sftregs_completion_date := NVL(TO_DATE(end_date_in,
                                                       twbklibs.date_input_fmt),
                                               local_ssbsect_row.ssbsect_ptrm_end_date);

    dbms_output.put_line('sobptrmc cursor started.');
    IF sftregs_row.sftregs_start_date IS NULL THEN
      OPEN sobptrmc(stcr_row.sftregs_term_code, stcr_row.sftregs_ptrm_code);
      FETCH sobptrmc
        INTO local_sobptrm_row;
      CLOSE sobptrmc;
      sftregs_row.sftregs_start_date      := local_sobptrm_row.sobptrm_start_date;
      sftregs_row.sftregs_completion_date := local_sobptrm_row.sobptrm_end_date;
    END IF;
    dbms_output.put_line('sobptrmc cursor passed.');
    -----------------------------------------------------------------
    sftregs_row.sftregs_number_of_units := local_ssbsect_row.ssbsect_number_of_units;
    sftregs_row.sftregs_dunt_code       := local_ssbsect_row.ssbsect_dunt_code;
    dbms_output.put_line('stvsstsc cursor started.');
    dbms_output.put_line('sftregs_row.sftregs_error_flag : ' ||
                         sftregs_row.sftregs_error_flag);
    OPEN stvssts_c(ssbsect_row.ssbsect_ssts_code);
    FETCH stvssts_c
      INTO sftregs_row.sftregs_rmsg_cde,
           sftregs_row.sftregs_message,
           sftregs_row.sftregs_error_flag;
    CLOSE stvssts_c;
    dbms_output.put_line('stvsstsc cursor passed.');
    dbms_output.put_line('p_pre_edit Status : ');
    DBMS_OUTPUT.PUT_LINE('sftregs_row.sftregs_error_flag' ||
                         sftregs_row.sftregs_error_flag);
    IF (sftregs_row.sftregs_error_flag = 'F' AND
       sftregs_row.sftregs_rmsg_cde = 'STAT') THEN
      stcr_err_ind := 'Y';
    ELSE
      -- =======================================================
      -- This procedure performs edits based on a single course
      -- Checks approval code restrictions.
      -- Checks level restrictions.
      -- Checks college restrictions.
      -- Checks degree restrictions.
      -- Checks program restrictions.
      -- Checks major restrictions.
      -- Checks campus restrictions.
      -- Checks class restrictions.
      -- Checks repeat restrictions
      -- Checks capacity.
      -- =======================================================
      bwcklibs.p_getsobterm(gv_term_in, sobterm_row);

      DBMS_OUTPUT.PUT_LINE('Level Restricion Severity Value : ' ||
                           sobterm_row.sobterm_levl_severity);
      DBMS_OUTPUT.PUT_LINE('Degree Restricion Severity Value : ' ||
                           sobterm_row.sobterm_degree_severity);

      sfkedit.p_pre_edit(sftregs_row,
                         stcr_err_ind,
                         appr_err,
                         old_stvrsts_row.stvrsts_incl_sect_enrl,
                         old_stvrsts_row.stvrsts_wait_ind,
                         stvrsts_row.stvrsts_incl_sect_enrl,
                         stvrsts_row.stvrsts_wait_ind,
                         sobterm_row.sobterm_appr_severity,
                         sobterm_row.sobterm_levl_severity,
                         sobterm_row.sobterm_coll_severity,
                         sobterm_row.sobterm_degree_severity,
                         sobterm_row.sobterm_program_severity,
                         sobterm_row.sobterm_majr_severity,
                         sobterm_row.sobterm_camp_severity,
                         sobterm_row.sobterm_clas_severity,
                         sobterm_row.sobterm_capc_severity,
                         sobterm_row.sobterm_rept_severity,
                         sobterm_row.sobterm_rpth_severity,
                         sobterm_row.sobterm_dept_severity,
                         sobterm_row.sobterm_atts_severity,
                         sobterm_row.sobterm_chrt_severity,
                         sgrclsr_clas_code,
                         scbcrse_row.scbcrse_max_rpt_units,
                         scbcrse_row.scbcrse_repeat_limit,
                         ssbsect_row.ssbsect_sapr_code,
                         ssbsect_row.ssbsect_reserved_ind,
                         ssbsect_row.ssbsect_seats_avail,
                         ssbsect_row.ssbsect_wait_count,
                         ssbsect_row.ssbsect_wait_capacity,
                         ssbsect_row.ssbsect_wait_avail,
                         'WA');
      dbms_output.put_line('p_pre_edit passed.');
      DBMS_OUTPUT.PUT_LINE('sftregs_row.sftregs_error_flag' ||
                           sftregs_row.sftregs_error_flag);
    END IF;

    IF ssbsect_row.ssbsect_tuiw_ind = 'Y' THEN
      sftregs_row.sftregs_waiv_hr := '0';
    ELSE
      sftregs_row.sftregs_waiv_hr := sftregs_row.sftregs_bill_hr;
    END IF;

    sftregs_row.sftregs_activity_date := SYSDATE;

    IF stcr_err_ind = 'Y' THEN
      sftregs_row.sftregs_rsts_code      := SUBSTR(f_stu_getwebregsrsts('D'),
                                                   1,
                                                   2);
      sftregs_row.sftregs_vr_status_type := sfkfunc.f_get_rsts_type(sftregs_row.sftregs_rsts_code);
      sftregs_row.sftregs_remove_ind     := 'Y';
      sftregs_row.sftregs_rec_stat       := 'N';
    END IF;
    sftregs_row.sftregs_camp_code := camp;
    bwcklibs.p_add_sftregs(sftregs_row);

  END p_addcrse;

  PROCEDURE p_regschk(stcr_row sftregs%ROWTYPE,
                      subj     ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse     ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq      ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                      over     CHAR DEFAULT NULL,
                      add_ind  CHAR DEFAULT NULL) IS
    stvrsts_row stkrsts.stvrstsc%ROWTYPE;
  BEGIN
    dbms_output.put_line('Entered into p_regschk procedure ');
    sftregs_row := stcr_row;
    dbms_output.put_line('sftregs_row.sftregs_rsts_code :' ||
                         sftregs_row.sftregs_rsts_code);
    regs_date := bwcklibs.f_getregdate;

    ssbsect_row.ssbsect_subj_code := subj;
    ssbsect_row.ssbsect_crse_numb := crse;
    ssbsect_row.ssbsect_seq_numb  := seq;

    sftregs_row.sftregs_sect_subj_code := ssbsect_row.ssbsect_subj_code;
    sftregs_row.sftregs_sect_crse_numb := ssbsect_row.ssbsect_crse_numb;
    sftregs_row.sftregs_sect_seq_numb  := ssbsect_row.ssbsect_seq_numb;
    sftregs_row.sftregs_sect_schd_code := ssbsect_row.ssbsect_schd_code;

    IF over IS NOT NULL THEN
      override := over;
    END IF;

    IF sftregs_row.sftregs_preq_over IS NULL THEN
      sftregs_row.sftregs_preq_over := 'N';
    END IF;

    IF sftregs_row.sftregs_rept_over IS NULL THEN
      sftregs_row.sftregs_rept_over := 'N';
    END IF;

    IF sftregs_row.sftregs_coll_over IS NULL THEN
      sftregs_row.sftregs_coll_over := 'N';
    END IF;

    IF sftregs_row.sftregs_degc_over IS NULL THEN
      sftregs_row.sftregs_degc_over := 'N';
    END IF;

    IF sftregs_row.sftregs_prog_over IS NULL THEN
      sftregs_row.sftregs_prog_over := 'N';
    END IF;

    IF sftregs_row.sftregs_clas_over IS NULL THEN
      sftregs_row.sftregs_clas_over := 'N';
    END IF;

    IF sftregs_row.sftregs_camp_over IS NULL THEN
      sftregs_row.sftregs_camp_over := 'N';
    END IF;

    IF sftregs_row.sftregs_dept_over IS NULL THEN
      sftregs_row.sftregs_dept_over := 'N';
    END IF;

    IF sftregs_row.sftregs_chrt_over IS NULL THEN
      sftregs_row.sftregs_chrt_over := 'N';
    END IF;
    IF sftregs_row.sftregs_mexc_over IS NULL THEN
      sftregs_row.sftregs_mexc_over := 'N';
    END IF;
    IF sftregs_row.sftregs_atts_over IS NULL THEN
      sftregs_row.sftregs_atts_over := 'N';
    END IF;

    IF sftregs_row.sftregs_link_over IS NULL THEN
      sftregs_row.sftregs_link_over := 'N';
    END IF;

    IF sftregs_row.sftregs_corq_over IS NULL THEN
      sftregs_row.sftregs_corq_over := 'N';
    END IF;

    IF sftregs_row.sftregs_appr_over IS NULL THEN
      sftregs_row.sftregs_appr_over := 'N';
    END IF;

    IF sftregs_row.sftregs_majr_over IS NULL THEN
      sftregs_row.sftregs_majr_over := 'N';
    END IF;

    IF sftregs_row.sftregs_dupl_over IS NULL THEN
      sftregs_row.sftregs_dupl_over := 'N';
    END IF;

    IF sftregs_row.sftregs_levl_over IS NULL THEN
      sftregs_row.sftregs_levl_over := 'N';
    END IF;

    IF sftregs_row.sftregs_capc_over IS NULL THEN
      sftregs_row.sftregs_capc_over := 'N';
    END IF;

    IF sftregs_row.sftregs_time_over IS NULL THEN
      sftregs_row.sftregs_time_over := 'N';
    END IF;

    IF sftregs_row.sftregs_rpth_over IS NULL THEN
      sftregs_row.sftregs_rpth_over := 'N';
    END IF;

    IF sftregs_row.sftregs_add_date IS NULL THEN
      sftregs_row.sftregs_add_date := SYSDATE;
    END IF;

    sftregs_row.sftregs_user          := USER;
    sftregs_row.sftregs_activity_date := SYSDATE;
    sql_error                         := 0;
    dbms_output.put_line('validcrn in p_regschk procedure.');
    IF NOT me_bwckregs.f_validcrn(sql_error, sftregs_row.sftregs_term_code) THEN
      dbms_output.put_line('bwckregs.f_validcrn returned false.');
    else
      dbms_output.put_line('bwckregs.f_validcrn returned true.');
    END IF;
    bwckregs.p_defstcr(add_ind);
    dbms_output.put_line('Validations Started.');
    dbms_output.put_line('Level Validation Started.');
    IF NOT me_bwckregs.f_validlevl(sql_error, sftregs_row.sftregs_term_code) THEN
      dbms_output.put_line('Level is not proper for the registration');
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    dbms_output.put_line('Level Validation Completed.');
    dbms_output.put_line('Registration Status Validation Started.');
    IF NOT me_bwckregs.f_validrsts(sql_error) THEN
      dbms_output.put_line('Registration Status is not proper for the registration');
      dbms_output.put_line('RSTS sftregs_row.sftregs_rsts_code :' ||
                           sftregs_row.sftregs_rsts_code);
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    IF NOT me_bwckregs.f_validgmod(sql_error, sftregs_row.sftregs_term_code) THEN
      dbms_output.put_line('sftregs_row.sftregs_term_code :' ||
                           sftregs_row.sftregs_term_code);
      dbms_output.put_line('Grade Mode is not proper for the registration.');
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    dbms_output.put_line('Grade Mode Validation Completed.');
    dbms_output.put_line('Credit Hours Validation Started.');
    IF NOT me_bwckregs.f_validcredhr(sql_error) THEN
      dbms_output.put_line('Credit hours are not proper for the registration.');
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    dbms_output.put_line('Credit Hours Validation Completed.');
    dbms_output.put_line('Bill Hours Validation Started.');
    IF NOT me_bwckregs.f_validbillhr(sql_error) THEN
      dbms_output.put_line('Bill Hours are not proper for the registration.');
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    dbms_output.put_line('Bill Hours Validation Completed.');
    dbms_output.put_line('Appr Validation Started.');
    IF NOT me_bwckregs.f_validappr(sql_error) THEN
      dbms_output.put_line('Approval Code is not proper for the registration.');
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    dbms_output.put_line('Appr Validation Completed.');
    FOR stvrsts IN stkrsts.stvrstsc(sftregs_row.sftregs_rsts_code) LOOP
      stvrsts_row := stvrsts;
    END LOOP;

    IF stvrsts_row.stvrsts_incl_sect_enrl = 'Y' OR
       (stvrsts_row.stvrsts_wait_ind = 'Y' AND NVL(add_ind, 'N') = 'Y') THEN
      /*me_bwckregs.p_getoverride;*/
      MOBILE_APPS.me_BWCKREGS.p_getoverride;
    END IF;

    dbms_output.put_line('p_regschk procedure Completed.');
  END p_regschk;

  PROCEDURE p_regschk(stdn_row      sgbstdn%ROWTYPE,
                      multi_term_in BOOLEAN DEFAULT FALSE) IS
    astd_code     stvastd.stvastd_code%TYPE;
    cast_code     stvcast.stvcast_code%TYPE;
    sobterm_rec   sobterm%ROWTYPE;
    atmp_hrs      VARCHAR2(1);
    genpidm       spriden.spriden_pidm%TYPE;
    lv_levl_code  sorlcur.sorlcur_levl_code%TYPE;
    lv_admit_term sorlcur.sorlcur_term_code_admit%TYPE;

  BEGIN

    /*************************************************************/
    /* Determine the Class of the Student - for Fees calculation */
    /*************************************************************/

    OPEN soklibs.sobterm_webc(term);
    FETCH soklibs.sobterm_webc
      INTO sobterm_rec;

    IF soklibs.sobterm_webc%NOTFOUND THEN
      atmp_hrs := NULL;
    ELSE
      IF sobterm_rec.sobterm_incl_attmpt_hrs_ind = 'Y' THEN
        atmp_hrs := 'A';
      ELSE
        atmp_hrs := NULL;
      END IF;
    END IF;

    CLOSE soklibs.sobterm_webc;
    sgrclsr_clas_code := NULL;
    clas_desc         := NULL;

    dbms_output.put_line('STST code:' || stdn_row.sgbstdn_stst_code);

    dbms_output.put_line('sokccur.f_curriculum_value started...');

    lv_levl_code := sokccur.f_curriculum_value(p_pidm      => genpidm,
                                               p_lmod_code => sb_curriculum_str.f_learner,
                                               p_term_code => term,
                                               p_key_seqno => 99,
                                               p_eff_term  => term,
                                               p_order     => 1,
                                               p_field     => 'LEVL');

    dbms_output.put_line('soklibs.p_class_calc completed...levl code:' ||
                         lv_levl_code);

    dbms_output.put_line('soklibs.p_class_calc started...');

    soklibs.p_class_calc(genpidm,
                         lv_levl_code,
                         term,
                         atmp_hrs,
                         sgrclsr_clas_code,
                         clas_desc);

    dbms_output.put_line('soklibs.p_class_calc completed... clas_code and desc values' ||
                         sgrclsr_clas_code || clas_desc);

    /*************************************************************/
    /* End of Class Determination                                */
    /*************************************************************/
    dbms_output.put_line('bwckregs.f_stuhld started...');

    IF me_bwckregs.f_stuhld(sql_error, global_pidm) AND NOT multi_term_in THEN
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;
    dbms_output.put_line('bwckregs.f_stuhld Completed...');

    row_count := 0;

    dbms_output.put_line('STST code:' || stdn_row.sgbstdn_stst_code);

    FOR stvstst IN stkstst.stvststc(stdn_row.sgbstdn_stst_code) LOOP
      stst_reg_ind := stvstst.stvstst_reg_ind;
      row_count    := stkstst.stvststc%rowcount;
    END LOOP;

    IF row_count <> 1 AND NOT multi_term_in THEN
      raise_application_error(-20526, bwcklibs.error_msg_table(-20526));
    END IF;

    FOR shrttrm IN shklibs.shrttrmc(genpidm, term) LOOP
      astd_code := shrttrm.shrttrm_astd_code_end_of_term;
      cast_code := shrttrm.shrttrm_cast_code;
    END LOOP;

    dbms_output.put_line('ASTD code:' || astd_code || ', cast_code:' ||
                         cast_code);

    IF stdn_row.sgbstdn_term_code_astd = term THEN
      astd_code := stdn_row.sgbstdn_astd_code;
    END IF;

    dbms_output.put_line('ASTD code:' || astd_code);

    IF stdn_row.sgbstdn_term_code_cast = term THEN
      cast_code := stdn_row.sgbstdn_cast_code;
    END IF;

    dbms_output.put_line('Cast_code:' || cast_code);

    IF cast_code IS NOT NULL THEN
      row_count := 0;

      FOR stvcast IN stkcast.stvcastc(cast_code) LOOP
        cast_prevent_reg_ind := stvcast.stvcast_prevent_reg_ind;
        row_count            := stkcast.stvcastc%rowcount;
      END LOOP;

      dbms_output.put_line('cast_prevent_reg_ind:' || cast_prevent_reg_ind);

      IF row_count <> 1 AND NOT multi_term_in THEN
        raise_application_error(-20603, bwcklibs.error_msg_table(-20603));
      END IF;
    ELSIF astd_code IS NOT NULL THEN
      row_count := 0;

      FOR stvastd IN stkastd.stvastdc(astd_code) LOOP
        astd_prevent_reg_ind := stvastd.stvastd_prevent_reg_ind;
        row_count            := stkastd.stvastdc%rowcount;
      END LOOP;

      IF NOT multi_term_in THEN
        IF row_count <> 1 THEN
          raise_application_error(-20527, bwcklibs.error_msg_table(-20527));
        ELSIF astd_prevent_reg_ind = 'Y' THEN
          raise_application_error(-20529, bwcklibs.error_msg_table(-20529));
        END IF;
      END IF;
    END IF;

    -- Ensure below proc is called, commenting for now

    /*  dbms_output.put_line('Calling sfksels.p_get_min_max_by_curric_stand....');

      p_get_min_max_by_curric_stand(p_term         => term,
                                    p_pidm         => genpidm,
                                    p_seq_no       => NULL,
                                    p_astd_code    => astd_code,
                                    p_cast_code    => cast_code,
                                    p_min_hrs_out  => mhrs_min_hrs,
                                    p_max_hrs_out  => mhrs_max_hrs,
                                    p_min_srce_out => minh_srce,
                                    p_max_srce_out => maxh_srce);

      dbms_output.put_line('sfksels.p_get_min_max_by_curric_stand completed...' ||
                           'p_min_hrs_out:' || mhrs_min_hrs ||
                           ',p_max_hrs_out:' || mhrs_max_hrs ||
                           ',p_min_srce_out:' || minh_srce ||
                           ',p_max_srce_out:' || maxh_srce);

    */
    lv_admit_term := sokccur.f_curriculum_value(p_pidm      => genpidm,
                                                p_lmod_code => sb_curriculum_str.f_learner,
                                                p_term_code => term,
                                                p_key_seqno => 99,
                                                p_eff_term  => term,
                                                p_order     => 1,
                                                p_field     => 'TADMIT');

    dbms_output.put_line('sokccur.f_curriculum_value...' ||
                         'lv_admit_term:' || lv_admit_term);

    IF f_readmit_required(term,
                          genpidm,
                          lv_admit_term,
                          sobterm_row.sobterm_readm_req,
                          multi_term_in) THEN
      raise_application_error(-20505, bwcklibs.error_msg_table(-20505));
    END IF;

    sobterm_row.sobterm_fee_assessmnt_eff_date := GREATEST(TRUNC(regs_date),
                                                           NVL(TRUNC(sobterm_row.sobterm_fee_assessmnt_eff_date),
                                                               TRUNC(regs_date)));

    FOR advr IN soklibs.advisorc(genpidm, term) LOOP
      advr_row := advr;
    END LOOP;
  END p_regschk;

  PROCEDURE p_defstcr(add_ind CHAR DEFAULT NULL) IS
    ssts_code stvssts.stvssts_code%TYPE;
    gmod_code stvgmod.stvgmod_code%TYPE;
    l_found   BOOLEAN;

    CURSOR ssts_code_c(ssts_code_in ssbsect.ssbsect_ssts_code%TYPE) IS
      SELECT MIN(stvssts_code)
        FROM stvssts
       WHERE (ssts_code_in IS NOT NULL AND stvssts_code = ssts_code_in)
         AND stvssts_reg_ind = 'N';

    cursor c_get_ptrm_code(cv_crn  in SSBSECT.SSBSECT_CRN%TYPE,
                           cv_term in STVTERM.STVTERM_CODE%TYPE) is
      select *
        from ssbsect
       where ssbsect_term_code = cv_term
         and ssbsect_crn = cv_crn;
  BEGIN
    dbms_output.put_line('Entered into p_defstcr procedure.');
    OPEN ssts_code_c(ssbsect_row.ssbsect_ssts_code);
    FETCH ssts_code_c
      INTO ssts_code;
    CLOSE ssts_code_c;

    IF ssts_code IS NOT NULL THEN
      null;
    END IF;

    row_count := 0;
    regs_date := sysdate;
    l_found   := FALSE;
    dbms_output.put_line('sftregs_row.sftregs_rsts_code :' ||
                         sftregs_row.sftregs_rsts_code);
    for r_get_ptrm_code in c_get_ptrm_code(sftregs_row.sftregs_term_code,
                                           sftregs_row.sftregs_crn) loop
      ssbsect_row.ssbsect_ptrm_code := r_get_ptrm_code.ssbsect_ptrm_code;
    end loop;
    IF ssbsect_row.ssbsect_ptrm_code IS NOT NULL THEN
      FOR sfrrsts IN sfkcurs.sfrrstsc(sftregs_row.sftregs_rsts_code,
                                      sftregs_row.sftregs_term_code,
                                      ssbsect_row.ssbsect_ptrm_code) LOOP
        IF TRUNC(regs_date) >= TRUNC(sfrrsts.sfrrsts_start_date) AND
           TRUNC(regs_date) <= TRUNC(sfrrsts.sfrrsts_end_date) THEN
          IF add_ind = 'Y' THEN
            sftregs_row.sftregs_ptrm_code := ssbsect_row.ssbsect_ptrm_code;
            sftregs_row.sftregs_gmod_code := ssbsect_row.ssbsect_gmod_code;
            sftregs_row.sftregs_credit_hr := ssbsect_row.ssbsect_credit_hrs;
            sftregs_row.sftregs_bill_hr   := ssbsect_row.ssbsect_bill_hrs;
            sftregs_row.sftregs_camp_code := ssbsect_row.ssbsect_camp_code;
            dbms_output.put_line('sftregs_row.sftregs_rsts_code :' ||
                                 sftregs_row.sftregs_rsts_code);
          END IF;
          l_found := TRUE;
        END IF;

        row_count := sfkcurs.sfrrstsc%rowcount;
      END LOOP;

      IF row_count = 0 THEN
        null;
      END IF;

      IF NOT l_found THEN
        raise_application_error(-20513, bwcklibs.error_msg_table(-20513));
      END IF;
    ELSE
      FOR ssrrsts_rec IN sfkcurs.ssrrstsc(sftregs_row.sftregs_rsts_code,
                                          sftregs_row.sftregs_term_code,
                                          sftregs_row.sftregs_crn) LOOP
        IF TRUNC(regs_date) BETWEEN ssrrsts_rec.ssbsect_reg_from_date AND
           ssrrsts_rec.ssbsect_reg_to_date THEN
          IF add_ind = 'Y' THEN
            sftregs_row.sftregs_ptrm_code := ssbsect_row.ssbsect_ptrm_code;
            sftregs_row.sftregs_gmod_code := ssbsect_row.ssbsect_gmod_code;
            sftregs_row.sftregs_credit_hr := ssbsect_row.ssbsect_credit_hrs;
            sftregs_row.sftregs_bill_hr   := ssbsect_row.ssbsect_bill_hrs;
            sftregs_row.sftregs_camp_code := ssbsect_row.ssbsect_camp_code;
          END IF;
          l_found := TRUE;
        END IF;

        row_count := sfkcurs.ssrrstsc%rowcount;
      END LOOP;

      /* IF row_count = 0 THEN
        raise_application_error(-20608, bwcklibs.error_msg_table(-20608));
      END IF;*/

      /*  IF NOT l_found THEN
        raise_application_error(-20609, bwcklibs.error_msg_table(-20609));
      END IF; */
    END IF;

    IF add_ind IS NOT NULL AND add_ind = 'Y' THEN
      IF NVL(sftregs_row.sftregs_credit_hr, 00.00) = 00.00 THEN
        sftregs_row.sftregs_credit_hr := scbcrse_row.scbcrse_credit_hr_low;
      END IF;

      IF NVL(sftregs_row.sftregs_bill_hr, 00.00) = 00.00 THEN
        sftregs_row.sftregs_bill_hr := scbcrse_row.scbcrse_bill_hr_low;
      END IF;

      IF ssbsect_row.ssbsect_gmod_code IS NOT NULL THEN
        sftregs_row.sftregs_gmod_code := ssbsect_row.ssbsect_gmod_code;
      ELSE
        FOR scrgmod IN scklibs.scrgmodc(ssbsect_row.ssbsect_subj_code,
                                        ssbsect_row.ssbsect_crse_numb,
                                        sftregs_row.sftregs_term_code) LOOP
          gmod_code := scrgmod.scrgmod_gmod_code;

          IF scrgmod.scrgmod_default_ind = 'D' THEN
            sftregs_row.sftregs_gmod_code := scrgmod.scrgmod_gmod_code;
            EXIT;
          END IF;
        END LOOP;

        IF sftregs_row.sftregs_gmod_code IS NULL THEN
          sftregs_row.sftregs_gmod_code := gmod_code;
        END IF;
      END IF;
    END IF;
    dbms_output.put_line('p_defstcr procedure completed.');
    dbms_output.put_line('sftregs_row.sftregs_rsts_code :' ||
                         sftregs_row.sftregs_rsts_code);
  EXCEPTION
    WHEN bwcklibs.sect_prev_regs THEN
      raise_application_error(-20511, bwcklibs.error_msg_table(-20511));
    WHEN bwcklibs.ptrm_stat_undefined THEN
      raise_application_error(-20512, bwcklibs.error_msg_table(-20512));
    WHEN bwcklibs.crse_date_err THEN
      raise_application_error(-20513, bwcklibs.error_msg_table(-20513));
    WHEN bwcklibs.olr_stat_undefined THEN
      raise_application_error(-20608, bwcklibs.error_msg_table(-20608));
    WHEN bwcklibs.olr_crse_date_err THEN
      raise_application_error(-20609, bwcklibs.error_msg_table(-20609));
    WHEN OTHERS THEN
      raise_application_error(-20533, bwcklibs.error_msg_table(-20533));
  END p_defstcr;

  function f_get_ptrm_code(p_crn_in    in SSBSECT.SSBSECT_CRN%TYPE,
                           p_term_code in STVTERM.STVTERM_CODE%TYPE)
    return varchar2 is
    v_ptrm_code varchar2(20);
  begin
    select ssbsect_ptrm_code
      into v_ptrm_code
      from ssbsect
     where ssbsect_crn = p_crn_in
       and ssbsect_term_code = p_term_code;
    return v_ptrm_code;
  exception
    when others then
      return v_ptrm_code;
  end f_get_ptrm_code;

  PROCEDURE p_initvalue(pidm_in        spriden.spriden_pidm%TYPE,
                        term_in        stvterm.stvterm_code%TYPE,
                        id_in          spriden.spriden_id%TYPE,
                        regs_date_in   DATE,
                        hold_passwd_in CHAR,
                        samsys_in      CHAR) IS
  BEGIN
    dbms_output.put_line('Control now owned by p_initval');
    global_pidm := pidm_in;
    term        := term_in;
    id          := id_in;
    regs_date   := TO_DATE(TO_CHAR(NVL(regs_date_in, SYSDATE),
                                   '' || G$_DATE.GET_NLS_DATE_FORMAT ||
                                   'HH24:MI:SS'),
                           '' || G$_DATE.GET_NLS_DATE_FORMAT || 'HH24:MI:SS');
    hold_passwd := hold_passwd_in;
    samsys      := samsys_in;

    IF NOT bwcklibs.f_validterm(sql_error, sobterm_row, term) THEN
      dbms_output.put_line('bwcklibs.f_validterm returned false');
    else
      dbms_output.put_line('bwcklibs.f_validterm returned true');
    END IF;
    dbms_output.put_line('Control is returning back to pz_student_course_register.');
  END p_initvalue;

  PROCEDURE p_insert_sfrstcr(sftregs_rec_in    IN sftregs%ROWTYPE,
                             class_sort_key_in IN sfrstcr.sfrstcr_class_sort_key%TYPE,
                             reg_seq_in        IN sfrstcr.sfrstcr_reg_seq%TYPE,
                             attend_hr_in      IN sfrstcr.sfrstcr_attend_hr%TYPE,
                             p_rowid           OUT varchar2) IS
  BEGIN
    INSERT INTO sfrstcr
      (sfrstcr_term_code,
       sfrstcr_pidm,
       sfrstcr_crn,
       sfrstcr_class_sort_key,
       sfrstcr_reg_seq,
       sfrstcr_ptrm_code,
       sfrstcr_rsts_code,
       sfrstcr_rsts_date,
       sfrstcr_error_flag,
       sfrstcr_rmsg_cde,
       sfrstcr_message,
       sfrstcr_bill_hr,
       sfrstcr_waiv_hr,
       sfrstcr_credit_hr,
       sfrstcr_bill_hr_hold,
       sfrstcr_credit_hr_hold,
       sfrstcr_gmod_code,
       sfrstcr_grde_code,
       sfrstcr_grde_code_mid,
       sfrstcr_grde_date,
       sfrstcr_dupl_over,
       sfrstcr_link_over,
       sfrstcr_corq_over,
       sfrstcr_preq_over,
       sfrstcr_time_over,
       sfrstcr_capc_over,
       sfrstcr_levl_over,
       sfrstcr_coll_over,
       sfrstcr_majr_over,
       sfrstcr_clas_over,
       sfrstcr_appr_over,
       sfrstcr_appr_received_ind,
       sfrstcr_add_date,
       sfrstcr_activity_date,
       sfrstcr_levl_code,
       sfrstcr_camp_code,
       sfrstcr_reserved_key,
       sfrstcr_attend_hr,
       sfrstcr_rept_over,
       sfrstcr_rpth_over,
       sfrstcr_test_over,
       sfrstcr_camp_over,
       sfrstcr_user,
       sfrstcr_degc_over,
       sfrstcr_prog_over,
       sfrstcr_dept_over,
       sfrstcr_atts_over,
       sfrstcr_chrt_over,
       sfrstcr_mexc_over,
       sfrstcr_gcmt_code,
       sfrstcr_stsp_key_sequence,
       sfrstcr_assess_activity_date)
    VALUES
      (sftregs_rec_in.sftregs_term_code,
       sftregs_rec_in.sftregs_pidm,
       sftregs_rec_in.sftregs_crn,
       class_sort_key_in,
       reg_seq_in,
       sftregs_rec_in.sftregs_ptrm_code,
       sftregs_rec_in.sftregs_rsts_code,
       sftregs_rec_in.sftregs_rsts_date,
       sftregs_rec_in.sftregs_error_flag,
       sftregs_rec_in.sftregs_rmsg_cde,
       sftregs_rec_in.sftregs_message,
       sftregs_rec_in.sftregs_bill_hr,
       sftregs_rec_in.sftregs_waiv_hr,
       sftregs_rec_in.sftregs_credit_hr,
       sftregs_rec_in.sftregs_bill_hr_hold,
       sftregs_rec_in.sftregs_credit_hr_hold,
       sftregs_rec_in.sftregs_gmod_code,
       sftregs_rec_in.sftregs_grde_code,
       sftregs_rec_in.sftregs_grde_code_mid,
       sftregs_rec_in.sftregs_grde_date,
       sftregs_rec_in.sftregs_dupl_over,
       sftregs_rec_in.sftregs_link_over,
       sftregs_rec_in.sftregs_corq_over,
       sftregs_rec_in.sftregs_preq_over,
       sftregs_rec_in.sftregs_time_over,
       sftregs_rec_in.sftregs_capc_over,
       sftregs_rec_in.sftregs_levl_over,
       sftregs_rec_in.sftregs_coll_over,
       sftregs_rec_in.sftregs_majr_over,
       sftregs_rec_in.sftregs_clas_over,
       sftregs_rec_in.sftregs_appr_over,
       sftregs_rec_in.sftregs_appr_received_ind,
       sftregs_rec_in.sftregs_add_date,
       sftregs_rec_in.sftregs_activity_date,
       sftregs_rec_in.sftregs_levl_code,
       sftregs_rec_in.sftregs_camp_code,
       sftregs_rec_in.sftregs_reserved_key,
       attend_hr_in,
       sftregs_rec_in.sftregs_rept_over,
       sftregs_rec_in.sftregs_rpth_over,
       sftregs_rec_in.sftregs_test_over,
       sftregs_rec_in.sftregs_camp_over,
       sftregs_rec_in.sftregs_user,
       sftregs_rec_in.sftregs_degc_over,
       sftregs_rec_in.sftregs_prog_over,
       sftregs_rec_in.sftregs_dept_over,
       sftregs_rec_in.sftregs_atts_over,
       sftregs_rec_in.sftregs_chrt_over,
       sftregs_rec_in.sftregs_mexc_over,
       sftregs_rec_in.sftregs_gcmt_cde,
       sftregs_rec_in.sftregs_stsp_key_sequence,
       SYSDATE)
    returning rowid into p_rowid;
    commit;
  END p_insert_sfrstcr;

  FUNCTION f_validlevl(sql_err OUT NUMBER,
                       term_in sftregs.sftregs_term_code%TYPE,
                       levl    sftregs.sftregs_levl_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN IS
    genpidm spriden.spriden_pidm%TYPE;
    CURSOR scrlevl_c(term_in      sftregs.sftregs_term_code%TYPE,
                     subj_code_in ssbsect.ssbsect_subj_code%TYPE,
                     crse_numb_in ssbsect.ssbsect_crse_numb%TYPE) IS
      SELECT MIN(scrlevl_levl_code) levl_code
        FROM scrlevl
       WHERE scrlevl_subj_code = subj_code_in
         AND scrlevl_crse_numb = crse_numb_in
         AND scrlevl_eff_term =
             (SELECT MAX(scrlevl_eff_term)
                FROM scrlevl
               WHERE scrlevl_subj_code = subj_code_in
                 AND scrlevl_crse_numb = crse_numb_in
                 AND scrlevl_eff_term <= term_in);
  BEGIN

    IF levl IS NOT NULL THEN
      sftregs_row.sftregs_levl_code := levl;
    END IF;

    IF sftregs_row.sftregs_levl_code IS NULL THEN
      IF stud_level IS NULL THEN
        stud_level := sokccur.f_curriculum_value(p_pidm      => global_pidm,
                                                 p_lmod_code => sb_curriculum_str.f_learner,
                                                 p_term_code => term,
                                                 p_key_seqno => 99,
                                                 p_eff_term  => term_in,
                                                 p_order     => 1,
                                                 p_field     => 'LEVL');
      END IF;
      for r_scrlevl_c in scrlevl_c(term_in,
                                   ssbsect_row.ssbsect_subj_code,
                                   ssbsect_row.ssbsect_crse_numb) loop
        sftregs_row.sftregs_levl_code := r_scrlevl_c.levl_code;
      end loop;
      if sftregs_row.sftregs_levl_code is null then
        sftregs_row.sftregs_levl_code := stud_level;
      end if;
    END IF;

    row_count := 0;

    FOR scrlevl IN scklibs.scrlevlc(ssbsect_row.ssbsect_subj_code,
                                    ssbsect_row.ssbsect_crse_numb,
                                    term_in,
                                    sftregs_row.sftregs_levl_code) LOOP
      row_count := scklibs.scrlevlc%rowcount;
    END LOOP;
    dbms_output.put_line('CRSE in Level Validation : ' ||
                         ssbsect_row.ssbsect_crse_numb);
    dbms_output.put_line('LEVL in Level Validation : ' ||
                         sftregs_row.sftregs_levl_code);
    dbms_output.put_line('sftregs_row.sftregs_levl_code: ' ||
                         sftregs_row.sftregs_levl_code);
    IF row_count = 0 THEN
      sql_err := -20532;
      RETURN FALSE;
    END IF;

    RETURN TRUE;
  END f_validlevl;

  procedure pz_stu_regs_commit(p_crn         IN ssbsect.ssbsect_crn%type,
                               P_student_ID1 IN VARCHAR2,
                               P_TERM        IN varchar2,
                               P_RSTS_CODE   IN SFRSTCR.SFRSTCR_RSTS_CODE%type,
                               P_ERROR_MSG   OUT VARCHAR2,
                               P_ROWID_OUT   OUT VARCHAR2) is
    v_pidm number;
    cursor c_get_pidm is
      select spriden_pidm
        from spriden
       where spriden_id = p_student_id1
         and spriden_change_ind is null;
    p_student_id varchar2(10);
  begin
    for r_get_pidm in c_get_pidm loop
      v_pidm := r_get_pidm.spriden_pidm;
    end loop;
    if v_pidm is null then
      p_error_msg := 'Invalid student ID.';
      return;
    end if;
    pz_student_course_register(v_pidm,
                               p_term,
                               p_crn,
                               P_RSTS_CODE,
                               p_error_msg,
                               p_rowid_out);

    --If success then take the rowid
    if p_error_msg is null then
      begin
        select rowid
          into P_ROWID_STCR
          from sftregs
         where sftregs_pidm = global_pidm
           and sftregs_term_code = gv_term_in
           and sftregs_crn = gv_crn_in;
      exception
        when others then
          P_ROWID_STCR := null;
      end;
    end if;
    dbms_output.put_line('Error : ' || p_error_msg);
    dbms_output.put_line('ROWID : ' || P_ROWID_STCR);

    /*begin

    exception when others then
      p_error_msg := 'No records in temporary table';
    end;
    */
    if P_ROWID_STCR is null and p_error_msg is null then
      begin
        select sftregs_message
          into p_error_msg
          from sftregs
         where sftregs_pidm = global_pidm
           and sftregs_crn = gv_crn_in
           and sftregs_term_code = gv_term_in
           and sftregs_error_flag = 'F';
      exception
        when others then
          p_error_msg := 'No records in temporary table';

      end;
    end if;

    P_ROWID_OUT  := P_ROWID_STCR;
    P_ROWID_STCR := null;

  exception
    when others then
      P_ERROR_MSG  := sqlerrm;
      p_student_id := baninst1.gb_common.f_get_id(v_pidm);

  end PZ_STU_REGS_COMMIT;

  PROCEDURE p_update_regs(pidm_in            IN sftregs.sftregs_pidm%TYPE,
                          term_in            IN sftregs.sftregs_term_code%TYPE,
                          reg_date_in        IN sftregs.sftregs_add_date%TYPE,
                          clas_code_in       IN sgrclsr.sgrclsr_clas_code%TYPE,
                          styp_code_in       IN sgbstdn.sgbstdn_styp_code%TYPE,
                          capc_severity_in   IN sobterm.sobterm_capc_severity%TYPE,
                          tmst_calc_ind_in   IN sobterm.sobterm_tmst_calc_ind%TYPE,
                          tmst_maint_ind_in  IN sfbetrm.sfbetrm_tmst_maint_ind%TYPE,
                          tmst_code_in       IN sfbetrm.sfbetrm_tmst_code%TYPE,
                          drop_last_class_in IN BOOLEAN DEFAULT TRUE,
                          system_in          IN VARCHAR2,
                          error_rec_out      OUT sftregs%ROWTYPE,
                          error_flag_out     OUT VARCHAR2,
                          tmst_flag_out      OUT VARCHAR2,
                          p_error_msg        OUT VARCHAR2) IS
    source_pgm             VARCHAR2(7);
    dummy_var              VARCHAR2(1);
    save_act_date          VARCHAR2(20);
    stcr_count             NUMBER(3) := 0;
    reg_count              NUMBER(3) := 0;
    deleted_rec            sftregs%ROWTYPE;
    sftregs_rec            sftregs%ROWTYPE;
    sfrstcr_rec            sfrstcr%ROWTYPE;
    stvrsts_rec            stvrsts%ROWTYPE;
    ssrresv_rec            ssrresv%ROWTYPE;
    ssrxlst_rec            ssrxlst%ROWTYPE;
    sfrregd_rec            sfrregd%ROWTYPE;
    reg_oneup              ssbsect.ssbsect_reg_oneup%TYPE;
    seats_avail            ssbsect.ssbsect_seats_avail%TYPE;
    wait_avail             ssbsect.ssbsect_wait_avail%TYPE;
    wait_count             ssbsect.ssbsect_wait_count%TYPE;
    wait_capc              ssbsect.ssbsect_wait_capacity%TYPE;
    census_enrl            ssbsect.ssbsect_census_enrl%TYPE;
    census_date            ssbsect.ssbsect_census_enrl_date%TYPE;
    census_2_enrl          ssbsect.ssbsect_census_2_enrl%TYPE;
    census_2_date          ssbsect.ssbsect_census_2_date%TYPE;
    hold_old_incl_enrl     stvrsts.stvrsts_incl_sect_enrl%TYPE;
    old_incl_enrl          stvrsts.stvrsts_incl_sect_enrl%TYPE;
    old_incl_assess        stvrsts.stvrsts_incl_assess%TYPE;
    old_wait_ind           stvrsts.stvrsts_wait_ind%TYPE;
    old_wl_priority        sfrstcr_rec.sfrstcr_wl_priority%TYPE;
    old_withdraw_ind       stvrsts.stvrsts_withdraw_ind%TYPE;
    old_rsts_code          sftregs.sftregs_rsts_code%TYPE;
    old_error_flag         sftregs.sftregs_error_flag%TYPE;
    old_credit_hr          sftregs.sftregs_credit_hr%TYPE;
    resv_ind               ssbsect.ssbsect_reserved_ind%TYPE;
    resv_seats_avail       ssrresv.ssrresv_seats_avail%TYPE;
    resv_wait_avail        ssrresv.ssrresv_wait_avail%TYPE;
    resv_wait_count        ssrresv.ssrresv_wait_count%TYPE;
    resv_wait_capc         ssrresv.ssrresv_wait_capacity%TYPE;
    resv_overflow_ind      ssrresv.ssrresv_overflow_ind%TYPE;
    xlst_seats_avail       ssbxlst.ssbxlst_seats_avail%TYPE;
    xlst_group             ssbxlst.ssbxlst_xlst_group%TYPE;
    err_dummy              sftregs.sftregs_error_flag%TYPE;
    hold_rmsg_cde          sftregs.sftregs_rmsg_cde%TYPE;
    hold_message           sftregs.sftregs_message%TYPE;
    class_sort_key         sfrstcr.sfrstcr_class_sort_key%TYPE;
    attend_hr              sfrstcr.sfrstcr_attend_hr%TYPE;
    reg_seq                sfrstcr.sfrstcr_reg_seq%TYPE;
    this_crn               sfrstcr.sfrstcr_crn%TYPE;
    tmst_code              stvtmst.stvtmst_code%TYPE;
    tmst_desc              stvtmst.stvtmst_desc%TYPE;
    eff_ext_num            sfrareg.sfrareg_extension_number%TYPE;
    last_assess_date       sfbetrm.sfbetrm_assessment_date%TYPE;
    credit_hr_hold         sfrstcr.sfrstcr_credit_hr_hold%TYPE;
    bill_hr_hold           sfrstcr.sfrstcr_bill_hr_hold%TYPE;
    ssbsect_rowid          VARCHAR2(26);
    ssrresv_rowid          VARCHAR2(26);
    ssbxlst_rowid          VARCHAR2(26);
    old_ssrresv_rowid      VARCHAR2(26);
    fa_return_status       NUMBER(1);
    NULL_RESVC             VARCHAR2(82) := '##################################################################################';
    lv_wl_term_control_ref sb_wl_term_control.wl_term_control_ref;
    lv_wl_term_control_rec sb_wl_term_control.wl_term_control_rec;
    lv_control_term_exists VARCHAR2(1) := 'N';
    lv_appr_severity       sobterm.sobterm_appr_severity%TYPE;
    lv_levl_severity       sobterm.sobterm_levl_severity%TYPE;
    lv_coll_severity       sobterm.sobterm_coll_severity%TYPE;
    lv_degr_severity       sobterm.sobterm_degree_severity%TYPE;
    lv_prog_severity       sobterm.sobterm_program_severity%TYPE;
    lv_majr_severity       sobterm.sobterm_majr_severity%TYPE;
    lv_camp_severity       sobterm.sobterm_camp_severity%TYPE;
    lv_clas_severity       sobterm.sobterm_clas_severity%TYPE;
    lv_capc_severity       sobterm.sobterm_capc_severity%TYPE;
    lv_dept_severity       sobterm.sobterm_dept_severity%TYPE;
    lv_atts_severity       sobterm.sobterm_atts_severity%TYPE;
    lv_chrt_severity       sobterm.sobterm_chrt_severity%TYPE;
    lv_mexc_severity       sobterm.sobterm_mexc_severity%TYPE;
    --
    CURSOR sfbetrm_c IS
      SELECT sfbetrm_assessment_date
        FROM sfbetrm
       WHERE sfbetrm_assessment_date is not null
         AND sfbetrm_term_code = term_in
         AND sfbetrm_pidm = pidm_in;
    --
    CURSOR sfrfaud_c IS
      SELECT 'X'
        FROM sfrfaud
       WHERE sfrfaud_term_code = term_in
         AND sfrfaud_pidm = pidm_in;
    --
    CURSOR sftregs_row_c IS
      SELECT *
        FROM sftregs
       WHERE sftregs_term_code = term_in
         AND sftregs_pidm = pidm_in
       ORDER BY sftregs_add_date, sftregs_ptrm_code, sftregs_crn;
    --
    CURSOR sfrstcr_row_c IS
      SELECT *
        FROM sfrstcr
       WHERE sfrstcr_crn = this_crn
         AND sfrstcr_term_code = term_in
         AND sfrstcr_pidm = pidm_in;
    --
    CURSOR section_hrs_c(p_term VARCHAR2, p_crn VARCHAR2) IS
      SELECT NVL(ssbsect_credit_hrs, scbcrse_credit_hr_low),
             NVL(ssbsect_bill_hrs, scbcrse_bill_hr_low)
        FROM scbcrse a, ssbsect
       WHERE scbcrse_crse_numb = ssbsect_crse_numb
         AND scbcrse_subj_code = ssbsect_subj_code
         AND scbcrse_eff_term =
             (SELECT MAX(b.scbcrse_eff_term)
                FROM scbcrse b
               WHERE b.scbcrse_crse_numb = ssbsect_crse_numb
                 AND b.scbcrse_subj_code = ssbsect_subj_code
                 AND b.scbcrse_eff_term <= p_term)
         AND ssbsect_term_code = p_term
         AND ssbsect_crn = p_crn;
    --
  BEGIN
    --
    gokfgac.p_turn_fgac_off;
    --
    gb_common.p_commit;
    error_flag_out := 'N';
    --

    lv_wl_term_control_ref := sb_wl_term_control.f_query_one(p_term_code => term_in); --ssbwltc
    FETCH lv_wl_term_control_ref
      INTO lv_wl_term_control_rec;
    IF lv_wl_term_control_ref%FOUND THEN
      lv_control_term_exists := 'Y';
    END IF;
    CLOSE lv_wl_term_control_ref;

    IF NOT drop_last_class_in THEN
      SELECT count(*)
        INTO stcr_count
        FROM stvrsts, sfrstcr
       WHERE stvrsts_code = sfrstcr_rsts_code
         AND NVL(stvrsts_voice_type, 'N') NOT IN ('D', 'W')
         AND NVL(sfrstcr_error_flag, 'N') NOT IN ('D', 'F', 'L')
         AND sfrstcr_term_code = term_in
         AND sfrstcr_pidm = pidm_in;

      SELECT count(*)
        INTO reg_count
        FROM stvrsts, sftregs
       WHERE stvrsts_code = sftregs_rsts_code
         AND NVL(stvrsts_voice_type, 'N') NOT IN ('D', 'W')
         AND NVL(sftregs_error_flag, 'N') NOT IN ('D', 'F', 'L')
         AND sftregs_term_code = term_in
         AND sftregs_pidm = pidm_in;
      --
      IF (stcr_count > 0) and (reg_count = 0) THEN
        error_flag_out := 'L';
        goto end_p_update_regs;
      END IF;
    END IF;
    --
    --  Set SFBETRM_INITIAL_REG_DATE if currently NULL for this pidm/term.
    --
    UPDATE sfbetrm
       SET sfbetrm_initial_reg_date = reg_date_in
     WHERE sfbetrm_initial_reg_date IS NULL
       AND sfbetrm_term_code = term_in
       AND sfbetrm_pidm = pidm_in;

    dbms_output.put_line('Process fee assesment details');
    dbms_output.put_line('Term:' || term_in || ';pidm_in:' || pidm_in ||
                         ';source_pgm:' || source_pgm ||
                         '||save_act_date:' || save_act_date);

    SFKFEES.p_processfeeassessment(term_in,
                                   pidm_in,
                                   NULL,
                                   SYSDATE,
                                   'R',
                                   'Y',
                                   'SFKFEES',
                                   'Y',
                                   save_act_date,
                                   'N',
                                   fa_return_status);

    dbms_output.put_line('Fee assesment completed, return status:' ||
                         fa_return_status);

    --
    --  If no rows exist in SFRFAUD for this pidm/term, and Fee Assessment
    --  Date in SFBETRM is not null, create a row in SFRFAUD before updating
    --  SFRSTCR. (SFRFAUD row is created by sfkfees.p_processfeeassessment).
    --
    OPEN sfbetrm_c;
    FETCH sfbetrm_c
      INTO last_assess_date;
    IF sfbetrm_c%NOTFOUND THEN
      CLOSE sfbetrm_c;
      last_assess_date := NULL;
    ELSE
      CLOSE sfbetrm_c;
      OPEN sfrfaud_c;
      FETCH sfrfaud_c
        INTO dummy_var;
      IF sfrfaud_c%NOTFOUND THEN
        IF system_in = 'S' THEN
          source_pgm := 'SFAREGS';
        ELSIF system_in in ('WA', 'WC') THEN
          source_pgm := 'WEB';
        ELSIF system_in = 'V' THEN
          source_pgm := 'VR';
        ELSE
          source_pgm := 'SFKEDIT';
        END IF;
        BEGIN
          sfkfees.p_processfeeassessment(term_in,
                                         pidm_in,
                                         NULL,
                                         SYSDATE,
                                         'R',
                                         'N',
                                         source_pgm,
                                         'Y',
                                         save_act_date,
                                         'N',
                                         fa_return_status);
        EXCEPTION
          WHEN OTHERS THEN
            gokfgac.p_turn_fgac_on;
            p_error_msg := 'Exception raised while frocessing fee assessment.';
            raise_application_error(-20044,
                                    G$_NLS.GET('SFKEDIT1-0047',
                                               'SQL',
                                               'sfkfees.p_processfeeassessment') ||
                                    SQLCODE || ' ' || SQLERRM);
        END;
        CLOSE sfrfaud_c;
      ELSE
        CLOSE sfrfaud_c;
      END IF;
    END IF;
    --
    BEGIN
      sskmods.p_lock_all_sects_for_pidm_term(term_in, pidm_in);
    EXCEPTION
      WHEN OTHERS THEN
        gokfgac.p_turn_fgac_on;
        p_error_msg := 'Exception raised while locking sections for the pidm.';
        raise_application_error(-20045,
                                G$_NLS.GET('SFKEDIT1-0048',
                                           'SQL',
                                           'sskmods.p_lock_all_sects_for_pidm_term') ||
                                SQLCODE || ' ' || SQLERRM);
    END;
    --
    IF NOT sftregs_row_c%ISOPEN THEN
      OPEN sftregs_row_c;
    END IF;
    LOOP
      FETCH sftregs_row_c
        INTO sftregs_rec;
      EXIT WHEN sftregs_row_c%NOTFOUND;
      this_crn := sftregs_rec.sftregs_crn;
      IF NVL(sftregs_rec.sftregs_rec_stat, 'Q') <> 'Q' THEN
        dbms_output.put_line('output1' || this_crn);
        /*                                                                            */
        /* Delete row that may exist in SFRREGD for this pidm/term/crn. The new       */
        /* action taken on this registration supersedes the drop/delete that was      */
        /* waiting in the collector table for processing by Fee Assessment.           */
        /*                                                                            */
        BEGIN
          dbms_output.put_line('output2');
          sfkmods.p_delete_sfrregd(sftregs_rec.sftregs_pidm,
                                   sftregs_rec.sftregs_term_code,
                                   sftregs_rec.sftregs_crn);
        EXCEPTION
          WHEN OTHERS THEN
            gokfgac.p_turn_fgac_on;
            raise_application_error(-20046,
                                    G$_NLS.GET('SFKEDIT1-0049',
                                               'SQL',
                                               'sfkmods.p_delete_sfrregd') ||
                                    SQLCODE || ' ' || SQLERRM);
        END;
        --
        BEGIN
          dbms_output.put_line('output3');
          eff_ext_num := sfkfunc.f_sfrareg_eff_ext(sftregs_rec.sftregs_pidm,
                                                   sftregs_rec.sftregs_term_code,
                                                   sftregs_rec.sftregs_crn,
                                                   reg_date_in);
        EXCEPTION
          WHEN OTHERS THEN
            gokfgac.p_turn_fgac_on;
            null;
            /*  raise_application_error(-20047,
            G$_NLS.GET('SFKEDIT1-0050',
                       'SQL',
                       'sfkfunc.f_sfrareg_eff_ext') ||
            SQLCODE || ' ' || SQLERRM); */
        END;
        sftregs_rec.sftregs_user := USER;
        dbms_output.put_line('output4' || 'user' || ' ' ||
                             sftregs_rec.sftregs_user);
        IF NVL(sftregs_rec.sftregs_rec_stat, 'Q') <> 'N' THEN
          dbms_output.put_line('output5');
          IF NOT sfrstcr_row_c%ISOPEN THEN
            OPEN sfrstcr_row_c;
          END IF;
          FETCH sfrstcr_row_c
            INTO sfrstcr_rec;
          IF sfrstcr_row_c%NOTFOUND THEN
            error_flag_out := 'D';
            gb_common.p_rollback;
            EXIT;
          END IF;
          CLOSE sfrstcr_row_c;
          reg_seq            := sfrstcr_rec.sfrstcr_reg_seq;
          attend_hr          := sfrstcr_rec.sfrstcr_attend_hr;
          class_sort_key     := sfrstcr_rec.sfrstcr_class_sort_key;
          stvrsts_rec        := stksels.f_get_stvrsts_rec(sfrstcr_rec.sfrstcr_rsts_code);
          old_incl_enrl      := stvrsts_rec.stvrsts_incl_sect_enrl;
          hold_old_incl_enrl := stvrsts_rec.stvrsts_incl_sect_enrl;
          old_incl_assess    := stvrsts_rec.stvrsts_incl_assess;
          old_wait_ind       := stvrsts_rec.stvrsts_wait_ind;
          old_withdraw_ind   := stvrsts_rec.stvrsts_withdraw_ind;
          old_rsts_code      := sfrstcr_rec.sfrstcr_rsts_code;
          old_error_flag     := sfrstcr_rec.sfrstcr_error_flag;
          old_credit_hr      := sfrstcr_rec.sfrstcr_credit_hr;
          old_wl_priority    := sfrstcr_rec.sfrstcr_wl_priority;

        ELSE
          hold_old_incl_enrl := 'N';
          old_incl_enrl      := 'N';
          old_withdraw_ind   := 'N';
          old_wait_ind       := 'N';
          old_credit_hr      := 0;
        END IF;
        dbms_output.put_line('output6' || 'reserved key' ||
                             sfrstcr_rec.sfrstcr_reserved_key);
        IF sfrstcr_rec.sfrstcr_reserved_key IS NOT NULL THEN
          BEGIN
            old_ssrresv_rowid := ssksels.f_get_ssrresv_rowid(p_resv_key  => sfrstcr_rec.sfrstcr_reserved_key,
                                                             p_term_code => sftregs_rec.sftregs_term_code,
                                                             p_crn       => sftregs_rec.sftregs_crn);
          EXCEPTION
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20050,
                                      G$_NLS.GET('SFKEDIT1-0051',
                                                 'SQL',
                                                 'sfkwait.f_get_ssrresv_rowid_prior') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
        ELSE
          old_ssrresv_rowid := NULL;
        END IF;

        stvrsts_rec := stksels.f_get_stvrsts_rec(sftregs_rec.sftregs_rsts_code);
        ssrxlst_rec := ssksels.f_get_ssrxlst_rec(sftregs_rec.sftregs_crn,
                                                 sftregs_rec.sftregs_term_code);

        dbms_output.put_line('output7' || ' credit_hr' ||
                             sftregs_rec.sftregs_credit_hr || ' ' ||
                             stvrsts_rec.stvrsts_incl_sect_enrl);
        --
        IF (sftregs_rec.sftregs_credit_hr > 0) OR
           (stvrsts_rec.stvrsts_incl_sect_enrl = 'Y') THEN
          sftregs_rec.sftregs_credit_hr_hold := sftregs_rec.sftregs_credit_hr;
        END IF;
        --
        IF (sftregs_rec.sftregs_bill_hr > 0) OR
           (stvrsts_rec.stvrsts_incl_assess = 'Y') THEN
          sftregs_rec.sftregs_bill_hr_hold := sftregs_rec.sftregs_bill_hr;
        END IF;
        --
        IF (sftregs_rec.sftregs_credit_hr_hold IS NULL) OR
           (sftregs_rec.sftregs_bill_hr_hold IS NULL) THEN
          dbms_output.put_line('output8');
          OPEN section_hrs_c(sftregs_rec.sftregs_term_code,
                             sftregs_rec.sftregs_crn);
          FETCH section_hrs_c
            INTO credit_hr_hold, bill_hr_hold;
          dbms_output.put_line('output9' || 'credit_hr_hold' ||
                               credit_hr_hold || ' ' || bill_hr_hold);
          IF section_hrs_c%NOTFOUND THEN
            dbms_output.put_line('output10');
            CLOSE section_hrs_c;
            error_flag_out := 'D';
            gb_common.p_rollback;
            EXIT;
          END IF;
          CLOSE section_hrs_c;
          IF sftregs_rec.sftregs_credit_hr_hold IS NULL THEN
            dbms_output.put_line('output11');
            sftregs_rec.sftregs_credit_hr_hold := credit_hr_hold;
          END IF;
          IF sftregs_rec.sftregs_bill_hr_hold IS NULL THEN
            dbms_output.put_line('output12');
            sftregs_rec.sftregs_bill_hr_hold := bill_hr_hold;
          END IF;
        END IF;
        --
        -- Event though all CRNs were locked above, lock each again before the update,
        -- so that all OUT parameters are correctly valued for the update.
        --
        BEGIN
          dbms_output.put_line('output13');
          sskmods.p_lock_ssbsect_seats_avail(ssbsect_rowid_in_out => ssbsect_rowid,
                                             ssrresv_rowid_in_out => ssrresv_rowid,
                                             ssbxlst_rowid_in_out => ssbxlst_rowid,
                                             reg_oneup_in_out     => reg_oneup,
                                             seats_avail_in_out   => seats_avail,
                                             wait_avail_in_out    => wait_avail,
                                             wait_count_in_out    => wait_count,
                                             wait_capc_in_out     => wait_capc,
                                             census_enrl_in_out   => census_enrl,
                                             census_date_in_out   => census_date,
                                             census_2_enrl_in_out => census_2_enrl,
                                             census_2_date_in_out => census_2_date,
                                             resv_ind_in_out      => resv_ind,
                                             resv_key_in_out      => sftregs_rec.sftregs_reserved_key,
                                             rseats_avail_in_out  => resv_seats_avail,
                                             rwait_avail_in_out   => resv_wait_avail,
                                             rwait_count_in_out   => resv_wait_count,
                                             rwait_capc_in_out    => resv_wait_capc,
                                             roverflow_in_out     => resv_overflow_ind,
                                             xlst_seats_in_out    => xlst_seats_avail,
                                             xlst_group_in        => ssrxlst_rec.ssrxlst_xlst_group,
                                             wait_ind_in          => stvrsts_rec.stvrsts_wait_ind,
                                             clas_in              => clas_code_in,
                                             term_in              => sftregs_rec.sftregs_term_code,
                                             crn_in               => sftregs_rec.sftregs_crn,
                                             pidm_in              => sftregs_rec.sftregs_pidm);
        EXCEPTION
          WHEN OTHERS THEN
            gokfgac.p_turn_fgac_on;
            raise_application_error(-20048,
                                    G$_NLS.GET('SFKEDIT1-0052',
                                               'SQL',
                                               'sskmods.p_lock_ssbsect_seats_avail') ||
                                    SQLCODE || ' ' || SQLERRM);
        END;
        --
        -- If capacity checking is turned on, and this is a reserve where seats are no
        -- longer available and overflow is allowed, check if seats are available in
        -- the NULL reserved row.  If so, change reserved_key to NULL key (all ##s);
        -- otherwise, leave the original reserved_key intact.
        -- This check must be done on all new registrations, and on existing records
        -- where the reserved key has changed.
        --
        lv_capc_severity := capc_severity_in;
        dbms_output.put_line('output14' || lv_capc_severity || ' ' ||
                             'rsts_wait_ind' ||
                             stvrsts_rec.stvrsts_wait_ind);
        IF stvrsts_rec.stvrsts_wait_ind = 'Y' THEN
          BEGIN
            dbms_output.put_line('output15');
            sfkwlat.p_set_severity_flags(p_term               => sftregs_rec.sftregs_term_code,
                                         p_crn                => sftregs_rec.sftregs_crn,
                                         appr_severity_in_out => lv_appr_severity,
                                         levl_severity_in_out => lv_levl_severity,
                                         coll_severity_in_out => lv_coll_severity,
                                         degr_severity_in_out => lv_degr_severity,
                                         prog_severity_in_out => lv_prog_severity,
                                         majr_severity_in_out => lv_majr_severity,
                                         camp_severity_in_out => lv_camp_severity,
                                         clas_severity_in_out => lv_clas_severity,
                                         capc_severity_in_out => lv_capc_severity,
                                         dept_severity_in_out => lv_dept_severity,
                                         atts_severity_in_out => lv_atts_severity,
                                         chrt_severity_in_out => lv_chrt_severity);
          EXCEPTION
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20049,
                                      G$_NLS.GET('SFKEDIT1-0053',
                                                 'SQL',
                                                 'sfkwlat.p_set_severity_flags') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
        END IF;
        dbms_output.put_line('output16' || lv_capc_severity || ' ' ||
                             resv_overflow_ind);
        IF (NVL(lv_capc_severity, 'N') <> 'N' AND resv_overflow_ind = 'Y') THEN
          dbms_output.put_line('output17');
          IF (NVL(sftregs_rec.sftregs_rec_stat, 'Q') = 'N' OR
             NVL(sftregs_rec.sftregs_reserved_key, 'X') <>
             NVL(sfrstcr_rec.sfrstcr_reserved_key, 'X')) THEN
            dbms_output.put_line('output18');
            IF NVL(sftregs_rec.sftregs_reserved_key, NULL_RESVC) <>
               NULL_RESVC AND resv_seats_avail <= 0 AND
               stvrsts_rec.stvrsts_incl_sect_enrl = 'Y' THEN
              dbms_output.put_line('output19');
              OPEN ssksels.ssrresv_lock_null_c(p_term => sftregs_rec.sftregs_term_code,
                                               p_crn  => sftregs_rec.sftregs_crn,
                                               p_old_wait_ind => old_wait_ind);
              FETCH ssksels.ssrresv_lock_null_c
                INTO ssrresv_rec;
              IF ssksels.ssrresv_lock_null_c%FOUND THEN
                dbms_output.put_line('output20');
                sftregs_rec.sftregs_reserved_key := NULL_RESVC;
                resv_seats_avail                 := ssrresv_rec.ssrresv_seats_avail;
                resv_wait_avail                  := ssrresv_rec.ssrresv_wait_avail;
                resv_wait_count                  := ssrresv_rec.ssrresv_wait_count;
                resv_wait_capc                   := ssrresv_rec.ssrresv_wait_capacity;
                BEGIN
                  dbms_output.put_line('output21');
                  ssrresv_rowid := ssksels.f_get_ssrresv_rowid(p_resv_key  => sftregs_rec.sftregs_reserved_key,
                                                               p_term_code => sftregs_rec.sftregs_term_code,
                                                               p_crn       => sftregs_rec.sftregs_crn);
                EXCEPTION
                  WHEN OTHERS THEN
                    gokfgac.p_turn_fgac_on;
                    raise_application_error(-20050,
                                            G$_NLS.GET('SFKEDIT1-0054',
                                                       'SQL',
                                                       'sfkwait.f_get_ssrresv_rowid') ||
                                            SQLCODE || ' ' || SQLERRM);
                END;
              END IF;
              CLOSE ssksels.ssrresv_lock_null_c;
            END IF;
          END IF;
        END IF;
        --
        dbms_output.put_line('output22');
        IF (NVL(sftregs_rec.sftregs_remove_ind, 'N') <> 'Y' AND
           NVL(lv_capc_severity, 'N') <> 'N' AND
           (NVL(old_incl_enrl, 'N') <> stvrsts_rec.stvrsts_incl_sect_enrl OR
           NVL(old_wait_ind, 'N') <> stvrsts_rec.stvrsts_wait_ind)) THEN
          dbms_output.put_line('output23');
          BEGIN
            dbms_output.put_line('output24' || 'begin capacity check');
            sfkwait.p_capacity_check(sftregs_rec.sftregs_term_code,
                                     sftregs_rec.sftregs_crn,
                                     lv_capc_severity,
                                     sftregs_rec.sftregs_error_flag,
                                     sftregs_rec.sftregs_rmsg_cde,
                                     sftregs_rec.sftregs_message,
                                     err_dummy,
                                     sftregs_rec.sftregs_capc_over,
                                     sftregs_rec.sftregs_wait_over,
                                     stvrsts_rec.stvrsts_wait_ind,
                                     stvrsts_rec.stvrsts_incl_sect_enrl,
                                     old_wait_ind,
                                     old_incl_enrl,
                                     resv_ind,
                                     seats_avail,
                                     wait_count,
                                     wait_capc,
                                     wait_avail,
                                     ssrxlst_rec.ssrxlst_xlst_group,
                                     xlst_seats_avail,
                                     resv_seats_avail,
                                     resv_wait_count,
                                     resv_wait_capc,
                                     resv_wait_avail,
                                     sftregs_rec.sftregs_pidm);
          EXCEPTION
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20050,
                                      G$_NLS.GET('SFKEDIT1-0055',
                                                 'SQL',
                                                 'sfkwait.p_capacity_check') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
          --
          -- If no seats left for this section, cannot finish processing reg updates.
          -- Rollback, to undo those updates that have already occurred in base tables.
          --
          IF NVL(sftregs_rec.sftregs_error_flag, '*') = 'F' THEN
            dbms_output.put_line('output25');
            IF sftregs_rec.sftregs_rmsg_cde = 'CLOS' OR
               sftregs_rec.sftregs_rmsg_cde = 'WAIT' OR
               sftregs_rec.sftregs_rmsg_cde = 'WAIF' OR
               sftregs_rec.sftregs_rmsg_cde = 'RESC' OR
               sftregs_rec.sftregs_rmsg_cde = 'RESV' OR
               sftregs_rec.sftregs_rmsg_cde = 'RESF' THEN
              dbms_output.put_line('output26');
              error_flag_out := 'Y';
              hold_rmsg_cde  := sftregs_rec.sftregs_rmsg_cde;
              hold_message   := sftregs_rec.sftregs_message;
              gb_common.p_rollback;
              --
              -- ROLLBACK wiped out this capacity error in SFTREGS record, so put it back.
              --
              UPDATE sftregs
                 SET sftregs_error_flag = 'F',
                     sftregs_rmsg_cde   = hold_rmsg_cde,
                     sftregs_message    = hold_message
               WHERE sftregs_crn = sftregs_rec.sftregs_crn
                 AND sftregs_term_code = sftregs_rec.sftregs_term_code
                 AND sftregs_pidm = sftregs_rec.sftregs_pidm;
              dbms_output.put_line('output27' ||
                                   'updated sftregs_error_flag' || ' ' ||
                                   hold_message);
              EXIT;
              dbms_output.put_line('output28');
            END IF;
          END IF;
        END IF;
        --
        IF NOT (NVL(sftregs_rec.sftregs_remove_ind, 'N') = 'Y' AND
            NVL(sftregs_rec.sftregs_rec_stat, 'Q') = 'N') THEN
          dbms_output.put_line('output29');
          BEGIN
            sskfunc.p_upd_section_hours(old_incl_enrl,
                                        old_wait_ind,
                                        stvrsts_rec.stvrsts_wait_ind,
                                        stvrsts_rec.stvrsts_incl_sect_enrl,
                                        sftregs_rec.sftregs_credit_hr,
                                        old_credit_hr,
                                        census_enrl,
                                        census_2_enrl,
                                        census_date,
                                        census_2_date,
                                        resv_ind,
                                        ssrxlst_rec.ssrxlst_xlst_group,
                                        reg_date_in,
                                        ssbsect_rowid,
                                        ssrresv_rowid,
                                        ssbxlst_rowid,
                                        old_ssrresv_rowid);
          EXCEPTION
            WHEN sokexps.section_does_not_exist THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20051,
                                      G$_NLS.GET('SFKEDIT1-0056',
                                                 'SQL',
                                                 'sskfunc.p_upd_section_hours - SSBSECT record not found'));
            WHEN sokexps.reserve_grouping_not_found THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20051,
                                      G$_NLS.GET('SFKEDIT1-0057',
                                                 'SQL',
                                                 'sskfunc.p_upd_section_hours - SSRRESV record not found'));
            WHEN sokexps.unable_to_update_ssbsect THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20051,
                                      G$_NLS.GET('SFKEDIT1-0058',
                                                 'SQL',
                                                 'sskfunc.p_upd_section_hours - Error when updating SSBSECT'));
            WHEN sokexps.unable_to_update_ssbxlst THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20051,
                                      G$_NLS.GET('SFKEDIT1-0059',
                                                 'SQL',
                                                 'sskfunc.p_upd_section_hours - Error when updating SSBXLST'));
            WHEN sokexps.unable_to_update_ssrresv THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20051,
                                      G$_NLS.GET('SFKEDIT1-0060',
                                                 'SQL',
                                                 'sskfunc.p_upd_section_hours - Error when updating SSRRESV'));
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20051,
                                      G$_NLS.GET('SFKEDIT1-0061',
                                                 'SQL',
                                                 'sskfunc.p_upd_section_hours') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
        END IF;
        --
        -- Check to see if pre-existing registration is being deleted from SFRSTCR
        --
        dbms_output.put_line('output30' || 'remove_ind' ||
                             sftregs_rec.sftregs_remove_ind || ' ' ||
                             'rec_stat' || sftregs_rec.sftregs_rec_stat);
        IF NVL(sftregs_rec.sftregs_remove_ind, 'N') = 'Y' THEN
          dbms_output.put_line('output31');
          IF sftregs_rec.sftregs_rec_stat <> 'N' THEN
            dbms_output.put_line('output32');
            --
            -- If Flat Charge Refunding is in effect, write a record to the Drop/Delete
            -- collector table (SFRREGD), unless the prior RSTS code was count_in_assess=N
            -- and that registration record was already processed by Fee Assessment
            --
            IF sfkfees.f_flat_rule_exists(term_in) THEN
              dbms_output.put_line('output33');
              IF NOT (old_incl_assess = 'N' AND
                  sfrstcr_rec.sfrstcr_activity_date <
                  NVL(last_assess_date,
                          TO_DATE(G$_DATE.NORMALISE_GREG_DATE('01-01-1900',
                                                              'DD-MM-YYYY'),
                                  G$_DATE.GET_NLS_DATE_FORMAT))) THEN
                dbms_output.put_line('output34');
                /*                                                                            */
                /* Write record to collector table, SFRREGD, before deleting from SFRSTCR.    */
                /*                                                                            */
                sfrregd_rec.sfrregd_term_code     := sftregs_rec.sftregs_term_code;
                sfrregd_rec.sfrregd_pidm          := sftregs_rec.sftregs_pidm;
                sfrregd_rec.sfrregd_crn           := sftregs_rec.sftregs_crn;
                sfrregd_rec.sfrregd_rsts_code     := sftregs_rec.sftregs_rsts_code;
                sfrregd_rec.sfrregd_rsts_date     := sftregs_rec.sftregs_rsts_date;
                sfrregd_rec.sfrregd_bill_hr       := sftregs_rec.sftregs_bill_hr;
                sfrregd_rec.sfrregd_waiv_hr       := sftregs_rec.sftregs_waiv_hr;
                sfrregd_rec.sfrregd_credit_hr     := sftregs_rec.sftregs_credit_hr;
                sfrregd_rec.sfrregd_add_date      := sftregs_rec.sftregs_add_date;
                sfrregd_rec.sfrregd_levl_code     := sftregs_rec.sftregs_levl_code;
                sfrregd_rec.sfrregd_camp_code     := sftregs_rec.sftregs_camp_code;
                sfrregd_rec.sfrregd_schd_code     := sftregs_rec.sftregs_sect_schd_code;
                sfrregd_rec.sfrregd_ptrm_code     := sftregs_rec.sftregs_ptrm_code;
                sfrregd_rec.sfrregd_gmod_code     := sftregs_rec.sftregs_gmod_code;
                sfrregd_rec.sfrregd_user          := sftregs_rec.sftregs_user;
                sfrregd_rec.sfrregd_activity_date := SYSDATE;
                BEGIN
                  dbms_output.put_line('output35');
                  sfkmods.p_insert_sfrregd(sfrregd_rec);
                EXCEPTION
                  WHEN OTHERS THEN
                    gokfgac.p_turn_fgac_on;
                    raise_application_error(-20052,
                                            G$_NLS.GET('SFKEDIT1-0062',
                                                       'SQL',
                                                       'sfkmods.p_insert_sfrregd') ||
                                            SQLCODE || ' ' || SQLERRM);
                END;
              END IF;
            END IF; /* IF sfkfees.f_flat_rule_exists */
            /*                                                                            */
            /* Update SFRSTCR fields needed for SFRSTCA record, so they will be correct   */
            /* when the reg audit record is written upon deletion of SFRSTCR record.      */
            /*                                                                            */
            sfrstcr_rec.sfrstcr_rsts_code     := sftregs_rec.sftregs_rsts_code;
            sfrstcr_rec.sfrstcr_rsts_date     := sftregs_rec.sftregs_rsts_date;
            sfrstcr_rec.sfrstcr_bill_hr       := sftregs_rec.sftregs_bill_hr;
            sfrstcr_rec.sfrstcr_credit_hr     := sftregs_rec.sftregs_credit_hr;
            sfrstcr_rec.sfrstcr_rmsg_cde      := sftregs_rec.sftregs_rmsg_cde;
            sfrstcr_rec.sfrstcr_message       := sftregs_rec.sftregs_message;
            sfrstcr_rec.sfrstcr_error_flag    := 'X';
            sfrstcr_rec.sfrstcr_user          := USER;
            sfrstcr_rec.sfrstcr_activity_date := SYSDATE;
            BEGIN
              dbms_output.put_line('output36');
              sfkmods.p_update_sfrstcr(sfrstcr_rec);
            EXCEPTION
              WHEN OTHERS THEN
                gokfgac.p_turn_fgac_on;
                raise_application_error(-20053,
                                        G$_NLS.GET('SFKEDIT1-0063',
                                                   'SQL',
                                                   'sfkmods.p_update_sfrstcr') ||
                                        SQLCODE || ' ' || SQLERRM);
            END;
            dbms_output.put_line('output37');
            DELETE FROM sfrstcr
             WHERE sfrstcr_crn = sftregs_rec.sftregs_crn
               AND sfrstcr_term_code = sftregs_rec.sftregs_term_code
               AND sfrstcr_pidm = sftregs_rec.sftregs_pidm;
            deleted_rec := sftregs_rec;
            dbms_output.put_line('output38');

            IF sfkwlat.f_wl_automation_active(p_term => sftregs_rec.sftregs_term_code,
                                              p_crn  => sftregs_rec.sftregs_crn) = 'Y' THEN
              BEGIN
                sfkwlat.p_notification_expires(p_term => sftregs_rec.sftregs_term_code,
                                               p_crn  => sftregs_rec.sftregs_crn);

              EXCEPTION
                WHEN OTHERS THEN
                  raise_application_error(-20056,
                                          G$_NLS.GET('SFKEDIT1-0064',
                                                     'SQL',
                                                     'sfkwlat.p_notification_expires') ||
                                          SQLCODE || ' ' || SQLERRM);
              END;
            END IF;

          END IF;
        ELSE
          dbms_output.put_line('output39');
          IF NVL(sftregs_rec.sftregs_error_flag, 'N') NOT IN
             ('F', 'W', 'L') THEN
            sftregs_rec.sftregs_rmsg_cde := '';
            sftregs_rec.sftregs_message  := '';
          END IF;
          IF sftregs_rec.sftregs_rec_stat = 'N' THEN
            /* new registration */
            IF system_in <> 'S' THEN
              sftregs_rec.sftregs_add_date := SYSDATE;
            END IF;
            sftregs_rec.sftregs_activity_date := SYSDATE;
            sftregs_rec.sftregs_user          := USER;
            IF NOT (stvrsts_rec.stvrsts_incl_sect_enrl = 'Y' OR
                stvrsts_rec.stvrsts_wait_ind = 'Y') THEN
              sftregs_rec.sftregs_reserved_key := '';
            END IF;
            BEGIN
              sfkmods.p_insert_sfrstcr(sftregs_rec, '', reg_oneup, '');
            EXCEPTION
              WHEN OTHERS THEN
                gokfgac.p_turn_fgac_on;
                raise_application_error(-20054,
                                        G$_NLS.GET('SFKEDIT1-0065',
                                                   'SQL',
                                                   'sfkmods.p_insert_sfrstcr') ||
                                        SQLCODE || ' ' || SQLERRM);
            END;
            begin
              select rowid
                into P_ROWID_STCR
                from sfrstcr
               where sfrstcr_pidm = global_pidm
                 and sfrstcr_term_code = gv_term_in
                 and sfrstcr_crn = gv_crn_in;
            exception
              when others then
                P_ROWID_STCR := null;
            end;
            IF sfkwlat.f_wl_automation_active(p_term => sftregs_rec.sftregs_term_code,
                                              p_crn  => sftregs_rec.sftregs_crn) = 'Y' THEN
              BEGIN
                sfkwlat.p_notification_expires(p_term => sftregs_rec.sftregs_term_code,
                                               p_crn  => sftregs_rec.sftregs_crn);

              EXCEPTION
                WHEN OTHERS THEN
                  raise_application_error(-20056,
                                          G$_NLS.GET('SFKEDIT1-0066',
                                                     'SQL',
                                                     'sfkwlat.p_notification_expires') ||
                                          SQLCODE || ' ' || SQLERRM);
              END;
            END IF;

            IF lv_control_term_exists = 'Y' AND
               stvrsts_rec.stvrsts_wait_ind = 'Y' THEN
              IF sfkwlat.f_student_is_notified(sftregs_rec.sftregs_term_code,
                                               sftregs_rec.sftregs_crn,
                                               sftregs_rec.sftregs_pidm) = 'N' THEN
                IF old_wl_priority IS NULL THEN
                  BEGIN
                    sfkwlat.p_waitlist_assign_prio(p_term => sftregs_rec.sftregs_term_code,
                                                   p_crn  => sftregs_rec.sftregs_crn,
                                                   p_pidm => sftregs_rec.sftregs_pidm);
                  EXCEPTION
                    WHEN OTHERS THEN
                      gokfgac.p_turn_fgac_on;
                      raise_application_error(-20055,
                                              G$_NLS.GET('SFKEDIT1-0067',
                                                         'SQL',
                                                         'sfkwlat.p_waitlist_assign_prio') ||
                                              SQLCODE || ' ' || SQLERRM);
                  END;
                END IF;
              ELSE
                UPDATE SFRSTCR
                   SET SFRSTCR_WL_PRIORITY      = 0,
                       SFRSTCR_WL_PRIORITY_ORIG = 'S',
                       SFRSTCR_DATA_ORIGIN      = gb_common.data_origin,
                       SFRSTCR_ACTIVITY_DATE    = SYSDATE,
                       SFRSTCR_USER             = gb_common.F_SCT_USER
                 WHERE SFRSTCR_TERM_CODE = sftregs_rec.sftregs_term_code
                   AND SFRSTCR_CRN = sftregs_rec.sftregs_crn
                   AND SFRSTCR_PIDM = sftregs_rec.sftregs_pidm;
              END IF;
            ELSIF lv_control_term_exists = 'N' AND
                  stvrsts_rec.stvrsts_wait_ind = 'Y' THEN
              <<update_sfrstcr>>
              UPDATE SFRSTCR
                 SET SFRSTCR_WL_PRIORITY      = 1,
                     SFRSTCR_WL_PRIORITY_ORIG = 'S',
                     SFRSTCR_DATA_ORIGIN      = gb_common.data_origin,
                     SFRSTCR_ACTIVITY_DATE    = SYSDATE,
                     SFRSTCR_USER             = gb_common.F_SCT_USER
               WHERE SFRSTCR_TERM_CODE = sftregs_rec.sftregs_term_code
                 AND SFRSTCR_CRN = sftregs_rec.sftregs_crn
                 AND SFRSTCR_PIDM = sftregs_rec.sftregs_pidm;
            END IF;
          ELSE
            /* updated registration */ /*                                                                            */
            /* Check to see if RSTS_CODE changed.                                         */
            /*                                                                            */
            IF sfrstcr_rec.sfrstcr_rsts_code <>
               sftregs_rec.sftregs_rsts_code THEN
              /*                                                                            */
              /* If RSTS code changed, and either old or new is Withdrawal code, update     */
              /* SFRSTCR_ASSESS_ACTIVITY_DATE.                                              */
              /*                                                                            */
              IF NOT (old_withdraw_ind = 'N' AND
                  stvrsts_rec.stvrsts_withdraw_ind = 'N') THEN
                sfrstcr_rec.sfrstcr_assess_activity_date := SYSDATE;
              END IF;
              /*                                                                            */
              /* ELSE the RSTS_CODE has not changed.  Update SFRSTCR_ASSESS_ACTIVITY_DATE   */
              /* if the RSTS_CODE is a withdrawal and the RSTS_DATE has changed.  This      */
              /* would be the case where operator wants to invoke a different refund period */
              /* by changing KEY-BLOCK date on SFAREGS and retyping the same RSTS code.     */
              /*                                                                            */
            ELSE
              IF (stvrsts_rec.stvrsts_withdraw_ind = 'Y' AND
                 TRUNC(sfrstcr_rec.sfrstcr_rsts_date) <>
                 TRUNC(sftregs_rec.sftregs_rsts_date)) THEN
                sfrstcr_rec.sfrstcr_assess_activity_date := SYSDATE;
              END IF; /* IF stvrsts_withdraw_ind = 'Y'... */
            END IF; /* IF sfrstcr_rsts_code <> sftregs_rsts_code */

            sfrstcr_rec.sfrstcr_ptrm_code         := sftregs_rec.sftregs_ptrm_code;
            sfrstcr_rec.sfrstcr_activity_date     := SYSDATE;
            sfrstcr_rec.sfrstcr_user              := USER;
            sfrstcr_rec.sfrstcr_rsts_code         := sftregs_rec.sftregs_rsts_code;
            sfrstcr_rec.sfrstcr_rsts_date         := sftregs_rec.sftregs_rsts_date;
            sfrstcr_rec.sfrstcr_bill_hr           := sftregs_rec.sftregs_bill_hr;
            sfrstcr_rec.sfrstcr_waiv_hr           := sftregs_rec.sftregs_waiv_hr;
            sfrstcr_rec.sfrstcr_credit_hr         := sftregs_rec.sftregs_credit_hr;
            sfrstcr_rec.sfrstcr_error_flag        := sftregs_rec.sftregs_error_flag;
            sfrstcr_rec.sfrstcr_rmsg_cde          := sftregs_rec.sftregs_rmsg_cde;
            sfrstcr_rec.sfrstcr_message           := sftregs_rec.sftregs_message;
            sfrstcr_rec.sfrstcr_credit_hr_hold    := sftregs_rec.sftregs_credit_hr_hold;
            sfrstcr_rec.sfrstcr_bill_hr_hold      := sftregs_rec.sftregs_bill_hr_hold;
            sfrstcr_rec.sfrstcr_appr_received_ind := sftregs_rec.sftregs_appr_received_ind;
            sfrstcr_rec.sfrstcr_dupl_over         := sftregs_rec.sftregs_dupl_over;
            sfrstcr_rec.sfrstcr_link_over         := sftregs_rec.sftregs_link_over;
            sfrstcr_rec.sfrstcr_corq_over         := sftregs_rec.sftregs_corq_over;
            sfrstcr_rec.sfrstcr_preq_over         := sftregs_rec.sftregs_preq_over;
            sfrstcr_rec.sfrstcr_time_over         := sftregs_rec.sftregs_time_over;
            sfrstcr_rec.sfrstcr_capc_over         := sftregs_rec.sftregs_capc_over;
            sfrstcr_rec.sfrstcr_levl_over         := sftregs_rec.sftregs_levl_over;
            sfrstcr_rec.sfrstcr_coll_over         := sftregs_rec.sftregs_coll_over;
            sfrstcr_rec.sfrstcr_majr_over         := sftregs_rec.sftregs_majr_over;
            sfrstcr_rec.sfrstcr_clas_over         := sftregs_rec.sftregs_clas_over;
            sfrstcr_rec.sfrstcr_appr_over         := sftregs_rec.sftregs_appr_over;
            sfrstcr_rec.sfrstcr_rept_over         := sftregs_rec.sftregs_rept_over;
            sfrstcr_rec.sfrstcr_rpth_over         := sftregs_rec.sftregs_rpth_over;
            sfrstcr_rec.sfrstcr_test_over         := sftregs_rec.sftregs_test_over;
            sfrstcr_rec.sfrstcr_camp_over         := sftregs_rec.sftregs_camp_over;
            sfrstcr_rec.sfrstcr_degc_over         := sftregs_rec.sftregs_degc_over;
            sfrstcr_rec.sfrstcr_prog_over         := sftregs_rec.sftregs_prog_over;
            sfrstcr_rec.sfrstcr_dept_over         := sftregs_rec.sftregs_dept_over;
            sfrstcr_rec.sfrstcr_atts_over         := sftregs_rec.sftregs_atts_over;
            sfrstcr_rec.sfrstcr_chrt_over         := sftregs_rec.sftregs_chrt_over;
            sfrstcr_rec.sfrstcr_mexc_over         := sftregs_rec.sftregs_mexc_over;
            sfrstcr_rec.sfrstcr_grde_code         := sftregs_rec.sftregs_grde_code;
            sfrstcr_rec.sfrstcr_grde_date         := sftregs_rec.sftregs_grde_date;
            sfrstcr_rec.sfrstcr_levl_code         := sftregs_rec.sftregs_levl_code;
            sfrstcr_rec.sfrstcr_camp_code         := sftregs_rec.sftregs_camp_code;
            sfrstcr_rec.sfrstcr_gmod_code         := sftregs_rec.sftregs_gmod_code;
            sfrstcr_rec.sfrstcr_stsp_key_sequence := sftregs_rec.sftregs_stsp_key_sequence;
            IF (stvrsts_rec.stvrsts_incl_sect_enrl = 'Y' OR
               stvrsts_rec.stvrsts_wait_ind = 'Y') THEN
              sfrstcr_rec.sfrstcr_reserved_key := sftregs_rec.sftregs_reserved_key;
            ELSE
              sfrstcr_rec.sfrstcr_reserved_key := '';
            END IF;
            BEGIN
              sfkmods.p_update_sfrstcr(sfrstcr_rec);
            EXCEPTION
              WHEN OTHERS THEN
                gokfgac.p_turn_fgac_on;
                raise_application_error(-20056,
                                        G$_NLS.GET('SFKEDIT1-0068',
                                                   'SQL',
                                                   'sfkmods.p_update_sfrstcr') ||
                                        SQLCODE || ' ' || SQLERRM);
            END;
            --

            IF sfkwlat.f_wl_automation_active(p_term => sftregs_rec.sftregs_term_code,
                                              p_crn  => sftregs_rec.sftregs_crn) = 'Y' THEN
              BEGIN
                sfkwlat.p_notification_expires(p_term => sftregs_rec.sftregs_term_code,
                                               p_crn  => sftregs_rec.sftregs_crn);

              EXCEPTION
                WHEN OTHERS THEN
                  raise_application_error(-20056,
                                          G$_NLS.GET('SFKEDIT1-0069',
                                                     'SQL',
                                                     'sfkwlat.p_notification_expires') ||
                                          SQLCODE || ' ' || SQLERRM);
              END;
            END IF;

            IF lv_control_term_exists = 'Y' AND
               stvrsts_rec.stvrsts_wait_ind = 'Y' THEN
              IF sfkwlat.f_student_is_notified(sftregs_rec.sftregs_term_code,
                                               sftregs_rec.sftregs_crn,
                                               sftregs_rec.sftregs_pidm) = 'N' THEN
                IF old_wl_priority IS NULL THEN
                  BEGIN
                    sfkwlat.p_waitlist_assign_prio(p_term => sftregs_rec.sftregs_term_code,
                                                   p_crn  => sftregs_rec.sftregs_crn,
                                                   p_pidm => sftregs_rec.sftregs_pidm);
                  EXCEPTION
                    WHEN OTHERS THEN
                      gokfgac.p_turn_fgac_on;
                      raise_application_error(-20057,
                                              G$_NLS.GET('SFKEDIT1-0070',
                                                         'SQL',
                                                         'sfkwlat.p_waitlist_assign_prio') ||
                                              SQLCODE || ' ' || SQLERRM);
                  END;
                END IF;
              ELSE
                UPDATE SFRSTCR
                   SET SFRSTCR_WL_PRIORITY      = 0,
                       SFRSTCR_WL_PRIORITY_ORIG = 'S',
                       SFRSTCR_DATA_ORIGIN      = gb_common.data_origin,
                       SFRSTCR_ACTIVITY_DATE    = SYSDATE,
                       SFRSTCR_USER             = gb_common.F_SCT_USER
                 WHERE SFRSTCR_TERM_CODE = sftregs_rec.sftregs_term_code
                   AND SFRSTCR_CRN = sftregs_rec.sftregs_crn
                   AND SFRSTCR_PIDM = sftregs_rec.sftregs_pidm;
              END IF;
            ELSIF lv_control_term_exists = 'N' AND
                  stvrsts_rec.stvrsts_wait_ind = 'Y' THEN
              <<update_sfrstcr>>
              UPDATE SFRSTCR
                 SET SFRSTCR_WL_PRIORITY      = 1,
                     SFRSTCR_WL_PRIORITY_ORIG = 'S',
                     SFRSTCR_DATA_ORIGIN      = gb_common.data_origin,
                     SFRSTCR_ACTIVITY_DATE    = SYSDATE,
                     SFRSTCR_USER             = gb_common.F_SCT_USER
               WHERE SFRSTCR_TERM_CODE = sftregs_rec.sftregs_term_code
                 AND SFRSTCR_CRN = sftregs_rec.sftregs_crn
                 AND SFRSTCR_PIDM = sftregs_rec.sftregs_pidm;
            END IF;
          END IF; /* sftregs_rec_stat = 'N' */
        END IF; /* sftregs_remove_ind = 'Y' */
        IF NOT (NVL(sftregs_rec.sftregs_rec_stat, 'N') = 'N' AND
            NVL(sftregs_rec.sftregs_remove_ind, 'N') = 'Y') THEN
          dbms_output.put_line('output40');
          --
          --  Reset old_incl_enrl, because it may have been wiped out by
          --  call to sskfunc.p_upd_section_hours, in which it is an IN/OUT parm.
          --
          old_incl_enrl := hold_old_incl_enrl;
          BEGIN
            dbms_output.put_line('output41');
            sfkfunc.p_process_sfrareg(sftregs_rec,
                                      eff_ext_num,
                                      old_withdraw_ind,
                                      stvrsts_rec.stvrsts_withdraw_ind,
                                      old_incl_enrl,
                                      stvrsts_rec.stvrsts_incl_sect_enrl);
          EXCEPTION
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20058,
                                      G$_NLS.GET('SFKEDIT1-0071',
                                                 'SQL',
                                                 'sfkfunc.p_process_sfrareg') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
        END IF;
      END IF; /*  sftregs_rec_stat <> 'Q' */
    END LOOP;
    --
    CLOSE sftregs_row_c;
    --
    IF error_flag_out = 'N' THEN
      IF NVL(tmst_calc_ind_in, 'N') = 'Y' THEN
        BEGIN
          sfkfunc.p_calc_tmst(tmst_code,
                              tmst_desc,
                              styp_code_in,
                              term_in,
                              pidm_in);
        EXCEPTION
          WHEN OTHERS THEN
            gokfgac.p_turn_fgac_on;
            raise_application_error(-20059,
                                    G$_NLS.GET('SFKEDIT1-0072',
                                               'SQL',
                                               'sfkfunc.p_calc_tmst') ||
                                    SQLCODE || ' ' || SQLERRM);
        END;
        IF tmst_code IS NOT NULL THEN
          IF NVL(tmst_code_in, '*') <> tmst_code THEN
            IF NVL(tmst_maint_ind_in, 'S') = 'S' THEN
              BEGIN
                sfkmods.p_update_sfbetrm_tmst_code(tmst_code,
                                                   term_in,
                                                   pidm_in);
              EXCEPTION
                WHEN OTHERS THEN
                  gokfgac.p_turn_fgac_on;
                  raise_application_error(-20060,
                                          G$_NLS.GET('SFKEDIT1-0073',
                                                     'SQL',
                                                     'sfkmods.p_update_sfbetrm_tmst_code') ||
                                          SQLCODE || ' ' || SQLERRM);
              END;
              BEGIN
                sfkmods.p_insert_sfrthst(tmst_code, term_in, pidm_in);
              EXCEPTION
                WHEN OTHERS THEN
                  gokfgac.p_turn_fgac_on;
                  raise_application_error(-20061,
                                          G$_NLS.GET('SFKEDIT1-0074',
                                                     'SQL',
                                                     'sfkmods.p_insert_sfrthst') ||
                                          SQLCODE || ' ' || SQLERRM);
              END;
              tmst_flag_out := 'S';
            ELSE
              tmst_flag_out := 'M';
            END IF;
          END IF;
        ELSE
          tmst_code := '99';
          BEGIN
            sfkmods.p_update_sfbetrm_tmst_code(tmst_code, term_in, pidm_in);
          EXCEPTION
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20062,
                                      G$_NLS.GET('SFKEDIT1-0075',
                                                 'SQL',
                                                 'sfkmods.p_update_sfbetrm_tmst_code') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
          --
          BEGIN
            sfkmods.p_insert_sfrthst(tmst_code, term_in, pidm_in);
          EXCEPTION
            WHEN OTHERS THEN
              gokfgac.p_turn_fgac_on;
              raise_application_error(-20063,
                                      G$_NLS.GET('SFKEDIT1-0076',
                                                 'SQL',
                                                 'sfkmods.p_insert_sfrthst') ||
                                      SQLCODE || ' ' || SQLERRM);
          END;
          --
          tmst_flag_out := '9';
        END IF;
      END IF; /* tmst_calc_ind_in  = 'Y' */
      --
      UPDATE sftregs
         SET sftregs_rec_stat        = 'Q',
             sftregs_hold_rsts_code  = sftregs_rsts_code,
             sftregs_hold_rsts_type  = sftregs_vr_status_type,
             sftregs_hold_credits    = sftregs_credit_hr,
             sftregs_hold_grde_code  = sftregs_grde_code,
             sftregs_hold_gmod_code  = sftregs_gmod_code,
             sftregs_hold_levl_code  = sftregs_levl_code,
             sftregs_hold_rmsg_cde   = sftregs_rmsg_cde,
             sftregs_hold_message    = sftregs_message,
             sftregs_hold_error_flag = sftregs_error_flag,
             sftregs_error_link      = NULL
       WHERE NVL(sftregs_remove_ind, 'N') <> 'Y'
         AND sftregs_pidm = pidm_in
         AND sftregs_term_code = term_in;
      gb_common.p_commit;
    END IF;
    <<end_p_update_regs>>
    gokfgac.p_turn_fgac_on;

    BEGIN
      sokccur.p_delete_tabs;
    EXCEPTION
      WHEN OTHERS THEN
        gokfgac.p_turn_fgac_on;
        raise_application_error(-20064,
                                G$_NLS.GET('SFKEDIT1-0077',
                                           'SQL',
                                           'sokccur.p_delete_tabs') ||
                                SQLCODE || ' ' || SQLERRM);
    END;
    --
  EXCEPTION
    WHEN OTHERS THEN

      gokfgac.p_turn_fgac_on;
      raise_application_error(-20065,
                              G$_NLS.GET('SFKEDIT1-0078',
                                         'SQL',
                                         'sfkedit.p_update_regs') ||
                              SQLCODE || ' ' || SQLERRM);
  END p_update_regs;

  FUNCTION f_readmit_required(term_in            IN stvterm.stvterm_code%TYPE,
                              pidm_in            IN spriden.spriden_pidm%TYPE,
                              term_code_admit_in IN sorlcur.sorlcur_term_code_admit%TYPE,
                              readm_req_in       IN sobterm.sobterm_readm_req%TYPE,
                              multi_term_in      IN BOOLEAN DEFAULT FALSE)
    RETURN BOOLEAN IS
    last_term_attended stvterm.stvterm_code%TYPE;

    CURSOR last_term_attended_c(p_term               stvterm.stvterm_code%TYPE,
                                p_pidm               spriden.spriden_pidm%TYPE,
                                p_last_term_attended stvterm.stvterm_code%TYPE) IS
      SELECT GREATEST(NVL(MAX(sfbetrm_term_code), '000000'),
                      NVL(p_last_term_attended, '000000'))
        FROM sfbetrm, stvests
       WHERE sfbetrm_term_code < p_term
         AND sfbetrm_pidm = p_pidm
         AND sfbetrm_ests_code = stvests_code
         AND stvests_eff_headcount = 'Y';

  BEGIN
    IF (term_code_admit_in <> term_in OR term_code_admit_in IS NULL) AND
       readm_req_in IS NOT NULL THEN

      FOR shrttrm IN shklibs.shrttrmc(pidm_in, term_in) LOOP
        last_term_attended := shrttrm.shrttrm_term_code;
      END LOOP;

      OPEN last_term_attended_c(term_in, pidm_in, last_term_attended);
      FETCH last_term_attended_c
        INTO last_term_attended;
      CLOSE last_term_attended_c;

      IF NVL(last_term_attended, '0') < readm_req_in AND NOT multi_term_in THEN
        RETURN TRUE;
      END IF;
    END IF;

    RETURN FALSE;

  END f_readmit_required;

  --This Procedure will take the courses from the cart for the student id for the term provided
  --and tries to register all those courses.
  --If there are no courses in the cart , then Appropriate message will be displayed.
  procedure pz_register_courses_in_cart(p_student_id in spriden.spriden_id%type,
                                        p_term_in    in stvterm.stvterm_code%type,
                                        p_response   out varchar2) is
    v_error                varchar2(400);
    mobedu_row             mobedu_cart%rowtype;
    v_check                number := 0;
    term_in                OWA_UTIL.ident_arr;
    drop_problems_in_out   sfkcurs.drop_problems_rec_tabtype;
    drop_failures_in_out   sfkcurs.drop_problems_rec_tabtype;
    sgbstdn_row            sgbstdn%rowtype;
    sfbetrm_rec            sfbetrm%rowtype;
    error_rec_out          sftregs%rowtype;
    error_flag_out         varchar2(40);
    tmst_flag_out          varchar2(40);
    etrm_done_in_out       BOOLEAN := FALSE;
    p_pidm                 number;
    capp_tech_error_in_out varchar2(4);
    P_ROWID_OUT            rowid;
    v_cart_count           number := 0;
    fa_return_status       number;
    save_act_date          VARCHAR2(200);
    sobterm_row            SOBTERM%ROWTYPE;
    --get the courses to register from cart
    cursor c_register_these_courses is
      select *
        from mobedu_cart
       where student_id = p_student_id
         and term = p_term_in
         and processed_ind = 'N';
    --get v_rsts_code here
    cursor c_get_rsts(cv_crn in varchar2, cv_term in varchar2) is
      select decode(ssbsect_seats_avail,
                    0,
                    decode(ssbsect_wait_avail, 0, 'RW', 'WL'),
                    'RW') rsts_code
        from ssbsect
       where ssbsect_crn = cv_crn
         and ssbsect_term_code = cv_term;

    cursor c_get_mobedu_cart(cv_student_id in varchar2,
                             cv_term_code  in varchar2) is
      select *
        from mobedu_cart
       where student_id = cv_student_id
         and term = cv_term_code
         and processed_ind = 'Q';

    cursor c_get_regs_entry(cv_pidm in number,
                            cv_term in varchar2,
                            cv_crn  in varchar2) is
      select *
        from sftregs
       where sftregs_term_code = cv_term
         and sftregs_crn = cv_crn
         and sftregs_pidm = cv_pidm
         and sftregs_error_flag is not null
         and sftregs_message is not null;
  begin
    p_error_msg := '';
    --if there are any previous errors for the student , term combination
    --(previously failed) , these will be corrected(making eligible to register)

    pz_rect5_prev_errs_nd_ad_2_crt(p_student_id, p_term_in, v_error);

    --pz_check_cart_empty(p_student_id, p_term_in, p_response);
    /*      f_check_cart_empty(); */
    --If the cart is empty , then do nothing by leaving the end user
    --appropriate message

    for c_register_this_course in c_register_these_courses loop
      v_cart_count := c_register_these_courses%rowcount;
    end loop;
    if v_cart_count = 0 then
      p_response := 'Empty Cart!';
      goto quit_from_program;
    end if;
    me_valid.pz_registration_checks(p_student_id => p_student_id,
                                  p_term => p_term_in,
                                  p_error_msg => p_response);
    IF p_response IS NOT NULL THEN
      GOTO quit_from_program;
    END IF;
    /*  p_clean_unnecessary_entries()*/
    --This procedure will remove the previous trial entries in registration tables
    p_pidm := baninst1.gb_common.f_get_pidm(p_student_id);

    --Refresh SFTREGS entries.
    sfkmods.p_delete_sftregs_by_pidm_term(p_pidm, p_term_in);
    sfkmods.p_insert_sftregs_from_stcr(p_pidm, p_term_in, SYSDATE);
    begin
      delete from sfrstcr
       where sfrstcr_pidm = p_pidm
         and sfrstcr_term_code = p_term_in
         and sfrstcr_error_flag = 'F'
         and sfrstcr_message is not null
         and sfrstcr_rsts_code = 'DW';
      gb_common.p_commit;
    end;

    /*  p_add_to_sftregs()*/
    -- Stage 1 : Temporary Table insertion
    --If any errors/exception arised , then
    --do commit(to identify upto where the flow continued)
    --(since we are cleaning everytime , it wont impact)
    --and throw the error message to mobedu_cart table
    --and do nothing from there

    begin
      for r_register_this_course in c_register_these_courses loop
        /*for r_get_rsts in c_get_rsts(r_register_this_course.crn,
        r_register_this_course.term) loop*/
        begin
          P_ERROR_MSG := '';
          P_ROWID_OUT := '';
          dbms_output.put_line('CRN :' || r_register_this_course.crn);
          dbms_output.put_line('ID : ' ||
                               r_register_this_course.student_id);
          dbms_output.put_line('RSTS CODe : ' ||
                               r_register_this_course.rsts_code);
          dbms_output.put_line('TERM : ' || r_register_this_course.term);

          pz_stu_regs_commit(r_register_this_course.crn,
                             r_register_this_course.student_id,
                             r_register_this_course.term,
                             r_register_this_course.rsts_code,
                             P_ERROR_MSG,
                             P_ROWID_OUT);

          gb_common.p_commit;

        exception
          when others then
            dbms_output.put_line('Exception raised while creating sftregs : ' ||
                                 sqlerrm);
            P_ERROR_MSG := 'Failed to register the student due to ' ||
                           sqlerrm;
            DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
            DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

            GB_COMMON.P_ROLLBACK;

        end;
        --update cart status here
        update mobedu_cart
           set status        = decode(P_ERROR_MSG,
                                      null,
                                      decode(P_ROWID_OUT,
                                             null,
                                             'FAILURE',
                                             'SUCCESS'),
                                      P_ERROR_MSG),
               error_flag    = decode(P_ERROR_MSG, null, 'N', 'Y'),
               processed_ind = decode(P_ERROR_MSG, null, 'Q', 'Y') /*,
                         rsts_code     = r_get_rsts.rsts_code*/
         where crn = r_register_this_course.crn
           and term = r_register_this_course.term
           and student_id = r_register_this_course.student_id;

        v_cart_count := c_register_these_courses%rowcount;

        gb_common.p_commit;

      /* end loop;*/
      end loop;

      dbms_output.put_line('Cart count:' || v_cart_count);
    end;

    /*   pz_do_validations()*/
    --Do all Validations
    --If any error/exception arised while doing validations ,
    --then do commit() and throw the error to mobedu_cart and
    --from there do nothing
    begin
      DBMS_OUTPUT.PUT_LINE('Validation phase started:');
      dbms_output.put_line('PIDM:' || p_pidm);
      term_in(1) := gv_term_in;
      dbms_output.put_line('TERM:' || term_in(1));
      p_error_msg := '';

      dbms_output.put_line('p_pidm in add_course_pkg_corq : ' || p_pidm);
      dbms_output.put_line('p_term_in in add_course_pkg_corq : ' ||
                           p_term_in);
      bwcklibs.p_initvalue(p_pidm, p_term_in, '', '', '', '');

      --if error/exception occurred/arised during
      --temporary table insertion , then validations and commit phase
      --should not be executed.
      if P_ERROR_MSG is not null then
        goto quit_from_program;
      end if;

      pz_do_validations(term_in,
                        p_pidm,
                        etrm_done_in_out,
                        capp_tech_error_in_out,
                        drop_problems_in_out,
                        drop_failures_in_out,
                        p_error_msg);
      DBMS_OUTPUT.PUT_LINE('Validation phase completed:');
      gb_common.p_commit;
    exception
      when others then
        p_error_msg := 'Registration failed while validating. Error :' ||
                       sqlerrm;
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        GB_COMMON.P_ROLLBACK;

    end;
    IF P_ERROR_MSG IS NOT NULL THEN
      BEGIN
        update mobedu_cart
           set status = P_ERROR_MSG, error_flag = 'Y', processed_ind = 'Y'
         where term = p_term_in
           and student_id = p_student_id;

        gb_common.p_commit;

        P_ERROR_MSG := '';
        p_response  := 'Registration completed.';
        goto quit_from_program;
      END;
    END IF;

    /*  pz_move_to_sfrstcr() */
    --If any error/exception arised , then
    --do commit and throw the error to mobedu_cart
    --and then do nothing from there on.
    begin

      DBMS_OUTPUT.PUT_LINE('Commit phase started:');

      pz_final_commit(p_pidm,
                      gv_term_in,
                      regs_date,
                      '01',
                      sgbstdn_row.sgbstdn_styp_code,
                      'F',
                      'Y',
                      sfbetrm_rec.sfbetrm_tmst_maint_ind,
                      sfbetrm_rec.sfbetrm_tmst_code,
                      null,
                      'WA',
                      error_rec_out,
                      error_flag_out,
                      tmst_flag_out);

      DBMS_OUTPUT.PUT_LINE('Commit phase completed:');
      --      bwcklibs.p_initvalue('', '', '', '', '', '');
      gb_common.p_commit;

    exception
      when others then
        p_error_msg := 'Registration Failed while commit.Error :' ||
                       sqlerrm;
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        GB_COMMON.P_ROLLBACK;

    end;

    for r_process_cart_entry in c_get_mobedu_cart(p_student_id, p_term_in) loop
      for r_get_regs_entry in c_get_regs_entry(p_pidm,
                                               r_process_cart_entry.term,
                                               r_process_cart_entry.crn) loop

        begin
          update mobedu_cart
             set error_flag    = decode(r_get_regs_entry.sftregs_error_flag,
                                        null,
                                        'N','W','W',
                                        'Y'),
                 status        = DECODE(r_get_regs_entry.sftregs_message,
                                        NULL,
                                        'SUCCESS',
                                        r_get_regs_entry.sftregs_message),
                 processed_ind = 'Y'
           where student_id = r_process_cart_entry.student_id
             and term = r_process_cart_entry.term
             and crn = r_process_cart_entry.crn;

          gb_common.p_commit;

        exception
          when others then
            GB_COMMON.P_ROLLBACK;
        end;

      end loop;

      select count(1)
        into v_check
        from sfrstcr A
       where A.SFRSTCR_TERM_CODE = r_process_cart_entry.term
         and A.SFRSTCR_PIDM = p_pidm
         and A.SFRSTCR_CRN = r_process_cart_entry.crn;

      if (v_check > 0) then

        update mobedu_cart
           set status = 'SUCCESS', processed_ind = 'Y', error_flag = 'N'
         where student_id = r_process_cart_entry.student_id
           and term = r_process_cart_entry.term
           and crn = r_process_cart_entry.crn
           and processed_ind = 'Q';
        BEGIN
          bwcklibs.p_getsobterm(p_term_in, sobterm_row);
          IF sobterm_row.sobterm_fee_assess_vr = 'Y' AND
             sobterm_row.sobterm_fee_assessment = 'Y' THEN
            SFKFEES.p_processfeeassessment(p_term_in,
                                           p_pidm,
                                           SYSDATE,
                                           SYSDATE,
                                           'R',
                                           'Y',
                                           'BWCKREGS',
                                           'Y',
                                           save_act_date,
                                           'N',
                                           fa_return_status);

            gb_common.p_commit;
          END IF;
        END;
      else

        update mobedu_cart
           set status        = 'Fatal Error in commit phase',
               processed_ind = 'Y',
               error_flag    = 'Y'
         where student_id = r_process_cart_entry.student_id
           and term = r_process_cart_entry.term
           and crn = r_process_cart_entry.crn
           and processed_ind = 'Q';

        gb_common.p_commit;

      end if;

    end loop;

    /*DELETE from mobedu_cart
    where error_flag <> 'Y'
      and student_id = p_student_id
      and term = p_term_in;*/

    /* pz_clean_registration_tables() */
    --for safety , we are cleaning registration tables
    --as this package processing should not impact banner processing
    --Refresh SFTREGS entries.
    sfkmods.p_delete_sftregs_by_pidm_term(p_pidm, p_term_in);
    sfkmods.p_insert_sftregs_from_stcr(p_pidm, p_term_in, SYSDATE);

    gb_common.p_commit;

    p_response := 'Registration completed.';

    <<quit_from_program>>
    null;

  exception
    when others then
      DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
      DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      rollback;
      p_response := sqlerrm;
  end;

  procedure pz_do_validations(term_in                IN OWA_UTIL.ident_arr,
                              pidm_in                IN spriden.spriden_pidm%TYPE,
                              etrm_done_in_out       IN OUT BOOLEAN,
                              capp_tech_error_in_out IN OUT VARCHAR2,
                              drop_problems_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              drop_failures_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              error_msg              out varchar2) is
  begin

    dbms_output.put_line('me_bwckcoms.p_group_edits started.');

    me_bwckcoms.p_group_edits(term_in,
                              pidm_in,
                              etrm_done_in_out,
                              capp_tech_error_in_out,
                              drop_problems_in_out,
                              drop_failures_in_out);

    dbms_output.put_line('me_bwckcoms.p_group_edits completed.');

    commit;

  exception
    when others then

      error_msg := SQLERRM;
      DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
      DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);

  end pz_do_validations;

  procedure pz_final_commit(pidm_in            IN sftregs.sftregs_pidm%TYPE,
                            term_in            IN sftregs.sftregs_term_code%TYPE,
                            reg_date_in        IN sftregs.sftregs_add_date%TYPE,
                            clas_code_in       IN sgrclsr.sgrclsr_clas_code%TYPE,
                            styp_code_in       IN sgbstdn.sgbstdn_styp_code%TYPE,
                            capc_severity_in   IN sobterm.sobterm_capc_severity%TYPE,
                            tmst_calc_ind_in   IN sobterm.sobterm_tmst_calc_ind%TYPE,
                            tmst_maint_ind_in  IN sfbetrm.sfbetrm_tmst_maint_ind%TYPE,
                            tmst_code_in       IN sfbetrm.sfbetrm_tmst_code%TYPE,
                            drop_last_class_in IN BOOLEAN DEFAULT TRUE,
                            system_in          IN VARCHAR2,
                            error_rec_out      OUT sftregs%ROWTYPE,
                            error_flag_out     OUT VARCHAR2,
                            tmst_flag_out      OUT VARCHAR2) is
    global_pidm number;
    p_error_msg varchar2(400);
  begin

    /* baninst1.SFKEDIT.p_update_regs(pidm_in            => v_pidm,
    term_in            => p_term_code,
    reg_date_in        => v_add_date,
    clas_code_in       => '01',
    styp_code_in       => v_styp_code,
    capc_severity_in   => 'F',
    tmst_calc_ind_in   => 'Y',
    tmst_maint_ind_in  => v_tmst_ind,
    tmst_code_in       => v_tmst_code,
    drop_last_class_in => null,
    system_in          => 'WA',
    error_rec_out      => v_sftregs_rec,
    error_flag_out     => v_error_out,
    tmst_flag_out      => v_tmst_out);  */

    dbms_output.put_line('PIDM:' || pidm_in);
    dbms_output.put_line('TERM:' || term_in);
    dbms_output.put_line('STYP:' || sgbstdn_row.sgbstdn_styp_code);
    dbms_output.put_line('TMST_MAINT:' || sgbstdn_row.sgbstdn_styp_code);
    dbms_output.put_line('TMST:' || sfbetrm_rec.sfbetrm_tmst_code);

    sfkedit.p_update_regs(pidm_in,
                          term_in,
                          reg_date_in,
                          '01',
                          sgbstdn_row.sgbstdn_styp_code,

                          'F',
                          'Y',
                          sfbetrm_rec.sfbetrm_tmst_maint_ind,
                          sfbetrm_rec.sfbetrm_tmst_code,
                          null,
                          'WA',
                          error_rec_out,
                          error_flag_out,
                          tmst_flag_out);

    add_course_pkg_corq.p_update_regs(pidm_in,
                                      term_in,
                                      reg_date_in,
                                      '01',
                                      sgbstdn_row.sgbstdn_styp_code,
                                      'F',
                                      'Y',
                                      sfbetrm_rec.sfbetrm_tmst_maint_ind,
                                      sfbetrm_rec.sfbetrm_tmst_code,
                                      NULL,
                                      'WA',
                                      error_rec_out,
                                      error_flag_out,
                                      tmst_flag_out,
                                      p_error_msg);

  exception
    when others then
      p_error_msg := 'Failed to register the student.';

      DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
      DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      -- RAISE;

  end;

  procedure pz_remove_from_cart(p_student_id in varchar2,
                                p_crn        in varchar2,
                                p_term       in varchar2,
                                p_error_msg  out varchar2) is
    v_deleted_rows_no number;
  begin
    delete from mobile_apps.mobedu_cart
     where student_id = p_student_id
       and crn = p_crn
       and term = p_term;
    v_deleted_rows_no := sql%rowcount;
    commit;
    if v_deleted_rows_no = 0 then
      p_error_msg := 'There is no course in the cart.';
    end if;
  exception
    when others then
      p_error_msg := 'Failed to remove from the cart.';
  end pz_remove_from_cart;

  PROCEDURE pz_add_to_cart(p_student_id in varchar2,
                           p_crn        in varchar2,
                           p_term       in varchar2,
                           p_rsts_code  in varchar2,
                           p_error_msg  out varchar2) is
    cart_row        mobedu_cart%rowtype;
    v_cart_rowcount number := 0;
    cursor c_get_cart_entry(cv_student_id in varchar2,
                            cv_crn        in varchar2,
                            cv_term       in varchar2) is
      select *
        from MOBILE_APPS.mobedu_cart
       where student_id = cv_student_id
         and crn = cv_crn
         and term = cv_term;
  begin
    open c_get_cart_entry(p_student_id, p_crn, p_term);
    fetch c_get_cart_entry
      into cart_row;
    v_cart_rowcount := c_get_cart_entry%rowcount;
    close c_get_cart_entry;
    if v_cart_rowcount <> 0 then
      p_error_msg := 'The Course you are trying to add is already in the cart.';
      goto quit_from_program;
    end if;
    insert into mobedu_cart
      (student_id, crn, term, status, processed_ind, error_flag, rsts_code)
    values
      (p_student_id, p_crn, p_term, '', 'N', 'N', p_rsts_code);
    commit;
    <<quit_from_program>>
    null;
  exception
    when others then
      p_error_msg := 'Failed to add to cart.';
  end PZ_ADD_TO_CART;

  procedure pz_clear_cart(p_student_id in varchar2,
                          p_term       in varchar2,
                          p_error_msg  out varchar2) is
    mobedu_row mobedu_cart%rowtype;
    v_rowcount number := 0;
    cursor c_get_mobedu_entries(cv_student_id in varchar2,
                                cv_term       in varchar2) is
      select *
        from MOBILE_APPS.mobedu_cart
       where student_id = cv_student_id
         and term = cv_term;
  begin
    open c_get_mobedu_entries(p_student_id, p_term);
    fetch c_get_mobedu_entries
      into mobedu_row;
    v_rowcount := c_get_mobedu_entries%rowcount;
    close c_get_mobedu_entries;
    if v_rowcount = 0 then
      p_error_msg := 'There are no courses in cart to clear.';
      goto quit_from_program;
    end if;
    delete from MOBILE_APPS.mobedu_cart
     where student_id = p_student_id
       and term = p_term;
    commit;
    <<quit_from_program>>
    null;
  exception
    when others then
      rollback;
      p_error_msg := 'Failed to clear the cart';
  end pz_clear_cart;

begin
  p_student_id := null;
  regs_date    := sysdate;
  global_pidm  := null;
  gv_term_in   := null;
  gv_crn_in    := null;
end add_course_pkg_corq;
/

