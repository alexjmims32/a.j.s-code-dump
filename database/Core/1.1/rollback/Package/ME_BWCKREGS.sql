PROMPT CREATE OR REPLACE PACKAGE me_bwckregs
CREATE OR REPLACE PACKAGE me_bwckregs AS
  --
  -- FILE NAME..: bwckregs.sql
  -- RELEASE....: 8.1
  -- OBJECT NAME: BWCKREGS
  -- PRODUCT....: SCOMWEB
  -- USAGE......:
  -- COPYRIGHT..: Copyright (C) SCT Corporation 2002.  All rights reserved.

  -- DESCRIPTION:
  -- DESCRIPTION END
  --  OVERVIEW

  -- Registration Process

  -- Declare externally visible constant,type,cursor,variable and exception.

  /* CURSORs and TYPEs are now in BWSKCURS */

  --  FUNCTIONS AND PROCEDURES (alphabetical order)

  FUNCTION f_deceasedperscheck(sql_err OUT NUMBER, pidm_in NUMBER)
    RETURN BOOLEAN;

  FUNCTION f_getstuclas RETURN stvclas.stvclas_code%TYPE;

  FUNCTION f_stuhld(sql_err      OUT NUMBER,
                    pidm_in      NUMBER DEFAULT NULL,
                    regs_date_in DATE DEFAULT NULL,
                    hold_serv    CHAR DEFAULT NULL,
                    hold_pass    CHAR DEFAULT NULL) RETURN BOOLEAN;

  -- Return TRUE if student have the hold

  FUNCTION f_validacpt(sql_err OUT NUMBER, ar CHAR DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   AR_IND
  --     Student registration acceptance indicator
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validappr(sql_err OUT NUMBER, appr_ind CHAR DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   APPR_IND
  --     Special Approval Indicator
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validbillhr(sql_err OUT NUMBER,
                         bill_hr SFTREGS.SFTREGS_BILL_HR%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   BILL_HR
  --     Billing Hours
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validcredhr(sql_err OUT NUMBER,
                         cred_hr SFTREGS.SFTREGS_CREDIT_HR%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   CRED_HR
  --     Credit Hours
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  PROCEDURE p_getsection(term_in sftregs.sftregs_term_code%TYPE DEFAULT NULL,
                         crn     sftregs.sftregs_crn%TYPE DEFAULT NULL,
                         subj    OUT ssbsect.ssbsect_subj_code%TYPE,
                         crse    OUT ssbsect.ssbsect_crse_numb%TYPE,
                         seq     OUT ssbsect.ssbsect_seq_numb%TYPE);

  -- Input parameters:
  --   CRN
  --      Course Reference Number
  -- Output parameters:
  --   SUBJ
  --      Subject Code
  --   CRSE
  --      Course Number
  --   SEQ
  --     Course Sequence Number

  FUNCTION f_validcrn(sql_err OUT NUMBER,
                      term_in IN sftregs.sftregs_term_code%TYPE,
                      crn     sftregs.sftregs_crn%TYPE DEFAULT NULL,
                      subj    ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse    ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq     ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   CRN
  --      Course Reference Number
  --   SUBJ
  --      Subject Code
  --   CRSE
  --      Course Number
  --   SEQ
  --     Course Sequence Number
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validenrl(sql_err OUT NUMBER,
                       term_in IN sftregs.sftregs_term_code%TYPE,
                       ests    sfbetrm.sfbetrm_ests_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   ESTS
  --    the enrollment status of the given student
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validgmod(sql_err   OUT NUMBER,
                       term_in   sftregs.sftregs_term_code%TYPE,
                       gmod      sftregs.sftregs_gmod_code%TYPE DEFAULT NULL,
                       subj      ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                       crse      ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                       sect_gmod ssbsect.ssbsect_gmod_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   GMOD
  --      Grade Mode
  --   SUBJ
  --      Subject Code
  --   CRSE
  --      Course Number
  --   SEQ
  --     Course Sequence Number
  --   SECT_GMOD
  --     Section Grade Mode
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validlevl(sql_err OUT NUMBER,
                       term_in sftregs.sftregs_term_code%TYPE,
                       levl    sftregs.sftregs_levl_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Input parameters:
  --   LEVL
  --     Level Code
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validrgre(sql_err OUT NUMBER,
                       rgre    stvrgre.stvrgre_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Check Reason Code
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_validrsts(sql_err OUT NUMBER,
                       rsts    stvrsts.stvrsts_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN;

  --  Check RSTS Code
  --  Return value:
  --    TRUE if the given value is valid
  --    FALSE if it is not.

  FUNCTION f_readmit_required(term_in            IN stvterm.stvterm_code%TYPE,
                              pidm_in            IN spriden.spriden_pidm%TYPE,
                              term_code_admit_in IN sorlcur.sorlcur_term_code_admit%TYPE,
                              readm_req_in       IN sobterm.sobterm_readm_req%TYPE,
                              multi_term_in      IN BOOLEAN DEFAULT FALSE)
    RETURN BOOLEAN;

  --  Input parameters:
  --   term_in
  --     term
  --   pidm_in
  --     pidm
  --   term_code_admit_in
  --     term when the student was last admitted
  --   readm_req_in
  --     whether readmit requirement is set to TRUE on SOATERM
  --   multi_term_in
  --     whether this call was made during a multiple term registration session
  --  Return value:
  --    TRUE if the student needs to be re-admitted
  --    FALSE if they do not.

  PROCEDURE p_addcrse(stcr_row      sftregs%ROWTYPE,
                      subj          ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse          ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq           ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                      start_date_in VARCHAR2 DEFAULT NULL,
                      end_date_in   VARCHAR2 DEFAULT NULL);

  -- Adding Course

  PROCEDURE p_allcrsechk(term_in              IN sftregs.sftregs_term_code%TYPE,
                         called_by_in         IN VARCHAR2 DEFAULT 'ADD_DROP',
                         capp_tech_error_out  OUT VARCHAR2,
                         drop_problems_in_out IN OUT sfkcurs.drop_problems_rec_tabtype,
                         drop_failures_in_out IN OUT sfkcurs.drop_problems_rec_tabtype,
                         multi_term_list_in   IN OWA_UTIL.ident_arr);

  -- Checking after all the courses are added or updated to the database

  PROCEDURE p_calchrs(tot_cred          OUT SFTREGS.SFTREGS_CREDIT_HR%TYPE,
                      tot_bill          OUT SFTREGS.SFTREGS_BILL_HR%TYPE,
                      tot_ceu           OUT SFTREGS.SFTREGS_CREDIT_HR%TYPE,
                      min_hour_override IN CHAR DEFAULT NULL,
                      max_hour_override IN CHAR DEFAULT NULL);

  -- Calculate TOTAL HOURS

  PROCEDURE p_capcchk;

  -- Check CAPC

  PROCEDURE p_defstcr(add_ind CHAR DEFAULT NULL);

  -- Defaut values to SFRSTCR

  PROCEDURE p_dropcrse(term stvterm.stvterm_code%TYPE,
                       crn  sftregs.sftregs_crn%TYPE,
                       rsts sftregs.sftregs_rsts_code%TYPE,
                       /* reserved_key   sftregs.sftregs_reserved_key%TYPE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             rec_stat       sftregs.sftregs_rec_stat%TYPE,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             subj           ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             crse           ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             seq            ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             del_ind        VARCHAR2 DEFAULT NULL,*/
                       pidm    spriden.spriden_pidm%TYPE,
                       p_error out varchar2);

  -- Drop Course

  PROCEDURE p_getoverride;

  ---------------------------------------------------------------------------

  PROCEDURE p_regschk(etrm_row      sfbetrm%ROWTYPE,
                      add_ind       VARCHAR2 DEFAULT NULL,
                      multi_term_in BOOLEAN DEFAULT FALSE);

  --  Comment
  --    Validate SFBETRM record
  --  Input parameters:
  --   ETRM_ROW
  --      The record return from cursor SFBETRMC

  ---------------------------------------------------------------------------

  PROCEDURE p_regschk(stcr_row sftregs%ROWTYPE,
                      subj     ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse     ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq      ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                      over     CHAR DEFAULT NULL,
                      add_ind  CHAR DEFAULT NULL);

  --  Comment
  --    Validate SFRSTCR record
  --  Input parameters:
  --   STCR_ROW
  --      The record returned from cursor SFRSTCRC

  ---------------------------------------------------------------------------

  PROCEDURE p_regschk(stdn_row      sgbstdn%ROWTYPE,
                      multi_term_in BOOLEAN DEFAULT FALSE);

  --  Comment
  --    Validate SGBSTDN record for registration
  --   STDN_ROW
  --      The record returned from cursor SGBSTDNC.

  ---------------------------------------------------------------------------

  PROCEDURE p_regsfees;

  -- Process Fee Assessment

  PROCEDURE p_statuschg;

  -- Check if SFBETRM_ESTS_CODE change effect any courses

  PROCEDURE p_updcrse(stcr_row sftregs%ROWTYPE,
                      crn      sftregs.sftregs_crn%TYPE DEFAULT NULL,
                      subj     ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse     ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq      ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL);

  ---------------------------------------------------------------------------
  PROCEDURE p_init_final_update_vars(pidm_in IN sfrracl.sfrracl_pidm%TYPE,
                                     term_in IN sfrracl.sfrracl_term_code%TYPE);

  ---------------------------------------------------------------------------
  PROCEDURE p_final_updates(term_in              IN OWA_UTIL.ident_arr,
                            err_term             IN OWA_UTIL.ident_arr,
                            err_crn              IN OWA_UTIL.ident_arr,
                            err_subj             IN OWA_UTIL.ident_arr,
                            err_crse             IN OWA_UTIL.ident_arr,
                            err_sec              IN OWA_UTIL.ident_arr,
                            err_code             IN OWA_UTIL.ident_arr,
                            err_levl             IN OWA_UTIL.ident_arr,
                            err_cred             IN OWA_UTIL.ident_arr,
                            err_gmod             IN OWA_UTIL.ident_arr,
                            drop_result_label_in IN twgrinfo.twgrinfo_label%TYPE,
                            drop_problems_in     IN sfkcurs.drop_problems_rec_tabtype,
                            drop_failures_in     IN sfkcurs.drop_problems_rec_tabtype);

  ---------------------------------------------------------------------------
  FUNCTION f_registration_access(pidm_in      IN sfrracl.sfrracl_pidm%TYPE,
                                 term_in      IN sfrracl.sfrracl_term_code%TYPE,
                                 access_id_in IN sfrracl.sfrracl_reg_access_ID%TYPE)
    RETURN BOOLEAN;

  ---------------------------------------------------------------------------
  FUNCTION f_finalize_admindrops(pidm_in      IN sfrracl.sfrracl_pidm%TYPE,
                                 term_in      IN sfrracl.sfrracl_term_code%TYPE,
                                 access_id_in IN sfrracl.sfrracl_reg_access_ID%TYPE)
    RETURN BOOLEAN;

END me_bwckregs;




/

