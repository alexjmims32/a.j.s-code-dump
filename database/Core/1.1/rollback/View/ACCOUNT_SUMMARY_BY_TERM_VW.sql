PROMPT CREATE OR REPLACE VIEW account_summary_by_term_vw
CREATE OR REPLACE FORCE VIEW account_summary_by_term_vw (
  term_description,
  term_code,
  category,
  cat_description,
  tbbdetc_type_ind,
  amount,
  balance,
  student_id
) AS
SELECT stvterm_desc term_description,
       tbraccd_term_code term_code,
       ttvdcat_code category,
       ttvdcat_desc cat_description,
       tbbdetc_type_ind,
       SUM(tbraccd_amount) amount,
       SUM(tbraccd_balance) balance,
       spriden_id student_id
  FROM stvterm, tbbdetc, tbraccd, ttvdcat, spriden c
 --WHERE tbraccd_pidm = 40005156 --pidm
   where tbbdetc_detail_code = tbraccd_detail_code
   AND tbbdetc_dcat_code = ttvdcat_code
   AND stvterm_code(+) = tbraccd_term_code
   and tbraccd_pidm = spriden_pidm
   and spriden_change_ind is null
 GROUP BY stvterm_desc,
          tbraccd_term_code,
          tbbdetc_type_ind,
          ttvdcat_code,
          ttvdcat_desc,
          spriden_id
 ORDER BY tbraccd_term_code DESC,
          tbbdetc_type_ind,
          ttvdcat_code,
          ttvdcat_desc
/

