PROMPT CREATE OR REPLACE VIEW person_vw
CREATE OR REPLACE FORCE VIEW person_vw (
  id,
  pidm,
  lst_name,
  first_name,
  middle_name,
  ssn,
  dob,
  gender,
  prefix,
  category,
  published,
  updated,
  campus
) AS
select
a.spriden_id id,
a.spriden_pidm pidm,
a.spriden_last_name lst_name,
a.spriden_first_name first_name,
a.spriden_mi middle_name,
'' ssn,
b.spbpers_birth_date dob,
DECODE(b.spbpers_sex,'M','Male','F','Female','') gender,
b.spbpers_name_prefix prefix,
z_cm_mobile_campus.fz_category(A.SPRIDEN_PIDM) CATEGORY,
A.SPRIDEN_CREATE_DATE published,
A.SPRIDEN_ACTIVITY_DATE updated,
me_reg_utils.fz_get_camp(A.SPRIDEN_PIDM) campus
from spriden a join spbpers b
on a.spriden_pidm=b.spbpers_pidm
and a.spriden_change_ind is null
and a.spriden_entity_ind='P'
/

