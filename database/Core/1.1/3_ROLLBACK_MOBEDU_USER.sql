PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start 2_Rollback.sql"   
PROMPT ====================================================

spool 2_Rollback.log

@@rollback\Types.sql
@@rollback\Type\T_ARRAY_TYPE.sql
@@rollback\Function\FZ_PROCESSES_AFFECT_BY_HOLD.sql
@@rollback\Procedure\PZ_DROP_COURSE.sql
@@rollback\Procedure\PZ_ME_AUTHENTICATION.sql
@@rollback\Package\ADD_COURSE_PKG_CORQ.sql
@@rollback\Package\CART_PKG.sql
@@rollback\Package\DROP_COURSE_PKG.sql
@@rollback\Package\ME_ACCOUNT_OBJECTS.sql
@@rollback\Package\ME_ALT_PIN_PKG.sql
@@rollback\Package\ME_BWCKCOMS.sql
@@rollback\Package\ME_BWCKGENS.sql
@@rollback\Package\ME_BWCKREGS.sql
@@rollback\Package\ME_REG_UTILS.sql
@@rollback\Package\ME_SFKFEES.sql
@@rollback\Package\ME_VALID.sql
@@rollback\Package\PAYMENT_GATEWAY.sql
@@rollback\Package\Z_CM_MOBILE_CAMPUS.sql
@@rollback\View\ACCOUNT_SUMMARY_VIEW.sql
@@rollback\View\ACCT_SUMM_VW.sql
@@rollback\View\CART_DETAILS_VW.sql
@@rollback\View\CHARGE_PAY_DETAIL_VW.sql
@@rollback\View\CONTACT_VW.sql
@@rollback\View\TERMS_TO_REGISTER_VW.sql
@@rollback\View\COURSE_SEARCH_VW.sql
@@rollback\View\CRSE_MEET_VW.sql
@@rollback\View\ENUM_VW.sql
@@rollback\View\FACULTY_FEEDBACK_VW.sql
@@rollback\View\GENERAL_PERSON_VW.sql
@@rollback\View\ME_HOURS_SUMMARY_BY_TERM.sql
@@rollback\View\ME_STUDENT_CRSE_GRDE.sql
@@rollback\View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@rollback\View\MOBEDU_HOLIDAY_VW.sql
@@rollback\View\PERSON_CAMPUS_VW.sql
@@rollback\View\PERSON_VW.sql
@@rollback\View\SPBPERS_VIEW.sql
@@rollback\View\STUDENT_ACCOUNT_DETAIL.sql
@@rollback\View\STU_CURCULAM_INFO.sql
@@rollback\View\STU_HOLD_INFO.sql
@@rollback\View\STU_PROFILE_INFO.sql
@@rollback\View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@rollback\View\SUMMARY_BY_TERM_VW.sql
@@rollback\View\TERMS_TO_BURSAR_VW.sql
@@rollback\View\TERM_COURSE_DETAILS_VW.sql
@@rollback\View\TEST_STU_INFO.sql
@@rollback\View\VW_PREVTERMS_CHARGES.sql
@@rollback\View\VW_TERM_CHARGES.sql
@@rollback\View\VW_TERM_PAYMENTS.sql
@@rollback\PackageBody\ADD_COURSE_PKG_CORQ.sql
@@rollback\PackageBody\CART_PKG.sql
@@rollback\PackageBody\DROP_COURSE_PKG.sql
@@rollback\PackageBody\ME_ACCOUNT_OBJECTS.sql
@@rollback\PackageBody\ME_ALT_PIN_PKG.sql
@@rollback\PackageBody\ME_BWCKCOMS.sql
@@rollback\PackageBody\ME_BWCKGENS.sql
@@rollback\PackageBody\ME_BWCKREGS.sql
@@rollback\PackageBody\ME_REG_UTILS.sql
@@rollback\PackageBody\ME_SFKFEES.sql
@@rollback\PackageBody\ME_VALID.sql
@@rollback\PackageBody\PAYMENT_GATEWAY.sql
@@rollback\PackageBody\Z_CM_MOBILE_CAMPUS.sql

EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
