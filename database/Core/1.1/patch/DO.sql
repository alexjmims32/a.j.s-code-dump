PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start DO.sql"   
PROMPT ====================================================

spool DO.log

@@Types.sql
@@Type\T_ARRAY_TYPE.sql
@@Procedure\PZ_ME_AUTHENTICATION.sql
@@Package\ADD_COURSE_PKG_CORQ.sql
@@Package\CART_PKG.sql
@@Package\DROP_COURSE_PKG.sql
@@Package\ME_ACCOUNT_OBJECTS.sql
@@Package\ME_ALT_PIN_PKG.sql
@@Package\ME_BWCKCOMS.sql
@@Package\ME_BWCKGENS.sql
@@Package\ME_BWCKREGS.sql
@@Package\ME_REG_UTILS.sql
@@Package\ME_VALID.sql
@@Package\ME_WITHDRAWL.sql
@@Package\Z_CM_MOBILE_CAMPUS.sql
@@View\ACCOUNT_SUMMARY_VIEW.sql
@@View\ACCT_SUMM_VW.sql
@@View\CART_DETAILS_VW.sql
@@View\CHARGE_PAY_DETAIL_VW.sql
@@View\CONTACT_VW.sql
@@View\TERMS_TO_REGISTER_VW.sql
@@View\COURSE_SEARCH_VW.sql
@@View\CRSE_MEET_VW.sql
@@View\GENERAL_PERSON_VW.sql
@@View\ME_HOURS_SUMMARY_BY_TERM.sql
@@View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@View\MOBEDU_HOLIDAY_VW.sql
@@View\PERSON_CAMPUS_VW.sql
@@View\PERSON_VW.sql
@@View\STUDENT_ACCOUNT_DETAIL.sql
@@View\STU_CURCULAM_INFO.sql
@@View\STU_HOLD_INFO.sql
@@View\STU_PROFILE_INFO.sql
@@View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@View\SUMMARY_BY_TERM_VW.sql
@@View\TERMS_TO_BURSAR_VW.sql
@@View\TERM_COURSE_DETAILS_VW.sql
@@View\VW_TERM_CHARGES.sql
@@View\VW_TERM_PAYMENTS.sql
@@PackageBody\ADD_COURSE_PKG_CORQ.sql
@@PackageBody\CART_PKG.sql
@@PackageBody\DROP_COURSE_PKG.sql
@@PackageBody\ME_ACCOUNT_OBJECTS.sql
@@PackageBody\ME_ALT_PIN_PKG.sql
@@PackageBody\ME_BWCKCOMS.sql
@@PackageBody\ME_BWCKGENS.sql
@@PackageBody\ME_BWCKREGS.sql
@@PackageBody\ME_REG_UTILS.sql
@@PackageBody\ME_VALID.sql
@@PackageBody\ME_WITHDRAWL.sql
@@PackageBody\Z_CM_MOBILE_CAMPUS.sql

EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
