PROMPT CREATE OR REPLACE PACKAGE me_bwckcoms
CREATE OR REPLACE PACKAGE me_bwckcoms AS
  --
  -- FILE NAME..: bwckcoms.sql
  -- RELEASE....: 8.0
  -- OBJECT NAME: BWCKCOMS
  -- PRODUCT....: SCOMWEB
  -- USAGE......: One line description of this object. Max line length 80 bytes.
  -- COPYRIGHT..: Copyright (C) SCT Corporation 2002. All rights reserved.
  --
  -- DESCRIPTION:
  --
  -- This is a description of what this object does.
  --
  --  Cursors:
  --
  --   cursor1      - This cursor is used by function 1 and returns....
  --
  --  Functions:
  --
  --   function1    - This function is used by procedure 1 to.....
  --
  --  Procedures:
  --
  --   procedure1   - This procedure.....
  --
  -- DESCRIPTION END
  --

  TYPE varchar2_tabtype IS TABLE OF VARCHAR2(100) INDEX BY BINARY_INTEGER;

  TYPE rstsrowc_rec_typ IS RECORD(

    stvrsts_code                  stvrsts.stvrsts_code%TYPE,
    stvrsts_desc                  stvrsts.stvrsts_desc%TYPE,
    ssrrsts_usage_cutoff_pct_from ssrrsts.ssrrsts_usage_cutoff_pct_from%TYPE,
    ssrrsts_usage_cutoff_pct_to   ssrrsts.ssrrsts_usage_cutoff_pct_to%TYPE,
    ssrrsts_usage_cutoff_dur_from ssrrsts.ssrrsts_usage_cutoff_dur_from%TYPE,
    ssrrsts_usage_cutoff_dur_to   ssrrsts.ssrrsts_usage_cutoff_dur_to%TYPE);

  TYPE rstsrowc_typ IS REF CURSOR RETURN rstsrowc_rec_typ;

  sgbstdn_rec sgbstdn%ROWTYPE;
  lcur_tab    sokccur.curriculum_savedtab;

  ---------------------------------------------------------------------------
  FUNCTION f_reg_access_still_good(pidm_in      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                                   term_in      IN STVTERM.STVTERM_CODE%TYPE,
                                   call_path_in IN VARCHAR2,
                                   proc_name_in IN VARCHAR2) RETURN BOOLEAN;

  FUNCTION f_trim_sel_crn(sel_crn_in IN VARCHAR2) RETURN VARCHAR2;

  FUNCTION f_trim_sel_crn_term(sel_crn_in IN VARCHAR2) RETURN VARCHAR2;

  PROCEDURE p_adddrop(term       IN OWA_UTIL.ident_arr,
                      assoc_term IN OWA_UTIL.ident_arr,
                      sel_crn    IN OWA_UTIL.ident_arr);

  ---------------------------------------------------------------------------

  PROCEDURE p_adddrop1(term_in       IN OWA_UTIL.ident_arr,
                       sel_crn       IN OWA_UTIL.ident_arr,
                       assoc_term_in IN OWA_UTIL.ident_arr,
                       crn_in        IN OWA_UTIL.ident_arr,
                       start_date_in IN OWA_UTIL.ident_arr,
                       end_date_in   IN OWA_UTIL.ident_arr,
                       rsts_in       IN OWA_UTIL.ident_arr,
                       subj          IN OWA_UTIL.ident_arr,
                       crse          IN OWA_UTIL.ident_arr,
                       sec           IN OWA_UTIL.ident_arr,
                       levl          IN OWA_UTIL.ident_arr,
                       cred          IN OWA_UTIL.ident_arr,
                       gmod          IN OWA_UTIL.ident_arr,
                       title         IN bwckcoms.varchar2_tabtype,
                       mesg          IN OWA_UTIL.ident_arr,
                       regs_row      NUMBER,
                       add_row       NUMBER,
                       wait_row      NUMBER);

  ---------------------------------------------------------------------------

  PROCEDURE p_adddrop2(term_in                IN OWA_UTIL.ident_arr,
                       err_term               IN OWA_UTIL.ident_arr,
                       err_crn                IN OWA_UTIL.ident_arr,
                       err_subj               IN OWA_UTIL.ident_arr,
                       err_crse               IN OWA_UTIL.ident_arr,
                       err_sec                IN OWA_UTIL.ident_arr,
                       err_code               IN OWA_UTIL.ident_arr,
                       err_levl               IN OWA_UTIL.ident_arr,
                       err_cred               IN OWA_UTIL.ident_arr,
                       err_gmod               IN OWA_UTIL.ident_arr,
                       capp_tech_error_in_out IN OUT VARCHAR2,
                       drop_result_label_in   IN twgrinfo.twgrinfo_label%TYPE DEFAULT NULL,
                       drop_problems_in       IN sfkcurs.drop_problems_rec_tabtype,
                       drop_failures_in       IN sfkcurs.drop_problems_rec_tabtype);

  ---------------------------------------------------------------------------

  PROCEDURE p_addfromsearch(term_in       IN OWA_UTIL.ident_arr,
                            assoc_term_in IN OWA_UTIL.ident_arr,
                            sel_crn       IN OWA_UTIL.ident_arr,
                            add_btn       IN OWA_UTIL.ident_arr);

  ---------------------------------------------------------------------------

  PROCEDURE p_addfromsearch1(term_in       IN OWA_UTIL.ident_arr,
                             assoc_term_in IN OWA_UTIL.ident_arr,
                             sel_crn       IN OWA_UTIL.ident_arr,
                             rsts          IN OWA_UTIL.ident_arr,
                             crn           IN OWA_UTIL.ident_arr,
                             start_date_in IN OWA_UTIL.ident_arr,
                             end_date_in   IN OWA_UTIL.ident_arr,
                             subj          IN OWA_UTIL.ident_arr,
                             crse          IN OWA_UTIL.ident_arr,
                             sec           IN OWA_UTIL.ident_arr,
                             levl          IN OWA_UTIL.ident_arr,
                             cred          IN OWA_UTIL.ident_arr,
                             gmod          IN OWA_UTIL.ident_arr,
                             title         IN bwckcoms.varchar2_tabtype,
                             mesg          IN OWA_UTIL.ident_arr,
                             regs_row      NUMBER,
                             add_row       NUMBER,
                             wait_row      NUMBER,
                             add_btn       IN OWA_UTIL.ident_arr);

  ---------------------------------------------------------------------------
  PROCEDURE p_regs_etrm_chk(pidm_in           NUMBER,
                            term_in           stvterm.stvterm_code%TYPE,
                            clas_code         IN OUT SGRCLSR.SGRCLSR_CLAS_CODE%TYPE,
                            multi_term_in     BOOLEAN DEFAULT FALSE,
                            create_sfbetrm_in BOOLEAN DEFAULT TRUE);

  ---------------------------------------------------------------------------
  PROCEDURE p_regs(term_in       IN OWA_UTIL.ident_arr,
                   rsts_in       IN OWA_UTIL.ident_arr,
                   assoc_term_in IN OWA_UTIL.ident_arr,
                   crn_in        IN OWA_UTIL.ident_arr,
                   start_date_in IN OWA_UTIL.ident_arr,
                   end_date_in   IN OWA_UTIL.ident_arr,
                   subj          IN OWA_UTIL.ident_arr,
                   crse          IN OWA_UTIL.ident_arr,
                   sec           IN OWA_UTIL.ident_arr,
                   levl          IN OWA_UTIL.ident_arr,
                   cred          IN OWA_UTIL.ident_arr,
                   gmod          IN OWA_UTIL.ident_arr,
                   title         IN bwckcoms.varchar2_tabtype,
                   mesg          IN OWA_UTIL.ident_arr,
                   reg_btn       IN OWA_UTIL.ident_arr,
                   regs_row      NUMBER,
                   add_row       NUMBER,
                   wait_row      NUMBER);

  ---------------------------------------------------------------------------

  PROCEDURE p_disp_pin_prompt;

  ---------------------------------------------------------------------------

  FUNCTION f_validatestudentpin(pidm        SPRIDEN.SPRIDEN_PIDM%TYPE,
                                pin         GOBTPAC.GOBTPAC_PIN%TYPE,
                                ldap_userid IN VARCHAR2 DEFAULT NULL)
    RETURN BOOLEAN;

  ---------------------------------------------------------------------------

  PROCEDURE p_add_drop_init(term_in IN OWA_UTIL.ident_arr);

  ---------------------------------------------------------------------------

  PROCEDURE p_curr_sched_heading(heading_displayed IN OUT BOOLEAN,
                                 term              IN STVTERM.STVTERM_CODE%TYPE DEFAULT NULL,
                                 multi_term_in     IN BOOLEAN DEFAULT FALSE);

  ---------------------------------------------------------------------------

  PROCEDURE p_summary(term          IN STVTERM.STVTERM_CODE%TYPE DEFAULT NULL,
                      tot_credit_hr IN SFTREGS.SFTREGS_CREDIT_HR%TYPE DEFAULT NULL,
                      tot_bill_hr   IN SFTREGS.SFTREGS_BILL_HR%TYPE DEFAULT NULL,
                      tot_ceu       IN SFTREGS.SFTREGS_CREDIT_HR%TYPE DEFAULT NULL,
                      regs_count    IN NUMBER DEFAULT NULL);

  ---------------------------------------------------------------------------

  PROCEDURE p_reg_err_heading(call_type     IN NUMBER DEFAULT NULL,
                              multi_term_in IN BOOLEAN DEFAULT FALSE);

  PROCEDURE p_add_drop_crn1(call_type        IN NUMBER DEFAULT NULL,
                            sel_crn_count_in IN NUMBER DEFAULT 0);

  ---------------------------------------------------------------------------

  PROCEDURE p_add_drop_crn2(add_row_number IN NUMBER DEFAULT NULL,
                            regs_count     IN NUMBER DEFAULT NULL,
                            wait_count     IN NUMBER DEFAULT NULL);

  ---------------------------------------------------------------------------
  PROCEDURE p_disp_start_date_confirm(term_in       IN OWA_UTIL.ident_arr,
                                      rsts_in       IN OWA_UTIL.ident_arr,
                                      assoc_term_in IN OWA_UTIL.ident_arr,
                                      crn_in        IN OWA_UTIL.ident_arr,
                                      start_date_in IN OWA_UTIL.ident_arr,
                                      end_date_in   IN OWA_UTIL.ident_arr,
                                      subj          IN OWA_UTIL.ident_arr,
                                      crse          IN OWA_UTIL.ident_arr,
                                      sec           IN OWA_UTIL.ident_arr,
                                      levl          IN OWA_UTIL.ident_arr,
                                      cred          IN OWA_UTIL.ident_arr,
                                      gmod          IN OWA_UTIL.ident_arr,
                                      title         IN bwckcoms.varchar2_tabtype,
                                      mesg          IN OWA_UTIL.ident_arr,
                                      reg_btn       IN OWA_UTIL.ident_arr,
                                      regs_row      NUMBER,
                                      add_row       NUMBER,
                                      wait_row      NUMBER,
                                      next_proc_in  IN VARCHAR2,
                                      msg_in        IN VARCHAR2 DEFAULT NULL);

  --------------------------------------------------------------------------

  PROCEDURE p_proc_start_date_confirm(term_in            IN OWA_UTIL.ident_arr,
                                      rsts_in            IN OWA_UTIL.ident_arr,
                                      assoc_term_in      IN OWA_UTIL.ident_arr,
                                      crn_in             IN OWA_UTIL.ident_arr,
                                      start_date_in      IN OWA_UTIL.ident_arr,
                                      end_date_in        IN OWA_UTIL.ident_arr,
                                      subj               IN OWA_UTIL.ident_arr,
                                      crse               IN OWA_UTIL.ident_arr,
                                      sec                IN OWA_UTIL.ident_arr,
                                      levl               IN OWA_UTIL.ident_arr,
                                      cred               IN OWA_UTIL.ident_arr,
                                      gmod               IN OWA_UTIL.ident_arr,
                                      title              IN bwckcoms.varchar2_tabtype,
                                      mesg               IN OWA_UTIL.ident_arr,
                                      reg_btn            IN OWA_UTIL.ident_arr,
                                      regs_row           NUMBER,
                                      add_row            NUMBER,
                                      wait_row           NUMBER,
                                      start_date_from_in IN OWA_UTIL.ident_arr,
                                      start_date_to_in   IN OWA_UTIL.ident_arr,
                                      end_date_from_in   IN OWA_UTIL.ident_arr,
                                      end_date_to_in     IN OWA_UTIL.ident_arr,
                                      next_proc_in       IN VARCHAR2);

  ---------------------------------------------------------------------------
  PROCEDURE p_open_rstsrowc(rstsrowc          IN OUT rstsrowc_typ,
                            open_learning     OUT BOOLEAN,
                            term_in           VARCHAR2,
                            pidm_in           NUMBER,
                            crn_in            VARCHAR2,
                            rsts_in           VARCHAR2,
                            ptrm_in           VARCHAR2,
                            sdax_rsts_code_in VARCHAR2);

  ---------------------------------------------------------------------------
  PROCEDURE p_open_waitrstsrowc(waitrstsrowc  IN OUT rstsrowc_typ,
                                open_learning OUT BOOLEAN,
                                term_in       VARCHAR2,
                                pidm_in       NUMBER,
                                crn_in        VARCHAR2,
                                ptrm_in       VARCHAR2);

  ---------------------------------------------------------------------------
  PROCEDURE p_build_action_pulldown(term_in            stvterm.stvterm_code%TYPE,
                                    pidm_in            spriden.spriden_pidm%TYPE,
                                    crn_in             ssbsect.ssbsect_crn%TYPE,
                                    start_date_in      sftregs.sftregs_start_date%TYPE,
                                    completion_date_in sftregs.sftregs_completion_date%TYPE,
                                    regstatus_date_in  sftregs.sftregs_rsts_date%TYPE,
                                    dunt_in            sftregs.sftregs_dunt_code%TYPE,
                                    regs_count_in      NUMBER,
                                    rsts_code_in       stvrsts.stvrsts_code%TYPE,
                                    ptrm_code_in       ssbsect.ssbsect_ptrm_code%TYPE DEFAULT NULL,
                                    sdax_rsts_code_in  VARCHAR2 DEFAULT NULL,
                                    hold_rsts_in       VARCHAR2 DEFAULT NULL);

  ---------------------------------------------------------------------------

  PROCEDURE p_build_wait_action_pulldown(term_in       stvterm.stvterm_code%TYPE,
                                         pidm_in       spriden.spriden_pidm%TYPE,
                                         crn_in        ssbsect.ssbsect_crn%TYPE,
                                         wait_count_in NUMBER,
                                         ptrm_code_in  ssbsect.ssbsect_ptrm_code%TYPE DEFAULT NULL,
                                         rsts_in       VARCHAR2 DEFAULT NULL,
                                         row_count_out OUT NUMBER);

  ---------------------------------------------------------------------------
  PROCEDURE p_group_edits(term_in                IN OWA_UTIL.ident_arr,
                          pidm_in                IN spriden.spriden_pidm%TYPE,
                          etrm_done_in_out       IN OUT BOOLEAN,
                          capp_tech_error_in_out IN OUT VARCHAR2,
                          drop_problems_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                          drop_failures_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype);

  ---------------------------------------------------------------------------
  PROCEDURE p_problems(term_in          IN OWA_UTIL.ident_arr,
                       err_term         IN OWA_UTIL.ident_arr,
                       err_crn          IN OWA_UTIL.ident_arr,
                       err_subj         IN OWA_UTIL.ident_arr,
                       err_crse         IN OWA_UTIL.ident_arr,
                       err_sec          IN OWA_UTIL.ident_arr,
                       err_code         IN OWA_UTIL.ident_arr,
                       err_levl         IN OWA_UTIL.ident_arr,
                       err_cred         IN OWA_UTIL.ident_arr,
                       err_gmod         IN OWA_UTIL.ident_arr,
                       drop_problems_in IN sfkcurs.drop_problems_rec_tabtype,
                       drop_failures_in IN sfkcurs.drop_problems_rec_tabtype);

  ---------------------------------------------------------------------------
  PROCEDURE p_disp_confirm_drops(term_in          IN OWA_UTIL.ident_arr,
                                 err_term_in      IN OWA_UTIL.ident_arr,
                                 err_crn_in       IN OWA_UTIL.ident_arr,
                                 err_subj_in      IN OWA_UTIL.ident_arr,
                                 err_crse_in      IN OWA_UTIL.ident_arr,
                                 err_sec_in       IN OWA_UTIL.ident_arr,
                                 err_code_in      IN OWA_UTIL.ident_arr,
                                 err_levl_in      IN OWA_UTIL.ident_arr,
                                 err_cred_in      IN OWA_UTIL.ident_arr,
                                 err_gmod_in      IN OWA_UTIL.ident_arr,
                                 drop_problems_in IN sfkcurs.drop_problems_rec_tabtype,
                                 drop_failures_in IN sfkcurs.drop_problems_rec_tabtype);

  ---------------------------------------------------------------------------
  PROCEDURE p_proc_confirm_drops(term_in        IN OWA_UTIL.ident_arr,
                                 err_term_in    IN OWA_UTIL.ident_arr,
                                 err_crn_in     IN OWA_UTIL.ident_arr,
                                 err_subj_in    IN OWA_UTIL.ident_arr,
                                 err_crse_in    IN OWA_UTIL.ident_arr,
                                 err_sec_in     IN OWA_UTIL.ident_arr,
                                 err_code_in    IN OWA_UTIL.ident_arr,
                                 err_levl_in    IN OWA_UTIL.ident_arr,
                                 err_cred_in    IN OWA_UTIL.ident_arr,
                                 err_gmod_in    IN OWA_UTIL.ident_arr,
                                 d_p_term_code  IN OWA_UTIL.ident_arr,
                                 d_p_crn        IN OWA_UTIL.ident_arr,
                                 d_p_subj       IN OWA_UTIL.ident_arr,
                                 d_p_crse       IN OWA_UTIL.ident_arr,
                                 d_p_sec        IN OWA_UTIL.ident_arr,
                                 d_p_ptrm_code  IN OWA_UTIL.ident_arr,
                                 d_p_rmsg_cde   IN OWA_UTIL.ident_arr,
                                 d_p_message    IN OWA_UTIL.ident_arr,
                                 d_p_start_date IN OWA_UTIL.ident_arr,
                                 d_p_comp_date  IN OWA_UTIL.ident_arr,
                                 d_p_rsts_date  IN OWA_UTIL.ident_arr,
                                 d_p_dunt_code  IN OWA_UTIL.ident_arr,
                                 d_p_drop_code  IN OWA_UTIL.ident_arr,
                                 d_p_drop_conn  IN bwckcoms.varchar2_tabtype,
                                 d_f_term_code  IN OWA_UTIL.ident_arr,
                                 d_f_crn        IN OWA_UTIL.ident_arr,
                                 d_f_subj       IN OWA_UTIL.ident_arr,
                                 d_f_crse       IN OWA_UTIL.ident_arr,
                                 d_f_sec        IN OWA_UTIL.ident_arr,
                                 d_f_ptrm_code  IN OWA_UTIL.ident_arr,
                                 d_f_rmsg_cde   IN OWA_UTIL.ident_arr,
                                 d_f_message    IN OWA_UTIL.ident_arr,
                                 d_f_start_date IN OWA_UTIL.ident_arr,
                                 d_f_comp_date  IN OWA_UTIL.ident_arr,
                                 d_f_rsts_date  IN OWA_UTIL.ident_arr,
                                 d_f_dunt_code  IN OWA_UTIL.ident_arr,
                                 d_f_drop_code  IN OWA_UTIL.ident_arr,
                                 d_f_drop_conn  IN bwckcoms.varchar2_tabtype,
                                 submit_btn     IN VARCHAR2);

  ---------------------------------------------------------------------------
  PROCEDURE p_drop_problems_table_open(multi_term_in IN BOOLEAN DEFAULT FALSE);

  ---------------------------------------------------------------------------
  PROCEDURE p_drop_problems_list(drop_problems_in  IN sfkcurs.drop_problems_rec_tabtype,
                                 multi_term_in     IN BOOLEAN DEFAULT FALSE,
                                 table_open_in_out IN OUT BOOLEAN);

  ---------------------------------------------------------------------------
  PROCEDURE p_drop_failures_list(drop_failures_in  IN sfkcurs.drop_problems_rec_tabtype,
                                 multi_term_in     IN BOOLEAN DEFAULT FALSE,
                                 table_open_in_out IN OUT BOOLEAN);
  procedure p_intglobal(p_pidm number /*,
                                                               p_term stvterm.stvterm_code%type*/);
END ME_BWCKCOMS;




/

