PROMPT CREATE OR REPLACE PACKAGE drop_course_pkg
CREATE OR REPLACE PACKAGE drop_course_pkg AS

  procedure pz_drop_validations(p_pidm      in number,
                                p_term_code in varchar2,
                                p_crn       in ssbsect.ssbsect_crn%type,
                                p_error_msg out varchar2);

  procedure pz_remove_drop_failures(p_drop_failures in sfkcurs.drop_problems_rec_tabtype,
                                    p_pidm          in spriden.spriden_pidm%type,
                                    p_term_code     in sftregs.sftregs_term_code%type);

  procedure pz_drop_course(p_student_id in SPRIDEN.SPRIDEN_ID%TYPE,
                           p_term_code  in varchar2,
                           p_crn        in varchar2,
                           p_error_msg  out varchar2,
                           p_response   out varchar2,
                           p_debug      in varchar2 default 'N');

end drop_course_pkg;
/

