PROMPT CREATE OR REPLACE VIEW me_student_crse_regstrn_vw
CREATE OR REPLACE FORCE VIEW me_student_crse_regstrn_vw (
  term_code,
  pidm,
  student_id,
  registered_user,
  registered_date,
  course_number,
  course_title,
  subject,
  subject_description,
  credits,
  course_level,
  lecture_hours,
  schedule_type,
  department,
  crse_location,
  college,
  prerequisite,
  crn,
  schedule_date,
  schedule_time,
  faculty,
  faculty_email,
  registration_status,
  status_message,
  term_desc,
  flag,
  flag_desc,
  error_message,
  wait_list_priority,
  available_wait_list,
  var_credit,
  var_credit_hrs,
  instructor_email,
  course_comments,
  sect_seq_numb
) AS
SELECT A.SFRSTCR_TERM_CODE TERM_CODE,
       A.SFRSTCR_PIDM PIDM,
       SPRIDEN_ID STUDENT_ID,
       A.SFRSTCR_USER REGISTERED_USER,
       A.SFRSTCR_RSTS_DATE REGISTERED_DATE,
       B.SSBSECT_CRSE_NUMB COURSE_NUMBER,
       R.SCBCRSE_TITLE COURSE_TITLE,
       B.SSBSECT_SUBJ_CODE SUBJECT,
       J.STVSUBJ_DESC SUBJECT_DESCRIPTION,
       A.SFRSTCR_CREDIT_HR CREDITS,
       ME_REG_UTILS.FZ_GET_COURSE_LEVEL(R.SCBCRSE_CRSE_NUMB,
                                        R.SCBCRSE_EFF_TERM,
                                        R.SCBCRSE_SUBJ_CODE) COURSE_LEVEL,
       nvl(B.ssbsect_lec_hr, R.SCBCRSE_LEC_HR_LOW) LECTURE_HOURS, --CHECK IT
       B.SSBSECT_SCHD_CODE SCHEDULE_TYPE,
       R.SCBCRSE_DEPT_CODE DEPARTMENT,
       STVCAMP.STVCAMP_DESC CRSE_LOCATION,
       STVCOLL.STVCOLL_DESC COLLEGE,
       '' PREREQUISITE,
       A.SFRSTCR_CRN CRN,
       '' SCHEDULE_DATE,
       '' SCHEDULE_TIME,
       case
          when BANINST1.F_FORMAT_NAME(SIRASGN_PIDM, 'FL') like 'NAME NOT%' then
           ' '
          else
           BANINST1.F_FORMAT_NAME(SIRASGN_PIDM, 'FL')
        end
       /*BANINST1.F_FORMAT_NAME(SIRASGN_PIDM, 'FL')*/ FACULTY,
       ME_REG_UTILS.FZ_GET_EMP_EMAIL(SIRASGN_PIDM) FACULTY_EMAIL,
       A.SFRSTCR_RSTS_CODE REGISTRATION_STATUS,
       trim(both '*' from STVRSTS.STVRSTS_DESC) STATUS_MESSAGE,
       T.STVTERM_DESC TERM_DESC,
       A.SFRSTCR_ERROR_FLAG FLAG,
       DECODE(A.SFRSTCR_ERROR_FLAG,
              'F',
              'Failed',
              'L',
              'Wait List',
              'Success') FLAG_DESC,
       A.SFRSTCR_MESSAGE ERROR_MESSAGE,
       trunc(A.SFRSTCR_WL_PRIORITY) wait_list_priority,
       b.ssbsect_wait_avail available_wait_list,
       me_bwckgens.fz_get_var_ind(A.SFRSTCR_TERM_CODE, A.SFRSTCR_CRN) var_credit,
       CASE
         WHEN me_bwckgens.fz_get_var_ind(A.SFRSTCR_TERM_CODE, A.SFRSTCR_CRN) = 'Y' THEN
          R.SCBCRSE_CREDIT_HR_LOW || ' ' || R.SCBCRSE_CREDIT_HR_IND || ' ' ||
          R.SCBCRSE_CREDIT_HR_HIGH
         ELSE
          TO_CHAR(A.SFRSTCR_CREDIT_HR)
       END VAR_CREDIT_HRS,
       ME_REG_UTILS.FZ_GET_EMP_EMAIL(SHRINST_PIDM) INSTRUCTOR_EMAIL,
       me_reg_utils.wf_course_comment(b.ssbsect_term_code, b.ssbsect_crn) course_comments,
       B.SSBSECT_SEQ_NUMB sect_seq_numb
  FROM SATURN.SFRSTCR A
  JOIN SSBSECT B
    ON A.SFRSTCR_TERM_CODE = B.SSBSECT_TERM_CODE
   AND A.SFRSTCR_CRN = B.SSBSECT_CRN
   AND nvl(A.SFRSTCR_ERROR_FLAG, 'D') <> 'F'
   JOIN SPRIDEN
    ON SPRIDEN_PIDM = SFRSTCR_PIDM
   AND SPRIDEN_CHANGE_IND IS NULL
  JOIN SCBCRSE R
    ON R.SCBCRSE_SUBJ_CODE = B.SSBSECT_SUBJ_CODE
   AND R.SCBCRSE_CRSE_NUMB = B.SSBSECT_CRSE_NUMB
   AND R.scbcrse_eff_term =
       (select max(x.scbcrse_eff_term)
          from scbcrse x
         where x.scbcrse_subj_code = R.scbcrse_subj_code
           and x.scbcrse_crse_numb = R.scbcrse_crse_numb
           and x.scbcrse_eff_term <= a.sfrstcr_term_code)
  LEFT JOIN STVSUBJ J
    ON B.SSBSECT_SUBJ_CODE = J.STVSUBJ_CODE
  LEFT JOIN SIRASGN F
    ON A.SFRSTCR_TERM_CODE = F.SIRASGN_TERM_CODE
   AND A.SFRSTCR_CRN = F.SIRASGN_CRN
   AND F.SIRASGN_PRIMARY_IND = 'Y'
  LEFT JOIN STVCAMP
    ON B.SSBSECT_CAMP_CODE = STVCAMP.STVCAMP_CODE
  LEFT JOIN STVCOLL
    ON R.SCBCRSE_COLL_CODE = STVCOLL.STVCOLL_CODE
  LEFT JOIN STVRSTS
    ON A.SFRSTCR_RSTS_CODE = STVRSTS.STVRSTS_CODE
  LEFT JOIN STVTERM T
    ON A.SFRSTCR_TERM_CODE = T.STVTERM_CODE
  LEFT JOIN SHRINST
    ON B.SSBSECT_TERM_CODE = SHRINST_TERM_CODE
   AND B.SSBSECT_CRN = SHRINST_CRN
   AND SHRINST_PRIMARY_IND = 'Y'
/

