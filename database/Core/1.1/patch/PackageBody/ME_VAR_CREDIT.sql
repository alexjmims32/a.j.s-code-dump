PROMPT CREATE OR REPLACE PACKAGE BODY ME_VAR_CREDIT
CREATE OR REPLACE PACKAGE BODY ME_VAR_CREDIT AS
PROCEDURE PZ_CHANGE_CREDITS(p_student_id VARCHAR2,
                               P_TERM_CODE VARCHAR2,
                               P_CRN NUMBER,
                               P_cred NUMBER,
                               P_CRED_OLD NUMBER,
                               P_ERROR OUT VARCHAR2) IS


  crn owa_util.ident_arr;
  cred owa_util.ident_arr;
  gmod owa_util.ident_arr;
  levl owa_util.ident_arr;
  cred_old owa_util.ident_arr;
  gmod_old owa_util.ident_arr;
  levl_old owa_util.ident_arr;/*
  term_in stvterm.stvterm_code%type;*/
  sftregs_rec             sftregs%ROWTYPE;
  global_pidm spriden.spriden_pidm%type;
  save_act_date varchar2(100);
  fa_return_status varchar2(100);
  V_LEVEL STVLEVL.STVLEVL_CODE%TYPE;
  V_GMOD VARCHAR2(10);
  V_CHECK varchar2(10);
  drop_problems   sfkcurs.drop_problems_rec_tabtype;
  drop_failures   sfkcurs.drop_problems_rec_tabtype;
  capp_tech_error VARCHAR2(40);
  term_in         OWA_UTIL.ident_arr;
begin

  global_pidm:=baninst1.gb_common.f_get_pidm(p_student_id);

  if P_cred is null then
    P_ERROR:='Credit hours can not be null';
   /* GOTO QUIT_TO_PROGRAM;*/
  end if;
  BEGIN
  SELECT A.SFRSTCR_LEVL_CODE,A.SFRSTCR_GMOD_CODE INTO V_LEVEL,V_GMOD
   FROM SFRSTCR A WHERE A.SFRSTCR_PIDM=global_pidm AND A.SFRSTCR_TERM_CODE=P_TERM_CODE
  AND A.SFRSTCR_CRN=P_CRN;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END;
  V_CHECK:=me_valid.fz_check_sfrrsts(P_TERM_CODE,p_crn,'R');
    IF V_CHECK ='N' THEN
      P_ERROR := 'Registration not available at this time';
    /*  goto QUIT_TO_PROGRAM;*/
    END if;
  crn(1):='dummy';
  cred(1):='dummy';
  gmod(1):='dummy';
  levl(1):='dummy';
  cred_old(1):='dummy';
  gmod_old(1):='dummy';
  levl_old(1):='dummy';
  crn(2):=P_CRN;
  cred(2):=P_cred;
  gmod(2):=V_GMOD;
  levl(2):=V_LEVEL;
  cred_old(2):=P_CRED_OLD;
  gmod_old(2):=V_GMOD;
  levl_old(2):=V_LEVEL;
  -- Call the procedure
  me_bwckgens.p_intglobal(global_pidm,P_TERM_CODE);
  --me_bwckgens.p_disp;
  
  
   sfkmods.p_delete_sftregs_by_pidm_term(global_pidm, p_term_code);
  dbms_output.put_line('delete1');
 
  sfkmods.p_insert_sftregs_from_stcr(global_pidm, p_term_code, SYSDATE);
  dbms_output.put_line('insert1');
  select * into sftregs_rec from sftregs where sftregs_pidm=global_pidm
  and sftregs_term_code=P_TERM_CODE and sftregs_crn=P_CRN;
  sftregs_rec.sftregs_credit_hr:=3;
   me_bwckregs.p_updcrse(sftregs_rec, sftregs_rec.sftregs_crn);
   term_in(1) := p_term_code;
   
 me_bwckregs.p_allcrsechk(P_TERM_CODE,
                           'ADD_DROP',
                           capp_tech_error,
                           drop_problems,
                           drop_failures,
                           term_in);
  dbms_output.put_line('P_ERROR:='||P_ERROR);
  FOR i IN 1 .. drop_failures.COUNT LOOP
     
      /*\* dbms_output.put_line('d_p_term_code:' || drop_failures(i).term_code);
      dbms_output.put_line('d_p_crn:' || drop_failures(i).crn);
      dbms_output.put_line('d_p_subj:' || drop_failures(i).subj);
      dbms_output.put_line('d_p_crse:' || drop_failures(i).crse);
      dbms_output.put_line('d_p_sec:' || drop_failures(i).sec);
      dbms_output.put_line('d_p_ptrm_code:' || drop_failures(i).ptrm_code);
      dbms_output.put_line('d_p_rmsg_cde:' || drop_failures(i).rmsg_cde);
      dbms_output.put_line('d_p_message:' || drop_failures(i).message);
      dbms_output.put_line('d_p_start_date:' || drop_failures(i)
                           .start_date);
      dbms_output.put_line('d_p_comp_date:' || drop_failures(i).comp_date);
      dbms_output.put_line('d_p_rsts_date:' || drop_failures(i).rsts_date);
      dbms_output.put_line('d_p_dunt_code:' || drop_failures(i).dunt_code);
      dbms_output.put_line('d_p_drop_code:' || drop_failures(i).drop_code);
      dbms_output.put_line('d_p_drop_conn:' || drop_failures(i)
                           .connected_crns); *\*/
   
      dbms_output.put_line(bwcklibs.f_connected_crn_message(drop_failures(i).rmsg_cde,
                                       drop_failures(i).message,
                                       drop_failures(i).connected_crns));
    END LOOP;
  
  /*bwcklibs.p_initvalue(global_pidm, P_TERM_CODE, '', '', '', '');*/
  
  -- Call update procedure
/*  twbkwbis.p_setparam (global_pidm, 'STUFAC_IND', 'STU');
  twbkwbis.p_setparam (global_pidm, 'TERM', P_TERM_CODE);
  me_bwckgens.p_regsupd(term => P_TERM_CODE,
                        crn => crn,
                        cred => cred,
                        gmod => gmod,
                        levl => levl,
                        cred_old => cred_old,
                        gmod_old => gmod_old,
                        levl_old => levl_old,
                        P_ERROR => P_ERROR);

dbms_output.put_line('P_ERROR:='||P_ERROR);
 term_in(1) := p_term_code;
 me_bwckregs.p_allcrsechk(P_TERM_CODE,
                           'CHANGE',
                           capp_tech_error,
                           drop_problems,
                           drop_failures,
                           term_in);
  dbms_output.put_line('P_ERROR:='||P_ERROR);
  FOR i IN 1 .. drop_failures.COUNT LOOP
     
      \* dbms_output.put_line('d_p_term_code:' || drop_failures(i).term_code);
      dbms_output.put_line('d_p_crn:' || drop_failures(i).crn);
      dbms_output.put_line('d_p_subj:' || drop_failures(i).subj);
      dbms_output.put_line('d_p_crse:' || drop_failures(i).crse);
      dbms_output.put_line('d_p_sec:' || drop_failures(i).sec);
      dbms_output.put_line('d_p_ptrm_code:' || drop_failures(i).ptrm_code);
      dbms_output.put_line('d_p_rmsg_cde:' || drop_failures(i).rmsg_cde);
      dbms_output.put_line('d_p_message:' || drop_failures(i).message);
      dbms_output.put_line('d_p_start_date:' || drop_failures(i)
                           .start_date);
      dbms_output.put_line('d_p_comp_date:' || drop_failures(i).comp_date);
      dbms_output.put_line('d_p_rsts_date:' || drop_failures(i).rsts_date);
      dbms_output.put_line('d_p_dunt_code:' || drop_failures(i).dunt_code);
      dbms_output.put_line('d_p_drop_code:' || drop_failures(i).drop_code);
      dbms_output.put_line('d_p_drop_conn:' || drop_failures(i)
                           .connected_crns); *\
   
      dbms_output.put_line(bwcklibs.f_connected_crn_message(drop_failures(i).rmsg_cde,
                                       drop_failures(i).message,
                                       drop_failures(i).connected_crns));
    END LOOP;
  IF P_ERROR IS NULL THEN

  SFKFEES.p_processfeeassessment(P_TERM_CODE,
                                      global_pidm,
                                      SYSDATE,
                                      SYSDATE,
                                      'R',
                                      'Y',
                                      'BWCKREGS',
                                      'Y',
                                      save_act_date,
                                      'N',
                                      fa_return_status);


     gb_common.p_commit;
    END IF;
    <<QUIT_TO_PROGRAM>>
    NULL;*/
     /*EXCEPTION WHEN OTHERS THEN
       P_ERROR:=SQLERRM;*/
   END PZ_CHANGE_CREDITS;

END ME_VAR_CREDIT;
/

