PROMPT CREATE OR REPLACE PACKAGE BODY me_alt_pin_pkg
CREATE OR REPLACE PACKAGE BODY me_alt_pin_pkg AS
  CURSOR sfrctrl_row_by_term_c(term_in IN sfrctrl.sfrctrl_term_code_host%TYPE) RETURN sfrctrl%ROWTYPE IS
    SELECT *
      FROM sfrctrl
     WHERE sfrctrl_term_code_host = term_in
       AND trunc(SYSDATE) BETWEEN
           NVL(sfrctrl_begin_date,
               TO_DATE(G$_DATE.NORMALISE_GREG_DATE('01-01-1000',
                                                   'DD-MM-YYYY'),
                       G$_DATE.GET_NLS_DATE_FORMAT)) AND
           NVL(sfrctrl_end_date,
               TO_DATE(G$_DATE.NORMALISE_GREG_DATE('31-12-4712',
                                                   'DD-MM-YYYY'),
                       G$_DATE.GET_NLS_DATE_FORMAT))
       AND TO_CHAR(SYSDATE, 'HH24MISS') BETWEEN
           NVL(sfrctrl_hour_begin, '0000') || '00' and
           NVL(sfrctrl_hour_end, '2359') || '59'
     ORDER BY sfrctrl_seq_no;

  FUNCTION f_check_time_ticket(pidm_in    IN spriden.spriden_pidm%TYPE,
                               term_in    IN sfrctrl.sfrctrl_term_code_host%TYPE,
                               reg_date   DATE DEFAULT SYSDATE,
                               flag_value VARCHAR2) RETURN BOOLEAN IS
    sfbrgrpc_count NUMBER := 0;
    timetksc_count NUMBER := 0;
  BEGIN
    FOR sfbrgrp IN sfkcurs.sfbrgrpc(pidm_in, term_in) LOOP
      sfbrgrpc_count := sfkcurs.sfbrgrpc%rowcount;
    END LOOP;

    IF sfbrgrpc_count = 0 THEN
      IF flag_value = 'Y' THEN
        RETURN FALSE;
      ELSE
        RETURN TRUE;
      END IF;
    ELSE
      FOR time_rec IN sfkcurs.timetksc(pidm_in, term_in, reg_date) LOOP
        timetksc_count := sfkcurs.timetksc%rowcount;
      END LOOP;

      IF timetksc_count = 0 THEN
        RETURN FALSE;
      ELSE
        RETURN TRUE;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END f_check_time_ticket;

  FUNCTION f_check_mgmt_control(pidm_in   IN spriden.spriden_pidm%TYPE,
                                term_in   IN sfrctrl.sfrctrl_term_code_host%TYPE,
                                web_vr_in IN VARCHAR2) RETURN BOOLEAN IS
    gpa_type       shrlgpa.shrlgpa_gpa_type_ind%TYPE := 'O';
    ctrl_group     gortctl.gortctl_group%TYPE := 'REGISTRATION';
    alt_pin_func   gortctl.gortctl_function%TYPE := 'REG_USE_ALT_PIN';
    treg_proc      sprapin.sprapin_process_name%TYPE := 'TREG';
    get_next_rec   VARCHAR2(1);
    continue_check VARCHAR2(1);
    reg_allowed    VARCHAR2(1);
    curr_date      DATE;
    curr_time      VARCHAR2(4);
    stdn_hours     shrlgpa.shrlgpa_hours_earned%TYPE;
    stdn_clas      sfrctrl.sfrctrl_cls_1%TYPE;
    use_alt_pin    gortctl.gortctl_value%TYPE;
    pin            gobtpac.gobtpac_pin%TYPE;
    last_name      spriden.spriden_last_name%TYPE;
    sgrclsr_rec    sgrclsr%ROWTYPE;
    sfrctrl_rec    sfrctrl%ROWTYPE;
    spriden_rec    spriden%ROWTYPE;
    sgbstdn_rec    sgbstdn%ROWTYPE;
    sobterm_rec    sobterm%ROWTYPE;
    lcur_tab       sokccur.curriculum_savedtab;
    lfos_majr      sorlfos.sorlfos_majr_code%TYPE;
    lfos_dept      sorlfos.sorlfos_dept_code%TYPE;
    clas_code      sfrctrl.sfrctrl_cls_1%TYPE;
    clas_desc      stvclas.stvclas_desc%TYPE;
    internal_code  gtvsdax.gtvsdax_internal_code%TYPE;
    gtvsdax_group  gtvsdax.gtvsdax_internal_code_group%TYPE;
    last_nam_start sfrctrl.sfrctrl_last_nam_start%TYPE;
    last_nam_end   sfrctrl.sfrctrl_last_nam_end%TYPE;
  BEGIN
    curr_date := TRUNC(SYSDATE);
    curr_time := TO_CHAR(SYSDATE, 'HH24MI');

    IF web_vr_in = 'W' THEN
      internal_code := 'WEBALTPINU';
      gtvsdax_group := 'WEBREG';
      OPEN sfkcurs.get_gtvsdaxc(internal_code, gtvsdax_group);
      FETCH sfkcurs.get_gtvsdaxc
        INTO use_alt_pin;
      CLOSE sfkcurs.get_gtvsdaxc;
    ELSIF web_vr_in = 'V' THEN
      use_alt_pin := gokfunc.f_get_ctrl_value(ctrl_group, alt_pin_func);
    END IF;

    IF use_alt_pin = 'Y' THEN
      pin := spksels.f_get_alt_pin(pidm_in, term_in, treg_proc);
    ELSE
      pin := goksels.f_get_current_pin(pidm_in);
    END IF;
    --Retrieve student info
    sobterm_rec := soksels.f_get_sobterm_row(term_in);
    sgbstdn_rec := sgksels.f_get_sgbstdn_row(pidm_in, term_in);
    --
    -- Get student's primary curriculum and primary major
    --
    lcur_tab  := sokccur.f_current_active_curriculum(p_pidm      => pidm_in,
                                                     p_lmod_code => sb_curriculum_str.f_learner,
                                                     p_eff_term  => term_in);
    lfos_majr := sokccur.f_fieldofstudy_value(p_pidm       => pidm_in,
                                              p_lmod_code  => sb_curriculum_str.f_learner,
                                              p_term_code  => lcur_tab(1)
                                                              .r_term_code,
                                              p_key_seqno  => lcur_tab(1)
                                                              .r_key_seqno,
                                              p_lcur_seqno => lcur_tab(1)
                                                              .r_seqno,
                                              p_lfst_code  => sb_fieldofstudy_str.f_major,
                                              p_order      => 1,
                                              p_field      => 'MAJR');
    lfos_dept := sokccur.f_fieldofstudy_value(p_pidm       => pidm_in,
                                              p_lmod_code  => sb_curriculum_str.f_learner,
                                              p_term_code  => lcur_tab(1)
                                                              .r_term_code,
                                              p_key_seqno  => lcur_tab(1)
                                                              .r_key_seqno,
                                              p_lcur_seqno => lcur_tab(1)
                                                              .r_seqno,
                                              p_lfst_code  => sb_fieldofstudy_str.f_major,
                                              p_order      => 1,
                                              p_field      => 'DEPT');
    --
    stdn_hours := shksels.f_get_credit_hours(pidm_in,
                                             lcur_tab(1).r_levl_code,
                                             gpa_type);
    IF sobterm_rec.sobterm_incl_attmpt_hrs_ind = 'Y' THEN
      sobterm_rec.sobterm_incl_attmpt_hrs_ind := 'A';
    END IF;
    soklibs.p_class_calc(pidm_in,
                         lcur_tab(1).r_levl_code,
                         term_in,
                         sobterm_rec.sobterm_incl_attmpt_hrs_ind,
                         clas_code,
                         clas_desc);
    stdn_clas := clas_code;
    --   The purpose of the following variables:
    --
    --    get_next_rec   - Continue to the next record in the loop
    --    continue_check - Keep continuing down the various checks for the current
    --                     record
    --    reg_allowed    - Indicates whether the person is allowed to register

    get_next_rec   := 'Y';
    continue_check := 'Y';
    reg_allowed    := 'N';

    OPEN sfrctrl_row_by_term_c(term_in);
    WHILE get_next_rec = 'Y' LOOP
      continue_check := 'Y';

      FETCH sfrctrl_row_by_term_c
        INTO sfrctrl_rec;
      EXIT WHEN sfrctrl_row_by_term_c%NOTFOUND;

      IF sfrctrl_row_by_term_c%ROWCOUNT > 0 THEN
        reg_allowed := 'Y';
      END IF;

      IF continue_check = 'Y' then
        -- If PIN restrictions have been set, verify that the
        -- student's PIN meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_pin_start IS NOT NULL AND
           sfrctrl_rec.sfrctrl_pin_end IS NOT NULL THEN
          IF pin BETWEEN sfrctrl_rec.sfrctrl_pin_start AND
             sfrctrl_rec.sfrctrl_pin_end THEN
            get_next_rec   := 'N';
            continue_check := 'N';
            reg_allowed    := 'Y';
          ELSE
            get_next_rec   := 'Y';
            continue_check := 'N';
            reg_allowed    := 'N';
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_pin_start IS NOT NULL AND
             sfrctrl_rec.sfrctrl_pin_end IS NULL THEN
            IF pin >= sfrctrl_rec.sfrctrl_pin_start THEN
              get_next_rec   := 'N';
              continue_check := 'N';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          ELSE
            IF sfrctrl_rec.sfrctrl_pin_start IS NULL AND
               sfrctrl_rec.sfrctrl_pin_end IS NOT NULL THEN
              IF pin <= sfrctrl_rec.sfrctrl_pin_end THEN
                get_next_rec   := 'N';
                continue_check := 'N';
                reg_allowed    := 'Y';
              ELSE
                get_next_rec   := 'N';
                continue_check := 'N';
                reg_allowed    := 'N';
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If name restrictions have been set, verify that the
        -- student's last name meets the specified criteria.
        OPEN spksels.spriden_row_by_pidm_c(pidm_in);
        FETCH spksels.spriden_row_by_pidm_c
          INTO spriden_rec;
        CLOSE spksels.spriden_row_by_pidm_c;
        last_name      := UPPER(spriden_rec.spriden_last_name);
        last_nam_start := UPPER(sfrctrl_rec.sfrctrl_last_nam_start);
        last_nam_end   := UPPER(sfrctrl_rec.sfrctrl_last_nam_end);
        IF sfrctrl_rec.sfrctrl_last_nam_start IS NOT NULL AND
           sfrctrl_rec.sfrctrl_last_nam_end IS NOT NULL THEN
          IF last_name BETWEEN last_nam_start AND last_nam_end THEN
            get_next_rec   := 'N';
            continue_check := 'Y';
            reg_allowed    := 'Y';
          ELSE
            get_next_rec   := 'Y';
            continue_check := 'N';
            reg_allowed    := 'N';
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_last_nam_start IS NOT NULL AND
             sfrctrl_rec.sfrctrl_last_nam_end IS NULL THEN
            IF last_name >= last_nam_start THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          ELSE
            IF sfrctrl_rec.sfrctrl_last_nam_start IS NULL AND
               sfrctrl_rec.sfrctrl_last_nam_end IS NOT NULL THEN
              IF last_name <= last_nam_end THEN
                get_next_rec   := 'N';
                continue_check := 'Y';
                reg_allowed    := 'Y';
              ELSE
                get_next_rec   := 'Y';
                continue_check := 'N';
                reg_allowed    := 'N';
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If student type restrictions have been set, verify that
        -- the student's student type meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_stud_type_1 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_stud_type_2 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_stud_type_3 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_stud_type_4 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_stud_type_5 IS NOT NULL THEN
          IF sfrctrl_rec.sfrctrl_stud_type_1 =
             sgbstdn_rec.sgbstdn_styp_code OR sfrctrl_rec.sfrctrl_stud_type_2 =
             sgbstdn_rec.sgbstdn_styp_code OR sfrctrl_rec.sfrctrl_stud_type_3 =
             sgbstdn_rec.sgbstdn_styp_code OR sfrctrl_rec.sfrctrl_stud_type_4 =
             sgbstdn_rec.sgbstdn_styp_code OR sfrctrl_rec.sfrctrl_stud_type_5 =
             sgbstdn_rec.sgbstdn_styp_code THEN
            get_next_rec   := 'N';
            continue_check := 'Y';
            reg_allowed    := 'Y';
          ELSE
            get_next_rec   := 'Y';
            continue_check := 'N';
            reg_allowed    := 'N';
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If level restrictions have been set, verify that
        -- the student's level meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_levl_1 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_levl_2 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_levl_3 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_levl_4 IS NOT NULL OR
           sfrctrl_rec.sfrctrl_levl_5 IS NOT NULL THEN
          IF sfrctrl_rec.sfrctrl_levl_1 = lcur_tab(1).r_levl_code OR
             sfrctrl_rec.sfrctrl_levl_2 = lcur_tab(1).r_levl_code OR
             sfrctrl_rec.sfrctrl_levl_3 = lcur_tab(1).r_levl_code OR
             sfrctrl_rec.sfrctrl_levl_4 = lcur_tab(1).r_levl_code OR
             sfrctrl_rec.sfrctrl_levl_5 = lcur_tab(1).r_levl_code THEN
            get_next_rec   := 'N';
            continue_check := 'Y';
            reg_allowed    := 'Y';
          ELSE
            get_next_rec   := 'Y';
            continue_check := 'N';
            reg_allowed    := 'N';
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If class restrictions have been set, verify that the
        -- student's class meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_cls_incl_excl = 'E' THEN
          IF sfrctrl_rec.sfrctrl_cls_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_cls_1 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_2 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_3 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_4 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_5 = stdn_clas THEN
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            ELSE
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            END IF;
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_cls_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cls_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_cls_1 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_2 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_3 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_4 = stdn_clas OR
               sfrctrl_rec.sfrctrl_cls_5 = stdn_clas THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If major restrictions have been set, verify that the
        -- student's major meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_majr_incl_excl = 'E' THEN
          IF sfrctrl_rec.sfrctrl_majr_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_majr_1 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_2 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_3 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_4 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_5 = lfos_majr THEN
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            ELSE
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            END IF;
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_majr_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_majr_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_majr_1 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_2 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_3 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_4 = lfos_majr OR
               sfrctrl_rec.sfrctrl_majr_5 = lfos_majr THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If college restrictions have been set, verify that the
        -- student's college meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_coll_incl_excl = 'E' THEN
          IF sfrctrl_rec.sfrctrl_coll_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_coll_1 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_2 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_3 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_4 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_5 = lcur_tab(1).r_coll_code THEN
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            ELSE
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            END IF;
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_coll_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_coll_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_coll_1 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_2 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_3 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_4 = lcur_tab(1).r_coll_code OR
               sfrctrl_rec.sfrctrl_coll_5 = lcur_tab(1).r_coll_code THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If department restrictions have been set, verify that
        -- the student's department meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_dept_incl_excl = 'E' THEN
          IF sfrctrl_rec.sfrctrl_dept_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_dept_1 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_2 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_3 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_4 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_5 = lfos_dept THEN
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            ELSE
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            END IF;
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_dept_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_dept_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_dept_1 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_2 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_3 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_4 = lfos_dept OR
               sfrctrl_rec.sfrctrl_dept_5 = lfos_dept THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If degree restrictions have been set, verify that the
        -- student's degree meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_degr_incl_excl = 'E' THEN
          IF sfrctrl_rec.sfrctrl_degr_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_degr_1 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_2 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_3 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_4 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_5 = lcur_tab(1).r_degc_code THEN
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            ELSE
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            END IF;
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_degr_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_degr_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_degr_1 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_2 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_3 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_4 = lcur_tab(1).r_degc_code OR
               sfrctrl_rec.sfrctrl_degr_5 = lcur_tab(1).r_degc_code THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If campus restrictions have been set, verify that the
        -- student's campus meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_cmps_incl_excl = 'E' THEN
          IF sfrctrl_rec.sfrctrl_cmps_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_cmps_1 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_2 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_3 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_4 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_5 = lcur_tab(1).r_camp_code THEN
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            ELSE
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            END IF;
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_cmps_1 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_2 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_3 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_4 IS NOT NULL OR
             sfrctrl_rec.sfrctrl_cmps_5 IS NOT NULL THEN
            IF sfrctrl_rec.sfrctrl_cmps_1 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_2 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_3 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_4 = lcur_tab(1).r_camp_code OR
               sfrctrl_rec.sfrctrl_cmps_5 = lcur_tab(1).r_camp_code THEN
              get_next_rec   := 'N';
              continue_check := 'Y';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          END IF;
        END IF;
      END IF;

      IF continue_check = 'Y' THEN
        -- If earned hours restrictions have been set, verify that
        -- the student's earned hours meets the specified criteria.
        IF sfrctrl_rec.sfrctrl_earn_hrs_begin IS NOT NULL AND
           sfrctrl_rec.sfrctrl_earn_hrs_end IS NOT NULL THEN
          IF stdn_hours BETWEEN sfrctrl_rec.sfrctrl_earn_hrs_begin AND
             sfrctrl_rec.sfrctrl_earn_hrs_end THEN
            get_next_rec   := 'N';
            continue_check := 'N';
            reg_allowed    := 'Y';
          ELSE
            get_next_rec   := 'Y';
            continue_check := 'N';
            reg_allowed    := 'N';
          END IF;
        ELSE
          IF sfrctrl_rec.sfrctrl_earn_hrs_begin IS NOT NULL AND
             sfrctrl_rec.sfrctrl_earn_hrs_end IS NULL THEN
            IF stdn_hours >= sfrctrl_rec.sfrctrl_earn_hrs_begin THEN
              get_next_rec   := 'N';
              continue_check := 'N';
              reg_allowed    := 'Y';
            ELSE
              get_next_rec   := 'Y';
              continue_check := 'N';
              reg_allowed    := 'N';
            END IF;
          ELSE
            IF sfrctrl_rec.sfrctrl_earn_hrs_begin IS NULL and
               sfrctrl_rec.sfrctrl_earn_hrs_end IS NOT NULL THEN
              IF stdn_hours <= sfrctrl_rec.sfrctrl_earn_hrs_end THEN
                get_next_rec   := 'N';
                continue_check := 'N';
                reg_allowed    := 'Y';
              ELSE
                get_next_rec   := 'Y';
                continue_check := 'N';
                reg_allowed    := 'N';
              END IF;
            END IF;
          END IF;
        END IF;
      END IF;
    END LOOP;
    CLOSE sfrctrl_row_by_term_c;

    IF reg_allowed = 'Y' THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN FALSE;
  END f_check_mgmt_control;

  FUNCTION f_check_reg_appointment(pidm                 spriden.spriden_pidm%TYPE,
                                   term                 stvterm.stvterm_code%TYPE,
                                   use_mgmt_control     VARCHAR2,
                                   restrict_time_ticket VARCHAR2,
                                   web_vr_ind           VARCHAR2)
    RETURN BOOLEAN IS
  BEGIN
    IF NVL(use_mgmt_control, 'N') <> 'Y' THEN
      IF NOT sfkrctl.f_check_time_ticket(pidm,
                                         term,
                                         sysdate,
                                         NVL(restrict_time_ticket, 'N')) THEN
        RETURN FALSE;
      ELSE
        RETURN TRUE;
      END IF;
    ELSE
      IF NOT sfkrctl.f_check_mgmt_control(pidm, term, web_vr_ind) THEN
        RETURN FALSE;
      ELSE
        RETURN TRUE;
      END IF;
    END IF;
  END f_check_reg_appointment;

  FUNCTION F_ALT_PIN_SETUP_EXISTS RETURN VARCHAR2 is

    REG_PIN_SETUP VARCHAR2(2);

  BEGIN

    -- Documentation exists within Banner Student Self-Service User Guide 8.1.1 beginning in section 6-36 on page 266 out of 578 pages.

    SELECT NVL((SELECT GTVSDAX_EXTERNAL_CODE
                 FROM GTVSDAX
                WHERE GTVSDAX_INTERNAL_CODE = 'WEBALTPINA'),
               'N')
      INTO REG_PIN_SETUP
      FROM DUAL;

    RETURN REG_PIN_SETUP;

  END F_ALT_PIN_SETUP_EXISTS;

  FUNCTION F_STUDNT_PIN_EXISTS(pidm         IN saturn.spriden.spriden_pidm%TYPE,
                               term_code    in saturn.stvterm.stvterm_code%type,
                               process_name in saturn.SPRAPIN.SPRAPIN_PROCESS_NAME%type)

   RETURN VARCHAR2 is

    STUDENT_PIN_EXIST VARCHAR2(2);

  BEGIN

    -- Documentation exists within Banner Student Self-Service User Guide 8.1.1 beginning in section 6-36 on page 266 out of 578 pages.

    SELECT NVL((select 'Y'
                 from SPRAPIN
                where pidm = sprapin_pidm
                  and sprapin_term_code = term_code
                  and SPRAPIN_PROCESS_NAME = process_name),
               'N')
      INTO STUDENT_PIN_EXIST
      FROM DUAL;

    RETURN STUDENT_PIN_EXIST;

  END F_STUDNT_PIN_EXISTS;

  FUNCTION F_VALIDATE_PIN(P_STUDENT_ID   IN saturn.spriden.spriden_id%TYPE,
                          P_TERM_CODE    in saturn.stvterm.stvterm_code%type,
                          P_PROCESS_NAME IN SATURN.SPRAPIN.SPRAPIN_PROCESS_NAME%TYPE,
                          P_PIN_ENTERED  IN SATURN.SPRAPIN.SPRAPIN_PIN%TYPE)

   RETURN VARCHAR2 is

    VALID_PIN VARCHAR2(2);
    v_pidm    saturn.spriden.spriden_pidm%TYPE;

  BEGIN

    select baninst1.gb_common.f_get_pidm(P_STUDENT_ID)
      into v_pidm
      from dual;

    -- Documentation exists within Banner Student Self-Service User Guide 8.1.1 beginning in section 6-36 on page 266 out of 578 pages.

    SELECT NVL((select 'Y'
                 from SPRAPIN
                where sprapin_pidm = v_pidm
                  and sprapin_term_code = p_term_code
                  and SPRAPIN_PROCESS_NAME = p_process_name
                  and SPRAPIN_PIN = p_pin_entered),
               'N')
      INTO VALID_PIN
      FROM DUAL;

    RETURN VALID_PIN;

  END F_VALIDATE_PIN;

  PROCEDURE PZ_CHECK_ALT_PIN_REQ(P_STUDENT_ID IN saturn.spriden.spriden_id%TYPE,
                                 P_TERM_CODE  in saturn.stvterm.stvterm_code%type,
                                 P_CHECK_PIN  OUT VARCHAR2,
                                 P_ERROR_MSG  OUT VARCHAR2) AS

    v_pidm saturn.spriden.spriden_pidm%TYPE;

  BEGIN

    select baninst1.gb_common.f_get_pidm(P_STUDENT_ID)
      into v_pidm
      from dual;

    IF F_ALT_PIN_SETUP_EXISTS = 'Y' then

      IF F_STUDNT_PIN_EXISTS(pidm         => v_pidm,
                             term_code    => P_TERM_CODE,
                             process_name => 'TREG') = 'Y' then
        --TREG = Registration process

        P_CHECK_PIN := 'Y';

      ELSE
        P_CHECK_PIN := 'N';
      END IF;
    else
      P_CHECK_PIN := 'N';
    END IF;

  EXCEPTION
    WHEN OTHERS THEN

      P_ERROR_MSG := SQLERRM;

  END PZ_CHECK_ALT_PIN_REQ;

  -------
  --Fuction to check the record in SFRCTRL
  FUNCTION FZ_VALID_SFRCTRL( /*P_STUDENT_ID IN VARCHAR2,*/P_TERMCODE IN VARCHAR2,
                            P_PIN      IN saturn.SFRCTRL.SFRCTRL_PIN_START%TYPE)
    RETURN VARCHAR2 AS
    V_VALID VARCHAR2(1);
    V_PIDM  VARCHAR2(10);
  BEGIN
    /*SELECT BANINST1.GB_COMMON.f_get_pidm(P_STUDENT_ID)
    INTO V_PIDM
    FROM DUAL;*/
    dbms_output.put_line('pidm' || V_PIDM);

    SELECT NVL((SELECT 'Y'
                 FROM SFRCTRL
                WHERE SFRCTRL_TERM_CODE_HOST = P_TERMCODE
                  AND P_PIN BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),
               'N')
      INTO V_VALID
      FROM DUAL;
    dbms_output.put_line('v_valid' || V_VALID);

    return V_VALID;
    /*IF V_VALID = 'Y' THEN
      RETURN V_VALID;
    ELSE
      RETURN V_VALID;
    END IF;*/
  EXCEPTION
    WHEN OTHERS THEN
      V_VALID := 'N';
  END;

  -------
  --Function to check the record in SFBRGRP

  FUNCTION FZ_VALID_SFBRGRP(P_STUDENT_ID IN VARCHAR2,
                            P_TERMCODE   IN VARCHAR2) RETURN VARCHAR2 AS
    V_VALID VARCHAR2(1);
    V_PIDM  VARCHAR2(10);
  BEGIN
    SELECT BANINST1.GB_COMMON.f_get_pidm(P_STUDENT_ID)
      INTO V_PIDM
      FROM DUAL;
    BEGIN
      SELECT NVL((SELECT 'Y'
                   FROM SFBRGRP
                  WHERE sfbrgrp_term_code = P_TERMCODE
                    AND sfbrgrp_pidm = v_pidm),
                 'N')
        INTO V_VALID
        FROM DUAL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        V_VALID := 'N';
    END;
    IF V_VALID = 'Y' THEN
      RETURN V_VALID;
    ELSE
      RETURN V_VALID;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      V_VALID := 'N';
  END;

  ----------
  --Procedure to validate the time ticket

  procedure PZ_TIME_TICKET_CONTROL(P_STUDENT_ID  IN saturn.spriden.spriden_id%TYPE,
                                   P_TERM_CODE   in saturn.stvterm.stvterm_code%type,
                                   P_PIN         in saturn.SFRCTRL.SFRCTRL_PIN_START%TYPE,
                                   P_ERROR_MSG   OUT VARCHAR2,
                                   P_SUCCESS_MSG OUT VARCHAR2) is

    V_WEBMANCONT  VARCHAR2(2);
    V_WEBRESTTKT  VARCHAR2(2);
    V_SFARGTC     VARCHAR2(2);
    v_SFBRGRP     varchar2(2);
    V_SUCCESS_MSG varchar2(100);
    v_pidm        number;
    V_TICKET      BOOLEAN;
  begin

    select NVL((SELECT T.GTVSDAX_EXTERNAL_CODE
                 FROM GTVSDAX T
                WHERE GTVSDAX_INTERNAL_CODE = 'WEBRESTTKT'),
               'N')
      into V_WEBRESTTKT
      from dual;

    select NVL((SELECT C.GTVSDAX_EXTERNAL_CODE
                 FROM GTVSDAX C
                WHERE GTVSDAX_INTERNAL_CODE = 'WEBMANCONT'),
               'N')
      into V_WEBMANCONT
      from dual;
    BEGIN
      v_pidm := baninst1.gb_common.f_get_pidm(p_student_id);
    EXCEPTION
      WHEN OTHERS THEN
        P_ERROR_MSG := 'Invalid Userid';
    END;
    DBMS_OUTPUT.PUT_LINE(V_WEBMANCONT || 'TIME' || V_WEBRESTTKT);
    if v_pidm is not null then
      V_TICKET := ME_ALT_PIN_PKG.f_check_reg_appointment(v_pidm,
                                                                P_TERM_CODE,
                                                                V_WEBMANCONT,
                                                                V_WEBRESTTKT,
                                                                'W');

      IF V_TICKET = TRUE THEN
        DBMS_OUTPUT.put_line('TRUE');
        V_TICKET := me_alt_pin_pkg.f_check_webregister_term(P_TERM_CODE);
      ELSE
        DBMS_OUTPUT.put_line('FALSE');
      END IF;
      /* V_SFARGTC := mobedu.ME_ALT_PIN_PKG.FZ_VALID_SFRCTRL(\*P_STUDENT_ID,*\
      P_TERM_CODE,
      P_PIN);*/
      /*v_SFBRGRP := mobedu.ME_ALT_PIN_PKG.FZ_VALID_SFBRGRP(P_STUDENT_ID,
      P_TERM_CODE);*/
      /*SELECT NVL((SELECT 'Y'
                 FROM SFRCTRL
                WHERE SFRCTRL_TERM_CODE_HOST = P_TERM_CODE
                  AND P_PIN BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),
               'N')
      INTO V_SFARGTC
      FROM DUAL;*/

      DBMS_OUTPUT.PUT_LINE('WEBRESTTKT:' || V_WEBRESTTKT || ';WEBMANCONT:' ||
                           V_WEBMANCONT || ';SFARGTC:' || V_SFARGTC);

      IF V_WEBMANCONT = 'Y' AND V_WEBRESTTKT = 'N' AND V_TICKET = FALSE /*V_SFARGTC = 'N'*/
       THEN
        P_ERROR_MSG := 'Not permitted to register or drop at this time.';
      elsif V_WEBMANCONT = 'Y' AND V_WEBRESTTKT = 'N' AND V_TICKET = TRUE /*V_SFARGTC = 'Y'*/
       THEN
        v_SUCCESS_MSG := 'Success';
      elsif V_WEBMANCONT = 'Y' AND V_WEBRESTTKT = 'Y' AND V_TICKET = FALSE /*V_SFARGTC = 'N'*/
       THEN
        P_ERROR_MSG := 'Not permitted to register or drop at this time.';
      elsif V_WEBMANCONT = 'N' AND V_WEBRESTTKT = 'N' and V_TICKET = FALSE /*v_SFBRGRP = 'N'*/
       then
        P_ERROR_MSG := 'Not permitted to register or drop at this time.';
        /* v_SUCCESS_MSG := 'Success';*/
      elsif V_WEBMANCONT = 'N' and V_WEBRESTTKT = 'N' and V_TICKET = TRUE /*v_SFBRGRP = 'Y'*/
       then
        v_SUCCESS_MSG := 'Successs.';
      elsif V_WEBMANCONT = 'N' and V_WEBRESTTKT = 'Y' and V_TICKET = FALSE /*v_SFBRGRP = 'N'*/
       then
        P_ERROR_MSG := 'Contact the registration Adminstrator for your time ticket.';

      elsif V_WEBMANCONT = 'N' and V_WEBRESTTKT = 'Y' and V_TICKET = TRUE /*v_SFBRGRP = 'Y'*/
       then
        v_SUCCESS_MSG := 'Success';
      end if;
      if P_ERROR_MSG is not null then
        P_SUCCESS_MSG := 'N';
      elsif v_SUCCESS_MSG is not null then
        P_SUCCESS_MSG := 'Y';
      end if;
    end if;
  end PZ_TIME_TICKET_CONTROL;
  FUNCTION F_CHECK_WEBREGISTER_TERM(P_TERM STVTERM.STVTERM_CODE%TYPE)
    RETURN BOOLEAN IS
    CURSOR C_TERM IS
      SELECT A.SORRTRM_TERM_CODE
        FROM sorrtrm a, sobterm
       WHERE a.sorrtrm_term_code = P_TERM
         AND a.sorrtrm_term_code = sobterm_term_code
         AND sobterm_dynamic_sched_term_ind = 'Y'
         AND (TRUNC(SYSDATE) BETWEEN trunc(a.sorrtrm_start_date) AND
             trunc(a.sorrtrm_end_date));
  BEGIN
    FOR R_TERM IN C_TERM LOOP
      RETURN TRUE;
    END LOOP;
    RETURN FALSE;
  END F_CHECK_WEBREGISTER_TERM;
END ME_ALT_PIN_PKG;

/

