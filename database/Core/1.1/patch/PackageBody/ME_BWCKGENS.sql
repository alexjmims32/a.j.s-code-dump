PROMPT CREATE OR REPLACE PACKAGE BODY me_bwckgens
create or replace package body me_bwckgens is

  global_pidm spriden.spriden_pidm%TYPE;
  term        stvterm.stvterm_code%TYPE;

  sftregs_rec  sftregs%ROWTYPE;
  sgbstdn_rec  sgbstdn%ROWTYPE;
  stvterm_rec  stvterm%ROWTYPE;
  sorrtrm_rec  sorrtrm%ROWTYPE;
  sfbetrm_rec  sfbetrm%ROWTYPE;
  curr_release VARCHAR2(10) := '8.2';

  CURSOR SSBSECTC(CRN VARCHAR2, TERM VARCHAR2) RETURN SSBSECT%ROWTYPE IS
    SELECT *
      FROM SSBSECT
     WHERE SSBSECT_CRN = CRN
       AND SSBSECT_TERM_CODE = TERM;

  function fz_get_var_ind(p_term varchar2, p_crn number) return varchar2 is
    cursor c_var is
      select *
        from ssbsect a
        join sobterm c
          on a.ssbsect_term_code = c.sobterm_term_code
         and nvl(c.sobterm_cred_web_upd_ind, 'N') = 'Y'
         AND A.SSBSECT_TERM_CODE = p_term
         AND A.SSBSECT_CRN = P_CRN
        join scbcrse b
          on a.ssbsect_subj_code = b.scbcrse_subj_code
         and a.ssbsect_crse_numb = b.scbcrse_crse_numb
         and b.scbcrse_eff_term =
             (select max(t.scbcrse_eff_term)
                from scbcrse t
               where t.scbcrse_subj_code = a.ssbsect_subj_code
                 and t.scbcrse_crse_numb = a.ssbsect_crse_numb
                 and t.scbcrse_eff_term <= a.ssbsect_term_code)
         AND B.SCBCRSE_CREDIT_HR_IND IS NOT NULL
         AND A.SSBSECT_CREDIT_HRS IS NULL
         AND A.SSBSECT_VOICE_AVAIL = 'Y';
  begin

    FOR I IN C_VAR LOOP
      RETURN 'Y';
    END LOOP;
    RETURN 'N';
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'N';
  end;

  PROCEDURE PZ_CHANGE_CREDITS(p_student_id VARCHAR2,
                              P_TERM_CODE  VARCHAR2,
                              P_CRN        NUMBER,
                              P_cred       NUMBER,
                              P_CRED_OLD   NUMBER,
                              P_ERROR      OUT VARCHAR2) IS

    crn      owa_util.ident_arr;
    cred     owa_util.ident_arr;
    gmod     owa_util.ident_arr;
    levl     owa_util.ident_arr;
    cred_old owa_util.ident_arr;
    gmod_old owa_util.ident_arr;
    levl_old owa_util.ident_arr;
    --term_in          stvterm.stvterm_code%type;
    global_pidm      spriden.spriden_pidm%type;
    save_act_date    varchar2(100);
    fa_return_status varchar2(100);
    V_LEVEL          STVLEVL.STVLEVL_CODE%TYPE;
    V_GMOD           VARCHAR2(10);
    V_CHECK          varchar2(10);
    sobterm_row      SOBTERM%ROWTYPE;
  begin

    global_pidm := baninst1.gb_common.f_get_pidm(p_student_id);

    if P_cred is null then
      P_ERROR := 'CREDIT HOURS CAN NOT BE NULL';
      GOTO QUIT_TO_PROGRAM;
    end if;
    BEGIN
      SELECT A.SFRSTCR_LEVL_CODE, A.SFRSTCR_GMOD_CODE
        INTO V_LEVEL, V_GMOD
        FROM SFRSTCR A
       WHERE A.SFRSTCR_PIDM = global_pidm
         AND A.SFRSTCR_TERM_CODE = P_TERM_CODE
         AND A.SFRSTCR_CRN = P_CRN;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    V_CHECK := me_valid.fz_check_sfrrsts(P_TERM_CODE, p_crn, 'R');
    IF V_CHECK = 'N' THEN
      P_ERROR := 'Registration not available at this time';
      goto QUIT_TO_PROGRAM;
    END if;
    crn(1) := 'dummy';
    cred(1) := 'dummy';
    gmod(1) := 'dummy';
    levl(1) := 'dummy';
    cred_old(1) := 'dummy';
    gmod_old(1) := 'dummy';
    levl_old(1) := 'dummy';
    crn(2) := P_CRN;
    cred(2) := P_cred;
    gmod(2) := V_GMOD;
    levl(2) := V_LEVEL;
    cred_old(2) := P_CRED_OLD;
    gmod_old(2) := V_GMOD;
    levl_old(2) := V_LEVEL;
    -- Call the procedure
    me_bwckgens.p_intglobal(global_pidm, P_TERM_CODE);
    --me_bwckgens.p_disp;
    bwcklibs.p_initvalue(global_pidm, P_TERM_CODE, '', '', '', '');
    -- Call update procedure
    twbkwbis.p_setparam(global_pidm, 'STUFAC_IND', 'STU');
    twbkwbis.p_setparam(global_pidm, 'TERM', P_TERM_CODE);
    me_bwckgens.p_regsupd(term     => P_TERM_CODE,
                          crn      => crn,
                          cred     => cred,
                          gmod     => gmod,
                          levl     => levl,
                          cred_old => cred_old,
                          gmod_old => gmod_old,
                          levl_old => levl_old,
                          P_ERROR  => P_ERROR);

    IF P_ERROR IS NULL THEN
      BEGIN
         bwcklibs.p_getsobterm (p_term_code, sobterm_row);
       IF sobterm_row.sobterm_fee_assess_vr = 'Y'
      AND sobterm_row.sobterm_fee_assessment = 'Y'
      THEN
      SFKFEES.p_processfeeassessment(P_TERM_CODE,
                                     global_pidm,
                                     SYSDATE,
                                     SYSDATE,
                                     'R',
                                     'Y',
                                     'BWCKREGS',
                                     'Y',
                                     save_act_date,
                                     'N',
                                     fa_return_status);

      gb_common.p_commit;
       END IF;
      END;
    END IF;
    <<QUIT_TO_PROGRAM>>
    NULL;
  EXCEPTION
    WHEN OTHERS THEN
      P_ERROR := SQLERRM;
  END PZ_CHANGE_CREDITS;

  PROCEDURE P_RegsUpd(term     IN stvterm.stvterm_code%TYPE DEFAULT NULL,
                      crn      IN OWA_UTIL.ident_arr,
                      cred     IN OWA_UTIL.ident_arr,
                      gmod     IN OWA_UTIL.ident_arr,
                      levl     IN OWA_UTIL.ident_arr,
                      cred_old IN OWA_UTIL.ident_arr,
                      gmod_old IN OWA_UTIL.ident_arr,
                      levl_old IN OWA_UTIL.ident_arr,
                      P_ERROR  OUT VARCHAR2) IS
    i               INTEGER := 2;
    scbcrse_row     scbcrse%ROWTYPE;
    err_code        OWA_UTIL.ident_arr;
    err_msg         OWA_UTIL.ident_arr;
    clas_code       sgrclsr.sgrclsr_clas_code%TYPE;
    genpidm         spriden.spriden_pidm%TYPE;
    capp_tech_error VARCHAR2(4);
    call_path       VARCHAR2(1);
    proc_called_by  VARCHAR2(50);
    error_exists    BOOLEAN := FALSE;
    drop_problems   sfkcurs.drop_problems_rec_tabtype;
    drop_failures   sfkcurs.drop_problems_rec_tabtype;
    multi_term_list OWA_UTIL.ident_arr;

  BEGIN
    /*IF NOT twbkwbis.f_validuser (global_pidm)
    THEN
       RETURN;
    END IF;*/
    dbms_output.put_line('ME_BWCKGENS: 1');
    multi_term_list(1) := NULL;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      genpidm        := TO_NUMBER(twbkwbis.f_getparam(global_pidm,
                                                      'STUPIDM'),
                                  '999999999');
      call_path      := 'F';
      proc_called_by := 'bwlkfrad.P_FacChangeCrseOpt';
    ELSE
      dbms_output.put_line('ME_BWCKGENS: 2');
      genpidm        := global_pidm;
      call_path      := 'S';
      proc_called_by := 'bwskfreg.P_ChangeCrseOpt';
    END IF;

    IF NVL(twbkwbis.f_getparam(global_pidm, 'STUFAC_IND'), 'STU') = 'FAC' THEN
      --  FACWEB-specific code
      IF NOT bwckcoms.f_reg_access_still_good(genpidm,
                                              term,
                                              call_path || global_pidm,
                                              proc_called_by) THEN
        RETURN;
      END IF;
      dbms_output.put_line('ME_BWCKGENS: 3');
      --  Check to see if add/drop activity is allowed for selected term
      IF NOT bwlkilib.f_add_drp(term) THEN
        bwckfrmt.p_open_doc('bwlkfrad.P_FacAddDropCrse', term);
        twbkfrmt.p_printmessage(g$_nls.get('BWCKGEN1-0028',
                                           'SQL',
                                           'Registration is not allowed for this term at this time'),
                                'ERROR');
        twbkwbis.p_closedoc(curr_release);
        RETURN;
      END IF;
      dbms_output.put_line('ME_BWCKGENS: 4');
      IF NOT bwcksams.F_RegsStu(global_pidm,
                                term,
                                'bwlkfrad.P_FacChangeCrseOpt') THEN
        RETURN;
      END IF;
    ELSE
      -- STUWEB-specific code
      dbms_output.put_line('ME_BWCKGENS: 3');
      IF NOT bwckcoms.f_reg_access_still_good(genpidm,
                                              term,
                                              call_path || global_pidm,
                                              proc_called_by) THEN
        RETURN;
      END IF;

      IF NOT bwskflib.f_validterm(term, stvterm_rec, sorrtrm_rec) THEN
        bwskflib.p_seldefterm(term, 'bwskfreg.p_changecrseopt');
        RETURN;
      END IF;

      /*IF NOT bwcksams.F_RegsStu (
            global_pidm,
            term,
            'bwskfreg.p_changecrseopt'
         )
      THEN
         RETURN;
      END IF;*/
    END IF; /* END stu fac specific code sections */

    err_code(1) := 'dummy';
    err_msg(1) := 'dummy';
    dbms_output.put_line('ME_BWCKGENS: 4');
    FOR i IN 2 .. crn.count LOOP
      dbms_output.put_line('ME_BWCKGENS: 4.1');
      BEGIN
        IF NOT goklibs.f_isnumber(cred(i)) THEN
          RAISE bwcklibs.inv_cred_hrs;
        END IF;

        IF nvl(TO_NUMBER(cred(i)), 0) = nvl(TO_NUMBER(cred_old(i)), 0) AND
           nvl(gmod(i), 'X') = nvl(gmod_old(i), 'X') AND
           nvl(levl(i), 'X') = nvl(levl_old(i), 'X') THEN
          NULL;
          dbms_output.put_line('ME_BWCKGENS: 5');
        ELSE
          dbms_output.put_line('ME_BWCKGENS: 5.1');
          IF TO_NUMBER(cred(i)) <> TO_NUMBER(cred_old(i)) THEN
            FOR ssbsect IN ssbsectc(crn(i), term) LOOP
              OPEN scklibs.scbcrsec(ssbsect.ssbsect_subj_code,
                                    ssbsect.ssbsect_crse_numb,
                                    term);
              FETCH scklibs.scbcrsec
                INTO scbcrse_row;
              dbms_output.put_line('ME_BWCKGENS: 6');
              IF scklibs.scbcrsec%NOTFOUND THEN
                scbcrse_row.scbcrse_credit_hr_ind  := NULL;
                scbcrse_row.scbcrse_credit_hr_low  := NULL;
                scbcrse_row.scbcrse_credit_hr_high := NULL;
                scbcrse_row.scbcrse_bill_hr_ind    := NULL;
                scbcrse_row.scbcrse_bill_hr_low    := NULL;
                scbcrse_row.scbcrse_bill_hr_high   := NULL;
              END IF;

              CLOSE scklibs.scbcrsec;
            END LOOP;

            IF scbcrse_row.scbcrse_credit_hr_ind = 'TO' THEN
              IF TO_NUMBER(cred(i)) <
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_low) OR
                 TO_NUMBER(cred(i)) >
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_high) THEN
                RAISE bwcklibs.inv_cred_hrs;
              END IF;
            ELSE
              IF TO_NUMBER(cred(i)) <>
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_low) AND
                 TO_NUMBER(cred(i)) <>
                 TO_NUMBER(scbcrse_row.scbcrse_credit_hr_high) THEN
                RAISE bwcklibs.inv_cred_hrs;
              END IF;
            END IF;

            IF scbcrse_row.scbcrse_bill_hr_ind = 'TO' THEN
              IF TO_NUMBER(cred(i)) BETWEEN
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_low) AND
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_high) THEN
                sftregs_rec.sftregs_bill_hr := TO_NUMBER(cred(i));
              ELSE
                sftregs_rec.sftregs_bill_hr := NULL;
                dbms_output.put_line('ME_BWCKGENS: 7');
              END IF;
            ELSIF scbcrse_row.scbcrse_bill_hr_ind = 'OR' THEN
              IF TO_NUMBER(cred(i)) =
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_low) OR
                 TO_NUMBER(cred(i)) =
                 TO_NUMBER(scbcrse_row.scbcrse_bill_hr_high) THEN
                sftregs_rec.sftregs_bill_hr := TO_NUMBER(cred(i));
              ELSE
                sftregs_rec.sftregs_bill_hr := NULL;
              END IF;
            END IF;

          ELSE
            sftregs_rec.sftregs_bill_hr := NULL;
          END IF;
          dbms_output.put_line('ME_BWCKGENS: 8');
          bwcklibs.p_initvalue(global_pidm, term, '', SYSDATE, '', '');

          FOR sgbstdn IN sgklibs.sgbstdnc(genpidm, term) LOOP
            dbms_output.put_line('ME_BWCKGENS: 9');
            sgbstdn_rec := sgbstdn;
          END LOOP;

          me_bwckregs.p_regschk(sgbstdn_rec);
          dbms_output.put_line('ME_BWCKGENS: 10');
          clas_code := me_bwckregs.f_getstuclas;
          dbms_output.put_line('ME_BWCKGENS: 11  ' || clas_code);
          FOR sfbetrm IN sfkcurs.sfbetrmc(genpidm, term) LOOP
            sfbetrm_rec := sfbetrm;
          END LOOP;

          me_bwckregs.p_regschk(sfbetrm_rec);
          dbms_output.put_line('ME_BWCKGENS: 12');
          sftregs_rec.sftregs_pidm      := genpidm;
          sftregs_rec.sftregs_term_code := term;
          sftregs_rec.sftregs_crn       := crn(i);
          sftregs_rec.sftregs_credit_hr := TO_NUMBER(cred(i));
          sftregs_rec.sftregs_gmod_code := gmod(i);
          sftregs_rec.sftregs_levl_code := levl(i);
          --
          -- Get the section information.
          -- ==============================
          me_bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                                   sftregs_rec.sftregs_crn,
                                   sftregs_rec.sftregs_sect_subj_code,
                                   sftregs_rec.sftregs_sect_crse_numb,
                                   sftregs_rec.sftregs_sect_seq_numb);
          --
          dbms_output.put_line('ME_BWCKGENS: 13');
          me_bwckregs.p_updcrse(sftregs_rec, sftregs_rec.sftregs_crn);
        END IF;
        dbms_output.put_line('ME_BWCKGENS: 14');
      EXCEPTION
        WHEN OTHERS THEN
          err_code(i) := SQLCODE;
          error_exists := TRUE;
          P_ERROR := BWCKLIBS.error_msg_table(SQLCODE);
      END;

    END LOOP;

    FOR i IN 2 .. crn.count LOOP
      FOR sftregs IN sfkcurs.sftregsc(genpidm, term) LOOP
        IF sftregs.sftregs_error_flag = 'F' AND
           sftregs.sftregs_crn = crn(i) THEN
          err_msg(i) := sftregs.sftregs_message;
          err_code(i) := '';
          sfkmods.p_reset_sftregs_fields(genpidm, term, crn(i), NULL);
          EXIT;
        END IF;
      END LOOP;
    END LOOP;

    me_bwckregs.p_allcrsechk(term,
                             'CHANGE',
                             capp_tech_error,
                             drop_problems,
                             drop_failures,
                             multi_term_list);

    -- Check for minimum hours error
    IF NVL(twbkwbis.f_getparam(genpidm, 'ERROR_FLAG'), 'N') = 'M' THEN
      -- don't pass other potential edit errors back to page for redisplay, as
      -- we are starting from scratch. sfrstcr was not updated in p_allcrsechk.
      bwcksams.P_DispChgCrseOpt2(term);
      RETURN;
    END IF;

    FOR i IN 2 .. crn.count LOOP
      FOR sftregs IN sfkcurs.sftregsc(genpidm, term) LOOP
        IF sftregs.sftregs_error_flag = 'F' AND
           sftregs.sftregs_crn = crn(i) THEN
          err_msg(i) := sftregs.sftregs_message;
          err_code(i) := '';
          error_exists := TRUE;
          EXIT;
        END IF;
      END LOOP;
    END LOOP;

    -- repopulate sftregs from sfrstcr if any errors found during
    -- registration checking
    -------------------------------------------------------------
    IF error_exists THEN
      IF NOT sfkfunc.f_registration_access(genpidm,
                                           term,
                                           'PROXY',
                                           call_path || global_pidm,
                                           bypass_admin_in => 'Y') THEN
        twbkwbis.p_dispinfo(proc_called_by, 'SESSIONBLOCKED');
        HTP.formclose;
        twbkwbis.p_closedoc(curr_release);
        RETURN;
      END IF;

    END IF;

    /* bwcksams.P_DispChgCrseOpt (
       term,
       crn,
       cred,
       gmod,
       levl,
       err_code,
       err_msg,
       FALSE,
       capp_tech_error
    );*/

  END P_RegsUpd;

  procedure p_intglobal(p_pidm number, p_term stvterm.stvterm_code%type) is

  begin
    global_pidm := p_pidm;
    term        := p_term;
  end;
  
  
end;
/

