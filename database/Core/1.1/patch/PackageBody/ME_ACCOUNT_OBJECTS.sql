PROMPT CREATE OR REPLACE PACKAGE BODY me_account_objects
CREATE OR REPLACE PACKAGE BODY me_account_objects is

  function fz_future_term_charges(p_pidm spriden.spriden_pidm%type)
    return number is
    result number;
  begin
    begin
      select SUM(decode(C.tbbdetc_type_ind, 'P', -1, 'C', 1) *
                 A.TBRACCD_AMOUNT) OVER(PARTITION BY A.TBRACCD_PIDM)
        into result
        from tbraccd a
        join tbbdetc c
          on c.tbbdetc_detail_code = a.tbraccd_detail_code
         and a.tbraccd_pidm = p_pidm
        join stvterm b
          on b.stvterm_code = a.tbraccd_term_code
         and b.stvterm_start_date > sysdate;
    exception
      when others then
        result := 0;
    end;
    return result;
  end fz_future_term_charges;

  function fz_get_acc_balance(stu_pidm in number) return number is
    Acc_Bal number;
  begin
    SELECT nvl(SUM(DECODE(TBBDETC_TYPE_IND,
                          'C',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)) -
               SUM(DECODE(TBBDETC_TYPE_IND,
                          'P',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)),
               0)
      INTO Acc_Bal
      FROM TBBDETC, TBRACCD
     WHERE TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE
       AND TBRACCD_PIDM = stu_pidm
     GROUP BY TBRACCD_PIDM;
    return(Acc_Bal);
  end fz_get_acc_balance;

  function fz_get_tot_due_bal(stu_pidm in number) return number is
    DueBal number;
  begin
    select nvl(SUM(A.TBRACCD_BALANCE), 0)
      INTO DueBal
      from tbraccd a
     where a.tbraccd_pidm = stu_pidm
       and trunc(a.tbraccd_effective_date) <= trunc(sysdate)
          --         and a.tbraccd_due_date < trunc(sysdate)
       and a.tbraccd_balance > 0;
    return(DueBal);
  end FZ_GET_TOT_DUE_BAL;

  function fz_get_currtrm_acc_bal(stu_pidm in number) return number is
    Acc_Bal number;
  begin
    SELECT nvl(SUM(DECODE(TBBDETC_TYPE_IND,
                          'C',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)) -
               SUM(DECODE(TBBDETC_TYPE_IND,
                          'P',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)),
               0)
      INTO Acc_Bal
      FROM TBBDETC a
      join TBRACCD b
        on b.tbraccd_detail_code = a.tbbdetc_detail_code
       AND TBRACCD_PIDM = stu_pidm
       and b.tbraccd_term_code IN
           (select t.stvterm_code
              from stvterm t
             where sysdate between t.stvterm_start_date and
                   t.stvterm_end_date
               and t.stvterm_code not like '0%')

     GROUP BY TBRACCD_PIDM;
    return(Acc_Bal);
  end fz_get_currtrm_acc_bal;

  function fz_get_prevtrm_acc_bal(stu_pidm in number) return number is
    Acc_Bal number;
  begin
    SELECT nvl(SUM(DECODE(TBBDETC_TYPE_IND,
                          'C',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)) -
               SUM(DECODE(TBBDETC_TYPE_IND,
                          'P',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)),
               0)
      INTO Acc_Bal
      FROM TBBDETC a
      join TBRACCD b
        on b.tbraccd_detail_code = a.tbbdetc_detail_code
       AND TBRACCD_PIDM = stu_pidm
       and b.tbraccd_term_code <
           (select t.stvterm_code
              from stvterm t
             where sysdate between t.stvterm_start_date and
                   t.stvterm_end_date
               and t.stvterm_code not like '0%'
               AND T.STVTERM_CODE NOT LIKE '9%')

     GROUP BY TBRACCD_PIDM;
    return(Acc_Bal);
  end fz_get_prevtrm_acc_bal;

  FUNCTION fz_get_hold_flag(p_id        IN spriden.spriden_id%TYPE,
                            p_hold_type VARCHAR2) RETURN VARCHAR2 IS

    RESULT VARCHAR2(300) := 'N';
    CURSOR c_hold_info IS

      SELECT X.id,
             x.pidm,
             x.hold_code,
             x.hold_description,
             x.hold_from_date,
             x.hold_to_date,
             SUM(X.REGISTRATION_HOLDS) over(partition by id) REGISTRATION_HOLDS,
             SUM(X.transcript_holds) over(partition by id) transcript_holds,
             SUM(X.Graduation_holds) over(partition by id) Graduation_holds,
             SUM(X.Financial_holds) over(partition by id) Financial_holds,
             SUM(X.Enrollment_Verification_holds) over(partition by id) Enrollment_Verification_holds,
             SUM(X.Grade_Release_holds) over(partition by id) Grade_Release_holds
        FROM (SELECT DISTINCT b.spriden_id id,
                              sprhold_pidm pidm,
                              a.sprhold_hldd_code hold_code,
                              ME_REG_UTILS.fz_get_desc(a.sprhold_hldd_code,
                                                 'STVHLDD','') Hold_description,
                              a.sprhold_from_date Hold_From_Date,
                              a.sprhold_to_date Hold_To_Date,
                              count(d.stvhldd_reg_hold_ind) over(PARTITION BY sprhold_pidm, d.stvhldd_reg_hold_ind) registration_holds,
                              COUNT(e.stvhldd_trans_hold_ind) over(PARTITION BY sprhold_pidm, e.stvhldd_trans_hold_ind) transcript_holds,
                              COUNT(f.stvhldd_grad_hold_ind) over(PARTITION BY sprhold_pidm, f.stvhldd_grad_hold_ind) Graduation_holds,
                              COUNT(g.stvhldd_grade_hold_ind) over(PARTITION BY sprhold_pidm, g.stvhldd_grade_hold_ind) Grade_Release_holds,
                              COUNT(decode(i.stvhldd_code,
                                           'F0',
                                           NULL,
                                           i.stvhldd_code)) over(PARTITION BY sprhold_pidm, i.stvhldd_code) Financial_holds,
                              COUNT(h.stvhldd_env_hold_ind) over(PARTITION BY sprhold_pidm, h.stvhldd_env_hold_ind) Enrollment_verification_holds
                FROM sprhold a
                JOIN spriden b
                  ON b.spriden_pidm = a.sprhold_pidm
                 AND b.spriden_change_ind IS NULL
                LEFT OUTER JOIN stvhldd d
                  ON d.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(a.sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND d.stvhldd_reg_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd e
                  ON e.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(a.sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND e.stvhldd_trans_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd f
                  ON f.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(a.sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND f.stvhldd_grad_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd g
                  ON g.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND g.stvhldd_grade_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd h
                  ON h.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND h.stvhldd_env_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd i
                  ON i.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND i.stvhldd_code LIKE 'F%'
                 AND i.stvhldd_code <> 'F0'
               WHERE spriden_id = p_id
                 AND b.spriden_change_ind IS NULL) X;

  BEGIN
    FOR R_HOLD_INFO IN c_hold_info LOOP
      --     dbms_output.put_line('Steve: ' || r_hold_info.registration_holds);
      IF r_hold_info.registration_holds >= 1 AND
         p_hold_type = 'REGISTRATION_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.transcript_holds >= 1 AND
            p_hold_type = 'TRANSCRIPT_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.graduation_holds >= 1 AND
            p_hold_type = 'GRADUATION_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.grade_release_holds >= 1 AND
            p_hold_type = 'GRADE_RELEASE_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.financial_holds >= 1 AND
            p_hold_type = 'FINANCIAL_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.enrollment_verification_holds >= 1 AND
            p_hold_type = 'ENROLLMENT_VERIFICATION' THEN
        RESULT := 'Y';
      ELSE
        RESULT := 'N';
        --        dbms_output.put_line('Steve: returning N');
      END IF;

    END LOOP;

    RETURN RESULT;

  END fz_get_hold_flag;

  function fz_unbilled_charges(p_pidm spriden.spriden_pidm%type)
    return number is
    result number;
  begin
    begin
      select SUM(decode(C.tbbdetc_type_ind, 'P', -1, 'C', 1) *
                 A.TBRACCD_AMOUNT) OVER(PARTITION BY A.TBRACCD_PIDM)
        into result
        from tbraccd a
        join tbbdetc c
          on c.tbbdetc_detail_code = a.tbraccd_detail_code
         and a.tbraccd_pidm = p_pidm
         and a.tbraccd_bill_date is null;

    exception
      when others then
        result := 0;
    end;
    return result;
  end fz_unbilled_charges;

  /**
  * Function to get net past due based on pidm
  * @return                    Past Due Amount
  * @param p_pidm              Internal identification number p_pidm
  */
  function fz_net_past_due(p_pidm spriden.spriden_pidm%type) return number is
    result number;
  begin
    begin
      select nvl(SUM(A.TBRACCD_BALANCE), 0)
        into result
        from tbraccd a
       where a.tbraccd_pidm = p_pidm
         and trunc(a.tbraccd_effective_date) <= trunc(sysdate)
         and a.tbraccd_due_date < trunc(sysdate)
         and a.tbraccd_balance > 0;

    exception
      when others then
        result := 0;
    end;
    return result;
  end fz_net_past_due;

end me_account_objects;






/

