CREATE OR REPLACE PROCEDURE pz_me_authentication(USER_ID         in varchar2,
                                                 PASSWORD        in varchar2,
                                                 /*ME_REGISTRATION OUT VARCHAR2,
                                                 ME_BURSAR       OUT VARCHAR2,*/
                                                 ROLE_CODE       OUT VARCHAR2,
                                                 DB_STATUS       OUT VARCHAR2) IS
  --

  OBJECT        VARCHAR2(30);
  PASSWORD_     VARCHAR2(30);
  ROLE_NAME     VARCHAR2(30);
  VERSION       VARCHAR2(10);
  USERID        varchar2(60);
  USER_NAME     varchar2(200);
  p_pin         varchar2(30);
  login_access  boolean;
  pidm          varchar2(30);
  p_expire_ind  varchar2(300);
  p_disable_ind varchar2(300);
  -- Exceptions.
  NO_INST EXCEPTION;
  NO_ACCESS EXCEPTION;
  NO_PASSWORD EXCEPTION;
  INVALID_VERSION EXCEPTION;
  INVALID_ACCESS EXCEPTION;
  --
BEGIN
  OBJECT  := 'MOB_EDU';
  VERSION := '1.0';
  USERID  := UPPER(USER_ID);
  pidm    := baninst1.gb_common.f_get_pidm(userid);
  p_pin   := PASSWORD;

  /*login_access := baninst1.gb_third_party_access.f_validate_pin(p_pidm        => pidm,
  p_pin         => p_pin,
  p_expire_ind  => p_expire_ind,
  p_disable_ind => p_disable_ind);*/
  login_access := baninst1.gb_third_party_access.f_validate_pin(pidm,
                                                                p_pin,
                                                                p_expire_ind,
                                                                p_disable_ind);

  --
  -- Obtain Spriden Name for oracle user
  --
  --DBMS_OUTPUT.PUT_LINE('OUTPUT:1' || OBJECT || ' ' || VERSION);
  --user_name := g$_security.g$_get_username_name(USERID);
  -- Obtain encrypted password.
  --

  --G$_SECURITY.G$_VERIFY_PASSWORD1_PRD(OBJECT, VERSION, PASSWORD_, ROLE_NAME);
  --DBMS_OUTPUT.PUT_LINE('OUTPUT:2' || USER_NAME);
  --DBMS_OUTPUT.put_line(OBJECT || VERSION || PASSWORD || ROLE_NAME || USER);
  -- Obtain Flex form roles for logged in user
  --

  /*ME_REGISTRATION := NVL(G$_SECURITY.G$_GET_ROLE_FOR_OBJECT_FNC('ME_REGISTRATION',
                                                                USER),
                         'BAN_DEFAULT_NV');
  ME_BURSAR       := NVL(G$_SECURITY.G$_GET_ROLE_FOR_OBJECT_FNC('ME_BURSAR',
                                                                USER),
                         'BAN_DEFAULT_NV');*/
  DBMS_OUTPUT.put_line('output:3' || OBJECT || VERSION || PASSWORD ||
                       ROLE_NAME || USER);

  /* db_status := object || ',' || version || ',' ||
  password || ',' || role_name;*/
  if login_access = true then
    ROLE_CODE:='STUDENT';
    DB_STATUS := 'ACCESS SUCCESSFUL';
  else
    ROLE_CODE:=NULL;
    DB_STATUS := 'ACCESS DENIED';
  end if;
EXCEPTION
  --
  WHEN NO_INST THEN
    DB_STATUS := 'FORM *ERROR* No records found on GUBIPRF.';
    RAISE_APPLICATION_ERROR(-20301, DB_STATUS);
    --
  WHEN NO_ACCESS THEN
    DB_STATUS := 'FORM *ERROR* User %01% not authorized to access %02%';
    RAISE_APPLICATION_ERROR(-20302, DB_STATUS);
    --
  WHEN NO_PASSWORD THEN
    DB_STATUS := 'FORM *ERROR* No password found on GUBROLE.';
    RAISE_APPLICATION_ERROR(-20303, DB_STATUS);
    --
  WHEN INVALID_VERSION THEN
    DB_STATUS := 'FORM *ERROR* Invalid version of %01%.';
    RAISE_APPLICATION_ERROR(-20304, DB_STATUS);
    --
  WHEN INVALID_ACCESS THEN
    DB_STATUS := 'FORM *ERROR* Invalid password tried.';
    RAISE_APPLICATION_ERROR(-20305, DB_STATUS);
    --
  WHEN OTHERS THEN
    --SWAGLOGS.P_DB_ERRORLOG('Error in SWAGCTRL Procedure - ' || SQLERRM);
    DB_STATUS := ' ERROR- ' || SQLERRM;
END PZ_ME_AUTHENTICATION;
/

