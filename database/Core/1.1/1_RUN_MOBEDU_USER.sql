PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start 1_RUN_MOBEDU_USER.sql"   
PROMPT ====================================================

spool 1_RUN_MOBEDU_USER.log

@@patch\Types.sql
@@patch\Type\T_ARRAY_TYPE.sql
@@patch\Table\MOBEDU_CART.sql
@@patch\Procedure\PZ_ME_AUTHENTICATION.sql
@@patch\Package\ADD_COURSE_PKG_CORQ.sql
@@patch\Package\CART_PKG.sql
@@patch\Package\DROP_COURSE_PKG.sql
@@patch\Package\ME_ACCOUNT_OBJECTS.sql
@@patch\Package\ME_ALT_PIN_PKG.sql
@@patch\Package\ME_BWCKCOMS.sql
@@patch\Package\ME_BWCKGENS.sql
@@patch\Package\ME_BWCKREGS.sql
@@patch\Package\ME_REG_UTILS.sql
@@patch\Package\ME_VALID.sql
@@patch\Package\ME_WITHDRAWL.sql
@@patch\Package\Z_CM_MOBILE_CAMPUS.sql
@@patch\Package\ME_VAR_CREDIT.sql
@@patch\View\ACCOUNT_SUMMARY_VIEW.sql
@@patch\View\ACCT_SUMM_VW.sql
@@patch\View\CART_DETAILS_VW.sql
@@patch\View\CHARGE_PAY_DETAIL_VW.sql
@@patch\View\CONTACT_VW.sql
@@patch\View\TERMS_TO_REGISTER_VW.sql
@@patch\View\COURSE_SEARCH_VW.sql
@@patch\View\CRSE_MEET_VW.sql
@@patch\View\GENERAL_PERSON_VW.sql
@@patch\View\ME_HOURS_SUMMARY_BY_TERM.sql
@@patch\View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@patch\View\MOBEDU_HOLIDAY_VW.sql
@@patch\View\PERSON_CAMPUS_VW.sql
@@patch\View\PERSON_VW.sql
@@patch\View\STUDENT_ACCOUNT_DETAIL.sql
@@patch\View\STU_CURCULAM_INFO.sql
@@patch\View\STU_HOLD_INFO.sql
@@patch\View\STU_PROFILE_INFO.sql
@@patch\View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@patch\View\SUMMARY_BY_TERM_VW.sql
@@patch\View\TERMS_TO_BURSAR_VW.sql
@@patch\View\TERM_COURSE_DETAILS_VW.sql
@@patch\View\VW_TERM_CHARGES.sql
@@patch\View\VW_TERM_PAYMENTS.sql
@@patch\PackageBody\ADD_COURSE_PKG_CORQ.sql
@@patch\PackageBody\CART_PKG.sql
@@patch\PackageBody\DROP_COURSE_PKG.sql
@@patch\PackageBody\ME_ACCOUNT_OBJECTS.sql
@@patch\PackageBody\ME_ALT_PIN_PKG.sql
@@patch\PackageBody\ME_BWCKCOMS.sql
@@patch\PackageBody\ME_BWCKGENS.sql
@@patch\PackageBody\ME_BWCKREGS.sql
@@patch\PackageBody\ME_REG_UTILS.sql
@@patch\PackageBody\ME_VALID.sql
@@patch\PackageBody\ME_WITHDRAWL.sql
@@patch\PackageBody\Z_CM_MOBILE_CAMPUS.sql
@@patch\PackageBody\ME_VAR_CREDIT.sql

EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
