spool  INVALID_OBJECTS.log
PROMPT ====================================================
PROMPT Gathering invalid objects for MOBEDU/MOBILE User
PROMPT ====================================================
select object_type,count(*) from user_objects where status = 'INVALID' 
group by object_type;
select object_name, object_type  from user_objects where status = 'INVALID';
PROMPT ====================================================================================
PROMPT Contact us if this you see any invalid objects with results from INVALID_OBJECTS.log
PROMPT ====================================================================================
show errors;
spool off