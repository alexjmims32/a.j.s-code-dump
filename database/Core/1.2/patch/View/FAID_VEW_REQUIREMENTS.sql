--------------------------------------------------------
--  DDL for View FAID_VEW_REQUIREMENTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_VEW_REQUIREMENTS" ("ID", "FULL_NAME", "LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "PIDM", "REQUIREMENT_CODE", "REQUIREMENT_DESC", "STAT_DATE", "REQ_STATUS_DESC", "SAT_IND", "FUND_CODE", "FUND_TITLE", "SBGI_TYPE_IND", "SBGI_CODE", "SBGI_DESC", "TERM_CODE", "TERM_DESC", "URL", "INSTRUCTIONS", "AIDY_CODE", "AIDY_DESC", "RTVTREQ_ACCESS_IND", "RTVTRST_ACCESS_IND", "RFRBASE_ACCESS_IND", "RRRAREQ_ACCESS_IND", "REFRESH_TIMESTAMP") AS 
  SELECT SPRIDEN_ID ID,
       (SPRIDEN_LAST_NAME ||', '||SPRIDEN_FIRST_NAME||' '||SPRIDEN_MI) FULL_NAME,
       SPRIDEN_LAST_NAME LAST_NAME,
       SPRIDEN_FIRST_NAME FIRST_NAME,
       SPRIDEN_MI MIDDLE_NAME,
       rrrareq_pidm pidm,
       rrrareq_treq_code requirement_code,
       nvl2(rrrareq_treq_code,rtvtreq_long_desc,rrrareq_treq_desc) requirement_desc,
       rrrareq_stat_date stat_date,
       NVL(rtvtrst_long_desc, rtvtrst_desc) req_status_desc,
       rrrareq_sat_ind sat_ind,
       rrrareq_fund_code fund_code,
       y.rfrbase_fund_title fund_title,
       rrrareq_sbgi_type_ind sbgi_type_ind,
       rrrareq_sbgi_code sbgi_code,
       z.stvsbgi_desc sbgi_desc,
       rrrareq_period term_code,
       a.robprds_desc term_desc,
       rtvtreq_url url,
       DECODE(rtvtreq_instructions, '', 'N', 'Y') instructions,
       rrrareq_aidy_code aidy_code,
       robinst_aidy_desc aidy_desc,
       rtvtreq_info_access_ind rtvtreq_access_ind,
       rtvtrst_info_access_ind rtvtrst_access_ind,
       NVL(y.rfrbase_info_access_ind, 'Y') rfrbase_access_ind,
       rrrareq_info_access_ind rrrareq_access_ind,
       sysdate refresh_timestamp
  FROM rtvtrst, spriden, robinst, rrrareq x
  left outer join rtvtreq
    on (rtvtreq_code=rrrareq_treq_code)
  left outer join rfrbase y
    on (x.rrrareq_fund_code = y.rfrbase_fund_code)
  left outer join stvsbgi z
    on (x.rrrareq_sbgi_code = z.stvsbgi_code and
       x.rrrareq_sbgi_type_ind = z.stvsbgi_type_ind)
  left outer join robprds a
    on (x.rrrareq_period = a.robprds_period)
 WHERE rrrareq_trst_code = rtvtrst_code
   AND spriden_pidm=rrrareq_pidm
   AND spriden_change_ind is null
   AND robinst_aidy_code=rrrareq_aidy_code
 /* rrrareq_info_access_ind = 'Y'
   AND (rrrareq_treq_code is NULL or
        rtvtreq_info_access_ind='Y')*/
   --AND (p_sat_ind = 'A' OR rrrareq_sat_ind = p_sat_ind)
   --AND (p_disb_ind = 'A' OR rrrareq_disb_ind = p_disb_ind)
   /*AND*/
   --AND rtvtrst_info_access_ind = 'Y'
   --AND rrrareq_treq_code = rtvtreq_code
   --AND rtvtreq_info_access_ind = 'Y'
   --AND NVL(y.rfrbase_info_access_ind, 'Y') = 'Y'
   ORDER BY sat_ind,
          requirement_desc,
          fund_code nulls last,
          sbgi_code nulls last,
          term_code nulls last
;
