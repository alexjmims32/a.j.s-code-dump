--------------------------------------------------------
--  DDL for View FAID_REQUIREMENTS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_REQUIREMENTS" ("REQUIREMENT_CODE", "REQUIREMENT_DESC", "STAT_DATE", "REQ_STATUS_DESC", "SAT_IND", "FUND_CODE", "FUND_TITLE", "SBGI_TYPE_IND", "SBGI_CODE", "SBGI_DESC", "TERM_CODE", "TERM_DESC", "URL", "INSTRUCTIONS", "PIDM", "AIDY_CODE") AS 
  SELECT rrrareq_treq_code requirement_code,
       rtvtreq_long_desc requirement_desc,
       rrrareq_stat_date stat_date,
       NVL(rtvtrst_long_desc, rtvtrst_desc) req_status_desc,
       rrrareq_sat_ind sat_ind,
       rrrareq_fund_code fund_code,
       y.rfrbase_fund_title fund_title,
       rrrareq_sbgi_type_ind sbgi_type_ind,
       rrrareq_sbgi_code sbgi_code,
       z.stvsbgi_desc sbgi_desc,
       rrrareq_period term_code,
       a.robprds_desc term_desc,
       rtvtreq_url url,
       DECODE(rtvtreq_instructions, '', 'N', 'Y') instructions,
       rrrareq_pidm pidm,
       rrrareq_aidy_code aidy_code
  FROM rtvtreq, rtvtrst, rrrareq x
  left outer join rfrbase y
    on (x.rrrareq_fund_code = y.rfrbase_fund_code)
  left outer join stvsbgi z
    on (x.rrrareq_sbgi_code = z.stvsbgi_code and
       x.rrrareq_sbgi_type_ind = z.stvsbgi_type_ind)
  left outer join robprds a
    on (x.rrrareq_period = a.robprds_period)
 WHERE rrrareq_info_access_ind = 'Y'
   AND rrrareq_treq_code IS NOT NULL
   --AND (p_sat_ind = 'A' OR rrrareq_sat_ind = p_sat_ind)
   --AND (p_disb_ind = 'A' OR rrrareq_disb_ind = p_disb_ind)
   AND rrrareq_trst_code = rtvtrst_code
   AND rtvtrst_info_access_ind = 'Y'
   AND rrrareq_treq_code = rtvtreq_code
   AND rtvtreq_info_access_ind = 'Y'
   AND NVL(y.rfrbase_info_access_ind, 'Y') = 'Y'
UNION
SELECT rrrareq_treq_code requirement_code,
       rrrareq_treq_desc requirement_desc,
       rrrareq_stat_date stat_date,
       NVL(rtvtrst_long_desc, rtvtrst_desc) trst_desc,
       rrrareq_sat_ind sat_ind,
       rrrareq_fund_code fund_code,
       c.rfrbase_fund_title fund_title,
       rrrareq_sbgi_type_ind sbgi_type_ind,
       rrrareq_sbgi_code sbgi_code,
       d.stvsbgi_desc sbgi_desc,
       rrrareq_period term_code,
       e.robprds_desc term_desc,
       '' url,
       'N' instructions,
       rrrareq_pidm pidm,
       rrrareq_aidy_code aidy_code
  FROM rtvtrst, rrrareq b
  left outer join rfrbase c
    on (b.rrrareq_fund_code = c.rfrbase_fund_code)
  left outer join stvsbgi d
    on (b.rrrareq_sbgi_code = d.stvsbgi_code AND
       b.rrrareq_sbgi_type_ind = d.stvsbgi_type_ind)
  left outer join robprds e
    on (b.rrrareq_period = e.robprds_period)
 WHERE rrrareq_info_access_ind = 'Y'
   AND rrrareq_treq_code IS NULL
   --AND (p_sat_ind = 'A' OR rrrareq_sat_ind = p_sat_ind)
   AND rrrareq_trst_code = rtvtrst_code
   --AND (p_disb_ind = 'A' OR rrrareq_disb_ind = p_disb_ind)
   AND rtvtrst_info_access_ind = 'Y'
   AND NVL(c.rfrbase_info_access_ind, 'Y') = 'Y'
UNION
SELECT '' requirement_code,
       NVL(rfraspc_pn_desc, 'Promissory Note') requirement_desc,
       NVL(rfrprom_sat_date, SYSDATE) stat_date,
       DECODE(rfrprom_sat_ind, 'Y', 'Signed', 'Not Signed') trst_desc,
       rfrprom_sat_ind sat_ind,
       rfrprom_fund_code fund_code,
       rfrbase_fund_title fund_title,
       '' sbgi_type_ind,
       '' sbgi_code,
       '' sbgi_desc,
       rfrprom_period term_code,
       h.robprds_desc term_desc,
       rfraspc_pn_url url,
       DECODE(rfraspc_pn_instructions, '', 'N', 'Y') instructions,
       rfrprom_pidm pidm,
       rfrprom_aidy_code aidy_code
  FROM rfrbase, rfraspc, rfrprom f
  left outer join robprds h
    on (f.rfrprom_period = h.robprds_period)
 WHERE rfrprom_dl_seq_no IS NULL
   AND rfrprom_info_access_ind = 'Y'
   --AND (p_sat_ind = 'A' OR rfrprom_sat_ind = p_sat_ind)
   AND rfraspc_aidy_code = rfrprom_aidy_code
   AND rfraspc_fund_code = rfrprom_fund_code
   AND rfraspc_display_pn_ind = 'Y'
   AND NVL(rfrprom_dl_note_status, '#') <> 'D'
   AND rfrbase_fund_code = rfrprom_fund_code
   AND rfrbase_info_access_ind = 'Y'
   AND EXISTS (SELECT 'Y'
          FROM rprawrd
         WHERE rprawrd_pidm = f.rfrprom_pidm
           AND rprawrd_fund_code = f.rfrprom_fund_code
           AND rprawrd_aidy_code = f.rfrprom_aidy_code
           AND rprawrd_offer_amt > 0)
UNION
SELECT '' requirement_code,
       NVL(rfraspc_pn_desc, 'Promissory Note') requirement_desc,
       NVL(rfrprom_sat_date, SYSDATE) stat_date,
       DECODE(rfrprom_sat_ind, 'Y', 'Signed', 'Not Signed') trst_desc,
       rfrprom_sat_ind sat_ind,
       rfrprom_fund_code fund_code,
       rfrbase_fund_title fund_title,
       '' sbgi_type_ind,
       '' sbgi_code,
       '' sbgi_desc,
       rfrprom_period term_code,
       k.robprds_desc term_desc,
       rfraspc_pn_url url,
       DECODE(rfraspc_pn_instructions, '', 'N', 'Y') instructions,
       rfrprom_pidm pidm,
       rfrprom_aidy_code aidy_code
  FROM rfrbase, rfraspc, rfrprom i
  left outer join robprds k
    on (i.rfrprom_period = k.robprds_period)
 WHERE rfrprom_dl_seq_no IS NOT NULL
   AND NVL(rfrprom_dl_note_status, 'A') <> 'A'
   --AND (p_sat_ind = 'A' OR rfrprom_sat_ind = p_sat_ind)
   AND rfraspc_aidy_code = rfrprom_aidy_code
   AND rfraspc_fund_code = rfrprom_fund_code
   AND rfraspc_display_pn_ind = 'Y'
   AND NVL(rfrprom_dl_note_status, '#') <> 'D'
   AND rfrbase_fund_code = rfrprom_fund_code
   AND rfrbase_info_access_ind = 'Y'
   AND EXISTS (SELECT 'Y'
          FROM rtvlnst, rprlapp
         WHERE rprlapp_pidm = i.rfrprom_pidm
           AND rprlapp_fund_code = i.rfrprom_fund_code
           AND rprlapp_dl_seq_no = i.rfrprom_dl_seq_no
           AND rprlapp_aidy_code = i.rfrprom_aidy_code
           AND rprlapp_lnst_code = rtvlnst_code
           AND rtvlnst_info_access_ind = 'Y')
   AND EXISTS (SELECT 'Y'
          FROM rprawrd
         WHERE rprawrd_pidm = i.rfrprom_pidm
           AND rprawrd_fund_code = i.rfrprom_fund_code
           AND rprawrd_aidy_code = i.rfrprom_aidy_code
           AND rprawrd_offer_amt > 0)
 ORDER BY sat_ind,
          requirement_desc,
          fund_code nulls last,
          sbgi_code nulls last,
          term_code nulls last

;
