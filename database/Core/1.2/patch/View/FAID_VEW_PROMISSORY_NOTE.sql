--------------------------------------------------------
--  DDL for View FAID_VEW_PROMISSORY_NOTE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_VEW_PROMISSORY_NOTE" ("ID", "FULL_NAME", "LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "PIDM", "REQUIREMENT_DESC", "STAT_DATE", "TRST_DESC", "SAT_IND", "FUND_CODE", "FUND_TITLE", "TERM_CODE", "TERM_DESC", "URL", "INSTRUCTIONS", "AIDY_CODE", "AIDY_DESC", "RFRBASE_ACCESS_IND", "DISPLAY_PN_IND", "REFRESH_TIMESTAMP") AS 
  SELECT SPRIDEN_ID ID,
       (SPRIDEN_LAST_NAME ||', '||SPRIDEN_FIRST_NAME||' '||SPRIDEN_MI) FULL_NAME,
       SPRIDEN_LAST_NAME LAST_NAME,
       SPRIDEN_FIRST_NAME FIRST_NAME,
       SPRIDEN_MI MIDDLE_NAME,
       rfrprom_pidm pidm,
       NVL(rfraspc_pn_desc, 'Promissory Note') requirement_desc,
       NVL(rfrprom_sat_date, SYSDATE) stat_date,
       DECODE(rfrprom_sat_ind, 'Y', 'Signed', 'Not Signed') trst_desc,
       rfrprom_sat_ind sat_ind,
       rfrprom_fund_code fund_code,
       rfrbase_fund_title fund_title,
       rfrprom_period term_code,
       robprds_desc term_desc,
       rfraspc_pn_url url,
       DECODE(rfraspc_pn_instructions, '', 'N', 'Y') instructions,
       rfrprom_aidy_code aidy_code,
       robinst_aidy_desc aidy_desc,
       rfrbase_info_access_ind rfrbase_access_ind,
       rfraspc_display_pn_ind display_pn_ind,
       sysdate refresh_timestamp
  FROM rfrbase, rfraspc, rfrprom f, spriden, robinst,robprds
 WHERE rfrprom_dl_seq_no IS NULL
   AND rfrprom_info_access_ind = 'Y'
   AND spriden_pidm=rfrprom_pidm
   AND spriden_change_ind is null
   and robinst_aidy_code=rfrprom_aidy_code
   and rfrprom_period = robprds_period(+)
   --AND (p_sat_ind = 'A' OR rfrprom_sat_ind = p_sat_ind)
   AND rfraspc_aidy_code = rfrprom_aidy_code
   AND rfraspc_fund_code = rfrprom_fund_code
   --AND rfraspc_display_pn_ind = 'Y'
   AND NVL(rfrprom_dl_note_status, '#') <> 'D'
   AND rfrbase_fund_code = rfrprom_fund_code
   --AND rfrbase_info_access_ind = 'Y'
   AND EXISTS (SELECT 'Y'
          FROM rprawrd
         WHERE rprawrd_pidm = f.rfrprom_pidm
           AND rprawrd_fund_code = f.rfrprom_fund_code
           AND rprawrd_aidy_code = f.rfrprom_aidy_code
           AND rprawrd_offer_amt > 0)
UNION
SELECT SPRIDEN_ID ID,
       (SPRIDEN_LAST_NAME ||', '||SPRIDEN_FIRST_NAME||' '||SPRIDEN_MI) FULL_NAME,
       SPRIDEN_LAST_NAME LAST_NAME,
       SPRIDEN_FIRST_NAME FIRST_NAME,
       SPRIDEN_MI MIDDLE_NAME,
       rfrprom_pidm pidm,
       NVL(rfraspc_pn_desc, 'Promissory Note') requirement_desc,
       NVL(rfrprom_sat_date, SYSDATE) stat_date,
       DECODE(rfrprom_sat_ind, 'Y', 'Signed', 'Not Signed') trst_desc,
       rfrprom_sat_ind sat_ind,
       rfrprom_fund_code fund_code,
       rfrbase_fund_title fund_title,
       rfrprom_period term_code,
       k.robprds_desc term_desc,
       rfraspc_pn_url url,
       DECODE(rfraspc_pn_instructions, '', 'N', 'Y') instructions,
       rfrprom_aidy_code aidy_code,
       robinst_aidy_desc aidy_desc,
       rfrbase_info_access_ind rfrbase_access_ind,
       rfraspc_display_pn_ind display_pn_ind,
       sysdate refresh_timestamp
  FROM rfrbase, rfraspc, spriden, robinst, rfrprom i
  left outer join robprds k
    on (i.rfrprom_period = k.robprds_period)
 WHERE rfrprom_dl_seq_no IS NOT NULL
   AND NVL(rfrprom_dl_note_status, 'A') <> 'A'
   AND spriden_pidm=rfrprom_pidm
   AND spriden_change_ind is null
   and robinst_aidy_code=rfrprom_aidy_code
   --AND (p_sat_ind = 'A' OR rfrprom_sat_ind = p_sat_ind)
   AND rfraspc_aidy_code = rfrprom_aidy_code
   AND rfraspc_fund_code = rfrprom_fund_code
   --AND rfraspc_display_pn_ind = 'Y'
   AND NVL(rfrprom_dl_note_status, '#') <> 'D'
   AND rfrbase_fund_code = rfrprom_fund_code
   --AND rfrbase_info_access_ind = 'Y'
   AND EXISTS (SELECT 'Y'
          FROM rtvlnst, rprlapp
         WHERE rprlapp_pidm = i.rfrprom_pidm
           AND rprlapp_fund_code = i.rfrprom_fund_code
           AND rprlapp_dl_seq_no = i.rfrprom_dl_seq_no
           AND rprlapp_aidy_code = i.rfrprom_aidy_code
           AND rprlapp_lnst_code = rtvlnst_code
           AND rtvlnst_info_access_ind = 'Y')
   AND EXISTS (SELECT 'Y'
          FROM rprawrd
         WHERE rprawrd_pidm = i.rfrprom_pidm
           AND rprawrd_fund_code = i.rfrprom_fund_code
           AND rprawrd_aidy_code = i.rfrprom_aidy_code
           AND rprawrd_offer_amt > 0)
 ORDER BY sat_ind,
          requirement_desc,
          fund_code nulls last,
          term_code nulls last
;
