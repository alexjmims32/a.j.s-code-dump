--------------------------------------------------------
--  DDL for View FAID_LINKS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_LINKS" ("MENU_NAME", "MENU_SEQUENCE", "URL_TEXT", "URL", "URL_DESC", "IMAGE", "ENABLED", "SOURCE_IND") AS 
  select TWGRMENU_NAME MENU_NAME,
       TWGRMENU_SEQUENCE MENU_SEQUENCE,
       TWGRMENU_URL_TEXT URL_TEXT,
       TWGRMENU_URL URL,
       TWGRMENU_URL_DESC URL_DESC,
       TWGRMENU_IMAGE IMAGE,
       TWGRMENU_ENABLED ENABLED,
       TWGRMENU_SOURCE_IND SOURCE_IND
  from twgrmenu
 where twgrmenu_name = 'bmenu.P_FAASecMnu'
   and twgrmenu_enabled = 'Y'
   and twgrmenu_source_ind =
       (select nvl(null, nvl(max(twgrmenu_source_ind), 'B'))
          from twgrmenu
         where twgrmenu_name = 'bmenu.P_FAASecMnu'
           and twgrmenu_source_ind = 'L')
 order by twgrmenu_sequence

;
