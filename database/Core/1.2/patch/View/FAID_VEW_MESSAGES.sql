--------------------------------------------------------
--  DDL for View FAID_VEW_MESSAGES
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_VEW_MESSAGES" ("ID", "FULL_NAME", "LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "PIDM", "TREQ_CODE", "REQUIREMENT_DESC", "MESSAGE_CODE", "MESSAGE_DESC", "AIDY_CODE", "AIDY_DESC", "RRRAREQ_ACCESS_IND", "RTVTREQ_ACCESS_IND", "RTVMESG_ACCESS_IND", "RFRBASE_ACCESS_IND", "RTVTRST_ACCESS_IND", "REFRESH_TIMESTAMP") AS 
  SELECT distinct SPRIDEN_ID ID,
                (SPRIDEN_LAST_NAME || ', ' || SPRIDEN_FIRST_NAME || ' ' ||
                SPRIDEN_MI) FULL_NAME,
                SPRIDEN_LAST_NAME LAST_NAME,
                SPRIDEN_FIRST_NAME FIRST_NAME,
                SPRIDEN_MI MIDDLE_NAME,
                rrrareq_pidm pidm,
                rrrareq_treq_code treq_code,
                rtvtreq_long_desc requirement_desc,
                rtvmesg_code message_code,
                rtvmesg_mesg_desc message_desc,
                rrrareq_aidy_code aidy_code,
                robinst_aidy_desc aidy_desc,
                rrrareq_info_access_ind rrrareq_access_ind,
                rtvtreq_info_access_ind rtvtreq_access_ind,
                rtvmesg_info_access_ind rtvmesg_access_ind,
                rfrbase_info_access_ind rfrbase_access_ind,
                rtvtrst_info_access_ind rtvtrst_access_ind,
                sysdate refresh_timestamp
  FROM rtvmesg, rrrtmsg, rtvtreq, rrrareq x, spriden, rfrbase, rtvtrst ,robinst
 WHERE spriden_pidm = rrrareq_pidm
   AND spriden_change_ind is null
      -- AND rrrareq_info_access_ind = 'Y'
   AND rrrareq_treq_code IS NOT NULL
   AND robinst_aidy_code= rrrareq_aidy_code
   AND rrrareq_sat_ind = 'N'
   AND rrrareq_fund_code = rfrbase_fund_code(+)
      /*OR   EXISTS
          ( SELECT 'Y'
              FROM rfrbase
             WHERE rfrbase_fund_code       = x.rrrareq_fund_code
               \*AND rfrbase_info_access_ind = 'Y' *\)
      )*/
   AND rrrareq_treq_code = rtvtreq_code
      --AND rtvtreq_info_access_ind = 'Y'
   AND rtvtreq_code = rrrtmsg_treq_code
   AND rrrtmsg_aidy_code = rrrareq_aidy_code
   AND rrrtmsg_mesg_code = rtvmesg_code
      -- AND rtvmesg_info_access_ind = 'Y'
   AND rtvtrst_code = rtvtrst_code
/*AND EXISTS
( SELECT 'Y'
    FROM rtvtrst
   WHERE rtvtrst_code            = x.rtvtrst_code
     \*AND rtvtrst_info_access_ind = 'Y'*\ )*/
 ORDER BY rrrareq_treq_code, rtvmesg_mesg_desc
;
