--------------------------------------------------------
--  DDL for View FAID_RESOURCE
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_RESOURCE" ("RESOURCE_TYPE", "CONTRACT_PIDM", "RESOURCE_CODE", "RESOURCE_DESC", "TERM_CODE", "EXPECTED_AMT", "AMOUNT", "ACCESS_IND", "ACTIVITY_DATE", "PIDM", "AIDY_CODE", "TERM_DESC") AS 
  SELECT 'C' resource_type,
       tbbcstu_contract_pidm contract_pidm,
       LPAD(SUBSTR(TO_CHAR(tbbcstu_contract_number), 1, 8), 8, '0') resource_code,
       tbbcont_desc resource_desc,
       tbbcstu_term_code term_code,
       NVL(NVL(tbbcstu_max_student_amount, rprcont_estimated_amt), 0) expected_amt, -- 70800-1
       rovcsum_v.sum_amount amount,
       rprcont_info_access_ind access_ind, -- 80200-1
       NVL(rovcsum_v.activity_date, tbbcstu_activity_date) activity_date,
       tbbcstu_stu_pidm pidm,
       robinst_aidy_code aidy_code,
       STVTERM_DESC term_desc
  FROM (SELECT tbraccd_pidm pidm,
               tbraccd_crossref_pidm contract_pidm,
               tbraccd_crossref_number contract_number,
               tbraccd_term_code term_code,
               SUM(tbraccd_amount) sum_amount,
               MAX(tbraccd_activity_date) activity_date
          FROM tbraccd
         WHERE tbraccd_srce_code = 'C'
         GROUP BY tbraccd_pidm,
                  tbraccd_crossref_pidm,
                  tbraccd_crossref_number,
                  tbraccd_term_code) rovcsum_v,
       rprcont,
       tbbcont,
       tbbcstu,
       robinst,
       STVTERM
 WHERE tbbcstu_stu_pidm = rovcsum_v.pidm(+)
   AND tbbcstu_contract_pidm = rovcsum_v.contract_pidm(+)
   AND tbbcstu_contract_number = rovcsum_v.contract_number(+)
   AND tbbcstu_term_code = rovcsum_v.term_code(+)
   AND tbbcstu_contract_pidm = tbbcont_pidm
   AND tbbcstu_contract_number = tbbcont_contract_number
   AND tbbcstu_term_code = tbbcont_term_code
   AND tbbcstu_contract_pidm = rprcont_contract_pidm(+)
   AND tbbcstu_contract_number = rprcont_contract_no(+)
   AND tbbcstu_term_code = rprcont_term_code(+)
   AND tbbcstu_term_code = STVTERM_CODE(+)
   AND EXISTS
 (SELECT rorprds_term_code -- 80900-1
          FROM rorprds, robprds
         WHERE rorprds_period = robprds_period
           AND robinst_aidy_code IN (robprds_aidy_code, robprds_aidy_code_cross)
           AND rorprds_term_code = tbbcstu_term_code)
   AND NVL(tbbcstu_del_ind, 'X') <> 'D'
   AND NVL(rprcont_exclude_resource, 'N') = 'N' -- 70800-1
   AND EXISTS (SELECT rpbopts_interface_3party_ind ind
          FROM rpbopts
         WHERE rpbopts_aidy_code = robinst_aidy_code
           AND rpbopts_interface_3party_ind = 'Y')
union
SELECT 'E',
       0,
       tbbestu_exemption_code,
       tbbexpt_desc,
       tbbestu_term_code,
       NVL(NVL(tbbestu_max_student_amount, rprexpt_estimated_amt), 0), -- 70800-1
       rovesum_v.sum_amount,
       rprexpt_info_access_ind, -- 80200-1
       NVL(rovesum_v.activity_date, tbbestu_activity_date),
       tbbestu_pidm pidm,
       robinst_aidy_code aidy_code,
       STVTERM_DESC term_desc
  FROM (SELECT tbraccd_pidm pidm,
               tbraccd_crossref_number exemption_code,
               tbraccd_term_code term_code,
               NVL(SUM(tbraccd_amount), 0) sum_amount,
               MAX(tbraccd_activity_date) activity_date
          FROM tbraccd
         WHERE tbraccd_srce_code = 'E'
         group by tbraccd_pidm, tbraccd_crossref_number, tbraccd_term_code) rovesum_v,
       rprexpt,
       tbbexpt,
       tbbestu,
       robinst,
       stvterm
 WHERE tbbestu_pidm = rovesum_v.pidm(+)
   AND tbbestu_exemption_code = rovesum_v.exemption_code(+)
   AND tbbestu_term_code = rovesum_v.term_code(+)
   AND tbbestu_exemption_code = tbbexpt_exemption_code
   AND tbbestu_term_code = tbbexpt_term_code
   AND tbbestu_exemption_code = rprexpt_exemption_code(+)
   AND tbbestu_term_code = rprexpt_term_code(+)
   AND tbbestu_term_code = STVTERM_CODE(+)
   AND EXISTS
 (SELECT rorprds_term_code -- 80900-1
          FROM rorprds, robprds
         WHERE rorprds_period = robprds_period
           AND robinst_aidy_code IN (robprds_aidy_code, robprds_aidy_code_cross)
           AND rorprds_term_code = tbbestu_term_code)
   AND NVL(tbbestu_del_ind, 'X') <> 'D'
   AND NVL(rprexpt_exclude_resource, 'N') = 'N' -- 70800-1
   AND EXISTS (SELECT rpbopts_interface_exempt_ind
          FROM rpbopts
         WHERE rpbopts_aidy_code = robinst_aidy_code
           AND rpbopts_interface_exempt_ind = 'Y')
;
