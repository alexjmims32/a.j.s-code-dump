PROMPT CREATE OR REPLACE VIEW faid_vew_student_summary
CREATE OR REPLACE VIEW faid_vew_student_summary (
  id,
  full_name,
  last_name,
  first_name,
  middle_name,
  pidm,
  aidy_code,
  aidy_desc,
  period_code,
  period,
  budget_group_code,
  budget_group,
  budget_amount,
  award_amount,
  sap_term_code,
  sap_status,
  sat_count,
  usat_count,
  total_req,
  holds_count,
  loan_paid_history,
  payment_amount,
  refresh_timestamp
) AS
SELECT SPRIDEN_ID ID,
       (SPRIDEN_LAST_NAME || ', ' || SPRIDEN_FIRST_NAME || ' ' ||
       SPRIDEN_MI) FULL_NAME,
       SPRIDEN_LAST_NAME LAST_NAME,
       SPRIDEN_FIRST_NAME FIRST_NAME,
       SPRIDEN_MI MIDDLE_NAME,
       SPRIDEN_PIDM PIDM,
       RORSTAT_AIDY_CODE AIDY_CODE,
       ROBINST_AIDY_DESC AIDY_DESC,
       RORSTAT_APRD_CODE Period_Code,
       RTVAPRD_DESC Period,
       RORSTAT_BGRP_CODE Budget_Group_Code,
       RTVBGRP_DESC Budget_group,
       nvl(bwrkbudg.f_get_budget_total(RORSTAT_AIDY_CODE, RORSTAT_PIDM),0) Budget_Amount,
       mobedu.faid_pkg.fz_get_award_total(RORSTAT_PIDM, RORSTAT_AIDY_CODE) Award_Amount,
       STVTERM_DESC SAP_Term_Code,
       RTVSAPR_DESC SAP_Status,
       R.SAT_COUNT,
       R.USAT_COUNT,
       R.TOTAL_REQ,
       (SELECT COUNT(*)
          FROM RORHOLD Y, RTVHOLD
         WHERE SYSDATE BETWEEN RORHOLD_FROM_DATE AND RORHOLD_TO_DATE
           AND Y.RORHOLD_HOLD_CODE = RTVHOLD_CODE
           AND RTVHOLD_INFO_ACCESS_IND = 'Y'
           AND RORHOLD_FUND_CODE IS NULL
           AND RORSTAT_PIDM = RORHOLD_PIDM
           AND ((RORHOLD_AIDY_CODE IS NOT NULL AND
               RORHOLD_AIDY_CODE = RORSTAT_AIDY_CODE) OR
               (RORHOLD_AIDY_CODE IS NULL AND RORHOLD_PERIOD IS NULL) OR
               EXISTS((SELECT 'Y'
                         FROM RORTPRD
                        WHERE RORTPRD_AIDY_CODE = RORSTAT_AIDY_CODE
                          AND RORTPRD_APRD_CODE = RORSTAT_APRD_CODE
                          AND RORTPRD_PERIOD = RORHOLD_PERIOD)))) HOLDS_COUNT,
       PAID_AMT LOAN_PAID_HISTORY,
       PAYMENT_AMOUNT,
       SYSDATE REFRESH_TIMESTAMP
  FROM RORSTAT
  JOIN SPRIDEN
    ON SPRIDEN_PIDM = RORSTAT_PIDM
   AND SPRIDEN_CHANGE_IND IS NULL
  JOIN ROBINST
    ON ROBINST_AIDY_CODE = RORSTAT_AIDY_CODE
  LEFT JOIN RTVAPRD
    ON RORSTAT_APRD_CODE = RTVAPRD_CODE
  LEFT JOIN RTVBGRP
    ON RORSTAT_BGRP_CODE = RTVBGRP_CODE
  LEFT JOIN (SELECT RORSAPR_TERM_CODE TERM_CODE,
                    RORSAPR_SAPR_CODE SAPR_CODE,
                    RORSAPR_PIDM PIDM,
                    ROW_NUMBER() OVER(PARTITION BY RORSAPR_PIDM ORDER BY RORSAPR_TERM_CODE DESC) ROW_NUM
               FROM RORSAPR) SAP
    ON SAP.PIDM = RORSTAT_PIDM
   AND SAP.ROW_NUM = 1
  LEFT JOIN STVTERM
    ON SAP.TERM_CODE = STVTERM_CODE
  LEFT JOIN RTVSAPR
    ON SAP.SAPR_CODE = RTVSAPR_CODE
  LEFT JOIN (select RRRAREQ_PIDM PIDM,
                    RRRAREQ_AIDY_CODE AIDY_CODE,
                    COUNT(CASE
                            WHEN RRRAREQ_SAT_IND = 'Y' THEN
                             1
                          END) SAT_COUNT,
                    COUNT(CASE
                            WHEN RRRAREQ_SAT_IND = 'N' THEN
                             1
                          END) USAT_COUNT,
                    COUNT(*) TOTAL_REQ
               from RRRAREQ, rtvtrst
              where rrrareq_info_access_ind = 'Y'
                and (rrrareq_treq_code is null or exists
                     (select 1
                        from rtvtreq
                       where rtvtreq_code = rrrareq_treq_code
                         and rtvtreq_info_access_ind = 'Y'))
                AND rrrareq_trst_code = rtvtrst_code
                AND rtvtrst_info_access_ind = 'Y'
              group by RRRAREQ_PIDM, RRRAREQ_AIDY_CODE) R
    ON R.PIDM = RORSTAT_PIDM
   AND R.AIDY_CODE = RORSTAT_AIDY_CODE
  LEFT JOIN (SELECT RPRLADB_PIDM, --Join to show the loan History
                    SUM(RPRLADB_CHECK_AMT) PAID_AMT,
                    SUM(RPRLADB_CHECK_RETURN_AMT) RETURN_AMT
               FROM RPRLADB
              WHERE RPRLADB_FEED_IND = 'F'
              GROUP BY RPRLADB_PIDM) D
    ON D.RPRLADB_PIDM = RORSTAT_PIDM
  LEFT JOIN (select RPRADSB_PIDM PIDM,
                    RPRADSB_AIDY_CODE AIDY_CODE,
                    round(sum(RPRADSB_DISBURSE_AMT) /
                          (select sum(D.RPRADSB_DISBURSE_AMT)
                             from RPRADSB D
                            where D.RPRADSB_PIDM = RPRADSB_PIDM
                              and D.RPRADSB_AIDY_CODE = RPRADSB_AIDY_CODE),
                          2) PAYMENT_AMOUNT
               from RPRADSB
              where RPRADSB_SCHEDULE_DATE <= sysdate
              group by RPRADSB_PIDM, RPRADSB_AIDY_CODE) PAY
    ON PAY.PIDM = RORSTAT_PIDM
   AND PAY.AIDY_CODE = RORSTAT_AIDY_CODE
/

