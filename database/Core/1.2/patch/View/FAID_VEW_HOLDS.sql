--------------------------------------------------------
--  DDL for View FAID_VEW_HOLDS
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_VEW_HOLDS" ("ID", "FULL_NAME", "LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "PIDM", "HOLD_REASON", "FROM_DATE", "HOLD_DESC", "AIDY_CODE", "AIDY_DESC", "RTVHOLD_ACCESS_IND", "REFRESH_TIMESTAMP") AS 
  SELECT SPRIDEN_ID ID,
       (SPRIDEN_LAST_NAME ||', '||SPRIDEN_FIRST_NAME||' '||SPRIDEN_MI) FULL_NAME,
       SPRIDEN_LAST_NAME LAST_NAME,
       SPRIDEN_FIRST_NAME FIRST_NAME,
       SPRIDEN_MI MIDDLE_NAME,
       RORSTAT_PIDM      PIDM,
       RORHOLD_REASON HOLD_REASON,
       RORHOLD_FROM_DATE FROM_DATE,
       RTVHOLD_DESC HOLD_DESC,
       RORSTAT_AIDY_CODE AIDY_CODE,
       ROBINST_AIDY_DESC AIDY_DESC,
       RTVHOLD_INFO_ACCESS_IND RTVHOLD_ACCESS_IND,
       SYSDATE REFRESH_TIMESTAMP
  FROM RORHOLD Y, RTVHOLD, RORSTAT,SPRIDEN,ROBINST
 WHERE SPRIDEN_PIDM=RORSTAT_PIDM
   AND SPRIDEN_CHANGE_IND IS NULL
   AND RORHOLD_HOLD_CODE = RTVHOLD_CODE
   AND ROBINST_AIDY_CODE= RORSTAT_AIDY_CODE
   --AND RTVHOLD_INFO_ACCESS_IND = 'Y'
   AND SYSDATE BETWEEN RORHOLD_FROM_DATE AND RORHOLD_TO_DATE
   AND RORHOLD_FUND_CODE IS NULL
   AND RORSTAT_PIDM = RORHOLD_PIDM
   AND ((RORHOLD_AIDY_CODE IS NOT NULL AND
       RORHOLD_AIDY_CODE = RORSTAT_AIDY_CODE) OR
       (RORHOLD_AIDY_CODE IS NULL AND RORHOLD_PERIOD IS NULL)
       OR EXISTS((SELECT 'Y'
                    FROM RORTPRD
                   WHERE RORTPRD_AIDY_CODE = RORSTAT_AIDY_CODE
                     AND RORTPRD_APRD_CODE = RORSTAT_APRD_CODE
                     AND RORTPRD_PERIOD = RORHOLD_PERIOD)))
 ORDER BY RORHOLD_FROM_DATE DESC
;
