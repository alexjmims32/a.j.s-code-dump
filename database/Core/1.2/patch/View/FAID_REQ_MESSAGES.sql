--------------------------------------------------------
--  DDL for View FAID_REQ_MESSAGES
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "MOBEDU"."FAID_REQ_MESSAGES" ("TREQ_CODE", "REQUIREMENT_DESC", "MESSAGE_CODE", "MESSAGE_DESC", "PIDM", "AIDY_CODE", "ID") AS 
  SELECT distinct rrrareq_treq_code treq_code,
                    rtvtreq_long_desc requirement_desc,
                    rtvmesg_code message_code,
                    rtvmesg_mesg_desc message_desc,
                    rrrareq_pidm pidm,
                    rrrareq_aidy_code aidy_code,
                    spriden_id id
               FROM rtvmesg,
                    rrrtmsg,
                    rtvtreq,
                    rrrareq x,
                    spriden
              WHERE spriden_pidm=rrrareq_pidm
                AND spriden_change_ind is null
                AND rrrareq_info_access_ind = 'Y'
                AND rrrareq_treq_code      IS NOT NULL
                AND rrrareq_sat_ind         = 'N'
                AND ( rrrareq_fund_code    IS NULL
                 OR   EXISTS
                        ( SELECT 'Y'
                            FROM rfrbase
                           WHERE rfrbase_fund_code       = x.rrrareq_fund_code
                             AND rfrbase_info_access_ind = 'Y' )
                    )
                AND rrrareq_treq_code       = rtvtreq_code
                AND rtvtreq_info_access_ind = 'Y'
                AND rtvtreq_code            = rrrtmsg_treq_code
                AND rrrtmsg_aidy_code       = rrrareq_aidy_code
                AND rrrtmsg_mesg_code       = rtvmesg_code
                AND rtvmesg_info_access_ind = 'Y'
                AND EXISTS
                        ( SELECT 'Y'
                            FROM rtvtrst
                           WHERE rtvtrst_code            = x.rrrareq_trst_code
                             AND rtvtrst_info_access_ind = 'Y' )
             ORDER BY rrrareq_treq_code,
                      rtvmesg_mesg_desc
;
