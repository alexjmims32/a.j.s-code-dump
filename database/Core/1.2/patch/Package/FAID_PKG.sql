--------------------------------------------------------
--  DDL for Package FAID_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "MOBEDU"."FAID_PKG" AS
  TYPE budget_ref IS REF CURSOR;
  FUNCTION FZ_GET_AWARD_TOTAL(P_PIDM NUMBER, P_AIDY_CODE VARCHAR2)
    RETURN NUMBER;
  FUNCTION FZ_NON_PELL_COUNT(P_PIDM NUMBER, P_AIDY_CODE VARCHAR2)
    RETURN NUMBER;
  FUNCTION f_unsatisfied_prom_note(p_pidm NUMBER, p_aid_year VARCHAR2)
    RETURN VARCHAR2;
  function F_AllowWebUpdate(pidm IN NUMBER,
                            aidy IN VARCHAR2,
                            fund IN VARCHAR2) RETURN VARCHAR2;
  function F_AllowPartialAccept(pidm IN NUMBER,
                                aidy IN VARCHAR2,
                                fund IN VARCHAR2) RETURN VARCHAR2;
  PROCEDURE PZ_ACCEPT_AWARD(P_ID          VARCHAR2,
                            P_AIDY_CODE   VARCHAR2,
                            P_FUND_CODE   VARCHAR2,
                            P_ACCEPT_CODE VARCHAR2,
                            P_ACCEPT_AMT  NUMBER,
                            P_ERROR       OUT VARCHAR2);
  function F_GetURL(menu_name VARCHAR2, image_name VARCHAR2) RETURN VARCHAR2;
  procedure PZ_GET_T_AND_C(P_ID        VARCHAR2,
                           P_AIDY_CODE VARCHAR2,
                           P_RESULT    OUT CLOB);
                          
  procedure pz_accept_terms(P_ID        VARCHAR2,
                            P_AIDY_CODE VARCHAR2,
                            P_ERROR     OUT VARCHAR2);
END FAID_PKG;

/
