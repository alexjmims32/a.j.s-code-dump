--------------------------------------------------------
--  DDL for Package Body FAID_PKG
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "MOBEDU"."FAID_PKG" AS
  FUNCTION FZ_GET_AWARD_TOTAL(P_PIDM NUMBER, P_AIDY_CODE VARCHAR2)
    RETURN NUMBER AS
    CURSOR C_AWARD_TOTAL IS
      SELECT NVL(SUM(RPRATRM_OFFER_AMT), 0)
        FROM ( -- SCHEDULED AWARDS
              SELECT NVL(SUM(RPRATRM_OFFER_AMT), 0) RPRATRM_OFFER_AMT
                FROM RPRAWRD, RPRATRM, RORSTAT, RFRBASE, RORWEBR
               WHERE RPRAWRD_PIDM = P_PIDM
                 AND NVL(RPRAWRD_INFO_ACCESS_IND, 'Y') = 'Y'
                 AND RFRBASE_INFO_ACCESS_IND = 'Y'
                 AND RPRAWRD_FUND_CODE = RFRBASE_FUND_CODE
                 AND RPRAWRD_AIDY_CODE = RPRATRM_AIDY_CODE
                 AND RPRAWRD_PIDM = RPRATRM_PIDM
                 AND RPRAWRD_PIDM = RORSTAT_PIDM
                 AND RORSTAT_AIDY_CODE = P_AIDY_CODE
                 AND RORWEBR_AIDY_CODE = RORSTAT_AIDY_CODE
                 AND NVL(RORSTAT_INFO_ACCESS_IND, RORWEBR_NULL_INFOACCESS_IND) = 'Y'
                 AND ((RORSTAT_PGRP_CODE IS NOT NULL AND
                     RORSTAT_PGRP_CODE IN
                     (SELECT RTVPGRP_CODE
                          FROM RTVPGRP
                         WHERE RTVPGRP_INFO_ACCESS_IND = 'Y')) OR
                     RORSTAT_PGRP_CODE IS NULL)
                 AND RPRAWRD_FUND_CODE = RPRATRM_FUND_CODE
                 AND ((NVL(RFRBASE_FED_FUND_ID, '*') = 'PELL' AND
                     RPRATRM_OFFER_AMT > 0 AND
                     BWRKOLIB.F_CHECKPELLCROSSOVER(P_AIDY_CODE,
                                                     P_PIDM,
                                                     RPRAWRD_AIDY_CODE,
                                                     RPRATRM_PERIOD) = 'Y') OR
                     (NVL(RFRBASE_FED_FUND_ID, '*') <> 'PELL' AND
                     RPRAWRD_AIDY_CODE = P_AIDY_CODE))
              UNION ALL
              -- UNSCHEDULED AWARDS
              SELECT NVL(SUM(RPRAWRD_OFFER_AMT), 0)
                FROM RPRAWRD, RORSTAT, RFRBASE, RORWEBR
               WHERE RPRAWRD_AIDY_CODE = P_AIDY_CODE
                 AND RPRAWRD_PIDM = P_PIDM
                 AND NVL(RPRAWRD_INFO_ACCESS_IND, 'Y') = 'Y'
                 AND RFRBASE_INFO_ACCESS_IND = 'Y'
                 AND RPRAWRD_FUND_CODE = RFRBASE_FUND_CODE
                 AND RPRAWRD_PIDM = RORSTAT_PIDM
                 AND RPRAWRD_AIDY_CODE = RORSTAT_AIDY_CODE
                 AND RPRAWRD_AIDY_CODE = RORWEBR_AIDY_CODE
                 AND NVL(RORSTAT_INFO_ACCESS_IND, RORWEBR_NULL_INFOACCESS_IND) = 'Y'
                 AND ((RORSTAT_PGRP_CODE IS NOT NULL AND
                     RORSTAT_PGRP_CODE IN
                     (SELECT RTVPGRP_CODE
                          FROM RTVPGRP
                         WHERE RTVPGRP_INFO_ACCESS_IND = 'Y')) OR
                     RORSTAT_PGRP_CODE IS NULL)
                 AND NOT EXISTS
               (SELECT 'X'
                        FROM RPRATRM
                       WHERE RPRAWRD_AIDY_CODE = RPRATRM_AIDY_CODE
                         AND RPRAWRD_PIDM = RPRATRM_PIDM
                         AND RPRAWRD_FUND_CODE = RPRATRM_FUND_CODE)) A;
    V_AWARD_TOTAL NUMBER;
  BEGIN
    OPEN C_AWARD_TOTAL;
    FETCH C_AWARD_TOTAL
      INTO V_AWARD_TOTAL;
    CLOSE C_AWARD_TOTAL;
    RETURN V_AWARD_TOTAL;
  
  END FZ_GET_AWARD_TOTAL;
  FUNCTION FZ_NON_PELL_COUNT(P_PIDM NUMBER, P_AIDY_CODE VARCHAR2)
    RETURN NUMBER AS
    V_NON_PELL_COUNT NUMBER;
  BEGIN
    SELECT COUNT(*)
      INTO V_NON_PELL_COUNT
      FROM RBRACMP, RBBABUD, RTVCOMP
     WHERE RBRACMP_AIDY_CODE = p_aidy_code
       AND RBRACMP_PIDM = p_pidm
       AND RBRACMP_COMP_CODE = RTVCOMP_CODE
       AND RBBABUD_AIDY_CODE = RBRACMP_AIDY_CODE
       AND RBBABUD_PIDM = RBRACMP_PIDM
       AND RBBABUD_BTYP_CODE = RBRACMP_BTYP_CODE
       AND NOT EXISTS (SELECT 'X'
              FROM RTVBTYP
             WHERE RTVBTYP_PELL_IND = 'Y'
               AND RBRACMP_BTYP_CODE = RTVBTYP_CODE);
    RETURN V_NON_PELL_COUNT;
  END FZ_NON_PELL_COUNT;
  FUNCTION f_unsatisfied_prom_note(p_pidm NUMBER, p_aid_year VARCHAR2)
    RETURN VARCHAR2 AS
    v_unsat_prom_note VARCHAR2(1) := 'N';
    CURSOR check_for_rfrprom IS
      SELECT 'Y'
        FROM rfrprom
       WHERE rfrprom_pidm = p_pidm
         AND rfrprom_aidy_code = p_aid_year
         AND rfrprom_sat_ind = 'N';
  BEGIN
    OPEN check_for_rfrprom;
    FETCH check_for_rfrprom
      INTO v_unsat_prom_note;
    CLOSE check_for_rfrprom;
    RETURN v_unsat_prom_note;
  END;
  function F_AllowWebUpdate(pidm IN NUMBER,
                            aidy IN VARCHAR2,
                            fund IN VARCHAR2) RETURN VARCHAR2 IS
    -- This function will return true if accepting the award on the web is allowed.
    -- It must meet the following criteria:
    --   Is Not PELL
    --   Is in Offered status
    --   Aidy is not locked
    --   Award is not locked
    --   The packing group is not locked
    --   Web accept for fund is allowed
  
    ok VARCHAR2(1) := 'N';
  
  begin
    SELECT 'Y'
      INTO ok
      FROM RPRAWRD, RFRASPC, RORSTAT, ROBINST
     WHERE RPRAWRD_PIDM = pidm
       AND RPRAWRD_AIDY_CODE = aidy
       AND RPRAWRD_FUND_CODE = fund
       AND RORSTAT_PIDM = RPRAWRD_PIDM
       AND RORSTAT_AIDY_CODE = RPRAWRD_AIDY_CODE
       AND ROBINST_AIDY_CODE = RPRAWRD_AIDY_CODE
       AND RFRASPC_AIDY_CODE = RPRAWRD_AIDY_CODE
       AND RFRASPC_FUND_CODE = RPRAWRD_FUND_CODE
       AND RFRASPC_WEB_ACCEPT_FLAG = 'Y'
       AND RPRAWRD_AWST_CODE IN
           (SELECT RTVAWST_CODE FROM RTVAWST WHERE RTVAWST_OFFER_IND = 'Y')
       AND RPRAWRD_FUND_CODE NOT IN
           (SELECT RFRBASE_FUND_CODE
              FROM RFRBASE
             WHERE RFRBASE_FED_FUND_ID = 'PELL')
       AND RORSTAT_PGRP_LOCK_IND = 'N'
       AND ROBINST_STATUS_IND = 'A'
       AND ((RPRAWRD_LOCK_IND NOT IN ('Y', 'E') AND NOT EXISTS
            (SELECT 'X'
                FROM RPRATRM
               WHERE RPRATRM_AIDY_CODE = RPRAWRD_AIDY_CODE
                 AND RPRATRM_PIDM = RPRAWRD_PIDM
                 AND RPRATRM_FUND_CODE = RPRAWRD_FUND_CODE
                 AND RPRATRM_LOCK_IND IN ('Y', 'E'))) OR
           ROBINST_UPD_LOCKED_AWARD_IND = 'Y');
  
    RETURN ok;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 'N';
  end;
  function F_AllowPartialAccept(pidm IN NUMBER,
                                aidy IN VARCHAR2,
                                fund IN VARCHAR2) RETURN VARCHAR2 IS
  
    ok VARCHAR2(1) := 'N';
  
  begin
    SELECT 'Y'
      INTO ok
      FROM RPRAWRD
     WHERE RPRAWRD_PIDM = pidm
       AND RPRAWRD_AIDY_CODE = aidy
       AND RPRAWRD_FUND_CODE = fund
       AND RPRAWRD_LOCK_IND NOT IN ('Y', 'E')
       AND NOT EXISTS -- if record exists, then it can not be locked
     (SELECT 'X'
              FROM RPRATRM
             WHERE RPRATRM_AIDY_CODE = RPRAWRD_AIDY_CODE
               AND RPRATRM_PIDM = RPRAWRD_PIDM
               AND RPRATRM_FUND_CODE = RPRAWRD_FUND_CODE
               AND RPRATRM_LOCK_IND IN ('Y', 'E'))
          
       AND NOT EXISTS -- fund can not be ACG, SMART or TEACH for aidy end year > 2009
     (SELECT 'X'
              FROM RFRBASE
             WHERE RFRBASE_FUND_CODE = fund
               AND RFRBASE_FED_FUND_ID IN ('ACG', 'SMRT', 'TCH')
               AND rb_common.f_sel_robinst_aidy_end_year(aidy) > 2009);
  
    RETURN ok;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 'N';
  end;
  function F_GetMinAmt(aidy VARCHAR2, fund VARCHAR2) RETURN NUMBER IS
  
    minamt RFRASPC.RFRASPC_MIN_AWARD_AMT%TYPE := 0;
  
  begin
    SELECT RFRASPC_MIN_AWARD_AMT
      INTO minamt
      FROM RFRASPC
     WHERE RFRASPC_AIDY_CODE = aidy
       AND RFRASPC_FUND_CODE = fund;
    RETURN minamt;
  end f_getminamt;
  PROCEDURE PZ_ACCEPT_AWARD(P_ID          VARCHAR2,
                            P_AIDY_CODE   VARCHAR2,
                            P_FUND_CODE   VARCHAR2,
                            P_ACCEPT_CODE VARCHAR2,
                            P_ACCEPT_AMT  NUMBER,
                            P_ERROR       OUT VARCHAR2) AS
    v_pidm         NUMBER;
    v_accept_code  RFRASPC.RFRASPC_WEB_ACCEPT_AWST_CODE%TYPE;
    v_decline_code RFRASPC.RFRASPC_WEB_DECLINE_AWST_CODE%TYPE;
    v_amount       NUMBER;
    CURSOR status_code_c(aidy ROBINST.ROBINST_AIDY_CODE%TYPE,
                         fund RPRAWRD.RPRAWRD_FUND_CODE%TYPE) IS
      SELECT RFRASPC_WEB_ACCEPT_AWST_CODE, RFRASPC_WEB_DECLINE_AWST_CODE
        FROM RFRASPC
       WHERE RFRASPC_AIDY_CODE = aidy
         AND RFRASPC_FUND_CODE = fund;
  
  BEGIN
    BEGIN
      v_pidm := baninst1.gb_common.f_get_pidm(P_ID);
    EXCEPTION
      WHEN OTHERS THEN
        P_ERROR := 'Invalid UserId';
        goto QUITPROGRAM;
    END;
    IF faid_pkg.F_AllowWebUpdate(v_pidm, P_AIDY_CODE, P_FUND_CODE) <> 'Y' THEN
    
      P_ERROR := 'Not allowed to Update Award';
      goto QUITPROGRAM;
    END IF;
    BEGIN
      OPEN status_code_c(P_AIDY_CODE, P_FUND_CODE);
      FETCH status_code_c
        INTO v_accept_code, v_decline_code;
      CLOSE status_code_c;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    IF P_ACCEPT_CODE = 'Accept' THEN
      begin
        v_amount := TO_NUMBER(P_ACCEPT_AMT);
      exception
        when others then
          P_ERROR := 'Not a valid Amount';
          goto QUITPROGRAM;
      end;
      v_amount := F_GetMinAmt(P_AIDY_CODE, P_FUND_CODE);
      if P_ACCEPT_AMT < v_amount then
        P_ERROR := 'Partial amount must be greater than ' || v_amount ||
                   ' minimum amount';
        goto QUITPROGRAM;
      end if;
      begin
        rp_award.p_update(p_aidy_code       => P_AIDY_CODE,
                          p_pidm            => v_pidm,
                          p_fund_code       => P_FUND_CODE,
                          p_awst_code       => v_accept_code,
                          p_accept_amt      => P_ACCEPT_AMT,
                          p_last_web_update => SYSDATE);
      exception
        when others then
          P_ERROR := 'Accept amount failed' || sqlerrm;
          goto QUITPROGRAM;
      end;
    ELSIF P_ACCEPT_CODE = 'Decline' THEN
      begin
        rp_award.p_update(p_aidy_code       => P_AIDY_CODE,
                          p_pidm            => v_pidm,
                          p_fund_code       => P_FUND_CODE,
                          p_awst_code       => v_decline_code,
                          p_accept_amt      => P_ACCEPT_AMT,
                          p_last_web_update => SYSDATE);
      exception
        when others then
          P_ERROR := 'Decline amount failed' || sqlerrm;
          goto QUITPROGRAM;
      end;
    END IF;
    <<QUITPROGRAM>>
    if P_ERROR is null then
      gb_common.p_commit;
    else
      gb_common.p_rollback;
    end if;
  END PZ_ACCEPT_AWARD;
  function F_GetURL(menu_name VARCHAR2, image_name VARCHAR2) RETURN VARCHAR2 IS
  
    url TWGRMENU.TWGRMENU_URL%TYPE := NULL;
  
    CURSOR URL_C IS
      SELECT TWGRMENU_URL
        FROM TWGRMENU
       WHERE TWGRMENU_NAME = menu_name
         AND TWGRMENU_IMAGE = image_name
       ORDER BY TWGRMENU_SOURCE_IND DESC;
  
  begin
    OPEN URL_C;
    FETCH URL_C
      INTO url;
    CLOSE URL_C;
    return url;
  end;
  procedure PZ_GET_T_AND_C(P_ID        VARCHAR2,
                           P_AIDY_CODE VARCHAR2,
                           P_RESULT    OUT CLOB) AS
    cursor c_terms is
      SELECT RORWTXT_HEADING HEADING, RORWTXT_TEXT TEXT
        FROM RORWTXT, RORWTAB, RORWSQL, RORSTAT, SPRIDEN
       WHERE SPRIDEN_PIDM = RORSTAT_PIDM
         AND SPRIDEN_CHANGE_IND IS NULL
         AND RORWTXT_AIDY_CODE = RORSTAT_AIDY_CODE
         AND RORWTXT_TAB = 'TC'
         AND RORWTXT_ACTIVE_IND = 'Y'
         AND RORWTXT_AIDY_CODE = RORWTAB_AIDY_CODE
         AND RORWTXT_WTXT_CODE = RORWTAB_WTXT_CODE
         AND RORWTXT_TAB = RORWTAB_TAB
         AND RORWTXT_AIDY_CODE = RORWSQL_AIDY_CODE
         AND RORWTXT_WTXT_CODE = RORWSQL_WTXT_CODE
         AND RORWSQL_ACTIVE_IND = 'Y'
         AND RORWSQL_VALIDATED_IND = 'Y'
         AND SPRIDEN_ID = P_ID
         AND RORSTAT_AIDY_CODE = P_AIDY_CODE
         AND RORWTXT_SELECT_VALUE =
             ROKWTXT.f_calc_select_value(RORSTAT_AIDY_CODE,
                                         RORWTXT_WTXT_CODE,
                                         RORSTAT_PIDM,
                                         NULL)
       ORDER BY RORWTAB_DISPLAY_SEQ_NO;
    v_terms clob;
  begin
    v_terms := '<html><table>';
    for r_terms in c_terms loop
      v_terms := v_terms || '<tr><th><h2>' || r_terms.heading ||
                 '</h2></th></tr><tr><td><h4>' || r_terms.text ||
                 '</h4></td></tr>';
    end loop;
    v_terms  := v_terms || '</table></html>';
    P_RESULT := v_terms;
  exception
    when others then
      P_RESULT := '<html>Error</html>';
  end PZ_GET_T_AND_C;
  procedure pz_accept_terms(P_ID        VARCHAR2,
                            P_AIDY_CODE VARCHAR2,
                            P_ERROR     OUT VARCHAR2) IS
  
    CURSOR web_requirement_c(pidm     NUMBER,
                             aidy     ROBINST.ROBINST_AIDY_CODE%TYPE,
                             req_type RORWEBA.RORWEBA_TYPE_CODE%TYPE) IS
      SELECT RORWEBA_TREQ_CODE, RORWEBA_TRST_CODE, RRRAREQ_FUND_CODE -- 80201-1
        FROM RRRAREQ, RORWEBA
       WHERE RORWEBA_AIDY_CODE = aidy
         AND RORWEBA_TYPE_CODE = req_type
         AND RRRAREQ_PIDM = pidm
         AND RRRAREQ_AIDY_CODE = RORWEBA_AIDY_CODE
         AND RRRAREQ_TREQ_CODE = RORWEBA_TREQ_CODE
         AND RRRAREQ_SAT_IND = 'N'
         AND (RRRAREQ_FUND_CODE IS NULL OR
             (RRRAREQ_FUND_CODE IS NOT NULL AND NOT EXISTS
              (SELECT 'X'
                  FROM RFRBASE
                 WHERE RFRBASE_FUND_CODE = RRRAREQ_FUND_CODE
                   AND RFRBASE_FED_FUND_ID = 'PELL')));
  
    req_code   RORWEBA.RORWEBA_TREQ_CODE%TYPE;
    req_status RORWEBA.RORWEBA_TRST_CODE%TYPE;
    fund_code  RPRAWRD.RPRAWRD_FUND_CODE%TYPE;
    v_pidm number;
  begin
    BEGIN
      v_pidm := baninst1.gb_common.f_get_pidm(P_ID);
    EXCEPTION
      WHEN OTHERS THEN
        P_ERROR := 'Invalid UserId';
        goto QUITPROGRAM;
    END;
    begin
      roklogs.p_set_user_id(v_pidm, twbklogn.f_get_banner_id(v_pidm));
      OPEN web_requirement_c(v_pidm, P_AIDY_CODE, 'T');
      FETCH web_requirement_c
        INTO req_code, req_status, fund_code; -- 80201-1
    
      WHILE web_requirement_c%Found LOOP
        rp_requirement.p_update(p_aidy_code     => P_AIDY_CODE,
                             p_pidm          => v_pidm,
                             p_treq_code     => req_code,
                             p_stat_date     => SYSDATE,    -- 80300-3
                             p_trst_code     => req_status,
                             p_fund_code     => fund_code,  -- 80201-1
                             p_sys_ind       => 'W',
                             p_sbgi_code     => NULL,
                             p_sbgi_type_ind => NULL,       -- 080500-3
                             p_term_code     => NULL);    
        FETCH web_requirement_c
          INTO req_code, req_status, fund_code; 
      end LOOP;
      CLOSE web_requirement_c;
    
      gb_common.p_commit;
    
      roklogs.p_clear_user_id(v_pidm); 
      gb_common.p_commit;
    end;
    <<QUITPROGRAM>>
    null;
  end pz_accept_terms;
END FAID_PKG;

/
