PROMPT CREATE OR REPLACE PACKAGE BODY ME_REG_CLIENT
create or replace package body ME_REG_CLIENT is

  function fz_get_linked_courses(P_CRN       ssrlink.ssrlink_crn%type,
                                 P_TERM_CODE ssrlink.ssrlink_term_code%type)
  
   RETURN varchar2 as
  
    v_linked_courses varchar2(100);
    
      cursor get_linked_courses is
      select LINKS.*
        from ssrlink C, ssrlink LINKS
       where C.ssrlink_term_code = P_TERM_CODE
         and C.ssrlink_crn = P_CRN
         and C.ssrlink_term_code = LINKS.ssrlink_term_code
         and REVERSE(C.ssrlink_link_conn) = LINKS.ssrlink_link_conn
         order by LINKS.SSRLINK_CRN;
  
  BEGIN
  
    FOR R_DATA IN get_linked_courses LOOP
    
      v_linked_courses := R_DATA.SSRLINK_CRN || ',' || v_linked_courses;
    
    END LOOP;
  
    v_linked_courses := RTRIM(v_linked_courses, ',');
  
    RETURN v_linked_courses;
  
  END;

begin

  NULL;

end ME_REG_CLIENT;
/