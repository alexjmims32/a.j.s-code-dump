PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start DO.sql"   
PROMPT ====================================================

spool DO.log

@@Types.sql
@@Type\T_ARRAY_TYPE.sql
@@Procedure\PZ_ME_AUTHENTICATION.sql
@@Package\ADD_COURSE_PKG_CORQ.sql
@@Package\CART_PKG.sql
@@Package\DROP_COURSE_PKG.sql
@@Package\ME_ACCOUNT_OBJECTS.sql
@@Package\ME_ALT_PIN_PKG.sql
@@Package\ME_BWCKCOMS.sql
@@Package\ME_BWCKGENS.sql
@@Package\ME_BWCKREGS.sql
@@Package\ME_REG_UTILS.sql
@@Package\ME_SFKFEES.sql
@@Package\ME_VALID.sql
@@Package\ME_WEB_INVOICE.sql
@@Package\ME_WITHDRAWL.sql
@@Package\PAYMENT_GATEWAY.sql
@@Package\Z_CM_MOBILE_CAMPUS.sql
@@PackageBody\ADD_COURSE_PKG_CORQ.sql
@@PackageBody\CART_PKG.sql
@@PackageBody\DROP_COURSE_PKG.sql
@@PackageBody\ME_ACCOUNT_OBJECTS.sql
@@PackageBody\ME_ALT_PIN_PKG.sql
@@PackageBody\ME_BWCKCOMS.sql
@@PackageBody\ME_BWCKGENS.sql
@@PackageBody\ME_BWCKREGS.sql
@@PackageBody\ME_REG_UTILS.sql
@@PackageBody\ME_SFKFEES.sql
@@PackageBody\ME_VALID.sql
@@PackageBody\ME_WEB_INVOICE.sql
@@PackageBody\ME_WITHDRAWL.sql
@@PackageBody\PAYMENT_GATEWAY.sql
@@PackageBody\Z_CM_MOBILE_CAMPUS.sql

EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
