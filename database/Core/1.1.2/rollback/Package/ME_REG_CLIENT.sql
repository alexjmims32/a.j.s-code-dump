PROMPT CREATE OR REPLACE PACKAGE ME_REG_CLIENT 
create or replace package ME_REG_CLIENT as

  -- Author  : n2n IT Services
  -- Created : 03-08-2013 19:04:59
  -- Purpose : Client specific code, differs with client version

  -- Public function and procedure declarations

  function fz_get_linked_courses(P_CRN       ssrlink.ssrlink_crn%type,
                                 P_TERM_CODE ssrlink.ssrlink_term_code%type)
  
   RETURN varchar2;

end ME_REG_CLIENT;
/
