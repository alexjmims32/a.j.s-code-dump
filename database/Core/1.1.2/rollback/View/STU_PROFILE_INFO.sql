CREATE OR REPLACE VIEW STU_PROFILE_INFO AS
SELECT sgbstdn_pidm pidm_key,
       spriden_id student_id,
       f_format_name(sgbstdn_pidm, 'FMIL') stu_name,
       decode((substr(me_reg_utils.fz_registered_this_term(SPRIDEN_pidm, stvterm_code),
                      1,
                      1)),
              'Y',
              'Yes',
              'N',
              'No',
              NULL) is_registered,
       sgbstdn_term_code_eff from_term,
       me_reg_utils.fz_get_desc((SELECT nvl(MIN(sgbstdn_term_code_eff),
                                           '999999')
                                  FROM sgbstdn
                                 WHERE sgbstdn_pidm = SPRIDEN_PIDM
                                   AND sgbstdn_term_code_eff > STVTERM_CODE),
                                'STVTERM',
                                '') TO_TERM,
       me_reg_utils.fz_gettermfirst(SPRIDEN_PIDM,
                                    sgbstdn_levl_code,
                                    STVTERM_CODE) first_term,
       me_reg_utils.fz_get_desc(me_reg_utils.fz_latest_term_fnc(sgbstdn_pidm),
                                'STVTERM',
                                '') last_term,
       me_reg_utils.fz_get_desc(sgbstdn_stst_code, 'STVSTST', '') student_status,
       me_reg_utils.fz_get_desc(sgbstdn_term_code_matric, 'STVTERM', '') term_code_matric,
       sgbstdn_site_code site_code,
       me_reg_utils.fz_get_desc(sgbstdn_resd_code, 'STVRESD', '') residence,
       me_reg_utils.fz_get_desc(spbpers_citz_code, 'STVCITZ', '') citizen,
       sgbstdn_sess_code sess_code,
       me_reg_utils.fz_get_desc(sgbstdn_styp_code, 'STVSTYP', '') studnet_type,
       sgbstdn_rate_code rate_code,
       sgbstdn_astd_code override_astd_code,
       sgbstdn_term_code_astd override_astd_code_term,
       sgbstdn_blck_code blck_code,
       decode(sgbstdn_degc_code_dual, 'Y', 'Yes', 'N', 'No', NULL) is_dual_deg,
       sgbstdn_program_1 program_1,
       sgbstdn_program_2 program_2,
       sgbstdn_term_code_admit term_code_admit,
       sgbstdn_term_code_admit_2 term_code_admit_2,
       sgbstdn_admt_code admt_code,
       sgbstdn_admt_code_2 admt_code_2,
       sgbstdn_term_code_ctlg_1 term_catalog,
       sgbstdn_term_code_ctlg_2 term_catalog_2,
       sgbstdn_levl_code levl_code,
       sgbstdn_levl_code_2 levl_code_2,
       sgbstdn_degc_code_1 degc_code_1,
       sgbstdn_degc_code_2 degc_code_2,
       sgbstdn_coll_code_1 coll_code_1,
       sgbstdn_coll_code_2 coll_code_2,
       sgbstdn_camp_code camp_code,
       sgbstdn_camp_code_2 camp_code_2,
       sgbstdn_dept_code dept_code_1,
       sgbstdn_dept_code_2 dept_code_2,
       sgbstdn_majr_code_1 majr_code_1,
       sgbstdn_majr_code_1_2 majr_code_1_2,
       sgbstdn_majr_code_minr_1 majr_code_minor_1,
       sgbstdn_majr_code_minr_1_2 majr_code_minor_1_2,
       sgbstdn_majr_code_conc_1 majr_code_conc_1,
       sgbstdn_majr_code_conc_1_2 majr_code_conc_1_2,
       sgbstdn_majr_code_conc_1_3 majr_code_conc_1_3,
       to_char(sgbstdn_exp_grad_date, 'MM/DD/YYYY') exp_grad_date,
       sgbstdn_term_code_grad exp_grad_date_term,
       sgbstdn_acyr_code exp_grad_date_year,
       sgbstdn_majr_code_2 majr_code_2,
       sgbstdn_majr_code_2_2 majr_code_2_2,
       sgbstdn_majr_code_minr_2 majr_code_minor_2,
       sgbstdn_majr_code_minr_2_2 majr_code_minor_2_2,
       sgbstdn_majr_code_conc_2 majr_code_conc_2,
       sgbstdn_majr_code_conc_2_2 majr_code_conc_2_2,
       sgbstdn_majr_code_conc_2_3 majr_code_conc_2_3,
       sgbstdn_leav_code leav_code,
       to_char(sgbstdn_leav_from_date, 'MM/DD/YYYY') leav_from_date_char,
       to_char(sgbstdn_leav_to_date, 'MM/DD/YYYY') leav_to_date_char,
       STVTERM_CODE term_code
  FROM SGBSTDN
  JOIN SPRIDEN
    ON SPRIDEN_PIDM = SGBSTDN_PIDM
   AND SPRIDEN_CHANGE_IND IS NULL
  JOIN STVTERM
    ON sgbstdn_term_code_eff =
       (SELECT MAX(sgbstdn_term_code_eff)
          from sgbstdn
         where sgbstdn_pidm = SPRIDEN_PIDM
           and sgbstdn_term_code_eff <= STVTERM_CODE)
  LEFT JOIN SPBPERS
    ON SPBPERS_PIDM = SPRIDEN_PIDM;

