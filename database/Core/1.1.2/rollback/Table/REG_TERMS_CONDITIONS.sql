PROMPT CREATE TABLE REG_TERMS_CONDITIONS
create table REG_TERMS_CONDITIONS
(
  campuscode       VARCHAR2(10 BYTE),
  terms_conditions VARCHAR2(4000 BYTE),
  version_no       VARCHAR2(10 BYTE),
  lastmodifiedby   VARCHAR2(30 BYTE),
  lastmodifiedon   VARCHAR2(30 BYTE)
)
/


