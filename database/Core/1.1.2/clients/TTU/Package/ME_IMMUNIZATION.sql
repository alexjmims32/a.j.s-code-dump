PROMPT CREATE OR REPLACE PACKAGE me_immunization
CREATE OR REPLACE PACKAGE me_immunization IS



  PROCEDURE Pz_ViewImmz(p_student_id varchar2, p_immz_form_data out clob);

  PROCEDURE Pz_ViewImmzSubmit(p_student_id varchar2,
                              a_ques IN varchar2 default null,
                              b_ques IN varchar2 default null);

END me_immunization;
/

