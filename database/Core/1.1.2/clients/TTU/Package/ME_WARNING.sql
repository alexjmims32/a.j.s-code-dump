PROMPT CREATE OR REPLACE PACKAGE me_warning
CREATE OR REPLACE PACKAGE me_warning AS
  --AUDIT_TRAIL_C_I18N
  -- PROJECT : MSGKEY
  -- MODULE  : BZSKAWEL
  -- SOURCE  : enUS
  -- TARGET  : I18N
  -- DATE    : Fri Aug 21 08:08:41 2009
  -- MSGSIGN : #0000000000000000
  --TMI18N.ETR DO NOT CHANGE--
  --
  -- FILE NAME..: bzskawel.sql
  -- RELEASE....: 8.2 TBR8: 1.0
  -- OBJECT NAME: BZSKAWEL
  -- PRODUCT....: STUWEB
  -- COPYRIGHT..: Copyright (C) Sungard HE 2006. All rights reserved.

  --------Determines value for 'Eligible for Awarded Amount Column in Table'---------

  FUNCTION F_Get_Elig_Amt_Text(rfrbase_fund_code          IN rfrbase.rfrbase_fund_code%TYPE,
                               rpratrm_pckg_load_ind      IN rpratrm.rpratrm_pckg_load_ind%TYPE,
                               rfraspc_3Quarter_load_pct  IN rfraspc.rfraspc_3Quarter_load_pct%TYPE,
                               rfraspc_half_load_pct      IN rfraspc.rfraspc_half_load_pct%TYPE,
                               rfraspc_less_half_load_pct IN rfraspc.rfraspc_less_half_load_pct%TYPE,
                               rfraspc_proration_ind      IN rfraspc.rfraspc_proration_ind%TYPE,
                               load_ind                   IN VARCHAR2,
                               rfrbase_fed_fund_id        IN rfrbase.rfrbase_fed_fund_id%TYPE)
    RETURN VARCHAR2;

  --------------------------------------------
  PROCEDURE PZ_DisplayAwardEligable(p_term_in      varchar2,
                                    p_student_id   in varchar2,
                                    p_warning_form out clob);
  ------------------------------------------
  PROCEDURE P_DisplayAwardEligable_new(p_term_in      in varchar2,
                                       p_student_id   in varchar2,
                                       p_add_drop_ind in varchar2,
                                       p_crn          in varchar2 default null,
                                       p_warning_form out clob);

END me_warning;
/

