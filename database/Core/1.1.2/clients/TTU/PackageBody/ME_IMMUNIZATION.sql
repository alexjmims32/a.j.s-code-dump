PROMPT CREATE OR REPLACE PACKAGE BODY me_immunization
CREATE OR REPLACE PACKAGE BODY me_immunization IS


  /* variables visible only in this package or
  any items which must be maintined throughout
  a session or across transactions */
  /* Global type and variable declarations for package */
  global_pidm      spriden.spriden_pidm%TYPE;
  global_term_code stvterm.stvterm_code%TYPE;
  curr_release CONSTANT VARCHAR2(30) := G$_NLS.Get('BZSKIMM1-0000',
                                                   'SQL',
                                                   '8.2 TBR8: 1.0');

  /* Fully define subprograms specified in package */
  /* View Immunization information */
    -------------------------------------------------------------------------
    -------------------------------------------------------------------------
  --Procedure to sending the Immunization infromation in clob
  PROCEDURE Pz_ViewImmz(p_student_id     varchar2,
                                        p_immz_form_data out clob)

 IS
  under_18     VARCHAR2(1) DEFAULT 'N';
  birth_date   DATE;
  v_clob_var   clob;
  v_radio_var1 varchar2(100);
  v_radio_var2 varchar2(100);
  v_radio_var3 varchar2(100);
  v_radio_var4 varchar2(100);
  v_pidm       number;
  --global_pidm  number;

  -------------------------------------------
  --
  -- Birthdate Cursor
  --
  CURSOR birth_c(pidm_in IN spriden.spriden_pidm%TYPE) IS
    select nvl(spbpers_birth_date, add_months(sysdate, -228))
      from spbpers
     where spbpers_pidm = global_pidm;

  -------------------------------------------
BEGIN
  begin

    v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);

  exception
    when others then

      v_pidm := -100;

  end;
  global_pidm := v_pidm;

  -- Check to see if student is under 18
  OPEN BIRTH_C(global_pidm);
  FETCH birth_c
    INTO birth_date;
  CLOSE BIRTH_C;
  if birth_date is null then
    birth_date := add_months(sysdate, -228);
  end if;
  if birth_date >= add_months(sysdate, -216) then
    under_18 := 'Y';
  end if;
  -- Check for  hepatitus and menengitus immunizations
  if (szkitrk.f_szritrk_exists(global_pidm, 'HEPB') and
     NOT szkitrk.f_szritrk_satisfied(global_pidm, 'HEPB')) or
     (szkitrk.f_szritrk_exists(global_pidm, 'MENG') and
     NOT szkitrk.f_szritrk_satisfied(global_pidm, 'MENG')) THEN
    --  Ask questions for over 18
    if under_18 != 'Y' then

      -- Hepatitis  Section
      if NOT szkitrk.f_szritrk_satisfied(global_pidm, 'HEPB') AND
         szkitrk.f_immz_reg_prevent(global_pidm, 'HEPB') then
        -- MC:8.0.1
        v_clob_var   := v_clob_var ||'<p><h4>' ||
                        'The General Assembly of the State of Tennessee mandates that each public or private postsecondary institution in the state provide information concerning Hepatitis B
                        infection to all students entering the institution for the first time. Those students who will be living in on-campus housing must also be informed about the risk of
                        meningococcal meningitis infection.The required information below includes the risk factors and dangers of each disease as well as information on the availability and
                        effectiveness of the respective vaccines for persons who are at-risk for the disease. The information concerning these diseases is from the Centers for Disease Control
                        and the American College Health Association. The law does not require that students receive vaccination for enrollment. Furthermore, the institution is not required by
                        law to provide vaccination and/or reimbursement for the vaccine. For more information about Meningococcal Meningitis and Hepatitis B vaccines, please contact your local
                        health care provider or consult the Centers for Disease Control and Prevention web site at ' ||'<a href="http://www.cdc.gov/health/default.htm.">'||'www.cdc.gov/health/default.htm.'||'</a>' ||
                        '</h4></p>'||'<br>';
          v_clob_var:= v_clob_var || '<p><h4>'||
          'Hepatitis B (HBV) is a serious viral infection of the liver that can lead to chronic liver disease, cirrhosis, liver cancer, liver failure, and even death.
          The disease is transmitted by blood and or body fluids and many people will have no symptoms when they develop the disease. The primary risk factors for Hepatitis B
          are sexual activity and injecting drug use. This disease is completely preventable. Hepatitis B vaccine is available to all age groups to prevent Hepatitis B viral infection.
          A series of three (3) doses of vaccine are required for optimal protection. Missed doses may still be sought to complete the series if only one or two have been acquired.
          The HBV vaccine has a record of safety and is believed to confer lifelong immunity in most cases. '||'</h4></p>'||'<br>';
                       /* v_clob_var := '<p><h4>'||twbkwbis.f_dispinfo('bzskimmz.P_ViewImmz', 'DEFAULT')||'</h4></p>';
                               v_clob_var := v_clob_var ||'<p><h4>'||
                                             twbkwbis.f_dispinfo('bzskimmz.P_ViewImmz', 'REQUIRED')||'</h4></p>';*/

          v_clob_var:= v_clob_var || '<p><font color=red><h4>'||'You are required to select one of the following statements before registration can continue:' ||'</h4></font></p>';
             v_radio_var1 :=
                        '<INPUT TYPE="radio" NAME="a_ques" VALUE="Y"  ID="STYPE1">';
        v_radio_var2 := '<INPUT TYPE="radio" NAME="a_ques" VALUE="Y"  ID="STYPE2">';
        v_clob_var   := v_clob_var || '<h4>' || v_radio_var1 ||
                        'I hereby certify that I have read this information and I have elected to receive
                 and/or have started or have received the complete three dose series of the Hepatitis B vaccine.' ||
                        '<br>' || v_radio_var2 ||
                        'I hereby certify that I have read this information and I have elected not to receive
                          the Hepatitis B vaccine.' ||
                        '</h4>' || '<br>' || '<br>';

      end if;

      -- Meningitus Section
      if NOT szkitrk.f_szritrk_satisfied(global_pidm, 'MENG') AND
         szkitrk.f_immz_reg_prevent(global_pidm, 'MENG') then
        -- MC:8.0.1 then
        v_clob_var:= v_clob_var ||'<p><h4>'||
        'Meningococcal disease is a rare but potentially fatal bacterial infection, expressed as either meningitis (infection of the membranes surrounding the brain and spinal cord) or meningococcemia (bacteria in the blood).
         Meningococcal disease strikes about 3,000 Americans each year and is responsible for about 300 deaths annually. The disease is spread by airborne transmission, primarily by coughing. The disease can onset very quickly
         and without warning. Rapid intervention and treatment is required to avoid serious illness and or death. There are 5 different subtypes (called sereogroups) of the bacterium that causes Meningococcal Meningitis.
         The current vaccine does not stimulate protective antibodies to Sereogroups B, but it does protect against the most common strains of the disease, including sereogroups A, C, Y and W-135. The duration of protection is
         approximately three to five years. The vaccine is very safe and adverse reactions are mild and infrequent, consisting primarily of redness and pain at the site of injection lasting up to two days. The Advisory Committee
         on Immunization Practices (ACIP) of the U.S. Centers for Disease Control and Prevention (CDC) recommends that college freshmen (particularly those who live in dormitories or residence halls) be informed about meningococcal
         disease and the benefits of vaccination and those students who wish to reduce their risk for meningococcal disease be immunized. Other undergraduate students who wish to reduce their risk for meningococcal disease may also
         choose to be vaccinated.'
        ||'</h4></p>'||'<br>';
        v_clob_var:= v_clob_var || '<p><font color=red><h4>'||'You are required to select one of the following statements before registration can continue:' ||'</h4></font></p>';
       /* v_clob_var := v_clob_var || '<p><h4>' ||
                      twbkwbis.f_dispinfo('bzskimmz.P_ViewImmz', 'DEFAULT1') ||
                      '</h4></p>';
        v_clob_var := v_clob_var || '<p><h4>' ||
                      twbkwbis.f_dispinfo('bzskimmz.P_ViewImmz', 'REQUIRED') ||
                      '</h4></p>';*/

        v_radio_var3 := '<INPUT TYPE="radio" NAME="b_ques" VALUE="Y"  ID="STYPE1">';
        v_radio_var4 := '<INPUT TYPE="radio" NAME="b_ques" VALUE="Y"  ID="STYPE2">';

        v_clob_var := v_clob_var || '<h4>' || v_radio_var3 ||
                      'I hereby certify that I have read this information and I have elected to receive
                 or have received the vaccine for Meningococcal Meningitis.' ||
                      '<br>' || v_radio_var4 ||
                      'I hereby certify that I have read this information and I have elected not to receive
                          the vaccine for Meningococcal Meningitis.' ||
                      '</h4>' || '<br>' || '<br>';

      end if;
      p_immz_form_data := v_clob_var;

      --  If under 18 display text only
    else
      /*v_clob_var       := twbkwbis.f_dispinfo('bzskimmz.P_ViewImmz',
                                              'UNDER18');*/
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Message for students under the age of 18:' ||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'If you are under 18, before you can register for classes, you need to provide Health Services with a Hepatitis B/Meningitis Form that has been filled out and signed by a parent. Please note that this form is only a waiver and does not require the submission of medical records.' ||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Hepatitis B/Meningitis Form:'||'<a href="http://www.tntech.edu/healthservices/forms">'||'http://www.tntech.edu/healthservices/forms'||'</a>' ||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Other Potentially Relevant Immunization Information: ' ||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Full-Time Registration Requirement � ALL NEW STUDENTS:' ||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Students must provide proof of two (2) MMR (measles, mumps, and rubella) vaccinations or proof of immunity to measles, mumps, and rubella by meeting one of the following 4 criteria: � Date of birth before 1957, or � Documentation of 2 doses of the vaccine against measles, mumps, and rubella given at least 28 days apart, excluding doses given earlier than 4 days before the first birthday, or � Tennessee high school graduation after 1999, or � Documentation of blood test (serology) showing immunity to measles, mumps, and rubella. If any one of the three is negative, evidence of 2 doses of vaccine must be provided to Health Services. '||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Student must also provide proof of (2) Varicella (chicken pox) vaccinations or proof of immunity to chickenpox by meeting one of the following 4 criteria: � Date of birth before 1980, or � History of chickenpox illness diagnosed by a healthcare provider or verified by a physician, advanced practice nurse or physician assistant to whom the illness is described, or � Documentation of 2 doses of the Varicella vaccine given at least 28 days apart, excluding doses given earlier than 4 days before the first birthday, or � Documentation of blood test (serology) showing immunity to Varicella.'||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Registration Requirement � INTERNATIONAL STUDENTS ONLY:'||'</h4></p>'||'<br>';
      v_clob_var:= v_clob_var ||'<p><h4>' ||'Student must provide documentation of a TB Skin Test.'||'</h4></p>'||'<br>';
      p_immz_form_data := v_clob_var;
    end if;
  end if;

END pz_ViewImmz;
  --Procedure to update szritrk table
  PROCEDURE Pz_ViewImmzSubmit(p_student_id varchar2,
                              a_ques IN varchar2 default null,
                              b_ques IN varchar2 default null) IS
    v_pidm number;
  BEGIN
    begin

      v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);

    exception
      when others then

        v_pidm := -100;

    end;

    global_pidm := v_pidm;

    if a_ques = 'Y' then

      update szritrk
         set szritrk_received_ind = 'Y', szritrk_refused_ind = 'N'
       where szritrk_pidm = global_pidm
         and szritrk_immz_code = 'HEPB';

    elsif a_ques = 'N' then

      update szritrk
         set szritrk_refused_ind = 'Y', szritrk_received_ind = 'N'
       where szritrk_pidm = global_pidm
         and szritrk_immz_code = 'HEPB';

    end if;

    if b_ques = 'Y' then

      update szritrk
         set szritrk_received_ind = 'Y', szritrk_refused_ind = 'N'
       where szritrk_pidm = global_pidm
         and szritrk_immz_code = 'MENG';

    elsif b_ques = 'N' then

      update szritrk
         set szritrk_refused_ind = 'Y', szritrk_received_ind = 'N'
       where szritrk_pidm = global_pidm
         and szritrk_immz_code = 'MENG';
    end if;


    -- Start logic to perform SFBETRM_MHRS_OVER maintenance
    -- when all SZRITRK with FT restriction are "waived"
    -- or "satisfied".
    -- ===================================================
    IF NOT (szkitrk.f_FT_restricted(global_pidm)) THEN
      szkitrk.p_update_mhrs(global_pidm);
    END IF;

    commit;

  END Pz_ViewImmzSubmit;
END me_immunization;
/

