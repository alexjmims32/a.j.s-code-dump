PROMPT CREATE OR REPLACE PACKAGE BODY me_warning
CREATE OR REPLACE PACKAGE BODY me_warning AS
  --AUDIT_TRAIL_C_I18N
  -- PROJECT : MSGKEY
  -- MODULE  : BZSKAWEL1
  -- SOURCE  : enUS
  -- TARGET  : I18N
  -- DATE    : Wed Sep 02 08:18:32 2009
  -- MSGSIGN : #02676c92475b1aa2
  --TMI18N.ETR DO NOT CHANGE--
  --
  -- FILE NAME..: bzskawel1.sql
  -- RELEASE....: 7.2 MC:18.0
  -- OBJECT NAME: BZSKAWEL
  -- PRODUCT....: STUWEB
  -- COPYRIGHT..: Copyright (C) Sungard HE 2006. All rights reserved.

  -- Global type and variable declarations for package
  curr_release CONSTANT VARCHAR2(20) := G$_NLS.Get('BZSKAWEL1-0000',
                                                   'SQL',
                                                   '7.2 MC:18.0');
  global_pidm spriden.spriden_pidm%TYPE;
  global_term spriden.spriden_pidm%TYPE;
  aidy_in     STVTERM.STVTERM_FA_PROC_YR%TYPE;

  ------------------------------------------------------------------------------------

  FUNCTION F_Get_Elig_Amt_Text(rfrbase_fund_code          IN rfrbase.rfrbase_fund_code%TYPE,
                               rpratrm_pckg_load_ind      IN rpratrm.rpratrm_pckg_load_ind%TYPE,
                               rfraspc_3Quarter_load_pct  IN rfraspc.rfraspc_3Quarter_load_pct%TYPE,
                               rfraspc_half_load_pct      IN rfraspc.rfraspc_half_load_pct%TYPE,
                               rfraspc_less_half_load_pct IN rfraspc.rfraspc_less_half_load_pct%TYPE,
                               rfraspc_proration_ind      IN rfraspc.rfraspc_proration_ind%TYPE,
                               load_ind                   IN VARCHAR2,
                               rfrbase_fed_fund_id        IN rfrbase.rfrbase_fed_fund_id%TYPE)
    RETURN VARCHAR2

   IS
  BEGIN

    IF rfrbase_fed_fund_id IN
       (G$_NLS.Get('BZSKAWEL1-0018', 'SQL', 'STFD'),
        G$_NLS.Get('BZSKAWEL1-0019', 'SQL', 'PLUS')) -- Added for CR-001 - loans
     THEN
      IF load_ind <= 3 THEN
        RETURN G$_NLS.Get('BZSKAWEL1-0020',
                          'SQL',
                          'Yes-eligible for full amount');
      ELSE
        RETURN G$_NLS.Get('BZSKAWEL1-0021',
                          'SQL',
                          'No-not eligible for any portion');
      END IF;
      /* BA MC:4.0.5 */
      -- ELSIF rfrbase_fund_code = 'PELL'
      --ELSIF rfrbase_fund_code = '%PELL%'
      /* EA MC:4.0.5 */
      -- MC:18.0 BA
    ELSIF rfrbase_fund_code LIKE
          G$_NLS.Get('BZSKAWEL1-0022', 'SQL', '%PELL%')
    -- MC:18.0 EA
     THEN
      IF load_ind > rpratrm_pckg_load_ind THEN
        RETURN G$_NLS.Get('BZSKAWEL1-0023',
                          'SQL',
                          'May be eligible for a reduced amount');
      ELSE
        RETURN G$_NLS.Get('BZSKAWEL1-0024',
                          'SQL',
                          'Yes-eligible for full amount');
      END IF;
    ELSE
      --(Non PELL)
      IF rfraspc_proration_ind = 'P' THEN
        IF load_ind <= rpratrm_pckg_load_ind THEN
          RETURN G$_NLS.Get('BZSKAWEL1-0025',
                            'SQL',
                            'Yes-eligible for full amount.');
        ELSE
          --(load_ind > rpratrm_pckg_load_ind)
          IF load_ind = '2' THEN
            IF rfraspc_3quarter_load_pct > 0 THEN
              RETURN G$_NLS.Get('BZSKAWEL1-0026',
                                'SQL',
                                'Yes-eligible for reduced amount');
            ELSE
              --(rfraspc_3quarter_load_pct = 0)
              RETURN G$_NLS.Get('BZSKAWEL1-0027',
                                'SQL',
                                'No-not eligible for any portion');
            END IF;
          ELSIF load_ind = '3' THEN
            IF rfraspc_half_load_pct > 0 THEN
              RETURN G$_NLS.Get('BZSKAWEL1-0028',
                                'SQL',
                                'Yes-eligible for reduced amount');
            ELSE
              --(rfraspc_half_load_pct = 0)
              RETURN G$_NLS.Get('BZSKAWEL1-0029',
                                'SQL',
                                'No-not eligible for any portion');
            END IF;
          ELSE
            --(load_ind = '4')
            IF rfraspc_less_half_load_pct > 0 THEN
              RETURN G$_NLS.Get('BZSKAWEL1-0030',
                                'SQL',
                                'Yes-eligible for reduced amount');
            ELSE
              --(rfraspc_less_half_load_pct = 0)
              RETURN G$_NLS.Get('BZSKAWEL1-0031',
                                'SQL',
                                'No-not eligible for any portion');
            END IF;
          END IF;
        END IF;
      ELSIF rfraspc_proration_ind = 'D' THEN
        RETURN G$_NLS.Get('BZSKAWEL1-0032',
                          'SQL',
                          'Yes-eligible for full amount');
      ELSE
        --(rfraspc_proration_ind = 'N')
        IF load_ind <= rpratrm_pckg_load_ind THEN
          RETURN G$_NLS.Get('BZSKAWEL1-0033',
                            'SQL',
                            'Yes-eligible for full amount');
        ELSE
          RETURN G$_NLS.Get('BZSKAWEL1-0034',
                            'SQL',
                            'No-not eligible for any portion');
        END IF;
      END IF; --proration
    END IF; --pell or non pell

  END F_Get_Elig_Amt_Text;
  --------------------------------------------
  PROCEDURE PZ_DisplayAwardEligable(p_term_in      varchar2,
                                    p_student_id   in varchar2,
                                    p_warning_form out clob) IS

    v_warning_form  clob;
    v_pidm          number;
    init_array      OWA_UTIL.ident_arr;
    fac_stu_ind     VARCHAR2(10);
    multi_term_list OWA_UTIL.ident_arr;
    global_term     varchar2(10);
    global_pidm     number;
    called_by_in    VARCHAR2(100) DEFAULT 'ADD_DROP';

    CURSOR awards_c(pidm_in NUMBER, term_in VARCHAR2, aidy_in VARCHAR2) IS

      SELECT rpratrm_fund_code,
             rfrbase_fund_title,
             rtvawst_desc,
             rtvawst_offer_ind, -- MC:4.0.1
             rtvawst_accept_ind, -- MC:4.0.1
             rpratrm_offer_amt,
             rpratrm_accept_amt,
             rpratrm_pckg_load_ind,
             rprawrd_awst_code,
             rfrbase_fund_code,
             rfrbase_fed_fund_id,
             rfraspc_less_half_load_pct,
             rfraspc_half_load_pct,
             rfraspc_3Quarter_load_pct,
             rfraspc_proration_ind
        from rpratrm, rprawrd, rfrbase, rtvawst, rfraspc
       where rpratrm_fund_code = rprawrd_fund_code
         and rpratrm_aidy_code = rprawrd_aidy_code
         and rpratrm_pidm = rprawrd_pidm
         and rpratrm_term_code = term_in
         and rpratrm_pidm = pidm_in
         and rpratrm_aidy_code = aidy_in
         and rfraspc_aidy_code = rprawrd_aidy_code
         and rfrbase_fund_code = rpratrm_fund_code
         and rfraspc_fund_code = rprawrd_fund_code
         and rtvawst_code = rprawrd_awst_code
         and rtvawst_decline_ind = 'N'
         and rtvawst_cancel_ind = 'N';

    tot_cred    SFTREGS.SFTREGS_CREDIT_HR%TYPE;
    tot_bill_hr SFTREGS.SFTREGS_BILL_HR%TYPE;
    tot_ceu     SFTREGS.SFTREGS_CREDIT_HR%TYPE;
    enrl_status VARCHAR2(30);
    load_ind    VARCHAR2(1);
    award_rec   awards_c%ROWTYPE;
    row_count   NUMBER DEFAULT 0;
    stvterm_row stvterm%ROWTYPE;
    sql_error   NUMBER;
    sobterm_row sobterm%ROWTYPE;
    aidy_in     varchar2(10);
    sfbetrm_rec sfbetrm%ROWTYPE;
    max_hr      SFBETRM.SFBETRM_MHRS_OVER%TYPE := 0;

  BEGIN

    begin

      v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);

    exception
      when others then

        v_pidm := -100;

    end;
    global_pidm := v_pidm;
    global_term := p_term_in;

    open stkterm.stvtermc(global_term);
    fetch stkterm.stvtermc
      into stvterm_row;
    close stkterm.stvtermc;

    IF NOT bwcklibs.f_validterm(sql_error, sobterm_row, global_term) THEN
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;

    sfksels.p_get_total_hours_sftregs(tot_cred, --out
                                      tot_bill_hr, --out
                                      tot_ceu, --out
                                      global_term,
                                      global_pidm);

    rzkfarc.P_Get_Enrl_Status(global_pidm,
                              global_term,
                              tot_cred,
                              enrl_status,
                              load_ind);

    aidy_in := stvterm_row.stvterm_fa_proc_yr;

    ---
    IF (SZKITRK.f_FT_restricted(global_pidm)) THEN
      OPEN sfkcurs.sfbetrmc(global_pidm, global_term);
      FETCH sfkcurs.sfbetrmc
        INTO sfbetrm_rec;
      IF sfkcurs.sfbetrmc%FOUND THEN
        max_hr         := sfbetrm_rec.sfbetrm_mhrs_over;
        max_hr         := RPAD(TO_CHAR(max_hr, '999990D990'), 11);
        v_warning_form := '<p>' || '<h4>' ||
                          'You are only permitted to register for ' ||
                          max_hr ||
                          ' hours due to outstanding immunization requirements.  Please submit proof of MMR and/or Varicella (Chicken Pox) if full-time registration is desired.' --TBR8: 1.1
                          || '</h4>' || '</p>' || '<br><br>';
      END IF;
      CLOSE sfkcurs.sfbetrmc;
    END IF;
    ---

    ------Check the condition
    fac_stu_ind := G$_NLS.Get('BWCKREG1-Z001', 'SQL', 'STU');
    dbms_output.put_line('fac_stu_ind' || fac_stu_ind);
    dbms_output.put_line('global awel' || rzkfarc.global_awel_alert);

    --populate sftregs
    /*sfkmods.p_delete_sftregs_by_pidm_term(global_pidm, global_term);
    sfkmods.p_insert_sftregs_from_stcr(global_pidm, global_term, SYSDATE);

    bwcklibs.p_del_sftregs(global_pidm,
                                  global_term,
                                  '82745',
                                  'DW',
                                  'C');
     commit;*/

    IF NVL(rzkfarc.global_awel_alert, 'N') <> 'Y' THEN
      rzkfarc.global_web_ind := 'Y';
      IF rzkfarc.F_Check_Award_Elig_Below(global_pidm, global_term) = 'Y' or
         rzkfarc.F_Check_DSP_Exceeded(global_pidm, global_term) = 'Y' then
        dbms_output.put_line('fac');
        if fac_stu_ind = G$_NLS.Get('BWCKREG1-0036', 'SQL', 'STU') then
          --bwckfrmt.p_open_doc ('bzskawel.P_DispAwardElig');
          --twbkwbis.P_DispInfo ('bzskawel.P_DispAwardElig', 'DEFAULT');
          v_warning_form := v_warning_form || '<p>' || '<h2>' ||
                            'Term Enrollment and Award Eligibility' ||
                            '</h2>' || '</p>' || '<br>';

          v_warning_form := v_warning_form || '<p> <h4>' ||
                            'Your financial aid award is based on a certain enrollment status. If you enroll in fewer credits than required for that status,
you may be ineligible for some or all of the aid for the term. Below are the drops and/or adds you entered on the previous page.
If you select Cancel, your registered courses will NOT be changed. The "revised enrollment" hours below reflect what they WILL BE
only if you select Continue. Please verify that your schedule is accurate before you exit Self Service by returning to the Drop/Add
page after you make your decision.' ||
                            '</h4> </p>';

          v_warning_form := v_warning_form ||
                            twbkfrmt.f_TableOpen('DATADISPLAY',
                                                 cattributes => 'SUMMARY="' ||
                                                                g$_nls.get('BZSKAWEL1-0001',
                                                                           'SQL',
                                                                           'This table will display the student information') || '"');
          v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
          /* BA MC:4.0.5 */
          /*
           twbkfrmt.p_tabledata(
             g$_nls.get ('XXX', 'SQL', 'You are currently enrolled in') ||': '
          );
          */
          v_warning_form := v_warning_form ||
                            twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0002',
                                                            'SQL',
                                                            '<h4>' ||
                                                            'Your revised enrollment hours are') || ': ' ||
                                                 '</h4>');
          /* EA MC:4.0.5 */

          v_warning_form := v_warning_form ||
                            twbkfrmt.f_tabledata('<h4>' ||
                                                 RPAD(TO_CHAR(NVL(tot_cred,
                                                                  0),
                                                              '9990D990'),
                                                      9) || '</h4>');
          v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
          v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
          v_warning_form := v_warning_form ||
                            twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0003',
                                                            'SQL',
                                                            '<h4>' ||
                                                            'Your enrollment status is') || ': ' -- MC:4.0.4 - fixed spelling
                                                 || '</h4>');
          v_warning_form := v_warning_form ||
                            twbkfrmt.f_tabledata('<h4>' || enrl_status ||
                                                 '</h4>');
          v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
          v_warning_form := v_warning_form || twbkfrmt.f_tableclose ||
                            '<br>';

          FOR award_rec IN awards_c(global_pidm, global_term, aidy_in) LOOP
            IF awards_c%ROWCOUNT = 1 THEN
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tableopen('DATADISPLAY',
                                                     cattributes  => 'SUMMARY="' ||
                                                                     g$_nls.get('BZSKAWEL1-0004',
                                                                                'SQL',
                                                                                'summary="This table will display the financial aid information."') ||
                                                                     '" WIDTH="90%"',
                                                     ccaption     => g$_nls.get('BZSKAWEL1-0005',
                                                                                'SQL',
                                                                                '<b><i><h4>' ||
                                                                                'Your Financial Aid Award for %01%' ||
                                                                                '</h4></i></b>',
                                                                                stvterm_row.stvterm_desc));

              v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0006',
                                                                'SQL',
                                                                '<b><h4>' ||
                                                                'Award' ||
                                                                '</h4></b>'));

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0007',
                                                                'SQL',
                                                                '<b><h4>' ||
                                                                'Status' ||
                                                                '</h4></b>'));

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0008',
                                                                'SQL',
                                                                '<b><h4>' ||
                                                                'Amount' ||
                                                                '</h4></b>'));

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0009',
                                                                'SQL',
                                                                '<b><h4>' ||
                                                                'Award Based on This Status:' ||
                                                                '</h4></b>'));

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0010',
                                                                'SQL',
                                                                '<b><h4>' ||
                                                                'Eligible for Awarded Amount:' ||
                                                                '</h4></b>'));

              v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
            END IF;

            v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata('<h4>' ||
                                                   award_rec.rfrbase_fund_title ||
                                                   '</h4>');
            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata('<h4>' ||
                                                   award_rec.rtvawst_desc ||
                                                   '</h4>');

            If award_rec.rtvawst_offer_ind = 'Y' -- MC:4.0.1
             THEN
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     TO_CHAR(award_rec.rpratrm_offer_amt,
                                                             'L9G999G999G999D99') ||
                                                     '</h4>');
            ELSIF award_rec.rtvawst_accept_ind = 'Y' -- MC:4.0.1
             THEN
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     TO_CHAR(award_rec.rpratrm_accept_amt,
                                                             'L9G999G999G999D99') ||
                                                     '</h4>');
            ELSE
              v_warning_form := v_warning_form || twbkfrmt.f_tabledata(' ');
            END IF;

            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata('<h4>' ||
                                                   rzkfarc.F_enrollment_load_desc(award_rec.rpratrm_pckg_load_ind) ||
                                                   '</h4>');

            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata('<h4>' ||
                                                   me_warning.f_get_elig_amt_text(award_rec.rfrbase_fund_code,
                                                                                  award_rec.rpratrm_pckg_load_ind,
                                                                                  award_rec.rfraspc_3Quarter_load_pct,
                                                                                  award_rec.rfraspc_half_load_pct,
                                                                                  award_rec.rfraspc_less_half_load_pct,
                                                                                  award_rec.rfraspc_proration_ind,
                                                                                  load_ind,
                                                                                  award_rec.rfrbase_fed_fund_id) ||
                                                   '</h4>');

            v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;

            row_count := row_count + 1;

          END LOOP;

          v_warning_form := v_warning_form || twbkfrmt.f_tableclose;

          /*IF row_count = 0 THEN
            v_warning_form := v_warning_form || twbkfrmt.P_PrintMessage(g$_nls.get('BZSKAWEL1-0011',
                                               'SQL',
                                               'No Records Found')) ||'<br>';
          END IF;*/

          -- htp.br;
          /* BA MC:4.0.5 */
          /*    Move this web note to below the CANCEL button and web note. */
          /*
          twbkwbis.P_DispInfo ('bzskawel.P_DispAwardElig', 'CONTACT');
          */
          /* EA MC:4.0.5 */

          -- htp.br;

          -- ======================================================
          /*HTP.formopen(twbkwbis.f_cgibin || 'bzskawel.P_ProcAwardElig',
                       cattributes => 'onSubmit="return checkSubmit()"');
          HTP.formhidden('drop_result_label_in', NULL);
          HTP.formhidden('awel_alert_in', rzkfarc.global_awel_alert);
          HTP.formhidden('dsp_alert_in', rzkfarc.global_dsp_alert);
          HTP.formhidden('called_by_in', called_by_in);
          HTP.formhidden('reg_btn', 'dummy');*/
          v_warning_form := v_warning_form || '<br>' || '<h4>' ||
                            'If you select CONTINUE, the enrollment changes (drops/adds) you selected on the previous page will be processed.' ||
                            '<br> <br>' ||
                            'If you select CANCEL, your enrollment will be unchanged. Your drops/adds will NOT be processed.' ||
                            '<br> <br>' ||
                            'Contact the Financial Aid Office if you have questions about changes to your financial aid eligibility based upon enrollment changes.' ||
                            '</h4>';
          p_warning_form := v_warning_form;
          --HTP.formclose;
        end if;
      else
        dbms_output.put_line('else');
        p_warning_form := null;
      end if;
    end if;

    --twbkwbis.P_CloseDoc(curr_release);

  END PZ_DisplayAwardEligable;
  ----------------------------------
  PROCEDURE P_DisplayAwardEligable_new(p_term_in      in varchar2,
                                       p_student_id   in varchar2,
                                       p_add_drop_ind in varchar2,
                                       p_crn          in varchar2 default null,
                                       p_warning_form out clob) IS

    v_warning_form  clob;
    v_pidm          number;
    init_array      OWA_UTIL.ident_arr;
    fac_stu_ind     VARCHAR2(10);
    multi_term_list OWA_UTIL.ident_arr;
    global_term     varchar2(10);
    global_pidm     number;
    called_by_in    VARCHAR2(100) DEFAULT 'ADD_DROP';
    sftregs_rec     sftregs%ROWTYPE;
    V_ROWID_OUT     varchar2(100);
    V_ERROR_MSG     varchar2(500);

    CURSOR C_GET_ALL_CRNS IS
      /*select regexp_substr(P_CRN, '[^,]+', 1, level) CRN
        from dual
      connect by regexp_substr(P_CRN, '[^,]+', 1, level) is not null;*/
      select substr(TRIM(REGEXP_SUBSTR(trim(P_CRN),
                                       '[^,]+',
                                       1,
                                       LEVEL)),
                    1,
                    instr(TRIM(REGEXP_SUBSTR(trim(P_CRN),
                                             '[^,]+',
                                             1,
                                             LEVEL)),
                          '-') - 1) CRN
        from dual
      CONNECT BY REGEXP_SUBSTR(trim(P_CRN), '[^,]+', 1, LEVEL) IS NOT NULL;

    cursor c_register_these_courses is
      select crn, term, rsts_code
        from mobedu_cart
       where student_id = p_student_id
         and term = p_term_in
         and (error_flag = 'Y' or processed_ind = 'N');

    CURSOR awards_c(pidm_in NUMBER, term_in VARCHAR2, aidy_in VARCHAR2) IS

      SELECT rpratrm_fund_code,
             rfrbase_fund_title,
             rtvawst_desc,
             rtvawst_offer_ind, -- MC:4.0.1
             rtvawst_accept_ind, -- MC:4.0.1
             rpratrm_offer_amt,
             rpratrm_accept_amt,
             rpratrm_pckg_load_ind,
             rprawrd_awst_code,
             rfrbase_fund_code,
             rfrbase_fed_fund_id,
             rfraspc_less_half_load_pct,
             rfraspc_half_load_pct,
             rfraspc_3Quarter_load_pct,
             rfraspc_proration_ind
        from rpratrm, rprawrd, rfrbase, rtvawst, rfraspc
       where rpratrm_fund_code = rprawrd_fund_code
         and rpratrm_aidy_code = rprawrd_aidy_code
         and rpratrm_pidm = rprawrd_pidm
         and rpratrm_term_code = term_in
         and rpratrm_pidm = pidm_in
         and rpratrm_aidy_code = aidy_in
         and rfraspc_aidy_code = rprawrd_aidy_code
         and rfrbase_fund_code = rpratrm_fund_code
         and rfraspc_fund_code = rprawrd_fund_code
         and rtvawst_code = rprawrd_awst_code
         and rtvawst_decline_ind = 'N'
         and rtvawst_cancel_ind = 'N';

    tot_cred     SFTREGS.SFTREGS_CREDIT_HR%TYPE;
    tot_bill_hr  SFTREGS.SFTREGS_BILL_HR%TYPE;
    tot_ceu      SFTREGS.SFTREGS_CREDIT_HR%TYPE;
    enrl_status  VARCHAR2(30);
    load_ind     VARCHAR2(1);
    award_rec    awards_c%ROWTYPE;
    row_count    NUMBER DEFAULT 0;
    stvterm_row  stvterm%ROWTYPE;
    sql_error    NUMBER;
    sobterm_row  sobterm%ROWTYPE;
    aidy_in      varchar2(10);
    sfbetrm_rec  sfbetrm%ROWTYPE;
    max_hr       SFBETRM.SFBETRM_MHRS_OVER%TYPE := 0;
    v_disp_cr_hr varchar2(20);

  BEGIN

    begin

      v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);
    dbms_output.put_line('pidm'||v_pidm);
    exception
      when others then

        v_pidm := -100;

    end;
    global_pidm := v_pidm;
    global_term := p_term_in;
    dbms_output.put_line('global pidm '||global_pidm);
    dbms_output.put_line('global term '||global_term);

    open stkterm.stvtermc(global_term);
    fetch stkterm.stvtermc
      into stvterm_row;
    close stkterm.stvtermc;

    IF NOT bwcklibs.f_validterm(sql_error, sobterm_row, global_term) THEN
      raise_application_error(sql_error,
                              bwcklibs.error_msg_table(sql_error));
    END IF;

    aidy_in := stvterm_row.stvterm_fa_proc_yr;
    dbms_output.put_line('aidy code '||aidy_in);
    ---
    IF (SZKITRK.f_FT_restricted(global_pidm)) THEN
      OPEN sfkcurs.sfbetrmc(global_pidm, global_term);
      FETCH sfkcurs.sfbetrmc
        INTO sfbetrm_rec;
      IF sfkcurs.sfbetrmc%FOUND THEN
        max_hr         := sfbetrm_rec.sfbetrm_mhrs_over;
        max_hr         := RPAD(TO_CHAR(max_hr, '999990D990'), 11);
        v_warning_form := '<p>' || '<h4>' ||
                          'You are only permitted to register for ' ||
                          max_hr ||
                          ' hours due to outstanding immunization requirements.  Please submit proof of MMR and/or Varicella (Chicken Pox) if full-time registration is desired.' --TBR8: 1.1
                          || '</h4>' || '</p>' || '<br><br>';
      END IF;
      CLOSE sfkcurs.sfbetrmc;
    END IF;
    ---

    ------Check the condition
    fac_stu_ind := G$_NLS.Get('BWCKREG1-Z001', 'SQL', 'STU');
    dbms_output.put_line('fac_stu_ind' || fac_stu_ind);
    dbms_output.put_line('global awel' || rzkfarc.global_awel_alert);
    dbms_output.put_line('p_ind '|| p_add_drop_ind);
    if p_add_drop_ind = 'D' then
      for R_GET_ALL_CRNS in C_GET_ALL_CRNS loop
        --populate sftregs
        --sfkmods.p_delete_sftregs_by_pidm_term(global_pidm, global_term);
        --sfkmods.p_insert_sftregs_from_stcr(global_pidm, global_term, SYSDATE);

        bwcklibs.p_del_sftregs(global_pidm,
                               global_term,
                               R_GET_ALL_CRNS.CRN,
                               'DW',
                               'C');
        --commit;
        /*select sfrstcr_credit_hr into v_disp_cr_hr
         from sfrstcr
        where sfrstcr_pidm = global_pidm
          and sfrstcr_term_code = global_term
          and sfrstcr_crn = R_GET_ALL_CRNS.CRN;
          v_disp_cr_hr:=v_disp_cr_hr + v_disp_cr_hr;*/
      end loop;
      dbms_output.put_line('v_disp_cr_hr ' || v_disp_cr_hr);
      sfksels.p_get_total_hours_sftregs(tot_cred, --out
                                        tot_bill_hr, --out
                                        tot_ceu, --out
                                        global_term,
                                        global_pidm);

      rzkfarc.P_Get_Enrl_Status(global_pidm,
                                global_term,
                                tot_cred,
                                enrl_status,
                                load_ind);
      IF NVL(rzkfarc.global_awel_alert, 'N') <> 'Y' THEN
        rzkfarc.global_web_ind := 'Y';
        IF rzkfarc.F_Check_Award_Elig_Below(global_pidm, global_term) = 'Y' then
          dbms_output.put_line('fac');
          if fac_stu_ind = G$_NLS.Get('BWCKREG1-0036', 'SQL', 'STU') then
            --bwckfrmt.p_open_doc ('bzskawel.P_DispAwardElig');
            --twbkwbis.P_DispInfo ('bzskawel.P_DispAwardElig', 'DEFAULT');
            v_warning_form := v_warning_form || '<p>' || '<h2>' ||
                              'Term Enrollment and Award Eligibility' ||
                              '</h2>' || '</p>' || '<br>';

            v_warning_form := v_warning_form || '<p> <h4>' ||
                              'Your financial aid award is based on a certain enrollment status. If you enroll in fewer credits than required for that status,
you may be ineligible for some or all of the aid for the term. Below are the drops and/or adds you entered on the previous page.
If you select Cancel, your registered courses will NOT be changed. The "revised enrollment" hours below reflect what they WILL BE
only if you select Continue. Please verify that your schedule is accurate before you exit Self Service by returning to the Drop/Add
page after you make your decision.' ||
                              '</h4> </p>'|| '<br>';

            v_warning_form := v_warning_form ||
                              twbkfrmt.f_TableOpen('DATADISPLAY',
                                                   cattributes => 'SUMMARY="' ||
                                                                  g$_nls.get('BZSKAWEL1-0001',
                                                                             'SQL',
                                                                             'This table will display the student information') || '"');
            v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
            /* BA MC:4.0.5 */
            /*
             twbkfrmt.p_tabledata(
               g$_nls.get ('XXX', 'SQL', 'You are currently enrolled in') ||': '
            );
            */
            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0002',
                                                              'SQL',
                                                              '<h4>' ||
                                                              'Your revised enrollment hours are') || ': ' ||
                                                   '</h4>');
            /* EA MC:4.0.5 */

            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata('<h4>' ||
                                                   RPAD(TO_CHAR(NVL(tot_cred,
                                                                    0),
                                                                '9990D990'),
                                                        9) || '</h4>');
            v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
            v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0003',
                                                              'SQL',
                                                              '<h4>' ||
                                                              'Your enrollment status is') || ': ' -- MC:4.0.4 - fixed spelling
                                                   || '</h4>');
            v_warning_form := v_warning_form ||
                              twbkfrmt.f_tabledata('<h4>' || enrl_status ||
                                                   '</h4>');
            v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
            v_warning_form := v_warning_form || twbkfrmt.f_tableclose ||
                              '<br>';

            FOR award_rec IN awards_c(global_pidm, global_term, aidy_in) LOOP
              IF awards_c%ROWCOUNT = 1 THEN
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tableopen('DATADISPLAY',
                                                       cattributes  => 'SUMMARY="' ||
                                                                       g$_nls.get('BZSKAWEL1-0004',
                                                                                  'SQL',
                                                                                  'summary="This table will display the financial aid information."') ||
                                                                       '" WIDTH="90%"',
                                                       ccaption     => g$_nls.get('BZSKAWEL1-0005',
                                                                                  'SQL',
                                                                                  '<b><i><h4>' ||
                                                                                  'Your Financial Aid Award for %01%' ||
                                                                                  '</h4></i></b>',
                                                                                  stvterm_row.stvterm_desc));

                v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0006',
                                                                  'SQL',
                                                                  '<b><h4>' ||
                                                                  'Award' ||
                                                                  '</h4></b>'));

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0007',
                                                                  'SQL',
                                                                  '<b><h4>' ||
                                                                  'Status' ||
                                                                  '</h4></b>'));

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0008',
                                                                  'SQL',
                                                                  '<b><h4>' ||
                                                                  'Amount' ||
                                                                  '</h4></b>'));

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0009',
                                                                  'SQL',
                                                                  '<b><h4>' ||
                                                                  'Award Based on This Status:' ||
                                                                  '</h4></b>'));

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0010',
                                                                  'SQL',
                                                                  '<b><h4>' ||
                                                                  'Eligible for Awarded Amount:' ||
                                                                  '</h4></b>'));

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tablerowclose;
              END IF;

              v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     award_rec.rfrbase_fund_title ||
                                                     '</h4>');
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     award_rec.rtvawst_desc ||
                                                     '</h4>');

              If award_rec.rtvawst_offer_ind = 'Y' -- MC:4.0.1
               THEN
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata('<h4>' ||
                                                       TO_CHAR(award_rec.rpratrm_offer_amt,
                                                               'L9G999G999G999D99') ||
                                                       '</h4>');
              ELSIF award_rec.rtvawst_accept_ind = 'Y' -- MC:4.0.1
               THEN
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata('<h4>' ||
                                                       TO_CHAR(award_rec.rpratrm_accept_amt,
                                                               'L9G999G999G999D99') ||
                                                       '</h4>');
              ELSE
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata(' ');
              END IF;

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     rzkfarc.F_enrollment_load_desc(award_rec.rpratrm_pckg_load_ind) ||
                                                     '</h4>');

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     me_warning.f_get_elig_amt_text(award_rec.rfrbase_fund_code,
                                                                                    award_rec.rpratrm_pckg_load_ind,
                                                                                    award_rec.rfraspc_3Quarter_load_pct,
                                                                                    award_rec.rfraspc_half_load_pct,
                                                                                    award_rec.rfraspc_less_half_load_pct,
                                                                                    award_rec.rfraspc_proration_ind,
                                                                                    load_ind,
                                                                                    award_rec.rfrbase_fed_fund_id) ||
                                                     '</h4>');

              v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;

              row_count := row_count + 1;

            END LOOP;

            v_warning_form := v_warning_form || twbkfrmt.f_tableclose;

            v_warning_form := v_warning_form || '<br>' || '<h4>' ||
                              'If you select CONTINUE, the enrollment changes (drops/adds) you selected on the previous page will be processed.' ||
                              '<br> <br>' ||
                              'If you select CANCEL, your enrollment will be unchanged. Your drops/adds will NOT be processed.' ||
                              '<br> <br>' ||
                              'Contact the Financial Aid Office if you have questions about changes to your financial aid eligibility based upon enrollment changes.' ||
                              '</h4>';
            p_warning_form := v_warning_form;

          end if;
        else
          dbms_output.put_line('else');
          p_warning_form := null;
        end if;
      end if;
      sfkmods.p_delete_sftregs_by_pidm_term(global_pidm, global_term);
      sfkmods.p_insert_sftregs_from_stcr(global_pidm, global_term, SYSDATE);

    elsif p_add_drop_ind = 'A' then
      dbms_output.put_line('courses to cart');
      bwcklibs.p_initvalue(global_pidm, global_term, '', '', '', '');
      for r_register_this_course in c_register_these_courses loop
        dbms_output.put_line('adding course to cart');
         sfkmods.p_delete_sftregs_by_pidm_term(global_pidm, global_term);
        sfkmods.p_insert_sftregs_from_stcr(global_pidm, global_term, SYSDATE);
        add_course_pkg_corq.p_regs_mobile(r_register_this_course.term,
                                          r_register_this_course.crn,
                                          r_register_this_course.rsts_code,
                                          global_pidm,
                                          V_ERROR_MSG,
                                          V_ROWID_OUT);
       -- commit;

      end loop;
      if V_ERROR_MSG is null then
        sfksels.p_get_total_hours_sftregs(tot_cred, --out
                                          tot_bill_hr, --out
                                          tot_ceu, --out
                                          global_term,
                                          global_pidm);

        rzkfarc.P_Get_Enrl_Status(global_pidm,
                                  global_term,
                                  tot_cred,
                                  enrl_status,
                                  load_ind);

        IF NVL(rzkfarc.global_awel_alert, 'N') <> 'Y' THEN
          rzkfarc.global_web_ind := 'Y';
          IF rzkfarc.F_Check_Award_Elig_Below(global_pidm, global_term) = 'Y' then
            dbms_output.put_line('fac');
            if fac_stu_ind = G$_NLS.Get('BWCKREG1-0036', 'SQL', 'STU') then
              --bwckfrmt.p_open_doc ('bzskawel.P_DispAwardElig');
              --twbkwbis.P_DispInfo ('bzskawel.P_DispAwardElig', 'DEFAULT');
              v_warning_form := v_warning_form || '<p>' || '<h2>' ||
                                'Term Enrollment and Award Eligibility' ||
                                '</h2>' || '</p>' || '<br>';

              v_warning_form := v_warning_form || '<p> <h4>' ||
                                'Your financial aid award is based on a certain enrollment status. If you enroll in fewer credits than required for that status,
you may be ineligible for some or all of the aid for the term. Below are the drops and/or adds you entered on the previous page.
If you select Cancel, your registered courses will NOT be changed. The "revised enrollment" hours below reflect what they WILL BE
only if you select Continue. Please verify that your schedule is accurate before you exit Self Service by returning to the Drop/Add
page after you make your decision.' ||
                                '</h4> </p>'|| '<br>';

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_TableOpen('DATADISPLAY',
                                                     cattributes => 'SUMMARY="' ||
                                                                    g$_nls.get('BZSKAWEL1-0001',
                                                                               'SQL',
                                                                               'This table will display the student information') || '"');
              v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
              /* BA MC:4.0.5 */
              /*
               twbkfrmt.p_tabledata(
                 g$_nls.get ('XXX', 'SQL', 'You are currently enrolled in') ||': '
              );
              */
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0002',
                                                                'SQL',
                                                                '<h4>' ||
                                                                'Your revised enrollment hours are') || ': ' ||
                                                     '</h4>');
              /* EA MC:4.0.5 */

              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' ||
                                                     RPAD(TO_CHAR(NVL(tot_cred,
                                                                      0),
                                                                  '9990D990'),
                                                          9) || '</h4>');
              v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
              v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0003',
                                                                'SQL',
                                                                '<h4>' ||
                                                                'Your enrollment status is') || ': ' -- MC:4.0.4 - fixed spelling
                                                     || '</h4>');
              v_warning_form := v_warning_form ||
                                twbkfrmt.f_tabledata('<h4>' || enrl_status ||
                                                     '</h4>');
              v_warning_form := v_warning_form || twbkfrmt.f_tablerowclose;
              v_warning_form := v_warning_form || twbkfrmt.f_tableclose ||
                                '<br>';

              FOR award_rec IN awards_c(global_pidm, global_term, aidy_in) LOOP
                IF awards_c%ROWCOUNT = 1 THEN
                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tableopen('DATADISPLAY',
                                                         cattributes  => 'SUMMARY="' ||
                                                                         g$_nls.get('BZSKAWEL1-0004',
                                                                                    'SQL',
                                                                                    'summary="This table will display the financial aid information."') ||
                                                                         '" WIDTH="90%"',
                                                         ccaption     => g$_nls.get('BZSKAWEL1-0005',
                                                                                    'SQL',
                                                                                    '<b><i><h4>' ||
                                                                                    'Your Financial Aid Award for %01%' ||
                                                                                    '</h4></i></b>',
                                                                                    stvterm_row.stvterm_desc));

                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tablerowopen;
                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0006',
                                                                    'SQL',
                                                                    '<b><h4>' ||
                                                                    'Award' ||
                                                                    '</h4></b>'));

                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0007',
                                                                    'SQL',
                                                                    '<b><h4>' ||
                                                                    'Status' ||
                                                                    '</h4></b>'));

                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0008',
                                                                    'SQL',
                                                                    '<b><h4>' ||
                                                                    'Amount' ||
                                                                    '</h4></b>'));

                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0009',
                                                                    'SQL',
                                                                    '<b><h4>' ||
                                                                    'Award Based on This Status:' ||
                                                                    '</h4></b>'));

                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata(g$_nls.get('BZSKAWEL1-0010',
                                                                    'SQL',
                                                                    '<b><h4>' ||
                                                                    'Eligible for Awarded Amount:' ||
                                                                    '</h4></b>'));

                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tablerowclose;
                END IF;

                v_warning_form := v_warning_form || twbkfrmt.f_tablerowopen;
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata('<h4>' ||
                                                       award_rec.rfrbase_fund_title ||
                                                       '</h4>');
                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata('<h4>' ||
                                                       award_rec.rtvawst_desc ||
                                                       '</h4>');

                If award_rec.rtvawst_offer_ind = 'Y' -- MC:4.0.1
                 THEN
                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata('<h4>' ||
                                                         TO_CHAR(award_rec.rpratrm_offer_amt,
                                                                 'L9G999G999G999D99') ||
                                                         '</h4>');
                ELSIF award_rec.rtvawst_accept_ind = 'Y' -- MC:4.0.1
                 THEN
                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata('<h4>' ||
                                                         TO_CHAR(award_rec.rpratrm_accept_amt,
                                                                 'L9G999G999G999D99') ||
                                                         '</h4>');
                ELSE
                  v_warning_form := v_warning_form ||
                                    twbkfrmt.f_tabledata(' ');
                END IF;

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata('<h4>' ||
                                                       rzkfarc.F_enrollment_load_desc(award_rec.rpratrm_pckg_load_ind) ||
                                                       '</h4>');

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tabledata('<h4>' ||
                                                       me_warning.f_get_elig_amt_text(award_rec.rfrbase_fund_code,
                                                                                      award_rec.rpratrm_pckg_load_ind,
                                                                                      award_rec.rfraspc_3Quarter_load_pct,
                                                                                      award_rec.rfraspc_half_load_pct,
                                                                                      award_rec.rfraspc_less_half_load_pct,
                                                                                      award_rec.rfraspc_proration_ind,
                                                                                      load_ind,
                                                                                      award_rec.rfrbase_fed_fund_id) ||
                                                       '</h4>');

                v_warning_form := v_warning_form ||
                                  twbkfrmt.f_tablerowclose;

                row_count := row_count + 1;

              END LOOP;

              v_warning_form := v_warning_form || twbkfrmt.f_tableclose;

              v_warning_form := v_warning_form || '<br>' || '<h4>' ||
                                'If you select CONTINUE, the enrollment changes (drops/adds) you selected on the previous page will be processed.' ||
                                '<br> <br>' ||
                                'If you select CANCEL, your enrollment will be unchanged. Your drops/adds will NOT be processed.' ||
                                '<br> <br>' ||
                                'Contact the Financial Aid Office if you have questions about changes to your financial aid eligibility based upon enrollment changes.' ||
                                '</h4>';
              p_warning_form := v_warning_form;

            end if;
          else
            dbms_output.put_line('else');
            p_warning_form := null;
          end if;
        end if;
      else
        GB_COMMON.P_ROLLBACK;
      end if;
      sfkmods.p_delete_sftregs_by_pidm_term(global_pidm, global_term);
    end if;
  GB_COMMON.P_COMMIT;
    --twbkwbis.P_CloseDoc(curr_release);

  END P_DisplayAwardEligable_new;

----------------------------------

END me_warning;
/

