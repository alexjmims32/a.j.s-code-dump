prompt Importing table mobile_invoice_links...
set feedback off
set define off
truncate table mobile_invoice_links;
insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('MHP1', 'https://entappst0.utoledo.edu/mobileselfservice/?f=ins', 'Click here to waive or upgrade', to_date('16-07-2013 15:45:26', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('MHP2', 'https://entappst0.utoledo.edu/mobileselfservice/?f=ins', 'Click here to waive or upgrade', to_date('16-07-2013 15:45:26', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('M1LF', 'https://entappst0.utoledo.edu/mobileselfservice/?f=leg', 'Click here to Waive', to_date('16-07-2013 15:45:27', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('TIV', 'https://entappst0.utoledo.edu/mobileselfservice/?f=tit', 'Click for Title IV Info', to_date('16-07-2013 15:45:28', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('IPP', 'https://entappst0.utoledo.edu/mobileselfservice/?f=ipp', 'Click for IPP', to_date('16-07-2013 15:45:28', 'dd-mm-yyyy hh24:mi:ss'));

COMMIT;

prompt Done.
