PROMPT CREATE OR REPLACE PACKAGE me_curriculum_change
CREATE OR REPLACE package me_curriculum_change as

  -- Author  : n2n IT Services
  -- Created : 03-08-2013 19:04:59
  -- Purpose : Client specific code, differs with client version

  -- Public function and procedure declarations

  -------------------------------------------------------------------------------

  FUNCTION FZ_SMC_CatalogYear(term STVTERM.STVTERM_CODE%TYPE) RETURN VARCHAR2;
  FUNCTION FZ_CHECK_CURR(P_STUDENT_ID VARCHAR2, P_TERM_CODE VARCHAR2)
    RETURN VARCHAR2;

  --PROCEDURE TO VALIDATE THE STUDENT HAS A RECORD IN TEMP TABLE
  PROCEDURE PZ_CHECK_CURRCULUM(P_STUDENT_ID VARCHAR2,
                               P_TERM_CODE  VARCHAR2,
                               P_CURR_IND   VARCHAR2 DEFAULT NULL,
                               P_CAT_IND    VARCHAR2 DEFAULT NULL,
                               P_GRAD_IND   VARCHAR2 DEFAULT NULL,
                               P_OUT_MSG    OUT VARCHAR2,
                               P_SHOW_MSG   OUT CLOB);
  --------------
  --FUNCTION TO GET DEFAULT CURRICULUM
  FUNCTION FZ_GET_DEFAULT_VALUES(P_STUDENT_ID VARCHAR2,
                                 P_TERM_CODE  VARCHAR2,
                                 P_TYPE       VARCHAR2) RETURN VARCHAR2;
  FUNCTION FZ_SMC_GetCatalogYears(P_STUDENT_ID VARCHAR2,
                                  P_TERM_CODE  STVTERM.STVTERM_CODE%TYPE)
    RETURN SYS_REFCURSOR;

  PROCEDURE PZ_SMC_CURR_SUBMIT(P_STUDENT_ID     IN SPRIDEN.SPRIDEN_ID%TYPE,
                               P_TERM_CODE      IN SZTCATT.SZTCATT_TERM_CODE%TYPE,
                               P_MAJR_CODE      IN SZTCATT.SZTCATT_MAJR_CODE%TYPE DEFAULT NULL,
                               P_CATALOG_YEAR   IN VARCHAR2 DEFAULT NULL,
                               P_GRAD_TERM_CODE IN SZTCATT.SZTCATT_EXP_GRAD_TERM%TYPE DEFAULT NULL,
                               P_OUT_MSG        OUT VARCHAR2);
  PROCEDURE PZ_CURR_DATA(P_STUDENT_ID VARCHAR2,
                         P_TERM_CODE  VARCHAR2,
                         P_CURR_XML   OUT CLOB);

end me_curriculum_change;
/

