PROMPT CREATE OR REPLACE PACKAGE BODY me_curriculum_change
CREATE OR REPLACE package body me_curriculum_change is
  -----------
  TYPE curriculum_table_rec IS RECORD(
    code         SORLFOS.SORLFOS_MAJR_CODE%TYPE,
    description  STVMAJR.STVMAJR_DESC%TYPE,
    degree_code  SORLCUR.SORLCUR_DEGC_CODE%TYPE,
    catalog_term SORLCUR.SORLCUR_TERM_CODE_CTLG%TYPE,
    program      SORLCUR.SORlCUR_PROGRAM%TYPE,
    catalog_year VARCHAR2(11),
    admittable   NUMBER);
  TYPE term_table_rec IS RECORD(
    code        STVTERM.STVTERM_CODE%TYPE,
    description STVTERM.STVTERM_DESC%TYPE);

  ------------

  --------------------
  -- Retrieve formatted catalog year for the term. ie. for 201420, returns 2013 - 2014
  --
  FUNCTION FZ_SMC_CatalogYear(term STVTERM.STVTERM_CODE%TYPE) RETURN VARCHAR2 IS
    CURSOR c_select(term STVTERM.STVTERM_CODE%TYPE) IS
      SELECT CASE
               WHEN STVTERM_FA_PROC_YR like '0%' THEN
                '20' || SUBSTR(STVTERM_FA_PROC_YR, 1, 2) || ' - 20' ||
                SUBSTR(STVTERM_FA_PROC_YR, 3, 2)
               WHEN STVTERM_FA_PROC_YR like '1%' THEN
                '20' || SUBSTR(STVTERM_FA_PROC_YR, 1, 2) || ' - 20' ||
                SUBSTR(STVTERM_FA_PROC_YR, 3, 2)
               ELSE
                '19' || SUBSTR(STVTERM_FA_PROC_YR, 1, 2) || ' - 19' ||
                SUBSTR(STVTERM_FA_PROC_YR, 3, 2)
             END AS CATALOG_YEAR
        FROM STVTERM
       WHERE STVTERM_CODE = term;

    catalogYear VARCHAR2(11) := NULL;
  BEGIN
    open c_select(term);
    fetch c_select
      into catalogYear;
    close c_select;

    RETURN catalogYear;
  END;

  -- Retrieve catalog information from queue (SZTCATT table) in case there's multiple terms open
  -- and catalog info given for one term hasn't been applied to banner yet before the student goes
  -- in to the next term (and gets prompted - they should see their choices (from queue) not from
  -- old values in Banner
  --
  FUNCTION FZ_SMC_GetQueuedCatalogTerm(pidm SZTCATT.SZTCATT_PIDM%TYPE)
    RETURN VARCHAR2 IS
    CURSOR c_select(pidm SZTCATT.SZTCATT_PIDM%TYPE) IS
      SELECT FZ_SMC_CatalogYear(SZTCATT_TERM_CODE_CTLG)
        FROM SZTCATT
       WHERE SZTCATT_PIDM = pidm
         AND SZTCATT_PROCESSED_IND IS NULL;

    catalogYear VARCHAR2(11) := NULL;

  BEGIN
    open c_select(pidm);
    fetch c_select
      into catalogYear;
    close c_select;

    RETURN catalogYear;
  END FZ_SMC_GetQueuedCatalogTerm;
  -------------------------
  -- Retrieve expected graduation term from queue (SZTCATT table) in case there's multiple terms open
  -- and exp grad term info given for one term hasn't been applied to banner yet before the student goes
  -- in to the next term (and gets prompted - they should see their choices (from queue) not from
  -- old values in Banner
  --
  FUNCTION FZ_SMC_GetQueuedExpGradTerm(pidm SZTCATT.SZTCATT_PIDM%TYPE)
    RETURN term_table_rec IS
    CURSOR c_select(pidm SZTCATT.SZTCATT_PIDM%TYPE) IS
      SELECT SZTCATT.SZTCATT_EXP_GRAD_TERM, STVTERM.STVTERM_DESC
        FROM SZTCATT
       INNER JOIN STVTERM
          ON SZTCATT_EXP_GRAD_TERM = STVTERM_CODE
       WHERE SZTCATT_PIDM = pidm
         AND SZTCATT_PROCESSED_IND IS NULL;

    grad_term term_table_rec;

  BEGIN
    open c_select(pidm);
    fetch c_select
      into grad_term;
    close c_select;

    RETURN grad_term;
  END FZ_SMC_GetQueuedExpGradTerm;
  ------------------------------
  -- Retrieve current expected graduation date for the term
  --
  -- We always pull the latest SGBSTDN record, regardless
  -- whether it is in the future or not.
  -- Also the SGBSTDN field is a Date field and we want a
  -- STVTERM code returned so we find the STVTERM code
  -- that the date falls within.
  --
  -- We can not use SGBSTDN_TERM_CODE_GRAD since it's always
  -- populated.
  --
  FUNCTION FZ_SMC_GetCurrExpectGradDate(pidm SGBSTDN.SGBSTDN_PIDM%TYPE)
    RETURN term_table_rec IS
    CURSOR c_select(pidm SGBSTDN.SGBSTDN_PIDM%TYPE) IS
      SELECT STVTERM_CODE, STVTERM_DESC
        FROM SGBSTDN A, STVTERM
       WHERE SGBSTDN_PIDM = pidm
         AND STVTERM_START_DATE <= SGBSTDN_EXP_GRAD_DATE
         AND STVTERM_END_DATE >= SGBSTDN_EXP_GRAD_DATE
         AND SGBSTDN_TERM_CODE_EFF =
             (SELECT MAX(SGBSTDN_TERM_CODE_EFF)
                FROM SGBSTDN B
               WHERE A.SGBSTDN_PIDM = B.SGBSTDN_PIDM
                 AND B.SGBSTDN_TERM_CODE_EFF <= '999999');
    gradDate term_table_rec;

  BEGIN

    open c_select(pidm);
    fetch c_select
      into gradDate;
    close c_select;

    RETURN gradDate;
  END FZ_SMC_GetCurrExpectGradDate;

  -----------------------
  -- Returns student's curriculum info
  --
  -- By 'current', we are referring to their active curriculum
  -- regardless of term (it may be a future term)
  --
  FUNCTION FZ_SMC_GetStuCurrentCurriculum(pidm SORLCUR.SORLCUR_PIDM%TYPE)
    RETURN curriculum_table_rec IS
    CURSOR c_select(pidm SORLCUR.SORLCUR_PIDM%TYPE) IS
      SELECT SORLFOS_MAJR_CODE,
             STVMAJR_DESC,
             SORLCUR_DEGC_CODE,
             SORLCUR_TERM_CODE_CTLG,
             SORLCUR_PROGRAM,
             FZ_SMC_CatalogYear(STVTERM_CODE),
             NULL AS ADMITTABLE
        FROM SORLCUR A
        left outer join SMRPRLE
          on SORLCUR_PROGRAM = SMRPRLE_PROGRAM, SORLFOS
       INNER JOIN STVTERM
          ON SORLFOS_TERM_CODE = STVTERM_CODE, STVMAJR, STVCACT
       WHERE SORLCUR_PIDM = pidm
         AND SORLCUR_CACT_CODE = STVCACT_CODE
         AND SORLCUR_PIDM = SORLFOS_PIDM
         AND SORLFOS_LCUR_SEQNO = SORLCUR_SEQNO
         AND SORLFOS_LFST_CODE = 'MAJOR'
         AND SORLCUR_LMOD_CODE = 'LEARNER'
         AND SORLCUR_CACT_CODE = 'ACTIVE'
         AND SORLCUR_CURRENT_CDE = 'Y'
         AND STVMAJR_CODE = SORLFOS_MAJR_CODE
         AND SORLCUR_SEQNO =
             (SELECT MAX(SORLCUR_SEQNO)
                FROM SORLCUR B
               where B.SORLCUR_PIDM = A.SORLCUR_PIDM
                 AND A.SORLCUR_LMOD_CODE = B.SORLCUR_LMOD_CODE
                 AND B.SORLCUR_TERM_CODE =
                     (SELECT MAX(SORLCUR_TERM_CODE)
                        FROM SORLCUR C
                       WHERE C.SORLCUR_PIDM = B.SORLCUR_PIDM
                         AND C.SORLCUR_LMOD_CODE = B.SORLCUR_LMOD_CODE
                         AND C.SORLCUR_TERM_CODE <= '999999'))
       ORDER BY SORLCUR_PIDM ASC, SORLCUR_TERM_CODE_CTLG DESC;

    curriculum curriculum_table_rec;

  BEGIN

    open c_select(pidm);
    fetch c_select
      into curriculum;
    close c_select;

    RETURN curriculum;
  END FZ_SMC_GetStuCurrentCurriculum;
  ---------------------------------
  -- Retrieve curriculum information from queue (SZTCATT table) in case there's multiple terms open
  -- and curriculum info given for one term hasn't been applied to banner yet before the student goes
  -- in to the next term (and gets prompted - they should see their choices (from queue) not from
  -- old values in Banner
  --
  FUNCTION FZ_SMC_GetStuQueuedCurriculum(pidm SZTCATT.SZTCATT_PIDM%TYPE,
                                         term SORCMJR.SORCMJR_TERM_CODE_EFF%TYPE)
    RETURN curriculum_table_rec IS
    CURSOR c_degree_code(term      SORCMJR.SORCMJR_TERM_CODE_EFF%TYPE,
                         majr_code SORCMJR.SORCMJR_MAJR_CODE%TYPE) IS
      SELECT SOBCURR_DEGC_CODE
        FROM SOBCURR
       INNER JOIN SORCMJR A
          ON SORCMJR_CURR_RULE = SOBCURR_CURR_RULE
       INNER JOIN STVMAJR
          ON SORCMJR_MAJR_CODE = STVMAJR_CODE
       WHERE STVMAJR_CODE = majr_code
         AND SORCMJR_TERM_CODE_EFF =
             (SELECT MAX(SORCMJR_TERM_CODE_EFF)
                FROM SORCMJR B
               WHERE A.SORCMJR_CURR_RULE = B.SORCMJR_CURR_RULE
                 AND SORCMJR_TERM_CODE_EFF <= term)
         AND SORCMJR_MAJR_CODE NOT IN ('MACR');
    CURSOR c_select(pidm SZTCATT.SZTCATT_PIDM%TYPE) IS
      SELECT SZTCATT_MAJR_CODE,
             STVMAJR.STVMAJR_DESC,
             NULL,
             SZTCATT_TERM_CODE_CTLG,
             NULL,
             FZ_SMC_CatalogYear(SZTCATT_TERM_CODE_CTLG),
             NULL
        FROM SZTCATT
       INNER JOIN STVMAJR
          ON SZTCATT_MAJR_CODE = STVMAJR_CODE
       WHERE SZTCATT_PIDM = pidm
         AND SZTCATT_PROCESSED_IND IS NULL;

    curriculum curriculum_table_rec := NULL;

  BEGIN
    open c_select(pidm);
    fetch c_select
      into curriculum;
    close c_select;

    open c_degree_code(term, curriculum.code);
    fetch c_degree_code
      into curriculum.degree_code;
    close c_degree_code;

    RETURN curriculum;
  END FZ_SMC_GetStuQueuedCurriculum;
  --*************************************************************--
  --FUNCTION TO VALIDATE THE STUDENT HAS A RECORD IN TEMP TABLE
  FUNCTION FZ_CHECK_CURR(P_STUDENT_ID VARCHAR2, P_TERM_CODE VARCHAR2)
    RETURN VARCHAR2 IS
    V_PIDM  NUMBER;
    V_COUNT NUMBER;
  BEGIN
    BEGIN
      V_PIDM := GB_COMMON.F_GET_PIDM(P_STUDENT_ID);
      DBMS_OUTPUT.PUT_LINE('PIDM ' || V_PIDM);
    EXCEPTION
      WHEN OTHERS THEN
        RETURN 'INVALID STUDENT';
    END;
    BEGIN
      select COUNT(1)
        INTO V_COUNT
        from sztcatt
       where sztcatt_pidm = V_PIDM
         AND SZTCATT_TERM_CODE = P_TERM_CODE;
    END;
    IF V_COUNT = 0 THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;
  END;
  --PROCEDURE TO VALIDATE THE STUDENT HAS A RECORD IN TEMP TABLE
  PROCEDURE PZ_CHECK_CURRCULUM(P_STUDENT_ID VARCHAR2,
                               P_TERM_CODE  VARCHAR2,
                               P_CURR_IND   VARCHAR2 DEFAULT NULL,
                               P_CAT_IND    VARCHAR2 DEFAULT NULL,
                               P_GRAD_IND   VARCHAR2 DEFAULT NULL,
                               P_OUT_MSG    OUT VARCHAR2,
                               P_SHOW_MSG   OUT CLOB) IS
    V_PIDM                NUMBER;
    V_COUNT               NUMBER := 0;
    currentGraduationTerm term_table_rec;
    queuedGraduationTerm  term_table_rec;
    currentCurriculum     curriculum_table_rec;
    queuedCurriculum      curriculum_table_rec;
    currentCatalogYear    VARCHAR2(11);
    queuedCatalogYear     VARCHAR2(11);
    --PAGE_NAME VARCHAR2(128):='bzskcatt.P_SMC_AddDropVerCurr';
  BEGIN

    BEGIN
      V_PIDM := GB_COMMON.F_GET_PIDM(P_STUDENT_ID);
      DBMS_OUTPUT.PUT_LINE('PIDM ' || V_PIDM);
    EXCEPTION
      WHEN OTHERS THEN
        P_OUT_MSG := 'INVALID STUDENT';
    END;
    BEGIN
      select COUNT(1)
        INTO V_COUNT
        from sztcatt
       where sztcatt_pidm = V_PIDM
         AND SZTCATT_TERM_CODE = P_TERM_CODE;
      IF V_COUNT = 0 AND P_CURR_IND IS NULL AND P_CAT_IND IS NULL AND
         P_GRAD_IND IS NULL THEN
        -- show current values then ask if they would like to change
        /*queuedCurriculum   := FZ_SMC_GetStuQueuedCurriculum(V_PIDM,
        P_TERM_CODE);*/
        currentCurriculum  := FZ_SMC_GetStuCurrentCurriculum(V_PIDM);
        currentCatalogYear := currentCurriculum.catalog_year;

        -- Check if the queue curriculum exists and if so, does it differ.
        -- If it does, replace currentCurriculum var with it
        --
        /* IF queuedCurriculum.code IS NOT NULL THEN
          IF queuedCurriculum.catalog_term <>
             currentCurriculum.catalog_term AND
             queuedCurriculum.catalog_year <>
             currentCurriculum.catalog_year AND
             queuedCurriculum.code <> currentCurriculum.code THEN
            currentCurriculum := queuedCurriculum;
          END IF;
        END IF;*/

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'You are currently pursuing a curriculum of <b>' ||
                      currentCurriculum.description || ' (' ||
                      currentCurriculum.degree_code || ')</b>' ||
                      '</h4></p><br>';

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Is this correct, or would you like to change the curriculum that you are pursuing?' ||
                      '</h4></p><br>';

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Changing your curriculum can affect dramatically your financial aid. Please talk with financial aid before changing your major.' ||
                      '</h4></p><br>';
        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Once changes have been submitted each term, further changes will need to be submitted through the Advising Office, CSBG 1104, or the Niles Area Campus.' ||
                      '</h4></p><br>';

      ELSIF P_CURR_IND = 'Y' AND P_CAT_IND IS NULL AND P_GRAD_IND IS NULL THEN
        queuedCatalogYear := FZ_SMC_GetQueuedCatalogTerm(V_PIDM);
        IF queuedCatalogYear IS NOT NULL THEN
          currentCatalogYear := queuedCatalogYear;
        END IF;
        -- show current catalog year and ask if they would like to change

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'You are currently following the catalog year of ' ||
                      currentCatalogYear || '</h4></p><br>';

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' || 'Is this correct?' ||
                      '</h4></p><br>';

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Once changes have been submitted each term, further changes will need to be submitted through the Advising Office, CSBG 1104, or the Niles Area Campus.' ||
                      '</h4></p><br>';

      ELSIF P_CURR_IND IS NULL AND P_CAT_IND = 'Y' AND P_GRAD_IND IS NULL THEN
        queuedGraduationTerm := FZ_SMC_GetQueuedExpGradTerm(V_PIDM);
        IF queuedGraduationTerm.code IS NOT NULL THEN
          currentGraduationTerm := queuedGraduationTerm;
        END IF;
        -- show current expected graduation date and ask if they would like to change

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'You are currently expecting to graduate in the term of ' ||
                      currentGraduationTerm.description || '</h4></p><br>';

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      '<br /><br />Is this correct?' || '</h4></p><br>';

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Once changes have been submitted each term, further changes will need to be submitted through the Advising Office, CSBG 1104, or the Niles Area Campus.' ||
                      '</h4></p><br>';
      ELSIF P_CURR_IND = 'N' AND P_CAT_IND IS NULL AND P_GRAD_IND IS NULL THEN

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'What curriculum are you pursuing?' ||
                      '</h4></p><br>';
        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'If the curriculum you are seeking is not displayed, please click the back button and return to the curriculum confirmation page and confirm your current curriculum by clicking "Yes, it is correct."  Then contact the Advising Office, CSBG 1104, or the Niles Area Campus to change your curriculum.  Continue with the catalog year and graduation term screens.' ||
                      '</h4></p><br>';

        --SHOW THE AVAILABLE CURRICULUM
        HTP.formsubmit(NULL, 'Submit');
        HTP.formclose;
        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Once changes have been submitted each term, further changes will need to be submitted through the Advising Office, CSBG 1104, or the Niles Area Campus.' ||
                      '</h4></p><br>';
      ELSIF P_CURR_IND IS NULL AND P_CAT_IND = 'N' AND P_GRAD_IND IS NULL THEN

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'What catalog year are you following?' ||
                      '</h4></p><br>';
        --SHOW THE AVAILABLE CATALOG YEARS

        HTP.formsubmit(NULL, 'Submit');
        HTP.formclose;
        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Once changes have been submitted each term, further changes will need to be submitted through the Advising Office, CSBG 1104, or the Niles Area Campus.' ||
                      '</h4></p><br>';

      ELSIF P_CURR_IND IS NULL AND P_CAT_IND IS NULL AND P_GRAD_IND = 'N' THEN

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'What is your expected graduation term?' ||
                      '</h4></p><br>';

        --SHOW THE EXPECTED GRADUATION TERM

        HTP.formsubmit(NULL, 'Submit');
        HTP.formclose;

        P_SHOW_MSG := P_SHOW_MSG || '<p><h4>' ||
                      'Once changes have been submitted each term, further changes will need to be submitted through the Advising Office, CSBG 1104, or the Niles Area Campus.' ||
                      '</h4></p><br>';

      END IF;
    END;
  END;
  ----------------------------
  FUNCTION FZ_GET_DEFAULT_VALUES(P_STUDENT_ID VARCHAR2,
                                 P_TERM_CODE  VARCHAR2,
                                 P_TYPE       VARCHAR2) RETURN VARCHAR2 IS
    V_PIDM                NUMBER;
    V_DEFAULT_CURRICULUM  VARCHAR2(500);
    currentCurriculum     curriculum_table_rec;
    queuedCurriculum      curriculum_table_rec;
    queuedCatalogYear     VARCHAR2(11);
    currentCatalogYear    VARCHAR2(11);
    queuedGraduationTerm  term_table_rec;
    currentGraduationTerm term_table_rec;
  BEGIN
    BEGIN
      V_PIDM := GB_COMMON.F_GET_PIDM(P_STUDENT_ID);
      DBMS_OUTPUT.PUT_LINE('PIDM ' || V_PIDM);
    EXCEPTION
      WHEN OTHERS THEN
        V_PIDM := -1;
    END;
    queuedCurriculum      := FZ_SMC_GetStuQueuedCurriculum(V_PIDM,
                                                           P_TERM_CODE);
    currentCurriculum     := FZ_SMC_GetStuCurrentCurriculum(V_PIDM);
    queuedCatalogYear     := FZ_SMC_GetQueuedCatalogTerm(V_PIDM);
    currentCatalogYear    := currentCurriculum.catalog_year;
    queuedGraduationTerm  := FZ_SMC_GetQueuedExpGradTerm(V_PIDM);
    currentGraduationTerm := FZ_SMC_GetCurrExpectGradDate(V_PIDM);
    IF P_TYPE = 'CURR' THEN
      IF queuedCurriculum.code IS NOT NULL THEN
        IF queuedCurriculum.catalog_term <> currentCurriculum.catalog_term AND
           queuedCurriculum.catalog_year <> currentCurriculum.catalog_year AND
           queuedCurriculum.code <> currentCurriculum.code THEN
          currentCurriculum := queuedCurriculum;
        END IF;
      END IF;
      V_DEFAULT_CURRICULUM := currentCurriculum.description;
      RETURN V_DEFAULT_CURRICULUM;
    ELSIF P_TYPE = 'CATL' THEN
      IF queuedCatalogYear IS NOT NULL THEN
        DBMS_OUTPUT.PUT_LINE('WHEN QUEUED CATALOG IS NOT NULL');
        currentCatalogYear := queuedCatalogYear;
      END IF;
      DBMS_OUTPUT.PUT_LINE('C_YEAR' || currentCatalogYear);
      RETURN currentCatalogYear;
    ELSIF P_TYPE = 'GRAD' THEN
      IF queuedGraduationTerm.code IS NOT NULL THEN
        currentGraduationTerm := queuedGraduationTerm;
      END IF;
      RETURN currentGraduationTerm.description;
    END IF;
  END;
  --------
  --FUNCTION TO GET CATALOG YEAR
  -- Retrieve 7 catalog years from current year (if they have a correspending term in
  -- SMC academic history). Any furture term available in SOBTERM (on SOATERM form) is
  -- included.
  --
  FUNCTION FZ_SMC_GetCatalogYears(P_STUDENT_ID VARCHAR2,
                                  P_TERM_CODE  STVTERM.STVTERM_CODE%TYPE)
    RETURN SYS_REFCURSOR IS
    P_COURSE_OUT SYS_REFCURSOR;
    numberOfYears CONSTANT NUMBER := 7;
    V_PIDM NUMBER;

  BEGIN
    BEGIN
      V_PIDM := GB_COMMON.F_GET_PIDM(P_STUDENT_ID);
      DBMS_OUTPUT.PUT_LINE('PIDM ' || V_PIDM);
    EXCEPTION
      WHEN OTHERS THEN
        V_PIDM := -1;
    END;
    open P_COURSE_OUT FOR
      SELECT DISTINCT FZ_SMC_CatalogYear(STVTERM_CODE) AS CATALOG_YEAR,
                      STVTERM_CODE CATALOG_TERM_CODE
        FROM STVTERM
       INNER JOIN SOBTERM
          ON STVTERM_CODE = SOBTERM_TERM_CODE
        LEFT OUTER JOIN SHRTCKN
          ON STVTERM_CODE = SHRTCKN_TERM_CODE
         AND SHRTCKN_PIDM = V_PIDM
       WHERE STVTERM_CODE NOT LIKE '%99'
         AND STVTERM_CODE >= (SUBSTR(P_TERM_CODE, 1, 4) - numberOfYears ||
             SUBSTR(P_TERM_CODE, -2)) HAVING
       SHRTCKN_PIDM IS NOT NULL
          OR STVTERM_CODE >= P_TERM_CODE
       GROUP BY SHRTCKN_PIDM, STVTERM_CODE
       ORDER BY CATALOG_YEAR ASC;

    RETURN P_COURSE_OUT;

  END Fz_SMC_GetCatalogYears;
  --  Save 'attributes' in temp table.
  --
  PROCEDURE PZ_SMC_CURR_SUBMIT(P_STUDENT_ID     IN SPRIDEN.SPRIDEN_ID%TYPE,
                               P_TERM_CODE      IN SZTCATT.SZTCATT_TERM_CODE%TYPE,
                               P_MAJR_CODE      IN SZTCATT.SZTCATT_MAJR_CODE%TYPE DEFAULT NULL,
                               P_Catalog_Year   In Varchar2 Default Null,
                               P_Grad_Term_Code In Sztcatt.Sztcatt_Exp_Grad_Term%Type Default Null,
                               P_OUT_MSG        OUT VARCHAR2) IS

    V_PIDM NUMBER;
  Begin
    Begin
      P_OUT_MSG :='SUCCESS';
      V_PIDM := GB_COMMON.F_GET_PIDM(P_STUDENT_ID);
      DBMS_OUTPUT.PUT_LINE('PIDM ' || V_PIDM);
    EXCEPTION
      WHEN OTHERS THEN
        V_PIDM := -1;
    END;
    IF V_PIDM > 0 THEN
      INSERT INTO SZTCATT
        (SZTCATT_SURROGATE_ID,
         SZTCATT_PIDM,
         SZTCATT_TERM_CODE,
         SZTCATT_MAJR_CODE,
         SZTCATT_EXP_GRAD_TERM,
         SZTCATT_TERM_CODE_CTLG,
         SZTCATT_ACTIVITY_DATE,
         SZTCATT_CREATE_DATE)
      VALUES
        (SZTCATT_ID_SEQ.NEXTVAL,
         V_PIDM,
         P_TERM_CODE,
         P_MAJR_CODE,
         P_GRAD_TERM_CODE,
         P_CATALOG_YEAR,
         SYSDATE,
         SYSDATE);
      COMMIT;
    End If;

    Exception When Others Then

    P_OUT_MSG := 'Error occured while updating.';
  END PZ_SMC_CURR_SUBMIT;

  PROCEDURE PZ_CURR_DATA(P_STUDENT_ID VARCHAR2,
                         P_TERM_CODE  VARCHAR2,
                         P_CURR_XML   OUT CLOB) IS
    V_PIDM                 NUMBER;
    v_seq                  number := 1;
    V_MAJOR_CODE_DESC      VARCHAR2(100);
    V_CATALOG_YEAR         VARCHAR2(100);
    V_GRADUATION_CODE_DESC VARCHAR2(100);
    CURR_xml_data          XMLTYPE;
    l_refcursor            SYS_REFCURSOR;
    V_COUNT                number;
    V_TERM_CODE_CTLG       VARCHAR2(10);
    CURSOR C_MAJOR IS
      SELECT MAJR_CODE, MAJR_DESC FROM ME_AVAIL_CURRICULUM;
    CURSOR C_GRAD IS
      SELECT TERM_CODE, TERM_DESC
        FROM ME_AVAIL_GRAD_TERM
       WHERE TERM_CODE >= P_TERM_CODE;

    CURSOR C_CATALOG(pidm_in spriden.spriden_pidm%TYPE,
                     term_in stvterm.stvterm_code%TYPE) IS
      SELECT DISTINCT FZ_SMC_CatalogYear(STVTERM_CODE) AS CATALOG_YEAR,
                      STVTERM_CODE CATALOG_TERM_CODE
        FROM STVTERM
       INNER JOIN SOBTERM
          ON STVTERM_CODE = SOBTERM_TERM_CODE
        LEFT OUTER JOIN SHRTCKN
          ON STVTERM_CODE = SHRTCKN_TERM_CODE
         AND SHRTCKN_PIDM = pidm_in
       WHERE STVTERM_CODE NOT LIKE '%99'
         AND STVTERM_CODE >=
             (SUBSTR(term_in, 1, 4) - 7 || SUBSTR(term_in, -2))
       HAVING SHRTCKN_PIDM IS NOT NULL
          OR STVTERM_CODE >= term_in
       GROUP BY SHRTCKN_PIDM, STVTERM_CODE
       ORDER BY CATALOG_YEAR ASC;
  BEGIN
    v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);

    dbms_output.put_line('Pidm:' || v_pidm);
    BEGIN
      select COUNT(1)
        INTO V_COUNT
        from sztcatt
       where sztcatt_pidm = V_PIDM
         AND SZTCATT_TERM_CODE = P_TERM_CODE;
    END;
    dbms_output.put_line('V_COUNT:' || V_COUNT);
    IF V_COUNT = 0 THEN

      SELECT me_curriculum_change.fz_get_default_values(P_STUDENT_ID,
                                                        P_TERM_CODE,
                                                        'CURR')
        INTO V_MAJOR_CODE_DESC
        FROM DUAL;
      SELECT me_curriculum_change.fz_get_default_values(P_STUDENT_ID,
                                                        P_TERM_CODE,
                                                        'CATL')
        INTO V_CATALOG_YEAR
        FROM DUAL;

      SELECT me_curriculum_change.fz_get_default_values(P_STUDENT_ID,
                                                        P_TERM_CODE,
                                                        'GRAD')
        INTO V_GRADUATION_CODE_DESC
        FROM DUAL;

      begin
        begin
        SELECT SZTCATT_TERM_CODE_CTLG
          into V_TERM_CODE_CTLG
          FROM SZTCATT
         WHERE SZTCATT_PIDM = V_PIDM
           AND SZTCATT_PROCESSED_IND IS NULL;
           dbms_output.put_line('catalog'||V_TERM_CODE_CTLG);
           exception when others then
             V_TERM_CODE_CTLG:=null;
             end;
        if V_TERM_CODE_CTLG is null then
          SELECT SORLCUR_TERM_CODE_CTLG
            into V_TERM_CODE_CTLG
            FROM SORLCUR A
            left outer join SMRPRLE
              on SORLCUR_PROGRAM = SMRPRLE_PROGRAM, SORLFOS
           INNER JOIN STVTERM
              ON SORLFOS_TERM_CODE = STVTERM_CODE, STVMAJR, STVCACT
           WHERE SORLCUR_PIDM = V_PIDM
             AND SORLCUR_CACT_CODE = STVCACT_CODE
             AND SORLCUR_PIDM = SORLFOS_PIDM
             AND SORLFOS_LCUR_SEQNO = SORLCUR_SEQNO
             AND SORLFOS_LFST_CODE = 'MAJOR'
             AND SORLCUR_LMOD_CODE = 'LEARNER'
             AND SORLCUR_CACT_CODE = 'ACTIVE'
             AND SORLCUR_CURRENT_CDE = 'Y'
             AND STVMAJR_CODE = SORLFOS_MAJR_CODE
             AND SORLCUR_SEQNO =
                 (SELECT MAX(SORLCUR_SEQNO)
                    FROM SORLCUR B
                   where B.SORLCUR_PIDM = A.SORLCUR_PIDM
                     AND A.SORLCUR_LMOD_CODE = B.SORLCUR_LMOD_CODE
                     AND B.SORLCUR_TERM_CODE =
                         (SELECT MAX(SORLCUR_TERM_CODE)
                            FROM SORLCUR C
                           WHERE C.SORLCUR_PIDM = B.SORLCUR_PIDM
                             AND C.SORLCUR_LMOD_CODE = B.SORLCUR_LMOD_CODE
                             AND C.SORLCUR_TERM_CODE <= '999999'))
           ORDER BY  SORLCUR_TERM_CODE_CTLG DESC;
           dbms_output.put_line('cat'||V_TERM_CODE_CTLG);
        end if;

      end;

      OPEN l_refcursor FOR
        SELECT P_STUDENT_ID AS STUDENT, P_TERM_CODE AS TERM FROM DUAL;

      CURR_xml_data := XMLTYPE(l_refcursor);

      CLOSE l_refcursor;

      FOR R_MAJOR IN C_MAJOR LOOP
        SELECT INSERTCHILDXML(CURR_xml_data,
                              '/ROWSET',
                              'RECORD',
                              XMLElement("RECORD",
                                         XMLElement("MAJOR_CODE_DESCRIPTION",
                                                    MAJOR_CODE_DESCRIPTION),
                                         XMLElement("MAJOR_CODE", MAJOR_CODE),
                                         XMLElement("SEQ_NO", SEQ_NO),
                                         XMLElement("DEFAULT", DEFAULT_VALUE)))
          into CURR_xml_data
          FROM (select R_MAJOR.MAJR_DESC MAJOR_CODE_DESCRIPTION,
                       R_MAJOR.MAJR_CODE MAJOR_CODE,
                       v_seq SEQ_NO,
                       DECODE(V_MAJOR_CODE_DESC, R_MAJOR.MAJR_DESC, 'Y', 'N') DEFAULT_VALUE
                  FROM DUAL) XD;
        v_seq := v_seq + 1;
      END LOOP;
      --
      SELECT INSERTCHILDXML(CURR_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("CATALOG_YEAR",
                                                  CATALOG_YEAR),
                                       XMLElement("CATALOG_TERM_CODE",
                                                  CATALOG_TERM_CODE),
                                       XMLElement("SEQ_NO", SEQ_NO),
                                       XMLElement("DEFAULT", DEFAULT_VALUE)))
        into CURR_xml_data
        FROM (select V_CATALOG_YEAR CATALOG_YEAR,
                     V_TERM_CODE_CTLG CATALOG_TERM_CODE,
                     v_seq SEQ_NO,
                     'Y' DEFAULT_VALUE
                FROM DUAL) XD;

      v_seq := v_seq + 1;

      /*FOR R_CATALOG IN C_CATALOG(V_PIDM, P_TERM_CODE) LOOP
        SELECT INSERTCHILDXML(CURR_xml_data,
                              '/ROWSET',
                              'RECORD',
                              XMLElement("RECORD",
                                         XMLElement("CATALOG_YEAR",
                                                    CATALOG_YEAR),
                                         XMLElement("CATALOG_TERM_CODE",
                                                    CATALOG_TERM_CODE),
                                         XMLElement("SEQ_NO", SEQ_NO),
                                         XMLElement("DEFAULT", DEFAULT_VALUE)))
          into CURR_xml_data
          FROM (select R_CATALOG.CATALOG_YEAR CATALOG_YEAR,
                       R_CATALOG.CATALOG_TERM_CODE CATALOG_TERM_CODE,
                       v_seq SEQ_NO,
                       'N' DEFAULT_VALUE
                  FROM DUAL) XD;
        v_seq := v_seq + 1;
      END LOOP;*/
      --
      FOR R_GRAD IN C_GRAD LOOP
        SELECT INSERTCHILDXML(CURR_xml_data,
                              '/ROWSET',
                              'RECORD',
                              XMLElement("RECORD",
                                         XMLElement("TERM_CODE_DESCRIPTION",
                                                    TERM_CODE_DESCRIPTION),
                                         XMLElement("TERM_CODE", TERM_CODE),
                                         XMLElement("SEQ_NO", SEQ_NO),
                                         XMLElement("DEFAULT", DEFAULT_VALUE)))
          into CURR_xml_data
          FROM (select R_GRAD.TERM_DESC TERM_CODE_DESCRIPTION,
                       R_GRAD.TERM_CODE TERM_CODE,
                       v_seq SEQ_NO,
                       DECODE(V_GRADUATION_CODE_DESC,
                              R_GRAD.TERM_DESC,
                              'Y',
                              'N') DEFAULT_VALUE
                  FROM DUAL) XD;
        v_seq := v_seq + 1;
      END LOOP;

      if CURR_xml_data is not null then
        P_CURR_XML := CURR_xml_data.getClobVal;
      end if;
    ELSE
      P_CURR_XML := NULL;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      SELECT INSERTCHILDXML(CURR_xml_data,
                            '/ROWSET',
                            'ERROR',
                            XMLElement("ERROR",
                                       XMLElement("ERROR_MSG", ERROR_MSG)))
        into CURR_xml_data
        FROM (select 'System Error! Could not get data.' ERROR_MSG FROM DUAL) XD;

      P_CURR_XML := CURR_xml_data.getClobVal;

      GB_COMMON.p_rollback;
  END;
begin

  NULL;

end me_curriculum_change;
/

