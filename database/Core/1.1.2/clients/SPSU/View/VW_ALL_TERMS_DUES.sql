create or replace view vw_all_terms_dues as
with Memo_Amounts as
  (
  select  tbrmemo_pidm Memo_pidm, tbrmemo_term_code Memo_term_code, sum(decode(tbbdetc_type_ind,'C',tbrmemo_amount,tbrmemo_amount*-1))
  Memo_Balance from TBRMEMO join  tbbdetc
   on tbbdetc_detail_code = tbrmemo_detail_code
  group by tbrmemo_pidm, tbrmemo_term_code
  ) ,
  Financial_Aid_Amounts as
  (
select tbraccd_pidm FinAid_pidm , tbraccd_term_code FinAid_term_code , sum(decode(tbbdetc_type_ind,'C',tbraccd_amount,tbraccd_amount*-1)) FinAid_Balance from tbraccd join tbbdetc on
  tbraccd_detail_code = tbbdetc_detail_code
  and tbraccd_srce_code = 'F'
  and tbbdetc_dcat_code  = 'FA' group by tbraccd_pidm, tbraccd_term_code
  ) ,
Balance_Amounts as
(
select tbraccd_pidm PIDM , tbraccd_term_code TERM_CODE , sum(decode(tbbdetc_type_ind,'C',tbraccd_amount,tbraccd_amount*-1))  Balance_Amount from tbraccd join tbbdetc on
  tbraccd_detail_code = tbbdetc_detail_code
 group by tbraccd_pidm, tbraccd_term_code
)
select PIDM , TERM_CODE , Memo_Balance , FinAid_Balance , Balance_Amount + nvl(Memo_Balance,0) + nvl(FinAid_Balance,0) Total_Balance from
Balance_Amounts left join
Memo_Amounts
on PIDM = Memo_pidm
and TERM_CODE = Memo_term_code
left join Financial_Aid_Amounts
on Memo_pidm = FinAid_pidm
and Memo_term_code = FinAid_term_code;

