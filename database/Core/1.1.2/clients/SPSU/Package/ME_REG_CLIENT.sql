PROMPT CREATE OR REPLACE PACKAGE ME_REG_CLIENT 
create or replace package ME_REG_CLIENT as

  -- Author  : n2n IT Services
  -- Created : 03-08-2013 19:04:59
  -- Purpose : Client specific code, differs with client version

  -- Public function and procedure declarations

  function fz_get_linked_courses(P_CRN       ssrlink.ssrlink_crn%type,
                                 P_TERM_CODE ssrlink.ssrlink_term_code%type)
  
   RETURN varchar2;

   FUNCTION fz_check_biographic_info(P_STUDENT_ID SPRIDEN.SPRIDEN_ID%TYPE)
    RETURN CHAR;

  function fz_check_grad_term(V_PIDM sgbstdn.sgbstdn_pidm%type,
                              P_TERM stvterm.stvterm_code%type) RETURN CHAR;
  FUNCTION fz_reg_prevented(p_pidm IN spriden.spriden_pidm%TYPE)
    RETURN varchar2;
end ME_REG_CLIENT;
/
