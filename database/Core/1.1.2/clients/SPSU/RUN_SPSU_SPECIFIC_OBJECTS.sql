
spool 1_RUN_SPSU_SPECIFIC_OBJECTS.log
@@Synonym\spbpers.sql
@@Procedure\PZ_ME_AUTHENTICATION.sql
@@Package\ME_ACCOUNT_OBJECTS.sql
@@Package\Z_CM_MOBILE_CAMPUS.sql
@@Package\ME_REG_CLIENT.sql
@@View\ACCT_SUMM_SPSU.sql
@@View\CHARGE_PAY_DETAIL_SPSU.sql
@@View\PERSON_CAMPUS_VW.sql
@@View\SUMMARY_BY_TERM_SPSU.sql
@@View\VW_ALL_TERMS_DUES.sql
@@PackageBody\ME_ACCOUNT_OBJECTS.sql
@@PackageBody\Z_CM_MOBILE_CAMPUS.sql
@@PackageBody\ME_REG_CLIENT.sql

spool off
exit
