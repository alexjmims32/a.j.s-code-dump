PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start 2_Rollback.sql"   
PROMPT ====================================================

spool 2_Rollback.log


@@rollback\Package\DROP_COURSE_PKG.sql
@@rollback\PackageBody\DROP_COURSE_PKG.sql


EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
