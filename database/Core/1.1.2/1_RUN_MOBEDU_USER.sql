PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start 1_RUN_MOBEDU_USER.sql"   
PROMPT ====================================================

spool 1_RUN_MOBEDU_USER.log

@@patch\Table\PRIVILEGE.sql
@@patch\Package\ADD_COURSE_PKG_CORQ.sql
@@patch\Package\DROP_COURSE_PKG.sql
@@patch\Package\ME_REG_CLIENT.sql
@@patch\Package\ME_REG_UTILS.sql
@@patch\Package\ME_VALID.sql
@@patch\MaterializedView\person_mv.sql
@@patch\PackageBody\ADD_COURSE_PKG_CORQ.sql
@@patch\PackageBody\DROP_COURSE_PKG.sql
@@patch\PackageBody\ME_REG_CLIENT.sql
@@patch\PackageBody\ME_REG_UTILS.sql
@@patch\PackageBody\ME_VALID.sql
@@patch\View\STU_HOLD_INFO.sql
@@patch\View\COURSE_SEARCH_VW.sql
@@patch\View\TERM_COURSE_DETAILS_VW.sql
@@patch\View\ENUM_VW.sql

EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
