--created materialized view to increase the performance of the query
CREATE MATERIALIZED VIEW person_mv
BUILD IMMEDIATE 
REFRESH COMPLETE
START WITH systdate
NEXT sysdate+60/1440
AS
select 
a.spriden_id id,
a.spriden_pidm pidm,
a.spriden_last_name lst_name,
a.spriden_first_name first_name,
a.spriden_mi middle_name,
a.spriden_first_name || ' ' || a.spriden_last_name Full_name,
--'' ssn,
b.spbpers_birth_date dob,
DECODE(b.spbpers_sex,'M','Male','F','Female','') gender,
b.spbpers_name_prefix prefix,
z_cm_mobile_campus.fz_category(A.SPRIDEN_PIDM) CATEGORY,
A.SPRIDEN_CREATE_DATE published,
A.SPRIDEN_ACTIVITY_DATE updated/*,
me_reg_utils.fz_get_camp(A.SPRIDEN_PIDM) campus*/
from spriden a join SPBPERS b
on a.spriden_pidm=b.spbpers_pidm
and a.spriden_change_ind is null
and a.spriden_entity_ind='P';