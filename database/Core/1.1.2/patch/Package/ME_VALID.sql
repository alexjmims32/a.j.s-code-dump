PROMPT CREATE OR REPLACE PACKAGE me_valid
CREATE OR REPLACE PACKAGE me_valid is

 TYPE rstsrowc_rec_typ IS RECORD(

      stvrsts_code                  stvrsts.stvrsts_code%TYPE,
      stvrsts_desc                  stvrsts.stvrsts_desc%TYPE,
      ssrrsts_usage_cutoff_pct_from ssrrsts.ssrrsts_usage_cutoff_pct_from%TYPE,
      ssrrsts_usage_cutoff_pct_to   ssrrsts.ssrrsts_usage_cutoff_pct_to%TYPE,
      ssrrsts_usage_cutoff_dur_from ssrrsts.ssrrsts_usage_cutoff_dur_from%TYPE,
      ssrrsts_usage_cutoff_dur_to   ssrrsts.ssrrsts_usage_cutoff_dur_to%TYPE);

   TYPE rstsrowc_typ IS REF CURSOR
      RETURN rstsrowc_rec_typ;

  FUNCTION f_reg_access(P_PIDM_IN     SPRIDEN.SPRIDEN_PIDM%TYPE,
                        P_TERM_CODE   STVTERM.STVTERM_CODE%TYPE,
                        P_SOURCE      VARCHAR2,
                        P_ACCESS_ID   VARCHAR2,
                        P_REGS_DATE   DATE,
                        P_BY_PASS_IND VARCHAR2) RETURN VARCHAR2;

  FUNCTION f_reg_attempts_good(pidm_in IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                               term_in IN STVTERM.STVTERM_CODE%TYPE)
    RETURN VARCHAR2;

  FUNCTION f_reg_access_still_good(pidm_in      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                                   term_in      IN STVTERM.STVTERM_CODE%TYPE,
                                   call_path_in IN VARCHAR2,
                                   proc_name_in IN VARCHAR2) RETURN BOOLEAN;

  function fz_me_val_adms(pidm in varchar2, term_code in varchar2)
    return varchar2;
  --

  --
  function fz_me_val_student(pidm in varchar2, term_code in varchar2)
    return varchar2;
  --

  FUNCTION FZ_GET_ALL_RESTRICTIONS(P_CRN IN NUMBER, P_TERM_CODE IN NUMBER)
    RETURN T_ARRAY_TYPE;
  --
  FUNCTION FZ_GET_FAILED_RESTRICTIONS(P_CRN        IN NUMBER,
                                      P_TERM_CODE  IN NUMBER,
                                      P_STUDENT_ID VARCHAR2) RETURN VARCHAR2;
  --
  FUNCTION FZ_DUPL_CHECK(P_CRN       IN NUMBER,
                         P_TERM_CODE IN NUMBER,
                         P_PIDM      NUMBER) RETURN VARCHAR2;
  --
  FUNCTION FZ_TIME_CHECK(P_CRN       VARCHAR2,
                         P_TERM_CODE VARCHAR2,
                         P_PIDM      NUMBER) RETURN VARCHAR2;
  --
  FUNCTION FZ_GROUP_CHECK(P_PIDM NUMBER, P_TERM VARCHAR2, P_CRN VARCHAR2)
    RETURN VARCHAR2;
  --
  function fz_me_corq_check(p_pidm number,
                            p_crn  in number,
                            p_term in number) return varchar2;
  --
  FUNCTION FZ_MHRS_CHECK(P_PIDM NUMBER, P_TERM VARCHAR2, P_CRN VARCHAR2)
    RETURN VARCHAR2;
  --
  function FZ_PREREQ_CHECK(P_CRN                 IN NUMBER,
                           P_TERM_CODE           IN NUMBER,
                           P_STUDENT_ID          IN VARCHAR2,
                           P_ERROR_MSG           OUT VARCHAR2,
                           a_failed_prereqs_subj OUT t_array_type,
                           a_failed_prereqs_crse OUT t_array_type,
                           a_failed_prereq_desc  out t_array_type)
    RETURN VARCHAR2;
  --------------
  --function to check alternate pin
  /* FUNCTION F_ALT_PIN(pidm      IN saturn.spriden.spriden_pidm%TYPE,
                   term_code in saturn.stvterm.stvterm_code%type)
  RETURN VARCHAR2; */

  ----------------------
  /* PROCEDURE PZ_STUDENT_COURSE_REGISTRATION(p_crn        IN NUMBER,
  P_student_ID IN VARCHAR2,
  P_TERM       IN NUMBER,
  P_ERROR_MSG  OUT VARCHAR2);*/
  --function to get meeting number,room code,start date and end date.
  FUNCTION fz_ssrmeetvalues(p_term     IN VARCHAR2,
                            p_crn      IN VARCHAR2,
                            p_position IN NUMBER,
                            p_ind      IN VARCHAR2) RETURN VARCHAR2;

  PROCEDURE PZ_REGISTRATION_CHECKS(P_STUDENT_ID IN VARCHAR2,
                                   P_TERM       IN varchar2,
                                   P_ERROR_MSG  OUT VARCHAR2);
  function FZ_GET_TERM(P_CREATE_DATE varchar2) return varchar2;
  procedure PZ_GET_TERM(P_CREATE_DATE varchar2,
                        P_TERM        OUT VARCHAR2,
                        P_TERM_DESC   OUT VARCHAR2,
                        P_ERROR_MSG   OUT VARCHAR2);

  FUNCTION F_STU_GETWEBREGSRSTS
               (req_ind  in varchar2)
   return varchar2;
  FUNCTION FZ_CHECK_SFRRSTS(P_TERM_CODE VARCHAR2,
                            P_CRN       NUMBER,
                            P_REG_IND   VARCHAR2) RETURN VARCHAR2;
  FUNCTION FZ_GET_RSTS_CODE(p_term_code VARCHAR2,
                            p_ptrm_code VARCHAR2,
                            p_rsts_code VARCHAR2,
                            p_pidm      NUMBER,
                            p_crn       VARCHAR2,
                            p_field     VARCHAR2) RETURN VARCHAR2;
end me_valid;
/

