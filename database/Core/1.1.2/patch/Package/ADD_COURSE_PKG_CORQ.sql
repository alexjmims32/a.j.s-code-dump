PROMPT CREATE OR REPLACE PACKAGE add_course_pkg_corq
CREATE OR REPLACE PACKAGE add_course_pkg_corq AS

  function f_get_ptrm_code(p_crn_in    in SSBSECT.SSBSECT_CRN%TYPE,
                           p_term_code in STVTERM.STVTERM_CODE%TYPE)
    return varchar2;

  procedure pz_rect5_prev_errs_nd_ad_2_crt(p_student_id in varchar2,
                                           p_term_in    in varchar2,
                                           p_error      out varchar2);

  PROCEDURE p_regs_mobile(p_term_in   sftregs.sftregs_term_code%type,
                          p_crn_in    sftregs.sftregs_crn%type,
                          p_rsts_in   sftregs.sftregs_rsts_code%type,
                          p_pidm_in   sftregs.sftregs_pidm%type,
                          p_error_msg out varchar2,
                          p_rowid_in  out varchar2);

  procedure pz_register_courses_in_cart(p_student_id in spriden.spriden_id%type,
                                        p_term_in    in stvterm.stvterm_code%type,
                                        p_response   out varchar2,
                                        p_debug      in varchar2 default 'N');

  procedure pz_do_validations(term_in                IN OWA_UTIL.ident_arr,
                              pidm_in                IN spriden.spriden_pidm%TYPE,
                              etrm_done_in_out       IN OUT BOOLEAN,
                              capp_tech_error_in_out IN OUT VARCHAR2,
                              drop_problems_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              drop_failures_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              error_msg              out varchar2);

  procedure pz_final_commit(pidm_in            IN sftregs.sftregs_pidm%TYPE,
                            term_in            IN sftregs.sftregs_term_code%TYPE,
                            reg_date_in        IN sftregs.sftregs_add_date%TYPE,
                            clas_code_in       IN sgrclsr.sgrclsr_clas_code%TYPE,
                            styp_code_in       IN sgbstdn.sgbstdn_styp_code%TYPE,
                            capc_severity_in   IN sobterm.sobterm_capc_severity%TYPE,
                            tmst_calc_ind_in   IN sobterm.sobterm_tmst_calc_ind%TYPE,
                            tmst_maint_ind_in  IN sfbetrm.sfbetrm_tmst_maint_ind%TYPE,
                            tmst_code_in       IN sfbetrm.sfbetrm_tmst_code%TYPE,
                            drop_last_class_in IN BOOLEAN DEFAULT TRUE,
                            system_in          IN VARCHAR2,
                            error_rec_out      OUT sftregs%ROWTYPE,
                            error_flag_out     OUT VARCHAR2,
                            tmst_flag_out      OUT VARCHAR2);

end add_course_pkg_corq;

/

