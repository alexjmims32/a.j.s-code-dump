PROMPT ====================================================
PROMPT      <<< Login as MOBEDU/MOBILE USER to run >>>      
PROMPT      USAGE: Run SQL Plus and input "start DO.sql"   
PROMPT ====================================================

spool DO.log

prompt
prompt Creating Synonyms
prompt =========================
prompt
@@Synonyms.sql
prompt
prompt Creating Types
prompt =========================
prompt
@@Types.sql
@@Type\T_ARRAY_TYPE.sql
prompt
prompt Creating Tables
prompt =========================
prompt
@@Table\ADMIN_PRIV.sql
@@Table\ADMIN_ROLE.sql
@@Table\ADMIN_USER.sql
@@Table\CAMPUS.sql
@@Table\EMERGENCY_CONTACTS.sql
@@Table\EMERGENCY_CONTACTS_AUDIT.sql
@@Table\FAQ.sql
@@Table\FEEDBACK.sql
@@Table\FEEDBACK_DATA.sql
@@Table\FEEDS.sql
@@Table\FEEDS_AUDIT.sql
@@Table\HELP.sql
@@Table\HELP_AUDIT.sql
@@Table\MAPS.sql
@@Table\MAPS_AUDIT.sql
@@Table\MOBEDU_CART.sql
@@Table\MOBEDU_TRASH.sql
@@Table\MODULE.sql
@@Table\MODULES.sql
@@Table\NOTICE.sql
@@Table\NOTICELOG.sql
@@Table\NOTICEPOPULATION.sql
@@Table\NOTICETYPE.sql
@@Table\POPULATIONTYPE.sql
@@Table\PRIVILEGE.sql
@@Table\PRIVILEGE_AUDIT.sql
@@Table\ROLE.sql
@@Table\ADMIN_ROLE~INX.sql
@@Table\MOBEDU_CART~INX.sql
@@Table\MOBEDU_TRASH~INX.sql
@@Table\NOTICE~UNQ.sql
@@Table\NOTICEPOPULATION~UNQ.sql
@@Table\NOTICETYPE~UNQ.sql
@@Table\POPULATIONTYPE~UNQ.sql
@@Table\NOTICEPOPULATION~FK.sql
@@Table\PLSQL_PROFILER_DATA~FK.sql
@@Table\PLSQL_PROFILER_UNITS~FK.sql
prompt
prompt Creating Sequences
prompt =========================
prompt
@@Sequence\FEEDBACK_SEQ.sql
@@Sequence\NOTICELOG_SEQ.sql
@@Sequence\NOTICE_SEQ.sql
prompt
prompt Creating Procedures
prompt =========================
prompt
@@Procedure\PZ_DROP_COURSE.sql
@@Procedure\PZ_ME_AUTHENTICATION.sql
prompt
prompt Creating Packages
prompt =========================
prompt
@@Package\ADD_COURSE_PKG_CORQ.sql
@@Package\CART_PKG.sql
@@Package\DROP_COURSE_PKG.sql
@@Package\GOKODSF.sql
@@Package\ME_ACCOUNT_OBJECTS.sql
@@Package\ME_ALT_PIN_PKG.sql
@@Package\ME_BWCKCOMS.sql
@@Package\ME_BWCKGENS.sql
@@Package\ME_BWCKREGS.sql
@@Package\ME_REG_UTILS.sql
@@Package\ME_VALID.sql
@@Package\ME_WITHDRAWL.sql
@@Package\Z_CM_MOBILE_CAMPUS.sql
prompt
prompt Creating Views
prompt =========================
prompt
@@View\ACCOUNT_SUMMARY_VIEW.sql
@@View\ACCT_SUMM_VW.sql
@@View\CART_DETAILS_VW.sql
@@View\CHARGE_PAY_DETAIL_VW.sql
@@View\CONTACT_VW.sql
@@View\TERMS_TO_REGISTER_VW.sql
@@View\COURSE_SEARCH_VW.sql
@@View\CRSE_MEET_VW.sql
@@View\GENERAL_PERSON_VW.sql
@@View\ME_HOURS_SUMMARY_BY_TERM.sql
@@View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@View\MOBEDU_HOLIDAY_VW.sql
@@View\PERSON_CAMPUS_VW.sql
@@View\PERSON_VW.sql
@@View\STUDENT_ACCOUNT_DETAIL.sql
@@View\STU_CURCULAM_INFO.sql
@@View\STU_HOLD_INFO.sql
@@View\STU_PROFILE_INFO.sql
@@View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@View\SUMMARY_BY_TERM_VW.sql
@@View\TERMS_TO_BURSAR_VW.sql
@@View\TERM_COURSE_DETAILS_VW.sql
@@View\VW_TERM_CHARGES.sql
@@View\VW_TERM_PAYMENTS.sql
prompt
prompt Creating Package Bodies
prompt =========================
prompt
@@PackageBody\ADD_COURSE_PKG_CORQ.sql
@@PackageBody\CART_PKG.sql
@@PackageBody\DROP_COURSE_PKG.sql
@@PackageBody\GOKODSF.sql
@@PackageBody\ME_ACCOUNT_OBJECTS.sql
@@PackageBody\ME_ALT_PIN_PKG.sql
@@PackageBody\ME_BWCKCOMS.sql
@@PackageBody\ME_BWCKGENS.sql
@@PackageBody\ME_BWCKREGS.sql
@@PackageBody\ME_REG_UTILS.sql
@@PackageBody\ME_VALID.sql
@@PackageBody\ME_WITHDRAWL.sql
@@PackageBody\Z_CM_MOBILE_CAMPUS.sql

prompt
prompt Recompiling Schema
prompt =========================
prompt
EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
