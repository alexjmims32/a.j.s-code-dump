PROMPT CREATE OR REPLACE PACKAGE add_course_pkg_corq
CREATE OR REPLACE PACKAGE add_course_pkg_corq AS

  procedure pz_rect5_prev_errs_nd_ad_2_crt(p_student_id in varchar2,
                                           p_term_in    in varchar2,
                                           p_error      out varchar2);

  /*  procedure pz_check_cart_empty(p_student_id in varchar2,
  p_term_in    in varchar2,
  p_error      out varchar2);*/

  PROCEDURE p_regs(term_in       IN OWA_UTIL.ident_arr,
                   rsts_in       IN OWA_UTIL.ident_arr,
                   assoc_term_in IN OWA_UTIL.ident_arr,
                   crn_in        IN OWA_UTIL.ident_arr,
                   start_date_in IN OWA_UTIL.ident_arr,
                   end_date_in   IN OWA_UTIL.ident_arr,
                   subj          IN OWA_UTIL.ident_arr,
                   crse          IN OWA_UTIL.ident_arr,
                   sec           IN OWA_UTIL.ident_arr,
                   levl          IN OWA_UTIL.ident_arr,
                   cred          IN OWA_UTIL.ident_arr,
                   gmod          IN OWA_UTIL.ident_arr,
                   title         IN bwckcoms.varchar2_tabtype,
                   mesg          IN OWA_UTIL.ident_arr,
                   reg_btn       IN OWA_UTIL.ident_arr,
                   regs_row      NUMBER,
                   add_row       NUMBER,
                   wait_row      NUMBER,
                   error_msg     out varchar2);
  procedure pz_student_course_register(p_pidm      in number,
                                       p_term_in   in STVTERM.STVTERM_CODE%TYPE,
                                       p_crn_in    in ssbsect.ssbsect_crn%type,
                                       P_RSTS_CODE IN SFRSTCR.SFRSTCR_RSTS_CODE%type,
                                       p_error_msg out varchar2,
                                       p_rowid     out varchar2);

  FUNCTION f_reg_access_still_good(pidm_in      IN SPRIDEN.SPRIDEN_PIDM%TYPE,
                                   term_in      IN STVTERM.STVTERM_CODE%TYPE,
                                   call_path_in IN VARCHAR2,
                                   proc_name_in IN VARCHAR2) RETURN BOOLEAN;

  FUNCTION F_ValidTerm(term     IN VARCHAR2 DEFAULT NULL,
                       term_rec OUT stvterm%ROWTYPE,
                       rtrm_rec OUT sorrtrm%ROWTYPE) RETURN BOOLEAN;

  PROCEDURE p_get_min_max_by_curric_stand(p_term         IN sfrmhrs.sfrmhrs_term_code%TYPE,
                                          p_pidm         IN spriden.spriden_pidm%TYPE,
                                          p_seq_no       IN sfrmhrs.sfrmhrs_seq_no%TYPE DEFAULT NULL,
                                          p_new_cast     IN VARCHAR DEFAULT 'N',
                                          p_new_astd     IN VARCHAR DEFAULT 'N',
                                          p_astd_code    IN stvastd.stvastd_code%TYPE,
                                          p_cast_code    IN stvcast.stvcast_code%TYPE,
                                          p_min_hrs_out  OUT sfrmhrs.sfrmhrs_min_hrs%TYPE,
                                          p_max_hrs_out  OUT sfrmhrs.sfrmhrs_max_hrs%TYPE,
                                          p_min_srce_out OUT sfbetrm.sfbetrm_minh_srce_cde%TYPE,
                                          p_max_srce_out OUT sfbetrm.sfbetrm_maxh_srce_cde%TYPE);

  PROCEDURE p_get_min_max_by_curric(p_term        IN sfrmhrs.sfrmhrs_term_code%TYPE,
                                    p_pidm        IN spriden.spriden_pidm%TYPE,
                                    p_seq_no      IN sfrmhrs.sfrmhrs_seq_no%TYPE DEFAULT NULL,
                                    p_min_hrs_out OUT sfrmhrs.sfrmhrs_min_hrs%TYPE,
                                    p_max_hrs_out OUT sfrmhrs.sfrmhrs_max_hrs%TYPE);
  FUNCTION f_query_last_term(p_pidm          sgbstdn.sgbstdn_pidm%TYPE,
                             p_term_code_eff sgbstdn.sgbstdn_term_code_eff%TYPE)
    RETURN sb_learner.learner_ref;

  FUNCTION F_RegsStu(pidm_in   IN spriden.spriden_pidm%TYPE,
                     term      IN stvterm.stvterm_code%TYPE DEFAULT NULL,
                     proc_name IN VARCHAR2 DEFAULT NULL) RETURN BOOLEAN;

  PROCEDURE p_addcrse(stcr_row      sftregs%ROWTYPE,
                      subj          ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse          ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq           ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                      start_date_in VARCHAR2 DEFAULT NULL,
                      end_date_in   VARCHAR2 DEFAULT NULL);

  PROCEDURE p_regschk(stcr_row sftregs%ROWTYPE,
                      subj     ssbsect.ssbsect_subj_code%TYPE DEFAULT NULL,
                      crse     ssbsect.ssbsect_crse_numb%TYPE DEFAULT NULL,
                      seq      ssbsect.ssbsect_seq_numb%TYPE DEFAULT NULL,
                      over     CHAR DEFAULT NULL,
                      add_ind  CHAR DEFAULT NULL);

  PROCEDURE p_regschk(stdn_row      sgbstdn%ROWTYPE,
                      multi_term_in BOOLEAN DEFAULT FALSE);
  PROCEDURE p_defstcr(add_ind CHAR DEFAULT NULL);
  function f_get_ptrm_code(p_crn_in    in SSBSECT.SSBSECT_CRN%TYPE,
                           p_term_code in STVTERM.STVTERM_CODE%TYPE)
    return varchar2;
  PROCEDURE p_initvalue(pidm_in        spriden.spriden_pidm%TYPE,
                        term_in        stvterm.stvterm_code%TYPE,
                        id_in          spriden.spriden_id%TYPE,
                        regs_date_in   DATE,
                        hold_passwd_in CHAR,
                        samsys_in      CHAR);

  PROCEDURE p_insert_sfrstcr(sftregs_rec_in    IN sftregs%ROWTYPE,
                             class_sort_key_in IN sfrstcr.sfrstcr_class_sort_key%TYPE,
                             reg_seq_in        IN sfrstcr.sfrstcr_reg_seq%TYPE,
                             attend_hr_in      IN sfrstcr.sfrstcr_attend_hr%TYPE,
                             p_rowid           OUT varchar2);

  FUNCTION f_validlevl(sql_err OUT NUMBER,
                       term_in sftregs.sftregs_term_code%TYPE,
                       levl    sftregs.sftregs_levl_code%TYPE DEFAULT NULL)
    RETURN BOOLEAN;
  procedure pz_stu_regs_commit(p_crn         IN ssbsect.ssbsect_crn%type,
                               P_student_ID1 IN VARCHAR2,
                               P_TERM        IN varchar2,
                               P_RSTS_CODE   IN SFRSTCR.SFRSTCR_RSTS_CODE%type,
                               P_ERROR_MSG   OUT VARCHAR2,
                               P_ROWID_OUT   OUT VARCHAR2);
  PROCEDURE p_update_regs(pidm_in            IN sftregs.sftregs_pidm%TYPE,
                          term_in            IN sftregs.sftregs_term_code%TYPE,
                          reg_date_in        IN sftregs.sftregs_add_date%TYPE,
                          clas_code_in       IN sgrclsr.sgrclsr_clas_code%TYPE,
                          styp_code_in       IN sgbstdn.sgbstdn_styp_code%TYPE,
                          capc_severity_in   IN sobterm.sobterm_capc_severity%TYPE,
                          tmst_calc_ind_in   IN sobterm.sobterm_tmst_calc_ind%TYPE,
                          tmst_maint_ind_in  IN sfbetrm.sfbetrm_tmst_maint_ind%TYPE,
                          tmst_code_in       IN sfbetrm.sfbetrm_tmst_code%TYPE,
                          drop_last_class_in IN BOOLEAN DEFAULT TRUE,
                          system_in          IN VARCHAR2,
                          error_rec_out      OUT sftregs%ROWTYPE,
                          error_flag_out     OUT VARCHAR2,
                          tmst_flag_out      OUT VARCHAR2,
                          p_error_msg        OUT VARCHAR2);

  FUNCTION f_readmit_required(term_in            IN stvterm.stvterm_code%TYPE,
                              pidm_in            IN spriden.spriden_pidm%TYPE,
                              term_code_admit_in IN sorlcur.sorlcur_term_code_admit%TYPE,
                              readm_req_in       IN sobterm.sobterm_readm_req%TYPE,
                              multi_term_in      IN BOOLEAN DEFAULT FALSE)
    RETURN BOOLEAN;

  procedure pz_register_courses_in_cart(p_student_id in spriden.spriden_id%type,
                                        p_term_in    in stvterm.stvterm_code%type,
                                        p_response   out varchar2);

  procedure pz_do_validations(term_in                IN OWA_UTIL.ident_arr,
                              pidm_in                IN spriden.spriden_pidm%TYPE,
                              etrm_done_in_out       IN OUT BOOLEAN,
                              capp_tech_error_in_out IN OUT VARCHAR2,
                              drop_problems_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              drop_failures_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              error_msg              out varchar2);

  procedure pz_final_commit(pidm_in            IN sftregs.sftregs_pidm%TYPE,
                            term_in            IN sftregs.sftregs_term_code%TYPE,
                            reg_date_in        IN sftregs.sftregs_add_date%TYPE,
                            clas_code_in       IN sgrclsr.sgrclsr_clas_code%TYPE,
                            styp_code_in       IN sgbstdn.sgbstdn_styp_code%TYPE,
                            capc_severity_in   IN sobterm.sobterm_capc_severity%TYPE,
                            tmst_calc_ind_in   IN sobterm.sobterm_tmst_calc_ind%TYPE,
                            tmst_maint_ind_in  IN sfbetrm.sfbetrm_tmst_maint_ind%TYPE,
                            tmst_code_in       IN sfbetrm.sfbetrm_tmst_code%TYPE,
                            drop_last_class_in IN BOOLEAN DEFAULT TRUE,
                            system_in          IN VARCHAR2,
                            error_rec_out      OUT sftregs%ROWTYPE,
                            error_flag_out     OUT VARCHAR2,
                            tmst_flag_out      OUT VARCHAR2);

  procedure pz_remove_from_cart(p_student_id in varchar2,
                                p_crn        in varchar2,
                                p_term       in varchar2,
                                p_error_msg  out varchar2);

  PROCEDURE pz_add_to_cart(p_student_id in varchar2,
                           p_crn        in varchar2,
                           p_term       in varchar2,
                           p_rsts_code  in varchar2,
                           p_error_msg  out varchar2);

  procedure pz_clear_cart(p_student_id in varchar2,
                          p_term       in varchar2,
                          p_error_msg  out varchar2);
end add_course_pkg_corq;



/

