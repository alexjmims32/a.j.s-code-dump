PROMPT CREATE OR REPLACE PACKAGE mobedu.me_sfkfees
CREATE OR REPLACE PACKAGE mobedu.me_sfkfees as
  --AUDIT_TRAIL_MSGKEY_UPDATE
  -- PROJECT : MSGKEY
  -- MODULE  : SFKFEES
  -- SOURCE  : enUS
  -- TARGET  : I18N
  -- DATE    : Mon Aug 31 14:16:27 2009
  -- MSGSIGN : #9cc4268c820cc1fa
  --TMI18N.ETR DO NOT CHANGE--
  --
  -- FILE NAME..: sfkfees.sql
  -- RELEASE....: 8.3
  -- OBJECT NAME: SFKFEES
  -- PRODUCT....: STUDENT
  -- USAGE......: Package Spec for procedures and functions needed for
  --              Registration Fee Assessment processing.
  -- COPYRIGHT..: -- COPYRIGHT..: ? 2003, 2009 SunGard. All rights reserved.
  --  This  software  contains confidential and proprietary information of SunGard and its subsidiaries.
  --   Use of this  software  is limited to SunGard Higher Education licensees, and is subject to the terms
  --   and conditions of one or more written license agreements between SunGard Higher Education and the
  --   licensee in question.
  --
  -- DESCRIPTION:
  --
  --   This package spec is for the supporting procedures and functions
  --   needed for Fee Assessment proecssing. These procedures and functions
  --   are called from all places that perform fee assessment across Banner.
  --
  --   FUNCTIONS:
  --      f_calc_per_cred_charge
  --      f_calc_rule_hrs
  --      f_DD_since_last_assessment
  --      f_establishoriginalcharge
  --      f_ests_means_wd
  --      f_ests_wd_assessed
  --      f_flat_rule_exists
  --      f_get_accd_eff_date
  --      f_get_DD_handled_date
  --      f_get_prev_nondrop_hrs
  --      f_get_sfrfaud_seqno
  --      f_get_sfrstcr_date
  --      f_get_tbbdetc_dcat
  --      f_prev_flat_rule_met
  --      f_rsts_include_in_assess
  --      f_rsts_means_wd
  --      f_rule_type_defined
  --      f_sfrfmax_defined
  --      f_tbraccd_exists
  --      f_flat_rule_exists
  --      f_last_dd_activity_date
  --      f_var_credit_intermediate
  --
  --   PROCEDURES:
  --      p_additionalfees
  --      p_applyrules
  --      p_attributefees
  --      p_calc_flat_hr_liability
  --      p_calc_reg_hr_liability
  --      p_calc_rfst_liability
  --      p_calc_swap_liability
  --      p_courses_by_rfcr
  --      p_create_nondrop_hrs_sfrfaud
  --      p_create_nondrop_type
  --      p_delete_mock_assessment
  --      p_delete_sfrbtch
  --      p_extensionfees
  --      p_get_last_assess_flat_date
  --      p_get_sfrstcr_data
  --      p_init_global_vars
  --      p_insert_sfrfaud
  --      p_insert_tbraccd_rec
  --      p_processcourse
  --      p_process_etrm_drop
  --      p_processfeeassessment
  --      p_process_hours_swap
  --      p_rbt_refunds
  --      p_reinit_temp_vars
  --      p_rev_nonswap_RBT
  --      p_reverse_na_charges
  --      p_sectionfees
  --      p_set_rbt_variables
  --      p_set_rfst_variables
  --      p_studentcourses
  --      p_calcliability
  --      p_totalfees
  --      p_get_ndrop_hrs
  --      p_print_dbms
  --     p_multiple_rules
  --
  -- DESCRIPTION END
  --

  --
  -- GLOBAL variables
  --
  -- records referred to throughout processing
  sobterm_rec sobterm%ROWTYPE;
  sfbetrm_rec sfbetrm%ROWTYPE;
  sgbstdn_rec sgbstdn%ROWTYPE;

  -- global vars that will be populated one time as constants
  source_pgm         VARCHAR2(30) := '';
  rule_entry_type    sfrrgfe.sfrrgfe_entry_type%TYPE := '';
  create_accd_ind    VARCHAR2(1) := '';
  orig_chg_ind       tbraccd.tbraccd_orig_chg_ind%TYPE;
  save_activity_date DATE; -- static date/time for SFRFAUD records
  save_sessionid     VARCHAR2(30) := '';
  clas_code          VARCHAR2(2);
  assess_eff_date    DATE;
  rbt_rfnd_date      DATE; -- date to use for penalty lookup
  rbt_rfnd_eff_date  DATE; -- date to use for TBRACCD_EFF_DATE
  eff_date_ind       tbbctrl.tbbctrl_effective_date_ind%TYPE := '';

  -- global var indicating that assessment postings occurred;
  -- used in conditional updating of sfbetrm_assessment_date
  ASSESSMENT_POSTED BOOLEAN := FALSE;

  -- global vars for refund by enrollment status
  RFST_RULE_DEFINED         BOOLEAN := FALSE;
  RFST_RFND_APPLIED         BOOLEAN := FALSE;
  ESTS_WD_PROCESSED         BOOLEAN := FALSE;
  rfst_tuit_liab_percentage NUMBER(5, 2) := 0;
  rfst_fees_liab_percentage NUMBER(5, 2) := 0;

  -- global vars for refund by total processing
  RBT_RULE_DEFINED         BOOLEAN := FALSE;
  RBT_RFND_DATE_SPECIFIED  BOOLEAN := FALSE;
  rbt_srce_code            sfrrfnd.sfrrfnd_srce_code%TYPE := '';
  rbt_clr_tuit_detl_code   sfrrfnd.sfrrfnd_tui_code%TYPE := '';
  rbt_clr_fees_detl_code   sfrrfnd.sfrrfnd_fee_code%TYPE := '';
  rbt_tuit_penalty_percent NUMBER(5, 2) := 0.00;
  rbt_fees_penalty_percent NUMBER(5, 2) := 0.00;
  SWAP                     BOOLEAN := FALSE;
  swap_assess_rfnd_cde     sfrfaud.sfrfaud_assess_rfnd_penlty_cde%TYPE := '';
  --104070
  SWAP_RBC_MORE_DROPPED  BOOLEAN := FALSE;
  SWAP_RBC_EQUAL         BOOLEAN := FALSE;
  SWAP_RBC_MORE_ADDS     BOOLEAN := FALSE;
  tui_below_flat_pcent   sftfees.sftfees_tuit_liab_percentage%TYPE := '';
  fee_below_flat_pcent   sftfees.sftfees_fees_liab_percentage%TYPE := '';
  rev_penalty_hrs_tui    sftfees.sftfees_reg_bill_hr%TYPE := '';
  rev_penalty_hrs_fee    sftfees.sftfees_reg_bill_hr%TYPE := '';
  rev_dropped_hrs        sftfees.sftfees_reg_bill_hr%TYPE := '';
  recalc_penalty_hrs_tui sftfees.sftfees_reg_bill_hr%TYPE := '';
  recalc_penalty_hrs_fee sftfees.sftfees_reg_bill_hr%TYPE := '';
  rev_tuit               sftfees.sftfees_tuit_liab_percentage%TYPE := 0;
  rev_fees               sftfees.sftfees_fees_liab_percentage%TYPE := 0;
  rev_lv_dhrs            sftfees.sftfees_reg_bill_hr%TYPE := '';
  -- 7.4.0.3 add new hours for swapping
  swap_date         date := null;
  first_swap_date   date := null;
  start_swap_hours  sfrfaud.sfrfaud_reg_bill_hr%type := 0;
  before_swap_hours sfrfaud.sfrfaud_reg_bill_hr%type := 0;
  --104667
  REVERSE_ZEROTONINE BOOLEAN := FALSE;
  PENALTY_REV_DONE   BOOLEAN := FALSE;

  --  8.1.1 max flat to hour
  max_flat_to_hours sfrrgfe.sfrrgfe_to_flat_hrs%type;

  -- common RBT vars for single insert statements in processing:
  rbt_clr_detl_code   sfrrfnd.sfrrfnd_fee_code%TYPE := '';
  rbt_penalty_percent NUMBER(5, 2) := 0.00;

  -- global var for audit trail seqno used in several procedures
  audit_seqno NUMBER := 0;

  -- global vars for drops involving prev flat hour rule qualification
  DROPS                      BOOLEAN := FALSE;
  ANY_DROPS                  BOOLEAN := FALSE;
  PREV_FLAT_HR_RULE_MET      BOOLEAN := FALSE;
  DD_SINCE_LAST_ASSESSMENT   BOOLEAN := FALSE;
  dd_assess_rfnd_cde         VARCHAR2(1) := '';
  LAST_ASSESSMENT_HANDLED_DD BOOLEAN := FALSE;
  intermediate_date          DATE;
  last_assessment_date       DATE;
  -- 1-2O637G 7.4.0.1
  prev_assessment_date  DATE := null;
  last_dd_activity_date DATE := null;
  new_nondrop_hrs       NUMBER := null;
  new_nonwaiv_hrs       NUMBER := null;
  --- 105831  7.4.0.2
  variable_act_date DATE := null;
  -- I18N
  c_ESTS_liability VARCHAR2(40) := g$_nls.get('SFKFEES-0000',
                                              'SQL',
                                              'ESTS liability');

  -- Defect 99507 for intermediate assessment.
  do_intermediate_ind VARCHAR2(1) := 'N'; -- Defect 99507
  DO_INTERMEDIATE     BOOLEAN := FALSE; -- Defect 99507

  -- reusable temporary variables for processing types of fees
  t_crn                  sftfees.sftfees_crn%TYPE := '';
  t_levl_code_crse       stvlevl.stvlevl_code%TYPE := '';
  t_camp_code_crse       stvcamp.stvcamp_code%TYPE := '';
  t_ptrm_code            stvptrm.stvptrm_code%TYPE := '';
  t_ptrm_rule            stvptrm.stvptrm_code%TYPE := '';
  t_add_date             sftfees.sftfees_add_date%TYPE := '';
  t_rsts_code            stvrsts.stvrsts_code%TYPE := '';
  t_rsts_date            sftfees.sftfees_rsts_date%TYPE := '';
  t_orig_chg_ind         tbraccd.tbraccd_orig_chg_ind%TYPE := '';
  t_crse_start_date      sftfees.sftfees_crse_start_date%TYPE := '';
  t_crse_end_date        sftfees.sftfees_crse_end_date%TYPE := '';
  t_reg_bill_hr          sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_bill_hr_tuit         sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_bill_hr_fees         sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_liab_bill_hr_tuit    sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_liab_bill_hr_fees    sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_reg_waiv_hr          sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_waiv_hr_tuit         sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_waiv_hr_fees         sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_liab_waiv_hr_tuit    sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_liab_waiv_hr_fees    sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_tuit_liab_percentage sftfees.sftfees_tuit_liab_percentage%TYPE := 0;
  t_fees_liab_percentage sftfees.sftfees_fees_liab_percentage%TYPE := 0;
  t_overload_hr_tuit     sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE := 0;
  t_overload_hr_fees     sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE := 0;

  -- temporary variables for 'assess by course' processing
  t_gmod_code stvgmod.stvgmod_code%TYPE := '';
  t_insm_code gtvinsm.gtvinsm_code%TYPE := '';
  t_schd_code stvschd.stvschd_code%TYPE := '';

  -- variables for overall student liable hours
  t_stud_liab_bill_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_stud_liab_bill_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_stud_liab_waiv_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
  t_stud_liab_waiv_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;

  -- variables for course values and totals for student
  crn_tuit_refund    sfrrfcr.sfrrfcr_tuit_refund%TYPE := 0;
  crn_fees_refund    sfrrfcr.sfrrfcr_fees_refund%TYPE := 0;
  crn_rfnd_from_date sfrrfcr.sfrrfcr_from_date%TYPE;
  crn_rfnd_to_date   sfrrfcr.sfrrfcr_to_date%TYPE;
  crn_rfnd_table     VARCHAR2(7) := '';

  crn_sfrstcr_crn         sfrstcr.sfrstcr_crn%TYPE := '';
  crn_sfrstcr_levl_code   stvlevl.stvlevl_code%TYPE := '';
  crn_sfrstcr_camp_code   stvcamp.stvcamp_code%TYPE := '';
  crn_sfrstcr_ptrm_code   stvptrm.stvptrm_code%TYPE := '';
  crn_sfrstcr_rsts_code   sfrstcr.sfrstcr_rsts_code%TYPE := '';
  crn_sfrstcr_rsts_date   sfrstcr.sfrstcr_rsts_date%TYPE;
  crn_sfrstcr_credit_hr   sfrstcr.sfrstcr_credit_hr%TYPE := 0;
  crn_sfrstcr_bill_hr     sfrstcr.sfrstcr_bill_hr%TYPE := 0;
  crn_sfrstcr_waiv_hr     sfrstcr.sfrstcr_waiv_hr%TYPE := 0;
  crn_sfrstcr_gmod_code   sfrstcr.sfrstcr_gmod_code%TYPE := '';
  crn_status_withdraw_ind stvrsts.stvrsts_withdraw_ind%TYPE := '';
  crn_status_enroll_ind   stvrsts.stvrsts_incl_sect_enrl%TYPE := '';

  crn_start_date        DATE;
  crn_end_date          DATE;
  crn_ssbsect_insm_code gtvinsm.gtvinsm_code%TYPE := '';
  crn_ssbsect_schd_code stvschd.stvschd_code%TYPE := '';

  crn_tuit_liab_percentage NUMBER(5, 2) := 0;
  crn_fees_liab_percentage NUMBER(5, 2) := 0;

  liab_crn_bill_hr_tuit sfrstcr.sfrstcr_bill_hr%TYPE := 0;
  liab_crn_bill_hr_fees sfrstcr.sfrstcr_bill_hr%TYPE := 0;
  liab_crn_waiv_hr_tuit sfrstcr.sfrstcr_waiv_hr%TYPE := 0;
  liab_crn_waiv_hr_fees sfrstcr.sfrstcr_waiv_hr%TYPE := 0;

  crn_bill_hr           sfrstcr.sfrstcr_bill_hr%TYPE := 0;
  crn_bill_hr_tuit      sfrstcr.sfrstcr_bill_hr%TYPE := 0;
  crn_bill_hr_fees      sfrstcr.sfrstcr_bill_hr%TYPE := 0;
  crn_bill_waiv_hr      sfrstcr.sfrstcr_waiv_hr%TYPE := 0;
  crn_bill_waiv_hr_tuit sfrstcr.sfrstcr_waiv_hr%TYPE := 0;
  crn_bill_waiv_hr_fees sfrstcr.sfrstcr_waiv_hr%TYPE := 0;

  unable_to_insert_tbraccd_rec EXCEPTION;

  -- global variable to allow printing of debug messages
  gv_print varchar2(1) := 'Y';

  --- 7.4.0.4
  rbt_dd_exists boolean;
  rbt_wd_exists boolean;

  TYPE sfrrgfe_record is RECORD(
    SFRRGFE_TERM_CODE              SFRRGFE.SFRRGFE_TERM_CODE%type,
    SFRRGFE_DETL_CODE              SFRRGFE.SFRRGFE_DETL_CODE%type,
    SFRRGFE_ACTIVITY_DATE          SFRRGFE.SFRRGFE_ACTIVITY_DATE%type,
    SFRRGFE_PER_CRED_CHARGE        SFRRGFE.SFRRGFE_PER_CRED_CHARGE%type,
    SFRRGFE_MIN_CHARGE             SFRRGFE.SFRRGFE_MIN_CHARGE%type,
    SFRRGFE_MAX_CHARGE             SFRRGFE.SFRRGFE_MAX_CHARGE%type,
    SFRRGFE_PTRM_CODE              SFRRGFE.SFRRGFE_PTRM_CODE%type,
    SFRRGFE_RESD_CODE              SFRRGFE.SFRRGFE_RESD_CODE%type,
    SFRRGFE_LEVL_CODE              SFRRGFE.SFRRGFE_LEVL_CODE%type,
    SFRRGFE_COLL_CODE              SFRRGFE.SFRRGFE_COLL_CODE%type,
    SFRRGFE_MAJR_CODE              SFRRGFE.SFRRGFE_MAJR_CODE%type,
    SFRRGFE_CLAS_CODE              SFRRGFE.SFRRGFE_CLAS_CODE%type,
    SFRRGFE_RATE_CODE              SFRRGFE.SFRRGFE_RATE_CODE%type,
    SFRRGFE_CRSE_WAIV_IND          SFRRGFE.SFRRGFE_CRSE_WAIV_IND%type,
    SFRRGFE_FROM_CRED_HRS          SFRRGFE.SFRRGFE_FROM_CRED_HRS%type,
    SFRRGFE_TO_CRED_HRS            SFRRGFE.SFRRGFE_TO_CRED_HRS%type,
    SFRRGFE_FROM_ADD_DATE          SFRRGFE.SFRRGFE_FROM_ADD_DATE%type,
    SFRRGFE_TO_ADD_DATE            SFRRGFE.SFRRGFE_TO_ADD_DATE%type,
    SFRRGFE_TYPE                   SFRRGFE.SFRRGFE_TYPE%type,
    SFRRGFE_CAMP_CODE              SFRRGFE.SFRRGFE_CAMP_CODE%type,
    SFRRGFE_LEVL_CODE_CRSE         SFRRGFE.SFRRGFE_LEVL_CODE_CRSE%type,
    SFRRGFE_CAMP_CODE_CRSE         SFRRGFE.SFRRGFE_CAMP_CODE_CRSE%type,
    SFRRGFE_ENTRY_TYPE             SFRRGFE.SFRRGFE_ENTRY_TYPE%type,
    SFRRGFE_FROM_STUD_HRS          SFRRGFE.SFRRGFE_FROM_STUD_HRS%type,
    SFRRGFE_TO_STUD_HRS            SFRRGFE.SFRRGFE_TO_STUD_HRS%type,
    SFRRGFE_CRED_IND               SFRRGFE.SFRRGFE_CRED_IND%type,
    SFRRGFE_FLAT_HRS               SFRRGFE.SFRRGFE_FLAT_HRS%type,
    SFRRGFE_COPY_IND               SFRRGFE.SFRRGFE_COPY_IND%type,
    SFRRGFE_SEQNO                  SFRRGFE.SFRRGFE_SEQNO%type,
    SFRRGFE_PROGRAM                SFRRGFE.SFRRGFE_PROGRAM%type,
    SFRRGFE_DEPT_CODE              SFRRGFE.SFRRGFE_DEPT_CODE%type,
    SFRRGFE_DEGC_CODE              SFRRGFE.SFRRGFE_DEGC_CODE%type,
    SFRRGFE_STYP_CODE              SFRRGFE.SFRRGFE_STYP_CODE%type,
    SFRRGFE_TERM_CODE_ADMIT        SFRRGFE.SFRRGFE_TERM_CODE_ADMIT%type,
    SFRRGFE_ATTR_CODE_CRSE         SFRRGFE.SFRRGFE_ATTR_CODE_CRSE%type,
    SFRRGFE_ATTS_CODE              SFRRGFE.SFRRGFE_ATTS_CODE%type,
    SFRRGFE_GMOD_CODE              SFRRGFE.SFRRGFE_GMOD_CODE%type,
    SFRRGFE_ASSESS_BY_COURSE_IND   SFRRGFE.SFRRGFE_ASSESS_BY_COURSE_IND%type,
    SFRRGFE_USER_ID                SFRRGFE.SFRRGFE_USER_ID%type,
    SFRRGFE_FROM_FLAT_HRS          SFRRGFE.SFRRGFE_FROM_FLAT_HRS%type,
    SFRRGFE_TO_FLAT_HRS            SFRRGFE.SFRRGFE_TO_FLAT_HRS%type,
    SFRRGFE_FLAT_FEE_AMOUNT        SFRRGFE.SFRRGFE_FLAT_FEE_AMOUNT%type,
    SFRRGFE_CRSE_OVERLOAD_START_HR SFRRGFE.SFRRGFE_CRSE_OVERLOAD_START_HR%type,
    SFRRGFE_INSM_CODE              SFRRGFE.SFRRGFE_INSM_CODE%type,
    SFRRGFE_SCHD_CODE              SFRRGFE.SFRRGFE_SCHD_CODE%type,
    SFRRGFE_LFST_CODE              SFRRGFE.SFRRGFE_LFST_CODE%type,
    SFRRGFE_PRIM_SEC_CDE           SFRRGFE.SFRRGFE_PRIM_SEC_CDE%type,
    SFRRGFE_RATE_CODE_CURRIC       SFRRGFE.SFRRGFE_RATE_CODE_CURRIC%type,
    SFRRGFE_STYP_CODE_CURRIC       SFRRGFE.SFRRGFE_STYP_CODE_CURRIC%type,
    SFRRGFE_CHRT_CODE              SFRRGFE.SFRRGFE_CHRT_CODE%type,
    SFRRGFE_VTYP_CODE              SFRRGFE.SFRRGFE_VTYP_CODE%type,
    SFRRGFE_LFST_PRIM_SEC_CDE      SFRRGFE.SFRRGFE_LFST_PRIM_SEC_CDE%TYPE);
  ---  record type and ref cursor for sfkfees
  TYPE sftfees_rec IS RECORD(
    r_pidm                 sftfees.SFTFEES_PIDM%type,
    r_TERM_CDE             sftfees.SFTFEES_TERM_CDE%type,
    r_CRN                  sftfees.SFTFEES_CRN%type,
    r_LEVL_CDE_CRSE        sftfees.SFTFEES_LEVL_CDE_CRSE%type,
    r_CAMP_CDE_CRSE        sftfees.SFTFEES_CAMP_CDE_CRSE%type,
    r_PTRM_CDE             sftfees.SFTFEES_PTRM_CDE%type,
    r_ADD_DATE             sftfees.SFTFEES_ADD_DATE%type,
    r_RSTS_CDE             sftfees.SFTFEES_RSTS_CDE%type,
    r_RSTS_DATE            sftfees.SFTFEES_RSTS_DATE%type,
    r_ESTS_CDE             sftfees.SFTFEES_ESTS_CDE%type,
    r_ESTS_DATE            sftfees.SFTFEES_ESTS_DATE%type,
    r_REFUND_SOURCE_TABLE  sftfees.SFTFEES_REFUND_SOURCE_TABLE%type,
    r_GMOD_CDE             sftfees.SFTFEES_GMOD_CDE%type,
    r_CRSE_START_DATE      sftfees.SFTFEES_CRSE_START_DATE%type,
    r_CRSE_END_DATE        sftfees.SFTFEES_CRSE_END_DATE%type,
    r_REG_BILL_HR          sftfees.SFTFEES_REG_BILL_HR%type,
    r_BILL_HR_TUIT         sftfees.SFTFEES_BILL_HR_TUIT%type,
    r_BILL_HR_FEES         sftfees.SFTFEES_BILL_HR_FEES%type,
    r_REG_WAIV_HR          sftfees.SFTFEES_REG_WAIV_HR%type,
    r_WAIV_HR_TUIT         sftfees.SFTFEES_WAIV_HR_TUIT%type,
    r_WAIV_HR_FEES         sftfees.SFTFEES_WAIV_HR_FEES%type,
    r_TUIT_LIAB_PERCENTAGE sftfees.SFTFEES_TUIT_LIAB_PERCENTAGE%type,
    r_FEES_LIAB_PERCENTAGE sftfees.SFTFEES_FEES_LIAB_PERCENTAGE%type,
    r_INSM_CDE             sftfees.SFTFEES_INSM_CDE%type,
    R_SCHD_CDE             sftfees.sftfees_schd_cde%TYPE);

  type sftfees_type_rec is RECORD(
    sftfees_rec_type varchar2(30));

  TYPE sftfees_tab IS TABLE OF sftfees_rec INDEX BY BINARY_INTEGER;
  TYPE sftfees_ref IS REF CURSOR RETURN sftfees_rec;
  TYPE sftfees_type_ref IS REF CURSOR RETURN sftfees_type_rec;

  STUDENT_TYPE CONSTANT sfrrgfe.sfrrgfe_type%TYPE := 'STUDENT';
  ATTR_TYPE    CONSTANT sfrrgfe.sfrrgfe_type%TYPE := 'ATTR';
  LEVEL_TYPE   CONSTANT sfrrgfe.sfrrgfe_type%TYPE := 'LEVEL';
  CAMPUS_TYPE  CONSTANT sfrrgfe.sfrrgfe_type%TYPE := 'CAMPUS';

  --- 7.5 variable to keep track of when the N type record is created for each rule type
  N_FAUD_CREATED BOOLEAN := FALSE;

  -- sfrrgfe record for collection table

  TYPE rgfe_tab IS TABLE OF sfrrgfe%rowtype INDEX BY BINARY_INTEGER;

  type flat_hour_ranges_rec is RECORD(
    r_flat_from_hrs sfrrgfe.sfrrgfe_from_flat_hrs%type,
    r_flat_to_hrs   sfrrgfe.sfrrgfe_to_flat_hrs%type);

  TYPE flat_tab is TABLE OF flat_hour_ranges_rec INDEX BY BINARY_INTEGER;

  flat_ranges flat_tab;

  --- new globals to minimize some of the lookup for existence of rules
  save_term              stvterm.stvterm_code%type := null;
  save_clas_code         stvclas.stvclas_code%type := null;
  save_primary_levl_code stvlevl.stvlevl_code%type := null;

  gv_rgfe_student_read varchar2(1) := null;
  gv_rgfe_level_read   varchar2(1) := null;
  gv_rgfe_attr_read    varchar2(1) := null;
  gv_rgfe_campus_read  varchar2(1) := null;
  -- keep track if rules exists for the rule tpe
  gv_rgfe_student_exists varchar2(1) := null;
  gv_rgfe_level_exists   varchar2(1) := null;
  gv_rgfe_attr_exists    varchar2(1) := null;
  gv_rgfe_campus_exists  varchar2(1) := null;
  --- keep track if rules exists with curriculum
  gv_rgfe_student_cc_exists varchar2(1) := null;
  gv_rgfe_level_cc_exists   varchar2(1) := null;
  gv_rgfe_attr_cc_exists    varchar2(1) := null;
  gv_rgfe_campus_cc_exists  varchar2(1) := null;
  --- keep track if rules exists for crses
  gv_rgfe_student_course_exists varchar2(1) := null;
  gv_rgfe_level_course_exists   varchar2(1) := null;
  gv_rgfe_attr_course_exists    varchar2(1) := null;
  gv_rgfe_campus_course_exists  varchar2(1) := null;

  --
  -- FUNCTIONS
  --

  /* ****************************************************************** */
  /* Function to determine if DD occurred since the last assessment.    */
  /* ****************************************************************** */
  FUNCTION f_DD_since_last_assessment(pidm_in IN spriden.spriden_pidm%TYPE,
                                      term_in IN stvterm.stvterm_code%TYPE)
    RETURN BOOLEAN;

  /* ****************************************************************** */
  /* Function to determine assessment date when DD was handled.         */
  /* ****************************************************************** */
  FUNCTION f_get_DD_handled_date(pidm_in IN spriden.spriden_pidm%TYPE,
                                 term_in IN stvterm.stvterm_code%TYPE)
    RETURN DATE;

  /* ****************************************************************** */
  /* Function to retreive the activity date on the most recent DD          */
  /* ****************************************************************** */
  FUNCTION f_last_dd_activity_date(pidm_in IN spriden.spriden_pidm%TYPE,
                                   term_in IN stvterm.stvterm_code%TYPE)
    RETURN DATE;

  /* ****************************************************************** */
  /* 7.4.0.2 Function to find if there was a flat, drop and change in credit hours for intermediate         */
  /* ****************************************************************** */

  FUNCTION f_var_credit_intermediate(pidm_in IN spriden.spriden_pidm%TYPE,
                                     term_in IN stvterm.stvterm_code%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Function to determine the setting of the TBRACCD_ORIG_CHG_IND.      */
  /* This code was copied from package SFKFCAL and is used by Location   */
  /* Management Assessment in SLRFASM.pc. Moving the function code here  */
  /* allowed the obsoletion of package SFKFCAL.                          */
  /* ******************************************************************* */
  FUNCTION f_establishoriginalcharge(pidm_in        IN spriden.spriden_pidm%TYPE,
                                     term_code_in   IN stvterm.stvterm_code%TYPE,
                                     source_code_in IN VARCHAR2)
    RETURN VARCHAR2;

  /* ******************************************************************* */
  /* Function to determine the next seqno for the SFRFAUD record.        */
  /* ******************************************************************* */
  FUNCTION f_get_sfrfaud_seqno(pidm_in      IN spriden.spriden_pidm%TYPE,
                               term_in      IN stvterm.stvterm_code%TYPE,
                               sessionid_in IN sfrfaud.sfrfaud_sessionid%TYPE)
    RETURN NUMBER;

  /* ******************************************************************* */
  /* Function to determine if assessment accounting records exist for a  */
  /* detail code and term for the student. Function is called prior to   */
  /* performing a reversal to ensure that accounting actually exists.    */
  /* ******************************************************************* */
  FUNCTION f_tbraccd_exists(pidm_in      IN spriden.spriden_pidm%TYPE,
                            term_in      IN stvterm.stvterm_code%TYPE,
                            detl_code_in IN tbbdetc.tbbdetc_detail_code%TYPE,
                            amount_in    IN tbraccd.tbraccd_amount%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Function to determine total per credit charge a student is liable   */
  /* for using the liable hours if the DCAT (detail code category) is    */
  /* either TUI or FEE and based on whether a course override is         */
  /* specified in the assessment rule. For all other DCAT codes, use the */
  /* determined bill or waived hours                                     */
  /*                                                                     */
  /* If crse_waiv_ind is set to 'Y', then the charge can be overridden   */
  /* by a course fee override; use waived hours.                         */
  /* ******************************************************************* */
  FUNCTION f_calc_per_cred_charge(max_charge        IN sfrrgfe.sfrrgfe_max_charge%TYPE,
                                  min_charge        IN sfrrgfe.sfrrgfe_min_charge%TYPE,
                                  dcat_code         IN ttvdcat.ttvdcat_code%TYPE,
                                  crse_waiv_ind     IN sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                  bill_hr           IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_bill_hr_tuit IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_bill_hr_fees IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  waiv_hr           IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_waiv_hr_tuit IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_waiv_hr_fees IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  per_cred_charge   IN sfrrgfe.sfrrgfe_per_cred_charge%TYPE)
    RETURN NUMBER;

  /* ******************************************************************* */
  /* Function to determine if bill or waive hours are used for the rule. */
  /* ******************************************************************* */
  /* For detail codes having a DCAT code of TUI or FEE:                  */
  /*   If the course waive indicator is set to Y, use the waive hours.   */
  /*   If the course waive indicator is set to N, use the bill hours.    */
  /*                                                                     */
  /* For all other detail codes:                                         */
  /*   If the course waive indicator is set to Y, use the waive hours    */
  /*   from the course registration record (not adjusted for refunds)    */
  /*                                                                     */
  /*   If the course waive indicator is set to N, use the bill hours     */
  /*   from the course registration record (not adjusted for refunds)    */
  /* ******************************************************************* */
  FUNCTION f_calc_rule_hrs(dcat_code         IN ttvdcat.ttvdcat_code%TYPE,
                           crse_waiv_ind     IN sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                           reg_bill_hr       IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                           liab_bill_hr_tuit IN NUMBER, -- sfrstcr_bill_hr * liable tui %
                           liab_bill_hr_fees IN NUMBER, -- sfrstcr_bill_hr * liable fee %
                           reg_waiv_hr       IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                           liab_waiv_hr_tuit IN NUMBER, -- sfrstcr_waiv_hr * liable tui %
                           liab_waiv_hr_fees IN NUMBER) -- sfrstcr_waiv_hr * liable fee %
   RETURN NUMBER;

  /* ******************************************************************* */
  /* Determine the effective date to use in posting assessment charges.  */
  /* ******************************************************************* */
  /* Regardless of the value of the accept charges flag (sfbetrm_ar_ind) */
  /* if the tbbctrl_effective_date_ind = 'C' (date of charges), then use */
  /* sobterm_fee_assessmnt_eff_date if it's in the future; if SYSDATE is */
  /* greater than sobterm_fee_assessmnt_eff_date, then use SYSDATE since */
  /* sobterm_fee_assessmnt_eff_date is only to indicate a future date.   */
  /*                                                                     */
  /* If the tbbctrl_effective_date_ind = 'T' (use today's date) and the  */
  /* sfbetrm_ar_ind = 'Y', then use SYSDATE so the data feeds.           */
  /*                                                                     */
  /* If the tbbctrl_effective_date_ind = 'T' (use today's date) and the  */
  /* sfbetrm_ar_ind = 'N', then use sobterm_fee_assessmnt_eff_date if it */
  /* is in the future; if SYSDATE is greater than the sobterm date, then */
  /* use SYSDATE since sobterm_fee_assessmnt_eff_date is only to         */
  /* indicate a future date.                                             */
  /* ******************************************************************* */
  FUNCTION f_get_accd_eff_date(fa_eff_date_in IN DATE) RETURN DATE;

  /* ******************************************************************* */
  /* Determine the total registration hours from the last assessment     */
  /* by selecting the SFRFAUD record with an assess code of 'N' for      */
  /* non-dropped hours assessment. The audit record is created as part   */
  /* of assessment processing.                                           */
  /*                                                                     */
  /* 7.1: If the last assessment handled any DD/'not count in assess'    */
  /* registration activity, the starting hours to retrieve are the total */
  /* hours that the intermediate assessment arrived at.                  */
  /* 7.3:  Defect 99226                                                  */
  /* 8.0.1 add rule type and rule value in arguements */
  /* ******************************************************************* */
  FUNCTION f_get_prev_nondrop_hrs(pidm_in       IN spriden.spriden_pidm%TYPE,
                                  term_in       IN stvterm.stvterm_code%TYPE,
                                  crse_waiv_in  IN sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                  type_in       IN sfrrgfe.sfrrgfe_type%TYPE default null,
                                  rule_value_in IN VARCHAR2 default null)
    RETURN NUMBER;

  /* ******************************************************************* */
  /* Function to return SFRSTCR_ACTIVITY_DATE.                           */
  /* ******************************************************************* */
  FUNCTION f_get_sfrstcr_date(pidm_in IN spriden.spriden_pidm%TYPE,
                              term_in IN stvterm.stvterm_code%TYPE,
                              crn_in  IN sfrstcr.sfrstcr_crn%TYPE)
    RETURN DATE;

  /* ******************************************************************* */
  /* Determine if rules defined for term and rule type.                  */
  /* ******************************************************************* */
  FUNCTION f_rule_type_defined(term_in      IN stvterm.stvterm_code%TYPE,
                               rule_type_in IN sfrrgfe.sfrrgfe_type%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine if enrollment status code indicates withdrawl.            */
  /* ******************************************************************* */
  FUNCTION f_ests_means_wd(ests_code_in IN stvests.stvests_code%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine if registration status code indicates withdrawl.          */
  /* ******************************************************************* */
  FUNCTION f_rsts_means_wd(rsts_code_in IN stvrsts.stvrsts_code%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine if enrollment status withdrawl already assessed so as     */
  /* to prevent reassessing. Assessing for refunding by enrollment       */
  /* status is performed using total charges under accounting and not by */
  /* liable bill hours.                                                  */
  /* ******************************************************************* */
  FUNCTION f_ests_wd_assessed(pidm_in                   IN spriden.spriden_pidm%TYPE,
                              term_in                   IN stvterm.stvterm_code%TYPE,
                              changed_ests_date_ind_out OUT VARCHAR2)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine if SFRFMAX rules defiend for detail code/term.            */
  /* ******************************************************************* */
  /* SFRFMAX is Registration Fees Maximum Table. The min/max are for the */
  /* detail code for the term. The rule is checked for each generated    */
  /* amount prior to posting to accounting.                              */
  /* ******************************************************************* */
  FUNCTION f_sfrfmax_defined(term_in      IN stvterm.stvterm_code%TYPE,
                             detl_code_in IN tbbdetc.tbbdetc_detail_code%TYPE,
                             min_charge   OUT sfrfmax.sfrfmax_min_charge%TYPE,
                             max_charge   OUT sfrfmax.sfrfmax_max_charge%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine if RSTS code is include in assessment.                    */
  /* ******************************************************************* */
  FUNCTION f_rsts_include_in_assess(rsts_code_in IN stvrsts.stvrsts_code%TYPE)
    RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine the most recent flat hour qualification information for   */
  /* the student for the rule type from the audit data. Flat hour        */
  /* refunding needs to know the starting point for the flat hour        */
  /* qualification.                                                      */
  /* ******************************************************************* */
  FUNCTION f_prev_flat_rule_met(pidm_in       IN spriden.spriden_pidm%TYPE,
                                term_in       IN stvterm.stvterm_code%TYPE,
                                rule_type_in  IN VARCHAR2,
                                rule_value_in IN VARCHAR2) RETURN BOOLEAN;

  /* ******************************************************************* */
  /* Determine if any flat charge rules are defined for a term.          */
  /* For use in registration when inserting rows into SFRREGD, which     */
  /* only needs to occur if flat charge rules are defined for the term.  */
  /* ******************************************************************* */
  FUNCTION f_flat_rule_exists(term_in IN stvterm.stvterm_code%TYPE)
    RETURN BOOLEAN;

  --
  -- PROCEDURES
  --
  /* ******************************************************************* */
  /*  Ref cursor for non student type  fees used in p_nonstudent fees                                           */
  /* ******************************************************************* */
  function f_course_types(term_in IN stvterm.stvterm_code%TYPE,
                          pidm_in IN spriden.spriden_pidm%TYPE,
                          type_in IN sfrrgfe.sfrrgfe_type%TYPE)
    return sftfees_type_ref;

  function f_course_records(term_in       IN stvterm.stvterm_code%TYPE,
                            pidm_in       IN spriden.spriden_pidm%TYPE,
                            type_in       IN sfrrgfe.sfrrgfe_type%TYPE,
                            type_value_in IN varchar2) return sftfees_ref;

  function f_crn_records(term_in       IN stvterm.stvterm_code%TYPE,
                         pidm_in       IN spriden.spriden_pidm%TYPE,
                         type_in       IN sfrrgfe.sfrrgfe_type%TYPE,
                         type_value_in IN varchar2) return sftfees_ref;

  /* ******************************************************************* */
  /*  Main procedure for Fee Assessment                                  */
  /* ******************************************************************* */
  /*  If either the SOBTERM, SFBETRM or SGBSTDN records are not found,   */
  /*  set status code and return. If update occurred in assessment,      */
  /*  set the return status for conditional printing by the caller.      */
  /*  Values:                                                            */
  /*   1 = no SOBETRM record                                             */
  /*   2 = no SFBETRM record                                             */
  /*   3 = no SGBSTDN record                                             */
  /*   4 = assessment posted/account was updated                         */
  /*   5 = no SFRAREG record for open learning section                   */
  /*                                                                     */
  /*  Return the value of the saved date and time as a string for use    */
  /*  in reporting by the caller.                                        */
  /* ******************************************************************* */
  PROCEDURE p_processfeeassessment(term_in               IN stvterm.stvterm_code%TYPE,
                                   pidm_in               IN spriden.spriden_pidm%TYPE,
                                   fa_eff_date_in        IN DATE,
                                   rbt_rfnd_date_in      IN DATE,
                                   rule_entry_type_in    IN sfrrgfe.sfrrgfe_entry_type%TYPE,
                                   create_accd_ind_in    IN VARCHAR2,
                                   source_pgm_in         IN VARCHAR2,
                                   commit_ind_in         IN VARCHAR2,
                                   save_act_date_out     OUT VARCHAR2,
                                   ignore_sfrfmax_ind_in IN VARCHAR2,
                                   return_status_in_out  IN OUT NUMBER);

  /* Supporting procedures for consolidated fee assessment */

  /* ******************************************************************* */
  /* Procedure to get SFRSTCR_ASSESS_ACTIVITY_DATE for use in processing */
  /* a course in flat charge refunding.                                  */
  /* ******************************************************************* */
  PROCEDURE p_get_sfrstcr_data(pidm_in        IN spriden.spriden_pidm%TYPE,
                               term_in        IN stvterm.stvterm_code%TYPE,
                               act_date       IN OUT DATE,
                               add_date       OUT DATE,
                               first_act_date OUT DATE,
                               orig_bill_hrs  OUT sfrstcr.sfrstcr_bill_hr%type);

  /* ******************************************************************* */
  /* Procedure to get SUM of all bill hours for TUIT and FEES            */
  /* Defect 102188                                                       */
  /* ******************************************************************* */
  PROCEDURE p_get_ndrop_hrs(pidm_in                   IN spriden.spriden_pidm%TYPE,
                            term_in                   IN stvterm.stvterm_code%TYPE,
                            sum_ndrop_bill_hr_tuit_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                            sum_ndrop_bill_hr_fees_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                            sum_ndrop_waiv_hr_tuit_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                            sum_ndrop_waiv_hr_fees_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE);

  /* ******************************************************************* */
  /* Procedure to determine the SFRRFND refund data for refund by total  */
  /* processing. Since it's assummed that the site will be current with  */
  /* assessment, all dropped courses during this assessment should       */
  /* qualify for the same refund period defined in SFRRFND. Use the date */
  /* specified by the caller for the refund by total refund date to      */
  /* determine the refund. If found, set the global refund by total      */
  /* variables for use in the assessment refund/penalty processing.      */
  /* ******************************************************************* */
  PROCEDURE p_set_rbt_variables(term_in IN stvterm.stvterm_code%TYPE);

  /* ****************************************************************** */
  /* Set the global variables for refunds by enrollment status.         */
  /* ****************************************************************** */
  PROCEDURE p_set_rfst_variables(term_in      IN stvterm.stvterm_code%TYPE,
                                 ests_code_in IN stvests.stvests_code%TYPE,
                                 ests_date_in IN DATE);

  /* ******************************************************************* */
  /* Determine and save the total registration hours in the assessment   */
  /* that do not involve drops. This data is need for refunds involving  */
  /* previously met flat hour rules.                                     */
  /* ******************************************************************* */
  PROCEDURE p_create_nondrop_hrs_sfrfaud(pidm_in       IN spriden.spriden_pidm%TYPE,
                                         term_in       IN stvterm.stvterm_code%TYPE,
                                         source_pgm_in IN sfrfaud.sfrfaud_assessment_source%TYPE);

  PROCEDURE p_create_nondrop_type(pidm_in       IN spriden.spriden_pidm%TYPE,
                                  term_in       IN stvterm.stvterm_code%TYPE,
                                  source_pgm_in IN varchar2,
                                  type_in       IN sfrrgfe.sfrrgfe_type%TYPE,
                                  rule_value_in IN varchar2);
  /* ******************************************************************* */
  /* RPE 35999                                               03/2005 BAG */
  /* ******************************************************************* */
  /* Need:   Show rule values used in charge calcuation in form SFAFAUD. */
  /* Change: Store rule values utilized in charge calc in SFRFAUD table. */
  /* Added new columns: SFRFAUD_RGFE_PER_CRED_CHARGE  NUMBER(12,2)       */
  /*                    SFRFAUD_RGFE_FLAT_FEE_AMOUNT  NUMBER(12,2)       */
  /*                    SFRFAUD_RGFE_FROM_FLAT_HRS    NUMBER(9,3)        */
  /*                    SFRFAUD_RGFE_TO_FLAT_HRS      NUMBER(9,3)        */
  /*                    SFRFAUD_RGFE_CRSE_OL_START_HR NUMBER(9,3)        */
  /* Add new input parms to procedure:                                   */
  /*   rule_per_cred_charge_in    rule_flat_fee_amount_in                */
  /*   rule_from_flat_hrs_in      rule_to_flat_hrs_in                    */
  /*   rule_crse_ol_start_hr_in                                          */
  /* ******************************************************************* */
  /* Insert the SFRFAUD audit record.                                    */
  /* ******************************************************************* */
  PROCEDURE p_insert_sfrfaud(pidm_in                   IN spriden.spriden_pidm%TYPE,
                             term_in                   IN stvterm.stvterm_code%TYPE,
                             sfrfaud_seqno_in          IN sfrfaud.sfrfaud_seqno%TYPE,
                             assess_rfnd_penlty_cde_in IN sfrfaud.sfrfaud_assess_rfnd_penlty_cde%TYPE,
                             dcat_code_in              IN ttvdcat.ttvdcat_code%TYPE,
                             detl_code_in              IN sfrfaud.sfrfaud_detl_code%TYPE,
                             charge_in                 IN sfrfaud.sfrfaud_charge%TYPE,
                             assess_by_crse_ind_in     IN sfrfaud.sfrfaud_assess_by_course_ind%TYPE,
                             source_pgm_in             IN sfrfaud.sfrfaud_assessment_source%TYPE,
                             sfrrgfe_seqno_in          IN sfrfaud.sfrfaud_rgfe_seqno%TYPE,
                             sfrrgfe_type_in           IN sfrfaud.sfrfaud_rgfe_type%TYPE,
                             crn_in                    IN sfrfaud.sfrfaud_crn%TYPE,
                             rsts_code_in              IN stvrsts.stvrsts_code%TYPE,
                             rsts_date_in              IN DATE,
                             ests_code_in              IN stvests.stvests_code%TYPE,
                             ests_date_in              IN DATE,
                             rfnd_table_in             IN sfrfaud.sfrfaud_refund_source_table%TYPE,
                             reg_bill_hr_in            IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                             reg_waiv_hr_in            IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                             rule_cred_hr_in           IN sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                             rule_stud_hr_in           IN sfrfaud.sfrfaud_rule_liable_stud_hrs%TYPE,
                             per_cred_charge_in        IN sfrfaud.sfrfaud_tot_per_cred_charge%TYPE,
                             flat_fee_amt_in           IN sfrfaud.sfrfaud_flat_fee_amount%TYPE,
                             overload_hrs_in           IN sfrfaud.sfrfaud_crse_overload_hrs%TYPE,
                             overload_charge_in        IN sfrfaud.sfrfaud_crse_overload_charge%TYPE,
                             tran_number_in            IN tbraccd.tbraccd_tran_number%TYPE,
                             note_in                   IN sfrfaud.sfrfaud_note%TYPE,
                             liable_cred_hr_in         IN sfrfaud.sfrfaud_liable_cred_hrs%TYPE,
                             rule_per_cred_charge_in   IN sfrfaud.sfrfaud_rgfe_per_cred_charge%TYPE,
                             rule_flat_fee_amount_in   IN sfrfaud.sfrfaud_rgfe_flat_fee_amount%TYPE,
                             rule_from_flat_hrs_in     IN sfrfaud.sfrfaud_rgfe_from_flat_hrs%TYPE,
                             rule_to_flat_hrs_in       IN sfrfaud.sfrfaud_rgfe_to_flat_hrs%TYPE,
                             rule_crse_ol_start_hr_in  IN sfrfaud.sfrfaud_rgfe_crse_ol_start_hr%TYPE,
                             RGFE_RULE_VALUE           IN sfrfaud.SFRFAUD_RGFE_RULE_VALUE%type default null);

  /* ******************************************************************* */
  /* Process the course registrations for the student/term.              */
  /* ******************************************************************* */
  /* Select all the course registrations for the student, including      */
  /* refund information. Check first for refund information by           */
  /* enrollment status (SFBRFST) since this takes presidence over        */
  /* refunding by registration status (SFRRFCR). If no refund info for   */
  /* the enrollment status, go on to process the courses using the       */
  /* refund information for the registration status. Determine the       */
  /* student's liability in terms of course charges based on whether or  */
  /* not a refund percentage is applicable and if the enrollment status  */
  /* or the registration status means WD. Insert the course values into  */
  /* temp table, SFTFEES, and go on to calculate the section fees for    */
  /* the course.                                                         */
  /* ******************************************************************* */
  PROCEDURE p_studentcourses(pidm_in              IN spriden.spriden_pidm%TYPE,
                             term_in              IN stvterm.stvterm_code%TYPE,
                             source_pgm_in        IN VARCHAR2,
                             return_status_in_out IN OUT NUMBER);

  /* ******************************************************************* */
  /* Process the course registrations for the student/term strictly      */
  /* by course refunding rules in SFRRFCR.                               */
  /* ******************************************************************* */
  PROCEDURE p_courses_by_rfcr(pidm_in              IN spriden.spriden_pidm%TYPE,
                              term_in              IN stvterm.stvterm_code%TYPE,
                              source_pgm_in        IN VARCHAR2,
                              return_status_in_out IN OUT NUMBER);

  /* ******************************************************************* */
  /* Process the course.                                                 */
  /* ******************************************************************* */
  PROCEDURE p_processcourse(pidm_in              IN spriden.spriden_pidm%TYPE,
                            term_in              IN stvterm.stvterm_code%TYPE,
                            rfnd_source_table_in IN VARCHAR2,
                            source_pgm_in        IN VARCHAR2,
                            return_status_in_out IN OUT NUMBER);

  /* ******************************************************************* */
  /* Process fees for the course sections.                               */
  /* ******************************************************************* */
  PROCEDURE p_sectionfees(pidm_in       IN spriden.spriden_pidm%TYPE,
                          term_in       IN stvterm.stvterm_code%TYPE,
                          source_pgm_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Process fees for the course extensions.                             */
  /* ******************************************************************* */
  PROCEDURE p_extensionfees(pidm_in       IN spriden.spriden_pidm%TYPE,
                            term_in       IN stvterm.stvterm_code%TYPE,
                            source_pgm_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Calculate course hour liability when the assessment involves drops  */
  /* and previous flat hour rule qualification.                          */
  /* ******************************************************************* */
  /* Defect 102188 to add/replace parm vars:                             */
  /* prev_rule_from_flat_hr_tui_in                                       */
  /* prev_rule_OL_start_hr_tui_in                                        */
  /* last_met_flat_rule_ind_tui_in                                       */
  /* prev_rule_from_flat_hr_fee_in                                       */
  /* prev_rule_OL_start_hr_fee_in                                        */
  /* last_met_flat_rule_ind_fee_in                                       */
  /* tui_calculated_ind_in_out                                           */
  /* fee_calculated_ind_in_out                                           */
  /* ******************************************************************* */

  PROCEDURE p_calc_flat_hr_liability(pidm_in                    IN spriden.spriden_pidm%TYPE,
                                     term_in                    IN stvterm.stvterm_code%TYPE,
                                     tui_nondrop_bill_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     tui_nondrop_waiv_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     fee_nondrop_bill_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     fee_nondrop_waiv_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     --   nondrop_bill_hr_in_out        IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     --   nondrop_waiv_hr_in_out        IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_bill_hr_tuit_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_waiv_hr_tuit_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_bill_hr_tuit_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_waiv_hr_tuit_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_bill_hr_tuit_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_waiv_hr_tuit_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_bill_hr_fees_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_waiv_hr_fees_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_bill_hr_fees_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_waiv_hr_fees_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_bill_hr_fees_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_waiv_hr_fees_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     prev_rule_from_flat_hr_tui_in IN sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                     prev_rule_OL_start_hr_tui_in  IN sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                     last_met_flat_rule_ind_tui_in IN VARCHAR2,
                                     prev_rule_from_flat_hr_fee_in IN sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                     prev_rule_OL_start_hr_fee_in  IN sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                     last_met_flat_rule_ind_fee_in IN VARCHAR2,
                                     tui_calculated_ind_in_out     IN OUT BOOLEAN,
                                     fee_calculated_ind_in_out     IN OUT BOOLEAN,
                                     tui_flat_cnt                  IN OUT pls_integer,
                                     tui_waiv_flat_cnt             IN OUT pls_integer,
                                     fee_flat_cnt                  IN OUT pls_integer,
                                     fee_waiv_flat_cnt             IN OUT pls_integer);

  /* ******************************************************************* */
  /* Calculate conventional course hour liability.                       */
  /* Request made by JC to replace tot* with p_tot* so that Test Harness */
  /* will work.                                                          */
  /* ******************************************************************* */
  PROCEDURE p_calc_reg_hr_liability(p_tot_reg_bill_hr       IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_bill_hr_tuit IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_bill_hr_fees IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_reg_waiv_hr       IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_waiv_hr_tuit IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_waiv_hr_fees IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE);

  /* ******************************************************************* */
  /* Determine student's liability based on their enrollment status.     */
  /* ******************************************************************* */
  /* Sum the charges for detail codes under the student's account that   */
  /* originated from assessment (source 'R') for the term. Apply the     */
  /* liability percentage determined from the refund rule to the total   */
  /* for the detail code based on the DCAT code (TUI or FEE) for the     */
  /* detail code. Record the liability in the student's audit data.      */
  /* ******************************************************************* */
  PROCEDURE p_calc_rfst_liability(pidm_in                 IN spriden.spriden_pidm%TYPE,
                                  term_in                 IN stvterm.stvterm_code%TYPE,
                                  ests_wd_assessed_ind_in IN VARCHAR2,
                                  source_pgm_in           IN VARCHAR2);

  /* ******************************************************************* */
  /* Procedure to retrieve the liability data from the last assessment   */
  /* audit if a flat rule was met for the rule type.                     */
  /* ******************************************************************* */
  /* Defect 102188 - add/replaced parm variables                         */
  /* prev_rule_from_flat_hr_tui_in_out                                   */
  /* prev_rule_from_flat_hr_fee_in_out                                   */
  /* prev_rule_OL_start_hr_tui_in_out                                    */
  /* prev_rule_OL_start_hr_fee_in_out                                    */
  /* last_met_flat_ind_tui_in_out                                        */
  /* last_met_flat_ind_fee_in_out                                        */
  /* starting_liab_bill_hr_tui_out                                       */
  /* starting_liab_bill_hr_fee_out                                       */
  /* last_flat_assessment_date_tui                                       */
  /* last_flat_assessment_date_fee                                       */
  /* ******************************************************************* */

  PROCEDURE p_get_last_assess_flat_data(pidm_in                        IN spriden.spriden_pidm%TYPE,
                                        term_in                        IN stvterm.stvterm_code%TYPE,
                                        prev_liab_bill_hr_in_out       IN OUT sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                                        prev_rule_from_flat_hr_tui_io  IN OUT sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                        prev_rule_from_flat_hr_fee_io  IN OUT sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                        prev_rule_OL_start_hr_tui_io   IN OUT sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                        prev_rule_OL_start_hr_fee_io   IN OUT sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                        rule_type_in                   IN sfrrgfe.sfrrgfe_type%TYPE,
                                        rule_value_in                  IN sfrrgfe.sfrrgfe_type%TYPE,
                                        last_met_flat_ind_tui_in_out   IN OUT VARCHAR2,
                                        last_met_flat_ind_fee_in_out   IN OUT VARCHAR2,
                                        prev_rule_crse_waiv_ind_tui_io IN OUT sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                        prev_rule_crse_waiv_ind_fee_io IN OUT sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                        starting_liab_bill_hr_tui_out  OUT sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                                        starting_liab_bill_hr_fee_out  OUT sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                                        process_flat_from_hrs          sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                        process_flat_to_hrs            sfrrgfe.sfrrgfe_from_flat_hrs%TYPE);

  /* ******************************************************************* */
  /* Find all flat rule ranges used in past assessment for student, type of rule and value     */
  /* We need to process each flat separately because the liability may be different based on drops       */
  /* ******************************************************************* */

  PROCEDURE p_multiple_rules(pidm_in       IN spriden.spriden_pidm%TYPE,
                             term_in       IN stvterm.stvterm_code%TYPE,
                             rule_type_in  IN VARCHAR2,
                             rule_value_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Process the fees associated with each course level within the       */
  /* student's registration for the term.                                */
  /* ******************************************************************* */
  /*  Course   fees for non student type rules are processed a little diferently than student.  */
  /*  For each course level, campus, attr determined within the student's registration */
  /*  for the term, we need to process the totals for the course type  */
  /*  itself and then process the courses that the student is registered */
  /*  for that have the current course level being processed.            */
  /* ******************************************************************* */
  PROCEDURE p_calcliability(pidm_in       IN spriden.spriden_pidm%TYPE,
                            term_in       IN stvterm.stvterm_code%TYPE,
                            source_pgm_in IN VARCHAR2,
                            type_in       IN sfrrgfe.sfrrgfe_type%type);

  /* ******************************************************************* */
  /* Process the additional fees for the student.                        */
  /* ******************************************************************* */
  PROCEDURE p_additionalfees(pidm_in       IN spriden.spriden_pidm%TYPE,
                             term_in       IN stvterm.stvterm_code%TYPE,
                             source_pgm_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Apply the rule.                                                     */
  /* ******************************************************************* */
  PROCEDURE p_applyrules(pidm_in                 IN spriden.spriden_pidm%TYPE,
                         term_in                 IN stvterm.stvterm_code%TYPE,
                         rule_source_in          IN VARCHAR2,
                         rule_type_in            IN VARCHAR2,
                         rule_value_in           IN VARCHAR2,
                         ptrm_rule_in            IN VARCHAR2,
                         gmod_code_in            IN stvgmod.stvgmod_code%TYPE,
                         schd_code_in            IN stvschd.stvschd_code%TYPE,
                         insm_code_in            IN gtvinsm.gtvinsm_code%TYPE,
                         assess_by_course_ind_in IN VARCHAR2,
                         source_pgm_in           IN VARCHAR2,
                         process_flat_from_hrs   IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                         process_flat_to_hrs     IN sfrfaud.sfrfaud_reg_bill_hr%TYPE);

  /* ******************************************************************* */
  /* Sum the audit rows and insert needed charges into TBRACCD.          */
  /* ******************************************************************* */
  PROCEDURE p_totalfees(pidm_in               IN spriden.spriden_pidm%TYPE,
                        term_in               IN stvterm.stvterm_code%TYPE,
                        source_pgm_in         IN VARCHAR2,
                        ignore_sfrfmax_ind_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Reverse charges from previous assessment that are no longer         */
  /* applicable for the student.                                         */
  /* ******************************************************************* */
  PROCEDURE p_reverse_na_charges(pidm_in       IN spriden.spriden_pidm%TYPE,
                                 term_in       IN stvterm.stvterm_code%TYPE,
                                 source_pgm_in IN VARCHAR2);

  /* ******************************************************************* */
  /*  Drop the student from the institution.                             */
  /* ******************************************************************* */
  /* Processing occurs when the SFBETRM record is deleted via SFAREGS.   */
  /* This logic comes from the trigger code from SFAREGS.                */
  /* ******************************************************************* */
  PROCEDURE p_process_etrm_drop(pidm_in      IN spriden.spriden_pidm%TYPE,
                                term_in      IN stvterm.stvterm_code%TYPE,
                                drop_date_in IN sfrstcr.sfrstcr_rsts_date%TYPE);

  /* ******************************************************************* */
  /* Initialize the global working variables.                            */
  /* ******************************************************************* */
  PROCEDURE p_init_global_vars;

  /* ******************************************************************* */
  /* Reinitialize the reusable working variables.                        */
  /* ******************************************************************* */
  PROCEDURE p_reinit_temp_vars;

  /* ******************************************************************* */
  /* Procedure to insert the accounting record for the assessment and    */
  /*  set a global BOOLEAN indicating that an assessment occurred.       */
  /* ******************************************************************* */
  PROCEDURE p_insert_tbraccd_rec(pidm_in              IN spriden.spriden_pidm%TYPE,
                                 accd_tran_num_in_out IN OUT tbraccd.tbraccd_tran_number%TYPE,
                                 term_in              IN stvterm.stvterm_code%TYPE,
                                 aud_detl_code_in     IN tbraccd.tbraccd_detail_code%TYPE,
                                 amount_in            IN tbraccd.tbraccd_amount%TYPE,
                                 balance_in           IN tbraccd.tbraccd_balance%TYPE,
                                 eff_date_in          IN tbraccd.tbraccd_effective_date%TYPE,
                                 srce_code_in         IN tbraccd.tbraccd_srce_code%TYPE,
                                 crn_in               IN tbraccd.tbraccd_crn%TYPE,
                                 trans_date_in        IN tbraccd.tbraccd_trans_date%TYPE,
                                 orig_chg_ind_in      IN tbraccd.tbraccd_orig_chg_ind%TYPE);

  /* ******************************************************************* */
  /* Delete SFRFAUD rows created for mock assessment done via SFAREGF    */
  /* Pass in activity date string as 'DD-MON-YYYY HH24:MI:SS'.           */
  /* ******************************************************************* */
  PROCEDURE p_delete_mock_assessment(pidm_in         IN spriden.spriden_pidm%TYPE,
                                     term_in         IN stvterm.stvterm_code%TYPE,
                                     sessionid_in    IN VARCHAR2,
                                     act_date_str_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Delete SFRBTCH collector record if it exists.                       */
  /* ******************************************************************* */
  PROCEDURE p_delete_sfrbtch(pidm_in IN spriden.spriden_pidm%TYPE,
                             term_in IN stvterm.stvterm_code%TYPE);

  /* ******************************************************************* */
  /* Process the refund by total refunds and penalties for the TUI and   */
  /* FEE DCAT codes when refund by total enabled for the term.           */
  /* ******************************************************************* */
  PROCEDURE p_calc_rbt_refunds(pidm_in               IN spriden.spriden_pidm%TYPE,
                               term_in               IN stvterm.stvterm_code%TYPE,
                               source_pgm_in         IN VARCHAR2,
                               ignore_sfrfmax_ind_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Post accrued total refund by total penalties for TUI and FEE to     */
  /* clearing account detail codes, if appropriate.                      */
  /* ******************************************************************* */
  PROCEDURE p_post_rbt_penalties(pidm_in       IN spriden.spriden_pidm%TYPE,
                                 term_in       IN stvterm.stvterm_code%TYPE,
                                 source_pgm_in IN VARCHAR2);

  /* ******************************************************************* */
  /* Swapping.                                                3/2005 BAG */
  /* ******************************************************************* */
  /* Procedure to determine the amount of dropped hours that qualify for */
  /* swapping. Update SFTFEES to capture the amount of swapped hours:    */
  /* RBC: Set both sftfees_tuit_liab_percentage = 0 and                  */
  /*      sftfees_fees_liab_percentage = 0 for dropped hours that swap.  */
  /* ******************************************************************* */
  PROCEDURE p_process_hours_swap(pidm_in    IN spriden.spriden_pidm%TYPE,
                                 term_in    IN stvterm.stvterm_code%TYPE,
                                 phrs_out   OUT sfrstcr.sfrstcr_bill_hr%TYPE,
                                 pstart_out OUT DATE,
                                 pend_out   OUT DATE);

  /* ******************************************************************* */
  /* Procedure to determine the swap liability and store in SFRFAUD.     */
  /* ******************************************************************* */
  PROCEDURE p_calc_swap_liability(pidm_in       IN spriden.spriden_pidm%TYPE,
                                  term_in       IN stvterm.stvterm_code%TYPE,
                                  phrs_in       IN sfrstcr.sfrstcr_bill_hr%TYPE,
                                  pstart_in     IN DATE,
                                  pend_in       IN DATE,
                                  source_pgm_in IN VARCHAR2);

  /* ******************************************************************* */
  /* 104667, new procedure                                               */
  /* ******************************************************************* */
  /* Non Swapping.                                                       */
  /* ******************************************************************* */
  /* Procedure to determine the amount for penalty reversal in a RBT and */
  /* Non-SWAPPING.                                                       */
  /* ******************************************************************* */
  PROCEDURE p_rev_nonswap_RBT(pidm_in IN spriden.spriden_pidm%TYPE,
                              term_in IN stvterm.stvterm_code%TYPE);

  /* ******************************************************************* */
  /* Procedure to print debug dbms messages                              */
  /* ******************************************************************* */
  PROCEDURE p_print_dbms(dbms_msg_in IN VARCHAR2);

END ME_SFKFEES;
/

