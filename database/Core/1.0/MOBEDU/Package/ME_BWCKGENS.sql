PROMPT CREATE OR REPLACE PACKAGE me_bwckgens
CREATE OR REPLACE package me_bwckgens is

  -- Author  : USER
  -- Created : 16-01-2013 11:10:59
  -- Purpose :

  -- Public type declarations

  function fz_get_var_ind(p_term varchar2, p_crn number) return varchar2;

  PROCEDURE PZ_CHANGE_CREDITS(p_student_id VARCHAR2,
                              P_TERM_CODE  VARCHAR2,
                              P_CRN        NUMBER,
                              P_cred       NUMBER,
                              P_CRED_OLD   NUMBER,
                              P_ERROR      OUT VARCHAR2);

  PROCEDURE P_RegsUpd(term     IN stvterm.stvterm_code%TYPE DEFAULT NULL,
                      crn      IN OWA_UTIL.ident_arr,
                      cred     IN OWA_UTIL.ident_arr,
                      gmod     IN OWA_UTIL.ident_arr,
                      levl     IN OWA_UTIL.ident_arr,
                      cred_old IN OWA_UTIL.ident_arr,
                      gmod_old IN OWA_UTIL.ident_arr,
                      levl_old IN OWA_UTIL.ident_arr,
                      P_ERROR  OUT VARCHAR2);

  procedure p_intglobal(p_pidm number, p_term stvterm.stvterm_code%type);

end ME_BWCKGENS;

/

