PROMPT CREATE OR REPLACE PACKAGE z_cm_mobile_campus
CREATE OR REPLACE package z_cm_mobile_campus as

  /* TODO enter package declarations (types, exceptions, methods etc) here */
  /* EMPLOYEE_POSN varchar2(6);

  REPORTTO_POSN varchar2(6);

  REPORTTO_PIDM number;*/

  type SPRTELETABTYP is table of SPRTELE.SPRTELE_ATYP_CODE%type index by binary_integer;

  SPRTELETYP SPRTELETABTYP;

  INDEXOF number;

  RETURN_VALUE varchar2(30);

  TELEPHONE varchar2(16);

  function FZ_GET_SPRADDR_ROWID(PIDM     number,
                                ADDRESS1 varchar2 default 'BU',
                                ADDRESS2 varchar2 default 'BI',
                                ADDRESS3 varchar2 default 'MA')
    return varchar2;
  function FZ_GET_SPRTELE_ROWID(PIDM     number,
                                ADDRESS1 varchar2 default 'BU',
                                ADDRESS2 varchar2 default 'BI',
                                ADDRESS3 varchar2 default 'MA')
    return varchar2;

  FUNCTION FZ_GET_GOREMAL_ROWID(PIDM     NUMBER,
                                ADDRESS1 VARCHAR2 DEFAULT 'BU',
                                ADDRESS2 VARCHAR2 DEFAULT 'BI',
                                ADDRESS3 VARCHAR2 DEFAULT 'MA')
    RETURN VARCHAR2;
  FUNCTION FZ_CATEGORY(P_PIDM NUMBER)RETURN VARCHAR2;
  FUNCTION FZ_GET_STATE(P_STATE VARCHAR2) RETURN VARCHAR2;
  FUNCTION FZ_GET_CAMPUS(P_PIDM NUMBER) RETURN VARCHAR2;
  FUNCTION F_GET_COLLEGE(P_PIDM NUMBER) RETURN VARCHAR2;
end Z_CM_MOBILE_CAMPUS;


/

