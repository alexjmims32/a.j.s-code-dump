PROMPT CREATE OR REPLACE PACKAGE me_account_objects
CREATE OR REPLACE PACKAGE me_account_objects is

  -- Author  : N2N Services Inc.
  --           www.n2nservices.net

  /**
  * Function to get future term charges based on pidm
  * @return                    Future term charges
  * @param p_pidm              Internal identification number
  */

  function fz_future_term_charges(p_pidm spriden.spriden_pidm%type)
    return number;

  function fz_get_acc_balance(stu_pidm in number) return number;

  function fz_get_tot_due_bal(stu_pidm in number) return number;

  function fz_get_currtrm_acc_bal(stu_pidm in number) return number;

  function fz_get_prevtrm_acc_bal(stu_pidm in number) return number;

  FUNCTION fz_get_hold_flag(p_id        IN spriden.spriden_id%TYPE,
                            p_hold_type VARCHAR2) RETURN VARCHAR2;

  /**
  * Function to get unbilled charges based on pidm
  * @return                    Unbilled charges
  * @param p_pidm              Internal identification number p_pidm
  */
  function fz_unbilled_charges(p_pidm spriden.spriden_pidm%type)
    return number;

  /**
  * Function to get net past due based on pidm
  * @return                    Unbilled charges
  * @param p_pidm              Internal identification number p_pidm
  */
  function fz_net_past_due(p_pidm spriden.spriden_pidm%type) return number;

/**
  * Procedure to audit TZRMEMO by interface unique id
  * @return
  * @param p_intf_event_id        Event ID of the Interface
  * @param p_intf_unique_id       Unique ID of the Interface
  * @param n_total_records        Total Records
  * @param p_total_records        Total Records
  * @param p_total_amount         Total Amount
  * @param e_total_records        Total Records
  * @param e_total_amount         Total Amount
  * @param p_error                Error
  */

end me_account_objects;





/

