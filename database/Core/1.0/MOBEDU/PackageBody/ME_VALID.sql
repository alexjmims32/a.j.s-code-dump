PROMPT CREATE OR REPLACE PACKAGE BODY me_valid
CREATE OR REPLACE PACKAGE BODY me_valid IS
  --FUNCTION TO VALIDATE THE ADMISSION OF THE STUDENT FOR A TERM
  FUNCTION FZ_ME_VAL_ADMS(PIDM IN VARCHAR2, TERM_CODE IN VARCHAR2)
    RETURN VARCHAR2 IS
    CURSOR C_EXIST(C_PIDM IN VARCHAR2, C_TERM IN VARCHAR2) IS
    /* SELECT DISTINCT 'Y'
                                                FROM SATURN.SARADAP D
                                                JOIN SATURN.SPRIDEN S
                                                  ON D.SARADAP_PIDM = S.SPRIDEN_PIDM
                                                 AND S.SPRIDEN_CHANGE_IND IS NULL
                                                 AND D.SARADAP_APPL_NO =
                                                     (SELECT MAX(B1.SARADAP_APPL_NO)
                                                        FROM SATURN.SARADAP B1
                                                       WHERE B1.SARADAP_PIDM = C_PIDM
                                                         AND B1.SARADAP_TERM_CODE_ENTRY = C_TERM)
                                                 and D.SARADAP_PIDM = C_PIDM
                                                 AND D.SARADAP_TERM_CODE_ENTRY = C_TERM
                                                 \*AND SARADAP_APST_CODE = 'D'*\;*/
      select 'Y'
        from SATURN.SPRIDEN S
       where S.SPRIDEN_CHANGE_IND IS NULL
         and s.spriden_entity_ind = 'P'
         and S.SPRIDEN_PIDM = c_pidm;
    C_DATA VARCHAR2(100);
  BEGIN
    OPEN C_EXIST(PIDM, TERM_CODE);
    FETCH C_EXIST
      INTO C_DATA;
    CLOSE C_EXIST;
    IF C_DATA = 'Y' THEN
      RETURN C_DATA;
    ELSE
      RETURN 'N';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'EXCEPTION RAISED WHILE ADMISSION';
  END FZ_ME_VAL_ADMS;

  --FUNCTION VALIDATE THE EXISTANCE OF THE STUDENT

  --
  FUNCTION FZ_ME_VAL_STUDENT(PIDM IN VARCHAR2, TERM_CODE IN VARCHAR2)
    RETURN VARCHAR2 IS
    CURSOR C_EXIST(P_PIDM IN VARCHAR2, P_TERM IN VARCHAR2) IS
      WITH W_TERM_CODE AS
       (SELECT MAX(SGBSTDN_TERM_CODE_EFF) TERM_CODE
          FROM SGBSTDN A
         WHERE A.SGBSTDN_PIDM = P_PIDM
           AND A.SGBSTDN_TERM_CODE_EFF <= P_TERM)
      SELECT 'Y'
        FROM DUAL
       WHERE EXISTS (SELECT 1
                FROM SGBSTDN A
               WHERE A.SGBSTDN_PIDM = P_PIDM
                 AND SGBSTDN_STST_CODE = 'AS'
                 AND A.SGBSTDN_TERM_CODE_EFF =
                     (SELECT TERM_CODE FROM W_TERM_CODE));

    C_DATA VARCHAR2(100);

  BEGIN
    OPEN C_EXIST(PIDM, TERM_CODE);
    FETCH C_EXIST
      INTO C_DATA;
    CLOSE C_EXIST;
    IF C_DATA = 'Y' THEN
      RETURN C_DATA;
    ELSE
      RETURN 'N';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'EXCEPTION RAISED WHILE CHECKING THE VALIDATION OF THE STUDENT';
  END FZ_ME_VAL_STUDENT;

  --
  FUNCTION FZ_GET_ALL_RESTRICTIONS(P_CRN IN NUMBER, P_TERM_CODE IN NUMBER)
    RETURN T_ARRAY_TYPE IS
    P_CRSE_NUMB    number;
    P_SUBJ_CODE    varchar2(40);
    A_RESTRICTIONS T_ARRAY_TYPE := T_ARRAY_TYPE();
    CURSOR C_GET_ALL_RESTR IS
      SELECT 'X' IND, 'PROGRAM' RESTR_CATEG
        FROM SCRRPRG T1,
             (SELECT MAX(T2.SCRRPRG_TERM_CODE_EFFECTIVE) COL1
                FROM SCRRPRG T2
               WHERE T2.SCRRPRG_SUBJ_CODE = P_SUBJ_CODE
                 AND T2.SCRRPRG_CRSE_NUMB = P_CRSE_NUMB
                 AND T2.SCRRPRG_TERM_CODE_EFFECTIVE <= P_TERM_CODE
                 AND T2.SCRRPRG_PROGRAM IS NOT NULL) T3
       WHERE T1.SCRRPRG_SUBJ_CODE = P_SUBJ_CODE
         AND T1.SCRRPRG_CRSE_NUMB = P_CRSE_NUMB
         AND T1.SCRRPRG_TERM_CODE_EFFECTIVE = T3.COL1
      UNION
      SELECT 'X', 'LEVEL'
        FROM SCRRLVL
       WHERE SCRRLVL_SUBJ_CODE = P_SUBJ_CODE
         AND SCRRLVL_CRSE_NUMB = P_CRSE_NUMB
         AND SCRRLVL_EFF_TERM =
             (SELECT MAX(SCRRLVL_EFF_TERM)
                FROM SCRRLVL
               WHERE SCRRLVL_SUBJ_CODE = P_SUBJ_CODE
                 AND SCRRLVL_CRSE_NUMB = P_CRSE_NUMB
                 AND SCRRLVL_EFF_TERM <= P_TERM_CODE
                 AND SCRRLVL_LEVL_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'DEGREE'
        FROM SCRRDEG
       WHERE SCRRDEG_SUBJ_CODE = P_SUBJ_CODE
         AND SCRRDEG_CRSE_NUMB = P_CRSE_NUMB
         AND SCRRDEG_TERM_CODE_EFFECTIVE =
             (SELECT MAX(SCRRDEG_TERM_CODE_EFFECTIVE)
                FROM SCRRDEG
               WHERE SCRRDEG_SUBJ_CODE = P_SUBJ_CODE
                 AND SCRRDEG_CRSE_NUMB = P_CRSE_NUMB
                 AND SCRRDEG_TERM_CODE_EFFECTIVE <= P_TERM_CODE
                 AND SCRRDEG_DEGC_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'COLLEGE'
        FROM SCRRCOL
       WHERE SCRRCOL_SUBJ_CODE = P_SUBJ_CODE
         AND SCRRCOL_CRSE_NUMB = P_CRSE_NUMB
         AND SCRRCOL_EFF_TERM =
             (SELECT MAX(SCRRCOL_EFF_TERM)
                FROM SCRRCOL
               WHERE SCRRCOL_SUBJ_CODE = P_SUBJ_CODE
                 AND SCRRCOL_CRSE_NUMB = P_CRSE_NUMB
                 AND SCRRCOL_EFF_TERM <= P_TERM_CODE
                 AND SCRRCOL_COLL_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'MAJOR'
        FROM SCRRMAJ X
       WHERE X.SCRRMAJ_SUBJ_CODE = P_SUBJ_CODE
         AND X.SCRRMAJ_CRSE_NUMB = P_CRSE_NUMB
         AND X.SCRRMAJ_EFF_TERM =
             (SELECT MAX(Y.SCRRMAJ_EFF_TERM)
                FROM SCRRMAJ Y
               WHERE Y.SCRRMAJ_SUBJ_CODE = P_SUBJ_CODE
                 AND Y.SCRRMAJ_CRSE_NUMB = P_CRSE_NUMB
                 AND Y.SCRRMAJ_EFF_TERM <= P_TERM_CODE
                 AND NVL(Y.SCRRMAJ_LFST_CODE, 'N') =
                     NVL(X.SCRRMAJ_LFST_CODE, 'N')
                 AND Y.SCRRMAJ_MAJR_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'DEPARTMENT'
        FROM SCRRDEP X
       WHERE X.SCRRDEP_SUBJ_CODE = P_SUBJ_CODE
         AND X.SCRRDEP_CRSE_NUMB = P_CRSE_NUMB
         AND X.SCRRDEP_EFF_TERM =
             (SELECT MAX(Y.SCRRDEP_EFF_TERM)
                FROM SCRRDEP Y
               WHERE Y.SCRRDEP_SUBJ_CODE = P_SUBJ_CODE
                 AND Y.SCRRDEP_CRSE_NUMB = P_CRSE_NUMB
                 AND Y.SCRRDEP_EFF_TERM <= P_TERM_CODE
                 AND Y.SCRRDEP_DEPT_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'COHORT'
        FROM SCRRCHR X
       WHERE X.SCRRCHR_SUBJ_CODE = P_SUBJ_CODE
         AND X.SCRRCHR_CRSE_NUMB = P_CRSE_NUMB
         AND X.SCRRCHR_EFF_TERM =
             (SELECT MAX(Y.SCRRCHR_EFF_TERM)
                FROM SCRRCHR Y
               WHERE Y.SCRRCHR_SUBJ_CODE = P_SUBJ_CODE
                 AND Y.SCRRCHR_CRSE_NUMB = P_CRSE_NUMB
                 AND Y.SCRRCHR_EFF_TERM <= P_TERM_CODE
                 AND Y.SCRRCHR_CHRT_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'ATTRIBUTE'
        FROM SCRRATT X
       WHERE X.SCRRATT_SUBJ_CODE = P_SUBJ_CODE
         AND X.SCRRATT_CRSE_NUMB = P_CRSE_NUMB
         AND X.SCRRATT_EFF_TERM =
             (SELECT MAX(Y.SCRRATT_EFF_TERM)
                FROM SCRRATT Y
               WHERE Y.SCRRATT_SUBJ_CODE = P_SUBJ_CODE
                 AND Y.SCRRATT_CRSE_NUMB = P_CRSE_NUMB
                 AND Y.SCRRATT_EFF_TERM <= P_TERM_CODE
                 AND Y.SCRRATT_ATTS_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'CAMPUS'
        FROM SCRRCAM
       WHERE SCRRCAM_SUBJ_CODE = P_SUBJ_CODE
         AND SCRRCAM_CRSE_NUMB = P_CRSE_NUMB
         AND SCRRCAM_EFF_TERM =
             (SELECT MAX(SCRRCAM_EFF_TERM)
                FROM SCRRCAM
               WHERE SCRRCAM_SUBJ_CODE = P_SUBJ_CODE
                 AND SCRRCAM_CRSE_NUMB = P_CRSE_NUMB
                 AND SCRRCAM_EFF_TERM <= P_TERM_CODE
                 AND SCRRCAM_CAMP_CODE IS NOT NULL)
      UNION
      SELECT 'X', 'CLASS'
        FROM SCRRCLS
       WHERE SCRRCLS_SUBJ_CODE = P_SUBJ_CODE
         AND SCRRCLS_CRSE_NUMB = P_CRSE_NUMB
         AND SCRRCLS_EFF_TERM =
             (SELECT MAX(SCRRCLS_EFF_TERM)
                FROM SCRRCLS
               WHERE SCRRCLS_SUBJ_CODE = P_SUBJ_CODE
                 AND SCRRCLS_CRSE_NUMB = P_CRSE_NUMB
                 AND SCRRCLS_EFF_TERM <= P_TERM_CODE
                 AND SCRRCLS_CLAS_CODE IS NOT NULL);

  BEGIN
    begin
      select ssbsect_subj_code, ssbsect_crse_numb
        into P_SUBJ_CODE, P_CRSE_NUMB
        from ssbsect
       where ssbsect_crn = P_CRN
         and ssbsect_term_code = P_TERM_CODE;
    exception
      when others then
        null;
    end;
    FOR R_GET_RESTR IN C_GET_ALL_RESTR LOOP
      A_RESTRICTIONS.EXTEND;
      A_RESTRICTIONS(A_RESTRICTIONS.COUNT) := R_GET_RESTR.RESTR_CATEG;
    END LOOP;
    RETURN A_RESTRICTIONS;
    dbms_output.put_line('Temp is ' || A_RESTRICTIONS.COUNT);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END FZ_GET_ALL_RESTRICTIONS;
  --
  FUNCTION FZ_GET_FAILED_RESTRICTIONS(P_CRN        IN NUMBER,
                                      P_TERM_CODE  IN NUMBER,
                                      P_STUDENT_ID VARCHAR2) RETURN VARCHAR2 IS
    V_PIDM        SPRIDEN.SPRIDEN_PIDM%TYPE;
    V_SOBTERM_ROW SOBTERM%ROWTYPE;
    V_ERROR_FLAG  VARCHAR2(50);
    V_ERROR_MSG   VARCHAR2(200);
    V_RMSG_CDE    VARCHAR2(50);
    V_TOTAL_MSG   VARCHAR2(300);
    V_ERROR       VARCHAR2(200);
    V_CLAS_CODE   VARCHAR2(10);
    V_CLAS_DESC   VARCHAR2(50);
    lcur_levl     VARCHAR2(10);
  BEGIN
    BEGIN
      select baninst1.gb_common.f_get_pidm(P_STUDENT_ID)
        into v_pidm
        from dual;
    exception
      when others then
        RETURN 'STUDENT NOT EXISTED IN BANNER.';
        --GOTO QUIT_FROM_PROGRAM;
    end;
    --- GET SOBTERM RECORD
    BEGIN
      SELECT *
        INTO V_SOBTERM_ROW
        FROM SOBTERM
       WHERE SOBTERM_TERM_CODE = P_TERM_CODE;
    EXCEPTION
      WHEN OTHERS THEN
        RETURN 'INVALID TERM CODE';
    END;
    --CHECK PROG RESTRICTIONS
    BEGIN
      sfkfunc.p_check_prog(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           prog_severity_in  => V_SOBTERM_ROW.SOBTERM_PROGRAM_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE,
                           pidm_in           => V_PIDM);
      IF V_ERROR_MSG IS NOT NULL THEN
        V_TOTAL_MSG :=  /*V_TOTAL_MSG||','||*/
         V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := V_ERROR || SQLERRM;
        --RETURN V_ERROR;
    END;
    -- CHECK LEVEL RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_levl(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           levl_severity_in  => V_SOBTERM_ROW.SOBTERM_LEVL_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE,
                           pidm_in           => V_PIDM);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    --CHECK DEGREE RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure

      sfkfunc.p_check_degc(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           degc_severity_in  => V_SOBTERM_ROW.SOBTERM_DEGREE_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE,
                           pidm_in           => V_PIDM);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    --CHECK COLLEGE RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_coll(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           coll_severity_in  => V_SOBTERM_ROW.SOBTERM_COLL_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE,
                           pidm_in           => V_PIDM);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    -- CHECK MAJOR RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_majr(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           majr_severity_in  => V_SOBTERM_ROW.SOBTERM_MAJR_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE,
                           pidm_in           => V_PIDM);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    -- CHECK DEPARTMENT RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_dept(p_pidm           => V_PIDM,
                           p_term_code      => P_TERM_CODE,
                           p_crn            => P_CRN,
                           p_dept_severity  => V_SOBTERM_ROW.SOBTERM_DEPT_SEVERITY,
                           p_rmsg_cde_inout => V_RMSG_CDE,
                           p_message_inout  => V_ERROR_MSG,
                           p_err_flag_inout => V_ERROR_FLAG);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    --CHECK COHORT RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_chrt(p_pidm           => V_PIDM,
                           p_term_code      => P_TERM_CODE,
                           p_crn            => P_CRN,
                           p_chrt_severity  => V_SOBTERM_ROW.SOBTERM_CHRT_SEVERITY,
                           p_rmsg_cde_inout => V_RMSG_CDE,
                           p_message_inout  => V_ERROR_MSG,
                           p_err_flag_inout => V_ERROR_FLAG);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    --CHECK ATTRIBUTE RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_atts(p_pidm           => V_PIDM,
                           p_term_code      => P_TERM_CODE,
                           p_crn            => P_CRN,
                           p_atts_severity  => V_SOBTERM_ROW.SOBTERM_ATTS_SEVERITY,
                           p_rmsg_cde_inout => V_RMSG_CDE,
                           p_message_inout  => V_ERROR_MSG,
                           p_err_flag_inout => V_ERROR_FLAG);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    --CHECK CAMPUS RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      sfkfunc.p_check_camp(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           camp_severity_in  => V_SOBTERM_ROW.SOBTERM_CAMP_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE,
                           pidm_in           => V_PIDM);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    --CHECK CLASS RESTRICTIONS
    begin
      V_RMSG_CDE   := NULL;
      V_ERROR_MSG  := NULL;
      V_ERROR_FLAG := NULL;
      -- Call the procedure
      lcur_levl := sokccur.f_curriculum_value(p_pidm      => V_PIDM,
                                              p_lmod_code => sb_curriculum_str.f_learner,
                                              p_term_code => P_TERM_CODE,
                                              p_key_seqno => 99,
                                              p_eff_term  => P_TERM_CODE,
                                              p_order     => 1,
                                              p_field     => 'LEVL');
      soklibs.p_class_calc(pidm            => V_PIDM,
                           levl_code       => lcur_levl,
                           term_code       => P_TERM_CODE,
                           in_progress_ind => V_SOBTERM_ROW.SOBTERM_INCL_ATTMPT_HRS_IND,
                           clas_code       => V_CLAS_CODE,
                           clas_desc       => V_CLAS_DESC);

      -- Call the procedure
      sfkfunc.p_check_clas(p_rmsg_cde_in_out => V_RMSG_CDE,
                           message_in_out    => V_ERROR_MSG,
                           err_flag_in_out   => V_ERROR_FLAG,
                           clas_in           => V_CLAS_CODE,
                           clas_severity_in  => V_SOBTERM_ROW.SOBTERM_CLAS_SEVERITY,
                           crn_in            => P_CRN,
                           term_in           => P_TERM_CODE);
      IF V_ERROR_FLAG IS NOT NULL THEN
        V_TOTAL_MSG := V_TOTAL_MSG || V_ERROR_MSG || ',';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
        V_ERROR := SQLERRM;
        --RETURN V_ERROR;
    end;
    RETURN V_TOTAL_MSG;
  END;
  --
  FUNCTION FZ_DUPL_CHECK(P_CRN       IN NUMBER,
                         P_TERM_CODE IN NUMBER,
                         P_PIDM      NUMBER) RETURN VARCHAR2 IS
    v_pidm        SPRIDEN.SPRIDEN_PIDM%TYPE;
    V_SSBSECT_ROW SSBSECT%ROWTYPE;
    V_COUNT       NUMBER(2);
  BEGIN
    V_PIDM := P_PIDM;
    /* BEGIN
    select baninst1.gb_common.f_get_pidm(P_STUDENT_ID)
        into v_pidm
        from dual;
    exception
      when others then
        RETURN 'STUDENT NOT EXISTED IN BANNER.';
        --GOTO QUIT_FROM_PROGRAM;
    end;*/
    SELECT *
      INTO V_SSBSECT_ROW
      FROM SSBSECT
     WHERE SSBSECT_CRN = P_CRN
       AND SSBSECT_TERM_CODE = P_TERM_CODE;
    ---=========================================
    SELECT COUNT(*)
      INTO V_COUNT
      FROM (SELECT A.SSBSECT_SUBJ_CODE SUBJ_CODE,
                   A.SSBSECT_CRSE_NUMB CRSE_NUMB,
                   A.SSBSECT_SCHD_CODE SCHD_CODE
              FROM SSBSECT A, SFRSTCR B
             WHERE A.SSBSECT_CRN = B.SFRSTCR_CRN
               AND A.SSBSECT_TERM_CODE = B.SFRSTCR_TERM_CODE
               AND B.SFRSTCR_PIDM = v_pidm
               AND B.SFRSTCR_TERM_CODE = P_TERM_CODE
               and nvl(B.SFRSTCR_ERROR_FLAG, 'N') NOT IN ('F', 'D')) D
     WHERE D.SUBJ_CODE = V_SSBSECT_ROW.SSBSECT_SUBJ_CODE
       AND D.CRSE_NUMB = V_SSBSECT_ROW.SSBSECT_CRSE_NUMB
       AND D.SCHD_CODE = V_SSBSECT_ROW.SSBSECT_SCHD_CODE;
    IF V_COUNT <> 0 THEN
      RETURN 'DUPL COURSE EXIST';
    ELSE
      RETURN NULL;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'SECTION RECORD NOT FOUND';
  END;
  --
  FUNCTION FZ_TIME_CHECK(P_CRN       VARCHAR2,
                         P_TERM_CODE VARCHAR2,
                         P_PIDM      NUMBER) RETURN VARCHAR2 IS

    V_ERROR VARCHAR2(300);

  BEGIN
    FOR I IN (SELECT 'TIME',
                     SB_REGISTRATION_MSG.F_GET_MESSAGE('TIME',
                                                       1,
                                                       X.SFRSTCR_CRN) MSG,
                     X.SFRSTCR_CRN,
                     D.SSRMEET_CRN
                FROM SSRMEET A, SSRMEET D, SFRSTCR X, SSBSECT Y
               WHERE (((A.SSRMEET_START_DATE BETWEEN D.SSRMEET_START_DATE AND
                     D.SSRMEET_END_DATE) OR
                     (A.SSRMEET_END_DATE BETWEEN D.SSRMEET_START_DATE AND
                     D.SSRMEET_END_DATE)) OR
                     ((D.SSRMEET_START_DATE BETWEEN A.SSRMEET_START_DATE AND
                     A.SSRMEET_END_DATE) OR
                     (D.SSRMEET_END_DATE BETWEEN A.SSRMEET_START_DATE AND
                     A.SSRMEET_END_DATE)))
                 AND (A.SSRMEET_MON_DAY = D.SSRMEET_MON_DAY OR
                     A.SSRMEET_TUE_DAY = D.SSRMEET_TUE_DAY OR
                     A.SSRMEET_WED_DAY = D.SSRMEET_WED_DAY OR
                     A.SSRMEET_THU_DAY = D.SSRMEET_THU_DAY OR
                     A.SSRMEET_FRI_DAY = D.SSRMEET_FRI_DAY OR
                     A.SSRMEET_SAT_DAY = D.SSRMEET_SAT_DAY OR
                     A.SSRMEET_SUN_DAY = D.SSRMEET_SUN_DAY)
                 AND (((A.SSRMEET_BEGIN_TIME BETWEEN D.SSRMEET_BEGIN_TIME AND
                     D.SSRMEET_END_TIME) OR
                     (A.SSRMEET_END_TIME BETWEEN D.SSRMEET_BEGIN_TIME AND
                     D.SSRMEET_END_TIME)) OR
                     ((D.SSRMEET_BEGIN_TIME BETWEEN A.SSRMEET_BEGIN_TIME AND
                     A.SSRMEET_END_TIME) OR
                     (D.SSRMEET_END_TIME BETWEEN A.SSRMEET_BEGIN_TIME AND
                     A.SSRMEET_END_TIME)))
                 AND D.SSRMEET_CRN <> A.SSRMEET_CRN
                 AND A.SSRMEET_CRN = X.SFRSTCR_CRN
                 AND A.SSRMEET_TERM_CODE = X.SFRSTCR_TERM_CODE
                 AND D.SSRMEET_CRN = Y.SSBSECT_CRN
                 AND Y.SSBSECT_TERM_CODE = P_TERM_CODE
                 AND Y.SSBSECT_CRN = P_CRN
                 AND (X.SFRSTCR_TIME_OVER = 'N' OR
                     X.SFRSTCR_TIME_OVER IS NULL)
                 AND NVL(X.sfrstcr_error_flag, 'N') NOT IN ('F', 'D')
                 AND X.SFRSTCR_TERM_CODE = P_TERM_CODE
                 AND X.SFRSTCR_PIDM = P_PIDM) LOOP
      RETURN I.MSG;
    END LOOP;
    RETURN NULL;

  EXCEPTION
    WHEN OTHERS THEN
      V_ERROR := 'EXCEPTION ' || SQLERRM;
      RETURN V_ERROR;
  END;
  --
  FUNCTION FZ_MHRS_CHECK(P_PIDM NUMBER, P_TERM VARCHAR2, P_CRN VARCHAR2)
    RETURN VARCHAR2 IS
    V_TOT_CREDIT NUMBER(7, 3);
    V_TOT_BILL   NUMBER(7, 3);
    V_TOT_CEU    NUMBER(7, 3);
    lcur_levl    VARCHAR2(10);
    V_MAX_HRS    SFRMHRS.SFRMHRS_MAX_HRS%TYPE;
    V_CREDIT_HRS NUMBER(7, 3);
  BEGIN
    BEGIN

      sfksels.p_get_total_hours(tot_cred_hr_in_out => V_TOT_CREDIT,
                                tot_bill_hr_in_out => V_TOT_BILL,
                                tot_ceu_hr_in_out  => V_TOT_CEU,
                                term_in            => P_TERM,
                                pidm_in            => P_PIDM);

      lcur_levl := sokccur.f_curriculum_value(p_pidm      => P_PIDM,
                                              p_lmod_code => sb_curriculum_str.f_learner,
                                              p_term_code => P_TERM,
                                              p_key_seqno => 99,
                                              p_eff_term  => P_TERM,
                                              p_order     => 1,
                                              p_field     => 'LEVL');
      V_MAX_HRS := sfksels.f_get_max_hours(term_in => P_TERM,
                                           levl_in => lcur_levl);
    EXCEPTION
      WHEN OTHERS THEN
        RETURN 'ERROR' || SQLERRM;
    END;
    BEGIN
      SELECT A.SSBSECT_CREDIT_HRS
        INTO V_CREDIT_HRS
        FROM SSBSECT A
       WHERE A.SSBSECT_TERM_CODE = P_TERM
         AND A.SSBSECT_CRN = P_CRN;
      IF (V_TOT_CREDIT + V_CREDIT_HRS) > V_MAX_HRS THEN
        RETURN 'N';
      END IF;
      RETURN 'Y';
    EXCEPTION
      WHEN OTHERS THEN
        RETURN 'ERROR';
    END;
  END;
  --
  FUNCTION FZ_GROUP_CHECK(P_PIDM NUMBER, P_TERM VARCHAR2, P_CRN VARCHAR2)
    RETURN VARCHAR2 IS
    V_ERROR VARCHAR2(200);
    V_MHRS  VARCHAR2(10);
  BEGIN
    V_ERROR := FZ_DUPL_CHECK(P_CRN       => P_CRN,
                             P_TERM_CODE => P_TERM,
                             P_PIDM      => P_PIDM);
    /* V_ERROR := V_ERROR || FZ_TIME_CHECK(P_CRN       => P_CRN,
    P_TERM_CODE => P_TERM,
    P_PIDM      => P_PIDM);*/
    V_MHRS := FZ_MHRS_CHECK(P_PIDM => P_PIDM,
                            P_TERM => P_TERM,
                            P_CRN  => P_CRN);
    IF V_MHRS = 'N' THEN
      V_ERROR := 'MAX CRED HOURS';
    END IF;
    RETURN V_ERROR;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'ERROR';
  END;
  --
  FUNCTION FZ_ME_CORQ_CHECK(P_PIDM NUMBER,
                            P_CRN  IN NUMBER,
                            P_TERM IN NUMBER) RETURN VARCHAR2 IS
    V_SUBJ_CORQ VARCHAR2(100);
    V_CRSE_CORQ VARCHAR2(100);
    V_CRSE      VARCHAR2(40);
    V_SUBJ      VARCHAR2(40);
    V_CORQ_CRN  NUMBER;
    v_corq_reg  VARCHAR2(1);
    CURSOR C_CORQ(P_SUBJ IN VARCHAR2, P_CRSE IN VARCHAR2, P_TERM IN NUMBER) IS
      WITH W_TERM_CODE_CORQ AS
       (SELECT MAX(A.SCRCORQ_EFF_TERM) TERM_CODE
          FROM SCRCORQ A
         WHERE A.SCRCORQ_SUBJ_CODE = P_SUBJ
           AND A.SCRCORQ_CRSE_NUMB = P_CRSE
           AND A.SCRCORQ_EFF_TERM <= P_TERM)
      select SCRCORQ_SUBJ_CODE, SCRCORQ_CRSE_NUMB_CORQ
        from scrcorq
       WHERE SCRCORQ_SUBJ_CODE = P_SUBJ
         AND SCRCORQ_CRSE_NUMB = P_CRSE
         AND SCRCORQ_EFF_TERM = (SELECT TERM_CODE FROM W_TERM_CODE_CORQ);

  BEGIN
    --Get the Course and the Subject from CRN
    BEGIN
      SELECT SSBSECT_CRSE_NUMB, SSBSECT_SUBJ_CODE
        INTO V_CRSE, V_SUBJ
        FROM SSBSECT
       WHERE SSBSECT_CRN = P_CRN
         AND SSBSECT_TERM_CODE = P_TERM;
    EXCEPTION
      WHEN OTHERS THEN
        RETURN 'N' || 'No CRN and TERM Combination.';
    END;
    OPEN C_CORQ(V_SUBJ, V_CRSE, P_TERM);
    FETCH C_CORQ
      INTO V_SUBJ_CORQ, V_CRSE_CORQ;
    IF SQL%NOTFOUND THEN
      return 'Y' || 'Co-Requisite Not Existed.';
    END IF;
    CLOSE C_CORQ;
    BEGIN
      SELECT SSBSECT_CRN
        INTO V_CORQ_CRN
        FROM SSBSECT b
       WHERE b.SSBSECT_SUBJ_CODE = V_SUBJ_CORQ
         AND b.SSBSECT_TERM_CODE = P_TERM
         AND b.SSBSECT_CRSE_NUMB = V_CRSE_CORQ
         AND b.SSBSECT_SSTS_CODE = 'A'
         and b.ssbsect_activity_date =
             (select max(ssbsect_activity_date)
                from ssbsect c
               where c.ssbsect_subj_code = b.ssbsect_subj_code
                 and c.ssbsect_crse_numb = b.ssbsect_crse_numb
                 and c.ssbsect_ssts_code = b.ssbsect_ssts_code
                 and c.ssbsect_term_code = b.ssbsect_term_code);
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    BEGIN
      SELECT 'Y'
        into v_corq_reg
        FROM DUAL
       WHERE EXISTS (SELECT 1
                FROM SFRSTCR Y
               WHERE y.sfrstcr_pidm = p_pidm
                 and Y.SFRSTCR_CRN = V_CORQ_CRN
                 AND y.sFRSTCR_TERM_CODE = P_TERM);
    EXCEPTION
      WHEN OTHERS THEN
        v_corq_reg := 'N';
    END;

    if v_corq_reg = 'N' then
      RETURN 'N' || 'You must Register the Course ' || V_CRSE_CORQ || ' and the Subject ' || V_CRSE_CORQ;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'N';
  END FZ_ME_CORQ_CHECK;
  --
  --Pre-Requisites Checking is done here
  function FZ_PREREQ_CHECK(P_CRN                 IN NUMBER,
                           P_TERM_CODE           IN NUMBER,
                           P_STUDENT_ID          IN VARCHAR2,
                           P_ERROR_MSG           OUT VARCHAR2,
                           a_failed_prereqs_subj OUT t_array_type,
                           a_failed_prereqs_crse OUT t_array_type,
                           a_failed_prereq_desc  out t_array_type)
    RETURN VARCHAR2 is
    r_check_this_prereq   varchar2(1);
    a_crse_prereqs        t_array_type := t_array_type();
    a_subj_prereqs        t_array_type := t_array_type();
    a_desc_prereqs        t_array_type := t_array_type();
    a_success_prereq_subj t_array_type := t_array_type();
    a_success_prereq_crse t_array_type := t_array_type();
    a_success_prereq_desc t_array_type := t_array_type();
    /* a_failed_prereqs_subj t_array_type := t_array_type();
    a_failed_prereqs_crse t_array_type := t_array_type();*/
    v_pidm        number;
    v_failures    number := 0;
    v_success_ind varchar2(1);
    cursor c_get_all_prereqs is
      select ssrrtst_subj_code_preq subj_preq,
             ssrrtst_crse_numb_preq crse_preq,
             stvsubj_desc           subj_desc
        from ssrrtst, stvsubj
       where ssrrtst_subj_code_preq = stvsubj_code
         and ssrrtst_crn = p_crn
         and ssrrtst_term_code = p_term_code
         and (ssrrtst_subj_code_preq is not null)
         and (ssrrtst_crse_numb_preq is not null); --check whether the student has taken the pre-requisite course
    cursor c_check_this_prereq(v_pidm      in number,
                               p_term_code in number,
                               p_subj_code in varchar2,
                               p_crse_numb in number) is
      select 'Y' success_ind
        from sfrstcr a
        join ssbsect b
          on a.sfrstcr_crn = b.ssbsect_crn
         and a.sfrstcr_term_code = b.ssbsect_term_code
       where a.sfrstcr_pidm = v_pidm
         and a.sfrstcr_term_code = p_term_code
         and b.ssbsect_subj_code = p_subj_code
         and b.ssbsect_crse_numb = p_crse_numb;
  begin
    a_crse_prereqs        := t_array_type();
    a_subj_prereqs        := t_array_type();
    a_desc_prereqs        := t_array_type();
    a_success_prereq_subj := t_array_type();
    a_success_prereq_crse := t_array_type();
    a_success_prereq_desc := t_array_type();
    a_failed_prereqs_subj := t_array_type();
    a_failed_prereqs_crse := t_array_type();
    a_failed_prereq_desc  := t_array_type();
    begin
      --check whether the student existed in banner.
      select baninst1.gb_common.f_get_pidm(p_student_id)
        into v_pidm
        from dual;
    exception
      when others then
        p_error_msg := 'STUDENT NOT EXISTED IN BANNER.';
        GOTO QUIT_FROM_PROGRAM;
    end;
    if p_error_msg is null then
      --collect the pre-requisite courses for the selected course.
      for r_single_prereq in c_get_all_prereqs loop
        a_subj_prereqs.extend;
        a_crse_prereqs.extend;
        a_desc_prereqs.extend;
        a_subj_prereqs(a_subj_prereqs.count) := r_single_prereq.subj_preq;
        a_crse_prereqs(a_crse_prereqs.count) := r_single_prereq.crse_preq;
        a_desc_prereqs(a_desc_prereqs.count) := r_single_prereq.subj_desc;
        --check whether the pre-requisite course is taken by the student
        v_success_ind := 'N';
        for r_check_this_prereq in c_check_this_prereq(v_pidm,
                                                       p_term_code,
                                                       r_single_prereq.subj_preq,
                                                       r_single_prereq.crse_preq) loop
          a_success_prereq_subj.extend;
          a_success_prereq_crse.extend;
          a_success_prereq_desc.extend;
          a_success_prereq_subj(a_success_prereq_subj.count) := r_single_prereq.subj_preq;
          a_success_prereq_crse(a_success_prereq_crse.count) := r_single_prereq.crse_preq;
          a_success_prereq_desc(a_success_prereq_desc.count) := r_single_prereq.subj_desc;
          v_success_ind := 'Y';
        end loop;
        --if the pre-requisite course is not taken , then collect the related details here for displaying purpose(if in case).
        if v_success_ind = 'N' then
          a_failed_prereqs_subj.extend;
          a_failed_prereqs_crse.extend;
          a_failed_prereq_desc.extend;
          a_failed_prereqs_subj(a_failed_prereqs_subj.count) := r_single_prereq.subj_preq;
          a_failed_prereqs_crse(a_failed_prereqs_crse.count) := r_single_prereq.crse_preq;
          a_failed_prereq_desc(a_failed_prereq_desc.count) := r_single_prereq.subj_desc;
          v_failures := v_failures + 1;
        end if;
      end loop;
      for i in 1 .. a_failed_prereqs_subj.count loop
        dbms_output.put_line(a_failed_prereqs_subj(i) || ' ' ||
                             a_failed_prereqs_crse(i) || ' ' ||
                             a_failed_prereq_desc(i));
      end loop;
      --dbms_output.put_line('Success Count : ' || a_success_prereq_subj.count);
      for i in 1 .. a_success_prereq_subj.count loop
        dbms_output.put_line(a_success_prereq_subj(i) || ' ' ||
                             a_success_prereq_crse(i) || ' ' ||
                             a_success_prereq_desc(i));
      end loop;
      if v_failures <> 0 then
        return 'N';
      else
        return 'Y';
      end if;
    end if;
    <<QUIT_FROM_PROGRAM>>
    return '0';
  exception
    when others then
      p_error_msg := 'PRE-REQUISITES CHECK FAILED BECAUSE OF ' || sqlerrm;
      return '0';
  end fz_prereq_check;
  /*
    --function to check alternate pin
   FUNCTION F_ALT_PIN
  ( pidm IN saturn.spriden.spriden_pidm%TYPE,
    term_code in saturn.stvterm.stvterm_code%type) RETURN VARCHAR2 AS
  \*----------------------------------------------------------------------------
  -- Developer: Nathan L. Roberts
  -- Date: 29-MAY-2012
  -- Purpose: Determine if Alternate PIN Registration process is being utlized
  --          and then handle the student's registration capabilities appropriately.
  -----------------------------------------------------------------------------*\
    REG_PIN_SETUP varchar2(1);
    STUDENT_pin_exist varchar2(6);
    PIN_ACTIVE VARCHAR2(1);
    COLL_INCL_EXCL_IND VARCHAR2(1);
    DEGR_INCL_EXCL_IND VARCHAR2(1);
    DEPT_INCL_EXCL_IND VARCHAR2(1);
    CMPS_INCL_EXCL_IND VARCHAR2(1);
    CLS_INCL_EXCL_IND VARCHAR2(1);
    MAJR_INCL_EXCL_IND VARCHAR2(1);
    END_DATE_EXIST VARCHAR2(1);
    EARN_HRS_BEGIN NUMBER(11,3);
    EARN_HRS_END NUMBER(11,3);
    STUD_EARNED_HOURS NUMBER(11,3);
    RESULT varchar2(1); -- if the value of Y is returned than registration may proceed
                          -- and if value of N or NULL is returned than registration should not be allowed
  BEGIN

  -- Documentation exists within Banner Student Self-Service User Guide 8.1.1 beginning in section 6-36 on page 266 out of 578 pages.

     SELECT NVL((SELECT GTVSDAX_EXTERNAL_CODE
                   FROM GTVSDAX
                   WHERE GTVSDAX_INTERNAL_CODE = 'WEBALTPINA'),'N')
     INTO REG_PIN_SETUP
     FROM DUAL;

     -- IF PIN PROCESS IS NOT UTILIZIED THEN REGISTRATION MAY PROCEED WITHOUT FURTHER CHECKS
     IF REG_PIN_SETUP = 'N' THEN -- 8

           -- REGISTRATION MAY PROCEED SINCE THE ALTERNATE PIN PROCESS FOR REGISTRATION IS NOT BE UTILIZED
           RESULT := 'Y';

     -- IF PIN PROCESS IS UTILIZIED THEN FURTHER CHECKS MUST OCCUR PRIOR TO REGISTRATION
     ELSIF REG_PIN_SETUP = 'Y' THEN


           -- QUERY CHECKS TO SEE IF THE STUDENT HAS A PIN IN THE PARTICULAR TERM CODE
           SELECT NVL((select SPRAPIN_PIN
                       from SPRAPIN
                       where pidm = sprapin_pidm
                       and sprapin_term_code = term_code
                       ),'STOP')
           INTO STUDENT_pin_exist
           FROM DUAL;

           -- IF THE STUDENT HAS A PIN THEN FURTHER CHECKS MUST OCCUR TO VALIDATE IF THEY CAN BEGIN REGISTERING FOR COURSES IN THE GIVEN TERM
           IF STUDENT_pin_exist <> 'STOP' then -- 7

               SELECT CASE WHEN SFRCTRL_END_DATE IS NULL THEN 'Y' ELSE 'N' END
               INTO END_DATE_EXIST
               FROM SFRCTRL
               WHERE SFRCTRL_TERM_CODE_HOST = TERM_CODE
               AND STUDENT_PIN_EXIST BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END;

                  IF END_DATE_EXIST = 'Y' THEN -- A

                       -- DETERMINES WHETHER OR NOT THE PIN ASSIGNED TO THE STUDENT IN THE GIVEN TERM ALLOWS REGISTRATION
                       SELECT NVL((SELECT 'Y'
                                     FROM SFRCTRL
                                     WHERE SFRCTRL_TERM_CODE_HOST = TERM_CODE
                                     AND STUDENT_PIN_EXIST BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END
                                     AND SYSDATE > SUBSTR((SELECT trunc(SFRCTRL_BEGIN_DATE)
                                                                    FROM SFRCTRL
                                                                    WHERE SFRCTRL_TERM_CODE_HOST = term_code
                                                                    AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),1,11)
                                                                           ||' '||
                                                                         SUBSTR(TO_DATE((SELECT SUBSTR(LPAD(NVL(SFRCTRL_HOUR_BEGIN,'0000'),4,'0'),1,2)||SUBSTR(LPAD(NVL(SFRCTRL_HOUR_BEGIN,'0000'),4,'0'),3,2)
                                                                                         FROM SFRCTRL
                                                                                         WHERE SFRCTRL_TERM_CODE_HOST = term_code
                                                                                         AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),'HH24:MI'),13,11)),'N')
                       INTO PIN_ACTIVE
                       FROM DUAL;

                  ELSIF END_DATE_EXIST = 'N' THEN

                       -- DETERMINES WHETHER OR NOT THE PIN ASSIGNED TO THE STUDENT IN THE GIVEN TERM ALLOWS REGISTRATION
                       SELECT NVL((SELECT 'Y'
                                     FROM SFRCTRL
                                     WHERE SFRCTRL_TERM_CODE_HOST = TERM_CODE
                                     AND STUDENT_PIN_EXIST BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END
                                     AND SYSDATE BETWEEN SUBSTR((SELECT trunc(SFRCTRL_BEGIN_DATE)
                                                                    FROM SFRCTRL
                                                                    WHERE SFRCTRL_TERM_CODE_HOST = term_code
                                                                    AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),1,11)
                                                                           ||' '||
                                                                         SUBSTR(TO_DATE((SELECT SUBSTR(LPAD(NVL(SFRCTRL_HOUR_BEGIN,'0000'),4,'0'),1,2)||SUBSTR(LPAD(NVL(SFRCTRL_HOUR_BEGIN,'0000'),4,'0'),3,2)
                                                                                         FROM SFRCTRL
                                                                                         WHERE SFRCTRL_TERM_CODE_HOST = term_code
                                                                                         AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),'HH24:MI'),13,11)
                                                  AND
                                                           SUBSTR((SELECT trunc(SFRCTRL_END_DATE)
                                                                   FROM SFRCTRL
                                                                   WHERE SFRCTRL_TERM_CODE_HOST = term_code
                                                                   AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),1,11)
                                                                           ||' '||
                                                                         SUBSTR(TO_DATE((SELECT SUBSTR(LPAD(NVL(SFRCTRL_HOUR_END,'0000'),4,'0'),1,2)||SUBSTR(LPAD(NVL(SFRCTRL_HOUR_END,'0000'),4,'0'),3,2)
                                                                                         FROM SFRCTRL
                                                                                         WHERE SFRCTRL_TERM_CODE_HOST = term_code
                                                                                         AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END),'HH24:MI'),13,11)),'N')
                       INTO PIN_ACTIVE
                       FROM DUAL;

                  END IF; -- A

              IF PIN_ACTIVE = 'N' THEN -- 6

                 RESULT := 'N';

              ELSIF PIN_ACTIVE ='Y' THEN


                  SELECT NVL(SFRCTRL_EARN_HRS_BEGIN,0),
                          NVL(SFRCTRL_EARN_HRS_END,0)
                  INTO EARN_HRS_BEGIN,
                        EARN_HRS_END
                  FROM SFRCTRL
                  WHERE SFRCTRL_TERM_CODE_HOST = TERM_CODE
                  AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END;


                 -- ONLY CALCULATING EARNED HOURS FROM THE ATTENDING INSTITUTION AND NOT THE TRANSFERED HOURS
                SELECT NVL((SELECT SUM(SHRTGPA_HOURS_EARNED)
                             FROM SHRTGPA
                             WHERE SHRTGPA_TERM_CODE = TERM_CODE
                             AND SHRTGPA_GPA_TYPE_IND = ('I')
                             AND SHRTGPA_PIDM = PIDM
                             AND SHRTGPA_LEVL_CODE = (SELECT A.SGBSTDN_LEVL_CODE
                                                      FROM SGBSTDN A
                                                      WHERE A.SGBSTDN_PIDM = PIDM
                                                      AND  A.SGBSTDN_TERM_CODE_EFF = (SELECT MAX(B.SGBSTDN_TERM_CODE_EFF)
                                                                                     FROM SGBSTDN B
                                                                                     WHERE B.SGBSTDN_PIDM = A.SGBSTDN_PIDM
                                                                                     AND   B.SGBSTDN_TERM_CODE_EFF <= TERM_CODE))),0)
                INTO STUD_EARNED_HOURS
                FROM DUAL;

            IF STUD_EARNED_HOURS >= EARN_HRS_BEGIN -- 13
               AND STUD_EARNED_HOURS <= EARN_HRS_END THEN


                -- LAST NAME START
                IF mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LAST_NAM_START','I') = 'Y' -- 5
                   AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LAST_NAM_END','I') = 'Y'
                   THEN

                   -- Verify that the student type code and level code criteria is not in conflict
                   IF mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'STUD_TYPE_1','I') = 'Y'  -- 4
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'STUD_TYPE_2','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'STUD_TYPE_3','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'STUD_TYPE_4','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'STUD_TYPE_5','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LEVL_1','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LEVL_2','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LEVL_3','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LEVL_4','I') = 'Y'
                      AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'LEVL_5','I') = 'Y' THEN

                      SELECT SFRCTRL_COLL_INCL_EXCL,
                              SFRCTRL_DEGR_INCL_EXCL,
                              SFRCTRL_DEPT_INCL_EXCL,
                              SFRCTRL_CMPS_INCL_EXCL,
                              SFRCTRL_CLS_INCL_EXCL,
                              SFRCTRL_MAJR_INCL_EXCL
                      INTO COLL_INCL_EXCL_IND,
                            DEGR_INCL_EXCL_IND,
                            DEPT_INCL_EXCL_IND,
                            CMPS_INCL_EXCL_IND,
                            CLS_INCL_EXCL_IND,
                            MAJR_INCL_EXCL_IND
                      FROM SFRCTRL
                      WHERE SFRCTRL_TERM_CODE_HOST = TERM_CODE
                      AND STUDENT_pin_exist BETWEEN SFRCTRL_PIN_START AND SFRCTRL_PIN_END;

                      -- CHECKS TO MAKE SURE COLLEGE CODE CRITERIA IS NOT IN CONFLICT
                      IF (COLL_INCL_EXCL_IND = 'I' -- 2
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_1','I') = 'Y'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_2','I') = 'Y'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_3','I') = 'Y'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_4','I') = 'Y'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_5','I') = 'Y')
                          OR
                         (COLL_INCL_EXCL_IND = 'E' -- 2
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_1','E') = 'N'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_2','E') = 'N'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_3','E') = 'N'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_4','E') = 'N'
                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'COLL_5','E') = 'N') THEN

                              -- CHECKS TO MAKE SURE DEGREE CODE CRITERIA IS NOT IN CONFLICT
                              IF (DEGR_INCL_EXCL_IND = 'I' -- 1
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_1','I') = 'Y'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_2','I') = 'Y'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_3','I') = 'Y'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_4','I') = 'Y'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_5','I') = 'Y')
                                  OR
                                 (DEGR_INCL_EXCL_IND = 'E'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_1','E') = 'N'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_2','E') = 'N'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_3','E') = 'N'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_4','E') = 'N'
                                  AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEGR_5','E') = 'N') THEN

                                      -- CHECKS TO MAKE SURE DEPARTMENT CODE CRITERIA IS NOT IN CONFLICT
                                      IF (DEPT_INCL_EXCL_IND = 'I' -- 9
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_1','I') = 'Y'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_2','I') = 'Y'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_3','I') = 'Y'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_4','I') = 'Y'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_5','I') = 'Y')
                                          OR
                                          (DEPT_INCL_EXCL_IND = 'E'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_1','E') = 'N'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_2','E') = 'N'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_3','E') = 'N'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_4','E') = 'N'
                                          AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'DEPT_5','E') = 'N') THEN

                                          -- CHECKS TO MAKE SURE CAMPUS CODE CRITERIA IS NOT IN CONFLICT
                                          IF (CMPS_INCL_EXCL_IND = 'I' -- 10
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_1','I') = 'Y'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_2','I') = 'Y'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_3','I') = 'Y'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_4','I') = 'Y'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_5','I') = 'Y')
                                              OR
                                              (CMPS_INCL_EXCL_IND = 'E'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_1','E') = 'N'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_2','E') = 'N'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_3','E') = 'N'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_4','E') = 'N'
                                              AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CMPS_5','E') = 'N') THEN

                                                -- CHECKS TO MAKE SURE CLASS CALCULATION CODE CRITERIA IS NOT IN CONFLICT
                                                IF (CLS_INCL_EXCL_IND = 'I' -- 11
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_1','I') = 'Y'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_2','I') = 'Y'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_3','I') = 'Y'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_4','I') = 'Y'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_5','I') = 'Y')
                                                    OR
                                                    (CLS_INCL_EXCL_IND = 'E'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_1','E') = 'N'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_2','E') = 'N'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_3','E') = 'N'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_4','E') = 'N'
                                                    AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'CLS_5','E') = 'N') THEN

                                                    -- CHECKS TO MAKE SURE MAJOR CODE CRITERIA IS NOT IN CONFLICT
                                                    IF (MAJR_INCL_EXCL_IND = 'I' -- 12
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_1','I') = 'Y'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_2','I') = 'Y'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_3','I') = 'Y'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_4','I') = 'Y'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_5','I') = 'Y')
                                                        OR
                                                        (MAJR_INCL_EXCL_IND = 'E'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_1','E') = 'N'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_2','E') = 'N'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_3','E') = 'N'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_4','E') = 'N'
                                                        AND mobedu.F_PIN_CTRL(STUDENT_pin_exist,term_code,pidm,'MAJR_5','E') = 'N') THEN

                                                       RESULT := 'Y';

                                                    ELSE

                                                         -- REGISTRATION IS NOT ALLOWED FOR THIS STUDENT
                                                         RESULT := 'N';

                                                    END IF; -- 12

                                                ELSE

                                                     -- REGISTRATION IS NOT ALLOWED FOR THIS STUDENT
                                                     RESULT := 'N';

                                                END IF; -- 11

                                              ELSE

                                                   -- REGISTRATION IS NOT ALLOWED FOR THIS STUDENT
                                                   RESULT := 'N';

                                              END IF; -- 10

                                      ELSE

                                           -- REGISTRATION IS NOT ALLOWED FOR THIS STUDENT
                                           RESULT := 'N';

                                      END IF; -- 9

                              ELSE

                                -- REGISTRATION IS NOT ALLOWED FOR THIS STUDENT
                                RESULT := 'N';

                              END IF; -- 1

                      ELSE

                          -- REGISTRATION IS NOT ALLOWED FOR THIS STUDENT
                          RESULT := 'N';

                      END IF; -- 2

                   ELSE

                      -- registration may not proceed
                      RESULT := 'N';

                   END IF; -- 4

                ELSE

                 RESULT := 'N';

                END IF; -- 5

             ELSE

                 RESULT := 'N';

             END IF; -- 13

            END IF; -- 6

           -- SINCE PIN PROCESS IS NOT SETUP FOR THE STUDENT THEN REGISTRATION MAY PROCEED
           ELSIF STUDENT_pin_exist = 'STOP' then

             -- REGISTRATION MAY PROCEED FOR THE STUDENT SINCE THE ALTERNATE PIN
             -- PROCESS FOR REGISTRATION IS NOT BE UTILIZED WITH HIM/HER IN THE GIVEN TERM
             RESULT := 'Y';

           END IF; -- 7

     END IF; -- 8

     RETURN RESULT;
  END F_ALT_PIN;

  */
  --function to get meeting number,room code,start date and end date.
  FUNCTION fz_ssrmeetvalues(p_term     IN VARCHAR2,
                            p_crn      IN VARCHAR2,
                            p_position IN NUMBER,
                            p_ind      IN VARCHAR2) RETURN VARCHAR2 IS

    gv_ssrmeet_tab ssksels.gt_ssrmeet;
    ctr            NUMBER := 0;

    CURSOR ssrmeet_c(p_term ssrmeet.ssrmeet_term_code%TYPE,
                     p_crn  ssrmeet.ssrmeet_crn%TYPE) IS
      SELECT *
        FROM SSRMEET
       WHERE ssrmeet_term_code = p_term
         AND ssrmeet_crn = p_crn;

  BEGIN

    IF gv_ssrmeet_tab.COUNT = 0 OR gv_ssrmeet_tab(1)
      .ssrmeet_term_code <> p_term OR gv_ssrmeet_tab(1)
      .ssrmeet_crn <> p_crn THEN
      gv_ssrmeet_tab.DELETE;
      OPEN ssrmeet_c(p_term, p_crn);
      FETCH ssrmeet_c BULK COLLECT
        INTO gv_ssrmeet_tab;
      CLOSE ssrmeet_c;
    END IF;

    FOR i IN 1 .. gv_ssrmeet_tab.COUNT LOOP
      --IF gv_ssrmeet_tab(i).ssrmeet_crn = p_crn THEN

      ctr := ctr + 1;

      IF ctr >= P_position THEN
        CASE p_ind
          WHEN 'RM' THEN
            RETURN gv_ssrmeet_tab(i).ssrmeet_room_code;
          WHEN 'MN' THEN
            RETURN gv_ssrmeet_tab(i).ssrmeet_meet_no;
          WHEN 'SD' THEN
            RETURN gv_ssrmeet_tab(i).ssrmeet_start_date;
          WHEN 'ED' THEN
            RETURN gv_ssrmeet_tab(i).ssrmeet_end_date;
          ELSE
            RETURN('');
        END CASE;
      END IF;
      --END IF;
    END LOOP;

    RETURN NULL;
  END fz_ssrmeetvalues;
  function FZ_GET_TERM(P_CREATE_DATE varchar2) return varchar2 is
    result varchar2(8);
    cursor CURRENTTERM_C is
      select V1.STVTERM_CODE       TERMCODE,
             V1.STVTERM_START_DATE STARTDATE,
             V1.STVTERM_END_DATE   ENDDATE
        from STVTERM V1
       where V1.STVTERM_CODE not like '9%'
         and V1.STVTERM_CODE not like '5%'
         and UPPER(STVTERM_DESC) not like 'LEGACY%';
  begin
    for CUR_CURRENTTERM in CURRENTTERM_C loop
      exit when CURRENTTERM_C%notfound;
      if to_date(P_CREATE_DATE, 'MM/DD/YYYY') between
         CUR_CURRENTTERM.STARTDATE and CUR_CURRENTTERM.ENDDATE and
         CUR_CURRENTTERM.TERMCODE is not null then
        DBMS_OUTPUT.PUT_LINE(CUR_CURRENTTERM.TERMCODE);
        result := CUR_CURRENTTERM.TERMCODE;
      elsif to_date(P_CREATE_DATE, 'MM/DD/YYYY') + 45 between
            CUR_CURRENTTERM.STARTDATE and CUR_CURRENTTERM.ENDDATE then
        DBMS_OUTPUT.PUT_LINE(CUR_CURRENTTERM.TERMCODE);
        result := CUR_CURRENTTERM.TERMCODE;
      end if;
    end loop;
    return(result);
  exception
    when others then
      return SUBSTR('ERR - in FZ_GET_TERM; ' || sqlerrm, 1, 200);
  end;
  PROCEDURE PZ_REGISTRATION_CHECKS(P_STUDENT_ID IN VARCHAR2,
                                   P_TERM       IN varchar2,
                                   P_ERROR_MSG  OUT VARCHAR2) IS

    V_PIDM        NUMBER;
    v_pin         varchar2(6);
    v_msg         varchar2(200);
    v_success_msg varchar2(2);
    pin_check     varchar2(2);
    p_error       varchar2(100);
    V_ESTS_CODE   STVESTS.STVESTS_CODE%TYPE;
    V_ESTS_CHK    VARCHAR2(1) := 'N';
    --  V_CORQS VARCHAR2(100);
    V_SOBTERM     VARCHAR2(10);
  BEGIN
    --did he/she applied for the requested term.
    BEGIN
      SELECT BANINST1.GB_COMMON.F_GET_PIDM(P_STUDENT_ID)
        INTO V_PIDM
        FROM DUAL;
      DBMS_OUTPUT.PUT_LINE('pidm' || ' ' || V_PIDM);
    EXCEPTION
      WHEN OTHERS THEN
        P_ERROR_MSG := 'NO STUDENT ID IN BANNER.';
    END;
    BEGIN

    SELECT SOBTERM_HOLD_SEVERITY INTO V_SOBTERM FROM SOBTERM
      WHERE SOBTERM_TERM_CODE=P_TERM;

      EXCEPTION WHEN OTHERS THEN
        NULL;
      END;
    -----
    begin
      select SPRAPIN_PIN
        into v_pin
        from SPRAPIN
       where V_PIDM = sprapin_pidm
         and sprapin_term_code = P_TERM;
    exception
      when no_data_found then
        v_pin := 'N';
      when others then
        P_ERROR_MSG := 'Pin doesnot exists' || sqlerrm;
    end;
    -----
    IF P_ERROR_MSG IS NOT NULL THEN
      GOTO QUIT_FROM_PROGRAM;
    END IF;
    DBMS_OUTPUT.PUT_LINE('term' || ' ' || P_TERM);
    begin
      DBMS_OUTPUT.PUT_LINE('In begin..');
      IF 'N' = ME_VALID.FZ_ME_VAL_ADMS(V_PIDM, P_TERM) THEN
        P_ERROR_MSG := 'HAVE NOT APPLIED FOR THIS TERM.';
        --check whether the student is an active student or not.

      ELSIF 'N' = ME_VALID.FZ_ME_VAL_STUDENT(V_PIDM, P_TERM) THEN
        P_ERROR_MSG := 'NOT AN ACTIVE STUDENT IN THIS TERM.';

        --check whether the student has any holds in previous terms.

      ELSIF 'Y' = GB_HOLD.F_ANY_HOLD_EXISTS(V_PIDM) AND V_SOBTERM='F' THEN

        P_ERROR_MSG := 'HOLDS EXISTS.';

        --check whether the student is enrolled and is eligible to register.

      /*ELSIF 'N' = SB_ENROLLMENT.F_ELIGIBLE_TO_REGISTER(V_PIDM, P_TERM) THEN
        P_ERROR_MSG := 'Please contact the Admissions office to verify your status.';*/

        /*ELSIF nvl(MOBEDU.f_alt_pin(V_PIDM, P_TERM), 'N') = 'N' THEN
        P_ERROR_MSG := 'ALTERNATE PIN VALIDATION FAILED';*/
      ELSE
        /* me_alt_pin_pkg.pz_check_alt_pin_req(P_STUDENT_ID,
        P_TERM,
        pin_check,
        p_error);*/
        dbms_output.put_line('pin_check' || pin_check);
        /* if pin_check = 'Y' then*/
        ME_ALT_PIN_PKG.PZ_TIME_TICKET_CONTROL(P_STUDENT_ID,
                                                     P_TERM,
                                                     NULL,
                                                     v_msg,
                                                     v_success_msg);
        if v_success_msg = 'N' then
          P_ERROR_MSG := v_msg;
        end if;
        /* end if;*/
      END IF;
    exception
      when no_data_found then
        P_ERROR_MSG := 'Exception' || sqlerrm;
        DBMS_OUTPUT.PUT_LINE('out5' || P_ERROR_MSG);
     /* when others then
        DBMS_OUTPUT.PUT_LINE('out5' ||sqlerrm);*/
    end;
    --- SFBETRM VALIDATION

    BEGIN
      SELECT sfbetrm_ests_code
        INTO V_ESTS_CODE
        FROM SFBETRM
       WHERE SFBETRM_TERM_CODE = P_TERM
         AND SFBETRM_PIDM = V_PIDM;

    EXCEPTION
      WHEN OTHERS THEN
        V_ESTS_CODE := 'EL';
    END;
    BEGIN
      SELECT 'Y'
        INTO V_ESTS_CHK
        FROM sfbests
       WHERE TRUNC(SYSDATE) BETWEEN TRUNC(sfbests_start_date) AND
             TRUNC(sfbests_end_date)
         AND sfbests_ests_code = V_ESTS_CODE
         AND sfbests_term_code = P_TERM;
          DBMS_OUTPUT.PUT_LINE('CHK   '||V_ESTS_CHK);
      IF V_ESTS_CHK <> 'Y' THEN
         DBMS_OUTPUT.PUT_LINE('Invalid or undefined Enrollment Status or date range invalid');
        P_ERROR_MSG := 'Invalid or undefined Enrollment Status or date range invalid';
      END IF;
    EXCEPTION
      WHEN OTHERS THEN
         DBMS_OUTPUT.PUT_LINE('Invalid or undefined Enrollment Status or date range invalid');
        P_ERROR_MSG := 'Invalid or undefined Enrollment Status or date range invalid';
    END;
    DBMS_OUTPUT.PUT_LINE('out6' || ' ' || 'last');
    --check whether the student is registered or not. if the student is not registered,
    --then how can he register for a course.
    <<QUIT_FROM_PROGRAM>>
    NULL;
  EXCEPTION
    WHEN no_data_found THEN
      P_ERROR_MSG := 'STUDENT REGISTRATION FAILED';
      DBMS_OUTPUT.PUT_LINE('out' || ' ' || 'last' || sqlerrm);
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('out7' || ' ' || 'last' || sqlerrm);
      P_ERROR_MSG := 'STUDENT REGISTRATION FAILED BECAUSE OF ' || SQLERRM;
  END PZ_REGISTRATION_CHECKS;

   procedure PZ_GET_TERM(P_CREATE_DATE varchar2,
                        P_TERM        OUT VARCHAR2,
                        P_TERM_DESC   OUT VARCHAR2,
                        P_ERROR_MSG   OUT VARCHAR2) is
    result varchar2(100);
    cursor CURRENTTERM_C is
      select V1.STVTERM_CODE       TERMCODE,
             V1.STVTERM_START_DATE STARTDATE,
             V1.STVTERM_END_DATE   ENDDATE,
             v1.stvterm_desc       term_desc
        from STVTERM V1
       where V1.STVTERM_CODE not like '9%'
         and V1.STVTERM_CODE not like '5%'
         and UPPER(STVTERM_DESC) not like 'LEGACY%'
         and v1.stvterm_end_date>=to_date(P_CREATE_DATE, 'MM/DD/YYYY') order by 1;
  begin
    for CUR_CURRENTTERM in CURRENTTERM_C loop
      exit when CURRENTTERM_C%notfound;
      if to_date(P_CREATE_DATE, 'MM/DD/YYYY') between
         CUR_CURRENTTERM.STARTDATE and CUR_CURRENTTERM.ENDDATE and
         CUR_CURRENTTERM.TERMCODE is not null then
        DBMS_OUTPUT.PUT_LINE(CUR_CURRENTTERM.TERMCODE);
        P_TERM      := CUR_CURRENTTERM.TERMCODE;
        P_TERM_DESC := cur_currentterm.term_desc;
      elsif to_date(P_CREATE_DATE, 'MM/DD/YYYY') + 45 between
            CUR_CURRENTTERM.STARTDATE and CUR_CURRENTTERM.ENDDATE then
        DBMS_OUTPUT.PUT_LINE(CUR_CURRENTTERM.TERMCODE);
        P_TERM      := CUR_CURRENTTERM.TERMCODE;
        P_TERM_DESC := cur_currentterm.term_desc;
      end if;
    end loop;

  exception
    when others then
      P_ERROR_MSG := SUBSTR('ERR - in FZ_GET_TERM; ' || sqlerrm, 1, 200);
      DBMS_OUTPUT.put_line(P_ERROR_MSG);
  end;

  FUNCTION FZ_CHECK_SFRRSTS(P_TERM_CODE VARCHAR2, P_CRN NUMBER,P_REG_IND VARCHAR2)
    RETURN VARCHAR2 IS
    V_PTRM     SSBSECT.SSBSECT_PTRM_CODE%TYPE;
   /* V_REG_IND  VARCHAR2(1) DEFAULT 'R';*/
    V_ICODE    VARCHAR2(100);
    V_WEB_RSTS STVRSTS.STVRSTS_CODE%TYPE;
    V_VALID    VARCHAR2(1):='N';
    v_result   BOOLEAN;
    CURSOR C_GET_GTVSDAX_CODE IS
      SELECT GTVSDAX_EXTERNAL_CODE
        FROM GTVSDAX
       WHERE GTVSDAX_INTERNAL_CODE = V_ICODE
         AND GTVSDAX_INTERNAL_CODE_GROUP = 'WEBREG';
         CURSOR ssrrstsc (term VARCHAR2, crn VARCHAR2)
      IS
         SELECT *
           FROM ssrrsts
          WHERE ssrrsts_rsts_code = SUBSTR (f_stu_getwebregsrsts ('R'), 1, 2)
            AND ssrrsts_term_code = term
            AND ssrrsts_crn = crn;
  BEGIN
    SELECT SSBSECT_PTRM_CODE
      INTO V_PTRM
      FROM SSBSECT
     WHERE SSBSECT_TERM_CODE = P_TERM_CODE
       AND SSBSECT_CRN = P_CRN;

    IF P_REG_IND = 'R' THEN
      V_ICODE := 'WEBRSTSREG';
    ELSIF P_REG_IND = 'D' THEN
      V_ICODE := 'WEBRSTSDRP';
    END IF;
    OPEN C_GET_GTVSDAX_CODE;
    FETCH C_GET_GTVSDAX_CODE
      INTO V_WEB_RSTS;
    IF C_GET_GTVSDAX_CODE%NOTFOUND THEN
      IF P_REG_IND = 'R' THEN
        V_WEB_RSTS := 'RE';
      ELSIF P_REG_IND = 'D' THEN
        V_WEB_RSTS := 'DD';
      ELSE
        V_WEB_RSTS:=P_REG_IND;
      END IF;
    END IF;

    SELECT 'Y'
    into V_VALID
      FROM sfrrsts
     WHERE sfrrsts_rsts_code = V_WEB_RSTS
       AND sfrrsts_term_code = P_TERM_CODE
       AND sfrrsts_ptrm_code = V_PTRM
       AND trunc(sysdate) BETWEEN TRUNC(sfrrsts_start_date) AND
           TRUNC(sfrrsts_end_date);

      if V_VALID = 'Y' then
      v_result := bwcksams.f_validregdate(term => P_TERM_CODE);
      if v_result then
        RETURN V_VALID;
      else
        /*dbms_output.put_line('false::: Registration is not allowed at this time');*/
        RETURN 'N';
      end if;
    end if;
    RETURN V_VALID;

  EXCEPTION WHEN OTHERS THEN
    RETURN V_VALID;
  END FZ_CHECK_SFRRSTS;


END ME_VALID;

/

