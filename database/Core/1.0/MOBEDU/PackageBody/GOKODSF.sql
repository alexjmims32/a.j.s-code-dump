PROMPT CREATE OR REPLACE PACKAGE BODY gokodsf
CREATE OR REPLACE PACKAGE BODY gokodsf AS
FUNCTION F_GET_DESC
        (INCOMING_PARAMETER_VALUE         VARCHAR2,
         WHICH_TABLE                      VARCHAR2,
         OVERRIDE_SELECT_COLUMN           VARCHAR2 DEFAULT NULL,
         FINANCE_IND                      VARCHAR2 DEFAULT 'N',
         OVERRIDE_COMPARISON_COLUMN       VARCHAR2 DEFAULT NULL,
         OVERRIDE_LENGTH                  NUMBER DEFAULT NULL,
         SECOND_PARAMETER                 VARCHAR2 DEFAULT NULL,
         SYSTEM_IND                       VARCHAR2 DEFAULT NULL)
RETURN VARCHAR2
AS
--
-- Declare local variables
--
RETURN_DESC         VARCHAR2(2000) := NULL;
HOLD_SELECT         VARCHAR2(2000) := NULL;
CURSOR_ID_NUM       INTEGER;
DBMS_SQL_FEEDBACK   INTEGER;
--
BEGIN
--
--
    IF FINANCE_IND = 'Y' THEN
        IF SECOND_PARAMETER IS NOT NULL THEN
          HOLD_SELECT := 'SELECT ' || NVL(OVERRIDE_SELECT_COLUMN,
                          WHICH_TABLE || '_DESC') ||
                         '  FROM ' || WHICH_TABLE ||
                         ' WHERE ' || WHICH_TABLE || '_COAS_CODE = :W2'
                                   || ' AND ' || NVL(OVERRIDE_COMPARISON_COLUMN,
                                       WHICH_TABLE || '_CODE') ||' = :W1'
                           || ' AND ' || WHICH_TABLE || '_EFF_DATE <= SYSDATE'
                           || ' AND (' || WHICH_TABLE || '_NCHG_DATE > SYSDATE
                                  OR ' || WHICH_TABLE || '_NCHG_DATE IS NULL)';
        ELSE
          HOLD_SELECT := 'SELECT ' || NVL(OVERRIDE_SELECT_COLUMN,
                          WHICH_TABLE || '_DESC') ||
                         '  FROM ' || WHICH_TABLE ||
                         ' WHERE ' || NVL(OVERRIDE_COMPARISON_COLUMN,
                           WHICH_TABLE || '_CODE') ||' = :W1'
                         || ' AND ' || WHICH_TABLE || '_EFF_DATE <= SYSDATE'
                         || ' AND (' || WHICH_TABLE || '_NCHG_DATE > SYSDATE
                                OR ' || WHICH_TABLE || '_NCHG_DATE IS NULL)';
        END IF;
    ELSIF SYSTEM_IND = 'HR' THEN
       HOLD_SELECT := 'SELECT ' || NVL(OVERRIDE_SELECT_COLUMN, WHICH_TABLE ||
                       '_DESC')  || '  FROM ' || WHICH_TABLE ||
                       ' WHERE ' || WHICH_TABLE || '_CODE' ||' = :W1' ||
                       ' AND ' || OVERRIDE_COMPARISON_COLUMN || ' = :W2';
    ELSE
       HOLD_SELECT := 'SELECT ' || NVL(OVERRIDE_SELECT_COLUMN, WHICH_TABLE ||
                       '_DESC')  || '  FROM ' || WHICH_TABLE ||
                       ' WHERE ' || NVL(OVERRIDE_COMPARISON_COLUMN,
                       WHICH_TABLE || '_CODE') ||' = :W1';
    END IF;
--
    IF SECOND_PARAMETER IS NOT NULL THEN
      EXECUTE IMMEDIATE HOLD_SELECT INTO RETURN_DESC USING INCOMING_PARAMETER_VALUE, SECOND_PARAMETER;
    ELSE
      EXECUTE IMMEDIATE HOLD_SELECT INTO RETURN_DESC USING INCOMING_PARAMETER_VALUE;
    END IF;
--
    RETURN RETURN_DESC;
END F_GET_DESC;

END;

/

