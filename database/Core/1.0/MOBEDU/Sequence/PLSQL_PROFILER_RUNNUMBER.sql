PROMPT CREATE SEQUENCE plsql_profiler_runnumber
CREATE SEQUENCE plsql_profiler_runnumber
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  START WITH 6
  NOCYCLE
  NOORDER
  NOCACHE
/

