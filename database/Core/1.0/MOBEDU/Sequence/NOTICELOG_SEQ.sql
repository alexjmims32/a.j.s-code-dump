PROMPT CREATE SEQUENCE noticelog_seq
CREATE SEQUENCE noticelog_seq
  MINVALUE 0
  MAXVALUE 99999999999
  INCREMENT BY 1
  START WITH 540
  NOCYCLE
  NOORDER
  CACHE 20
/

