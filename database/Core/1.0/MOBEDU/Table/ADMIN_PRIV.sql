PROMPT CREATE TABLE admin_priv
CREATE TABLE admin_priv (
  username    VARCHAR2(20)  NOT NULL,
  privcode    VARCHAR2(20)  NOT NULL,
  accessflag  CHAR(1)       DEFAULT 'Y' NOT NULL,
  description VARCHAR2(100) NOT NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


