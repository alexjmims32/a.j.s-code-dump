PROMPT CREATE TABLE mobedu_trash
CREATE TABLE mobedu_trash (
  student_id   VARCHAR2(10)   NULL,
  student_pidm NUMBER(8,0)    NULL,
  term_code    VARCHAR2(6)    NULL,
  crn          NUMBER         NULL,
  status       VARCHAR2(100)  NULL,
  error_msg    VARCHAR2(4000) NULL,
  rsts_code    VARCHAR2(10)   NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


