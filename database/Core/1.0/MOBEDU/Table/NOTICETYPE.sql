PROMPT CREATE TABLE noticetype
CREATE TABLE noticetype (
  id             NUMBER(11,0)   NOT NULL,
  category       VARCHAR2(20)   NOT NULL,
  subcategory    VARCHAR2(20)   NULL,
  title          VARCHAR2(50)   NOT NULL,
  description    VARCHAR2(100)  NOT NULL,
  priority       NUMBER(2,0)    DEFAULT 1 NOT NULL,
  msgtemplate    VARCHAR2(4000) NULL,
  lastmodifiedby VARCHAR2(50)   NOT NULL,
  lastmodifiedon TIMESTAMP(6)   NOT NULL
)
/

COMMENT ON COLUMN noticetype.id IS 'Primary Key';
COMMENT ON COLUMN noticetype.category IS 'Main categories';
COMMENT ON COLUMN noticetype.subcategory IS 'Sub categories under each category. Use NULL for main categories';
COMMENT ON COLUMN noticetype.priority IS 'Use range of 1 to 10';
COMMENT ON COLUMN noticetype.lastmodifiedby IS 'id from user table';

