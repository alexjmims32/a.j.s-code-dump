PROMPT CREATE INDEX mobedu_trash_idx
CREATE INDEX mobedu_trash_idx
  ON mobedu_trash (
    student_id,
    term_code
  )
  STORAGE (
    NEXT       1024 K
  )
/

