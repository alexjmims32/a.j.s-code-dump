PROMPT CREATE TABLE feedback
CREATE TABLE feedback (
  id          NUMBER(5,0)   NOT NULL,
  title       VARCHAR2(50)  NULL,
  email       VARCHAR2(100) NULL,
  description VARCHAR2(100) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


