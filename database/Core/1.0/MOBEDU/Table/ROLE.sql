PROMPT CREATE TABLE role
CREATE TABLE role (
  campuscode  VARCHAR2(100) NOT NULL,
  roleid      VARCHAR2(100) NOT NULL,
  role        VARCHAR2(100) NOT NULL,
  description VARCHAR2(500) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


