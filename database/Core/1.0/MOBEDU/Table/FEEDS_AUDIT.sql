PROMPT CREATE TABLE feeds_audit
CREATE TABLE feeds_audit (
  id             NUMBER(5,0)   NOT NULL,
  parentid       NUMBER(5,0)   NOT NULL,
  type           VARCHAR2(20)  NOT NULL,
  link           VARCHAR2(200) NULL,
  format         VARCHAR2(100) NULL,
  moduleid       NUMBER(5,0)   NOT NULL,
  name           VARCHAR2(100) NULL,
  icon           VARCHAR2(100) NULL,
  campuscode     VARCHAR2(6)   NULL,
  version_no     NUMBER(5,0)   NULL,
  lastmodifiedby VARCHAR2(40)  NULL,
  lastmodifiedon TIMESTAMP(6)  NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


