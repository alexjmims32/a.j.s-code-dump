PROMPT CREATE TABLE emergency_contacts
CREATE TABLE emergency_contacts (
  id             NUMBER             NOT NULL,
  campus         VARCHAR2(100)      NULL,
  name           VARCHAR2(200 BYTE) NULL,
  phone          VARCHAR2(15)       NULL,
  address        VARCHAR2(50)       NULL,
  email          VARCHAR2(30)       NULL,
  picture_url    VARCHAR2(150)      NULL,
  category       VARCHAR2(34)       NULL,
  version_no     NUMBER(5,0)        NULL,
  lastmodifiedby VARCHAR2(40)       NULL,
  lastmodifiedon TIMESTAMP(6)       NULL,
  comments       VARCHAR2(1000)     NULL,
  seq_num        NUMBER             NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


