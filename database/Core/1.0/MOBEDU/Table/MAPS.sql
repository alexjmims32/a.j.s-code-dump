PROMPT CREATE TABLE maps
CREATE TABLE maps (
  id             NUMBER         NULL,
  campus_code    VARCHAR2(6)    NULL,
  building_code  VARCHAR2(6)    NULL,
  building_name  VARCHAR2(100)  NULL,
  building_desc  VARCHAR2(2000) NULL,
  phone          VARCHAR2(15)   NULL,
  email          VARCHAR2(30)   NULL,
  img_url        VARCHAR2(2048)  NULL,
  longitude      VARCHAR2(30)   NULL,
  latitude       VARCHAR2(30)   NULL,
  version_no     NUMBER(5,0)    NULL,
  lastmodifiedby VARCHAR2(40)   NULL,
  lastmodifiedon TIMESTAMP(6)   NULL,
  address        VARCHAR2(4000) NULL,
  center_flag    VARCHAR2(10)   DEFAULT 'false' NULL,
  category       VARCHAR2(100)  NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


