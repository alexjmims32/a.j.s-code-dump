PROMPT CREATE TABLE admin_user
CREATE TABLE admin_user (
  username  VARCHAR2(20)  NOT NULL,
  firstname VARCHAR2(100) NOT NULL,
  lastname  VARCHAR2(100) NOT NULL,
  password  VARCHAR2(100) NOT NULL,
  active    CHAR(1)       DEFAULT 'Y' NOT NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


