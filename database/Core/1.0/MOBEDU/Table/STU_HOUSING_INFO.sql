PROMPT CREATE TABLE mobedu.stu_housing_info
CREATE TABLE mobedu.stu_housing_info (
  fullname           VARCHAR2(100) NOT NULL,
  classof            VARCHAR2(100) NULL,
  studenid           VARCHAR2(50)  NOT NULL,
  gender             VARCHAR2(10)  NULL,
  birthdate          DATE          NULL,
  address            VARCHAR2(500) NULL,
  city               VARCHAR2(50)  NULL,
  state              VARCHAR2(50)  NULL,
  pincode            VARCHAR2(10)  NULL,
  phone              VARCHAR2(20)  NULL,
  email              VARCHAR2(50)  NULL,
  beginterm          VARCHAR2(50)  NULL,
  beginyear          VARCHAR2(50)  NULL,
  applicationtype    VARCHAR2(50)  NULL,
  hallsizepreference VARCHAR2(50)  NULL,
  floorpreference    VARCHAR2(50)  NULL,
  issmoker           VARCHAR2(50)  NULL,
  livewithsmoker     VARCHAR2(50)  NULL,
  studyprefer        VARCHAR2(50)  NULL,
  wakeuppref         VARCHAR2(50)  NULL,
  roompref           VARCHAR2(50)  NULL,
  names              VARCHAR2(50)  NULL,
  specialneeds       VARCHAR2(100) NULL,
  mealplan           VARCHAR2(100) NULL,
  comments           VARCHAR2(500) NULL
)
/


