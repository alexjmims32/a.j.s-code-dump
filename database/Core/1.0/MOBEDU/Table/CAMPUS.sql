PROMPT CREATE TABLE campus
CREATE TABLE campus (
  code          VARCHAR2(4000) NULL,
  title         VARCHAR2(4000) NULL,
  description   VARCHAR2(4000) NULL,
  client        VARCHAR2(100)  NULL,
  principalname VARCHAR2(100)  NULL,
  website       VARCHAR2(4000) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


