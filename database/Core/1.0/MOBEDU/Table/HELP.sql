PROMPT CREATE TABLE help
CREATE TABLE help (
  campuscode     VARCHAR2(100)  NULL,
  abouttext      VARCHAR2(4000) NULL,
  version_no     NUMBER(5,0)    NULL,
  lastmodifiedby VARCHAR2(40)   NULL,
  lastmodifiedon TIMESTAMP(6)   NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


