PROMPT CREATE TABLE privilege
CREATE TABLE privilege (
  roleid         NUMBER        NOT NULL,
  modulecode     VARCHAR2(100) NOT NULL,
  accessflag     VARCHAR2(1)   NULL,
  authrequired   CHAR(1)       DEFAULT 'Y' NULL,
  lastmodifiedon TIMESTAMP(6)  NULL,
  lastmodifiedby VARCHAR2(40)  NULL,
  version_no     NUMBER(5,0)   NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


