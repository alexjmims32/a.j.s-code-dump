PROMPT CREATE TABLE noticelog
CREATE TABLE noticelog (
  id             NUMBER(11,0)   NOT NULL,
  noticeid       NUMBER(11,0)   NOT NULL,
  username       VARCHAR2(20)   NOT NULL,
  message        VARCHAR2(4000) NOT NULL,
  expirydate     DATE           NULL,
  deliverymethod VARCHAR2(20)   NULL,
  delivered      NUMBER(1,0)    DEFAULT 0 NOT NULL,
  readflag       NUMBER(1,0)    DEFAULT 0 NOT NULL,
  lastmodifiedby VARCHAR2(50)   NOT NULL,
  lastmodifiedon TIMESTAMP(6)   NULL,
  deleted        NUMBER(1,0)    DEFAULT 0 NOT NULL,
  type           VARCHAR2(100)  NULL,
  title          VARCHAR2(100)  NULL,
  duedate        DATE           NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


