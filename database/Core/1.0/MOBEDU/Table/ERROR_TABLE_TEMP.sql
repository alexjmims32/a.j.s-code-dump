PROMPT CREATE TABLE mobedu.error_table_temp
CREATE TABLE mobedu.error_table_temp (
  student_id    VARCHAR2(10)   NULL,
  crn           VARCHAR2(50)   NULL,
  term          NUMBER         NULL,
  error_msg     VARCHAR2(1000) NULL,
  activity_date DATE           NULL
)
/


