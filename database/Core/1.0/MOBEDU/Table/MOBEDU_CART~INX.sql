PROMPT CREATE INDEX mobedu_cart_idx
CREATE INDEX mobedu_cart_idx
  ON mobedu_cart (
    student_id,
    term,
    processed_ind
  )
  STORAGE (
    NEXT       1024 K
  )
/

