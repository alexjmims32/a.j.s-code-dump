PROMPT CREATE OR REPLACE VIEW account_summary_view
CREATE OR REPLACE FORCE VIEW account_summary_view (
  category,
  description,
  tbbdetc_type_ind,
  amount,
  balance,
  student_id
) AS
SELECT c.ttvdcat_code category,
       c.ttvdcat_desc description,
       tbbdetc_type_ind ,
       SUM(tbraccd_amount) amount,
       SUM(tbraccd_balance) balance,
       spriden_id student_id
  FROM tbbdetc a
  JOIN tbraccd b
    ON tbbdetc_detail_code = tbraccd_detail_code
  JOIN ttvdcat c
    ON a.tbbdetc_dcat_code = c.ttvdcat_code
  join spriden c
    on tbraccd_pidm = spriden_pidm
   and spriden_change_ind is null
--WHERE tbraccd_pidm = 40005156 --pidm
--WHERE spriden_id = 'S00018238'
 GROUP BY tbbdetc_type_ind, c.ttvdcat_code, c.ttvdcat_desc, spriden_id
 ORDER BY tbbdetc_type_ind, c.ttvdcat_code, c.ttvdcat_desc
/

