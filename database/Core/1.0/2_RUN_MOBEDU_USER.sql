PROMPT =================================================================
PROMPT      <<< Login as MOBEDU/MOBILE USER to run >>>      
PROMPT      USAGE: Run SQL Plus and input "start 2_RUN_MOBEDU_USER.sql"   
PROMPT =================================================================

spool RUN_USER_OBJECTS.log

prompt
prompt Creating Synonyms
prompt =========================
prompt
@@MOBEDU\Synonyms.sql
prompt
prompt Creating Types
prompt =========================
prompt
@@MOBEDU\Types.sql
@@MOBEDU\Type\T_ARRAY_TYPE.sql
prompt
prompt Creating Tables
prompt =========================
prompt
@@MOBEDU\Table\ADMIN_PRIV.sql
@@MOBEDU\Table\ADMIN_ROLE.sql
@@MOBEDU\Table\ADMIN_USER.sql
@@MOBEDU\Table\CAMPUS.sql
@@MOBEDU\Table\EMERGENCY_CONTACTS.sql
@@MOBEDU\Table\EMERGENCY_CONTACTS_AUDIT.sql
@@MOBEDU\Table\FAQ.sql
@@MOBEDU\Table\FEEDBACK.sql
@@MOBEDU\Table\FEEDBACK_DATA.sql
@@MOBEDU\Table\FEEDS.sql
@@MOBEDU\Table\FEEDS_AUDIT.sql
@@MOBEDU\Table\HELP.sql
@@MOBEDU\Table\HELP_AUDIT.sql
@@MOBEDU\Table\MAPS.sql
@@MOBEDU\Table\MAPS_AUDIT.sql
@@MOBEDU\Table\MOBEDU_CART.sql
@@MOBEDU\Table\MOBEDU_TRASH.sql
@@MOBEDU\Table\MODULE.sql
@@MOBEDU\Table\MODULES.sql
@@MOBEDU\Table\NOTICE.sql
@@MOBEDU\Table\NOTICELOG.sql
@@MOBEDU\Table\NOTICEPOPULATION.sql
@@MOBEDU\Table\NOTICETYPE.sql
@@MOBEDU\Table\POPULATIONTYPE.sql
@@MOBEDU\Table\PRIVILEGE.sql
@@MOBEDU\Table\PRIVILEGE_AUDIT.sql
@@MOBEDU\Table\ROLE.sql
@@MOBEDU\Table\ADMIN_ROLE~INX.sql
@@MOBEDU\Table\MOBEDU_CART~INX.sql
@@MOBEDU\Table\MOBEDU_TRASH~INX.sql
@@MOBEDU\Table\NOTICE~UNQ.sql
@@MOBEDU\Table\NOTICEPOPULATION~UNQ.sql
@@MOBEDU\Table\NOTICETYPE~UNQ.sql
@@MOBEDU\Table\POPULATIONTYPE~UNQ.sql
@@MOBEDU\Table\NOTICEPOPULATION~FK.sql
@@MOBEDU\Table\PLSQL_PROFILER_DATA~FK.sql
@@MOBEDU\Table\PLSQL_PROFILER_UNITS~FK.sql
prompt
prompt Creating Sequences
prompt =========================
prompt
@@MOBEDU\Sequence\FEEDBACK_SEQ.sql
@@MOBEDU\Sequence\NOTICELOG_SEQ.sql
@@MOBEDU\Sequence\NOTICE_SEQ.sql
prompt
prompt Creating Procedures
prompt =========================
prompt
@@MOBEDU\Procedure\PZ_ME_AUTHENTICATION.sql
prompt
prompt Creating Packages
prompt =========================
prompt
@@MOBEDU\Package\ADD_COURSE_PKG_CORQ.sql
@@MOBEDU\Package\CART_PKG.sql
@@MOBEDU\Package\DROP_COURSE_PKG.sql
@@MOBEDU\Package\GOKODSF.sql
@@MOBEDU\Package\ME_ACCOUNT_OBJECTS.sql
@@MOBEDU\Package\ME_ALT_PIN_PKG.sql
@@MOBEDU\Package\ME_BWCKCOMS.sql
@@MOBEDU\Package\ME_BWCKGENS.sql
@@MOBEDU\Package\ME_BWCKREGS.sql
@@MOBEDU\Package\ME_REG_UTILS.sql
@@MOBEDU\Package\ME_VALID.sql
@@MOBEDU\Package\ME_WITHDRAWL.sql
@@MOBEDU\Package\Z_CM_MOBILE_CAMPUS.sql
prompt
prompt Creating Views
prompt =========================
prompt
@@MOBEDU\View\ACCOUNT_SUMMARY_VIEW.sql
@@MOBEDU\View\ACCT_SUMM_VW.sql
@@MOBEDU\View\CART_DETAILS_VW.sql
@@MOBEDU\View\CHARGE_PAY_DETAIL_VW.sql
@@MOBEDU\View\CONTACT_VW.sql
@@MOBEDU\View\TERMS_TO_REGISTER_VW.sql
@@MOBEDU\View\COURSE_SEARCH_VW.sql
@@MOBEDU\View\CRSE_MEET_VW.sql
@@MOBEDU\View\GENERAL_PERSON_VW.sql
@@MOBEDU\View\ME_HOURS_SUMMARY_BY_TERM.sql
@@MOBEDU\View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@MOBEDU\View\MOBEDU_HOLIDAY_VW.sql
@@MOBEDU\View\PERSON_CAMPUS_VW.sql
@@MOBEDU\View\PERSON_VW.sql
@@MOBEDU\View\STUDENT_ACCOUNT_DETAIL.sql
@@MOBEDU\View\STU_CURCULAM_INFO.sql
@@MOBEDU\View\STU_HOLD_INFO.sql
@@MOBEDU\View\STU_PROFILE_INFO.sql
@@MOBEDU\View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@MOBEDU\View\SUMMARY_BY_TERM_VW.sql
@@MOBEDU\View\TERMS_TO_BURSAR_VW.sql
@@MOBEDU\View\TERM_COURSE_DETAILS_VW.sql
@@MOBEDU\View\VW_TERM_CHARGES.sql
@@MOBEDU\View\VW_TERM_PAYMENTS.sql
prompt
prompt Creating Package Bodies
prompt =========================
prompt
@@MOBEDU\PackageBody\ADD_COURSE_PKG_CORQ.sql
@@MOBEDU\PackageBody\CART_PKG.sql
@@MOBEDU\PackageBody\DROP_COURSE_PKG.sql
@@MOBEDU\PackageBody\GOKODSF.sql
@@MOBEDU\PackageBody\ME_ACCOUNT_OBJECTS.sql
@@MOBEDU\PackageBody\ME_ALT_PIN_PKG.sql
@@MOBEDU\PackageBody\ME_BWCKCOMS.sql
@@MOBEDU\PackageBody\ME_BWCKGENS.sql
@@MOBEDU\PackageBody\ME_BWCKREGS.sql
@@MOBEDU\PackageBody\ME_REG_UTILS.sql
@@MOBEDU\PackageBody\ME_VALID.sql
@@MOBEDU\PackageBody\ME_WITHDRAWL.sql
@@MOBEDU\PackageBody\Z_CM_MOBILE_CAMPUS.sql
prompt
prompt Loading data to tables
prompt =========================
prompt
@@MOBEDU\Data\ADMIN_PRIV.sql
@@MOBEDU\Data\ADMIN_ROLE.sql
@@MOBEDU\Data\ADMIN_USER.sql
@@MOBEDU\Data\CAMPUS.sql
@@MOBEDU\Data\EMERGENCY_CONTACTS_AUDIT.sql
@@MOBEDU\Data\EMERGENCY_CONTACTS.sql
@@MOBEDU\Data\FAQ.sql
@@MOBEDU\Data\FEEDBACK_DATA.sql
@@MOBEDU\Data\FEEDBACK.sql
@@MOBEDU\Data\FEEDS_AUDIT.sql
@@MOBEDU\Data\FEEDS.sql
@@MOBEDU\Data\HELP_AUDIT.sql
@@MOBEDU\Data\HELP.sql
@@MOBEDU\Data\MAPS_AUDIT.sql
@@MOBEDU\Data\MAPS.sql
@@MOBEDU\Data\MOBEDU_CART.sql
@@MOBEDU\Data\MODULE.sql
@@MOBEDU\Data\MODULES.sql
@@MOBEDU\Data\NOTICE.sql
@@MOBEDU\Data\NOTICELOG.sql
@@MOBEDU\Data\NOTICEPOPULATION.sql
@@MOBEDU\Data\NOTICETYPE.sql
@@MOBEDU\Data\POPULATIONTYPE.sql
@@MOBEDU\Data\PRIVILEGE_AUDIT.sql
@@MOBEDU\Data\PRIVILEGE.sql
@@MOBEDU\Data\ROLE.sql
prompt
prompt Recompiling Schema
prompt =========================
prompt
EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
