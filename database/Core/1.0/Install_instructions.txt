Installation Steps

Follow the below steps in the same sequence to install the database scripts
1.	Create MOBEDU Schema or Schema of your choice and Objects
	Login to Banner database as SYSDBA
	Change directory to SCHEMA\
	Execute the file 1_CREATE_MOBEDU_USER.sql
	Please enter user name and password for mobile app user (mobedu is the username advised) when prompted.
	Save the password provided for MOBEDU(Or user you created for mobile app) user, for next steps.
	In this step user for mobile app is created and grants required are provided for the user.
	A log file CREATE_MOBEDU.log will be generated
	
2.      Create user objects for mobile app user
	Login as mobile app user created in step 1.
	Execute the file 2_RUN_MOBEDU_USER.sql
	During this step, all objects required for the mobile app are created.
	Please enter all the configuration values as and when prompted.
	A log file RUN_USER_OBJECTS.log will be generated

3.      Check Invalid objects
	Execute the file 3_RUN_INVALID_CHECK.sql
	This will show if there are any invalid objects.
	A log file INVALID_OBJECTS.log will be generated.
	If no invalid objects then installation is success.
	If you find invalid objects then goto step 4 and send us the below log files.
		A. CREATE_MOBEDU.log
		B. RUN_USER_OBJECTS.log
		C. INVALID_OBJECTS.log
	
4.	Rollback Scripts
	Login to Banner database as SYSDBA
	Logoff all mobile app user session
	Change directory to SCHEMA\
	Execute the file 4_ROLLBACK_MOBEDU_USER.sql
	Report the issues to N2N support team with all the log files.

Installation Checklist

To verify success installation, perform the below steps:
1.	Verify the below installation logs file for any errors
	a.	CREATE_MOBEDU.log , RUN_USER_OBJECTS.log, INVALID_OBJECTS.log 
	b.	rollback_mobEdu.log (In case of a rollback)
2.	Login to Banner database as mobile app user to ensure a successful user creation
