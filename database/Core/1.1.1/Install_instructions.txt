Installation Steps

Follow the below steps in the same sequence to install the database scripts

1.	Login as mobile app databse user (eg. mobedu) and run 1_RUN_MOBEDU_USER.sql
2.	Run 2_RUN_INVALID_CHECK.sql to check is all objects are installed correctly.
3.	If no invalid objects are seen in step 2 then we good. If there are any invalid objects run 3_ROLLBACK_MOBEDU_USER.sql.
4.	Repeat step 2, if you still see any invalid objects this has to be escalted to N2N services immediately with all log files. 

Installation Checklist

To verify success installation, perform the below steps:
1.	Verify the below installation logs file for any errors
	a.	RUN_USER_OBJECTS.log, INVALID_OBJECTS.log 
	b.	rollback.log (In case of a rollback)
2.	Login to Banner database as mobile app user to ensure a successful user creation
