PROMPT CREATE TABLE att_instructor_code_ref
create table ATT_INSTRUCTOR_CODE_REF
(
  instructor_id         VARCHAR2(20),
  term_code             VARCHAR2(10),
  crn                   NUMBER,
  attendence_code       VARCHAR2(50),
  attendence_id         NUMBER not null,
  attendence_date       DATE,
  attn_class_start_time VARCHAR2(6),
  attn_class_end_time   VARCHAR2(6),
  code_expiry_datetime  DATE
);
/


