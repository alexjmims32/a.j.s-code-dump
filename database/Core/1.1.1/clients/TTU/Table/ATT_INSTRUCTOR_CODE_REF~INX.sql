PROMPT CREATE INDEX ATTN_IDX 
create unique index ATTN_IDX 
on ATT_INSTRUCTOR_CODE_REF 
(TERM_CODE, CRN, ATTENDENCE_DATE, 
ATTN_CLASS_START_TIME, 
ATTN_CLASS_END_TIME, INSTRUCTOR_ID)
/


PROMPT CREATE INDEX att_term_course_idx
CREATE INDEX att_term_course_idx
  ON att_instructor_code_ref (
    term_code,
    crn,
    attendence_date
  )
  STORAGE (
    NEXT       1024 K
  )
/

