create table ATT_RANDOM_CODE
(
  ATTENDENCE_ID        NUMBER,
  ATT_RANDOM_CODE      VARCHAR2(8),
  CODE_EXPIRY_DATETIME DATE
)
;

CREATE INDEX ATTN_ID_IDX ON ATT_RANDOM_CODE (ATTENDENCE_ID);
/

