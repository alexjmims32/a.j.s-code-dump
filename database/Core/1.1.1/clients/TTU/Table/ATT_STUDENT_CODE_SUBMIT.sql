PROMPT CREATE TABLE att_student_code_submit
CREATE TABLE att_student_code_submit (
  attendence_student_id         VARCHAR2(20)  NULL,
  attendence_instructor_id      VARCHAR2(20)  NULL,
  attendence_term_code          VARCHAR2(10)  NULL,
  attendence_crn                NUMBER        NULL,
  attendence_code               VARCHAR2(50)  NULL,
  attendence_id                 NUMBER        NOT NULL,
  attendence_submit_datetime    TIMESTAMP(6)  NULL,
  attendence_valid_ind          VARCHAR2(1)   NULL,
  attendence_over_write         VARCHAR2(1)   DEFAULT 'N' NULL,
  attendence_over_comments      VARCHAR2(100) NULL,
  attendence_overwrite_by       VARCHAR2(20)  NULL,
  attendence_gpslocation_match  VARCHAR2(1)   DEFAULT 'N' NULL,
  attendence_last_modified_date DATE          NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


