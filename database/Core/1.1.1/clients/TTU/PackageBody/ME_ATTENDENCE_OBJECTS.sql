create or replace package body ME_ATTENDENCE_OBJECTS is

  function fz_get_attendence_code return varchar2 is
  
    v_attn_code varchar2(50);
  
    v_exist    number := 1;
    v_loop_cnt number := 2000;
  
  begin
  
    WHILE v_exist > 0 and v_loop_cnt > 0 LOOP
    
      select LOWER(DBMS_RANDOM.STRING('l', 4)) INTO v_attn_code from dual;
    
      SELECT COUNT(1)
        into v_exist
        FROM ATT_INSTRUCTOR_CODE_REF
       where attendence_code = v_attn_code;
    
      v_loop_cnt := v_loop_cnt - 1;
    
      if v_exist > 0 then
        v_attn_code := null;
      end if;
    
    END LOOP;
  
    if v_attn_code is null then
    
      /*      v_attn_code := RPAD(LPAD(To_CHAR(ATTENDENCE_SEQ.NEXTVAL), 5, 'N'),
      8,
      'A'); */ --DBMS_RANDOM.STRING('X', 1)
    
      v_attn_code := LOWER(RPAD(LPAD(To_CHAR(ATTENDENCE_SEQ.NEXTVAL),
                                     2,
                                     DBMS_RANDOM.STRING('l', 3)),
                                2,
                                DBMS_RANDOM.STRING('l', 3)));
    
    end if;
  
    return v_attn_code;
  
  end;

  function fz_validate_class_meeting(P_TERM_CODE   varchar2,
                                     P_CRN         number,
                                     P_START_TIME  varchar2,
                                     P_END_TIME    varchar2,
                                     P_ATTEND_DATE varchar2) --Date in MM/DD/YYYY format 
   return char is
  
    TYPE DataCurTyp IS REF CURSOR;
    V_DATA DataCurTyp;
  
    V_SELECT   varchar2(3000);
    V_DATA_REC NUMBER;
  
    V_DAY VARCHAR2(100);
  
    /*P_TERM_CODE   VARCHAR2(10) := '201380';
    P_CRN         NUMBER := 80005;
    P_START_TIME  VARCHAR2(10) := '1220';
    P_END_TIME    VARCHAR2(10) := '1420';
    P_ATTEND_DATE VARCHAR2(15) := '08/30/2013';*/
  
    v_quote char(1) := '''';
  
  begin
  
    V_DAY := to_char(TO_DATE(P_ATTEND_DATE, 'MM/DD/YYYY'), 'DY') || 'DAY'; --Like THUDAY 
  
    DBMS_OUTPUT.PUT_LINE(V_DAY);
  
    V_DAY := CASE
               WHEN V_DAY = 'SUNDAY' THEN
                ' AND 1=2 '
               ELSE
                ' AND ' || V_DAY || ' IS NOT NULL '
             END;
  
    V_SELECT := 'select COUNT(1)
  from crse_meet_vw
 where term_code = ' || v_quote || TRIM(P_TERM_CODE) ||
                v_quote || ' and crn =' || v_quote || TRIM(P_CRN) ||
                v_quote || '  and BEGIN_TIME = ' || v_quote ||
                TRIM(P_START_TIME) || v_quote || '  and END_TIME = ' ||
                v_quote || TRIM(P_END_TIME) || v_quote || '   and TO_DATE(' ||
                v_quote || TRIM(P_ATTEND_DATE) || v_quote ||
                ', ''MM/DD/YYYY'') between start_date and end_date  ' ||
                V_DAY;
  
    -- DBMS_OUTPUT.PUT_LINE(V_SELECT);
  
    /*   select COUNT(1)
     from crse_meet_vw
    where term_code = '201380'
      and crn = '80005'
      and BEGIN_TIME = '1220'
      and END_TIME = '1420'
      and TO_DATE('08-30-2013', 'MM-DD-YYYY') between start_date and end_date
      AND FRIDAY IS NOT NULL  -- This line will be dynamically derived 
      */
  
    OPEN V_DATA FOR V_SELECT;
  
    LOOP
      FETCH V_DATA
        INTO V_DATA_REC;
      EXIT WHEN V_DATA%NOTFOUND;
    
      DBMS_OUTPUT.PUT_LINE(V_DATA_REC);
    
    END LOOP;
  
    CLOSE V_DATA;
  
    IF V_DATA_REC > 0 THEN
      DBMS_OUTPUT.PUT_LINE('Y');
      RETURN 'Y';
    ELSE
      DBMS_OUTPUT.PUT_LINE('N');
      RETURN 'N';
    END IF;
  
    RETURN 'N';
  
  end;

  PROCEDURE PZ_CREATE_UPDATE_ATTN_CODE(P_instructor_id   VARCHAR2,
                                       p_term_code       varchar2,
                                       p_crn             number,
                                       p_attn_code       in out varchar2,
                                       p_attn_date       varchar2,
                                       p_start_time      varchar2,
                                       p_end_time        varchar2,
                                       p_expiry_interval number default 15)
  
   IS
  
    v_code_exist number;
    V_attn_date  date;
  
  BEGIN
  
    v_attn_date := NVL(TO_DATE(p_attn_date, 'MM/DD/YYYY'), TRUNC(SYSDATE));
  
    IF fz_validate_class_meeting(p_term_code,
                                 P_CRN,
                                 P_START_TIME,
                                 P_END_TIME,
                                 p_attn_date) <> 'Y' THEN
    
      raise_application_error(-20001,
                              'Section not defined for the date ' ||
                              p_attn_date);
    
    END IF;
  
    IF TRIM(p_attn_code) IS NULL THEN
    
      begin
        select attendence_code
          into p_attn_code
          from ATT_INSTRUCTOR_CODE_REF
         where upper(instructor_id) = upper(P_instructor_id)
           and term_code = p_term_code
           and crn = p_crn
           and attendence_date = v_attn_date
           and attn_class_start_time = p_start_time
           and attn_class_end_time = p_end_time;
      exception
        when others then
          p_attn_code := NULL;
      end;
    
      p_attn_code := NVL(p_attn_code, fz_get_attendence_code());
    
    END IF;
  
    select count(1)
      into v_code_exist
      from ATT_INSTRUCTOR_CODE_REF
     where upper(instructor_id) = upper(P_instructor_id)
       and term_code = p_term_code
       and crn = p_crn
       and attendence_code = p_attn_code
       and attendence_date = v_attn_date
       and attn_class_start_time = p_start_time
       and attn_class_end_time = p_end_time;
  
    if v_code_exist = 0 then
    
      begin
        insert into ATT_INSTRUCTOR_CODE_REF
        values
          (upper(P_instructor_id),
           p_term_code,
           p_crn,
           p_attn_code,
           null,
           TRUNC(v_attn_date),
           p_start_time,
           p_end_time,
           sysdate + NVL(p_expiry_interval, 1) / 24 / 3600);
      
      exception
        when others then
          rollback;
        
          raise_application_error(-20001, 'Server Error. Retry!');
        
      end;
    
    else
    
      begin
        dbms_output.put_line('v_code_exist' || v_code_exist);
        update ATT_INSTRUCTOR_CODE_REF
           set code_expiry_datetime = case
                                        when nvl(code_expiry_datetime,
                                                 sysdate) > sysdate then
                                         code_expiry_datetime +
                                         (p_expiry_interval / 86400)
                                        else
                                         sysdate +
                                         (p_expiry_interval / 86400)
                                      end
        --NVL(p_expiry_interval, 1) / 24 / 3600
         where attendence_code = p_attn_code;
      
      exception
        when others then
          rollback;
        
          raise_application_error(-20001, 'Server Error. Retry!');
        
      end;
    
    end if;
  
    COMMIT;
  
  END;

  PROCEDURE PZ_SUBMIT_ATTN_CODE(P_student_id    VARCHAR2,
                                p_attn_code     VARCHAR2,
                                p_overwrite_by  VARCHAR2 default null,
                                p_overwite_comm varchar2 default null,
                                p_attn_ind      varchar2 default 'Y',
                                P_RESPONSE      OUT varchar2) IS
    v_exist        number;
    v_att_date     date;
    v_overwrite_by varchar2(50);
  
  BEGIN
  
    P_RESPONSE := 'SUCCESS';
  
    /* Student can not overwrite attendance */
  
    IF TRIM(P_student_id) = TRIM(p_overwrite_by) THEN
    
      v_overwrite_by := NULL;
    
    ELSE
    
      v_overwrite_by := TRIM(p_overwrite_by);
    
    END IF;
  
    /*select count(1)
      into v_exist
      from ATT_INSTRUCTOR_CODE_REF A
     where attendence_code = p_attn_code;*/
     
     select count(1)
       into v_exist
       from ATT_ATTENDENCE_SHEET
      where ATTENDENCE_CODE = p_attn_code
        and student_id = UPPER(TRIM(P_student_id));
  
    if v_exist = 0 then
      raise_application_error(-20001, 'Invalid attendance code.');
    else
    
      select A.ATTENDENCE_DATE
        into v_att_date
        from ATT_INSTRUCTOR_CODE_REF A
       where attendence_code = p_attn_code;
    
       IF TRUNC(NVL(v_att_date, SYSDATE + 100)) > TRUNC(SYSDATE) THEN
      
        raise_application_error(-20001,
                                'Attendance date cannot be future.');
      
      END IF;
    
    end if;
  
    select count(1)
      into v_exist
      from ATT_STUDENT_CODE_SUBMIT
     where attendence_student_id = P_student_id
       and attendence_code = p_attn_code;
  
    if v_exist = 0 then
    
      insert into ATT_STUDENT_CODE_SUBMIT
        select *
          from (
                
                select P_student_id,
                        instructor_id,
                        term_code,
                        crn,
                        attendence_code,
                        attendence_id,
                        TRUNC(SYSDATE),
                        NVL(p_attn_ind, 'N'),
                        case
                          when v_overwrite_by is not null then
                           'Y'
                          else
                           'N'
                        END over_ind,
                        p_overwite_comm,
                        p_overwrite_by,
                        '',
                        SYSDATE
                  from ATT_INSTRUCTOR_CODE_REF A
                 where attendence_code = p_attn_code
                   and (code_expiry_datetime >= sysdate or
                       v_overwrite_by is not null));
    
      select count(1)
        into v_exist
        from ATT_STUDENT_CODE_SUBMIT
       where attendence_student_id = P_student_id
         and attendence_code = p_attn_code;
    
      if v_exist = 0 then
        raise_application_error(-20001, 'Attendance time expired.');
      end if;
    
    ELSE
    
      if v_overwrite_by is null OR NVL(v_overwrite_by, '*') = P_student_id then
        --Instructor can update attendance but not student. 
        raise_application_error(-20001,
                                'Attendance already submitted for this date.');
      end if;
    
      UPDATE ATT_STUDENT_CODE_SUBMIT
         set attendence_valid_ind          = p_attn_ind,
             attendence_over_write = case
                                       when v_overwrite_by is not null then
                                        'Y'
                                       else
                                        'N'
                                     END,
             attendence_over_comments      = p_overwite_comm,
             attendence_overwrite_by       = v_overwrite_by,
             attendence_gpslocation_match  = '',
             attendence_last_modified_date = SYSDATE
       where attendence_student_id = P_student_id
         and attendence_code = p_attn_code;
    
    END IF;
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
    
      ROLLBACK;
      P_RESPONSE := SQLERRM;
    
  END;

begin

  NULL;

end ME_ATTENDENCE_OBJECTS;
/

