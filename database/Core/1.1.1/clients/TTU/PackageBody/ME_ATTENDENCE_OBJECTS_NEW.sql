PROMPT CREATE OR REPLACE PACKAGE BODY me_attendence_objects_new
create or replace package body ME_ATTENDENCE_OBJECTS_NEW is

  function fz_get_attendence_code return varchar2 is
  
    v_attn_code varchar2(50);
  
    v_exist    number := 1;
    v_loop_cnt number := 5000;
  
  begin
  
    WHILE v_exist > 0 and v_loop_cnt > 0 LOOP
    
      select LOWER(DBMS_RANDOM.STRING('x', 3)) ||
             TO_CHAR(TRUNC(DBMS_RANDOM.VALUE(1, 9)))
        INTO v_attn_code
        from dual;
    
      SELECT COUNT(1)
        into v_exist
        FROM ATT_RANDOM_CODE
       where att_random_code = v_attn_code;
    
      v_loop_cnt := v_loop_cnt - 1;
    
      if v_exist > 0 then
        v_attn_code := null;
      end if;
    
    END LOOP;
  
    if v_attn_code is null then
    
      /*      v_attn_code := RPAD(LPAD(To_CHAR(ATTENDENCE_SEQ.NEXTVAL), 5, 'N'),
      8,
      'A'); */ --DBMS_RANDOM.STRING('X', 1)
    
      v_attn_code := LOWER(RPAD(LPAD(To_CHAR(ATTENDENCE_SEQ.CURRVAL),
                                     2,
                                     DBMS_RANDOM.STRING('l', 3)),
                                2,
                                DBMS_RANDOM.STRING('l', 3)));
    
    end if;
  
    return lower(v_attn_code);
  
  end;

  function fz_validate_class_meeting(P_TERM_CODE   varchar2,
                                     P_CRN         number,
                                     P_START_TIME  varchar2,
                                     P_END_TIME    varchar2,
                                     P_ATTEND_DATE varchar2) --Date in MM/DD/YYYY format
   return char is
  
    TYPE DataCurTyp IS REF CURSOR;
    V_DATA DataCurTyp;
  
    V_SELECT   varchar2(3000);
    V_DATA_REC NUMBER;
  
    V_DAY VARCHAR2(100);
  
    /*P_TERM_CODE   VARCHAR2(10) := '201380';
    P_CRN         NUMBER := 80005;
    P_START_TIME  VARCHAR2(10) := '1220';
    P_END_TIME    VARCHAR2(10) := '1420';
    P_ATTEND_DATE VARCHAR2(15) := '08/30/2013';*/
  
    v_quote char(1) := '''';
  
  begin
  
    V_DAY := to_char(TO_DATE(P_ATTEND_DATE, 'MM/DD/YYYY'), 'DY') || 'DAY'; --Like THUDAY
  
    DBMS_OUTPUT.PUT_LINE('v_day '||V_DAY);
  
    /* V_DAY := CASE
      WHEN V_DAY = 'SUNDAY' THEN                      -- Added column SUNDAY to crse_meet_vw, hence commented this.
       ' AND 1=2 '
      ELSE
       ' AND ' || V_DAY || ' IS NOT NULL '
    END; */
  
    V_DAY := ' AND ' || V_DAY || ' IS NOT NULL ';
  
    V_SELECT := 'select COUNT(1)
  from crse_meet_vw
 where term_code = ' || v_quote || TRIM(P_TERM_CODE) ||
                v_quote || ' and crn =' || v_quote || TRIM(P_CRN) ||
                v_quote || '  and BEGIN_TIME = ' || v_quote ||
                TRIM(P_START_TIME) || v_quote || '  and END_TIME = ' ||
                v_quote || TRIM(P_END_TIME) || v_quote || '   and TO_DATE(' ||
                v_quote || TRIM(P_ATTEND_DATE) || v_quote ||
                ', ''MM/DD/YYYY'') between start_date and end_date  ' ||
                V_DAY;
  
    -- DBMS_OUTPUT.PUT_LINE(V_SELECT);
  
    /*   select COUNT(1)
     from crse_meet_vw
    where term_code = '201380'
      and crn = '80005'
      and BEGIN_TIME = '1220'
      and END_TIME = '1420'
      and TO_DATE('08-30-2013', 'MM-DD-YYYY') between start_date and end_date
      AND FRIDAY IS NOT NULL  -- This line will be dynamically derived
      */
  
    OPEN V_DATA FOR V_SELECT;
  
    LOOP
      FETCH V_DATA
        INTO V_DATA_REC;
      EXIT WHEN V_DATA%NOTFOUND;
    
      DBMS_OUTPUT.PUT_LINE('v_data_rec'||V_DATA_REC);
    
    END LOOP;
  
    CLOSE V_DATA;
  
    IF V_DATA_REC > 0 THEN
      DBMS_OUTPUT.PUT_LINE('Y');
      RETURN 'Y';
    ELSE
      DBMS_OUTPUT.PUT_LINE('N');
      RETURN 'N';
    END IF;
  
    RETURN 'N';
  
  end;

  function fz_validate_instructor(P_TERM_CODE     varchar2,
                                  P_CRN           number,
                                  P_INSTRUCTOR_ID varchar2) return char is
    v_exist number := 0;
  begin
  
    select count(1)
      into v_exist
      from ATT_FACULTY_CRSE
     where CRN = P_CRN
       and FACULTY_ID = TRIM(UPPER(P_INSTRUCTOR_ID))
       and TERM_CODE = P_TERM_CODE;
  
    IF v_exist > 0 THEN
      RETURN 'Y';
    ELSE
      RETURN 'N';
    END IF;
  
    RETURN 'N';
  
  end;

  PROCEDURE PZ_CREATE_UPDATE_ATTN_CODE(P_instructor_id   VARCHAR2,
                                       p_term_code       varchar2,
                                       p_crn             number,
                                       p_attn_code       in out varchar2,
                                       p_attn_date       varchar2,
                                       p_start_time      varchar2,
                                       p_end_time        varchar2,
                                       p_expiry_interval number default 15,
                                       p_response        OUT varchar2)
  
   IS
  
    v_code_exist number;
    V_attn_date  date;
    v_att_id     number;
  
  BEGIN
  
    begin
      PZ_CLEAN_RANDOM_CODE(1);
    exception
      when others then
        NULL;
    end;
  
    p_response := 'SUCCESS';
  
    v_attn_date := NVL(TO_DATE(p_attn_date, 'MM/DD/YYYY'), TRUNC(SYSDATE));
  
    IF fz_validate_class_meeting(p_term_code,
                                 P_CRN,
                                 P_START_TIME,
                                 P_END_TIME,
                                 p_attn_date) <> 'Y' THEN
    
      raise_application_error(-20001,
                              'Section not defined for the date ' ||
                              p_attn_date);
    
    END IF;
  
    IF fz_validate_instructor(p_term_code, P_CRN, P_instructor_id) <> 'Y' THEN
    
      raise_application_error(-20001,
                              'CRN ' || P_CRN ||
                              ' not assigned to Instructor ' ||
                              P_instructor_id);
    
    END IF;
  
    begin
      select attendence_id
        into v_att_id
        from ATT_INSTRUCTOR_CODE_REF
       where instructor_id = upper(P_instructor_id)
         and term_code = p_term_code
         and crn = p_crn
         and attendence_date = v_attn_date
         and attn_class_start_time = p_start_time
         and attn_class_end_time = p_end_time
         and attendence_id > 0;
    exception
      when others then
        v_att_id := NULL;
    end;
  
  DBMS_OUTPUT.PUT_LINE('v_att_id'||v_att_id); 
  
    IF v_att_id IS NULL THEN
    
      v_att_id := ATTENDENCE_SEQ.NEXTVAL;
    
      begin
      
        insert into ATT_INSTRUCTOR_CODE_REF
        values
          (upper(P_instructor_id),
           p_term_code,
           p_crn,
           p_attn_code,
           v_att_id,
           TRUNC(v_attn_date),
           p_start_time,
           p_end_time,
           NULL);
      
      exception
        when others then
          rollback;
        DBMS_OUTPUT.PUT_LINE('rollback while insert'||sqlerrm);
          raise_application_error(-20001, 'Server Error. Retry!');
        
      end;
    
      COMMIT;
    
    END IF;
  
    IF p_attn_code IS NULL THEN
      /* By chance p_attn_code is sent null for update then get it*/
      begin
        select att_random_code
          into p_attn_code
          from ATT_RANDOM_CODE
         where attendence_id = v_att_id
           and code_expiry_datetime >= SYSDATE
           and rownum = 1; -- for safety
      exception
        when others then
          p_attn_code := NULL;
      end;
    END IF;
  
    select count(1)
      into v_code_exist
      from ATT_RANDOM_CODE
     where attendence_id = v_att_id
       and att_random_code = p_attn_code;
  
    /* Trying to update but code has expired and cleaned*/
    if v_code_exist = 0 and p_attn_code is not null then
      raise_application_error(-20001, 'Server Error. Retry!');
    end if;
  
    p_attn_code := NVL(p_attn_code, fz_get_attendence_code());
  
    if v_code_exist = 0 then
    
      begin
      
        delete ATT_RANDOM_CODE where attendence_id = v_att_id;
      
        insert into ATT_RANDOM_CODE
        values
          (v_att_id,
           TRIM(p_attn_code),
           sysdate + NVL(p_expiry_interval, 1) / 86400);
      
      exception
        when others then
          rollback;
        
          raise_application_error(-20001, 'Server Error. Retry!');
        
      end;
    
    else
    
      begin
        dbms_output.put_line('v_code_exist' || v_code_exist);
        update ATT_RANDOM_CODE
           set code_expiry_datetime = case
                                        when nvl(code_expiry_datetime,
                                                 sysdate) > sysdate then
                                         code_expiry_datetime +
                                         (p_expiry_interval / 86400)
                                        else
                                         sysdate +
                                         (p_expiry_interval / 86400)
                                      end
         where att_random_code = p_attn_code
           and attendence_id = v_att_id;
      
      exception
        when others then
          rollback;
        
          raise_application_error(-20001, 'Server Error. Retry!');
        
      end;
    
    end if;
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
    
      ROLLBACK;
    
      P_RESPONSE := SQLERRM;
    
  END;

  PROCEDURE PZ_SUBMIT_ATTN_CODE(P_student_id    VARCHAR2,
                                p_attn_code     VARCHAR2,
                                p_overwrite_by  VARCHAR2 default null,
                                p_overwite_comm varchar2 default null,
                                p_attn_ind      varchar2 default 'Y',
                                P_RESPONSE      OUT varchar2) IS
    v_exist        number;
    v_att_date     date;
    v_overwrite_by varchar2(50);
    v_att_id       number;
  
  BEGIN
  
    P_RESPONSE := 'SUCCESS';
  
    /* Student can not overwrite attendance */
  
    IF TRIM(P_student_id) = TRIM(p_overwrite_by) THEN
    
      v_overwrite_by := NULL;
    
    ELSE
    
      v_overwrite_by := TRIM(p_overwrite_by);
    
    END IF;
  
    begin
    
      SELECT attendence_id
        into v_att_id
        FROM ATT_RANDOM_CODE
       WHERE att_random_code = TRIM(p_attn_code)
         and code_expiry_datetime >= SYSDATE;
    
    exception
      when others then
        v_att_id := NULL;
    end;
  
    /*select count(1)
     into v_exist
     from ATT_INSTRUCTOR_CODE_REF A
    where attendence_id = NVL(v_att_id, 0);*/
  
    /*Check if student can submit this attendance code i.e has registered for this class*/
    select count(1)
      into v_exist
      from ATT_ATTENDENCE_SHEET_NEW
     where attendence_id = NVL(v_att_id, 0)
       and student_id = UPPER(TRIM(P_student_id));
  
    if v_exist = 0 then
      raise_application_error(-20001,
                              'Invalid or expired attendance code.');
    else
    
      select A.ATTENDENCE_DATE
        into v_att_date
        from ATT_INSTRUCTOR_CODE_REF A
       where attendence_id = NVL(v_att_id, 0);
    
      IF TRUNC(NVL(v_att_date, SYSDATE + 100)) > TRUNC(SYSDATE) THEN
      
        raise_application_error(-20001,
                                'Attendance date cannot be future.');
      
      END IF;
    
    end if;
  
    select count(1)
      into v_exist
      from ATT_STUDENT_CODE_SUBMIT
     where attendence_student_id = P_student_id
       and attendence_id = NVL(v_att_id, 0);
  
    if v_exist = 0 then
    
      insert into ATT_STUDENT_CODE_SUBMIT
        select *
          from (
                
                select P_student_id,
                        instructor_id,
                        term_code,
                        crn,
                        attendence_code,
                        attendence_id,
                        TRUNC(SYSDATE),
                        NVL(p_attn_ind, 'N'),
                        case
                          when v_overwrite_by is not null then
                           'Y'
                          else
                           'N'
                        END over_ind,
                        p_overwite_comm,
                        p_overwrite_by,
                        '',
                        SYSDATE
                  from ATT_INSTRUCTOR_CODE_REF A
                 where attendence_id = NVL(v_att_id, 0));
    
      select count(1)
        into v_exist
        from ATT_STUDENT_CODE_SUBMIT
       where attendence_student_id = P_student_id
         and attendence_id = NVL(v_att_id, 0);
    
      if v_exist = 0 then
        raise_application_error(-20001, 'Attendance time expired.');
      end if;
    
    ELSE
    
      v_exist := 0;
    
      SELECT COUNT(1)
        INTO v_exist
        FROM ATT_STUDENT_CODE_SUBMIT
       WHERE attendence_student_id = P_student_id
         and attendence_id = NVL(v_att_id, 0)
         AND attendence_valid_ind = 'Y'; -- instructor can mark absent 'N' and hence student should contact instructor
    
      if v_exist > 0 then
        raise_application_error(-20001,
                                'Attendance already submitted for this date.');
      else
        raise_application_error(-20001,
                                'Contact Instructor to update attendance for this date.');
      end if;
    
    END IF;
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      P_RESPONSE := SQLERRM;
    
  END;

  PROCEDURE PZ_UPDATE_ATTENDANCE(P_student_ids   VARCHAR2,
                                 p_attn_id       NUMBER,
                                 p_overwrite_by  VARCHAR2 default null,
                                 p_overwite_comm varchar2 default null,
                                 P_RESPONSE      OUT varchar2) IS
  
    CURSOR C_STUDENTS IS
      SELECT TRIM(REGEXP_SUBSTR(trim(P_student_ids), '[^,]+', 1, LEVEL)) STUDENT_ID_IND
        FROM DUAL
      CONNECT BY REGEXP_SUBSTR(trim(P_student_ids), '[^,]+', 1, LEVEL) IS NOT NULL;
  
    v_exist      number := 0;
    V_STUDENT_ID VARCHAR2(20);
    V_ATT_IND    VARCHAR2(2);
    v_att_date   date;
  
  BEGIN
  
    P_RESPONSE := 'SUCCESS';
  
    begin
      select A.ATTENDENCE_DATE
        into v_att_date
        from ATT_INSTRUCTOR_CODE_REF A
       where attendence_id = NVL(p_attn_id, 0);
    exception
      when others then
        raise_application_error(-20001, 'Invalid attendance!');
    end;
  
    IF TRUNC(NVL(v_att_date, SYSDATE + 100)) > TRUNC(SYSDATE) THEN
    
      raise_application_error(-20001,
                              'Cannot Update Future Attendance ' ||
                              TRUNC(v_att_date) || '.');
    
    END IF;
  
    FOR R_STUDENT IN C_STUDENTS LOOP
    
      v_exist      := 0;
      V_STUDENT_ID := NULL;
      V_ATT_IND    := NULL;
    
      begin
      
        SELECT TRIM(SUBSTR(R_STUDENT.STUDENT_ID_IND,
                           1,
                           decode(instr(R_STUDENT.STUDENT_ID_IND, '-'),
                                  0,
                                  length(R_STUDENT.STUDENT_ID_IND) + 1,
                                  instr(R_STUDENT.STUDENT_ID_IND, '-')) - 1)),
               TRIM(SUBSTR(R_STUDENT.STUDENT_ID_IND,
                           decode(instr(R_STUDENT.STUDENT_ID_IND, '-'),
                                  0,
                                  length(R_STUDENT.STUDENT_ID_IND) + 1,
                                  instr(R_STUDENT.STUDENT_ID_IND, '-')) + 1))
        
          INTO V_STUDENT_ID, V_ATT_IND
          FROM DUAL;
      
        DBMS_OUTPUT.put_line('STUDENT:' || V_STUDENT_ID || ',IND:' ||
                             V_ATT_IND || ',ATT ID:' || p_attn_id);
      
        select count(1)
          into v_exist
          from ATT_STUDENT_CODE_SUBMIT
         where attendence_student_id = V_STUDENT_ID
           and attendence_id = NVL(p_attn_id, 0);
      
        IF v_exist = 0 THEN
        
          insert into ATT_STUDENT_CODE_SUBMIT
            select *
              from (
                    
                    select V_STUDENT_ID,
                            instructor_id,
                            term_code,
                            crn,
                            attendence_code,
                            attendence_id,
                            TRUNC(SYSDATE),
                            NVL(V_ATT_IND, 'N'),
                            case
                              when p_overwrite_by is not null then
                               'Y'
                              else
                               'N'
                            END over_ind,
                            p_overwite_comm,
                            UPPER(p_overwrite_by),
                            '',
                            SYSDATE
                      from ATT_INSTRUCTOR_CODE_REF A
                     where attendence_id = NVL(p_attn_id, 0));
        
        ELSE
        
          UPDATE ATT_STUDENT_CODE_SUBMIT
             set attendence_valid_ind          = V_ATT_IND,
                 attendence_over_write = case
                                           when p_overwrite_by is not null then
                                            'Y'
                                           else
                                            'N'
                                         END,
                 attendence_over_comments      = p_overwite_comm,
                 attendence_overwrite_by       = UPPER(p_overwrite_by),
                 attendence_gpslocation_match  = '',
                 attendence_last_modified_date = SYSDATE
           WHERE attendence_student_id = V_STUDENT_ID
             and attendence_id = NVL(p_attn_id, 0);
        
        END IF;
      
      exception
        when others then
        
          ROLLBACK;
        
          P_RESPONSE := 'Server Error. Not all students updated, try again!';
      end;
    
      COMMIT;
    
    END LOOP;
  
  exception
    when others then
      ROLLBACK;
      P_RESPONSE := SQLERRM;
    
  END;

  PROCEDURE PZ_CLEAN_RANDOM_CODE(P_DEL_DAYS NUMBER default 0) IS
  
    v_count number := 5000;
    v_rowid varchar2(50);
  
  BEGIN
  
    WHILE v_count > 0 LOOP
    
      V_ROWID := NULL;
    
      begin
      
        SELECT MAX(ROWID)
          into V_ROWID
          from ATT_RANDOM_CODE
         WHERE code_expiry_datetime < SYSDATE - P_DEL_DAYS - 2 / 24; -- delete two hour defore data.
      exception
        when others then
        
          V_ROWID := NULL;
          v_count := 0;
      end;
    
      IF V_ROWID IS NULL THEN
        v_count := 0;
      END IF;
    
      DELETE ATT_RANDOM_CODE WHERE ROWID = V_ROWID;
    
      COMMIT;
    
      v_count := v_count - 1;
    
    END LOOP;
  
  END;

  /*PROCEDURE PZ_GET_CURRENT_CLASS(P_ID         SPRIDEN.SPRIDEN_ID%TYPE,
                                   P_ROLE       VARCHAR2,
                                   P_TERM       STVTERM.STVTERM_CODE%TYPE default NULL,
                                   P_CLASS_DATE VARCHAR2 default NULL,
                                   P_TIME       VARCHAR2 default NULL,
                                   P_COURSE_OUT OUT VARCHAR2) IS
    
      V_COURSE_JSON  JSON.JSONSTRUCTOBJ;
      V_COURSE_ARRAY JSON.JSONArray;
      V_COURSE_LIST  JSON.JSONSTRUCTOBJ;
      i              number := 1;
    
      \*CURSOR C_GET_STDNT_COURSES(V_ID   SPRIDEN.SPRIDEN_ID%TYPE,
                                 V_TERM STVTERM.STVTERM_CODE%TYPE) IS
        SELECT CRN
          FROM ATT_REGISTERED_STUDENTS
         WHERE ID = V_ID
           AND TERM_CODE = V_TERM;
      
      CURSOR C_GET_FACLTY_COURSES(V_ID   SPRIDEN.SPRIDEN_ID%TYPE,
                                  V_TERM STVTERM.STVTERM_CODE%TYPE) IS
        SELECT CRN
          FROM ATT_FACULTY_CRSE
         WHERE FACULTY_ID = V_ID
           AND TERM_CODE = V_TERM;*\
    
      V_STUDENT_CRSE varchar2(4000);
      V_FACULTY_CRSE varchar2(4000);
    
      CURSOR C_GET_CRSE_DETAIL(V_CRN  SSBSECT.SSBSECT_CRN%TYPE,
                               V_TERM STVTERM.STVTERM_CODE%TYPE,
                               V_DATE DATE) IS
        SELECT M.CRN,
               BEGIN_TIME,
               END_TIME,
               BLDG_DESC,
               ROOM_CODE,
               COURSE_TITLE,
               DECODE(SUNDAY, NULL, NULL, 'SUN') || '-' ||
               DECODE(MONDAY, NULL, NULL, 'MON') || '-' ||
               DECODE(TUEDAY, NULL, NULL, 'TUE') || '-' ||
               DECODE(WEDDAY, NULL, NULL, 'WED') || '-' ||
               DECODE(THUDAY, NULL, NULL, 'THU') || '-' ||
               DECODE(FRIDAY, NULL, NULL, 'FRI') || '-' ||
               DECODE(SATDAY, NULL, NULL, 'SAT') MEETING_DAY
          FROM CRSE_MEET_VW M
          JOIN COURSE_SEARCH_VW S
            ON M.TERM_CODE = S.TERM_CODE
           AND M.CRN = S.CRN
         WHERE M.TERM_CODE = V_TERM
           AND M.CRN = V_CRN
           AND V_DATE BETWEEN M.START_DATE AND M.END_DATE;
    
      TYPE DataCurTyp IS REF CURSOR;
      V_GET_CRN DataCurTyp;
    
      V_SEL_TAB    varchar2(4000);
      V_CRN        SSBSECT.SSBSECT_CRN%TYPE;
      V_CLASS_DATE DATE;
      V_DAY        VARCHAR2(3);
      V_CUR_TIME   VARCHAR2(4);
    
      V_XML_COURSES xmltype;
    
    BEGIN
    
      V_STUDENT_CRSE := 'SELECT CRN FROM ATT_REGISTERED_STUDENTS WHERE ID =' || '''' || P_ID || '''' ||
                        'AND TERM_CODE =' || '''' || P_TERM || '''';
      V_FACULTY_CRSE := 'SELECT CRN FROM ATT_FACULTY_CRSE WHERE FACULTY_ID =' || '''' || P_ID || '''' ||
                        ' AND TERM_CODE =' || '''' || P_TERM || '''';
    
      V_CLASS_DATE := NVL(TO_DATE(P_CLASS_DATE, 'MM/DD/YYYY'),
                          TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MM/DD/YYYY'));
      V_CUR_TIME   := NVL(P_TIME, to_char(SYSDATE, 'HH24MI'));
    
      SELECT to_char(V_CLASS_DATE, 'DY') into V_DAY from dual;
    
      IF P_ROLE = 'STUDENT' THEN
        V_SEL_TAB := V_STUDENT_CRSE;
      ELSIF P_ROLE = 'FACULTY' THEN
        V_SEL_TAB := V_FACULTY_CRSE;
      ELSE
        V_SEL_TAB := 'SELECT 0 FROM DUAL';
      END IF;
    
      JSON.newJSONObj(V_COURSE_JSON);
      V_COURSE_JSON := JSON.addAttr(V_COURSE_JSON, 'status', 'SUCCESS');
      -- JSON.closeJSONObj(V_COURSE_JSON);
      JSON.print(JSON.JSON2String(V_COURSE_JSON));
    
      OPEN V_GET_CRN FOR V_SEL_TAB;
      LOOP
        FETCH V_GET_CRN
          INTO V_CRN;
        EXIT WHEN V_GET_CRN%NOTFOUND;
      
        FOR R_MEET IN C_GET_CRSE_DETAIL(V_CRN, P_TERM, V_CLASS_DATE) LOOP
        
          IF instr(R_MEET.MEETING_DAY, V_DAY) > 0 AND
             (R_MEET.BEGIN_TIME <= V_CUR_TIME AND
              R_MEET.END_TIME >= V_CUR_TIME) THEN
          
            JSON.newJSONObj(V_COURSE_LIST);
          
            V_COURSE_LIST := JSON.addAttr(V_COURSE_LIST,
                                          'CourseTitle',
                                          R_MEET.COURSE_TITLE || '(' ||
                                          R_MEET.CRN || ')');
            V_COURSE_LIST := JSON.addAttr(V_COURSE_LIST,
                                          'Time',
                                          R_MEET.BEGIN_TIME || '-' ||
                                          R_MEET.END_TIME);
            V_COURSE_LIST := JSON.addAttr(V_COURSE_LIST,
                                          'Location',
                                          R_MEET.BLDG_DESC || '(' ||
                                          R_MEET.ROOM_CODE || ')');
            JSON.closeJSONObj(V_COURSE_LIST);
          
            --V_COURSE_JSON := JSON.addAttr(V_COURSE_JSON,'Course'||i, V_COURSE_LIST);
          
            JSON.print(JSON.JSON2String(V_COURSE_LIST));
          
            V_COURSE_ARRAY(i) := JSON.JSON2String(V_COURSE_LIST);
          
            i := i + 1;
          
            select value(courserec) as "COURSE_REC"
              into V_XML_COURSES
              from table(xmlsequence(cursor (select R_MEET.COURSE_TITLE || '(' ||
                                             R_MEET.CRN || ')' TITLE,
                                             R_MEET.BEGIN_TIME || '-' ||
                                             R_MEET.END_TIME TIME_SLOT,
                                             R_MEET.BLDG_DESC || '(' ||
                                             R_MEET.ROOM_CODE || ')' LOCATION
                                        from dual))) courserec;
          
          END IF;
        
        END LOOP;
      
      END LOOP;
    
      CLOSE V_GET_CRN;
    
      V_COURSE_JSON := JSON.addAttr(V_COURSE_JSON,
                                    'courseList',
                                    JSON.addArray(V_COURSE_ARRAY, true));
    
      --  V_COURSE_JSON := JSON.addArray(V_COURSE_JSON, V_COURSE_ARRAY, true);
    
      JSON.print(JSON.JSON2String(V_COURSE_JSON));
    
      JSON.closeJSONObj(V_COURSE_JSON);
    
      JSON.print(JSON.JSON2String(V_COURSE_JSON, true));
    
      P_COURSE_OUT := JSON.JSON2String(V_COURSE_JSON);
    
      dbms_output.put_line(V_XML_COURSES.getClobVal);
    
    END;
  */
  FUNCTION FZ_GET_CURRENT_CLASS(P_ID         SPRIDEN.SPRIDEN_ID%TYPE,
                                P_ROLE       VARCHAR2,
                                P_TERM       STVTERM.STVTERM_CODE%TYPE default NULL,
                                P_CLASS_DATE VARCHAR2 default NULL,
                                P_TIME       VARCHAR2 default NULL)
    return SYS_REFCURSOR IS
  
    V_CLASS_DATE DATE;
    V_DAY        VARCHAR2(3);
    V_CUR_TIME   VARCHAR2(4);
    P_COURSE_OUT SYS_REFCURSOR;
  
  BEGIN
  
    V_CLASS_DATE := NVL(TO_DATE(P_CLASS_DATE, 'MM/DD/YYYY'),
                        TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MM/DD/YYYY'));
    V_CUR_TIME   := NVL(P_TIME, to_char(SYSDATE, 'HH24MI'));
  
    SELECT to_char(V_CLASS_DATE, 'DY') into V_DAY from dual;
  
    IF P_ROLE = 'STUDENT' THEN
    
      OPEN P_COURSE_OUT FOR
        SELECT P_ID ID,
               M.TERM_CODE,
               S.COURSE_TITLE,
               M.CRN,
               V_CLASS_DATE CLASS_DATE,
               M.BEGIN_TIME || '-' || M.END_TIME MEETING_TIME,
               M.BLDG_DESC,
               M.ROOM_CODE
          FROM ATT_REGISTERED_STUDENTS R
          JOIN CRSE_MEET_VW M
            ON M.TERM_CODE = R.TERM_CODE
           AND M.CRN = R.CRN
          JOIN COURSE_SEARCH_VW S
            ON M.TERM_CODE = S.TERM_CODE
           AND M.CRN = S.CRN
         WHERE ID = P_ID
           AND R.TERM_CODE = P_TERM
           AND M.TERM_CODE = P_TERM
           AND V_CLASS_DATE BETWEEN M.START_DATE AND
               NVL(M.END_DATE, SYSDATE + 1)
           AND M.BEGIN_TIME <= V_CUR_TIME
           AND (M.END_TIME >= V_CUR_TIME OR M.BEGIN_TIME > M.END_TIME)
           AND INSTR(DECODE(M.SUNDAY, NULL, NULL, 'SUN') || '-' ||
                     DECODE(M.MONDAY, NULL, NULL, 'MON') || '-' ||
                     DECODE(M.TUEDAY, NULL, NULL, 'TUE') || '-' ||
                     DECODE(M.WEDDAY, NULL, NULL, 'WED') || '-' ||
                     DECODE(M.THUDAY, NULL, NULL, 'THU') || '-' ||
                     DECODE(M.FRIDAY, NULL, NULL, 'FRI') || '-' ||
                     DECODE(M.SATDAY, NULL, NULL, 'SAT'),
                     V_DAY) > 0;
    
    ELSIF P_ROLE = 'FACULTY' THEN
    
      OPEN P_COURSE_OUT FOR
        SELECT P_ID ID,
               M.TERM_CODE,
               S.COURSE_TITLE,
               M.CRN,
               V_CLASS_DATE CLASS_DATE,
               M.BEGIN_TIME || '-' || M.END_TIME MEETING_TIME,
               M.BLDG_DESC,
               M.ROOM_CODE
          FROM ATT_FACULTY_CRSE R
          JOIN CRSE_MEET_VW M
            ON M.TERM_CODE = R.TERM_CODE
           AND M.CRN = R.CRN
          JOIN COURSE_SEARCH_VW S
            ON M.TERM_CODE = S.TERM_CODE
           AND M.CRN = S.CRN
         WHERE FACULTY_ID = P_ID
           AND R.TERM_CODE = P_TERM
           AND M.TERM_CODE = P_TERM
           AND V_CLASS_DATE BETWEEN M.START_DATE AND
               NVL(M.END_DATE, SYSDATE + 1)
           AND M.BEGIN_TIME <= V_CUR_TIME
           AND (M.END_TIME >= V_CUR_TIME OR M.BEGIN_TIME > M.END_TIME)
           AND INSTR(DECODE(M.SUNDAY, NULL, NULL, 'SUN') || '-' ||
                     DECODE(M.MONDAY, NULL, NULL, 'MON') || '-' ||
                     DECODE(M.TUEDAY, NULL, NULL, 'TUE') || '-' ||
                     DECODE(M.WEDDAY, NULL, NULL, 'WED') || '-' ||
                     DECODE(M.THUDAY, NULL, NULL, 'THU') || '-' ||
                     DECODE(M.FRIDAY, NULL, NULL, 'FRI') || '-' ||
                     DECODE(M.SATDAY, NULL, NULL, 'SAT'),
                     V_DAY) > 0;
    
    ELSE
      NULL;
    END IF;
  
    NULL;
  
    return P_COURSE_OUT;
  
  END;
  --------------
  PROCEDURE PZ_GENERATE_ATT_CODE_BJOB(P_ERROR_MSG OUT VARCHAR2) IS
    CURSOR C_FACULTY_DATA(C_DAY VARCHAR2) IS
      SELECT R.FACULTY_ID ID,
             M.TERM_CODE,
             M.CRN,
             M.BEGIN_TIME MEET_START_TIME,
             M.END_TIME   MEET_END_TIME
        FROM ATT_FACULTY_CRSE R
        JOIN CRSE_MEET_VW M
          ON M.TERM_CODE = R.TERM_CODE
         AND M.CRN = R.CRN
        WHERE TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MM/DD/YYYY') BETWEEN
             M.START_DATE AND NVL(M.END_DATE, SYSDATE + 1)
         AND M.BEGIN_TIME IS NOT NULL
         AND M.END_TIME IS NOT NULL
         AND INSTR(DECODE(M.SUNDAY, NULL, NULL, 'SUN') || '-' ||
                   DECODE(M.MONDAY, NULL, NULL, 'MON') || '-' ||
                   DECODE(M.TUEDAY, NULL, NULL, 'TUE') || '-' ||
                   DECODE(M.WEDDAY, NULL, NULL, 'WED') || '-' ||
                   DECODE(M.THUDAY, NULL, NULL, 'THU') || '-' ||
                   DECODE(M.FRIDAY, NULL, NULL, 'FRI') || '-' ||
                   DECODE(M.SATDAY, NULL, NULL, 'SAT'),
                   C_DAY) > 0
         AND NOT EXISTS (SELECT 1
                from ATT_INSTRUCTOR_CODE_REF
               where instructor_id = R.FACULTY_ID
                 and term_code = M.TERM_CODE
                 and crn = M.CRN
                 and attendence_date = TRUNC(SYSDATE)
                 and attn_class_start_time = M.BEGIN_TIME
                 and attn_class_end_time = M.END_TIME
                 and attendence_id > 0);
    V_CLASS_DATE VARCHAR2(100);
    V_DAY        VARCHAR2(3);
    V_RESPONSE   VARCHAR2(100);
    V_ATTN_CODE  VARCHAR2(100);
    CURSR_COUNT  NUMBER;
  BEGIN
    V_CLASS_DATE := TO_CHAR(SYSDATE, 'MM/DD/YYYY');
    SELECT to_char(SYSDATE, 'DY') into V_DAY from dual;
    DBMS_OUTPUT.PUT_LINE('V_DAY' || V_DAY);
    OPEN C_FACULTY_DATA(V_DAY);
    --FETCH C_FACULTY_DATA%rowcount INTO CURSR_COUNT;
    DBMS_OUTPUT.PUT_LINE('CURSR_COUNT:' || CURSR_COUNT);
    if C_FACULTY_DATA%rowcount = 0 then
      P_ERROR_MSG := 'No Classes to Generate Attendence Code.';
    end if;
    CLOSE C_FACULTY_DATA;
    FOR R_FACULTY_DATA IN C_FACULTY_DATA(V_DAY) LOOP
      -- DBMS_OUTPUT.PUT_LINE('ID:' || R_FACULTY_DATA.ID);
      begin
        V_ATTN_CODE := null;
        PZ_CREATE_UPDATE_ATTN_CODE(R_FACULTY_DATA.ID,
                                   R_FACULTY_DATA.TERM_CODE,
                                   R_FACULTY_DATA.CRN,
                                   V_ATTN_CODE,
                                   V_CLASS_DATE,
                                   R_FACULTY_DATA.MEET_START_TIME,
                                   R_FACULTY_DATA.MEET_END_TIME,
                                   1,
                                   V_RESPONSE);
      
      exception
        when others then
          P_ERROR_MSG := sqlerrm;
      end;
      -- DBMS_OUTPUT.PUT_LINE('ATT_CODE' || V_ATTN_CODE);
      IF V_ATTN_CODE IS NULL THEN
        P_ERROR_MSG := 'Could not generate Attendence code for '||R_FACULTY_DATA.CRN;
        -- DBMS_OUTPUT.PUT_LINE('ATT_CODE is null ' || P_ERROR_MSG);
      END IF;
      P_ERROR_MSG := V_RESPONSE;
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      P_ERROR_MSG := 'Failed to run the BJOB';
  END;

  FUNCTION FZ_GET_ABSENT_DATES(P_ID   spriden.spriden_id%type,
                               P_TERM stvterm.stvterm_code%type,
                               P_CRN  ssbsect.ssbsect_crn%type)
    return varchar2 is
  
    v_dates varchar2(10000);
  
    cursor c_absent_dates is
      SELECT LISTAGG(TO_CHAR(AT_DATE.ATTENDENCE_DATE, 'MM/DD/YYYY'), ',') WITHIN GROUP(ORDER BY AT_DATE.ATTENDENCE_DATE ASC) ABSENT_DATES
        FROM MOBEDU.ATT_ATTENDENCE_SHEET AT_DATE
       WHERE ATTENDENCE_IND = 'N'
         AND STUDENT_ID = P_ID
         AND CRN = P_CRN
         and TERM_CODE = P_TERM;
  
  begin
  
    FOR R_DATES IN c_absent_dates LOOP
    
      v_dates := R_DATES.ABSENT_DATES;
    
    END LOOP;
  
    return v_dates;
  
  end;

  PROCEDURE PZ_GET_CURRENT_CLASS(P_ID         SPRIDEN.SPRIDEN_ID%TYPE,
                                 P_ROLE       VARCHAR2,
                                 P_TERM       STVTERM.STVTERM_CODE%TYPE default NULL,
                                 P_CLASS_DATE VARCHAR2 default NULL,
                                 P_TIME       VARCHAR2 default NULL,
                                 P_RESULT     OUT CLOB) IS
  
    V_CLASS_DATE DATE;
    V_DAY        VARCHAR2(3);
    V_CUR_TIME   VARCHAR2(4);
    P_COURSE_OUT SYS_REFCURSOR;
    V_XMLTYPE    xmltype;
    --v_len        number;
  
  BEGIN
  
    V_CLASS_DATE := NVL(TO_DATE(P_CLASS_DATE, 'MM/DD/YYYY'),
                        TO_DATE(TO_CHAR(SYSDATE, 'MM/DD/YYYY'), 'MM/DD/YYYY'));
    V_CUR_TIME   := NVL(P_TIME, to_char(SYSDATE, 'HH24MI'));
  
    SELECT to_char(V_CLASS_DATE, 'DY') into V_DAY from dual;
  
    IF P_ROLE = 'STUDENT' THEN
    
      OPEN P_COURSE_OUT FOR
        SELECT P_ID ID,
               M.TERM_CODE,
               S.COURSE_TITLE,
               M.CRN,
               V_CLASS_DATE CLASS_DATE,
               M.BEGIN_TIME || '-' || M.END_TIME MEETING_TIME,
               M.BLDG_DESC,
               M.ROOM_CODE
          FROM ATT_REGISTERED_STUDENTS R
          JOIN CRSE_MEET_VW M
            ON M.TERM_CODE = R.TERM_CODE
           AND M.CRN = R.CRN
          JOIN COURSE_SEARCH_VW S
            ON M.TERM_CODE = S.TERM_CODE
           AND M.CRN = S.CRN
         WHERE ID = P_ID
           AND R.TERM_CODE = P_TERM
           AND M.TERM_CODE = P_TERM
           AND V_CLASS_DATE BETWEEN M.START_DATE AND
               NVL(M.END_DATE, SYSDATE + 1)
           AND M.BEGIN_TIME <= V_CUR_TIME
           AND (M.END_TIME >= V_CUR_TIME OR M.BEGIN_TIME > M.END_TIME)
           AND INSTR(DECODE(M.SUNDAY, NULL, NULL, 'SUN') || '-' ||
                     DECODE(M.MONDAY, NULL, NULL, 'MON') || '-' ||
                     DECODE(M.TUEDAY, NULL, NULL, 'TUE') || '-' ||
                     DECODE(M.WEDDAY, NULL, NULL, 'WED') || '-' ||
                     DECODE(M.THUDAY, NULL, NULL, 'THU') || '-' ||
                     DECODE(M.FRIDAY, NULL, NULL, 'FRI') || '-' ||
                     DECODE(M.SATDAY, NULL, NULL, 'SAT'),
                     V_DAY) > 0;
    
      V_XMLTYPE := xmltype.createxml(P_COURSE_OUT);
      CLOSE P_COURSE_OUT;
    ELSIF P_ROLE = 'FACULTY' THEN
    
      OPEN P_COURSE_OUT FOR
        SELECT P_ID ID,
               M.TERM_CODE,
               S.COURSE_TITLE,
               M.CRN,
               V_CLASS_DATE CLASS_DATE,
               M.BEGIN_TIME || '-' || M.END_TIME MEETING_TIME,
               M.BLDG_DESC,
               M.ROOM_CODE
          FROM ATT_FACULTY_CRSE R
          JOIN CRSE_MEET_VW M
            ON M.TERM_CODE = R.TERM_CODE
           AND M.CRN = R.CRN
          JOIN COURSE_SEARCH_VW S
            ON M.TERM_CODE = S.TERM_CODE
           AND M.CRN = S.CRN
         WHERE FACULTY_ID = P_ID
           AND R.TERM_CODE = P_TERM
           AND M.TERM_CODE = P_TERM
           AND V_CLASS_DATE BETWEEN M.START_DATE AND
               NVL(M.END_DATE, SYSDATE + 1)
           AND M.BEGIN_TIME <= V_CUR_TIME
           AND (M.END_TIME >= V_CUR_TIME OR M.BEGIN_TIME > M.END_TIME)
           AND INSTR(DECODE(M.SUNDAY, NULL, NULL, 'SUN') || '-' ||
                     DECODE(M.MONDAY, NULL, NULL, 'MON') || '-' ||
                     DECODE(M.TUEDAY, NULL, NULL, 'TUE') || '-' ||
                     DECODE(M.WEDDAY, NULL, NULL, 'WED') || '-' ||
                     DECODE(M.THUDAY, NULL, NULL, 'THU') || '-' ||
                     DECODE(M.FRIDAY, NULL, NULL, 'FRI') || '-' ||
                     DECODE(M.SATDAY, NULL, NULL, 'SAT'),
                     V_DAY) > 0;
    
      V_XMLTYPE := xmltype.createxml(P_COURSE_OUT);
      CLOSE P_COURSE_OUT;
    ELSE
      NULL;
    END IF;
    --V_XMLTYPE.extract()
    select V_XMLTYPE.getClobVal() into P_RESULT from dual;
    if nullif(length(P_RESULT), 0) is null then
      P_RESULT:='<?xml version="1.0"?>'||chr(10)||'<ROWSET>'||chr(10)||'</ROWSET>';
      dbms_output.put_line('empty');
    end if;
  END;

begin

  NULL;

end ME_ATTENDENCE_OBJECTS_NEW;
/

