PROMPT CREATE INDEX INDEX att_id_idx
CREATE UNIQUE INDEX att_id_idx
  ON att_instructor_code_ref (
    attendence_id
  )
  STORAGE (
    NEXT       1024 K
  )
/

