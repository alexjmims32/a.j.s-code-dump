PROMPT CREATE OR REPLACE VIEW att_attendence_sheet
create or replace view att_attendence_sheet as
select R.instructor_id,
       A.ID as STUDENT_ID,
       R.term_code,
       R.crn,
       R.attendence_code,
       R.attendence_date,
       R.attn_class_start_time,
       R.attn_class_end_time,
       A.NAME,
       A.FIRST_NAME,
       A.LAST_NAME,
       A.EMAIL,
       A.PHONE_AREA || '-' || A.PHONE_NUMB PHONE,
       A.PHONE_EXT EXTN,
       A.CONFIDENTIAL_IND,
       NVL(S.attendence_valid_ind, 'N') ATTENDENCE_IND
  FROM att_instructor_code_ref R
  JOIN att_registered_students A
    ON R.term_code = A.term_code
   and R.crn = A.crn
  LEFT JOIN att_student_code_submit S
    ON R.attendence_code = S.attendence_code
   and S.attendence_student_id = A.id
   ORDER BY A.LAST_NAME;
/

