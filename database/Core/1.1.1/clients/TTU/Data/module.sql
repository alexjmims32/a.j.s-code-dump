prompt inserting into module table.....
insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 23, 'ATTD', 'Attendance Reports', 'Y', 'Y', 'attendance.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 22, 'SETTINGS', 'Settings', 'N', 'Y', 'settings.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 25, 'CUSTSERV', 'Customer Service', 'Y', 'Y', 'courses.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 24, 'DIRECTORY', 'Directory', 'N', 'Y', 'dir.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 19, 'FACEBOOK', 'Facebook', 'N', 'Y', 'facebook.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 20, 'TWITTER', 'Twitter', 'N', 'Y', 'twitter-white.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 10, 'NOTIF', 'Notices', 'Y', 'Y', 'notification_white.png', 'TOPLEFT');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 2, 'ATHLETICS', 'Athletics', 'N', 'Y', 'athletics.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 3, 'EVENTS', 'Events', 'N', 'Y', 'events.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 4, 'MAPS', 'Maps', 'N', 'Y', 'campus-map.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 7, 'COURSES', 'My Courses', 'Y', 'Y', 'courses.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 6, 'PROFILE', 'My Profile', 'Y', 'Y', 'student-profile.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 8, 'ACCOUNTS', 'My Account', 'Y', 'Y', 'student-accounts.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 9, 'REG', 'Registration', 'N', 'N', 'registration.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 13, 'HELP', 'Help/FAQs', 'N', 'Y', 'help.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 1, 'NEWS', 'News', 'N', 'Y', 'campus-news.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 12, 'EMER', 'Emergency Contacts', 'N', 'Y', 'dir.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 21, 'VIDEOS', 'Videos', 'N', 'Y', 'video.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('TTU', 26, 'FEEDBACK', 'Feedback', 'N', 'Y', 'feedback.png', 'MAIN');

