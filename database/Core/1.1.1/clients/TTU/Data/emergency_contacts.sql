prompt inserting into emergency_contacts table.....
insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (1, 'TTU', 'Campus Police', '931-372-3234', '1 William L Jones Dr, Cookeville, TN 38505', 'police@ttu.edu', '', 'EMR', 2, 'superadmin', '22-AUG-13 08.25.04.000000 AM', 'Campus Police or University police in TTU.', null);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (2, 'TTU', 'Fire', '(931) 526-2121', '', '', '', 'EMR', null, '', '', '', null);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (3, 'TTU', 'Health Services', '(931) 372-3320', '', '', '', 'EMR', null, '', '', '', null);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (4, 'TTU', 'Counseling Center', '(931) 372-3331', '', '', '', 'EMR', null, '', '', '', null);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (5, 'TTU', 'News and Communications', '(931) 372-3214', '', '', '', 'EMR', null, '', '', '', null);

