prompt inserting into admin_priv table.....
insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'MAPS', 'Y', 'MAPS Module Access', 3);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', 4);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', 6);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', 8);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'ROLES', 'Y', 'ROLES Module Access', 5);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', 9);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'USERS', 'Y', 'USERS Module Access', 10);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'FEEDS', 'Y', 'FEEDS Module Access', 2);

