create or replace package ME_ATTENDENCE_OBJECTS is

  -- Author  : N2N IT Solutions
  -- Created : 03-08-2013 23:43:40
  -- Purpose : Functions and Procedures used in attendence module.

  function fz_get_attendence_code return varchar2;

  function fz_validate_class_meeting(P_TERM_CODE   varchar2,
                                     P_CRN         number,
                                     P_START_TIME  varchar2,
                                     P_END_TIME    varchar2,
                                     P_ATTEND_DATE varchar2) return char;

  PROCEDURE PZ_CREATE_UPDATE_ATTN_CODE(P_instructor_id   VARCHAR2,
                                       p_term_code       varchar2,
                                       p_crn             number,
                                       p_attn_code       in out varchar2,
                                       p_attn_date       varchar2,
                                       p_start_time      varchar2,
                                       p_end_time        varchar2,
                                       p_expiry_interval number default 15);

  PROCEDURE PZ_SUBMIT_ATTN_CODE(P_student_id    VARCHAR2,
                                p_attn_code     VARCHAR2,
                                p_overwrite_by  VARCHAR2 default null,
                                p_overwite_comm varchar2 default null,
                                p_attn_ind      varchar2 default 'Y',
                                P_RESPONSE      OUT varchar2);

end ME_ATTENDENCE_OBJECTS;
/

