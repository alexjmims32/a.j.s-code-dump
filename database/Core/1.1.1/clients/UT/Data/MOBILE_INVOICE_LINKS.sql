prompt Importing table mobile_invoice_links...
set feedback off
set define off
truncate table mobile_invoice_links;

prompt Importing table mobile_invoice_links...
set feedback off
set define off
insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('MHP1', '<a href=https://entappst0.utoledo.edu/mobileselfservice/?f=ins>Click here to waive or upgrade</a>', null, to_date('09-08-2013 11:41:03', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('MHP2', '<a href=https://entappst0.utoledo.edu/mobileselfservice/?f=ins>Click here to waive or upgrade</a>', null, to_date('09-08-2013 11:41:03', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('M1LF', '<a href=https://entappst0.utoledo.edu/mobileselfservice/?f=leg>Click here to Waive</a>', null, to_date('09-08-2013 11:41:04', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('TIV', 'Please <a href=https://entappst0.utoledo.edu/mobileselfservice/?f=tit> click here </a> for an explanation on how this impacts the charges listed in <font color="red"> RED </font>', null, to_date('09-08-2013 11:41:04', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links (DETAIL_CODE, URL_LINK, DISPLAY_VALUE, ACTIVITY_DATE)
values ('IPP', 'For addtional information on IPP and monthly payment calculations <a href=https://entappst0.utoledo.edu/mobileselfservice/?f=ipp> click here</a>', null, to_date('09-08-2013 11:41:05', 'dd-mm-yyyy hh24:mi:ss'));

insert into mobile_invoice_links
  values ('LINK', 'Click  <a target="_blank"  href="http://www.utoledo.edu/offices/registrar/linked%20courses.html">here</a>  for information on linked courses.' , '' ,SYSDATE);   


COMMIT;

prompt Done.
