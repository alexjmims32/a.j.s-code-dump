prompt Importing table ME_DEPARTMENT_CHAIR
set feedback off
set define off

TRUNCATE TABLE ME_DEPARTMENT_CHAIR;

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('00', 'No College Designated', 'BGSU', 'BGSU', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AL', 'Adult and Lifelong Learning', 'ISP', 'Interdisc and Special Programs', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'AMST', 'American Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'ART', 'Art', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'ASIP', 'Arts and Sci Interdisc Prgms', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'BIOL', 'Biology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'CHEM', 'Chemistry', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'COMM', 'Communication', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'ECON', 'Economics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'EEES', 'Environmental Sciences', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'ENGL', 'English Language and Lit', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'FLNG', 'Foreign Languages and Lit', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'GEPL', 'Geography and Planning', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'HIST', 'History', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'HON', 'Honors', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'HUM', 'Humanities', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'MATH', 'Mathematics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'MLS', 'Master of Liberal Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'MUS', 'Music', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'PHIL', 'Philosophy', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'PHYS', 'Physics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'PSC', 'Political Sci and Public Admin', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'PSY', 'Psychology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'SOC', 'Sociology and Anthropology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'THR', 'Theatre and Film', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('AS', 'College of Arts and Sciences', 'WMST', 'Women''s and Gender Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'ACCT', 'Accounting', 'Donald Saftn                                                                                        ', null, null, '419.530.2327        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'AOT', 'Appl Organizational Technology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'BT', 'Business Technology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'FIBE', 'Finance and Business Economics', 'mark vonderembse                                                                                    ', null, null, ' 419.530.2564       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'IOTM', 'Info Operations and Tech Mgmt', ' T.S. Ragu -natan                                                                                   ', null, null, ' 419.530.227        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'MGMT', 'Management', ' Sony Ariss                                                                                         ', null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'MKIB', 'Marketing and Intrnational Bus', ' Anthony koh                                                                                        ', null, null, ' 41.530.2093        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('BU', 'Coll Business and Innovation', 'MKTG', 'Marketing', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb Coll of Education', 'CI', 'Curriculum and Instruction', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb Coll of Education', 'COMM', 'Communication', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb Coll of Education', 'ECPS', 'Erly Chldhd, Phys, and Spec Ed', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb Coll of Education', 'EDL', 'Educational Leadership', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb Coll of Education', 'EPRF', 'Ed Psych, Resrch and Soc Found', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb Coll of Education', 'FOED', 'Foundations of Education', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'BGSU', 'BGSU', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'CESP', 'Counselor Ed and School Psych', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'CI', 'Curriculum and Instruction', 'ralereck                                                                                            ', null, null, ' 419.530.5373       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'CJSW', 'Criminal Just and Social Work', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'CRIM', 'Criminal Justice', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'ECPS', 'Erly Chldhd, Phys, and Spec Ed', ' richard welsch                                                                                     ', null, null, ' 419.530.7736       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'EDL', 'Educational Leadership', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'EPRF', 'Ed Psych, Resrch and Soc Found', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'FOED', 'Foundations of Education', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'HP', 'Health Professions', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'KINE', 'Kinesiology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'MS', 'Military Science', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'OCCT', 'Occupational Therapy', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'PHRS', 'Health and Rehab Services', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'PHYT', 'Physical Therapy', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'RESC', 'Rehabilitation Sciences', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'SOC', 'Sociology and Anthropology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'SOCW', 'Social Work', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'SPED', 'Special Education - DO NOT USE', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EH', 'J Herb Edu, Hlt Sci, Human Ser', 'ULS', 'Undergraduate Legal Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'BIOE', 'Bioengineering', ' Arun nadarajah                                                                                     ', null, null, ' 419.530.8031       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'CHE', 'Chemical and Envrnmntl Engnrng', ' Glen lipscomb                                                                                      ', null, null, ' 419.530.8085       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'CIVE', 'Civil Engineering', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'EECS', 'Electrical Eng and Computr Sci', ' mansoor alam                                                                                       ', null, null, ' 419.530.8196       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'ENGT', 'Engineering Technology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'MIME', 'Mech, Indust, and Mnfctrng Eng', ' Abdollah Afjeh                                                                                     ', null, null, ' 419.530.8210       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'BGSU', 'BGSU', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'BICB', 'Biochemistry and Cancer Biol', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'ISP', 'Interdisc and Special Programs', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'MICB', 'Microbiology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'MPHY', 'Medical Physics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'PHYA', 'Physician Assistant', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'PHYS', 'Physics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HC', 'Honors College', 'HON', 'Honors', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'BGSU', 'BGSU', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'CESP', 'Counselor Ed and School Psych', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'CRIM', 'Criminal Justice', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'HP', 'Health Professions', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'KINE', 'Kinesiology', ' Barry scheuremann                                                                                  ', null, null, ' 419.350.2692       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'MS', 'Military Science', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'OCCT', 'Occupational Therapy', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'PHRS', 'Health and Rehab Services', ' joseph dake                                                                                        ', null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'PHYA', 'Physician Assistant', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'PHYT', 'Physical Therapy', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'RESC', 'Rehabilitation Sciences', ' michelle masterson                                                                                 ', null, null, ' 419.530.6671       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'SOCW', 'Social Work', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('HS', 'Coll of Hlth Sci and Hum Serv', 'ULS', 'Undergraduate Legal Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('IL', 'College of Innovative Learning', 'ISP', 'Interdisc and Special Programs', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('IL', 'College of Innovative Learning', 'LIBR', 'Libraries', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'AMST', 'American Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'ASIP', 'Arts and Sci Interdisc Prgms', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'COMM', 'Communication', ' paul fritz                                                                                         ', null, null, ' 419.530.2006       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'ECON', 'Economics', ' mike dowd                                                                                          ', null, null, ' 419.530.4603       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'ENGL', 'English Language and Lit', ' sra lundquist                                                                                      ', null, null, ' 419.530.2506       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'FLNG', 'Foreign Languages and Lit', ' Ruth hottel                                                                                        ', null, null, ' 419.530.4651       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'GEPL', 'Geography and Planning', ' patrick lawremce                                                                                   ', null, null, ' 419.530.4128       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'HIST', 'History', ' william o neal                                                                                     ', null, null, ' 419.530.2242       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'HUM', 'Humanities', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'MLS', 'Master of Liberal Studies', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'PHIL', 'Philosophy', ' joh sarnecki                                                                                       ', null, null, ' 419.530.6185       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'PSC', 'Political Sci and Public Admin', ' mark denham                                                                                        ', null, null, ' 419.530.4062       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'PSY', 'Psychology', ' john                                                                                               ', null, null, ' 419.530.4130       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'SOC', 'Sociology and Anthropology', ' rubin patterson                                                                                    ', null, null, ' 419.530.4963       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LS', 'Coll Lang, Lit, and Soc Sci', 'WMST', 'Women''s and Gender Studies', ' charlene gilbert                                                                                   ', null, null, ' 419.530.2233       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('LW', 'College of Law', 'LAW', 'Law', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'ANAT', 'Anatomy and Neurosciences', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'ANES', 'Anesthesiology', ' Alan marco                                                                                         ', null, null, ' 419.383.3556       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'BGSU', 'BGSU', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'BICB', 'Biochemistry and Cancer Biol', ' wiliam A maltese                                                                                   ', null, null, ' 419.3834161        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'BIOC', 'Biochemistry and Molecular Bio', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'BIPG', 'Bioinform and Proteom-Genomics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'DENT', 'Oral Biology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'EMS', 'Emergency Medical Services', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'ERMD', 'Emergency Medicine', ' kris brickman                                                                                      ', null, null, ' 419.383.3888       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'FMMD', 'Family Medicine', ' linda french                                                                                       ', null, null, ' 419.383.5572       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'INMD', 'Internal Medicine', ' chrtopher cooper                                                                                   ', null, null, ' 419.383.6030       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'MEDI', 'Medicine', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'MICB', 'Microbiology', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'MMI', 'Medical Microbiol and Immunol', ' akira takashima                                                                                    ', null, null, ' 419.383.5423       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'MPHY', 'Medical Physics', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'NERS', 'Neurosciences', '  bryan yamamato                                                                                    ', null, null, ' 419.383.4126       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'NEUR', 'Neurology', ' gretchen tietjen                                                                                   ', null, null, ' 419.383.6087       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'OBGY', 'Obstetrics and Gynecology', ' kelly manahan                                                                                      ', null, null, ' 419.383.4536       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'OCCH', 'Occupational Health', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'ORTH', 'Orthopedics', ' nabil ebrahiem                                                                                     ', null, null, ' 419.383.4020       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PATH', 'Pathology', ' robert mrak                                                                                        ', null, null, ' 419.383.3469       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PEDS', 'Pediatrics', ' jefferry blumer                                                                                    ', null, null, ' 419.291.2191       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PHHS', 'Pub Hlth and Homeland Security', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PHRS', 'Health and Rehab Services', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PHYA', 'Physician Assistant', ' patricia hogue                                                                                     ', null, null, ' 419.383.4807       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PMNR', 'Physical Med and Rehabilitatn', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PPMC', 'Phys, Phrmcol, Metab, Card Sci', 'howard rosenberg                                                                                    ', null, null, ' 419.383.3982       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PSCH', 'Psychiatry', ' marijo tamburino                                                                                   ', null, null, '419.383.5650        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'PUBH', 'Public Health', 'sheryl milz                                                                                         ', null, null, ' 419.383.6346       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'RADI', 'Radiology', ' lee s. woldenberg                                                                                  ', null, null, ' 419.383.3428       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'RDON', 'Radiation Oncology', ' changhu chen                                                                                       ', null, null, ' 419.383.4541       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'SURG', 'Surgery', ' geraldzerlnack                                                                                     ', null, null, ' 419.383.6940       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('MD', 'Coll Medicine, Life Sciences', 'UROL', 'Urology', ' steven selmen                                                                                      ', null, null, '419.383.3584        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('NU', 'College of Nursing', 'NUR', 'Nursing', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('NU', 'College of Nursing', 'NURS', 'Nursing - DO NOT USE', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('PH', 'Coll Pharmacy, Pharm Sciences', 'MBC', 'Medicinal and Biological Chem', ' katherine wall                                                                                     ', null, null, ' 419.530.2902       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('PH', 'Coll Pharmacy, Pharm Sciences', 'PHCL', 'Pharmacology', ' william messer                                                                                     ', null, null, ' 419.383.1958       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('PH', 'Coll Pharmacy, Pharm Sciences', 'PHPR', 'Pharmacy Practice', ' stevenj martin                                                                                     ', null, null, ' 419.383.1964       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('SM', 'Coll Nat Sci and Mathematics', 'BIOL', 'Biology', ' douglas leaman                                                                                     ', null, null, ' 419.530.1555       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('SM', 'Coll Nat Sci and Mathematics', 'CHEM', 'Chemistry', ' ronald viola                                                                                       ', null, null, ' 419.530.7902       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('SM', 'Coll Nat Sci and Mathematics', 'EEES', 'Environmental Sciences', ' timothy fisher                                                                                     ', null, null, ' 419.530.4572       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('SM', 'Coll Nat Sci and Mathematics', 'MATH', 'Mathematics', ' paul hewitt                                                                                        ', null, null, '419.530.2568        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('SM', 'Coll Nat Sci and Mathematics', 'PHYS', 'Physics', ' lawrence anderson-huang                                                                            ', null, null, '419.530.4906        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('UC', 'University College', 'ISP', 'Interdisc and Special Programs', null, null, null, null, null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('VP', 'Coll Visual and Perf Arts', 'ART', 'Art', ' cristopher burnett                                                                                 ', null, null, ' 419.530.8301       ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('VP', 'Coll Visual and Perf Arts', 'MUS', 'Music', ' john                                                                                               ', null, null, '419.530.5062        ', null, null, 'I');

insert into Me_Department_Chair (COLL_CODE, COLLEGE, DEPT_CODE, DEPARTMENT, CHAIR_NAME, CHAIR_EMAIL, CHAIR_PHONE_AREA, CHAIR_PHONE, CHAIR_EXTN, CHAIR_FAX, FEEDBACK_IND)
values ('VP', 'Coll Visual and Perf Arts', 'THR', 'Theatre and Film', 'ed lingan                                                                                           ', null, null, ' 419.530.4350       ', null, null, 'I');

COMMIT;

prompt Done.
