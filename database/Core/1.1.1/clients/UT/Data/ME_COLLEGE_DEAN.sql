prompt Importing table ME_COLLEGE_DEAN...
set feedback off
set define off

TRUNCATE TABLE ME_COLLEGE_DEAN;

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('AL', 'College of Adult and Lifelong Learning', 'Dennis Lettman                                                                                      ', 'dennis.lettman@utoledo.edu                                                                          ', null, '419.530.3303        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('BU', 'College of Business and Innovation', 'Thomas Sharkey                                                                                      ', 'thomas.sharkey@utoledo.edu                                                                             ', null, '419.530.4612        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('ED', 'Judith Herb College of Education', 'Penny Poplin GosettI                                                                                ', 'penny.poplin.gosetti@utoledo.edu                                                                    ', null, '419.530.5402        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('EN', 'College of Engineering', 'Nagi Naganathan                                                                                     ', 'nagi.naganathan@utoledo.edu                                                                         ', null, '419 530.8000        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('GS', 'College of Graduate Studies', 'Patricia Komuniecki                                                                                 ', 'patricia.komuniecki@utoledo.edu                                                                     ', null, '419.530.4968        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('HC', 'Jesup W. Scott Honors College', 'Lakeesha Ransom                                                                                     ', 'lakeesha.ransom@utoledo.edu                                                                         ', null, '1419.530.6033       ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('HS', 'College of Social Justice and Human Service', 'Thomas Guttendge                                                                                    ', 'thomas.gutteridge@utoledo.edu                                                                       ', null, '419 530.2755        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('IL', 'College of Innovative Learning', null, null, null, null, null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('LS', 'College of Language, Literature and Social Sciences', 'Jamle Barlowe                                                                                       ', 'jamie.barlowe@utoledo.edu                                                                           ', null, '419.530.2413        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('LW', 'College of Law', 'Daniel Steinbock                                                                                    ', 'daniel.steinbock@utoledo.edu                                                                        ', null, '419 530.2379        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('MD', 'College of Medicine and Life Sciences', 'Jeffrey Gold                                                                                        ', 'jeffrey.gold@utoledo.edu                                                                            ', null, '419.383 4243        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('NU', 'College of Nursing', 'Timothy Gaspar                                                                                      ', 'tim.gaspar@utoledo.edu                                                                              ', null, '419.383.5858        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('PH', 'College of Pharmacy and Pharmaceutical Sciences', 'Johnny Early                                                                                        ', 'johnnie.early@utoledo.edu                                                                           ', null, '419.383.1931        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('SM', 'College of Natural Sciences and Mathematics', 'Karen Bjorkman                                                                                      ', 'karen.bjorkman@utoledo.edu                                                                          ', null, '419.530 7840        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('UC', 'YouCollege', 'D�Naie Jacobs                                                                                       ', 'dnaie.jacobs@utoledo.edu                                                                            ', null, null, null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values ('VP', 'College of Communication and the Arts', 'Debra Davis                                                                                         ', 'debra.davis@utoledo.edu                                                                             ', null, '419.530.8355        ', null, null, 'I');

insert into ME_COLLEGE_DEAN (COLL_CODE, COLLEGE, DEAN_NAME, DEAN_EMAIL, DEAN_PHONE_AREA, DEAN_PHONE, DEAN_EXTN, DEAN_FAX, FEEDBACK_IND)
values (null, 'College of Health Sciences', 'Beverly J. Schmoll                                                                                  ', 'beverly.schmoll@utoledo.edu                                                                         ', null, null, null, null, 'I');

COMMIT;

prompt Done.
