PROMPT CREATE TABLE proxy_session_audit
CREATE TABLE proxy_session_audit (
  username         VARCHAR2(50 BYTE) NOT NULL,
  studentid        VARCHAR2(20 BYTE) NOT NULL,
  requesttimestamp TIMESTAMP(6)      NULL,
  logintime        TIMESTAMP(6)      NULL,
  ipaddress        VARCHAR2(50 BYTE) NULL,
  logouttime       TIMESTAMP(6)      NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


