PROMPT CREATE TABLE proxy_reg_users
CREATE TABLE proxy_reg_users (
  username   VARCHAR2(100 BYTE) NOT NULL,
  active     VARCHAR2(1 BYTE)   DEFAULT 'Y' NOT NULL,
  accessflag CHAR(1 BYTE)       DEFAULT 0 NULL
)
  STORAGE (
    NEXT       1024 K
  )
/

COMMENT ON COLUMN proxy_reg_users.accessflag IS '0-READ-ONLYaccess, 1-READWRITE access';

