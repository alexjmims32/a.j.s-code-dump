spool 1_RUN_CONCORDIA_SPECIFIC_DATA.log

@@Data\maps.sql
@@Data\emergency_contacts.sql
@@Data\module.sql
@@Data\feeds.sql
@@Data\admin_user.sql
@@Data\admin_priv.sql
@@Data\feedback.sql
@@Data\noticelog.sql
@@Data\privilege.sql
@@Data\help.sql
@@Data\faq.sql
@@Data\category_filter.sql
@@Data\role.sql
@@Data\campus.sql
commit;
spool off
exit