PROMPT CREATE OR REPLACE PACKAGE me_biographic
CREATE OR REPLACE package me_biographic is

  -- Author  : MOBEDU
  -- Created : 11-09-2013 14:33:28
  -- Purpose : To display, create and  update biographic information of a person

  FUNCTION f_clean_text(p_text VARCHAR2) RETURN VARCHAR2;

  FUNCTION fz_get_email_address(P_PIDM       NUMBER,
                                P_EMAIL_CODE GOREMAL.GOREMAL_EMAL_CODE%TYPE default null,
                                P_PREF_IND   GOREMAL.GOREMAL_PREFERRED_IND%TYPE default 'N')
    RETURN VARCHAR2;



  procedure pz_convert_commastr_to_table(p_in_string     varchar2,
                                         p_varchar_table IN OUT twbklibs.varchar2_tabtype);

  PROCEDURE pz_procaddrupdate(pidm      IN OUT spriden.spriden_pidm%type,
                              atyp      VARCHAR2,
                              seqno     IN OUT NUMBER,
                              deladdr   VARCHAR2,
                              fdate     VARCHAR2,
                              tdate     VARCHAR2,
                              houseno   VARCHAR2,
                              str1      VARCHAR2,
                              str2      VARCHAR2,
                              str3      VARCHAR2,
                              str4      VARCHAR2,
                              city      VARCHAR2,
                              stat      VARCHAR2,
                              zip       VARCHAR2,
                              cnty      VARCHAR2,
                              natn      VARCHAR2,
                              ctry      VARCHAR2 DEFAULT NULL,
                              area      VARCHAR2 DEFAULT NULL,
                              num       VARCHAR2 DEFAULT NULL,
                              ext       VARCHAR2 DEFAULT NULL,
                              accs      VARCHAR2 DEFAULT NULL,
                              unl       VARCHAR2 DEFAULT NULL,
                              seqnos    IN OUT twbklibs.varchar2_tabtype,
                              teles     twbklibs.varchar2_tabtype,
                              ctrys     twbklibs.varchar2_tabtype,
                              areas     twbklibs.varchar2_tabtype,
                              phones    twbklibs.varchar2_tabtype,
                              exts      twbklibs.varchar2_tabtype,
                              accss     twbklibs.varchar2_tabtype,
                              unl1      VARCHAR2 DEFAULT NULL,
                              unl2      VARCHAR2 DEFAULT NULL,
                              unl3      VARCHAR2 DEFAULT NULL,
                              unl4      VARCHAR2 DEFAULT NULL,
                              unl5      VARCHAR2 DEFAULT NULL,
                              del1      VARCHAR2 DEFAULT NULL,
                              del2      VARCHAR2 DEFAULT NULL,
                              del3      VARCHAR2 DEFAULT NULL,
                              del4      VARCHAR2 DEFAULT NULL,
                              del5      VARCHAR2 DEFAULT NULL,
                              P_OUT_MSG OUT VARCHAR2);

  PROCEDURE pz_process_address(p_id         spriden.spriden_id%type,
                               p_role       varchar2,
                               p_atyp       VARCHAR2,
                               p_seqno      NUMBER,
                               p_rel        VARCHAR2 default NULL, -- used for emergency contact, relationship
                               p_order      NUMBER default NULL, -- used for emergency contact, new priority, p_seqno will have old priority
                               p_deladdr    VARCHAR2,
                               p_fdate      VARCHAR2,
                               p_tdate      VARCHAR2,
                               p_lname_pfx  IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_lname      IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_fname      IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_mi         IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_houseno    VARCHAR2,
                               p_str1       VARCHAR2,
                               p_str2       VARCHAR2,
                               p_str3       VARCHAR2,
                               p_str4       VARCHAR2,
                               p_city       VARCHAR2,
                               p_stat       VARCHAR2,
                               p_zip        VARCHAR2,
                               p_cnty       VARCHAR2,
                               p_natn       VARCHAR2,
                               p_ctry       VARCHAR2 DEFAULT NULL,
                               p_area       VARCHAR2 DEFAULT NULL,
                               p_num        VARCHAR2 DEFAULT NULL,
                               p_ext        VARCHAR2 DEFAULT NULL,
                               p_accs       VARCHAR2 DEFAULT NULL,
                               p_unl        VARCHAR2 DEFAULT NULL,
                               p_seqnos_lst VARCHAR2,
                               p_teles_lst  VARCHAR2,
                               p_ctrys_lst  VARCHAR2,
                               p_areas_lst  VARCHAR2,
                               p_phones_lst VARCHAR2,
                               p_exts_lst   VARCHAR2,
                               p_accss_lst  VARCHAR2,
                               p_unlist_lst VARCHAR2 DEFAULT NULL,
                               p_delete_lst VARCHAR2 DEFAULT NULL,
                               P_OUT_MSG    OUT VARCHAR2);

  PROCEDURE pz_confirm_biographic(P_ID      SPRIDEN.SPRIDEN_ID%TYPE,
                                  p_indtype VARCHAR2,
                                  P_OUT_MSG OUT VARCHAR2);

  PROCEDURE pz_get_biographic_info(P_ID      SPRIDEN.SPRIDEN_ID%TYPE,
                                   P_ROLE    VARCHAR2 DEFAULT 'STUDENT',
                                   P_XML_BIO OUT CLOB);

end ME_BIOGRAPHIC;
/

