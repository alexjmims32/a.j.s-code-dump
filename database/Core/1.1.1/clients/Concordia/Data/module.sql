prompt Importing table module...
set feedback off
set define off
insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 1, 'PROFILE', 'My Info', 'Y', 'Y', 'student-profile.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 2, 'EVENTS', 'Calendar', 'N', 'Y', 'events.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 3, 'NEWS', 'News', 'N', 'Y', 'campus-news.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 4, 'DINSERV', 'Dining Services', 'N', 'Y', 'food.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 5, 'IMPNUM', 'Contact Info', 'N', 'Y', 'dir.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 6, 'EMER', 'Emergency Info', 'N', 'N', 'dir.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 7, 'SMEDIA', 'Social Media', 'N', 'Y', 'socialmedia.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 8, 'MAPS', 'Maps', 'N', 'N', 'campus-map.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 9, 'HELP', 'Help/FAQs', 'N', 'Y', 'help.png', 'MAIN');

commit;
prompt Done.

