prompt Importing table help...
set feedback off
set define off

insert into help (CAMPUSCODE, ABOUTTEXT, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values ('CCM', 'Concordia is a private, four-year liberal arts college with an extensive global education program, state-of-the-art science curriculum and a learning environment that fosters a partnership between intellect and faith. We offer more than 60 majors, including 18 honors majors and 12 preprofessional programs in medicine, law, engineering, dentistry and more. Our 2,600+ students benefit from a ''globalized'' curriculum combining significant study abroad, hands-on research and exceptional instruction in nine world languages..<br> We are adding this to to test changes to reflect in main app.<br /><br> Test the changes</br>', 2, 'SUPERADMIN', '20-MAY-13 10.13.23.000000 AM');

insert into help (CAMPUSCODE, ABOUTTEXT, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values ('CCN', 'Concordia is a private, four-year liberal arts college with an extensive global education program, state-of-the-art science curriculum and a learning environment that fosters a partnership between intellect and faith. We offer more than 60 majors, including 18 honors majors and 12 preprofessional programs in medicine, law, engineering, dentistry and more. Our 2,600  students benefit from a ''globalized'' curriculum combining significant study abroad, hands-on research and exceptional instruction in nine world languages.', 1, 'SUPERADMIN', '06-MAR-13 08.11.11.000000 AM');

commit;
prompt Done.

