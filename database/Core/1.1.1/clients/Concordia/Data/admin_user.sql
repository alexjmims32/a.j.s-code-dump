prompt Importing table admin_user...
set feedback off
set define off
insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('mmbreth', 'Martin', 'Breth', 'b40f0d37213c93cd11ff5c5b23e97146', 'Y');

insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('superadmin', 'Admin', 'Super', 'c3VwZXJhZG1pbjpBZG1pbg==', 'Y');

insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('eramstad', 'Erik', 'Ramstad', 'dafbb65961e45a9c84b9abea0d1d722a', 'Y');

insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('NotificationAdmin', 'AdminGroup', 'One', '5f4dcc3b5aa765d61d8327deb882cf99', 'Y');

commit;
prompt Done.
