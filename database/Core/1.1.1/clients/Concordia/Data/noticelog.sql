prompt Importing table noticelog...
set feedback off
set define off
insert into noticelog (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (540, 3, '900019523', 'hello sample notification for test', to_date('29-06-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 1, 'n2njmork2', '28-JUN-13 03.14.12.000000 AM', 0, 'Academics', 'Note-1', to_date('27-06-2013', 'dd-mm-yyyy'));

insert into noticelog (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (520, 2, '900019523', 'Notification-1', to_date('20-06-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'n2njmork2', '19-JUN-13 03.04.23.000000 AM', 0, 'Academics', 'Sample-1', to_date('19-06-2013', 'dd-mm-yyyy'));

insert into noticelog (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (501, 1, '900019523', 'notification', to_date('30-06-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'n2njmork2', '18-JUN-13 08.44.38.000000 AM', 0, 'Sports', 'Test', to_date('18-06-2013', 'dd-mm-yyyy'));

commit;
prompt Done.

