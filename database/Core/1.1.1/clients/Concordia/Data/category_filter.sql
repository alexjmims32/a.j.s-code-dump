prompt Importing table category_filter...
set feedback off
set define off
insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (1, 'Academic Buildings', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (3, 'Athletic Facilities', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (2, 'Administrative Buildings', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (4, 'Campus Grounds', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (5, 'Concordia Language Villages', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (6, 'Dining Facilities', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (7, 'Parking Lots', 'Y');

insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (8, 'Residential Buildings', 'Y');

commit;
prompt Done.

