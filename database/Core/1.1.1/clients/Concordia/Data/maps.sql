prompt Importing table maps...
set feedback off
set define off
insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (42, 'CCM', 'KCC', 'Knutson Campus Center', 'Knutson Campus Center', '', '', 'https://m-test.cord.edu/images/building_images/campus_center.jpg', '-96.769933', '46.865322', 2, 'SUPERADMIN', '29-MAY-13 10.21.49.000000 AM', '624 9th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (45, 'CCM', 'OLIN', 'Olin Art and Communications Center', 'Olin Art and Communications Center', '', '', 'https://m-test.cord.edu/images/building_images/olin.jpg', '-96.77055', '46.863727', 2, 'SUPERADMIN', '29-MAY-13 10.30.10.000000 AM', '1010 6th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (46, 'CCM', 'FFCT', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', '', '', 'https://m-test.cord.edu/images/building_images/comstock_theatre.jpg', '-96.770427', '46.86416', 2, 'SUPERADMIN', '29-MAY-13 10.39.15.000000 AM', '1001 7th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (58, 'CLV', 'NORWAY', 'Skogfjorden, Norwegian Village', 'Skogfjorden, Norwegian Village, Bemidji Site, June-July', '218-586-8730', 'clv@cord.edu', '', '-94.730816', '47.551463', 2, 'SUPERADMIN', '17-MAY-13 04.57.48.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (59, 'CLV', 'SPNSH1', 'El Lago del Bosque, Spanish Village', 'El Lago del Bosque, Spanish Village, Bemidji Site, March-August', '218-586-8930', 'clv@cord.edu', '', '-94.7269', '47.563084', 2, 'SUPERADMIN', '17-MAY-13 04.58.04.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (60, 'CLV', 'PRTUGS', 'Mar e Floresta, Portugese Village', 'Mar e Floresta, Portugese Village, Callaway Site, July-August', '218-375-4021', 'clv@cord.edu', '', '-95.776663', '47.009523', 2, 'SUPERADMIN', '17-MAY-13 04.58.19.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (61, 'CLV', 'CHINA', 'Sen Lin Hu, Chinese Village', 'Sen Lin Hu, Chinese Village, Callaway Site, June-August', '218-375-4021', 'clv@cord.edu', '', '-95.776298', '47.008602', 2, 'SUPERADMIN', '17-MAY-13 04.58.34.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (64, 'CLV', 'RUSSIA', 'Lesnoe Ozero, Russian Village', 'Lesnoe Ozero, Russian Village, Bemidji Site, ', '218-586-8430', 'clv@cord.edu', '', '-94.714910', '47.575447', 2, 'n2njmork2', '04-JUN-13 08.25.23.000000 AM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (66, 'CLV', 'ITALY', 'Lago Del Bosco-Camp Holiday, Italian Village', 'Lago Del Bosco-Camp Holiday, Italian Village, Hackensack Site, July-August', '218-682-2570', 'clv@cord.edu', '', '-94.351954', '46.970513', 2, 'SUPERADMIN', '17-MAY-13 05.04.32.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (67, 'CLV', 'JAPAN', 'Mori no Ike, Japanese Village', 'Mori no Ike, Japanese Village, Dent Site, 40225 Purlieu Rd  Dent, MN 56528, June-August', '218-758-2112', 'clv@cord.edu', '', '-95.774777', '46.547282', 2, 'SUPERADMIN', '17-MAY-13 05.05.07.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (8, 'CCM', 'BRWN', 'Brown Hall', 'Brown Hall', '', '', 'https://m-test.cord.edu/images/building_images/brown_hall.jpg', '-96.771977', '46.864398', 2, 'SUPERADMIN', '29-MAY-13 10.17.57.000000 AM', '515 9th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (15, 'CCM', 'MUS', 'Hvidsten Hall of Music', 'Hvidsten Hall of Music', '', '', 'https://m-test.cord.edu/images/building_images/hvidsten.jpg', '-96.771709', '46.862817', 2, 'SUPERADMIN', '29-MAY-13 10.20.51.000000 AM', '1112 5th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (49, 'CCM', 'POND', 'Prexy''s Pond', 'POND', '', '', 'https://m-test.cord.edu/images/building_images/pond.jpg', '-96.769064', '46.866295', 2, 'SUPERADMIN', '07-JUN-13 06.47.19.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (51, 'CLV', 'ARABIC', 'Al-Waha, Arabic Village', 'Al-Waha, Arabic Village, Bemidji Site, July-August', '218-586-8730', 'clv@cord.edu', '', '-94.730381', '47.552085', 2, 'SUPERADMIN', '17-MAY-13 04.21.25.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (53, 'CLV', 'FINNSH', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site, June-July', '218-586-8830', 'clv@cord.edu', '', '-94.72454', '47.55546', 2, 'SUPERADMIN', '17-MAY-13 04.56.13.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (54, 'CLV', 'FRNCH1', 'Lac du Bois, French Village', 'Lac du Bois, French Village, Bemidji Site, February-August', '218-586-8530', 'clv@cord.edu', '', '-94.738879', '47.564709', 2, 'SUPERADMIN', '17-MAY-13 04.56.36.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (55, 'CLV', 'FRNCH2', 'Les Voyageurs, French Camping', 'Les Voyageurs, French Camping, base camp Bemidji, June-August', '218-586-8600', 'clv@cord.edu', '', '-94.738246', '47.563674', 2, 'SUPERADMIN', '17-MAY-13 04.56.54.000000 PM', '', 'true', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (56, 'CLV', 'FRNCH3', 'Lac du Bois-Camp Holiday', 'Lac du Bois-Camp Holiday, Hackensack Site, June-July', '218-682-2570', 'clv@cord.edu', '', '-94.35304', '46.970332', 2, 'SUPERADMIN', '17-MAY-13 04.57.11.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (62, 'CLV', 'SPNSH2', 'El Lago del Bosque-Wilder Forest, Spanish Village', 'El Lago del Bosque-Wilder Forest, Spanish Village, Marine on St. Croix, June-August', '651-433-4375', 'clv@cord.edu', '', '-92.815601', '45.166487', 2, 'SUPERADMIN', '17-MAY-13 04.58.55.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (63, 'CLV', 'SWEDEN', 'Sjolunden, Swedish Village', 'Sjolunden, Swedish Village, Bemidji Site, July-August', '218-586-8830', 'clv@cord.edu', '', '-94.725688', '47.555547', 2, 'SUPERADMIN', '17-MAY-13 05.03.51.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (65, 'CLV', 'KOREAN', 'Sup sogui Hosu, Korean Village', 'Sup sogui Hosu, Korean Village, Bemidji Site, July-August', '218-586-8430', 'clv@cord.edu', '', '-94.715828', '47.575994', 2, 'SUPERADMIN', '17-MAY-13 05.03.13.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (69, 'CLV', 'CLVMHD', 'Concordia Language Villages, Moorhead Administration', 'Concordia Language Villages, Moorhead Administrative Offices at Riverside Center', '218-299-4544', 'clv@cord.edu', '', '-96.773959', '46.860870', 2, 'SUPERADMIN', '17-MAY-13 03.53.49.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (70, 'CLV', 'CLVBMJ', 'Concordia Language Villages, Bemidji Administration', 'Concordia Language Villages, Bemidji Administrative Offices at Centrum', '218-586-8600', 'clv@cord.edu', '', '-94.728515', '47.550062', 2, 'SUPERADMIN', '17-MAY-13 03.55.12.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (32, 'CCM', 'GCTR', 'Grant Center', 'Grant Center', '', '', 'https://m-test.cord.edu/images/building_images/grant_center.jpg', '-96.767235', '46.86113', 2, 'SUPERADMIN', '29-MAY-13 10.16.22.000000 AM', '1300 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (41, 'CCM', 'JONE', 'Jones Science Center', 'Jones Science Center', '', '', 'https://m-test.cord.edu/images/building_images/jones.jpg', '-96.76908', '46.863492', 2, 'SUPERADMIN', '29-MAY-13 10.15.53.000000 AM', '1021 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'Bogstad East', '', '', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'n2njmork2', '16-JUN-13 06.59.01.000000 AM', '617 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (50, 'CCM', 'BELL', 'Bell Tower', 'Bell Tower ', '', '', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.769434', '46.864529', 2, 'n2njmork2', '19-JUN-13 03.40.19.000000 AM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (52, 'CLV', 'DANISH', 'Skovsoen, Danish Village', 'Skovsoen, Danish Village, Bemidji Site, June-July', '218-586-8820', 'clv@cord.edu', '', '-94.724443', '47.55597', 2, 'SUPERADMIN', '17-MAY-13 04.55.58.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (57, 'CLV', 'GERMAN', 'Waldsee, German Village', 'Waldsee, German Village, Bemidji Site, February-August', '218-586-8630', 'clv@cord.edu', '', '-94.738787', '47.559402', 2, 'SUPERADMIN', '17-MAY-13 04.57.32.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site, June-August', '218-335-6304', 'clv@cord.edu', '', '-94.638231', '47.465051', 2, 'SUPERADMIN', '17-MAY-13 05.05.36.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (14, 'CCM', 'MPOC', 'Mugaas Plant Operation Center', 'Mugaas Plant Operation Center', '', '', 'https://m-test.cord.edu/images/building_images/mugaas.jpg', '-96.771333', '46.865928', 2, 'SUPERADMIN', '08-JUN-13 09.27.22.000000 PM', '801 6th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (24, 'CCM', 'SWIM', 'Swimming Pool', 'Swimming Pool', '', '', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', '14-JUN-13 06.30.46.000000 AM', '810 12th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (38, 'CCM', 'IC06', 'International Apartments 1106', 'International Apartments 1106', '', '', 'https://m-test.cord.edu/images/building_images/international_apartments_1106.jpg', '-96.767272', '46.862806', 2, 'SUPERADMIN', '08-JUN-13 09.41.53.000000 PM', '1106 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center Riverside Center ', '123-123-1234', 'bhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', '15-JUN-13 05.55.44.000000 AM', '310 14th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (28, 'CCM', 'CC ECO', 'Eco House', 'Eco House', '', '', 'https://m-test.cord.edu/images/building_images/eco_house.jpg', '-96.770588', '46.867607', 2, 'SUPERADMIN', '07-JUN-13 06.44.45.000000 PM', '618 South 6th St, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (29, 'CCM', 'ERIC', 'Erickson Hall', 'Erickson Hall', '', '', 'https://m-test.cord.edu/images/building_images/erickson_hall.jpg', '-96.766891', '46.860499', 2, 'SUPERADMIN', '07-JUN-13 06.51.14.000000 PM', '1320 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (48, 'CCM', 'PRES', 'President''s Residence', 'President''s Residence', '', '', 'https://m-test.cord.edu/images/building_images/presidents_residence.jpg', '-96.768458', '46.866997', 2, 'SUPERADMIN', '08-JUN-13 09.36.48.000000 PM', '', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (11, 'CCM', 'LIV', 'Livedalen Hall', 'Livedalen Hall', '', '', 'https://m-test.cord.edu/images/building_images/livedalen_hall.jpg', '-96.771575', '46.864138', 2, 'SUPERADMIN', '07-JUN-13 07.02.02.000000 PM', '1002 5th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (12, 'CCM', 'LORE', 'Lorentzsen Hall', 'Lorentzsen Hall', '', '', 'https://m-test.cord.edu/images/building_images/lorentzsen_hall.jpg', '-96.768731', '46.864743', 2, 'SUPERADMIN', '08-JUN-13 09.25.58.000000 PM', '901 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (16, 'CCM', 'NORM', 'Normandy/Bookstore', 'Normandy/Bookstore', '', '', 'https://m-test.cord.edu/images/building_images/bookstore.jpg', '-96.771183', '46.864314', 2, 'SUPERADMIN', '29-MAY-13 10.29.31.000000 AM', '929 6th St S, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (17, 'CCM', 'OFC', 'Offutt Concourse', 'Offutt Concourse', '', '', 'https://m-test.cord.edu/images/building_images/offutt_concourse.jpg', '-96.769445', '46.862788', 2, 'SUPERADMIN', '08-JUN-13 09.28.53.000000 PM', '1111 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (18, 'CCM', 'OF', 'Olson Forum', 'Olson Forum', '', '', 'https://m-test.cord.edu/images/building_images/olson_forum.jpg', '-96.768876', '46.862678', 2, 'SUPERADMIN', '29-MAY-13 10.12.29.000000 AM', '1111 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (19, 'CCM', 'OLDM', 'Old Main', 'Old Main', '', '', 'https://m-test.cord.edu/images/building_images/old_main.jpg', '-96.768898', '46.865583', 2, 'SUPERADMIN', '29-MAY-13 10.13.55.000000 AM', '815 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (22, 'CCM', 'SKY', 'Olson Skyway', 'Olson Skyway', '', '', 'https://m-test.cord.edu/images/building_images/olson_skyway.jpg', '-96.767814', '46.861728', 2, 'SUPERADMIN', '07-JUN-13 06.46.31.000000 PM', '1101 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (23, 'CCM', 'STAD', 'Jake Christiansen Stadium', 'Jake Christiansen Stadium', '', '', 'https://m-test.cord.edu/images/building_images/jake_christiansen_side.jpg', '-96.765829', '46.859017', 2, 'SUPERADMIN', '29-MAY-13 10.20.17.000000 AM', '1500 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (25, 'CCM', 'TE', 'Townhouse East', 'Townhouse East', '', '', 'https://m-test.cord.edu/images/building_images/townhouse_east.jpg', '-96.765121', '46.861541', 2, 'SUPERADMIN', '07-JUN-13 06.42.57.000000 PM', '917 12th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (26, 'CCM', 'TW', 'Townhouse West', 'Townhouse West', '', '', 'https://m-test.cord.edu/images/building_images/townhouse_west.jpg', '-96.765904', '46.861592', 2, 'SUPERADMIN', '07-JUN-13 06.41.08.000000 PM', '915 12th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (27, 'CCM', 'WELC', 'Welcome Center', 'Welcome Center', '', '', 'https://m-test.cord.edu/images/building_images/welcome_center.jpg', '-96.767514', '46.862473', 2, 'SUPERADMIN', '29-MAY-13 10.19.43.000000 AM', '1118 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (43, 'CCM', 'MP', 'Car Pool', 'Car Pool', '', '', 'https://m-test.cord.edu/images/building_images/car_pool.jpg', '-96.76614', '46.859527', 2, 'SUPERADMIN', '07-JUN-13 06.41.58.000000 PM', '1402 9th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (3, 'CCM', 'ADVCTR', 'Advancement Center', 'Advancement Center', '', '', 'https://m-test.cord.edu/images/building_images/advancement_center.jpg', '-96.767337', '46.863228', 2, 'SUPERADMIN', '07-JUN-13 06.38.34.000000 PM', '1022 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', 'Berg Steam Plant', '', '', 'https://m-test.cord.edu/images/building_images/berg_steam_plant.jpg', '-96.770791', '46.865884', 2, 'superadmin', '23-AUG-13 05.46.46.000000 AM', '812 6th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'Aasgaard House', '', '', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'superadmin', '23-AUG-13 08.08.59.000000 AM', '714 7th Street South, Moorhead, MN, US', 'false', '2');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', '', '', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', '23-AUG-13 08.08.47.000000 AM', '821 7th Street South, Moorhead, MN, US', 'false', '2');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (10, 'CCM', 'LIB', 'Carl B. Ylvisaker Library', 'Carl B. Ylvisaker Library', '', '', 'https://m-test.cord.edu/images/building_images/library.jpg', '-96.769761', '46.864593', 2, 'SUPERADMIN', '29-MAY-13 10.22.04.000000 AM', '1010 10th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (7, 'CCM', 'BOGM', 'Bogstad Manor', 'Bogstad Manor', '', '', 'https://m-test.cord.edu/images/building_images/bogstad_manor.jpg', '-96.769059', '46.867864', 2, 'SUPERADMIN', '29-MAY-13 10.15.27.000000 AM', '618 7th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (5, 'CCM', 'BOEO', 'Boe-Olsen Apartments', 'Boe-Olsen Apartments', '', '', 'https://m-test.cord.edu/images/building_images/boe_olsen_apartments.jpg', '-96.767492', '46.863646', 2, 'SUPERADMIN', '07-JUN-13 06.52.31.000000 PM', '801 South 10th Avenue, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (9, 'CCM', 'BW', 'Bishop Whipple Hall', 'Bishop Whipple Hall', '', '', 'https://m-test.cord.edu/images/building_images/bishop_whipple.jpg', '-96.770089', '46.865942', 2, 'SUPERADMIN', '29-MAY-13 10.18.25.000000 AM', '801 7th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (20, 'CCM', 'PARK', 'Park Region Hall', 'Park Region Hall', '', '', 'https://m-test.cord.edu/images/building_images/park_region.jpg', '-96.769338', '46.867035', 2, 'SUPERADMIN', '29-MAY-13 10.13.27.000000 AM', '701 7th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (30, 'CCM', 'FH', 'Memorial Auditorium', 'Memorial Auditorium', '', '', 'https://m-test.cord.edu/images/building_images/memorial.jpg', '-96.770078', '46.862935', 2, 'SUPERADMIN', '29-MAY-13 10.22.27.000000 AM', '1117 7th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (31, 'CCM', 'FJEL', 'Fjelstad Hall', 'Fjelstad Hall', '', '', 'https://m-test.cord.edu/images/building_images/fjelstad.jpg', '-96.770228', '46.86654', 2, 'SUPERADMIN', '29-MAY-13 10.17.32.000000 AM', '720 6th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (33, 'CCM', 'GROS', 'Grose Hall', 'Grose Hall', '', '', 'https://m-test.cord.edu/images/building_images/grose_hall.jpg', '-96.770078', '46.865744', 2, 'SUPERADMIN', '07-JUN-13 06.56.59.000000 PM', '811 7th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (34, 'CCM', 'HAL', 'Hallett Hall', 'Hallett Hall', '', '', 'https://m-test.cord.edu/images/building_images/hallet_hall.jpg', '-96.767071', '46.861449', 2, 'n2njmork2', '04-JUN-13 08.25.00.000000 AM', '815 12th Avenue South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (35, 'CCM', 'HLTH', 'Kjos Health Center', 'Kjos Health Center', '', '', 'https://m-test.cord.edu/images/building_images/kjos_health_center.jpg', '-96.771081', '46.863687', 2, 'SUPERADMIN', '08-JUN-13 09.30.18.000000 PM', '1011 6th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (36, 'CCM', 'HOY', 'Hoyum Hall', 'Hoyum Hall', '', '', 'https://m-test.cord.edu/images/building_images/hoyum_hall.jpg', '-96.771116', '46.863399', 2, 'SUPERADMIN', '07-JUN-13 07.04.52.000000 PM', '1021 6th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (37, 'CCM', 'IC02', 'International Apartments 1102', 'International Apartments 1102', '', '', 'https://m-test.cord.edu/images/building_images/international_apartments_1102.jpg', '-96.767535', '46.862931', 2, 'SUPERADMIN', '07-JUN-13 07.00.10.000000 PM', '1102 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (39, 'CCM', 'IC10', 'International Apartments 1110', 'International Apartments 1110', '', '', 'https://m-test.cord.edu/images/building_images/international_apartments_1110.jpg', '-96.767546', '46.862711', 2, 'SUPERADMIN', '08-JUN-13 09.41.31.000000 PM', '1110 8th Street South, Moorhead, MN, US', 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (40, 'CCM', 'IVER', 'Ivers Science Building', 'Ivers Science Building', '', '', 'https://m-test.cord.edu/images/building_images/ivers.jpg', '-96.768914', '46.863885', 2, 'SUPERADMIN', '29-MAY-13 10.20.33.000000 AM', '1001 8th Street South, Moorhead, MN, US', 'false', '1');

commit;
prompt Done.

