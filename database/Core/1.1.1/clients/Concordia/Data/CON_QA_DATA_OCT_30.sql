﻿set feedback off
set define off
prompt Loading ADMIN_PRIV...
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'FEEDS', 'Y', 'FEEDS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'MAPS', 'Y', 'MAPS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'ROLES', 'Y', 'ROLES Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'FEEDS', 'Y', 'FEEDS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'MAPS', 'Y', 'MAPS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'ROLES', 'Y', 'ROLES Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', null);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'FEEDS', 'Y', 'FEEDS Module Access', 2);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'MAPS', 'Y', 'MAPS Module Access', 3);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', 4);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', 6);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', 8);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'ROLES', 'Y', 'ROLES Module Access', 5);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', 9);
insert into ADMIN_PRIV (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'USERS', 'Y', 'USERS Module Access', 10);
commit;
prompt 25 records loaded
prompt Loading ADMIN_ROLE...
prompt Table is empty
prompt Loading ADMIN_USER...
insert into ADMIN_USER (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('mmbreth', 'Martin', 'Breth', 'b40f0d37213c93cd11ff5c5b23e97146', 'Y');
insert into ADMIN_USER (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('superadmin', 'Admin', 'Super', 'c3VwZXJhZG1pbjpBZG1pbg==', 'Y');
insert into ADMIN_USER (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('eramstad', 'Erik', 'Ramstad', 'dafbb65961e45a9c84b9abea0d1d722a', 'Y');
insert into ADMIN_USER (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('NotificationAdmin', 'AdminGroup', 'One', '5f4dcc3b5aa765d61d8327deb882cf99', 'Y');
commit;
prompt 4 records loaded
prompt Loading CAMPUS...
insert into CAMPUS (CODE, TITLE, DESCRIPTION, CLIENT, PRINCIPALNAME, WEBSITE)
values ('CLV', 'Concordia Language Villages', 'Concordia Language Villages', 'CCM', null, null);
insert into CAMPUS (CODE, TITLE, DESCRIPTION, CLIENT, PRINCIPALNAME, WEBSITE)
values ('CCM', 'Concordia College', 'CCM Main Campus', 'CCM', null, null);
commit;
prompt 2 records loaded
prompt Loading CATEGORY_FILTER...
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (1, 'Academic Buildings', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (3, 'Athletic Facilities', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (2, 'Administrative Buildings', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (4, 'Campus Grounds', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (5, 'Concordia Language Villages', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (6, 'Dining Facilities', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (7, 'Parking Lots', 'Y');
insert into CATEGORY_FILTER (ID, CATEGORY, SHOW_BY_DEFAULT)
values (8, 'Residential Buildings', 'Y');
commit;
prompt 8 records loaded
prompt Loading EMERGENCY_CONTACTS...
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (233, 'CLV', 'ZZZZZZAAAA', '123-123-2484', null, null, null, 'IMPNUM', 2, 'superadmin', to_timestamp('28-10-2013 05:55:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 229);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (234, 'CLV', 'Moorhead Police Department', '218-299-0911', '915 9th Ave N, Moorhead,MN', null, null, 'EMR', 4, 'superadmin', to_timestamp('03-10-2013 08:07:39.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'For fires, ambulance or police.', 230);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (1, 'CCM', 'Office of the President, Lorentzsen', '218-299-3000', null, null, null, 'IMPNUM', 2, 'eramstad', to_timestamp('18-06-2013 15:28:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 1);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (2, 'CCM', 'Academic Affairs -Office of the Provost — Lorentzsen', '218-299-3606', null, null, null, 'IMPNUM', null, null, null, null, 0);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (3, 'CCM', 'Academic Affairs -Arts and Sciences', '218-299-4541', null, null, null, 'IMPNUM', 2, 'superadmin', to_timestamp('22-10-2013 10:42:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 2);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (4, 'CCM', 'Academic Affairs -Academic Division Chairs', '218-299-3606', null, null, null, 'IMPNUM', 2, 'superadmin', to_timestamp('22-10-2013 10:40:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 3);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (5, 'CCM', 'Academic Affairs -Core and Advising', '218-299-4541', null, null, null, 'IMPNUM', null, null, null, null, 4);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (6, 'CCM', 'Academic Affairs -Foundation Relations and Research Grants – Advancement Center', '218-299-4981', null, null, null, 'IMPNUM', null, null, null, null, 5);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (7, 'CCM', 'Academic Affairs -Sustainability', '218-299-3071', null, null, null, 'IMPNUM', null, null, null, null, 6);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (8, 'CCM', 'Academic Affairs -Undergraduate Research, Scholarship and National Fellowships', '218-299-3252', null, null, null, 'IMPNUM', null, null, null, null, 7);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (9, 'CCM', 'Academic Assessment (see Institutional Research and Assessment)', '218-299-4723', null, null, null, 'IMPNUM', null, null, null, null, 8);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (10, 'CCM', 'Academic Enhancement and Writing Center — Fjelstad Hall B10 (lower level)', '218-299-4551', null, null, null, 'IMPNUM', null, null, null, null, 9);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (11, 'CCM', 'Admissions (see Enrollment)', '218-299-3004', null, null, null, 'IMPNUM', null, null, null, null, 10);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (13, 'CCM', 'Advancement -Office of Advancement ', '218-299-3733', null, null, null, 'IMPNUM', null, null, null, null, 11);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (14, 'CCM', 'Advancement -Office of Alumni Relations ', '218-299-3734', null, null, null, 'IMPNUM', null, null, null, null, 12);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (15, 'CCM', 'Advancement -Office of Annual Fund ', '218-299-3454', null, null, null, 'IMPNUM', null, null, null, null, 13);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (16, 'CCM', 'Advancement -Toll free (Advancement/Annual Fund) ', '800-699-9896', null, null, null, 'IMPNUM', null, null, null, null, 14);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (17, 'CCM', 'Advancement -Toll free (Alumni Relations) ', '800-699-9020', null, null, null, 'IMPNUM', null, null, null, null, 15);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (18, 'CCM', 'Anderson Office of Career Success - Grant Center', '218-299-3486', null, null, null, 'IMPNUM', null, null, null, null, 16);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (19, 'CCM', 'Archives — Library', '218-299-3241', null, null, null, 'IMPNUM', null, null, null, null, 17);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (20, 'CCM', 'Art — Olin (second floor)', '218-299-4623', null, null, null, 'IMPNUM', null, null, null, null, 18);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (22, 'CCM', 'Athletics -Memorial Auditorium Office ', '218-299-4434', null, null, null, 'IMPNUM', null, null, null, null, 19);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (23, 'CCM', 'Athletics -Equipment Room, Memorial Auditorium ', '218-299-3209', null, null, null, 'IMPNUM', null, null, null, null, 20);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (24, 'CCM', 'Athletics -Intramurals ', '218-299-4921', null, null, null, 'IMPNUM', null, null, null, null, 21);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (25, 'CCM', 'Athletics -Locker Room, Jake Christiansen Stadium ', '218-359-0283', null, null, null, 'IMPNUM', null, null, null, null, 22);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (26, 'CCM', 'Athletics -Olson Forum Desk ', '218-299-4310', null, null, null, 'IMPNUM', null, null, null, null, 23);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (27, 'CCM', 'Athletics -Press Box, Stadium ', '218-359-0285', null, null, null, 'IMPNUM', null, null, null, null, 24);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (28, 'CCM', 'Athletics -Sports Information ', '218-299-3194', null, null, null, 'IMPNUM', null, null, null, null, 25);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (29, 'CCM', 'Athletics -Swimming Pool ', '218-299-3591', null, null, null, 'IMPNUM', null, null, null, null, 26);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (30, 'CCM', 'Biology — Jones Science Center (second floor)', '218-299-3085', null, null, null, 'IMPNUM', null, null, null, null, 27);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (31, 'CCM', 'Bookstore (see Cobber Bookstore)', '218-299-3017', null, null, null, 'IMPNUM', null, null, null, null, 28);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (32, 'CCM', 'Business Office — Lorentzsen', '218-299-3150', null, null, null, 'IMPNUM', null, null, null, null, 29);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (33, 'CCM', 'Business (see Offutt School of Business)', '218-299-4411', null, null, null, 'IMPNUM', null, null, null, null, 30);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (34, 'CCM', 'Campus Information — Knutson Campus Center', '218-299-4000', null, null, null, 'IMPNUM', null, null, null, null, 31);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (35, 'CCM', 'Campus Lights (see Communication Studies and Theatre Art)', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 32);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (36, 'CCM', 'Campus Ministry Office (see Ministry)', '218-299-4161', null, null, null, 'IMPNUM', null, null, null, null, 33);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (37, 'CCM', 'Career Center — Academy', '218-299-3020', null, null, null, 'IMPNUM', null, null, null, null, 34);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (38, 'CCM', 'Cooperative Education', '218-299-3492', null, null, null, 'IMPNUM', null, null, null, null, 35);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (39, 'CCM', 'Job Shop ', '218-299-4509', null, null, null, 'IMPNUM', null, null, null, null, 36);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (40, 'CCM', 'Chemistry — Ivers (third floor)', '218-299-3101', null, null, null, 'IMPNUM', null, null, null, null, 37);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (41, 'CCM', 'Chinese — Bishop Whipple (third floor)', '218-299-3106', null, null, null, 'IMPNUM', null, null, null, null, 38);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (42, 'CCM', 'Church Relations (see Vocation and Church Leadership)', '218-299-3146', null, null, null, 'IMPNUM', null, null, null, null, 39);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (43, 'CCM', 'Classical Studies — Bishop Whipple (first floor)', '218-299-3946', null, null, null, 'IMPNUM', null, null, null, null, 40);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (44, 'CCM', 'Clinical Laboratory Science — Jones Science Center', '218-299-3797', null, null, null, 'IMPNUM', null, null, null, null, 41);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (45, 'CCM', 'Cobber Bookstore', '218-299-3017', null, null, null, 'IMPNUM', null, null, null, null, 42);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (46, 'CCM', 'Cobber Bookstore - Toll Free', '800-828-6409', null, null, null, 'IMPNUM', null, null, null, null, 43);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (47, 'CCM', 'Cobber Kids — Riverside Center', '218-299-4204', null, null, null, 'IMPNUM', null, null, null, null, 44);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (48, 'CCM', 'Cobber Kids -Preschool Room', '218-299-4198', null, null, null, 'IMPNUM', null, null, null, null, 45);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (49, 'CCM', 'Cobber Kids -Toddlers Room', '218-299-4951', null, null, null, 'IMPNUM', null, null, null, null, 46);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (50, 'CCM', 'Communication Studies and Theatre Art (CSTA) — Olin (third floor)', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 47);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (51, 'CCM', 'Communications and Marketing, Office of — Aasgaard', '218-299-3147', null, null, null, 'IMPNUM', null, null, null, null, 48);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (52, 'CCM', 'Comm -Media Relations', '218-299-3642', null, null, null, 'IMPNUM', null, null, null, null, 49);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (53, 'CCM', 'Comm -Online Communications — Riverside Center', '218-299-3939', null, null, null, 'IMPNUM', null, null, null, null, 50);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (54, 'CCM', 'Comm -Photo Studio – Mugaas  ', '218-299-3938', null, null, null, 'IMPNUM', null, null, null, null, 51);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (55, 'CCM', 'Comm -Print Shop — Mugaas', '218-299-3429', null, null, null, 'IMPNUM', null, null, null, null, 52);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (56, 'CCM', 'Comm -Publications', '218-299-4957', null, null, null, 'IMPNUM', null, null, null, null, 53);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (57, 'CCM', 'Comm -Design', '218-299-4948', null, null, null, 'IMPNUM', 2, 'n2njmork2', to_timestamp('16-06-2013 09:01:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 54);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (58, 'CCM', 'Comm -Sports Information - Memorial Auditorium', '218-299-3194', null, null, null, 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('05-06-2013 10:23:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 55);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (59, 'CCM', 'Communiversity (see F/M Communiversity)', '218-299-3438', null, null, null, 'IMPNUM', null, null, null, null, 56);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (60, 'CCM', 'Computer Science (see Offutt School of Business)', '218-299-4411', null, null, null, 'IMPNUM', null, null, null, null, 57);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (61, 'CCM', 'Concordia Language Villages Bemidji', '218-586-8600', null, null, null, 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('05-06-2013 10:22:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 58);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (62, 'CCM', 'Concordia Language Villages — Bemidji -Toll Free', '800-450-2214', null, null, null, 'IMPNUM', null, null, null, null, 59);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (63, 'CCM', 'Concordia Language Villages — The Forest', '651-433-4375', null, null, null, 'IMPNUM', null, null, null, null, 60);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (64, 'CCM', 'Concordia Language Villages — Moorhead', '218-299-4544', null, null, null, 'IMPNUM', null, null, null, null, 61);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (65, 'CCM', 'Concordia Language Villages — Moorhead -Toll Free', '800-222-4750', null, null, null, 'IMPNUM', null, null, null, null, 62);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (66, 'CCM', 'Concordia Language Villages — St. Paul', '651-647-4357', null, null, null, 'IMPNUM', null, null, null, null, 63);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (67, 'CCM', 'Concordia On-Air (student television) — Olin', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 64);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (68, 'CCM', 'Concordian, The (student newspaper) — Fjelstad Hall (lower level)', '218-299-3826', null, null, null, 'IMPNUM', null, null, null, null, 65);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (69, 'CCM', 'Counseling Center — Academy', '218-299-3514', null, null, null, 'IMPNUM', null, null, null, null, 66);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (70, 'CCM', 'Credo – Lorentzsen ', '218-299-4959', null, null, null, 'IMPNUM', null, null, null, null, 67);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (71, 'CCM', 'CSTA (see Communication Studies and Theatre Art)', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 68);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (72, 'CCM', 'Cultural Events and Music Organizations — Riverside Center', '218-299-4366', null, null, null, 'IMPNUM', null, null, null, null, 69);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (73, 'CCM', 'Music Events and Recordings Information Line ', '218-299-4515', null, null, null, 'IMPNUM', null, null, null, null, 70);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (74, 'CCM', 'Dining Services — Knutson Campus Center', '218-299-3706', null, null, null, 'IMPNUM', null, null, null, null, 71);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (75, 'CCM', 'Dining Services -Office ', '218-299-3706', null, null, null, 'IMPNUM', null, null, null, null, 72);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (76, 'CCM', 'Dining Services -After 5:30 p.m./weekends ', '218-299-3463', null, null, null, 'IMPNUM', null, null, null, null, 73);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (77, 'CCM', 'Dining Services -Anderson Commons (checker) ', '218-299-3451', null, null, null, 'IMPNUM', null, null, null, null, 74);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (78, 'CCM', 'Dining Services -Anderson Commons (kitchen) ', '218-299-3463', null, null, null, 'IMPNUM', null, null, null, null, 75);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (79, 'CCM', 'Dining Services -Catering ', '218-299-4271', null, null, null, 'IMPNUM', null, null, null, null, 76);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (80, 'CCM', 'Dining Services -Coffee Stop ', '218-299-4356', null, null, null, 'IMPNUM', null, null, null, null, 77);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (81, 'CCM', 'Dining Services -Knutson Kitchen/Bakery ', '218-299-3593', null, null, null, 'IMPNUM', null, null, null, null, 78);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (82, 'CCM', 'Dining Services -Knutson Production ', '218-299-4205', null, null, null, 'IMPNUM', null, null, null, null, 79);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (83, 'CCM', 'Dining Services -Korn Krib ', '218-299-3005', null, null, null, 'IMPNUM', null, null, null, null, 80);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (84, 'CCM', 'Dining Services -The Maize ', '218-299-3563', null, null, null, 'IMPNUM', null, null, null, null, 81);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (85, 'CCM', 'Dining Services -Vending ', '218-299-3915', null, null, null, 'IMPNUM', null, null, null, null, 82);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (86, 'CCM', 'Disability Services (see Counseling Center)', '218-299-3514', null, null, null, 'IMPNUM', null, null, null, null, 83);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (87, 'CCM', 'Dovre Center for Faith and Learning', '218-299-3430', null, null, null, 'IMPNUM', null, null, null, null, 84);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)Old Main (First floor)Old M1', null, null, 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:17:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)', 85);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (176, 'CCM', 'Physics — Ivers (first floor)', '218-299-4413', null, null, null, 'IMPNUM', null, null, null, null, 86);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (177, 'CCM', 'Physics - Observatory ', '218-299-3896', null, null, null, 'IMPNUM', null, null, null, null, 87);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (178, 'CCM', 'Political Science — Old Main (third floor)', '218-299-3501', null, null, null, 'IMPNUM', null, null, null, null, 88);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (179, 'CCM', 'Post Office — Knutson Campus Center (mezzanine)', '218-299-3770', null, null, null, 'IMPNUM', null, null, null, null, 89);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (180, 'CCM', 'Print Shop (see Communications and Marketing)', '218-299-3429', null, null, null, 'IMPNUM', null, null, null, null, 90);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (181, 'CCM', 'Psychology — Ivers (second floor)', '218-299-4030', null, null, null, 'IMPNUM', null, null, null, null, 91);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (182, 'CCM', 'Public Safety — Knutson Campus Center', '218-299-3123', null, null, null, 'IMPNUM', null, null, null, null, 92);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (183, 'CCM', 'Public Safety SAFE Walk ', '218-299-3123', null, null, null, 'IMPNUM', null, null, null, null, 93);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (184, 'CCM', 'Registrar’s Office — Lorentzsen', '218-299-3250', null, null, null, 'IMPNUM', null, null, null, null, 94);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (185, 'CCM', 'Religion — Academy (second floor)', '218-299-3334', null, null, null, 'IMPNUM', null, null, null, null, 95);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (186, 'CCM', 'Residence Life — Knutson Campus Center', '218-299-3872', null, null, null, 'IMPNUM', null, null, null, null, 96);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (187, 'CCM', 'Residence Halls', null, null, null, null, 'IMPNUM', null, null, null, null, 97);
commit;
prompt 100 records committed...
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (188, 'CCM', 'Boe-Olsen Apartments -Director', '218-299-3899', null, null, null, 'IMPNUM', null, null, null, null, 98);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (189, 'CCM', 'Bogstad East Apartment -Director', '218-299-3899', null, null, null, 'IMPNUM', null, null, null, null, 99);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (190, 'CCM', 'Bogstad Manor Apartment -Director', '218-299-3899', null, null, null, 'IMPNUM', null, null, null, null, 100);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (191, 'CCM', 'Brown -Director', '218-299-4485', null, null, null, 'IMPNUM', null, null, null, null, 101);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (192, 'CCM', 'Erickson -Director', '218-299-4697', null, null, null, 'IMPNUM', null, null, null, null, 102);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (193, 'CCM', 'Erickson -Desk ', '218-299-4703', null, null, null, 'IMPNUM', null, null, null, null, 103);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (194, 'CCM', 'Fjelstad -Director', '218-299-4359', null, null, null, 'IMPNUM', null, null, null, null, 104);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (195, 'CCM', 'Fjelstad -Desk', '218-299-3564', null, null, null, 'IMPNUM', null, null, null, null, 105);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (196, 'CCM', 'Hallett -Director', '218-299-3940', null, null, null, 'IMPNUM', null, null, null, null, 106);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (197, 'CCM', 'Hallett -Desk', '218-299-3535', null, null, null, 'IMPNUM', null, null, null, null, 107);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (198, 'CCM', 'Hoyum -Director', '218-299-3319', null, null, null, 'IMPNUM', null, null, null, null, 108);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (199, 'CCM', 'Hoyum -Desk', '218-299-4547', null, null, null, 'IMPNUM', null, null, null, null, 109);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (200, 'CCM', 'International Center -Director', '218-299-3899', null, null, null, 'IMPNUM', null, null, null, null, 110);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (201, 'CCM', 'Livedalen -Director', '218-299-3722', null, null, null, 'IMPNUM', null, null, null, null, 111);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (202, 'CCM', 'Livedalen -Desk', '218-299-3336', null, null, null, 'IMPNUM', null, null, null, null, 112);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (203, 'CCM', 'Park Region -Director', '218-299-4359', null, null, null, 'IMPNUM', null, null, null, null, 113);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (204, 'CCM', 'Park Region -Desk', '218-299-4219', null, null, null, 'IMPNUM', null, null, null, null, 114);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (205, 'CCM', 'Townhouse East/Townhouse West -Director', '218-299-4697', null, null, null, 'IMPNUM', null, null, null, null, 115);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (206, 'CCM', 'Risk Management — Lorentzsen', '218-299-3682', null, null, null, 'IMPNUM', null, null, null, null, 116);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (207, 'CCM', 'Scandinavian Studies — Bishop Whipple (third floor)', '218-299-4462', null, null, null, 'IMPNUM', null, null, null, null, 117);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (208, 'CCM', 'Security (see Public Safety)', '218-299-3123', null, null, null, 'IMPNUM', null, null, null, null, 118);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (209, 'CCM', 'Sociology and Social Work — Old Main (third floor)', '218-299-3501', null, null, null, 'IMPNUM', null, null, null, null, 119);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (210, 'CCM', 'Solution Center (see Information Technology Services)', '218-299-3375', null, null, null, 'IMPNUM', null, null, null, null, 120);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (211, 'CCM', 'Spanish and Hispanic Studies — Academy (first floor)', '218-299-3754', null, null, null, 'IMPNUM', null, null, null, null, 121);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (212, 'CCM', 'Sports Information (see Athletics)', '218-299-3194', null, null, null, 'IMPNUM', null, null, null, null, 122);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (213, 'CCM', 'Student Affairs, Office of — Knutson Campus Center', '218-299-3455', null, null, null, 'IMPNUM', null, null, null, null, 123);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (214, 'CCM', 'Student Government Association — Knutson Campus Center', '218-299-4518', null, null, null, 'IMPNUM', null, null, null, null, 124);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (215, 'CCM', 'SGA -Student Government Association ', '218-299-4507', null, null, null, 'IMPNUM', null, null, null, null, 125);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (216, 'CCM', 'SGA -Campus Entertainment Commission ', '218-299-4246', null, null, null, 'IMPNUM', null, null, null, null, 126);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (217, 'CCM', 'SGA -Campus Ministry Commission ', '218-299-4245', null, null, null, 'IMPNUM', null, null, null, null, 127);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (218, 'CCM', 'SGA -Campus Service Commission ', '218-299-4167', null, null, null, 'IMPNUM', null, null, null, null, 128);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (219, 'CCM', 'Student Leadership and Service — Knutson Campus Center', '218-299-3640', null, null, null, 'IMPNUM', null, null, null, null, 129);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (220, 'CCM', 'Student Success and Retention, Office of - Fjelstad Hall B02 (lower level)', '218-299-3045', null, null, null, 'IMPNUM', null, null, null, null, 130);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (221, 'CCM', 'Peer Mentoring Office – Fjelstad Hall B03 (lower level)', '218-299-3040', null, null, null, 'IMPNUM', null, null, null, null, 131);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (222, 'CCM', 'Summer Programs, Office of — Riverside Center', '218-299-3566', null, null, null, 'IMPNUM', null, null, null, null, 132);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (223, 'CCM', 'Sustainability (see Office of the Provost)', '218-299-3071', null, null, null, 'IMPNUM', null, null, null, null, 133);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (224, 'CCM', 'Theatre, Frances Frazier Comstock', '218-299-3314', null, null, null, 'IMPNUM', null, null, null, null, 134);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (225, 'CCM', 'Theatre Box Office ', '218-299-3314', null, null, null, 'IMPNUM', null, null, null, null, 135);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (226, 'CCM', 'Theatre Workshop ', '218-299-3821', null, null, null, 'IMPNUM', null, null, null, null, 136);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (227, 'CCM', 'Transportation Services — Jake Christiansen Stadium', '218-299-3259', null, null, null, 'IMPNUM', null, null, null, null, 137);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (228, 'CCM', 'Tri-College University', '701-231-8170', null, null, null, 'IMPNUM', null, null, null, null, 138);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (229, 'CCM', 'Undergraduate Research, Scholarship and National Fellowships', '218-299-3252', null, null, null, 'IMPNUM', null, null, null, null, 139);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (230, 'CCM', 'Vocation and Church Leadership — Knutson Campus Center', '218-299-3146', null, null, null, 'IMPNUM', null, null, null, null, 140);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (231, 'CCM', 'Women’s Studies', '218-299-4236', null, null, null, 'IMPNUM', null, null, null, null, 141);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (232, 'CCM', 'Campus Public Safety', '218-299-3123', 'Knutson Campus Center', 'security@cord.edu', null, 'EMR', 2, 'SUPERADMIN', to_timestamp('28-05-2013 11:25:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Call 911 for emergency response to Fire/Police/Ambulance', 142);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (89, 'CCM', 'ELCA, Northwestern MN Synod — Riverside Center', '218-299-3019', null, null, null, 'IMPNUM', null, null, null, null, 143);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (90, 'CCM', 'English — Academy (third floor)', '218-299-3812', null, null, null, 'IMPNUM', null, null, null, null, 144);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (91, 'CCM', 'Enrollment -Office of Admissions - Welcome Center', '218-299-3004', null, null, null, 'IMPNUM', null, null, null, null, 145);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (92, 'CCM', 'Enrollment -Office of Financial Aid - Welcome Center', '218-299-3010', null, null, null, 'IMPNUM', null, null, null, null, 146);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (93, 'CCM', 'Enrollment Office - Toll free ', '800-699-9897', null, null, null, 'IMPNUM', null, null, null, null, 147);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (94, 'CCM', 'Environmental Studies', '218-299-3085', null, null, null, 'IMPNUM', null, null, null, null, 148);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (95, 'CCM', 'Exercise Science (see Physical Education and Health)', '218-299-4434', null, null, null, 'IMPNUM', null, null, null, null, 149);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (96, 'CCM', 'Facilities Management — Mugaas', null, null, null, null, 'IMPNUM', null, null, null, null, 150);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (97, 'CCM', 'Facilities Management -Office ', '218-299-3362', null, null, null, 'IMPNUM', null, null, null, null, 151);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (98, 'CCM', 'Facilities Management -After-Hour Emergency Maintenance (24-hour) ', '218-299-3123', null, null, null, 'IMPNUM', null, null, null, null, 152);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (99, 'CCM', 'Facilities Management -Electrical Services ', '218-299-4626', null, null, null, 'IMPNUM', null, null, null, null, 153);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (100, 'CCM', 'Facilities Management -General Construction and Finishes — Carpentry ', '218-299-3679', null, null, null, 'IMPNUM', null, null, null, null, 154);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (101, 'CCM', 'Facilities Management -General Construction and Finishes — Locks and Hardware ', '218-299-3679', null, null, null, 'IMPNUM', null, null, null, null, 155);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (102, 'CCM', 'Facilities Management -General Construction and Finishes — Painting ', '218-299-3679', null, null, null, 'IMPNUM', null, null, null, null, 156);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (103, 'CCM', 'Facilities Management -Grounds Services ', '218-299-3066', null, null, null, 'IMPNUM', null, null, null, null, 157);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (104, 'CCM', 'Facilities Management -Heating and Cooling Plant ', '218-299-3688', null, null, null, 'IMPNUM', null, null, null, null, 158);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (105, 'CCM', 'Facilities Management -Parking Services ', '218-299-3267', null, null, null, 'IMPNUM', null, null, null, null, 159);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (106, 'CCM', 'Facilities Management -Plumbing Services ', '218-299-4686', null, null, null, 'IMPNUM', null, null, null, null, 160);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (107, 'CCM', 'Facilities Management -Telecom Services', '218-299-4629', null, null, null, 'IMPNUM', null, null, null, null, 161);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (108, 'CCM', 'Facilities Management -Transportation Services ', '218-299-3259', null, null, null, 'IMPNUM', null, null, null, null, 162);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (109, 'CCM', 'Finance/Treasurer’s Office — Lorentzsen', '218-299-3150', null, null, null, 'IMPNUM', null, null, null, null, 163);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (110, 'CCM', 'Financial Aid (see Enrollment)', '218-299-3010', null, null, null, 'IMPNUM', null, null, null, null, 164);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (111, 'CCM', 'F/M Communiversity — Riverside Center', '218-299-3488', null, null, null, 'IMPNUM', null, null, null, null, 165);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (112, 'CCM', 'Food/Nutrition/Dietetics (see Nutrition and Dietetics)', '218-299-4442', null, null, null, 'IMPNUM', null, null, null, null, 166);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (113, 'CCM', 'Forum on Faith and Life — Academy', '218-299-3146', null, null, null, 'IMPNUM', null, null, null, null, 167);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (114, 'CCM', 'French — Academy (second floor)', '218-299-3695', null, null, null, 'IMPNUM', null, null, null, null, 168);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (115, 'CCM', 'German  — Bishop Whipple (third floor)', '218-299-3106', null, null, null, 'IMPNUM', null, null, null, null, 169);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (116, 'CCM', 'Global Learning — Lorentzsen', '218-299-3927', null, null, null, 'IMPNUM', null, null, null, null, 170);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (117, 'CCM', 'Global Education', '218-299-3927', null, null, null, 'IMPNUM', null, null, null, null, 171);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (118, 'CCM', 'Global Studies', '218-299-3528', null, null, null, 'IMPNUM', null, null, null, null, 172);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (119, 'CCM', 'Intercultural Affairs', '218-299-4517', null, null, null, 'IMPNUM', null, null, null, null, 173);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (120, 'CCM', 'Graduate and Continuing Studies, Office of — Lorentzsen', '218-299-3257', null, null, null, 'IMPNUM', null, null, null, null, 174);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (121, 'CCM', 'Health Center — Kjos  (also see Emergency Contacts) ', '218-299-3662', null, null, null, 'IMPNUM', null, null, null, null, 175);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (122, 'CCM', 'Health Professions', '218-299-4302', null, null, null, 'IMPNUM', null, null, null, null, 176);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (123, 'CCM', 'History — Old Main (third floor)', '218-299-3501', null, null, null, 'IMPNUM', null, null, null, null, 177);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (124, 'CCM', 'Human Resources and Payroll — Lorentzsen', '218-299-3339', null, null, null, 'IMPNUM', null, null, null, null, 178);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (126, 'CCM', 'Humanities', '218-299-3422', null, null, null, 'IMPNUM', null, null, null, null, 179);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (127, 'CCM', 'Information Technology Services', null, null, null, null, 'IMPNUM', null, null, null, null, 180);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (128, 'CCM', 'ITS -Chief Information Officer', '218-299-4737', null, null, null, 'IMPNUM', null, null, null, null, 181);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (129, 'CCM', 'ITS -Digital Media Services — Olin', '218-299-4202', null, null, null, 'IMPNUM', null, null, null, null, 182);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (130, 'CCM', 'ITS -Enterprise Systems and Services — Lorentzsen', '218-299-4923', null, null, null, 'IMPNUM', null, null, null, null, 183);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (131, 'CCM', 'ITS -Infrastructure Services — Lorentzsen', '218-299-4192', null, null, null, 'IMPNUM', null, null, null, null, 184);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (132, 'CCM', 'ITS -Solution Center / Help Desk — Frances Frazier Comstock Theatre', '218-299-3375', null, null, null, 'IMPNUM', null, null, null, null, 185);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (133, 'CCM', 'Institutional Research and Assessment, Office of — Lorentzsen', null, null, null, null, 'IMPNUM', null, null, null, null, 186);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (134, 'CCM', 'IR -Institutional Assessment ', '218-299-4723', null, null, null, 'IMPNUM', null, null, null, null, 187);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (135, 'CCM', 'IR -Institutional Research ', '218-299-3549', null, null, null, 'IMPNUM', null, null, null, null, 188);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (136, 'CCM', 'Intercultural Affairs, Office of (see Global Learning)', '218-299-3927', null, null, null, 'IMPNUM', null, null, null, null, 189);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (137, 'CCM', 'International Business ', '218-299-3478', null, null, null, 'IMPNUM', null, null, null, null, 190);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (138, 'CCM', 'Journalism (see Multimedia Journalism)', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 191);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (139, 'CCM', 'KORD (student radio) — Olin', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 192);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (140, 'CCM', 'Library, Carl B. Ylvisaker', '218-299-4640', null, null, null, 'IMPNUM', null, null, null, null, 193);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (141, 'CCM', 'Library -Archives ', '218-299-3241', null, null, null, 'IMPNUM', null, null, null, null, 194);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (142, 'CCM', 'Library -Circulation Desk ', '218-299-4641', null, null, null, 'IMPNUM', null, null, null, null, 195);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (143, 'CCM', 'Library -Curriculum Center ', '218-299-4499', null, null, null, 'IMPNUM', null, null, null, null, 196);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (144, 'CCM', 'Library -Interlibrary Loan ', '218-299-4641', null, null, null, 'IMPNUM', null, null, null, null, 197);
commit;
prompt 200 records committed...
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (145, 'CCM', 'Library -Reference Desk ', '218-299-4656', null, null, null, 'IMPNUM', null, null, null, null, 198);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (146, 'CCM', 'Lorentzsen Center for Faith and Work - Grant Center', '218-299-3305', null, null, null, 'IMPNUM', null, null, null, null, 199);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (147, 'CCM', 'Management Information Systems (see Offutt School of Business)', '218-299-4411', null, null, null, 'IMPNUM', null, null, null, null, 200);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (148, 'CCM', 'Mathematics — Ivers (second floor)', '218-299-4151', null, null, null, 'IMPNUM', null, null, null, null, 201);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (149, 'CCM', 'May Seminars Abroad (see Global Learning) - Lorentzsen Hall', '218-299-3927', null, null, null, 'IMPNUM', null, null, null, null, 202);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (150, 'CCM', 'Media Relations (see Communications and Marketing)', '218-299-3147', null, null, null, 'IMPNUM', null, null, null, null, 203);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (151, 'CCM', 'Ministry, Office of — Knutson Campus Center', '218-299-4161', null, null, null, 'IMPNUM', null, null, null, null, 204);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (152, 'CCM', 'Minnesota Public Radio KCCD 90.3 FM/KCCM 91.1 FM – Welcome Center ', '218-287-0666', null, null, null, 'IMPNUM', null, null, null, null, 205);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (153, 'CCM', 'Multimedia Journalism', '218-299-3143', null, null, null, 'IMPNUM', null, null, null, null, 206);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (154, 'CCM', 'Music — Hvidsten', '218-299-4414', null, null, null, 'IMPNUM', null, null, null, null, 207);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (155, 'CCM', 'Music Events and Recordings Information Line ', '218-299-4515', null, null, null, 'IMPNUM', null, null, null, null, 208);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (156, 'CCM', 'Music Organizations (see Cultural Events)', '218-299-4366', null, null, null, 'IMPNUM', null, null, null, null, 209);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (157, 'CCM', 'Neuroscience - Ivers Science', '218-299-3112', null, null, null, 'IMPNUM', null, null, null, null, 210);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (158, 'CCM', 'Norwegian — Bishop Whipple (third floor)', '218-299-3106', null, null, null, 'IMPNUM', null, null, null, null, 211);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (159, 'CCM', 'Nursing — Jones Science Center (first floor)', '218-299-3879', null, null, null, 'IMPNUM', null, null, null, null, 212);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (160, 'CCM', 'Nutrition and Dietetics — Jones Science Center (third floor)', '218-299-4442', null, null, null, 'IMPNUM', null, null, null, null, 213);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (161, 'CCM', 'Offutt School of Business — Grant Center', '218-299-4411', null, null, null, 'IMPNUM', null, null, null, null, 214);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (162, 'CCM', 'Small Business Development Center – Grant Center', '218-299-3035', null, null, null, 'IMPNUM', null, null, null, null, 215);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (163, 'CCM', 'Olson Forum Desk (lower level, Staffed Academic Year Only)', '218-299-4310', null, null, null, 'IMPNUM', null, null, null, null, 216);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (164, 'CCM', 'Oral Communication Center — Olin 122', '218-299-3740', null, null, null, 'IMPNUM', null, null, null, null, 217);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (165, 'CCM', 'Orientation — Knutson Campus Center', '218-299-3455', null, null, null, 'IMPNUM', null, null, null, null, 218);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (166, 'CCM', 'Parke Student Leadership Center  — Knutson Campus Center (second floor)', '218-299-4518', null, null, null, 'IMPNUM', null, null, null, null, 219);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (167, 'CCM', 'Student Business Office ', '218-299-3084', null, null, null, 'IMPNUM', null, null, null, null, 221);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (168, 'CCM', 'Parking Services — Mugaas', '218-299-3267', null, null, null, 'IMPNUM', null, null, null, null, 220);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (169, 'CCM', 'Philosophy — Bishop Whipple (first floor)', '218-299-3946', null, null, null, 'IMPNUM', null, null, null, null, 222);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (170, 'CCM', 'Photo Studio (see Communications and Marketing)', '218-299-3938', null, null, null, 'IMPNUM', null, null, null, null, 223);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (171, 'CCM', 'Physical Education and Health — Memorial Auditorium (first floor)', '218-299-4434', null, null, null, 'IMPNUM', null, null, null, null, 224);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (172, 'CCM', 'PE&H -Equipment Room, Memorial Auditorium ', '218-299-3209', null, null, null, 'IMPNUM', null, null, null, null, 225);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (173, 'CCM', 'PE&H -Olson Forum Desk ', '218-299-4310', null, null, null, 'IMPNUM', null, null, null, null, 226);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (174, 'CCM', 'PE&H -Sports Information ', '218-299-3194', null, null, null, 'IMPNUM', null, null, null, null, 227);
insert into EMERGENCY_CONTACTS (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (175, 'CCM', 'PE&H -Swimming Pool ', '218-299-3591', null, null, null, 'IMPNUM', null, null, null, null, 228);
commit;
prompt 231 records loaded
prompt Loading EMERGENCY_CONTACTS_AUDIT...
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:14:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:15:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAA', '123-123-2484', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('07-10-2013 01:51:12.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (3, 'CCM', 'Academic Affairs -Arts and Sciences', '218-299-4541', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('22-10-2013 10:42:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Academic Affairs -Academic Division Chairs', '218-299-3606', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('03-10-2013 04:48:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAAA', '123-123-2484', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('28-10-2013 05:55:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013088, 'CCM Main Campus', 'Education — Old Main (first floor)', '218-299-3910', 'null', 'null', 'null', 'Department Phone Numbers', null, 'null', to_timestamp('28-05-2013 11:19:09.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013088, 'CCM Main Campus', 'Education ? Old Main (first floor)', '218-299-3910', 'Old Main (first floor)', 'null', 'null', 'Department Phone Numbers', 2, 'SUPERADMIN', to_timestamp('28-05-2013 11:20:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (first floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (61, 'CCM', 'Concordia Language Villages — Bemidji', '218-586-8600', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('05-06-2013 10:22:42.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (58, 'CCM', 'Comm -Sports Information — Memorial Auditorium', '218-299-3194', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('05-06-2013 10:23:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013057, 'CCM Main Campus', 'Comm -Design', '218-299-4948', 'null', 'null', 'null', 'Department Phone Numbers', null, 'null', to_timestamp('24-05-2013 08:00:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013232, 'CLV', 'Test contact', '112-110-1100', 'null', 'null', 'null', 'LEI', 12, 'SUPERADMIN', to_timestamp('24-05-2013 08:10:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Comments');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013232, 'CLV', 'Test', '110-110-1100', 'null', 'null', 'null', 'Department Phone Numbers', 12, 'SUPERADMIN', to_timestamp('24-05-2013 12:34:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013232, 'CCM', 'local emer', '333-333-3333', 'address 1', 'aa@aa.com', 'http', 'Local Emergency Information', 12, 'SUPERADMIN', to_timestamp('24-05-2013 22:21:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'local emer');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (21, 'CCM', 'Athletics — Memorial Auditorium', '218-299-4434', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('18-06-2013 15:34:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (21, 'CCM', 'Athletics ? Memorial Auditorium', '218-299-4434', 'Memorial Auditorium', 'null', 'null', 'IMPNUM', 2, 'eramstad', to_timestamp('18-06-2013 15:36:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (57, 'CCM', 'Comm -Design', '218-299-4948', 'null', 'null', 'null', 'IMPNUM', 2, 'n2njmork2', to_timestamp('16-06-2013 09:01:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'comments');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (1, 'CCM', 'Office of the President — Lorentzsen', '218-299-3000', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('18-06-2013 15:28:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Alumni Contact', '218-299-3734', 'Office of Alumni Relations Advancement Center', 'alumni@cord.edu', 'null', 'EMR', 4, 'SUPERADMIN', to_timestamp('21-03-2013 08:58:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Alumni Contact', '218-299-3734', 'Office of Alumni Relations Advancement Center', 'alumni@cord.edu', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('21-03-2013 09:39:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Comments');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2, 'CCM', 'General Contact', '218-299-4000', '901 8th St S Moorhead MN 56562', 'null', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('22-03-2013 11:27:03.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Alumni Contact', '218-299-3734', 'Office of Alumni Relations Advancement Center', 'alumni@cord.edu', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('22-03-2013 11:39:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Comments added');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (7, 'CCM', 'Office of the President â?? Lorentzsen', '218-299-3000', 'null', 'null', 'null', 'IMPNUM', 7, 'SUPERADMIN', to_timestamp('22-03-2013 11:42:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (3, 'CCN', 'Admissions Contact', '218-299-3004', 'Office of Admissions Welcome Center', 'admissions@cord.edu', 'null', 'EMR', 3, 'SUPERADMIN', to_timestamp('06-03-2013 07:04:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (3, 'CCN', 'Admissions Contact', '218-299-3004', 'Office of Admissions Welcome Center', 'admissions@cord.edu', 'Test', 'EMR', 2, 'SUPERADMIN', to_timestamp('06-03-2013 07:05:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2, 'CCM', 'General Contact', '218-299-4000', '901 8th St S Moorhead MN 56562', 'campinfo@cord.edu', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('22-03-2013 11:27:38.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Campus Information');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (3, 'CCM', 'Admissions Contact', '218-299-3004', 'Office of Admissions Welcome Center', 'admissions@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('22-03-2013 11:38:24.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (7, 'CCM', 'Office of the President', '218-299-3000', 'Lorentzsen Hall', 'null', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('25-03-2013 04:36:47.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'President');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (6, 'CCM', 'Chris Horsagers', '404-555-1212', 'null', 'ch@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('19-03-2013 14:06:55.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (6, 'CCM', 'Chris Horsager', '404-555-1212', 'null', 'ch@cord.edu', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('20-03-2013 11:53:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (6, 'CCM', 'Chris Horsager', '404-555-1212', 'null', 'ch@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('20-03-2013 13:53:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '<b>test</b> <br>test');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Alumni Contact', '218-299-3734', 'Office of Alumni Relations Advancement Center', 'alumni@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('21-03-2013 08:58:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Comments');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (13, 'CCM', 'B Imp Number', '121-212-6325', 'Address 1', 'dfd@dfds.com', 'http', 'IMPNUM', 13, 'SUPERADMIN', to_timestamp('21-03-2013 09:07:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Need to testin app aboutthe linesformat');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (13, 'CCM', 'B Emer Number', '121-212-6325', 'Address 1', 'dfd@dfds.com', 'http', 'EMR', 2, 'SUPERADMIN', to_timestamp('21-03-2013 09:08:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Need to testin app aboutthe linesformat');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (5, 'CCM', 'Graduate Programs Contact', '218-299-3257', 'Office of Academic Affairs Lorentzsen 210', 'graduate@cord.edu', 'null', 'EMR', 5, 'SUPERADMIN', to_timestamp('22-03-2013 11:35:20.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (12, 'CCM', 'Test', '111-222-3333', '123 Test Street', 'test@cord.edu', 'null', 'EMR', 12, 'SUPERADMIN', to_timestamp('25-03-2013 03:32:42.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (13, 'CCM', 'Emer Number', '123-456-7890', 'null', 'null', 'null', 'EMR', 12, 'SUPERADMIN', to_timestamp('25-03-2013 04:43:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Comments');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2, 'CCN', 'General Contact', '218-299-4000', '901 8th St S Moorhead MN 56562', 'null', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:25:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2, 'CCN', 'Generals Contact', '218-299-4000', '901 8th St S Moorhead MN 56562', 'null', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:25:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (6, 'CCM', 'Chris Horsager', '404-555-1212', 'null', 'ch@cord.edu', 'null', 'EMR', 6, 'SUPERADMIN', to_timestamp('19-03-2013 14:06:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (6, 'CCM', 'Chris Horsager', '404-555-1212', 'null', 'ch@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('20-03-2013 11:53:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (6, 'CCM', 'Chris Horsager', '404-555-1212', 'null', 'ch@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('20-03-2013 13:44:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (5, 'CCM', 'Graduate Programs Contact', '218-299-3257', 'Office of Academic Affairs Lorentzsen 210', 'graduate@cord.edu', 'null', 'EMR', 5, 'SUPERADMIN', to_timestamp('22-03-2013 11:34:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (7, 'CCM', 'Office of the President', '218-299-3000', 'Lorentzsen Hall', 'null', 'png', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('25-03-2013 04:36:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'President');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013232, 'CLV', 'local emer', '333-333-3333', 'address 1', 'aa@aa.com', 'http', 'Local Emergency Information', 2, 'SUPERADMIN', to_timestamp('28-05-2013 11:25:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'local emer');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (2013057, 'CCM Main Campus', 'Comm -Design', '218-299-4948', 'null', 'null', 'null', 'Department Phone Numbers', 2, 'SUPERADMIN', to_timestamp('28-05-2013 11:31:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'comments added');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (12, 'CCM', 'Test', '111-222-3333', '123 Test Street', 'test@cord.edu', 'null', 'EMR', 2, 'SUPERADMIN', to_timestamp('28-05-2013 11:31:15.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Test emergency contact');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (57, 'CCM', 'Comm -Design', '218-299-4948', 'null', 'null', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('05-06-2013 08:26:42.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'comments added');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:14:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)Old Main (First floor)Old M', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:16:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZ', '123-123-1234', 'null', 'null', 'null', 'IMPNUM', 3, 'superadmin', to_timestamp('05-10-2013 04:19:45.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAA', '123-123-2445', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('07-10-2013 01:44:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAAA', '123-123-2484', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('07-10-2013 02:47:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAAA', '123-123-2484', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('07-10-2013 02:48:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'this');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Academic Affairs -Academic Division Chairs', '218-299-3606', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('22-10-2013 10:40:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (first floor)', 'null', 'null', 'IMPNUM', 2, 'SUPERADMIN', to_timestamp('26-08-2013 08:33:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (2nd floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (4, 'CCM', 'Academic Affairs -Academic Division Chairs', '218-299-3606', 'null', 'null', 'null', 'IMPNUM', null, 'null', to_timestamp('03-10-2013 04:43:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)Old Main (First floor)Old M', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:16:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAAA', '123-123-2484', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('28-10-2013 05:47:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (233, 'CLV', 'ZZZZZZAAAAA', '123-123-2484', 'null', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('28-10-2013 05:49:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:14:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:15:55.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:16:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:16:21.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)Old Main (First floor)Old M', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:17:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)Old Main (First floor)Old M', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:17:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
insert into EMERGENCY_CONTACTS_AUDIT (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (First floor)Old Main (First floor)Old M1', 'null', 'null', 'IMPNUM', 2, 'superadmin', to_timestamp('04-10-2013 06:18:09.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'Old Main (First floor)');
commit;
prompt 67 records loaded
prompt Loading FAQ...
insert into FAQ (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (4, 'CCM', 'hi', 'hi', 4, 'superadmin', to_timestamp('22-10-2013 10:55:55.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FAQ (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 'CCM', 'What username and password is used to access the MY INFO module?', 'MY INFO is only accessible with an active Concordia College user account.', 2, 'superadmin', to_timestamp('18-09-2013 09:35:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FAQ (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (3, 'CCM', 'Who do I contact with questions about the mobile application.', 'Please contact the Solution Center at 218-299-3375 or pcsupport@cord.edu.', 3, 'superadmin', to_timestamp('18-09-2013 09:57:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FAQ (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (1, 'CCN', 'Test question?', 'Test answer.', 1, 'SUPERADMIN', to_timestamp('13-03-2013 14:41:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
commit;
prompt 4 records loaded
prompt Loading FEEDS...
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (11, 10, 'LINK', 'http://www.concordiacollege.edu/student-life/dining-services/dining-on-campus/', 'xml', 4, 'Menu', 'food.png', 'CCM', 2, 'superadmin', to_timestamp('23-08-2013 05:58:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (14, 0, 'LINK', 'https://secure.touchnet.com/C20618test_tsa/web/login.jsp', 'jsp', 0, 'Make Payment', 'student-account.png', 'CCM', null, null, null);
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (10, 0, 'MENU', null, null, 4, 'Dining Services', 'food.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('17-05-2013 07:58:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (13, 10, 'LINK', 'http://www.concordiacollege.edu/student-life/dining-services/dining-on-campus/the-maize/', 'xml', 4, 'The Maize', 'food.png', 'CCM', 2, 'superadmin', to_timestamp('23-08-2013 06:00:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (12, 10, 'LINK', 'http://www.concordiacollege.edu/student-life/dining-services/hours-events/', 'xml', 4, 'Dining Events', 'food.png', 'CCM', 2, 'superadmin', to_timestamp('23-08-2013 05:57:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 2, 'Events', 'events.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('06-03-2013 10:07:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (5, 0, 'LINK', 'http://instagram.com/concordia_mn', 'html', 7, 'Instagram', 'photos.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('18-03-2013 09:58:06.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (4, 0, 'LINK', 'https://mobile.twitter.com/concordia_mn', 'html', 7, 'Twitter', 'twitter.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('19-02-2013 09:59:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (1, 0, 'LINK', 'http://www.concordiacollege.edu/feeds/news/', 'xml', 3, 'News', 'news.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('19-02-2013 06:50:38.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (3, 0, 'LINK', 'https://m.facebook.com/concordiacollege', 'html', 7, 'Facebook', 'facebook.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('19-02-2013 09:58:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (9, 0, 'LINK', 'http://moodlet1.cord.edu/', 'html', 16, 'Moodle', 'moodle.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('31-05-2013 05:58:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (15, 0, 'LINK', 'http://www.linkedin.com/groups?home=&gid=66978&tr...', 'html', 7, 'Linkedin', 'linkedin.png', 'CCM', 2, 'superadmin', to_timestamp('01-10-2013 06:16:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (16, 0, 'LINK', 'http://pinterest.com/concordia_mn/', 'html', 7, 'Pinterest', 'pinterest.png', 'CCM', 2, 'superadmin', to_timestamp('01-10-2013 06:17:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
commit;
prompt 13 records loaded
prompt Loading FEEDS_AUDIT...
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (13, 10, 'LINK', 'http://www.cord.edu/Studentlife/Housinganddining/Diningservices/RetailOperations/TheMaize.php', 'xml', 4, 'The Maize', 'food.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('23-08-2013 06:00:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (9, 0, 'LINK', 'http://moodlet1.cord.edu/', 'html', 16, 'Moodle', 'moodle.png', 'CCM', null, 'null', to_timestamp('31-05-2013 05:58:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (7, 6, 'LINK', 'http://www.concordianow.com/s/1351/start.aspx', 'xml', 5, 'Alumni', 'alumni.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('02-06-2013 07:07:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (7, 6, 'LINK', 'http://www.concordianow.com/s/1351/start.aspx', 'xml', 5, 'Alumnis', 'alumni.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('03-06-2013 10:18:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 3, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:03:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (5, 0, 'LINK', 'http://instagram.com/concordia_mn', 'html', 5, 'Photos', 'photos.png', 'CCN', 1, 'SUPERADMIN', to_timestamp('14-03-2013 09:01:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'aspx', 2, 'Events', 'events.png', 'CCN', 1, 'SUPERADMIN', to_timestamp('19-02-2013 06:55:30.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 2, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('19-02-2013 10:41:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (5, 0, 'LINK', 'http://instagram.com/concordia_mn', 'xml', 5, 'Photos', 'photos.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('19-02-2013 11:19:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 3, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:10:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 3, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:11:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 2, 'Events', 'events.png', 'CCN', 1, 'SUPERADMIN', to_timestamp('19-02-2013 09:47:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 2, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('19-02-2013 10:02:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 2, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('19-02-2013 10:44:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 3, 'Event', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:03:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 3, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('06-03-2013 10:07:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (3, 0, 'LINK', 'http://www.facebook.com/concordiacollege', 'html', 3, 'Facebook', 'facebook.png', 'CCN', 1, 'SUPERADMIN', to_timestamp('19-02-2013 09:58:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (5, 0, 'LINK', 'http://instagram.com/concordia_mn', 'html', 5, 'Photos', 'photos.png', 'CCN', 1, 'SUPERADMIN', to_timestamp('19-02-2013 10:58:15.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'html', 2, 'Events', 'events.png', 'CCN', 2, 'SUPERADMIN', to_timestamp('19-02-2013 11:06:35.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (5, 0, 'LINK', 'http://instagram.com/concordia_mn', 'html', 5, 'Instagram', 'photos.png', 'CCM', 2, 'SUPERADMIN', to_timestamp('18-03-2013 09:58:06.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (8, 6, 'LINK', 'http://www.concordianow.com/s/1351/start.aspx', 'xml', 6, 'Alumni1', 'alumni.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('07-05-2013 07:36:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (11, 10, 'LINK', 'http://www.cord.edu/dept/ds/menu.php', 'xml', 4, 'Menu', 'food.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('23-08-2013 05:58:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into FEEDS_AUDIT (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (12, 10, 'LINK', 'http://www.cord.edu/Studentlife/Housinganddining/Diningservices/hours.php', 'xml', 4, 'Dining Events', 'food.png', 'CCM', 1, 'SUPERADMIN', to_timestamp('23-08-2013 05:57:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
commit;
prompt 23 records loaded
prompt Loading HELP...
insert into HELP (CAMPUSCODE, ABOUTTEXT, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values ('CCM', 'Concordia is a private, four-year liberal arts college with an extensive global education program, state-of-the-art science curriculum and a learning environment that fosters a partnership between intellect and faith. We offer more than 60 majors, including 18 honors majors and 12 preprofessional programs in medicine, law, engineering, dentistry and more. Our 2,600+ students benefit from a \''globalized\'' curriculum combining significant study abroad, hands-on research and exceptional instruction in nine world languages.', 2, 'SUPERADMIN', to_timestamp('20-05-2013 10:13:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
insert into HELP (CAMPUSCODE, ABOUTTEXT, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values ('CCN', 'Concordia is a private, four-year liberal arts college with an extensive global education program, state-of-the-art science curriculum and a learning environment that fosters a partnership between intellect and faith. We offer more than 60 majors, including 18 honors majors and 12 preprofessional programs in medicine, law, engineering, dentistry and more. Our 2,600  students benefit from a ''globalized'' curriculum combining significant study abroad, hands-on research and exceptional instruction in nine world languages.', 1, 'SUPERADMIN', to_timestamp('06-03-2013 08:11:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'));
commit;
prompt 2 records loaded
prompt Loading HELP_AUDIT...
prompt Table is empty
prompt Loading MAPS...
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (42, 'CCM', 'KCC', 'Knutson Campus Center', null, null, null, 'https://m-test.cord.edu/images/building_images/campus_center.jpg', '-96.769933', '46.865322', 2, 'superadmin', to_timestamp('28-08-2013 10:26:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '624 9th Avenue South, Moorhead, MN, US', 'false', '1,2,6', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (45, 'CCM', 'OLIN', 'Olin Art and Communications Center', null, null, null, 'https://m-test.cord.edu/images/building_images/olin.jpg', '-96.77055', '46.863727', 2, 'superadmin', to_timestamp('28-08-2013 10:27:21.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1010 6th Street South, Moorhead, MN, US', 'false', '1,2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (46, 'CCM', 'FFCT', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', null, null, null, 'https://m-test.cord.edu/images/building_images/comstock_theatre.jpg', '-96.770427', '46.86416', 2, 'superadmin', to_timestamp('28-08-2013 10:27:38.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1001 7th Street South, Moorhead, MN, US', 'false', '1,2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (58, 'CLV', 'NORWAY', 'Skogfjorden, Norwegian Village', 'Skogfjorden, Norwegian Village, Bemidji Site, June-July', '218-586-8730', 'clv@cord.edu', null, '-94.730816', '47.551463', 2, 'superadmin', to_timestamp('28-08-2013 10:30:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (59, 'CLV', 'SPNSH1', 'El Lago del Bosque, Spanish Village', 'El Lago del Bosque, Spanish Village, Bemidji Site, March-August', '218-586-8930', 'clv@cord.edu', null, '-94.7269', '47.563084', 2, 'superadmin', to_timestamp('28-08-2013 10:31:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (60, 'CLV', 'PRTUGS', 'Mar e Floresta, Portugese Village', 'Mar e Floresta, Portugese Village, Callaway Site, July-August', '218-375-4021', 'clv@cord.edu', null, '-95.776663', '47.009523', 2, 'superadmin', to_timestamp('28-08-2013 10:31:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (61, 'CLV', 'CHINA', 'Sen Lin Hu, Chinese Village', 'Sen Lin Hu, Chinese Village, Callaway Site, June-August', '218-375-4021', 'clv@cord.edu', null, '-95.776298', '47.008602', 2, 'superadmin', to_timestamp('28-08-2013 10:31:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (64, 'CLV', 'RUSSIA', 'Lesnoe Ozero, Russian Village', 'Lesnoe Ozero, Russian Village, Bemidji Site, ', '218-586-8430', 'clv@cord.edu', null, '-94.714910', '47.575447', 2, 'superadmin', to_timestamp('28-08-2013 10:32:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (66, 'CLV', 'ITALY', 'Lago Del Bosco-Camp Holiday, Italian Village', 'Lago Del Bosco-Camp Holiday, Italian Village, Hackensack Site, July-August', '218-682-2570', 'clv@cord.edu', null, '-94.351954', '46.970513', 2, 'superadmin', to_timestamp('28-08-2013 10:32:26.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (67, 'CLV', 'JAPAN', 'Mori no Ike, Japanese Village', 'Mori no Ike, Japanese Village, Dent Site, 40225 Purlieu Rd  Dent, MN 56528, June-August', '218-758-2112', 'clv@cord.edu', null, '-95.774777', '46.547282', 2, 'superadmin', to_timestamp('28-08-2013 10:32:35.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (8, 'CCM', 'BRWN', 'Brown Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/brown_hall.jpg', '-96.771977', '46.864398', 2, 'superadmin', to_timestamp('28-08-2013 09:34:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '515 9th Avenue South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (15, 'CCM', 'MUS', 'Hvidsten Hall of Music', null, null, null, 'https://m-test.cord.edu/images/building_images/hvidsten.jpg', '-96.771709', '46.862817', 2, 'superadmin', to_timestamp('28-08-2013 09:37:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1112 5th Street South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (49, 'CCM', 'POND', 'Prexy''s Pond', null, null, null, 'https://m-test.cord.edu/images/building_images/pond.jpg', '-96.769064', '46.866295', 2, 'superadmin', to_timestamp('28-08-2013 10:28:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '4', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (51, 'CLV', 'ARABIC', 'Al-Waha, Arabic Village', 'Al-Waha, Arabic Village, Bemidji Site, July-August', '218-586-8730', 'clv@cord.edu', null, '-94.730381', '47.552085', 2, 'superadmin', to_timestamp('22-10-2013 03:37:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINNSH', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site, June-July', '218-586-8830', 'clv@cord.edu', null, '-94.72454', '47.55546', 2, 'superadmin', to_timestamp('28-08-2013 10:29:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (54, 'CLV', 'FRNCH1', 'Lac du Bois, French Village', 'Lac du Bois, French Village, Bemidji Site, February-August', '218-586-8530', 'clv@cord.edu', null, '-94.738879', '47.564709', 2, 'superadmin', to_timestamp('28-08-2013 10:30:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (55, 'CLV', 'FRNCH2', 'Les Voyageurs, French Camping', 'Les Voyageurs, French Camping, base camp Bemidji, June-August', '218-586-8600', 'clv@cord.edu', null, '-94.738246', '47.563674', 2, 'superadmin', to_timestamp('28-08-2013 10:30:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'true', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (56, 'CLV', 'FRNCH3', 'Lac du Bois-Camp Holiday', 'Lac du Bois-Camp Holiday, Hackensack Site, June-July', '218-682-2570', 'clv@cord.edu', null, '-94.35304', '46.970332', 2, 'superadmin', to_timestamp('28-08-2013 10:30:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (62, 'CLV', 'SPNSH2', 'El Lago del Bosque-Wilder Forest, Spanish Village', 'El Lago del Bosque-Wilder Forest, Spanish Village, Marine on St. Croix, June-August', '651-433-4375', 'clv@cord.edu', null, '-92.815601', '45.166487', 2, 'superadmin', to_timestamp('28-08-2013 10:31:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (63, 'CLV', 'SWEDEN', 'Sjolunden, Swedish Village', 'Sjolunden, Swedish Village, Bemidji Site, July-August', '218-586-8830', 'clv@cord.edu', null, '-94.725688', '47.555547', 2, 'superadmin', to_timestamp('28-08-2013 10:31:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (65, 'CLV', 'KOREAN', 'Sup sogui Hosu, Korean Village', 'Sup sogui Hosu, Korean Village, Bemidji Site, July-August', '218-586-8430', 'clv@cord.edu', null, '-94.715828', '47.575994', 2, 'superadmin', to_timestamp('28-08-2013 10:32:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (69, 'CLV', 'CLVMHD', 'Concordia Language Villages, Moorhead Administration', 'Concordia Language Villages, Moorhead Administrative Offices at Riverside Center', '218-299-4544', 'clv@cord.edu', null, '-96.773959', '46.860870', 2, 'superadmin', to_timestamp('28-08-2013 10:32:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (70, 'CLV', 'CLVBMJ', 'Concordia Language Villages, Bemidji Administration', 'Concordia Language Villages, Bemidji Administrative Offices at Centrum', '218-586-8600', 'clv@cord.edu', null, '-94.728515', '47.550062', 2, 'superadmin', to_timestamp('28-08-2013 10:33:08.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (32, 'CCM', 'GCTR', 'Grant Center', 'Offutt School of Business', null, null, 'https://m-test.cord.edu/images/building_images/grant_center.jpg', '-96.767235', '46.86113', 2, 'superadmin', to_timestamp('28-08-2013 10:05:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1300 8th Street South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (41, 'CCM', 'JONE', 'Jones Science Center', null, null, null, 'https://m-test.cord.edu/images/building_images/jones.jpg', '-96.76908', '46.863492', 2, 'superadmin', to_timestamp('28-08-2013 10:25:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1021 8th Street South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', null, null, null, 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'superadmin', to_timestamp('22-10-2013 07:29:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '617 8th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (50, 'CCM', 'BELL', 'Bell Tower', null, null, null, 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.769434', '46.864529', 2, 'superadmin', to_timestamp('28-08-2013 10:28:48.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '4', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (52, 'CLV', 'DANISH', 'Skovsoen, Danish Village', 'Skovsoen, Danish Village, Bemidji Site, June-July', '218-586-8820', 'clv@cord.edu', null, '-94.724443', '47.55597', 2, 'superadmin', to_timestamp('28-08-2013 10:29:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (57, 'CLV', 'GERMAN', 'Waldsee, German Village', 'Waldsee, German Village, Bemidji Site, February-August', '218-586-8630', 'clv@cord.edu', null, '-94.738787', '47.559402', 2, 'superadmin', to_timestamp('28-08-2013 10:30:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site, June-August', '218-335-6304', 'clv@cord.edu', null, '-94.638231', '47.465051', 2, 'superadmin', to_timestamp('28-08-2013 10:32:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (14, 'CCM', 'MPOC', 'Mugaas Plant Operation Center', null, null, null, 'https://m-test.cord.edu/images/building_images/mugaas.jpg', '-96.771333', '46.865928', 2, 'superadmin', to_timestamp('28-08-2013 09:37:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 6th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool', null, null, null, 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'superadmin', to_timestamp('28-08-2013 09:45:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '810 12th Avenue South, Moorhead, MN, US', 'false', '3', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (38, 'CCM', 'IC06', 'International Apartments 1106', null, null, null, 'https://m-test.cord.edu/images/building_images/international_apartments_1106.jpg', '-96.767272', '46.862806', 2, 'superadmin', to_timestamp('28-08-2013 10:25:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1106 8th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', null, null, null, 'https://m-test.cord.edu/images/building_images/riverside.jpg', '-96.774069', '46.860888', 2, 'superadmin', to_timestamp('04-10-2013 15:15:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '310 14th Avenue South, Moorhead, MN, US', 'false', '2,5', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (28, 'CCM', 'CC ECO', 'Eco House', null, null, null, 'https://m-test.cord.edu/images/building_images/eco_house.jpg', '-96.770588', '46.867607', 2, 'superadmin', to_timestamp('28-08-2013 10:01:24.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '618 South 6th St, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (29, 'CCM', 'ERIC', 'Erickson Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/erickson_hall.jpg', '-96.766891', '46.860499', 2, 'superadmin', to_timestamp('28-08-2013 10:01:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1320 8th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (48, 'CCM', 'PRES', 'President''s Residence', null, null, null, 'https://m-test.cord.edu/images/building_images/presidents_residence.jpg', '-96.768458', '46.866997', 2, 'superadmin', to_timestamp('28-08-2013 10:28:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (11, 'CCM', 'LIV', 'Livedalen Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/livedalen_hall.jpg', '-96.771575', '46.864138', 2, 'superadmin', to_timestamp('28-08-2013 09:36:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1002 5th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (12, 'CCM', 'LORE', 'Lorentzsen Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/lorentzsen_hall.jpg', '-96.768731', '46.864743', 2, 'superadmin', to_timestamp('28-08-2013 09:36:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '901 8th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (16, 'CCM', 'NORM', 'Normandy/Bookstore', null, null, null, 'https://m-test.cord.edu/images/building_images/bookstore.jpg', '-96.771183', '46.864314', 2, 'superadmin', to_timestamp('28-08-2013 09:37:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '929 6th St S, Moorhead, MN, US', 'false', '1,2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (17, 'CCM', 'OFC', 'Offutt Concourse', null, null, null, 'https://m-test.cord.edu/images/building_images/offutt_concourse.jpg', '-96.769445', '46.862788', 2, 'superadmin', to_timestamp('28-08-2013 09:38:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1111 8th Street South, Moorhead, MN, US', 'false', '1,3,2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (18, 'CCM', 'OF', 'Olson Forum', null, null, null, 'https://m-test.cord.edu/images/building_images/olson_forum.jpg', '-96.768876', '46.862678', 2, 'superadmin', to_timestamp('28-08-2013 09:38:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1111 8th Street South, Moorhead, MN, US', 'false', '1,3', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (19, 'CCM', 'OLDM', 'Old Main', null, null, null, 'https://m-test.cord.edu/images/building_images/old_main.jpg', '-96.768898', '46.865583', 2, 'superadmin', to_timestamp('28-08-2013 09:38:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '815 8th Street South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (22, 'CCM', 'SKY', 'Olson Skyway', null, null, null, 'https://m-test.cord.edu/images/building_images/olson_skyway.jpg', '-96.767814', '46.861728', 2, 'superadmin', to_timestamp('28-08-2013 09:43:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1101 8th Street South, Moorhead, MN, US', 'false', '8,2,1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (23, 'CCM', 'STAD', 'Jake Christiansen Stadium', null, null, null, 'https://m-test.cord.edu/images/building_images/jake_christiansen_side.jpg', '-96.765829', '46.859017', 2, 'superadmin', to_timestamp('28-08-2013 09:45:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1500 8th Street South, Moorhead, MN, US', 'false', '3', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (25, 'CCM', 'TE', 'Townhouse East', null, null, null, 'https://m-test.cord.edu/images/building_images/townhouse_east.jpg', '-96.765121', '46.861541', 2, 'superadmin', to_timestamp('28-08-2013 09:56:21.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '917 12th Avenue South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (26, 'CCM', 'TW', 'Townhouse West', null, null, null, 'https://m-test.cord.edu/images/building_images/townhouse_west.jpg', '-96.765904', '46.861592', 2, 'superadmin', to_timestamp('28-08-2013 09:58:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '915 12th Avenue South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (27, 'CCM', 'WELC', 'Welcome Center', 'Admissions, Financial Aid, Minnesota Public Radio', null, null, 'https://m-test.cord.edu/images/building_images/welcome_center.jpg', '-96.767514', '46.862473', 2, 'superadmin', to_timestamp('28-08-2013 09:59:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1118 8th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (43, 'CCM', 'MP', 'Car Pool', null, null, null, 'https://m-test.cord.edu/images/building_images/car_pool.jpg', '-96.76614', '46.859527', 2, 'superadmin', to_timestamp('28-08-2013 10:27:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1402 9th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (3, 'CCM', 'ADVCTR', 'Advancement Center', 'AdvancementAlumni Relations', null, null, 'https://m-test.cord.edu/images/building_images/advancement_center.jpg', '-96.767337', '46.863228', 2, 'superadmin', to_timestamp('28-08-2013 09:17:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1022 8th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', null, null, null, 'https://m-test.cord.edu/images/building_images/berg_steam_plant.jpg', '-96.770791', '46.865884', 2, 'superadmin', to_timestamp('28-08-2013 09:31:42.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '812 6th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('22-10-2013 05:51:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', 'false', '2,1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (10, 'CCM', 'LIB', 'Carl B. Ylvisaker Library', null, '218-299-4640', null, 'https://m-test.cord.edu/images/building_images/library.jpg', '-96.769761', '46.864593', 2, 'superadmin', to_timestamp('28-08-2013 09:35:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1010 10th Avenue South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (7, 'CCM', 'BOGM', 'Bogstad Manor', null, null, null, 'https://m-test.cord.edu/images/building_images/bogstad_manor.jpg', '-96.769059', '46.867864', 2, 'superadmin', to_timestamp('28-08-2013 09:34:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '618 7th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (5, 'CCM', 'BOEO', 'Boe-Olsen Apartments', null, null, null, 'https://m-test.cord.edu/images/building_images/boe_olsen_apartments.jpg', '-96.767492', '46.863646', 2, 'superadmin', to_timestamp('28-08-2013 09:34:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 South 10th Avenue, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (9, 'CCM', 'BW', 'Bishop Whipple Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/bishop_whipple.jpg', '-96.770089', '46.865942', 2, 'superadmin', to_timestamp('28-08-2013 09:34:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 7th Street South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (20, 'CCM', 'PARK', 'Park Region Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/park_region.jpg', '-96.769338', '46.867035', 2, 'superadmin', to_timestamp('28-08-2013 09:39:09.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '701 7th Avenue South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (30, 'CCM', 'FH', 'Memorial Auditorium', null, null, null, 'https://m-test.cord.edu/images/building_images/memorial.jpg', '-96.770078', '46.862935', 2, 'superadmin', to_timestamp('28-08-2013 10:02:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1117 7th Street South, Moorhead, MN, US', 'false', '3', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (31, 'CCM', 'FJEL', 'Fjelstad Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/fjelstad.jpg', '-96.770228', '46.86654', 2, 'superadmin', to_timestamp('28-08-2013 10:04:06.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '720 6th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (33, 'CCM', 'GROS', 'Grose Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/grose_hall.jpg', '-96.770078', '46.865744', 2, 'superadmin', to_timestamp('28-08-2013 10:22:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '811 7th Street South, Moorhead, MN, US', 'false', '1', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (34, 'CCM', 'HAL', 'Hallett Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/hallet_hall.jpg', '-96.767071', '46.861449', 2, 'superadmin', to_timestamp('28-08-2013 10:23:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '815 12th Avenue South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (35, 'CCM', 'HLTH', 'Kjos Health Center', null, null, null, 'https://m-test.cord.edu/images/building_images/kjos_health_center.jpg', '-96.771081', '46.863687', 2, 'superadmin', to_timestamp('28-08-2013 10:24:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1011 6th Street South, Moorhead, MN, US', 'false', '2', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (36, 'CCM', 'HOY', 'Hoyum Hall', null, null, null, 'https://m-test.cord.edu/images/building_images/hoyum_hall.jpg', '-96.771116', '46.863399', 2, 'superadmin', to_timestamp('28-08-2013 10:24:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1021 6th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (37, 'CCM', 'IC02', 'International Apartments 1102', null, null, null, 'https://m-test.cord.edu/images/building_images/international_apartments_1102.jpg', '-96.767535', '46.862931', 2, 'superadmin', to_timestamp('28-08-2013 10:24:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1102 8th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (39, 'CCM', 'IC10', 'International Apartments 1110', null, null, null, 'https://m-test.cord.edu/images/building_images/international_apartments_1110.jpg', '-96.767546', '46.862711', 2, 'superadmin', to_timestamp('28-08-2013 10:25:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1110 8th Street South, Moorhead, MN, US', 'false', '8', 'Y');
insert into MAPS (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY, SHOWBYDEFAULT)
values (40, 'CCM', 'IVER', 'Ivers Science Building', null, null, null, 'https://m-test.cord.edu/images/building_images/ivers.jpg', '-96.768914', '46.863885', 2, 'superadmin', to_timestamp('28-08-2013 10:25:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1001 8th Street South, Moorhead, MN, US', 'false', '1', 'Y');
commit;
prompt 66 records loaded
prompt Loading MAPS_AUDIT...
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCN', 'RIV', 'Riverside  Center', 'Riverside Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.774069', '46.860888', null, 'null', to_timestamp('13-03-2013 14:48:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCN', 'RIV', 'Riverside Center', 'Riverside Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.774069', '46.860888', null, 'null', to_timestamp('06-03-2013 06:58:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCN', 'RIV', 'Riverside', 'Riverside Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.774069', '46.860888', 2, 'SUPERADMIN', to_timestamp('06-03-2013 06:59:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (48, 'CCN', 'PRES', 'Presidents Residence', 'Presidents Residence', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.77055', '46.863727', null, 'null', to_timestamp('07-03-2013 10:36:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.769863', '46.865561', null, 'null', to_timestamp('20-03-2013 15:46:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (50, 'CCM', 'BELL', 'Bell Tower', 'Centennial Campanile', 'null', 'null', 'null', '-96.769434', '46.864529', null, 'null', to_timestamp('17-05-2013 11:02:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINN', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site', '218-586-8830', 'null', 'null', '-94.725634', '47.555539', 2, 'SUPERADMIN', to_timestamp('17-05-2013 13:14:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINN', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site', '218-586-8830', 'null', '47.55546', '-94.72454', '47.555539', 2, 'SUPERADMIN', to_timestamp('17-05-2013 13:15:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINN', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site', '218-586-8830', 'null', 'null', '-94.72454', '47.55546', 2, 'SUPERADMIN', to_timestamp('17-05-2013 13:16:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (54, 'CLV', 'FRNCH1', 'Lac du Bois, French Village', 'Lac du Bois, French Village, Bemidji Site', '218-586-8530', 'null', 'null', '-94.738799â', '47.564552', null, 'null', to_timestamp('17-05-2013 13:50:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (57, 'CLV', 'GERMAN', 'Waldsee, German Village', 'Waldsee, German Village, Bemidji Site', '218-586-8530', 'clv@cord.edu', 'null', '-94.738787', '47.559402', null, 'null', to_timestamp('17-05-2013 14:14:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (62, 'CLV', 'SPNSH2', 'El Lago del Bosque-Wilder Forest, Spanish Village', 'El Lago del Bosque-Wilder Forest, Spanish Village, Marine on St. Croix', '651-433-4375', 'clv@cord.edu', 'null', '-92.818766', '45.163673', null, 'null', to_timestamp('17-05-2013 14:48:06.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (63, 'CLV', 'SWDISH', 'Sjolunden, Swedish Village', 'Sjolunden, Swedish Village, Bemidji Site', '218-586-8830', 'clv@cord.edu', 'null', '-94.725688', '47.555547', null, 'null', to_timestamp('17-05-2013 15:11:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site, ', 'null', 'clv@cord.edu', 'null', '-94.638231', '47.465051', 2, 'SUPERADMIN', to_timestamp('17-05-2013 15:30:39.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINNSH', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site', '218-586-8830', 'clv@cord.edu', 'null', '-94.72454', '47.55546', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:56:13.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (54, 'CLV', 'FRNCH1', 'Lac du Bois, French Village', 'Lac du Bois, French Village, Bemidji Site', '218-586-8530', 'clv@cord.edu', 'null', '-94.738879', '47.564709', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:56:35.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (58, 'CLV', 'NORWAY', 'Skogfjorden, Norwegian Village', 'Skogfjorden, Norwegian Village, Bemidji Site', '218-586-8730', 'clv@cord.edu', 'null', '-94.730816', '47.551463', null, 'null', to_timestamp('17-05-2013 16:57:48.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (62, 'CLV', 'SPNSH2', 'El Lago del Bosque-Wilder Forest, Spanish Village', 'El Lago del Bosque-Wilder Forest, Spanish Village, Marine on St. Croix', '651-433-4375', 'clv@cord.edu', 'null', '-92.815601', '45.166487', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:58:55.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (65, 'CLV', 'KOREAN', 'Sup sogui Hosu, Korean Village', 'Sup sogui Hosu, Korean Village, Bemidji Site', '218-586-8430', 'clv@cord.edu', 'null', '-94.715828', '47.575994', null, 'null', to_timestamp('17-05-2013 17:03:13.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House ', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/entourage/resources/005/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'SUPERADMIN', to_timestamp('04-06-2013 08:24:45.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool', 'Swimming Pool', 'null', 'null', 'null', '-96.770014', '46.862429', 2, 'SUPERADMIN', to_timestamp('04-06-2013 08:47:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swim Pool', 'Swimming Pool', 'null', 'null', 'null', '-96.770014', '46.862429', 2, 'SUPERADMIN', to_timestamp('04-06-2013 09:49:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', to_timestamp('14-06-2013 23:51:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', 'Berg Steam Plant', 'null', 'null', 'https://m-test.cord.edu/images/building_images/berg_steam_plant.jpg', '-96.770791', '46.865884', 2, 'SUPERADMIN', to_timestamp('23-08-2013 05:46:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '812 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', 'Berg Steam Plant', 'null', 'null', 'https://m-test.cord.edu/images/building_images/berg_steam_plant.jpg', '-96.770791', '46.865884', 2, 'superadmin', to_timestamp('23-08-2013 05:46:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '812 6th Street South, Moorhead, MN, US', '1,2,3', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'n2njmork2', to_timestamp('23-08-2013 08:08:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '714 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'n2njmork2', to_timestamp('23-08-2013 08:08:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('23-08-2013 08:08:47.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '1,2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'superadmin', to_timestamp('23-08-2013 08:08:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '714 7th Street South, Moorhead, MN, US', '1,2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (3, 'CCM', 'ADVCTR', 'Advancement Center', 'Advancement Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/advancement_center.jpg', '-96.767337', '46.863228', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:17:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1022 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'superadmin', to_timestamp('28-08-2013 09:27:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '714 7th Street South, Moorhead, MN, US', '2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('28-08-2013 09:30:45.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', 'Berg Steam Plant', 'null', 'null', 'https://m-test.cord.edu/images/building_images/berg_steam_plant.jpg', '-96.770791', '46.865884', 2, 'superadmin', to_timestamp('28-08-2013 09:31:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '812 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/berg_steam_plant.jpg', '-96.770791', '46.865884', 2, 'superadmin', to_timestamp('28-08-2013 09:31:42.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '812 6th Street South, Moorhead, MN, US', '1,2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (5, 'CCM', 'BOEO', 'Boe-Olsen Apartments', 'Boe-Olsen Apartments', 'null', 'null', 'https://m-test.cord.edu/images/building_images/boe_olsen_apartments.jpg', '-96.767492', '46.863646', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:33:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 South 10th Avenue, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'Bogstad East', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'n2njmork2', to_timestamp('28-08-2013 09:33:15.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '617 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (7, 'CCM', 'BOGM', 'Bogstad Manor', 'Bogstad Manor', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad_manor.jpg', '-96.769059', '46.867864', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:33:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '618 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (8, 'CCM', 'BRWN', 'Brown Hall', 'Brown Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/brown_hall.jpg', '-96.771977', '46.864398', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:33:42.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '515 9th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (5, 'CCM', 'BOEO', 'Boe-Olsen Apartments', 'Boe-Olsen Apartments', 'null', 'null', 'https://m-test.cord.edu/images/building_images/boe_olsen_apartments.jpg', '-96.767492', '46.863646', 2, 'superadmin', to_timestamp('28-08-2013 09:34:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 South 10th Avenue, Moorhead, MN, US', '8', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'Bogstad East', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'superadmin', to_timestamp('28-08-2013 09:34:26.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '617 8th Street South, Moorhead, MN, US', '8', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (7, 'CCM', 'BOGM', 'Bogstad Manor', 'Bogstad Manor', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad_manor.jpg', '-96.769059', '46.867864', 2, 'superadmin', to_timestamp('28-08-2013 09:34:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '618 7th Street South, Moorhead, MN, US', '8', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (8, 'CCM', 'BRWN', 'Brown Hall', 'Brown Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/brown_hall.jpg', '-96.771977', '46.864398', 2, 'superadmin', to_timestamp('28-08-2013 09:34:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '515 9th Avenue South, Moorhead, MN, US', '8', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (9, 'CCM', 'BW', 'Bishop Whipple Hall', 'Bishop Whipple Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bishop_whipple.jpg', '-96.770089', '46.865942', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:34:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('28-08-2013 09:35:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (10, 'CCM', 'LIB', 'Carl B. Ylvisaker Library', 'Carl B. Ylvisaker Library', 'null', 'null', 'https://m-test.cord.edu/images/building_images/library.jpg', '-96.769761', '46.864593', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:35:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1010 10th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (10, 'CCM', 'LIB', 'Carl B. Ylvisaker Library', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/library.jpg', '-96.769761', '46.864593', 2, 'superadmin', to_timestamp('28-08-2013 09:35:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1010 10th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (11, 'CCM', 'LIV', 'Livedalen Hall', 'Livedalen Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/livedalen_hall.jpg', '-96.771575', '46.864138', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:36:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1002 5th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (12, 'CCM', 'LORE', 'Lorentzsen Hall', 'Lorentzsen Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/lorentzsen_hall.jpg', '-96.768731', '46.864743', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:36:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '901 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (14, 'CCM', 'MPOC', 'Mugaas Plant Operation Center', 'Mugaas Plant Operation Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/mugaas.jpg', '-96.771333', '46.865928', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:37:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '801 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (15, 'CCM', 'MUS', 'Hvidsten Hall of Music', 'Hvidsten Hall of Music', 'null', 'null', 'https://m-test.cord.edu/images/building_images/hvidsten.jpg', '-96.771709', '46.862817', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:37:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1112 5th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (16, 'CCM', 'NORM', 'Normandy/Bookstore', 'Normandy/Bookstore', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bookstore.jpg', '-96.771183', '46.864314', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:37:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '929 6th St S, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (17, 'CCM', 'OFC', 'Offutt Concourse', 'Offutt Concourse', 'null', 'null', 'https://m-test.cord.edu/images/building_images/offutt_concourse.jpg', '-96.769445', '46.862788', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:38:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1111 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (18, 'CCM', 'OF', 'Olson Forum', 'Olson Forum', 'null', 'null', 'https://m-test.cord.edu/images/building_images/olson_forum.jpg', '-96.768876', '46.862678', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:38:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1111 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (19, 'CCM', 'OLDM', 'Old Main', 'Old Main', 'null', 'null', 'https://m-test.cord.edu/images/building_images/old_main.jpg', '-96.768898', '46.865583', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:38:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '815 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (20, 'CCM', 'PARK', 'Park Region Hall', 'Park Region Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/park_region.jpg', '-96.769338', '46.867035', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:39:09.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '701 7th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center Riverside Center ', '123-123-1234', 'bhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', to_timestamp('28-08-2013 09:43:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '310 14th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (22, 'CCM', 'SKY', 'Olson Skyway', 'Olson Skyway', 'null', 'null', 'https://m-test.cord.edu/images/building_images/olson_skyway.jpg', '-96.767814', '46.861728', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:43:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1101 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (23, 'CCM', 'STAD', 'Jake Christiansen Stadium', 'Jake Christiansen Stadium', 'null', 'null', 'https://m-test.cord.edu/images/building_images/jake_christiansen_side.jpg', '-96.765829', '46.859017', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:45:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1500 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool', 'Swimming Pool', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('28-08-2013 09:45:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '810 12th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (25, 'CCM', 'TE', 'Townhouse East', 'Townhouse East', 'null', 'null', 'https://m-test.cord.edu/images/building_images/townhouse_east.jpg', '-96.765121', '46.861541', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:56:21.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '917 12th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (26, 'CCM', 'TW', 'Townhouse West', 'Townhouse West', 'null', 'null', 'https://m-test.cord.edu/images/building_images/townhouse_west.jpg', '-96.765904', '46.861592', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:58:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '915 12th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (27, 'CCM', 'WELC', 'Welcome Center', 'Welcome Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/welcome_center.jpg', '-96.767514', '46.862473', 2, 'SUPERADMIN', to_timestamp('28-08-2013 09:58:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1118 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (27, 'CCM', 'WELC', 'Welcome Center', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/welcome_center.jpg', '-96.767514', '46.862473', 2, 'superadmin', to_timestamp('28-08-2013 09:59:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1118 8th Street South, Moorhead, MN, US', '2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (28, 'CCM', 'CC ECO', 'Eco House', 'Eco House', 'null', 'null', 'https://m-test.cord.edu/images/building_images/eco_house.jpg', '-96.770588', '46.867607', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:01:24.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '618 South 6th St, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (29, 'CCM', 'ERIC', 'Erickson Hall', 'Erickson Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/erickson_hall.jpg', '-96.766891', '46.860499', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:01:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1320 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (30, 'CCM', 'FH', 'Memorial Auditorium', 'Memorial Auditorium', 'null', 'null', 'https://m-test.cord.edu/images/building_images/memorial.jpg', '-96.770078', '46.862935', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:02:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1117 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (31, 'CCM', 'FJEL', 'Fjelstad Hall', 'Fjelstad Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/fjelstad.jpg', '-96.770228', '46.86654', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:04:06.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '720 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (32, 'CCM', 'GCTR', 'Grant Center', 'Grant Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/grant_center.jpg', '-96.767235', '46.86113', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:05:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1300 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (33, 'CCM', 'GROS', 'Grose Hall', 'Grose Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/grose_hall.jpg', '-96.770078', '46.865744', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:22:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '811 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (34, 'CCM', 'HAL', 'Hallett Hall', 'Hallett Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/hallet_hall.jpg', '-96.767071', '46.861449', 2, 'n2njmork2', to_timestamp('28-08-2013 10:23:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '815 12th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (35, 'CCM', 'HLTH', 'Kjos Health Center', 'Kjos Health Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/kjos_health_center.jpg', '-96.771081', '46.863687', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:23:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1011 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (35, 'CCM', 'HLTH', 'Kjos Health Center', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/kjos_health_center.jpg', '-96.771081', '46.863687', 2, 'superadmin', to_timestamp('28-08-2013 10:24:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1011 6th Street South, Moorhead, MN, US', '1,2', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (36, 'CCM', 'HOY', 'Hoyum Hall', 'Hoyum Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/hoyum_hall.jpg', '-96.771116', '46.863399', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:24:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1021 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (37, 'CCM', 'IC02', 'International Apartments 1102', 'International Apartments 1102', 'null', 'null', 'https://m-test.cord.edu/images/building_images/international_apartments_1102.jpg', '-96.767535', '46.862931', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:24:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1102 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (38, 'CCM', 'IC06', 'International Apartments 1106', 'International Apartments 1106', 'null', 'null', 'https://m-test.cord.edu/images/building_images/international_apartments_1106.jpg', '-96.767272', '46.862806', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:25:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1106 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (51, 'CLV', 'ARABIC', 'Al-Waha', 'Al-Waha, Arabic LV, Bemidji Site', 'null', 'null', 'null', '-94.730816', '47.551463', null, 'null', to_timestamp('17-05-2013 11:24:35.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (49, 'CCM', 'POND', 'Prexy''s Pond', null, 'null', 'null', 'null', '-96.769064', '46.866295', null, 'null', to_timestamp('24-05-2013 21:59:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (18, 'CCM', 'OF', 'Olson Forum', 'Olson Forum', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768876', '46.862678', null, 'null', to_timestamp('29-05-2013 10:12:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (7, 'CCM', 'BOGM', 'Bogstad Manor', 'Bogstad Manor', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.769059', '46.867864', null, 'null', to_timestamp('29-05-2013 10:15:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (8, 'CCM', 'BRWN', 'Brown Hall', 'Brown Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771977', '46.864398', null, 'null', to_timestamp('29-05-2013 10:17:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (9, 'CCM', 'BW', 'Bishop Whipple Hall', 'Bishop Whipple Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770089', '46.865942', null, 'null', to_timestamp('29-05-2013 10:18:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (23, 'CCM', 'STAD', 'Jake Christiansen Stadium', 'Jake Christiansen Stadium', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.765829', '46.859017', null, 'null', to_timestamp('29-05-2013 10:20:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (15, 'CCM', 'MUS', 'Hvidsten Hall of Music', 'Hvidsten Hall of Music', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771709', '46.862817', null, 'null', to_timestamp('29-05-2013 10:20:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (42, 'CCM', 'KCC', 'Knutson Campus Center', 'Knutson Campus Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.769933', '46.865322', null, 'null', to_timestamp('29-05-2013 10:21:49.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (30, 'CCM', 'FH', 'Memorial Auditorium', 'Memorial Auditorium', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770078', '46.862935', null, 'null', to_timestamp('29-05-2013 10:22:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (46, 'CCM', 'FFCT', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770427', '46.86416', null, 'null', to_timestamp('29-05-2013 10:39:15.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (43, 'CCM', 'MP', 'Car Pool', 'Car Pool', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.76614', '46.859527', null, 'null', to_timestamp('29-05-2013 10:40:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (33, 'CCM', 'GROS', 'Grose Hall', 'Grose Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770078', '46.865744', null, 'null', to_timestamp('29-05-2013 10:40:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool', 'Swimming Pool', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770014', '46.862429', null, 'null', to_timestamp('29-05-2013 11:17:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (71, 'CCM', 'TE', 'TEST', 'Description', 'null', 'null', 'null', '54.12321', '54.12131', null, 'null', to_timestamp('05-06-2013 10:06:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (3, 'CCM', 'ADVCTR', 'Advancement Center', 'Advancement Center', 'null', 'null', 'null', '-96.767337', '46.863228', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:38:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (26, 'CCM', 'TW', 'Townhouse West', 'Townhouse West', 'null', 'null', 'null', '-96.765904', '46.861592', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:41:08.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (43, 'CCM', 'MP', 'Car Pool', 'Car Pool', 'null', 'null', 'null', '-96.76614', '46.859527', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:41:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (25, 'CCM', 'TE', 'Townhouse East', 'Townhouse East', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.765121', '46.861541', null, 'null', to_timestamp('07-06-2013 18:42:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', 'Berg Steam Plant', 'null', 'null', 'null', '-96.770791', '46.865884', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:44:03.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (28, 'CCM', 'CC ECO', 'Eco House', 'Eco House', 'null', 'null', 'null', '-96.770588', '46.867607', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:44:45.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (50, 'CCM', 'BELL', 'Bell Tower', 'Bell Tower', 'null', 'null', 'null', '-96.769434', '46.864529', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:45:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (22, 'CCM', 'SKY', 'Olson Skyway', 'Olson Skyway', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767814', '46.861728', null, 'null', to_timestamp('07-06-2013 18:46:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (49, 'CCM', 'POND', 'Prexy''s Pond', 'POND', 'null', 'null', 'null', '-96.769064', '46.866295', 2, 'n2njmork2', to_timestamp('07-06-2013 18:47:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.774069', '46.860888', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:47:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
commit;
prompt 100 records committed...
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (14, 'CCM', 'MPOC', 'Mugaas Plant Operation Center', 'Mugaas Plant Operation Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771333', '46.865928', null, 'null', to_timestamp('07-06-2013 18:47:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (29, 'CCM', 'ERIC', 'Erickson Hall', 'Erickson Hall', 'null', 'null', 'null', '-96.766891', '46.860499', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:51:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (5, 'CCM', 'BOEO', 'Boe-Olsen Apartments', 'Boe-Olsen Apartments', 'null', 'null', 'null', '-96.767492', '46.863646', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:52:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (33, 'CCM', 'GROS', 'Grose Hall', 'Grose Hall', 'null', 'null', 'null', '-96.770078', '46.865744', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:56:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard HouseS', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/entourage/resources/005/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'SUPERADMIN', to_timestamp('07-06-2013 18:59:12.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (37, 'CCM', 'IC02', 'International Apartments 1102', 'International Apartments 1102', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767535', '46.862931', null, 'null', to_timestamp('07-06-2013 19:00:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (39, 'CCM', 'IC10', 'International Apartments 1110', 'International Apartments 1110', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767546', '46.862711', null, 'null', to_timestamp('07-06-2013 19:00:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (38, 'CCM', 'IC06', 'International Apartments 1106', 'International Apartments 1106', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767272', '46.862806', null, 'null', to_timestamp('07-06-2013 19:00:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (11, 'CCM', 'LIV', 'Livedalen Hall', 'Livedalen Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771575', '46.864138', null, 'null', to_timestamp('07-06-2013 19:02:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (12, 'CCM', 'LORE', 'Lorentzsen Hall', 'Lorentzsen Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768731', '46.864743', null, 'null', to_timestamp('07-06-2013 19:02:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (35, 'CCM', 'HLTH', 'Kjos Health Center', 'Kjos Health Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771081', '46.863687', null, 'null', to_timestamp('07-06-2013 19:03:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (36, 'CCM', 'HOY', 'Hoyum Hall', 'Hoyum Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771116', '46.863399', null, 'null', to_timestamp('07-06-2013 19:04:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool', '123-123-1234', 'bbhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:24:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center', '123-123-1234', 'bhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', to_timestamp('15-06-2013 05:32:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'n2njmork2', to_timestamp('28-06-2013 09:11:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINN', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site', '218-586-8830', 'null', 'null', '-94.724444', '47.55598', null, 'null', to_timestamp('17-05-2013 13:12:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (54, 'CLV', 'FRNCH1', 'Lac du Bois, French Village', 'Lac du Bois, French Village, Bemidji Site', '218-586-8530', 'null', 'null', '-94.738787', '47.559424', 2, 'SUPERADMIN', to_timestamp('17-05-2013 14:12:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (51, 'CLV', 'ARABIC', 'Al-Waha, Arabic Village', 'Al-Waha, Arabic Village, Bemidji Site', '218-586-8730', 'null', 'null', '-94.730816', '47.551463', 2, 'SUPERADMIN', to_timestamp('17-05-2013 14:19:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (52, 'CLV', 'DANISH', 'Skovsoen, Danish Village', 'Skovsoen, Danish Village, Bemidji Site', '218-586-8820', 'null', 'null', '-94.724443', '47.55597', null, 'null', to_timestamp('17-05-2013 14:23:49.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (67, 'CLV', 'JAPAN', 'Mori no Ike, Japanese Village', 'Mori no Ike, Japanese Village, Dent SiteLakeside Camp40225 Purlieu RdDent, MN 56528', '218-758-2112', 'clv@cord.edu', 'null', '-95.774777', '46.547282', null, 'null', to_timestamp('17-05-2013 15:15:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site', '218-335-6304', 'clv@cord.edu', 'null', '-94.638231', '47.465051', 2, 'SUPERADMIN', to_timestamp('17-05-2013 15:31:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (69, 'CLV', 'CLVMHD', 'Concordia Language Villages-Moorhead Offices', 'Concordia Language Villages-Moorhead,  Administrative Offices', '218-299-4544', 'clv@cord.edu', 'null', '-96.773959', '46.860870', null, 'null', to_timestamp('17-05-2013 15:51:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (70, 'CLV', 'CLVBMJ', 'Concordia Language Villages-Bemidji Administration', 'Concordia Language Villages-Bemidji Administrative Offices', '218-586-8600', 'clv@cord.edu', 'null', '-94.728515', '47.550062', null, 'null', to_timestamp('17-05-2013 15:52:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (69, 'CLV', 'CLVMHD', 'Concordia Language Villages, Moorhead Administration', 'Concordia Language Villages-Moorhead,  Administrative Offices', '218-299-4544', 'clv@cord.edu', 'null', '-96.773959', '46.860870', 2, 'SUPERADMIN', to_timestamp('17-05-2013 15:53:49.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (70, 'CLV', 'CLVBMJ', 'Concordia Language Villages, Bemidji Administration', 'Concordia Language Villages-Bemidji Administrative Offices', '218-586-8600', 'clv@cord.edu', 'null', '-94.728515', '47.550062', 2, 'SUPERADMIN', to_timestamp('17-05-2013 15:55:12.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (51, 'CLV', 'ARABIC', 'Al-Waha, Arabic Village', 'Al-Waha, Arabic Village, Bemidji Site', '218-586-8730', 'clv@cord.edu', 'null', '-94.730381', '47.552085', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:21:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (55, 'CLV', 'FRNCH2', 'Les Voyageurs, French Camping', 'Les Voyageurs, French Camping, base camp Bemidji', '218-586-8600', 'clv@cord.edu', 'null', '-94.738246', '47.563674', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:56:54.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (59, 'CLV', 'SPNSH1', 'El Lago del Bosque, Spanish Village', 'El Lago del Bosque, Spanish Village, Bemidji Site', '218-586-8930', 'clv@cord.edu', 'null', '-94.7269', '47.563084', null, 'null', to_timestamp('17-05-2013 16:58:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (64, 'CLV', 'RUSSIA', 'Lesnoe Ozero, Russian Village', 'Lesnoe Ozero, Russian Village, Bemidji Site', '218-586-8430', 'clv@cord.edu', 'null', '-94.714910', '47.575447', null, 'null', to_timestamp('17-05-2013 17:03:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://mapps.cord.edu/entourage/resources/005/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'SUPERADMIN', to_timestamp('25-05-2013 08:36:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House ', 'Aasgaard House', 'null', 'null', 'https://mapps.cord.edu/entourage/resources/005/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'SUPERADMIN', to_timestamp('29-05-2013 10:11:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (20, 'CCM', 'PARK', 'Park Region Hall', 'Park Region Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.769338', '46.867035', null, 'null', to_timestamp('29-05-2013 10:13:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'Bogstad East', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768415', '46.867541', null, 'null', to_timestamp('29-05-2013 10:15:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (41, 'CCM', 'JONE', 'Jones Science Center', 'Jones Science Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.76908', '46.863492', 2, 'SUPERADMIN', to_timestamp('29-05-2013 10:15:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (31, 'CCM', 'FJEL', 'Fjelstad Hall', 'Fjelstad Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770228', '46.86654', null, 'null', to_timestamp('29-05-2013 10:17:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (16, 'CCM', 'NORM', 'Normandy/Bookstore', 'Normandy/Bookstore', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.771183', '46.864314', null, 'null', to_timestamp('29-05-2013 10:29:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (29, 'CCM', 'ERIC', 'Erickson Hall', 'Erickson Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.766891', '46.860499', null, 'null', to_timestamp('29-05-2013 11:16:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (4, 'CCM', 'BERG', 'Berg Steam Plant', 'Berg Steam Plant', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770791', '46.865884', null, 'null', to_timestamp('29-05-2013 11:16:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (17, 'CCM', 'OFC', 'Offutt Concourse', 'Offutt Concourse', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.769445', '46.862788', null, 'null', to_timestamp('29-05-2013 11:17:13.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (26, 'CCM', 'TW', 'Townhouse West', 'Townhouse West', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.765904', '46.861592', null, 'null', to_timestamp('29-05-2013 11:17:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'n2njmork2', to_timestamp('04-06-2013 08:20:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (34, 'CCM', 'HAL', 'Hallett Hall', 'Hallett Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/hallet_hall.jpg', '-96.767071', '46.861449', 2, 'SUPERADMIN', to_timestamp('04-06-2013 08:20:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (49, 'CCM', 'POND', 'Prexy''s Pond', null, 'null', 'null', 'null', '-96.769064', '46.866295', 2, 'SUPERADMIN', to_timestamp('04-06-2013 08:21:02.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (34, 'CCM', 'HAL', 'Hallett Hall', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/hallet_hall.jpg', '-96.767071', '46.861449', 2, 'n2njmork2', to_timestamp('04-06-2013 08:24:59.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (64, 'CLV', 'RUSSIA', 'Lesnoe Ozero, Russian Village', 'Lesnoe Ozero, Russian Village, Bemidji Site, June-July', '218-586-8430', 'clv@cord.edu', 'null', '-94.714910', '47.575447', 2, 'SUPERADMIN', to_timestamp('04-06-2013 08:25:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'n2njmork2', to_timestamp('14-06-2013 06:40:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'Bogstad East', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'SUPERADMIN', to_timestamp('16-06-2013 06:58:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'Bogstad', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'n2njmork2', to_timestamp('16-06-2013 06:58:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', 'BogstadOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1GOGFad6gWgT1G', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'n2njmork2', to_timestamp('16-06-2013 06:59:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'n2njmork2', to_timestamp('28-06-2013 09:09:55.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (48, 'CCM', 'PRES', 'President''s Residence', 'President''s Residence', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.77055', '46.863727', 2, 'SUPERADMIN', to_timestamp('17-05-2013 10:56:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (55, 'CLV', 'FRNCH2', 'Les Voyageurs, French Camping', 'Les Voyageurs, French Camping, base camp Bemidji', '218-586-8600', 'clv@cord.edu', 'null', '-94.738771', '47.558696', null, 'null', to_timestamp('17-05-2013 14:13:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINNSH', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site', '218-586-8830', 'null', 'null', '-94.72454', '47.55546', 2, 'SUPERADMIN', to_timestamp('17-05-2013 14:23:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan', null, 'null', 'null', 'null', '-94.638231', '47.465051', null, 'null', to_timestamp('17-05-2013 15:29:47.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site, ', 'null', 'clv@cord.edu', 'null', '-94.638231', '47.465051', 2, 'SUPERADMIN', to_timestamp('17-05-2013 15:30:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site', '218-335-6304', 'clv@cord.edu', 'null', '-94.638231', '47.465051', 2, 'SUPERADMIN', to_timestamp('17-05-2013 15:31:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (56, 'CLV', 'FRNCH3', 'Lac du Bois-Camp Holiday', 'Lac du Bois-Camp Holiday, Hackensack Site', '218-682-2570', 'clv@cord.edu', 'null', '-94.35304', '46.970332', null, 'null', to_timestamp('17-05-2013 16:57:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (60, 'CLV', 'PRTUGS', 'Mar e Floresta, Portugese Village', 'Mar e Floresta, Portugese Village, Callaway Site', '218-375-4021', 'clv@cord.edu', 'null', '-95.776663', '47.009523', null, 'null', to_timestamp('17-05-2013 16:58:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (63, 'CLV', 'SWEDEN', 'Sjolunden, Swedish Village', 'Sjolunden, Swedish Village, Bemidji Site', '218-586-8830', 'clv@cord.edu', 'null', '-94.725688', '47.555547', 2, 'SUPERADMIN', to_timestamp('17-05-2013 17:03:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (66, 'CLV', 'ITALY', 'Lago Del Bosco-Camp Holiday, Italian Village', 'Lago Del Bosco-Camp Holiday, Italian Village, Hackensack Site', '218-682-2570', 'clv@cord.edu', 'null', '-94.351954', '46.970513', null, 'null', to_timestamp('17-05-2013 17:04:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (67, 'CLV', 'JAPAN', 'Mori no Ike, Japanese Village', 'Mori no Ike, Japanese Village, Dent Site, Lakeside Camp  40225 Purlieu Rd  Dent, MN 56528', '218-758-2112', 'clv@cord.edu', 'null', '-95.774777', '46.547282', 2, 'SUPERADMIN', to_timestamp('17-05-2013 17:05:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site', '218-335-6304', 'clv@cord.edu', 'null', '-94.638231', '47.465051', 2, 'SUPERADMIN', to_timestamp('17-05-2013 17:05:36.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'n2njmork2', to_timestamp('14-06-2013 06:13:09.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool', 'Swimming Pool', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'SUPERADMIN', to_timestamp('14-06-2013 06:20:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool  - change', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:23:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool  - change', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:23:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool  - change', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:23:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool - change', '123-123-1234', 'bbhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:24:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool - change', '123-123-1234', 'bbhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:25:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool - change', '123-123-1234', 'bbhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:30:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard HouseS', 'Aasgaard House', 'null', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'SUPERADMIN', to_timestamp('14-06-2013 06:37:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center - Change', 'Riverside Center - Change', '123-123-1234', 'bhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', to_timestamp('15-06-2013 05:31:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center - Change', 'Riverside Center - Change', '123-123-1234', 'bhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', to_timestamp('15-06-2013 05:32:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (52, 'CLV', 'DANISH', 'Skovsoen, Danish Village', 'Skovsoen, Danish Village, Bemidji Site', '218-586-8820', 'clv@cord.edu', 'null', '-94.724443', '47.55597', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:55:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (57, 'CLV', 'GERMAN', 'Waldsee, German Village', 'Waldsee, German Village, Bemidji Site', '218-586-8630', 'clv@cord.edu', 'null', '-94.738787', '47.559402', 2, 'SUPERADMIN', to_timestamp('17-05-2013 16:57:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (61, 'CLV', 'CHINA', 'Sen Lin Hu, Chinese Village', 'Sen Lin Hu, Chinese Village, Callaway Site', '218-375-4021', 'clv@cord.edu', 'null', '-95.776298', '47.008602', null, 'null', to_timestamp('17-05-2013 16:58:34.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (67, 'CLV', 'JAPAN', 'Mori no Ike, Japanese Village', 'Mori no Ike, Japanese Village, Dent Site, Lakeside Camp  40225 Purlieu Rd  Dent, MN 56528', '218-758-2112', 'clv@cord.edu', 'null', '-95.774777', '46.547282', 2, 'SUPERADMIN', to_timestamp('17-05-2013 17:04:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://mapps.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'SUPERADMIN', to_timestamp('29-05-2013 10:10:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (19, 'CCM', 'OLDM', 'Old Main', 'Old Main', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768898', '46.865583', null, 'null', to_timestamp('29-05-2013 10:13:55.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (32, 'CCM', 'GCTR', 'Grant Center', 'Grant Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767235', '46.86113', null, 'null', to_timestamp('29-05-2013 10:16:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (27, 'CCM', 'WELC', 'Welcome Center', 'Welcome Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767514', '46.862473', null, 'null', to_timestamp('29-05-2013 10:19:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (40, 'CCM', 'IVER', 'Ivers Science Building', 'Ivers Science Building', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768914', '46.863885', null, 'null', to_timestamp('29-05-2013 10:20:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (10, 'CCM', 'LIB', 'Carl B. Ylvisaker Library', 'Carl B. Ylvisaker Library', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.769761', '46.864593', null, 'null', to_timestamp('29-05-2013 10:22:04.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (34, 'CCM', 'HAL', 'Hallett Hall', 'Hallett Hall', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767071', '46.861449', null, 'null', to_timestamp('29-05-2013 10:28:49.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (45, 'CCM', 'OLIN', 'Olin Art and Communications Center', 'Olin Art and Communications Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.77055', '46.863727', null, 'null', to_timestamp('29-05-2013 10:30:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (3, 'CCM', 'ADVCTR', 'Advancement Center', 'Advancement Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767337', '46.863228', null, 'null', to_timestamp('29-05-2013 10:32:51.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (48, 'CCM', 'PRES', 'President''s Residence', 'President''s Residence', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768458', '46.866997', 2, 'SUPERADMIN', to_timestamp('29-05-2013 10:39:52.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (5, 'CCM', 'BOEO', 'Boe-Olsen Apartments', 'Boe-Olsen Apartments', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.767492', '46.863646', null, 'null', to_timestamp('29-05-2013 11:16:45.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (28, 'CCM', 'CC ECO', 'Eco House', 'Eco House', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.770588', '46.867607', null, 'null', to_timestamp('29-05-2013 11:17:21.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy Hall', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'SUPERADMIN', to_timestamp('04-06-2013 08:18:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (12, 'CCM', 'LORE', 'Lorentzsen Hall', 'Lorentzsen Hall', 'null', 'null', 'null', '-96.768731', '46.864743', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:25:58.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (14, 'CCM', 'MPOC', 'Mugaas Plant Operation Center', 'Mugaas Plant Operation Center', 'null', 'null', 'null', '-96.771333', '46.865928', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:27:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (17, 'CCM', 'OFC', 'Offutt Concourse', 'Offutt Concourse', 'null', 'null', 'null', '-96.769445', '46.862788', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:28:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (35, 'CCM', 'HLTH', 'Kjos Health Center', 'Kjos Health Center', 'null', 'null', 'null', '-96.771081', '46.863687', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:30:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool', 'Swimming Pool', 'null', 'null', 'null', '-96.770014', '46.862429', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:33:57.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (48, 'CCM', 'PRES', 'President''s Residence', 'President''s Residence', 'null', 'null', 'null', '-96.768458', '46.866997', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:36:48.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (39, 'CCM', 'IC10', 'International Apartments 1110', 'International Apartments 1110', 'null', 'null', 'null', '-96.767546', '46.862711', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:41:31.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (38, 'CCM', 'IC06', 'International Apartments 1106', 'International Apartments 1106', 'null', 'null', 'null', '-96.767272', '46.862806', 2, 'SUPERADMIN', to_timestamp('08-06-2013 21:41:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'Academy', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'n2njmork2', to_timestamp('14-06-2013 06:13:20.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center', 'null', 'null', 'null', '-96.774069', '46.860888', 2, 'SUPERADMIN', to_timestamp('14-06-2013 06:15:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
commit;
prompt 200 records committed...
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:22:13.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (24, 'CCM', 'SWIM', 'Swimming Pool - change', 'Swimming Pool  - change', 'null', 'null', 'https://m-test.cord.edu/images/building_images/swimming_pool.jpg', '-96.770014', '46.862429', 2, 'n2njmork2', to_timestamp('14-06-2013 06:23:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', 'Riverside Center', '123-123-1234', 'bhanuprakash@yahoo.com', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'n2njmork2', to_timestamp('15-06-2013 05:55:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (50, 'CCM', 'BELL', 'Bell Tower', 'Bell Tower', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.769434', '46.864529', 2, 'SUPERADMIN', to_timestamp('19-06-2013 03:37:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (50, 'CCM', 'BELL', 'Bell Towericonandas', 'Bell Tower is good', '789-458-7894', 'null', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.769434', '46.864529', 2, 'n2njmork2', to_timestamp('19-06-2013 03:40:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (13, 'CCN', 'MP', 'Car Pool', 'Car Pool', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.76614', '46.859527', null, 'null', to_timestamp('06-03-2013 03:37:38.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (13, 'CCN', 'MP', 'Car', 'Car Pool', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.76614', '46.859527', 2, 'SUPERADMIN', to_timestamp('06-03-2013 03:37:53.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (41, 'CCN', 'JONE', 'Jones Science Center', 'Jones Science Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.76908', '46.863492', null, 'null', to_timestamp('06-03-2013 08:24:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (41, 'CCN', 'JONE', 'Jone Science Center', 'Jone Science Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.76908', '46.863492', 2, 'SUPERADMIN', to_timestamp('06-03-2013 08:25:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCN', 'AAS', 'Aasgaard House ', 'Aasgaard House', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768951', '46.866646', null, 'null', to_timestamp('07-03-2013 10:33:15.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCN', 'AAS', 'Aasgaard''s House ', 'Aasgaard''s House', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768951', '46.866646', 2, 'SUPERADMIN', to_timestamp('07-03-2013 10:33:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House ', 'Aasgaard House', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.768951', '46.866646', 2, 'SUPERADMIN', to_timestamp('20-03-2013 15:44:32.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCN', 'RIV', 'Riverside  Center', 'Riverside Center', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.774069', '46.860888', 2, 'SUPERADMIN', to_timestamp('06-03-2013 07:11:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCN', 'RIV', 'Riverside  Center', 'Riverside', 'null', 'null', 'http://www.cord.edu/About/Visit/campusmap.php', '-96.774069', '46.860888', 2, 'SUPERADMIN', to_timestamp('06-03-2013 07:11:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, null, 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'Communications and Marketing', '218-299-3147', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'superadmin', to_timestamp('02-10-2013 01:22:22.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '714 7th Street South, Moorhead, MN, US', '2', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (40, 'CCM', 'IVER', 'Ivers Science Building', 'Ivers Science Building', 'null', 'null', 'https://m-test.cord.edu/images/building_images/ivers.jpg', '-96.768914', '46.863885', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:25:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1001 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (41, 'CCM', 'JONE', 'Jones Science Center', 'Jones Science Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/jones.jpg', '-96.76908', '46.863492', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:25:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1021 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (43, 'CCM', 'MP', 'Car Pool', 'Car Pool', 'null', 'null', 'https://m-test.cord.edu/images/building_images/car_pool.jpg', '-96.76614', '46.859527', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:27:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1402 9th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (45, 'CCM', 'OLIN', 'Olin Art and Communications Center', 'Olin Art and Communications Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/olin.jpg', '-96.77055', '46.863727', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:27:21.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1010 6th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (48, 'CCM', 'PRES', 'President''s Residence', 'President''s Residence', 'null', 'null', 'https://m-test.cord.edu/images/building_images/presidents_residence.jpg', '-96.768458', '46.866997', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:28:10.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (49, 'CCM', 'POND', 'Prexy''s Pond', 'POND', 'null', 'null', 'https://m-test.cord.edu/images/building_images/pond.jpg', '-96.769064', '46.866295', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:28:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (51, 'CLV', 'ARABIC', 'Al-Waha, Arabic Village', 'Al-Waha, Arabic Village, Bemidji Site, July-August', '218-586-8730', 'clv@cord.edu', 'null', '-94.730381', '47.552085', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:29:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (52, 'CLV', 'DANISH', 'Skovsoen, Danish Village', 'Skovsoen, Danish Village, Bemidji Site, June-July', '218-586-8820', 'clv@cord.edu', 'null', '-94.724443', '47.55597', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:29:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (54, 'CLV', 'FRNCH1', 'Lac du Bois, French Village', 'Lac du Bois, French Village, Bemidji Site, February-August', '218-586-8530', 'clv@cord.edu', 'null', '-94.738879', '47.564709', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:30:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (55, 'CLV', 'FRNCH2', 'Les Voyageurs, French Camping', 'Les Voyageurs, French Camping, base camp Bemidji, June-August', '218-586-8600', 'clv@cord.edu', 'null', '-94.738246', '47.563674', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:30:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (57, 'CLV', 'GERMAN', 'Waldsee, German Village', 'Waldsee, German Village, Bemidji Site, February-August', '218-586-8630', 'clv@cord.edu', 'null', '-94.738787', '47.559402', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:30:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (58, 'CLV', 'NORWAY', 'Skogfjorden, Norwegian Village', 'Skogfjorden, Norwegian Village, Bemidji Site, June-July', '218-586-8730', 'clv@cord.edu', 'null', '-94.730816', '47.551463', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:30:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (61, 'CLV', 'CHINA', 'Sen Lin Hu, Chinese Village', 'Sen Lin Hu, Chinese Village, Callaway Site, June-August', '218-375-4021', 'clv@cord.edu', 'null', '-95.776298', '47.008602', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:31:25.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (62, 'CLV', 'SPNSH2', 'El Lago del Bosque-Wilder Forest, Spanish Village', 'El Lago del Bosque-Wilder Forest, Spanish Village, Marine on St. Croix, June-August', '651-433-4375', 'clv@cord.edu', 'null', '-92.815601', '45.166487', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:31:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (64, 'CLV', 'RUSSIA', 'Lesnoe Ozero, Russian Village', 'Lesnoe Ozero, Russian Village, Bemidji Site, ', '218-586-8430', 'clv@cord.edu', 'null', '-94.714910', '47.575447', 2, 'n2njmork2', to_timestamp('28-08-2013 10:32:05.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (65, 'CLV', 'KOREAN', 'Sup sogui Hosu, Korean Village', 'Sup sogui Hosu, Korean Village, Bemidji Site, July-August', '218-586-8430', 'clv@cord.edu', 'null', '-94.715828', '47.575994', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:32:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (67, 'CLV', 'JAPAN', 'Mori no Ike, Japanese Village', 'Mori no Ike, Japanese Village, Dent Site, 40225 Purlieu Rd  Dent, MN 56528, June-August', '218-758-2112', 'clv@cord.edu', 'null', '-95.774777', '46.547282', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:32:35.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (68, 'CLV', 'SPNSH3', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village', 'El Lago del Bosque-Camp Minne-wa-Kan, Spanish Village, Cass Lake Site, June-August', '218-335-6304', 'clv@cord.edu', 'null', '-94.638231', '47.465051', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:32:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (70, 'CLV', 'CLVBMJ', 'Concordia Language Villages, Bemidji Administration', 'Concordia Language Villages, Bemidji Administrative Offices at Centrum', '218-586-8600', 'clv@cord.edu', 'null', '-94.728515', '47.550062', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:33:08.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'The Office of Communications and Marketing is charged with protecting and promoting the Concordia College brand.', '218-299-3147', 'null', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'superadmin', to_timestamp('02-10-2013 01:49:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '714 7th Street South, Moorhead, MN, US', '2', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (51, 'CLV', 'ARABIC', 'Al-Waha, Arabic Village', 'Al-Waha, Arabic Village, Bemidji Site, July-August', '218-586-8730', 'clv@cord.edu', 'null', '-94.730381', '47.552085', 2, 'superadmin', to_timestamp('22-10-2013 03:37:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '5', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (39, 'CCM', 'IC10', 'International Apartments 1110', 'International Apartments 1110', 'null', 'null', 'https://m-test.cord.edu/images/building_images/international_apartments_1110.jpg', '-96.767546', '46.862711', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:25:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1110 8th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (42, 'CCM', 'KCC', 'Knutson Campus Center', 'Knutson Campus Center', 'null', 'null', 'https://m-test.cord.edu/images/building_images/campus_center.jpg', '-96.769933', '46.865322', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:26:41.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '624 9th Avenue South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (46, 'CCM', 'FFCT', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', 'Frances Frazier Comstock Theatre / Cyrus M. Running Gallery', 'null', 'null', 'https://m-test.cord.edu/images/building_images/comstock_theatre.jpg', '-96.770427', '46.86416', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:27:38.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1001 7th Street South, Moorhead, MN, US', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (50, 'CCM', 'BELL', 'Bell Tower', 'Bell Tower ', 'null', 'null', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.769434', '46.864529', 2, 'n2njmork2', to_timestamp('28-08-2013 10:28:48.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (53, 'CLV', 'FINNSH', 'Salolampi, Finnish Village', 'Salolampi, Finnish Village, Bemidji Site, June-July', '218-586-8830', 'clv@cord.edu', 'null', '-94.72454', '47.55546', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:29:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (56, 'CLV', 'FRNCH3', 'Lac du Bois-Camp Holiday', 'Lac du Bois-Camp Holiday, Hackensack Site, June-July', '218-682-2570', 'clv@cord.edu', 'null', '-94.35304', '46.970332', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:30:28.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (59, 'CLV', 'SPNSH1', 'El Lago del Bosque, Spanish Village', 'El Lago del Bosque, Spanish Village, Bemidji Site, March-August', '218-586-8930', 'clv@cord.edu', 'null', '-94.7269', '47.563084', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:31:07.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (60, 'CLV', 'PRTUGS', 'Mar e Floresta, Portugese Village', 'Mar e Floresta, Portugese Village, Callaway Site, July-August', '218-375-4021', 'clv@cord.edu', 'null', '-95.776663', '47.009523', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:31:16.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (63, 'CLV', 'SWEDEN', 'Sjolunden, Swedish Village', 'Sjolunden, Swedish Village, Bemidji Site, July-August', '218-586-8830', 'clv@cord.edu', 'null', '-94.725688', '47.555547', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:31:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (66, 'CLV', 'ITALY', 'Lago Del Bosco-Camp Holiday, Italian Village', 'Lago Del Bosco-Camp Holiday, Italian Village, Hackensack Site, July-August', '218-682-2570', 'clv@cord.edu', 'null', '-94.351954', '46.970513', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:32:26.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (69, 'CLV', 'CLVMHD', 'Concordia Language Villages, Moorhead Administration', 'Concordia Language Villages, Moorhead Administrative Offices at Riverside Center', '218-299-4544', 'clv@cord.edu', 'null', '-96.773959', '46.860870', 2, 'SUPERADMIN', to_timestamp('28-08-2013 10:32:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', '1', 'Y');
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (21, 'CCM', 'RIV', 'Riverside Center', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/bell_tower.jpg', '-96.774069', '46.860888', 2, 'superadmin', to_timestamp('04-10-2013 15:15:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '310 14th Avenue South, Moorhead, MN, US', '2,5', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('03-10-2013 03:19:14.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2,1', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', 'jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf jdfkdjsfkdsjf ', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('03-10-2013 03:20:48.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2,1', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (1, 'CCM', 'AAS', 'Aasgaard House', 'The Office of Communications and Marketing is charged with protecting and promoting the Concordia College brand.', '218-299-3147', 'aasgaard@cord.edu', 'https://m-test.cord.edu/images/building_images/aasgaard.jpg', '-96.768951', '46.866646', 2, 'superadmin', to_timestamp('03-10-2013 07:54:03.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '714 7th Street South, Moorhead, MN, US', '2', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('03-10-2013 03:18:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2,1', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('03-10-2013 03:19:50.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2,1', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890', 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('03-10-2013 03:21:01.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2,1', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (2, 'CCM', 'ACAD', 'Academy Hall', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/academy.jpg', '-96.769863', '46.865561', 2, 'superadmin', to_timestamp('22-10-2013 05:51:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '821 7th Street South, Moorhead, MN, US', '2,1', null);
insert into MAPS_AUDIT (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CATEGORY, SHOWBYDEFAULT)
values (6, 'CCM', 'BOGE', 'Bogstad East', null, 'null', 'null', 'https://m-test.cord.edu/images/building_images/bogstad.jpg', '-96.768415', '46.867541', 2, 'superadmin', to_timestamp('22-10-2013 07:29:11.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '617 8th Street South, Moorhead, MN, US', '8', null);
commit;
prompt 256 records loaded
prompt Loading ME_COLLEGE_DEAN...
prompt Table is empty
prompt Loading ME_DEPARTMENT_CHAIR...
prompt Table is empty
prompt Loading ME_DETAIL_CODE_DESC...
prompt Table is empty
prompt Loading MOBILE_INVOICE_LINKS...
prompt Table is empty
prompt Loading MODULE...
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 1, 'PROFILE', 'My Info', 'Y', 'Y', 'student-profile.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 2, 'EVENTS', 'Calendar', 'N', 'Y', 'events.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 3, 'NEWS', 'News', 'N', 'Y', 'campus-news.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 4, 'DINSERV', 'Dining Services', 'N', 'Y', 'food.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 5, 'IMPNUM', 'Contact Info', 'N', 'Y', 'dir.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 6, 'EMER', 'Emergency Info', 'N', 'Y', 'emergency.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 7, 'SMEDIA', 'Social Media', 'N', 'Y', 'socialmedia.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 8, 'MAPS', 'Maps', 'N', 'Y', 'campus-map.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 9, 'HELP', 'Help', 'N', 'Y', 'help.png', 'MAIN');
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('CCM', 10, 'FINAID', 'FINANCIAL AID', 'N', 'N', 'fin-aid.png', 'MAIN');
commit;
prompt 10 records loaded
prompt Loading MODULES...
prompt Table is empty
prompt Loading NOTICE...
prompt Table is empty
prompt Loading NOTICELOG...
insert into NOTICELOG (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (560, 4, '900019523', 'Test Message', to_date('22-10-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'superadmin', to_timestamp('18-10-2013 05:46:40.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 0, 'Sports', 'Meeting about Games', to_date('18-10-2013', 'dd-mm-yyyy'));
insert into NOTICELOG (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (580, 5, 'S02467717', 'Test message', to_date('20-10-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'superadmin', to_timestamp('20-10-2013 07:04:29.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 0, 'Academics', 'Test Message', to_date('20-10-2013', 'dd-mm-yyyy'));
insert into NOTICELOG (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (581, 6, 'S02467717', 'Test message', to_date('20-10-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'superadmin', to_timestamp('20-10-2013 07:05:03.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 0, 'Academics', 'Test message', to_date('20-10-2013', 'dd-mm-yyyy'));
insert into NOTICELOG (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (540, 3, '900019523', 'hello sample notification for test', to_date('29-06-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 1, 'n2njmork2', to_timestamp('28-06-2013 03:14:12.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 0, 'Academics', 'Note-1', to_date('27-06-2013', 'dd-mm-yyyy'));
insert into NOTICELOG (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (520, 2, '900019523', 'Notification-1', to_date('20-06-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'n2njmork2', to_timestamp('19-06-2013 03:04:23.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 0, 'Academics', 'Sample-1', to_date('19-06-2013', 'dd-mm-yyyy'));
insert into NOTICELOG (ID, NOTICEID, USERNAME, MESSAGE, EXPIRYDATE, DELIVERYMETHOD, DELIVERED, READFLAG, LASTMODIFIEDBY, LASTMODIFIEDON, DELETED, TYPE, TITLE, DUEDATE)
values (501, 1, '900019523', 'notification', to_date('30-06-2013 23:59:59', 'dd-mm-yyyy hh24:mi:ss'), 'PUSH', 0, 0, 'n2njmork2', to_timestamp('18-06-2013 08:44:38.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 0, 'Sports', 'Test', to_date('18-06-2013', 'dd-mm-yyyy'));
commit;
prompt 6 records loaded
prompt Loading POPULATIONTYPE...
prompt Table is empty
prompt Loading NOTICEPOPULATION...
prompt Table is empty
prompt Loading NOTICETYPE...
prompt Table is empty
prompt Loading PRIVILEGE...
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (2, 'HELP', 'Y', 'Y', null, 'SUPERADMIN', 16, '9');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (2, 'EVENTS', 'Y', 'Y', to_timestamp('11-10-2013 12:46:43.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 16, '2');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (2, 'MAPS', 'Y', 'Y', to_timestamp('11-10-2013 12:47:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 16, '8');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (2, 'EMER', 'Y', 'Y', to_timestamp('11-10-2013 12:48:19.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 16, '6');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (2, 'ATHLETICS', 'Y', 'Y', to_timestamp('11-10-2013 12:49:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 16, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'MOODLE', 'Y', 'Y', null, null, null, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'ENROUTE', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 15, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'ENTER', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 13, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'ENQ', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 14, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'ATHLETICS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 2, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'EVENTS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 3, '2');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'MAPS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 4, '8');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'COURSES', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 5, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'DIRECTORY', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 6, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'PROFILE', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 7, '1');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'ACCOUNTS', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 8, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'REG', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 9, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'JOBS', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 10, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'HELP', 'Y', 'Y', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 11, '9');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'FEEDBACK', 'N', 'N', to_timestamp('28-01-2013 20:29:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 11, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'EMER', 'Y', 'Y', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 16, '6');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'CUSTSERV', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 12, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (1, 'SCHOOLS', 'Y', 'N', to_timestamp('30-01-2013 20:56:45.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 1, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'ENROUTE', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 15, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'ENTER', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 13, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'ENQ', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 14, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'ATHLETICS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 2, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'EVENTS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 3, '2');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'MAPS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 4, '8');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'COURSES', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 5, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'DIRECTORY', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 6, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'PROFILE', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 7, '1');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'ACCOUNTS', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 8, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'REG', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 9, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'JOBS', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 10, null);
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'HELP', 'Y', 'Y', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 11, '9');
insert into PRIVILEGE (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO, MODULEID)
values (3, 'FEEDBACK', 'N', 'N', to_timestamp('28-01-2013 20:29:37.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 11, null);
commit;
prompt 37 records loaded
prompt Loading PRIVILEGE_AUDIT...
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'ATHLETICS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 2);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'ATHLETICS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 2);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'EVENTS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 3);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'EVENTS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 3);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'MAPS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 4);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'MAPS', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 4);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'COURSES', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 5);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'COURSES', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 5);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'DIRECTORY', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 6);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'DIRECTORY', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 6);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'PROFILE', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 7);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'PROFILE', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 7);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'ACCOUNTS', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 8);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'ACCOUNTS', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 8);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'REG', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', 1);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'REG', 'Y', 'Y', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'null', 1);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'JOBS', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 9);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'JOBS', 'N', 'N', to_timestamp('12-03-2013 04:01:17.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 9);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'HELP', 'Y', 'Y', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 10);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'HELP', 'Y', 'Y', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 10);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'CUSTSERV', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 11);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'ENTER', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 12);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'ENTER', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 12);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'ENQ', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 13);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'ENQ', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 13);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'ENROUTE', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 14);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (3, 'ENROUTE', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 14);
insert into PRIVILEGE_AUDIT (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED, LASTMODIFIEDON, LASTMODIFIEDBY, VERSION_NO)
values (1, 'EMER', 'N', 'N', to_timestamp('12-03-2013 04:01:18.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), 'SUPERADMIN', 15);
commit;
prompt 28 records loaded
prompt Loading REG_TERMS_CONDITIONS...
prompt Table is empty
prompt Loading ROLE...
insert into ROLE (CAMPUSCODE, ROLEID, ROLE, DESCRIPTION)
values ('1', '3', 'FACULTY', 'FACULTY');
insert into ROLE (CAMPUSCODE, ROLEID, ROLE, DESCRIPTION)
values ('1', '1', 'STUDENT', 'STUDENT');
insert into ROLE (CAMPUSCODE, ROLEID, ROLE, DESCRIPTION)
values ('1', '2', 'DEFAULT', 'DEFAULT');
commit;
prompt 3 records loaded
set feedback on
set define on
prompt Done.
