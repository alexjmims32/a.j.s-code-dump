prompt Importing table admin_priv...
set feedback off
set define off

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'FEEDS', 'Y', 'FEEDS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'MAPS', 'Y', 'MAPS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'ROLES', 'Y', 'ROLES Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mmbreth', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'FEEDS', 'Y', 'FEEDS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'MAPS', 'Y', 'MAPS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'ROLES', 'Y', 'ROLES Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('eramstad', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', null);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'FEEDS', 'Y', 'FEEDS Module Access', 2);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'MAPS', 'Y', 'MAPS Module Access', 3);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', 4);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', 6);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', 8);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'ROLES', 'Y', 'ROLES Module Access', 5);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', 9);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('superadmin', 'USERS', 'Y', 'USERS Module Access', 10);

commit;
prompt Done.
