prompt Importing table emergency_contacts...
set feedback off
set define off
insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (1, 'CCM', 'Office of the President, Lorentzsen', '218-299-3000', '', '', '', 'IMPNUM', 2, 'eramstad', '18-JUN-13 03.28.44.000000 PM', '', 1);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (2, 'CCM', 'Academic Affairs -Office of the Provost — Lorentzsen', '218-299-3606', '', '', '', 'IMPNUM', null, '', '', '', 0);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (3, 'CCM', 'Academic Affairs -Arts and Sciences', '218-299-4541', '', '', '', 'IMPNUM', null, '', '', '', 2);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (4, 'CCM', 'Academic Affairs -Academic Division Chairs', '218-299-3606', '', '', '', 'IMPNUM', null, '', '', '', 3);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (5, 'CCM', 'Academic Affairs -Core and Advising', '218-299-4541', '', '', '', 'IMPNUM', null, '', '', '', 4);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (6, 'CCM', 'Academic Affairs -Foundation Relations and Research Grants – Advancement Center', '218-299-4981', '', '', '', 'IMPNUM', null, '', '', '', 5);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (7, 'CCM', 'Academic Affairs -Sustainability', '218-299-3071', '', '', '', 'IMPNUM', null, '', '', '', 6);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (8, 'CCM', 'Academic Affairs -Undergraduate Research, Scholarship and National Fellowships', '218-299-3252', '', '', '', 'IMPNUM', null, '', '', '', 7);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (9, 'CCM', 'Academic Assessment (see Institutional Research and Assessment)', '218-299-4723', '', '', '', 'IMPNUM', null, '', '', '', 8);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (10, 'CCM', 'Academic Enhancement and Writing Center — Fjelstad Hall B10 (lower level)', '218-299-4551', '', '', '', 'IMPNUM', null, '', '', '', 9);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (11, 'CCM', 'Admissions (see Enrollment)', '218-299-3004', '', '', '', 'IMPNUM', null, '', '', '', 10);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (13, 'CCM', 'Advancement -Office of Advancement ', '218-299-3733', '', '', '', 'IMPNUM', null, '', '', '', 11);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (14, 'CCM', 'Advancement -Office of Alumni Relations ', '218-299-3734', '', '', '', 'IMPNUM', null, '', '', '', 12);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (15, 'CCM', 'Advancement -Office of Annual Fund ', '218-299-3454', '', '', '', 'IMPNUM', null, '', '', '', 13);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (16, 'CCM', 'Advancement -Toll free (Advancement/Annual Fund) ', '800-699-9896', '', '', '', 'IMPNUM', null, '', '', '', 14);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (17, 'CCM', 'Advancement -Toll free (Alumni Relations) ', '800-699-9020', '', '', '', 'IMPNUM', null, '', '', '', 15);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (18, 'CCM', 'Anderson Office of Career Success - Grant Center', '218-299-3486', '', '', '', 'IMPNUM', null, '', '', '', 16);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (19, 'CCM', 'Archives — Library', '218-299-3241', '', '', '', 'IMPNUM', null, '', '', '', 17);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (20, 'CCM', 'Art — Olin (second floor)', '218-299-4623', '', '', '', 'IMPNUM', null, '', '', '', 18);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (22, 'CCM', 'Athletics -Memorial Auditorium Office ', '218-299-4434', '', '', '', 'IMPNUM', null, '', '', '', 19);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (23, 'CCM', 'Athletics -Equipment Room, Memorial Auditorium ', '218-299-3209', '', '', '', 'IMPNUM', null, '', '', '', 20);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (24, 'CCM', 'Athletics -Intramurals ', '218-299-4921', '', '', '', 'IMPNUM', null, '', '', '', 21);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (25, 'CCM', 'Athletics -Locker Room, Jake Christiansen Stadium ', '218-359-0283', '', '', '', 'IMPNUM', null, '', '', '', 22);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (26, 'CCM', 'Athletics -Olson Forum Desk ', '218-299-4310', '', '', '', 'IMPNUM', null, '', '', '', 23);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (27, 'CCM', 'Athletics -Press Box, Stadium ', '218-359-0285', '', '', '', 'IMPNUM', null, '', '', '', 24);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (28, 'CCM', 'Athletics -Sports Information ', '218-299-3194', '', '', '', 'IMPNUM', null, '', '', '', 25);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (29, 'CCM', 'Athletics -Swimming Pool ', '218-299-3591', '', '', '', 'IMPNUM', null, '', '', '', 26);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (30, 'CCM', 'Biology — Jones Science Center (second floor)', '218-299-3085', '', '', '', 'IMPNUM', null, '', '', '', 27);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (31, 'CCM', 'Bookstore (see Cobber Bookstore)', '218-299-3017', '', '', '', 'IMPNUM', null, '', '', '', 28);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (32, 'CCM', 'Business Office — Lorentzsen', '218-299-3150', '', '', '', 'IMPNUM', null, '', '', '', 29);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (33, 'CCM', 'Business (see Offutt School of Business)', '218-299-4411', '', '', '', 'IMPNUM', null, '', '', '', 30);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (34, 'CCM', 'Campus Information — Knutson Campus Center', '218-299-4000', '', '', '', 'IMPNUM', null, '', '', '', 31);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (35, 'CCM', 'Campus Lights (see Communication Studies and Theatre Art)', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 32);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (36, 'CCM', 'Campus Ministry Office (see Ministry)', '218-299-4161', '', '', '', 'IMPNUM', null, '', '', '', 33);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (37, 'CCM', 'Career Center — Academy', '218-299-3020', '', '', '', 'IMPNUM', null, '', '', '', 34);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (38, 'CCM', 'Cooperative Education', '218-299-3492', '', '', '', 'IMPNUM', null, '', '', '', 35);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (39, 'CCM', 'Job Shop ', '218-299-4509', '', '', '', 'IMPNUM', null, '', '', '', 36);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (40, 'CCM', 'Chemistry — Ivers (third floor)', '218-299-3101', '', '', '', 'IMPNUM', null, '', '', '', 37);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (41, 'CCM', 'Chinese — Bishop Whipple (third floor)', '218-299-3106', '', '', '', 'IMPNUM', null, '', '', '', 38);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (42, 'CCM', 'Church Relations (see Vocation and Church Leadership)', '218-299-3146', '', '', '', 'IMPNUM', null, '', '', '', 39);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (43, 'CCM', 'Classical Studies — Bishop Whipple (first floor)', '218-299-3946', '', '', '', 'IMPNUM', null, '', '', '', 40);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (44, 'CCM', 'Clinical Laboratory Science — Jones Science Center', '218-299-3797', '', '', '', 'IMPNUM', null, '', '', '', 41);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (45, 'CCM', 'Cobber Bookstore', '218-299-3017', '', '', '', 'IMPNUM', null, '', '', '', 42);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (46, 'CCM', 'Cobber Bookstore - Toll Free', '800-828-6409', '', '', '', 'IMPNUM', null, '', '', '', 43);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (47, 'CCM', 'Cobber Kids — Riverside Center', '218-299-4204', '', '', '', 'IMPNUM', null, '', '', '', 44);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (48, 'CCM', 'Cobber Kids -Preschool Room', '218-299-4198', '', '', '', 'IMPNUM', null, '', '', '', 45);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (49, 'CCM', 'Cobber Kids -Toddlers Room', '218-299-4951', '', '', '', 'IMPNUM', null, '', '', '', 46);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (50, 'CCM', 'Communication Studies and Theatre Art (CSTA) — Olin (third floor)', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 47);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (51, 'CCM', 'Communications and Marketing, Office of — Aasgaard', '218-299-3147', '', '', '', 'IMPNUM', null, '', '', '', 48);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (52, 'CCM', 'Comm -Media Relations', '218-299-3642', '', '', '', 'IMPNUM', null, '', '', '', 49);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (53, 'CCM', 'Comm -Online Communications — Riverside Center', '218-299-3939', '', '', '', 'IMPNUM', null, '', '', '', 50);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (54, 'CCM', 'Comm -Photo Studio – Mugaas  ', '218-299-3938', '', '', '', 'IMPNUM', null, '', '', '', 51);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (55, 'CCM', 'Comm -Print Shop — Mugaas', '218-299-3429', '', '', '', 'IMPNUM', null, '', '', '', 52);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (56, 'CCM', 'Comm -Publications', '218-299-4957', '', '', '', 'IMPNUM', null, '', '', '', 53);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (57, 'CCM', 'Comm -Design', '218-299-4948', '', '', '', 'IMPNUM', 2, 'n2njmork2', '16-JUN-13 09.01.19.000000 AM', '', 54);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (58, 'CCM', 'Comm -Sports Information - Memorial Auditorium', '218-299-3194', '', '', '', 'IMPNUM', 2, 'SUPERADMIN', '05-JUN-13 10.23.01.000000 AM', '', 55);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (59, 'CCM', 'Communiversity (see F/M Communiversity)', '218-299-3438', '', '', '', 'IMPNUM', null, '', '', '', 56);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (60, 'CCM', 'Computer Science (see Offutt School of Business)', '218-299-4411', '', '', '', 'IMPNUM', null, '', '', '', 57);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (61, 'CCM', 'Concordia Language Villages Bemidji', '218-586-8600', '', '', '', 'IMPNUM', 2, 'SUPERADMIN', '05-JUN-13 10.22.43.000000 AM', '', 58);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (62, 'CCM', 'Concordia Language Villages — Bemidji -Toll Free', '800-450-2214', '', '', '', 'IMPNUM', null, '', '', '', 59);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (63, 'CCM', 'Concordia Language Villages — The Forest', '651-433-4375', '', '', '', 'IMPNUM', null, '', '', '', 60);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (64, 'CCM', 'Concordia Language Villages — Moorhead', '218-299-4544', '', '', '', 'IMPNUM', null, '', '', '', 61);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (65, 'CCM', 'Concordia Language Villages — Moorhead -Toll Free', '800-222-4750', '', '', '', 'IMPNUM', null, '', '', '', 62);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (66, 'CCM', 'Concordia Language Villages — St. Paul', '651-647-4357', '', '', '', 'IMPNUM', null, '', '', '', 63);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (67, 'CCM', 'Concordia On-Air (student television) — Olin', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 64);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (68, 'CCM', 'Concordian, The (student newspaper) — Fjelstad Hall (lower level)', '218-299-3826', '', '', '', 'IMPNUM', null, '', '', '', 65);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (69, 'CCM', 'Counseling Center — Academy', '218-299-3514', '', '', '', 'IMPNUM', null, '', '', '', 66);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (70, 'CCM', 'Credo – Lorentzsen ', '218-299-4959', '', '', '', 'IMPNUM', null, '', '', '', 67);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (71, 'CCM', 'CSTA (see Communication Studies and Theatre Art)', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 68);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (72, 'CCM', 'Cultural Events and Music Organizations — Riverside Center', '218-299-4366', '', '', '', 'IMPNUM', null, '', '', '', 69);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (73, 'CCM', 'Music Events and Recordings Information Line ', '218-299-4515', '', '', '', 'IMPNUM', null, '', '', '', 70);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (74, 'CCM', 'Dining Services — Knutson Campus Center', '218-299-3706', '', '', '', 'IMPNUM', null, '', '', '', 71);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (75, 'CCM', 'Dining Services -Office ', '218-299-3706', '', '', '', 'IMPNUM', null, '', '', '', 72);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (76, 'CCM', 'Dining Services -After 5:30 p.m./weekends ', '218-299-3463', '', '', '', 'IMPNUM', null, '', '', '', 73);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (77, 'CCM', 'Dining Services -Anderson Commons (checker) ', '218-299-3451', '', '', '', 'IMPNUM', null, '', '', '', 74);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (78, 'CCM', 'Dining Services -Anderson Commons (kitchen) ', '218-299-3463', '', '', '', 'IMPNUM', null, '', '', '', 75);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (79, 'CCM', 'Dining Services -Catering ', '218-299-4271', '', '', '', 'IMPNUM', null, '', '', '', 76);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (80, 'CCM', 'Dining Services -Coffee Stop ', '218-299-4356', '', '', '', 'IMPNUM', null, '', '', '', 77);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (81, 'CCM', 'Dining Services -Knutson Kitchen/Bakery ', '218-299-3593', '', '', '', 'IMPNUM', null, '', '', '', 78);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (82, 'CCM', 'Dining Services -Knutson Production ', '218-299-4205', '', '', '', 'IMPNUM', null, '', '', '', 79);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (83, 'CCM', 'Dining Services -Korn Krib ', '218-299-3005', '', '', '', 'IMPNUM', null, '', '', '', 80);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (84, 'CCM', 'Dining Services -The Maize ', '218-299-3563', '', '', '', 'IMPNUM', null, '', '', '', 81);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (85, 'CCM', 'Dining Services -Vending ', '218-299-3915', '', '', '', 'IMPNUM', null, '', '', '', 82);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (86, 'CCM', 'Disability Services (see Counseling Center)', '218-299-3514', '', '', '', 'IMPNUM', null, '', '', '', 83);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (87, 'CCM', 'Dovre Center for Faith and Learning', '218-299-3430', '', '', '', 'IMPNUM', null, '', '', '', 84);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (88, 'CCM', 'Education Old Main (first floor)', '218-299-3910', 'Old Main (first floor)', '', '', 'IMPNUM', 2, 'SUPERADMIN', '28-MAY-13 11.20.22.000000 AM', 'Old Main (2nd floor)', 85);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (176, 'CCM', 'Physics — Ivers (first floor)', '218-299-4413', '', '', '', 'IMPNUM', null, '', '', '', 86);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (177, 'CCM', 'Physics - Observatory ', '218-299-3896', '', '', '', 'IMPNUM', null, '', '', '', 87);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (178, 'CCM', 'Political Science — Old Main (third floor)', '218-299-3501', '', '', '', 'IMPNUM', null, '', '', '', 88);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (179, 'CCM', 'Post Office — Knutson Campus Center (mezzanine)', '218-299-3770', '', '', '', 'IMPNUM', null, '', '', '', 89);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (180, 'CCM', 'Print Shop (see Communications and Marketing)', '218-299-3429', '', '', '', 'IMPNUM', null, '', '', '', 90);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (181, 'CCM', 'Psychology — Ivers (second floor)', '218-299-4030', '', '', '', 'IMPNUM', null, '', '', '', 91);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (182, 'CCM', 'Public Safety — Knutson Campus Center', '218-299-3123', '', '', '', 'IMPNUM', null, '', '', '', 92);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (183, 'CCM', 'Public Safety SAFE Walk ', '218-299-3123', '', '', '', 'IMPNUM', null, '', '', '', 93);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (184, 'CCM', 'Registrar’s Office — Lorentzsen', '218-299-3250', '', '', '', 'IMPNUM', null, '', '', '', 94);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (185, 'CCM', 'Religion — Academy (second floor)', '218-299-3334', '', '', '', 'IMPNUM', null, '', '', '', 95);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (186, 'CCM', 'Residence Life — Knutson Campus Center', '218-299-3872', '', '', '', 'IMPNUM', null, '', '', '', 96);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (187, 'CCM', 'Residence Halls', '', '', '', '', 'IMPNUM', null, '', '', '', 97);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (188, 'CCM', 'Boe-Olsen Apartments -Director', '218-299-3899', '', '', '', 'IMPNUM', null, '', '', '', 98);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (189, 'CCM', 'Bogstad East Apartment -Director', '218-299-3899', '', '', '', 'IMPNUM', null, '', '', '', 99);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (190, 'CCM', 'Bogstad Manor Apartment -Director', '218-299-3899', '', '', '', 'IMPNUM', null, '', '', '', 100);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (191, 'CCM', 'Brown -Director', '218-299-4485', '', '', '', 'IMPNUM', null, '', '', '', 101);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (192, 'CCM', 'Erickson -Director', '218-299-4697', '', '', '', 'IMPNUM', null, '', '', '', 102);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (193, 'CCM', 'Erickson -Desk ', '218-299-4703', '', '', '', 'IMPNUM', null, '', '', '', 103);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (194, 'CCM', 'Fjelstad -Director', '218-299-4359', '', '', '', 'IMPNUM', null, '', '', '', 104);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (195, 'CCM', 'Fjelstad -Desk', '218-299-3564', '', '', '', 'IMPNUM', null, '', '', '', 105);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (196, 'CCM', 'Hallett -Director', '218-299-3940', '', '', '', 'IMPNUM', null, '', '', '', 106);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (197, 'CCM', 'Hallett -Desk', '218-299-3535', '', '', '', 'IMPNUM', null, '', '', '', 107);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (198, 'CCM', 'Hoyum -Director', '218-299-3319', '', '', '', 'IMPNUM', null, '', '', '', 108);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (199, 'CCM', 'Hoyum -Desk', '218-299-4547', '', '', '', 'IMPNUM', null, '', '', '', 109);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (200, 'CCM', 'International Center -Director', '218-299-3899', '', '', '', 'IMPNUM', null, '', '', '', 110);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (201, 'CCM', 'Livedalen -Director', '218-299-3722', '', '', '', 'IMPNUM', null, '', '', '', 111);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (202, 'CCM', 'Livedalen -Desk', '218-299-3336', '', '', '', 'IMPNUM', null, '', '', '', 112);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (203, 'CCM', 'Park Region -Director', '218-299-4359', '', '', '', 'IMPNUM', null, '', '', '', 113);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (204, 'CCM', 'Park Region -Desk', '218-299-4219', '', '', '', 'IMPNUM', null, '', '', '', 114);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (205, 'CCM', 'Townhouse East/Townhouse West -Director', '218-299-4697', '', '', '', 'IMPNUM', null, '', '', '', 115);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (206, 'CCM', 'Risk Management — Lorentzsen', '218-299-3682', '', '', '', 'IMPNUM', null, '', '', '', 116);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (207, 'CCM', 'Scandinavian Studies — Bishop Whipple (third floor)', '218-299-4462', '', '', '', 'IMPNUM', null, '', '', '', 117);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (208, 'CCM', 'Security (see Public Safety)', '218-299-3123', '', '', '', 'IMPNUM', null, '', '', '', 118);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (209, 'CCM', 'Sociology and Social Work — Old Main (third floor)', '218-299-3501', '', '', '', 'IMPNUM', null, '', '', '', 119);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (210, 'CCM', 'Solution Center (see Information Technology Services)', '218-299-3375', '', '', '', 'IMPNUM', null, '', '', '', 120);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (211, 'CCM', 'Spanish and Hispanic Studies — Academy (first floor)', '218-299-3754', '', '', '', 'IMPNUM', null, '', '', '', 121);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (212, 'CCM', 'Sports Information (see Athletics)', '218-299-3194', '', '', '', 'IMPNUM', null, '', '', '', 122);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (213, 'CCM', 'Student Affairs, Office of — Knutson Campus Center', '218-299-3455', '', '', '', 'IMPNUM', null, '', '', '', 123);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (214, 'CCM', 'Student Government Association — Knutson Campus Center', '218-299-4518', '', '', '', 'IMPNUM', null, '', '', '', 124);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (215, 'CCM', 'SGA -Student Government Association ', '218-299-4507', '', '', '', 'IMPNUM', null, '', '', '', 125);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (216, 'CCM', 'SGA -Campus Entertainment Commission ', '218-299-4246', '', '', '', 'IMPNUM', null, '', '', '', 126);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (217, 'CCM', 'SGA -Campus Ministry Commission ', '218-299-4245', '', '', '', 'IMPNUM', null, '', '', '', 127);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (218, 'CCM', 'SGA -Campus Service Commission ', '218-299-4167', '', '', '', 'IMPNUM', null, '', '', '', 128);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (219, 'CCM', 'Student Leadership and Service — Knutson Campus Center', '218-299-3640', '', '', '', 'IMPNUM', null, '', '', '', 129);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (220, 'CCM', 'Student Success and Retention, Office of - Fjelstad Hall B02 (lower level)', '218-299-3045', '', '', '', 'IMPNUM', null, '', '', '', 130);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (221, 'CCM', 'Peer Mentoring Office – Fjelstad Hall B03 (lower level)', '218-299-3040', '', '', '', 'IMPNUM', null, '', '', '', 131);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (222, 'CCM', 'Summer Programs, Office of — Riverside Center', '218-299-3566', '', '', '', 'IMPNUM', null, '', '', '', 132);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (223, 'CCM', 'Sustainability (see Office of the Provost)', '218-299-3071', '', '', '', 'IMPNUM', null, '', '', '', 133);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (224, 'CCM', 'Theatre, Frances Frazier Comstock', '218-299-3314', '', '', '', 'IMPNUM', null, '', '', '', 134);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (225, 'CCM', 'Theatre Box Office ', '218-299-3314', '', '', '', 'IMPNUM', null, '', '', '', 135);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (226, 'CCM', 'Theatre Workshop ', '218-299-3821', '', '', '', 'IMPNUM', null, '', '', '', 136);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (227, 'CCM', 'Transportation Services — Jake Christiansen Stadium', '218-299-3259', '', '', '', 'IMPNUM', null, '', '', '', 137);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (228, 'CCM', 'Tri-College University', '701-231-8170', '', '', '', 'IMPNUM', null, '', '', '', 138);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (229, 'CCM', 'Undergraduate Research, Scholarship and National Fellowships', '218-299-3252', '', '', '', 'IMPNUM', null, '', '', '', 139);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (230, 'CCM', 'Vocation and Church Leadership — Knutson Campus Center', '218-299-3146', '', '', '', 'IMPNUM', null, '', '', '', 140);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (231, 'CCM', 'Women’s Studies', '218-299-4236', '', '', '', 'IMPNUM', null, '', '', '', 141);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (232, 'CCM', 'Campus Public Safety', '218-299-3123', 'Knutson Campus Center', 'security@cord.edu', '', 'EMR', 2, 'SUPERADMIN', '28-MAY-13 11.25.32.000000 AM', 'Call 911 for emergency response to Fire/Police/Ambulance', 142);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (89, 'CCM', 'ELCA, Northwestern MN Synod — Riverside Center', '218-299-3019', '', '', '', 'IMPNUM', null, '', '', '', 143);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (90, 'CCM', 'English — Academy (third floor)', '218-299-3812', '', '', '', 'IMPNUM', null, '', '', '', 144);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (91, 'CCM', 'Enrollment -Office of Admissions - Welcome Center', '218-299-3004', '', '', '', 'IMPNUM', null, '', '', '', 145);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (92, 'CCM', 'Enrollment -Office of Financial Aid - Welcome Center', '218-299-3010', '', '', '', 'IMPNUM', null, '', '', '', 146);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (93, 'CCM', 'Enrollment Office - Toll free ', '800-699-9897', '', '', '', 'IMPNUM', null, '', '', '', 147);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (94, 'CCM', 'Environmental Studies', '218-299-3085', '', '', '', 'IMPNUM', null, '', '', '', 148);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (95, 'CCM', 'Exercise Science (see Physical Education and Health)', '218-299-4434', '', '', '', 'IMPNUM', null, '', '', '', 149);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (96, 'CCM', 'Facilities Management — Mugaas', '', '', '', '', 'IMPNUM', null, '', '', '', 150);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (97, 'CCM', 'Facilities Management -Office ', '218-299-3362', '', '', '', 'IMPNUM', null, '', '', '', 151);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (98, 'CCM', 'Facilities Management -After-Hour Emergency Maintenance (24-hour) ', '218-299-3123', '', '', '', 'IMPNUM', null, '', '', '', 152);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (99, 'CCM', 'Facilities Management -Electrical Services ', '218-299-4626', '', '', '', 'IMPNUM', null, '', '', '', 153);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (100, 'CCM', 'Facilities Management -General Construction and Finishes — Carpentry ', '218-299-3679', '', '', '', 'IMPNUM', null, '', '', '', 154);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (101, 'CCM', 'Facilities Management -General Construction and Finishes — Locks and Hardware ', '218-299-3679', '', '', '', 'IMPNUM', null, '', '', '', 155);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (102, 'CCM', 'Facilities Management -General Construction and Finishes — Painting ', '218-299-3679', '', '', '', 'IMPNUM', null, '', '', '', 156);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (103, 'CCM', 'Facilities Management -Grounds Services ', '218-299-3066', '', '', '', 'IMPNUM', null, '', '', '', 157);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (104, 'CCM', 'Facilities Management -Heating and Cooling Plant ', '218-299-3688', '', '', '', 'IMPNUM', null, '', '', '', 158);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (105, 'CCM', 'Facilities Management -Parking Services ', '218-299-3267', '', '', '', 'IMPNUM', null, '', '', '', 159);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (106, 'CCM', 'Facilities Management -Plumbing Services ', '218-299-4686', '', '', '', 'IMPNUM', null, '', '', '', 160);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (107, 'CCM', 'Facilities Management -Telecom Services', '218-299-4629', '', '', '', 'IMPNUM', null, '', '', '', 161);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (108, 'CCM', 'Facilities Management -Transportation Services ', '218-299-3259', '', '', '', 'IMPNUM', null, '', '', '', 162);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (109, 'CCM', 'Finance/Treasurer’s Office — Lorentzsen', '218-299-3150', '', '', '', 'IMPNUM', null, '', '', '', 163);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (110, 'CCM', 'Financial Aid (see Enrollment)', '218-299-3010', '', '', '', 'IMPNUM', null, '', '', '', 164);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (111, 'CCM', 'F/M Communiversity — Riverside Center', '218-299-3488', '', '', '', 'IMPNUM', null, '', '', '', 165);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (112, 'CCM', 'Food/Nutrition/Dietetics (see Nutrition and Dietetics)', '218-299-4442', '', '', '', 'IMPNUM', null, '', '', '', 166);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (113, 'CCM', 'Forum on Faith and Life — Academy', '218-299-3146', '', '', '', 'IMPNUM', null, '', '', '', 167);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (114, 'CCM', 'French — Academy (second floor)', '218-299-3695', '', '', '', 'IMPNUM', null, '', '', '', 168);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (115, 'CCM', 'German  — Bishop Whipple (third floor)', '218-299-3106', '', '', '', 'IMPNUM', null, '', '', '', 169);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (116, 'CCM', 'Global Learning — Lorentzsen', '218-299-3927', '', '', '', 'IMPNUM', null, '', '', '', 170);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (117, 'CCM', 'Global Education', '218-299-3927', '', '', '', 'IMPNUM', null, '', '', '', 171);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (118, 'CCM', 'Global Studies', '218-299-3528', '', '', '', 'IMPNUM', null, '', '', '', 172);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (119, 'CCM', 'Intercultural Affairs', '218-299-4517', '', '', '', 'IMPNUM', null, '', '', '', 173);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (120, 'CCM', 'Graduate and Continuing Studies, Office of — Lorentzsen', '218-299-3257', '', '', '', 'IMPNUM', null, '', '', '', 174);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (121, 'CCM', 'Health Center — Kjos  (also see Emergency Contacts) ', '218-299-3662', '', '', '', 'IMPNUM', null, '', '', '', 175);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (122, 'CCM', 'Health Professions', '218-299-4302', '', '', '', 'IMPNUM', null, '', '', '', 176);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (123, 'CCM', 'History — Old Main (third floor)', '218-299-3501', '', '', '', 'IMPNUM', null, '', '', '', 177);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (124, 'CCM', 'Human Resources and Payroll — Lorentzsen', '218-299-3339', '', '', '', 'IMPNUM', null, '', '', '', 178);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (126, 'CCM', 'Humanities', '218-299-3422', '', '', '', 'IMPNUM', null, '', '', '', 179);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (127, 'CCM', 'Information Technology Services', '', '', '', '', 'IMPNUM', null, '', '', '', 180);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (128, 'CCM', 'ITS -Chief Information Officer', '218-299-4737', '', '', '', 'IMPNUM', null, '', '', '', 181);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (129, 'CCM', 'ITS -Digital Media Services — Olin', '218-299-4202', '', '', '', 'IMPNUM', null, '', '', '', 182);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (130, 'CCM', 'ITS -Enterprise Systems and Services — Lorentzsen', '218-299-4923', '', '', '', 'IMPNUM', null, '', '', '', 183);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (131, 'CCM', 'ITS -Infrastructure Services — Lorentzsen', '218-299-4192', '', '', '', 'IMPNUM', null, '', '', '', 184);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (132, 'CCM', 'ITS -Solution Center / Help Desk — Frances Frazier Comstock Theatre', '218-299-3375', '', '', '', 'IMPNUM', null, '', '', '', 185);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (133, 'CCM', 'Institutional Research and Assessment, Office of — Lorentzsen', '', '', '', '', 'IMPNUM', null, '', '', '', 186);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (134, 'CCM', 'IR -Institutional Assessment ', '218-299-4723', '', '', '', 'IMPNUM', null, '', '', '', 187);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (135, 'CCM', 'IR -Institutional Research ', '218-299-3549', '', '', '', 'IMPNUM', null, '', '', '', 188);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (136, 'CCM', 'Intercultural Affairs, Office of (see Global Learning)', '218-299-3927', '', '', '', 'IMPNUM', null, '', '', '', 189);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (137, 'CCM', 'International Business ', '218-299-3478', '', '', '', 'IMPNUM', null, '', '', '', 190);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (138, 'CCM', 'Journalism (see Multimedia Journalism)', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 191);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (139, 'CCM', 'KORD (student radio) — Olin', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 192);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (140, 'CCM', 'Library, Carl B. Ylvisaker', '218-299-4640', '', '', '', 'IMPNUM', null, '', '', '', 193);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (141, 'CCM', 'Library -Archives ', '218-299-3241', '', '', '', 'IMPNUM', null, '', '', '', 194);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (142, 'CCM', 'Library -Circulation Desk ', '218-299-4641', '', '', '', 'IMPNUM', null, '', '', '', 195);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (143, 'CCM', 'Library -Curriculum Center ', '218-299-4499', '', '', '', 'IMPNUM', null, '', '', '', 196);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (144, 'CCM', 'Library -Interlibrary Loan ', '218-299-4641', '', '', '', 'IMPNUM', null, '', '', '', 197);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (145, 'CCM', 'Library -Reference Desk ', '218-299-4656', '', '', '', 'IMPNUM', null, '', '', '', 198);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (146, 'CCM', 'Lorentzsen Center for Faith and Work - Grant Center', '218-299-3305', '', '', '', 'IMPNUM', null, '', '', '', 199);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (147, 'CCM', 'Management Information Systems (see Offutt School of Business)', '218-299-4411', '', '', '', 'IMPNUM', null, '', '', '', 200);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (148, 'CCM', 'Mathematics — Ivers (second floor)', '218-299-4151', '', '', '', 'IMPNUM', null, '', '', '', 201);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (149, 'CCM', 'May Seminars Abroad (see Global Learning) - Lorentzsen Hall', '218-299-3927', '', '', '', 'IMPNUM', null, '', '', '', 202);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (150, 'CCM', 'Media Relations (see Communications and Marketing)', '218-299-3147', '', '', '', 'IMPNUM', null, '', '', '', 203);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (151, 'CCM', 'Ministry, Office of — Knutson Campus Center', '218-299-4161', '', '', '', 'IMPNUM', null, '', '', '', 204);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (152, 'CCM', 'Minnesota Public Radio KCCD 90.3 FM/KCCM 91.1 FM – Welcome Center ', '218-287-0666', '', '', '', 'IMPNUM', null, '', '', '', 205);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (153, 'CCM', 'Multimedia Journalism', '218-299-3143', '', '', '', 'IMPNUM', null, '', '', '', 206);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (154, 'CCM', 'Music — Hvidsten', '218-299-4414', '', '', '', 'IMPNUM', null, '', '', '', 207);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (155, 'CCM', 'Music Events and Recordings Information Line ', '218-299-4515', '', '', '', 'IMPNUM', null, '', '', '', 208);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (156, 'CCM', 'Music Organizations (see Cultural Events)', '218-299-4366', '', '', '', 'IMPNUM', null, '', '', '', 209);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (157, 'CCM', 'Neuroscience - Ivers Science', '218-299-3112', '', '', '', 'IMPNUM', null, '', '', '', 210);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (158, 'CCM', 'Norwegian — Bishop Whipple (third floor)', '218-299-3106', '', '', '', 'IMPNUM', null, '', '', '', 211);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (159, 'CCM', 'Nursing — Jones Science Center (first floor)', '218-299-3879', '', '', '', 'IMPNUM', null, '', '', '', 212);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (160, 'CCM', 'Nutrition and Dietetics — Jones Science Center (third floor)', '218-299-4442', '', '', '', 'IMPNUM', null, '', '', '', 213);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (161, 'CCM', 'Offutt School of Business — Grant Center', '218-299-4411', '', '', '', 'IMPNUM', null, '', '', '', 214);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (162, 'CCM', 'Small Business Development Center – Grant Center', '218-299-3035', '', '', '', 'IMPNUM', null, '', '', '', 215);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (163, 'CCM', 'Olson Forum Desk (lower level, Staffed Academic Year Only)', '218-299-4310', '', '', '', 'IMPNUM', null, '', '', '', 216);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (164, 'CCM', 'Oral Communication Center — Olin 122', '218-299-3740', '', '', '', 'IMPNUM', null, '', '', '', 217);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (165, 'CCM', 'Orientation — Knutson Campus Center', '218-299-3455', '', '', '', 'IMPNUM', null, '', '', '', 218);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (166, 'CCM', 'Parke Student Leadership Center  — Knutson Campus Center (second floor)', '218-299-4518', '', '', '', 'IMPNUM', null, '', '', '', 219);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (167, 'CCM', 'Student Business Office ', '218-299-3084', '', '', '', 'IMPNUM', null, '', '', '', 221);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (168, 'CCM', 'Parking Services — Mugaas', '218-299-3267', '', '', '', 'IMPNUM', null, '', '', '', 220);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (169, 'CCM', 'Philosophy — Bishop Whipple (first floor)', '218-299-3946', '', '', '', 'IMPNUM', null, '', '', '', 222);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (170, 'CCM', 'Photo Studio (see Communications and Marketing)', '218-299-3938', '', '', '', 'IMPNUM', null, '', '', '', 223);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (171, 'CCM', 'Physical Education and Health — Memorial Auditorium (first floor)', '218-299-4434', '', '', '', 'IMPNUM', null, '', '', '', 224);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (172, 'CCM', 'PE&H -Equipment Room, Memorial Auditorium ', '218-299-3209', '', '', '', 'IMPNUM', null, '', '', '', 225);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (173, 'CCM', 'PE&H -Olson Forum Desk ', '218-299-4310', '', '', '', 'IMPNUM', null, '', '', '', 226);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (174, 'CCM', 'PE&H -Sports Information ', '218-299-3194', '', '', '', 'IMPNUM', null, '', '', '', 227);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (175, 'CCM', 'PE&H -Swimming Pool ', '218-299-3591', '', '', '', 'IMPNUM', null, '', '', '', 228);

commit;
prompt Done.
