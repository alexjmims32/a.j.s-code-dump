prompt Importing table feeds...
set feedback off
set define off
insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (11, 10, 'LINK', 'http://www.concordiacollege.edu/student-life/dining-services/dining-on-campus/', 'xml', 4, 'Menu', 'food.png', 'CCM', 2, 'superadmin', '23-AUG-13 05.58.51.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (14, 0, 'LINK', 'https://secure.touchnet.com/C20618test_tsa/web/login.jsp', 'jsp', 0, 'Make Payment', 'student-account.png', 'CCM', null, '', '');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (10, 0, 'MENU', '', '', 4, 'Dining Services', 'food.png', 'CCM', 1, 'SUPERADMIN', '17-MAY-13 07.58.57.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (13, 10, 'LINK', 'http://www.concordiacollege.edu/student-life/dining-services/dining-on-campus/the-maize/', 'xml', 4, 'The Maize', 'food.png', 'CCM', 2, 'superadmin', '23-AUG-13 06.00.22.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (12, 10, 'LINK', 'http://www.concordiacollege.edu/student-life/dining-services/hours-events/', 'xml', 4, 'Dining Events', 'food.png', 'CCM', 2, 'superadmin', '23-AUG-13 05.57.28.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'LINK', 'http://events.cord.edu/MasterCalendar/RSSFeeds.aspx?Name=General30', 'xml', 2, 'Events', 'events.png', 'CCM', 2, 'SUPERADMIN', '06-MAR-13 10.07.33.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (5, 0, 'LINK', 'http://instagram.com/concordia_mn', 'html', 7, 'Instagram', 'photos.png', 'CCM', 2, 'SUPERADMIN', '18-MAR-13 09.58.06.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (4, 0, 'LINK', 'https://mobile.twitter.com/concordia_mn', 'html', 7, 'Twitter', 'twitter.png', 'CCM', 1, 'SUPERADMIN', '19-FEB-13 09.59.53.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (1, 0, 'LINK', 'http://www.concordiacollege.edu/feeds/news/', 'xml', 3, 'News', 'news.png', 'CCM', 1, 'SUPERADMIN', '19-FEB-13 06.50.38.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (3, 0, 'LINK', 'https://m.facebook.com/concordiacollege', 'html', 7, 'Facebook', 'facebook.png', 'CCM', 2, 'SUPERADMIN', '19-FEB-13 09.58.32.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (9, 0, 'LINK', 'http://moodlet1.cord.edu/', 'html', 16, 'Moodle', 'moodle.png', 'CCM', 2, 'SUPERADMIN', '31-MAY-13 05.58.59.000000 AM');

commit;
prompt Done.

