PROMPT CREATE OR REPLACE PACKAGE BODY me_biographic
CREATE OR REPLACE package body me_biographic is

  lv_saved         VARCHAR2(01);
  lv_found         VARCHAR2(01);
  spremrg_rec_save gb_emergency_contact.emergency_contact_rec;

  stvrelt_rec stvrelt%ROWTYPE;
  stvstat_rec stvstat%ROWTYPE;
  stvnatn_rec stvnatn%ROWTYPE;

  CURSOR GORADRL_C(P_ROLE VARCHAR2) IS
    select goradrl_atyp_code,
           NVL(stvatyp_desc, 'ADDRESS ' || goradrl_atyp_code) atyp_desc,
           goradrl_role,
           goradrl_priv_ind
      from GENERAL.GORADRL
      LEFT JOIN STVATYP
        ON goradrl_atyp_code = stvatyp_code
     WHERE GORADRL_ROLE = P_ROLE
       and GORADRL_PRIV_IND in ('U', 'D')
     order by GORADRL_PRIV_IND DESC, GORADRL_ATYP_CODE;

  cursor szbgtrm_c is
    select *
      from szbgtrm
     where szbgtrm_check_ind = 'Y'
     order by szbgtrm_term_code desc;

  cursor szbgflg_c(pidm_in szbgflg.szbgflg_pidm%type,
                   term_in szbgflg.szbgflg_term_code%type) is
    select *
      from szbgflg
     where szbgflg_pidm = pidm_in
       and szbgflg_term_code = term_in;

  cursor spraddr_c(pidm_in spraddr.spraddr_pidm%type,
                   atyp_in spraddr.spraddr_atyp_code%type) is
    select *
      from spraddr
     where spraddr_pidm = pidm_in
       and spraddr_atyp_code = atyp_in
       and spraddr_status_ind is null
       and ((sysdate >= spraddr_from_date) or (spraddr_from_date is null))
       and ((sysdate <= spraddr_to_date) or (spraddr_to_date is null))
     order by trunc(spraddr_from_date), spraddr_seqno;

  cursor sprtele_primary_c(pidm_in   spraddr.spraddr_pidm%type,
                           atyp_in   spraddr.spraddr_atyp_code%type,
                           aseqno_in spraddr.spraddr_seqno%type) is
    select *
      from sprtele
     where sprtele_pidm = pidm_in
       and sprtele_atyp_code = atyp_in
       and sprtele_addr_seqno = aseqno_in
       and sprtele_primary_ind = 'Y'
       and sprtele_status_ind is null
     order by sprtele_seqno;

  cursor sprtele_nonprimary_c(pidm_in   spraddr.spraddr_pidm%type,
                              atyp_in   spraddr.spraddr_atyp_code%type,
                              aseqno_in spraddr.spraddr_seqno%type) is
    select *
      from sprtele
     where sprtele_pidm = pidm_in
       and sprtele_atyp_code = atyp_in
       and sprtele_addr_seqno = aseqno_in
       and sprtele_primary_ind is null
       and sprtele_status_ind is null
     order by sprtele_seqno;

  cursor sprtele_type_c(pidm_in spraddr.spraddr_pidm%type,
                        tele_in sprtele.sprtele_tele_code%type) is
    select *
      from sprtele
     where sprtele_pidm = pidm_in
       and sprtele_tele_code = tele_in
       and sprtele_status_ind is null
       and sprtele_atyp_code is null
     order by sprtele_seqno desc;

  cursor spremrg_c(pidm_in spremrg.spremrg_pidm%type) is
    select *
      from spremrg
     where spremrg_pidm = pidm_in
     order by spremrg_priority;

  CURSOR spremrgc(pidm_in spremrg.spremrg_pidm%type) IS
    SELECT *
      FROM spremrg
     WHERE spremrg_pidm = pidm_in
     ORDER BY spremrg_priority, spremrg_last_name;

  CURSOR spremrgcontactc(pidm_in NUMBER, priority VARCHAR2) IS
    SELECT *
      FROM spremrg
     WHERE spremrg_pidm = pidm_in
       AND spremrg_priority = priority;

  CURSOR spremrgmaxpriorityc(pidm_in NUMBER) IS
    SELECT TO_NUMBER(MAX(spremrg_priority))
      FROM spremrg
     WHERE spremrg_pidm = pidm_in;

  cursor stvstat_c(code_in stvstat.stvstat_code%type) is
    select * from stvstat where stvstat_code = code_in;

  cursor stvcnty_c(code_in stvcnty.stvcnty_code%type) is
    select * from stvcnty where stvcnty_code = code_in;

  cursor stvnatn_c(code_in stvnatn.stvnatn_code%type) is
    select * from stvnatn where stvnatn_code = code_in;

  cursor stvrelt_c(code_in stvrelt.stvrelt_code%type) is
    select * from stvrelt where stvrelt_code = code_in;

  cursor sprtele_c(pidm_in  spraddr.spraddr_pidm%type,
                   tele_in  sprtele.sprtele_tele_code%type,
                   seqno_in sprtele.sprtele_seqno%type) is
    select *
      from sprtele
     where sprtele_pidm = pidm_in
       and sprtele_tele_code = tele_in
       and sprtele_seqno = seqno_in
       and sprtele_status_ind is null;

  atyp_arr owa_util.ident_arr;

  FUNCTION f_clean_text(p_text VARCHAR2) RETURN VARCHAR2 IS
    lv_char         gtvsdax.gtvsdax_internal_code%TYPE;
    lv_chr          gtvsdax.gtvsdax_external_code%TYPE;
    lv_changes_chr  VARCHAR2(2000) := '?';
    lv_changes_char VARCHAR2(500) := '?';
    lv_temp         VARCHAR2(10);
    CURSOR translate_c IS
      SELECT gtvsdax_internal_code, gtvsdax_external_code
        FROM gtvsdax
       WHERE gtvsdax_internal_code_group = 'CLEAN_TEXT';
  BEGIN
    IF p_text IS NULL THEN
      RETURN NULL;
    END IF;
    -- Get any user defined translations
    OPEN translate_c;
    LOOP
      FETCH translate_c
        INTO lv_char, lv_chr;
      EXIT WHEN translate_c%NOTFOUND;
      -- need to validate the proper info, and ignore if invalid
      BEGIN
        lv_temp := chr(lv_chr);
      EXCEPTION
        WHEN OTHERS THEN
          lv_char := NULL;
          lv_chr  := NULL;
      END;
      IF lv_char IS NOT NULL AND lv_chr IS NOT NULL THEN
        lv_changes_chr := lv_changes_chr || chr(lv_chr);
        if lv_char = '<SPACE>' then
          lv_char := ' ';
        end if;
        lv_changes_char := lv_changes_char || substr(lv_char, 1, 1);
      END IF;
    END LOOP;
    CLOSE translate_c;
    --
    -- replace tabs(09), new line(10), vertical tab(11), form feed(12), carriage return(13) with a space
    -- smart single quotes with a standard quote and smart double quotes with a standard double quote
    -- and trim leading and trailing spaces
    RETURN(TRIM(translate(p_text,
                          CHR(09) || chr(10) || chr(11) || chr(12) ||
                          chr(13) || chr(14844056) || chr(14844057) ||
                          chr(14844060) || chr(14844061) || lv_changes_chr,
                          '     ''''""' || lv_changes_char)));
    --
  END f_clean_text;

  function fz_sprtele_create(pidm  spraddr.spraddr_pidm%type,
                             tele  sprtele.sprtele_tele_code%type,
                             seqno sprtele.sprtele_seqno%type,
                             area  sprtele.sprtele_phone_area%type,
                             num   sprtele.sprtele_phone_number%type,
                             ext   sprtele.sprtele_phone_ext%type,
                             unl   sprtele.sprtele_unlist_ind%type,
                             accs  sprtele.sprtele_intl_access%type)
    return varchar2 IS
    sprtele_rec sprtele%rowtype;
    msg         varchar2(100);

  BEGIN
    open sprtele_c(pidm, tele, seqno);
    fetch sprtele_c
      into sprtele_rec;
    if sprtele_c%found then
      msg := 'Record already exists.';
    end if;
    close sprtele_c;
    if msg is not null then
      return msg;
    end if;

    if ((num is null) and (accs is null)) then
      return 'Phone Number is required.';
    end if;

    insert into sprtele
      (sprtele_pidm,
       sprtele_tele_code,
       sprtele_seqno,
       sprtele_activity_date,
       sprtele_phone_area,
       sprtele_phone_number,
       sprtele_phone_ext,
       sprtele_status_ind,
       sprtele_atyp_code,
       sprtele_addr_seqno,
       sprtele_primary_ind,
       sprtele_unlist_ind,
       sprtele_comment,
       sprtele_intl_access,
       sprtele_data_origin,
       sprtele_user_id,
       sprtele_ctry_code_phone)
    values
      (pidm,
       tele,
       seqno,
       sysdate,
       area,
       num,
       ext,
       null,
       null,
       null,
       null,
       unl,
       null,
       accs,
       'ZWBKBIOP',
       user,
       null);

    return null;
  exception
    when others then
      rollback;
      return 'Create failed.';

  END fz_sprtele_create;

  function fz_sprtele_inactivate(pidm  spraddr.spraddr_pidm%type,
                                 tele  sprtele.sprtele_tele_code%type,
                                 seqno sprtele.sprtele_seqno%type)
    return varchar2 IS
    sprtele_rec sprtele%rowtype;
    msg         varchar2(100);

  BEGIN
    open sprtele_c(pidm, tele, seqno);
    fetch sprtele_c
      into sprtele_rec;
    if sprtele_c%notfound then
      msg := 'Record does not exist.';
    end if;
    close sprtele_c;
    if msg is not null then
      return msg;
    end if;

    update sprtele
       set sprtele_status_ind    = 'I',
           sprtele_activity_date = sysdate,
           sprtele_user_id       = user
     where sprtele_pidm = pidm
       and sprtele_tele_code = tele
       and sprtele_seqno = seqno;

    return null;
  exception
    when others then
      rollback;
      return 'Inactivate old failed.';

  END fz_sprtele_inactivate;

  function fz_sprtele_update(pidm     spraddr.spraddr_pidm%type,
                             tele     sprtele.sprtele_tele_code%type,
                             seqno    sprtele.sprtele_seqno%type,
                             newseqno sprtele.sprtele_seqno%type,
                             area     sprtele.sprtele_phone_area%type,
                             num      sprtele.sprtele_phone_number%type,
                             ext      sprtele.sprtele_phone_ext%type,
                             unl      sprtele.sprtele_unlist_ind%type,
                             accs     sprtele.sprtele_intl_access%type)
    return varchar2 IS
    sprtele_rec sprtele%rowtype;
    msg         varchar2(100);

  BEGIN
    open sprtele_c(pidm, tele, seqno);
    fetch sprtele_c
      into sprtele_rec;
    if sprtele_c%notfound then
      msg := 'Record does not exist.';
    end if;
    close sprtele_c;
    if msg is not null then
      return msg;
    end if;

    if ((nvl(area, 'nullstr') =
       nvl(sprtele_rec.sprtele_phone_area, 'nullstr')) and
       (nvl(num, 'nullstr') =
       nvl(sprtele_rec.sprtele_phone_number, 'nullstr')) and
       (nvl(ext, 'nullstr') =
       nvl(sprtele_rec.sprtele_phone_ext, 'nullstr')) and
       (nvl(accs, 'nullstr') =
       nvl(sprtele_rec.sprtele_intl_access, 'nullstr')) and
       (nvl(unl, 'nullstr') =
       nvl(sprtele_rec.sprtele_unlist_ind, 'nullstr'))) then
      return null;
    end if;

    msg := fz_sprtele_create(pidm,
                             tele,
                             newseqno,
                             area,
                             num,
                             ext,
                             unl,
                             accs);
    if msg is null then
      msg := fz_sprtele_inactivate(pidm, tele, seqno);
    end if;

    return msg;
  exception
    when others then
      rollback;
      return 'Update failed.';

  END fz_sprtele_update;

  FUNCTION fz_emrg_contactexists(pidm_in     IN NUMBER,
                                 priority_in IN VARCHAR2 DEFAULT NULL)
    RETURN BOOLEAN IS
    contactexists BOOLEAN;
    spremrg_rec   spremrg%ROWTYPE;

  BEGIN
    IF priority_in IS NULL THEN
      OPEN spremrgc(pidm_in);
      FETCH spremrgc
        INTO spremrg_rec;
      contactexists := spremrgc%FOUND;
      CLOSE spremrgc;
    ELSE
      OPEN spremrgcontactc(pidm_in, priority_in);
      FETCH spremrgcontactc
        INTO spremrg_rec;
      contactexists := spremrgcontactc%FOUND;
      CLOSE spremrgcontactc;
    END IF;

    RETURN contactexists;
  END fz_emrg_contactexists;

  procedure pz_convert_commastr_to_table(p_in_string     varchar2,
                                         p_varchar_table IN OUT twbklibs.varchar2_tabtype) is

    CURSOR C_SPLIT_DATA IS with test as(
      select p_in_string str
        from dual)
      SELECT regexp_substr(str, '([^,]*)(,|$)', 1, rownum, 'i', 1) value_of
        FROM test
      CONNECT BY LEVEL <= length(regexp_replace(str, '[^,]+')) + 1;

    v_idx number := 1;

  begin

    FOR R_DATA IN C_SPLIT_DATA LOOP

      p_varchar_table(v_idx) := TRIM(R_DATA.value_of);

      v_idx := v_idx + 1;

    END LOOP;

  end;

  ---------------------------------------------------------------------------
  /* This procedure will "compress" the numbers used for prioritizing the
     contacts.  For example, if contact 3 is removed from (1,2,3,4,5),
     then the list will be compressed from (1,2,4,5) to (1,2,3,4).
  */

  PROCEDURE pz_emrg_compresspriority(pidm_in     IN NUMBER,
                                     priority_in IN VARCHAR2,
                                     maxpriority IN NUMBER) IS
    onelower      NUMBER;
    newseq        NUMBER;
    spremrg_cv    gb_emergency_contact.emergency_contact_ref;
    spremrg_rec   gb_emergency_contact.emergency_contact_rec;
    spremrg_rowid VARCHAR2(30);
  BEGIN
    onelower := TO_NUMBER(priority_in) + 1;
    lv_found := 'N';

    IF TO_NUMBER(priority_in) < maxpriority THEN
      --
      -- go from (OneLower) to (MaxPriority)
      -- and individually move (update) each priority to its value - 1
      --
      FOR pointer IN onelower .. maxpriority LOOP
        IF pointer = 10 THEN
          IF lv_saved = 'Y' THEN
            spremrg_rec := spremrg_rec_save;
            lv_found    := 'Y';
          END IF;
        ELSE
          spremrg_cv := gb_emergency_contact.f_query_one(pidm_in,
                                                         TO_CHAR(pointer));
          FETCH spremrg_cv
            INTO spremrg_rec;
          IF spremrg_cv%FOUND THEN
            lv_found := 'Y';
            gb_emergency_contact.p_delete(p_pidm     => spremrg_rec.r_pidm,
                                          p_priority => spremrg_rec.r_priority);
          END IF;
        END IF;
        IF lv_found = 'Y' THEN
          IF pointer = 10 THEN
            newseq := 10 - 1;
          ELSE
            newseq := TO_NUMBER(spremrg_rec.r_priority - 1);
          END IF;
          gb_emergency_contact.p_create(p_pidm            => spremrg_rec.r_pidm,
                                        p_priority        => TO_CHAR(newseq),
                                        p_surname_prefix  => /*goksels.*/f_clean_text(spremrg_rec.r_surname_prefix),
                                        p_last_name       => /*goksels.*/f_clean_text(spremrg_rec.r_last_name),
                                        p_first_name      => /*goksels.*/f_clean_text(spremrg_rec.r_first_name),
                                        p_mi              => /*goksels.*/f_clean_text(spremrg_rec.r_mi),
                                        p_house_number    => /*goksels.*/f_clean_text(spremrg_rec.r_house_number),
                                        p_street_line1    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line1),
                                        p_street_line2    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line2),
                                        p_street_line3    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line3),
                                        p_street_line4    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line4),
                                        p_city            => /*goksels.*/f_clean_text(spremrg_rec.r_city),
                                        p_stat_code       => spremrg_rec.r_stat_code,
                                        p_natn_code       => spremrg_rec.r_natn_code,
                                        p_zip             => /*goksels.*/f_clean_text(spremrg_rec.r_zip),
                                        p_ctry_code_phone => spremrg_rec.r_ctry_code_phone,
                                        p_phone_area      => spremrg_rec.r_phone_area,
                                        p_phone_number    => spremrg_rec.r_phone_number,
                                        p_phone_ext       => spremrg_rec.r_phone_ext,
                                        p_relt_code       => spremrg_rec.r_relt_code,
                                        p_atyp_code       => spremrg_rec.r_atyp_code,
                                        p_data_origin     => gb_common.DATA_ORIGIN,
                                        p_user_id         => twbklogn.f_get_banner_id(pidm_in),
                                        p_rowid_out       => spremrg_rowid);
        END IF;
        IF spremrg_cv%ISOPEN THEN
          CLOSE spremrg_cv;
        END IF;
        --
        gb_common.p_commit;
        --
      END LOOP;
    END IF;
  END pz_emrg_compresspriority;

  ---------------------------------------------------------------------------
  /*
     This procedure will shuffle the contacts if the user
     changes the
     priority number of a contact.  Basically, the contacts with priority
     values greater than the requested priority are bumped up one.  Then
     the contact with the original priority number is renumbered to the
     desired priority number.

     There are two situations handled by this procedure:  1) the new
     priority is lower (a larger number) than the original priority; and 2)
     the new priority is higher (a smaller number) than the original
     priority.

     Example 1):
                   If there are six contacts (a,b,c,d,e,f) and the user
     chooses to move contact (d) to the 2nd position, the procedure bumps
     the (b,c,d,e,f) so the list becomes (a, ,b,c,d,e,f) and then moves the
     (d) to the empty position (the 2nd position) and the list becomes
     (a,d,b,c, ,e,f).  The P_Compress_Priority procedure is then called
     to compress out the space. The end result is (a,d,b,c,e,f).  Notice
     that the (d) is now in the 2nd position.

     Example 2):
                   If there are six contacts (a,b,c,d,e,f) and the user
     chooses to move contact (b) to the 5th position, the procedure bumps
     the (f) so the list becomes (a,b,c,d,e, ,f) and then moves the (b)
     to the empty position (the 6th position) and the list becomes
     (a, ,c,d,e,b,f).  The P_Compress_Priority procedure is then called
     to compress out the space. The end result is (a,c,d,e,b,f).  Notice
     that the (b) is now in the 5th position.
  */

  PROCEDURE pz_emrg_shufflepriority(pidm_in     IN NUMBER,
                                    priority_in IN VARCHAR2,
                                    oldpri      IN VARCHAR2,
                                    maxpriority IN NUMBER) IS
    origpri       VARCHAR2(2);
    newpri        VARCHAR2(2);
    maxpriority2  VARCHAR2(2);
    stoppingpoint NUMBER;
    frompri       VARCHAR2(2);
    topri         VARCHAR2(2);
    spremrg_cv    gb_emergency_contact.emergency_contact_ref;
    spremrg_rec   gb_emergency_contact.emergency_contact_rec;
    spremrg_rowid VARCHAR2(30);
  BEGIN
    lv_saved := 'N';
    IF priority_in != oldpri THEN
      origpri := oldpri;
      newpri  := priority_in;

      --
      -- Bumping always begins at the last number.  We have to decide
      -- how far up the list we continue to bump ...
      --
      -- If the new priority is higher (smaller) than original,
      -- then stop bumping at the new priority (NewPri).
      --
      -- Otherwise, stop bumping at the next one down (NewPri + 1).
      --
      IF newpri < origpri THEN
        stoppingpoint := TO_NUMBER(newpri);
      ELSE
        stoppingpoint := TO_NUMBER(newpri) + 1;
      END IF;

      --
      -- go from (MaxPriority) to (StoppingPoint)  [in reverse]
      -- and individually move (update) each priority to its value + 1
      --

      FOR pointer IN REVERSE stoppingpoint .. maxpriority LOOP
        spremrg_cv := gb_emergency_contact.f_query_one(pidm_in,
                                                       TO_CHAR(pointer));
        FETCH spremrg_cv
          INTO spremrg_rec;
        IF spremrg_cv%FOUND THEN
          gb_emergency_contact.p_delete(p_pidm     => spremrg_rec.r_pidm,
                                        p_priority => spremrg_rec.r_priority);
          IF TO_NUMBER(spremrg_rec.r_priority + 1) = 10 THEN
            spremrg_rec_save := spremrg_rec;
            lv_saved         := 'Y';
          ELSE
            gb_emergency_contact.p_create(p_pidm            => spremrg_rec.r_pidm,
                                          p_priority        => TO_CHAR(TO_NUMBER(spremrg_rec.r_priority + 1)),
                                          p_surname_prefix  => /*goksels.*/f_clean_text(spremrg_rec.r_surname_prefix),
                                          p_last_name       => /*goksels.*/f_clean_text(spremrg_rec.r_last_name),
                                          p_first_name      => /*goksels.*/f_clean_text(spremrg_rec.r_first_name),
                                          p_mi              => /*goksels.*/f_clean_text(spremrg_rec.r_mi),
                                          p_house_number    => /*goksels.*/f_clean_text(spremrg_rec.r_house_number),
                                          p_street_line1    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line1),
                                          p_street_line2    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line2),
                                          p_street_line3    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line3),
                                          p_street_line4    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line4),
                                          p_city            => /*goksels.*/f_clean_text(spremrg_rec.r_city),
                                          p_stat_code       => spremrg_rec.r_stat_code,
                                          p_natn_code       => spremrg_rec.r_natn_code,
                                          p_zip             => /*goksels.*/f_clean_text(spremrg_rec.r_zip),
                                          p_ctry_code_phone => spremrg_rec.r_ctry_code_phone,
                                          p_phone_area      => spremrg_rec.r_phone_area,
                                          p_phone_number    => spremrg_rec.r_phone_number,
                                          p_phone_ext       => spremrg_rec.r_phone_ext,
                                          p_relt_code       => spremrg_rec.r_relt_code,
                                          p_atyp_code       => spremrg_rec.r_atyp_code,
                                          p_data_origin     => gb_common.DATA_ORIGIN,
                                          p_user_id         => twbklogn.f_get_banner_id(pidm_in),
                                          p_rowid_out       => spremrg_rowid);
          END IF;
        END IF;
        IF spremrg_cv%ISOPEN THEN
          CLOSE spremrg_cv;
        END IF;
        --
        gb_common.p_commit;
        --
      END LOOP;

      --
      -- Update the priority of the contact the user has chosen.
      --
      -- If the original priority (OrigPri) is higher (smaller number)
      -- than the new priority (NewPri), then move the original
      -- priority (OrigPri) to a spot one lower (larger number) than
      -- the new priority (NewPri + 1).
      --
      -- If the original priority is lower (larger number) than the new
      -- priority, then move the contact that is one lower (larger number)
      -- than the original priority (OrigPri + 1) to the new position (NewPri).
      --
      IF origpri < newpri THEN
        frompri := origpri;
        topri   := TO_CHAR(TO_NUMBER(newpri) + 1);
      ELSE
        frompri := TO_CHAR(TO_NUMBER(origpri) + 1);
        topri   := newpri;
      END IF;

      lv_found := 'N';
      IF frompri = 10 THEN
        IF lv_saved = 'Y' THEN
          spremrg_rec := spremrg_rec_save;
          lv_found    := 'Y';
        END IF;
      ELSE
        spremrg_cv := gb_emergency_contact.f_query_one(pidm_in, frompri);
        FETCH spremrg_cv
          INTO spremrg_rec;
        IF spremrg_cv%FOUND THEN
          gb_emergency_contact.p_delete(p_pidm     => spremrg_rec.r_pidm,
                                        p_priority => spremrg_rec.r_priority);
          lv_found := 'Y';
        ELSE
          lv_found := 'N';
        END IF;
      END IF;
      IF lv_found = 'Y' THEN
        IF topri = 10 THEN
          spremrg_rec_save := spremrg_rec;
          lv_saved         := 'Y';
        ELSE
          gb_emergency_contact.p_create(p_pidm            => spremrg_rec.r_pidm,
                                        p_priority        => topri,
                                        p_surname_prefix  => /*goksels.*/f_clean_text(spremrg_rec.r_surname_prefix),
                                        p_last_name       => /*goksels.*/f_clean_text(spremrg_rec.r_last_name),
                                        p_first_name      => /*goksels.*/f_clean_text(spremrg_rec.r_first_name),
                                        p_mi              => /*goksels.*/f_clean_text(spremrg_rec.r_mi),
                                        p_house_number    => /*goksels.*/f_clean_text(spremrg_rec.r_house_number),
                                        p_street_line1    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line1),
                                        p_street_line2    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line2),
                                        p_street_line3    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line3),
                                        p_street_line4    => /*goksels.*/f_clean_text(spremrg_rec.r_street_line4),
                                        p_city            => /*goksels.*/f_clean_text(spremrg_rec.r_city),
                                        p_stat_code       => spremrg_rec.r_stat_code,
                                        p_natn_code       => spremrg_rec.r_natn_code,
                                        p_zip             => /*goksels.*/f_clean_text(spremrg_rec.r_zip),
                                        p_ctry_code_phone => spremrg_rec.r_ctry_code_phone,
                                        p_phone_area      => spremrg_rec.r_phone_area,
                                        p_phone_number    => spremrg_rec.r_phone_number,
                                        p_phone_ext       => spremrg_rec.r_phone_ext,
                                        p_relt_code       => spremrg_rec.r_relt_code,
                                        p_atyp_code       => spremrg_rec.r_atyp_code,
                                        p_data_origin     => gb_common.DATA_ORIGIN,
                                        p_user_id         => twbklogn.f_get_banner_id(pidm_in),
                                        p_rowid_out       => spremrg_rowid);
        END IF;
      END IF;
      IF spremrg_cv%ISOPEN THEN
        CLOSE spremrg_cv;
      END IF;

      --
      -- Figure out what the maximum priority is NOW so that we can
      -- compress the list.
      --
      OPEN spremrgmaxpriorityc(pidm_in);
      FETCH spremrgmaxpriorityc
        INTO maxpriority2;

      IF spremrgmaxpriorityc%NOTFOUND THEN
        maxpriority2 := 0;
      END IF;
      IF lv_saved = 'Y' THEN
        maxpriority2 := 10;
      END IF;

      CLOSE spremrgmaxpriorityc;
      --
      pz_emrg_compresspriority(pidm_in, frompri, maxpriority2);
      --
    END IF;
  END pz_emrg_shufflepriority;

  FUNCTION fz_get_email_address(P_PIDM       NUMBER,
                                P_EMAIL_CODE GOREMAL.GOREMAL_EMAL_CODE%TYPE default null,
                                P_PREF_IND   GOREMAL.GOREMAL_PREFERRED_IND%TYPE default 'N')
    RETURN VARCHAR2 IS
    V_EMAIL      VARCHAR2(180);
    V_EMAIL_CODE VARCHAR2(4) := NULL;
  BEGIN

    IF P_PREF_IND = 'Y' THEN

      begin
        SELECT GOREMAL_EMAIL_ADDRESS
          INTO V_EMAIL
          FROM GOREMAL
         WHERE GOREMAL_PIDM = P_PIDM
           AND GOREMAL_STATUS_IND = 'A'
           AND GOREMAL_PREFERRED_IND = 'Y'
           AND ROWNUM = 1;
      exception
        when others then

          V_EMAIL := NULL;
          /* if no GOREMAL_PREFERRED_IND is set then get from gtvsdax */
          begin
            SELECT GTVSDAX_EXTERNAL_CODE
              INTO V_EMAIL_CODE
              FROM GTVSDAX
             WHERE GTVSDAX_INTERNAL_CODE_SEQNO =
                   (SELECT MIN(GTVSDAX_INTERNAL_CODE_SEQNO)
                      FROM GTVSDAX, GOREMAL
                     WHERE GOREMAL_PIDM = P_PIDM
                       AND GOREMAL_STATUS_IND = 'A'
                       AND GOREMAL_PREFERRED_IND = 'Y'
                       AND GTVSDAX_EXTERNAL_CODE = GOREMAL_EMAL_CODE
                       AND GTVSDAX_INTERNAL_CODE_GROUP = 'EMAIL'
                       AND GTVSDAX_INTERNAL_CODE = 'GENEMAIL')
               AND GTVSDAX_INTERNAL_CODE_GROUP = 'EMAIL'
               AND GTVSDAX_INTERNAL_CODE = 'GENEMAIL';
          exception
            when others then
              V_EMAIL_CODE := NULL;
          end;

      end;

    END IF;

    IF V_EMAIL_CODE IS NULL THEN
      V_EMAIL_CODE := P_EMAIL_CODE;
    END IF;

    FOR I IN (SELECT GOREMAL_EMAIL_ADDRESS
                INTO V_EMAIL
                FROM GOREMAL
               WHERE GOREMAL_PIDM = P_PIDM
                 AND GOREMAL_STATUS_IND = 'A'
                 AND GOREMAL_EMAL_CODE = V_EMAIL_CODE) LOOP
      V_EMAIL := I.GOREMAL_EMAIL_ADDRESS;
    END LOOP;

    RETURN V_EMAIL; /* This will have value either from prefered or from email code */

  exception
    when others then
      RETURN V_EMAIL;
  END;

  PROCEDURE pz_procaddrupdate(pidm      IN OUT spriden.spriden_pidm%type,
                              atyp      VARCHAR2,
                              seqno     IN OUT NUMBER,
                              deladdr   VARCHAR2,
                              fdate     VARCHAR2,
                              tdate     VARCHAR2,
                              houseno   VARCHAR2,
                              str1      VARCHAR2,
                              str2      VARCHAR2,
                              str3      VARCHAR2,
                              str4      VARCHAR2,
                              city      VARCHAR2,
                              stat      VARCHAR2,
                              zip       VARCHAR2,
                              cnty      VARCHAR2,
                              natn      VARCHAR2,
                              ctry      VARCHAR2 DEFAULT NULL,
                              area      VARCHAR2 DEFAULT NULL,
                              num       VARCHAR2 DEFAULT NULL,
                              ext       VARCHAR2 DEFAULT NULL,
                              accs      VARCHAR2 DEFAULT NULL,
                              unl       VARCHAR2 DEFAULT NULL,
                              seqnos    IN OUT twbklibs.varchar2_tabtype,
                              teles     twbklibs.varchar2_tabtype,
                              ctrys     twbklibs.varchar2_tabtype,
                              areas     twbklibs.varchar2_tabtype,
                              phones    twbklibs.varchar2_tabtype,
                              exts      twbklibs.varchar2_tabtype,
                              accss     twbklibs.varchar2_tabtype,
                              unl1      VARCHAR2 DEFAULT NULL,
                              unl2      VARCHAR2 DEFAULT NULL,
                              unl3      VARCHAR2 DEFAULT NULL,
                              unl4      VARCHAR2 DEFAULT NULL,
                              unl5      VARCHAR2 DEFAULT NULL,
                              del1      VARCHAR2 DEFAULT NULL,
                              del2      VARCHAR2 DEFAULT NULL,
                              del3      VARCHAR2 DEFAULT NULL,
                              del4      VARCHAR2 DEFAULT NULL,
                              del5      VARCHAR2 DEFAULT NULL,
                              P_OUT_MSG OUT VARCHAR2) IS
    fcmp_date      DATE;
    t_date         DATE;
    f_date         DATE;
    aseqno         NUMBER;
    pseqno         NUMBER;
    newaddr        VARCHAR2(5);
    stvstat_rec    stvstat%ROWTYPE;
    stvnatn_rec    stvnatn%ROWTYPE;
    trimmed_num    VARCHAR2(13);
    trimmed_phones twbklibs.varchar2_tabtype;
    -- local variables to store address data with tabs removed
    /* BEGIN: CLEAN_Address Changes - use local variables lv_ */
    --lv_houseno       SPRADDR.SPRADDR_HOUSE_NUMBER%TYPE;
    --lv_str1          SPRADDR.SPRADDR_STREET_LINE1%TYPE;
    --lv_str2          SPRADDR.SPRADDR_STREET_LINE2%TYPE;
    --lv_str3          SPRADDR.SPRADDR_STREET_LINE3%TYPE;
    --lv_str4          SPRADDR.SPRADDR_STREET_LINE4%TYPE;
    --lv_city          SPRADDR.SPRADDR_CITY%TYPE;
    --lv_zip           SPRADDR.SPRADDR_ZIP%TYPE;
    lv_houseno SPRADDR.SPRADDR_HOUSE_NUMBER%TYPE := houseno;
    lv_str1    spraddr.spraddr_street_line1%TYPE := str1;
    lv_str2    spraddr.spraddr_street_line2%TYPE := str2;
    lv_str3    spraddr.spraddr_street_line3%TYPE := str3;
    lv_str4    spraddr.spraddr_street_line4%TYPE := str4;
    lv_city    spraddr.spraddr_city%TYPE := city;
    lv_stat    spraddr.spraddr_stat_code%TYPE := stat;
    lv_zip     spraddr.spraddr_zip%TYPE := zip;
    lv_cnty    spraddr.spraddr_cnty_code%TYPE := cnty;
    lv_natn    spraddr.spraddr_natn_code%TYPE := natn;
    /* END: CLEAN_Address Changes - use local variables lv_ */

    i number := 1;

  BEGIN

    --Not required in Mobile

    /* IF NOT twbkwbis.f_validuser(pidm) THEN
      RETURN;
    END IF;*/

    /* Initialise the error message to null */
    zwgkoadr.msg_text := null;

    --Used below while debugging

    /*FOR i IN 1 .. 5 LOOP

      insert into mobedu.phone_pram
      values
        (pidm,
         atyp,
         seqno,
         seqnos(i),
         teles(i),
         ctrys(i),
         areas(i),
         phones(i),
         unl1,
         del1);

      COMMIT;

    END LOOP;

    insert into mobedu.addr_pram
    values
      (pidm,
       atyp,
       seqno,
       deladdr,
       fdate,
       tdate,
       houseno,
       str1,
       str2,
       str3,
       str4,
       city,
       stat,
       zip,
       cnty,
       natn,
       ctry,
       area,
       num,
       ext,
       accs,
       unl);
    commit;*/

    --
    /* Validate hidden fields */

    DBMS_OUTPUT.PUT_LINE('Atyp Validation Start');
    zwgkoadr.p_validateatyp(atyp, 'U');
    DBMS_OUTPUT.PUT_LINE('Atyp Validation End');

    IF zwgkoadr.msg_text IS NOT NULL THEN
      zwgkoadr.msg_level := 1;

      DBMS_OUTPUT.PUT_LINE('Error:' || zwgkoadr.msg_text);

      P_OUT_MSG := G$_NLS.Get('BWGKOGA1-0008',
                              'SQL',
                              'Errors occurred.  Please try again.%01%%02%',
                              HTF.br,
                              zwgkoadr.msg_text);
      RETURN;
    END IF;

    --
    trimmed_num := SUBSTR(REPLACE(num, '-'), 1, 13);
    trimmed_num := SUBSTR(REPLACE(trimmed_num, ' '), 1, 12);

    FOR i IN 1 .. phones.COUNT LOOP
      trimmed_phones(i) := SUBSTR(REPLACE(phones(i), '-'), 1, 13);
      trimmed_phones(i) := SUBSTR(REPLACE(trimmed_phones(i), ' '), 1, 12);
    END LOOP;

    --97926 Strip tabs
    lv_houseno := REPLACE(houseno, chr(9));
    lv_str1    := REPLACE(str1, chr(9));
    lv_str2    := REPLACE(str2, chr(9));
    lv_str3    := REPLACE(str3, chr(9));
    lv_str4    := REPLACE(str4, chr(9));
    lv_city    := REPLACE(city, chr(9));
    lv_zip     := REPLACE(zip, chr(9));

    DBMS_OUTPUT.PUT_LINE('Address validation start' || seqno);

    zwgkoadr.p_valaddrupdate(pidm,
                             atyp,
                             seqno,
                             deladdr,
                             fdate,
                             tdate,
                             /* BEGIN: CLEAN_Address Changes - use local variables lv_ */
                             lv_houseno,
                             lv_str1,
                             lv_str2,
                             lv_str3,
                             lv_str4,
                             lv_city,
                             lv_stat,
                             lv_zip,
                             lv_cnty,
                             lv_natn,
                             /* END: CLEAN_Address Changes - use local variables lv_ */
                             ctry,
                             area,
                             trimmed_num,
                             ext,
                             accs,
                             unl,
                             seqnos,
                             teles,
                             ctrys,
                             areas,
                             trimmed_phones,
                             exts,
                             accss,
                             unl1,
                             unl2,
                             unl3,
                             unl4,
                             unl5,
                             del1,
                             del2,
                             del3,
                             del4,
                             del5);

    DBMS_OUTPUT.PUT_LINE('Address validation end');

    IF zwgkoadr.msg_text IS NOT NULL THEN

      DBMS_OUTPUT.PUT_LINE('Error:' || zwgkoadr.msg_text);

      P_OUT_MSG := zwgkoadr.msg_text;
      RETURN;
    END IF;

    --
    DBMS_OUTPUT.PUT_LINE('Update/Insert Address Start');
    zwgkoadr.p_storeaddrupdate(pidm,
                               atyp,
                               seqno,
                               deladdr,
                               fdate,
                               tdate,
                               /* BEGIN: CLEAN_Address Changes - use local variables lv_ */
                               lv_houseno,
                               lv_str1,
                               lv_str2,
                               lv_str3,
                               lv_str4,
                               lv_city,
                               lv_stat,
                               lv_zip,
                               lv_cnty,
                               lv_natn,
                               /* END: CLEAN_Address Changes - use local variables lv_ */
                               ctry,
                               area,
                               trimmed_num,
                               ext,
                               accs,
                               unl,
                               seqnos,
                               teles,
                               ctrys,
                               areas,
                               trimmed_phones,
                               exts,
                               accss,
                               unl1,
                               unl2,
                               unl3,
                               unl4,
                               unl5,
                               del1,
                               del2,
                               del3,
                               del4,
                               del5);

    DBMS_OUTPUT.PUT_LINE('Update/Insert Address Start');

  EXCEPTION
    WHEN OTHERS THEN
      gb_common.p_rollback;

      DBMS_OUTPUT.PUT_LINE('Error: Update/Insert Address ' || SQLERRM);

      IF SQLCODE = gb_event.APP_ERROR THEN

        P_OUT_MSG := SQLERRM;

      ELSE
        --RAISE;
        P_OUT_MSG := SQLERRM;

      END IF;
  END;

  PROCEDURE pz_process_tele(pidm      IN OUT spriden.spriden_pidm%type,
                            tele      varchar2 default null,
                            seqno     varchar2 default null,
                            area      varchar2 default null,
                            num       varchar2 default null,
                            ext       varchar2 default null,
                            accs      varchar2 default null,
                            unl       varchar2 default null,
                            del       varchar2 default null,
                            P_OUT_MSG OUT varchar2) IS

    etele       sprtele.sprtele_tele_code%type;
    eseqno      sprtele.sprtele_seqno%type;
    enewseqno   sprtele.sprtele_seqno%type;
    earea       sprtele.sprtele_phone_area%type;
    enum        sprtele.sprtele_phone_number%type;
    eext        sprtele.sprtele_phone_ext%type;
    eaccs       sprtele.sprtele_intl_access%type;
    eunl        sprtele.sprtele_unlist_ind%type;
    edel        varchar2(1);
    sprtele_rec sprtele%rowtype;
    tele_desc   stvtele.stvtele_desc%type;

  BEGIN

    etele  := tele;
    eseqno := NVL(to_number(seqno), 0);
    earea  := trim(area);
    enum   := trim(replace(replace(num, '-'), ' '));
    eext   := trim(ext);
    eaccs  := trim(accs);
    eunl   := unl;
    edel   := del;
    begin
      select max(sprtele_seqno) + 1
        into enewseqno
        from sprtele
       where sprtele_pidm = pidm;
    exception
      when others then
        enewseqno := 1;
    end;
    -- enewseqno:=eseqno+1;
    if edel = 'Y' then
      P_OUT_MSG := fz_sprtele_inactivate(pidm, etele, eseqno);
    elsif eseqno = 0 then
      P_OUT_MSG := fz_sprtele_create(pidm,
                                     etele,
                                     enewseqno,
                                     earea,
                                     enum,
                                     eext,
                                     eunl,
                                     eaccs);
    else
      P_OUT_MSG := fz_sprtele_update(pidm,
                                     etele,
                                     eseqno,
                                     enewseqno,
                                     earea,
                                     enum,
                                     eext,
                                     eunl,
                                     eaccs);
    end if;

    if P_OUT_MSG is not null then
      rollback;
    else
      commit;
    end if;

  END;

  PROCEDURE pz_process_emrgcontacts(p_pidm        IN spriden.spriden_pidm%type,
                                    p_priority_in IN VARCHAR2,
                                    p_rship       IN VARCHAR2 DEFAULT NULL,
                                    p_lname_pfx   IN VARCHAR2 DEFAULT NULL,
                                    p_lname       IN VARCHAR2 DEFAULT NULL,
                                    p_fname       IN VARCHAR2 DEFAULT NULL,
                                    p_mi          IN VARCHAR2 DEFAULT NULL,
                                    p_atyp        IN VARCHAR2 DEFAULT NULL,
                                    p_houseno     IN VARCHAR2 DEFAULT NULL,
                                    p_addr1       IN VARCHAR2 DEFAULT NULL,
                                    p_addr2       IN VARCHAR2 DEFAULT NULL,
                                    p_addr3       IN VARCHAR2 DEFAULT NULL,
                                    p_addr4       IN VARCHAR2 DEFAULT NULL,
                                    p_city        IN VARCHAR2 DEFAULT NULL,
                                    p_stat        IN VARCHAR2 DEFAULT NULL,
                                    p_natn        IN VARCHAR2 DEFAULT NULL,
                                    p_zip         IN VARCHAR2 DEFAULT NULL,
                                    p_ctry        IN VARCHAR2 DEFAULT NULL,
                                    p_area        IN VARCHAR2 DEFAULT NULL,
                                    p_phone       IN VARCHAR2 DEFAULT NULL,
                                    p_ext         IN VARCHAR2 DEFAULT NULL,
                                    p_remove_it   IN VARCHAR2 DEFAULT NULL,
                                    p_oldpri      IN VARCHAR2 DEFAULT NULL,
                                    p_last_active IN VARCHAR2 DEFAULT NULL,
                                    P_OUT_MSG     OUT VARCHAR2) IS

    maxpriority   NUMBER;
    spremrg_rowid VARCHAR2(30);
    lv_url        VARCHAR2(2000);
    spremrg_cv    gb_emergency_contact.emergency_contact_ref;
    return_date   DATE DEFAULT NULL;
    spremrg_rec   spremrg%ROWTYPE;

  BEGIN

    DBMS_OUTPUT.PUT_LINE('Emergency conact:BEGIN');

    IF spremrgcontactc%ISOPEN THEN
      CLOSE spremrgcontactc;
    END IF;

    OPEN spremrgcontactc(p_pidm, p_priority_in);
    FETCH spremrgcontactc
      INTO spremrg_rec;
    CLOSE spremrgcontactc;

    IF spremrgmaxpriorityc%ISOPEN THEN
      CLOSE spremrgmaxpriorityc;
    END IF;

    OPEN spremrgmaxpriorityc(p_pidm);
    FETCH spremrgmaxpriorityc
      INTO maxpriority;
    IF spremrgmaxpriorityc%NOTFOUND THEN
      maxpriority := 0;
    END IF;
    CLOSE spremrgmaxpriorityc;

    DBMS_OUTPUT.PUT_LINE('Emergency conact:Cursor records retrived');

    IF NVL(p_remove_it, 'N') = 'Y' THEN
      IF NOT (fz_emrg_contactexists(p_pidm, p_oldpri)) THEN

        P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0037',
                                'SQL',
                                'Could not remove the contact; it does not exist.');
      ELSE

        DBMS_OUTPUT.PUT_LINE('Emergency conact:Delete Conact');

        gb_emergency_contact.p_delete(p_pidm     => p_pidm,
                                      p_priority => p_oldpri);
        gb_common.p_commit;

        IF fz_emrg_contactexists(p_pidm) THEN
          DBMS_OUTPUT.PUT_LINE('Emergency conact:priority compression');
          pz_emrg_compresspriority(p_pidm, p_oldpri, maxpriority);
        ELSE
          spremrg_rec.spremrg_priority := NULL;
        END IF;

      END IF;

      RETURN; /* Once contact is removed and priorities are compressed just return */

    END IF;

    ----From here create or update starts

    ------------------------------------------------------------------------------------------
    /* Validation phase */

    IF NOT (goklibs.f_isnumber(p_priority_in)) THEN
      --global_msg_level := 1; -- error
      P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0038',
                              'SQL',
                              'Order value must be a number. You entered %01%.',
                              p_priority_in);
    END IF;

    IF p_priority_in IS NULL THEN
      P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0040',
                              'SQL',
                              '%01%Order must be entered.%02%',
                              P_OUT_MSG,
                              HTF.br);
    END IF;

    IF /*goksels.*/f_clean_text(p_fname) IS NULL THEN
      P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0041',
                              'SQL',
                              '%01%First Name must be entered.%02%',
                              P_OUT_MSG,
                              HTF.br);
    END IF;

    IF /*goksels.*/f_clean_text(p_lname) IS NULL THEN
      P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0042',
                              'SQL',
                              '%01%Last Name must be entered.%02%',
                              P_OUT_MSG,
                              HTF.br);
    END IF;

    IF (p_houseno IS NOT NULL) OR (p_addr1 IS NOT NULL) OR
       (p_addr2 IS NOT NULL) OR (p_addr3 IS NOT NULL) OR
       (p_addr4 IS NOT NULL) OR (p_city IS NOT NULL) OR
       (p_stat IS NOT NULL) OR (p_zip IS NOT NULL) OR (p_natn IS NOT NULL) THEN
      IF /*goksels.*/f_clean_text(p_addr1) IS NULL THEN
        P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0043',
                                'SQL',
                                '%01%Address line 1 must be entered.%02%',
                                P_OUT_MSG,
                                HTF.br);
      END IF;

      IF /*goksels.*/f_clean_text(p_city) IS NULL THEN
        P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0044',
                                'SQL',
                                '%01%City must be entered.%02%',
                                P_OUT_MSG,
                                HTF.br);
      END IF;

      IF (p_stat IS NULL AND p_natn IS NULL) OR
         (/*goksels.*/f_clean_text(p_zip) IS NULL AND p_natn IS NULL) OR
         (p_stat IS NOT NULL AND /*goksels.*/f_clean_text(p_zip) IS NULL) THEN
        P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0045',
                                'SQL',
                                '%01%State and Zip or country must be entered.%02%',
                                P_OUT_MSG,
                                HTF.br);
      END IF;

      IF p_stat IS NOT NULL THEN
        OPEN stkstat.stvstatc(p_stat);
        FETCH stkstat.stvstatc
          INTO stvstat_rec;

        IF (stkstat.stvstatc%NOTFOUND) THEN
          P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0046',
                                  'SQL',
                                  '%01%Invalid State value.%02%',
                                  P_OUT_MSG,
                                  HTF.br);
        END IF;

        CLOSE stkstat.stvstatc;
      END IF;

      IF p_natn IS NOT NULL THEN
        OPEN stknatn.stvnatnc(p_natn);
        FETCH stknatn.stvnatnc
          INTO stvnatn_rec;

        IF (stknatn.stvnatnc%NOTFOUND) THEN
          P_OUT_MSG := G$_NLS.Get('BWGKOEM1-0047',
                                  'SQL',
                                  '%01%Invalid Country value.%02%',
                                  P_OUT_MSG,
                                  HTF.br);
        END IF;

        CLOSE stknatn.stvnatnc;
      END IF;
    END IF;

    --------------------------------------------------------------------------------------------

    IF P_OUT_MSG IS NOT NULL THEN
      RETURN; -- Any of above validations failed then return.
    END IF;

    IF NOT (fz_emrg_contactexists(p_pidm, p_oldpri)) THEN
      gb_emergency_contact.p_create(p_pidm            => p_pidm,
                                    p_priority        => p_oldpri,
                                    p_surname_prefix  => /*goksels.*/f_clean_text(p_lname_pfx),
                                    p_last_name       => /*goksels.*/f_clean_text(p_lname),
                                    p_first_name      => /*goksels.*/f_clean_text(p_fname),
                                    p_mi              => /*goksels.*/f_clean_text(p_mi),
                                    p_house_number    => /*goksels.*/f_clean_text(p_houseno),
                                    p_street_line1    => /*goksels.*/f_clean_text(p_addr1),
                                    p_street_line2    => /*goksels.*/f_clean_text(p_addr2),
                                    p_street_line3    => /*goksels.*/f_clean_text(p_addr3),
                                    p_street_line4    => /*goksels.*/f_clean_text(p_addr4),
                                    p_city            => /*goksels.*/f_clean_text(p_city),
                                    p_stat_code       => p_stat,
                                    p_natn_code       => p_natn,
                                    p_zip             => /*goksels.*/f_clean_text(p_zip),
                                    p_ctry_code_phone => p_ctry,
                                    p_phone_area      => p_area,
                                    p_phone_number    => p_phone,
                                    p_phone_ext       => p_ext,
                                    p_relt_code       => p_rship,
                                    p_atyp_code       => p_atyp,
                                    p_data_origin     => gb_common.DATA_ORIGIN,
                                    p_user_id         => twbklogn.f_get_banner_id(p_pidm),
                                    p_rowid_out       => spremrg_rowid);
      --
      gb_common.p_commit;
      --
      OPEN spremrgmaxpriorityc(p_pidm);
      FETCH spremrgmaxpriorityc
        INTO maxpriority;

      IF spremrgmaxpriorityc%NOTFOUND THEN
        maxpriority := 0;
      END IF;

      CLOSE spremrgmaxpriorityc;
      --
      pz_emrg_shufflepriority(p_pidm, p_priority_in, p_oldpri, maxpriority);
      --
    ELSE
      -- it exists!
      --
      --
      -- Update the 'oldpri' record.
      -- (If the priority was not changed by the user, then the
      --  priority and oldpri values will be the same.)
      --
      gb_emergency_contact.p_update(p_pidm            => p_pidm,
                                    p_priority        => p_oldpri,
                                    p_surname_prefix  => /*goksels.*/f_clean_text(p_lname_pfx),
                                    p_last_name       => /*goksels.*/f_clean_text(p_lname),
                                    p_first_name      => /*goksels.*/f_clean_text(p_fname),
                                    p_mi              => /*goksels.*/f_clean_text(p_mi),
                                    p_house_number    => /*goksels.*/f_clean_text(p_houseno),
                                    p_street_line1    => /*goksels.*/f_clean_text(p_addr1),
                                    p_street_line2    => /*goksels.*/f_clean_text(p_addr2),
                                    p_street_line3    => /*goksels.*/f_clean_text(p_addr3),
                                    p_street_line4    => /*goksels.*/f_clean_text(p_addr4),
                                    p_city            => /*goksels.*/f_clean_text(p_city),
                                    p_stat_code       => p_stat,
                                    p_natn_code       => p_natn,
                                    p_zip             => /*goksels.*/f_clean_text(p_zip),
                                    p_ctry_code_phone => p_ctry,
                                    p_phone_area      => p_area,
                                    p_phone_number    => p_phone,
                                    p_phone_ext       => p_ext,
                                    p_relt_code       => p_rship,
                                    p_data_origin     => gb_common.DATA_ORIGIN,
                                    p_user_id         => twbklogn.f_get_banner_id(p_pidm));
      --
      gb_common.p_commit;
      OPEN spremrgmaxpriorityc(p_pidm);
      FETCH spremrgmaxpriorityc
        INTO maxpriority;

      IF spremrgmaxpriorityc%NOTFOUND THEN
        maxpriority := 0;
      END IF;

      CLOSE spremrgmaxpriorityc;
      --
      pz_emrg_shufflepriority(p_pidm, p_priority_in, p_oldpri, maxpriority);
      --
    END IF;

  EXCEPTION
    WHEN OTHERS THEN

      IF spremrg_cv%ISOPEN THEN
        CLOSE spremrg_cv;
      END IF;
      IF spremrgmaxpriorityc%ISOPEN THEN
        CLOSE spremrgmaxpriorityc;
      END IF;
      IF spremrgcontactc%ISOPEN THEN
        CLOSE spremrgcontactc;
      END IF;

      gb_common.p_rollback;

      P_OUT_MSG := SQLERRM;

  END;

  PROCEDURE pz_process_address(p_id         spriden.spriden_id%type,
                               p_role       varchar2,
                               p_atyp       VARCHAR2,
                               p_seqno      NUMBER,
                               p_rel        VARCHAR2 default NULL, -- used for emergency contact, relationship
                               p_order      NUMBER default NULL, -- used for emergency contact, new priority, p_seqno will have old priority
                               p_deladdr    VARCHAR2,
                               p_fdate      VARCHAR2,
                               p_tdate      VARCHAR2,
                               p_lname_pfx  IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_lname      IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_fname      IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_mi         IN VARCHAR2 DEFAULT NULL, -- used for emergency contact
                               p_houseno    VARCHAR2,
                               p_str1       VARCHAR2,
                               p_str2       VARCHAR2,
                               p_str3       VARCHAR2,
                               p_str4       VARCHAR2,
                               p_city       VARCHAR2,
                               p_stat       VARCHAR2,
                               p_zip        VARCHAR2,
                               p_cnty       VARCHAR2,
                               p_natn       VARCHAR2,
                               p_ctry       VARCHAR2 DEFAULT NULL,
                               p_area       VARCHAR2 DEFAULT NULL,
                               p_num        VARCHAR2 DEFAULT NULL,
                               p_ext        VARCHAR2 DEFAULT NULL,
                               p_accs       VARCHAR2 DEFAULT NULL,
                               p_unl        VARCHAR2 DEFAULT NULL,
                               p_seqnos_lst VARCHAR2,
                               p_teles_lst  VARCHAR2,
                               p_ctrys_lst  VARCHAR2,
                               p_areas_lst  VARCHAR2,
                               p_phones_lst VARCHAR2,
                               p_exts_lst   VARCHAR2,
                               p_accss_lst  VARCHAR2,
                               p_unlist_lst VARCHAR2 DEFAULT NULL,
                               p_delete_lst VARCHAR2 DEFAULT NULL,
                               P_OUT_MSG    OUT VARCHAR2) IS
    lv_seqno NUMBER;

    v_seqnos      twbklibs.varchar2_tabtype;
    v_teles       twbklibs.varchar2_tabtype;
    v_ctrys       twbklibs.varchar2_tabtype;
    v_areas       twbklibs.varchar2_tabtype;
    v_phones      twbklibs.varchar2_tabtype;
    v_exts        twbklibs.varchar2_tabtype;
    v_accss       twbklibs.varchar2_tabtype;
    v_phone_unlst twbklibs.varchar2_tabtype;
    v_phone_del   twbklibs.varchar2_tabtype;

    v_idx  number;
    v_pidm spriden.spriden_pidm%type;

  BEGIN

    BEGIN
      v_pidm := GB_COMMON.F_GET_PIDM(P_ID);
    EXCEPTION
      WHEN OTHERS THEN
        P_OUT_MSG := 'Invalid Banner ID';
        RETURN;
    END;

    /* Globally set role */
    ----------------------------------

    CASE
      WHEN NVL(P_ROLE, 'STUDENT') = 'STUDENT' THEN
        TWBKSLIB.role_rec.role_student_ind := 'Y';
      WHEN P_ROLE = 'FACULTY' THEN
        TWBKSLIB.role_rec.role_faculty_ind  := 'Y';
        TWBKSLIB.role_rec.role_employee_ind := 'Y';
      WHEN P_ROLE = 'EMPLOYEE' THEN
        TWBKSLIB.role_rec.role_employee_ind := 'Y';
        TWBKSLIB.role_rec.role_faculty_ind  := 'Y';
      WHEN P_ROLE = 'ALMUNI' THEN
        TWBKSLIB.role_rec.role_alumni_ind := 'Y';
      WHEN P_ROLE = 'FINANCE' THEN
        TWBKSLIB.role_rec.role_finance_ind := 'Y';
      WHEN P_ROLE = 'EXECADMIN' THEN
        TWBKSLIB.role_rec.role_exec_admin_ind := 'Y';
      WHEN P_ROLE = 'EXECUTIVE' THEN
        TWBKSLIB.role_rec.role_executive_ind := 'Y';
      WHEN P_ROLE = 'FRIEND' THEN
        TWBKSLIB.role_rec.role_friend_ind := 'Y';
      WHEN P_ROLE = 'FAID' THEN
        TWBKSLIB.role_rec.role_finaid_ind := 'Y';
      WHEN P_ROLE = 'BSAC' THEN
        TWBKSLIB.role_rec.role_bsac_ind := 'Y';
      ELSE
        NULL;
    END CASE;

    -----------------------------------

    DBMS_OUTPUT.PUT_LINE('Converting list to table- start');

    ------------------------------------------------------------
    /*Seq number table */
    pz_convert_commastr_to_table(p_seqnos_lst, v_seqnos);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Tele codes table */
    pz_convert_commastr_to_table(p_teles_lst, v_teles);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Country codes table */
    pz_convert_commastr_to_table(p_ctrys_lst, v_ctrys);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Area codes table */
    pz_convert_commastr_to_table(p_areas_lst, v_areas);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Phone number table */
    pz_convert_commastr_to_table(p_phones_lst, v_phones);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Phone extns table */
    pz_convert_commastr_to_table(p_exts_lst, v_exts);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Phone intl access table */
    pz_convert_commastr_to_table(p_accss_lst, v_accss);
    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Phone unlist  table */
    pz_convert_commastr_to_table(p_unlist_lst, v_phone_unlst);

    ---------------------------------------------------------------

    ------------------------------------------------------------
    /*Phone delete table */
    pz_convert_commastr_to_table(p_delete_lst, v_phone_del);
    ---------------------------------------------------------------

    DBMS_OUTPUT.PUT_LINE('Converting list to table- complete');

    lv_seqno := p_seqno;

    ----------------------------------------------------------------
    --Insert/Update Cell

    IF p_atyp = 'CL' THEN

      /* There will be only one Cell-phone */

      DBMS_OUTPUT.PUT_LINE('Cellphone processing');

      pz_process_tele(v_pidm,
                      p_atyp,
                      lv_seqno, /*v_seqnos(1)*/
                      p_area, --v_areas(1),
                      p_num, --v_phones(1),
                      p_ext, --v_exts(1),
                      p_accs, --v_accss(1),
                      REPLACE(p_unl, 'N', ''), --v_phone_unlst(1),
                      p_deladdr, --v_phone_del(1),
                      P_OUT_MSG);

      IF P_OUT_MSG IS NULL THEN
        P_OUT_MSG := 'SUCCESS';
      END IF;

      IF P_OUT_MSG IS NULL THEN
        P_OUT_MSG := 'SUCCESS';
        GB_COMMON.p_commit;
      ELSE
        GB_COMMON.p_rollback;
      END IF;

      RETURN;

    END IF;

    ---------------------------------------------------------------

    ---------------------------------------------------------------
    --Insert/Update Emergency Contact

    IF p_atyp = 'EMERG' THEN

      DBMS_OUTPUT.PUT_LINE('Emergency processing');

      pz_process_emrgcontacts(v_pidm,
                              p_order, -- new priority
                              p_rel,
                              p_lname_pfx,
                              p_lname,
                              p_fname,
                              p_mi,
                              '', --address is not required
                              p_houseno,
                              p_str1,
                              p_str2,
                              p_str3,
                              p_str4,
                              p_city,
                              p_stat,
                              p_natn,
                              p_zip,
                              p_ctry,
                              p_area,
                              p_num,
                              p_ext,
                              p_deladdr,
                              lv_seqno, -- old priority
                              NULL, /*p_last_active IN VARCHAR2 DEFAULT*/ --check this?
                              P_OUT_MSG);

      DBMS_OUTPUT.PUT_LINE('Emergency Complete:' || P_OUT_MSG);

      IF P_OUT_MSG IS NULL THEN
        P_OUT_MSG := 'SUCCESS';
        GB_COMMON.p_commit;
      ELSE
        GB_COMMON.p_rollback;
      END IF;

      RETURN;

    END IF;

    -----------------------------------------------------------------

    pz_procaddrupdate(v_pidm,
                      p_atyp,
                      lv_seqno,
                      p_deladdr,
                      p_fdate,
                      p_tdate,
                      p_houseno,
                      p_str1,
                      p_str2,
                      p_str3,
                      p_str4,
                      p_city,
                      p_stat,
                      p_zip,
                      p_cnty,
                      p_natn,
                      p_ctry,
                      p_area,
                      p_num,
                      p_ext,
                      p_accs,
                      REPLACE(p_unl, 'N', ''),
                      v_seqnos,
                      v_teles,
                      v_ctrys,
                      v_areas,
                      v_phones,
                      v_exts,
                      v_accss,
                      REPLACE(v_phone_unlst(1), 'N', ''),
                      REPLACE(v_phone_unlst(2), 'N', ''),
                      REPLACE(v_phone_unlst(3), 'N', ''),
                      REPLACE(v_phone_unlst(4), 'N', ''),
                      REPLACE(v_phone_unlst(5), 'N', ''),
                      REPLACE(v_phone_del(1), 'N', ''),
                      REPLACE(v_phone_del(2), 'N', ''),
                      REPLACE(v_phone_del(3), 'N', ''),
                      REPLACE(v_phone_del(4), 'N', ''),
                      REPLACE(v_phone_del(5), 'N', ''),
                      P_OUT_MSG);

    IF P_OUT_MSG IS NULL THEN
      gb_common.p_commit;
      P_OUT_MSG := 'SUCCESS';
    ELSE
      gb_common.p_rollback;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      gb_common.p_rollback;
      P_OUT_MSG := 'Server Error. Please retry!';
  END pz_process_address;

  PROCEDURE pz_confirm_biographic(P_ID      SPRIDEN.SPRIDEN_ID%TYPE,
                                  p_indtype VARCHAR2,
                                  P_OUT_MSG OUT VARCHAR2) IS

    v_pidm      SPRIDEN.SPRIDEN_PIDM%TYPE;
    v_term      szbgtrm.szbgtrm_term_code%type;
    szbgtrm_rec szbgtrm%rowtype;
    szbgflg_rec szbgflg%rowtype;

  BEGIN

    P_OUT_MSG := 'SUCCESS';

    open szbgtrm_c;
    fetch szbgtrm_c
      into szbgtrm_rec;
    if szbgtrm_c%notfound then
      v_term := null;
    else
      v_term := szbgtrm_rec.szbgtrm_term_code;
    end if;
    close szbgtrm_c;

    BEGIN
      v_pidm := GB_COMMON.F_GET_PIDM(P_ID);
    EXCEPTION
      WHEN OTHERS THEN
        P_OUT_MSG := 'Invalid Banner ID';
    END;

    open szbgflg_c(v_pidm, v_term);
    fetch szbgflg_c
      into szbgflg_rec;
    if szbgflg_c%notfound then
      select v_pidm,
             v_term,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             sysdate,
             user
        into szbgflg_rec
        from dual;
      insert into szbgflg values szbgflg_rec;
      commit;
    end if;
    close szbgflg_c;

    if v_term is not null then
      if p_indtype = 'PR' then
        szbgflg_rec.szbgflg_addr_pr_ind := 'Y';
      elsif p_indtype = 'OF' then
        szbgflg_rec.szbgflg_addr_of_ind := 'Y';
      elsif p_indtype = 'PA' then
        szbgflg_rec.szbgflg_addr_pa_ind := 'Y';
      elsif p_indtype = 'P1' then
        szbgflg_rec.szbgflg_addr_p1_ind := 'Y';
      elsif p_indtype = 'P2' then
        szbgflg_rec.szbgflg_addr_p2_ind := 'Y';
      elsif p_indtype = 'CL' then
        szbgflg_rec.szbgflg_tele_cl_ind := 'Y';
      elsif p_indtype = 'EMERG' then
        szbgflg_rec.szbgflg_emrg_ind := 'Y';
      end if;
      szbgflg_rec.szbgflg_activity_date := sysdate;
      szbgflg_rec.szbgflg_user_id       := user;
      update szbgflg
         set row = szbgflg_rec
       where szbgflg_pidm = v_pidm
         and szbgflg_term_code = v_term;
      commit;
    end if;

  exception
    when others then

      P_OUT_MSG := 'Failed to confirm, try again!';

  END;

  PROCEDURE pz_get_biographic_info(P_ID      SPRIDEN.SPRIDEN_ID%TYPE,
                                   P_ROLE    VARCHAR2 DEFAULT 'STUDENT',
                                   P_XML_BIO OUT CLOB) IS

    bio_xml_data XMLTYPE;
    l_refcursor  SYS_REFCURSOR;
    v_pidm       SPRIDEN.SPRIDEN_PIDM%TYPE;
    v_name       varchar2(100);
    v_error      varchar2(3000);
    v_term       szbgtrm.szbgtrm_term_code%type;
    v_status     varchar2(20);
    szbgtrm_rec  szbgtrm%rowtype;
    szbgflg_rec  szbgflg%rowtype;
    spraddr_rec  spraddr%rowtype;
    sprtele_rec  sprtele%rowtype;

    spremrg_rec spremrg%rowtype;
    stvstat_rec stvstat%rowtype;

    stvnatn_rec stvnatn%rowtype;
    stvrelt_rec stvrelt%rowtype;

    v_order number := 0;

  BEGIN

    BEGIN
      v_pidm := GB_COMMON.F_GET_PIDM(P_ID);
      v_name := f_format_name(v_pidm, 'FMIL');
    EXCEPTION
      WHEN OTHERS THEN
        v_error := 'Invalid Banner ID';
    END;

    open szbgtrm_c;
    fetch szbgtrm_c
      into szbgtrm_rec;
    if szbgtrm_c%notfound then
      v_term := null;
    else
      v_term := szbgtrm_rec.szbgtrm_term_code;
    end if;
    close szbgtrm_c;

    /* Check if student has confirmed Bio-graphic */

    if v_term is not null then
      open szbgflg_c(v_pidm, v_term);
      fetch szbgflg_c
        into szbgflg_rec;
      if szbgflg_c%notfound then
        select v_pidm,
               v_term,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               sysdate,
               user
          into szbgflg_rec
          from dual;
        insert into szbgflg values szbgflg_rec;
        commit;
      end if;
      close szbgflg_c;
    end if;

    OPEN l_refcursor FOR
      SELECT P_ID AS BANNER_ID,
             v_name AS NAME,
             ME_BIOGRAPHIC.FZ_GET_EMAIL_ADDRESS(v_pidm, NULL, 'Y') PREFERRED_EMAIL,
             SYSDATE AS AS_ON_DATE
        FROM DUAL;
    bio_xml_data := XMLTYPE(l_refcursor);
    close l_refcursor;

    --Get Cellular phone record
    sprtele_rec := null;
    open sprtele_type_c(v_pidm, 'CL');
    fetch sprtele_type_c
      into sprtele_rec;
    close sprtele_type_c;

    if szbgflg_rec.szbgflg_tele_cl_ind is null then
      v_status := 'Confirm';
    else
      v_status := 'Confirmed';
    end if;

    -- Since address update doesn't exist for Celluar removing this
    /* SELECT INSERTCHILDXML(bio_xml_data,
                        '/ROWSET',
                        'RECORD',
                        XMLElement("RECORD",
                                   XMLElement("ADDRESS_CODE", ADDRESS_CODE),
                                   XMLElement("ADDRESS_DESCRIPTION",
                                              ADDRESS_DESCRIPTION),
                                   XMLElement("RECORD_TYPE", RECORD_TYPE),
                                   XMLElement("SEQ_NO", SEQ_NO),
                                   XMLElement("ACTION", ACTION),
                                   XMLElement("STATUS", STATUS),
                                   XMLElement("FROM_DATE", FROM_DATE),
                                   XMLElement("TO_DATE", TO_DATE),
                                   XMLElement("STREET_LINE1", STREET_LINE1),
                                   XMLElement("STREET_LINE2", STREET_LINE2),
                                   XMLElement("STREET_LINE3", STREET_LINE3),
                                   XMLElement("CITY", CITY),
                                   XMLElement("STATE_CODE", STATE_CODE),
                                   XMLElement("STATE_DESC", STATE_DESC),
                                   XMLElement("ZIP_CODE", ZIP_CODE),
                                   XMLElement("COUNTY_CODE", COUNTY_CODE),
                                   XMLElement("COUNTY_DESC", COUNTY_DESC),
                                   XMLElement("NATION_CODE", NATION_CODE),
                                   XMLElement("NATION_DESC", NATION_DESC),
                                   XMLElement("PRIMARY_PHONE_AREA",
                                              PRIMARY_PHONE_AREA),
                                   XMLElement("PRIMARY_PHONE_NUM",
                                              PRIMARY_PHONE_NUM),
                                   XMLElement("PRIMARY_PHONE_INTL_ACCESS",
                                              PRIMARY_PHONE_INTL_ACCESS),
                                   XMLElement("PRIMARY_UNLIST_IND",
                                              PRIMARY_UNLIST_IND)))
    into bio_xml_data
    FROM (select 'CL' ADDRESS_CODE,
                 'CELLULAR' ADDRESS_DESCRIPTION,
                 'ADDRESS' RECORD_TYPE,
                 sprtele_rec.sprtele_seqno SEQ_NO,
                 'UPDATE' ACTION,
                 v_status STATUS,
                 '' FROM_DATE,
                 '' TO_DATE,
                 '' STREET_LINE1,
                 '' STREET_LINE2,
                 '' STREET_LINE3,
                 '' CITY,
                 '' STATE_CODE,
                 '' STATE_DESC,
                 '' ZIP_CODE,
                 '' COUNTY_CODE,
                 '' COUNTY_DESC,
                 '' NATION_CODE,
                 '' NATION_DESC,
                 '' PRIMARY_PHONE_AREA,
                 '' PRIMARY_PHONE_NUM,
                 '' PRIMARY_PHONE_INTL_ACCESS,
                 '' PRIMARY_UNLIST_IND
            FROM DUAL) XD;*/

    SELECT INSERTCHILDXML(bio_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("ADDRESS_CODE", ADDRESS_CODE),
                                     XMLElement("ADDRESS_DESCRIPTION",
                                                ADDRESS_DESCRIPTION),
                                     XMLElement("RECORD_TYPE", RECORD_TYPE),
                                     XMLElement("SEQ_NO", SEQ_NO),
                                     XMLElement("ACTION", ACTION),
                                     XMLElement("STATUS", STATUS),
                                     XMLElement("FROM_DATE", FROM_DATE),
                                     XMLElement("TELE_CODE", TELE_CODE),
                                     XMLElement("TELE_DESC", TELE_DESC),
                                     XMLElement("PHONE_AREA", PHONE_AREA),
                                     XMLElement("PHONE_NUM", PHONE_NUM),
                                     XMLElement("PHONE_INTL_ACCESS",
                                                PHONE_INTL_ACCESS),
                                     XMLElement("UNLIST_IND", UNLIST_IND)))
      into bio_xml_data
      FROM (select 'CL' ADDRESS_CODE,
                   'CELLULAR' ADDRESS_DESCRIPTION,
                   'CELL PHONE' RECORD_TYPE,
                   sprtele_rec.sprtele_seqno SEQ_NO,
                   'UPDATE' ACTION,
                   v_status STATUS,
                   sprtele_rec.sprtele_activity_date FROM_DATE,
                   sprtele_rec.sprtele_tele_code TELE_CODE,
                   me_reg_utils.fz_get_desc(sprtele_rec.sprtele_tele_code,
                                            'STVTELE',
                                            'STVTELE_DESC',
                                            'N',
                                            NULL,
                                            NULL,
                                            NULL,
                                            NULL) TELE_DESC,
                   sprtele_rec.sprtele_phone_area PHONE_AREA,
                   sprtele_rec.sprtele_phone_number PHONE_NUM,
                   sprtele_rec.sprtele_intl_access PHONE_INTL_ACCESS,
                   sprtele_rec.sprtele_unlist_ind UNLIST_IND
              FROM DUAL) XD;

    --Get address types to show

    FOR R_ATYP IN GORADRL_C(P_ROLE) LOOP
      /* Rest of the code goes here */
      spraddr_rec := NULL;
      sprtele_rec := NULL;

      if v_term is null then
        v_status := 'No Confirmation Needed';
      elsif (((R_ATYP.GORADRL_ATYP_CODE = 'PR') and
            (szbgflg_rec.szbgflg_addr_pr_ind is null)) or
            ((R_ATYP.GORADRL_ATYP_CODE = 'OF') and
            (szbgflg_rec.szbgflg_addr_of_ind is null)) or
            ((R_ATYP.GORADRL_ATYP_CODE = 'PA') and
            (szbgflg_rec.szbgflg_addr_pa_ind is null)) or
            ((R_ATYP.GORADRL_ATYP_CODE = 'P1') and
            (szbgflg_rec.szbgflg_addr_p1_ind is null)) or
            ((R_ATYP.GORADRL_ATYP_CODE = 'P2') and
            (szbgflg_rec.szbgflg_addr_p2_ind is null))) then
        v_status := 'Confirm';
      else
        v_status := 'Confirmed';
      end if;

      open spraddr_c(v_pidm, R_ATYP.GORADRL_ATYP_CODE);
      fetch spraddr_c
        into spraddr_rec;
      close spraddr_c;

      open sprtele_primary_c(v_pidm,
                             spraddr_rec.spraddr_atyp_code,
                             spraddr_rec.spraddr_seqno);
      fetch sprtele_primary_c
        into sprtele_rec;
      close sprtele_primary_c;

      SELECT INSERTCHILDXML(bio_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("ADDRESS_CODE",
                                                  ADDRESS_CODE),
                                       XMLElement("ADDRESS_DESCRIPTION",
                                                  ADDRESS_DESCRIPTION),
                                       XMLElement("RECORD_TYPE", RECORD_TYPE),
                                       XMLElement("SEQ_NO", SEQ_NO),
                                       XMLElement("ACTION", ACTION),
                                       XMLElement("STATUS", STATUS),
                                       XMLElement("FROM_DATE", FROM_DATE),
                                       XMLElement("TO_DATE", TO_DATE),
                                       XMLElement("STREET_LINE1",
                                                  STREET_LINE1),
                                       XMLElement("STREET_LINE2",
                                                  STREET_LINE2),
                                       XMLElement("STREET_LINE3",
                                                  STREET_LINE3),
                                       XMLElement("CITY", CITY),
                                       XMLElement("STATE_CODE", STATE_CODE),
                                       XMLElement("STATE_DESC", STATE_DESC),
                                       XMLElement("ZIP_CODE", ZIP_CODE),
                                       XMLElement("COUNTY_CODE", COUNTY_CODE),
                                       XMLElement("COUNTY_DESC", COUNTY_DESC),
                                       XMLElement("NATION_CODE", NATION_CODE),
                                       XMLElement("NATION_DESC", NATION_DESC),
                                       XMLElement("PRIMARY_PHONE_AREA",
                                                  PRIMARY_PHONE_AREA),
                                       XMLElement("PRIMARY_PHONE_NUM",
                                                  PRIMARY_PHONE_NUM),
                                       XMLElement("PRIMARY_PHONE_INTL_ACCESS",
                                                  PRIMARY_PHONE_INTL_ACCESS),
                                       XMLElement("PRIMARY_UNLIST_IND",
                                                  PRIMARY_UNLIST_IND)))
        into bio_xml_data
        FROM (select R_ATYP.GORADRL_ATYP_CODE ADDRESS_CODE,
                     R_ATYP.ATYP_DESC ADDRESS_DESCRIPTION,
                     'ADDRESS' RECORD_TYPE,
                     NVL(spraddr_rec.spraddr_seqno, 0) SEQ_NO,
                     DECODE(R_ATYP.GORADRL_PRIV_IND, 'U', 'UPDATE', '') ACTION,
                     v_status STATUS,
                     spraddr_rec.spraddr_from_date FROM_DATE,
                     spraddr_rec.spraddr_to_date TO_DATE,
                     spraddr_rec.spraddr_street_line1 STREET_LINE1,
                     spraddr_rec.spraddr_street_line2 STREET_LINE2,
                     spraddr_rec.spraddr_street_line3 STREET_LINE3,
                     spraddr_rec.spraddr_city CITY,
                     spraddr_rec.spraddr_stat_code STATE_CODE,
                     me_reg_utils.fz_get_desc(spraddr_rec.spraddr_stat_code,
                                              'STVSTAT',
                                              'STVSTAT_DESC',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) STATE_DESC,
                     spraddr_rec.spraddr_zip ZIP_CODE,
                     spraddr_rec.spraddr_cnty_code COUNTY_CODE,
                     me_reg_utils.fz_get_desc(spraddr_rec.spraddr_cnty_code,
                                              'STVCNTY',
                                              'STVCNTY_DESC',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) COUNTY_DESC,
                     spraddr_rec.spraddr_natn_code NATION_CODE,
                     me_reg_utils.fz_get_desc(spraddr_rec.spraddr_natn_code,
                                              'STVNATN',
                                              'STVNATN_NATION',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) NATION_DESC,
                     sprtele_rec.sprtele_phone_area PRIMARY_PHONE_AREA,
                     sprtele_rec.sprtele_phone_number PRIMARY_PHONE_NUM,
                     sprtele_rec.sprtele_intl_access PRIMARY_PHONE_INTL_ACCESS,
                     sprtele_rec.sprtele_unlist_ind PRIMARY_UNLIST_IND
                FROM DUAL) XD;

      sprtele_rec := NULL;
      open sprtele_nonprimary_c(v_pidm,
                                spraddr_rec.spraddr_atyp_code,
                                spraddr_rec.spraddr_seqno);
      loop
        fetch sprtele_nonprimary_c
          into sprtele_rec;
        exit when sprtele_nonprimary_c%notfound;

        -- Get other telephone records

        SELECT INSERTCHILDXML(bio_xml_data,
                              '/ROWSET',
                              'RECORD',
                              XMLElement("RECORD",
                                         XMLElement("ADDRESS_CODE",
                                                    ADDRESS_CODE),
                                         XMLElement("ADDRESS_DESCRIPTION",
                                                    ADDRESS_DESCRIPTION),
                                         XMLElement("RECORD_TYPE",
                                                    RECORD_TYPE),
                                         XMLElement("SEQ_NO", SEQ_NO),
                                         XMLElement("TELE_CODE", TELE_CODE),
                                         XMLElement("TELE_DESC", TELE_DESC),
                                         XMLElement("PHONE_AREA", PHONE_AREA),
                                         XMLElement("PHONE_NUM", PHONE_NUM),
                                         XMLElement("PHONE_INTL_ACCESS",
                                                    PHONE_INTL_ACCESS),
                                         XMLElement("UNLIST_IND", UNLIST_IND)))
          into bio_xml_data
          FROM (select R_ATYP.GORADRL_ATYP_CODE ADDRESS_CODE,
                       R_ATYP.ATYP_DESC ADDRESS_DESCRIPTION,
                       'OTHER_PHONE' RECORD_TYPE,
                       sprtele_rec.sprtele_seqno SEQ_NO,
                       sprtele_rec.sprtele_tele_code TELE_CODE,
                       me_reg_utils.fz_get_desc(sprtele_rec.sprtele_tele_code,
                                                'STVTELE',
                                                'STVTELE_DESC',
                                                'N',
                                                NULL,
                                                NULL,
                                                NULL,
                                                NULL) TELE_DESC,
                       sprtele_rec.sprtele_phone_area PHONE_AREA,
                       sprtele_rec.sprtele_phone_number PHONE_NUM,
                       sprtele_rec.sprtele_intl_access PHONE_INTL_ACCESS,
                       sprtele_rec.sprtele_unlist_ind UNLIST_IND
                  FROM DUAL) XD;

      end loop;
      close sprtele_nonprimary_c;

    END LOOP;

    --Get Emergency contact details

    open spremrg_c(v_pidm);
    loop
      fetch spremrg_c
        into spremrg_rec;
      exit when spremrg_c%notfound;

      if szbgflg_rec.szbgflg_emrg_ind is null then
        v_status := 'Confirm';
      else
        v_status := 'Confirmed';
      end if;

      if v_order < spremrg_rec.spremrg_priority then

        v_order := spremrg_rec.spremrg_priority;

      end if;

      SELECT INSERTCHILDXML(bio_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("ADDRESS_CODE",
                                                  ADDRESS_CODE),
                                       XMLElement("ADDRESS_DESCRIPTION",
                                                  ADDRESS_DESCRIPTION),
                                       XMLElement("RECORD_TYPE", RECORD_TYPE),
                                       XMLElement("SEQ_NO", SEQ_NO),
                                       XMLElement("ACTION", ACTION),
                                       XMLElement("STATUS", STATUS),
                                       XMLElement("FIRST_NAME", FIRST_NAME),
                                       XMLElement("MIDDLE_NAME", MIDDLE_NAME),
                                       XMLElement("LAST_NAME", LAST_NAME),
                                       XMLElement("RELATION_CODE",
                                                  RELATION_CODE),
                                       XMLElement("RELATION_DESC",
                                                  RELATION_DESC),
                                       XMLElement("RELATION_ORDER",
                                                  RELATION_ORDER),
                                       XMLElement("STREET_LINE1",
                                                  STREET_LINE1),
                                       XMLElement("STREET_LINE2",
                                                  STREET_LINE2),
                                       XMLElement("STREET_LINE3",
                                                  STREET_LINE3),
                                       XMLElement("CITY", CITY),
                                       XMLElement("STATE_CODE", STATE_CODE),
                                       XMLElement("STATE_DESC", STATE_DESC),
                                       XMLElement("ZIP_CODE", ZIP_CODE),
                                       XMLElement("COUNTY_CODE", COUNTY_CODE),
                                       XMLElement("COUNTY_DESC", COUNTY_DESC),
                                       XMLElement("NATION_CODE", NATION_CODE),
                                       XMLElement("NATION_DESC", NATION_DESC),
                                       XMLElement("PHONE_AREA", PHONE_AREA),
                                       XMLElement("PHONE_NUM", PHONE_NUM),
                                       XMLElement("PHONE_EXTN", PHONE_EXTN)

                                       ))
        into bio_xml_data
        FROM (select 'EMERG' ADDRESS_CODE,
                     'Emergency Contacts' ADDRESS_DESCRIPTION,
                     'EMERGENCY CONTACT' RECORD_TYPE,
                     spremrg_rec.spremrg_priority SEQ_NO,
                     'UPDATE' ACTION,
                     v_status STATUS,
                     spremrg_rec.spremrg_first_name FIRST_NAME,
                     spremrg_rec.spremrg_mi MIDDLE_NAME,
                     spremrg_rec.spremrg_last_name LAST_NAME,
                     spremrg_rec.spremrg_relt_code RELATION_CODE,
                     me_reg_utils.fz_get_desc(spremrg_rec.spremrg_relt_code,
                                              'STVRELT',
                                              'STVRELT_DESC',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) RELATION_DESC,
                     spremrg_rec.spremrg_priority RELATION_ORDER,
                     spremrg_rec.spremrg_street_line1 STREET_LINE1,
                     spremrg_rec.spremrg_street_line2 STREET_LINE2,
                     spremrg_rec.spremrg_street_line3 STREET_LINE3,
                     spremrg_rec.spremrg_city CITY,
                     spremrg_rec.spremrg_stat_code STATE_CODE,
                     me_reg_utils.fz_get_desc(spremrg_rec.spremrg_stat_code,
                                              'STVSTAT',
                                              'STVSTAT_DESC',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) STATE_DESC,
                     spremrg_rec.spremrg_zip ZIP_CODE,
                     spremrg_rec.spremrg_cnty_code COUNTY_CODE,
                     me_reg_utils.fz_get_desc(spremrg_rec.spremrg_cnty_code,
                                              'STVCNTY',
                                              'STVCNTY_DESC',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) COUNTY_DESC,
                     spremrg_rec.spremrg_natn_code NATION_CODE,
                     me_reg_utils.fz_get_desc(spremrg_rec.spremrg_natn_code,
                                              'STVNATN',
                                              'STVNATN_NATION',
                                              'N',
                                              NULL,
                                              NULL,
                                              NULL,
                                              NULL) NATION_DESC,
                     spremrg_rec.spremrg_phone_area PHONE_AREA,
                     spremrg_rec.spremrg_phone_number PHONE_NUM,
                     spremrg_rec.spremrg_phone_ext PHONE_EXTN

                FROM DUAL) XD;

    end loop;

    --A dummy Emergency contact is sent so that user will have option to update this, which is like entering a new record

    if szbgflg_rec.szbgflg_emrg_ind is null then
      v_status := 'Confirm';
    else
      v_status := 'Confirmed';
    end if;

    SELECT INSERTCHILDXML(bio_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("ADDRESS_CODE", ADDRESS_CODE),
                                     XMLElement("ADDRESS_DESCRIPTION",
                                                ADDRESS_DESCRIPTION),
                                     XMLElement("RECORD_TYPE", RECORD_TYPE),
                                     XMLElement("SEQ_NO", SEQ_NO),
                                     XMLElement("ACTION", ACTION),
                                     XMLElement("STATUS", STATUS),
                                     XMLElement("FIRST_NAME", FIRST_NAME),
                                     XMLElement("MIDDLE_NAME", MIDDLE_NAME),
                                     XMLElement("LAST_NAME", LAST_NAME),
                                     XMLElement("RELATION_CODE",
                                                RELATION_CODE),
                                     XMLElement("RELATION_DESC",
                                                RELATION_DESC),
                                     XMLElement("RELATION_ORDER",
                                                RELATION_ORDER),
                                     XMLElement("STREET_LINE1", STREET_LINE1),
                                     XMLElement("STREET_LINE2", STREET_LINE2),
                                     XMLElement("STREET_LINE3", STREET_LINE3),
                                     XMLElement("CITY", CITY),
                                     XMLElement("STATE_CODE", STATE_CODE),
                                     XMLElement("STATE_DESC", STATE_DESC),
                                     XMLElement("ZIP_CODE", ZIP_CODE),
                                     XMLElement("COUNTY_CODE", COUNTY_CODE),
                                     XMLElement("COUNTY_DESC", COUNTY_DESC),
                                     XMLElement("NATION_CODE", NATION_CODE),
                                     XMLElement("NATION_DESC", NATION_DESC),
                                     XMLElement("PHONE_AREA", PHONE_AREA),
                                     XMLElement("PHONE_NUM", PHONE_NUM),
                                     XMLElement("PHONE_EXTN", PHONE_EXTN)

                                     ))
      into bio_xml_data
      FROM (select 'EMERG' ADDRESS_CODE,
                   'Emergency Contacts' ADDRESS_DESCRIPTION,
                   'EMERGENCY CONTACT' RECORD_TYPE,
                   v_order + 1 SEQ_NO,
                   'UPDATE' ACTION,
                   v_status STATUS,
                   'NEW CONTACT' FIRST_NAME,
                   '' MIDDLE_NAME,
                   '' LAST_NAME,
                   '' RELATION_CODE,
                   '' RELATION_DESC,
                   v_order + 1 RELATION_ORDER,
                   '' STREET_LINE1,
                   '' STREET_LINE2,
                   '' STREET_LINE3,
                   '' CITY,
                   '' STATE_CODE,
                   '' STATE_DESC,
                   '' ZIP_CODE,
                   '' COUNTY_CODE,
                   '' COUNTY_DESC,
                   '' NATION_CODE,
                   '' NATION_DESC,
                   '' PHONE_AREA,
                   '' PHONE_NUM,
                   '' PHONE_EXTN
              FROM DUAL) XD;

    close spremrg_c;

    P_XML_BIO := bio_xml_data.getClobVal;

  END PZ_GET_BIOGRAPHIC_INFO;

begin
  -- Initialization
  NULL;

end ME_BIOGRAPHIC;
/

