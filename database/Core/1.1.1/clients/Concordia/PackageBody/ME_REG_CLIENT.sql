PROMPT CREATE OR REPLACE PACKAGE BODY ME_REG_CLIENT....

create or replace package body ME_REG_CLIENT is

  function fz_get_linked_courses(P_CRN       ssrlink.ssrlink_crn%type,
                                 P_TERM_CODE ssrlink.ssrlink_term_code%type)
  
   RETURN varchar2 as
  
    v_linked_courses varchar2(4000);
  
    cursor get_linked_courses is
      SELECT T.*
        FROM SSBSECT T
       WHERE T.SSBSECT_TERM_CODE = P_TERM_CODE
         AND T.SSBSECT_SUBJ_CODE =
             (SELECT SSBSECT_SUBJ_CODE
                FROM SSBSECT E
               WHERE E.SSBSECT_CRN = P_CRN
                 AND E.SSBSECT_TERM_CODE = P_TERM_CODE)
         AND T.SSBSECT_CRSE_NUMB =
             (SELECT SSBSECT_CRSE_NUMB
                FROM SSBSECT E
               WHERE E.SSBSECT_CRN = P_CRN
                 AND E.SSBSECT_TERM_CODE = P_TERM_CODE)
         AND SSBSECT_LINK_IDENT =
             (SELECT SSRLINK_LINK_CONN
                FROM SSRLINK
               WHERE SSRLINK_CRN = P_CRN
                 AND SSRLINK_TERM_CODE = P_TERM_CODE);
  
  BEGIN
  
    FOR R_DATA IN get_linked_courses LOOP
    
      v_linked_courses := R_DATA.SSBSECT_CRN || ',' || v_linked_courses;
    
    END LOOP;
  
    v_linked_courses := RTRIM(v_linked_courses, ',');
  
    RETURN v_linked_courses || '-' || me_account_objects.fz_get_detail_link('LINK');
  
  END;

  FUNCTION fz_check_biographic_info(P_STUDENT_ID SPRIDEN.SPRIDEN_ID%TYPE)
    RETURN CHAR IS
  
    v_bio_info char(1) := 'N';
    v_count    number := 0;
    v_pidm     number;
  
  BEGIN
  
    begin
    
      v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);
    
    exception
      when others then
      
        v_pidm := -100;
      
    end;
  
    select count(1)
      into v_count
      from SZBGFLG
     where SZBGFLG_PIDM = v_pidm
       and (SZBGFLG_ADDR_PR_IND = 'Y' and SZBGFLG_ADDR_OF_IND = 'Y' and
           SZBGFLG_ADDR_PA_IND = 'Y' and SZBGFLG_ADDR_P1_IND = 'Y' and
           SZBGFLG_ADDR_P2_IND = 'Y' and SZBGFLG_TELE_CL_IND = 'Y' and
           SZBGFLG_EMRG_IND = 'Y');
  
    IF V_COUNT > 0 THEN
    
      v_bio_info := 'Y';
    ELSE
      v_bio_info := 'N';
    
    END IF;
  
    RETURN v_bio_info;
  
  exception
    when others then
    
      RETURN v_bio_info;
    
  END;

  FUNCTION fz_check_grad_term(V_PIDM sgbstdn.sgbstdn_pidm%type,
                              P_TERM stvterm.stvterm_code%type) RETURN CHAR IS
  
    v_grad_date char(1) := 'N';
    v_exist     number := 0;
  
  BEGIN
  
    select count(1)
      into v_exist
      from sgbstdn
     where sgbstdn_pidm = V_PIDM
       and sgbstdn_term_code_grad >= P_TERM;
  
    IF v_exist > 0 THEN
      v_grad_date := 'Y';
    ELSE
      v_grad_date := 'N';
    END IF;
  
    RETURN v_grad_date;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN v_grad_date;
  END;

begin

  NULL;

end ME_REG_CLIENT;
/
