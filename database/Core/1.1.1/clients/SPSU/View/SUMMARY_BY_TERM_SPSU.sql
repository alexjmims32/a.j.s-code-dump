create or replace view summary_by_term_spsu as
select r.stvterm_code term_code,
       r.stvterm_desc term_description,
       b.tbbdetc_detail_code CATEGORY,
       b.tbbdetc_desc CAT_DESCRIPTION,
       s.spriden_id student_id,
       sum(t.tbraccd_amount) charge,
       0 payment,
       sum(t.tbraccd_balance) balance
  from tbraccd t
  join tbbdetc b
    on t.tbraccd_detail_code = b.tbbdetc_detail_code
   and b.tbbdetc_type_ind = 'C'
  join spriden s
    on t.tbraccd_pidm = s.spriden_pidm
   and s.spriden_change_ind is null
  join stvterm r
    on r.stvterm_code = t.tbraccd_term_code
 group by r.stvterm_code,
          r.stvterm_desc,
          b.tbbdetc_detail_code,
          b.tbbdetc_desc,
          s.spriden_id
union
select r.stvterm_code term_code,
       r.stvterm_desc term_description,
       b.tbbdetc_detail_code CATEGORY,
       b.tbbdetc_desc CAT_DESCRIPTION,
       s.spriden_id student_id,
       0 charge,
       sum(t.tbraccd_amount) payment,
       sum(t.tbraccd_balance) balance
  from tbraccd t
  join tbbdetc b
    on t.tbraccd_detail_code = b.tbbdetc_detail_code
   and b.tbbdetc_type_ind = 'P'
  join spriden s
    on t.tbraccd_pidm = s.spriden_pidm
   and s.spriden_change_ind is null
  join stvterm r
    on r.stvterm_code = t.tbraccd_term_code
 group by r.stvterm_code,
          r.stvterm_desc,
          b.tbbdetc_detail_code,
          b.tbbdetc_desc,
          s.spriden_id;

