﻿prompt Importing table module...
set feedback off
set define off
truncate table module;
insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 10, 'HISTORY', 'Student History', 'Y', 'N', 'student-history.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 23, 'VIDEOS', 'Videos', 'N', 'Y', 'video.png', 'BOT');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 24, 'WEBSITE', 'SPSU.edu', 'N', 'Y', 'website.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 25, 'NOTIF', 'Notification', 'Y', 'Y', 'notification_white.png', 'TOPLEFT');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 26, 'SETTINGS', 'Settings', 'N', 'Y', 'settings.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 12, 'ALUMNI', 'Alumni', 'N', 'N', 'alumni.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 2, 'ATHLETICS', 'Athletics', 'N', 'Y', 'athletics.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 3, 'EVENTS', 'Events', 'N', 'Y', 'events.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 4, 'MAPS', 'Campus Maps', 'N', 'Y', 'campus-map.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 5, 'COURSES', 'Courses', 'Y', 'Y', 'courses.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 6, 'DIRECTORY', 'Directory', 'N', 'Y', 'dir.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 7, 'PROFILE', 'My Profile', 'Y', 'Y', 'student-profile.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 8, 'ACCOUNTS', 'My Financials', 'Y', 'Y', 'student-accounts.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 9, 'REG', 'Registration', 'Y', 'Y', 'registration.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 11, 'JOBS', 'Jobs', 'N', 'Y', 'jobs.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 14, 'HELP', 'Help', 'N', 'Y', 'help.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 15, 'FEEDBACK', 'Feedback', 'N', 'Y', 'feedback.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 1, 'NEWS', 'Campus News', 'N', 'Y', 'campus-news.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 16, 'ENTER', 'Enter', 'Y', 'N', 'n2n.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 17, 'ENQ', 'Enquire', 'Y', 'N', 'n2n.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 18, 'ENROUTE', 'Enroute', 'Y', 'N', 'n2n.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 13, 'EMER', 'Contacts', 'N', 'Y', 'dir.png', 'MAIN');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 21, 'FACEBOOK', 'Facebook', 'N', 'Y', 'facebook.png', 'BOT');

insert into module (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON, POSITION)
values ('SPSU', 22, 'TWITTER', 'Twitter', 'N', 'Y', 'twitter-white.png', 'BOT');

commit;
prompt Done.
