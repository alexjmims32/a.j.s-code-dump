﻿prompt Importing table feeds...
set feedback off
set define off
truncate table feeds;
insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (35, 0, 'LINK', 'https://employment.spsu.edu/all_jobs.atom', 'xml', 11, 'Jobs', 'jobs.png', 'SPSU', 1, 'SUPERADMIN', '02-APR-13 03.27.04.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (39, 0, 'LINK', 'http://www.spsu.edu/', 'html', 24, 'Website', 'website.png', 'SPSU', null, null, null);

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (31, 0, 'GDATA', 'http://gdata.youtube.com/feeds/base/users/officialSPSUchannel/uploads', 'xml', 23, 'Videos', 'video.png', 'SPSU', 1, 'SUPERADMIN', '06-MAR-13 09.11.25.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (37, 36, 'LINK', 'http://www.spsuhornets.com/rss.php?categoryID=3', 'xml', 34, 'Baseball1', 'athletics.png', 'SPSU', 1, 'SUPERADMIN', '09-APR-13 08.32.59.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (1, 0, 'LINK', 'http://www.spsu.edu/_resources/rss/news.xml', 'xml', 1, 'Campus News', 'news.png', 'SPSU', 2, 'SUPERADMIN', '20-MAY-13 07.12.36.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 0, 'MENU', null, null, 2, 'Athletics', 'sports.png', 'SPSU', 2, 'SUPERADMIN', '23-APR-13 05.00.00.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (3, 0, 'LINK', 'http://calendar.spsu.edu/webcache/v1.0/rssDays/7/list-rss/no--filter.rss', 'xml', 3, 'Events', 'events.png', 'SPSU', 2, 'SUPERADMIN', '25-MAR-13 10.40.24.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (18, 2, 'LINK', 'http://www.spsuhornets.com/rss.php?categoryID=1', 'xml', 2, 'Men''s Basketball', 'athletics.png', 'SPSU', null, null, null);

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (19, 2, 'LINK', 'http://www.spsuhornets.com/rss.php?categoryID=4', 'xml', 2, 'Men''s Soccer', 'athletics.png', 'SPSU', null, null, null);

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (20, 2, 'LINK', 'http://www.spsuhornets.com/rss.php?categoryID=5', 'xml', 2, 'SPSU', 'athletics.png', 'SPSU', null, null, null);

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (30, 0, 'LINK', 'https://paypath.touchnet.com:8701/routecustomer/spt?CID=C21405&KEY=MjE0MDVTb3', 'html', 30, 'Make Payment', 'student-accounts.png', 'SPSU', 2, 'SUPERADMIN', '05-FEB-13 06.13.21.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (38, 2, 'LINK', 'http://www.spsuhornets.com/rss.php?categoryID=6', 'xml', 2, 'Basketball', 'Athletics.png', 'SPSU', 1, 'SUPERADMIN', '30-APR-13 04.32.01.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (24, 0, 'LINK', 'https://m.facebook.com/spsu', 'html', 24, 'Facebook', 'facebook.png', 'SPSU', 2, 'SUPERADMIN', '05-FEB-13 04.05.35.000000 AM');

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (25, 0, 'LINK', 'https://mobile.twitter.com/OfficialSPSU', 'html', 25, 'Twitter', 'twitter.png', 'SPSU', null, null, null);

insert into feeds (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (34, 32, 'LINK', 'test url', 'html', 32, 'test title', 'icon', 'SPSU', 1, 'SUPERADMIN', '25-MAR-13 10.42.11.000000 AM');

commit;
prompt Done.
