﻿prompt Importing table faq...
set feedback off
set define off
truncate table faq;
insert into faq (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (1, 'SPSU', 'Where is the Disability Services office located on campus?', 'Disability Services office is located in the Lower Level of the Student Center in the ATTIC, Suite 160.', 1, 'SUPERADMIN', '02-APR-13 09.57.52.000000 AM');

insert into faq (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (2, 'SPSU', 'Where is the SPSU Police Department located?', 'We are located in Norton Residence Hall (Building R2) on the ground floor. Our office faces Howell Residence Hall (Building R1) and is the middle entrance of Norton Residence Hall (Building R2).', 2, 'SUPERADMIN', '02-APR-13 09.59.31.000000 AM');

insert into faq (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (3, 'SPSU', 'What is the SPSU Foundation?', 'The SPSU Foundation was established in 1976 as the official fundraising arm of Southern Polytechnic State University. It is a 501(c)(3) organization which promotes the cause of higher education, expands educational opportunities, and acquires, invests, and administers private gifts donated to the mission of the University. Gifts to the Foundation are tax-deductible to the full extent of the law. ', 3, 'SUPERADMIN', '02-APR-13 10.00.20.000000 AM');

insert into faq (ID, CAMPUSCODE, QUESTION, ANSWER, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values (4, 'SPSU', 'Do I need visitor pass?', 'One time visitors can park in Lots 2, 12, and 60 (Parking Deck) without a parking pass. Frequent visitors must stop by the University Transportation Office and obtain a visitor''s parking pass every time you come to campus. ', 4, 'SUPERADMIN', '02-APR-13 10.00.56.000000 AM');

commit;
prompt Done.
