﻿prompt Importing table category_filter...
set feedback off
set define off
truncate table category_filter ;
insert into category_filter (ID, CATEGORY, SHOW_BY_DEFAULT)
values (1, 'Academic Buildings', 'Y');

commit;

prompt Done.
