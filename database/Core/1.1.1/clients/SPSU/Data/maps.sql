﻿prompt Importing table maps...
set feedback off
set define off
truncate table maps;
insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (1, 'SPSU', 'A', 'Joe Mack Wilson Student Center', 'Joe Mack Wilson Student Center. The Student Center is the focal point of campus life. Located in the building are the Campus Grill, Bookstore, Post Office', null, null, 'http://i.goldstar.com/gse_media/111/9/4389869259_82a4042647.jpg?c=1&h=128&w=204', '-84.520400', '33.940577', 2, 'SUPERADMIN', '24-APR-13 10.27.46.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (3, 'SPSU', 'S1', 'Recreation and Wellness Center', 'Recreation and Wellness Center', null, null, 'http://upload.wikimedia.org/wikipedia/commons/4/47/UH_Recreation_and_Wellness_Center.jpg', '-84.517739', '33.941236', 2, 'SUPERADMIN', '06-AUG-13 05.03.49.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (4, 'SPSU', 'C', 'Lawrence V. Johnson Library', ' Library of Southern Polytechnic State University, a four-year, special-purpose institution in the University System of Georgia', null, null, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSrOP5x5v0w90rwBz9KCiPiMbXS68DGpj6Ao0QIOIpGOFC26uVf', '-84.520177', '33.939084', 2, 'SUPERADMIN', '06-AUG-13 02.50.35.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (5, 'SPSU', 'Q', 'Engineering Technology Center', 'The Engineering Technology Center (ETC) houses five of SPSUâ&#128;&#153;s academic programs: electrical, computer, and telecommunications engineering technology, mechanical engineering technology, and engineering (robotics).', null, null, 'http://www.grda.com/wp-content/uploads/2012/06/2012-EngineeringTechnologyCenter-1.png', '-84.522334', '33.938319', 2, 'SUPERADMIN', '06-AUG-13 05.26.30.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (6, 'SPSU', 'J', 'Atrium Building', 'Atrium Building', null, null, 'http://cselabs.spsu.edu/images/jbuilding.jpg', '-84.520091', '33.937642', 2, 'SUPERADMIN', '07-AUG-13 02.44.08.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (7, 'SPSU', 'R702', 'Hornet Village 2', 'Hornet Village 2', null, null, 'http://www.jordanskala.com/wp-content/themes/jse2012/timthumb.php?src=http://www.jordanskala.com/wp-content/uploads/2013/02/SPSU-SIH-1.jpg&w=265&zc=1', '-84.521411', '33.936441', 2, 'SUPERADMIN', '07-AUG-13 02.48.13.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (8, 'SPSU', 'ARB', 'Architecture Building', 'Architecture Building', null, null, null, '-84.519158', '33.936272', 2, 'SUPERADMIN', '06-AUG-13 05.28.55.000000 AM', null, 'false', '1');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, ADDRESS, CENTER_FLAG, CATEGORY)
values (9, 'SPSU', 'UC', 'University Courtyard', 'University Courtyard - this is a test of the description', '111-222-3345', 'test@spsu.edu', 'http://media.circlepix.com/media/tours/2010/dec/3/REHWD4/REHWD4_still_ex.jpg', '-84.516787', '33.939067', 2, 'SUPERADMIN', '07-AUG-13 02.54.50.000000 AM', 'University Courtyard IN Main Campus', 'false', '1');
commit;
prompt Done.
