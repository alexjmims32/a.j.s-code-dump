﻿prompt Importing table role...
set feedback off
set define off
truncate table role;
insert into role (CAMPUSCODE, ROLEID, ROLE, DESCRIPTION)
values ('1', '1', 'STUDENT', 'STUDENT');

insert into role (CAMPUSCODE, ROLEID, ROLE, DESCRIPTION)
values ('1', '2', 'FACULTY', 'FACULTY');

commit;
prompt Done.
