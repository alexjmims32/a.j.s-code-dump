﻿prompt Importing table help...
set feedback off
set define off
truncate table help;
insert into help (CAMPUSCODE, ABOUTTEXT, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON)
values ('SPSU', 'Today''s industry must apply technology in every way imaginable. That''s why at Southern Polytechnic State University, our students study the sciences and technologies in a unique, practical manner that provides an education that is career-based and balanced.<br><br>Southern Polytechnic is a residential, co-educational member of the very progressive University System of Georgia. Located on 203 acres of naturally wooded landscape in the historic and vibrant city of Marietta, we are just 20 minutes from downtown Atlanta. Approximately 5,500 students study here, including student representation from 36 states and 64 countries.<br><br>Since our founding in 1948, the university has earned an exceptional academic reputation. But it''s more than just our programs of study that make SPSU a great place to live and learn -- it''s the bright students, the dedicated and experienced faculty, and our hands-on approach to education ', 1, 'SUPERADMIN', '25-MAR-13 05.25.49.000000 AM');
commit;
prompt Done.
