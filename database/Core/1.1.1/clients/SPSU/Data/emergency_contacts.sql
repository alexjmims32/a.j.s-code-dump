﻿prompt Importing table emergency_contacts...
set feedback off
set define off
truncate table emergency_contacts ;
insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (23, 'SPSU', 'Police', '123-123-1234', '1100 S. Marietta Pkwy,Marietta, GA 30060', null, null, 'EMR', 5, 'SUPERADMIN', '25-MAR-13 10.33.24.000000 AM', null, 0);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (4, 'SPSU', 'Undergraduate Admission', '678-915-4188', '1100 South Marietta Pkwy', 'admiss@spsu.edu', 'JPEG', 'EMR', 2, 'SUPERADMIN', '06-AUG-13 05.27.08.000000 AM', null, 2);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (6, 'SPSU', 'Continuing Education', '678-915-7240', 'Building 200, Suite 234,Marietta, Georgia 30067', null, 'http://i.goldstar.com/gse_media/111/9/4389869259_82a4042647.jpg?c=1&h=128&w=204', 'EMR', 2, 'SUPERADMIN', '05-JUL-13 02.45.00.000000 PM', null, 1);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (7, 'SPSU', 'School of Architecture,Civil Engineering', '678-915-5481', '1100 S. Marietta Pkwy,Marietta, GA 30060', 'shodge@spsu.edu', 'JPEG', 'EMR', 2, 'SUPERADMIN', '17-APR-13 08.39.00.000000 AM', null, 3);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (8, 'SPSU', 'School of Computing & Software Engineering', '(678)915-5572', '1100 S. Marietta Pkwy,Marietta, GA 30060', 'pstadnic@spsu.edu', 'JPEG', 'EMR', null, null, null, null, 4);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (9, 'SPSU', 'School of Engineering Technology ', '678-915-7234', '1100 S. Marietta Pkwy,Marietta, GA 30060', 'alay@spsu.edu', 'JPEG', 'EMR', 2, 'SUPERADMIN', '19-FEB-13 08.05.41.000000 AM', null, 5);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (11, 'SPSU', 'School of Engineering', '(678)915-7482', '1100 S. Marietta Pkwy,Marietta, GA 30060', 'tcurrin@spsu.edu', 'JPEG', 'EMR', null, null, null, null, 6);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (13, 'SPSU', 'Athletics / Sports Information', '(678)915-3439', '1100 S. Marietta Pkwy, Marietta, GA 30060', 'kjudge@spsu.edu', 'JPEG', 'EMR', null, null, null, null, 7);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (14, 'SPSU', 'Career Services', '(678)915-7391', '1100 S. Marietta Pkwy, Marietta, GA 30060', null, 'JPEG', 'EMR', null, null, null, null, 8);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (15, 'SPSU', 'Facilities / Reservations', '(678)915-4100', '1100 S. Marietta Pkwy, Marietta, GA 30060', 'reservations@spsu.edu', 'JPEG', 'EMR', null, null, null, null, 10);

insert into emergency_contacts (ID, CAMPUS, NAME, PHONE, ADDRESS, EMAIL, PICTURE_URL, CATEGORY, VERSION_NO, LASTMODIFIEDBY, LASTMODIFIEDON, COMMENTS, SEQ_NUM)
values (18, 'SPSU', 'Information Technology Division', '(678)915-4357', '1100 S. Marietta Pkwy. Marietta, GA  30060', null, 'JPEG', 'EMR', null, null, null, null, 9);

commit;

prompt Done.
