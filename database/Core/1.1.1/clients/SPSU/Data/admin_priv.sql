﻿prompt Importing table admin_priv...
set feedback off
set define off
TRUNCATE TABLE admin_priv ;
insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('test', 'FEEDS', 'Y', 'FEEDS Module Access', 2);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('test', 'MAPS', 'Y', 'MAPS Module Access', 3);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('test', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('test', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', 4);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mpavan', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mpavan', 'FEEDS', 'Y', 'FEEDS Module Access', 2);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('mpavan', 'MAPS', 'Y', 'MAPS Module Access', 3);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('pavanm', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'MAPS', 'Y', 'MAPS Module Access', 3);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'EMERGENCYCONTACTS', 'Y', 'EMERGENCYCONTACTS Module Access', 4);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'HELP-ABOUT', 'Y', 'HELP-ABOUT Module Access', 6);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'HELP-FAQ', 'Y', 'HELP-FAQ Module Access', 8);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'ROLES', 'Y', 'ROLES Module Access', 5);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'FEEDBACK', 'Y', 'FEEDBACK Module Access', 9);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'USERS', 'Y', 'USERS Module Access', 10);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('pavanm', 'FEEDS', 'Y', 'FEEDS Module Access', 2);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'NOTIFICATIONS', 'Y', 'NOTIFICATIONS Module Access', 1);

insert into admin_priv (USERNAME, PRIVCODE, ACCESSFLAG, DESCRIPTION, TAB_SEQ_NO)
values ('SUPERADMIN', 'FEEDS', 'Y', 'FEEDS Module Access', 2);

commit;
prompt Done.
