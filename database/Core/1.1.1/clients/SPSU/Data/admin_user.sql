﻿prompt Importing table admin_user...
set feedback off
set define off
truncate table admin_user;
insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('SUPERADMIN', 'Admin', 'Super', 'U1VQRVJBRE1JTjpBRE1JTg==', 'Y');

insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('pavanm', 'pavan', 'm', 'cGF2YW5NOlBBVkFO', 'Y');

insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('test', 'pavan', 'kumar', 'dGVzdDpURVNU', 'Y');

insert into admin_user (USERNAME, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('mpavan', 'pavan', 'm', 'bXBhdmFuOnBhdmFu', 'Y');

commit;

prompt Done.
