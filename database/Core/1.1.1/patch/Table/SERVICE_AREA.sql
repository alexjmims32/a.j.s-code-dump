PROMPT CREATE TABLE service_area
CREATE TABLE service_area (
  id         INTEGER            NOT NULL,
  area       VARCHAR2(100 BYTE) NOT NULL,
  subarea    VARCHAR2(100 BYTE) NULL,
  supervisor VARCHAR2(100 BYTE) NULL,
  assocdean  VARCHAR2(100 BYTE) NULL,
  dean       VARCHAR2(100 BYTE) NULL,
  hr         VARCHAR2(100 BYTE) NULL,
  provost    VARCHAR2(100 BYTE) NULL
)
  STORAGE (
    NEXT       1024 K
  )
/


