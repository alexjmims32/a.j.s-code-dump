PROMPT CREATE TABLE ME_COLLEGE_DEAN
create table ME_COLLEGE_DEAN
(
  coll_code       VARCHAR2(2),
  college         VARCHAR2(500),
  dean_name       VARCHAR2(100),
  dean_email      VARCHAR2(100),
  dean_phone_area VARCHAR2(10),
  dean_phone      VARCHAR2(20),
  dean_extn       VARCHAR2(10),
  dean_fax        VARCHAR2(30),
  feedback_ind    VARCHAR2(1)
);

