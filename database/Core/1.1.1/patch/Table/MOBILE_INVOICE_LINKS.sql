PROMPT CREATE TABLE MOBILE_INVOICE_LINKS
create table MOBILE_INVOICE_LINKS
(
  detail_code   VARCHAR2(30),
  url_link      VARCHAR2(500),
  display_value VARCHAR2(300),
  activity_date DATE
)
/


