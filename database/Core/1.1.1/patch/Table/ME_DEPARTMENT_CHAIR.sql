PROMPT CREATE TABLE ME_DEPARTMENT_CHAIR
create table ME_DEPARTMENT_CHAIR
(
  coll_code        VARCHAR2(2),
  college          VARCHAR2(500),
  dept_code        VARCHAR2(4),
  department       VARCHAR2(500),
  chair_name       VARCHAR2(100),
  chair_email      VARCHAR2(100),
  chair_phone_area VARCHAR2(10),
  chair_phone      VARCHAR2(20),
  chair_extn       VARCHAR2(10),
  chair_fax        VARCHAR2(30),
  feedback_ind     VARCHAR2(1)
);
