CREATE OR REPLACE PACKAGE me_reg_utils is
  FUNCTION FZ_ME_REG_SEQ(P_CRN       IN SSBSECT.SSBSECT_CRN%TYPE,
                         P_TERM_CODE IN SSBSECT.SSBSECT_TERM_CODE%TYPE)
    RETURN NUMBER;
  FUNCTION FZ_ME_SORT_SEQ(P_CRN       IN SSBSECT.SSBSECT_CRN%TYPE,
                          P_TERM_CODE IN SSBSECT.SSBSECT_TERM_CODE%TYPE)
    RETURN NUMBER;
  function FZ_ADDRESS(PIDM  in SATURN.SPRIDEN.SPRIDEN_PIDM%type,
                      FIELD varchar2) return varchar2;
  function FZ_PHONE(PIDM  in SATURN.SPRIDEN.SPRIDEN_PIDM%type,
                    FIELD varchar2) return varchar2;
  FUNCTION FZ_FLAG_DESC(P_FLAG VARCHAR2) RETURN VARCHAR2;
  FUNCTION FZ_GET_EMAIL_ADDRESS(P_PIDM NUMBER) RETURN VARCHAR2;
  FUNCTION FZ_GET_CAMP(P_PIDM NUMBER) RETURN VARCHAR2;
  FUNCTION FZ_GET_INSTRUCTOR(P_TERM_CODE VARCHAR2, P_CRN NUMBER)
    RETURN VARCHAR2;
  FUNCTION FZ_GET_COURSE_LEVEL(P_CRSE_NUMB VARCHAR2,
                               P_TERM      VARCHAR2,
                               P_SUBJ_CODE VARCHAR2) RETURN VARCHAR2;
  FUNCTION FZ_GET_EMP_EMAIL(P_PIDM VARCHAR2) RETURN VARCHAR2;
  FUNCTION wf_course_comment(term  VARCHAR2,
                             crn   NUMBER,
                             delim VARCHAR2 DEFAULT ' ') RETURN VARCHAR2;
  FUNCTION fz_course_meeting_time(term VARCHAR2, crn NUMBER) RETURN VARCHAR2;
  FUNCTION fz_course_meeting_date(term VARCHAR2, crn NUMBER) RETURN VARCHAR2;
  function fz_get_desc(p_which_table varchar2, p_code varchar2)
    return varchar2;
  FUNCTION fz_gettermfirst(p_stupidm   IN NUMBER,
                           p_levl_code IN VARCHAR2,
                           p_term      varchar2) RETURN VARCHAR2;

  FUNCTION FZ_GET_DESC(INCOMING_PARAMETER_VALUE   VARCHAR2,
                       WHICH_TABLE                VARCHAR2,
                       OVERRIDE_SELECT_COLUMN     VARCHAR2 DEFAULT NULL,
                       FINANCE_IND                VARCHAR2 DEFAULT 'N',
                       OVERRIDE_COMPARISON_COLUMN VARCHAR2 DEFAULT NULL,
                       OVERRIDE_LENGTH            NUMBER DEFAULT NULL,
                       SECOND_PARAMETER           VARCHAR2 DEFAULT NULL,
                       SYSTEM_IND                 VARCHAR2 DEFAULT NULL)
    RETURN VARCHAR2;

  FUNCTION FZ_PROCESSES_AFFECT_BY_HOLD(P_HOLD_CODE VARCHAR2) RETURN VARCHAR2;
  
   FUNCTION FZ_REGISTERED_THIS_TERM(pidm number, term varchar2)
    return varchar2;
    FUNCTION FZ_LATEST_TERM_FNC (pidm number)
    return varchar2;
FUNCTION FZ_GET_SECT_ATTR(
    P_TERM_CODE VARCHAR2,
    P_CRN       NUMBER)
  RETURN VARCHAR2;

end me_reg_utils;
/

