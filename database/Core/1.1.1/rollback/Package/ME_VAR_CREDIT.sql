PROMPT CREATE OR REPLACE PACKAGE ME_VAR_CREDIT
CREATE OR REPLACE PACKAGE MOBEDU.ME_VAR_CREDIT AS
PROCEDURE PZ_CHANGE_CREDITS(p_student_id VARCHAR2,
                               P_TERM_CODE VARCHAR2,
                               P_CRN NUMBER,
                               P_cred NUMBER,
                               P_CRED_OLD NUMBER,
                               P_ERROR OUT VARCHAR2);
end ME_VAR_CREDIT;
/

