PROMPT CREATE OR REPLACE PACKAGE me_withdrawl
CREATE OR REPLACE package me_withdrawl as
  PROCEDURE P_WITHDRAWL(P_STUDENT_ID VARCHAR2,
                        P_TERM_CODE  VARCHAR2,
                        P_CRN        NUMBER,
                        P_RSTS       VARCHAR2,
                        P_ERROR      OUT VARCHAR2);
end;



/

