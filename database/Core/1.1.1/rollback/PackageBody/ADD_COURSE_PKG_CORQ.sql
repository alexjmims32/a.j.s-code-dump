PROMPT CREATE OR REPLACE PACKAGE BODY add_course_pkg_corq
CREATE OR REPLACE PACKAGE BODY add_course_pkg_corq AS

  v_debug        boolean;
  p_error_msg    varchar2(4000);
  sgbstdn_row    sgbstdn%rowtype;
  p_student_id   varchar2(10);
  global_pidm    SPRIDEN.SPRIDEN_PIDM%TYPE;
  sfbetrm_rec    sfbetrm%ROWTYPE;
  sftregs_rec    sftregs%ROWTYPE;
  sfrbtch_row    sfrbtch%ROWTYPE;
  tbrcbrq_row    tbrcbrq%ROWTYPE;
  sobterm_row    sobterm%ROWTYPE;
  regs_date      date;
  gv_term_in     STVTERM.STVTERM_CODE%TYPE;
  gv_crn_in      SSBSECT.SSBSECT_CRN%TYPE;
  term           STVTERM.STVTERM_CODE%TYPE;
  error_rec_out  sftregs%ROWTYPE;
  error_flag_out VARCHAR2(40);
  tmst_flag_out  VARCHAR2(40);

  function f_get_ptrm_code(p_crn_in    in SSBSECT.SSBSECT_CRN%TYPE,
                           p_term_code in STVTERM.STVTERM_CODE%TYPE)
    return varchar2 is
    v_ptrm_code varchar2(20);
  begin
    select ssbsect_ptrm_code
      into v_ptrm_code
      from ssbsect
     where ssbsect_crn = p_crn_in
       and ssbsect_term_code = p_term_code;
    return v_ptrm_code;
  exception
    when others then
      return v_ptrm_code;
  end f_get_ptrm_code;

  procedure pz_rect5_prev_errs_nd_ad_2_crt(p_student_id in varchar2,
                                           p_term_in    in varchar2,
                                           p_error      out varchar2) is
  begin

    update mobedu_cart
       set error_flag = 'N', processed_ind = 'N', Status = ''
     where error_flag = 'Y'
       and student_id = p_student_id
       and term = p_term_in;

    gb_common.p_commit;

  exception
    when others then
      gb_common.p_commit;
      p_error := 'Failed to rectify previous errors in the cart';
  end pz_rect5_prev_errs_nd_ad_2_crt;

  PROCEDURE p_regs_mobile(p_term_in   sftregs.sftregs_term_code%type,
                          p_crn_in    sftregs.sftregs_crn%type,
                          p_rsts_in   sftregs.sftregs_rsts_code%type,
                          p_pidm_in   sftregs.sftregs_pidm%type,
                          p_error_msg out varchar2,
                          p_rowid_in  out varchar2) is

    CURSOR web_rsts_checkc(rsts_in VARCHAR2) IS
      SELECT 'Y'
        FROM stvrsts
       WHERE stvrsts_code = rsts_in
         AND stvrsts_web_ind = 'Y';

    web_rsts_checkc_flag varchar2(10);

    v_rsts_desc varchar2(100);

  begin

    IF p_rsts_in IS NOT NULL THEN
      OPEN web_rsts_checkc(p_rsts_in);
      FETCH web_rsts_checkc
        INTO web_rsts_checkc_flag;

      IF web_rsts_checkc%NOTFOUND THEN

        begin
          select stvrsts_desc
            into v_rsts_desc
            from stvrsts
           WHERE stvrsts_code = p_rsts_in
             and rownum = 1;
        exception
          when others then
            v_rsts_desc := null;
        end;

        p_error_msg := NVL(v_rsts_desc, 'Registration ') ||
                       ' through web is not allowed for this term.';
        return;

      END IF;

      CLOSE web_rsts_checkc;
    END IF;

    --Check if registration is allowed for term, CRN, part of the term
    web_rsts_checkc_flag := '';

    web_rsts_checkc_flag := me_valid.fz_check_sfrrsts(p_term_in,
                                                      p_crn_in,
                                                      'R');
    IF web_rsts_checkc_flag = 'N' THEN
      p_error_msg := 'Registration not available at this time.';
      return;
    END if;

    sftregs_rec := null;

    sftregs_rec.sftregs_crn       := p_crn_in;
    sftregs_rec.sftregs_rsts_code := p_rsts_in;
    sftregs_rec.sftregs_rsts_date := SYSDATE;
    sftregs_rec.sftregs_pidm      := p_pidm_in;
    sftregs_rec.sftregs_term_code := p_term_in;

    --get sftregs_rec.sftregs_sect_subj_code, sftregs_rec.sftregs_sect_crse_numb, sftregs_rec.sftregs_sect_seq_numb
    begin
      me_bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                               sftregs_rec.sftregs_crn,
                               sftregs_rec.sftregs_sect_subj_code,
                               sftregs_rec.sftregs_sect_crse_numb,
                               sftregs_rec.sftregs_sect_seq_numb);
    exception
      when others then
        p_error_msg := 'Unable to derive section details.';
        return;
    end;
    --get sftregs_rec.sftregs_ptrm_code
    begin
      sftregs_rec.sftregs_ptrm_code := f_get_ptrm_code(sftregs_rec.sftregs_crn,
                                                       sftregs_rec.sftregs_term_code);
    exception
      when others then
        p_error_msg := 'Unable to derive part of term.';
        return;
    end;

    IF (sb_course_registration.f_exists(sftregs_rec.sftregs_term_code,
                                        sftregs_rec.sftregs_pidm,
                                        sftregs_rec.sftregs_crn) = 'Y') THEN
      p_error_msg := 'Already registered for this course,' || 'CRN(' ||
                     sftregs_rec.sftregs_crn || ')!';

      return;
    END IF;

    IF v_debug THEN

      dbms_output.put_line('Subj:' || sftregs_rec.sftregs_sect_subj_code ||
                           ';Crse:' || sftregs_rec.sftregs_sect_crse_numb ||
                           ';Seq:' || sftregs_rec.sftregs_sect_seq_numb ||
                           ';Ptrm:' || sftregs_rec.sftregs_ptrm_code ||
                           ';Pidm:' || sftregs_rec.sftregs_pidm || ';Crn:' ||
                           sftregs_rec.sftregs_crn);
    END IF;

    begin
      me_bwckregs.p_addcrse(sftregs_rec,
                            sftregs_rec.sftregs_sect_subj_code,
                            sftregs_rec.sftregs_sect_crse_numb,
                            sftregs_rec.sftregs_sect_seq_numb,
                            null,
                            null);
    exception
      when others then
        p_error_msg := 'Could not add registration record.' || SQLERRM;

        IF v_debug THEN
          dbms_output.put_line('Could not add registration record.' ||
                               SQLERRM);
        END IF;

        return;
    end;

    begin
      select rowid
        into P_ROWID_IN
        from sftregs
       where sftregs_pidm = p_pidm_in
         and sftregs_term_code = p_term_in
         and sftregs_crn = p_crn_in;

      IF v_debug THEN
        dbms_output.put_line(sftregs_rec.sftregs_pidm || ';' ||
                             sftregs_rec.sftregs_term_code || ';' ||
                             sftregs_rec.sftregs_crn || ';' || P_ROWID_IN);
      END IF;

    exception
      when others then

        P_ROWID_IN  := null;
        p_error_msg := 'Registration record failed.';

    end;

    if p_error_msg is null then
      begin
        select sftregs_message
          into p_error_msg
          from sftregs
         where sftregs_pidm = p_pidm_in
           and sftregs_crn = p_crn_in
           and sftregs_term_code = p_term_in;

      exception
        when others then
          p_error_msg := 'No records in temporary table';

      end;
    end if;

  exception
    when others then
      p_error_msg := 'Registration record failed.' || SQLERRM;
  end;

  --This Procedure will take the courses from the cart for the student id for the term provided
  --and tries to register all those courses.
  --If there are no courses in the cart , then Appropriate message will be displayed.

  procedure pz_register_courses_in_cart(p_student_id in spriden.spriden_id%type,
                                        p_term_in    in stvterm.stvterm_code%type,
                                        p_response   out varchar2,
                                        p_debug      in varchar2 default 'N') is
    v_error varchar2(400);

    v_check                number := 0;
    term_in                OWA_UTIL.ident_arr;
    drop_problems_in_out   sfkcurs.drop_problems_rec_tabtype;
    drop_failures_in_out   sfkcurs.drop_problems_rec_tabtype;
    sgbstdn_row            sgbstdn%rowtype;
    sfbetrm_rec            sfbetrm%rowtype;
    error_rec_out          sftregs%rowtype;
    error_flag_out         varchar2(40);
    tmst_flag_out          varchar2(40);
    etrm_done_in_out       BOOLEAN := FALSE;
    p_pidm                 number;
    capp_tech_error_in_out varchar2(4);
    P_ROWID_OUT            rowid;
    v_cart_count           number := 0;
    fa_return_status       number;
    save_act_date          VARCHAR2(200);
    sobterm_row            SOBTERM%ROWTYPE;
    clas_code              SGRCLSR.SGRCLSR_CLAS_CODE%TYPE;
    stufac_ind             varchar2(10) := 'S';
    called_by_proc_name    varchar2(50) := 'bwskfreg.P_AddDropCrse';

    --get the courses to register from cart
    cursor c_register_these_courses is
      select *
        from mobedu_cart
       where student_id = p_student_id
         and term = p_term_in
         and processed_ind = 'N';

    cursor c_get_mobedu_cart(cv_student_id in varchar2,
                             cv_term_code  in varchar2) is
      select *
        from mobedu_cart
       where student_id = cv_student_id
         and term = cv_term_code
         and processed_ind = 'Q';

  begin
    p_error_msg := '';

    if p_debug = 'Y' then
      v_debug := TRUE;
    else
      v_debug := FALSE;
    end if;

    --if there are any previous errors for the student , term combination
    --(previously failed) , these will be corrected(making eligible to register)

    pz_rect5_prev_errs_nd_ad_2_crt(p_student_id, p_term_in, v_error);

    for c_register_this_course in c_register_these_courses loop
      v_cart_count := c_register_these_courses%rowcount;
    end loop;

    if v_cart_count = 0 then
      p_response := 'Empty Cart!';
      goto quit_from_program;
    end if;

    p_pidm := baninst1.gb_common.f_get_pidm(p_student_id);

    bwcklibs.p_initvalue(p_pidm, p_term_in, '', '', '', '');

    global_pidm         := p_pidm;
    gv_term_in          := p_term_in;
    regs_date           := sysdate;
    stufac_ind          := 'S';
    called_by_proc_name := 'bwskfreg.P_AddDropCrse';

    me_valid.pz_registration_checks(p_student_id => p_student_id,
                                    p_term       => p_term_in,
                                    p_error_msg  => p_response);

    IF v_debug THEN
      dbms_output.put_line('pz_registration_checks:' || p_response);
    END IF;

    IF p_response IS NOT NULL THEN
      GOTO quit_from_program;
    END IF;

    IF NOT me_valid.f_reg_access_still_good(global_pidm,
                                            gv_term_in,
                                            stufac_ind || global_pidm,
                                            called_by_proc_name) THEN

      p_response := 'Your registration attempts exceeded in this term. Contact your registrar for assistance';

    END IF;

    IF v_debug THEN
      dbms_output.put_line('f_reg_access_still_good:' || p_response);
    END IF;

    IF p_response IS NOT NULL THEN
      GOTO quit_from_program;
    END IF;

    --This procedure will remove the previous trial entries in registration tables
    --Refresh SFTREGS entries.
    sfkmods.p_delete_sftregs_by_pidm_term(p_pidm, p_term_in);
    sfkmods.p_insert_sftregs_from_stcr(p_pidm, p_term_in, SYSDATE);

    begin
      delete from sfrstcr
       where sfrstcr_pidm = p_pidm
         and sfrstcr_term_code = p_term_in
         and sfrstcr_error_flag = 'F'
         and sfrstcr_message is not null
         and sfrstcr_rsts_code = 'DW';
      gb_common.p_commit;
    end;

    me_bwckcoms.p_regs_etrm_chk(p_pidm, p_term_in, clas_code); --will create sfbetrm record if there is no record already

    IF v_debug THEN
      dbms_output.put_line('Class code is:' || clas_code);
    END IF;

    for r_register_this_course in c_register_these_courses loop

      begin
        P_ERROR_MSG := '';
        P_ROWID_OUT := '';

        IF v_debug THEN
          dbms_output.put_line('CRN :' || r_register_this_course.crn);
          dbms_output.put_line('ID : ' ||
                               r_register_this_course.student_id);
          dbms_output.put_line('RSTS CODe : ' ||
                               r_register_this_course.rsts_code);
          dbms_output.put_line('TERM : ' || r_register_this_course.term);
        END IF;

        p_regs_mobile(r_register_this_course.term,
                      r_register_this_course.crn,
                      r_register_this_course.rsts_code,
                      p_pidm,
                      P_ERROR_MSG,
                      P_ROWID_OUT);

        gb_common.p_commit;

        IF v_debug THEN
          dbms_output.put_line('p_regs_mobile:' || P_ERROR_MSG ||
                               ';Row ID:' || P_ROWID_OUT);
        END IF;

      exception
        when others then
          IF v_debug THEN
            dbms_output.put_line('Exception raised while creating sftregs : ' ||
                                 sqlerrm);
          END IF;
          P_ERROR_MSG := 'Failed to register the student due to ' ||
                         sqlerrm;
          IF v_debug THEN
            DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
            DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
          END IF;

          GB_COMMON.P_ROLLBACK;

      end;
      --update cart status here
      update mobedu_cart
         set status        = decode(P_ERROR_MSG,
                                    null,
                                    decode(P_ROWID_OUT, null, 'FAILURE', ''),
                                    P_ERROR_MSG),
             error_flag    = decode(P_ERROR_MSG, null, 'N', 'Y'),
             processed_ind = decode(P_ERROR_MSG, null, 'Q', 'Y'),
             pidm          = p_pidm
       where crn = r_register_this_course.crn
         and term = r_register_this_course.term
         and student_id = r_register_this_course.student_id;

      v_cart_count := c_register_these_courses%rowcount;

      gb_common.p_commit;

    end loop;

    IF v_debug THEN
      dbms_output.put_line('Cart count:' || v_cart_count);
    END IF;

    begin

      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE('Validation phase started:');
        dbms_output.put_line('PIDM:' || p_pidm);
      END IF;

      term_in(1) := gv_term_in;
      IF v_debug THEN
        dbms_output.put_line('TERM:' || term_in(1));
      END IF;
      p_error_msg := '';

      pz_do_validations(term_in,
                        p_pidm,
                        etrm_done_in_out,
                        capp_tech_error_in_out,
                        drop_problems_in_out,
                        drop_failures_in_out,
                        p_error_msg);

      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE('Validation phase completed:');
      END IF;
      gb_common.p_commit;
    exception
      when others then
        p_error_msg := 'Registration failed while validating. Error :' ||
                       sqlerrm;
        IF v_debug THEN
          DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
          DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        END IF;
        GB_COMMON.P_ROLLBACK;

    end;

    --If any error/exception arised in validation, then
    --do commit and throw the error to mobedu_cart
    --and then do nothing from there on.

    IF P_ERROR_MSG IS NOT NULL THEN
      BEGIN
        update mobedu_cart
           set status = P_ERROR_MSG, error_flag = 'Y', processed_ind = 'Y'
         where term = p_term_in
           and student_id = p_student_id;

        gb_common.p_commit;

        P_ERROR_MSG := '';
        p_response  := 'Registration completed.';
        goto quit_from_program;
      END;
    END IF;

    begin

      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE('Commit phase started:');
      END IF;

      FOR sfbetrm IN sfkcurs.sfbetrmc(p_pidm, gv_term_in) LOOP
        sfbetrm_rec := sfbetrm;
      END LOOP;

      FOR sgbstdn IN sgklibs.sgbstdnc(p_pidm, gv_term_in) LOOP
        sgbstdn_row := sgbstdn;
      END LOOP;

      pz_final_commit(p_pidm,
                      gv_term_in,
                      regs_date,
                      nvl(clas_code, '01'),
                      sgbstdn_row.sgbstdn_styp_code,
                      'F',
                      'Y',
                      sfbetrm_rec.sfbetrm_tmst_maint_ind,
                      sfbetrm_rec.sfbetrm_tmst_code,
                      null,
                      'WA',
                      error_rec_out,
                      error_flag_out,
                      tmst_flag_out);

      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE('Commit phase completed:');
      END IF;

      gb_common.p_commit;

    exception
      when others then
        p_error_msg := 'Registration Failed while commit.Error :' ||
                       sqlerrm;
        IF v_debug THEN
          DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
          DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        END IF;
        GB_COMMON.P_ROLLBACK;

    end;

    for r_process_cart_entry in c_get_mobedu_cart(p_student_id, p_term_in) loop

      v_check := 0;

      select count(1)
        into v_check
        from sfrstcr A
       where A.SFRSTCR_TERM_CODE = r_process_cart_entry.term
         and A.SFRSTCR_PIDM = p_pidm
         and A.SFRSTCR_CRN = r_process_cart_entry.crn;

      if (v_check > 0) then

        update mobedu_cart
           set status = 'SUCCESS', processed_ind = 'Y', error_flag = 'N'
         where student_id = r_process_cart_entry.student_id
           and term = r_process_cart_entry.term
           and crn = r_process_cart_entry.crn
           and nvl(error_flag, 'N') = 'N'
           and status is null;

        gb_common.p_commit;

      else

        update (select t.processed_ind,
                       t.error_flag,
                       t.status,
                       R.SFTREGS_ERROR_FLAG,
                       R.sftregs_message
                  from mobedu_cart T, sftregs R
                 where T.pidm = R.sftregs_pidm
                   and t.crn = sftregs_crn
                   and t.term = R.sftregs_term_code
                   and t.student_id = r_process_cart_entry.student_id
                   and t.term = r_process_cart_entry.term
                   and t.crn = r_process_cart_entry.crn
                   and nvl(t.error_flag, 'N') = 'N'
                   and t.status is null) UP
           set UP.status        = sftregs_message,
               UP.Error_Flag    = decode(SFTREGS_ERROR_FLAG,
                                         'F',
                                         'Y',
                                         'W',
                                         'W',
                                         'N'),
               UP.processed_ind = 'Y';

        update mobedu_cart
           set status        = 'Could not register this course. Try again!',
               processed_ind = 'Y',
               error_flag    = 'Y'
         where student_id = r_process_cart_entry.student_id
           and term = r_process_cart_entry.term
           and crn = r_process_cart_entry.crn
           and nvl(error_flag, 'N') = 'N'
           and status is null;

        gb_common.p_commit;

      end if;

    end loop;

    BEGIN

      --
      -- Create a batch fee assessment record.
      -- ===================================================
      sfrbtch_row.sfrbtch_term_code     := p_term_in;
      sfrbtch_row.sfrbtch_pidm          := p_pidm;
      sfrbtch_row.sfrbtch_clas_code     := clas_code;
      sfrbtch_row.sfrbtch_activity_date := SYSDATE;
      bwcklibs.p_add_sfrbtch(sfrbtch_row);

      tbrcbrq_row.tbrcbrq_term_code     := p_term_in;
      tbrcbrq_row.tbrcbrq_pidm          := p_pidm;
      tbrcbrq_row.tbrcbrq_activity_date := SYSDATE;
      bwcklibs.p_add_tbrcbrq(tbrcbrq_row);

      -- Online fee assessment if enabled
      bwcklibs.p_getsobterm(p_term_in, sobterm_row);
      IF sobterm_row.sobterm_fee_assess_vr = 'Y' AND
         sobterm_row.sobterm_fee_assessment = 'Y' THEN
        SFKFEES.p_processfeeassessment(p_term_in,
                                       p_pidm,
                                       SYSDATE,
                                       SYSDATE,
                                       'R',
                                       'Y',
                                       'BWCKREGS',
                                       'Y',
                                       save_act_date,
                                       'N',
                                       fa_return_status);

        gb_common.p_commit;
      END IF;

    exception
      when others then

        update mobedu_cart
           set status        = status || '*Fee assessment not done!',
               processed_ind = 'Y',
               error_flag    = decode(error_flag, 'Y', 'Y', 'W')
         where student_id = p_student_id
           and term = p_term_in;
        gb_common.p_commit;

    END;

    --for safety clean registration temp tables
    --as this package processing should not impact banner processing
    --Refresh SFTREGS entries.
    sfkmods.p_delete_sftregs_by_pidm_term(p_pidm, p_term_in);
    sfkmods.p_insert_sftregs_from_stcr(p_pidm, p_term_in, SYSDATE);

    gb_common.p_commit;

    p_response := 'Registration completed.';

    <<quit_from_program>>
    null;

  exception
    when others then
      p_error_msg := SQLERRM;
      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      END IF;
      gb_common.p_rollback;
      p_response := 'Banner system error!';

      update mobedu_cart
         set status = p_error_msg, processed_ind = 'Y', error_flag = 'Y'
       where student_id = p_student_id
         and term = p_term_in;
      gb_common.p_commit;

  end;

  procedure pz_do_validations(term_in                IN OWA_UTIL.ident_arr,
                              pidm_in                IN spriden.spriden_pidm%TYPE,
                              etrm_done_in_out       IN OUT BOOLEAN,
                              capp_tech_error_in_out IN OUT VARCHAR2,
                              drop_problems_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              drop_failures_in_out   IN OUT sfkcurs.drop_problems_rec_tabtype,
                              error_msg              out varchar2) is
  begin

    IF v_debug THEN
      dbms_output.put_line('me_bwckcoms.p_group_edits started.');
    END IF;

    me_bwckcoms.p_group_edits(term_in,
                              pidm_in,
                              etrm_done_in_out,
                              capp_tech_error_in_out,
                              drop_problems_in_out,
                              drop_failures_in_out);

    IF v_debug THEN
      dbms_output.put_line('me_bwckcoms.p_group_edits completed.');
    END IF;

    commit;

  exception
    when others then

      error_msg := SQLERRM;
      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      END IF;

  end pz_do_validations;

  procedure pz_final_commit(pidm_in            IN sftregs.sftregs_pidm%TYPE,
                            term_in            IN sftregs.sftregs_term_code%TYPE,
                            reg_date_in        IN sftregs.sftregs_add_date%TYPE,
                            clas_code_in       IN sgrclsr.sgrclsr_clas_code%TYPE,
                            styp_code_in       IN sgbstdn.sgbstdn_styp_code%TYPE,
                            capc_severity_in   IN sobterm.sobterm_capc_severity%TYPE,
                            tmst_calc_ind_in   IN sobterm.sobterm_tmst_calc_ind%TYPE,
                            tmst_maint_ind_in  IN sfbetrm.sfbetrm_tmst_maint_ind%TYPE,
                            tmst_code_in       IN sfbetrm.sfbetrm_tmst_code%TYPE,
                            drop_last_class_in IN BOOLEAN DEFAULT TRUE,
                            system_in          IN VARCHAR2,
                            error_rec_out      OUT sftregs%ROWTYPE,
                            error_flag_out     OUT VARCHAR2,
                            tmst_flag_out      OUT VARCHAR2) is

    p_error_msg varchar2(400);
  begin

    IF v_debug THEN
      dbms_output.put_line('PIDM:' || pidm_in);
      dbms_output.put_line('TERM:' || term_in);
      dbms_output.put_line('STYP:' || styp_code_in);
      dbms_output.put_line('TMST_MAINT:' || tmst_maint_ind_in);
      dbms_output.put_line('TMST:' || tmst_code_in);
    END IF;

    sfkedit.p_update_regs(pidm_in,
                          term_in,
                          reg_date_in,
                          clas_code_in,
                          styp_code_in,
                          'F',
                          'Y',
                          tmst_maint_ind_in,
                          tmst_code_in,
                          null,
                          'WA',
                          error_rec_out,
                          error_flag_out,
                          tmst_flag_out);

  exception
    when others then
      p_error_msg := 'Failed to register the student.';

      IF v_debug THEN
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_STACK);
        DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
      END IF;
      -- RAISE;

  end;

begin
  p_student_id := null;
  regs_date    := sysdate;
  global_pidm  := null;
  gv_term_in   := null;
  gv_crn_in    := null;
  v_debug      := FALSE;
  DBMS_OUTPUT.DISABLE;
end add_course_pkg_corq;
/

