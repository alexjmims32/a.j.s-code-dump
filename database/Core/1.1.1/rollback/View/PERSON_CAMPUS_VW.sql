PROMPT CREATE OR REPLACE VIEW person_campus_vw
CREATE OR REPLACE FORCE VIEW person_campus_vw (
  id,
  pidm,
  first_name,
  last_name,
  middle_name,
  full_name,
  category,
  campus,
  college
) AS
SELECT a.spriden_id Id,
       A.SPRIDEN_PIDM PIDM,
       a.spriden_first_name First_name,
       a.spriden_last_name Last_name,
       a.spriden_mi Middle_name,
       a.spriden_first_name || ' ' || a.spriden_last_name Full_name,
       DECODE((SELECT COUNT(*)
                FROM SIBINST
               WHERE SIBINST_PIDM = SPRIDEN_PIDM
                 AND EXISTS
               (SELECT 1
                        FROM STVFCST F
                       WHERE F.STVFCST_CODE = SIBINST_FCST_CODE
                         AND F.STVFCST_ACTIVE_IND <> 'I')),
              0,
              DECODE((SELECT COUNT(*)
                       FROM PEBEMPL
                      WHERE PEBEMPL_PIDM = SPRIDEN_PIDM),
                     0,
                     DECODE((SELECT COUNT(*)
                              FROM SGBSTDN
                             WHERE SGBSTDN_PIDM = SPRIDEN_PIDM),
                            0,
                            'OTHER',
                            'STUDENT'),
                     'EMPLOYEE'),
              'FACULTY') CATEGORY,
       nvl((SELECT P.PEBEMPL_CAMP_CODE
             FROM PEBEMPL P
            WHERE P.PEBEMPL_PIDM = spriden_pidm
              AND P.PEBEMPL_EMPL_STATUS = 'A'),
           (SELECT S.SGBSTDN_CAMP_CODE
              FROM SGBSTDN S
             WHERE S.SGBSTDN_PIDM = spriden_pidm
               AND S.SGBSTDN_TERM_CODE_EFF =
                   (SELECT MAX(T.SGBSTDN_TERM_CODE_EFF)
                      FROM SGBSTDN T
                     WHERE T.SGBSTDN_PIDM = S.SGBSTDN_PIDM))) campus,
       nvl((SELECT P.PEBEMPL_COLL_CODE
             FROM PEBEMPL P
            WHERE P.PEBEMPL_PIDM = spriden_pidm
              AND P.PEBEMPL_EMPL_STATUS = 'A'),
           (SELECT S.SGBSTDN_COLL_CODE_1
              FROM SGBSTDN S
             WHERE S.SGBSTDN_PIDM = spriden_pidm
               AND S.SGBSTDN_TERM_CODE_EFF =
                   (SELECT MAX(T.SGBSTDN_TERM_CODE_EFF)
                      FROM SGBSTDN T
                     WHERE T.SGBSTDN_PIDM = S.SGBSTDN_PIDM))) college
  FROM spriden a
 WHERE a.spriden_change_ind IS NULL
   AND A.SPRIDEN_ENTITY_IND = 'P'
/

