PROMPT CREATE OR REPLACE VIEW student_account_detail
CREATE OR REPLACE FORCE VIEW student_account_detail (
  pidm,
  student_id,
  last_name,
  first_name,
  middle_name,
  total_account_balance,
  total_amount_due,
  current_term_balance,
  previous_term_balance,
  total_memo_balance,
  oldest_effective_date,
  financial_holds,
  net_amount_due,
  total_amount_past_due,
  total_future_term_charges,
  total_unbilled_charges
) AS
select c.spriden_pidm PIDM,
       c.spriden_id STUDENT_ID,
       c.spriden_last_name LAST_NAME,
       c.spriden_first_name FIRST_NAME,
       c.spriden_mi MIDDLE_NAME,
       me_account_objects.FZ_GET_ACC_BALANCE(SPRIDEN_PIDM) TOTAL_ACCOUNT_BALANCE,
       me_account_objects.FZ_GET_TOT_DUE_BAL(SPRIDEN_PIDM) TOTAL_AMOUNT_DUE,
       me_account_objects.FZ_GET_CURRTRM_ACC_BAL(SPRIDEN_PIDM) CURRENT_TERM_BALANCE,
       me_account_objects.FZ_GET_PREVTRM_ACC_BAL(SPRIDEN_PIDM) PREVIOUS_TERM_BALANCE,
       BANINST1.F_MEMO_BALANCE(SPRIDEN_PIDM) TOTAL_MEMO_BALANCE,
       BANINST1.F_OLDEST_EFFECTIVE_DATE(SPRIDEN_PIDM) OLDEST_EFFECTIVE_DATE,
       me_account_objects.fz_get_hold_flag(c.spriden_id,'FINANCIAL_HOLDS') FINANCIAL_HOLDS,
       BANINST1.F_ACCOUNT_BALANCE(SPRIDEN_PIDM) + BANINST1.F_MEMO_BALANCE(SPRIDEN_PIDM) NET_AMOUNT_DUE,
       me_account_objects.fz_net_past_due(c.spriden_pidm) TOTAL_AMOUNT_PAST_DUE,
       me_account_objects.fz_future_term_charges (C.SPRIDEN_PIDM) TOTAL_FUTURE_TERM_CHARGES,
       me_account_objects.fz_unbilled_charges (c.spriden_pidm) TOTAL_UNBILLED_CHARGES
  from SPRIDEN C
 WHERE c.spriden_change_ind is null
   and (EXISTS (SELECT 'X' FROM TBRACCD WHERE TBRACCD_PIDM = SPRIDEN_PIDM) OR
        EXISTS (SELECT 'X' FROM TBRDEPO WHERE TBRDEPO_PIDM = SPRIDEN_PIDM) OR
        EXISTS (SELECT 'X' FROM TBRMEMO WHERE TBRMEMO_PIDM = SPRIDEN_PIDM))
/

