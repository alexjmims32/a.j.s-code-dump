PROMPT CREATE OR REPLACE PACKAGE BODY me_reg_client
CREATE OR REPLACE package body me_reg_client is

  function fz_get_linked_courses(P_CRN       ssrlink.ssrlink_crn%type,
                                 P_TERM_CODE ssrlink.ssrlink_term_code%type)

   RETURN varchar2 as

    v_linked_courses varchar2(4000);

    cursor get_linked_courses is
      SELECT T.*
        FROM SSBSECT T
       WHERE T.SSBSECT_TERM_CODE = P_TERM_CODE
         AND T.SSBSECT_SUBJ_CODE =
             (SELECT SSBSECT_SUBJ_CODE
                FROM SSBSECT E
               WHERE E.SSBSECT_CRN = P_CRN
                 AND E.SSBSECT_TERM_CODE = P_TERM_CODE)
         AND T.SSBSECT_CRSE_NUMB =
             (SELECT SSBSECT_CRSE_NUMB
                FROM SSBSECT E
               WHERE E.SSBSECT_CRN = P_CRN
                 AND E.SSBSECT_TERM_CODE = P_TERM_CODE)
         AND SSBSECT_LINK_IDENT =
             (SELECT SSRLINK_LINK_CONN
                FROM SSRLINK
               WHERE SSRLINK_CRN = P_CRN
                 AND SSRLINK_TERM_CODE = P_TERM_CODE);

  BEGIN

    FOR R_DATA IN get_linked_courses LOOP

      v_linked_courses := R_DATA.SSBSECT_CRN || ',' || v_linked_courses;

    END LOOP;

    v_linked_courses := RTRIM(v_linked_courses, ',');
    if v_linked_courses is not null then
      RETURN RTRIM(v_linked_courses || '-' ||
                   me_account_objects.fz_get_detail_link('LINK'),
                   '-');
    else
      return null;
    end if;

  END;

  FUNCTION fz_check_biographic_info(P_STUDENT_ID SPRIDEN.SPRIDEN_ID%TYPE)
    RETURN CHAR IS

    v_bio_info char(1) := 'N';
    v_count    number := 0;
    v_pidm     number;

  BEGIN

    begin

      v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);

    exception
      when others then

        v_pidm := -100;

    end;

    V_COUNT := 1;

    IF V_COUNT > 0 THEN

      v_bio_info := 'Y';
    ELSE
      v_bio_info := 'N';

    END IF;

    RETURN v_bio_info;

  exception
    when others then

      RETURN v_bio_info;

  END;

  FUNCTION fz_check_grad_term(V_PIDM sgbstdn.sgbstdn_pidm%type,
                              P_TERM stvterm.stvterm_code%type) RETURN CHAR IS

    v_grad_date char(1) := 'N';
    v_exist     number := 0;

  BEGIN

    select count(1)
      into v_exist
      from sgbstdn
     where sgbstdn_pidm = V_PIDM
       and sgbstdn_term_code_grad >= P_TERM;

    IF v_exist > 0 THEN
      v_grad_date := 'Y';
    ELSE
      v_grad_date := 'N';
    END IF;

    --  RETURN v_grad_date;

    RETURN 'Y';

  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'Y';
  END;

  --
  -- function to determine if student is prevented from registering
  --   due to having open immunization tracking records
  -- used in registration processing (SFAREGS and SS)
  --
  -- ====================================================================
  FUNCTION fz_reg_prevented(p_pidm IN spriden.spriden_pidm%TYPE)
    RETURN varchar2 IS
    CURSOR prevent_c(pc_pidm spriden.spriden_pidm%TYPE) IS
      SELECT 'Y'
        FROM szritrk
       WHERE szritrk_pidm = pc_pidm
         AND szritrk_proof_waived_ind = 'N'
         AND szritrk_waived_date IS NULL
         AND szritrk_received_ind = 'N'
         AND szritrk_refused_ind = 'N'
         AND szritrk_parent_approved_ind = 'N'
         AND szritrk_administered_date IS NULL
         AND szritrk_proof_receive_date IS NULL
         AND szritrk_prevent_reg_ind = 'Y'
         AND NOT EXISTS
       (SELECT 'suspended'
                FROM szbitrk
               WHERE szbitrk_pidm = pc_pidm
                 AND szbitrk_suspend_restrict_ind = 'Y'
                 AND NVL(szbitrk_suspend_to_date, TRUNC(SYSDATE)) >
                     TRUNC(SYSDATE));

    prevent_ind VARCHAR2(1) := 'N';
  BEGIN
    OPEN prevent_c(p_pidm);
    FETCH prevent_c
      INTO prevent_ind;
    CLOSE prevent_c;
    IF prevent_ind = 'Y' THEN
      RETURN prevent_ind;
    ELSE
      RETURN 'N';
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 'EXCEPTION RAISED WHILE CHECKING IMMUNIZATION';
  END fz_reg_prevented;
  --------------------------------------
  PROCEDURE PZ_GET_DSP_DROP_ALLOWED(P_CRN       VARCHAR2,
                                   P_TERM_CODE VARCHAR2,
                                   P_PTRM_CODE VARCHAR2,
                                   P_RSTS_CODE VARCHAR2,
                                   P_DATE      DATE DEFAULT SYSDATE,
                                   P_ERROR_MSG OUT VARCHAR2) IS
  BEGIN
    IF NOT SZKADEF.f_DSP_drop_allowed(p_crn,
                                      p_term_code,
                                      P_PTRM_CODE,
                                      P_RSTS_CODE,
                                      P_DATE) THEN
       P_ERROR_MSG := bwcklibs.error_msg_table(-20610);
    END IF;
    EXCEPTION WHEN OTHERS THEN
      P_ERROR_MSG:=SQLERRM;
  END;

begin

  NULL;

end ME_REG_CLIENT;
/

