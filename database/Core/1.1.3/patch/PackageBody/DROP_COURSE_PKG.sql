PROMPT CREATE OR REPLACE PACKAGE BODY drop_course_pkg
CREATE OR REPLACE PACKAGE BODY drop_course_pkg AS

  v_debug boolean;
  procedure pz_drop_validations(p_pidm      in number,
                                p_term_code in varchar2,
                                p_crn       in ssbsect.ssbsect_crn%type,
                                p_error_msg out varchar2) is

    v_check    int;
    v_ptrm_chk varchar2(1);
    v_rsts     stvrsts.stvrsts_code%type;
    V_PTRM     VARCHAR2(10);

  begin

    v_ptrm_chk := me_valid.fz_check_sfrrsts(p_term_code, p_crn, 'D');

    begin
      select count(*)
        into v_check
        from sftregs
       where sftregs_pidm = p_pidm
         and sftregs_term_code = p_term_code
         and sftregs_crn = p_crn;
    exception
      when no_data_found then
        v_check := 0;
    end;

    if not v_ptrm_chk <> 'N' then
      p_error_msg := 'Not permitted to drop at this time';
    end if;

    if p_error_msg is null then

      if v_check = 0 then

        p_error_msg := p_error_msg ||
                       'Course not registered, it cannot be dropped.';

      ELSE
        select a.sftregs_rsts_code, A.SFTREGS_PTRM_CODE
          into v_rsts, V_PTRM
          from sftregs a
         where sftregs_pidm = p_pidm
           and sftregs_term_code = p_term_code
           and sftregs_crn = p_crn;

        if v_rsts not in ('RE', 'RW', 'WL') THEN
          p_error_msg := p_error_msg || 'Course cannot be dropped.';

        END IF;

      end if;

    end if;
    DBMS_OUTPUT.PUT_LINE('V_PTRM' || V_PTRM || 'v_rsts' || v_rsts);
    if p_error_msg is null then
      me_reg_client.PZ_GET_DSP_DROP_ALLOWED(P_CRN,
                                            p_term_code,
                                            V_PTRM,
                                            'DW',
                                            SYSDATE,
                                            p_error_msg);
      /*IF NOT SZKADEF.f_DSP_drop_allowed(p_crn,
                                        p_term_code,
                                        V_PTRM,
                                        'DW' \*v_rsts*\,
                                        SYSDATE) THEN
        P_ERROR_MSG := bwcklibs.error_msg_table(-20610);
      END IF;*/
    END IF;
  end;

  procedure pz_remove_drop_failures(p_drop_failures in sfkcurs.drop_problems_rec_tabtype,
                                    p_pidm          in spriden.spriden_pidm%type,
                                    p_term_code     in sftregs.sftregs_term_code%type) is

    v_failed_crn ssbsect.ssbsect_crn%type;
    v_error_link sftregs.sftregs_error_link%type;
    v_error_msg  varchar2(4000);

  begin
    IF v_debug THEN
      FOR i IN 1 .. p_drop_failures.COUNT LOOP

        dbms_output.put_line('d_f_term_code:' || p_drop_failures(i)
                             .term_code);
        dbms_output.put_line('d_f_crn:' || p_drop_failures(i).crn);
        dbms_output.put_line('d_f_subj:' || p_drop_failures(i).subj);
        dbms_output.put_line('d_f_crse:' || p_drop_failures(i).crse);
        dbms_output.put_line('d_f_sec:' || p_drop_failures(i).sec);
        dbms_output.put_line('d_f_ptrm_code:' || p_drop_failures(i)
                             .ptrm_code);
        dbms_output.put_line('d_f_rmsg_cde:' || p_drop_failures(i)
                             .rmsg_cde);
        dbms_output.put_line('d_f_message:' || p_drop_failures(i).message);
        dbms_output.put_line('d_f_start_date:' || p_drop_failures(i)
                             .start_date);
        dbms_output.put_line('d_f_comp_date:' || p_drop_failures(i)
                             .comp_date);
        dbms_output.put_line('d_f_rsts_date:' || p_drop_failures(i)
                             .rsts_date);
        dbms_output.put_line('d_f_dunt_code:' || p_drop_failures(i)
                             .dunt_code);
        dbms_output.put_line('d_f_drop_code:' || p_drop_failures(i)
                             .drop_code);
        dbms_output.put_line('d_f_drop_conn:' || p_drop_failures(i)
                             .connected_crns);

        dbms_output.put_line(bwcklibs.f_connected_crn_message(p_drop_failures(i)
                                                              .rmsg_cde,
                                                              p_drop_failures(i)
                                                              .message,
                                                              p_drop_failures(i)
                                                              .connected_crns));
      END LOOP;

    END IF;

    FOR i IN 1 .. p_drop_failures.COUNT LOOP

      dbms_output.put_line('d_p_crn:' || p_drop_failures(i).crn);

      v_failed_crn := p_drop_failures(i).crn;

      begin

        select sftregs_error_link
          into v_error_link
          from sftregs
         where sftregs_pidm = p_pidm
           and sftregs_crn = p_drop_failures(i).crn
           and sftregs_term_code = p_term_code;
        if v_debug then
          dbms_output.put_line('v_error_link:' || v_error_link);
        end if;
      exception
        when others then

          v_error_link := null;

      end;

      v_error_msg := bwcklibs.f_connected_crn_message(p_drop_failures(i)
                                                      .rmsg_cde,
                                                      p_drop_failures(i)
                                                      .message,
                                                      p_drop_failures(i)
                                                      .connected_crns);
      if v_debug then
        dbms_output.put_line('v_error_msg:' || v_error_msg);
      end if;
      update sftregs
         set SFTREGS_ERROR_FLAG = 'F',
             SFTREGS_MESSAGE    = v_error_msg,
             SFTREGS_REC_STAT   = 'Q'
       where sftregs_pidm = p_pidm
         and sftregs_error_link = v_error_link
         and sftregs_term_code = p_term_code;

    END LOOP;
  exception
    when others then

      dbms_output.put_line(sqlerrm);

  end;

  procedure pz_drop_course_old(p_student_id in SPRIDEN.SPRIDEN_ID%TYPE,
                               p_term_code  in varchar2,
                               p_crn        in varchar2,
                               p_error_msg  out varchar2,
                               p_response   out varchar2,
                               p_debug      in varchar2 default 'N') is

    V_ERROR varchar2(4000);
    v_pidm  number;

    v_styp_code   varchar2(10);
    v_tmst_code   varchar2(10);
    v_tmst_ind    varchar2(10);
    v_sftregs_rec sftregs%rowtype;
    v_error_out   varchar2(500);
    v_tmst_out    varchar2(500);
    v_add_date    sfrstcr.sfrstcr_add_date%type;

    fa_return_status number;
    save_act_date    VARCHAR2(200);

    drop_problems   sfkcurs.drop_problems_rec_tabtype;
    drop_failures   sfkcurs.drop_problems_rec_tabtype;
    capp_tech_error VARCHAR2(40);
    term_in         OWA_UTIL.ident_arr;
    sobterm_row     SOBTERM%ROWTYPE;
    CURSOR C_CRN IS
      SELECT TRIM(REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL)) CRN
        FROM DUAL
      CONNECT BY REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL) IS NOT NULL;

  BEGIN

    if p_debug = 'Y' then
      v_debug := TRUE;
    else
      v_debug := FALSE;
    end if;

    begin
      select baninst1.gb_common.f_get_pidm(p_student_id)
        into v_pidm
        from dual;
    exception
      when others then
        p_error_msg := sqlerrm || ': Unable to get student details';
    end;

    if p_error_msg is not null then
      return;
    end if;

    p_error_msg := me_valid.f_reg_access(v_pidm,
                                         p_term_code,
                                         'DIRECT',
                                         'S' || v_pidm,
                                         sysdate,
                                         'N');

    IF p_error_msg IS NOT NULL THEN
      return;
    END IF;

    /* Below is taken care in sfkfunc.f_registration_access i.e me_valid.f_reg_access */

    /*sfkmods.p_delete_sftregs_by_pidm_term(v_pidm, p_term_code);
    sfkmods.p_insert_sftregs_from_stcr(v_pidm, p_term_code, SYSDATE);*/

    delete mobedu_trash
     where student_id = p_student_id
       and TERM_CODE = p_term_code;

    GB_COMMON.P_COMMIT;

    bwcklibs.p_initvalue(v_pidm, p_term_code, '', '', '', '');

    FOR R_CRN IN C_CRN LOOP

      V_ERROR := NULL;

      insert into mobedu_trash
        (STUDENT_ID,
         STUDENT_PIDM,
         TERM_CODE,
         CRN,
         STATUS,
         ERROR_MSG,
         RSTS_CODE)
      values
        (p_student_id, v_pidm, p_term_code, R_CRN.CRN, null, null, 'DW');

      pz_drop_validations(v_pidm, p_term_code, R_CRN.CRN, V_ERROR);

      IF v_debug THEN
        dbms_output.put_line('CRN:' || R_CRN.CRN || ';Error:' || V_ERROR);
      END IF;

      IF V_ERROR IS NULL THEN
        bwcklibs.p_del_sftregs(v_pidm, p_term_code, R_CRN.CRN, 'DW', 'C');
      ELSE

        update sftregs
           set SFTREGS_ERROR_FLAG = 'F',
               SFTREGS_MESSAGE    = V_ERROR,
               SFTREGS_REC_STAT   = 'Q'
         where sftregs_pidm = v_pidm
           and sftregs_crn = R_CRN.CRN
           and sftregs_term_code = p_term_code;

        update mobedu_trash
           set status = 'F', ERROR_MSG = V_ERROR
         where student_id = p_student_id
           and TERM_CODE = p_term_code
           and crn = R_CRN.CRN;

      END IF;

    END LOOP;

    term_in(1) := p_term_code;

    -- Validations start
    me_bwckregs.p_allcrsechk(term_in(1),
                             'ADD_DROP',
                             capp_tech_error,
                             drop_problems,
                             drop_failures,
                             term_in);

    IF v_debug THEN
      FOR i IN 1 .. drop_problems.COUNT LOOP
        dbms_output.put_line('d_p_term_code:' || drop_problems(i)
                             .term_code);
        dbms_output.put_line('d_p_crn:' || drop_problems(i).crn);
        dbms_output.put_line('d_p_subj:' || drop_problems(i).subj);
        dbms_output.put_line('d_p_crse:' || drop_problems(i).crse);
        dbms_output.put_line('d_p_sec:' || drop_problems(i).sec);
        dbms_output.put_line('d_p_ptrm_code:' || drop_problems(i)
                             .ptrm_code);
        dbms_output.put_line('d_p_rmsg_cde:' || drop_problems(i).rmsg_cde);
        dbms_output.put_line('d_p_message:' || drop_problems(i).message);
        dbms_output.put_line('d_p_start_date:' || drop_problems(i)
                             .start_date);
        dbms_output.put_line('d_p_comp_date:' || drop_problems(i)
                             .comp_date);
        dbms_output.put_line('d_p_rsts_date:' || drop_problems(i)
                             .rsts_date);
        dbms_output.put_line('d_p_dunt_code:' || drop_problems(i)
                             .dunt_code);
        dbms_output.put_line('d_p_drop_code:' || drop_problems(i)
                             .drop_code);
        dbms_output.put_line('d_p_drop_conn:' || drop_problems(i)
                             .connected_crns);
        NULL;
      END LOOP;
    END IF;

    IF v_debug THEN
      FOR i IN 1 .. drop_failures.COUNT LOOP

        dbms_output.put_line('d_p_term_code:' || drop_failures(i)
                             .term_code);
        dbms_output.put_line('d_p_crn:' || drop_failures(i).crn);
        dbms_output.put_line('d_p_subj:' || drop_failures(i).subj);
        dbms_output.put_line('d_p_crse:' || drop_failures(i).crse);
        dbms_output.put_line('d_p_sec:' || drop_failures(i).sec);
        dbms_output.put_line('d_p_ptrm_code:' || drop_failures(i)
                             .ptrm_code);
        dbms_output.put_line('d_p_rmsg_cde:' || drop_failures(i).rmsg_cde);
        dbms_output.put_line('d_p_message:' || drop_failures(i).message);
        dbms_output.put_line('d_p_start_date:' || drop_failures(i)
                             .start_date);
        dbms_output.put_line('d_p_comp_date:' || drop_failures(i)
                             .comp_date);
        dbms_output.put_line('d_p_rsts_date:' || drop_failures(i)
                             .rsts_date);
        dbms_output.put_line('d_p_dunt_code:' || drop_failures(i)
                             .dunt_code);
        dbms_output.put_line('d_p_drop_code:' || drop_failures(i)
                             .drop_code);
        dbms_output.put_line('d_p_drop_conn:' || drop_failures(i)
                             .connected_crns);

        dbms_output.put_line(bwcklibs.f_connected_crn_message(drop_failures(i)
                                                              .rmsg_cde,
                                                              drop_failures(i)
                                                              .message,
                                                              drop_failures(i)
                                                              .connected_crns));
      END LOOP;

    END IF;

    -- Validations end

    begin

      pz_remove_drop_failures(drop_failures, v_pidm, p_term_code);

    exception
      when others then

        IF v_debug THEN
          dbms_output.put_line(SQLERRM);
        END IF;
        p_error_msg := 'Drop failures exist and failed to remove from the list;' ||
                       sqlerrm;
    end;

    if p_error_msg is not null then

      GB_COMMON.P_ROLLBACK;

      update mobedu_trash
         set status = 'F', ERROR_MSG = p_error_msg
       where student_id = p_student_id
         and TERM_CODE = p_term_code
         and status is null;
      GB_COMMON.P_COMMIT;

      return;

    end if;

    begin

      IF v_debug THEN
        dbms_output.put_line('updating record in sfrstcr');
      END IF;

      baninst1.SFKEDIT.p_update_regs(pidm_in            => v_pidm,
                                     term_in            => p_term_code,
                                     reg_date_in        => v_add_date,
                                     clas_code_in       => '01',
                                     styp_code_in       => v_styp_code,
                                     capc_severity_in   => 'F',
                                     tmst_calc_ind_in   => 'Y',
                                     tmst_maint_ind_in  => v_tmst_ind,
                                     tmst_code_in       => v_tmst_code,
                                     drop_last_class_in => null,
                                     system_in          => 'WA',
                                     error_rec_out      => v_sftregs_rec,
                                     error_flag_out     => v_error_out,
                                     tmst_flag_out      => v_tmst_out);

      update (select t.status,
                     t.error_msg,
                     R.SFTREGS_ERROR_FLAG,
                     R.sftregs_message
                from mobedu_trash T, sftregs R
               where t.student_pidm = R.sftregs_pidm
                 and t.crn = sftregs_crn
                 and t.term_code = R.sftregs_term_code) UP
         set UP.status    = decode(SFTREGS_ERROR_FLAG, 'F', 'F', 'Y'),
             UP.error_msg = sftregs_message;

      fa_return_status := 0;

      /* Fee assessment after drop */
      BEGIN
        bwcklibs.p_getsobterm(p_term_code, sobterm_row);
        IF sobterm_row.sobterm_fee_assess_vr = 'Y' AND
           sobterm_row.sobterm_fee_assessment = 'Y' THEN
          SFKFEES.p_processfeeassessment(p_term_code,
                                         v_pidm,
                                         SYSDATE,
                                         SYSDATE,
                                         'R',
                                         'Y',
                                         'BWCKREGS',
                                         'Y',
                                         save_act_date,
                                         'N',
                                         fa_return_status);

        END IF;
      END;
    exception
      when others then
        p_error_msg := 'Exception in p_update_regs:' || sqlerrm;
    end;

    if p_error_msg is not null then

      GB_COMMON.P_ROLLBACK;
      update mobedu_trash
         set status = 'F', ERROR_MSG = p_error_msg
       where student_id = p_student_id
         and TERM_CODE = p_term_code
         and status is null;
      GB_COMMON.P_COMMIT;
      return;
    else
      GB_COMMON.P_COMMIT;
      P_RESPONSE := 'Course(s) dropped successfully.';

    end if;

  end;
  PROCEDURE pz_drop_course(p_student_id in SPRIDEN.SPRIDEN_ID%TYPE,
                           p_term_code  in varchar2,
                           p_crn        in varchar2,
                           p_error_msg  out varchar2,
                           p_response   out varchar2,
                           p_debug      in varchar2 default 'N') IS

    V_PIDM              NUMBER;
    V_DROP_RSTS_CODE    VARCHAR2(2);
    V_ERROR             VARCHAR2(4000);
    TERM_IN             OWA_UTIL.ident_arr;
    DROP_PROBLEMS       sfkcurs.drop_problems_rec_tabtype;
    DROP_FAILURES       sfkcurs.drop_problems_rec_tabtype;
    FA_RETURN_STATUS    NUMBER;
    SAVE_ACT_DATE       VARCHAR2(200);
    CAPP_TECH_ERROR     VARCHAR2(40);
    V_TMST_OUT          VARCHAR2(500);
    V_ADD_DATE          sfrstcr.sfrstcr_add_date%type;
    V_STYP_CODE         VARCHAR2(10);
    V_TMST_CODE         VARCHAR2(10);
    V_TMST_IND          VARCHAR2(10);
    V_SFTREGS_REC       sftregs%rowtype;
    SOBTERM_ROW         SOBTERM%ROWTYPE;
    V_ERROR_OUT         varchar2(500);
    SFTREGS_REC         sftregs%ROWTYPE;
    V_ESTS_CODE         varchar2(10);
    v_etrm_min_hrs      sfbetrm.sfbetrm_min_hrs%TYPE;
    v_etrm_max_hrs_over sfbetrm.sfbetrm_mhrs_over%TYPE;
    may_drop_last       BOOLEAN;
    v_error_flag        varchar2(1);
    v_capp_error        varchar2(4);
    v_minh_error        varchar2(1);
    --
    /* err_term         OWA_UTIL.ident_arr;
    err_crn          OWA_UTIL.ident_arr;
    err_subj         OWA_UTIL.ident_arr;
    err_crse         OWA_UTIL.ident_arr;
    err_sec          OWA_UTIL.ident_arr;
    err_code         OWA_UTIL.ident_arr;
    err_levl         OWA_UTIL.ident_arr;
    err_cred         OWA_UTIL.ident_arr;
    err_gmod         OWA_UTIL.ident_arr;*/
    --
    clas_code  SGRCLSR.SGRCLSR_CLAS_CODE%TYPE;
    etrm_done  BOOLEAN := FALSE;
    V_PTRM_CHK VARCHAR2(10);
    V_DEBUG    BOOLEAN;
  BEGIN
    IF P_DEBUG = 'Y' THEN
      V_DEBUG := TRUE;
    ELSE
      V_DEBUG := FALSE;
    END IF;
    -- VALIDATE PIDM
    begin
      select baninst1.gb_common.f_get_pidm(p_student_id)
        into v_pidm
        from dual;
    exception
      when others then
        p_error_msg := sqlerrm || ': Unable to get student details';
        GOTO QUITTOPROGRAM;
    end;
    -- Perform registration checks
    begin
      me_valid.pz_registration_checks(p_student_id => p_student_id,
                                      p_term       => p_term_code,
                                      p_error_msg  => V_ERROR);
      if v_error is not null then
        p_error_msg := V_ERROR;
        GOTO QUITTOPROGRAM;
      end if;
    exception
      when others then
        null;
    end;
    --validate if the student can drop his last course

    -- VALIDATE STUDENT ACCESS STILL GOOD
    P_ERROR_MSG := ME_VALID.F_REG_ACCESS(V_PIDM,
                                         P_TERM_CODE,
                                         'PROXY',
                                         'S' || V_PIDM,
                                         SYSDATE,
                                         'N');

    IF P_ERROR_MSG IS NOT NULL THEN
      RETURN;
    END IF;
    -- SET GLOBAL VARIABLES
    BWCKLIBS.P_INITVALUE(V_PIDM, P_TERM_CODE, '', '', '', '');
    -- DELETE PREVIOUS RECORDS FROM TRASH TABLE
    DELETE MOBEDU_TRASH
     WHERE STUDENT_ID = P_STUDENT_ID
       AND TERM_CODE = P_TERM_CODE;
    COMMIT;
    DBMS_OUTPUT.put_line('before etrm' || v_pidm);
    begin
      ME_bwckcoms.p_regs_etrm_chk(V_PIDM, P_TERM_CODE, CLAS_CODE);
      etrm_done := TRUE;
      gb_common.p_commit;
    exception
      when others then
        null;
    end;

    begin
      SELECT sfbetrm_ests_code, sfbetrm_min_hrs, sfbetrm_mhrs_over
        INTO V_ESTS_CODE, v_etrm_min_hrs, v_etrm_max_hrs_over
        FROM SFBETRM
       WHERE SFBETRM_TERM_CODE = p_term_code
         AND SFBETRM_PIDM = V_PIDM;
    exception
      when others then
        p_error_msg := sqlerrm;
    end;
    -- LOAD CRNS AND RSTS CODES INTO TRASH TABLE
    BEGIN
      INSERT INTO MOBEDU_TRASH
        SELECT p_student_id ID,
               v_pidm PIDM,
               p_term_code TERM_CODE,
               substr(TRIM(REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL)),
                      1,
                      instr(TRIM(REGEXP_SUBSTR(trim(p_crn),
                                               '[^,]+',
                                               1,
                                               LEVEL)),
                            '-') - 1) CRN,
               NULL STATUS,
               NULL ERROR_MSG,
               substr(TRIM(REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL)),
                      instr(TRIM(REGEXP_SUBSTR(trim(p_crn),
                                               '[^,]+',
                                               1,
                                               LEVEL)),
                            '-') + 1,
                      2) NEW_RSTS_CODE
          FROM DUAL
        CONNECT BY REGEXP_SUBSTR(trim(p_crn), '[^,]+', 1, LEVEL) IS NOT NULL;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        p_error_msg := sqlerrm || ': Unable to get course details';
        GOTO QUITTOPROGRAM;
    END;
    -- GET RSTS CODE FOR REGISTRATION DROP
    V_DROP_RSTS_CODE := NVL(SUBSTR(ME_VALID.f_stu_getwebregsrsts('D'), 1, 2),
                            'DW');
    -- PERFORM PRE VALIDATIONS

    -- PERFORM WEB DROP
    FOR R_DROP_CRNS IN (SELECT CRN
                          FROM MOBEDU_TRASH
                         WHERE STUDENT_PIDM = V_PIDM
                           AND TERM_CODE = p_term_code
                           AND RSTS_CODE = V_DROP_RSTS_CODE) LOOP
      BEGIN
        -- PERFORM DROP VALIDATIONS FOR CRN
        drop_course_pkg.pz_drop_validations(V_PIDM,
                                            P_TERM_CODE,
                                            R_DROP_CRNS.CRN,
                                            V_ERROR);

        --UPDATE SFTREGS IN PREPARATION FOR DELETION
        IF V_ERROR IS NULL THEN
          bwcklibs.p_del_sftregs(V_PIDM,
                                 P_TERM_CODE,
                                 R_DROP_CRNS.CRN,
                                 'DW',
                                 'C');
        ELSE
          -- UPDATE SFTREGS WITH ERROR MESSAGE AND FLAG
          UPDATE SFTREGS
             SET SFTREGS_ERROR_FLAG = 'F',
                 SFTREGS_MESSAGE    = V_ERROR,
                 SFTREGS_REC_STAT   = 'Q'
           WHERE SFTREGS_PIDM = V_PIDM
             AND SFTREGS_CRN = R_DROP_CRNS.CRN
             AND SFTREGS_TERM_CODE = P_TERM_CODE;
          -- UPDATE TRASH TABLE
          UPDATE MOBEDU_TRASH
             SET STATUS = 'F', ERROR_MSG = V_ERROR
           WHERE STUDENT_ID = P_STUDENT_ID
             AND TERM_CODE = P_TERM_CODE
             AND CRN = R_DROP_CRNS.CRN;

        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.put_line('ERROR IN DROP LOOP ' || SQLERRM);
      END;
    END LOOP;
    TERM_IN(1) := P_TERM_CODE;

    -- PERFORM VALIDATION FOR ALL DROP COURSES

    IF P_ERROR_MSG IS NOT NULL THEN

      GB_COMMON.P_ROLLBACK;

      UPDATE MOBEDU_TRASH
         SET STATUS = 'F', ERROR_MSG = P_ERROR_MSG
       WHERE STUDENT_ID = P_STUDENT_ID
         AND TERM_CODE = P_TERM_CODE
         AND STATUS IS NULL;
      GB_COMMON.P_COMMIT;

      --RETURN;

    END IF;

    FOR R_UPD_CRSE IN (SELECT CRN, RSTS_CODE
                         FROM MOBEDU_TRASH
                        WHERE STUDENT_PIDM = V_PIDM
                          AND TERM_CODE = p_term_code
                          AND RSTS_CODE <> V_DROP_RSTS_CODE) LOOP
      BEGIN
        V_ERROR                       := NULL;
        sftregs_rec.sftregs_crn       := R_UPD_CRSE.CRN;
        sftregs_rec.sftregs_pidm      := V_PIDM;
        sftregs_rec.sftregs_term_code := P_TERM_CODE;
        sftregs_rec.sftregs_rsts_code := R_UPD_CRSE.RSTS_CODE;

        --
        -- Get the section information.
        -- ===================================================
        bwckregs.p_getsection(sftregs_rec.sftregs_term_code,
                              sftregs_rec.sftregs_crn,
                              sftregs_rec.sftregs_sect_subj_code,
                              sftregs_rec.sftregs_sect_crse_numb,
                              sftregs_rec.sftregs_sect_seq_numb);
        BEGIN
          BEGIN
            ME_bwckregs.p_updcrse(sftregs_rec);
          EXCEPTION
            WHEN OTHERS THEN
              V_ERROR := BWCKLIBS.error_msg_table(SQLCODE);
              UPDATE SFTREGS
                 SET SFTREGS_ERROR_FLAG = 'F',
                     SFTREGS_MESSAGE    = V_ERROR,
                     SFTREGS_REC_STAT   = 'Q'
               WHERE SFTREGS_PIDM = V_PIDM
                 AND SFTREGS_CRN = R_UPD_CRSE.CRN
                 AND SFTREGS_TERM_CODE = P_TERM_CODE;
              -- UPDATE TRASH TABLE
              UPDATE MOBEDU_TRASH
                 SET STATUS = 'F', ERROR_MSG = V_ERROR
               WHERE STUDENT_ID = P_STUDENT_ID
                 AND TERM_CODE = P_TERM_CODE
                 AND CRN = R_UPD_CRSE.CRN;

          END;
        EXCEPTION
          WHEN OTHERS THEN
            V_ERROR := SQLERRM; --BWCKLIBS.error_msg_table(SQLCODE);
            UPDATE SFTREGS
               SET SFTREGS_ERROR_FLAG = 'F',
                   SFTREGS_MESSAGE    = V_ERROR,
                   SFTREGS_REC_STAT   = 'Q'
             WHERE SFTREGS_PIDM = V_PIDM
               AND SFTREGS_CRN = R_UPD_CRSE.CRN
               AND SFTREGS_TERM_CODE = P_TERM_CODE;
            -- UPDATE TRASH TABLE
            UPDATE MOBEDU_TRASH
               SET STATUS = 'F', ERROR_MSG = V_ERROR
             WHERE STUDENT_ID = P_STUDENT_ID
               AND TERM_CODE = P_TERM_CODE
               AND CRN = R_UPD_CRSE.CRN;
        END;
        V_PTRM_CHK := me_valid.fz_check_sfrrsts(P_TERM_CODE,
                                                R_UPD_CRSE.CRN,
                                                R_UPD_CRSE.RSTS_CODE);
        IF V_PTRM_CHK = 'N' THEN
          V_ERROR := 'Registration changes are not allowed. Course status dates not within range for part of term.';
          UPDATE SFTREGS
             SET SFTREGS_ERROR_FLAG = 'F',
                 SFTREGS_MESSAGE    = V_ERROR,
                 SFTREGS_REC_STAT   = 'Q'
           WHERE SFTREGS_PIDM = V_PIDM
             AND SFTREGS_CRN = R_UPD_CRSE.CRN
             AND SFTREGS_TERM_CODE = P_TERM_CODE;
          -- UPDATE TRASH TABLE
          UPDATE MOBEDU_TRASH
             SET STATUS = 'F', ERROR_MSG = V_ERROR
           WHERE STUDENT_ID = P_STUDENT_ID
             AND TERM_CODE = P_TERM_CODE
             AND CRN = R_UPD_CRSE.CRN;
          --P_ERROR := 'Not permitted to ' || V_RSTS_DESC || ' at this time';
          --GOTO QUITTOPROGRAM;
        END IF;
      END;
    END LOOP;
    --============================================================================================================
    --============================================================================================================
    --====================================== COMMOM FOR BOTH DROP AND WITHDRAW    ================================
    --============================================================================================================
    --============================================================================================================
    me_bwckregs.p_allcrsechk(TERM_IN(1),
                             'ADD_DROP',
                             CAPP_TECH_ERROR,
                             DROP_PROBLEMS,
                             DROP_FAILURES,
                             TERM_IN);

    IF v_debug THEN
      FOR i IN 1 .. drop_problems.COUNT LOOP
        dbms_output.put_line('d_p_term_code:' || drop_problems(i)
                             .term_code);
        dbms_output.put_line('d_p_crn:' || drop_problems(i).crn);
        dbms_output.put_line('d_p_subj:' || drop_problems(i).subj);
        dbms_output.put_line('d_p_crse:' || drop_problems(i).crse);
        dbms_output.put_line('d_p_sec:' || drop_problems(i).sec);
        dbms_output.put_line('d_p_ptrm_code:' || drop_problems(i)
                             .ptrm_code);
        dbms_output.put_line('d_p_rmsg_cde:' || drop_problems(i).rmsg_cde);
        dbms_output.put_line('d_p_message:' || drop_problems(i).message);
        dbms_output.put_line('d_p_start_date:' || drop_problems(i)
                             .start_date);
        dbms_output.put_line('d_p_comp_date:' || drop_problems(i)
                             .comp_date);
        dbms_output.put_line('d_p_rsts_date:' || drop_problems(i)
                             .rsts_date);
        dbms_output.put_line('d_p_dunt_code:' || drop_problems(i)
                             .dunt_code);
        dbms_output.put_line('d_p_drop_code:' || drop_problems(i)
                             .drop_code);
        dbms_output.put_line('d_p_drop_conn:' || drop_problems(i)
                             .connected_crns);
        NULL;
      END LOOP;
    END IF;

    IF v_debug THEN
      FOR i IN 1 .. drop_failures.COUNT LOOP

        dbms_output.put_line('d_f_term_code:' || drop_failures(i)
                             .term_code);
        dbms_output.put_line('d_f_crn:' || drop_failures(i).crn);
        dbms_output.put_line('d_f_subj:' || drop_failures(i).subj);
        dbms_output.put_line('d_f_crse:' || drop_failures(i).crse);
        dbms_output.put_line('d_f_sec:' || drop_failures(i).sec);
        dbms_output.put_line('d_f_ptrm_code:' || drop_failures(i)
                             .ptrm_code);
        dbms_output.put_line('d_f_rmsg_cde:' || drop_failures(i).rmsg_cde);
        dbms_output.put_line('d_f_message:' || drop_failures(i).message);
        dbms_output.put_line('d_f_start_date:' || drop_failures(i)
                             .start_date);
        dbms_output.put_line('d_f_comp_date:' || drop_failures(i)
                             .comp_date);
        dbms_output.put_line('d_f_rsts_date:' || drop_failures(i)
                             .rsts_date);
        dbms_output.put_line('d_f_dunt_code:' || drop_failures(i)
                             .dunt_code);
        dbms_output.put_line('d_f_drop_code:' || drop_failures(i)
                             .drop_code);
        dbms_output.put_line('d_f_drop_conn:' || drop_failures(i)
                             .connected_crns);

        dbms_output.put_line(bwcklibs.f_connected_crn_message(drop_failures(i)
                                                              .rmsg_cde,
                                                              drop_failures(i)
                                                              .message,
                                                              drop_failures(i)
                                                              .connected_crns));
      END LOOP;

    END IF;
    BEGIN
      IF v_debug THEN
        dbms_output.put_line('calling remove_drop proc');
      end if;
      drop_course_pkg.pz_remove_drop_failures(DROP_PROBLEMS,
                                              V_PIDM,
                                              P_TERM_CODE);
      drop_course_pkg.pz_remove_drop_failures(DROP_FAILURES,
                                              V_PIDM,
                                              P_TERM_CODE);

      UPDATE (SELECT T.STATUS,
                     T.ERROR_MSG,
                     R.SFTREGS_ERROR_FLAG,
                     R.SFTREGS_MESSAGE
                FROM MOBEDU_TRASH T, SFTREGS R
               WHERE T.STUDENT_PIDM = R.SFTREGS_PIDM
                 AND T.CRN = SFTREGS_CRN
                 AND T.TERM_CODE = R.SFTREGS_TERM_CODE
                 AND T.STUDENT_PIDM = V_PIDM
                 AND T.TERM_CODE = p_term_code) UP
         SET UP.STATUS    = DECODE(SFTREGS_ERROR_FLAG, 'F', 'F', 'Y'),
             UP.ERROR_MSG = SFTREGS_MESSAGE;
    EXCEPTION
      WHEN OTHERS THEN

        P_ERROR_MSG := 'Drop failures exist and failed to remove from the list;' ||
                       SQLERRM;
    END;
    --get sobterm record
    bwcklibs.p_getsobterm(P_TERM_CODE, SOBTERM_ROW);
    --validate last drop
    may_drop_last := sfkdrop.f_drop_last('W');
    IF CAPP_TECH_ERROR IS NULL THEN
      BEGIN
        ---------
        begin
          sfkedit.p_group_edits(pidm_in             => v_pidm,
                                term_in             => p_term_code,
                                dupl_severity_in    => SOBTERM_ROW.SOBTERM_DUPL_SEVERITY,
                                time_severity_in    => SOBTERM_ROW.SOBTERM_TIME_SEVERITY,
                                corq_severity_in    => SOBTERM_ROW.SOBTERM_CORQ_SEVERITY,
                                link_severity_in    => SOBTERM_ROW.SOBTERM_LINK_SEVERITY,
                                preq_severity_in    => SOBTERM_ROW.SOBTERM_PREQ_SEVERITY,
                                maxh_severity_in    => SOBTERM_ROW.SOBTERM_MAXH_SEVERITY,
                                minh_severity_in    => SOBTERM_ROW.SOBTERM_MINH_SEVERITY,
                                mexc_severity_in    => SOBTERM_ROW.SOBTERM_MEXC_SEVERITY,
                                mhrs_over_in        => v_etrm_max_hrs_over,
                                min_hrs_in          => v_etrm_min_hrs,
                                sessionid_in        => TO_CHAR(SYSDATE,
                                                               'YYYYMMDDHH24MISS'), --this was set to null, exception raised when this is null
                                system_in           => 'S',
                                error_flag_out      => v_error_flag,
                                capp_tech_error_out => v_capp_error,
                                minh_error_out      => v_minh_error);
        exception
          when others then

            IF v_debug THEN
              dbms_output.put_line('Error in group edits-' || sqlerrm); --
            end if;

        end;
        if v_minh_error = 'Y' then
          p_error_msg := 'A minimum hours restriction has prevented your transaction from being processed.';
          GOTO QUITTOPROGRAM;
        end if;
        ---------
        baninst1.SFKEDIT.p_update_regs(PIDM_IN            => V_PIDM,
                                       TERM_IN            => P_TERM_CODE,
                                       REG_DATE_IN        => V_ADD_DATE,
                                       CLAS_CODE_IN       => '01',
                                       STYP_CODE_IN       => V_STYP_CODE,
                                       CAPC_SEVERITY_IN   => SOBTERM_ROW.SOBTERM_CAPC_SEVERITY,
                                       TMST_CALC_IND_IN   => SOBTERM_ROW.SOBTERM_TMST_CALC_IND,
                                       TMST_MAINT_IND_IN  => V_TMST_IND,
                                       TMST_CODE_IN       => V_TMST_CODE,
                                       DROP_LAST_CLASS_IN => may_drop_last,
                                       SYSTEM_IN          => 'WA',
                                       ERROR_REC_OUT      => V_SFTREGS_REC,
                                       ERROR_FLAG_OUT     => V_ERROR_OUT,
                                       TMST_FLAG_OUT      => V_TMST_OUT);
        dbms_output.put_line('v_error' || V_ERROR_OUT);
        --p_error_msg:=V_ERROR_OUT;
        if V_ERROR_OUT = 'L' then
          p_error_msg := 'You cannot drop your last class. Please contact the Registrar''s office.';
          GOTO QUITTOPROGRAM;
        end if;
        /* IF V_ERROR_OUT = 'D'
        THEN
           \* sfrstcr row missing - system problem *\
         p_error_msg:=  twbkwbis.f_getparam (V_PIDM, 'ERROR_FLAG', 'D');

        ELSIF V_ERROR_OUT = 'L'
        THEN
          dbms_output.put_line('v_error=L');
           \* last class drop attempted *\
          p_error_msg:= twbkwbis.f_getparam (V_PIDM, 'ERROR_FLAG', 'L');
         end if;  */
        --
        /* ME_bwckcoms.p_problems(TERM_IN,
        err_term,
        err_crn,
        err_subj,
        err_crse,
        err_sec,
        err_code,
        err_levl,
        err_cred,
        err_gmod,
        drop_problems,
        drop_failures);*/
        --
        UPDATE (SELECT T.STATUS,
                       T.ERROR_MSG,
                       R.SFTREGS_ERROR_FLAG,
                       R.SFTREGS_MESSAGE
                  FROM MOBEDU_TRASH T, SFTREGS R
                 WHERE T.STUDENT_PIDM = R.SFTREGS_PIDM
                   AND T.CRN = SFTREGS_CRN
                   AND T.TERM_CODE = R.SFTREGS_TERM_CODE
                   AND T.STUDENT_PIDM = V_PIDM
                   AND T.TERM_CODE = p_term_code
                   AND T.STATUS <> 'F') UP
           SET UP.STATUS    = DECODE(SFTREGS_ERROR_FLAG, 'F', 'F', 'Y'),
               UP.ERROR_MSG = SFTREGS_MESSAGE;
      EXCEPTION
        WHEN OTHERS THEN
          p_error_msg := 'Failed Due to' || sqlerrm;
          DBMS_OUTPUT.put_line('ERROR IN FINAL ' || SQLERRM);
      END;
    END IF;

    --PERFORM FEE ASSESSMENT
    FA_RETURN_STATUS := 0;
    BEGIN

      IF SOBTERM_ROW.SOBTERM_FEE_ASSESS_VR = 'Y' AND
         SOBTERM_ROW.SOBTERM_FEE_ASSESSMENT = 'Y' THEN
        SFKFEES.P_PROCESSFEEASSESSMENT(P_TERM_CODE,
                                       V_PIDM,
                                       SYSDATE,
                                       SYSDATE,
                                       'R',
                                       'Y',
                                       'BWCKREGS',
                                       'Y',
                                       SAVE_ACT_DATE,
                                       'N',
                                       FA_RETURN_STATUS);

      END IF;
    END;
    --COMMIT HANDLING PENDING

    <<QUITTOPROGRAM>>
    IF P_ERROR_MSG IS NULL THEN

      GB_COMMON.P_COMMIT;
      p_response := 'Course Drop/Withdraw Successfull.';
    ELSE
      GB_COMMON.P_ROLLBACK;
    END IF;
    DBMS_OUTPUT.put_line(p_debug);
  END pz_drop_course;
begin

  v_debug := FALSE;

end drop_course_pkg;
/

