create or replace view acct_summ_vw as
select s.category, s.description, s.amount charge, v.amount payment,s.student_id student_id, S.balance
  from account_summary_view s
  left join account_summary_view v
    on s.student_id = v.student_id
   and s.category = v.category
   and v.tbbdetc_type_ind = 'P'
 where s.tbbdetc_type_ind = 'C'
union all
select s.category, s.description, v.amount charge, s.amount payment,s.student_id student_id, S.balance
  from account_summary_view s
  left join account_summary_view v
    on s.student_id = v.student_id
   and s.category = v.category
   and v.tbbdetc_type_ind = 'C'
 where s.tbbdetc_type_ind = 'P';

