PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start DO.sql"   
PROMPT ====================================================

spool DO.log
@@MOBEDU\Synonyms.sql
@@MOBEDU\Types.sql
@@MOBEDU\Type\T_ARRAY_TYPE.sql
prompt
prompt Creating table ADMIN_PRIV
prompt =========================
prompt
@@MOBEDU\Table\admin_priv.sql
prompt
prompt Creating table ADMIN_ROLE
prompt =========================
prompt
@@MOBEDU\Table\admin_role.sql
prompt
prompt Creating table ADMIN_USER
prompt =========================
prompt
@@MOBEDU\Table\admin_user.sql
prompt
prompt Creating table CAMPUS
prompt =====================
prompt
@@MOBEDU\Table\campus.sql
prompt
prompt Creating table EMERGENCY_CONTACTS
prompt =================================
prompt
@@MOBEDU\Table\emergency_contacts.sql
prompt
prompt Creating table EMERGENCY_CONTACTS_AUDIT
prompt =======================================
prompt
@@MOBEDU\Table\emergency_contacts_audit.sql
prompt
prompt Creating table FAQ
prompt ==================
prompt
@@MOBEDU\Table\faq.sql
prompt
prompt Creating table FEEDBACK
prompt =======================
prompt
@@MOBEDU\Table\feedback.sql
prompt
prompt Creating table FEEDBACK_DATA
prompt ============================
prompt
@@MOBEDU\Table\feedback_data.sql
prompt
prompt Creating table FEEDS
prompt ====================
prompt
@@MOBEDU\Table\feeds.sql
prompt
prompt Creating table FEEDS_AUDIT
prompt ==========================
prompt
@@MOBEDU\Table\feeds_audit.sql
prompt
prompt Creating table HELP
prompt ===================
prompt
@@MOBEDU\Table\help.sql
prompt
prompt Creating table HELP_AUDIT
prompt =========================
prompt
@@MOBEDU\Table\help_audit.sql
prompt
prompt Creating table MAPS
prompt ===================
prompt
@@MOBEDU\Table\maps.sql
prompt
prompt Creating table MAPS_AUDIT
prompt =========================
prompt
@@MOBEDU\Table\maps_audit.sql
prompt
prompt Creating table MOBEDU_CART
prompt ==========================
prompt
@@MOBEDU\Table\mobedu_cart.sql
prompt
prompt Creating table MODULE
prompt =====================
prompt
@@MOBEDU\Table\module.sql
prompt
prompt Creating table MODULES
prompt ======================
prompt
@@MOBEDU\Table\modules.sql
prompt
prompt Creating table NOTICE
prompt =====================
prompt
@@MOBEDU\Table\notice.sql
prompt
prompt Creating table NOTICELOG
prompt ========================
prompt
@@MOBEDU\Table\noticelog.sql
prompt
prompt Creating table POPULATIONTYPE
prompt =============================
prompt
@@MOBEDU\Table\populationtype.sql
prompt
prompt Creating table NOTICEPOPULATION
prompt ===============================
prompt
@@MOBEDU\Table\noticepopulation.sql
prompt
prompt Creating table NOTICETYPE
prompt =========================
prompt
@@MOBEDU\Table\noticetype.sql
prompt
prompt Creating table PRIVILEGE
prompt ========================
prompt
@@MOBEDU\Table\privilege.sql
prompt
prompt Creating table PRIVILEGE_AUDIT
prompt ==============================
prompt
@@MOBEDU\Table\privilege_audit.sql
prompt
prompt Creating table ROLE
prompt ===================
prompt
@@MOBEDU\Table\role.sql
@@MOBEDU\Table\mobedu_trash.sql
@@MOBEDU\Sequence\FEEDBACK_SEQ.sql
@@MOBEDU\Sequence\NOTICE_SEQ.sql
@@MOBEDU\Package\ADD_COURSE_PKG_CORQ.sql
@@MOBEDU\Package\CART_PKG.sql
@@MOBEDU\Package\GOKODSF.sql
@@MOBEDU\Package\ME_ACCOUNT_OBJECTS.sql
@@MOBEDU\Package\ME_ALT_PIN_PKG.sql
@@MOBEDU\Package\ME_BWCKCOMS.sql
@@MOBEDU\Package\ME_BWCKGENS.sql
@@MOBEDU\Package\ME_BWCKREGS.sql
@@MOBEDU\Package\ME_REG_UTILS.sql
@@MOBEDU\Package\ME_VALID.sql
@@MOBEDU\Package\ME_WITHDRAWL.sql
@@MOBEDU\Package\Z_CM_MOBILE_CAMPUS.sql
@@MOBEDU\Procedure\PZ_DROP_COURSE.sql
@@MOBEDU\Procedure\PZ_ME_AUTHENTICATION.sql
@@MOBEDU\View\ACCOUNT_SUMMARY_VIEW.sql
@@MOBEDU\View\ACCT_SUMM_VW.sql
@@MOBEDU\View\CART_DETAILS_VW.sql
@@MOBEDU\View\CHARGE_PAY_DETAIL_VW.sql
@@MOBEDU\View\CONTACT_VW.sql
@@MOBEDU\View\TERMS_TO_REGISTER_VW.sql
@@MOBEDU\View\COURSE_SEARCH_VW.sql
@@MOBEDU\View\CRSE_MEET_VW.sql
@@MOBEDU\View\GENERAL_PERSON_VW.sql
@@MOBEDU\View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@MOBEDU\View\MOBEDU_HOLIDAY_VW.sql
@@MOBEDU\View\PERSON_VW.sql
@@MOBEDU\View\STUDENT_ACCOUNT_DETAIL.sql
@@MOBEDU\View\STU_CURCULAM_INFO.sql
@@MOBEDU\View\STU_HOLD_INFO.sql
@@MOBEDU\View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@MOBEDU\View\SUMMARY_BY_TERM_VW.sql
@@MOBEDU\View\TERMS_TO_BURSAR_VW.sql
@@MOBEDU\View\TERM_COURSE_DETAILS_VW.sql
@@MOBEDU\View\VW_TERM_CHARGES.sql
@@MOBEDU\View\VW_TERM_PAYMENTS.sql
@@MOBEDU\PackageBody\ADD_COURSE_PKG_CORQ.sql
@@MOBEDU\PackageBody\CART_PKG.sql
@@MOBEDU\PackageBody\GOKODSF.sql
@@MOBEDU\PackageBody\ME_ACCOUNT_OBJECTS.sql
@@MOBEDU\PackageBody\ME_ALT_PIN_PKG.sql
@@MOBEDU\PackageBody\ME_BWCKCOMS.sql
@@MOBEDU\PackageBody\ME_BWCKGENS.sql
@@MOBEDU\PackageBody\ME_BWCKREGS.sql
@@MOBEDU\PackageBody\ME_REG_UTILS.sql
@@MOBEDU\PackageBody\ME_VALID.sql
@@MOBEDU\PackageBody\ME_WITHDRAWL.sql
@@MOBEDU\PackageBody\Z_CM_MOBILE_CAMPUS.sql
@@MOBEDU\Data\ADMIN_PRIV.sql
@@MOBEDU\Data\ADMIN_ROLE.sql
@@MOBEDU\Data\ADMIN_USER.sql
@@MOBEDU\Data\CAMPUS.sql
@@MOBEDU\Data\EMERGENCY_CONTACTS.sql
@@MOBEDU\Data\EMERGENCY_CONTACTS_AUDIT.sql
@@MOBEDU\Data\FAQ.sql
@@MOBEDU\Data\FEEDBACK.sql
@@MOBEDU\Data\FEEDBACK_DATA.sql
@@MOBEDU\Data\FEEDS.sql
@@MOBEDU\Data\FEEDS_AUDIT.sql
@@MOBEDU\Data\HELP.sql
@@MOBEDU\Data\HELP_AUDIT.sql
@@MOBEDU\Data\MAPS.sql
@@MOBEDU\Data\MAPS_AUDIT.sql
@@MOBEDU\Data\MODULE.sql
@@MOBEDU\Data\MODULES.sql
@@MOBEDU\Data\NOTICE.sql
@@MOBEDU\Data\NOTICELOG.sql
@@MOBEDU\Data\NOTICEPOPULATION.sql
@@MOBEDU\Data\NOTICETYPE.sql
@@MOBEDU\Data\POPULATIONTYPE.sql
@@MOBEDU\Data\PRIVILEGE.sql
@@MOBEDU\Data\PRIVILEGE_AUDIT.sql
@@MOBEDU\Data\ROLE.sql
commit;
EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
