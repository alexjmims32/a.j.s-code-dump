PROMPT CREATE OR REPLACE PROCEDURE mobedu.pz_drop_course
CREATE OR REPLACE PROCEDURE mobedu.pz_drop_course(p_student_id in varchar2,
                                           p_term_code  in varchar2,
                                           p_crn        in varchar2,
                                           p_error_msg  out varchar2,
                                           p_response   out varchar2) is
  v_check          int;
  loop_sfrstcr_row sfrstcr%rowtype;
  v_styp_code      varchar2(10);
  v_tmst_code      varchar2(10);
  v_tmst_ind       varchar2(10);
  v_sftregs_rec    sftregs%rowtype;
  v_error_out      varchar2(500);
  v_tmst_out       varchar2(500);
  v_add_date       sfrstcr.sfrstcr_add_date%type;
  v_pidm           number;
  r_count          int;
  sfrbtch_row      sfrbtch%ROWTYPE;
  tbrcbrq_row      tbrcbrq%ROWTYPE;
  V_SOBTERM_ROW    SOBTERM%ROWTYPE;
  v_clas_code      SGRCLSR.SGRCLSR_CLAS_CODE%TYPE;
  v_CLAS_DESC      VARCHAR2(50);
  v_lcur_levl      VARCHAR2(10);
  term_start_date  date;
  term_end_date    date;
  v_drop_check     number;
  v_ptrm_code      varchar2(5);
  v_rsts           STVRSTS.STVRSTS_CODE%TYPE;

  fa_return_status number;
  save_act_date    VARCHAR2(200);

  /*cursor sfrstcr_c(pidm NUMBER, term VARCHAR2, crn VARCHAR2 DEFAULT NULL) RETURN sfrstcr%ROWTYPE IS
  SELECT *
    FROM sfrstcr
   WHERE sfrstcr_pidm = pidm
     AND sfrstcr_term_code = term
     AND sfrstcr_crn = crn
   ORDER BY sfrstcr_add_date, sfrstcr_ptrm_code, sfrstcr_crn;*/

begin
  /*
  1.To drop a course from SFRSTCR table, we need to update the record in SFTREGS
  with SFTREGS_REC_STAT to C and SFTREGS_ERROR_FLAG='D' and SFTREGS_RSTS_CODE='DW'.

  */
  begin
    select baninst1.gb_common.f_get_pidm(p_student_id)
      into v_pidm
      from dual;
  exception
    when others then
      p_error_msg := sqlerrm || ': Unable to get student details';
  end;
  if p_error_msg is not null then
    GOTO QUIT_TO_PROGRAM;
  end if;

  select COUNT(1)
    INTO v_drop_check
    FROM TERMS_TO_REGISTER_VW
   WHERE term_CODE = p_term_code;

  select COUNT(1)
    into v_drop_check
    from SFRRSTS t
   where t.sfrrsts_term_code = p_term_code
     and t.sfrrsts_rsts_code = 'DW'
        /*       and t.sfrrsts_ptrm_code = v_ptrm_code*/ -- need to check this cond
     and sysdate between t.sfrrsts_start_date and t.sfrrsts_end_date;

  if v_drop_check = 0 then
    p_error_msg := 'Not permitted to drop at this time';
    GOTO QUIT_TO_PROGRAM;

  end if;

  dbms_output.put_line('pidm' || v_pidm || ' ' || 'crn' || p_crn || ' ' ||
                       p_term_code);

  -- Get the latest registration into SFTREGS.
  sfkmods.p_delete_sftregs_by_pidm_term(v_pidm, p_term_code);
  dbms_output.put_line('delete1');

  sfkmods.p_insert_sftregs_from_stcr(v_pidm, p_term_code, SYSDATE);
  dbms_output.put_line('insert1');

  begin

    dbms_output.put_line('pidm:' || v_pidm || ' ' || 'crn:' || p_crn || ' ' ||
                         'term:' || p_term_code);
    select count(*)
      into v_check
      from sftregs
     where sftregs_pidm = v_pidm
       and sftregs_term_code = p_term_code
       and sftregs_crn = p_crn;

    dbms_output.put_line('v_check:' || v_check);

  exception
    when no_data_found then
      v_check := 0;
      --p_error_msg := sqlerrm ;
      --p_error_msg:='exception in v_check'||sqlerrm;
      dbms_output.put_line('exception in v_check');
  end;

  dbms_output.put_line('v_check' || v_check);

  if v_check = 0 then

    p_error_msg := p_error_msg ||
                   'Course not registered, it cannot be dropped.';
    GOTO QUIT_TO_PROGRAM;
ELSE
    select a.sftregs_rsts_code into v_rsts from sftregs a
     where sftregs_pidm = v_pidm
       and sftregs_term_code = p_term_code
       and sftregs_crn = p_crn;

       if v_rsts not in ('RE','RW','WL') THEN
         p_error_msg := p_error_msg ||
                   'Course cannot be dropped.';
    GOTO QUIT_TO_PROGRAM;
       END IF;
  end if;

  begin
    mobedu.me_bwckregs.p_dropcrse(term    => p_term_code,
                                  crn     => p_crn,
                                  rsts    => 'DW', --replaced DD with DW
                                  pidm    => v_pidm,
                                  p_error => p_error_msg);

    dbms_output.put_line('sftregs record is updated for drop');

    GB_COMMON.P_COMMIT;

  exception
    when others then
      p_error_msg := 'exception raised in p_dropcrse' || sqlerrm;

      GB_COMMON.P_ROLLBACK;

  end;

  /*
  v_lcur_levl := sokccur.f_curriculum_value(p_pidm      => v_pidm,
                                            p_lmod_code => sb_curriculum_str.f_learner,
                                            p_term_code => p_term_code,
                                            p_key_seqno => 99,
                                            p_eff_term  => p_term_code,
                                            p_order     => 1,
                                            p_field     => 'LEVL');

  BEGIN
    SELECT *
      INTO V_SOBTERM_ROW
      FROM SOBTERM
     WHERE SOBTERM_TERM_CODE = P_TERM_CODE;
  EXCEPTION
    WHEN OTHERS THEN
      p_error_msg := 'INVALID TERM';
      GOTO QUIT_TO_PROGRAM;
  END;

  soklibs.p_class_calc(pidm            => v_pidm,
                       levl_code       => v_lcur_levl,
                       term_code       => p_term_code,
                       in_progress_ind => V_SOBTERM_ROW.SOBTERM_INCL_ATTMPT_HRS_IND,
                       clas_code       => v_clas_code, --out
                       clas_desc       => v_CLAS_DESC); --out

  dbms_output.put_line('LEVL:' || v_lcur_levl || ' CLASS CODE:' ||
                       v_clas_code);

  -- Create a batch fee assessment record.
  -- ===================================================
  sfrbtch_row.sfrbtch_term_code     := p_term_code;
  sfrbtch_row.sfrbtch_pidm          := v_pidm;
  sfrbtch_row.sfrbtch_clas_code     := v_clas_code; -- get class code, p_class_calc
  sfrbtch_row.sfrbtch_activity_date := SYSDATE;
  bwcklibs.p_add_sfrbtch(sfrbtch_row);

  tbrcbrq_row.tbrcbrq_term_code     := p_term_code;
  tbrcbrq_row.tbrcbrq_pidm          := v_pidm;
  tbrcbrq_row.tbrcbrq_activity_date := SYSDATE;
  bwcklibs.p_add_tbrcbrq(tbrcbrq_row);

  begin

    dbms_output.put_line('Call p_update_regs started');

    select sgbstdn_styp_code
      into v_styp_code
      from sgbstdn
     where sgbstdn_pidm = v_pidm
       and sgbstdn_term_code_eff = p_term_code;

    select sfbetrm_tmst_code, sfbetrm_tmst_maint_ind
      into v_tmst_code, v_tmst_ind
      from sfbetrm
     where sfbetrm_term_code = p_term_code
       and sfbetrm_pidm = v_pidm;
    dbms_output.put_line(v_styp_code || ' ' || v_tmst_code || ' ' ||
                         v_tmst_ind);

  exception
    when no_data_found then
      p_error_msg := sqlerrm || 'Exception before p_update_regs';
  end;*/

  begin

    dbms_output.put_line('updating record in sfrstcr');

    baninst1.SFKEDIT.p_update_regs(pidm_in            => v_pidm,
                                   term_in            => p_term_code,
                                   reg_date_in        => v_add_date,
                                   clas_code_in       => '01',
                                   styp_code_in       => v_styp_code,
                                   capc_severity_in   => 'F',
                                   tmst_calc_ind_in   => 'Y',
                                   tmst_maint_ind_in  => v_tmst_ind,
                                   tmst_code_in       => v_tmst_code,
                                   drop_last_class_in => null,
                                   system_in          => 'WA',
                                   error_rec_out      => v_sftregs_rec,
                                   error_flag_out     => v_error_out,
                                   tmst_flag_out      => v_tmst_out);

    fa_return_status := 0;

    /* Fee assessment after drop */

    SFKFEES.p_processfeeassessment(p_term_code,
                                   v_pidm,
                                   SYSDATE,
                                   SYSDATE,
                                   'R',
                                   'Y',
                                   'BWCKREGS',
                                   'Y',
                                   save_act_date,
                                   'N',
                                   fa_return_status);

    --if v_error_out is null then
    -- p_response := 'CRN'||crn||'Dropped';
    dbms_output.put_line('deleted and v_error value is ' || v_error_out);
    dbms_output.put_line('v_sftregs value is ' ||
                         v_sftregs_rec.sftregs_rsts_code);
    dbms_output.put_line('v_tmst value is ' || v_tmst_out);

  exception
    when others then
      p_error_msg := 'Exception in p_update_regs' || sqlerrm;
  end;
  if P_ERROR_MSG is null then
    --COMMIT THE TRANSACTIONS IF THE ERROR IS NULL
    GB_COMMON.P_COMMIT;
    P_RESPONSE := 'Selected course is dropped successfully.';
  else
    --ROLLBACK THE TRANSACTIONS IF THE ERROR IS NOT NULL
    GB_COMMON.P_ROLLBACK;
  end if;

  dbms_output.put_line('deleted from sftregs ');
  <<QUIT_TO_PROGRAM>>
  null;

exception
  when others then
    dbms_output.put_line('exception' || sqlerrm);

end;

/

