PROMPT CREATE OR REPLACE PACKAGE mobedu.me_valid
CREATE OR REPLACE PACKAGE mobedu.me_valid is
  function fz_me_val_adms(pidm in varchar2, term_code in varchar2)
    return varchar2;
  --

  --
  function fz_me_val_student(pidm in varchar2, term_code in varchar2)
    return varchar2;
  --

  FUNCTION FZ_GET_ALL_RESTRICTIONS(P_CRN IN NUMBER, P_TERM_CODE IN NUMBER)
    RETURN T_ARRAY_TYPE;
  --
  FUNCTION FZ_GET_FAILED_RESTRICTIONS(P_CRN        IN NUMBER,
                                      P_TERM_CODE  IN NUMBER,
                                      P_STUDENT_ID VARCHAR2) RETURN VARCHAR2;
  --
  FUNCTION FZ_DUPL_CHECK(P_CRN       IN NUMBER,
                         P_TERM_CODE IN NUMBER,
                         P_PIDM      NUMBER) RETURN VARCHAR2;
  --
  FUNCTION FZ_TIME_CHECK(P_CRN       VARCHAR2,
                         P_TERM_CODE VARCHAR2,
                         P_PIDM      NUMBER) RETURN VARCHAR2;
  --
  FUNCTION FZ_GROUP_CHECK(P_PIDM NUMBER, P_TERM VARCHAR2, P_CRN VARCHAR2)
    RETURN VARCHAR2;
  --
  function fz_me_corq_check(p_pidm number,
                            p_crn  in number,
                            p_term in number) return varchar2;
  --
  FUNCTION FZ_MHRS_CHECK(P_PIDM NUMBER, P_TERM VARCHAR2, P_CRN VARCHAR2)
    RETURN VARCHAR2;
  --
  function FZ_PREREQ_CHECK(P_CRN                 IN NUMBER,
                           P_TERM_CODE           IN NUMBER,
                           P_STUDENT_ID          IN VARCHAR2,
                           P_ERROR_MSG           OUT VARCHAR2,
                           a_failed_prereqs_subj OUT t_array_type,
                           a_failed_prereqs_crse OUT t_array_type,
                           a_failed_prereq_desc  out t_array_type)
    RETURN VARCHAR2;
  --------------
  --function to check alternate pin
  /* FUNCTION F_ALT_PIN(pidm      IN saturn.spriden.spriden_pidm%TYPE,
                   term_code in saturn.stvterm.stvterm_code%type)
  RETURN VARCHAR2; */

  ----------------------
  /* PROCEDURE PZ_STUDENT_COURSE_REGISTRATION(p_crn        IN NUMBER,
  P_student_ID IN VARCHAR2,
  P_TERM       IN NUMBER,
  P_ERROR_MSG  OUT VARCHAR2);*/
  --function to get meeting number,room code,start date and end date.
  FUNCTION fz_ssrmeetvalues(p_term     IN VARCHAR2,
                            p_crn      IN VARCHAR2,
                            p_position IN NUMBER,
                            p_ind      IN VARCHAR2) RETURN VARCHAR2;

  PROCEDURE PZ_REGISTRATION_CHECKS(P_STUDENT_ID IN VARCHAR2,
                                   P_TERM       IN varchar2,
                                   P_ERROR_MSG  OUT VARCHAR2);
  function FZ_GET_TERM(P_CREATE_DATE varchar2) return varchar2;
  procedure PZ_GET_TERM(P_CREATE_DATE varchar2,
                        P_TERM        OUT VARCHAR2,
                        P_TERM_DESC   OUT VARCHAR2,
                        P_ERROR_MSG   OUT VARCHAR2);

  FUNCTION FZ_CHECK_SFRRSTS(P_TERM_CODE VARCHAR2, P_CRN NUMBER,P_REG_IND VARCHAR2)
    RETURN VARCHAR2;
end me_valid;

/

