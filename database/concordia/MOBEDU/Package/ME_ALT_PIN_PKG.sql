PROMPT CREATE OR REPLACE PACKAGE mobedu.me_alt_pin_pkg
CREATE OR REPLACE PACKAGE mobedu.me_alt_pin_pkg AS
--  Management Control and Time Ticketing Checking Process

--  CURSORS

   CURSOR sfrctrl_row_by_term_c (
          term_in IN sfrctrl.sfrctrl_term_code_host%TYPE)
      RETURN sfrctrl%ROWTYPE;

--  FUNCTIONS AND PROCEDURES

   FUNCTION f_check_time_ticket (
  pidm_in               spriden.spriden_pidm%TYPE,
        term_in      sfrctrl.sfrctrl_term_code_host%TYPE,
  reg_date           DATE DEFAULT SYSDATE,
  flag_value    varchar2 DEFAULT 'Y')
     RETURN BOOLEAN;

   FUNCTION f_check_mgmt_control (
  pidm_in           IN spriden.spriden_pidm%TYPE,
  term_in            IN sfrctrl.sfrctrl_term_code_host%TYPE,
  web_vr_in     IN VARCHAR2)
     RETURN BOOLEAN;

   FUNCTION f_check_reg_appointment (pidm spriden.spriden_pidm%TYPE,
        term stvterm.stvterm_code%TYPE,
        use_mgmt_control VARCHAR2,
        restrict_time_ticket VARCHAR2,
        web_vr_ind VARCHAR2)
     RETURN BOOLEAN;
  FUNCTION F_ALT_PIN_SETUP_EXISTS RETURN VARCHAR2;

  FUNCTION F_STUDNT_PIN_EXISTS(pidm         IN saturn.spriden.spriden_pidm%TYPE,
                               term_code    in saturn.stvterm.stvterm_code%type,
                               process_name in saturn.SPRAPIN.SPRAPIN_PROCESS_NAME%type)

   RETURN VARCHAR2;

  FUNCTION F_VALIDATE_PIN(P_STUDENT_ID   IN saturn.spriden.spriden_id%TYPE,
                          P_TERM_CODE    in saturn.stvterm.stvterm_code%type,
                          P_PROCESS_NAME IN SATURN.SPRAPIN.SPRAPIN_PROCESS_NAME%TYPE,
                          P_PIN_ENTERED  IN SATURN.SPRAPIN.SPRAPIN_PIN%TYPE)

   RETURN VARCHAR2;

  PROCEDURE PZ_CHECK_ALT_PIN_REQ(P_STUDENT_ID IN saturn.spriden.spriden_id%TYPE,
                                 P_TERM_CODE  in saturn.stvterm.stvterm_code%type,
                                 P_CHECK_PIN  OUT VARCHAR2,
                                 P_ERROR_MSG  OUT VARCHAR2);
  ----------
  --Fuction to check the record in SFRCTRL
  FUNCTION FZ_VALID_SFRCTRL(/*P_STUDENT_ID IN VARCHAR2,*/
                            P_TERMCODE   IN VARCHAR2,
                            P_PIN        IN saturn.SFRCTRL.SFRCTRL_PIN_START%TYPE)
    RETURN VARCHAR2;

  -------
  --Function to check the record in SFBRGRP

  FUNCTION FZ_VALID_SFBRGRP(P_STUDENT_ID IN VARCHAR2,
                            P_TERMCODE   IN VARCHAR2) RETURN VARCHAR2;
  ----------
  --Procedure to validate the time ticket

  procedure PZ_TIME_TICKET_CONTROL(P_STUDENT_ID  IN saturn.spriden.spriden_id%TYPE,
                                   P_TERM_CODE   in saturn.stvterm.stvterm_code%type,
                                   P_PIN         in saturn.SFRCTRL.SFRCTRL_PIN_START%TYPE,
                                   P_ERROR_MSG   OUT VARCHAR2,
                                   P_SUCCESS_MSG OUT VARCHAR2);

FUNCTION F_CHECK_WEBREGISTER_TERM(P_TERM STVTERM.STVTERM_CODE%TYPE)RETURN BOOLEAN;

END ME_ALT_PIN_PKG;

/

