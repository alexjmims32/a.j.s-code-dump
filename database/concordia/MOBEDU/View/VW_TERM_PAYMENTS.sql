PROMPT CREATE OR REPLACE VIEW mobedu.vw_term_payments
CREATE OR REPLACE FORCE VIEW mobedu.vw_term_payments (
  pidm,
  term_code,
  amount,
  amnt_description,
  student_id,
  due_date
) AS
select TBRACCD_PIDM      PIDM,
       TBRACCD_TERM_CODE term_code,
       TBRACCD_AMOUNT    Amount,
       TBBDETC_DESC      Amnt_Description,
       spriden_id        Student_id,
       tbraccd_due_date  due_date
  from tbraccd
  join tbbdetc
    on tbraccd_detail_code = tbbdetc_detail_code
   and tbbdetc_type_ind = 'P'
  join spriden c
    on tbraccd_pidm = spriden_pidm
   and spriden_change_ind is null
/

