PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.me_withdrawl
CREATE OR REPLACE PACKAGE BODY mobedu.me_withdrawl AS
  PROCEDURE P_WITHDRAWL(P_STUDENT_ID VARCHAR2,
                        P_TERM_CODE  VARCHAR2,
                        P_CRN        NUMBER,
                        P_RSTS       VARCHAR2,
                        P_ERROR      OUT VARCHAR2) IS

    GLOBAL_PIDM   NUMBER;
    term          OWA_UTIL.ident_arr;
    rsts_in       OWA_UTIL.ident_arr;
    assoc_term_in OWA_UTIL.ident_arr;
    crn_in        OWA_UTIL.ident_arr;
    start_date_in OWA_UTIL.ident_arr;
    end_date_in   OWA_UTIL.ident_arr;
    subj          OWA_UTIL.ident_arr;
    crse          OWA_UTIL.ident_arr;
    sec           OWA_UTIL.ident_arr;
    levl          OWA_UTIL.ident_arr;
    cred          OWA_UTIL.ident_arr;
    gmod          OWA_UTIL.ident_arr;
    title         bwckcoms.varchar2_tabtype;
    mesg          OWA_UTIL.ident_arr;
    reg_btn       OWA_UTIL.ident_arr;
    regs_row      NUMBER;
    I             NUMBER;
    v_exist       number;
    v_ptrm_chk    VARCHAR2(10);
    V_RSTS_DESC   VARCHAR2(100);
    CURSOR C_SFTREGS(C_PIDM VARCHAR2) IS
      SELECT *
        FROM SFTREGS
       WHERE SFTREGS_PIDM = C_PIDM
         AND SFTREGS_TERM_CODE = P_TERM_CODE;
  BEGIN
    begin
      GLOBAL_PIDM := BANINST1.GB_COMMON.F_GET_PIDM(P_STUDENT_ID);
    exception
      when others then
        P_ERROR := 'Invalid Student Id';
              GOTO QUITTOPROGRAM;
    end;

    me_bwckcoms.P_INTGLOBAL(GLOBAL_PIDM);

    BWCKLIBS.P_INITVALUE(GLOBAL_PIDM, P_TERM_CODE, '', '', '', '');

    begin
      select count(*) into v_exist
        from sfrstcr
       where sfrstcr_pidm = GLOBAL_PIDM
         and sfrstcr_term_code = P_TERM_CODE
         and sfrstcr_crn = P_CRN;
    exception when others then
      p_error:='Student Not Register for the CRN';
      GOTO QUITTOPROGRAM;
    end;
  if v_exist = 0 then
    p_error:='Student Not Register for the CRN';
      GOTO QUITTOPROGRAM;
  end if;
  BEGIN
  SELECT STVRSTS_DESC INTO V_RSTS_DESC FROM STVRSTS WHERE STVRSTS_CODE=P_RSTS;
  EXCEPTION WHEN OTHERS THEN
    p_error:='Invalid Registration Status';
      GOTO QUITTOPROGRAM;
  END;
  IF P_RSTS='DW' THEN
    v_ptrm_chk:= me_valid.fz_check_sfrrsts(p_term_code,p_crn,'D');
  if  v_ptrm_chk='N' then
   p_error:='Not permitted to drop at this time';
   GOTO QUITTOPROGRAM;
 end if;
  ELSE
  v_ptrm_chk:= me_valid.fz_check_sfrrsts(p_term_code,p_crn,P_RSTS);
  if  v_ptrm_chk='N' then
   p_error:='Not permitted to'||V_RSTS_DESC||' at this time';
   GOTO QUITTOPROGRAM;
 end if;
 END IF;
    SFKMODS.P_DELETE_SFTREGS_BY_PIDM_TERM(GLOBAL_PIDM, P_TERM_CODE);
    SFKMODS.P_INSERT_SFTREGS_FROM_STCR(GLOBAL_PIDM, P_TERM_CODE, SYSDATE);
    term(1) := P_TERM_CODE;
    I := 1;
    SELECT COUNT(*)
      INTO regs_row
      FROM SFTREGS
     WHERE SFTREGS_PIDM = GLOBAL_PIDM
       AND SFTREGS_TERM_CODE = P_TERM_CODE;
    assoc_term_in(I) := 'DUMMY';
    crn_in(I) := 'DUMMY';
    start_date_in(I) := 'DUMMY';
    end_date_in(I) := 'DUMMY';
    subj(I) := 'DUMMY';
    crse(I) := 'DUMMY';
    sec(I) := 'DUMMY';
    levl(I) := 'DUMMY';
    cred(I) := 'DUMMY';
    gmod(I) := 'DUMMY';
    FOR R_SFTREGS IN C_SFTREGS(GLOBAL_PIDM) LOOP
      I := I + 1;
      IF R_SFTREGS.SFTREGS_CRN = P_CRN THEN
        DBMS_OUTPUT.put_line('IN WW');
        rsts_in(I) := P_RSTS;
      ELSE
        rsts_in(I) := R_SFTREGS.SFTREGS_RSTS_CODE;
      END IF;

      assoc_term_in(I) := R_SFTREGS.SFTREGS_TERM_CODE;
      crn_in(I) := R_SFTREGS.SFTREGS_CRN;
      start_date_in(I) := R_SFTREGS.SFTREGS_START_DATE;
      end_date_in(I) := R_SFTREGS.SFTREGS_COMPLETION_DATE;
      subj(I) := R_SFTREGS.SFTREGS_SECT_SUBJ_CODE;
      crse(I) := R_SFTREGS.SFTREGS_SECT_CRSE_NUMB;
      sec(I) := R_SFTREGS.SFTREGS_SECT_SEQ_NUMB;
      levl(I) := R_SFTREGS.SFTREGS_LEVL_CODE;
      cred(I) := R_SFTREGS.SFTREGS_CREDIT_HR;
      gmod(I) := R_SFTREGS.SFTREGS_GMOD_CODE;

    END LOOP;

    BEGIN
      me_bwckcoms.p_regs(term_in       => term,
                         rsts_in       => rsts_in,
                         assoc_term_in => assoc_term_in,
                         crn_in        => crn_in,
                         start_date_in => start_date_in,
                         end_date_in   => end_date_in,
                         subj          => subj,
                         crse          => crse,
                         sec           => sec,
                         levl          => levl,
                         cred          => cred,
                         gmod          => gmod,
                         title         => title,
                         mesg          => mesg,
                         reg_btn       => reg_btn,
                         regs_row      => regs_row,
                         add_row       => 10,
                         wait_row      => 0);

    EXCEPTION
      WHEN OTHERS THEN
        P_ERROR := BWCKLIBS.error_msg_table(SQLCODE);
    END;
  <<QUITTOPROGRAM>>
  NULL;
    /*EXCEPTION
    WHEN OTHERS THEN
      P_ERROR := SQLERRM;*/
  END P_WITHDRAWL;

END;

/

