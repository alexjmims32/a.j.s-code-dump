create table MOBEDU.FEEDS_AUDIT
(
  id             NUMBER(5) not null,
  parentid       NUMBER(5) not null,
  type           VARCHAR2(20) not null,
  link           VARCHAR2(200),
  format         VARCHAR2(100),
  moduleid       NUMBER(5) not null,
  name           VARCHAR2(100),
  icon           VARCHAR2(100),
  campuscode     VARCHAR2(6),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6)
)
;

