create table MOBEDU.EMERGENCY_CONTACTS_AUDIT
(
  id             NUMBER not null,
  campus         VARCHAR2(100),
  name           VARCHAR2(50),
  phone          VARCHAR2(15),
  address        VARCHAR2(50),
  email          VARCHAR2(30),
  picture_url    VARCHAR2(150),
  category       VARCHAR2(34),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6),
  comments       VARCHAR2(1000)
)
;

