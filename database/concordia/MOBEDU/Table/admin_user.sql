create table MOBEDU.ADMIN_USER
(
  username  VARCHAR2(20) not null,
  firstname VARCHAR2(100) not null,
  lastname  VARCHAR2(100) not null,
  password  VARCHAR2(100) not null,
  active    CHAR(1) default 'Y' not null
)
;

