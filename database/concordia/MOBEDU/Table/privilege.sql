create table MOBEDU.PRIVILEGE
(
  roleid         NUMBER not null,
  modulecode     VARCHAR2(100) not null,
  accessflag     VARCHAR2(1),
  authrequired   CHAR(1) default 'Y',
  lastmodifiedon TIMESTAMP(6),
  lastmodifiedby VARCHAR2(40),
  version_no     NUMBER(5)
)
;

