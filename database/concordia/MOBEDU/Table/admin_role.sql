create table MOBEDU.ADMIN_ROLE
(
  code        VARCHAR2(20) not null,
  description VARCHAR2(100) not null
)
;
create unique index MOBEDU.ADMIN_ROLE_IDX on MOBEDU.ADMIN_ROLE (CODE);

