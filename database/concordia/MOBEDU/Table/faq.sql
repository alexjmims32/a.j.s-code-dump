create table MOBEDU.FAQ
(
  id             NUMBER not null,
  campuscode     VARCHAR2(100),
  question       VARCHAR2(4000),
  answer         VARCHAR2(4000),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6)
)
;

