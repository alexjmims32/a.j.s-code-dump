create table MOBEDU.NOTICETYPE
(
  id             NUMBER(11) not null,
  category       VARCHAR2(20) not null,
  subcategory    VARCHAR2(20),
  title          VARCHAR2(50) not null,
  description    VARCHAR2(100) not null,
  priority       NUMBER(2) default 1 not null,
  msgtemplate    VARCHAR2(4000),
  lastmodifiedby VARCHAR2(50) not null,
  lastmodifiedon TIMESTAMP(6) not null
)
;
comment on column MOBEDU.NOTICETYPE.id
  is 'Primary Key';
comment on column MOBEDU.NOTICETYPE.category
  is 'Main categories';
comment on column MOBEDU.NOTICETYPE.subcategory
  is 'Sub categories under each category. Use NULL for main categories';
comment on column MOBEDU.NOTICETYPE.priority
  is 'Use range of 1 to 10';
comment on column MOBEDU.NOTICETYPE.lastmodifiedby
  is 'id from user table';
alter table MOBEDU.NOTICETYPE
  add constraint MOBEDU_NOTICE_TYPE_PK primary key (ID);

