create table MOBEDU.ADMIN_PRIV
(
  username    VARCHAR2(20) not null,
  privcode    VARCHAR2(20) not null,
  accessflag  CHAR(1) default 'Y' not null,
  description VARCHAR2(100) not null
)
;

