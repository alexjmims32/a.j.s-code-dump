create table MOBEDU.NOTICELOG
(
  id             NUMBER(11) not null,
  noticeid       NUMBER(11) not null,
  username       VARCHAR2(20) not null,
  message        VARCHAR2(4000) not null,
  expirydate     DATE,
  deliverymethod VARCHAR2(20),
  delivered      NUMBER(1) default 0 not null,
  readflag       NUMBER(1) default 0 not null,
  lastmodifiedby VARCHAR2(50) not null,
  lastmodifiedon TIMESTAMP(6),
  deleted        NUMBER(1) default 0 not null,
  type           VARCHAR2(100),
  title          VARCHAR2(100),
  duedate        DATE
)
;

