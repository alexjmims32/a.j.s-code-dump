create table MOBEDU.NOTICEPOPULATION
(
  noticeid         NUMBER(11) not null,
  populationtypeid NUMBER(11) not null,
  paramname        VARCHAR2(100) not null,
  paramvalue       VARCHAR2(1000)
)
;
alter table MOBEDU.NOTICEPOPULATION
  add constraint MOBEDU_NOTICE_POP_PK primary key (NOTICEID, POPULATIONTYPEID, PARAMNAME);
alter table MOBEDU.NOTICEPOPULATION
  add constraint MOBEDU_NOTICE_ID_FK foreign key (NOTICEID)
  references MOBEDU.NOTICE (ID) on delete cascade;
alter table MOBEDU.NOTICEPOPULATION
  add constraint MOBEDU_POP_TYPE_ID_FK foreign key (POPULATIONTYPEID)
  references MOBEDU.POPULATIONTYPE (ID);

