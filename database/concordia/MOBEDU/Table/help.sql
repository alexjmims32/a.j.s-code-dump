create table MOBEDU.HELP
(
  campuscode     VARCHAR2(100),
  abouttext      VARCHAR2(4000),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6)
)
;

