create table MOBEDU.CAMPUS
(
  code          VARCHAR2(4000),
  title         VARCHAR2(4000),
  description   VARCHAR2(4000),
  client        VARCHAR2(100),
  principalname VARCHAR2(100),
  website       VARCHAR2(4000)
)
;

