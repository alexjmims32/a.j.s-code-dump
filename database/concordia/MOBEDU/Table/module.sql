create table MOBEDU.MODULE
(
  campuscode    VARCHAR2(6 BYTE),
  id            NUMBER,
  code          VARCHAR2(10 BYTE),
  description   VARCHAR2(100 BYTE),
  authrequired  CHAR(1 BYTE) default 'Y',
  showbydefault CHAR(1 BYTE) default 'N',
  icon          VARCHAR2(100 BYTE),
  position      VARCHAR2(10 BYTE)
)
;

