create table MOBEDU.FEEDBACK
(
  id          NUMBER(5) not null,
  title       VARCHAR2(50),
  email       VARCHAR2(100),
  description VARCHAR2(100)
)
;

