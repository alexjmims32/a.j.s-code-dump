PROMPT ====================================================
PROMPT      <<< SQLTools extract DDL utility V1.2 >>>      
PROMPT      USAGE: Run SQL Plus and input "start DO.sql"   
PROMPT ====================================================

spool DO.log
@@Synonyms.sql
@@Types.sql
@@Type\T_ARRAY_TYPE.sql
prompt
prompt Creating table ADMIN_PRIV
prompt =========================
prompt
@@Table\admin_priv.tab
prompt
prompt Creating table ADMIN_ROLE
prompt =========================
prompt
@@Table\admin_role.tab
prompt
prompt Creating table ADMIN_USER
prompt =========================
prompt
@@Table\admin_user.tab
prompt
prompt Creating table CAMPUS
prompt =====================
prompt
@@Table\campus.tab
prompt
prompt Creating table EMERGENCY_CONTACTS
prompt =================================
prompt
@@Table\emergency_contacts.tab
prompt
prompt Creating table EMERGENCY_CONTACTS_AUDIT
prompt =======================================
prompt
@@Table\emergency_contacts_audit.tab
prompt
prompt Creating table FAQ
prompt ==================
prompt
@@Table\faq.tab
prompt
prompt Creating table FEEDBACK
prompt =======================
prompt
@@Table\feedback.tab
prompt
prompt Creating table FEEDBACK_DATA
prompt ============================
prompt
@@Table\feedback_data.tab
prompt
prompt Creating table FEEDS
prompt ====================
prompt
@@Table\feeds.tab
prompt
prompt Creating table FEEDS_AUDIT
prompt ==========================
prompt
@@Table\feeds_audit.tab
prompt
prompt Creating table HELP
prompt ===================
prompt
@@Table\help.tab
prompt
prompt Creating table HELP_AUDIT
prompt =========================
prompt
@@Table\help_audit.tab
prompt
prompt Creating table MAPS
prompt ===================
prompt
@@Table\maps.tab
prompt
prompt Creating table MAPS_AUDIT
prompt =========================
prompt
@@Table\maps_audit.tab
prompt
prompt Creating table MOBEDU_CART
prompt ==========================
prompt
@@Table\mobedu_cart.tab
prompt
prompt Creating table MODULE
prompt =====================
prompt
@@Table\module.tab
prompt
prompt Creating table MODULES
prompt ======================
prompt
@@Table\modules.tab
prompt
prompt Creating table NOTICE
prompt =====================
prompt
@@Table\notice.tab
prompt
prompt Creating table NOTICELOG
prompt ========================
prompt
@@Table\noticelog.tab
prompt
prompt Creating table POPULATIONTYPE
prompt =============================
prompt
@@Table\populationtype.tab
prompt
prompt Creating table NOTICEPOPULATION
prompt ===============================
prompt
@@Table\noticepopulation.tab
prompt
prompt Creating table NOTICETYPE
prompt =========================
prompt
@@Table\noticetype.tab
prompt
prompt Creating table PRIVILEGE
prompt ========================
prompt
@@Table\privilege.tab
prompt
prompt Creating table PRIVILEGE_AUDIT
prompt ==============================
prompt
@@Table\privilege_audit.tab
prompt
prompt Creating table ROLE
prompt ===================
prompt
@@Table\role.tab
@@Sequence\FEEDBACK_SEQ.sql
@@Sequence\NOTICE_SEQ.sql
@@Package\ADD_COURSE_PKG_CORQ.sql
@@Package\CART_PKG.sql
@@Package\GOKODSF.sql
@@Package\ME_ACCOUNT_OBJECTS.sql
@@Package\ME_ALT_PIN_PKG.sql
@@Package\ME_BWCKCOMS.sql
@@Package\ME_BWCKGENS.sql
@@Package\ME_BWCKREGS.sql
@@Package\ME_REG_UTILS.sql
@@Package\ME_VALID.sql
@@Package\ME_WITHDRAWL.sql
@@Package\Z_CM_MOBILE_CAMPUS.sql
@@Procedure\PZ_DROP_COURSE.sql
@@Procedure\PZ_ME_AUTHENTICATION.sql
@@View\ACCOUNT_SUMMARY_VIEW.sql
@@View\ACCT_SUMM_VW.sql
@@View\CART_DETAILS_VW.sql
@@View\CHARGE_PAY_DETAIL_VW.sql
@@View\CONTACT_VW.sql
@@View\TERMS_TO_REGISTER_VW.sql
@@View\COURSE_SEARCH_VW.sql
@@View\CRSE_MEET_VW.sql
@@View\GENERAL_PERSON_VW.sql
@@View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@View\MOBEDU_HOLIDAY_VW.sql
@@View\PERSON_VW.sql
@@View\STUDENT_ACCOUNT_DETAIL.sql
@@View\STU_CURCULAM_INFO.sql
@@View\STU_HOLD_INFO.sql
@@View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@View\SUMMARY_BY_TERM_VW.sql
@@View\TERMS_TO_BURSAR_VW.sql
@@View\TERM_COURSE_DETAILS_VW.sql
@@View\VW_TERM_CHARGES.sql
@@View\VW_TERM_PAYMENTS.sql
@@PackageBody\ADD_COURSE_PKG_CORQ.sql
@@PackageBody\CART_PKG.sql
@@PackageBody\GOKODSF.sql
@@PackageBody\ME_ACCOUNT_OBJECTS.sql
@@PackageBody\ME_ALT_PIN_PKG.sql
@@PackageBody\ME_BWCKCOMS.sql
@@PackageBody\ME_BWCKGENS.sql
@@PackageBody\ME_BWCKREGS.sql
@@PackageBody\ME_REG_UTILS.sql
@@PackageBody\ME_VALID.sql
@@PackageBody\ME_WITHDRAWL.sql
@@PackageBody\Z_CM_MOBILE_CAMPUS.sql
@@Data\ADMIN_PRIV.sql
@@Data\ADMIN_ROLE.sql
@@Data\ADMIN_USER.sql
@@Data\CAMPUS.sql
@@Data\EMERGENCY_CONTACTS.sql
@@Data\EMERGENCY_CONTACTS_AUDIT.sql
@@Data\FAQ.sql
@@Data\FEEDBACK.sql
@@Data\FEEDBACK_DATA.sql
@@Data\FEEDS.sql
@@Data\FEEDS_AUDIT.sql
@@Data\HELP.sql
@@Data\HELP_AUDIT.sql
@@Data\MAPS.sql
@@Data\MAPS_AUDIT.sql
@@Data\MODULE.sql
@@Data\MODULES.sql
@@Data\NOTICE.sql
@@Data\NOTICELOG.sql
@@Data\NOTICEPOPULATION.sql
@@Data\NOTICETYPE.sql
@@Data\POPULATIONTYPE.sql
@@Data\PRIVILEGE.sql
@@Data\PRIVILEGE_AUDIT.sql
@@Data\ROLE.sql
commit;
EXEC Dbms_Utility.compile_schema(USER, FALSE);

spool off
exit
