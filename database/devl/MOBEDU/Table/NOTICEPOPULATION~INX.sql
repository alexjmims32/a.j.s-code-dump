PROMPT CREATE INDEX mobedu.mobedu_notice_pop_pk
CREATE UNIQUE INDEX mobedu.mobedu_notice_pop_pk
  ON mobedu.noticepopulation (
    noticeid,
    populationtypeid,
    paramname
  )
/

