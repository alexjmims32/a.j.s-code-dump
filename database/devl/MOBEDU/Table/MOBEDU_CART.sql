PROMPT CREATE TABLE mobedu.mobedu_cart
CREATE TABLE mobedu.mobedu_cart (
  student_id        VARCHAR2(10)  NULL,
  crn               VARCHAR2(10)  NULL,
  term              VARCHAR2(10)  NULL,
  status            VARCHAR2(500) NULL,
  processed_ind     VARCHAR2(1)   NULL,
  error_flag        VARCHAR2(1)   NULL,
  rsts_code         VARCHAR2(2)   NULL,
  var_credit_hr_ind VARCHAR2(2)   NULL,
  var_credit_hrs    NUMBER(7,3)   NULL
)
/


