PROMPT ALTER TABLE mobedu.noticepopulation ADD CONSTRAINT mobedu_notice_id_fk FOREIGN KEY
ALTER TABLE mobedu.noticepopulation
  ADD CONSTRAINT mobedu_notice_id_fk FOREIGN KEY (
    noticeid
  ) REFERENCES mobedu.notice (
    id
  ) ON DELETE CASCADE
/

PROMPT ALTER TABLE mobedu.noticepopulation ADD CONSTRAINT mobedu_pop_type_id_fk FOREIGN KEY
ALTER TABLE mobedu.noticepopulation
  ADD CONSTRAINT mobedu_pop_type_id_fk FOREIGN KEY (
    populationtypeid
  ) REFERENCES mobedu.populationtype (
    id
  )
/

