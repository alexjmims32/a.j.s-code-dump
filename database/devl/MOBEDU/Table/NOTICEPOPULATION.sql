PROMPT CREATE TABLE mobedu.noticepopulation
CREATE TABLE mobedu.noticepopulation (
  noticeid         NUMBER(11,0)   NOT NULL,
  populationtypeid NUMBER(11,0)   NOT NULL,
  paramname        VARCHAR2(100)  NOT NULL,
  paramvalue       VARCHAR2(1000) NULL
)
/


