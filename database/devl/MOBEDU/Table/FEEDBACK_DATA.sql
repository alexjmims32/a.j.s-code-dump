create table FEEDBACK_DATA
(
  areaid       NUMBER(3),
  courseid     VARCHAR2(10),
  comments1    VARCHAR2(4000),
  comments2    VARCHAR2(4000),
  confidential CHAR(1),
  sender_name  VARCHAR2(100),
  sender_email VARCHAR2(100),
  created_date TIMESTAMP(6)
);