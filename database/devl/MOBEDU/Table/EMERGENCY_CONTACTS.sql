PROMPT CREATE TABLE mobedu.emergency_contacts
CREATE TABLE mobedu.emergency_contacts (
  id          NUMBER        NOT NULL,
  campus      VARCHAR2(100) NULL,
  name        VARCHAR2(50)  NULL,
  phone       VARCHAR2(13)  NULL,
  address     VARCHAR2(50)  NULL,
  email       VARCHAR2(30)  NULL,
  picture_url VARCHAR2(150) NULL,
  category    VARCHAR2(34),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6)
)
/


