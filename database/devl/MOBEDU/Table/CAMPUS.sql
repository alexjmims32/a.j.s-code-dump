PROMPT CREATE TABLE mobedu.campus
CREATE TABLE mobedu.campus (
  code        VARCHAR2(4000) NULL,
  title       VARCHAR2(4000) NULL,
  description VARCHAR2(4000) NULL,
  client      VARCHAR2(100)  NULL,
  principalname VARCHAR2(100),
  website       VARCHAR2(4000)
)
/


