PROMPT CREATE TABLE mobedu.populationtype
CREATE TABLE mobedu.populationtype (
  id             NUMBER(11,0)   NOT NULL,
  type           VARCHAR2(20)   NOT NULL,
  title          VARCHAR2(50)   NOT NULL,
  description    VARCHAR2(100)  NOT NULL,
  query          VARCHAR2(1000) NULL,
  lastmodifiedby VARCHAR2(50)   NOT NULL,
  lastmodifiedon TIMESTAMP(6)   NOT NULL
)
/


