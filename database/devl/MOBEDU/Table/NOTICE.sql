PROMPT CREATE TABLE mobedu.notice
CREATE TABLE mobedu.notice (
  id             NUMBER(11,0)   NOT NULL,
  typeid         NUMBER(11,0)   NOT NULL,
  title          VARCHAR2(50)   NOT NULL,
  message        VARCHAR2(4000) NOT NULL,
  duedate        DATE           NULL,
  expirydate     DATE           NULL,
  posted         NUMBER(1,0)    DEFAULT 0 NOT NULL,
  lastmodifiedby VARCHAR2(50)   NOT NULL,
  lastmodifiedon TIMESTAMP(6)   NOT NULL,
  url            VARCHAR2(100)  NULL
)
/

COMMENT ON COLUMN mobedu.notice.typeid IS 'refers to ID in noticetypes table';

