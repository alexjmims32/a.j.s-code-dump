PROMPT ALTER TABLE mobedu.noticelog ADD CONSTRAINT mobedu_notice_log_notice_id_fk FOREIGN KEY
ALTER TABLE mobedu.noticelog
  ADD CONSTRAINT mobedu_notice_log_notice_id_fk FOREIGN KEY (
    noticeid
  ) REFERENCES mobedu.notice (
    id
  ) ON DELETE CASCADE
  DISABLE
  NOVALIDATE
/

