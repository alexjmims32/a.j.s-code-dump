create table MOBEDU.HELP_AUDIT
(
  campuscode     VARCHAR2(100),
  abouttext      VARCHAR2(4000),
  faq            VARCHAR2(4000),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6)
)
;

