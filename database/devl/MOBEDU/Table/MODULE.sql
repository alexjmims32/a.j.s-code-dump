PROMPT CREATE TABLE mobedu.module
CREATE TABLE mobedu.module (
  campuscode    VARCHAR2(6)   NULL,
  id            NUMBER        NULL,
  code          VARCHAR2(10)  NULL,
  description   VARCHAR2(100) NULL,
  authrequired  CHAR(1)       DEFAULT 'Y' NULL,
  showbydefault CHAR(1)       DEFAULT 'N' NULL,
  icon          VARCHAR2(100) NULL
)
/


