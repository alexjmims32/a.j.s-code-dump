PROMPT CREATE TABLE mobedu.maps
CREATE TABLE mobedu.maps (
  id            NUMBER        NOT NULL,
  campus_code   VARCHAR2(6)   NULL,
  building_code VARCHAR2(6)   NULL,
  building_name VARCHAR2(100) NULL,
  building_desc VARCHAR2(100) NULL,
  phone         VARCHAR2(15)  NULL,
  email         VARCHAR2(30)  NULL,
  img_url       VARCHAR2(30)  NULL,
  longitude     VARCHAR2(30)  NULL,
  latitude      VARCHAR2(30)  NULL,
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6),
  address        VARCHAR2(4000)
)
/


