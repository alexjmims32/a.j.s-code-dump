create table SERVICE_AREA
(
  id         INTEGER not null,
  area       VARCHAR2(100) not null,
  subarea    VARCHAR2(100),
  supervisor VARCHAR2(100),
  assocdean  VARCHAR2(100),
  dean       VARCHAR2(100),
  hr         VARCHAR2(100),
  provost    VARCHAR2(100)
);
-- Create/Recreate primary, unique and foreign key constraints 
alter table SERVICE_AREA
  add primary key (ID)
  using index;