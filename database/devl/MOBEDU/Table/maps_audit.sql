create table MOBEDU.MAPS_AUDIT
(
  id             NUMBER not null,
  campus_code    VARCHAR2(6),
  building_code  VARCHAR2(6),
  building_name  VARCHAR2(100),
  building_desc  VARCHAR2(100),
  phone          VARCHAR2(15),
  email          VARCHAR2(30),
  img_url        VARCHAR2(30),
  longitude      VARCHAR2(30),
  latitude       VARCHAR2(30),
  version_no     NUMBER(5),
  lastmodifiedby VARCHAR2(40),
  lastmodifiedon TIMESTAMP(6),
  address        VARCHAR2(4000)
)
;

