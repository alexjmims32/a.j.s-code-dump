prompt Importing table privilege...
set feedback off
set define off
insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'ENROUTE', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'ENTER', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'ENQ', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'ATHLETICS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'EVENTS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'MAPS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'COURSES', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'DIRECTORY', 'N', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'PROFILE', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'ACCOUNTS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'REGISTRATION', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'JOBS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'HELP', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'FEEDBACK', 'Y', 'N');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (1, 'EMER', 'N', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'ENROUTE', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'ENTER', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'ENQ', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'ATHLETICS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'EVENTS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'MAPS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'COURSES', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'DIRECTORY', 'N', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'PROFILE', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'ACCOUNTS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'REGISTRATION', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'JOBS', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'HELP', 'Y', 'Y');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'FEEDBACK', 'Y', 'N');

insert into privilege (ROLEID, MODULECODE, ACCESSFLAG, AUTHREQUIRED)
values (3, 'EMER', 'N', 'Y');

prompt Done.
