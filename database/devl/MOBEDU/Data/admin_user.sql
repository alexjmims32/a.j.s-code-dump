prompt Importing table admin_user...
set feedback off
set define off
insert into admin_user (USERNAME, ROLE, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('admin', 'SUPERADMIN', 'Admin', 'Super', 'e3afed0047b08059d0fada10f400c1e5', 'Y');

insert into admin_user (USERNAME, ROLE, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('NotifAdmin', 'NOTIFADMIN', 'Notification', 'Admin', '96d008db67fc0b5551a926842bbb6a71', 'Y');

insert into admin_user (USERNAME, ROLE, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('App', 'APPADMIN', 'App', 'Admin', 'ac863f346e618f9a959b5c95d5d28941', 'Y');

insert into admin_user (USERNAME, ROLE, FIRSTNAME, LASTNAME, PASSWORD, ACTIVE)
values ('FeedAdmin', 'FEEDADMIN', 'Feeds', 'Admin', '8f8c62ea0175e358420ec4abf851ee3a', 'Y');

prompt Done.
