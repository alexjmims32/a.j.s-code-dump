prompt Importing table modules...
set feedback off
set define off
insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (1, 'ATHLETICS', 'Athletics');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (2, 'EVENTS', 'Events');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (3, 'MAPS', 'Maps');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (4, 'COURSES', 'Courses');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (5, 'DIRECTORY', 'Directory');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (6, 'PROFILE', 'Profile');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (7, 'ACCOUNTS', 'Accounts');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (8, 'REG', 'Registration');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (9, 'JOBS', 'jobs');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (10, 'HELP', 'Help');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (11, 'FEEDBACK', 'Feedback');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (12, 'NEWS', 'News');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (13, 'ENTER', 'Enter');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (14, 'ENQ', 'Enquire');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (15, 'ENROUTE', 'Enroute');

insert into modules (ID, MODULE_CODE, MODULE_DESC)
values (16, 'EMER', 'Emergency Contacts');

prompt Done.
