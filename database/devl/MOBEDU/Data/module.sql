prompt Importing table MODULE...
set feedback off
set define off
insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 12, 'ALUMNI', 'Alumni', 'Y', 'N', 'alumni.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 10, 'HISTORY', 'Student History', 'Y', 'N', 'student-history.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1012, 'ALUMNI', 'Alumni', 'Y', 'N', 'alumni.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1010, 'HISTORY', 'Student History', 'Y', 'N', 'student-history.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1002, 'ATHLETICS', 'Sports', 'N', 'Y', 'athletics.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1003, 'EVENTS', 'Events', 'N', 'Y', 'events.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1004, 'MAPS', 'Maps', 'N', 'Y', 'campus-map.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1005, 'COURSES', 'Courses', 'Y', 'N', 'courses.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1006, 'DIRECTORY', 'Directory', 'N', 'N', 'dir.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1007, 'PROFILE', 'INOW', 'Y', 'Y', 'student-profile.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1008, 'ACCOUNTS', 'Student Accounts', 'Y', 'N', 'student-accounts.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1009, 'REG', 'My Big Campus', 'Y', 'Y', 'registration.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1011, 'JOBS', 'Jobs', 'N', 'N', 'jobs.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1017, 'HELP', 'Help', 'N', 'N', 'help.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1018, 'FEEDBACK', 'Feedback', 'N', 'N', 'feedback.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1001, 'NEWS', 'News', 'N', 'Y', 'campus-news.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1013, 'ENTER', 'Enter', 'Y', 'N', 'n2n.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1014, 'ENQ', 'Enquire', 'Y', 'N', 'n2n.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1015, 'ENROUTE', 'Enroute', 'Y', 'N', 'n2n.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('NAFCS', 1016, 'EMER', 'Emergency Contacts', 'N', 'N', 'dir.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 2, 'ATHLETICS', 'Athletics', 'N', 'Y', 'athletics.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 3, 'EVENTS', 'Events', 'N', 'Y', 'events.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 4, 'MAPS', 'Campus Maps', 'N', 'Y', 'campus-map.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 5, 'COURSES', 'Courses', 'Y', 'Y', 'courses.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 6, 'DIRECTORY', 'Directory', 'N', 'Y', 'dir.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 7, 'PROFILE', 'Student Profile', 'Y', 'Y', 'student-profile.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 8, 'ACCOUNTS', 'Student Accounts', 'Y', 'Y', 'student-accounts.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 9, 'REG', 'Registration', 'Y', 'Y', 'registration.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 11, 'JOBS', 'Jobs', 'N', 'Y', 'jobs.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 17, 'HELP', 'Help', 'N', 'Y', 'help.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 18, 'FEEDBACK', 'Feedback', 'N', 'Y', 'feedback.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 1, 'NEWS', 'Campus News', 'N', 'Y', 'campus-news.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 13, 'ENTER', 'Enter', 'Y', 'N', 'n2n.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 14, 'ENQ', 'Enquire', 'Y', 'N', 'n2n.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 15, 'ENROUTE', 'Enroute', 'Y', 'N', 'n2n.png');

insert into MODULE (CAMPUSCODE, ID, CODE, DESCRIPTION, AUTHREQUIRED, SHOWBYDEFAULT, ICON)
values ('UT', 16, 'EMER', 'Emergency Contacts', 'N', 'Y', 'dir.png');

prompt Done.
