prompt Importing table maps...
set feedback off
set define off
insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (9, 'UT', 'UH', 'University Hall', null, null, null, null, '-83.614359', '41.662272');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (10, 'UT', 'GH', 'Gillham Hall', null, '123-234-3434', 'testing@dmail.com', null, '-83.612889', '41.662179');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (11, 'UT', 'UT', 'Stranahan Hall South', null, '234-234-1111', 'test@testmail.com', null, '-83.612671', '41.661848');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (12, 'UT', 'ST', 'Stranahan Hall North', null, null, null, null, '-83.612483', '41.662158');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (13, 'UT', 'RO', 'Ritter Astrophysical Research', null, '123-234-1234', 'UTMC@ut.com', null, '-83.612131', '41.662447');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (14, 'UT', 'HH', 'Health and Human Services', null, null, null, null, '-83.612316', '41.661255');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (15, 'UT', 'MH', 'McMaster Hall', null, null, null, null, '-83.611493', '41.662273');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (16, 'UT', 'PH', 'Peterson House', null, null, null, null, '-83.615081', '41.663268');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (17, 'UT', 'DC', 'Driscoll Alumni Center', null, null, null, null, '-83.61065', '41.663548');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (18, 'UT', 'SM', 'Snyder Memorial', null, null, null, null, '-83.613071', '41.660757');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (19, 'UT', 'SV', 'John F. Savage Arena', null, null, null, null, '-83.610859', '41.659949');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (20, 'UT', 'SU', 'Student Union', null, null, null, null, '-83.614278', '41.660736');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (21, 'UT', 'CL', 'Carlson Library', null, null, null, null, '-83.614769', '41.660188');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (22, 'UT', 'FH', 'Memorial Field House', ' ', ' ', ' ', ' ', '-83.616145', '41.661133');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (23, 'UT', 'MK', 'MacKinnon Hall', null, null, null, null, '-83.617203', '41.661495');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (24, 'UT', 'DH', 'Dowd Hall', null, null, null, null, '-83.617518', '41.662332');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (25, 'UT', 'WH', 'White Hall', null, null, null, null, '-83.617503', '41.66178');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (26, 'UT', 'NH', 'Nash Hall', null, null, null, null, '-83.618155', '41.66204');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (27, 'UT', 'SH', 'Scott Hall', null, null, null, null, '-83.616984', '41.66262');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (28, 'UT', 'TH', 'Tucker Hall', null, null, null, null, '-83.616948', '41.661996');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (29, 'UT', 'LH', 'Libbey Hall', null, null, null, null, '-83.616255', '41.661964');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (30, 'UT', 'HE', 'Health Education Center', null, null, null, null, '-83.612207', '41.65822');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (31, 'UT', 'LM', 'Larimer Athletic Center', null, null, null, null, '-83.613222', '41.658125');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (32, 'UT', 'RC', 'Recreation Center', null, null, null, null, '-83.611743', '41.656659');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (33, 'UT', 'MV', 'McComas Village', null, null, null, null, '-83.614777', '41.656042');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (34, 'UT', 'PT', 'Parks Tower', null, null, null, null, '-83.615712', '41.657277');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (35, 'UT', 'GB', 'Glass Bowl', null, null, null, null, '-83.614536', '41.657083');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (36, 'UT', 'UC', 'University Computer Center', null, null, null, null, '-83.609521', '41.656209');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (37, 'UT', 'PL', 'Palmer Hall', null, null, null, null, '-83.607366', '41.655744');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (38, 'UT', 'WA', 'Westwood Research Annex', null, null, null, null, '-83.607785', '41.655618');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (39, 'UT', 'NA', 'Nitschke Auditorium', null, null, null, null, '-83.606364', '41.655267');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (40, 'UT', 'NI', 'Nitschke Hall', null, null, null, null, '-83.606379', '41.656148');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (41, 'UT', 'NE', 'North Engineering', null, null, null, null, '-83.607333', '41.656544');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (42, 'UT', 'R1', 'Research & Tech. Complex 1', null, null, null, null, '-83.606151', '41.653561');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (43, 'UT', 'NT', 'Nitschke Technology Commercialization Complex', null, null, null, null, '-83.60638', '41.654394');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (44, 'UT', 'PO', 'Plant Operations', null, null, null, null, '-83.612745', '41.653794');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (45, 'UT', 'CH', 'Child Care Center', null, null, null, null, '-83.613347', '41.653592');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (46, 'UT', 'UR', 'Recycling Center', null, null, null, null, '-83.612091', '41.653439');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (47, 'UT', 'GF', 'Grounds and Fleet Services', null, null, null, null, '-83.611733', '41.653983');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (48, 'UT', 'RH', 'Rocket Hall', null, null, null, null, '-83.620066', '41.654776');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (49, 'UT', 'CR', 'The Crossings', null, null, null, null, '-83.617283', '41.654007');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (50, 'UT', 'OW', 'Ottawa West', null, null, null, null, '-83.617955', '41.654668');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (51, 'UT', 'OE', 'Ottawa East', null, null, null, null, '-83.616838', '41.654754');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (52, 'UT', 'VT', 'Varsity "T" Pavillion', null, null, null, null, '-83.614198', '41.655253');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (53, 'UT', 'CE', 'Carter Hall East', null, null, null, null, '-83.612379', '41.655496');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (54, 'UT', 'CW', 'Carter Hall West', null, null, null, null, '-83.613004', '41.655487');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (55, 'UT', 'TC', 'Transportation Center', null, null, null, null, '-83.615214', '41.654451');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (56, 'UT', 'MC', 'Medical Center', null, null, null, null, '-83.620089', '41.656792');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (57, 'UT', 'IH', 'Horton International House', null, null, null, null, '-83.61852', '41.656614');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (58, 'UT', 'SL', 'Sullivan Hall', null, null, null, null, '-83.617531', '41.657343');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (59, 'UT', 'AH', 'Academic House', null, null, null, null, '-83.618511', '41.657566');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (60, 'UT', 'LC', 'Law Center', null, null, null, null, '-83.619833', '41.658558');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (61, 'UT', 'PA', 'Center for Performing Arts', null, null, null, null, '-83.618775', '41.658833');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (62, 'UT', 'WO', 'Wolfe Hall', null, null, null, null, '-83.61752', '41.65982');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (63, 'UT', 'BO', 'Bowman-Oddy Laboratories', null, null, null, null, '-83.61839', '41.659398');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (64, 'UT', 'WR', 'West Parking Garage', null, null, null, null, '-83.618179', '41.660869');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (65, 'UT', 'FC', 'Fetterman Training Center', null, null, null, null, '-83.609577', '41.659403');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (66, 'UT', 'ER', 'East Parking Garage', null, null, null, null, '-83.609421', '41.662389');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (67, 'HSC', 'MLB', 'Mulford Library Building, Raymond H.', null, null, null, null, '-83.615419', '41.619938');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (68, 'HSC', 'COB', 'Collier Building, Howard L.', null, null, null, null, '-83.613772', '41.619457');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (69, 'HSC', 'FSB', 'Facilities Support Building', null, null, null, null, '-83.612457', '41.61864');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (70, 'HSC', 'GMW', 'Gas Meter Building West', null, null, null, null, '-83.611766', '41.619007');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (71, 'HSC', 'CCE', 'Center for Creative Education', null, null, null, null, '-83.614528', '41.618293');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (72, 'HSC', 'HSB', 'Health Science Building', null, null, null, null, '-83.614911', '41.618544');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (73, 'HSC', 'HEB', 'Health Education Building', null, null, null, null, '-83.616343', '41.618494');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (74, 'HSC', 'DOW', 'Dowling Hall, Ida Marie', null, null, null, null, '-83.617804', '41.619183');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (75, 'HSC', 'UMC', 'University Medical Center', null, null, null, null, '-83.616264', '41.620098');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (76, 'HSC', 'RHC', 'Rupert Health Center', null, null, null, null, '-83.61757', '41.617625');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (77, 'HSC', 'KOB', 'Kobacker Center, Lenore W. and Marvin S.', null, null, null, null, '-83.615633', '41.616637');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (78, 'HSC', 'DCC', 'Dana Cancer Center, Eleanor N.', null, null, null, null, '-83.617903', '41.61571');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (79, 'HSC', 'NWT', 'Northwest Ohio Medical Tech. Center', null, null, null, null, '-83.607213', '41.622466');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (80, 'HSC', 'UEC', 'University Energy Center', null, null, null, null, '-83.605121', '41.620258');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (81, 'HSC', 'COM', 'Communications Building', null, null, null, null, '-83.605256', '41.620388');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (82, 'HSC', 'GME', 'Gas Meter Building East', null, null, null, null, '-83.605504', '41.620397');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (83, 'HSC', 'GEN', 'Generator Building', null, null, null, null, '-83.605854', '41.620218');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (84, 'HSC', 'RRC', 'Records Retention Center', null, null, null, null, '-83.603051', '41.620048');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (85, 'HSC', 'LIC', 'Laboratory Incubator Center', null, null, null, null, '-83.603662', '41.620914');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (86, 'HSC', 'GMC', 'Glendale Medical Center', null, null, null, null, '-83.623609', '41.612028');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (87, 'HSC', 'VAB', 'Veteran''s Administration Building', null, null, null, null, '-83.622929', '41.61202');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (88, 'HSC', 'EDU', 'Educare Center', null, null, null, null, '-83.620024', '41.604259');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (89, 'SP', 'BS', 'Basic Science Laboratory', null, null, null, null, '-83.59753', '41.641975');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (90, 'SP', 'CC', 'Classroom Center', null, null, null, null, '-83.597982', '41.642514');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (91, 'SP', 'AS', 'Academic Services, Learning Resource Center, Student Center, Faculty Annex', null, null, null, null, '-83.595786', '41.642667');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (92, 'SP', 'FX', 'Findlay Athletic Center', null, null, null, null, '-83.594563', '41.64212');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (93, 'SP', 'ET', 'Engineering Technology Lab Center', null, null, null, null, '-83.598472', '41.641112');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (94, 'SP', 'WB', 'Westwood Annex', null, null, null, null, '-83.604189', '41.642015');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (95, 'SP', 'NS', 'Non-Academic Service Center', null, null, null, null, '-83.601036', '41.642304');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (96, 'SP', 'BP', 'Baseball & Softball Practice Facility', null, null, null, null, '-83.595281', '41.641315');

insert into maps (ID, CAMPUS_CODE, BUILDING_CODE, BUILDING_NAME, BUILDING_DESC, PHONE, EMAIL, IMG_URL, LONGITUDE, LATITUDE)
values (97, 'NAFCS', 'NH', 'New Albany High School', null, null, null, null, '-85.811514', '38.306642');

prompt Done.
