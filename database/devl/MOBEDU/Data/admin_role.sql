prompt Importing table admin_role...
set feedback off
set define off
insert into admin_role (CODE, DESCRIPTION)
values ('SUPERADMIN', 'Super Admin');

insert into admin_role (CODE, DESCRIPTION)
values ('NOTIFADMIN', 'Notifications Admin');

insert into admin_role (CODE, DESCRIPTION)
values ('FEEDADMIN', 'Feeds Admin');

insert into admin_role (CODE, DESCRIPTION)
values ('APPADMIN', 'App Admin');

prompt Done.
