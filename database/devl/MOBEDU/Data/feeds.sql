prompt Importing table FEEDS...
set feedback off
set define off
insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (643, 3, 'MENU', null, null, 3, 'Games', 'games', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (644, 643, 'LINK', 'url', 'xml', 3, 'myLink', 'icon', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1047, 2, 'LINK', 'https', 'xml', 2, 'LINK', 'LINK', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1047, 1047, 'MENU', null, null, 2, 'Games', 'Sports.jpeg', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1001, 0, 'MENU', null, null, 1001, 'News', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1021, 1001, 'LINK', 'http://www.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'Corporate', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1022, 1001, 'LINK', 'http://prosser.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'Prosser', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1023, 1001, 'LINK', 'http://fchs.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'Floyd Central HS', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1024, 1001, 'LINK', 'http://nahs.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'New Albany HS', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1025, 1001, 'LINK', 'http://hhms.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'Highland Hills MS', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1026, 1001, 'LINK', 'http://hms.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'Hazelwood MS', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1027, 1001, 'LINK', 'http://sms.nafcs.k12.in.us/feed.rss.asp', 'xml', 1001, 'Scribner MS', 'news.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1002, 0, 'MENU', null, null, 1002, 'Athletics', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1028, 1002, 'LINK', 'http://www.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'Corporate', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1029, 1002, 'LINK', 'http://prosser.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'Prosser', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1030, 1002, 'LINK', 'http://fchs.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'Floyd Central HS', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1031, 1002, 'LINK', 'http://nahs.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'New Albany HS', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1032, 1002, 'LINK', 'http://hhms.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'Highland Hills MS', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1033, 1002, 'LINK', 'http://hms.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'Hazelwood MS', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1034, 1002, 'LINK', 'http://sms.nafcs.k12.in.us/default.asp?q_areaprimaryid=11', 'xml', 1002, 'Scribner MS', 'athletics.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1003, 1003, 'LINK', 'http://nahs.nafcs.k12.in.us/feed.rss.events.asp', 'xml', 1003, 'Events', 'events.png', 'NAFCS');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (148, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=36', 'xml', 1, 'Honors', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (149, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=37', 'xml', 1, 'Languages, Literature and Social Sciences', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (150, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=30', 'xml', 1, 'Law', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (151, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=38', 'xml', 1, 'Medicine and Life Sciences', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (152, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=39', 'xml', 1, 'Natural Sciences and Mathematics', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (153, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=40', 'xml', 1, 'Nursing', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (154, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=41', 'xml', 1, 'Online Learning', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (155, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=42', 'xml', 1, 'Pharmacy and Pharmaceutical Sciences', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (156, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=43', 'xml', 1, 'Visual and Performing Arts', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (157, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=44', 'xml', 1, 'Alumni', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (158, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=45', 'xml', 1, 'Library', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (159, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=46', 'xml', 1, 'UTMC', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (160, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=47', 'xml', 1, 'Student Affairs', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (161, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=48', 'xml', 1, 'Institutional Advancement', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (162, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=49', 'xml', 1, 'Research', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (163, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed ', 'xml', 1, 'UT News (top stories, external audience)', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (164, 1, 'LINK', 'http://intranet.utoledo.edu/feeds/myut.xml', 'xml', 1, 'UT Announcements (lower level, internal audience)', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (165, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/category/utoday/feed', 'xml', 1, 'Utoday (top stories, internal audience)', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (166, 1, 'LINK', 'http://intranet.utoledo.edu/feeds/blogs.xml', 'xml', 1, 'University Journals (blogs for internal/external audience)', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (167, 1, 'LINK', 'http://gdata.youtube.com/feeds/api/playlists/28618F71CFFDF3D2', 'xml', 1, 'UT YouTube videos', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (168, 1, 'LINK', 'http://independentcollegian.com/se/1.904295', 'xml', 1, 'Independent Collegian (student newspaper) - News RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (169, 1, 'LINK', 'http://independentcollegian.com/se/1.904296', 'xml', 1, 'Independent Collegian - Sports RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (170, 1, 'LINK', 'http://independentcollegian.com/se/1.904298', 'xml', 1, 'Independent Collegian - Forum RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (171, 1, 'LINK', 'http://independentcollegian.com/se/1.904297', 'xml', 1, 'Independent Collegian - Arts & Life RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (172, 1, 'LINK', 'http://independentcollegian.com/se/1.904299-1.904299', 'xml', 1, 'Independent Collegian - Multimedia RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (173, 1, 'LINK', 'http://independentcollegian.campusave.com/rss.php?channelID=3', 'xml', 1, 'Independent Collegian - Classifieds - For Sale RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (174, 1, 'LINK', 'http://independentcollegian.campusave.com/rss.php?channelID=1', 'xml', 1, 'Independent Collegian - Classifieds - Housing RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (175, 1, 'LINK', 'http://independentcollegian.campusave.com/rss.php?channelID=4', 'xml', 1, 'Independent Collegian - Classifieds - Services RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (176, 1, 'LINK', 'http://independentcollegian.campusave.com/rss.php?channelID=2', 'xml', 1, 'Independent Collegian - Classifieds - Around Town RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (177, 1, 'LINK', 'http://independentcollegian.campusave.com/rss.php?channelID=5', 'xml', 1, 'Independent Collegian - Classifieds - Jobs RSS', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (1, 0, 'MENU', null, null, 1, 'Campus News', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (615, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=hhAbVFpDFO7OxcTcYlM9Lv1PU2%2fQTq8aMz712S0M1Mh5pYMawq7xxQ%3d%3d', 'xml', 3, 'Academic Calendar', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (616, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=fKKMKomtzIRQrJieD3uz15A0H4y0nZA5K%2bMwgbbp7fssBVcxM7A5MKt2q%2fyxklti', 'xml', 3, 'Admission & Visit Programs', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (617, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=sdJ%2fQt3yAULOHYrcvLhxTdMMIbP1tVZr4%2bLeCFu%2bXqc%3d', 'xml', 3, 'Art Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (618, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=lnMl0JholnLqY4bB7FYDfWM9tpRpflYgDszLnidZgfA6%2fFgHCOaE91eJaItkcDic6WlfbCNqMSc%3d', 'xml', 3, 'Arts, Music, Theatre, and Film Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (619, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=OiNeXA6LJItp%2bLkkMsbi4%2bK36EXPKEaiidIoMRpAv1UOaYUzqbTyCLFy%2fL6LS2ra', 'xml', 3, 'College of Arts and Sciences', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (620, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=rWVImWG4wi1iC5u2UcXAOW0oKN%2f82Y2uLnbaphljFnd0dP5ZQQ7NspkRN%2buwDs%2bj9lgqQUhdEg8%3d', 'xml', 3, 'College of Business Administration', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (621, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=rWVImWG4wi1iC5u2UcXAOW0oKN%2f82Y2uLnbaphljFnd0dP5ZQQ7NspkRN%2buwDs%2bj9lgqQUhdEg8%3d', 'xml', 3, 'College of Business Administration', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (622, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=VXLCORHhDCF9BYlLwAXiICmRq6wmB0A%2bBV0XQsO0kIZlO5zhNysCQ%2fydDD8O7r6s', 'xml', 3, 'College of Education and HSHS', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (623, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=mL1noRCrG1I%2fLTuo7OuVK4bjZ%2fOyB3KD2CsD9hEAFlYcD8%2bgzIuElA%3d%3d', 'xml', 3, 'College of Engineering', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (624, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=xK80F%2fbc%2f4UErlXy9atsfwB8ExnZqlQF48fyFbjcjJH5F3czbDo81WpLmHtEhcqNdEs%2bV3GxF0C7Y7w32uiXBA%3d%3d', 'xml', 3, 'College of Health Science & Human Service', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (625, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=E6dodmaNJ5NKdXCNLg2VPnEpNIVl5YmoRRwcbCLHbJk%3d', 'xml', 3, 'College of Law', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (626, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=qaUdY1wvEciJ2wvy%2fMP8soa2y4DWXn5NMTjOkEIa%2bO4NZFeX4Wi3iA%3d%3d', 'xml', 3, 'College of Medicine', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (627, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=HwqQnFd0XZw2ELTJ3w4YjXYEYpfROJIciBpB4mK1fgCY5bvuPgp9lQ%3d%3d', 'xml', 3, 'College of Nursing', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (628, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=exfsp9BbGwOa3zSOgl8KgEyTLH%2fFRxR48gXM4WIaw6asMOKZW5Bdbw%3d%3d', 'xml', 3, 'College of Pharmacy', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (629, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=dpjoLfD6uUgXzZkDLqbCyF8islTCbhYcHKvj%2fN3mgORX9ixdJoz5pA%3d%3d', 'xml', 3, 'Diversity Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (630, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=JsmUV31cWCY%2fJDT2ED9Qs%2bNvNSUL4Q6%2bFSATBZLdepCUJ%2fU0fa7DYA%3d%3d', 'xml', 3, 'HSC Student Activities', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (631, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=wADdD9yffs%2fBGbmzbTBOccrHZo43urkw1hzYAFWH3vt6hipzAco9%2bS6q7SI82rTFfKw%2b9X0ptNhvjgZxwFziYg%3d%3d', 'xml', 3, 'Important Dates & Deadlines - MyUT Faculty', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (632, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=qtalE2hWSqcsLfNfNoFG2sPV3oaOLUqyrNqmjoCyENn3dDmAnfns0QjbU6x730L8l05iQScAkAZIniiedu7MJQ%3d%3d', 'xml', 3, 'Important Dates & Deadlines - MyUT Student', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (633, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=j%2fxqvgLhwZs%2bG6vJ6fo56bvmib5SydzA4z20BPP0u0Q%3d', 'xml', 3, 'InsideUT', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (634, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=mhalGvYQmVVe4idPkK%2bt7GU%2fmc1g3Ga2Z%2fAIROzbFdq2IdrlcQZB2Zuz5DoUe1iqRM0341POsMg%3d', 'xml', 3, 'International Studies and Programs', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (635, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=R90GAApNPam4GY6v0AEJ1Y8uW9oQfcZRd0Z34%2ff9AMY%3d', 'xml', 3, 'Music Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (636, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=H2GRpTFklveE6KQ%2b0BAw5Ao7llIfTky%2bhtYJgITWWhOpDTI9pSu98ad7fmXJqt79ICnemjWlAC0%3d', 'xml', 3, 'Natural Science and Mathematics', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (637, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=lHvYsBf4encWo1oY4bd%2bg4P0IhQGhGxe1C8zauZVdDy3xeXTUyr8%2bJwH5nFQdtvVf1DyXEMZdGNzT%2fi1NJTHnQ%3d%3d', 'xml', 3, 'Office of Research Collaboration Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (638, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=6N%2bM7u2dU1sLnA7F%2fIU6TxpmnFPEZlVl8WN%2f0%2fybITL3GmU60orxwaIqHjO4M2vD', 'xml', 3, 'Research Campus Deadline', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (639, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=brsmCHWO1FinSXDSstpxf3IRnm0ZmQyI6YvbiSfdOti%2fpwPoslujv8ykWGzihusM', 'xml', 3, 'Research Sponsor Deadline', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (640, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=LbRHuXjdMpEESeFMowqcRBqizUZmPQy7agfvR4EyHcoK79qewB0FF1nhRZF0TGI0YY5X0dau4cQ%3d', 'xml', 3, 'Student Activities Events - MyUT', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (641, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=hcXQkEgwJvxUXnGiwD1523Jrd0ri8V5ypoTHB9uJ9%2bUt881nG0ILIg%3d%3d', 'xml', 3, 'Theatre/Film Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (642, 3, 'LINK', 'http://calendar.utoledo.edu//RSSFeeds.aspx?data=tA%2bhCNXmZerO%2bljV3wfOHaT9whKXJN9AvpsjiwnBuq32XJwlxADWlA%3d%3d', 'xml', 3, 'University College', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (3, 0, 'MENU', null, null, 3, 'Events', 'events.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (2, 0, 'MENU', null, null, 2, 'Athletics', 'sports.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (270, 212, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10713', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (271, 212, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10713', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (272, 213, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=38028', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (273, 214, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=37421', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (274, 215, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=37721', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (275, 216, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10722', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (276, 216, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10722', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (277, 216, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10722', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (278, 216, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10722', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (279, 216, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10722', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (280, 217, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=11441', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (281, 217, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=11441', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (282, 217, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=11441', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (283, 217, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=11441', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (284, 217, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=11441', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (285, 218, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10705', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (286, 218, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10705', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (287, 218, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10705', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (288, 218, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10705', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (289, 218, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10705', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (290, 219, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10709', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (291, 219, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10709', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (292, 219, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10709', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (293, 219, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10709', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (294, 219, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10709', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (295, 220, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10697', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (296, 220, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10697', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (297, 220, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10697', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (298, 220, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10697', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (299, 220, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10697', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (300, 221, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10716', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (301, 221, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10716', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (302, 221, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10716', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (303, 221, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10716', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (304, 221, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10716', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (305, 222, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10712', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (306, 222, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10712', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (307, 222, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10712', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (308, 222, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10712', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (309, 222, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10712', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (310, 223, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10717', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (311, 223, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10717', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (312, 223, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10717', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (313, 223, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10717', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (314, 223, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10717', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (201, 2, 'MENU', null, null, 2, 'All Sports', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (202, 2, 'MENU', null, null, 2, 'Administration', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (203, 2, 'MENU', null, null, 2, 'Athletics', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (204, 2, 'MENU', null, null, 2, 'Baseball', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (205, 2, 'MENU', null, null, 2, 'Development', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (206, 2, 'MENU', null, null, 2, 'Football', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (207, 2, 'MENU', null, null, 2, 'Men''s Basketball', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (208, 2, 'MENU', null, null, 2, 'Men''s Cross Country', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (209, 2, 'MENU', null, null, 2, 'Men''s Golf', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (210, 2, 'MENU', null, null, 2, 'Men''s Tennis', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (211, 2, 'MENU', null, null, 2, 'NCAA Championships', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (212, 2, 'MENU', null, null, 2, 'Softball', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (213, 2, 'MENU', null, null, 2, 'Student-Athlete Blogs', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (214, 2, 'MENU', null, null, 2, 'Toledo Rockets Apparel', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (215, 2, 'MENU', null, null, 2, 'Toledo Tickets', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (216, 2, 'MENU', null, null, 2, 'Women''s Basketball', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (217, 2, 'MENU', null, null, 2, 'Women''s Cross Country', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (218, 2, 'MENU', null, null, 2, 'Women''s Golf', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (219, 2, 'MENU', null, null, 2, 'Women''s Soccer ', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (220, 2, 'MENU', null, null, 2, 'Women''s Swimming & Diving', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (221, 2, 'MENU', null, null, 2, 'Women''s Tennis', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (222, 2, 'MENU', null, null, 2, 'Women''s Track', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (223, 2, 'MENU', null, null, 2, 'Women''s Volleyball', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (224, 201, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (225, 201, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (226, 201, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (227, 201, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (228, 201, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (229, 202, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10719', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (230, 202, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10719', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (231, 202, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10719', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (232, 203, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10702', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (233, 203, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10702', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (234, 204, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10718', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (235, 204, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10718', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (236, 204, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10718', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (237, 204, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10718', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (238, 204, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10718', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (239, 205, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10700', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (240, 205, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10700', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (241, 206, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10708', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (242, 206, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10708', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (243, 206, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10708', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (244, 206, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10708', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (245, 206, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10708', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (246, 207, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10721', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (247, 207, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10721', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (248, 207, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10721', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (249, 207, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10721', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (250, 207, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10721', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (251, 208, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=11405', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (252, 208, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=11405', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (253, 208, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=11405', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (254, 208, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=11405', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (255, 208, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=11405', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (256, 209, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10714', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (257, 209, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10714', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (258, 209, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10714', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (259, 209, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10714', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (260, 209, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10714', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (261, 210, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10715', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (262, 210, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10715', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (263, 210, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10715', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (264, 210, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=schedules&RSS_SPORT_ID=10715', 'xml', 2, 'Schedules', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (265, 210, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=photos&sport_id=10715', 'xml', 2, 'Photos', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (266, 211, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=100095', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (267, 212, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=news&RSS_SPORT_ID=10713', 'xml', 2, 'News', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (268, 212, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=ondemand&RSS_SPORT_ID=10713', 'xml', 2, 'Media', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (269, 212, 'LINK', 'http://www.utrockets.com/rss.dbml?db_oem_id=18000&media=results&RSS_SPORT_ID=10713', 'xml', 2, 'Results', 'baseball.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (143, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=31', 'xml', 1, 'Adult and Lifelong Learning', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (144, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=32', 'xml', 1, 'Business and Innovation', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (145, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=33', 'xml', 1, 'Education, Health Science and Human Service', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (146, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=34', 'xml', 1, 'Engineering', 'news.png', 'UT');

insert into FEEDS (ID, PARENTID, TYPE, LINK, FORMAT, MODULEID, NAME, ICON, CAMPUSCODE)
values (147, 1, 'LINK', 'http://utnews.utoledo.edu/index.php/feed?cat=35', 'xml', 1, 'Graduate Studies', 'news.png', 'UT');

prompt Done.
