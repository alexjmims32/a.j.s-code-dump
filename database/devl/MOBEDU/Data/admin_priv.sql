prompt Importing table admin_priv...
set feedback off
set define off
insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('SUPERADMIN', 'NOTIFICATIONS', 'Y', 'Notifications Module Access');

insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('SUPERADMIN', 'FEEDS', 'N', 'Feeds Module Access');

insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('APPADMIN', 'MAPS', 'Y', 'Maps Module Access');

insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('NOTIFADMIN', 'NOTIFICATIONS', 'Y', 'Notifications Module Access');

insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('FEEDADMIN', 'FEEDS', 'Y', 'Feeds Module Access');

insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('APPADMIN', 'EMERGENCYCONTACTS', 'Y', 'Emargency Contacts Module Access');

insert into admin_priv (ROLE, PRIVCODE, ACCESSFLAG, DESCRIPTION)
values ('APPADMIN', 'ROLES', 'Y', 'Roles And Privileges Module Access');

prompt Done.
