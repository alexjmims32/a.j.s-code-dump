PROMPT CREATE OR REPLACE VIEW mobedu.terms_to_register_vw
CREATE OR REPLACE FORCE VIEW mobedu.terms_to_register_vw (
  term_code,
  term_desc
) AS
SELECT STVTERM_CODE TERM_CODE, STVTERM_DESC TERM_DESC
        FROM STVTERM, SORRTRM, SOBTERM
       WHERE SOBTERM_DYNAMIC_SCHED_TERM_IND = 'Y'
         AND STVTERM_CODE = SOBTERM_TERM_CODE
         AND EXISTS (SELECT 'X'
                       FROM SSBSECT
                      WHERE SSBSECT_TERM_CODE = SOBTERM_TERM_CODE)
         AND SOBTERM_REG_ALLOWED = 'Y'
         AND SORRTRM_TERM_CODE = SOBTERM_TERM_CODE
         AND TRUNC (SYSDATE) BETWEEN SORRTRM_START_DATE AND SORRTRM_END_DATE
/

