PROMPT CREATE OR REPLACE PROCEDURE mobedu.p_test
CREATE OR REPLACE procedure mobedu.p_test (p_id number, o_id number) as

pragma autonomous_transaction;

begin

 update test1 set ref_id = p_id where ref_id = o_id;

end;
/

