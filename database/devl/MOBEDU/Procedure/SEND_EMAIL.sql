PROMPT CREATE OR REPLACE PROCEDURE mobedu.send_email
CREATE OR REPLACE PROCEDURE mobedu.send_email(p_email_text IN VARCHAR2,
                                       p_subject    IN VARCHAR2,
                                       p_email_id   IN VARCHAR2,
                                       p_from_email IN VARCHAR2) IS
  --
  conn_server    UTL_SMTP.connection;
  g_email_server varchar2(500);
  --
  PROCEDURE send_header(NAME IN VARCHAR2, header IN VARCHAR2) IS
  BEGIN
    UTL_SMTP.write_data(conn_server,
                        NAME || ': ' || header || UTL_TCP.crlf);
  END send_header;
  --
BEGIN
  g_email_server:='smtp.gmail.com';
  conn_server := UTL_SMTP.open_connection(g_email_server);
  UTL_SMTP.helo(conn_server, g_email_server);
  UTL_SMTP.mail(conn_server, p_from_email); -- sender
  UTL_SMTP.rcpt(conn_server, p_email_id); -- recipient
  UTL_SMTP.open_data(conn_server);
  --
  send_header('From', p_from_email);
  send_header('To', '''Recipient'' <' || p_email_id || '>');
  send_header('Subject', p_subject);
  --
  UTL_SMTP.write_data(conn_server, UTL_TCP.crlf || p_email_text);
  UTL_SMTP.close_data(conn_server);
  UTL_SMTP.quit(conn_server);
  --

END send_email;
/

