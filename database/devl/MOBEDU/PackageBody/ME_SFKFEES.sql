PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.me_sfkfees
CREATE OR REPLACE PACKAGE BODY mobedu.me_sfkfees as
  --AUDIT_TRAIL_MSGKEY_UPDATE
  -- PROJECT : MSGKEY
  -- MODULE  : SFKFEE1
  -- SOURCE  : enUS
  -- TARGET  : I18N
  -- DATE    : Fri Feb 05 11:09:17 2010
  -- MSGSIGN : #dc57f382a405fa7f
  --TMI18N.ETR DO NOT CHANGE--
  --
  -- FILE NAME..: sfkfee1.sql
  -- RELEASE....: 8.3.0.3
  -- OBJECT NAME: SFKFEES
  -- PRODUCT....: STUDENT
  -- USAGE......: Package Body for procedures and functions needed for
  --              Registration Fee Assessment processing.
  -- COPYRIGHT..: COPYRIGHT..:  2003, 2009 SunGard. All rights reserved.
  --  This  software  contains confidential and proprietary information of SunGard and its subsidiaries.
  --   Use of this  software  is limited to SunGard Higher Education licensees, and is subject to the terms
  --   and conditions of one or more written license agreements between SunGard Higher Education and the
  --   licensee in question.
  --
  -- DESCRIPTION:
  --
  --   This package spec is for the supporting procedures and functions
  --   needed for Fee Assessment processing. These procedures and functions
  --   are common and are called from all places that perform fee
  --   assessment across Banner.
  --
  --   FUNCTIONS:
  --      f_calc_per_cred_charge
  --      f_calc_rule_hrs
  --      f_DD_since_last_assessment
  --      f_establishoriginalcharge
  --      f_ests_means_wd
  --      f_ests_wd_assessed
  --      f_get_accd_eff_date
  --      f_get_DD_handled_date
  --      f_get_prev_nondrop_hrs
  --      f_get_sfrfaud_seqno
  --      f_get_sfrstcr_date
  --      f_get_tbbdetc_dcat
  --      f_prev_flat_rule_met
  --      f_rsts_include_in_assess
  --      f_rsts_means_wd
  --      f_rule_type_defined
  --      f_sfrfmax_defined
  --      f_tbraccd_exists
  --      f_flat_rule_exists
  --     f_last_dd_activity_date
  --
  --   PROCEDURES:
  --      p_additionalfees
  --      p_applyrules
  --      p_attributefees
  --      p_calc_flat_hr_liability
  --      p_calc_rbt_refunds
  --      p_calc_reg_hr_liability
  --      p_calc_rfst_liability
  --      p_calc_swap_liability
  --      p_courses_by_rfcr
  --      p_create_nondrop_hrs_sfrfaud
  --      p_create_nondrop_type
  --      p_delete_mock_assessment
  --      p_delete_sfrbtch
  --      p_extensionfees
  --      p_get_last_assess_flat_data
  --      p_get_sfrstcr_data
  --      p_init_global_vars
  --      p_insert_sfrfaud
  --      p_insert_tbraccd_rec
  --      p_post_rbt_penalties
  --      p_processcourse
  --      p_process_etrm_drop
  --      p_processfeeassessment
  --      p_process_hours_swap
  --      p_reinit_temp_vars
  --      p_rev_nonswap_RBT
  --      p_reverse_na_charges
  --      p_sectionfees
  --      p_set_rbt_variables
  --      p_set_rfst_variables
  --      p_studentcourses
  --      p_calcliability
  --      p_print_dbms
  ---    p_multiple_rules
  --
  -- DESCRIPTION END
  --
  --
  -- FUNCTIONS
  --

  -- 12345678901234567890123456789012345678901234567890123456789012345678901234567890

  /* ****************************************************************** */
  /* 7.1: Function to determine if DD occurred since last assessment.   */
  /* ****************************************************************** */
  FUNCTION f_DD_since_last_assessment(pidm_in IN spriden.spriden_pidm%TYPE,
                                      term_in IN stvterm.stvterm_code%TYPE)
    RETURN BOOLEAN IS

    --- 8.2 split the sfvregd cursor into 2 to improve on performance
    CURSOR drop_delete_c IS
      SELECT 'Y'
        FROM STVRSTS, sfrstcr
       WHERE STVRSTS_CODE = sfrstcr_RSTS_CODE
         and sfrstcr_term_code = term_in
         and sfrstcr_pidm = pidm_in
         and stvrsts_incl_assess = 'N'
         and sfrstcr_activity_date > sfbetrm_rec.sfbetrm_assessment_date;

    CURSOR drop_delete_regd_c IS
      SELECT 'Y'
        FROM STVRSTS, SFRREGD
       WHERE STVRSTS_CODE = SFRREGD_RSTS_CODE
         and sfrregd_term_code = term_in
         and sfrregd_pidm = pidm_in
         and stvrsts_incl_assess = 'N'
         and sfrregd_activity_date > sfbetrm_rec.sfbetrm_assessment_date;

    drop_delete_ind      VARCHAR2(1) := 'N';
    drop_delete_regd_ind VARCHAR2(1) := 'N';
  BEGIN
    p_print_dbms('f dd since last assessments begins');
    OPEN drop_delete_c;
    FETCH drop_delete_c
      INTO drop_delete_ind;
    IF drop_delete_c%NOTFOUND THEN
      drop_delete_ind := 'N';
      OPEN drop_delete_regd_c;
      FETCH drop_delete_regd_c
        into drop_delete_regd_ind;
      IF drop_delete_regd_c%NOTFOUND then
        drop_delete_regd_ind := 'N';
      END IF;
      CLOSE drop_delete_regd_c;
    END IF;
    CLOSE drop_delete_c;
    p_print_dbms('drop delete ind ' || drop_delete_ind || ' regd: ' ||
                 drop_delete_regd_ind);
    RETURN(drop_delete_ind = 'Y' OR drop_delete_regd_ind = 'Y');
  END f_DD_since_last_assessment;

  /* ****************************************************************** */
  /* 7.1: Function to determine assessment date when DD was handled.    */
  /* ****************************************************************** */
  FUNCTION f_get_DD_handled_date(pidm_in IN spriden.spriden_pidm%TYPE,
                                 term_in IN stvterm.stvterm_code%TYPE)
    RETURN DATE IS
    CURSOR DD_handled_date_c IS
      SELECT MAX(sfrfaud_activity_date)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_assess_rfnd_penlty_cde = 'D'
         AND sfrfaud_activity_date >
             NVL(intermediate_date,
                 TO_DATE(G$_DATE.NORMALISE_GREG_DATE('01-01-1900',
                                                     'DD-MM-YYYY'),
                         G$_DATE.GET_NLS_DATE_FORMAT));

    handled_date DATE;
  BEGIN
    p_print_dbms('f get dd handled date begins, intermediate date: ' ||
                 to_char(intermediate_date, 'DD-MON-YYYY HH24:MI:SS'));
    OPEN DD_handled_date_c;
    FETCH DD_handled_date_c
      INTO handled_date;
    IF DD_handled_date_c%NOTFOUND THEN
      handled_date := NULL;
    END IF;
    CLOSE DD_handled_date_c;
    p_print_dbms('handled date ' ||
                 to_char(handled_date, 'DD-MON-YYYY HH24:MI:SS'));
    RETURN handled_date;
  END f_get_DD_handled_date;

  /* ****************************************************************** */
  /* Function to retreive the activity date on the most recent DD          */
  /* ****************************************************************** */
  FUNCTION f_last_dd_activity_date(pidm_in IN spriden.spriden_pidm%TYPE,
                                   term_in IN stvterm.stvterm_code%TYPE)
    RETURN DATE IS
    last_dd_date date := null;
    CURSOR DD_activity_date_c IS
      SELECT MAX(sfrstca_activity_date)
        FROM stvrsts, sfrstca
       WHERE sfrstca_pidm = pidm_in
         AND sfrstca_term_code = term_in
         AND stvrsts_code = sfrstca_rsts_code
         AND stvrsts_incl_assess = 'N';

  BEGIN
    OPEN DD_activity_date_c;
    FETCH DD_activity_date_c
      INTO last_dd_date;
    IF DD_activity_date_c%NOTFOUND THEN
      last_dd_date := NULL;
    END IF;
    CLOSE DD_activity_date_c;
    p_print_dbms('DD act date ' ||
                 to_char(last_dd_date, 'DD-MON-YYYY HH24:MI:SS'));
    RETURN last_dd_date;

  end f_last_dd_activity_date;

  /* ****************************************************************** */
  /* 7.4.0.2 Function to find if there was a flat, drop and change in credit hours for intermediate         */
  /* ****************************************************************** */
  FUNCTION f_var_credit_intermediate(pidm_in IN spriden.spriden_pidm%TYPE,
                                     term_in IN stvterm.stvterm_code%TYPE)
    RETURN BOOLEAN IS

    drop_exists            varchar2(1) := 'N';
    flat_exists            varchar2(1) := 'N';
    variable_credit_change varchar2(1) := 'N';

    CURSOR drop_exists_c IS
      SELECT 'Y'
        FROM stvrsts, sfrrfcr, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND stvrsts_code = sfrstcr_rsts_code
         AND stvrsts_withdraw_ind = 'Y'
         AND stvrsts_incl_assess = 'Y'
         AND sfrrfcr_term_code = sfrstcr_term_code
         AND sfrrfcr_ptrm_code = sfrstcr_ptrm_code
            --- make sure the drop was a liability
         AND ((sfrrfcr_tuit_refund <> 0 and sfrrfcr_tuit_refund <> 100) OR
             (sfrrfcr_fees_refund <> 0 and sfrrfcr_fees_refund <> 100))
         AND trunc(sfrstcr_rsts_date) between sfrrfcr_from_date AND
             sfrrfcr_to_date;

    --- 8.0.1 remove sfrrgfe, it is recorded on the sfrfaud record and not needed in select
    CURSOR flat_exists_c IS
      SELECT 'Y'
        FROM sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_rgfe_seqno > 0
         AND a.sfrfaud_flat_fee_amount > 0;
    -- AND sfrrgfe_term_code = term_in
    --AND sfrrgfe_type = a.sfrfaud_rgfe_type
    --AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
    -- AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;

    CURSOR variable_credit_change_c is
      SELECT sfrstcr_activity_date
        from stvrsts r, stvrsts a, sfrstca, sfrstcr
       where r.stvrsts_incl_assess = 'Y'
         and r.stvrsts_withdraw_ind = 'N'
         and r.stvrsts_code = sfrstcr_rsts_code
         and sfrstca_crn = sfrstcr_crn
            --- 8.1.1 only compare to non dropped courses
         and a.stvrsts_incl_assess = 'Y'
         and a.stvrsts_withdraw_ind = 'N'
         and a.stvrsts_code = sfrstca_rsts_code
         and sfrstca_crn = sfrstcr_crn
         and sfrstca_pidm = sfrstcr_pidm
         and sfrstca_term_code = sfrstcr_term_code
         and sfrstcr_pidm = pidm_in
         and sfrstcr_term_code = term_in
         and sfrstcr_bill_hr <> sfrstca_bill_hr
            --- change happened after the last assessment
         and sfrstcr_activity_date > sfbetrm_rec.sfbetrm_assessment_date
            --- select the previous sfrstca to compare to sfrstcr
         and sfrstca_activity_date =
             (select max(m.sfrstca_activity_date)
                from sfrstca m
               where m.sfrstca_pidm = sfrstcr.sfrstcr_pidm
                 and m.sfrstca_term_code = sfrstcr.sfrstcr_term_code
                 and m.sfrstca_crn = sfrstcr.sfrstcr_crn
                 and m.sfrstca_activity_date < sfrstcr.sfrstcr_activity_date);

  BEGIN
    variable_act_date := null;
    IF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'N' THEN

      OPEN drop_exists_c;
      FETCH drop_exists_c
        INTO drop_exists;
      IF drop_exists_c%NOTFOUND THEN
        drop_exists := 'N';
      END IF;
      CLOSE drop_exists_c;
      p_print_dbms('drop exists? ' || drop_exists);

      if drop_exists = 'Y' then
        OPEN flat_exists_c;
        FETCH flat_exists_c
          INTO flat_exists;
        IF flat_exists_c%NOTFOUND THEN
          flat_exists := 'N';
        END IF;
        CLOSE flat_exists_c;

        p_print_dbms('flat exists ? ' || flat_exists);

        if flat_exists = 'Y' then
          OPEN variable_credit_change_c;
          FETCH variable_credit_change_c
            INTO variable_act_date;
          IF variable_credit_change_c%NOTFOUND THEN
            variable_credit_change := 'N';
          else
            variable_credit_change := 'Y';
          END IF;
          CLOSE variable_credit_change_c;
          p_print_dbms('variable credit change? ' ||
                       variable_credit_change || ' date: ' ||
                       to_char(variable_act_date, 'DD-MON-YYYY HH24:MI:SS'));
        end if;
      end if;
    END IF; --- refund by course only

    if drop_exists = 'Y' and flat_exists = 'Y' and
       variable_credit_change = 'Y' then
      p_print_dbms('Variable credit change, flat and drop also, need to execute intermediate');
      return(TRUE);
    else
      p_print_dbms('Varible credit change, flat or drop do not exists so do not execute intermediate');
      return(FALSE);
    end if;

  end f_var_credit_intermediate;

  /* ******************************************************************* */
  /* Function to determine the setting of the TBRACCD_ORIG_CHG_IND.      */
  /* This code was copied from package SFKFCAL and is used by Location   */
  /* Management Assessment in SLRFASM.pc. Moving the function code here  */
  /* allowed the obsoletion of package SFKFCAL.                          */
  /* ******************************************************************* */
  FUNCTION f_establishoriginalcharge(pidm_in        IN spriden.spriden_pidm%TYPE,
                                     term_code_in   IN stvterm.stvterm_code%TYPE,
                                     source_code_in IN VARCHAR2)
    RETURN VARCHAR2 IS
    orig_chg_ind VARCHAR2(1);
    CURSOR tbraccd_term_srce_cursor IS
      SELECT 'N'
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code || '' = term_code_in
         AND tbraccd_srce_code = source_code_in;
  BEGIN

    OPEN tbraccd_term_srce_cursor;
    FETCH tbraccd_term_srce_cursor
      into orig_chg_ind;
    IF tbraccd_term_srce_cursor%notfound THEN
      orig_chg_ind := 'Y';
    END IF;
    CLOSE tbraccd_term_srce_cursor;
    p_print_dbms('f establish original charge: orig chg ind ' ||
                 orig_chg_ind);
    RETURN orig_chg_ind;
  END f_establishoriginalcharge;

  /* ******************************************************************* */
  /* Function to determine the next seqno for the SFRFAUD record.        */
  /* ******************************************************************* */
  FUNCTION f_get_sfrfaud_seqno(pidm_in      IN spriden.spriden_pidm%TYPE,
                               term_in      IN stvterm.stvterm_code%TYPE,
                               sessionid_in IN sfrfaud.sfrfaud_sessionid%TYPE)
    RETURN NUMBER IS
    next_seqno NUMBER;
    cursor max_seqno_c(p_pidm    spriden.spriden_pidm%type,
                       p_term    stvterm.stvterm_code%type,
                       p_session varchar2) is
      SELECT NVL(MAX(sfrfaud_seqno + 1), 1)
        INTO next_seqno
        FROM sfrfaud
       WHERE sfrfaud_pidm = p_pidm
         AND sfrfaud_term_code = p_term
         AND sfrfaud_sessionid = p_session;

  BEGIN
    --- 8.0.1 remove implicit cursor
    open max_seqno_c(p_pidm    => pidm_in,
                     p_term    => term_in,
                     p_session => sessionid_in);
    fetch max_seqno_c
      into next_seqno;
    close max_seqno_c;
    RETURN next_seqno;
  END f_get_sfrfaud_seqno;

  /* ******************************************************************* */
  /* Determine if the student ever qualified for a flat charge rule for  */
  /* the rule type to cause student to fall into flat hour liability.    */
  /* ******************************************************************* */
  /* 92435: Rename from p_check_prev_flat_rule_met.                      */
  /* ******************************************************************* */
  /* 7.1: Update to check for any previous flat rule qualification.      */
  /*      Need to know this up front when having to handle DD/'not       */
  /*      count in assessment' registration activity.                    */
  /* ******************************************************************* */
  FUNCTION f_prev_flat_rule_met(pidm_in       IN spriden.spriden_pidm%TYPE,
                                term_in       IN stvterm.stvterm_code%TYPE,
                                rule_type_in  IN VARCHAR2,
                                rule_value_in IN VARCHAR2) RETURN BOOLEAN IS
    prev_flat_rule_met_ind VARCHAR2(1) := 'N';
    DD_handled_date        DATE;
    /*
     CURSOR prev_flat_rule_c IS
         SELECT 'Y'
           FROM sfrrgfe, sfrfaud a
          WHERE a.sfrfaud_pidm = pidm_in
            AND a.sfrfaud_term_code = term_in
            AND a.sfrfaud_flat_fee_amount > 0
            AND sfrrgfe_term_code = term_in
            -- AND sfrrgfe_type = rule_type_in  7.1
            AND sfrrgfe_type LIKE NVL(rule_type_in,'%')
           AND (   (rule_type_in = CAMPUS_TYPE AND sfrrgfe_camp_code_crse = rule_value_in)
                 OR (rule_type_in = LEVEL_TYPE  AND sfrrgfe_levl_code_crse = rule_value_in)
                 OR (rule_type_in = ATTR_TYPE  AND sfrrgfe_attr_code_crse = rule_value_in)
                 OR (rule_type_in = STUDENT_TYPE)
                 OR (rule_type_in IS NULL AND rule_value_in IS NULL) )
            AND sfrrgfe_type = a.sfrfaud_rgfe_type
            AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
             AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;



         ---  7.4.0.1 we need to consider the sfkfees audit record for any time there was a flat
         ---   AND ((a.sfrfaud_activity_date < DD_handled_date) OR DD_handled_date IS NULL);
    BEGIN
       ---  DD_handled_date := SFKFEES.f_get_DD_handled_date(pidm_in, term_in);
        p_print_dbms('Find Flat Rule type: ' || rule_type_in || ' value: ' || rule_value_in);
        OPEN prev_flat_rule_c;
        FETCH prev_flat_rule_c    INTO prev_flat_rule_met_ind;
        IF prev_flat_rule_c%NOTFOUND THEN
             prev_flat_rule_met_ind := 'N';
        END IF; --- dd handled date is not null
       CLOSE prev_flat_rule_c;

       p_print_dbms('in f_prev_flat_rule_met, : ' || prev_flat_rule_met_ind || ' dd date: ' || DD_handled_date);
       RETURN (prev_flat_rule_met_ind = 'Y');
        */
    -- 8.2 split into 4 cursors to remove the OR
    --  join to sfrrgfe to ensure rule still exists
    CURSOR prev_flat_rule_student_c IS
      SELECT 'Y'
        FROM sfrrgfe, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_flat_fee_amount > 0
         AND sfrrgfe_term_code = term_in
            -- AND sfrrgfe_type = rule_type_in  7.1
         AND sfrrgfe_type = STUDENT_TYPE
         AND sfrrgfe_type = a.sfrfaud_rgfe_type
         AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;

    CURSOR prev_flat_rule_level_c IS
      SELECT 'Y'
        FROM sfrrgfe, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_flat_fee_amount > 0
         AND sfrrgfe_type = LEVEL_TYPE
         AND sfrrgfe_term_code = term_in
         AND sfrrgfe_levl_code_crse = rule_value_in
         AND sfrrgfe_type = a.sfrfaud_rgfe_type
         AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;

    CURSOR prev_flat_rule_campus_c IS
      SELECT 'Y'
        FROM sfrrgfe, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_flat_fee_amount > 0
         AND sfrrgfe_term_code = term_in
         AND sfrrgfe_type = CAMPUS_TYPE
         AND sfrrgfe_camp_code_crse = rule_value_in
         AND sfrrgfe_type = a.sfrfaud_rgfe_type
         AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;

    CURSOR prev_flat_rule_attr_c IS
      SELECT 'Y'
        FROM sfrrgfe, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_flat_fee_amount > 0
         AND sfrrgfe_term_code = term_in
            -- AND sfrrgfe_type = rule_type_in  7.1
         AND sfrrgfe_type = ATTR_TYPE
         AND sfrrgfe_attr_code_crse = rule_value_in
         AND sfrrgfe_type = a.sfrfaud_rgfe_type
         AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;

    CURSOR prev_flat_rule_none_c IS
      SELECT 'Y'
        FROM sfrrgfe, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_flat_fee_amount > 0
         AND sfrrgfe_term_code = term_in
         AND sfrrgfe_type LIKE NVL(rule_type_in, '%')
         AND sfrrgfe_type = a.sfrfaud_rgfe_type
         AND sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount;

    ---  7.4.0.1 we need to consider the sfkfees audit record for any time there was a flat
    ---   AND ((a.sfrfaud_activity_date < DD_handled_date) OR DD_handled_date IS NULL);
  BEGIN
    p_print_dbms('Find Flat Rule type: ' || rule_type_in || ' value: ' ||
                 rule_value_in);
    CASE rule_type_in
      WHEN STUDENT_TYPE then
        p_print_dbms('Open Student');
        OPEN prev_flat_rule_student_c;
        FETCH prev_flat_rule_student_c
          INTO prev_flat_rule_met_ind;
        IF prev_flat_rule_student_c%NOTFOUND THEN
          prev_flat_rule_met_ind := 'N';
        END IF; --- dd handled date is not null
        CLOSE prev_flat_rule_student_c;
      WHEN CAMPUS_TYPE then
        p_print_dbms('Open camopus ');
        OPEN prev_flat_rule_campus_c;
        FETCH prev_flat_rule_campus_c
          INTO prev_flat_rule_met_ind;
        IF prev_flat_rule_campus_c%NOTFOUND THEN
          prev_flat_rule_met_ind := 'N';
        END IF; --- dd handled date is not null
        CLOSE prev_flat_rule_campus_c;
      WHEN LEVEL_TYPE then
        p_print_dbms('Open level');
        OPEN prev_flat_rule_level_c;
        FETCH prev_flat_rule_level_c
          INTO prev_flat_rule_met_ind;
        IF prev_flat_rule_level_c%NOTFOUND THEN
          prev_flat_rule_met_ind := 'N';
        END IF; --- dd handled date is not null
        CLOSE prev_flat_rule_level_c;
      WHEN ATTR_TYPE then
        p_print_dbms('Open attr');
        OPEN prev_flat_rule_attr_c;
        FETCH prev_flat_rule_attr_c
          INTO prev_flat_rule_met_ind;
        IF prev_flat_rule_attr_c%NOTFOUND THEN
          prev_flat_rule_met_ind := 'N';
        END IF; --- dd handled date is not null
        CLOSE prev_flat_rule_attr_c;
      ELSE
        p_print_dbms('Open none because none');
        OPEN prev_flat_rule_none_c;
        FETCH prev_flat_rule_none_c
          INTO prev_flat_rule_met_ind;
        IF prev_flat_rule_none_c%NOTFOUND THEN
          prev_flat_rule_met_ind := 'N';
        END IF; --- dd handled date is not null
        CLOSE prev_flat_rule_none_c;
    END CASE;
    p_print_dbms('in f_prev_flat_rule_met, : ' || prev_flat_rule_met_ind ||
                 ' dd date: ' || DD_handled_date);
    RETURN(prev_flat_rule_met_ind = 'Y');

  END f_prev_flat_rule_met;

  /* ******************************************************************* */
  /* 7.1 / swapping.                                         3/2005 BAG  */
  /* ******************************************************************* */
  /* Determine if any flat charge rules are defined for a term.          */
  /* For use in registration when inserting rows into SFRREGD, which     */
  /* only needs to occur if flat charge rules are defined for the term.  */
  /* ******************************************************************* */
  FUNCTION f_flat_rule_exists(term_in IN stvterm.stvterm_code%TYPE)
    RETURN BOOLEAN IS
    flat_rule_exists VARCHAR2(1) := 'N';

    CURSOR flat_rule_c IS
      SELECT 'Y'
        FROM DUAL
       WHERE EXISTS (SELECT 'flat'
                FROM sfrrgfe
               WHERE sfrrgfe_term_code = term_in
                 AND sfrrgfe_from_flat_hrs IS NOT NULL
                 AND sfrrgfe_to_flat_hrs IS NOT NULL
                 AND sfrrgfe_flat_fee_amount IS NOT NULL);
  BEGIN
    OPEN flat_rule_c;
    FETCH flat_rule_c
      INTO flat_rule_exists;
    IF flat_rule_c%NOTFOUND THEN
      flat_rule_exists := 'N';
    END IF;
    CLOSE flat_rule_c;
    RETURN(flat_rule_exists = 'Y');
  END f_flat_rule_exists;

  /* ******************************************************************* */
  /* Function to determine if assessment accounting records exist for a  */
  /* detail code and term for the student. Function is called prior to   */
  /* performing penalty processing for RBT.                              */
  /* ******************************************************************* */
  FUNCTION f_tbraccd_exists(pidm_in      IN spriden.spriden_pidm%TYPE,
                            term_in      IN stvterm.stvterm_code%TYPE,
                            detl_code_in IN tbbdetc.tbbdetc_detail_code%TYPE,
                            amount_in    IN tbraccd.tbraccd_amount%TYPE)
    RETURN BOOLEAN IS
    CURSOR tbraccd_c IS
      SELECT NVL(SUM(tbraccd_amount), 0)
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_detail_code = detl_code_in
         AND tbraccd_srce_code <> 'R';

    detl_sum      tbraccd.tbraccd_amount%TYPE := 0;
    charge_exists VARCHAR2(1) := 'N';
  BEGIN
    OPEN tbraccd_c;
    FETCH tbraccd_c
      INTO detl_sum;
    IF tbraccd_c%NOTFOUND THEN
      charge_exists := 'N';
    ELSIF detl_sum >= amount_in THEN
      charge_exists := 'Y';
    ELSE
      charge_exists := 'N';
    END IF;
    CLOSE tbraccd_c;
    RETURN(charge_exists = 'Y');
  END f_tbraccd_exists;

  /* ******************************************************************* */
  /* Function to determine total per credit charge a student is liable   */
  /* for using the liable hours if the DCAT (detail code category) is    */
  /* either TUI or FEE and based on whether a course override is         */
  /* specified in the assessment rule. For all other DCAT codes, use the */
  /* determined bill or waived hours.                                    */
  /*                                                                     */
  /* If crse_waiv_ind is set to 'Y', then the charge can be overridden   */
  /* by a course fee override; use waived hours.                         */
  /* ******************************************************************* */
  FUNCTION f_calc_per_cred_charge(max_charge        IN sfrrgfe.sfrrgfe_max_charge%TYPE,
                                  min_charge        IN sfrrgfe.sfrrgfe_min_charge%TYPE,
                                  dcat_code         IN ttvdcat.ttvdcat_code%TYPE,
                                  crse_waiv_ind     IN sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                  bill_hr           IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_bill_hr_tuit IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_bill_hr_fees IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  waiv_hr           IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_waiv_hr_tuit IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  liab_waiv_hr_fees IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                  per_cred_charge   IN sfrrgfe.sfrrgfe_per_cred_charge%TYPE)
    RETURN NUMBER IS
    chrg_amt NUMBER(8, 2) := 0;
  BEGIN
    p_print_dbms('f calc per cred charge begins');
    IF dcat_code = 'TUI' THEN
      IF crse_waiv_ind = 'Y' THEN
        chrg_amt := liab_waiv_hr_tuit * per_cred_charge;
      ELSE
        chrg_amt := liab_bill_hr_tuit * per_cred_charge;
      END IF;
    ELSIF dcat_code = 'FEE' THEN
      IF crse_waiv_ind = 'Y' THEN
        chrg_amt := liab_waiv_hr_fees * per_cred_charge;
      ELSE
        chrg_amt := liab_bill_hr_fees * per_cred_charge;
      END IF;
    ELSE
      -- all other detail codes
      IF crse_waiv_ind = 'Y' THEN
        chrg_amt := waiv_hr * per_cred_charge;
      ELSE
        chrg_amt := bill_hr * per_cred_charge;
      END IF;
    END IF;
    p_print_dbms('chrg amt ' || chrg_amt);
    RETURN chrg_amt;
  END f_calc_per_cred_charge;

  /* ******************************************************************* */
  /* Function to determine if bill or waive hours are used for the rule. */
  /* ******************************************************************* */
  /* For detail codes having a DCAT code of TUI or FEE:                  */
  /*   If the course waive indicator is set to Y, use the waive hours.   */
  /*   If the course waive indicator is set to N, use the bill hours.    */
  /*                                                                     */
  /* For all other detail codes:                                         */
  /*   If the course waive indicator is set to Y, use the waive hours    */
  /*   from the course registration record (not adjusted for refunds)    */
  /*                                                                     */
  /*   If the course waive indicator is set to N, use the bill hours     */
  /*   from the course registration record (not adjusted for refunds)    */
  /*                                                                     */
  /* The values for the liable hours coming in are:                      */
  /*   liab_bill_hr_tuit = sfrstcr_bill_hr * liable tuit %               */
  /*   liab_bill_hr_fees = sfrstcr_bill_hr * liable fee %                */
  /*   liab_waiv_hr_tuit = sfrstcr_waiv_hr * liable tuit %               */
  /*   liab_waiv_hr_fees = sfrstcr_waiv_hr * liable fee %                */
  /* ******************************************************************* */
  FUNCTION f_calc_rule_hrs(dcat_code         IN ttvdcat.ttvdcat_code%TYPE,
                           crse_waiv_ind     IN sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                           reg_bill_hr       IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                           liab_bill_hr_tuit IN NUMBER,
                           liab_bill_hr_fees IN NUMBER,
                           reg_waiv_hr       IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                           liab_waiv_hr_tuit IN NUMBER,
                           liab_waiv_hr_fees IN NUMBER) RETURN NUMBER IS
    rule_hrs NUMBER(10, 4) := 0;
  BEGIN
    p_print_dbms('f calc rule hrs ' || dcat_code || ' waive: ' ||
                 crse_waiv_ind);
    IF dcat_code = 'TUI' THEN
      IF crse_waiv_ind = 'Y' THEN
        rule_hrs := liab_waiv_hr_tuit;
      ELSE
        rule_hrs := liab_bill_hr_tuit;
      END IF;
    ELSIF dcat_code = 'FEE' THEN
      IF crse_waiv_ind = 'Y' THEN

        rule_hrs := liab_waiv_hr_fees;
      ELSE
        rule_hrs := liab_bill_hr_fees;
      END IF;
    ELSE
      -- all other detl codes; use unadj bill or waive hrs from SFRSTCR
      IF crse_waiv_ind = 'Y' THEN
        rule_hrs := reg_waiv_hr;
      ELSE
        rule_hrs := reg_bill_hr;
      END IF;
    END IF;
    p_print_dbms('rule hrs ' || rule_hrs);
    RETURN rule_hrs;
  END f_calc_rule_hrs;

  /* ******************************************************************* */
  /*  Determine the effective date to use in posting assessment charges. */
  /* ******************************************************************* */
  FUNCTION f_get_accd_eff_date(fa_eff_date_in IN DATE) RETURN DATE IS
    eff_date DATE;
  BEGIN
    /* ****************************************************** */
    /* 103863                                    10/20/05 JC  */
    /* IF tbbctrl_eff_date_ind = 'T' and sfbetrm_ar_ind = 'Y' */
    /*   use lesser of fa_eff_date_in or SYSDATE              */
    /* ELSIF sobterm_eff_date < fa_eff_date_in <= SYSDATE     */
    /*   use fa_eff_date_in                                   */
    /* ELSE                                                   */
    /*   use greatest sobterm_date, fa_eff_date_in, SYSDATE   */
    /* ****************************************************** */
    IF (NVL(sfbetrm_rec.sfbetrm_ar_ind, 'N') = 'Y' AND eff_date_ind = 'T') -- 'T'odays date
     THEN
      IF TRUNC(fa_eff_date_in) < TRUNC(SYSDATE) THEN
        eff_date := fa_eff_date_in;
      ELSE
        eff_date := SYSDATE;
      END IF;
    ELSIF TRUNC(fa_eff_date_in) <= TRUNC(SYSDATE) AND
          TRUNC(fa_eff_date_in) >
          TRUNC(NVL(sobterm_rec.sobterm_fee_assessmnt_eff_date,
                    fa_eff_date_in - 1)) THEN
      eff_date := NVL(fa_eff_date_in, SYSDATE);
    ELSE
      eff_date := GREATEST(NVL(sobterm_rec.sobterm_fee_assessmnt_eff_date,
                               SYSDATE),
                           NVL(fa_eff_date_in, SYSDATE),
                           SYSDATE);
    END IF;
    RETURN eff_date;
  END f_get_accd_eff_date;

  /* ******************************************************************* */
  /* Determine the total registration hours from the last assessment     */
  /* by selecting the SFRFAUD record with an assess code of 'N' for      */
  /* non-dropped hours assessment. The audit record is created as part   */
  /* of current assessment processing and is referred to during the next */
  /* assessment processing.                                              */
  /*                                                                     */
  /* 7.1: If the last assessment handled any DD/'not count in assess'    */
  /* registration activity, the starting hours to retrieve are the total */
  /* hours that the intermediate assessment arrived at.                  */
  /* 7.3:  Defect 99226                                                  */
  /* ******************************************************************* */
  FUNCTION f_get_prev_nondrop_hrs(pidm_in       IN spriden.spriden_pidm%TYPE,
                                  term_in       IN stvterm.stvterm_code%TYPE,
                                  crse_waiv_in  IN sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                  type_in       IN sfrrgfe.sfrrgfe_type%TYPE default null,
                                  rule_value_in IN VARCHAR2 default null)
    RETURN NUMBER IS
    nondrop_hrs     sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0.00;
    nonwaiv_hrs     sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0.00;
    DD_handled_date DATE := NULL;

    CURSOR nondrop_hrs_c IS
      SELECT NVL(sfrfaud_reg_bill_hr, 0), NVL(sfrfaud_reg_waiv_hr, 0)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NULL
         AND sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND sfrfaud_assess_rfnd_penlty_cde = 'N'
            --- new to 7.5 to get the hrs for the rule and value
         AND nvl(sfrfaud_rgfe_type, STUDENT_TYPE) =
             nvl(type_in, STUDENT_TYPE)
         AND decode(type_in,
                    STUDENT_TYPE,
                    '%',
                    nvl(sfrfaud_rgfe_rule_value, '%')) =
             nvl(rule_value_in, '%');

    CURSOR reg_bill_hrs_c IS
      SELECT NVL(sfrfaud_reg_bill_hr, 0), NVL(sfrfaud_reg_waiv_hr, 0)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NULL
         AND sfrfaud_activity_date = DD_handled_date
         AND sfrfaud_assess_rfnd_penlty_cde = 'D'
            --- new to 7.5 to get the hrs for the rule and value
         AND nvl(sfrfaud_rgfe_type, STUDENT_TYPE) =
             nvl(type_in, STUDENT_TYPE)
         AND decode(type_in,
                    STUDENT_TYPE,
                    '%',
                    nvl(sfrfaud_rgfe_rule_value, '%')) =
             nvl(rule_value_in, '%');
  BEGIN
    p_print_dbms('f get prev nondrop hrs, last assessment date: ' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DD-MON-YYYY HH24:MI:SS'));
    IF NOT (LAST_ASSESSMENT_HANDLED_DD) THEN
      p_print_dbms('not DD handled in lst assessment');
      OPEN nondrop_hrs_c;
      FETCH nondrop_hrs_c
        INTO nondrop_hrs, nonwaiv_hrs;
      IF nondrop_hrs_c%NOTFOUND THEN
        nondrop_hrs := 0.00;
        nonwaiv_hrs := 0.00;
      END IF;
      p_print_dbms('non drop hrs from faud with refund pnlty N for last assessment date: ' ||
                   nondrop_hrs);
      CLOSE nondrop_hrs_c;
      /* Defect 99226 */
      IF crse_waiv_in = 'Y' THEN
        nondrop_hrs := nonwaiv_hrs;
      END IF;
    ELSE
      DD_handled_date := ME_SFKFEES.f_get_DD_handled_date(pidm_in, term_in);
      p_print_dbms('DD handled in lst assessment, get bill hrs from faud with rnd pnty of D and for: ' ||
                   to_char(DD_handled_date, 'DD-MON-YYYY HH24:MI:SS'));
      OPEN reg_bill_hrs_c;
      FETCH reg_bill_hrs_c
        INTO nondrop_hrs, nonwaiv_hrs;
      IF reg_bill_hrs_c%NOTFOUND THEN
        nondrop_hrs := 0.00;
        nonwaiv_hrs := 0.00;
      END IF;
      CLOSE reg_bill_hrs_c;
      /* Defect 99226 */
      IF crse_waiv_in = 'Y' THEN
        nondrop_hrs := nonwaiv_hrs;
      END IF;
      p_print_dbms('bill hrs from faud where refund pnty is D: ' ||
                   nondrop_hrs);
    END IF;
    p_print_dbms('at end of f_get_prev_nondrop_hrs : nondrop hrs  ' ||
                 nondrop_hrs);
    RETURN nondrop_hrs;
  END f_get_prev_nondrop_hrs;

  /* ******************************************************************* */
  /* Function to return SFRSTCR_ACTIVITY_DATE.                           */
  /* ******************************************************************* */
  FUNCTION f_get_sfrstcr_date(pidm_in IN spriden.spriden_pidm%TYPE,
                              term_in IN stvterm.stvterm_code%TYPE,
                              crn_in  IN sfrstcr.sfrstcr_crn%TYPE)
    RETURN DATE IS
    CURSOR sfrstcr_c IS
      SELECT sfrstcr_activity_date
        FROM sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND sfrstcr_crn = crn_in;

    act_date DATE;
  BEGIN
    OPEN sfrstcr_c;
    FETCH sfrstcr_c
      INTO act_date;
    IF sfrstcr_c%NOTFOUND THEN
      act_date := NULL;
    END IF;
    CLOSE sfrstcr_c;
    RETURN act_date;
  END f_get_sfrstcr_date;

  /* ******************************************************************* */
  /* Determine if rules defined for term and rule type.                  */
  /* ******************************************************************* */
  FUNCTION f_rule_type_defined(term_in      IN stvterm.stvterm_code%TYPE,
                               rule_type_in IN sfrrgfe.sfrrgfe_type%TYPE)
    RETURN BOOLEAN IS
    type_defined VARCHAR2(1) := 'N';

    CURSOR rule_type_c IS
      SELECT 'Y'
        FROM DUAL
       WHERE EXISTS (SELECT 'Y'
                FROM sfrrgfe
               WHERE sfrrgfe_term_code = term_in
                 AND sfrrgfe_type = rule_type_in);

    type_cc_found VARCHAR(1) := 'N';
    CURSOR rule_cc_type_c IS
      select 'Y'
        from DUAL
       WHERE EXISTS (SELECT 'Y'
                FROM sfrrgfe
               WHERE sfrrgfe_term_code = term_in
                 AND sfrrgfe_type = rule_type_in
                 AND (sfrrgfe_majr_code is not null or
                     sfrrgfe_lfst_code is not null or
                     sfrrgfe_dept_code is not null or
                     sfrrgfe_levl_code is not null or
                     sfrrgfe_coll_code is not null OR
                     sfrrgfe_camp_code is not null OR
                     sfrrgfe_degc_code is not null OR
                     sfrrgfe_program is not null OR
                     sfrrgfe_term_code_admit is not null OR
                     sfrrgfe_rate_code_curric is not null OR
                     sfrrgfe_styp_code_curric is not null));

    type_course_found VARCHAR(1) := 'N';
    CURSOR rule_course_type_c IS
      SELECT 'Y'
        FROM DUAL
       WHERE EXISTS
       (SELECT 'X'
                FROM SFRRGFE
               WHERE sfrrgfe_term_code = term_in
                 AND sfrrgfe_type = rule_type_in
                 AND nvl(SFRRGFE_ASSESS_BY_COURSE_IND, 'N') = 'Y');
  BEGIN

    OPEN rule_type_c;
    FETCH rule_type_c
      INTO type_defined;
    IF rule_type_c%NOTFOUND THEN
      type_defined := 'N';
    END IF;
    CLOSE rule_type_c;

    --- find if there are rules defined with curriculum
    OPEN rule_cc_type_c;
    FETCH rule_cc_type_c
      into type_cc_found;
    if rule_cc_type_c%notfound then
      type_cc_found := 'N';
    end if;
    CLOSE rule_cc_type_c;
    CASE rule_type_in
      WHEN STUDENT_TYPE then
        if type_cc_found = 'Y' then
          gv_rgfe_student_cc_exists := 'Y';
        else
          gv_rgfe_student_cc_exists := 'N';
        end if;
      WHEN LEVEL_TYPE then
        if type_cc_found = 'Y' then
          gv_rgfe_level_cc_exists := 'Y';
        else
          gv_rgfe_level_cc_exists := 'N';
        end if;
      WHEN ATTR_TYPE then
        if type_cc_found = 'Y' then
          gv_rgfe_attr_cc_exists := 'Y';
        else
          gv_rgfe_attr_cc_exists := 'N';
        end if;
      WHEN CAMPUS_TYPE then
        if type_cc_found = 'Y' then
          gv_rgfe_campus_cc_exists := 'Y';
        else
          gv_rgfe_campus_cc_exists := 'N';
        end if;
      ELSE
        if type_cc_found = 'Y' then
          gv_rgfe_student_cc_exists := 'Y';
        else
          gv_rgfe_student_cc_exists := 'N';
        end if;
    end case;
    --- find if there are rules defined by course
    OPEN rule_course_type_c;
    FETCH rule_course_type_c
      into type_course_found;
    if rule_course_type_c%notfound then
      type_course_found := 'N';
    end if;
    CLOSE rule_course_type_c;
    CASE rule_type_in
      WHEN STUDENT_TYPE then
        if type_course_found = 'Y' then
          gv_rgfe_student_course_exists := 'Y';
        else
          gv_rgfe_student_course_exists := 'N';
        end if;
      WHEN LEVEL_TYPE then
        if type_course_found = 'Y' then
          gv_rgfe_level_course_exists := 'Y';
        else
          gv_rgfe_level_course_exists := 'N';
        end if;
      WHEN ATTR_TYPE then
        if type_course_found = 'Y' then
          gv_rgfe_attr_course_exists := 'Y';
        else
          gv_rgfe_attr_course_exists := 'N';
        end if;
      WHEN CAMPUS_TYPE then
        if type_course_found = 'Y' then
          gv_rgfe_campus_course_exists := 'Y';
        else
          gv_rgfe_campus_course_exists := 'N';
        end if;
      ELSE
        if type_course_found = 'Y' then
          gv_rgfe_student_course_exists := 'Y';
        else
          gv_rgfe_student_course_exists := 'N';
        end if;
    end case;

    RETURN(type_defined = 'Y');
  END f_rule_type_defined;

  /* ******************************************************************* */
  /* Determine if enrollment status code indicates withdrawl.            */
  /* ******************************************************************* */
  FUNCTION f_ests_means_wd(ests_code_in IN stvests.stvests_code%TYPE)
    RETURN BOOLEAN IS
    wd_ind VARCHAR2(1) := 'N';

    CURSOR ests_wd_c IS
      SELECT stvests_wd_ind FROM stvests WHERE stvests_code = ests_code_in;
  BEGIN
    p_print_dbms('f ests means wd begins');
    OPEN ests_wd_c;
    FETCH ests_wd_c
      INTO wd_ind;
    IF ests_wd_c%NOTFOUND THEN
      wd_ind := 'N';
    END IF;
    CLOSE ests_wd_c;
    p_print_dbms('wd ind ' || wd_ind);
    RETURN(wd_ind = 'Y');
  END f_ests_means_wd;

  /* ******************************************************************* */
  /* Determine if enrollment status withdrawl already assessed as part   */
  /* the last assessment to prevent reassessing and overrefunding. Also  */
  /* check if current assessment's ESTS date is that same as the previous*/
  /* assessment's ESTS date in case the date was changed to facilitate   */
  /* choosing a different refund period.                                 */
  /*                                                                     */
  /* Assessing for refunding by enrollment status is performed using     */
  /* total charges under accounting and not by liable bill hours.        */
  /* ******************************************************************* */
  FUNCTION f_ests_wd_assessed(pidm_in                   IN spriden.spriden_pidm%TYPE,
                              term_in                   IN stvterm.stvterm_code%TYPE,
                              changed_ests_date_ind_out OUT VARCHAR2)
    RETURN BOOLEAN IS
    ests_wd_assessed_ind VARCHAR2(1) := 'N';
    staging_date         DATE := '';

    CURSOR ests_wd_assessed_c IS
      SELECT 'Y'
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND sfrfaud_ests_code = sfbetrm_rec.sfbetrm_ests_code
         AND TRUNC(sfrfaud_ests_date) =
             TRUNC(sfbetrm_rec.sfbetrm_ests_date);

    -- Determine if different ESTS dates for the same ESTS WD code between
    -- the previous assessment audit and the current SFBETRM record. The
    -- different date in the SFBETRM record could cause a different refund
    -- rule to be selected for the ESTS WD.
    CURSOR changed_ests_date_c IS
      SELECT 'Y'
        FROM sfrfaud p
       WHERE p.sfrfaud_pidm = pidm_in
         AND p.sfrfaud_term_code = term_in
         AND p.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND p.sfrfaud_ests_code = sfbetrm_rec.sfbetrm_ests_code
         AND TRUNC(p.sfrfaud_ests_date) <>
             TRUNC(sfbetrm_rec.sfbetrm_ests_date)
            --         AND p.sfrfaud_note like '%ESTS liability%'; I18N Bug
         AND p.sfrfaud_note like '%' || c_ESTS_liability || '%';

    CURSOR staging_date_c IS
      SELECT MIN(survers_stage_date)
        FROM survers
       WHERE survers_release like '6.0%';
  BEGIN
    p_print_dbms('f ests wd assessed begins');
    /* This covers assessments post-6.0 install. */
    OPEN ests_wd_assessed_c;
    FETCH ests_wd_assessed_c
      INTO ests_wd_assessed_ind;
    IF ests_wd_assessed_c%NOTFOUND THEN
      OPEN changed_ests_date_c;
      FETCH changed_ests_date_c
        INTO changed_ests_date_ind_out;
      IF changed_ests_date_c%NOTFOUND THEN
        changed_ests_date_ind_out := 'N';
        ests_wd_assessed_ind      := 'N';
      ELSE
        ests_wd_assessed_ind := 'Y';
      END IF;
    END IF;
    p_print_dbms('ests wd assessed ind 1 ' || ests_wd_assessed_ind);
    CLOSE ests_wd_assessed_c;

    /* This covers assessments pre-6.0 install, which will only be invoked if */
    /* the ests_wd_assessed_ind is not set to Y during post-6.0 install check.*/

    /* ********************************************************************** */
    /* Defect 89669.                                             09/23/03 BAG */
    /* ********************************************************************** */
    /* If not deemed already assessed, check to see if ESTS WD and assessment */
    /* were done prior to 6.0 release installation. If they were and the last */
    /* assessment done pre-6.0 was after the ESTS WD date, then assume the    */
    /* last assessment done pre-6.0 processed the ESTS refund.                */
    /* ********************************************************************** */
    IF ests_wd_assessed_ind = 'N' THEN
      OPEN staging_date_c;
      FETCH staging_date_c
        INTO staging_date;
      IF staging_date_c%NOTFOUND THEN
        -- Check just in case SURVERS not updated for some reason.
        -- This code should only be available if 6.0 officially installed.
        IF TRUNC(sfbetrm_rec.sfbetrm_ests_date) <=
           TRUNC(sfbetrm_rec.sfbetrm_assessment_date) THEN
          ests_wd_assessed_ind := 'Y'; -- assume ESTS WD assessed
        ELSE
          ests_wd_assessed_ind := 'N';
        END IF;
      ELSE
        IF TRUNC(sfbetrm_rec.sfbetrm_ests_date) <= TRUNC(staging_date) AND
           TRUNC(sfbetrm_rec.sfbetrm_assessment_date) <=
           TRUNC(staging_date) AND
           TRUNC(sfbetrm_rec.sfbetrm_ests_date) <=
           TRUNC(sfbetrm_rec.sfbetrm_assessment_date) THEN
          ests_wd_assessed_ind := 'Y';
        END IF;
      END IF;
      CLOSE staging_date_c;
    END IF;
    p_print_dbms('ests wd assessed ind 2 ' || ests_wd_assessed_ind);
    RETURN(ests_wd_assessed_ind = 'Y');
  END f_ests_wd_assessed;

  /* ******************************************************************* */
  /* Determine if registration status code indicates withdrawl.          */
  /* ******************************************************************* */
  FUNCTION f_rsts_means_wd(rsts_code_in IN stvrsts.stvrsts_code%TYPE)
    RETURN BOOLEAN IS
    wd_ind VARCHAR2(1) := 'N';

    CURSOR rsts_wd_c IS
      SELECT stvrsts_withdraw_ind
        FROM stvrsts
       WHERE stvrsts_code = rsts_code_in;
  BEGIN

    OPEN rsts_wd_c;
    FETCH rsts_wd_c
      INTO wd_ind;
    IF rsts_wd_c%NOTFOUND THEN
      wd_ind := 'N';
    END IF;
    CLOSE rsts_wd_c;
    p_print_dbms('f rsts means wd begins :  wd ind ' || wd_ind);
    RETURN(wd_ind = 'Y');
  END f_rsts_means_wd;

  /* ******************************************************************* */
  /* Determine if SFRFMAX rules defiend for detail code/term.            */
  /* ******************************************************************* */
  /* SFRFMAX is Registration Fees Maximum Table. The min/max are for the */
  /* detail code for the term. The rule is checked for each generated    */
  /* amount prior to posting to accounting.                              */
  /* ******************************************************************* */
  FUNCTION f_sfrfmax_defined(term_in      IN stvterm.stvterm_code%TYPE,
                             detl_code_in IN tbbdetc.tbbdetc_detail_code%TYPE,
                             min_charge   OUT sfrfmax.sfrfmax_min_charge%TYPE,
                             max_charge   OUT sfrfmax.sfrfmax_max_charge%TYPE)
    RETURN BOOLEAN IS
    sfrfmax_defined BOOLEAN := FALSE;

    CURSOR sfrfmax_c IS
      SELECT sfrfmax_min_charge, sfrfmax_max_charge
        FROM sfrfmax
       WHERE sfrfmax_term_code = term_in
         AND sfrfmax_detl_code = detl_code_in;
  BEGIN
    OPEN sfrfmax_c;
    FETCH sfrfmax_c
      INTO min_charge, max_charge;
    IF sfrfmax_c%NOTFOUND THEN
      min_charge := 0;
      max_charge := 0;
    ELSE
      sfrfmax_defined := TRUE;
    END IF;
    CLOSE sfrfmax_c;
    RETURN(sfrfmax_defined = TRUE);
  END f_sfrfmax_defined;

  /* ******************************************************************* */
  /* Determine if RSTS code is include in assessment.                    */
  /* ******************************************************************* */
  FUNCTION f_rsts_include_in_assess(rsts_code_in IN stvrsts.stvrsts_code%TYPE)
    RETURN BOOLEAN IS
    include_in_assess VARCHAR2(1) := 'N';
    CURSOR rsts_include_c IS
      SELECT stvrsts_incl_assess
        FROM stvrsts
       WHERE stvrsts_code = rsts_code_in;
  BEGIN
    p_print_dbms('f rsts include in assess begins');
    OPEN rsts_include_c;
    FETCH rsts_include_c
      INTO include_in_assess;
    IF rsts_include_c%NOTFOUND THEN
      include_in_assess := 'N';
    END IF;
    CLOSE rsts_include_c;
    p_print_dbms('include in assess ' || include_in_assess);
    RETURN(include_in_assess = 'Y');
  END f_rsts_include_in_assess;

  /* ******************************************************************* */
  /* Determine DCAT code for detail code.                                */
  /* ******************************************************************* */
  FUNCTION f_get_tbbdetc_dcat(detl_code_in IN tbbdetc.tbbdetc_detail_code%TYPE)
    RETURN VARCHAR2 IS
    dcat_code ttvdcat.ttvdcat_code%TYPE := '';
    CURSOR dcat_code_c IS
      SELECT tbbdetc_dcat_code
        FROM tbbdetc
       WHERE tbbdetc_detail_code = detl_code_in;
  BEGIN
    OPEN dcat_code_c;
    FETCH dcat_code_c
      INTO dcat_code;
    IF dcat_code_c%NOTFOUND THEN
      dcat_code := '';
    END IF;
    CLOSE dcat_code_c;
    RETURN dcat_code;
  END f_get_tbbdetc_dcat;

  /* ******************************************************************* */
  /*  Ref cursor for non student type  fees used in p_nonstudent fees                                           */
  /* ******************************************************************* */
  function f_course_types(term_in IN stvterm.stvterm_code%TYPE,
                          pidm_in IN spriden.spriden_pidm%TYPE,
                          type_in IN sfrrgfe.sfrrgfe_type%TYPE)
    return sftfees_type_ref is

    rec_type sftfees_type_ref;

  begin
    CASE type_in
      when LEVEL_TYPE then
        OPEN rec_type FOR
          SELECT sftfees_levl_cde_crse
            FROM sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND sftfees_levl_cde_crse IS NOT NULL
           GROUP BY sftfees_levl_cde_crse
           ORDER BY sftfees_levl_cde_crse;

      when CAMPUS_TYPE then
        OPEN rec_type FOR
          SELECT sftfees_camp_cde_crse
            FROM sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND sftfees_camp_cde_crse IS NOT NULL
           GROUP BY sftfees_camp_cde_crse
           ORDER BY sftfees_camp_cde_crse;

      when ATTR_TYPE then
        --      OPEN  rec_type FOR
        --       SELECT ssrattr_attr_code
        --       FROM sfrrgfe, ssrattr, sftfees
        --      WHERE  sfrrgfe_type = ATTR_TYPE
        --        AND sfrrgfe_term_code = sftfees_term_code
        --        AND sfrrgfe_attr_code_crse = ssrattr_attr_code
        --      AND ssrattr_term_code = sftfees_term_cde
        --       AND ssrattr_crn = sftfees_crn
        --      AND sftfees_pidm = pidm_in
        --      AND sftfees_term_cde = term_in
        --   GROUP BY ssrattr_attr_code
        --   ORDER BY ssrattr_attr_code;
        OPEN rec_type FOR
          SELECT ssrattr_attr_code
            FROM ssrattr, sftfees
           WHERE ssrattr_term_code = sftfees_term_cde
             AND ssrattr_crn = sftfees_crn
             AND sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             and exists
           (select 'x'
                    from sfrrgfe
                   where sfrrgfe_type = ATTR_TYPE
                     AND sfrrgfe_term_code = sftfees.sftfees_term_cde
                     AND sfrrgfe_attr_code_crse = ssrattr.ssrattr_attr_code)
           GROUP BY ssrattr_attr_code
           ORDER BY ssrattr_attr_code;

      else
        --- student type
        OPEN rec_type FOR
          SELECT pidm_in FROM dual;
    end case;

    return rec_type;
  end f_course_types;

  function f_course_records(term_in       IN stvterm.stvterm_code%TYPE,
                            pidm_in       IN spriden.spriden_pidm%TYPE,
                            type_in       IN sfrrgfe.sfrrgfe_type%TYPE,
                            type_value_in IN varchar2) return sftfees_ref is
    rec_type sftfees_ref;
  begin
    CASE type_in
      when LEVEL_TYPE then
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 NVL(sftfees_ptrm_cde, 'x'),
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sfrstcr, stvrsts, sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND stvrsts_code = sftfees_rsts_cde
             AND sftfees_levl_cde_crse = type_value_in
             AND sfrstcr_term_code = term_in
             AND sfrstcr_crn = sftfees_crn
             AND sfrstcr_pidm = pidm_in
          --- 7.4.0.1 add sfrstcr to the join and sort by the rsts date
           order by sfrstcr_rsts_date,
                    sfrstcr_activity_date,
                    decode(stvrsts_incl_assess,
                           'N',
                           1,
                           decode(stvrsts_withdraw_ind, 'Y', 2, 3)),
                    sfrstcr_crn;

      when CAMPUS_TYPE then
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 NVL(sftfees_ptrm_cde, 'x'),
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sfrstcr, stvrsts, sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND stvrsts_code = sftfees_rsts_cde
             AND sftfees_camp_cde_crse = type_value_in
             AND sfrstcr_term_code = term_in
             AND sfrstcr_crn = sftfees_crn
             AND sfrstcr_pidm = pidm_in
          --- 7.4.0.1 add sfrstcr to the join and sort by the rsts date
           order by sfrstcr_rsts_date,
                    sfrstcr_activity_date,
                    decode(stvrsts_incl_assess,
                           'N',
                           1,
                           decode(stvrsts_withdraw_ind, 'Y', 2, 3)),
                    sfrstcr_crn;

      when ATTR_TYPE then
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 NVL(sftfees_ptrm_cde, 'x'),
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sfrstcr, stvrsts, ssrattr, sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND ssrattr_term_code = sftfees_term_cde
             AND ssrattr_crn = sftfees_crn
             AND ssrattr_attr_code = type_value_in
             AND stvrsts_code = sftfees_rsts_cde
             AND sfrstcr_term_code = term_in
             AND sfrstcr_crn = sftfees_crn
             AND sfrstcr_pidm = pidm_in
          --- 7.4.0.1 add sfrstcr to the join and sort by the rsts date
           order by sfrstcr_rsts_date,
                    sfrstcr_activity_date,
                    decode(stvrsts_incl_assess,
                           'N',
                           1,
                           decode(stvrsts_withdraw_ind, 'Y', 2, 3)),
                    sfrstcr_crn;

      else
        -- student type
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 NVL(sftfees_ptrm_cde, 'x'),
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sfrstcr, stvrsts, sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND stvrsts_code = sftfees_rsts_cde
             AND sfrstcr_term_code = term_in
             AND sfrstcr_crn = sftfees_crn
             AND sfrstcr_pidm = pidm_in
          --- 7.4.0.1 add sfrstcr to the join and sort by the rsts date
           order by sfrstcr_rsts_date,
                    sfrstcr_activity_date,
                    decode(stvrsts_incl_assess,
                           'N',
                           1,
                           decode(stvrsts_withdraw_ind, 'Y', 2, 3)),
                    sfrstcr_crn;
    end case;
    return rec_type;
  end f_course_records;

  function f_crn_records(term_in       IN stvterm.stvterm_code%TYPE,
                         pidm_in       IN spriden.spriden_pidm%TYPE,
                         type_in       IN sfrrgfe.sfrrgfe_type%TYPE,
                         type_value_in IN varchar2) return sftfees_ref is
    rec_type sftfees_ref;
  begin
    CASE type_in
      when LEVEL_TYPE then
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 SFTFEES_PTRM_CDE,
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND sftfees_levl_cde_crse = type_value_in;

      when CAMPUS_TYPE then
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 SFTFEES_PTRM_CDE,
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND sftfees_camp_cde_crse = type_value_in;

      when ATTR_TYPE then
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 SFTFEES_PTRM_CDE,
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM ssrattr, sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND ssrattr_term_code = sftfees_term_cde
             AND ssrattr_crn = sftfees_crn
             AND ssrattr_attr_code = type_value_in;

      else
        --- student type
        OPEN rec_type FOR
          SELECT SFTFEES_PIDM,
                 SFTFEES_TERM_CDE,
                 SFTFEES_CRN,
                 SFTFEES_LEVL_CDE_CRSE,
                 SFTFEES_CAMP_CDE_CRSE,
                 SFTFEES_PTRM_CDE,
                 SFTFEES_ADD_DATE,
                 SFTFEES_RSTS_CDE,
                 SFTFEES_RSTS_DATE,
                 SFTFEES_ESTS_CDE,
                 SFTFEES_ESTS_DATE,
                 SFTFEES_REFUND_SOURCE_TABLE,
                 SFTFEES_GMOD_CDE,
                 SFTFEES_CRSE_START_DATE,
                 SFTFEES_CRSE_END_DATE,
                 SFTFEES_REG_BILL_HR,
                 SFTFEES_BILL_HR_TUIT,
                 SFTFEES_BILL_HR_FEES,
                 SFTFEES_REG_WAIV_HR,
                 SFTFEES_WAIV_HR_TUIT,
                 SFTFEES_WAIV_HR_FEES,
                 SFTFEES_TUIT_LIAB_PERCENTAGE,
                 SFTFEES_FEES_LIAB_PERCENTAGE,
                 SFTFEES_INSM_CDE,
                 SFTFEES_SCHD_CDE
            FROM sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in;
    end case;
    return rec_type;
  end f_crn_records;

  --
  -- PROCEDURES
  --

  /* ******************************************************************* */
  /*  Main procedure for Fee Assessment                                  */
  /* ******************************************************************* */
  /*  If either the SOBTERM, SFBETRM or SGBSTDN records are not found,   */
  /*  set status code and return. If update occurred in assessment,      */
  /*  set the return status for conditional printing by the caller.      */
  /*  Values:                                                            */
  /*   1 = no SOBETRM record                                             */
  /*   2 = no SFBETRM record                                             */
  /*   3 = no SGBSTDN record                                             */
  /*   4 = update occurred                                               */
  /*   5 = no SFRAREG record for open learning section                   */
  /*                                                                     */
  /*  Return the value of the saved date and time as a string for use    */
  /*  in reporting by the caller.                                        */
  /* ******************************************************************* */
  PROCEDURE p_processfeeassessment(term_in               IN stvterm.stvterm_code%TYPE,
                                   pidm_in               IN spriden.spriden_pidm%TYPE,
                                   fa_eff_date_in        IN DATE,
                                   rbt_rfnd_date_in      IN DATE,
                                   rule_entry_type_in    IN sfrrgfe.sfrrgfe_entry_type%TYPE,
                                   create_accd_ind_in    IN VARCHAR2,
                                   source_pgm_in         IN VARCHAR2,
                                   commit_ind_in         IN VARCHAR2,
                                   save_act_date_out     OUT VARCHAR2,
                                   ignore_sfrfmax_ind_in IN VARCHAR2,
                                   return_status_in_out  IN OUT NUMBER) IS

    clas_desc stvclas.stvclas_desc%TYPE := ''; -- 103895

    -- intermediate assessment vars
    next_seqno            sfrfaud.sfrfaud_seqno%TYPE := 0;
    seconds               NUMBER;
    intermediate_date_str VARCHAR2(20) := '';
    minutes               NUMBER;
    hours                 NUMBER;

    -- swapping vars
    phrs   sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    pstart DATE := NULL;
    pend   DATE := NULL;

    --   8.0.1  1-CZLGN copy sfbetrm cursor to lock if user is committing transactions
    CURSOR sfbetrm_c IS
      SELECT *
        FROM sfbetrm
       WHERE sfbetrm_term_code = term_in
         AND sfbetrm_pidm = pidm_in
         for update;

    CURSOR sfbetrm_noupdate_c IS
      SELECT *
        FROM sfbetrm
       WHERE sfbetrm_term_code = term_in
         AND sfbetrm_pidm = pidm_in;

    CURSOR sobterm_c IS
      SELECT * FROM sobterm WHERE sobterm_term_code = term_in;

    CURSOR sgbstdn_c IS
      SELECT *
        FROM sgbstdn
       WHERE sgbstdn_pidm = pidm_in
         AND sgbstdn_term_code_eff =
             (SELECT max(x.sgbstdn_term_code_eff)
                FROM sgbstdn x
               WHERE x.sgbstdn_pidm = pidm_in
                 AND x.sgbstdn_term_code_eff <= term_in);

    CURSOR tbbctrl_c IS
      SELECT tbbctrl_effective_date_ind FROM tbbctrl;

    /* Defect 99507 */
    --- 8.0.1 remove sfbetrm from the select, we only need it for the assessment date and we already selected that
    -- sfbetrm_rec.sfbetrm_assessment_date
    --- cursor to find if person has withdraw and re since last assessment with the same crn
    --  do an intermediate if that is the case
    --- 8.2 performance improvements
    CURSOR sfrstca_c(pidm_in     spriden.spriden_pidm%type,
                     term_in     stvterm.stvterm_code%type,
                     assess_date date) IS
      SELECT (SELECT 'Y'
                FROM stvrsts z, sfrstca x
               WHERE z.stvrsts_code = x.sfrstca_rsts_code
                 AND z.stvrsts_withdraw_ind = 'N'
                 AND x.sfrstca_activity_date > assess_date
                 AND x.sfrstca_crn = a.sfrstca_crn
                 AND x.sfrstca_source_cde = 'BASE'
                 AND x.sfrstca_pidm = a.sfrstca_pidm
                 AND x.sfrstca_term_code = a.sfrstca_term_code
                 AND rownum < 2)
        FROM stvrsts c, sfrstca a
       WHERE c.stvrsts_withdraw_ind = 'Y'
         AND a.sfrstca_activity_date > assess_date
         AND a.sfrstca_source_cde = 'BASE'
         AND c.stvrsts_code = a.sfrstca_rsts_code
         AND a.sfrstca_pidm = pidm_in
         AND a.sfrstca_term_code = term_in;

    sovlcur_rec      sovlcur%rowtype;
    sovlfos_rec      sovlfos%rowtype;
    sovlcur_order    pls_integer;
    sovlfos_order    pls_integer;
    lv_primary_level stvlevl.stvlevl_code%TYPE := '';
    save_lfst        gtvlfst.gtvlfst_code%type;
    cursor learner_cc is
      select *
        from sovlcur
       where sovlcur_pidm = pidm_in
         and sovlcur_lmod_code = sb_curriculum_str.f_learner
         and sovlcur_current_ind = 'Y'
         and sovlcur_active_ind = 'Y'
       order by sovlcur_priority_no;
    cursor lfos_c is
      select *
        from sovlfos
       where sovlfos_pidm = pidm_in
         and sovlfos_lcur_seqno = sovlcur_rec.sovlcur_seqno
         and sovlfos_current_ind = 'Y'
         and sovlfos_active_ind = 'Y'
       order by decode(sovlfos_lfst_code,
                       sb_fieldofstudy_str.f_major,
                       1,
                       sb_fieldofstudy_str.f_minor,
                       2,
                       sb_fieldofstudy_str.f_concentration,
                       3,
                       4),
                sovlfos_priority_no;

    enr_ref sb_enrollment.enrollment_ref;
    enr_rec sb_enrollment.enrollment_rec;

  BEGIN
    source_pgm := source_pgm_in;

    p_init_global_vars;
    OPEN tbbctrl_c;
    FETCH tbbctrl_c
      INTO eff_date_ind;
    CLOSE tbbctrl_c;

    max_flat_to_hours := TO_NUMBER(NVL(gokeacc.F_GetGtvsdaxExtCode('FLATTOMAX',
                                                                   'FEERULES'),
                                       999),
                                   '9999');
    p_print_dbms('max to flat hours: ' || max_flat_to_hours);

    p_print_dbms('p processfeeassessment begins');
    /* ********************************************************* */
    /* 6/2004 BAG: Banner 7.0 TBRACCD API implementation.        */
    /* ********************************************************* */
    /* Set System Transaction Context. Call with stateless = 'N' */
    /* since multiple rows may be inserted for each pidm, and    */
    /* released at the end of the procedure.                     */
    /* ********************************************************* */
    gb_common.p_set_context('TB_RECEIVABLE', 'SYSTEM_TRAN', 'TRUE', 'N');

    /* **************************************************** */
    /* Save data needed throughout processing as constants: */
    /*   rule entry type, session id, class code,           */
    /*   create TBRACCD record indicator,                   */
    /*   value for TBRACCD_ORIG_CHG_IND,                    */
    /*   SOBTERM, SFBETRM and SGBSTDN record data           */
    /*                                                      */
    /* If either the SOBTERM, SFBETRM, SGBSTDN or SFRAREG   */
    /* records are not found, set status code and return.   */
    /* Values:                                              */
    /*  0 = success; all needed records present             */
    /*  1 = no SOBETRM record                               */
    /*  2 = no SFBETRM record                               */
    /*  3 = no SGBSTDN record                               */
    /*  5 = no SFRAREG record for open learning section     */
    /* **************************************************** */
    rule_entry_type := rule_entry_type_in;
    create_accd_ind := create_accd_ind_in;

    SELECT SYS_CONTEXT('USERENV', 'SESSIONID')
      INTO save_sessionid
      FROM DUAL;

    save_activity_date := SYSDATE;

    SELECT TO_CHAR(save_activity_date,
                   '' || G$_DATE.GET_NLS_DATE_FORMAT || ' HH24:MI:SS')
      INTO save_act_date_out
      FROM DUAL;

    -- set global for assessment source
    source_pgm := source_pgm_in;
    p_print_dbms('at start of pgm, source pgm ' || source_pgm ||
                 ' save act date: ' ||
                 to_char(save_activity_date, 'DD-MON-YYYY HH24:Mi:SS'));

    /* ****************************************************************** */
    /* 7.1                                                    02/2005 BAG */
    /* Construct the date/time for the intermediate assessment where      */
    /* the DD/'not count in assessment' registration activity is handled. */
    /* Make the date/time 1 second less than date/time of true assessment.*/
    /* ****************************************************************** */
    p_print_dbms('intermediate assessment date: ' ||
                 to_char(intermediate_date, 'DD-MON-YYYY HH24:MI:SS') ||
                 ' save acct date out: ' || save_act_date_out);

    IF source_pgm = 'SFKFEES' THEN
      --- intermediate date for DD is 1 sec earlier
      intermediate_date := save_activity_date - (1 / 86400);
    END IF;
    p_print_dbms('intermediate assessment: ' ||
                 to_char(intermediate_date, 'DD-MON-YYYY HH24:MI:SS'));

    OPEN sobterm_c;
    FETCH sobterm_c
      INTO sobterm_rec;
    IF sobterm_c%NOTFOUND THEN
      return_status_in_out := 1;
    END IF;
    CLOSE sobterm_c;
    IF return_status_in_out <> 0 THEN
      RETURN;
    END IF;

    --- 8.0.1 lock sfbetrm if committing, to prevent double assessments 1-CZLGN
    if commit_ind_in = 'Y' then
      OPEN sfbetrm_c;
      FETCH sfbetrm_c
        INTO sfbetrm_rec;
      IF sfbetrm_c%NOTFOUND THEN
        p_print_dbms('fetched sfbetrm not found, last assessment date: ' ||
                     to_char(sfbetrm_rec.sfbetrm_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS'));

        return_status_in_out := 2;

      else
        p_print_dbms('fetched sfbetrm found, last assessment date: ' ||
                     to_char(sfbetrm_rec.sfbetrm_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS') || ' pgm source ' ||
                     source_pgm_in);

      END IF;
      CLOSE sfbetrm_c;
      iF return_status_in_out <> 0 THEN
        RETURN;
      END IF;

    else
      OPEN sfbetrm_noupdate_c;
      FETCH sfbetrm_noupdate_c
        INTO sfbetrm_rec;
      IF sfbetrm_noupdate_c%NOTFOUND THEN
        p_print_dbms('fetched sfbetrm not found, last assessment date: ' ||
                     to_char(sfbetrm_rec.sfbetrm_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS'));

        return_status_in_out := 2;

      else
        p_print_dbms('fetched sfbetrm found, last assessment date: ' ||
                     to_char(sfbetrm_rec.sfbetrm_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS') || ' pgm source ' ||
                     source_pgm_in);

      END IF;
      CLOSE sfbetrm_noupdate_c;
      iF return_status_in_out <> 0 THEN
        RETURN;
      END IF;

    end if;
    p_print_dbms('after fetched sfbetrm, last assessment date: ' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DD-MON-YYYY HH24:MI:SS') || ' pgm source ' ||
                 source_pgm_in);

    OPEN sgbstdn_c;
    FETCH sgbstdn_c
      INTO sgbstdn_rec;
    IF sgbstdn_c%NOTFOUND THEN
      return_status_in_out := 3;
    END IF;
    CLOSE sgbstdn_c;
    IF return_status_in_out <> 0 THEN
      RETURN;
    END IF;

    /* **************************************************************** */
    /* Defect 102087.                                        3/2005 BAG */
    /* **************************************************************** */
    /* Specifically save the last assessment date in case the IA causes */
    /* it to be updated. The IA will be built if a DD/NCIA RSTS code    */
    /* has been issued since the last assessment.                       */
    /* IA = Intermediate Assessment built to process DD/NCIA codes      */
    /* **************************************************************** */

    last_assessment_date := sfbetrm_rec.sfbetrm_assessment_date;
    p_print_dbms('Last assessment: ' ||
                 to_char(last_assessment_date, 'DD-MON-YYYY HH24:MI:SS') ||
                 ' source pgm: ' || source_pgm_in);
    /* **************************************************************** */
    /* Changes for 7.1.                                     02/2005 BAG */
    /* **************************************************************** */
    /* Determine if DD/'not count in assessment' registration activity  */
    /* has occurred since the last assessment. If any flat charge rule  */
    /* qualification has occurred and DD/'not count in assessment'      */
    /* activity has also occured since the last assessment, assessment  */
    /* processing needs to 'start over' without including the RSTS      */
    /* codes that have STVRSTS_INCLUD_ASSESS set to N.                  */
    /* **************************************************************** */
    IF NOT (DD_SINCE_LAST_ASSESSMENT) THEN
      p_print_dbms('NOT dd since last assessment');
      LAST_ASSESSMENT_HANDLED_DD := FALSE;
      -- check for any prior flat rule qualification before checking for DD
      IF (f_prev_flat_rule_met(pidm_in, term_in, NULL, NULL)) THEN
        p_print_dbms('f prev flat rule MET');
        DD_SINCE_LAST_ASSESSMENT := ME_SFKFEES.f_DD_since_last_assessment(pidm_in,
                                                                          term_in);
        /* ************************************************************ */
        /* If DD/'not count in assessment' registration activity has    */
        /* happened since the last assessment and previous flat rule    */
        /* qualification of any kind occured, recursively call          */
        /* SFKFEES.p_processfeeassessment to build a series of          */
        /* intermediate assessment audit records to capture the         */
        /* would be the student's liability had the courses with        */
        /* statuses where STVRSTS_INCLUD_ASSESS = N are not considered. */
        /* The date/time of the intermediate assessment audit will be   */
        /* one second less than the date/time of the true assessment.   */
        /* ************************************************************ */
        IF (DD_SINCE_LAST_ASSESSMENT) THEN
          p_print_dbms('dd since last assessment 1a');
          dd_assess_rfnd_cde := 'D'; -- preset for audit row
          --- 1-2O637G  set the flat to true so intermediate is not done twice
          DO_INTERMEDIATE       := TRUE;
          last_dd_activity_date := f_last_dd_activity_date(pidm_in, term_in);
          -- 1-2O637G save the last assessment date and get activity date on the DD
          prev_assessment_date := last_assessment_date;
          p_print_dbms('-------Start intermediate 1:  save_act_date_out' ||
                       save_act_date_out || ' prev assessment: ' ||
                       to_char(prev_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS'));
          ME_SFKFEES.p_processfeeassessment(term_in,
                                            pidm_in,
                                            NULL, -- fa_eff_date_in
                                            NULL, -- rbt_rfnd_date_in
                                            rule_entry_type,
                                            'N', -- don't create TBRACCD rows
                                            'SFKFEES', -- source_pgm_in
                                            commit_ind_in,
                                            save_act_date_out,
                                            'N', -- ignore_sfrfmax_ind_in
                                            return_status_in_out);
          p_print_dbms('--------return from intermediate assessment 1 , pgm source is now : ' ||
                       source_pgm_in);
          LAST_ASSESSMENT_HANDLED_DD := TRUE;
          intermediate_date          := NULL;
          create_accd_ind            := create_accd_ind_in;
          source_pgm                 := source_pgm_in;
          /* *************************************************************** */
          /* If assessment is being run in audit mode, the intermediate      */
          /* assessment registration rows need to be cleared out of SFTFEES  */
          /* since a COMMIT isn't being issued (which would truncate it).    */
          /* Otherwise, the CRNs will be processed again in error when the   */
          /* true assessment is run next.                                    */
          /* *************************************************************** */
          IF commit_ind_in = 'N' THEN
            DELETE FROM SFTFEES;
          END IF;
          p_print_dbms('End intermediate 1');

        END IF;
      END IF;
    END IF;

    IF (DO_INTERMEDIATE) then
      p_print_dbms('**intermediate flag is true');
    else
      p_print_dbms('**intermediate flag is false');
    end if;

    -- Defect 99507 p_process
    -- checks if intermediate assessment is needed as far as Defect 99507 is concerned.
    IF NOT (DO_INTERMEDIATE) THEN
      --LAST_ASSESSMENT_HANDLED_DD := FALSE; Defect 1-E716D
      p_print_dbms('in assessment2  do_intermediate is false: ');
      OPEN sfrstca_c(pidm_in     => pidm_in,
                     term_in     => term_in,
                     assess_date => NVL(sfbetrm_rec.sfbetrm_assessment_date,
                                        TO_DATE(G$_DATE.NORMALISE_GREG_DATE('01-01-1900',
                                                                            'DD-MM-YYYY'),
                                                G$_DATE.GET_NLS_DATE_FORMAT)));

      FETCH sfrstca_c
        INTO do_intermediate_ind;
      IF nvl(do_intermediate_ind, 'N') = 'Y' and sfrstca_c%found THEN
        DO_INTERMEDIATE := TRUE;
      END IF;
      CLOSE sfrstca_c;
      --- 7.4.0.2 find out if we need to do an intermediate if the person has a flat,  drop and then
      --  a change in variable credits that has not been assessed
      IF NOT (DO_INTERMEDIATE) THEN
        DO_INTERMEDIATE := f_var_credit_intermediate(pidm_in, term_in);
        p_print_dbms('do an intermediate because of variable credit hour change');
      END IF;

      IF (DO_INTERMEDIATE) THEN
        dd_assess_rfnd_cde := 'D'; -- preset for audit row
        p_print_dbms('------Start intermediate 2-----');
        -- 1-2O637G save the last assessment date and get activity date on the DD
        prev_assessment_date  := last_assessment_date;
        last_dd_activity_date := f_last_dd_activity_date(pidm_in, term_in);
        ME_SFKFEES.p_processfeeassessment(term_in,
                                          pidm_in,
                                          NULL, -- fa_eff_date_in
                                          NULL, -- rbt_rfnd_date_in
                                          rule_entry_type,
                                          'N', -- don't create TBRACCD rows
                                          'SFKFEES', -- source_pgm_in
                                          commit_ind_in,
                                          save_act_date_out,
                                          'N', -- ignore_sfrfmax_ind_in
                                          return_status_in_out);
        p_print_dbms('-----return from intermediate assessment 2 , pgm source is now : ' ||
                     source_pgm_in);
        LAST_ASSESSMENT_HANDLED_DD := TRUE;
        --- intermediate_date := NULL;
        create_accd_ind := create_accd_ind_in;
        source_pgm      := source_pgm_in;
        /* *************************************************************** */
        /* If assessment is being run in audit mode, the intermediate      */
        /* assessment registration rows need to be cleared out of SFTFEES  */
        /* since a COMMIT isn't being issued (which would truncate it).    */
        /* Otherwise, the CRNs will be processed again in error when the   */
        /* true assessment is run next.                                    */
        /* *************************************************************** */
        IF commit_ind_in = 'N' THEN
          DELETE FROM SFTFEES;
        END IF;
        p_print_dbms('End intermediate 2');

      END IF;
    END IF;
    -- End of Defect 99507
    IF (LAST_ASSESSMENT_HANDLED_DD) then
      p_print_dbms('**last assessment handled dd flag is true');
    else
      p_print_dbms('**last assessment handled dd is false');
    end if;
    -- 103895: use class calc which supports including Attempted Hours if set in SOBTERM
    -- clas_code := sgkclas.f_class_code(pidm_in, sgbstdn_rec.sgbstdn_levl_code, term_in);

    delete sotfcur;
    sovlcur_order := 0;

    soklcur.p_create_sotvcur(p_pidm      => pidm_in,
                             p_lmod_code => sb_curriculum_str.f_learner,
                             p_term_code => term_in);
    open learner_cc;
    loop
      fetch learner_cc
        into sovlcur_rec;
      exit when learner_cc%notfound;
      sovlcur_order := sovlcur_order + 1;

      if sovlcur_order > 2 then
        sovlcur_order := 2;
      end if;
      if sovlcur_order = 1 then
        save_primary_levl_code := sovlcur_rec.sovlcur_levl_code;
      end if;
      p_print_dbms('Lcur order: ' || sovlcur_order || ' save prim: ' ||
                   save_primary_levl_code || ' lcur levl: ' ||
                   sovlcur_rec.sovlcur_levl_code);
      save_lfst     := '';
      sovlfos_order := 0;
      open lfos_c;
      loop
        fetch lfos_c
          into sovlfos_rec;
        exit when lfos_c%notfound;
        if save_lfst is null or save_lfst <> sovlfos_rec.sovlfos_lfst_code then
          sovlfos_order := 1;
        else
          sovlfos_order := 2;
        end if;
        save_lfst := sovlfos_rec.sovlfos_lfst_code;

        insert into sotfcur
          (sotfcur_pidm,
           sotfcur_seqno,
           sotfcur_order,
           sotfcur_levl_code,
           sotfcur_coll_code,
           sotfcur_camp_code,
           sotfcur_degc_code,
           sotfcur_program,
           sotfcur_term_code_admit,
           sotfcur_styp_code,
           sotfcur_rate_code,
           sotfcur_majr_code,
           sotfcur_dept_code,
           sotfcur_lfst_code,
           sotfcur_lfst_order)
        values
          (sovlcur_rec.sovlcur_pidm,
           sovlcur_rec.sovlcur_seqno,
           sovlcur_order,
           sovlcur_rec.sovlcur_levl_code,
           sovlcur_rec.sovlcur_coll_code,
           sovlcur_rec.sovlcur_camp_code,
           sovlcur_rec.sovlcur_degc_code,
           sovlcur_rec.sovlcur_program,
           sovlcur_rec.sovlcur_term_code_admit,
           sovlcur_rec.sovlcur_styp_code,
           sovlcur_rec.sovlcur_rate_code,
           sovlfos_rec.sovlfos_majr_code,
           sovlfos_rec.sovlfos_dept_code,
           sovlfos_rec.sovlfos_lfst_code,
           sovlfos_order);
      end loop;
      close lfos_c;
    end loop;
    close learner_cc;

    /*
    SOKLIBS.p_class_calc(pidm_in,
                         save_primary_levl_code,
                         term_in,
                         TRANSLATE(sobterm_rec.sobterm_incl_attmpt_hrs_ind,'Y','A'),
                         save_clas_code,
                         clas_desc); */

    /* Determine correct effective date to use for new assessment. */
    assess_eff_date := f_get_accd_eff_date(fa_eff_date_in);
    p_print_dbms('assess eff date ' || assess_eff_date);

    /* **************************************************************** */
    /* Determine what value to use for TBRACCD_ORIG_CHG_IND for the     */
    /* assessment. The column indicates, as best as possible, that an   */
    /* accounting record is for the first or original assessment.       */
    /*                                                                  */
    /* TBRACCD_ORIG_CHG_IND is set to 'Y' regardless of the cutoff date */
    /* if the assessment transaction(s) are the first transactions for  */
    /* the student.                                                     */
    /* **************************************************************** */
    IF ((sfbetrm_rec.sfbetrm_assessment_date IS NULL) OR
       (TRUNC(assess_eff_date) <= sobterm_rec.sobterm_cutoff_date)) THEN
      orig_chg_ind := 'Y';
    END IF;

    p_print_dbms('orig chg ind in p_processfeeassessment:  ' ||
                 orig_chg_ind);
    /* **************************************************************** */
    /* If refund by total for the term, populate needed global vars.    */
    /* If refund date not entered, refunds/penalties are not posted.    */
    /* **************************************************************** */
    IF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' THEN
      IF rbt_rfnd_date_in IS NOT NULL THEN
        RBT_RFND_DATE_SPECIFIED := TRUE;
        rbt_rfnd_date           := TRUNC(rbt_rfnd_date_in);
        p_set_rbt_variables(term_in);
        /* 89995                                        10/03 BAG */
        /* Validate effective date to use for RBT refund/penalty. */
        rbt_rfnd_eff_date := f_get_accd_eff_date(rbt_rfnd_date);
      END IF;
    END IF;

    p_studentcourses(pidm_in, term_in, source_pgm_in, return_status_in_out);

    /* **************************************************************** */
    /* If swapping is enabled for term and refund by total is not in    */
    /* effect for the term, determine what courses qualify for swapping */
    /* and set the liability to 0% in SFTFEES.                          */
    /* **************************************************************** */
    IF (sobterm_rec.sobterm_assess_swap_ind = 'Y' AND
       NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'N' AND ANY_DROPS) THEN
      p_process_hours_swap(pidm_in, term_in, phrs, pstart, pend);
    END IF;

    /* **************************************************************** */
    /* Go on to do rule assessment only if an ESTS WD was not processed.*/
    /* ESTS WD refunds are performed based on total charges in          */
    /* accounting and not liable bill hours.                            */
    /* **************************************************************** */
    IF NOT (ESTS_WD_PROCESSED) THEN
      p_print_dbms('NOT ests wd processed');
      --- p_studentfees(pidm_in, term_in, source_pgm_in);
      if term_in <> nvl(save_term, '@') OR gv_rgfe_student_read is null then
        IF (f_rule_type_defined(term_in, STUDENT_TYPE)) THEN
          gv_rgfe_student_exists := 'Y';
        else
          gv_rgfe_student_exists := 'N';
        end if;
        gv_rgfe_student_read := 'Y';
      end if;
      p_calcliability(pidm_in, term_in, source_pgm_in, STUDENT_TYPE);

      /* ********************************************************* */
      /* To improve performance, determine if course-related rule  */
      /* types are defined in SFRRGFE for the term and type before */
      /* invoking the course processing using the temporary table. */
      /* ********************************************************* */
      if term_in <> nvl(save_term, '@') OR gv_rgfe_level_read is null then
        IF (f_rule_type_defined(term_in, LEVEL_TYPE)) THEN
          gv_rgfe_level_exists := 'Y';
        else
          gv_rgfe_level_exists := 'N';
        end if;
        gv_rgfe_level_read := 'Y';
      end if;
      if gv_rgfe_level_exists = 'Y' then
        p_print_dbms('Begin to assess for level rules');
        p_calcliability(pidm_in, term_in, source_pgm_in, LEVEL_TYPE);
      END IF;

      if term_in <> nvl(save_term, '@') OR gv_rgfe_campus_read is null then
        IF (f_rule_type_defined(term_in, CAMPUS_TYPE)) THEN
          gv_rgfe_campus_exists := 'Y';
        else
          gv_rgfe_campus_exists := 'N';
        end if;
        gv_rgfe_campus_read := 'Y';
      end if;
      if gv_rgfe_campus_exists = 'Y' then
        p_print_dbms('Begin to assess for campus rules');
        p_calcliability(pidm_in, term_in, source_pgm_in, CAMPUS_TYPE);
      END IF;

      if term_in <> nvl(save_term, '@') OR gv_rgfe_attr_read is null then
        IF (f_rule_type_defined(term_in, ATTR_TYPE)) THEN
          gv_rgfe_attr_exists := 'Y';
        else
          gv_rgfe_attr_exists := 'N';
        end if;
        gv_rgfe_attr_read := 'Y';
      end if;
      if gv_rgfe_attr_exists = 'Y' then
        p_print_dbms('Begin to assess for attr rules');
        p_calcliability(pidm_in, term_in, source_pgm_in, ATTR_TYPE);
      END IF;

      p_print_dbms('Begin to assess for additional fees');
      p_additionalfees(pidm_in, term_in, source_pgm_in);
    END IF;
    save_term := term_in;

    IF (NOT (ESTS_WD_PROCESSED) AND -- Defect 99507
       (NOT (DO_INTERMEDIATE) AND NOT (DD_SINCE_LAST_ASSESSMENT))) THEN
      -- Defect 99507
      /* ******************************************************* */
      /* Perform reversal processing since ESTS WD not in effect.*/
      /* ******************************************************* */

      p_print_dbms('call to p reverse na charges');
      p_reverse_na_charges(pidm_in, term_in, source_pgm_in);
    END IF;

    p_print_dbms('call to p total fees');
    p_totalfees(pidm_in, term_in, source_pgm_in, ignore_sfrfmax_ind_in);

    /* *********************************************************** */
    /* Do not invoke refund by total processing if an ESTS WD has  */
    /* been processed. ESTS refunds can be defined in tandum with  */
    /* refund by course as well as refund by total, but the ESTS   */
    /* refund takes presedence if defined.                         */
    /* *********************************************************** */
    IF NOT (ESTS_WD_PROCESSED) THEN
      /* ********************************************************* */
      /* Separate RBT refund/penalty processing from p_totalfees.  */
      /* ********************************************************* */
      /* If refund by total enabled for the term and a refund date */
      /* specified, process the refund by total refunds and        */
      /* penalties for TUI and FEE DCAT codes.                     */
      /* ********************************************************* */
      IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
         RBT_RFND_DATE_SPECIFIED) THEN
        /* ****************************************************** */
        /* 104667, this condition also needs to check if the same */
        /* CRN that was previously dropped is currently being re- */
        /* REed. If it is being re-REd, then reverse the Penalty. */
        /* ****************************************************** */
        if (ANY_DROPS) then
          p_print_dbms('swapping and refund by total, any drops is true,swapping: ' ||
                       sobterm_rec.sobterm_assess_swap_ind);
        else
          p_print_dbms('swapping and refund by total, drops is false, swapping: ' ||
                       sobterm_rec.sobterm_assess_swap_ind);
        end if;
        IF (sobterm_rec.sobterm_assess_swap_ind = 'Y' AND ANY_DROPS) OR
           (sobterm_rec.sobterm_assess_swap_ind = 'Y' AND
           REVERSE_ZEROTONINE) THEN
          p_process_hours_swap(pidm_in, term_in, phrs, pstart, pend);
        END IF;

        /* ****************************************************** */
        /* 104667, if SWAPPING is OFF, and yet PENALTY REVERSAL is*/
        /* still expected in an RBT, then do the following.       */
        /* ****************************************************** */
        if REVERSE_ZEROTONINE then
          p_print_dbms('reverse zero');
        else
          p_print_dbms('not reverse zero');
        end if;
        if PENALTY_REV_DONE then
          p_print_dbms('PENALTY_REV_DONE is true');
        else
          p_print_dbms('PENALTY_REV_DONE is false');
        end if;

        IF (NOT (PENALTY_REV_DONE) AND
           NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
           REVERSE_ZEROTONINE) THEN
          p_print_dbms('reverse non swap for rbt');
          p_rev_nonswap_RBT(pidm_in, term_in);
        END IF;

        p_print_dbms('before calc rbt refunds');
        p_calc_rbt_refunds(pidm_in,
                           term_in,
                           source_pgm_in,
                           ignore_sfrfmax_ind_in);

        p_print_dbms('phrs: ' || phrs);

        IF SWAP and phrs > 0 THEN
          p_calc_swap_liability(pidm_in,
                                term_in,
                                phrs,
                                TRUNC(pstart),
                                TRUNC(pend),
                                source_pgm_in);
        END IF;
      END IF;

      IF ((NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
         RBT_RULE_DEFINED AND (rbt_clr_tuit_detl_code IS NOT NULL OR
         rbt_clr_fees_detl_code IS NOT NULL) AND NOT (SWAP))) THEN
        p_print_dbms('post rbt penalties');
        p_post_rbt_penalties(pidm_in, term_in, source_pgm_in);
      END IF;

    END IF;

    /* ********************************************************** */
    /* Only set return_status to 'update occurred' if assessment  */
    /* involved a posting or if performing migration processing   */
    /* and not performing mock assessment via SFAREGF or SFRFEES, */
    /* or if building intermediate assessment audi for handling   */
    /* DD/'not count in assessment' registration activity.        */
    /* ********************************************************** */
    IF ((ASSESSMENT_POSTED) OR
       (create_accd_ind_in = 'N' AND
       source_pgm_in NOT IN ('SFAREGF', 'SFRFEES', 'SFKFEES'))) THEN
      return_status_in_out := 4; -- update occurred
    END IF;

    /* ********************************************************** */
    /* Update assessment date if not performing mock assessment.  */
    /*                                                            */
    /* If performing Intermediate Assessment for DD, update       */
    /* sfbetrm_assessment_date with value of intermediate_date.   */
    /* ********************************************************** */
    /* ********************************************************** */
    /* 07/2004 BAG: By-pass edit checks for holds preventing      */
    /*     registration, etc, by setting application context for  */
    /*     enrollment_batch_rules prior to calling sb_enrollment. */
    /* ********************************************************** */

    IF source_pgm_in NOT IN ('SFAREGF', 'SFRFEES') THEN
      gb_common.p_set_context(p_package_name  => 'SB_ENROLLMENT',
                              p_context_name  => 'ENROLLMENT_BATCH_RULES',
                              p_context_value => 'Y');

      gb_common.p_set_context(p_package_name  => 'SB_ENROLLMENT',
                              p_context_name  => 'ENROLLMENT_HOLD_RULES',
                              p_context_value => 'Y');

      IF source_pgm_in = 'SFKFEES' THEN
        p_print_dbms('update the sfbetrm  during intermediate: ' ||
                     to_char(intermediate_date, 'DD-MON-YYYY HH24:MI:SS'));
        sb_enrollment.p_update(p_term_code       => term_in,
                               p_pidm            => pidm_in,
                               p_assessment_date => intermediate_date,
                               p_refund_date     => TRUNC(rbt_rfnd_date_in));
      ELSE
        p_print_dbms('update the sfbetrm regular assessment: ' ||
                     to_char(save_activity_date, 'DD-MON-YYYY HH24:MI:SS'));
        sb_enrollment.p_update(p_term_code       => term_in,
                               p_pidm            => pidm_in,
                               p_assessment_date => save_activity_date,
                               p_refund_date     => TRUNC(rbt_rfnd_date_in));
      END IF;
    END IF;

    /* ********************************************************* */
    /* If performing true assessment, delete collector records   */
    /* from SFRBTCH and SFRREGD.                                 */
    /* ********************************************************* */
    IF create_accd_ind_in = 'Y' THEN
      p_delete_sfrbtch(pidm_in, term_in);
      SFKMODS.p_delete_sfrregd(pidm_in, term_in, NULL);
    END IF;

    /* ********************************************************* */
    /* Commit the changes, if needed.                            */
    /* ********************************************************* */
    IF commit_ind_in = 'Y' THEN
      COMMIT;
    END IF;

    -- 7.1: reset key DD variables
    DD_SINCE_LAST_ASSESSMENT := FALSE;
    DO_INTERMEDIATE          := FALSE; --99507
    p_print_dbms('source at the end of the assessment ' || source_pgm_in);
    IF source_pgm_in <> 'SFKFEES' THEN
      p_print_dbms('reset the last assessment handled in dd');
      LAST_ASSESSMENT_HANDLED_DD := FALSE;
      prev_assessment_date       := null;
      last_dd_activity_date      := null;
    END IF;
    dd_assess_rfnd_cde := '';
    intermediate_date  := NULL;
    gv_print           := 'x';
    variable_act_date  := null;
    save_term          := term_in;
  END p_processfeeassessment;

  /* ** Supporting procedures for fee assessment ** */

  /* ******************************************************************* */
  /* Procedure to get SFRSTCR_ASSESS_ACTIVITY_DATE for use in processing */
  /* a course for flat charge refunding.                                 */
  /* SR 1-24124651 -  to replace sfrstcr_activity_date with add_date.    */
  /* 7.4.0.1 get the first activity date on the new RE so we can determine if it was added before */
  /* the most recnt DD    */
  /* ******************************************************************* */
  PROCEDURE p_get_sfrstcr_data(pidm_in        IN spriden.spriden_pidm%TYPE,
                               term_in        IN stvterm.stvterm_code%TYPE,
                               act_date       IN OUT DATE,
                               add_date       OUT DATE,
                               first_act_date OUT DATE,
                               orig_bill_hrs  OUT sfrstcr.sfrstcr_bill_hr%type) IS
    --- 8.1.1 add the extra activity date to the select
    acty_date date := null;
    CURSOR sfrstcr_c IS
      SELECT NVL(sfrstcr_assess_activity_date, sfrstcr_add_date),
             sfrstcr_add_date,
             sfrstcr_activity_date
        FROM sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND sfrstcr_crn = t_crn;
    -- get the original add date  7.4.0.1  1-2O637G
    CURSOR sfrstca_c IS
      SELECT min(sfrstca_activity_date)
        FROM stvrsts, sfrstca
       WHERE sfrstca_pidm = pidm_in
         AND sfrstca_term_code = term_in
         AND sfrstca_crn = t_crn
         and stvrsts_code = sfrstca_rsts_code
         and nvl(stvrsts_withdraw_ind, 'N') = 'N'
         and nvl(stvrsts_incl_assess, 'Y') = 'Y';
    --- get the var credit hrs frm the last assessment
    CURSOR variable_credits_c is
      select sfrstca_bill_hr
        from stvrsts, sfrstca
       WHERE sfrstca_pidm = pidm_in
         AND sfrstca_term_code = term_in
         AND sfrstca_crn = t_crn
         and stvrsts_code = sfrstca_rsts_code
         and nvl(stvrsts_withdraw_ind, 'N') = 'N'
         and nvl(stvrsts_incl_assess, 'Y') = 'Y'
         and sfrstca_activity_date =
             (select max(m.sfrstca_activity_date)
                from sfrstca m
               where m.sfrstca_pidm = pidm_in
                 and m.sfrstca_term_code = term_in
                 and m.sfrstca_crn = t_crn);

    --  8.1.1 send new activity date if the course was dropped and then readded, we need to
    -- readd the credits
    new_act_date date := null;
    CURSOR readd_dd_c is
      select sfrstcr_activity_date
        FROM stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND sfrstcr_crn = t_crn
         and stvrsts_code = sfrstcr_rsts_code
         and nvl(stvrsts_withdraw_ind, 'N') = 'N'
         and nvl(stvrsts_incl_assess, 'Y') = 'Y'
         and sfrstcr_credit_hr > 0
         and sfrstcr_activity_date > sfbetrm_rec.sfbetrm_assessment_date
         and exists
       (select 'x'
                from stvrsts mm, sfrstca m
               where m.sfrstca_pidm = pidm_in
                 and m.sfrstca_term_code = term_in
                 and m.sfrstca_crn = t_crn
                 and m.sfrstca_activity_date < sfrstcr.sfrstcr_activity_date
                 and m.sfrstca_credit_hr = 0
                 and nvl(mm.stvrsts_withdraw_ind, 'N') = 'N'
                 and nvl(mm.stvrsts_incl_assess, 'Y') = 'N'
                 and m.sfrstca_activity_date =
                     (select max(mmm.sfrstca_activity_date)
                        from sfrstca mmm
                       where mmm.sfrstca_pidm = pidm_in
                         and mmm.sfrstca_term_code = term_in
                         and mmm.sfrstca_crn = t_crn
                         and mmm.sfrstca_activity_date <
                             sfbetrm_rec.sfbetrm_assessment_date));

  BEGIN
    p_print_dbms('p get sfrstcr data begins: ' || t_crn);
    OPEN sfrstcr_c;
    FETCH sfrstcr_c
      INTO act_date, add_date, acty_date;
    p_print_dbms('act date on sfrstcr: ' ||
                 to_char(acty_date, 'DD-MON-YYYY HH24:I:SS') ||
                 ' last assmt: ' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DD-MON-YYYY HH24:I:SS'));
    IF sfrstcr_c%NOTFOUND THEN
      act_date := NULL;
    END IF;
    CLOSE sfrstcr_c;
    OPEN sfrstca_c;
    FETCH sfrstca_c
      INTO first_act_date;
    IF sfrstca_c%NOTFOUND THEN
      first_act_date := NULL;
    END IF;
    CLOSE sfrstca_c;
    p_print_dbms('prev assessment: ' || prev_assessment_date);
    if prev_assessment_date is not null then
      open variable_credits_c;
      fetch variable_credits_c
        into orig_bill_hrs;
      close variable_credits_c;
    end if;

    ---  8.1.1  for DD and readds, treat as new add
    open readd_dd_c;
    fetch readd_dd_c
      into new_act_date;
    p_print_dbms('new act date: ' || new_act_date);
    if readd_dd_c%found then
      p_print_dbms('change the new add and act date to : ' ||
                   to_char(new_act_date, 'DD-MON-YYYY HH24:I:SS') ||
                   '  from: ' ||
                   to_char(act_date, 'DD-MON-YYYY HH24:I:SS'));
      add_date := new_act_date;
      act_date := new_act_date;
    end if;
    close readd_dd_c;

    p_print_dbms('act date ' || to_char(act_date, 'DD-MON-YYYY HH24:I:SS') ||
                 ' Add Date: ' || add_date || ' first act date: ' ||
                 to_char(first_act_date, 'DD-MON-YYYY HH24:I:SS') ||
                 ' prev ass: ' ||
                 to_char(prev_assessment_date, 'DD-MON-YYYY HH24:I:SS') ||
                 ' bill hrs: ' || orig_bill_hrs);

  END p_get_sfrstcr_data;
  /* ******************************************************************* */
  /* Procedure to get SUM of all bill hours for TUIT and FEES            */
  /* Defect 102188                                                       */
  /* ******************************************************************* */
  PROCEDURE p_get_ndrop_hrs(pidm_in                   IN spriden.spriden_pidm%TYPE,
                            term_in                   IN stvterm.stvterm_code%TYPE,
                            sum_ndrop_bill_hr_tuit_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                            sum_ndrop_bill_hr_fees_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                            sum_ndrop_waiv_hr_tuit_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                            sum_ndrop_waiv_hr_fees_io IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE)

   IS

    CURSOR ndrop_bill_hr_tuit IS
      SELECT NVL(SUM(sftfees_bill_hr_tuit), 0)
        FROM sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND sftfees_tuit_liab_percentage = 1
         AND sftfees_rsts_cde IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_withdraw_ind = 'Y');

    CURSOR ndrop_bill_hr_fees IS
      SELECT NVL(SUM(sftfees_bill_hr_fees), 0)
        FROM sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND sftfees_fees_liab_percentage = 1
         AND sftfees_rsts_cde IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_withdraw_ind = 'Y');
    CURSOR ndrop_waiv_hr_tuit IS
      SELECT NVL(SUM(sftfees_waiv_hr_tuit), 0)
        FROM sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND sftfees_tuit_liab_percentage = 1
         AND sftfees_rsts_cde IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_withdraw_ind = 'Y');

    CURSOR ndrop_waiv_hr_fees IS
      SELECT NVL(SUM(sftfees_waiv_hr_fees), 0)
        FROM sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND sftfees_fees_liab_percentage = 1
         AND sftfees_rsts_cde IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_withdraw_ind = 'Y');

  BEGIN

    p_print_dbms('p get nondrop hours begins');
    OPEN ndrop_bill_hr_tuit;
    FETCH ndrop_bill_hr_tuit
      INTO sum_ndrop_bill_hr_tuit_io;
    IF ndrop_bill_hr_tuit%NOTFOUND THEN
      sum_ndrop_bill_hr_tuit_io := 0;
    END IF;
    CLOSE ndrop_bill_hr_tuit;

    OPEN ndrop_bill_hr_fees;
    FETCH ndrop_bill_hr_fees
      INTO sum_ndrop_bill_hr_fees_io;
    IF ndrop_bill_hr_fees%NOTFOUND THEN
      sum_ndrop_bill_hr_fees_io := 0;
    END IF;
    CLOSE ndrop_bill_hr_fees;

    OPEN ndrop_waiv_hr_tuit;
    FETCH ndrop_waiv_hr_tuit
      INTO sum_ndrop_waiv_hr_tuit_io;
    IF ndrop_waiv_hr_tuit%NOTFOUND THEN
      sum_ndrop_waiv_hr_tuit_io := 0;
    END IF;
    CLOSE ndrop_waiv_hr_tuit;

    OPEN ndrop_waiv_hr_fees;
    FETCH ndrop_waiv_hr_fees
      INTO sum_ndrop_waiv_hr_fees_io;
    IF ndrop_waiv_hr_fees%NOTFOUND THEN
      sum_ndrop_waiv_hr_fees_io := 0;
    END IF;
    CLOSE ndrop_waiv_hr_fees;

    p_print_dbms('sum ndrop bill hr tuit io ' || sum_ndrop_bill_hr_tuit_io);
    p_print_dbms('sum ndrop bill hr fees io ' || sum_ndrop_bill_hr_fees_io);
    p_print_dbms('sum ndrop waiv hr tuit io ' || sum_ndrop_waiv_hr_tuit_io);
    p_print_dbms('sum ndrop waiv hr fees io ' || sum_ndrop_waiv_hr_fees_io);

  END p_get_ndrop_hrs;

  /* ******************************************************************* */
  /* Procedure to determine the SFRRFND refund data for refund by total  */
  /* processing. Since it's assummed that the site will be current with  */
  /* assessment, all dropped courses during this assessment should       */
  /* qualify for the same refund period defined in SFRRFND. Use the date */
  /* specified by the caller for the refund by total refund date to      */
  /* determine the refund. If found, set the global refund by total      */
  /* variables for use in the assessment refund/penalty processing.      */
  /*                                                                     */
  /* The refund definition is only used to determine the penalty percent */
  /* as the penalty percent is the compliment to the refund percent. For */
  /* refund by total, there are no course refunds. The refund is the     */
  /* difference of the prev assessment charges and the current assessment*/
  /* charges, which are determined based only on the hours for the       */
  /* courses that the student is registered in.                          */
  /* ******************************************************************* */
  PROCEDURE p_set_rbt_variables(term_in IN stvterm.stvterm_code%TYPE) IS
    CURSOR sfrrfnd_c IS
      SELECT sfrrfnd_tui_percent,
             sfrrfnd_fee_percent,
             sfrrfnd_srce_code,
             sfrrfnd_tui_code,
             sfrrfnd_fee_code
        FROM sfrrfnd
       WHERE sfrrfnd_term_code = term_in
         AND rbt_rfnd_date BETWEEN sfrrfnd_beg_date AND sfrrfnd_end_date;

    rbt_fees_refund sfrrfnd.sfrrfnd_fee_percent%TYPE := 0;
    rbt_tuit_refund sfrrfnd.sfrrfnd_tui_percent%TYPE := 0;
  BEGIN
    p_print_dbms('p set rbt begins 1');
    p_print_dbms('rbt rfnd date ' || rbt_rfnd_date);
    OPEN sfrrfnd_c;
    FETCH sfrrfnd_c
      INTO rbt_tuit_refund,
           rbt_fees_refund,
           rbt_srce_code,
           rbt_clr_tuit_detl_code,
           rbt_clr_fees_detl_code;

    IF sfrrfnd_c%FOUND THEN
      RBT_RULE_DEFINED         := TRUE;
      rbt_tuit_penalty_percent := (100 - rbt_tuit_refund) / 100;
      rbt_fees_penalty_percent := (100 - rbt_fees_refund) / 100;
      p_print_dbms('p_set_rbt cursor found 1');
      p_print_dbms('rbt fees refund ' || rbt_fees_refund);
      p_print_dbms('rbt tuit refund ' || rbt_tuit_refund);
    ELSE
      p_print_dbms('p_set_rbt cursor notfound 3');
      GOTO exit_cursor;
    END IF;
    p_print_dbms('p_set_rbt cursor found 2');
    <<exit_cursor>>
    p_print_dbms('p_set_rbt cursor exit 1');
    p_print_dbms('rbt fees refund ' || rbt_fees_refund);
    p_print_dbms('rbt tuit refund ' || rbt_tuit_refund);
    CLOSE sfrrfnd_c;
  END p_set_rbt_variables;

  /* ****************************************************************** */
  /* Set global variables for refunding by enrollment status.           */
  /* ****************************************************************** */
  PROCEDURE p_set_rfst_variables(term_in      IN stvterm.stvterm_code%TYPE,
                                 ests_code_in IN stvests.stvests_code%TYPE,
                                 ests_date_in IN DATE) IS
    CURSOR sfbrfst_c IS
      SELECT (100 - NVL(sfbrfst_tuit_refund, 0)) / 100,
             (100 - NVL(sfbrfst_fees_refund, 0)) / 100
        FROM sfbrfst
       WHERE sfbrfst_term_code = term_in
         AND sfbrfst_ests_code = ests_code_in
         AND TRUNC(ests_date_in) BETWEEN TRUNC(sfbrfst_from_date) AND
             TRUNC(sfbrfst_to_date);
  BEGIN
    OPEN sfbrfst_c;
    FETCH sfbrfst_c
      INTO rfst_tuit_liab_percentage, rfst_fees_liab_percentage;
    IF sfbrfst_c%FOUND THEN
      RFST_RULE_DEFINED := TRUE;
    ELSE
      RFST_RULE_DEFINED := FALSE;
    END IF;
    CLOSE sfbrfst_c;
  END p_set_rfst_variables;

  /* ******************************************************************* */
  /* Determine and save the total registration hours in the assessment   */
  /* that do not involve drops. This data is need for refunds involving  */
  /* previously met flat hour rules.                                     */
  /* ******************************************************************* */
  PROCEDURE p_create_nondrop_hrs_sfrfaud(pidm_in       IN spriden.spriden_pidm%TYPE,
                                         term_in       IN stvterm.stvterm_code%TYPE,
                                         source_pgm_in IN sfrfaud.sfrfaud_assessment_source%TYPE) IS
    tot_nondrop_hrs sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    tot_nonwaiv_hrs sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    next_seqno      sfrfaud.sfrfaud_seqno%TYPE := 0;
    note            VARCHAR2(80) := '';
    crn             sfrstcr.sfrstcr_crn%TYPE;
    act_date        DATE;
    bill_hr         sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    waiv_hr         sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    rsts_code       sfrstcr.sfrstcr_rsts_code%TYPE;

    really_a_wd varchar2(1) := '';
    is_a_varadd varchar2(1) := '';

    cursor really_a_wd_c is
      select 'Y'
        from stvrsts, sfrstcr
       where sfrstcr_term_code = term_in
         and sfrstcr_pidm = pidm_in
         and stvrsts_code = sfrstcr_rsts_code
         and stvrsts_withdraw_ind = 'Y'
         and stvrsts_incl_assess = 'Y'
         and sfrstcr_crn = crn;

    --- 8.2 rewrite cursor for performance
    CURSOR nondrop_hrs_c IS
      SELECT NVL(SUM(sftfees_reg_bill_hr), 0),
             NVL(SUM(sftfees_reg_waiv_hr), 0)
        FROM stvrsts, sftfees
       WHERE stvrsts_code = sftfees_rsts_cde
         AND stvrsts_withdraw_ind <> 'Y'
         AND sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in;
    /*
    CURSOR nondrop_hrs_c IS
          SELECT NVL(SUM(sftfees_reg_bill_hr),0),
                 NVL(SUM(sftfees_reg_waiv_hr),0)
            FROM sftfees
           WHERE sftfees_pidm = pidm_in
             AND sftfees_term_cde = term_in
             AND NOT EXISTS
             (SELECT 'drop' FROM stvrsts
               WHERE stvrsts_code = sftfees_rsts_cde
                AND stvrsts_withdraw_ind = 'Y');
    */
    CURSOR incl_assess_c IS
      SELECT sftfees_crn,
             sftfees_rsts_cde,
             NVL(sftfees_reg_bill_hr, 0),
             NVL(sftfees_reg_waiv_hr, 0)
        FROM stvrsts, sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND stvrsts_code = sftfees_rsts_cde
         AND stvrsts_incl_assess = 'Y';

    new_var_hrs sfrstcr.sfrstcr_bill_hr%type := 0.0;
    CURSOR variable_credit_c is
      SELECT sfrstca_bill_hr
        from sfrstca
       where sfrstca_pidm = pidm_in
         and sfrstca_term_code = term_in
         and sfrstca_bill_hr <> bill_hr
            --- 8.3.0.1 need to select for crn
         and sfrstca_crn = crn
            --- select the previous sfrstca to compare to sfrstcr
         and sfrstca_activity_date =
             (select max(m.sfrstca_activity_date)
                from sfrstca m
               where m.sfrstca_pidm = pidm_in
                 and m.sfrstca_term_code = term_in
                 and m.sfrstca_crn = crn
                 and m.sfrstca_activity_date < act_date)
            -- 8.3.0.1 also make sure it existed previously
         and exists
       (select 'x'
                from sfrstca m2
               where m2.sfrstca_pidm = pidm_in
                 and m2.sfrstca_term_code = term_in
                 and m2.sfrstca_crn = crn
                 and m2.sfrstca_activity_date < act_date);

  BEGIN
    p_print_dbms('***p create nondrop hrs sfrfaud begins***');
    --- 7.4.0.2 105831  process variable credits intermediate like DD, or DC/RE same crn
    IF (DD_SINCE_LAST_ASSESSMENT) OR (variable_act_date is not null) THEN
      -- sum all bill hrs to start over
      p_print_dbms('dd or variable credits  since last assessment so do not just add up bill hrs');
      OPEN incl_assess_c;
      LOOP
        FETCH incl_assess_c
          INTO crn, rsts_code, bill_hr, waiv_hr;
        IF incl_assess_c%NOTFOUND THEN
          p_print_dbms('incl assess c NOTFOUND exit the loop with crns');
          EXIT;
        ELSE
          new_var_hrs := 0;
          is_a_varadd := 'N';
          p_print_dbms('start of crn: ' || crn || ' rst cde: ' ||
                       rsts_code || ' billhr: ' || bill_hr);
          act_date := f_get_sfrstcr_date(pidm_in, term_in, crn);
          p_print_dbms('incl assess c FOUND crn act date, (must be < last assessment date): ' ||
                       to_char(act_date, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' < ' || ' last assmt: ' ||
                       to_char(last_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS') ||
                       ' interm date: ' ||
                       to_char(intermediate_date, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' dd last date: ' ||
                       to_char(last_dd_activity_date,
                               'DD-MON-YYYY HH24:MI:SS') ||
                       ' var act date: ' ||
                       to_char(variable_act_date, 'DD-MON-YYYY HH24:MI:SS'));
          --- 1-2O637G  if intermediate assessment look for courses with activity date less then the interm
          --  else look for those with act date <= last assessemnt.
          --  The billing hrs on the D faud record was 0, which is created during the intermediate
          --  it needs to be sum of billing hours on courses before the DD
          --  if this is a dc after a dd include in the non drop hours but do not include if its an re after the dd
          -- 8.3.0.1 only execute the dd last act if there is a dd for this assessment
          if last_dd_activity_date is not null and
             ((variable_act_date is null) or
             (variable_act_date is not null and
             variable_act_date < last_dd_activity_date))
          ---   and variable_act_date is null
           then
            p_print_dbms('last dd activity date is not null');
            if (act_date >= last_dd_activity_date) then
              --- check if its a Drop originally
              open really_a_wd_c;
              fetch really_a_wd_c
                into really_a_wd;
              if really_a_wd_c%notfound then
                really_a_wd := 'N';
              end if;
              close really_a_wd_c;
            else
              really_a_wd := 'N';
            end if;

            ---  7.4.0.2  105831 if not a DD, but a variable credit hour change we do not want to include new REs if they
            --- occur at the same time or after the credit hour change
          elsif variable_act_date is not null then
            p_print_dbms('variable act date is not null for assessment');
            if (act_date >= variable_act_date) then
              open really_a_wd_c;
              fetch really_a_wd_c
                into really_a_wd;
              if really_a_wd_c%notfound then
                really_a_wd := 'N';
                --  now find out the original credit hours, we want to include the variable credit hrs but not
                --   other new adds
                if act_date > sfbetrm_rec.sfbetrm_assessment_date then
                  open variable_credit_c;
                  fetch variable_credit_c
                    into new_var_hrs;
                  if variable_credit_c%notfound then
                    -- not a variable credit
                    p_print_dbms('this was a variable credit: ' ||
                                 new_var_hrs);
                    really_a_wd := 'N';
                    is_a_varadd := 'N';
                  else
                    --  a variable credit change which needs to be included
                    really_a_wd := 'Y';
                    is_a_varadd := 'Y';
                  end if;
                  close variable_credit_c;
                end if;
              end if;
              close really_a_wd_c;
            else
              really_a_wd := 'N';
            end if;

          end if;
          p_print_dbms('before adding to the tot nondrop hrs: ' ||
                       tot_nondrop_hrs || ' + ' || bill_hr ||
                       ' new var hrs: ' || new_var_hrs);
          p_print_dbms('is a varadd: ' || is_a_varadd || ' really a wd: ' ||
                       really_a_wd);
          if (DO_INTERMEDIATE) then
            p_print_dbms('yes there was an intermediate');
          end if;
          p_print_dbms('variable_act_date ' ||
                       to_char(variable_act_date, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' act_date ' ||
                       to_char(act_date, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' really a wd: ' || really_a_wd ||
                       '   last_dd_activity_date ' ||
                       to_char(last_dd_activity_date,
                               'DD-MON-YYYY HH24:MI:SS'));
          -- 8.3.0.1 look at this and see why the increased hrs are not added in
          --  we only want to add in changes to var, not new RE
          IF ((DO_INTERMEDIATE and
             (((act_date < last_dd_activity_date and
             last_dd_activity_date is not null) OR
             (act_date >= last_dd_activity_date and
             last_dd_activity_date is not null and really_a_wd = 'Y')) OR
             (variable_act_date is not null and
             ((really_a_wd = 'Y') OR
             (really_a_wd = 'N' and
             ((act_date < variable_act_date) or
             (act_date = variable_act_date and is_a_varadd = 'Y')))))
             --   OR (really_a_wd = 'N' and is_a_varadd = 'Y'
             --- 8.3.0.1 change < to <= because activty date and variable may be the same
             --   and act_date <= variable_act_date -- and this is not the first add for the crse
             --      ) )  )
             OR (act_date < intermediate_date and
             last_dd_activity_date is null and
             variable_act_date is null))) or
             (NOT (DO_INTERMEDIATE) and act_date <= last_assessment_date)) and
             (not (f_rsts_means_wd(rsts_code))) THEN
            -- was originally before 7.4.0.1
            ----IF (   act_date < last_assessment_date
            ----      OR f_rsts_means_wd(rsts_code) ) THEN

            tot_nondrop_hrs := tot_nondrop_hrs + bill_hr;
            tot_nonwaiv_hrs := tot_nonwaiv_hrs + waiv_hr;
            p_print_dbms('--tot nondrop hrs after add: ' ||
                         tot_nondrop_hrs || ' tot nonwaiv hrs 5x ' ||
                         tot_nonwaiv_hrs);
          END IF;
        END IF;
      END LOOP;
      CLOSE incl_assess_c;
      note := g$_nls.get('SFKFEE1-0000',
                         'SQL',
                         'DD/not count in assessment status handled. Total registered hours are %01%.',
                         tot_nondrop_hrs);
    ELSE
      OPEN nondrop_hrs_c;
      FETCH nondrop_hrs_c
        INTO tot_nondrop_hrs, tot_nonwaiv_hrs;
      IF nondrop_hrs_c%NOTFOUND THEN
        p_print_dbms('nondrop hrs c NOTFOUND');
        tot_nondrop_hrs := 0.0;
        tot_nonwaiv_hrs := 0.0;
      END IF;
      CLOSE nondrop_hrs_c;
      note := g$_nls.get('SFKFEE1-0001',
                         'SQL',
                         'Total non-dropped hours for assessment is %01%. ',
                         tot_nondrop_hrs);
    END IF;

    -- determine next seqno for the audit record and insert it
    next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                 term_in,
                                                 save_sessionid);

    p_print_dbms('p_insert_sfrfaud 1, dd assess rfnd cde: ' ||
                 dd_assess_rfnd_cde || ' nondrop billhrs ' ||
                 tot_nondrop_hrs || ' non waiv hrs ' || tot_nonwaiv_hrs);
    --  note := g$_nls.get('x','SQL','Total non-dropped hours for assessment is %01%',
    --       to_char(tot_nondrop_hrs,'9999D99'));
    p_insert_sfrfaud(pidm_in,
                     term_in,
                     next_seqno,
                     NVL(dd_assess_rfnd_cde, 'N'), -- 'N'on-drop hours data or 'D'rop delete handled
                     NULL, -- no DCAT code, info rec
                     NULL, -- no detail code, info rec
                     0, -- no amount, info rec
                     'N', -- ABC: N/A for nondrop hrs total
                     source_pgm_in,
                     NULL, -- no rules
                     NULL, -- no rule type, is for overall reg
                     NULL, -- no CRN
                     NULL, -- no RSTS code
                     NULL, -- no RSTS date
                     NULL, -- no ESTS code
                     NULL, -- no ESTS date
                     NULL, -- no refund table
                     tot_nondrop_hrs,
                     tot_nonwaiv_hrs, -- 99226
                     NULL, -- no rule cred hours
                     NULL, -- no rule stud hours
                     NULL, -- no per cred charge
                     NULL, -- no flat fee rule
                     NULL, -- no overload hrs
                     NULL, -- no crse overload charge
                     NULL, -- TBRACCD tran no
                     note,
                     NULL, -- no liable credit hours
                     -- RPE 35999: following added to store rule calc values
                     NULL, -- rule per credit charge
                     NULL, -- rule flat fee amount
                     NULL, -- rule from flat hrs
                     NULL, -- rule to flat hrs
                     NULL); -- rule course OL start hr
    ---- 7.4.0.1 save non drop for flat processing
    new_nondrop_hrs := tot_nondrop_hrs;
    new_nonwaiv_hrs := tot_nonwaiv_hrs;
  END p_create_nondrop_hrs_sfrfaud;

  --- 7.5 for types
  PROCEDURE p_create_nondrop_type(pidm_in       IN spriden.spriden_pidm%TYPE,
                                  term_in       IN stvterm.stvterm_code%TYPE,
                                  source_pgm_in IN varchar2,
                                  type_in       IN sfrrgfe.sfrrgfe_type%TYPE,
                                  rule_value_in IN varchar2) IS
    tot_nondrop_hrs     sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    tot_nonwaiv_hrs     sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    next_seqno          sfrfaud.sfrfaud_seqno%TYPE := 0;
    note                VARCHAR2(80) := '';
    crn                 sfrstcr.sfrstcr_crn%TYPE;
    act_date            DATE;
    bill_hr             sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    waiv_hr             sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    rsts_code           sfrstcr.sfrstcr_rsts_code%TYPE;
    really_a_wd         varchar2(1) := '';
    lv_original_credits sfrstcr.sfrstcr_bill_hr%type := 0.0;

    faud_already_exists varchar2(1) := 'N';
    cursor faud_already_exists_c is
      select 'Y'
        from sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NULL
         AND sfrfaud_activity_date =
             NVL(intermediate_date, save_activity_date)
         AND sfrfaud_assess_rfnd_penlty_cde = NVL(dd_assess_rfnd_cde, 'N')
         AND sfrfaud_assessment_source = source_pgm_in
         AND nvl(sfrfaud_rgfe_type, STUDENT_TYPE) =
             nvl(type_in, STUDENT_TYPE)
         AND decode(type_in,
                    STUDENT_TYPE,
                    '%',
                    nvl(sfrfaud_RGFE_RULE_VALUE, '%')) =
             nvl(rule_value_in, '%');

    cursor really_a_wd_c is
      select 'Y'
        from stvrsts, sfrstcr
       where sfrstcr_term_code = term_in
         and sfrstcr_pidm = pidm_in
         and stvrsts_code = sfrstcr_rsts_code
         and stvrsts_withdraw_ind = 'Y'
         and stvrsts_incl_assess = 'Y'
         and sfrstcr_crn = crn;

    CURSOR nondrop_hrs_c IS
      SELECT NVL(SUM(sftfees_reg_bill_hr), 0),
             NVL(SUM(sftfees_reg_waiv_hr), 0)
        FROM sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         and ((type_in = CAMPUS_TYPE and
             sftfees_camp_cde_crse = rule_value_in) OR
             (type_in = LEVEL_TYPE and
             sftfees_levl_cde_crse = rule_value_in) OR
             (type_in = ATTR_TYPE and exists
              (select 'x'
                  from ssrattr
                 where ssrattr_term_code = term_in
                   and ssrattr_crn = sftfees_crn
                   and ssrattr_attr_code = rule_value_in)))
         AND NOT EXISTS (SELECT 'drop'
                FROM stvrsts
               WHERE stvrsts_code = sftfees_rsts_cde
                 AND stvrsts_withdraw_ind = 'Y');

    CURSOR incl_assess_c IS
      SELECT sftfees_crn,
             sftfees_rsts_cde,
             NVL(sftfees_reg_bill_hr, 0),
             NVL(sftfees_reg_waiv_hr, 0)
        FROM stvrsts, sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND stvrsts_code = sftfees_rsts_cde
         and ((type_in = CAMPUS_TYPE and
             sftfees_camp_cde_crse = rule_value_in) OR
             (type_in = LEVEL_TYPE and
             sftfees_levl_cde_crse = rule_value_in) OR
             (type_in = ATTR_TYPE and exists
              (select 'x'
                  from ssrattr
                 where ssrattr_term_code = term_in
                   and ssrattr_crn = sftfees_crn
                   and ssrattr_attr_code = rule_value_in)))
         AND stvrsts_incl_assess = 'Y';

    new_var_hrs sfrstcr.sfrstcr_bill_hr%type := 0.0;
    CURSOR variable_credit_c is
      SELECT sfrstca_bill_hr
        from sfrstca
       where sfrstca_pidm = pidm_in
         and sfrstca_term_code = term_in
         and sfrstca_bill_hr <> bill_hr
         and ((type_in = CAMPUS_TYPE and sfrstca_camp_code = rule_value_in) OR
             (type_in = LEVEL_TYPE and sfrstca_levl_code = rule_value_in) OR
             (type_in = ATTR_TYPE and exists
              (select 'x'
                  from ssrattr
                 where ssrattr_term_code = term_in
                   and ssrattr_crn = sfrstca_crn
                   and ssrattr_attr_code = rule_value_in)))
            --- select the previous sfrstca to compare to sfrstcr
         and sfrstca_activity_date =
             (select max(m.sfrstca_activity_date)
                from sfrstca m
               where m.sfrstca_pidm = pidm_in
                 and m.sfrstca_term_code = term_in
                 and m.sfrstca_crn = crn
                 and m.sfrstca_activity_date < act_date);

  BEGIN
    --- do not insert a second time
    faud_already_exists := 'N';
    open faud_already_exists_c;
    fetch faud_already_exists_c
      into faud_already_exists;
    if faud_already_exists_c%notfound then
      faud_already_exists := 'N';
    end if;
    close faud_already_exists_c;
    if faud_already_exists = 'Y' then
      return;
    end if;

    p_print_dbms('p create nondrop hrs sfrfaud begins');
    --- 7.4.0.2 105831  process variable credits intermediate like DD, or DC/RE same crn
    IF (DD_SINCE_LAST_ASSESSMENT) OR (variable_act_date is not null) THEN
      -- sum all bill hrs to start over
      p_print_dbms('dd or variable since last assessment');
      OPEN incl_assess_c;
      LOOP
        FETCH incl_assess_c
          INTO crn, rsts_code, bill_hr, waiv_hr;
        IF incl_assess_c%NOTFOUND THEN
          p_print_dbms('incl assess c NOTFOUND');
          EXIT;
        ELSE
          p_print_dbms('crn: ' || crn || ' rst cde: ' || rsts_code ||
                       ' billhr: ' || bill_hr);
          act_date := f_get_sfrstcr_date(pidm_in, term_in, crn);
          p_print_dbms('incl assess c FOUND crn act date, (must be < last assessment date): ' ||
                       to_char(act_date, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' < ' || ' last assmt: ' ||
                       to_char(last_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS') ||
                       ' interm date: ' ||
                       to_char(intermediate_date, 'DD-MON-YYYY HH24:MI:SS'));
          p_print_dbms(' dd last date: ' ||
                       to_char(last_dd_activity_date,
                               'DD-MON-YYYY HH24:MI:SS') ||
                       ' var act date: ' ||
                       to_char(variable_act_date, 'DD-MON-YYYY HH24:MI:SS'));
          --- 1-2O637G  if intermediate assessment look for courses with activity date less then the interm
          --  else look for those with act date <= last assessemnt.
          --  The billing hrs on the D faud record was 0, which is created during the intermediate
          --  it needs to be sum of billing hours on courses before the DD
          --  if this is a dc after a dd include in the non drop hours but do not include if its an re after the dd
          if last_dd_activity_date is not null then

            if (act_date >= last_dd_activity_date) then
              --- check if its a Drop originally
              open really_a_wd_c;
              fetch really_a_wd_c
                into really_a_wd;
              if really_a_wd_c%notfound then
                really_a_wd := 'N';
              end if;
              close really_a_wd_c;
            else
              really_a_wd := 'N';
            end if;
            ---  7.4.0.2  105831 if not a DD, but a variable credit hour change we do not want to include new REs if they
            --- occur at the same time or after the credit hour change
          elsif variable_act_date is not null then
            if (act_date >= variable_act_date) then
              open really_a_wd_c;
              fetch really_a_wd_c
                into really_a_wd;
              if really_a_wd_c%notfound then
                really_a_wd := 'N';
                --  now find out the original credit hours, we want to include the variable credit hrs but not
                --   other new adds
                if act_date > sfbetrm_rec.sfbetrm_assessment_date then
                  open variable_credit_c;
                  fetch variable_credit_c
                    into new_var_hrs;
                  if variable_credit_c%notfound then
                    -- not a variable credit
                    really_a_wd := 'N';
                  else
                    --  a variable credit change which needs to be included
                    really_a_wd := 'Y';
                  end if;
                  close variable_credit_c;
                end if;
              end if;
              close really_a_wd_c;
            else
              really_a_wd := 'N';
            end if;

          end if;
          p_print_dbms('really a wd: ' || really_a_wd || ' var hrs: ' ||
                       new_var_hrs);
          IF ((DO_INTERMEDIATE and
             (((act_date < last_dd_activity_date and
             last_dd_activity_date is not null) OR
             (act_date >= last_dd_activity_date and
             last_dd_activity_date is not null and really_a_wd = 'Y')) OR
             (variable_act_date is not null and
             ((really_a_wd = 'Y') OR
             (really_a_wd = 'N' and act_date < variable_act_date))) OR
             (act_date < intermediate_date and
             last_dd_activity_date is null and
             variable_act_date is null))) or
             (NOT (DO_INTERMEDIATE) and act_date <= last_assessment_date)) and
             (not (f_rsts_means_wd(rsts_code))) THEN
            -- was originally before 7.4.0.1
            ----IF (   act_date < last_assessment_date
            ----      OR f_rsts_means_wd(rsts_code) ) THEN

            tot_nondrop_hrs := tot_nondrop_hrs + bill_hr;
            tot_nonwaiv_hrs := tot_nonwaiv_hrs + waiv_hr;
            p_print_dbms('tot nondrop hrs 5x ' || tot_nondrop_hrs ||
                         '  tot nonwaiv hrs 5x ' || tot_nonwaiv_hrs);
          END IF;
        END IF;
      END LOOP;
      CLOSE incl_assess_c;
      note := 'DD/not count in assessment status handled. Total registered hours are ' ||
              tot_nondrop_hrs || '.';
    ELSE
      OPEN nondrop_hrs_c;
      FETCH nondrop_hrs_c
        INTO tot_nondrop_hrs, tot_nonwaiv_hrs;
      IF nondrop_hrs_c%NOTFOUND THEN
        p_print_dbms('nondrop hrs c NOTFOUND');
        tot_nondrop_hrs := 0.0;
        tot_nonwaiv_hrs := 0.0;
      END IF;
      CLOSE nondrop_hrs_c;
      note := 'Total non-dropped hours for assessment is ' ||
              tot_nondrop_hrs || '.';
    END IF;

    -- determine next seqno for the audit record and insert it
    next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                 term_in,
                                                 save_sessionid);

    p_print_dbms('p_insert_sfrfaud 1, dd assess rfnd cde: ' ||
                 dd_assess_rfnd_cde || ' nondrop billhrs ' ||
                 tot_nondrop_hrs || ' non waiv hrs ' || tot_nonwaiv_hrs);
    p_insert_sfrfaud(pidm_in,
                     term_in,
                     next_seqno,
                     NVL(dd_assess_rfnd_cde, 'N'), -- 'N'on-drop hours data or 'D'rop delete handled
                     NULL, -- no DCAT code, info rec
                     NULL, -- no detail code, info rec
                     0, -- no amount, info rec
                     'N', -- ABC: N/A for nondrop hrs total
                     source_pgm_in,
                     NULL, -- no rules
                     type_in, -- no rule type, is for overall reg --- fill in now
                     null, -- no CRN  --- use temp for 7.5
                     NULL, -- no RSTS code
                     NULL, -- no RSTS date
                     NULL, -- no ESTS code
                     NULL, -- no ESTS date
                     NULL, -- no refund table
                     tot_nondrop_hrs,
                     tot_nonwaiv_hrs, -- 99226
                     NULL, -- no rule cred hours
                     NULL, -- no rule stud hours
                     NULL, -- no per cred charge
                     NULL, -- no flat fee rule
                     NULL, -- no overload hrs
                     NULL, -- no crse overload charge
                     NULL, -- TBRACCD tran no
                     'Total non-dropped hours for assessment is ' ||
                     tot_nondrop_hrs,
                     NULL, -- no liable credit hours
                     -- RPE 35999: following added to store rule calc values
                     NULL, -- rule per credit charge
                     NULL, -- rule flat fee amount
                     NULL, -- rule from flat hrs
                     NULL, -- rule to flat hrs
                     NULL, -- rule course OL start hr
                     rule_value_in);
    ---- 7.4.0.1 save non drop for flat processing
    new_nondrop_hrs := tot_nondrop_hrs;
    new_nonwaiv_hrs := tot_nonwaiv_hrs;
    N_FAUD_CREATED  := TRUE;

  END p_create_nondrop_type;

  /* ******************************************************************* */
  /*  Process the course registrations for the student/term.             */
  /* ******************************************************************* */
  /*  Select all the course registrations for the student, including     */
  /*  refund information. Refund information determination occurs if the */
  /*  registration status code (RSTS code) means WD.                     */
  /*                                                                     */
  /*  If the site has chosen to refund by total (sobterm_ref_ind = 'Y'), */
  /*  then determine the refund information based on the registration    */
  /*  status date using SFRRFND, the refund control table, if the RSTS   */
  /*  code means WD. *NOTE* It is assummed that the site will be current */
  /*  with assessment (on-line assessment is turned on and/or assessment */
  /*  is run in batch nightly before midnight).                          */
  /*                                                                     */
  /*  If the site is not refunding by total, determine the refund for    */
  /*  each course registration status code and date using SFRRFCR when   */
  /*  the RSTS code means WD.                                            */
  /*                                                                     */
  /*  Insert the course values into temp table, SFTFEES, and go on to    */
  /*  calculate the section fees for the course.                         */
  /* ******************************************************************* */
  PROCEDURE p_studentcourses(pidm_in              IN spriden.spriden_pidm%TYPE,
                             term_in              IN stvterm.stvterm_code%TYPE,
                             source_pgm_in        IN VARCHAR2,
                             return_status_in_out IN OUT NUMBER) IS
    /* ****************************************************************** */
    /* The percentages from SFRRFND have already been selected in         */
    /* p_set_rbt_variables and are saved in rbt_% variables. There are no */
    /* course refunds for RBT. The refund is the difference in the prev   */
    /* and current assessment charges, which is based only on the hours   */
    /* for the courses that the student is registered in. The student is  */
    /* always considered 100% liable for the hours they are registered in.*/
    /* The penalty (percentage is the compliment of the refund percent    */
    /* definition) is applied to the refund amount.                       */
    /* ****************************************************************** */
    CURSOR reg_by_rfnd_c IS
      SELECT 0, -- no tui course refunds for RBT
             0, -- no fee course refunds for RBT
             sfrstcr_crn,
             sfrstcr_levl_code,
             sfrstcr_camp_code,
             sfrstcr_ptrm_code,
             sfrstcr_rsts_code,
             sfrstcr_rsts_date,
             sfrstcr_credit_hr,
             sfrstcr_bill_hr,
             sfrstcr_waiv_hr,
             sfrstcr_gmod_code,
             stvrsts_withdraw_ind,
             stvrsts_incl_sect_enrl
        FROM stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND stvrsts_code = sfrstcr_rsts_code
         AND stvrsts_incl_assess = 'Y';

    changed_ests_date_ind VARCHAR2(1) := 'N';
    last_crn_assmt_date   date;

    wd_exists varchar2(1) := 'N';
    CURSOR wd_exists_c is
      SELECT 'Y'
        from stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND stvrsts_code = sfrstcr_rsts_code
         AND stvrsts_withdraw_ind = 'Y';
  BEGIN
    p_reinit_temp_vars;

    p_print_dbms(' p studentcourse begins');
    /* ******************************************************** */
    /* Hierarchy for determining refund method in effect, if    */
    /* any, for the assessment:                                 */
    /* If ESTS code means WD                                    */
    /*   If ESTS refund rule defined                            */
    /*      Then perform ESTS refund                            */
    /*   Else If RBT in effect for the term                     */
    /*      Then check if RBT rule defined                      */
    /*      If RBT rule defined                                 */
    /*         Then process RBT                                 */
    /*      (if no RBT rule defined, no refunding occurs at all)*/
    /*   Else                                                   */
    /*      Default to/Process refund by course                 */
    /* Else If RBT in effect for the term                       */
    /*   If RBT rule defined                                    */
    /*      Then process RBT                                    */
    /*   (if no RBT rule defined, no refunding occurs at all)   */
    /* Else                                                     */
    /*   Process refund by course                               */
    /* ******************************************************** */
    IF f_ests_means_wd(sfbetrm_rec.sfbetrm_ests_code) THEN
      p_print_dbms(' f ests means wd 4r');
      p_set_rfst_variables(term_in,
                           sfbetrm_rec.sfbetrm_ests_code,
                           sfbetrm_rec.sfbetrm_ests_date);
      IF (f_ests_wd_assessed(pidm_in, term_in, changed_ests_date_ind)) THEN
        p_print_dbms(' f ests wd assessed 7s');
        IF changed_ests_date_ind = 'Y' THEN
          p_print_dbms(' changed ests date ind is Y');
          IF NOT (RFST_RULE_DEFINED) THEN
            -- no refund; 100% liable for AR total
            p_print_dbms(' NOT rfst rule defined');
            rfst_tuit_liab_percentage := 1; -- 100% liable for AR total
            rfst_fees_liab_percentage := 1; -- 100% liable for AR total
            p_print_dbms(' call to p calc rfst liability 5h');
            p_calc_rfst_liability(pidm_in, term_in, 'Y', source_pgm_in);
            RETURN; -- no courses to process
          ELSE
            p_print_dbms(' call to p calc rfst liability 9w');
            p_calc_rfst_liability(pidm_in, term_in, 'N', source_pgm_in);
            RETURN; -- no courses to process
          END IF;
        ELSE
          rfst_tuit_liab_percentage := 1; -- 100% liable for AR total
          rfst_fees_liab_percentage := 1; -- 100% liable for AR total
          p_print_dbms(' call to p calc rfst liability 8p');
          p_calc_rfst_liability(pidm_in, term_in, 'Y', source_pgm_in);
          RETURN; -- no courses to process
        END IF;
      ELSE
        -- ESTS refund not processed yet
        IF (RFST_RULE_DEFINED) THEN
          p_print_dbms(' call to p calc rfst liability 1z');
          p_calc_rfst_liability(pidm_in, term_in, 'N', source_pgm_in);
          p_print_dbms('return from p_calc_rfst_liab');
          RETURN; -- no courses to process
          --  rule not found; check for RBT
        ELSIF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' THEN

          open wd_exists_c;
          fetch wd_exists_c
            into wd_exists;
          if wd_exists_c%notfound then
            crn_rfnd_table := null;
          else
            crn_rfnd_table := 'SFRRFND';
          end if;
          close wd_exists_c;

          OPEN reg_by_rfnd_c;
          LOOP
            FETCH reg_by_rfnd_c
              INTO crn_tuit_refund, -- whole number for percent
                   crn_fees_refund, -- whole number for percent
                   crn_sfrstcr_crn,
                   crn_sfrstcr_levl_code,
                   crn_sfrstcr_camp_code,
                   crn_sfrstcr_ptrm_code,
                   crn_sfrstcr_rsts_code,
                   crn_sfrstcr_rsts_date,
                   crn_sfrstcr_credit_hr,
                   crn_sfrstcr_bill_hr,
                   crn_sfrstcr_waiv_hr,
                   crn_sfrstcr_gmod_code,
                   crn_status_withdraw_ind,
                   crn_status_enroll_ind;
            IF reg_by_rfnd_c%NOTFOUND THEN
              EXIT;
            ELSE
              p_print_dbms(' call to p process course 4f' ||
                           crn_sfrstcr_crn);
              p_processcourse(pidm_in,
                              term_in,
                              crn_rfnd_table,
                              source_pgm_in,
                              return_status_in_out);
            END IF;
          END LOOP;
          CLOSE reg_by_rfnd_c;
        ELSE
          -- refund by ESTS and RBT not in effect; default to refund by course
          p_print_dbms(' call to p courses by rfcr 7l');
          open wd_exists_c;
          fetch wd_exists_c
            into wd_exists;
          if wd_exists_c%notfound then
            crn_rfnd_table := null;
          else
            crn_rfnd_table := 'SFRRFCR';
          end if;
          close wd_exists_c;
          p_courses_by_rfcr(pidm_in,
                            term_in,
                            source_pgm_in,
                            return_status_in_out);
        END IF;
      END IF;
      -- not refunding by ESTS, check for RBT
    ELSIF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' THEN

      open wd_exists_c;
      fetch wd_exists_c
        into wd_exists;
      if wd_exists_c%notfound then
        crn_rfnd_table := null;
      else
        crn_rfnd_table := 'SFRRFND';
      end if;
      close wd_exists_c;

      OPEN reg_by_rfnd_c;
      LOOP
        FETCH reg_by_rfnd_c
          INTO crn_tuit_refund, -- whole number for percent
               crn_fees_refund, -- whole number for percent
               crn_sfrstcr_crn,
               crn_sfrstcr_levl_code,
               crn_sfrstcr_camp_code,
               crn_sfrstcr_ptrm_code,
               crn_sfrstcr_rsts_code,
               crn_sfrstcr_rsts_date,
               crn_sfrstcr_credit_hr,
               crn_sfrstcr_bill_hr,
               crn_sfrstcr_waiv_hr,
               crn_sfrstcr_gmod_code,
               crn_status_withdraw_ind,
               crn_status_enroll_ind;
        IF reg_by_rfnd_c%NOTFOUND THEN
          EXIT;
        ELSE
          p_print_dbms(' call to p process course 0j ' || crn_rfnd_table ||
                       ' crn:' || crn_sfrstcr_crn || ' rsts code: ' ||
                       crn_sfrstcr_rsts_code || ' bill: ' ||
                       crn_sfrstcr_bill_hr || ' crd: ' ||
                       crn_sfrstcr_credit_hr || ' with: ' ||
                       crn_status_withdraw_ind || ' tuitref: ' ||
                       crn_tuit_refund || ' feeref: ' || crn_fees_refund);
          p_processcourse(pidm_in,
                          term_in,
                          crn_rfnd_table,
                          source_pgm_in,
                          return_status_in_out);
        END IF;
      END LOOP;
      CLOSE reg_by_rfnd_c;
    ELSE
      -- default to refund by course
      p_print_dbms(' call to p courses by rfcr 3m');
      open wd_exists_c;
      fetch wd_exists_c
        into wd_exists;
      if wd_exists_c%notfound then
        crn_rfnd_table := null;
      else
        crn_rfnd_table := 'SFRRFCR';
      end if;
      close wd_exists_c;
      p_courses_by_rfcr(pidm_in,
                        term_in,
                        source_pgm_in,
                        return_status_in_out);
    END IF;
    p_print_dbms('call to p create nondrop hrs sfrfaud 2i');
    p_create_nondrop_hrs_sfrfaud(pidm_in, term_in, source_pgm_in);
  END p_studentcourses;

  /* ******************************************************************* */
  /*  Process the course registrations for the student/term strictly     */
  /*  by course refunding rules in SFRRFCR.                              */
  /* ******************************************************************* */
  PROCEDURE p_courses_by_rfcr(pidm_in              IN spriden.spriden_pidm%TYPE,
                              term_in              IN stvterm.stvterm_code%TYPE,
                              source_pgm_in        IN VARCHAR2,
                              return_status_in_out IN OUT NUMBER) IS
    actdate  date;
    crn_ptrm stvptrm.stvptrm_code%type;
    --- 8.0.1 remove outer join to separate cursor
    CURSOR reg_by_rfcr_c IS
      SELECT sfrstcr_crn,
             sfrstcr_levl_code,
             sfrstcr_camp_code,
             sfrstcr_ptrm_code,
             sfrstcr_rsts_code,
             sfrstcr_rsts_date,
             sfrstcr_credit_hr,
             sfrstcr_bill_hr,
             sfrstcr_waiv_hr,
             sfrstcr_gmod_code,
             stvrsts_withdraw_ind,
             stvrsts_incl_sect_enrl,
             sfrstcr_ptrm_code,
             sfrstcr_activity_date
        FROM stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND stvrsts_code = sfrstcr_rsts_code
         AND stvrsts_incl_assess = 'Y'
      --- 7.4.0.1 swappin needs courses to be in order of rsts date
       order by sfrstcr_activity_date;

    CURSOR rfcr_c IS
      SELECT sfrrfcr_tuit_refund,
             sfrrfcr_fees_refund,
             sfrrfcr_from_date,
             sfrrfcr_to_date
        from sfrrfcr /* PTRM refund percentage table */
       where sfrrfcr_term_code = term_in
         AND sfrrfcr_ptrm_code = crn_ptrm
         AND sfrrfcr_rsts_code = crn_sfrstcr_rsts_code
         AND TRUNC(crn_sfrstcr_rsts_date) BETWEEN sfrrfcr_from_date AND
             sfrrfcr_to_date;
  BEGIN
    p_print_dbms(' p courses by rfcr begins , insert crn: ' ||
                 crn_sfrstcr_crn);

    OPEN reg_by_rfcr_c;
    LOOP
      FETCH reg_by_rfcr_c
        INTO crn_sfrstcr_crn,
             crn_sfrstcr_levl_code,
             crn_sfrstcr_camp_code,
             crn_sfrstcr_ptrm_code,
             crn_sfrstcr_rsts_code,
             crn_sfrstcr_rsts_date,
             crn_sfrstcr_credit_hr,
             crn_sfrstcr_bill_hr,
             crn_sfrstcr_waiv_hr,
             crn_sfrstcr_gmod_code,
             crn_status_withdraw_ind,
             crn_status_enroll_ind,
             crn_ptrm,
             actdate;
      IF reg_by_rfcr_c%NOTFOUND THEN
        EXIT;
      ELSE
        OPEN rfcr_c;
        fetch rfcr_c
          into crn_tuit_refund, -- whole number for percent
               crn_fees_refund, -- whole number for percent
               crn_rfnd_from_date,
               crn_rfnd_to_date;
        if rfcr_c%notfound then
          crn_tuit_refund    := null;
          crn_fees_refund    := null;
          crn_rfnd_from_date := null;
          crn_rfnd_to_date   := null;
        end if;
        close rfcr_c;

        p_print_dbms(' call to p processcourse 6y: ' || crn_sfrstcr_crn ||
                     ' cmp: ' || crn_sfrstcr_camp_code || ' lv: ' ||
                     crn_sfrstcr_levl_code || ' bil hrs: ' ||
                     crn_sfrstcr_bill_hr || ' dateL ' ||
                     to_char(actdate, 'DD-MON-YYYY HH24:MI:SS'));
        p_processcourse(pidm_in,
                        term_in,
                        crn_rfnd_table,
                        source_pgm_in,
                        return_status_in_out);
      END IF;
    END LOOP;
    CLOSE reg_by_rfcr_c;
  END p_courses_by_rfcr;

  /* ******************************************************************* */
  /*  Process the course.                                                */
  /* ******************************************************************* */
  /* Handle both traditional and open learning courses. The existence of */
  /* a PTRM code indicates a traditional course. When no PTRM, we need   */
  /* to select the additional registration information from SFRAREG.     */
  /* ******************************************************************* */
  PROCEDURE p_processcourse(pidm_in              IN spriden.spriden_pidm%TYPE,
                            term_in              IN stvterm.stvterm_code%TYPE,
                            rfnd_source_table_in IN VARCHAR2,
                            source_pgm_in        IN VARCHAR2,
                            return_status_in_out IN OUT NUMBER) IS
    CURSOR ssbsect_c IS
      SELECT ssbsect_ptrm_start_date,
             ssbsect_ptrm_end_date,
             ssbsect_schd_code,
             ssbsect_insm_code
        FROM ssbsect
       WHERE ssbsect_term_code = term_in
         AND ssbsect_ptrm_code = crn_sfrstcr_ptrm_code
         AND ssbsect_crn = crn_sfrstcr_crn;

    /* ***************************************************************** */
    /* Find the additional registration (SFRAREG) row for main course    */
    /* registration (SFRSTCR) record being processed. This will be the   */
    /* SFRAREG record with extension number 0.                           */
    /* ***************************************************************** */
    CURSOR sfrareg_c IS
      SELECT sfrareg_start_date,
             sfrareg_completion_date,
             ssbsect_schd_code,
             ssbsect_insm_code,
             sfrareg_dunt_code,
             stvrsts_withdraw_ind
        FROM stvrsts, ssbsect, sfrareg
       WHERE sfrareg_pidm = pidm_in
         AND sfrareg_term_code = term_in
         AND sfrareg_crn = crn_sfrstcr_crn
         AND sfrareg_extension_number = 0 -- select the header record
         AND ssbsect_term_code = term_in
         AND ssbsect_ptrm_code IS NULL
         AND ssbsect_crn = crn_sfrstcr_crn
         AND stvrsts_code = sfrareg_rsts_code
         AND stvrsts_incl_assess = 'Y';

    crn_sfrareg_dunt_code         sfrareg.sfrareg_dunt_code%TYPE := '';
    crn_ssrrfnd_pct_complete      ssrrfnd.ssrrfnd_refund_pct_complete%TYPE := 0;
    crn_ssrrfnd_duration_complete ssrrfnd.ssrrfnd_duration_complete%TYPE := 0.0;
    crn_ssrrfnd_tuit_refund       ssrrfnd.ssrrfnd_tuition_refund_pct%TYPE := 0;
    crn_ssrrfnd_fee_refund        ssrrfnd.ssrrfnd_fee_refund_pct%TYPE := 0;
    crn_ssrrfnd_extn_refund       ssrrfnd.ssrrfnd_extension_refund_pct%TYPE := 0;
    crn_return_type               VARCHAR2(1) := '';
    crn_schd_code                 stvschd.stvschd_code%TYPE := '';
    crn_insm_code                 gtvinsm.gtvinsm_code%TYPE := '';

    -- new cursor for 8.1.1 to find previous non drop hrs
    --  for 8.3 change to look at the specific course from the audit
    -- need to find out if the variable hrs were changed
    hrs_last_assmt sftfees.sftfees_reg_bill_hr%TYPE := 0.0;
    CURSOR hrs_last_assmt_c IS
      SELECT NVL(sfrstca_bill_hr, 0)
        FROM stvrsts, sfrstca
       WHERE sfrstca_pidm = pidm_in
         AND sfrstca_term_code = term_in
         AND sfrstca_crn = crn_sfrstcr_crn
         and stvrsts_code = sfrstca_rsts_code
         and stvrsts_withdraw_ind = 'N'
         and stvrsts_incl_assess = 'Y'
         AND sfrstca_activity_date <= last_assessment_date
         and sfrstca_seq_number =
             (select max(m.sfrstca_seq_number)
                from sfrstca m
               where m.sfrstca_pidm = pidm_in
                 AND sfrstca_term_code = term_in
                 AND sfrstca_crn = crn_sfrstcr_crn
                 AND sfrstca_activity_date <= last_assessment_date);

    last_crn_assmt_date date;
    cursor last_assmt_date_c is
      select max(sfrstcr_activity_date) ---  NVL(sfrstcr_assess_activity_date,sfrstcr_add_date)
        FROM sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         AND sfrstcr_term_code = term_in
         AND sfrstcr_crn = crn_sfrstcr_crn;

  BEGIN
    p_print_dbms(' p processcourse begins :' || crn_sfrstcr_crn);
    open last_assmt_date_c;
    fetch last_assmt_date_c
      into last_crn_assmt_date;
    close last_assmt_date_c;
    p_print_dbms('last assment date:' ||
                 to_char(last_assessment_date, 'DD-MON-YYYY HH24:MI:SS') ||
                 ' drn last assmt: ' ||
                 to_char(last_crn_assmt_date, 'DD-MON-YYYY HH24:MI:SS'));

    -- Get start/end dates for traditional course from PTRM code, if available.
    IF crn_sfrstcr_ptrm_code IS NOT NULL THEN
      OPEN ssbsect_c;
      FETCH ssbsect_c
        INTO crn_start_date, crn_end_date, crn_schd_code, crn_insm_code;
      CLOSE ssbsect_c;
    ELSE
      OPEN sfrareg_c;
      FETCH sfrareg_c
        INTO crn_start_date,
             crn_end_date,
             crn_schd_code,
             crn_insm_code,
             crn_sfrareg_dunt_code,
             crn_status_withdraw_ind;

      /* ******************************************************** */
      /* SFRAREG rows will not exist for registrations prior to   */
      /* 6.0 install. There will always be an SFRAREG row for OL  */
      /* section. The extension number will be 0 if no extensions */
      /* applied to registration.                                 */
      /* ******************************************************** */
      IF sfrareg_c%NOTFOUND THEN
        return_status_in_out := 5;
      END IF;
      CLOSE sfrareg_c;
    END IF;

    IF return_status_in_out <> 0 THEN
      RETURN;
    END IF;

    /* ************************************************************* */
    /* Determine the liability percentage for the course up front    */
    /* based on whether or not the enrollment or registration status */
    /* means withdraw.                                               */
    /*                                                               */
    /* If the status does not mean withdraw, the student is treated  */
    /* as 100% liable for the course hours. The liability percent is */
    /* set to '1' since the percent is stored as a decimal and 1 is  */
    /* representative of the whole.                                  */
    /*                                                               */
    /* If there is a withdrawl/drop, set a boolean indicating drops  */
    /* were processed and increment the count of dropped courses.    */
    /* Will need to know if drops were involved when performing      */
    /* plateau or RBT processing later on.                           */
    /*                                                               */
    /* If the status does mean withdraw, refund by total is NOT in   */
    /* use for the term and the course is dropped during a refund    */
    /* period, the student is treated as being liable for some       */
    /* percentage of the course hours. The liability perecent is set */
    /* to the decimal for the percent. The formula used later is     */
    /* liable amount = (charge * liable %).                          */
    /*                                                               */
    /* If the status means withdraw and refund by total IS in use    */
    /* for the term, the hours for the course are set to zero. With  */
    /* refund by total, only the hours that the student is actually  */
    /* registered in matter. With refund by total, the student is    */
    /* always considered 100% liable for the hours, so when a drop   */
    /* occurs the student is in essence 100% liable for zero hours.  */
    /* This is what allows the process to yield the difference       */
    /* between the charges for previous assessment and the charges   */
    /* for the current assessment. With refund by total, the refund  */
    /* is the difference in charges between the previous assessment  */
    /* and the current assessment.                                   */
    /* ************************************************************* */
    IF crn_sfrstcr_ptrm_code IS NOT NULL THEN
      -- traditional course
      IF crn_status_withdraw_ind = 'N' THEN
        -- student not withdrawn
        p_print_dbms(' crn status withdraw ind = N');
        crn_tuit_liab_percentage := 1; -- student 100% liable
        crn_fees_liab_percentage := 1;

        --- 8.1.1 if rbt and there was a drop in variable hrs we need to set the drop
        IF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' and
           last_assessment_date is not null THEN
          -- RBT
          hrs_last_assmt := 0.0;
          open hrs_last_assmt_c;
          fetch hrs_last_assmt_c
            into hrs_last_assmt;
          close hrs_last_assmt_c;
          p_print_dbms('what is last assnt : ' || hrs_last_assmt ||
                       ' current: ' || crn_sfrstcr_bill_hr);
          --- 8.3 find the new non drop hours if its null

          p_print_dbms('check variable,  hrs last: ' || hrs_last_assmt ||
                       ' tot new non drop: ' || crn_sfrstcr_bill_hr);
          if nvl(hrs_last_assmt, 0) > nvl(crn_sfrstcr_bill_hr, 0) and
             nvl(hrs_last_assmt, 0) <> 0 and
             nvl(crn_sfrstcr_bill_hr, 0) <> 0 then
            DROPS     := TRUE;
            ANY_DROPS := TRUE;
            p_print_dbms('DROPS 1 is set to TRUE');
          end if;
        end if;

      ELSE
        -- withdrawn/drop
        p_print_dbms(' crn status withdraw ind = Y');
        /* ********************************************************* */
        /* If an intermediate assessment is being done, set the RSTS */
        /* code for drops to RE and liability to 100% to start over. */
        /* ********************************************************* */
        IF source_pgm = 'SFKFEES' THEN
          crn_sfrstcr_rsts_code    := 'RE';
          crn_tuit_liab_percentage := 1;
          crn_fees_liab_percentage := 1;

        ELSE
          IF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' THEN
            -- RBT
            -- 8.2 only count as drop if the crn was dropped since the last assessment
            if last_crn_assmt_date > last_assessment_date then
              DROPS := TRUE;
              p_print_dbms('rbt2: drops was set to true');
            else
              p_print_dbms('rbt2: drops was NOT set to true');
            end if;
            --- 8.2 need to check here for if the tuit liab is not 100%
            --  8.3.0.3 change the crn_tuit_liab_percentage to be the rbt percentage
            p_print_dbms('rbt tuit pen: ' || rbt_tuit_penalty_percent ||
                         ' rbc liab: ' || crn_tuit_liab_percentage ||
                         ' with ind: ' || crn_status_withdraw_ind);
            if rbt_tuit_penalty_percent > 0 or rbt_fees_penalty_percent > 0 then
              -- if crn_tuit_liab_percentage > 0  or
              --     crn_fees_liab_percentage > 0 then
              ANY_DROPS := TRUE;
              p_print_dbms('rbt3: any  drops was set to true ' ||
                           crn_tuit_liab_percentage);
            else
              p_print_dbms('rbt3: any drops was not  set to true ' ||
                           crn_tuit_liab_percentage);
            end if;

            crn_tuit_liab_percentage := 0; -- 0% liable for hrs
            crn_fees_liab_percentage := 0; -- 0% liable for hrs
          ELSE
            -- rbc
            --- 7.4.0.2  set the drop only if the refund percentage is something other than 0 or 100
            if (crn_tuit_refund > 0 and crn_tuit_refund < 100) OR
               (crn_fees_refund > 0 and crn_tuit_refund < 100) then

              DROPS     := TRUE;
              ANY_DROPS := TRUE;

              p_print_dbms('rbc1: drops was set to true ' ||
                           crn_tuit_refund || ' fee: ' || crn_fees_refund || ' ' ||
                           crn_sfrstcr_bill_hr);
            else

              ANY_DROPS := TRUE;

              p_print_dbms('rbc2: drops was   ' || crn_tuit_refund ||
                           ' fee: ' || crn_fees_refund || ' billhr ' ||
                           crn_sfrstcr_bill_hr);
            end if;
            crn_tuit_liab_percentage := (100 - NVL(crn_tuit_refund, 0)) / 100;
            crn_fees_liab_percentage := (100 - NVL(crn_fees_refund, 0)) / 100;
            p_print_dbms('rbc set the crn tuit liab ' ||
                         crn_tuit_liab_percentage);
          END IF;
        END IF;
      END IF;
    ELSE
      -- OLR
      /* ************************************************************ */
      /* Find appropriate refund percent for OL section. Procedure    */
      /* returns N, P or D for 'N'o refund, 'P'ercent complete or     */
      /* 'D'uration complete.                                         */
      /* ************************************************************ */
      SFKOLRL.p_determine_refund(term_in,
                                 crn_sfrstcr_crn,
                                 crn_start_date,
                                 crn_end_date,
                                 crn_sfrstcr_rsts_code,
                                 TRUNC(crn_sfrstcr_rsts_date),
                                 crn_sfrareg_dunt_code,
                                 crn_ssrrfnd_pct_complete,
                                 crn_ssrrfnd_duration_complete,
                                 crn_ssrrfnd_tuit_refund,
                                 crn_ssrrfnd_fee_refund,
                                 crn_ssrrfnd_extn_refund,
                                 crn_return_type);

      crn_tuit_liab_percentage := (100 - NVL(crn_ssrrfnd_tuit_refund, 0)) / 100;
      crn_fees_liab_percentage := (100 - NVL(crn_ssrrfnd_fee_refund, 0)) / 100;
    END IF;
    p_print_dbms('before insert into sftfees: ' || crn_sfrstcr_crn ||
                 ' tuit liab per: ' || crn_tuit_liab_percentage);
    --Insert student CRN values into temp table SFTFEES.
    INSERT INTO SFTFEES
      (SFTFEES_PIDM,
       SFTFEES_TERM_CDE,
       SFTFEES_CRN,
       SFTFEES_LEVL_CDE_CRSE,
       SFTFEES_CAMP_CDE_CRSE,
       SFTFEES_PTRM_CDE,
       SFTFEES_ADD_DATE,
       SFTFEES_RSTS_CDE,
       SFTFEES_RSTS_DATE,
       SFTFEES_ESTS_CDE,
       SFTFEES_ESTS_DATE,
       SFTFEES_REFUND_SOURCE_TABLE,
       SFTFEES_GMOD_CDE,
       SFTFEES_CRSE_START_DATE,
       SFTFEES_CRSE_END_DATE,
       SFTFEES_REG_BILL_HR,
       SFTFEES_BILL_HR_TUIT,
       SFTFEES_BILL_HR_FEES,
       SFTFEES_REG_WAIV_HR,
       SFTFEES_WAIV_HR_TUIT,
       SFTFEES_WAIV_HR_FEES,
       SFTFEES_TUIT_LIAB_PERCENTAGE,
       SFTFEES_FEES_LIAB_PERCENTAGE,
       SFTFEES_SCHD_CDE,
       SFTFEES_INSM_CDE)
    VALUES
      (pidm_in,
       term_in,
       crn_sfrstcr_crn,
       crn_sfrstcr_levl_code,
       crn_sfrstcr_camp_code,
       crn_sfrstcr_ptrm_code,
       sfbetrm_rec.sfbetrm_add_date,
       crn_sfrstcr_rsts_code,
       crn_sfrstcr_rsts_date,
       sfbetrm_rec.sfbetrm_ests_code,
       sfbetrm_rec.sfbetrm_ests_date,
       rfnd_source_table_in,
       crn_sfrstcr_gmod_code,
       crn_start_date,
       crn_end_date,
       crn_sfrstcr_bill_hr, -- crn_sfrstcr_reg_bill_hr,
       crn_sfrstcr_bill_hr,
       crn_sfrstcr_bill_hr,
       crn_sfrstcr_waiv_hr,
       crn_sfrstcr_waiv_hr,
       crn_sfrstcr_waiv_hr,
       crn_tuit_liab_percentage,
       crn_fees_liab_percentage,
       crn_schd_code,
       crn_insm_code);

    --Calculate section fees for the course and insert SFRFAUD record.
    p_sectionfees(pidm_in, term_in, source_pgm_in);

    -- Calculate extension fees for the OLR section and insert SFRFAUD record.
    -- Create the SFRFAUD record with an assessment type of 'E' for later
    -- exclusion during RBT summations. Similar to exclusion of 'P'enalties.
    IF crn_sfrstcr_ptrm_code IS NULL THEN
      p_extensionfees(pidm_in, term_in, source_pgm_in);
    END IF;
  END p_processcourse;

  /* ******************************************************************* */
  /*  Process fees for the course sections.                              */
  /* ******************************************************************* */
  PROCEDURE p_sectionfees(pidm_in       IN spriden.spriden_pidm%TYPE,
                          term_in       IN stvterm.stvterm_code%TYPE,
                          source_pgm_in IN VARCHAR2) IS
    sect_fee_detl_code    tbbdetc.tbbdetc_detail_code%TYPE := '';
    sect_fee_dcat_code    ttvdcat.ttvdcat_code%TYPE := '';
    sect_ftyp_code        ssrfees.ssrfees_ftyp_code%TYPE := '';
    sect_number_of_units  ssbsect.ssbsect_number_of_units%TYPE;
    fees_amt              ssrfees.ssrfees_amount%TYPE := 0;
    sect_fee_amt          tbraccd.tbraccd_amount%TYPE := 0;
    per_cred_fee          tbraccd.tbraccd_amount%TYPE := 0;
    per_dunt_fee          tbraccd.tbraccd_amount%TYPE := 0;
    next_seqno            sfrfaud.sfrfaud_seqno%TYPE := 0;
    note                  sfrfaud.sfrfaud_note%TYPE := '';
    lv_crn_rfnd_table     sfrfaud.sfrfaud_refund_source_table%type := crn_rfnd_table;
    tuit_refund           NUMBER(3); -- for note, whole number for %
    fees_refund           NUMBER(3); -- for note, whole number for %
    last_cred_hrs         sfrstcr.sfrstcr_credit_hr%TYPE := 0;
    clas_desc             stvclas.stvclas_desc%type;
    ssrfees_rec           ssrfees%rowtype;
    match_found           varchar2(1) := 'N';
    var_vtyp_code_current gorvisa.gorvisa_vtyp_code%TYPE := null;
    CURSOR sect_fees_c IS
      SELECT ssrfees_detl_code,
             NVL(ssrfees_ftyp_code, 'FLAT'),
             ssrfees_amount,
             tbbdetc_dcat_code,
             ssbsect_number_of_units,
             ssrfees_levl_code_stdn,
             ssrfees_coll_code,
             ssrfees_camp_code,
             ssrfees_program,
             ssrfees_degc_code,
             ssrfees_term_code_admit,
             ssrfees_rate_code_curric,
             ssrfees_styp_code_curric,
             ssrfees_lfst_code,
             ssrfees_majr_code,
             ssrfees_dept_code,
             ssrfees_prim_sec_cde,
             ssrfees_resd_code,
             ssrfees_clas_code,
             ssrfees_rate_code,
             ssrfees_styp_code,
             ssrfees_atts_code,
             ssrfees_chrt_code,
             ssrfees_vtyp_code
        FROM tbbdetc, ssbsect, ssrfees
       WHERE ssrfees_term_code = term_in
         AND ssrfees_crn = crn_sfrstcr_crn
         AND (ssrfees_levl_code = crn_sfrstcr_levl_code OR
             ssrfees_levl_code IS NULL)
         AND ssbsect_term_code = term_in
         AND ssbsect_crn = crn_sfrstcr_crn
         AND tbbdetc_detail_code = ssrfees_detl_code;

    matchcc varchar2(1) := 'N';
    cursor match_cc is
      select 'Y'
        from sotfcur
       where sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(ssrfees_rec.ssrfees_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(ssrfees_rec.ssrfees_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(ssrfees_rec.ssrfees_majr_code, nvl(sotfcur_majr_code, '@')) =
             nvl(sotfcur_majr_code, '@')
         AND NVL(ssrfees_rec.ssrfees_lfst_code, nvl(sotfcur_lfst_code, '@')) =
             nvl(sotfcur_lfst_code, '@')
         AND NVL(ssrfees_rec.ssrfees_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(ssrfees_rec.ssrfees_levl_code_stdn,
                 nvl(sotfcur_levl_code, '@')) = nvl(sotfcur_levl_code, '@')
         AND NVL(ssrfees_rec.ssrfees_coll_code, nvl(sotfcur_coll_code, '@')) =
             nvl(sotfcur_coll_code, '@')
         AND NVL(ssrfees_rec.ssrfees_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(ssrfees_rec.ssrfees_degc_code, nvl(sotfcur_degc_code, '@')) =
             nvl(sotfcur_degc_code, '@')
         AND NVL(ssrfees_rec.ssrfees_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(ssrfees_rec.ssrfees_term_code_admit,
                 nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(ssrfees_rec.ssrfees_rate_code_curric,
                 nvl(sotfcur_rate_code, '@')) = nvl(sotfcur_rate_code, '@')
         AND NVL(ssrfees_rec.ssrfees_styp_code_curric,
                 nvl(sotfcur_styp_code, '@')) = nvl(sotfcur_styp_code, '@');

    CURSOR cred_hrs_c IS
      SELECT NVL(SUM(c.sfrstca_credit_hr), 0)
        FROM sfrstca c
       WHERE c.sfrstca_pidm = pidm_in
         AND c.sfrstca_term_code = term_in
         AND c.sfrstca_crn = crn_sfrstcr_crn
         AND c.sfrstca_source_cde = 'BASE'
         AND c.sfrstca_rsts_code IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_incl_assess = 'Y'
                 AND stvrsts_withdraw_ind = 'N')
         AND c.sfrstca_seq_number =
             (SELECT MAX(z.sfrstca_seq_number)
                FROM sfrstca z
               WHERE z.sfrstca_term_code = term_in
                 AND z.sfrstca_pidm = pidm_in
                 AND z.sfrstca_source_cde = 'BASE'
                 AND z.sfrstca_crn = c.sfrstca_crn
                 AND z.sfrstca_rsts_code IN
                     (SELECT stvrsts_code
                        FROM stvrsts
                       WHERE stvrsts_incl_assess = 'Y'
                         AND stvrsts_withdraw_ind = 'N'));
  BEGIN
    OPEN sect_fees_c;
    LOOP
      FETCH sect_fees_c
        INTO sect_fee_detl_code,
             sect_ftyp_code,
             fees_amt,
             sect_fee_dcat_code,
             sect_number_of_units,
             Ssrfees_rec.ssrfees_levl_code_stdn,
             Ssrfees_rec.ssrfees_coll_code,
             Ssrfees_rec.ssrfees_camp_code,
             Ssrfees_rec.ssrfees_program,
             Ssrfees_rec.ssrfees_degc_code,
             Ssrfees_rec.ssrfees_term_code_admit,
             Ssrfees_rec.ssrfees_rate_code_curric,
             Ssrfees_rec.ssrfees_styp_code_curric,
             Ssrfees_rec.ssrfees_lfst_code,
             Ssrfees_rec.ssrfees_majr_code,
             Ssrfees_rec.ssrfees_dept_code,
             Ssrfees_rec.ssrfees_prim_sec_cde,
             Ssrfees_rec.ssrfees_resd_code,
             Ssrfees_rec.ssrfees_clas_code,
             Ssrfees_rec.ssrfees_rate_code,
             Ssrfees_rec.ssrfees_styp_code,
             Ssrfees_rec.ssrfees_atts_code,
             Ssrfees_rec.ssrfees_chrt_code,
             Ssrfees_rec.ssrfees_vtyp_code;

      IF sect_fees_c%NOTFOUND THEN
        EXIT;
      END IF;

      Match_found := 'Y';
      If Ssrfees_rec.ssrfees_levl_code_stdn is not null or
         Ssrfees_rec.ssrfees_coll_code is not null or
         Ssrfees_rec.ssrfees_camp_code is not null or
         Ssrfees_rec.ssrfees_program is not null or
         Ssrfees_rec.ssrfees_degc_code is not null or
         Ssrfees_rec.ssrfees_term_code_admit is not null or
         Ssrfees_rec.ssrfees_rate_code_curric is not null or
         Ssrfees_rec.ssrfees_styp_code_curric is not null or
         Ssrfees_rec.ssrfees_lfst_code is not null or
         Ssrfees_rec.ssrfees_majr_code is not null or
         Ssrfees_rec.ssrfees_dept_code is not null then

        match_found := 'N';
        open match_cc;
        fetch match_cc
          into match_found;
        if match_cc%notfound then
          match_found := 'N';
        end if;
        close match_cc;
        if match_found = 'N' then
          goto end_of_student_checks;
        end if;
      end if;
      --- 8.1.1 add nvl around sgbstdn rate
      if Ssrfees_rec.ssrfees_rate_code is not null and
         nvl(sgbstdn_rec.sgbstdn_rate_code, '@') <>
         Ssrfees_rec.ssrfees_rate_code then
        match_found := 'N';
        goto end_of_student_checks;
      end if;
      if Ssrfees_rec.ssrfees_styp_code is not null and
         sgbstdn_rec.sgbstdn_styp_code <> Ssrfees_rec.ssrfees_styp_code then
        match_found := 'N';
        goto end_of_student_checks;
      end if;
      if Ssrfees_rec.ssrfees_resd_code is not null and
         sgbstdn_rec.sgbstdn_resd_code <> Ssrfees_rec.ssrfees_resd_code then
        match_found := 'N';
        goto end_of_student_checks;
      end if;
      if Ssrfees_rec.ssrfees_styp_code is not null and
         sgbstdn_rec.sgbstdn_styp_code <> Ssrfees_rec.ssrfees_styp_code then
        match_found := 'N';
        goto end_of_student_checks;
      end if;
      --- 8.1.1 add nvl around clas code
      if Ssrfees_rec.ssrfees_clas_code is not null then
        if save_clas_code is null then
          SOKLIBS.p_class_calc(pidm_in,
                               save_primary_levl_code,
                               term_in,
                               TRANSLATE(sobterm_rec.sobterm_incl_attmpt_hrs_ind,
                                         'Y',
                                         'A'),
                               save_clas_code,
                               clas_desc);
          if save_clas_code is null then
            save_clas_code := ' ';
          end if;
        end if;
        if nvl(save_clas_code, '@') <> Ssrfees_rec.ssrfees_clas_code then
          match_found := 'N';
          goto end_of_student_checks;
        end if;
      end if;
      if Ssrfees_rec.ssrfees_chrt_code is not null then
        if sgksels.f_query_sgrchrt(p_pidm          => pidm_in,
                                   P_term_code_eff => term_in,
                                   P_chrt_code     => Ssrfees_rec.ssrfees_chrt_code,
                                   p_multiple_ind  => 'S') = 'N' then
          match_found := 'N';
          goto end_of_student_checks;
        end if;
      end if;
      if Ssrfees_rec.ssrfees_atts_code is not null then
        if sgksels.f_query_sgrsatt(p_pidm          => pidm_in,
                                   P_term_code_eff => term_in,
                                   P_atts_code     => Ssrfees_rec.ssrfees_atts_code,
                                   p_multiple_ind  => 'S') = 'N' then
          match_found := 'N';
          goto end_of_student_checks;
        end if;
      end if;
      if Ssrfees_rec.ssrfees_vtyp_code is not null then
        var_vtyp_code_current := gokvisa.f_check_visa(par_id         => gb_common.f_get_id(pidm_in),
                                                      par_as_of_date => sysdate);
        if nvl(var_vtyp_code_current, '@') <> Ssrfees_rec.ssrfees_vtyp_code then
          match_found := 'N';
          goto end_of_student_checks;

        end if;
      end if;
      --- end of checking ;
      lv_crn_rfnd_table := crn_rfnd_table;
      sect_fee_amt      := 0; /* reinit */

      /* ********************************************************************* */
      /* Although assessment is done based on liability, set refund percentage */
      /* variables for information use in SFRFAUD_NOTE. Multiply the liable    */
      /* percentage (decimal) amount by 100 to arrive at a whole number for    */
      /* the refund calculation. The note will show '<refund>%'.               */
      /* ********************************************************************* */
      tuit_refund := (100 - (crn_tuit_liab_percentage * 100));
      fees_refund := (100 - (crn_fees_liab_percentage * 100));

      /* ********************************************************************* */
      /* Determine section fees for detail codes by category (DCAT) code.      */
      /* ********************************************************************  */
      /* The liablity percentages for both TUI and FEE DCAT codes have already */
      /* been determined for the course. Always calculate the section fee      */
      /* amount factoring in the liability percentage.                         */
      /*                                                                       */
      /* The available SEED values for fee type are:                           */
      /*   FLAT = flat fee                                                     */
      /*   CRED = per credit hour                                              */
      /*   BILL = per billing hour                                             */
      /*   DURN = per duration unit                                            */
      /*                                                                       */
      /* If the fee type is FLAT, calculate the amount of the flat fee         */
      /* factoring in the FEE or TUI liability percentage.                     */
      /*                                                                       */
      /* If the fee type is CRED or BILL, first determine the total amount     */
      /* based on the hours * the per credit/bill charge amount. Then factor   */
      /* in the FEE or TUI liability percentage.                               */
      /*                                                                       */
      /* If the fee type is DURN, first get the total amount based on the      */
      /* number of units for the duration. Then factor in the FEE or TUI       */
      /* liability percentage.                                                 */
      /*                                                                       */
      /* For detail codes categorized as something other than TUI and FEE,     */
      /* perform the section fee calculation at face value since refunds are   */
      /* only available to categories TUI and FEE. They have 100% liability.   */
      /* ********************************************************************  */
      IF sect_fee_dcat_code = 'TUI' THEN
        IF sect_ftyp_code = 'FLAT' THEN
          /* ******************************************************** */
          /* If RBT and WD, no flat fee is generated since there is   */
          /* no percentage of liability for the drop: students are    */
          /* considered out of the class when dropping and RBT is in  */
          /* effect for the term to get the 100% refund.              */
          /* ******************************************************** */
          IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
             crn_status_withdraw_ind = 'Y') THEN
            sect_fee_amt := 0;
          ELSE
            sect_fee_amt := ROUND((fees_amt * crn_tuit_liab_percentage), 2);
          END IF;
        ELSIF sect_ftyp_code = 'CRED' THEN
          per_cred_fee := ROUND((crn_sfrstcr_credit_hr * fees_amt), 2);
          sect_fee_amt := ROUND((per_cred_fee * crn_tuit_liab_percentage),
                                2);
        ELSIF sect_ftyp_code = 'BILL' THEN
          per_cred_fee := ROUND((crn_sfrstcr_bill_hr * fees_amt), 2);
          sect_fee_amt := ROUND((per_cred_fee * crn_tuit_liab_percentage),
                                2);
        ELSE
          -- by duration
          per_dunt_fee := ROUND((sect_number_of_units * fees_amt), 2);
          sect_fee_amt := ROUND((per_dunt_fee * crn_tuit_liab_percentage),
                                2);
        END IF;
      ELSIF sect_fee_dcat_code = 'FEE' THEN
        IF sect_ftyp_code = 'FLAT' THEN
          /* ******************************************************** */
          /* If RBT and WD, no flat fee is generated since there is   */
          /* no percentage of liability for the drop: students are    */
          /* considered out of the class when dropping and RBT is in  */
          /* effect for the term to get the 100% refund.              */
          /* ******************************************************** */
          IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
             crn_status_withdraw_ind = 'Y') THEN
            sect_fee_amt := 0;
          ELSE
            sect_fee_amt := ROUND((fees_amt * crn_fees_liab_percentage), 2);
          END IF;
        ELSIF sect_ftyp_code = 'CRED' THEN
          per_cred_fee := ROUND((crn_sfrstcr_credit_hr * fees_amt), 2);
          sect_fee_amt := ROUND((per_cred_fee * crn_fees_liab_percentage),
                                2);
        ELSIF sect_ftyp_code = 'BILL' THEN
          per_cred_fee := ROUND((crn_sfrstcr_bill_hr * fees_amt), 2);
          sect_fee_amt := ROUND((per_cred_fee * crn_fees_liab_percentage),
                                2);
        ELSE
          -- by duration
          per_dunt_fee := ROUND((sect_number_of_units * fees_amt), 2);
          sect_fee_amt := ROUND((per_dunt_fee * crn_fees_liab_percentage),
                                2);
        END IF;
      ELSE
        -- handle DCATs other than TUI/FEE
        IF sect_ftyp_code = 'FLAT' THEN
          p_print_dbms('section fees, flat, crn status: ' ||
                       crn_status_withdraw_ind || ' rbt tui: ' ||
                       rbt_tuit_penalty_percent);
          IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND --DEFECT 96775
             crn_status_withdraw_ind = 'Y') THEN
            --DEFECT 96775
            IF ((rbt_fees_penalty_percent <> 0 AND --DEFECT 96775
               rbt_tuit_penalty_percent <> 0) OR
               --- 8.1.1 take into account rev nrf on soaterm
               (rbt_fees_penalty_percent = 0 and
               rbt_tuit_penalty_percent = 0 and
               sobterm_rec.SOBTERM_ASSESS_REV_NRF_IND = 'N')) THEN
              --DEFECT 96775
              sect_fee_amt := fees_amt; --DEFECT 96775
            ELSE
              --DEFECT 96775
              sect_fee_amt := 0;
            END IF; --DEFECT 96775
          ELSE
            --DEFECT 96775
            sect_fee_amt := fees_amt;
          END IF; --DEFECT 96775
        ELSIF sect_ftyp_code = 'CRED' THEN
          p_print_dbms(crn_sfrstcr_crn ||
                       'Section fees, CRED type, crn status: ' ||
                       crn_status_withdraw_ind || ' rbt tui: ' ||
                       rbt_tuit_penalty_percent || ' crn enroll: ' ||
                       crn_status_enroll_ind);
          /* ******************************************************** */
          /* Since registration sets credit hours to 0 when a course  */
          /* status code doesn't count in enrollment, assessment      */
          /* needs to determine the amount of credit hours for the    */
          /* course when the status code has count in enrollment set  */
          /* to N and the WD ind set to Y when the section fee is     */
          /* charged by credit hour. This is to make sure that the    */
          /* "non-refundable" charge calculated when the course was   */
          /* added remains on the account.                            */
          /*  credit hour charges are never refunded, reversed */
          /* ******************************************************** */
          IF (crn_status_withdraw_ind = 'Y' AND crn_status_enroll_ind = 'N') THEN
            OPEN cred_hrs_c;
            FETCH cred_hrs_c
              INTO last_cred_hrs;
            CLOSE cred_hrs_c;
            p_print_dbms('Old credit hours: ' || last_cred_hrs);
            -- reassign determined credit hours to course credit hours
            crn_sfrstcr_credit_hr := last_cred_hrs;
          END IF;
          sect_fee_amt := ROUND((crn_sfrstcr_credit_hr * fees_amt), 2);
        ELSIF sect_ftyp_code = 'BILL' THEN
          p_print_dbms(crn_sfrstcr_crn ||
                       'section fees, bill, crn status: ' ||
                       crn_status_withdraw_ind || ' rbt tui: ' ||
                       rbt_tuit_penalty_percent);
          IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND --DEFECT 96775
             crn_status_withdraw_ind = 'Y') THEN
            --DEFECT 96775
            IF ((rbt_fees_penalty_percent <> 0 AND --DEFECT 96775
               rbt_tuit_penalty_percent <> 0) OR
               --- 8.1.1 take into account rev nrf on soaterm
               (rbt_fees_penalty_percent = 0 and
               rbt_tuit_penalty_percent = 0 and
               sobterm_rec.sobterm_assess_rev_nrf_ind = 'N')) THEN
              --DEFECT 96775
              sect_fee_amt := ROUND((crn_sfrstcr_bill_hr * fees_amt), 2);
            ELSE
              --DEFECT 96775
              sect_fee_amt := 0; --DEFECT 96775
            END IF; --DEFECT 96775
          ELSE
            --DEFECT 96775
            sect_fee_amt := ROUND((crn_sfrstcr_bill_hr * fees_amt), 2); --DEFECT 96775
          END IF;
        ELSE
          -- by duration
          sect_fee_amt := ROUND((sect_number_of_units * fees_amt), 2);
        END IF;
      END IF;

      -- construct the notation for the SFRFAUD_NOTE column
      note := g$_nls.get('SFKFEE1-0002',
                         'SQL',
                         'Section Fee:  %01%',
                         sect_fee_dcat_code);
      IF sect_ftyp_code = 'FLAT' THEN
        note := note || g$_nls.get('SFKFEE1-0003',
                                   'SQL',
                                   ', flat fee amount %01%, ',
                                   fees_amt);
      ELSIF sect_ftyp_code = 'CRED' THEN
        note := note || g$_nls.get('SFKFEE1-0004',
                                   'SQL',
                                   ', per credit hour, %01%   hours at %02% per hour',
                                   crn_sfrstcr_credit_hr,
                                   fees_amt);
      ELSIF sect_ftyp_code = 'BILL' THEN
        note := note || g$_nls.get('SFKFEE1-0005',
                                   'SQL',
                                   ', per bill hour, %01% hours at %02% per hour,',
                                   crn_sfrstcr_bill_hr,
                                   fees_amt);
      ELSE
        -- DURN
        note := note || g$_nls.get('SFKFEE1-0006',
                                   'SQL',
                                   ', by duration unit %01% units at %02%',
                                   sect_number_of_units,
                                   fees_amt);
      END IF;

      -- append the kind of refund and the percent used
      IF sect_fee_dcat_code = 'TUI' THEN
        note := note || g$_nls.get('SFKFEE1-0007',
                                   'SQL',
                                   ' %01% %  refund',
                                   tuit_refund);
      ELSIF sect_fee_dcat_code = 'FEE' THEN
        note := note || g$_nls.get('SFKFEE1-0008',
                                   'SQL',
                                   ' %01% %  refund',
                                   fees_refund);
      ELSE
        IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND --DEFECT 96775
           crn_status_withdraw_ind = 'Y') THEN
          --DEFECT 96775
          note := note || g$_nls.get('SFKFEE1-0009', 'SQL', ' refund'); --DEFECT 96775
        ELSE
          --DEFECT 96775
          note := note || g$_nls.get('SFKFEE1-0010', 'SQL', ' no refund');
        END IF; --DEFECT 96775
      END IF;

      -- determine next seqno for the audit record and insert it
      next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                   term_in,
                                                   save_sessionid);

      p_print_dbms('p_insert_sfrfaud 2');

      p_insert_sfrfaud(pidm_in,
                       term_in,
                       next_seqno,
                       'A',
                       sect_fee_dcat_code,
                       sect_fee_detl_code,
                       sect_fee_amt,
                       'N', -- ABC: N/A for section fees
                       source_pgm_in,
                       NULL, -- no rules for section fees
                       NULL, -- no rules for section fees
                       crn_sfrstcr_crn,
                       crn_sfrstcr_rsts_code,
                       crn_sfrstcr_rsts_date,
                       NULL, -- no ests_code for section fees
                       NULL, -- no ests_date for section fees
                       lv_crn_rfnd_table,
                       crn_sfrstcr_bill_hr,
                       crn_sfrstcr_waiv_hr, -- 99226
                       NULL, -- no rule cred hours for section fee
                       NULL, -- no rule stud hours for section fee
                       NULL, -- no per cred charge for section fees
                       NULL, -- no flat fee rule for section fees
                       NULL, -- no overload hrs for section fees
                       NULL, -- no crse overload charge for section fees
                       NULL, -- TBRACCD tran no
                       note,
                       crn_sfrstcr_credit_hr,
                       -- RPE 35999: following added to store rule calc values
                       NULL, -- rule per credit charge
                       NULL, -- rule flat fee amount
                       NULL, -- rule from flat hrs
                       NULL, -- rule to flat hrs
                       NULL); -- rule course OL start hr

      --- label at end to branch if the new rules do not match the persons data
      <<end_of_student_checks>>
      null;

    END LOOP;
    CLOSE sect_fees_c;
  END p_sectionfees;

  /* ******************************************************************* */
  /*  Process fees for the course extensions                             */
  /* ******************************************************************* */
  PROCEDURE p_extensionfees(pidm_in       IN spriden.spriden_pidm%TYPE,
                            term_in       IN stvterm.stvterm_code%TYPE,
                            source_pgm_in IN VARCHAR2) IS
    extn_fee_amt       sfrareg.sfrareg_amount%TYPE := 0;
    extn_fee_detl_code tbbdetc.tbbdetc_detail_code%TYPE := '';
    extn_fee_dcat_code ttvdcat.ttvdcat_code%TYPE := '';
    extn_ftyp_code     ssrfees.ssrfees_ftyp_code%TYPE := '';
    next_seqno         sfrfaud.sfrfaud_seqno%TYPE := 0;
    note               sfrfaud.sfrfaud_note%TYPE := '';

    extn_ssrrfnd_pct_complete      ssrrfnd.ssrrfnd_refund_pct_complete%TYPE := 0;
    extn_ssrrfnd_duration_complete ssrrfnd.ssrrfnd_duration_complete%TYPE := 0.0;
    extn_ssrrfnd_tuit_refund       ssrrfnd.ssrrfnd_tuition_refund_pct%TYPE := 0;
    extn_ssrrfnd_fee_refund        ssrrfnd.ssrrfnd_fee_refund_pct%TYPE := 0;
    extn_ssrrfnd_extn_refund       ssrrfnd.ssrrfnd_extension_refund_pct%TYPE := 0;
    extn_return_type               VARCHAR2(1) := '';

    tuit_refund NUMBER(3); -- for note, whole number for %
    fees_refund NUMBER(3); -- for note, whole number for %

    CURSOR extn_fees_c IS
      SELECT ROWID,
             sfrareg_rsts_code,
             sfrareg_start_date,
             sfrareg_completion_date,
             sfrareg_detl_code,
             sfrareg_dunt_code,
             sfrareg_amount,
             sfrareg_fee_waiver_ind,
             sfrareg_rsts_date
        FROM sfrareg
       WHERE sfrareg_term_code = term_in
         AND sfrareg_pidm = pidm_in
         AND sfrareg_crn = crn_sfrstcr_crn
         AND sfrareg_extension_number > 0 -- don't want header rec
       ORDER BY sfrareg_extension_number DESC;

    extn_fees_rec extn_fees_c%ROWTYPE;

  BEGIN
    FOR extn_fees_rec IN extn_fees_c LOOP
      IF f_rsts_include_in_assess(extn_fees_rec.sfrareg_rsts_code) THEN
        /* ************************************************************ */
        /* If extension fee is waived, set fee to zero for assessment.  */
        /* Otherwise, go on to determine if there is a refund.          */
        /* The sfrareg_amount is the fee that needs to be charged.      */
        /* ************************************************************ */
        IF extn_fees_rec.sfrareg_fee_waiver_ind = 'Y' THEN
          extn_fee_amt := 0;
        ELSE
          -- calc the extension charge
          -- Find the appropriate refund percent for the OLR extension.
          IF crn_sfrstcr_ptrm_code IS NULL THEN
            SFKOLRL.p_determine_refund(term_in,
                                       crn_sfrstcr_crn,
                                       extn_fees_rec.sfrareg_start_date,
                                       extn_fees_rec.sfrareg_completion_date,
                                       extn_fees_rec.sfrareg_rsts_code,
                                       TRUNC(extn_fees_rec.sfrareg_rsts_date),
                                       extn_fees_rec.sfrareg_dunt_code,
                                       extn_ssrrfnd_pct_complete,
                                       extn_ssrrfnd_duration_complete,
                                       extn_ssrrfnd_tuit_refund,
                                       extn_ssrrfnd_fee_refund,
                                       extn_ssrrfnd_extn_refund, -- whole number for % refund
                                       extn_return_type);

            -- Set the tuit/fees refund for OL extension based on the returned
            -- extension refund determined by p_determine_refund.
            crn_tuit_liab_percentage := (100 -
                                        NVL(extn_ssrrfnd_extn_refund, 0)) / 100;
            crn_fees_liab_percentage := (100 -
                                        NVL(extn_ssrrfnd_extn_refund, 0)) / 100;
          END IF;
        END IF;

        /* ********************************************************************* */
        /* Although assessment is done based on liability, set refund percentage */
        /* variables for information use in SFRFAUD_NOTE. Multiply the liable    */
        /* percentage (decimal) amount by 100 to arrive at a whole number for    */
        /* the refund calculation. The note will show '<refund>%'.               */
        /* ********************************************************************* */
        tuit_refund := (100 - (crn_tuit_liab_percentage * 100));
        fees_refund := (100 - (crn_fees_liab_percentage * 100));

        /* ********************************************************************* */
        /* The liablity percentages for both TUI and FEE DCAT codes have already */
        /* been determined for the course. Always calculate the extension fee    */
        /* amount factoring in the liability percentage.                         */
        /*                                                                       */
        /* For an extension the amount has already been calculated based on      */
        /* ftyp code in the ssrextn rule used to create sfrareg, so treat it     */
        /* like a flat fee                                                       */
        /* ********************************************************************  */
        extn_fee_dcat_code := f_get_tbbdetc_dcat(extn_fees_rec.sfrareg_detl_code);

        /* ********************************************************************  */
        /* Default SFRAREG_AMOUNT to 0 if it's NULL in the record.               */
        /* ********************************************************************  */
        IF extn_fee_dcat_code = 'TUI' THEN
          extn_fee_amt := ROUND((NVL(extn_fees_rec.sfrareg_amount, 0) *
                                crn_tuit_liab_percentage),
                                2);
        ELSIF extn_fee_dcat_code = 'FEE' THEN
          extn_fee_amt := ROUND((NVL(extn_fees_rec.sfrareg_amount, 0) *
                                crn_fees_liab_percentage),
                                2);
        END IF;

        /* ********************************************************************  */
        /* Create the audit record for the extension regardless of the amount.   */
        /* This will show that the extension was processed even if the charge    */
        /* arrived at is zero.                                                   */
        /* ********************************************************************  */
        -- construct the notation for the SFRFAUD_NOTE column
        note := g$_nls.get('SFKFEE1-0011',
                           'SQL',
                           'Extension Fee: %01%; amount: %02%, ',
                           extn_fee_dcat_code,
                           extn_fee_amt);
        IF extn_fee_dcat_code = 'TUI' THEN
          note := g$_nls.get('SFKFEE1-0012',
                             'SQL',
                             '%01% %02% % refund',
                             note,
                             tuit_refund);
        ELSE
          note := g$_nls.get('SFKFEE1-0013',
                             'SQL',
                             '%01% %02% % refund',
                             note,
                             fees_refund);
        END IF;

        -- determine next seqno for the audit record and insert it
        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        p_print_dbms('p_insert_sfrfaud 3');

        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'E',
                         extn_fee_dcat_code,
                         extn_fees_rec.sfrareg_detl_code,
                         extn_fee_amt,
                         'N', -- ABC: N/A for extn fees
                         source_pgm_in,
                         NULL, -- no rules for extn fees
                         NULL, -- no rules for extn fees
                         crn_sfrstcr_crn,
                         extn_fees_rec.sfrareg_rsts_code, -- crn_sfrstcr_rsts_code,
                         extn_fees_rec.sfrareg_rsts_date, -- crn_sfrstcr_rsts_date,
                         NULL, -- no ests_code for extn fees
                         NULL, -- no ests_date for extn fees
                         crn_rfnd_table,
                         crn_sfrstcr_bill_hr,
                         crn_sfrstcr_waiv_hr, -- 99226
                         NULL, -- no rule cred hours for extn fee
                         NULL, -- no rule stud hours for extn fee
                         NULL, -- no per cred charge for extn fees
                         NULL, -- no flat fee rule for extn fees
                         NULL, -- no overload hrs
                         NULL, -- no plus per cred charge for extn fees
                         NULL, -- TBRACCD tran no
                         note,
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr

        UPDATE sfrareg
           SET sfrareg_effective_date = assess_eff_date,
               sfrareg_activity_date  = SYSDATE
         WHERE rowid = extn_fees_rec.ROWID;
      END IF;
    END LOOP;
  END p_extensionfees;

  /* ******************************************************************* */
  /* Procedure to retrieve the liability data from the last assessment   */
  /* audit if a flat rule was met for the rule type.                     */
  /* ******************************************************************* */
  /* Defect 92435.                                         2/10/2004 BAG */
  /* Flat charge refunding needs to determine the flat hour data from    */
  /* the most recent flat charge rue met for the rule type (will not     */
  /* necessarily be the last assessment) as well as the amount of OL     */
  /* hours and rule liable bill hours from the last assessment data for  */
  /* the rule type.                                                      */
  /* ******************************************************************* */
  /* Defect 102188 - add/replaced parm variables                         */
  /* prev_rule_from_flat_hr_tui_in_out                                   */
  /* prev_rule_from_flat_hr_fee_in_out                                   */
  /* prev_rule_OL_start_hr_tui_in_out                                    */
  /* prev_rule_OL_start_hr_fee_in_out                                    */
  /* last_met_flat_ind_tui_in_out                                        */
  /* last_met_flat_ind_fee_in_out                                        */
  /* starting_liab_bill_hr_tui_out                                       */
  /* starting_liab_bill_hr_fee_out                                       */
  /* last_flat_assessment_date_tui                                       */
  /* last_flat_assessment_date_fee                                       */
  /* ******************************************************************* */

  PROCEDURE p_get_last_assess_flat_data(pidm_in                        IN spriden.spriden_pidm%TYPE,
                                        term_in                        IN stvterm.stvterm_code%TYPE,
                                        prev_liab_bill_hr_in_out       IN OUT sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                                        prev_rule_from_flat_hr_tui_io  IN OUT sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                        prev_rule_from_flat_hr_fee_io  IN OUT sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                        prev_rule_OL_start_hr_tui_io   IN OUT sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                        prev_rule_OL_start_hr_fee_io   IN OUT sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                        rule_type_in                   IN sfrrgfe.sfrrgfe_type%TYPE,
                                        rule_value_in                  IN sfrrgfe.sfrrgfe_type%TYPE,
                                        last_met_flat_ind_tui_in_out   IN OUT VARCHAR2,
                                        last_met_flat_ind_fee_in_out   IN OUT VARCHAR2,
                                        prev_rule_crse_waiv_ind_tui_io IN OUT sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                        prev_rule_crse_waiv_ind_fee_io IN OUT sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE,
                                        starting_liab_bill_hr_tui_out  OUT sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                                        starting_liab_bill_hr_fee_out  OUT sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                                        process_flat_from_hrs          IN sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                        process_flat_to_hrs            IN sfrrgfe.sfrrgfe_from_flat_hrs%TYPE) IS
    DD_handled_date               DATE;
    last_flat_assessment_date_tui DATE;
    last_flat_assessment_date_fee DATE;
    flat_cnt                      pls_integer := 0;
    cnt                           PLS_INTEGER := 0;
    flat_rec                      flat_hour_ranges_rec;
    multiple_flat_met_cnt         pls_integer := 0;
    to_hrs                        sfrrgfe.sfrrgfe_to_cred_hrs%type;
    CURSOR sfbetrm_c IS
      SELECT *
        FROM sfbetrm
       WHERE sfbetrm_term_code = term_in
         AND sfbetrm_pidm = pidm_in;

    /* ******************************************************************* */
    /* Defect 102188 to create new cursors that will address TUI and FEE   */
    /* for most_recent_flat_rule_c cursor.  Thus, the original cursor      */
    /* were split into two separate cursors.                               */
    /* ******************************************************************* */
    CURSOR most_recent_flat_rule_tui_c IS
      SELECT NVL(b.sfrrgfe_from_flat_hrs, 0),
             NVL(b.sfrrgfe_crse_overload_start_hr, 0),
             NVL(b.sfrrgfe_crse_waiv_ind, 'N'),
             a.sfrfaud_activity_date
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_flat_fee_amount > 0
         AND a.sfrfaud_dcat_code = 'TUI'
         AND b.sfrrgfe_term_code = term_in
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND b.sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount
         and b.sfrrgfe_from_flat_hrs = process_flat_from_hrs
            --- 8.1.1  change the flat hrs
         and decode(greatest(b.sfrrgfe_TO_FLAT_HRS, max_flat_to_hours),
                    b.sfrrgfe_to_flat_hrs,
                    max_flat_to_hours,
                    b.sfrrgfe_to_flat_hrs) = process_flat_to_hrs
            -- and b.sfrrgfe_to_flat_hrs =process_flat_to_hrs
         AND a.sfrfaud_activity_date =
             (SELECT MAX(x.sfrfaud_activity_date)
                FROM sfrrgfe y, sfrfaud x
               WHERE x.sfrfaud_pidm = pidm_in
                 AND x.sfrfaud_term_code = term_in
                 AND x.sfrfaud_dcat_code = 'TUI'
                 AND x.sfrfaud_flat_fee_amount > 0
                 AND ((x.sfrfaud_activity_date >= DD_handled_date) OR
                     DD_handled_date IS NULL)
                 AND y.sfrrgfe_term_code = term_in
                 AND y.sfrrgfe_type = rule_type_in
                 AND ((rule_type_in = CAMPUS_TYPE AND
                     y.sfrrgfe_camp_code_crse = rule_value_in) OR
                     (rule_type_in = LEVEL_TYPE AND
                     y.sfrrgfe_levl_code_crse = rule_value_in) OR
                     (rule_type_in = ATTR_TYPE AND
                     y.sfrrgfe_attr_code_crse = rule_value_in) OR
                     (rule_type_in = STUDENT_TYPE))
                 AND y.sfrrgfe_type = x.sfrfaud_rgfe_type
                 AND y.sfrrgfe_seqno = x.sfrfaud_rgfe_seqno
                 AND y.sfrrgfe_flat_fee_amount = x.sfrfaud_flat_fee_amount
                 and y.sfrrgfe_from_flat_hrs = process_flat_from_hrs
                    --- 8.1.1  change the flat hrs
                 and decode(greatest(y.sfrrgfe_TO_FLAT_HRS,
                                     max_flat_to_hours),
                            y.sfrrgfe_to_flat_hrs,
                            max_flat_to_hours,
                            y. sfrrgfe_to_flat_hrs) = process_flat_to_hrs);
    ---    and y.sfrrgfe_to_flat_hrs = process_flat_to_hrs );

    CURSOR most_recent_flat_rule_fee_c IS
      SELECT NVL(b.sfrrgfe_from_flat_hrs, 0),
             NVL(b.sfrrgfe_crse_overload_start_hr, 0),
             NVL(b.sfrrgfe_crse_waiv_ind, 'N'),
             a.sfrfaud_activity_date
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_dcat_code = 'FEE'
         AND a.sfrfaud_flat_fee_amount > 0
         AND b.sfrrgfe_term_code = term_in
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND b.sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount
         and b.sfrrgfe_from_flat_hrs = process_flat_from_hrs
            --- 8.1.1  change the flat hrs
         and decode(greatest(b.sfrrgfe_TO_FLAT_HRS, max_flat_to_hours),
                    b.sfrrgfe_to_flat_hrs,
                    max_flat_to_hours,
                    b.sfrrgfe_to_flat_hrs) = process_flat_to_hrs
            -- and b.sfrrgfe_to_flat_hrs =process_flat_to_hrs
         AND a.sfrfaud_activity_date =
             (SELECT MAX(x.sfrfaud_activity_date)
                FROM sfrrgfe y, sfrfaud x
               WHERE x.sfrfaud_pidm = pidm_in
                 AND x.sfrfaud_term_code = term_in
                 AND x.sfrfaud_dcat_code = 'FEE'
                 AND x.sfrfaud_flat_fee_amount > 0
                 AND ((x.sfrfaud_activity_date >= DD_handled_date) OR
                     DD_handled_date IS NULL)
                 AND y.sfrrgfe_term_code = term_in
                 AND y.sfrrgfe_type = rule_type_in
                 AND ((rule_type_in = CAMPUS_TYPE AND
                     y.sfrrgfe_camp_code_crse = rule_value_in) OR
                     (rule_type_in = LEVEL_TYPE AND
                     y.sfrrgfe_levl_code_crse = rule_value_in) OR
                     (rule_type_in = ATTR_TYPE AND
                     y.sfrrgfe_attr_code_crse = rule_value_in) OR
                     (rule_type_in = STUDENT_TYPE))
                 AND y.sfrrgfe_type = x.sfrfaud_rgfe_type
                 AND y.sfrrgfe_seqno = x.sfrfaud_rgfe_seqno
                 AND y.sfrrgfe_flat_fee_amount = x.sfrfaud_flat_fee_amount
                 and y.sfrrgfe_from_flat_hrs = process_flat_from_hrs
                    --- 8.1.1 change the flat hrs
                 and decode(greatest(y.sfrrgfe_TO_FLAT_HRS,
                                     max_flat_to_hours),
                            y.sfrrgfe_to_flat_hrs,
                            max_flat_to_hours,
                            y. sfrrgfe_to_flat_hrs) = process_flat_to_hrs);
    ---  and y.sfrrgfe_to_flat_hrs = process_flat_to_hrs);

    /* ******************************************************************* */
    /* Defect 102188 to create new cursors that will address TUI and FEE   */
    /* for most_recent_OL_rule_c cursor.  Thus, the original cursor         */
    /* were split into two separate cursors.                               */
    /* ******************************************************************* */

    CURSOR most_recent_OL_rule_tui_c IS
      SELECT NVL(b.sfrrgfe_crse_overload_start_hr, 0)
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_dcat_code = 'TUI'
         AND a.sfrfaud_flat_fee_amount = 0
         AND b.sfrrgfe_term_code = term_in
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND b.sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount
         and b.sfrrgfe_from_flat_hrs = process_flat_from_hrs
            --- 8.1.1  change the flat hrs
         and decode(greatest(b.sfrrgfe_TO_FLAT_HRS, max_flat_to_hours),
                    b.sfrrgfe_to_flat_hrs,
                    max_flat_to_hours,
                    b.sfrrgfe_to_flat_hrs) = process_flat_to_hrs
            -- and b.sfrrgfe_to_flat_hrs =process_flat_to_hrs
         AND a.sfrfaud_activity_date =
             (SELECT MAX(x.sfrfaud_activity_date)
                FROM sfrrgfe y, sfrfaud x
               WHERE x.sfrfaud_pidm = pidm_in
                 AND x.sfrfaud_term_code = term_in
                 AND x.sfrfaud_dcat_code = 'TUI'
                 AND x.sfrfaud_flat_fee_amount = 0
                 AND ((x.sfrfaud_activity_date >= DD_handled_date) OR
                     DD_handled_date IS NULL)
                 AND y.sfrrgfe_term_code = term_in
                 AND y.sfrrgfe_type = rule_type_in
                 AND ((rule_type_in = CAMPUS_TYPE AND
                     y.sfrrgfe_camp_code_crse = rule_value_in) OR
                     (rule_type_in = LEVEL_TYPE AND
                     y.sfrrgfe_levl_code_crse = rule_value_in) OR
                     (rule_type_in = ATTR_TYPE AND
                     y.sfrrgfe_attr_code_crse = rule_value_in) OR
                     (rule_type_in = STUDENT_TYPE))
                 AND y.sfrrgfe_type = x.sfrfaud_rgfe_type
                 AND y.sfrrgfe_seqno = x.sfrfaud_rgfe_seqno
                 AND y.sfrrgfe_flat_fee_amount = x.sfrfaud_flat_fee_amount
                 and y.sfrrgfe_from_flat_hrs = process_flat_from_hrs
                    --- 8.1.1 change the flat hrs
                 and decode(greatest(y.sfrrgfe_TO_FLAT_HRS,
                                     max_flat_to_hours),
                            y.sfrrgfe_to_flat_hrs,
                            max_flat_to_hours,
                            y. sfrrgfe_to_flat_hrs) = process_flat_to_hrs);
    ---              and y.sfrrgfe_to_flat_hrs = process_flat_to_hrs);

    CURSOR most_recent_OL_rule_fee_c IS
      SELECT NVL(b.sfrrgfe_crse_overload_start_hr, 0)
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_dcat_code = 'FEE'
         AND a.sfrfaud_flat_fee_amount = 0
         AND b.sfrrgfe_term_code = term_in
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND b.sfrrgfe_flat_fee_amount = a.sfrfaud_flat_fee_amount
         and b.sfrrgfe_from_flat_hrs = process_flat_from_hrs
            --- 8.1.1  change the flat hrs
         and decode(greatest(b.sfrrgfe_TO_FLAT_HRS, max_flat_to_hours),
                    b.sfrrgfe_to_flat_hrs,
                    max_flat_to_hours,
                    b.sfrrgfe_to_flat_hrs) = process_flat_to_hrs
            -- and b.sfrrgfe_to_flat_hrs =process_flat_to_hrs
         AND a.sfrfaud_activity_date =
             (SELECT MAX(x.sfrfaud_activity_date)
                FROM sfrrgfe y, sfrfaud x
               WHERE x.sfrfaud_pidm = pidm_in
                 AND x.sfrfaud_term_code = term_in
                 AND x.sfrfaud_dcat_code = 'FEE'
                 AND x.sfrfaud_flat_fee_amount = 0
                 AND ((x.sfrfaud_activity_date >= DD_handled_date) OR
                     DD_handled_date IS NULL)
                 AND y.sfrrgfe_term_code = term_in
                 AND y.sfrrgfe_type = rule_type_in
                 AND ((rule_type_in = CAMPUS_TYPE AND
                     y.sfrrgfe_camp_code_crse = rule_value_in) OR
                     (rule_type_in = LEVEL_TYPE AND
                     y.sfrrgfe_levl_code_crse = rule_value_in) OR
                     (rule_type_in = ATTR_TYPE AND
                     y.sfrrgfe_attr_code_crse = rule_value_in) OR
                     (rule_type_in = STUDENT_TYPE))
                 AND y.sfrrgfe_type = x.sfrfaud_rgfe_type
                 AND y.sfrrgfe_seqno = x.sfrfaud_rgfe_seqno
                 AND y.sfrrgfe_flat_fee_amount = x.sfrfaud_flat_fee_amount
                 and y.sfrrgfe_from_flat_hrs = process_flat_from_hrs
                    --- 8.1.1 change the flat hrs
                 and decode(greatest(y.sfrrgfe_TO_FLAT_HRS,
                                     max_flat_to_hours),
                            y.sfrrgfe_to_flat_hrs,
                            max_flat_to_hours,
                            y. sfrrgfe_to_flat_hrs) = process_flat_to_hrs);
    ---             and y.sfrrgfe_to_flat_hrs = process_flat_to_hrs);

    /* ************************************************************************* */
    /* Defect 102188 Replaced last_assess_data_c with two new cursors            */
    /* last_assess_data_tui_c and last_assess_data_fee_c to handle the           */
    /* starting liable bill hrs per TUI and FEE.                                 */
    /* ************************************************************************* */

    CURSOR last_assess_data_tui_flat_c IS
      SELECT NVL(a.sfrfaud_rule_liable_bill_hrs, 0)
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND b.sfrrgfe_term_code = term_in
         and b.sfrrgfe_from_flat_hrs = process_flat_from_hrs
            --- 8.1.1  change the flat hrs
         and decode(greatest(b.sfrrgfe_TO_FLAT_HRS, max_flat_to_hours),
                    b.sfrrgfe_to_flat_hrs,
                    max_flat_to_hours,
                    b.sfrrgfe_to_flat_hrs) = process_flat_to_hrs
            -- and b.sfrrgfe_to_flat_hrs =process_flat_to_hrs
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND a.sfrfaud_dcat_code = 'TUI';

    CURSOR last_assess_data_fee_flat_c IS
      SELECT NVL(a.sfrfaud_rule_liable_bill_hrs, 0)
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND b.sfrrgfe_term_code = term_in
         and b.sfrrgfe_from_flat_hrs = process_flat_from_hrs
            --- 8.1.1  change the flat hrs
         and decode(greatest(b.sfrrgfe_TO_FLAT_HRS, max_flat_to_hours),
                    b.sfrrgfe_to_flat_hrs,
                    max_flat_to_hours,
                    b.sfrrgfe_to_flat_hrs) = process_flat_to_hrs
            -- and b.sfrrgfe_to_flat_hrs =process_flat_to_hrs
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND a.sfrfaud_dcat_code = 'FEE'
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno;

    ---- 8.0.1  cursors to find the previous assessment for the per credit if flat doesnt exists
    CURSOR last_assess_data_tui_noflat_c IS
      SELECT NVL(a.sfrfaud_rule_liable_bill_hrs, 0), b.SFRRGFE_TO_CRED_HRS
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND b.sfrrgfe_term_code = term_in
         and ((process_flat_from_hrs - nvl(b.SFRRGFE_TO_CRED_HRS, 0)) between 0 and 1)
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND a.sfrfaud_dcat_code = 'TUI';

    CURSOR last_assess_data_fee_noflat_c IS
      SELECT NVL(a.sfrfaud_rule_liable_bill_hrs, 0), b.SFRRGFE_TO_CRED_HRS
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND b.sfrrgfe_term_code = term_in
         and ((process_flat_from_hrs - nvl(b.SFRRGFE_TO_CRED_HRS, 0)) between 0 and 1)
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND a.sfrfaud_dcat_code = 'FEE'
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno;

    ---- cursors to find last assessment for intermediate
    CURSOR last_assess_data_tui_c IS
      SELECT NVL(a.sfrfaud_rule_liable_bill_hrs, 0)
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND b.sfrrgfe_term_code = term_in
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno
         AND a.sfrfaud_dcat_code = 'TUI';

    CURSOR last_assess_data_fee_c IS
      SELECT NVL(a.sfrfaud_rule_liable_bill_hrs, 0)
        FROM sfrrgfe b, sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND b.sfrrgfe_term_code = term_in
         AND b.sfrrgfe_type = rule_type_in
         AND ((rule_type_in = CAMPUS_TYPE AND
             b.sfrrgfe_camp_code_crse = rule_value_in) OR
             (rule_type_in = LEVEL_TYPE AND
             b.sfrrgfe_levl_code_crse = rule_value_in) OR
             (rule_type_in = ATTR_TYPE AND
             b.sfrrgfe_attr_code_crse = rule_value_in) OR
             (rule_type_in = STUDENT_TYPE))
         AND b.sfrrgfe_type = a.sfrfaud_rgfe_type
         AND a.sfrfaud_dcat_code = 'FEE'
         AND b.sfrrgfe_seqno = a.sfrfaud_rgfe_seqno;

  BEGIN
    DD_handled_date := ME_SFKFEES.f_get_DD_handled_date(pidm_in, term_in);

    p_print_dbms('Start p_get_last_assess_flat_data, dd handled date: ' ||
                 DD_handled_date || '  Last assessment date: ' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DDMMYYYY HH24MISS'));
    p_print_dbms('rule type: ' || rule_type_in || ' value: ' ||
                 rule_value_in);

    if SWAP_RBC_MORE_DROPPED then
      p_print_dbms('Get last assessment for flat SWAP RBC more dropped is true');
    end if;
    /* ******************************************************************* */
    /* Defect 102087.                                           3/2005 BAG */
    /* ******************************************************************* */
    /* Requery in SFBETRM record in case IA updated it.                    */
    /* IA = Intermediate Assessment built to process DD/NCIA RSTS codes    */
    /* NCIA = Not Count In Assessment                                      */
    /* ******************************************************************* */
    OPEN sfbetrm_c;
    FETCH sfbetrm_c
      INTO sfbetrm_rec;
    CLOSE sfbetrm_c;

    p_print_dbms('Last assessment date: ' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DDMMYYYY HH24:MI:SS') ||
                 ' should it really be this: ' ||
                 to_char(last_assessment_date, 'DDMMYYYY HH24:MI:SS'));

    /* ******************************************************************* */
    /* 102188 to create 2 cursors for most_recent_flat_rule_c the first    */
    /* will be for TUI.                                                    */
    /* ******************************************************************* */

    OPEN most_recent_flat_rule_tui_c;
    FETCH most_recent_flat_rule_tui_c
      INTO prev_rule_from_flat_hr_tui_io,
           prev_rule_OL_start_hr_tui_io,
           prev_rule_crse_waiv_ind_tui_io,
           last_flat_assessment_date_tui;
    p_print_dbms('last flat assesment date tui: ' ||
                 to_char(last_flat_assessment_date_tui,
                         'DDMMYYYY HH24MISS'));
    IF most_recent_flat_rule_tui_c%FOUND THEN
      IF prev_rule_OL_start_hr_tui_io = 0 THEN
        -- check for separate OL charge rule
        OPEN most_recent_OL_rule_tui_c;
        FETCH most_recent_OL_rule_tui_c
          INTO prev_rule_OL_start_hr_tui_io;
        IF most_recent_OL_rule_tui_c%NOTFOUND THEN
          prev_rule_OL_start_hr_tui_io := 0;
        END IF;
        CLOSE most_recent_OL_rule_tui_c;
      END IF;
    ELSE
      prev_rule_from_flat_hr_tui_io  := 0;
      prev_rule_OL_start_hr_tui_io   := 0;
      prev_rule_crse_waiv_ind_tui_io := 'N';
      last_met_flat_ind_tui_in_out   := 'N';
    END IF;
    CLOSE most_recent_flat_rule_tui_c;

    /* ******************************************************************* */
    /* If the date for the most recent flat rule being met matches the     */
    /* last stored assesment date, then set the indicator for the last     */
    /* assessment meeting a flat rule to Y.                                */
    /*                                                                     */
    /* This indicator is used in flat charge refunding to signify if the   */
    /* hours liability calculations need to determine what portion of any  */
    /* dropped hours are "free" or in the flat range.                      */
    /* If the indicator is set to N, then all dropped hours qualify for    */
    /* conventional liability processing; none can be "free" since the     */
    /* last assessment did not meet a flat charge rule.                    */
    /* ******************************************************************* */
    /* Pam */
    IF (last_flat_assessment_date_tui = sfbetrm_rec.sfbetrm_assessment_date) THEN
      last_met_flat_ind_tui_in_out := 'Y';
    END IF;

    /* ******************************************************************* */
    /* 102188 to create 2 cursors for most_recent_flat_rule_c the 2nd      */
    /* will be for FEE.                                                    */
    /* ******************************************************************* */
    OPEN most_recent_flat_rule_fee_c;
    FETCH most_recent_flat_rule_fee_c
      INTO prev_rule_from_flat_hr_fee_io,
           prev_rule_OL_start_hr_fee_io,
           prev_rule_crse_waiv_ind_fee_io,
           last_flat_assessment_date_fee;
    IF most_recent_flat_rule_fee_c%FOUND THEN
      IF prev_rule_OL_start_hr_fee_io = 0 THEN
        -- check for separate OL charge rule

        OPEN most_recent_OL_rule_fee_c;
        FETCH most_recent_OL_rule_fee_c
          INTO prev_rule_OL_start_hr_fee_io;
        IF most_recent_OL_rule_fee_c%NOTFOUND THEN
          prev_rule_OL_start_hr_fee_io := 0;
        END IF;
        CLOSE most_recent_OL_rule_fee_c;
      END IF;
    ELSE
      prev_rule_from_flat_hr_fee_io  := 0;
      prev_rule_OL_start_hr_fee_io   := 0;
      prev_rule_crse_waiv_ind_fee_io := 'N';
      last_met_flat_ind_fee_in_out   := 'N';
    END IF;
    CLOSE most_recent_flat_rule_fee_c;

    /* ******************************************************************* */
    /* If the date for the most recent flat rule being met matches the     */
    /* last stored assesment date, then set the indicator for the last     */
    /* assessment meeting a flat rule to Y.                                */
    /*                                                                     */
    /* This indicator is used in flat charge refunding to signify if the   */
    /* hours liability calculations need to determine what portion of any  */
    /* dropped hours are "free" or in the flat range.                      */
    /* If the indicator is set to N, then all dropped hours qualify for    */
    /* conventional liability processing; none can be "free" since the     */
    /* last assessment did not meet a flat charge rule.                    */
    /* ******************************************************************* */
    IF (last_flat_assessment_date_fee = sfbetrm_rec.sfbetrm_assessment_date) THEN
      last_met_flat_ind_fee_in_out := 'Y';
    END IF;

    /* ******************************************************************* */
    /* Defect 102188 to replace last_assess_data_c with 2 cursors, one for */
    /* TUI and the other for FEE.                                          */
    /* ******************************************************************* */
    if (LAST_ASSESSMENT_HANDLED_DD) then
      OPEN last_assess_data_tui_c;
      FETCH last_assess_data_tui_c
        INTO starting_liab_bill_hr_tui_out;
      IF last_assess_data_tui_c%NOTFOUND THEN
        starting_liab_bill_hr_tui_out := 0;
      END IF;
      CLOSE last_assess_data_tui_c;
      p_print_dbms('starting last ass from last_assess_data_tui_c from intermediate: ' ||
                   starting_liab_bill_hr_tui_out);
      OPEN last_assess_data_fee_c;
      FETCH last_assess_data_fee_c
        INTO starting_liab_bill_hr_fee_out;
      IF last_assess_data_fee_c%NOTFOUND THEN
        starting_liab_bill_hr_fee_out := 0;
      END IF;
      CLOSE last_assess_data_fee_c;
    else
      OPEN last_assess_data_tui_flat_c;
      FETCH last_assess_data_tui_flat_c
        INTO starting_liab_bill_hr_tui_out;
      p_print_dbms('flat doesnt exists in prev assessment, find corresponding per credit: ' ||
                   starting_liab_bill_hr_tui_out);
      IF last_assess_data_tui_flat_c%NOTFOUND THEN
        flat_cnt := 0;
        OPEN last_assess_data_tui_noflat_c;
        loop
          fetch last_assess_data_tui_noflat_c
            into starting_liab_bill_hr_tui_out, to_hrs;
          p_print_dbms('fetch from last assess data tui noflat: ' ||
                       starting_liab_bill_hr_tui_out);
          exit when last_assess_data_tui_noflat_c%notfound;
          multiple_flat_met_cnt := 0;
          cnt                   := 0;
          FOR cnt in 1 .. flat_ranges.count loop
            flat_rec := flat_ranges(cnt);
            if (flat_rec.r_flat_from_hrs - to_hrs) > 0 and
               (flat_rec.r_flat_from_hrs - to_hrs) <= 1 THEN
              multiple_flat_met_cnt := multiple_flat_met_cnt + 1;
            end if;
          end loop;
          if multiple_flat_met_cnt > 1 then
            starting_liab_bill_hr_tui_out := 0;
            p_print_dbms('per credit matches multiple flats, so do not include as starting liab: ' ||
                         starting_liab_bill_hr_tui_out);
          else
            exit; --- use first liab hrs we find that
          end if;
        end loop;
        close last_assess_data_tui_noflat_c;

      END IF;
      CLOSE last_assess_data_tui_flat_c;
      p_print_dbms('starting last ass from last_assess_data_tui_c from flat: ' ||
                   starting_liab_bill_hr_tui_out);
      OPEN last_assess_data_fee_flat_c;
      FETCH last_assess_data_fee_flat_c
        INTO starting_liab_bill_hr_fee_out;
      IF last_assess_data_fee_flat_c%NOTFOUND THEN
        flat_cnt := 0;
        OPEN last_assess_data_fee_noflat_c;
        loop
          fetch last_assess_data_fee_noflat_c
            into starting_liab_bill_hr_fee_out, to_hrs;
          exit when last_assess_data_fee_noflat_c%notfound;
          multiple_flat_met_cnt := 0;
          cnt                   := 0;
          FOR cnt in 1 .. flat_ranges.count loop
            flat_rec := flat_ranges(cnt);
            if (flat_rec.r_flat_from_hrs - to_hrs) > 0 and
               (flat_rec.r_flat_from_hrs - to_hrs) <= 1 THEN
              multiple_flat_met_cnt := multiple_flat_met_cnt + 1;
            end if;
          end loop;
          if multiple_flat_met_cnt > 1 then
            starting_liab_bill_hr_fee_out := 0;
            p_print_dbms('per credit matches multiple flats, so do not include as starting liab: ' ||
                         starting_liab_bill_hr_fee_out);
          else
            exit; --- use first liab hrs we find that
          end if;
        end loop;
        close last_assess_data_fee_noflat_c;
      END IF;
      CLOSE last_assess_data_fee_flat_c;
    end if;

    p_print_dbms('==last_met_flat_ind_tui_in_out, last_met_flat_ind_fee_in_out : ' ||
                 last_met_flat_ind_tui_in_out || ':' ||
                 last_met_flat_ind_fee_in_out);
    p_print_dbms('End p_get_assess_flat_data');
  END p_get_last_assess_flat_data;

  /* ******************************************************************* */
  /* Calculate course hour liability when the assessment involves drops  */
  /* and previous flat hour rule qualification.                          */
  /* ******************************************************************* */
  /* 102188 to add/replace parm vars:                                    */
  /* prev_rule_from_flat_hr_tui_in                                       */
  /* prev_rule_OL_start_hr_tui_in                                         */
  /* last_met_flat_rule_ind_tui_in                                       */
  /* prev_rule_from_flat_hr_fee_in                                       */
  /* prev_rule_OL_start_hr_fee_in                                         */
  /* last_met_flat_rule_ind_fee_in                                       */
  /* tui_nondrop_bill_hr_in_out                                          */
  /* tui_nondrop_waiv_hr_in_out                                          */
  /* fee_nondrop_bill_hr_in_out                                          */
  /* fee_nondrop_waiv_hr_in_out                                          */
  /* ******************************************************************* */
  PROCEDURE p_calc_flat_hr_liability(pidm_in IN spriden.spriden_pidm%TYPE,
                                     term_in IN stvterm.stvterm_code%TYPE,

                                     tui_nondrop_bill_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     tui_nondrop_waiv_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     fee_nondrop_bill_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     fee_nondrop_waiv_hr_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,

                                     liab_OL_bill_hr_tuit_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_waiv_hr_tuit_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_bill_hr_tuit_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_waiv_hr_tuit_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_bill_hr_tuit_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_waiv_hr_tuit_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_bill_hr_fees_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_OL_waiv_hr_fees_in_out   IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_bill_hr_fees_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_flat_waiv_hr_fees_in_out IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_bill_hr_fees_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     liab_waiv_hr_fees_in_out      IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                     prev_rule_from_flat_hr_tui_in IN sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                     prev_rule_OL_start_hr_tui_in  IN sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                     last_met_flat_rule_ind_tui_in IN VARCHAR2,
                                     prev_rule_from_flat_hr_fee_in IN sfrrgfe.sfrrgfe_from_flat_hrs%TYPE,
                                     prev_rule_OL_start_hr_fee_in  IN sfrrgfe.sfrrgfe_crse_overload_start_hr%TYPE,
                                     last_met_flat_rule_ind_fee_in IN VARCHAR2,
                                     tui_calculated_ind_in_out     IN OUT BOOLEAN,
                                     fee_calculated_ind_in_out     IN OUT BOOLEAN,
                                     tui_flat_cnt                  IN OUT pls_integer,
                                     tui_waiv_flat_cnt             IN OUT pls_integer,
                                     fee_flat_cnt                  IN OUT pls_integer,
                                     fee_waiv_flat_cnt             IN OUT pls_integer)

   IS
    lv_liab_OL_bill_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    lv_liab_OL_bill_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    lv_liab_OL_waiv_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    lv_liab_OL_waiv_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    act_date_in_out         DATE;
    add_date_io             DATE;
    first_act_io            DATE;
    -- 105831 7.4.0.2 orig bill hrs for variable credits
    orig_crn_bill_hrs sfrstcr.sfrstcr_bill_hr%type := 0.0;
  BEGIN
    p_print_dbms('at start of p_calc_flat_hr_liab, crn: ' || t_crn);
    p_get_sfrstcr_data(pidm_in,
                       term_in,
                       act_date_in_out,
                       add_date_io,
                       first_act_io,
                       orig_crn_bill_hrs);
    /* ******************************************************************* */
    /* Only process new adds/drops since last assessment.                  */
    /* ******************************************************************* */
    p_print_dbms('act_date_in_out > sfbetrm_rec.sfbetrm_assessment_date: ' ||
                 to_char(act_date_in_out, 'DD-MON-YYYY HH24:MI:SS') || ':' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DD-MON-YYYY HH24:MI:SS') ||
                 ' last assessment ate: ' ||
                 to_char(last_assessment_date, 'DD-MON-YYYY HH24:MI:SS'));
    if (LAST_ASSESSMENT_HANDLED_DD) then
      p_print_dbms('last assessment handled a dd is true');
    else
      p_print_dbms('last assessment handled a dd is not true');
    end if;

    IF ((act_date_in_out > sfbetrm_rec.sfbetrm_assessment_date) OR
       (LAST_ASSESSMENT_HANDLED_DD)) THEN

      p_print_dbms(' INSIDE p calc flat hr to process add/drops since last assmt ');

      IF (f_rsts_means_wd(t_rsts_code)) THEN
        -- new drop

        --
        /* ******************************************************************* */
        /* Do not process the registration drop if 100% liable (no refund).    */
        /* Defect 102188                                                       */
        /* ******************************************************************* */
        --1-1BCLLA, comment the condition below.  Instead replace it with another
        --condition that will process 100% liability in RBC Swapping.
        --IF (t_tuit_liab_percentage <> 1 OR t_fees_liab_percentage <> 1) THEN

        IF (t_tuit_liab_percentage <> 1 OR t_fees_liab_percentage <> 1) OR
           ((t_tuit_liab_percentage = 1 AND t_fees_liab_percentage = 1) AND
           SWAP_RBC_MORE_DROPPED) THEN
          p_print_dbms(' Processing new DROP : ' || t_tuit_liab_percentage || ':' ||
                       t_fees_liab_percentage);

          -- determine OL hour liability
          -- 102188 to process TUI

          --1-1BCLLA
          IF (t_tuit_liab_percentage <> 1) OR
             (t_tuit_liab_percentage = 1 AND SWAP_RBC_MORE_DROPPED) THEN

            /* ******************************************************************* */
            /* Reduce the nondropped hours total by the bill hours for the drop    */
            /* to determine if the drop causes the student to fall below the 'from */
            /* flat hours' (previous plateau).                                     */
            /* 1-1BCLAA - this has been moved from the outside of the above IF     */
            /*            condition.                                               */
            /* ******************************************************************* */
            p_print_dbms('cal tui non drop bill hrs : tui_nondrop_bill_hr_in_out - t_reg_bill_hr ' ||
                         tui_nondrop_bill_hr_in_out || ' : ' ||
                         t_reg_bill_hr);
            tui_nondrop_bill_hr_in_out := tui_nondrop_bill_hr_in_out -
                                          t_reg_bill_hr;
            tui_nondrop_waiv_hr_in_out := tui_nondrop_waiv_hr_in_out -
                                          t_reg_waiv_hr;

            tui_calculated_ind_in_out := TRUE;
            p_print_dbms('prev rule OL start hr tui in: ' ||
                         prev_rule_OL_start_hr_tui_in ||
                         ' check tui_nondrop_bill_hr_in_out >= prev_rule_OL_start_hr_tui_in ' ||
                         tui_nondrop_bill_hr_in_out || ' >= ' ||
                         prev_rule_OL_start_hr_tui_in);

            IF prev_rule_OL_start_hr_tui_in > 0 THEN
              IF tui_nondrop_bill_hr_in_out >= prev_rule_OL_start_hr_tui_in THEN
                --- do not populate overload liability if there was none 7.4.0.3
                p_print_dbms('tui non drop is >= prev rle OL: ' ||
                             tui_nondrop_bill_hr_in_out || '  >= ' ||
                             prev_rule_OL_start_hr_tui_in);
                if ((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_EQUAL)) THEN
                  if ((before_swap_hours >= prev_rule_OL_start_hr_tui_in) OR
                     (before_swap_hours <= prev_rule_OL_start_hr_tui_in and
                     start_swap_hours <= prev_rule_OL_start_hr_tui_in)) then
                    lv_liab_OL_bill_hr_tuit := (t_reg_bill_hr *
                                               t_tuit_liab_percentage);
                  end if;
                else
                  lv_liab_OL_bill_hr_tuit := (t_reg_bill_hr *
                                             t_tuit_liab_percentage);
                end if;
              ELSE
                -- determine portion of dropped bill hrs having liability
                p_print_dbms('when tui_nondrop_bill_hr_in_out + t_reg_bill_hr) >= prev_rule_OL_start_hr_tui_in: ' ||
                             tui_nondrop_bill_hr_in_out || ' + ' ||
                             t_reg_bill_hr || ' >=  ' ||
                             prev_rule_OL_start_hr_tui_in);
                IF (tui_nondrop_bill_hr_in_out + t_reg_bill_hr) >=
                   prev_rule_OL_start_hr_tui_in THEN
                  --- do not populate overload liability if there was none 7.4.0.3
                  if ((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_EQUAL)) and
                     (before_swap_hours <= prev_rule_OL_start_hr_tui_in) then
                    p_print_dbms('swap condition');
                  else
                    lv_liab_OL_bill_hr_tuit := (((tui_nondrop_bill_hr_in_out +
                                               t_reg_bill_hr) -
                                               prev_rule_OL_start_hr_tui_in) *
                                               t_tuit_liab_percentage);
                    p_print_dbms('liab ol bill1a: ' ||
                                 tui_nondrop_bill_hr_in_out || ' + ' ||
                                 t_reg_bill_hr || ' - ' ||
                                 prev_rule_OL_start_hr_tui_in || ' * ' ||
                                 t_tuit_liab_percentage);
                  end if;
                END IF;
              END IF;
              liab_OL_bill_hr_tuit_in_out := liab_OL_bill_hr_tuit_in_out +
                                             lv_liab_OL_bill_hr_tuit;
              p_print_dbms('lib3: iab_OL_bill_hr_tuit_in_out: ' ||
                           liab_OL_bill_hr_tuit_in_out);
              IF tui_nondrop_waiv_hr_in_out >= prev_rule_OL_start_hr_tui_in THEN
                lv_liab_OL_waiv_hr_tuit := (t_reg_waiv_hr *
                                           t_tuit_liab_percentage);

              ELSE
                -- determine portion dropped waive hrs having liability
                IF (tui_nondrop_waiv_hr_in_out + t_reg_waiv_hr) >=
                   prev_rule_OL_start_hr_tui_in THEN
                  lv_liab_OL_waiv_hr_tuit := (((tui_nondrop_waiv_hr_in_out +
                                             t_reg_waiv_hr) -
                                             prev_rule_OL_start_hr_tui_in) *
                                             t_tuit_liab_percentage);
                END IF;
              END IF;
              liab_OL_waiv_hr_tuit_in_out := liab_OL_waiv_hr_tuit_in_out +
                                             lv_liab_OL_waiv_hr_tuit;
              p_print_dbms('liab bil hr tuit: ' ||
                           liab_OL_bill_hr_tuit_in_out || ' waiv: ' ||
                           liab_OL_waiv_hr_tuit_in_out);
            END IF;

            -- determine flat hour liability
            p_print_dbms('DROP NSIDE p_clac_flat:  liab_flat_bill_hr_tuit_in_out: ' ||
                         liab_flat_bill_hr_tuit_in_out);
            p_print_dbms('==liab_bill_hr_tuit_in_out + (t_bill_hr_tuit * t_tuit_liab_percentage) : ' ||
                         liab_bill_hr_tuit_in_out || ':' || t_bill_hr_tuit || ':' ||
                         t_tuit_liab_percentage);

            p_print_dbms('###3 prev tui non drop:  prev flat: ' ||
                         prev_rule_from_flat_hr_tui_in || ' newnondrop: ' ||
                         new_nondrop_hrs ||
                         ' tui_nondrop_bill_hr_in_out: ' ||
                         tui_nondrop_bill_hr_in_out);
            p_print_dbms('was last flat rule met? ' ||
                         last_met_flat_rule_ind_tui_in ||
                         ' prev rule flat - tuit non drop: ' ||
                         prev_rule_from_flat_hr_tui_in || ' - ' ||
                         tui_nondrop_bill_hr_in_out || '  <=  ' ||
                         t_reg_bill_hr ||
                         ' is the tuit non drop bill hr < prev rule flat: ' ||
                         tui_nondrop_bill_hr_in_out || ' <  ' ||
                         prev_rule_from_flat_hr_tui_in || ' flat cnt: ' ||
                         tui_flat_cnt);
            IF (last_met_flat_rule_ind_tui_in = 'Y') THEN
              /* ******************************************************************* */
              /* If the drop causes the student's nondropped hours to be less than   */
              /* 'the from flat hours', determine what part of the dropped hours are */
              /* "free" and what part have liability.                                */
              /* ******************************************************************* */

              IF tui_nondrop_bill_hr_in_out < prev_rule_from_flat_hr_tui_in THEN
                --- 7.4.0.1 use per credit hr formula bill hrs * liab perc if the
                --  flat minus the liab is greater than the bill hrs, and this is not an intermediate
                --  and this is the first drop from the flat
                if ((prev_rule_from_flat_hr_tui_in -
                   tui_nondrop_bill_hr_in_out) <= t_reg_bill_hr) or
                   (LAST_ASSESSMENT_HANDLED_DD) OR (tui_flat_cnt > 0) then
                  liab_flat_bill_hr_tuit_in_out := ((prev_rule_from_flat_hr_tui_in -
                                                   tui_nondrop_bill_hr_in_out) *
                                                   t_tuit_liab_percentage);
                  tui_flat_cnt                  := tui_flat_cnt + 1;
                  p_print_dbms('##2x2   liab_flat_bill_hr_tuit_in_out := ' ||
                               liab_flat_bill_hr_tuit_in_out || ' = ' ||
                               prev_rule_from_flat_hr_tui_in || '  - ' ||
                               tui_nondrop_bill_hr_in_out || ' * ' ||
                               t_tuit_liab_percentage);
                else
                  liab_bill_hr_tuit_in_out := liab_bill_hr_tuit_in_out +
                                              (t_bill_hr_tuit *
                                              t_tuit_liab_percentage);
                  p_print_dbms('##2x2a   liab_flat_bill_hr_tuit_in_out := ' ||
                               liab_flat_bill_hr_tuit_in_out || ' = ' ||
                               t_bill_hr_tuit || ' * ' ||
                               t_tuit_liab_percentage);
                end if;
              END IF;
              p_print_dbms('==liab_flat_bill_hr_tuit_in_out =' ||
                           liab_flat_bill_hr_tuit_in_out);

              IF tui_nondrop_waiv_hr_in_out < prev_rule_from_flat_hr_tui_in THEN
                -- 7.4.0.1
                if ((prev_rule_from_flat_hr_tui_in -
                   tui_nondrop_waiv_hr_in_out) <= t_reg_waiv_hr) or
                   (LAST_ASSESSMENT_HANDLED_DD) OR (tui_waiv_flat_cnt > 0) then
                  liab_flat_waiv_hr_tuit_in_out := ((prev_rule_from_flat_hr_tui_in -
                                                   tui_nondrop_waiv_hr_in_out) *
                                                   t_tuit_liab_percentage);
                  tui_waiv_flat_cnt             := tui_waiv_flat_cnt + 1;
                else
                  liab_waiv_hr_tuit_in_out := liab_waiv_hr_tuit_in_out +
                                              (t_waiv_hr_tuit *
                                              t_tuit_liab_percentage);
                end if;
              END IF;
            ELSE
              liab_bill_hr_tuit_in_out := liab_bill_hr_tuit_in_out +
                                          (t_bill_hr_tuit *
                                          t_tuit_liab_percentage);
              p_print_dbms('==liab_bill_hr_tuit_in_out in else, not a prev flat: ' ||
                           liab_bill_hr_tuit_in_out);
              liab_waiv_hr_tuit_in_out := liab_waiv_hr_tuit_in_out +
                                          (t_waiv_hr_tuit *
                                          t_tuit_liab_percentage);

            END IF;
          END IF; /* IF t_tuit_liab_percentage <> 1 */

          -- 102188 FEES HANDLED HERE
          --1-1BCLLA
          IF (t_fees_liab_percentage <> 1) OR
             (t_fees_liab_percentage = 1 AND SWAP_RBC_MORE_DROPPED) THEN
            /* ******************************************************************* */
            /* Reduce the nondropped hours total by the bill hours for the drop    */
            /* to determine if the drop causes the student to fall below the 'from */
            /* flat hours' (previous plateau).                                     */
            /* 1-1BCLAA - this has been moved from the outside of the above IF     */
            /*            condition (condition for t_tui_liab_percentage).         */
            /* ******************************************************************* */

            fee_nondrop_bill_hr_in_out := fee_nondrop_bill_hr_in_out -
                                          t_reg_bill_hr;
            fee_nondrop_waiv_hr_in_out := fee_nondrop_waiv_hr_in_out -
                                          t_reg_waiv_hr;

            fee_calculated_ind_in_out := TRUE;
            -- determine OL hour liability
            p_print_dbms('Prev OL fee: ' || prev_rule_OL_start_hr_fee_in);
            IF prev_rule_OL_start_hr_fee_in > 0 THEN
              IF fee_nondrop_bill_hr_in_out >= prev_rule_OL_start_hr_fee_in THEN
                --- do not populate overload liability if there was none 7.4.0.3
                p_print_dbms('fee non drop is >= prev rle OL: ' ||
                             fee_nondrop_bill_hr_in_out || '  >= ' ||
                             prev_rule_OL_start_hr_fee_in);
                if ((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_EQUAL)) THEN
                  if ((before_swap_hours >= prev_rule_OL_start_hr_fee_in) OR
                     (before_swap_hours <= prev_rule_OL_start_hr_fee_in and
                     start_swap_hours <= prev_rule_OL_start_hr_fee_in)) then
                    lv_liab_OL_bill_hr_fees := (t_reg_bill_hr *
                                               t_fees_liab_percentage);
                  end if;
                else
                  lv_liab_OL_bill_hr_fees := (t_reg_bill_hr *
                                             t_fees_liab_percentage);
                end if;
              ELSE
                -- determine portion of dropped bill hrs having liability
                p_print_dbms('when fee_nondrop_bill_hr_in_out + t_reg_bill_hr) >= prev_rule_OL_start_hr_fee_in: ' ||
                             fee_nondrop_bill_hr_in_out || ' + ' ||
                             t_reg_bill_hr || ' >=  ' ||
                             prev_rule_OL_start_hr_fee_in);
                IF (fee_nondrop_bill_hr_in_out + t_reg_bill_hr) >=
                   prev_rule_OL_start_hr_fee_in THEN
                  --- do not populate overload liability if there was none 7.4.0.3
                  if ((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_EQUAL)) and
                     (before_swap_hours <= prev_rule_OL_start_hr_fee_in) then
                    p_print_dbms('swap condition');
                  else
                    lv_liab_OL_bill_hr_fees := (((fee_nondrop_bill_hr_in_out +
                                               t_reg_bill_hr) -
                                               prev_rule_OL_start_hr_fee_in) *
                                               t_fees_liab_percentage);
                    p_print_dbms('liab ol bill1a: ' ||
                                 fee_nondrop_bill_hr_in_out || ' + ' ||
                                 t_reg_bill_hr || ' - ' ||
                                 prev_rule_OL_start_hr_fee_in || ' * ' ||
                                 t_fees_liab_percentage);
                  end if;
                END IF;
              END IF;
              liab_OL_bill_hr_fees_in_out := liab_OL_bill_hr_fees_in_out +
                                             lv_liab_OL_bill_hr_fees;

              IF fee_nondrop_waiv_hr_in_out >= prev_rule_OL_start_hr_fee_in THEN
                lv_liab_OL_waiv_hr_fees := (t_reg_waiv_hr *
                                           t_fees_liab_percentage);

              ELSE
                -- determine portion dropped waive hrs having liability
                IF (fee_nondrop_waiv_hr_in_out + t_reg_waiv_hr) >=
                   prev_rule_OL_start_hr_fee_in THEN
                  lv_liab_OL_waiv_hr_fees := (((fee_nondrop_waiv_hr_in_out +
                                             t_reg_waiv_hr) -
                                             prev_rule_OL_start_hr_fee_in) *
                                             t_fees_liab_percentage);
                END IF;
              END IF;
              liab_OL_waiv_hr_fees_in_out := liab_OL_waiv_hr_fees_in_out +
                                             lv_liab_OL_waiv_hr_fees;
            END IF;

            -- determine flat hour liability
            p_print_dbms('INSIDE p_clac_flat liab_flat_bill_hr_fees_in_out');
            p_print_dbms('==liab_bill_hr_fees_in_out + (t_bill_hr_fees * t_fees_liab_percentage) : ' ||
                         liab_bill_hr_fees_in_out || ':' || t_bill_hr_fees || ':' ||
                         t_fees_liab_percentage);
            p_print_dbms('==t_fees_liab_percentage: ' ||
                         t_fees_liab_percentage);

            IF last_met_flat_rule_ind_fee_in = 'Y' THEN
              /* ******************************************************************* */
              /* If the drop causes the student's nondropped hours to be less than   */
              /* 'the from flat hours', determine what part of the dropped hours are */
              /* "free" and what part have liability.                                */
              /* ******************************************************************* */
              IF fee_nondrop_bill_hr_in_out < prev_rule_from_flat_hr_fee_in THEN
                --- 7.4.0.1
                if ((prev_rule_from_flat_hr_fee_in -
                   fee_nondrop_bill_hr_in_out) <= t_bill_hr_fees) or
                   (LAST_ASSESSMENT_HANDLED_DD) OR fee_flat_cnt > 0 then
                  liab_flat_bill_hr_fees_in_out := ((prev_rule_from_flat_hr_fee_in -
                                                   fee_nondrop_bill_hr_in_out) *
                                                   t_fees_liab_percentage);
                  fee_flat_cnt                  := fee_flat_cnt + 1;
                else
                  liab_bill_hr_fees_in_out := liab_bill_hr_fees_in_out +
                                              (t_bill_hr_fees *
                                              t_fees_liab_percentage);
                END IF;
              END IF;
              IF fee_nondrop_waiv_hr_in_out < prev_rule_from_flat_hr_fee_in THEN
                --- 7.4.0.1
                if ((prev_rule_from_flat_hr_fee_in -
                   fee_nondrop_waiv_hr_in_out) <= t_waiv_hr_fees) or
                   (LAST_ASSESSMENT_HANDLED_DD) OR fee_waiv_flat_cnt > 0 then
                  liab_flat_waiv_hr_fees_in_out := ((prev_rule_from_flat_hr_fee_in -
                                                   fee_nondrop_waiv_hr_in_out) *
                                                   t_fees_liab_percentage);
                  fee_waiv_flat_cnt             := fee_waiv_flat_cnt + 1;
                else
                  liab_waiv_hr_fees_in_out := liab_waiv_hr_fees_in_out +
                                              (t_waiv_hr_fees *
                                              t_fees_liab_percentage);
                end if;
              END IF;
            ELSE
              liab_bill_hr_fees_in_out := liab_bill_hr_fees_in_out +
                                          (t_bill_hr_fees *
                                          t_fees_liab_percentage);
              p_print_dbms('==liab_bill_hr_fees_in_out: ' ||
                           liab_bill_hr_fees_in_out);
              liab_waiv_hr_fees_in_out := liab_waiv_hr_fees_in_out +
                                          (t_waiv_hr_fees *
                                          t_fees_liab_percentage);

            END IF;

          END IF; /* IF t_fees_liab_percentage <> 1 */
        END IF; --(t_tuit_liab_percentage <> 1 OR t_fees_liab_percentage <> 1)

      ELSE
        -- new add

        p_print_dbms('before check for new add last assmnt, prev assmt,  actdate: ' ||
                     to_char(sfbetrm_rec.sfbetrm_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS') || ', ' ||
                     to_char(prev_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS') || ', ' ||
                     to_char(act_date_in_out, 'DD-MON-YYYY HH24:MI:SS'));
        p_print_dbms('check these before add  last dd: ' ||
                     to_char(last_dd_activity_date, 'HH24:MI:SS') ||
                     ' First act date: ' ||
                     to_char(first_act_io, 'HH24:MI:SS'));
        if (LAST_ASSESSMENT_HANDLED_DD) then
          p_print_dbms('last assessment handled dd');
        end if;

        p_print_dbms('new add calc 1:   last_dd_activity_date' ||
                     to_char(last_dd_activity_date,
                             'DD-MON-YYYY HH24:MI:SS') ||
                     ' act_date_in_out > last_assessment_date ' ||
                     to_char(act_date_in_out, 'DD-MON-YYYY HH24:MI:SS') ||
                     ' > ' || to_char(last_assessment_date,
                                      'DD-MON-YYYY HH24:MI:SS'));
        p_print_dbms('new add calc 2:    first_act_io >=  last_dd_activity_date ' ||
                     to_char(first_act_io, 'DD-MON-YYYY HH24:MI:SS') ||
                     '  >= ' ||
                     to_char(last_dd_activity_date,
                             'DD-MON-YYYY HH24:MI:SS'));
        p_print_dbms('and last dd has to be after the prev ass: ' ||
                     to_char(last_dd_activity_date,
                             'DD-MON-YYYY HH24:MI:SS') || ' > ' ||
                     to_char(prev_assessment_date,
                             'DD-MON-YYYY HH24:MI:SS'));

        --- 1-2O637G only process RE if new or this is the assessment after the intermediate and
        ---  the activity date on the RE is after the assessment before the intermediate
        IF (NOT (LAST_ASSESSMENT_HANDLED_DD) OR
           ((LAST_ASSESSMENT_HANDLED_DD) AND (SWAP_RBC_MORE_DROPPED) and
           last_dd_activity_date is null and
           act_date_in_out = first_swap_date) OR
           ((LAST_ASSESSMENT_HANDLED_DD) AND NOT (SWAP_RBC_MORE_DROPPED) AND
           last_dd_activity_date is null and
           act_date_in_out > last_assessment_date) OR
           ((LAST_ASSESSMENT_HANDLED_DD) AND
           first_act_io >= last_dd_activity_date
           --- 8.3.0.1  do not want to include REs if they occurred
           and last_dd_activity_date > prev_assessment_date)) THEN
          p_print_dbms('New ADD= ' || tui_nondrop_bill_hr_in_out || ' + ' ||
                       t_reg_bill_hr || ' waiv: ' ||
                       tui_nondrop_waiv_hr_in_out || ' + ' ||
                       t_reg_waiv_hr);
          tui_nondrop_bill_hr_in_out := tui_nondrop_bill_hr_in_out +
                                        t_reg_bill_hr;
          tui_nondrop_waiv_hr_in_out := tui_nondrop_waiv_hr_in_out +
                                        t_reg_waiv_hr;
          fee_nondrop_bill_hr_in_out := fee_nondrop_bill_hr_in_out +
                                        t_reg_bill_hr;
          fee_nondrop_waiv_hr_in_out := fee_nondrop_waiv_hr_in_out +
                                        t_reg_waiv_hr;
          -- Defect 1-9CE6N Problem 2.
          tui_calculated_ind_in_out := TRUE;
          fee_calculated_ind_in_out := TRUE;

        END IF;
      END IF;
    END IF; -- updated/new reg record since last assessment
    p_print_dbms('at end of p_calc_flat_hr_liab, tui non drop: ' ||
                 tui_nondrop_bill_hr_in_out);
  END p_calc_flat_hr_liability;

  /* ******************************************************************* */
  /* Calculate conventional course hour liability.                       */
  /* ******************************************************************* */
  PROCEDURE p_calc_reg_hr_liability(p_tot_reg_bill_hr       IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_bill_hr_tuit IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_bill_hr_fees IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_reg_waiv_hr       IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_waiv_hr_tuit IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                                    p_tot_liab_waiv_hr_fees IN OUT sfrfaud.sfrfaud_reg_bill_hr%TYPE) IS
  BEGIN

    p_print_dbms('P_CALC_REG_HR_LIABILITY BEGINS');
    p_print_dbms('p tot liab bill hr tuit ' || p_tot_liab_bill_hr_tuit);
    p_print_dbms('t bill hr tuit ' || t_bill_hr_tuit);
    p_print_dbms('t tuit liab percentage ' || t_tuit_liab_percentage);

    --   IF  p_tot_liab_bill_hr_tuit <> 0
    --   AND NOT f_ests_means_wd(sfbetrm_rec.sfbetrm_ests_code) THEN
    p_tot_liab_bill_hr_tuit := p_tot_liab_bill_hr_tuit +
                               (t_bill_hr_tuit * t_tuit_liab_percentage);

    --   END IF;

    --   IF  p_tot_liab_bill_hr_fees <> 0
    --   AND NOT f_ests_means_wd(sfbetrm_rec.sfbetrm_ests_code) THEN
    p_tot_liab_bill_hr_fees := p_tot_liab_bill_hr_fees +
                               (t_bill_hr_fees * t_fees_liab_percentage);
    --   END IF;

    --   IF  p_tot_liab_waiv_hr_tuit <> 0
    --   AND NOT f_ests_means_wd(sfbetrm_rec.sfbetrm_ests_code) THEN
    p_tot_liab_waiv_hr_tuit := p_tot_liab_waiv_hr_tuit +
                               (t_waiv_hr_tuit * t_tuit_liab_percentage);
    --   END IF;

    --   IF  p_tot_liab_waiv_hr_fees <> 0
    --   AND NOT f_ests_means_wd(sfbetrm_rec.sfbetrm_ests_code) THEN
    p_tot_liab_waiv_hr_fees := p_tot_liab_waiv_hr_fees +
                               (t_waiv_hr_fees * t_fees_liab_percentage);
    --   END IF;

  END p_calc_reg_hr_liability;

  /* ******************************************************************* */
  /* Determine student's liability based on their enrollment status.     */
  /* ******************************************************************* */
  /* Sum the charges for detail codes under the student's account that   */
  /* originated from assessment (source 'R') for the term. Apply the     */
  /* liability percentage determined from the refund rule to the total   */
  /* for the detail code based on the DCAT code (TUI or FEE) for the     */
  /* detail code. Record the liability in the student's audit data.      */
  /*                                                                     */
  /* Always create an audit record for the ESTS liability, even if the   */
  /* liable amount is 0.00. This will allow the needed refunding to      */
  /* occur. When an ESTS WD is in effect, reversal processing does not   */
  /* occur since the liability is based on the accounting balance for    */
  /* the detail code and not rule-based liability.                       */
  /* ******************************************************************* */
  PROCEDURE p_calc_rfst_liability(pidm_in                 IN spriden.spriden_pidm%TYPE,
                                  term_in                 IN stvterm.stvterm_code%TYPE,
                                  ests_wd_assessed_ind_in IN VARCHAR2,
                                  source_pgm_in           IN VARCHAR2) IS
    CURSOR detl_charges_for_term_c IS
      SELECT SUM(tbraccd_amount),
             tbbdetc_dcat_code,
             tbraccd_detail_code,
             DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', tbraccd_crn, NULL)
        FROM tbbdetc, tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = 'R'
         AND tbbdetc_detail_code = tbraccd_detail_code
      -- 89876 AND tbbdetc_dcat_code in ('TUI','FEE')
       GROUP BY tbbdetc_dcat_code,
                tbraccd_detail_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       tbraccd_crn,
                       NULL)
       ORDER BY tbbdetc_dcat_code,
                tbraccd_detail_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       tbraccd_crn,
                       NULL);

    /* ***************************************************** */
    /* Sum the charges for the detail code, but exclude any  */
    /* adjustment made for the last ESTS WD so the correct   */
    /* liability is based on the current ESTS code and date. */
    /* ***************************************************** */
    CURSOR detl_charges_for_wd_c IS
      SELECT SUM(tbraccd_amount),
             tbbdetc_dcat_code,
             tbraccd_detail_code,
             DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', tbraccd_crn, NULL)
        FROM tbbdetc, tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = 'R'
         AND tbbdetc_detail_code = tbraccd_detail_code
            -- 89876 AND tbbdetc_dcat_code in ('TUI','FEE')
         AND tbraccd_tran_number NOT IN
             (SELECT sfrfaud_accd_tran_number
                FROM sfrfaud
               WHERE sfrfaud_pidm = pidm_in
                 AND sfrfaud_term_code = term_in
                 AND sfrfaud_detl_code = tbraccd_detail_code
                 AND sfrfaud_activity_date =
                     sfbetrm_rec.sfbetrm_assessment_date
                    --              AND sfrfaud_note like '%ESTS liability%') I18N Bug
                 AND sfrfaud_note like '%' || c_ESTS_liability || '%')
       GROUP BY tbbdetc_dcat_code,
                tbraccd_detail_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       tbraccd_crn,
                       NULL)
       ORDER BY tbbdetc_dcat_code,
                tbraccd_detail_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       tbraccd_crn,
                       NULL);

    detl_total      tbraccd.tbraccd_amount%TYPE := 0.00;
    detl_dcat       ttvdcat.ttvdcat_code%TYPE := '';
    detl_code       tbbdetc.tbbdetc_detail_code%TYPE := '';
    detl_crn        tbraccd.tbraccd_crn%TYPE := '';
    detl_liab_amt   tbraccd.tbraccd_amount%TYPE := 0.00;
    next_seqno      sfrfaud.sfrfaud_seqno%TYPE := 0;
    note            sfrfaud.sfrfaud_note%TYPE := '';
    note_percentage NUMBER(3); -- for note, whole number for %

  BEGIN
    crn_rfnd_table    := 'SFBRFST';
    ESTS_WD_PROCESSED := TRUE;

    -- Defect 88708: Create nondropped hours audit record. 7/03 BAG
    p_create_nondrop_hrs_sfrfaud(pidm_in, term_in, source_pgm_in);

    IF ests_wd_assessed_ind_in = 'Y' THEN
      -- 100% liable for acct sum
      OPEN detl_charges_for_term_c;
      LOOP
        FETCH detl_charges_for_term_c
          INTO detl_total, detl_dcat, detl_code, detl_crn;
        EXIT WHEN detl_charges_for_term_c%NOTFOUND;

        IF detl_dcat = 'TUI' THEN
          detl_liab_amt   := (detl_total * rfst_tuit_liab_percentage);
          note_percentage := (rfst_tuit_liab_percentage * 100);
        ELSIF detl_dcat = 'FEE' THEN
          detl_liab_amt   := (detl_total * rfst_fees_liab_percentage);
          note_percentage := (rfst_fees_liab_percentage * 100);
        ELSE
          detl_liab_amt   := detl_total;
          note_percentage := 100; -- always 100% liability for non-TUI/FEE
        END IF;

        note := g$_nls.get('SFKFEE1-0014',
                           'SQL',
                           'ESTS %01% W/D prev processed. %02% %03% is %04% % of %05% total %06%',
                           sfbetrm_rec.sfbetrm_ests_code,
                           detl_dcat,
                           c_ESTS_liability,
                           note_percentage,
                           detl_code,
                           detl_total);

        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        p_print_dbms('p_insert_sfrfaud 4');
        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'A',
                         detl_dcat,
                         detl_code,
                         detl_liab_amt,
                         'N', -- N/A: no ABC rule
                         source_pgm_in,
                         NULL, -- N/A: no rule seqno
                         NULL, -- N/A: no rule type
                         detl_crn, -- 88805
                         NULL, -- N/A: no RSTS date
                         NULL, -- N/A: no RSTS date
                         sfbetrm_rec.sfbetrm_ests_code, -- ESTS code
                         sfbetrm_rec.sfbetrm_ests_date, -- ESTS date
                         crn_rfnd_table, -- refund table
                         NULL, -- N/A: no reg bill hour
                         NULL, -- N/A: no reg waiv hour
                         NULL, -- N/A: no rule cred hours
                         NULL, -- N/A: no rule stud hours
                         NULL, -- N/A: no rule per cred charge
                         NULL, -- N/A: no rule flat fee amount
                         NULL, -- N/A: no overload hrs for section fees
                         NULL, -- N/A: no rule overload charge
                         NULL, -- populate TBRACCD tran when charge is inserted
                         note, -- 89669
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr
      END LOOP;
      CLOSE detl_charges_for_term_c;
    ELSE
      -- use liable percents against acct sum excluding last ESTS WD adjustments
      OPEN detl_charges_for_wd_c;
      LOOP
        FETCH detl_charges_for_wd_c
          INTO detl_total, detl_dcat, detl_code, detl_crn;
        EXIT WHEN detl_charges_for_wd_c%NOTFOUND;
        IF detl_dcat = 'TUI' THEN
          detl_liab_amt   := (detl_total * rfst_tuit_liab_percentage);
          note_percentage := (rfst_tuit_liab_percentage * 100);
        ELSIF detl_dcat = 'FEE' THEN
          detl_liab_amt   := (detl_total * rfst_fees_liab_percentage);
          note_percentage := (rfst_fees_liab_percentage * 100);
        ELSE
          detl_liab_amt   := detl_total;
          note_percentage := 100; -- always 100% liability for non-TUI/FEE
        END IF;

        note := g$_nls.get('SFKFEE1-0015',
                           'SQL',
                           'ESTS refund for %01% in effect. %02% %03% is %04%% of %05% total %06%. ',
                           sfbetrm_rec.sfbetrm_ests_code,
                           detl_dcat,
                           c_ESTS_liability,
                           note_percentage,
                           detl_code,
                           detl_total);

        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        p_print_dbms('p_insert_sfrfaud 5');
        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'A',
                         detl_dcat,
                         detl_code,
                         detl_liab_amt,
                         'N', -- N/A: no ABC rule
                         source_pgm_in,
                         NULL, -- N/A: no rule seqno
                         NULL, -- N/A: no rule type
                         detl_crn, -- 88805
                         NULL, -- N/A: no RSTS date
                         NULL, -- N/A: no RSTS date
                         sfbetrm_rec.sfbetrm_ests_code, -- ESTS code
                         sfbetrm_rec.sfbetrm_ests_date, -- ESTS date
                         crn_rfnd_table, -- refund table
                         NULL, -- N/A: no reg bill hour
                         NULL, -- N/A: no reg waiv hour
                         NULL, -- N/A: no rule cred hours
                         NULL, -- N/A: no rule stud hours
                         NULL, -- N/A: no rule per cred charge
                         NULL, -- N/A: no rule flat fee amount
                         NULL, -- N/A: no overload hrs for section fees
                         NULL, -- N/A: no rule overload charge
                         NULL, -- populate TBRACCD tran when charge is inserted
                         note, -- 8966
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr
      END LOOP;
      CLOSE detl_charges_for_wd_c;
    END IF;
  END p_calc_rfst_liability;

  /* ******************************************************************* */
  /* Find all flat rule ranges used in past assessment for student, type of rule and value     */
  /* We need to process each flat separately because the liability may be different based on drops       */
  /* ******************************************************************* */
  PROCEDURE p_multiple_rules(pidm_in       IN spriden.spriden_pidm%TYPE,
                             term_in       IN stvterm.stvterm_code%TYPE,
                             rule_type_in  IN VARCHAR2,
                             rule_value_in IN VARCHAR2) is

    flat_from_hrs sfrrgfe.sfrrgfe_from_flat_hrs%type;
    flat_to_hrs   sfrrgfe.sfrrgfe_from_flat_hrs%type;

    flat_rec flat_hour_ranges_rec;
    tab_cnt  pls_integer := 0;

    CURSOR flat_rule_c IS
      SELECT sfrfaud_rgfe_FROM_FLAT_HRS,
             ---- 8.1.1 change all high hours over 99 to 999
             decode(greatest(sfrfaud_rgfe_TO_FLAT_HRS, max_flat_to_hours),
                    sfrfaud_rgfe_to_flat_hrs,
                    max_flat_to_hours,
                    sfrfaud_rgfe_to_flat_hrs)
      ---  sfrfaud_rgfe_TO_FLAT_HRS
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_rgfe_type = rule_type_in
         AND decode(rule_type_in,
                    STUDENT_TYPE,
                    '%',
                    nvl(sfrfaud_RGFE_RULE_VALUE, '%')) =
             nvl(rule_value_in, '%')
         AND nvl(sfrfaud_rgfe_flat_fee_amount, 0) > 0
       group by sfrfaud_rgfe_FROM_FLAT_HRS,
                decode(greatest(sfrfaud_rgfe_TO_FLAT_HRS, max_flat_to_hours),
                       sfrfaud_rgfe_to_flat_hrs,
                       max_flat_to_hours,
                       sfrfaud_rgfe_to_flat_hrs)
       order by sfrfaud_rgfe_FROM_FLAT_HRS,
                decode(greatest(sfrfaud_rgfe_TO_FLAT_HRS, max_flat_to_hours),
                       sfrfaud_rgfe_to_flat_hrs,
                       max_flat_to_hours,
                       sfrfaud_rgfe_to_flat_hrs);

  BEGIN
    p_print_dbms('Check for multiple rule types: ' || rule_type_in || ' ' ||
                 rule_value_in);

    OPEN flat_rule_c;
    LOOP
      --- multiple flat hr rules?
      fetch flat_rule_c
        into flat_from_hrs, flat_to_hrs;
      exit when flat_rule_c%notfound;
      tab_cnt := tab_cnt + 1;
      p_print_dbms('--mutiple cnt: ' || tab_cnt || ' flat from: ' ||
                   flat_from_hrs || ' flat to hrs: ' || flat_to_hrs);
      flat_rec.r_flat_from_hrs := flat_from_hrs;
      flat_rec.r_flat_to_hrs := flat_to_hrs;
      flat_ranges(tab_cnt) := flat_rec;
      p_print_dbms('--after writing to table: ' || flat_ranges(tab_cnt)
                   .r_flat_from_hrs || ' to: ' || flat_ranges(tab_cnt)
                   .r_flat_to_hrs || ' cnt: ' || flat_ranges.count);
    end loop;
    close flat_rule_c;

  END p_multiple_rules;

  /* ******************************************************************* */
  /*   Process the fees associated with each course level, campus, attr or for the student within the     */
  /*   student's registration for the term.                              */
  /* ******************************************************************* */
  /*  Course  fees for non student type rules  are processed a little differently than student. */
  /*  For each course level determined within the student's registration */
  /*  for the term, we need to process the totals for the course level   */
  /*  itself and then process the courses that the student is registered */
  /*  for that have the current course level being processed.            */
  /* ******************************************************************* */
  PROCEDURE p_calcliability(pidm_in       IN spriden.spriden_pidm%TYPE,
                            term_in       IN stvterm.stvterm_code%TYPE,
                            source_pgm_in IN VARCHAR2,
                            type_in       IN sfrrgfe.sfrrgfe_type%type) IS
    -- local working variables for totaling during level group processing
    prev_ptrm_code        stvptrm.stvptrm_code%TYPE := 'x'; -- force into test
    curr_type_code        varchar2(30) := '';
    tot_reg_bill_hr       sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tot_liab_bill_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tot_liab_bill_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tot_reg_waiv_hr       sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tot_liab_waiv_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tot_liab_waiv_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;

    --102188 start
    starting_liab_bill_hr_tui sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE := 0;
    starting_liab_bill_hr_fee sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE := 0;
    prev_liab_bill_tui_hr     sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    prev_liab_bill_fee_hr     sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    -- vars for drops involving prev flat hour rule qualification
    tui_nondrop_bill_hr sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tui_nondrop_waiv_hr sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    fee_nondrop_bill_hr sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    fee_nondrop_waiv_hr sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    --102188 end

    prev_liab_bill_hr sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE := 0;

    curr_liab_flat_bill_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_flat_waiv_hr_tuit sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_OL_bill_hr_tuit   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_OL_waiv_hr_tuit   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_bill_hr_tuit      sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_waiv_hr_tuit      sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_flat_bill_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_flat_waiv_hr_fees sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_OL_bill_hr_fees   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_OL_waiv_hr_fees   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_bill_hr_fees      sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    curr_liab_waiv_hr_fees      sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    --102188 start
    prev_rule_from_flat_hr_tui   sfrrgfe.sfrrgfe_from_flat_hrs%TYPE := 0;
    prev_rule_from_flat_hr_fee   sfrrgfe.sfrrgfe_from_flat_hrs%TYPE := 0;
    prev_rule_OL_start_hr_tui    sfrrgfe.sfrrgfe_from_flat_hrs%TYPE := 0;
    prev_rule_OL_start_hr_fee    sfrrgfe.sfrrgfe_from_flat_hrs%TYPE := 0;
    last_assess_met_flat_ind_tui VARCHAR2(1) := 'N';
    last_assess_met_flat_ind_fee VARCHAR2(1) := 'N';
    tui_calculated_ind           BOOLEAN := FALSE;
    fee_calculated_ind           BOOLEAN := FALSE;
    TYPE NUM_TAB is TABLE of NUMBER INDEX BY BINARY_INTEGER;
    multiple_tui_liab       NUM_TAB;
    multiple_fee_liab       NUM_TAB;
    multiple_tui_ctr        NUMBER := 0;
    multiple_fee_ctr        NUMBER := 0;
    multiple_tui_bill_hr    NUM_TAB;
    multiple_fee_bill_hr    NUM_TAB;
    multiple_tui_waiv_hr    NUM_TAB;
    multiple_fee_waiv_hr    NUM_TAB;
    empty_memory_table      NUM_TAB;
    t_liab_bill_hr_multiple sfrrgfe.sfrrgfe_from_flat_hrs%TYPE := 0;
    t_liab_waiv_hr_multiple sfrrgfe.sfrrgfe_from_flat_hrs%TYPE := 0;
    sum_ndrop_bill_hr_tuit  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    sum_ndrop_bill_hr_fees  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    sum_ndrop_waiv_hr_tuit  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    sum_ndrop_waiv_hr_fees  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;

    --- 8.3
    save_orig_hrs sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    -- 1-2O637G new add date
    add_date_io  DATE;
    first_act_io DATE;
    -- 1-2O637G
    CHECK_FLAT           BOOLEAN := FALSE;
    tui_flat_cnt         pls_integer := 0;
    tui_waiv_flat_cnt    pls_integer := 0;
    fee_flat_cnt         pls_integer := 0;
    fee_waiv_flat_cnt    pls_integer := 0;
    RECONSIDER_FLAT_TUIT BOOLEAN := TRUE;
    RECONSIDER_FLAT_FEES BOOLEAN := TRUE;
    save_tui_percentage  number := 0;
    save_fee_percentage  number := 0;
    -- 105831 7.4.0.1 orig bill hrs for variable credits
    orig_crn_bill_hrs sfrstcr.sfrstcr_bill_hr%type := 0.0;
    -- 102188 end
    -- 99226 start
    prev_rule_crse_waiv_ind_tui sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE := 'N';
    prev_rule_crse_waiv_ind_fee sfrrgfe.sfrrgfe_crse_waiv_ind%TYPE := 'N';
    -- 99226 end

    -- 7.5   for multiple types of rules
    tab_cnt               pls_integer := 0;
    flat_rec              flat_hour_ranges_rec;
    process_flat_from_hrs sfrrgfe.SFRRGFE_FROM_FLAT_HRS%TYPE := 0;
    process_flat_to_hrs   sfrrgfe.sfrrgfe_to_flat_hrs%TYPE := 0;
    process_flat_liab     BOOLEAN := FALSE;

    /* 104171, all variables below were created to support 104070/104171 */
    act_date_io             DATE;
    DROPS_SUCCESSION_BROKEN BOOLEAN := FALSE;
    CONTINUE_CALC           BOOLEAN := FALSE;
    num_of_drops            NUMBER := 0;
    drops_count             NUMBER := 0;
    tui_below_flat_bill_hr  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    fee_below_flat_bill_hr  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    tui_below_flat_waiv_hr  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    fee_below_flat_waiv_hr  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    orig_tui_ndrop_billhr   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    orig_fee_ndrop_billhr   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    orig_tui_ndrop_waivhr   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    orig_fee_ndrop_waivhr   sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    below_prev_flat_OL_tui  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    below_prev_flat_OL_fee  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    above_prev_flat_OL_tui  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    above_prev_flat_OL_fee  sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0;
    above_prev_oload_OL_tui sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0; --1-PX0E9
    above_prev_oload_OL_fee sfrfaud.sfrfaud_reg_bill_hr%TYPE := 0; --1-PX0E9

    --104070 Created this cursor to get the previous liable hours of the previous assessment.
    CURSOR rev_penalty_to_flat_c IS
      SELECT MAX(a.sfrfaud_activity_date), a.sfrfaud_rule_liable_bill_hrs
        FROM sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_rule_liable_bill_hrs IS NOT NULL
         AND a.sfrfaud_activity_date <
             (SELECT MAX(b.sfrfaud_activity_date)
                FROM sfrfaud b
               WHERE a.sfrfaud_pidm = b.sfrfaud_pidm
                 AND a.sfrfaud_term_code = b.sfrfaud_term_code)
       GROUP BY sfrfaud_rule_liable_bill_hrs
      --- 7.4.0.2 sort in desc order
       ORDER BY 1 desc;

    rev_penalty_rec       rev_penalty_to_flat_c%ROWTYPE;
    lv_course_types_ref   sftfees_type_ref;
    lv_course_records_ref sftfees_ref;
    lv_crn_records_ref    sftfees_ref;
    lv_fees_rec           sftfees_rec;
    curr_type             sftfees_type_rec;

    --- 7.4.0.4 find if any WD in any assessment for rbt reversal of penalities

    lv_find_any_wd varchar2(1);

    CURSOR find_any_wd_c is
      SELECT 'Y'
        from stvrsts, sfrstcr
       where stvrsts_code = sfrstcr_rsts_code
         and sfrstcr_pidm = pidm_in
         and sfrstcr_term_code = term_in
         and stvrsts_withdraw_ind = 'Y';

    ---- procedure to calculate the starting liability for flat, rbc and if there were drops
    ---  this section used to be at the start of the p_calcliability procedure but has
    --   been separated so it can be executed for each flat range
    procedure p_flat_starting_liability is

    begin

      p_print_dbms('start flat starting from: ' || process_flat_from_hrs ||
                   ' to flat: ' || process_flat_to_hrs);

      PREV_FLAT_HR_RULE_MET := TRUE;

      p_get_last_assess_flat_data(pidm_in,
                                  term_in,
                                  prev_liab_bill_hr,
                                  prev_rule_from_flat_hr_tui,
                                  prev_rule_from_flat_hr_fee,
                                  prev_rule_OL_start_hr_tui,
                                  prev_rule_OL_start_hr_fee,
                                  type_in,
                                  curr_type_code,
                                  last_assess_met_flat_ind_tui,
                                  last_assess_met_flat_ind_fee,
                                  prev_rule_crse_waiv_ind_tui,
                                  prev_rule_crse_waiv_ind_fee,
                                  starting_liab_bill_hr_tui,
                                  starting_liab_bill_hr_fee,
                                  process_flat_from_hrs,
                                  process_flat_to_hrs);

      p_print_dbms('just back from finding last assessment for flat and found maybe wrong starting: ' ||
                   starting_liab_bill_hr_tui || ' fee: ' ||
                   starting_liab_bill_hr_fee);
      p_print_dbms('Prev flat met: prev_rule_from_flat_hr_tui ' ||
                   prev_rule_from_flat_hr_tui ||
                   ' prev_rule_OL_start_hr_tui: ' ||
                   prev_rule_OL_start_hr_tui || ' fee waiv: ' ||
                   prev_rule_crse_waiv_ind_fee);
      -- Nondrop hr starting point for waived and non-waived hrs depends on rule waiv ind
      --- 7.5 pass the rule type and value to get new non drop hours
      tui_nondrop_bill_hr := f_get_prev_nondrop_hrs(pidm_in,
                                                    term_in,
                                                    prev_rule_crse_waiv_ind_tui,
                                                    type_in,
                                                    curr_type_code);
      tui_nondrop_waiv_hr := f_get_prev_nondrop_hrs(pidm_in,
                                                    term_in,
                                                    prev_rule_crse_waiv_ind_tui,
                                                    type_in,
                                                    curr_type_code);
      fee_nondrop_bill_hr := f_get_prev_nondrop_hrs(pidm_in,
                                                    term_in,
                                                    prev_rule_crse_waiv_ind_fee,
                                                    type_in,
                                                    curr_type_code);
      fee_nondrop_waiv_hr := f_get_prev_nondrop_hrs(pidm_in,
                                                    term_in,
                                                    prev_rule_crse_waiv_ind_fee,
                                                    type_in,
                                                    curr_type_code);

      p_print_dbms('p_calcliability call to f get prev nondrop, the 1 percent drops:  tui_nondrop_bill_hr= ' ||
                   tui_nondrop_bill_hr);
      p_print_dbms('tui nondrop bill hrs, comes from f_get_prev_nondrop from previous faud N rcord: ' ||
                   tui_nondrop_bill_hr);
      p_print_dbms('staring fee non drop bill hr: ' || fee_nondrop_bill_hr);
      save_orig_hrs := tui_nondrop_bill_hr;
      ---  find multiple rules (mixed flat and percredit with overlapping hours )

      if type_in <> STUDENT_TYPE then
        -- 1-OOYRP, reduce nondrop hr if greater than starting hr
        p_print_dbms('Not student type: ' || type_in || ' starting liab: ' ||
                     starting_liab_bill_hr_tui || ' tui non drop: ' ||
                     tui_nondrop_bill_hr);
        IF (starting_liab_bill_hr_tui < tui_nondrop_bill_hr) THEN
          tui_nondrop_bill_hr := starting_liab_bill_hr_tui;
        END IF;
        IF (starting_liab_bill_hr_tui < tui_nondrop_waiv_hr) THEN
          tui_nondrop_waiv_hr := starting_liab_bill_hr_tui;
        END IF;
        p_print_dbms('reduce the non drop hrs if > starting: ' ||
                     fee_nondrop_bill_hr || ' < ' ||
                     starting_liab_bill_hr_fee);
        IF (starting_liab_bill_hr_fee < fee_nondrop_bill_hr) THEN
          fee_nondrop_bill_hr := starting_liab_bill_hr_fee;
        END IF;
        IF (starting_liab_bill_hr_fee < fee_nondrop_waiv_hr) THEN
          fee_nondrop_waiv_hr := starting_liab_bill_hr_fee;
        END IF;
        p_print_dbms('new fee hrs: ' || fee_nondrop_bill_hr);
      end if;

      -- 104171, to save the original nondrop hours for later coomputation
      orig_tui_ndrop_billhr := tui_nondrop_bill_hr;
      orig_tui_ndrop_waivhr := tui_nondrop_bill_hr;
      orig_fee_ndrop_billhr := fee_nondrop_bill_hr;
      orig_fee_ndrop_waivhr := fee_nondrop_bill_hr;
      -- Sums nondrop_drop_bill_hrs for FEES and TUIT DEFECT 102188
      sum_ndrop_bill_hr_tuit := 0;
      sum_ndrop_bill_hr_fees := 0;
      sum_ndrop_waiv_hr_tuit := 0;
      sum_ndrop_waiv_hr_fees := 0;
      p_get_ndrop_hrs(pidm_in,
                      term_in,
                      sum_ndrop_bill_hr_tuit,
                      sum_ndrop_bill_hr_fees,
                      sum_ndrop_waiv_hr_tuit,
                      sum_ndrop_waiv_hr_fees);
      p_print_dbms('--> sum_ndrop_bill_hr_tuit: ' ||
                   sum_ndrop_bill_hr_tuit);

      --104171, reverse penalty hrs has been applied to so that during swapping, if two different
      --assessments fell in the same day for the same person, re-evaluation of the assessment will
      --be done, hence, penalty hours need to be adjusted based on the last assessment whose date
      --was different from the current.

      IF NOT (LAST_ASSESSMENT_HANDLED_DD) THEN
        --102188, create two new vars to replace prev_flat_OL_liab_hr
        --prev_flat_OL_liab_hr := prev_liab_bill_hr - nondrop_bill_hr;
        --prev_liab_bill_tui_hr   := starting_liab_bill_hr_tui - tui_nondrop_bill_hr;
        --prev_liab_bill_fee_hr   := starting_liab_bill_hr_fee - fee_nondrop_bill_hr;

        -- 104171, the conditions below are necessary to make sure if there is really a need
        -- for reverse penalty hours.  And since there are no conditions that govern this in
        -- p_process_hours_swap, this is where that need for reverse penalty will be determined.
        --- 7.4.0.1 1-1AZHFF    Changed the < 0 to <= 0
        p_print_dbms('starting_liab_bill_hr_tui - tui_nondrop_bill_hr - rev_penalty_hrs_tui: ' ||
                     starting_liab_bill_hr_tui || ' - ' ||
                     tui_nondrop_bill_hr || ' - ' || rev_penalty_hrs_tui ||
                     ' and tui_nondrop_bill_hr > prev_rule_from_flat_hr_tu: ' ||
                     tui_nondrop_bill_hr || ' > ' ||
                     prev_rule_from_flat_hr_tui || ' or >= ' ||
                     prev_rule_OL_start_hr_tui);
        p_print_dbms('new non drop: ' || new_nondrop_hrs);
        IF ((starting_liab_bill_hr_tui - tui_nondrop_bill_hr -
           rev_penalty_hrs_tui) <= 0) OR
           ((tui_nondrop_bill_hr > prev_rule_from_flat_hr_tui) AND
           (tui_nondrop_bill_hr < prev_rule_OL_start_hr_tui)) THEN
          -- do not want to reverse the penalty if drop from OL to percredit, then back to flat or ol
          p_print_dbms('before swap: ' || before_swap_hours || ' > ' ||
                       ' prev rule OL: ' || prev_rule_OL_start_hr_tui ||
                       ' and start swap: ' || start_swap_hours ||
                       ' < prev rule from flat: ' ||
                       prev_rule_from_flat_hr_tui ||
                       ' or tui non drop < prev rule from flat: ' ||
                       tui_nondrop_bill_hr || ' and tui nondrop: ' ||
                       new_nondrop_hrs || '  > prev rule from flat: ' ||
                       prev_rule_from_flat_hr_tui);

          if (SWAP_RBC_MORE_DROPPED) AND
             ((before_swap_hours > prev_rule_OL_start_hr_tui and
             prev_rule_OL_start_hr_tui > 0) AND
             (start_swap_hours < prev_rule_from_flat_hr_tui or
             tui_nondrop_bill_hr < prev_rule_from_flat_hr_tui) AND
             new_nondrop_hrs > prev_rule_from_flat_hr_tui) then
            p_print_dbms('do not reset rev pen: ' || rev_penalty_hrs_tui ||
                         ' / ' || rev_dropped_hrs);
            if rev_dropped_hrs > 0 then
              rev_penalty_hrs_tui := rev_penalty_hrs_tui / rev_dropped_hrs;
            else
              rev_penalty_hrs_tui := 0;
            end if;
          else
            p_print_dbms('reset rev penalty to 0');
            rev_penalty_hrs_tui := 0;
          end if;
        ELSE
          IF (before_swap_hours < prev_rule_from_flat_hr_tui and
             (before_swap_hours > 0 or start_swap_hours > 0) and
             new_nondrop_hrs >= prev_rule_from_flat_hr_tui) then
            p_print_dbms('reset tui rev penalty to 0 for swap more ');
            rev_penalty_hrs_tui := 0;
          end if;
        END IF;
        IF ((starting_liab_bill_hr_fee - fee_nondrop_bill_hr -
           rev_penalty_hrs_fee) <= 0) OR
           ((fee_nondrop_bill_hr > prev_rule_from_flat_hr_fee) AND
           (fee_nondrop_bill_hr < prev_rule_OL_start_hr_fee)) THEN
          if (SWAP_RBC_MORE_DROPPED) AND
             ((before_swap_hours > prev_rule_OL_start_hr_fee and
             prev_rule_OL_start_hr_fee > 0) AND
             (start_swap_hours < prev_rule_from_flat_hr_fee or
             fee_nondrop_bill_hr < prev_rule_from_flat_hr_fee) AND
             new_nondrop_hrs > prev_rule_from_flat_hr_fee) then
            p_print_dbms('do not reset rev pen fee: ' ||
                         rev_penalty_hrs_fee || ' / ' || rev_dropped_hrs);
            if rev_dropped_hrs > 0 then
              rev_penalty_hrs_fee := rev_penalty_hrs_fee / rev_dropped_hrs;
            else
              rev_penalty_hrs_fee := 0;
            end if;
          else
            p_print_dbms('reset rev penalty to 0');
            rev_penalty_hrs_fee := 0;
          end if;
        ELSE
          IF (before_swap_hours < prev_rule_from_flat_hr_fee and
             (before_swap_hours > 0 or start_swap_hours > 0) and
             new_nondrop_hrs >= prev_rule_from_flat_hr_fee) then
            p_print_dbms('reset fee rev penalty to 0 for swap more ');
            rev_penalty_hrs_fee := 0;
          end if;
        END IF;
        p_print_dbms('rev penalty hrs tui : ' || rev_penalty_hrs_tui ||
                     ' fees: ' || rev_penalty_hrs_fee);

        -- 104171 - rev_penalty_hrs_tui/fee have been added to cater for swapping in case two
        -- different assessments are applied in the same date.  The reverse penalty hrs are coming
        -- from p_process_hours_swap procedure.
        --  Previous flat OL liab is the liability from the previous assessment
        p_print_dbms('prev OL before changing: ' || prev_liab_bill_tui_hr ||
                     ' new non drop hrs: ' || new_nondrop_hrs ||
                     ' prev rule from flat: ' ||
                     prev_rule_from_flat_hr_tui || ' start: ' ||
                     starting_liab_bill_hr_tui || ' start swap hrs: ' ||
                     start_swap_hours || ' before: ' || before_swap_hours ||
                     ' prv ol: ' || prev_rule_OL_start_hr_tui);
        IF (starting_liab_bill_hr_tui <= 0) THEN
          prev_liab_bill_tui_hr := 0;
        ELSE
          IF (starting_liab_bill_hr_tui > tui_nondrop_bill_hr) THEN
            -- 1-2JTK2Y  7.4.0.2   and 7.4.0.3
            if ((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_EQUAL) OR
               (SWAP_RBC_MORE_ADDS)) THEN

              IF (starting_liab_bill_hr_tui < prev_rule_from_flat_hr_tui) AND
                 (new_nondrop_hrs >= starting_liab_bill_hr_tui OR
                 --- or the new non drop is less then the overload
                 new_nondrop_hrs < nvl(prev_rule_OL_start_hr_tui, 0)) then
                p_print_dbms('inside swap prev carryover calc ');
                --- 7.4.0.3  set the prev flat OL if swap more drops and if we start below flat or OL and
                --   the swap takes in back into, and then out of the flat or OL
                if (start_swap_hours >= prev_rule_from_flat_hr_tui) and
                   (new_nondrop_hrs < prev_rule_from_flat_hr_tui) then
                  p_print_dbms('swap set the prev liab if started out in flat ' ||
                               ' starting - new tui nondrop ' ||
                               starting_liab_bill_hr_tui || ' - ' ||
                               tui_nondrop_bill_hr);
                  prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                           tui_nondrop_bill_hr;
                elsif (before_swap_hours < prev_rule_from_flat_hr_tui) and
                      (start_swap_hours >= prev_rule_from_flat_hr_tui) and
                      (new_nondrop_hrs >= prev_rule_from_flat_hr_tui) then
                  p_print_dbms('swap into flat, set the prev liab if started out in flat ' ||
                               ' starting - new tui nondrop ' ||
                               starting_liab_bill_hr_tui || ' - ' ||
                               tui_nondrop_bill_hr);
                  prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                           tui_nondrop_bill_hr;
                elsif (before_swap_hours >= prev_rule_OL_start_hr_tui) and
                      (start_swap_hours < prev_rule_from_flat_hr_tui) and
                      (new_nondrop_hrs < prev_rule_from_flat_hr_tui) and
                      (nvl(prev_rule_OL_start_hr_tui, 0) > 0) then
                  p_print_dbms('start swap hrs less prev rule OL, change OL: ' ||
                               starting_liab_bill_hr_tui || ' - ' ||
                               tui_nondrop_bill_hr);
                  prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                           tui_nondrop_bill_hr;
                  /* 7.4.0.4 prev carry over if in per credit */
                elsif (before_swap_hours < prev_rule_from_flat_hr_tui) and
                      (start_swap_hours < prev_rule_from_flat_hr_tui) and
                      (tui_nondrop_bill_hr < prev_rule_from_flat_hr_tui) and
                      (new_nondrop_hrs < prev_rule_from_flat_hr_tui) and
                      (new_nondrop_hrs > 0) then
                  --  7.4.0.6  do not set up prev liab if prev non drop is less than new drop
                  if new_nondrop_hrs <= tui_nondrop_bill_hr then
                    p_print_dbms('in percredit hour rate: ' ||
                                 starting_liab_bill_hr_tui || ' - ' ||
                                 tui_nondrop_bill_hr);
                    prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                             tui_nondrop_bill_hr;
                  else
                    p_print_dbms('in percredit hour rate, bill hrs increased so do not carry prev liab: ' ||
                                 prev_liab_bill_tui_hr);
                    if rev_penalty_hrs_tui > 0 then
                      prev_liab_bill_tui_hr := rev_penalty_hrs_tui;
                      p_print_dbms('set the prev liab to the rev pen: ' ||
                                   prev_liab_bill_tui_hr);
                    end if;
                  end if;
                else
                  p_print_dbms('did not make any of the conditions for swapping and reseting pen.');
                end if;

                --  end if;
              elsif new_nondrop_hrs >= prev_rule_OL_start_hr_tui then
                p_print_dbms('TH: swap: new_nondrop_hrs >= prev_rule_OL_start_hr_tui : ' ||
                             new_nondrop_hrs || '  >= ' ||
                             prev_rule_OL_start_hr_tui ||
                             ' new_nondrop_hrs <= tui_nondrop_bill_hr ' ||
                             new_nondrop_hrs || '  <= ' ||
                             tui_nondrop_bill_hr);

                --- 7.4.0.3 make sure was in less than OL
                --  really only want to do this if in the OL for first time, else do not reset the prev ol
                if (tui_nondrop_bill_hr < prev_rule_OL_start_hr_tui and
                   new_nondrop_hrs = prev_rule_OL_start_hr_tui) then
                  p_print_dbms('I am changing the prev flat ol#1 = ' ||
                               starting_liab_bill_hr_tui || ' - ' ||
                               tui_nondrop_bill_hr);
                  prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                           tui_nondrop_bill_hr;

                elsif ((before_swap_hours < prev_rule_from_flat_hr_tui) and
                      (start_swap_hours >= prev_rule_from_flat_hr_tui)) OR
                      ((before_swap_hours <= prev_rule_OL_start_hr_tui) and
                      (start_swap_hours >= prev_rule_OL_start_hr_tui)) then
                  p_print_dbms('swap, set the prev liab if started out in flat OR OL ' ||
                               ' starting - new tui nondrop ' ||
                               starting_liab_bill_hr_tui || ' - ' ||
                               tui_nondrop_bill_hr || ' > OL ' ||
                               prev_rule_OL_start_hr_tui || ' > flat ' ||
                               prev_rule_from_flat_hr_tui);

                  prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                           tui_nondrop_bill_hr;
                elsif ((before_swap_hours >= prev_rule_from_flat_hr_tui) and
                      (before_swap_hours < prev_rule_OL_start_hr_tui and
                      prev_rule_OL_start_hr_tui > 0)) and
                      (start_swap_hours >= prev_rule_OL_start_hr_tui) and
                      (new_nondrop_hrs >= prev_rule_OL_start_hr_tui) then
                  p_print_dbms('swap,  started out in flat stayed in OL ' ||
                               ' starting - new tui nondrop ' ||
                               starting_liab_bill_hr_tui || ' - ' ||
                               tui_nondrop_bill_hr || ' > OL ' ||
                               prev_rule_OL_start_hr_tui || ' > flat ' ||
                               prev_rule_from_flat_hr_tui);

                  prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                           tui_nondrop_bill_hr;

                end if;

              else
                p_print_dbms('swapping, I am changing the prev flat ol no revpen = ' ||
                             starting_liab_bill_hr_tui || ' - ' ||
                             tui_nondrop_bill_hr); --- || ' - ' || rev_penalty_hrs_tui );
                prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                         tui_nondrop_bill_hr; ---  - rev_penalty_hrs_tui;
              end if;

            else
              -- no swapping
              p_print_dbms('no swapping, I am changing the prev flat ol no swapping = ' ||
                           starting_liab_bill_hr_tui || ' - ' ||
                           tui_nondrop_bill_hr);
              prev_liab_bill_tui_hr := starting_liab_bill_hr_tui -
                                       tui_nondrop_bill_hr;
            end if;
          end if;
        END IF;

        IF (starting_liab_bill_hr_fee <= 0) THEN
          prev_liab_bill_fee_hr := 0;
        ELSE
          IF (starting_liab_bill_hr_fee > fee_nondrop_bill_hr) THEN
            -- 1-2JTK2Y  7.4.0.2   and 7.4.0.3
            if ((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_EQUAL) OR
               (SWAP_RBC_MORE_ADDS)) THEN

              IF (starting_liab_bill_hr_fee < prev_rule_from_flat_hr_fee) AND
                 (new_nondrop_hrs >= starting_liab_bill_hr_fee OR
                 --- or the new non drop is less then the overload
                 new_nondrop_hrs < prev_rule_OL_start_hr_fee) then

                --- 7.4.0.3  set the prev flat OL if swap more drops and if we start below flat or OL and
                --   the swap takes in back into, and then out of the flat or OL
                if (start_swap_hours >= prev_rule_from_flat_hr_fee) and
                   (new_nondrop_hrs < prev_rule_from_flat_hr_fee) then
                  p_print_dbms('swap, set the prev liab if started out in flat ' ||
                               ' starting - new fee nondrop ' ||
                               starting_liab_bill_hr_fee || ' - ' ||
                               fee_nondrop_bill_hr);
                  prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                           fee_nondrop_bill_hr;
                elsif (before_swap_hours < prev_rule_from_flat_hr_fee) and
                      (start_swap_hours >= prev_rule_from_flat_hr_fee) and
                      (new_nondrop_hrs >= prev_rule_from_flat_hr_fee) then
                  p_print_dbms('swap into flat, set the prev liab if started out in flat ' ||
                               ' starting - new fee nondrop ' ||
                               starting_liab_bill_hr_fee || ' - ' ||
                               fee_nondrop_bill_hr);
                  prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                           fee_nondrop_bill_hr;
                elsif (before_swap_hours >= prev_rule_OL_start_hr_fee) and
                      (start_swap_hours < prev_rule_from_flat_hr_fee) and
                      (new_nondrop_hrs < prev_rule_from_flat_hr_fee) and
                      (nvl(prev_rule_OL_start_hr_fee, 0) > 0) then
                  p_print_dbms('start swap hrs less prev rule OL, change OL: ' ||
                               starting_liab_bill_hr_fee || ' - ' ||
                               fee_nondrop_bill_hr);
                  prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                           fee_nondrop_bill_hr;
                  /* 7.4.0.4 prev carry over if in per credit */
                elsif (before_swap_hours < prev_rule_from_flat_hr_fee) and
                      (start_swap_hours < prev_rule_from_flat_hr_fee) and
                      (fee_nondrop_bill_hr < prev_rule_from_flat_hr_fee) and
                      (new_nondrop_hrs < prev_rule_from_flat_hr_fee) and
                      (new_nondrop_hrs > 0) then
                  if new_nondrop_hrs <= fee_nondrop_bill_hr then
                    p_print_dbms('FEE in percredit hour rate: ' ||
                                 starting_liab_bill_hr_fee || ' - ' ||
                                 fee_nondrop_bill_hr);
                    prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                             fee_nondrop_bill_hr;
                  else
                    p_print_dbms('in percredit hour rate, bill hrs increased so do not carry prev liab: ' ||
                                 prev_liab_bill_fee_hr);
                    if rev_penalty_hrs_fee > 0 then
                      prev_liab_bill_fee_hr := rev_penalty_hrs_fee;
                      p_print_dbms('set the prev liab to the rev pen: ' ||
                                   prev_liab_bill_fee_hr);
                    end if;
                  end if;
                end if;
                --  end if;
              elsif new_nondrop_hrs >= prev_rule_OL_start_hr_fee then
                p_print_dbms('TH: swap equal: new_nondrop_hrs >= prev_rule_OL_start_hr_fee : ' ||
                             new_nondrop_hrs || '  >= ' ||
                             prev_rule_OL_start_hr_fee ||
                             ' new_nondrop_hrs <= fee_nondrop_bill_hr ' ||
                             new_nondrop_hrs || '  <= ' ||
                             fee_nondrop_bill_hr);

                --- 7.4.0.3 make sure was in less than OL
                if (fee_nondrop_bill_hr < prev_rule_OL_start_hr_fee and
                   new_nondrop_hrs = prev_rule_OL_start_hr_fee) then
                  p_print_dbms('I am changing the prev flat ol#1 = ' ||
                               starting_liab_bill_hr_fee || ' - ' ||
                               fee_nondrop_bill_hr);
                  prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                           fee_nondrop_bill_hr;

                elsif ((before_swap_hours < prev_rule_from_flat_hr_fee) and
                      (start_swap_hours >= prev_rule_from_flat_hr_fee)) OR
                      ((before_swap_hours <= prev_rule_OL_start_hr_fee) and
                      (start_swap_hours >= prev_rule_OL_start_hr_fee)) then
                  p_print_dbms('swap, set the prev liab if started out in flat OR OL ' ||
                               ' starting - new fee nondrop ' ||
                               starting_liab_bill_hr_fee || ' - ' ||
                               fee_nondrop_bill_hr || ' > OL ' ||
                               prev_rule_OL_start_hr_fee || ' > flat ' ||
                               prev_rule_from_flat_hr_fee);

                  prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                           fee_nondrop_bill_hr;
                elsif ((before_swap_hours >= prev_rule_from_flat_hr_fee) and
                      (before_swap_hours < prev_rule_OL_start_hr_fee and
                      prev_rule_OL_start_hr_fee > 0)) and
                      (start_swap_hours >= prev_rule_OL_start_hr_fee) and
                      (new_nondrop_hrs >= prev_rule_OL_start_hr_fee) then
                  p_print_dbms('swap,  started out in flat stayed in OL ' ||
                               ' starting - new fee nondrop ' ||
                               starting_liab_bill_hr_fee || ' - ' ||
                               fee_nondrop_bill_hr || ' > OL ' ||
                               prev_rule_OL_start_hr_fee || ' > flat ' ||
                               prev_rule_from_flat_hr_fee);

                  prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                           fee_nondrop_bill_hr;

                end if;

              else
                p_print_dbms('swapping, I am changing the prev flat ol no revpen = ' ||
                             starting_liab_bill_hr_fee || ' - ' ||
                             fee_nondrop_bill_hr); --- || ' - ' || rev_penalty_hrs_fee);
                prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                         fee_nondrop_bill_hr; ----   - rev_penalty_hrs_fee;
              end if;

            else
              -- no swapping
              p_print_dbms('I am changing the prev flat ol no swapping = ' ||
                           starting_liab_bill_hr_fee || ' - ' ||
                           fee_nondrop_bill_hr);
              prev_liab_bill_fee_hr := starting_liab_bill_hr_fee -
                                       fee_nondrop_bill_hr;
            end if;
          end if;
        END IF;

        p_print_dbms('prev liab carryover after changes for flat: ' ||
                     prev_liab_bill_tui_hr);
        --save the prev_flat_OL_liab_tui/fee_hr in a bucket, in case the assessment will still remain below flat.
        below_prev_flat_OL_tui := prev_liab_bill_tui_hr;
        below_prev_flat_OL_fee := prev_liab_bill_fee_hr;

        p_print_dbms('prev liab carryover after rev pen: ' ||
                     prev_liab_bill_tui_hr);
        p_print_dbms('==========START==================');
        p_print_dbms('prev_liab_bill_tui_hr   1: ' ||
                     prev_liab_bill_tui_hr);
        p_print_dbms('prev_liab_bill_fee_hr   1: ' ||
                     prev_liab_bill_fee_hr);
        p_print_dbms('rev_tuit 1: ' || rev_tuit);
        p_print_dbms('rev_fees 1: ' || rev_fees);
        p_print_dbms('rev_lv_dhrs 1: ' || rev_lv_dhrs);
        p_print_dbms('t_liab_bill_hr_tuit 1a: ' || t_liab_bill_hr_tuit);

        --save the prev_flat_OL_liab_tui/fee_hr in a bucket, in case the assessment will still remain in flat.
        above_prev_flat_OL_tui := prev_liab_bill_tui_hr;
        above_prev_flat_OL_fee := prev_liab_bill_fee_hr;
        p_print_dbms('above prev flat: ' || above_prev_flat_OL_tui);

        -- swapping on, from overload to flat to overload.
        IF ((prev_liab_bill_tui_hr > 0) AND (rev_tuit > 0) AND
           (rev_lv_dhrs > 0)) OR ((prev_liab_bill_fee_hr > 0) AND
           (rev_fees > 0) AND (rev_lv_dhrs > 0)) THEN
          FOR rev_penalty_rec IN rev_penalty_to_flat_c LOOP

            /* 1-BCLLA, to include the condition below to make sure that prev_OL is no longer needed   */
            /* If there is No Overload calculation needed, simply initialize prev_rule_OL_start_hr_tui */
            /* and fee to ZERO.                                                                        */
            IF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs <
               prev_rule_OL_start_hr_tui) THEN
              prev_rule_OL_start_hr_tui := 0;
            END IF;
            IF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs <
               prev_rule_OL_start_hr_fee) THEN
              prev_rule_OL_start_hr_fee := 0;
            END IF;
            /* 1-1BCLLA End */

            IF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs >=
               prev_rule_OL_start_hr_tui) AND
               ((prev_liab_bill_tui_hr > 0) AND (rev_tuit > 0) AND
               (rev_lv_dhrs > 0))
              --- 7.4.0.3
               and prev_rule_OL_start_hr_tui > 0 AND
               (SWAP_RBC_MORE_DROPPED) THEN
              prev_liab_bill_tui_hr := prev_liab_bill_tui_hr - ((rev_penalty_rec.sfrfaud_rule_liable_bill_hrs -
                                       prev_rule_OL_start_hr_tui) *
                                       rev_tuit);

              -- 1-1BCLLA start
              IF prev_liab_bill_tui_hr < 0 THEN
                prev_liab_bill_tui_hr := 0;
              END IF;
              -- 1-1BCLLA end

            END IF;
            IF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs >
               prev_rule_OL_start_hr_fee) AND
               ((prev_liab_bill_fee_hr > 0) AND (rev_fees > 0) AND
               (rev_lv_dhrs > 0))
              --- 7.4.0.3
               and prev_rule_OL_start_hr_fee > 0 AND
               (SWAP_RBC_MORE_DROPPED) THEN
              prev_liab_bill_fee_hr := prev_liab_bill_fee_hr - ((rev_penalty_rec.sfrfaud_rule_liable_bill_hrs -
                                       prev_rule_OL_start_hr_fee) *
                                       rev_fees);

              -- 1-1BCLLA start
              IF prev_liab_bill_fee_hr < 0 THEN
                prev_liab_bill_fee_hr := 0;
              END IF;
              -- 1-1BCLLA end

            END IF;
            EXIT;
          END LOOP;
        END IF;
        --save the prev_flat_OL_liab_tui/fee_hr in a bucket, in case the assessment will still remain in flat.
        above_prev_oload_OL_tui := prev_liab_bill_tui_hr;
        above_prev_oload_OL_fee := prev_liab_bill_fee_hr;
        p_print_dbms('save prev flat ol: ' || above_prev_oload_OL_tui ||
                     ' fee: ' || above_prev_oload_OL_fee);
      END IF;
      p_print_dbms('prev liab carryover after aove  pen: ' ||
                   prev_liab_bill_tui_hr);
      p_print_dbms('end of flat starting  liab hrs: ' ||
                   starting_liab_bill_hr_tui);
    end p_flat_starting_liability;

    ----  process the courses and determine liabilty for the type.  Only if there were flat, drops and rbc does the
    ---  liabilty start out as non zero. That starting liability is calculated in p_flat_starting_liability
    procedure p_process_course_liability is

    begin
      --104171, initialize the following.
      DROPS_SUCCESSION_BROKEN := FALSE;
      tui_flat_cnt            := 0;
      tui_waiv_flat_cnt       := 0;
      fee_flat_cnt            := 0;
      fee_waiv_flat_cnt       := 0;
      save_tui_percentage     := 0;
      save_fee_percentage     := 0;
      --- 7.4.0.1 initialize flag
      REVERSE_ZEROTONINE := TRUE;

      lv_fees_rec           := null;
      lv_course_records_ref := f_course_records(term_in       => term_in,
                                                pidm_in       => pidm_in,
                                                type_in       => type_in,
                                                type_value_in => curr_type_code);
      LOOP
        FETCH lv_course_records_ref
          into lv_fees_rec;

        EXIT WHEN lv_course_records_ref%NOTFOUND;
        -- get the totals for the course level grouping

        t_crn                  := lv_fees_rec.r_crn;
        t_ptrm_code            := lv_fees_rec.r_ptrm_cde;
        t_rsts_code            := lv_fees_rec.r_rsts_cde;
        t_rsts_date            := lv_fees_rec.r_rsts_date;
        t_reg_bill_hr          := lv_fees_rec.r_REG_BILL_HR;
        t_bill_hr_tuit         := lv_fees_rec.r_BILL_HR_TUIT;
        t_bill_hr_fees         := lv_fees_rec.r_BILL_HR_FEES;
        t_reg_waiv_hr          := lv_fees_rec.r_REG_WAIV_HR;
        t_waiv_hr_tuit         := lv_fees_rec.r_WAIV_HR_TUIT;
        t_waiv_hr_fees         := lv_fees_rec.r_WAIV_HR_FEES;
        t_tuit_liab_percentage := lv_fees_rec.r_TUIT_LIAB_PERCENTAGE;
        t_fees_liab_percentage := lv_fees_rec.r_FEES_LIAB_PERCENTAGE;

        p_print_dbms('----------------------------------------------');
        p_print_dbms('Type: ' || type_in || ' value: ' || curr_type_code ||
                     ' CRN:RSTS CODE:HR_TUIT::LIAB%: ' || t_crn || ':' ||
                     t_rsts_code || ':' || t_bill_hr_tuit || ':' ||
                     t_tuit_liab_percentage || ' t_bill_hr_fees: ' ||
                     t_bill_hr_fees || ' t_fees_liab_percentage: ' ||
                     t_fees_liab_percentage || ' waivhr: ' ||
                     t_waiv_hr_tuit || ' start liab: ' ||
                     t_liab_bill_hr_tuit || ' waiv: ' ||
                     t_liab_waiv_hr_tuit || ' start fee: ' ||
                     t_liab_bill_hr_fees);
        p_print_dbms('rsts date: ' ||
                     to_char(t_rsts_date, 'DD-MON-YYYY HH24:MI:SS'));
        p_print_dbms('sum ndrop bill hr tui a ' || sum_ndrop_bill_hr_tuit);
        p_print_dbms('tot liab bill hr tuit a ' || tot_liab_bill_hr_tuit ||
                     ' waived: ' || tot_liab_waiv_hr_tuit);
        p_print_dbms('tot liab bill hr fees c ' || tot_liab_bill_hr_fees ||
                     ' waived: ' || tot_liab_waiv_hr_fees);
        p_print_dbms('tui_nondrop_bill_hr : ' || tui_nondrop_bill_hr);
        p_print_dbms('student type liab at start of course: ' ||
                     t_stud_liab_bill_hr_tuit);
        ---  7.4.0.1 if we have multiple refund periods, and we leave the flat
        ---  on one of them, we may not want to process the remaining drops as a flat
        IF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'N' THEN
          if (save_tui_percentage <> t_tuit_liab_percentage) and
             (save_tui_percentage <> 1 and t_tuit_liab_percentage <> 1) and
             NOT (SWAP_RBC_MORE_DROPPED) then
            tui_flat_cnt      := 0;
            tui_waiv_flat_cnt := 0;
            -- are we at flat still
            if (PREV_FLAT_HR_RULE_MET) and (save_tui_percentage <> 1) and
               NOT (LAST_ASSESSMENT_HANDLED_DD) and
               (t_liab_bill_hr_tuit < prev_rule_from_flat_hr_tui) THEN
              RECONSIDER_FLAT_TUIT := FALSE;
            end if;
            save_tui_percentage := t_tuit_liab_percentage;
          end if;
          if (save_fee_percentage <> t_fees_liab_percentage) and
             (save_fee_percentage <> 1 and t_fees_liab_percentage <> 1) and
             NOT (SWAP_RBC_MORE_DROPPED) then
            fee_flat_cnt      := 0;
            fee_waiv_flat_cnt := 0;
            -- are we at flat still
            if (PREV_FLAT_HR_RULE_MET) and (save_fee_percentage <> 1) and
               NOT (LAST_ASSESSMENT_HANDLED_DD) and
               (t_liab_bill_hr_fees < prev_rule_from_flat_hr_fee) THEN
              RECONSIDER_FLAT_FEES := FALSE;
            end if;
            save_fee_percentage := t_fees_liab_percentage;
          end if;
        END IF;

        p_get_sfrstcr_data(pidm_in,
                           term_in,
                           act_date_io,
                           add_date_io,
                           first_act_io,
                           orig_crn_bill_hrs); --104171

        -- determine if one or multiple parts of term for the course level
        IF t_ptrm_code != prev_ptrm_code THEN
          IF prev_ptrm_code = 'x' THEN
            t_ptrm_rule := t_ptrm_code;
          ELSE
            t_ptrm_rule := 'C';
          END IF;
          prev_ptrm_code := t_ptrm_code;
        END IF;

        --       Defect 102188 Initialize
        tui_calculated_ind := FALSE;
        fee_calculated_ind := FALSE;

        /* ********************************************************************* */
        /*  Need to determine the hours the student is liable for up front for   */
        /*  both bill and waive hours since we don't yet know the DCAT code for  */
        /*  the detail code that will be processed for the assessment rule. The  */
        /*  liabilty for the course registration was determined and saved in     */
        /*  SFTFEES when each registration record was evaluated. The student is  */
        /*  either liable for 100% of the hours (no WD) or a percentage of the   */
        /*  hours if the course was dropped (was WD) during a refund period. If  */
        /*  the course was WD and the refund percentage ends up being 0% for the */
        /*  period, the student will be liable for 100% of the hours.            */
        /* ********************************************************************* */
        tot_reg_bill_hr := tot_reg_bill_hr + t_reg_bill_hr;
        tot_reg_waiv_hr := tot_reg_waiv_hr + t_reg_waiv_hr;

        IF (PREV_FLAT_HR_RULE_MET) THEN
          -- will never be TRUE for RBT

          /* *********************************************************************** */
          /* Defect 102188 to add TUI and FEE DCAT codes in p_calc_flat_hr_liability */
          /* prev_rule_from_flat_hr_<DCAT>                                           */
          /* prev_rule_OL_start_hr_<DCAT>                                            */
          /* last_assess_met_flat_ind_<DCAT>                                         */
          /* *********************************************************************** */
          --- tui nondrop bill hr is  the liab hrs from the previous faud record  ,
          --- the sum ndrop is total of all drops at 0 or 100 percent
          p_print_dbms('before calc of tui non drop: ' ||
                       tui_nondrop_bill_hr || ' sum ndrop hrs: ' ||
                       sum_ndrop_bill_hr_tuit);

          tui_nondrop_bill_hr := tui_nondrop_bill_hr +
                                 sum_ndrop_bill_hr_tuit;
          tui_nondrop_waiv_hr := tui_nondrop_waiv_hr +
                                 sum_ndrop_waiv_hr_tuit;
          fee_nondrop_bill_hr := fee_nondrop_bill_hr +
                                 sum_ndrop_bill_hr_fees;
          fee_nondrop_waiv_hr := fee_nondrop_waiv_hr +
                                 sum_ndrop_waiv_hr_fees;

          p_print_dbms('tui_nondrop_bill_hr is set equal to the value from the D or N bill hrs on faud plus 1perc drop hrs');
          p_print_dbms('Before P_CALC: tui_nondrop_bill_hr + sum_ndrop_bill_hr_tuit :' ||
                       tui_nondrop_bill_hr || ':' ||
                       sum_ndrop_bill_hr_tuit);
          p_print_dbms('==curr_liab_bill_hr_tuit, curr_liab_bill_hr_fees, curr_liab_OL_bill_hr_tuit: ' ||
                       curr_liab_bill_hr_tuit || ':' ||
                       curr_liab_bill_hr_fees || ':' ||
                       curr_liab_OL_bill_hr_tuit);
          p_print_dbms('==prev_rule_OL_start_hr_tui: ' ||
                       prev_rule_OL_start_hr_tui);
          p_print_dbms('non drop fees before pcalcflat: ' ||
                       fee_nondrop_bill_hr);
          p_print_dbms('student type liab before p_calc_flat_hr: ' ||
                       t_stud_liab_bill_hr_tuit);
          p_print_dbms('===============');
          p_calc_flat_hr_liability(pidm_in,
                                   term_in,
                                   tui_nondrop_bill_hr,
                                   tui_nondrop_waiv_hr,
                                   fee_nondrop_bill_hr,
                                   fee_nondrop_waiv_hr,
                                   curr_liab_OL_bill_hr_tuit,
                                   curr_liab_OL_waiv_hr_tuit,
                                   curr_liab_flat_bill_hr_tuit,
                                   curr_liab_flat_waiv_hr_tuit,
                                   curr_liab_bill_hr_tuit,
                                   curr_liab_waiv_hr_tuit,
                                   curr_liab_OL_bill_hr_fees,
                                   curr_liab_OL_waiv_hr_fees,
                                   curr_liab_flat_bill_hr_fees,
                                   curr_liab_flat_waiv_hr_fees,
                                   curr_liab_bill_hr_fees,
                                   curr_liab_waiv_hr_fees,
                                   prev_rule_from_flat_hr_tui,
                                   prev_rule_OL_start_hr_tui,
                                   last_assess_met_flat_ind_tui,
                                   prev_rule_from_flat_hr_fee,
                                   prev_rule_OL_start_hr_fee,
                                   last_assess_met_flat_ind_fee,
                                   tui_calculated_ind,
                                   fee_calculated_ind,
                                   tui_flat_cnt,
                                   tui_waiv_flat_cnt,
                                   fee_flat_cnt,
                                   fee_waiv_flat_cnt);

          p_print_dbms('After P_CALC 1: tui_nondrop_bill_hr - sum_ndrop_bill_hr_tuit :' ||
                       tui_nondrop_bill_hr || ':' ||
                       sum_ndrop_bill_hr_tuit);
          p_print_dbms('==curr_liab_bill_hr_tuit, curr_liab_bill_hr_fees, curr_liab_OL_bill_hr_tuit: ' ||
                       curr_liab_bill_hr_tuit || ':' ||
                       curr_liab_bill_hr_fees || ':' ||
                       curr_liab_OL_bill_hr_tuit);
          p_print_dbms('==prev_rule_OL_start_hr_tui: ' ||
                       prev_rule_OL_start_hr_tui);
          p_print_dbms('fee nondrop from p_calc_flat_hr liab: ' ||
                       fee_nondrop_bill_hr);

          --104070, if swapping is on, and there are swapping happened.  Make sure that the current liability
          --is being calcualted properly.  fee/tui_below_flat_pcent is a misnomer.  they are actually the liable %s
          --coming from SWAPPING.  they do not mean below flat all the time, they could be at flat that came from OL.
          p_print_dbms('curr_liab 0: ' || curr_liab_OL_bill_hr_tuit ||
                       ' fees: ' || curr_liab_OL_bill_hr_fees);
          IF (SWAP_RBC_MORE_DROPPED) and
             (before_swap_hours >= prev_rule_OL_start_hr_tui) THEN
            /* 1-1BCLLA, add an IF condition */
            p_print_dbms('orig_tui_ndrop_billhr, rev_dropped_hrs, prev_rule_OL_start_hr_tui, orig_tui_ndrop_billhr: ' ||
                         orig_tui_ndrop_billhr || ':' || rev_dropped_hrs || ':' ||
                         prev_rule_OL_start_hr_tui || ':' ||
                         orig_tui_ndrop_billhr);
            p_print_dbms('is tui nondrop bill hr < prev rule ol start tui: ' ||
                         tui_nondrop_bill_hr || ' < ' ||
                         prev_rule_OL_start_hr_tui || ' or feess: ' ||
                         fee_nondrop_bill_hr || ' < ' ||
                         prev_rule_OL_start_hr_fee);
            --- 7.4.0.1 split the fee and tui into separate statements
            IF (tui_nondrop_bill_hr < prev_rule_OL_start_hr_tui) THEN
              IF ((orig_tui_ndrop_billhr + rev_dropped_hrs) >
                 prev_rule_OL_start_hr_tui) AND
                 (prev_rule_OL_start_hr_tui > 0) AND
                 (curr_liab_OL_bill_hr_tuit > 0) THEN
                curr_liab_OL_bill_hr_tuit := (orig_tui_ndrop_billhr -
                                             prev_rule_OL_start_hr_tui) *
                                             tui_below_flat_pcent;

                curr_liab_OL_waiv_hr_tuit := (orig_tui_ndrop_waivhr -
                                             prev_rule_OL_start_hr_tui) *
                                             tui_below_flat_pcent;
              END IF;
              FOR rev_penalty_rec IN rev_penalty_to_flat_c LOOP
                IF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs >
                   prev_rule_OL_start_hr_tui) AND
                   (prev_rule_OL_start_hr_tui > 0) AND
                   (curr_liab_OL_bill_hr_tuit > 0) THEN
                  curr_liab_OL_bill_hr_tuit := (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs -
                                               prev_rule_OL_start_hr_tui) *
                                               tui_below_flat_pcent;
                  curr_liab_OL_waiv_hr_tuit := (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs -
                                               prev_rule_OL_start_hr_tui) *
                                               tui_below_flat_pcent;
                  p_print_dbms('curr_liab 2a: ' ||
                               rev_penalty_rec.sfrfaud_rule_liable_bill_hrs || ':' ||
                               prev_rule_from_flat_hr_tui);
                  p_print_dbms('curr_liab 2: ' ||
                               curr_liab_OL_bill_hr_tuit || ':' ||
                               rev_penalty_rec.sfrfaud_rule_liable_bill_hrs || ':' ||
                               prev_rule_OL_start_hr_tui || ':' ||
                               tui_below_flat_pcent);

                END IF;

                EXIT;
              END LOOP;
            END IF;
            IF (fee_nondrop_bill_hr < prev_rule_OL_start_hr_fee) THEN
              IF ((orig_fee_ndrop_billhr + rev_dropped_hrs) >
                 prev_rule_OL_start_hr_fee) AND
                 (prev_rule_OL_start_hr_fee > 0) AND
                 (curr_liab_OL_bill_hr_fees > 0) THEN
                curr_liab_OL_bill_hr_fees := (orig_fee_ndrop_billhr -
                                             prev_rule_OL_start_hr_fee) *
                                             fee_below_flat_pcent;
                curr_liab_OL_waiv_hr_fees := (orig_fee_ndrop_waivhr -
                                             prev_rule_OL_start_hr_fee) *
                                             fee_below_flat_pcent;
                p_print_dbms('curr_liab 1: ' || curr_liab_OL_bill_hr_tuit ||
                             ' = ' || orig_tui_ndrop_billhr || '  -  ' ||
                             prev_rule_OL_start_hr_tui || '  * ' ||
                             tui_below_flat_pcent);
              END IF;

              FOR rev_penalty_rec IN rev_penalty_to_flat_c LOOP
                IF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs >
                   prev_rule_OL_start_hr_fee) AND
                   (prev_rule_OL_start_hr_fee > 0) AND
                   (curr_liab_OL_bill_hr_fees > 0) THEN
                  --1-1BCLLAIF (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs > prev_rule_from_flat_hr_fee) AND (prev_rule_OL_start_hr_fee > 0) AND (curr_liab_OL_bill_hr_fees > 0) THEN
                  curr_liab_OL_bill_hr_fees := (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs -
                                               prev_rule_OL_start_hr_fee) *
                                               fee_below_flat_pcent;
                  curr_liab_OL_waiv_hr_fees := (rev_penalty_rec.sfrfaud_rule_liable_bill_hrs -
                                               prev_rule_OL_start_hr_fee) *
                                               fee_below_flat_pcent;
                END IF;
                EXIT;
              END LOOP;
            END IF; --1-1BCLLA
          END IF;

          --104070, if swapping is on, and the DROPS and ADDS are EQUAL, then let there be no curr_liab_flat_bill_hr_tuit
          -- so, set curr_liab_flat_bill_hr_tuit to 0
          -- 8.2.1  reset these  if the last assessment was before the first
          --   swap and they never left the flat at the end of the swaps (stayed within the flat
          --   because of the swap)
          p_print_dbms('before swap rbc equal or swap and assess before swaps, reset the cur flat  liab: ' ||
                       curr_liab_flat_bill_hr_tuit || ' ol: ' ||
                       curr_liab_OL_bill_hr_tuit || '   new liab: ' ||
                       tui_nondrop_bill_hr || ' prev flat: ' ||
                       prev_rule_from_flat_hr_tui || ' start swap: ' ||
                       to_char(first_swap_date, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' lst: ' ||
                       to_char(sfbetrm_rec.sfbetrm_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS'));
          p_print_dbms('overload: ' || prev_rule_OL_start_hr_tui ||
                       ' orig hrs: ' || save_orig_hrs);
          IF (SWAP_RBC_EQUAL) OR
             (((SWAP_RBC_MORE_DROPPED) OR (SWAP_RBC_MORE_ADDS)) AND
             (first_swap_date > sfbetrm_rec.sfbetrm_assessment_date) AND
             (tui_nondrop_bill_hr >= prev_rule_from_flat_hr_tui
             --- 8.3 and not above the overload
             and save_orig_hrs < prev_rule_OL_start_hr_tui)) THEN
            p_print_dbms('reset curr liab flat and curr liab ol to 0 for swap or swap rbc equal');

            curr_liab_flat_bill_hr_tuit := 0;
            curr_liab_flat_bill_hr_fees := 0;
            curr_liab_OL_bill_hr_tuit   := 0;
            curr_liab_OL_bill_hr_fees   := 0;
          END IF;

          p_print_dbms('tui_nondrop_bill_hr - sum_ndrop_bill_hr_tuit: ' ||
                       tui_nondrop_bill_hr || ':' ||
                       sum_ndrop_bill_hr_tuit);
          p_print_dbms('After P_CALC 2: tui_nondrop_bill_hr - sum_ndrop_bill_hr_tuit :' ||
                       tui_nondrop_bill_hr || ':' ||
                       sum_ndrop_bill_hr_tuit);

          tui_nondrop_bill_hr := tui_nondrop_bill_hr -
                                 sum_ndrop_bill_hr_tuit;
          tui_nondrop_waiv_hr := tui_nondrop_waiv_hr -
                                 sum_ndrop_waiv_hr_tuit;
          fee_nondrop_bill_hr := fee_nondrop_bill_hr -
                                 sum_ndrop_bill_hr_fees;
          fee_nondrop_waiv_hr := fee_nondrop_waiv_hr -
                                 sum_ndrop_waiv_hr_fees;

          --104070, number of total drops need to be identified to make sure that there
          --is a need to recalculate the bill hrs in case there are multiple DROPS.
          --   IF f_rsts_means_wd(t_rsts_code) and (tui_calculated_ind or fee_calculated_ind) THEN
          --Defect 1-1LD2RR, to add t_reg_bill_hr
          IF f_rsts_means_wd(t_rsts_code) and
             (tui_calculated_ind or fee_calculated_ind) and
             (t_reg_bill_hr > 0) THEN
            drops_count := drops_count + 1;
          END IF;

          -- Defect 104071, counts the number of drops in succession
          --- 7.4.0.1 1-2O637G if this is the assessment after an intermediate, check that the activity date
          ---  is between the original assessment and the intermediate assessment
          p_print_dbms('act date: ' ||
                       to_char(act_date_io, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' last ass: ' ||
                       to_char(sfbetrm_rec.sfbetrm_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS') || ' prev: ' ||
                       to_char(prev_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS'));

          IF ((prev_assessment_date is null and
             act_date_io > sfbetrm_rec.sfbetrm_assessment_date) OR
             (prev_assessment_date is not null and
             act_date_io between prev_assessment_date and
             sfbetrm_rec.sfbetrm_assessment_date)) AND
             (NOT (DROPS_SUCCESSION_BROKEN)) AND
             NOT (SWAP_RBC_MORE_DROPPED) THEN
            --               IF f_rsts_means_wd(t_rsts_code) THEN
            -- Defect 1-1LD2RR, added t_reg_bill_hr
            IF f_rsts_means_wd(t_rsts_code) then
              ----  8.1.1  defect  1-47D0BT and (t_reg_bill_hr > 0) THEN
              num_of_drops := num_of_drops + 1;
              p_print_dbms('Number of drops: ' || num_of_drops ||
                           ' rsts: ' || t_rsts_code || ' reg hrs: ' ||
                           t_reg_bill_hr);
              p_print_dbms('tui below the flat: ' ||
                           tui_below_flat_bill_hr || '  + ' ||
                           t_bill_hr_tuit);
              tui_below_flat_bill_hr := tui_below_flat_bill_hr +
                                        t_bill_hr_tuit;
              tui_below_flat_waiv_hr := tui_below_flat_waiv_hr +
                                        t_waiv_hr_tuit;
              fee_below_flat_bill_hr := fee_below_flat_bill_hr +
                                        t_bill_hr_fees;
              fee_below_flat_waiv_hr := fee_below_flat_waiv_hr +
                                        t_waiv_hr_fees;

              tui_below_flat_pcent := t_tuit_liab_percentage;
              fee_below_flat_pcent := t_fees_liab_percentage;
              --- 1-2O637G
              --   ELSE
              --      DROPS_SUCCESSION_BROKEN := TRUE;
            END IF;
          END IF;

          p_print_dbms('oload: ' || above_prev_oload_OL_tui ||
                       ' prev ol: ' || prev_liab_bill_tui_hr ||
                       ' tui non drop: ' || tui_nondrop_bill_hr ||
                       ' prev rule from flat : ' ||
                       prev_rule_from_flat_hr_tui || ' prv rule from OL: ' ||
                       prev_rule_OL_start_hr_tui || ' below prv flat ol: ' ||
                       below_prev_flat_OL_tui);
          p_print_dbms('oload fee: ' || above_prev_oload_OL_fee ||
                       ' prev ol: ' || prev_liab_bill_fee_hr ||
                       ' fee non drop: ' || fee_nondrop_bill_hr ||
                       ' prev rule from flat : ' ||
                       prev_rule_from_flat_hr_fee || ' prv rule from OL: ' ||
                       prev_rule_OL_start_hr_fee || ' below prv flat ol: ' ||
                       below_prev_flat_OL_fee);

          --- 7.4.0.2   add the below flat ol to the prev flat ol

          IF tui_nondrop_bill_hr < prev_rule_from_flat_hr_tui THEN
            prev_liab_bill_tui_hr := below_prev_flat_OL_tui;
          ELSE
            IF tui_nondrop_bill_hr < prev_rule_OL_start_hr_tui THEN
              prev_liab_bill_tui_hr := above_prev_flat_OL_tui;
            ELSE
              prev_liab_bill_tui_hr := above_prev_oload_OL_tui;
            END IF;
          END IF;
          IF fee_nondrop_bill_hr < prev_rule_from_flat_hr_fee THEN
            prev_liab_bill_fee_hr := below_prev_flat_OL_fee;
          ELSE
            IF fee_nondrop_bill_hr < prev_rule_OL_start_hr_fee THEN
              prev_liab_bill_fee_hr := above_prev_flat_OL_fee;
            ELSE
              prev_liab_bill_fee_hr := above_prev_oload_OL_fee;
            END IF;
          END IF;

          -- Defects 102188 and 99521
          --- 7.4.0.1 1-2O637G  do not insert into the multi table unless also a drop and an intermediate assessment
          ---  was previously just IF (tui_calculated_ind) then
          p_print_dbms('pv#1 before check to calc liable hrs, prev assessment: ' ||
                       to_char(prev_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS') ||
                       ' t_liab_bill_hr_tuit: ' || t_liab_bill_hr_tuit ||
                       ' t_liab_bill_hr_fees: ' || t_liab_bill_hr_fees ||
                       ' act_date_io: ' ||
                       to_char(act_date_io, 'DD-MON-YYYY HH24:MI:SS') ||
                       ' last: ' ||
                       to_char(last_assessment_date,
                               'DD-MON-YYYY HH24:MI:SS'));

          IF (tui_calculated_ind) and (NOT (LAST_ASSESSMENT_HANDLED_DD) OR
             ((LAST_ASSESSMENT_HANDLED_DD) AND
             (((f_rsts_means_wd(t_rsts_code) and
             prev_assessment_date is not null) or
             prev_assessment_date is null) OR
             NOT (f_rsts_means_wd(t_rsts_code))))) THEN

            multiple_tui_ctr := multiple_tui_ctr + 1;
            multiple_tui_liab(multiple_tui_ctr) := t_tuit_liab_percentage;
            multiple_tui_bill_hr(multiple_tui_ctr) := t_bill_hr_tuit;
            multiple_tui_waiv_hr(multiple_tui_ctr) := t_waiv_hr_tuit;
            p_print_dbms('cal new lib bill hr tui: ' ||
                         t_liab_bill_hr_tuit || ' := ' ||
                         tui_nondrop_bill_hr || ' + ' ||
                         curr_liab_OL_bill_hr_tuit || ' +  ' ||
                         curr_liab_flat_bill_hr_tuit || ' + ' ||
                         curr_liab_bill_hr_tuit || ' + ' ||
                         prev_liab_bill_tui_hr);

            t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                   curr_liab_OL_bill_hr_tuit +
                                   curr_liab_flat_bill_hr_tuit +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr;
            t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                   curr_liab_OL_waiv_hr_tuit +
                                   curr_liab_flat_waiv_hr_tuit +
                                   curr_liab_waiv_hr_tuit +
                                   prev_liab_bill_tui_hr;
            p_print_dbms('t_liab_bill_hr_tuit 2: ' || t_liab_bill_hr_tuit);
            p_print_dbms('multiple_tui_ctr 2: ' || multiple_tui_ctr);
            p_print_dbms('tui_nondrop_bill_hr: ' || tui_nondrop_bill_hr || ':' ||
                         'curr_liab_OL_bill_hr_tuit: ' ||
                         curr_liab_OL_bill_hr_tuit || ':' ||
                         'curr_liab_flat_bill_hr_tuit: ' ||
                         curr_liab_flat_bill_hr_tuit || ':' ||
                         'curr_liab_bill_hr_tuit: ' ||
                         curr_liab_bill_hr_tuit || ':' ||
                         'prev_liab_bill_tui_hr  : ' ||
                         prev_liab_bill_tui_hr);
            if type_in = STUDENT_TYPE then
              t_stud_liab_bill_hr_tuit := t_liab_bill_hr_tuit;
              t_stud_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit;
              p_print_dbms('student type liab1: ' ||
                           t_stud_liab_bill_hr_tuit);
            end if;

          END IF;
          --- 7.4.0.1 1-2O637G  do not insert into the multi table unless also a drop and an intermediate assessment
          ---  was previously just IF (fee_calculated_ind) then
          IF (fee_calculated_ind) and (NOT (LAST_ASSESSMENT_HANDLED_DD) OR
             ((LAST_ASSESSMENT_HANDLED_DD) AND
             (((f_rsts_means_wd(t_rsts_code) and
             prev_assessment_date is not null) or
             prev_assessment_date is null) OR
             NOT (f_rsts_means_wd(t_rsts_code))))) THEN
            multiple_fee_ctr := multiple_fee_ctr + 1;
            multiple_fee_liab(multiple_fee_ctr) := t_fees_liab_percentage;
            multiple_fee_bill_hr(multiple_fee_ctr) := t_bill_hr_fees;
            multiple_fee_waiv_hr(multiple_fee_ctr) := t_waiv_hr_fees;
            t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                   curr_liab_OL_bill_hr_fees +
                                   curr_liab_flat_bill_hr_fees +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr;
            t_liab_waiv_hr_fees := fee_nondrop_waiv_hr +
                                   curr_liab_OL_waiv_hr_fees +
                                   curr_liab_flat_waiv_hr_fees +
                                   curr_liab_waiv_hr_fees +
                                   prev_liab_bill_fee_hr;
            if type_in = STUDENT_TYPE then
              t_stud_liab_bill_hr_fees := t_liab_bill_hr_fees;
              t_stud_liab_waiv_hr_fees := t_liab_waiv_hr_fees;

            end if;
          END IF;

        ELSE
          --- 1-2O637G  Always include if not a flat, or if not an intermedate
          --   if this is an intermediate assessment include all with add date not activity date
          ---  less then last assessment. We do not want to include courses added the same
          --   time as the DD, and if this is an intermediate without DD do it
          --  7.4.0.2 105831  Variable credits.  Do not include if its a new add after or at the same time as the
          --   variable credit hour change and it isnt the variable credit hour change
          p_print_dbms('before p calc reg, t bill hrs: ' || t_bill_hr_tuit ||
                       ' org bill: ' || orig_crn_bill_hrs || ' vari act: ' ||
                       variable_act_date);
          p_print_dbms('add date: ' || to_char(first_act_io, 'HH24:MI:SS') ||
                       ' DD date: ' ||
                       to_char(last_dd_activity_date, 'HH24:MI:SS') ||
                       ' org bill: ' || orig_crn_bill_hrs);
          p_print_dbms('var date: ' ||
                       to_char(variable_act_date, 'DD-MON-YYYy HH24:MI:SS'));
          IF ((NOT (CHECK_FLAT) and NOT (DO_INTERMEDIATE)) OR
             NOT (DO_INTERMEDIATE) OR
             ((DO_INTERMEDIATE) and
             ((first_act_io < last_dd_activity_date and
             last_dd_activity_date is not null) OR
             (variable_act_date is not null and
             ((first_act_io < variable_act_date) or
             (first_act_io >= variable_act_date and
             nvl(orig_crn_bill_hrs, t_bill_hr_tuit) <> t_bill_hr_tuit))) OR
             (last_dd_activity_date is null and
             variable_act_date is null)))) THEN

            p_print_dbms('P_CALC_REG_HR_LIABILITY NOW, NOT FLAT or NOT INTER OR IS INTER AND DATES');
            p_print_dbms('prev liab bill hr ' || prev_liab_bill_hr);
            p_print_dbms('orig fee ndrop billhr ' || orig_fee_ndrop_billhr);
            p_print_dbms('tot reg bill hr ' || tot_reg_bill_hr);
            p_print_dbms('tot liab bill hr tuit ' || tot_liab_bill_hr_tuit);
            p_print_dbms('starting liab bill hr tui ' ||
                         starting_liab_bill_hr_tui);
            p_print_dbms('starting liab bill hr fee ' ||
                         starting_liab_bill_hr_fee);
            p_print_dbms('orig tui ndrop billhr ' || orig_tui_ndrop_billhr);
            p_print_dbms('orig fee ndrop billhr ' || orig_fee_ndrop_billhr);
            p_print_dbms('num of drops ' || num_of_drops);

            p_calc_reg_hr_liability(tot_reg_bill_hr,
                                    tot_liab_bill_hr_tuit,
                                    tot_liab_bill_hr_fees,
                                    tot_reg_waiv_hr,
                                    tot_liab_waiv_hr_tuit,
                                    tot_liab_waiv_hr_fees);
            p_print_dbms('after p calc reg hr liab, tot_liab_bill_hr_tuit: ' ||
                         tot_liab_bill_hr_tuit);
          END IF;

        END IF;
        p_print_dbms('end of crse,  tot liab bill hr tuit a ' ||
                     tot_liab_bill_hr_tuit || ' ' ||
                     'tot liab bill hr fees c ' || tot_liab_bill_hr_fees);
        p_print_dbms('end of processing course ' || t_crn || 'totl liab: ' ||
                     tot_liab_bill_hr_tuit || ' tot waiv: ' ||
                     tot_liab_waiv_hr_tuit || ' tot fees: ' ||
                     tot_liab_bill_hr_fees);
        p_print_dbms('at end of course: t_liab_bill_hr_tuit : ' ||
                     t_liab_bill_hr_tuit || ' number of drops: ' ||
                     num_of_drops || '  t_liab_bill_hr_fees : ' ||
                     t_liab_bill_hr_fees);
        p_print_dbms('end up total non drop: ' || tui_nondrop_bill_hr ||
                     ' student liab hrs: ' || t_stud_liab_bill_hr_tuit);
      END LOOP; -- totals for courses by type

      CLOSE lv_course_records_ref;

      REVERSE_ZEROTONINE := FALSE;
      --- 7.4.0.4
      rbt_dd_exists := FALSE;
      rbt_wd_exists := FALSE;

      If NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' then
        if f_DD_since_last_assessment(pidm_in, term_in) then
          p_print_dbms('there was a dd, set reverser zero to true, srce ' ||
                       rbt_srce_code);
          REVERSE_ZEROTONINE := TRUE;
          rbt_dd_exists      := TRUE;
        else
          lv_find_any_wd := 'N';
          open find_any_wd_c;
          fetch find_any_wd_c
            into lv_find_any_wd;
          if find_any_wd_c%notfound then
            lv_find_any_wd := 'N';
            p_print_dbms('no crses are WD ');
          end if;
          close find_any_wd_c;
          if lv_find_any_wd = 'Y' then
            p_print_dbms('there are crses with WD ');
            REVERSE_ZEROTONINE := FALSE;
          else
            REVERSE_ZEROTONINE := TRUE;
            rbt_wd_exists      := TRUE;
            p_print_dbms('all crses are RE do not execute non swap revs penalty ');
          end if;

        end if;
      end if;
      p_print_dbms('------------- ' || type_in ||
                   '------------------------');
      p_print_dbms('return from p calc_hr_liablity');
      p_print_dbms('sum ndrop bill hr tui a ' || sum_ndrop_bill_hr_tuit);
      p_print_dbms('tot liab bill hr tuit a ' || tot_liab_bill_hr_tuit ||
                   ' stud liab: ' || t_stud_liab_bill_hr_tuit);
      p_print_dbms('tot liab bill hr fees c ' || tot_liab_bill_hr_fees ||
                   ' stu liab: ' || t_stud_liab_bill_hr_fees);

      /* *********************************************************************************** */
      /* Defect 104070                                                                       */
      /* IF (RBC-SWAP) and From FLAT to BELOW FLAT THEN                                      */
      /* *********************************************************************************** */
      IF (SWAP_RBC_MORE_DROPPED) AND
         ((((f_get_prev_nondrop_hrs(pidm_in,
                                    term_in,
                                    prev_rule_crse_waiv_ind_tui) +
         rev_dropped_hrs) > prev_rule_from_flat_hr_tui) AND
         (prev_rule_from_flat_hr_tui > 0)) OR
         (((f_get_prev_nondrop_hrs(pidm_in,
                                    term_in,
                                    prev_rule_crse_waiv_ind_fee) +
         rev_dropped_hrs) > prev_rule_from_flat_hr_fee) AND
         (prev_rule_from_flat_hr_fee > 0))) THEN
        p_print_dbms('swap rbc more dropped');

        CONTINUE_CALC := FALSE;
        p_print_dbms('Swap RBC more dropped Check if Cont Calc is true,prev rule: ' ||
                     prev_rule_from_flat_hr_tui || ' must be > then ' ||
                     tui_nondrop_bill_hr || ' or waiver ' ||
                     tui_nondrop_waiv_hr || ' swap date: ' || swap_date);
        -- 1-2JTK2Y  change > to >=
        p_print_dbms('Prv rule from flat: ' || prev_rule_from_flat_hr_tui ||
                     ' tuit non drop: ' || tui_nondrop_bill_hr ||
                     ' orig: ' || orig_tui_ndrop_billhr ||
                     ' new nondrop: ' || new_nondrop_hrs);
        p_print_dbms(' RBC SWAP More Dropped : ' ||
                     prev_rule_from_flat_hr_tui || ':' ||
                     tui_nondrop_bill_hr || ':' || tui_nondrop_waiv_hr ||
                     ' recalc: ' || recalc_penalty_hrs_tui ||
                     ' start swap hours: ' || start_swap_hours ||
                     ' before ' || before_swap_hours);
        -- need to check if the current assessment is now below the flat and not add back in the swapping penalty
        -- cont calc is really a flag for there was a prev flat, but now in the per credit
        -- do this only the first time we go into the flat

        --- 7.4.0.2 add check to make sure current assessment is still below the flat
        IF (prev_rule_from_flat_hr_tui >= tui_nondrop_bill_hr) THEN
          CONTINUE_CALC := TRUE;
        ELSE
          IF (prev_rule_from_flat_hr_tui >= tui_nondrop_waiv_hr) THEN
            CONTINUE_CALC := TRUE;
          ELSE
            CONTINUE_CALC := FALSE;
          END IF;
        END IF;
        --- 7.4.0.3 drop from OL to pre credit, then swap back into flat
        IF NOT (CONTINUE_CALC) AND
           ((before_swap_hours > prev_rule_OL_start_hr_tui and
           prev_rule_OL_start_hr_tui > 0) AND
           (start_swap_hours < prev_rule_from_flat_hr_tui or
           orig_tui_ndrop_billhr < prev_rule_from_flat_hr_tui) AND
           tui_nondrop_bill_hr > prev_rule_from_flat_hr_tui) then
          CONTINUE_CALC := TRUE;
        END IF;

        p_print_dbms(' RBC SWAP More Dropped : ' ||
                     prev_rule_from_flat_hr_tui || ':' ||
                     tui_nondrop_bill_hr || ':' || tui_nondrop_waiv_hr ||
                     ' recalc: ' || recalc_penalty_hrs_tui ||
                     ' start swap hours: ' || start_swap_hours ||
                     ' before ' || before_swap_hours || ' OL: ' ||
                     prev_rule_OL_start_hr_tui || ' Flat Liab: ' ||
                     curr_liab_flat_bill_hr_tuit || ' rev pdn: ' ||
                     rev_penalty_hrs_tui);
        IF DROPS THEN
          p_print_dbms(' DROP IS TRUE ');
        END IF;
        IF PREV_FLAT_HR_RULE_MET THEN
          p_print_dbms(' PREV_FLAT_HR_RULE_MET IS TRUE ');
        END IF;
        IF CONTINUE_CALC THEN
          p_print_dbms(' CONTINUE_CALC IS TRUE ');
        END IF;

        IF (DROPS) AND (PREV_FLAT_HR_RULE_MET) AND (CONTINUE_CALC) THEN

          t_liab_bill_hr_tuit := 0;
          t_liab_waiv_hr_tuit := 0;

          if before_swap_hours < prev_rule_from_flat_hr_tui then

            t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr;
            t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr;
          elsif before_swap_hours < prev_rule_OL_start_hr_tui AND
                start_swap_hours >= prev_rule_OL_start_hr_tui THEN

            t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr +
                                   curr_liab_flat_waiv_hr_tuit;
            t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr +
                                   curr_liab_flat_bill_hr_tuit;
          elsif ((before_swap_hours > prev_rule_OL_start_hr_tui and
                prev_rule_OL_start_hr_tui > 0) AND
                (start_swap_hours < prev_rule_from_flat_hr_tui or
                orig_tui_ndrop_billhr < prev_rule_from_flat_hr_tui) AND
                tui_nondrop_bill_hr > prev_rule_from_flat_hr_tui) then

            if tui_nondrop_bill_hr <= prev_rule_OL_start_hr_tui then
              -- only dropped to flat, not in OL anymore
              t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                     (before_swap_hours -
                                     prev_rule_OL_start_hr_tui) *
                                     rev_penalty_hrs_tui;
              t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                     (before_swap_hours -
                                     prev_rule_OL_start_hr_tui) *
                                     rev_penalty_hrs_tui;
            else
              --- dropped out of flat but back to OL
              if tui_nondrop_bill_hr >= before_swap_hours then
                t_liab_bill_hr_tuit := tui_nondrop_bill_hr;
                t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr;
              else
                t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                       (before_swap_hours -
                                       tui_nondrop_bill_hr) *
                                       rev_penalty_hrs_tui;
                t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                       (before_swap_hours -
                                       tui_nondrop_waiv_hr) *
                                       rev_penalty_hrs_tui;
              end if;
            end if;
          else
            t_liab_bill_hr_multiple := prev_rule_from_flat_hr_tui -
                                       tui_nondrop_bill_hr;
            t_liab_waiv_hr_multiple := prev_rule_from_flat_hr_tui -
                                       tui_nondrop_waiv_hr;
            t_liab_bill_hr_tuit     := t_liab_bill_hr_tuit +
                                       (t_liab_bill_hr_multiple *
                                       tui_below_flat_pcent);
            t_liab_waiv_hr_tuit     := t_liab_waiv_hr_tuit +
                                       (t_liab_waiv_hr_multiple *
                                       tui_below_flat_pcent);

            t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                   curr_liab_OL_bill_hr_tuit +
                                   t_liab_bill_hr_tuit +
                                   curr_liab_bill_hr_tuit;

            t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                   curr_liab_OL_waiv_hr_tuit +
                                   t_liab_waiv_hr_tuit +
                                   curr_liab_waiv_hr_tuit;
          end if;

          if type_in = STUDENT_TYPE then
            t_stud_liab_bill_hr_tuit := t_liab_bill_hr_tuit;
            t_stud_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit;
            p_print_dbms('student type liab2: ' ||
                         t_stud_liab_bill_hr_tuit);
          end if;
        END IF;

        CONTINUE_CALC := FALSE; --104070
        -- 1-2JTK2Y  change > to >=
        IF (prev_rule_from_flat_hr_fee >= fee_nondrop_bill_hr) THEN
          CONTINUE_CALC := TRUE;
        ELSE
          IF (prev_rule_from_flat_hr_fee >= fee_nondrop_waiv_hr) THEN
            CONTINUE_CALC := TRUE;
          ELSE
            CONTINUE_CALC := FALSE;
          END IF;
        END IF;
        --- 7.4.0.3 drop from OL to pre credit, then swap back into flat
        IF NOT (CONTINUE_CALC) AND
           ((before_swap_hours > prev_rule_OL_start_hr_fee and
           prev_rule_OL_start_hr_fee > 0) AND
           (start_swap_hours < prev_rule_from_flat_hr_fee or
           orig_fee_ndrop_billhr < prev_rule_from_flat_hr_fee) AND
           fee_nondrop_bill_hr > prev_rule_from_flat_hr_fee) then
          CONTINUE_CALC := TRUE;
        END IF;
        IF (DROPS) AND (PREV_FLAT_HR_RULE_MET) AND (CONTINUE_CALC) THEN
          t_liab_bill_hr_fees := 0;
          t_liab_waiv_hr_fees := 0;

          if start_swap_hours < prev_rule_from_flat_hr_fee then
            t_liab_waiv_hr_fees := fee_nondrop_waiv_hr +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr;
            t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr;
          elsif before_swap_hours < prev_rule_OL_start_hr_fee AND
                start_swap_hours >= prev_rule_OL_start_hr_fee THEN

            t_liab_waiv_hr_fees := tui_nondrop_waiv_hr +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr +
                                   curr_liab_flat_waiv_hr_fees;
            t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr +
                                   curr_liab_flat_bill_hr_fees;

          elsif ((before_swap_hours > prev_rule_OL_start_hr_fee and
                prev_rule_OL_start_hr_fee > 0) AND
                (start_swap_hours < prev_rule_from_flat_hr_fee or
                orig_fee_ndrop_billhr < prev_rule_from_flat_hr_fee) AND
                fee_nondrop_bill_hr > prev_rule_from_flat_hr_fee) then

            if fee_nondrop_bill_hr <= prev_rule_OL_start_hr_fee then
              -- only dropped to flat, not in OL anymore
              t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                     (before_swap_hours -
                                     prev_rule_OL_start_hr_fee) *
                                     rev_penalty_hrs_fee;
              t_liab_waiv_hr_fees := fee_nondrop_waiv_hr +
                                     (before_swap_hours -
                                     prev_rule_OL_start_hr_fee) *
                                     rev_penalty_hrs_fee;
            else
              --- dropped out of flat but back to OL
              if fee_nondrop_bill_hr >= before_swap_hours then
                t_liab_bill_hr_fees := fee_nondrop_bill_hr;
                t_liab_waiv_hr_fees := fee_nondrop_waiv_hr;
              else
                t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                       (before_swap_hours -
                                       fee_nondrop_bill_hr) *
                                       rev_penalty_hrs_fee;
                t_liab_waiv_hr_fees := fee_nondrop_waiv_hr +
                                       (before_swap_hours -
                                       fee_nondrop_waiv_hr) *
                                       rev_penalty_hrs_fee;
              end if;
            end if;
          else
            t_liab_bill_hr_multiple := prev_rule_from_flat_hr_fee -
                                       fee_nondrop_bill_hr;
            t_liab_waiv_hr_multiple := prev_rule_from_flat_hr_fee -
                                       fee_nondrop_waiv_hr;

            t_liab_bill_hr_fees := t_liab_bill_hr_fees +
                                   (t_liab_bill_hr_multiple *
                                   fee_below_flat_pcent);
            t_liab_waiv_hr_fees := t_liab_waiv_hr_fees +
                                   (t_liab_waiv_hr_multiple *
                                   fee_below_flat_pcent);

            t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                   curr_liab_OL_bill_hr_fees +
                                   t_liab_bill_hr_fees +
                                   curr_liab_bill_hr_fees;
            t_liab_waiv_hr_fees := fee_nondrop_waiv_hr +
                                   curr_liab_OL_waiv_hr_fees +
                                   t_liab_waiv_hr_fees +
                                   curr_liab_waiv_hr_fees;
          end if;
          if type_in = STUDENT_TYPE then
            t_stud_liab_bill_hr_fees := t_liab_bill_hr_fees;
            t_stud_liab_waiv_hr_fees := t_liab_waiv_hr_fees;
          end if;
        END IF;

      ELSE
        p_print_dbms('not swap rbc more drops, num drops: ' ||
                     num_of_drops);
        p_print_dbms('t_liab_bill_hr_tuit orig_tui_ndrop_billhr tui_below_flat_bill_hr4b : ' ||
                     t_liab_bill_hr_tuit || ':' || orig_tui_ndrop_billhr || ':' ||
                     tui_below_flat_bill_hr);
        p_print_dbms('MULTIPLE DROPS IN SUCCESSION-Num of Drops: ' ||
                     num_of_drops);

        /* If MULTIPLE DROPS IN SUCCESSION AND FROM FLAT to BELOW FLAT, THEN Assess ALL DROPS FIRST */
        /* This ELSE part will only happen if DROPS were done first before ADDs                     */
        IF (num_of_drops > 1) AND
           ((((prev_rule_from_flat_hr_tui - tui_nondrop_waiv_hr) >= 0) OR
           ((prev_rule_from_flat_hr_tui - tui_nondrop_bill_hr) >= 0)) OR
           (((prev_rule_from_flat_hr_fee - fee_nondrop_waiv_hr) >= 0) OR
           ((prev_rule_from_flat_hr_fee - fee_nondrop_bill_hr) >= 0))) THEN

          CONTINUE_CALC := FALSE; --104070   1-1AZHFF change > to >=
          IF (prev_rule_from_flat_hr_tui >= tui_nondrop_bill_hr) AND
             (starting_liab_bill_hr_tui > 0) AND
             (orig_tui_ndrop_billhr >= prev_rule_from_flat_hr_tui) THEN
            -- 1-1DSEZH
            CONTINUE_CALC := TRUE;
          ELSE
            IF (prev_rule_from_flat_hr_tui >= tui_nondrop_waiv_hr) AND
               (starting_liab_bill_hr_tui > 0) AND
               (orig_tui_ndrop_waivhr >= prev_rule_from_flat_hr_tui) THEN
              -- 1-1DSEZH
              CONTINUE_CALC := TRUE;
            ELSE
              CONTINUE_CALC := FALSE;
            END IF;
          END IF;
          --- 7.4.0.1 reconsider flat if refund percentages changed
          IF NOT (RECONSIDER_FLAT_TUIT) then
            CONTINUE_CALC := FALSE;
          END IF;
          IF (DROPS) AND (PREV_FLAT_HR_RULE_MET) AND (CONTINUE_CALC) THEN
            t_liab_bill_hr_tuit     := orig_tui_ndrop_billhr -
                                       tui_below_flat_bill_hr;
            t_liab_waiv_hr_tuit     := orig_tui_ndrop_waivhr -
                                       tui_below_flat_waiv_hr;
            t_liab_bill_hr_multiple := prev_rule_from_flat_hr_tui -
                                       t_liab_bill_hr_tuit;
            t_liab_waiv_hr_multiple := prev_rule_from_flat_hr_tui -
                                       t_liab_waiv_hr_tuit;

            t_liab_bill_hr_tuit := t_liab_bill_hr_tuit +
                                   (t_liab_bill_hr_multiple *
                                   tui_below_flat_pcent);
            t_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit +
                                   (t_liab_waiv_hr_multiple *
                                   tui_below_flat_pcent);
            -- 1-2O637G do we want to replace the liability if this is the assessment after the intermediate
            IF NOT (LAST_ASSESSMENT_HANDLED_DD) THEN
              p_print_dbms('num drops liab3: ' || num_of_drops || ' cnt: ' ||
                           multiple_tui_ctr);
              WHILE multiple_tui_ctr > num_of_drops LOOP
                t_liab_bill_hr_tuit := t_liab_bill_hr_tuit +
                                       (multiple_tui_bill_hr(multiple_tui_ctr) *
                                       multiple_tui_liab(multiple_tui_ctr));
                t_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit +
                                       (multiple_tui_waiv_hr(multiple_tui_ctr) *
                                       multiple_tui_liab(multiple_tui_ctr));
                multiple_tui_ctr    := multiple_tui_ctr - 1;
                p_print_dbms('multiple tui ctr: ' || multiple_tui_ctr ||
                             ' tu: ' || t_liab_bill_hr_tuit);

              END LOOP;
            END IF;
            t_liab_bill_hr_tuit := curr_liab_OL_bill_hr_tuit +
                                   t_liab_bill_hr_tuit +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr;
            t_liab_waiv_hr_tuit := curr_liab_OL_waiv_hr_tuit +
                                   t_liab_waiv_hr_tuit +
                                   curr_liab_waiv_hr_tuit +
                                   prev_liab_bill_tui_hr;

            if type_in = STUDENT_TYPE then
              t_stud_liab_bill_hr_tuit := t_liab_bill_hr_tuit;
              t_stud_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit;
              p_print_dbms('student type liab3: ' ||
                           t_stud_liab_bill_hr_tuit);
            end if;
          END IF;

          CONTINUE_CALC := FALSE; --104070 1-1AZHFF change > to >=
          IF (prev_rule_from_flat_hr_fee >= fee_nondrop_bill_hr) AND
             (starting_liab_bill_hr_fee > 0) AND
             (orig_fee_ndrop_billhr >= prev_rule_from_flat_hr_fee) THEN
            -- 1-1DSEZH
            CONTINUE_CALC := TRUE;
          ELSE
            IF (prev_rule_from_flat_hr_fee >= fee_nondrop_waiv_hr) AND
               (starting_liab_bill_hr_fee > 0) AND
               (orig_fee_ndrop_waivhr >= prev_rule_from_flat_hr_fee) THEN
              -- 1-1DSEZH
              CONTINUE_CALC := TRUE;
            ELSE
              CONTINUE_CALC := FALSE;
            END IF;
          END IF;
          --- 7.4.0.1 set to false if refund percentages changed
          IF NOT (RECONSIDER_FLAT_FEES) then
            CONTINUE_CALC := FALSE;
          END IF;
          IF (DROPS) AND (PREV_FLAT_HR_RULE_MET) AND (CONTINUE_CALC) THEN
            t_liab_bill_hr_fees     := orig_fee_ndrop_billhr -
                                       fee_below_flat_bill_hr;
            t_liab_waiv_hr_fees     := orig_fee_ndrop_waivhr -
                                       fee_below_flat_waiv_hr;
            t_liab_bill_hr_multiple := prev_rule_from_flat_hr_fee -
                                       t_liab_bill_hr_fees;
            t_liab_waiv_hr_multiple := prev_rule_from_flat_hr_fee -
                                       t_liab_waiv_hr_fees;

            t_liab_bill_hr_fees := t_liab_bill_hr_fees +
                                   (t_liab_bill_hr_multiple *
                                   fee_below_flat_pcent);
            t_liab_waiv_hr_fees := t_liab_waiv_hr_fees +
                                   (t_liab_waiv_hr_multiple *
                                   fee_below_flat_pcent);
            -- 1-2O637G do we want to replace the liability if this is the assessment after the intermediate
            IF NOT (LAST_ASSESSMENT_HANDLED_DD) THEN

              WHILE multiple_fee_ctr > num_of_drops LOOP
                t_liab_bill_hr_fees := t_liab_bill_hr_fees +
                                       (multiple_fee_bill_hr(multiple_fee_ctr) *
                                       multiple_fee_liab(multiple_fee_ctr));
                t_liab_waiv_hr_fees := t_liab_waiv_hr_fees +
                                       (multiple_fee_waiv_hr(multiple_fee_ctr) *
                                       multiple_fee_liab(multiple_fee_ctr));
                multiple_fee_ctr    := multiple_fee_ctr - 1;
              END LOOP;
            END IF;
            t_liab_bill_hr_fees := curr_liab_OL_bill_hr_fees +
                                   t_liab_bill_hr_fees +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr;
            t_liab_waiv_hr_fees := curr_liab_OL_waiv_hr_fees +
                                   t_liab_waiv_hr_fees +
                                   curr_liab_waiv_hr_fees +
                                   prev_liab_bill_fee_hr;
            if type_in = STUDENT_TYPE then
              t_stud_liab_bill_hr_fees := t_liab_bill_hr_fees;
              t_stud_liab_waiv_hr_fees := t_liab_waiv_hr_fees;
            end if;

          END IF;
        ELSE
          /* Normal Processing */

          /* *********************************************************************************** */
          /* Defect 99521                                                                        */
          /* Created two conditions that will check if there is a multiple drop happened in this */
          /* assessment.  If there is, then check if it is for TUI or FEE and then do the right  */
          /* calculation. But, if there was no multiple DROP, then the calculation above is      */
          /* sufficient.                                                                         */
          /* The multiple DROP will be determined by multiple_<DCAT>_ctr.  If this variable is   */
          /* greater than 1, then that means, there was at least 2 DROPs that happened.          */
          /* *********************************************************************************** */
          p_print_dbms('NORMAL PROCESSING: ' || num_of_drops);
          CONTINUE_CALC := FALSE; --104070
          IF ((prev_rule_from_flat_hr_tui - tui_nondrop_bill_hr) >= 1) THEN
            CONTINUE_CALC := TRUE;
          ELSE
            IF ((prev_rule_from_flat_hr_tui - tui_nondrop_waiv_hr) >= 1) THEN
              CONTINUE_CALC := TRUE;
            ELSE
              CONTINUE_CALC := FALSE;
            END IF;
          END IF;

          IF NOT (RECONSIDER_FLAT_TUIT) then
            CONTINUE_CALC := FALSE;
          END IF;

          if (SWAP_RBC_EQUAL) then
            p_print_dbms('swap equal');
          end if;

          if (PREV_FLAT_HR_RULE_MET) then
            p_print_dbms('prev flat hr rule met');
          end if;
          if (CONTINUE_CALC) then
            p_print_dbms('CONTINUE_CALC met');
          end if;

          IF (drops_count > 1) AND (DROPS) AND (multiple_tui_ctr > 1) AND
             (PREV_FLAT_HR_RULE_MET) AND (CONTINUE_CALC) THEN

            t_liab_bill_hr_tuit     := 0;
            t_liab_waiv_hr_tuit     := 0;
            t_liab_bill_hr_multiple := prev_rule_from_flat_hr_tui -
                                       tui_nondrop_bill_hr;
            t_liab_waiv_hr_multiple := prev_rule_from_flat_hr_tui -
                                       tui_nondrop_waiv_hr;
            p_print_dbms('levl fees drop cnts: ' || drops_count ||
                         ' prev flat hr rule met, mult tui ctr: ' ||
                         multiple_tui_ctr);
            WHILE multiple_tui_ctr > 0 LOOP
              IF multiple_tui_bill_hr(multiple_tui_ctr) <
                 t_liab_bill_hr_multiple THEN
                t_liab_bill_hr_tuit     := t_liab_bill_hr_tuit +
                                           (multiple_tui_bill_hr(multiple_tui_ctr) *
                                           multiple_tui_liab(multiple_tui_ctr));
                t_liab_waiv_hr_tuit     := t_liab_waiv_hr_tuit +
                                           (multiple_tui_waiv_hr(multiple_tui_ctr) *
                                           multiple_tui_liab(multiple_tui_ctr));
                t_liab_bill_hr_multiple := t_liab_bill_hr_multiple -
                                           multiple_tui_bill_hr(multiple_tui_ctr);
                t_liab_waiv_hr_multiple := t_liab_waiv_hr_multiple -
                                           multiple_tui_waiv_hr(multiple_tui_ctr);
              ELSE
                t_liab_bill_hr_tuit := t_liab_bill_hr_tuit +
                                       (t_liab_bill_hr_multiple *
                                       multiple_tui_liab(multiple_tui_ctr));
                t_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit +
                                       (t_liab_waiv_hr_multiple *
                                       multiple_tui_liab(multiple_tui_ctr));
                exit;
              END IF;
              multiple_tui_ctr := multiple_tui_ctr - 1;
            END LOOP;
            -- in the future waiv hrs need to be added here
            t_liab_bill_hr_tuit := tui_nondrop_bill_hr +
                                   curr_liab_OL_bill_hr_tuit +
                                   t_liab_bill_hr_tuit +
                                   curr_liab_bill_hr_tuit +
                                   prev_liab_bill_tui_hr;
            t_liab_waiv_hr_tuit := tui_nondrop_waiv_hr +
                                   curr_liab_OL_waiv_hr_tuit +
                                   t_liab_waiv_hr_tuit +
                                   curr_liab_waiv_hr_tuit +
                                   prev_liab_bill_tui_hr;
            if type_in = STUDENT_TYPE then
              t_stud_liab_bill_hr_tuit := t_liab_bill_hr_tuit;
              t_stud_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit;
              p_print_dbms('student type liab4: ' ||
                           t_stud_liab_bill_hr_tuit);
            end if;
          END IF;

          CONTINUE_CALC := FALSE; --104070
          IF ((prev_rule_from_flat_hr_fee - fee_nondrop_bill_hr) >= 1) THEN
            CONTINUE_CALC := TRUE;
          ELSE
            IF ((prev_rule_from_flat_hr_fee - fee_nondrop_waiv_hr) >= 1) THEN
              CONTINUE_CALC := TRUE;
            ELSE
              CONTINUE_CALC := FALSE;
            END IF;
          END IF;
          IF NOT (RECONSIDER_FLAT_FEES) then
            CONTINUE_CALC := FALSE;
          END IF;
          IF (drops_count > 1) AND (DROPS) AND (multiple_fee_ctr > 1) AND
             (PREV_FLAT_HR_RULE_MET) AND (CONTINUE_CALC) THEN

            t_liab_bill_hr_fees     := 0;
            t_liab_waiv_hr_fees     := 0;
            t_liab_bill_hr_multiple := prev_rule_from_flat_hr_fee -
                                       fee_nondrop_bill_hr;
            t_liab_waiv_hr_multiple := prev_rule_from_flat_hr_fee -
                                       fee_nondrop_waiv_hr;
            WHILE multiple_fee_ctr > 0 LOOP
              IF multiple_fee_bill_hr(multiple_fee_ctr) <
                 t_liab_bill_hr_multiple THEN
                t_liab_bill_hr_fees     := t_liab_bill_hr_fees +
                                           (multiple_fee_bill_hr(multiple_fee_ctr) *
                                           multiple_fee_liab(multiple_fee_ctr));
                t_liab_waiv_hr_fees     := t_liab_waiv_hr_fees +
                                           (multiple_fee_waiv_hr(multiple_fee_ctr) *
                                           multiple_fee_liab(multiple_fee_ctr));
                t_liab_bill_hr_multiple := t_liab_bill_hr_multiple -
                                           multiple_fee_bill_hr(multiple_fee_ctr);
                t_liab_waiv_hr_multiple := t_liab_waiv_hr_multiple -
                                           multiple_fee_waiv_hr(multiple_fee_ctr);
              ELSE
                t_liab_bill_hr_fees := t_liab_bill_hr_fees +
                                       (t_liab_bill_hr_multiple *
                                       multiple_fee_liab(multiple_fee_ctr));
                t_liab_waiv_hr_fees := t_liab_waiv_hr_fees +
                                       (t_liab_waiv_hr_multiple *
                                       multiple_fee_liab(multiple_fee_ctr));
                exit;
              END IF;
              multiple_fee_ctr := multiple_fee_ctr - 1;
            END LOOP;
            -- in the future waiv hrs need to be added here
            t_liab_bill_hr_fees := fee_nondrop_bill_hr +
                                   curr_liab_OL_bill_hr_fees +
                                   t_liab_bill_hr_fees +
                                   curr_liab_bill_hr_fees +
                                   prev_liab_bill_fee_hr;
            t_liab_waiv_hr_fees := fee_nondrop_waiv_hr +
                                   curr_liab_OL_waiv_hr_fees +
                                   t_liab_waiv_hr_fees +
                                   curr_liab_waiv_hr_fees +
                                   prev_liab_bill_fee_hr;
            if type_in = STUDENT_TYPE then
              t_stud_liab_bill_hr_fees := t_liab_bill_hr_fees;
              t_stud_liab_waiv_hr_fees := t_liab_waiv_hr_fees;
            end if;
          END IF;

          --104070, the calculation below will only take place when there was a situation where
          --swapping occurred below FLAT. There are more DROPS than ADDs.
          IF SWAP_RBC_MORE_DROPPED AND PREV_FLAT_HR_RULE_MET THEN
            IF multiple_tui_ctr < 2 THEN
              t_liab_bill_hr_tuit := t_liab_bill_hr_tuit +
                                     recalc_penalty_hrs_tui;
              t_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit +
                                     recalc_penalty_hrs_tui;
              if type_in = STUDENT_TYPE then
                t_stud_liab_bill_hr_tuit := t_liab_bill_hr_tuit;
                t_stud_liab_waiv_hr_tuit := t_liab_waiv_hr_tuit;
                p_print_dbms('student type liab5: ' ||
                             t_stud_liab_bill_hr_tuit);
              end if;
            END IF;
            IF multiple_fee_ctr < 2 THEN
              t_liab_bill_hr_fees := t_liab_bill_hr_fees +
                                     recalc_penalty_hrs_fee;
              t_liab_waiv_hr_fees := t_liab_waiv_hr_fees +
                                     recalc_penalty_hrs_fee;
              if type_in = STUDENT_TYPE then
                t_stud_liab_bill_hr_fees := t_liab_bill_hr_fees;
                t_stud_liab_waiv_hr_fees := t_liab_waiv_hr_fees;
              end if;
            END IF;
          END IF;

        END IF; /*104071*/
        p_print_dbms(type_in || ' my final tlib is t_liab_bill_hr_tuit: ' ||
                     t_liab_bill_hr_tuit || 'fee: ' || t_liab_bill_hr_fees);

      END IF; /*104070*/

      --104070 Took all empty memory tables out of the the IF THEN statement.
      multiple_tui_liab    := empty_memory_table;
      multiple_tui_bill_hr := empty_memory_table;
      multiple_tui_waiv_hr := empty_memory_table;
      multiple_fee_liab    := empty_memory_table;
      multiple_fee_bill_hr := empty_memory_table;
      multiple_fee_waiv_hr := empty_memory_table;

      -- save unaltered total registration hours to global vars for p_applyrules
      t_reg_bill_hr := tot_reg_bill_hr;
      t_reg_waiv_hr := tot_reg_waiv_hr;

      IF NOT (PREV_FLAT_HR_RULE_MET) THEN
        -- conventional liability
        -- Defect 96580: save totals currently in local vars to
        --               common global vars for p_applyrules
        -- Defect 1-9CE6N: IF conditions were put to make sure
        -- that the right Student Liable Bill and Waiv hrs
        -- are being reflected.
        if type_in = STUDENT_TYPE then
          t_stud_liab_bill_hr_tuit := tot_liab_bill_hr_tuit;
          t_stud_liab_bill_hr_fees := tot_liab_bill_hr_fees;
          t_stud_liab_waiv_hr_tuit := tot_liab_waiv_hr_tuit;
          t_stud_liab_waiv_hr_fees := tot_liab_waiv_hr_fees;
          p_print_dbms('student type liab6: ' || t_stud_liab_bill_hr_tuit);
        else
          IF (t_stud_liab_bill_hr_tuit = 0) THEN
            t_stud_liab_bill_hr_tuit := tot_liab_bill_hr_tuit;
          END IF;
          IF (t_stud_liab_bill_hr_fees = 0) THEN
            t_stud_liab_bill_hr_fees := tot_liab_bill_hr_fees;
          END IF;
          IF (t_stud_liab_waiv_hr_tuit = 0) THEN
            t_stud_liab_waiv_hr_tuit := tot_liab_waiv_hr_tuit;
          END IF;
          IF (t_stud_liab_waiv_hr_fees = 0) THEN
            t_stud_liab_waiv_hr_fees := tot_liab_waiv_hr_fees;
          END IF;

        end if;
        p_print_dbms(type_in ||
                     ' fees no prev flat met , starting t_stud_liab_bill_hr_tuit: ' ||
                     t_stud_liab_bill_hr_tuit);
        t_liab_bill_hr_tuit := tot_liab_bill_hr_tuit;
        t_liab_bill_hr_fees := tot_liab_bill_hr_fees;
        t_liab_waiv_hr_tuit := tot_liab_waiv_hr_tuit;
        t_liab_waiv_hr_fees := tot_liab_waiv_hr_fees;
      END IF;
      p_print_dbms('end of course liab,  stud liab: ' ||
                   t_stud_liab_bill_hr_tuit);

    end p_process_course_liability;

    ---   execute the apply credits for the total liability and for the courses
    procedure p_apply_credits is

    begin
      -- reset t_ptrm_rule before processing courses for level

      -- calculate fees for the course level grouping
      p_print_dbms(type_in || 'Before apply rules: ' ||
                   t_liab_bill_hr_tuit || ' t reg bill hr: ' ||
                   t_reg_bill_hr || ' stdn liab: ' ||
                   t_stud_liab_bill_hr_tuit);
      p_applyrules(pidm_in,
                   term_in,
                   'SFRRGFE',
                   type_in,
                   curr_type_code,
                   t_ptrm_rule,
                   NULL, -- GMOD
                   NULL, -- SCHD
                   NULL, -- INSM
                   'N',
                   source_pgm_in,
                   process_flat_from_hrs,
                   process_flat_to_hrs);

      -- reset the total vars for the next course level grouping
      tot_reg_bill_hr       := 0;
      tot_reg_waiv_hr       := 0;
      tot_liab_bill_hr_tuit := 0;
      tot_liab_bill_hr_fees := 0;
      tot_liab_waiv_hr_tuit := 0;
      tot_liab_waiv_hr_fees := 0;
      -- process each course student is registered for that has the current course level
      lv_fees_rec := null;
      t_ptrm_rule := '';
      if (type_in = CAMPUS_TYPE and gv_rgfe_campus_course_exists = 'Y') OR
         (type_in = LEVEL_TYPE and gv_rgfe_level_course_exists = 'Y') OR
         (type_in = ATTR_TYPE and gv_rgfe_attr_course_exists = 'Y') OR
         (type_in = STUDENT_TYPE and gv_rgfe_student_course_exists = 'Y') then

        lv_crn_records_ref := f_crn_records(term_in       => term_in,
                                            pidm_in       => pidm_in,
                                            type_in       => type_in,
                                            type_value_in => curr_type_code);

        LOOP
          p_reinit_temp_vars;
          FETCH lv_crn_records_ref
            into lv_fees_rec;
          EXIT WHEN lv_crn_records_ref%NOTFOUND;
          t_crn                  := lv_fees_rec.r_crn;
          t_ptrm_code            := lv_fees_rec.r_ptrm_cde;
          t_rsts_code            := lv_fees_rec.r_rsts_cde;
          t_rsts_date            := lv_fees_rec.r_rsts_date;
          t_reg_bill_hr          := lv_fees_rec.r_REG_BILL_HR;
          t_bill_hr_tuit         := lv_fees_rec.r_BILL_HR_TUIT;
          t_bill_hr_fees         := lv_fees_rec.r_BILL_HR_FEES;
          t_reg_waiv_hr          := lv_fees_rec.r_REG_WAIV_HR;
          t_waiv_hr_tuit         := lv_fees_rec.r_WAIV_HR_TUIT;
          t_waiv_hr_fees         := lv_fees_rec.r_WAIV_HR_FEES;
          t_tuit_liab_percentage := lv_fees_rec.r_TUIT_LIAB_PERCENTAGE;
          t_fees_liab_percentage := lv_fees_rec.r_FEES_LIAB_PERCENTAGE;
          t_levl_code_crse       := lv_fees_rec.r_levl_cde_crse;
          t_camp_code_crse       := lv_fees_rec.r_camp_cde_crse;
          t_add_date             := lv_fees_rec.r_add_date;
          t_gmod_code            := lv_fees_rec.r_gmod_cde;
          t_crse_start_date      := lv_fees_rec.r_crse_start_date;
          t_crse_end_date        := lv_fees_rec.r_crse_end_date;
          t_schd_code            := lv_fees_rec.r_schd_cde;
          t_insm_code            := lv_fees_rec.r_insm_cde;

          t_ptrm_rule := t_ptrm_code;

          t_liab_bill_hr_tuit := t_bill_hr_tuit * t_tuit_liab_percentage;
          t_liab_bill_hr_fees := t_bill_hr_fees * t_fees_liab_percentage;
          t_liab_waiv_hr_tuit := t_waiv_hr_tuit * t_tuit_liab_percentage;
          t_liab_waiv_hr_fees := t_waiv_hr_fees * t_fees_liab_percentage;

          p_print_dbms('apply rules to crse: ' || type_in || ' value: ' ||
                       curr_type_code || ' crn: ' || t_crn || ' ptrm: ' ||
                       t_ptrm_rule || ' insm: ' || t_insm_code ||
                       ' gmod: ' || t_gmod_code || ' schd: ' ||
                       t_schd_code);
          -- calculate charges for each course for the student's registered course level
          -- check to see if there are any rules for this breakdown

          p_applyrules(pidm_in,
                       term_in,
                       'SFRRGFE',
                       type_in,
                       curr_type_code,
                       t_ptrm_rule,
                       t_gmod_code,
                       t_schd_code,
                       t_insm_code,
                       'Y',
                       source_pgm_in,
                       process_flat_from_hrs,
                       process_flat_to_hrs);

        END LOOP;
        CLOSE lv_crn_records_ref;
      else
        p_print_dbms('rules do not exist for courses : ' || type_in);
      end if; --- only execute course rules if actual rules existrs

    end p_apply_credits;
    -------------------------------------------------------------------------------------------------

  BEGIN
    p_print_dbms('------ Start processing for type : ' || type_in ||
                 ' ----------');
    p_reinit_temp_vars; -- reinit for each  code, 102188 moved this initialization.

    -- get the course types within the student's registration for the term and type
    lv_course_types_ref := f_course_types(term_in => term_in,
                                          pidm_in => pidm_in,
                                          type_in => type_in);
    LOOP
      FETCH lv_course_types_ref
        INTO curr_type;
      EXIT WHEN lv_course_types_ref%NOTFOUND;

      if type_in <> STUDENT_TYPE then
        curr_type_code := curr_type.sftfees_rec_type;
      else
        curr_type_code := null;
      end if;
      -- if drops, rbc and not the intermediate, get the mutliple flat ranges for separate processing
      flat_ranges.delete;
      IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') <> 'Y') AND (DROPS) AND
         (NOT (DD_SINCE_LAST_ASSESSMENT) AND NOT (DO_INTERMEDIATE)) THEN
        p_multiple_rules(pidm_in, term_in, type_in, curr_type_code);
      END IF;

      N_FAUD_CREATED        := FALSE;
      PREV_FLAT_HR_RULE_MET := FALSE;
      /* ********************************************************************************* */
      /* If there are drops for any registration and not RBT for the term,                 */
      /* check if the prev assessment involved flat hour rule processing for               */
      /* LEVEL rules. If yes, branch to processing for flat hour liability                 */
      /* processing later on.                                                              */
      /* 7.1: Also check if the last assessment did not handle any DD/'not                 */
      /* count in assessment' registration activity prior to setting                       */
      /* PREV_FLAT_HR_RULE_MET := TRUE.                                                    */
      /*                                                                                   */
      /* RBT only cares about the hours the student is registered in, so there             */
      /* is no need to perform calculations to determine liability for drops.              */
      /* ********************************************************************************* */
      /* Defect 102188 - needs to add the starting_liab_bill_hr_<DCAT> parms everytime     */
      /* p_get_last_assess_data_flat is being called. Also, prev_rule_from_flat_hr_<DCAT>, */
      /* prev_rule_OL_start_hr_<DCAT> and last_assess_met_flat_ind_<DCAT> were added/repl. */
      /* ********************************************************************************* */
      /* 1-E716D - To replace OR with AND for:                                             */
      /*   (NOT(DD_SINCE_LAST_ASSESSMENT) AND NOT(DO_INTERMEDIATE)))                       */
      /* ********************************************************************************* */

      starting_liab_bill_hr_tui := 0;
      starting_liab_bill_hr_fee := 0;
      starting_liab_bill_hr_tui := 0;
      starting_liab_bill_hr_fee := 0;
      if (DROPS) then
        p_print_dbms('++drops occurred++  ');
      else
        p_print_dbms('++drops did not occur++ ');
      end if;
      p_print_dbms('start of type: ' || type_in || ' value: ' ||
                   curr_type_code || ' number of flat ranges: ' ||
                   flat_ranges.count);
      process_flat_liab := TRUE;
      --- process all the flat ranges but only if there were drops, rbc  and flats for this particular type and value
      IF (DROPS) AND (NVL(sobterm_rec.sobterm_refund_ind, 'N') <> 'Y') AND
         flat_ranges.count > 0 AND
         (NOT (DD_SINCE_LAST_ASSESSMENT) AND NOT (DO_INTERMEDIATE)) THEN

        tab_cnt := 0;
        p_print_dbms('++process flat ranges: ' || flat_ranges.count);
        for tab_cnt in 1 .. flat_ranges.count loop
          flat_rec              := flat_ranges(tab_cnt);
          process_flat_from_hrs := flat_rec.r_flat_from_hrs;
          process_flat_to_hrs   := flat_rec.r_flat_to_hrs;
          p_print_dbms('++calc the liability: ' || process_flat_from_hrs ||
                       ' to: ' || process_flat_to_hrs);

          --- reset liab hrs
          prev_liab_bill_tui_hr       := 0;
          prev_liab_bill_fee_hr       := 0;
          prev_liab_bill_hr           := 0;
          curr_liab_flat_bill_hr_tuit := 0;
          curr_liab_flat_waiv_hr_tuit := 0;
          curr_liab_OL_bill_hr_tuit   := 0;
          curr_liab_OL_waiv_hr_tuit   := 0;
          curr_liab_bill_hr_tuit      := 0;
          curr_liab_waiv_hr_tuit      := 0;
          curr_liab_flat_bill_hr_fees := 0;
          curr_liab_flat_waiv_hr_fees := 0;
          curr_liab_OL_bill_hr_fees   := 0;
          curr_liab_OL_waiv_hr_fees   := 0;
          curr_liab_bill_hr_fees      := 0;
          curr_liab_waiv_hr_fees      := 0;
          prev_rule_from_flat_hr_tui  := 0;
          prev_rule_from_flat_hr_fee  := 0;
          prev_rule_OL_start_hr_tui   := 0;
          prev_rule_OL_start_hr_fee   := 0;
          tui_below_flat_bill_hr      := 0;
          fee_below_flat_bill_hr      := 0;
          tui_below_flat_waiv_hr      := 0;
          fee_below_flat_waiv_hr      := 0;
          orig_tui_ndrop_billhr       := 0;
          orig_fee_ndrop_billhr       := 0;
          orig_tui_ndrop_waivhr       := 0;
          orig_fee_ndrop_waivhr       := 0;
          below_prev_flat_OL_tui      := 0;
          below_prev_flat_OL_fee      := 0;
          above_prev_flat_OL_tui      := 0;
          above_prev_flat_OL_fee      := 0;
          above_prev_oload_OL_tui     := 0; --1-PX0E9
          above_prev_oload_OL_fee     := 0; --1-PX0E9
          multiple_tui_ctr            := 0;
          multiple_fee_ctr            := 0;
          t_liab_bill_hr_multiple     := 0;
          t_liab_waiv_hr_multiple     := 0;
          sum_ndrop_bill_hr_tuit      := 0;
          sum_ndrop_bill_hr_fees      := 0;
          sum_ndrop_waiv_hr_tuit      := 0;
          sum_ndrop_waiv_hr_fees      := 0;

          p_flat_starting_liability;

          t_liab_bill_hr_tuit := starting_liab_bill_hr_tui;
          t_liab_bill_hr_fees := starting_liab_bill_hr_fee;
          t_liab_waiv_hr_tuit := starting_liab_bill_hr_tui;
          t_liab_waiv_hr_fees := starting_liab_bill_hr_fee;
          p_print_dbms('before p course liab start liab: ' ||
                       t_stud_liab_bill_hr_tuit);
          ---  set up the starting liability,  only flat, rbc and with drops have a starting liability
          --   all others start with 0 and add on based on course evaluation.  When flats existed we
          --   subtract from the starting, or add f there were adds
          if type_in = STUDENT_TYPE then
            t_stud_liab_bill_hr_tuit := starting_liab_bill_hr_tui;
            t_stud_liab_bill_hr_fees := starting_liab_bill_hr_fee;

            t_stud_liab_waiv_hr_tuit := starting_liab_bill_hr_tui;
            t_stud_liab_waiv_hr_fees := starting_liab_bill_hr_fee;
            p_print_dbms('in studfees t student lib bill hr tui: ' ||
                         t_stud_liab_bill_hr_tuit ||
                         ' orign non drop bill hrs:  ' ||
                         orig_tui_ndrop_billhr || ' prev OL: ' ||
                         prev_liab_bill_tui_hr);
            p_print_dbms('in studfees t student lib bill hr eei: ' ||
                         t_stud_liab_bill_hr_fees ||
                         ' orign non drop  hrs:  ' ||
                         orig_tui_ndrop_billhr || ' prev OL: ' ||
                         prev_liab_bill_fee_hr);
          end if;

          ----- process the courses and calculate the liability based on the course activity
          p_process_course_liability;

          p_apply_credits;

        --   t_stud_liab_bill_hr_tuit    := 0;
        --   t_stud_liab_bill_hr_fees    := 0;
        --   t_stud_liab_waiv_hr_tuit    := 0;
        --   t_stud_liab_waiv_hr_fees   := 0;
        end loop;
        process_flat_from_hrs := 0;
        process_flat_to_hrs   := 0;

      end if;

      --- now do per credit  or rbt,  the starting liability is 0
      starting_liab_bill_hr_tui := 0;
      starting_liab_bill_hr_fee := 0;
      starting_liab_bill_hr_tui := 0;
      starting_liab_bill_hr_fee := 0;

      PREV_FLAT_HR_RULE_MET := FALSE;
      t_liab_bill_hr_tuit   := starting_liab_bill_hr_tui;
      t_liab_bill_hr_fees   := starting_liab_bill_hr_fee;
      t_liab_waiv_hr_tuit   := starting_liab_bill_hr_tui;
      t_liab_waiv_hr_fees   := starting_liab_bill_hr_fee;
      process_flat_liab     := FALSE;

      ----- process the courses and calculate the liability based on the course activity
      p_print_dbms('-----------------------------------------------------');
      p_print_dbms('before process course liab for per credit, rbt, non drops ' ||
                   flat_ranges.count);
      p_print_dbms('Before per credit process course liab, starting liab hrs: ' ||
                   starting_liab_bill_hr_tui);
      p_process_course_liability;

      p_apply_credits;

    END LOOP;
    CLOSE lv_course_types_ref;
    p_print_dbms('End tui and fees for ' || type_in);
    p_print_dbms('--------------------------------------');

  END p_calcliability;

  /* ******************************************************************* */
  /*  Process the additional fees for the student                        */
  /* ******************************************************************* */
  PROCEDURE p_additionalfees(pidm_in       IN spriden.spriden_pidm%TYPE,
                             term_in       IN stvterm.stvterm_code%TYPE,
                             source_pgm_in IN VARCHAR2) IS
    add_fee_amt       tbraccd.tbraccd_amount%TYPE := 0;
    add_fee_detl_code tbbdetc.tbbdetc_detail_code%TYPE := '';
    dcat_code         ttvdcat.ttvdcat_code%TYPE := '';
    next_seqno        sfrfaud.sfrfaud_seqno%TYPE := 0;

    CURSOR additional_fees_c IS
      SELECT sfrefee_detl_code, sfrafee_amount, tbbdetc_dcat_code
        FROM tbbdetc, sfrafee, sfrefee
       WHERE tbbdetc_detail_code = sfrefee_detl_code
         AND sfrafee_term_code = sfrefee_term_code
         AND sfrafee_detl_code = sfrefee_detl_code
         AND sfrefee_pidm = pidm_in
         AND sfrefee_term_code = term_in;
  BEGIN

    OPEN additional_fees_c;
    LOOP
      FETCH additional_fees_c
        INTO add_fee_detl_code, add_fee_amt, dcat_code;
      EXIT WHEN additional_fees_c%NOTFOUND;

      IF add_fee_amt <> 0 THEN
        -- determine next seqno for audit record and insert it
        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        p_print_dbms('p_insert_sfrfaud 6');
        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'A',
                         dcat_code,
                         add_fee_detl_code,
                         add_fee_amt,
                         'N', -- N/A: no ABC rule
                         source_pgm_in,
                         NULL, -- N/A: no rule seqno
                         NULL, -- N/A: no rule type
                         NULL, -- N/A: no CRN
                         NULL, -- N/A: no RSTS date
                         NULL, -- N/A: no RSTS date
                         NULL, -- N/A: no ESTS code
                         NULL, -- N/A: no ESTS date
                         NULL, -- N/A: no refund table
                         NULL, -- N/A: no reg bill hour
                         NULL, -- N/A: no reg waiv hour
                         NULL, -- N/A: no rule cred hours
                         NULL, -- N/A: no rule stud hours
                         NULL, -- N/A: no rule per cred charge
                         NULL, -- N/A: no rule flat fee amount
                         NULL, -- N/A: no overload hrs for section fees
                         NULL, -- N/A: no rule overload charge
                         NULL, -- populate TBRACCD tran when charge is inserted
                         g$_nls.get('SFKFEE1-0016', 'SQL', 'Additional Fee'),
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr
      END IF;
    END LOOP;
    CLOSE additional_fees_c;

  END p_additionalfees;

  /* ******************************************************************* */
  /*  Determine and apply the rule.                                      */
  /* ******************************************************************* */
  PROCEDURE p_applyrules(pidm_in                 IN spriden.spriden_pidm%TYPE,
                         term_in                 IN stvterm.stvterm_code%TYPE,
                         rule_source_in          IN VARCHAR2,
                         rule_type_in            IN VARCHAR2,
                         rule_value_in           IN VARCHAR2,
                         ptrm_rule_in            IN VARCHAR2,
                         gmod_code_in            IN stvgmod.stvgmod_code%TYPE,
                         schd_code_in            IN stvschd.stvschd_code%TYPE,
                         insm_code_in            IN gtvinsm.gtvinsm_code%TYPE,
                         assess_by_course_ind_in IN VARCHAR2,
                         source_pgm_in           IN VARCHAR2,
                         process_flat_from_hrs   IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                         process_flat_to_hrs     IN sfrfaud.sfrfaud_reg_bill_hr%TYPE) IS

    xx                number := 0;
    xy                number := 0;
    rule_cred_hrs     sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE := 0;
    rule_stud_hrs     sfrfaud.sfrfaud_rule_liable_stud_hrs%TYPE := 0;
    dcat_code         ttvdcat.ttvdcat_code%TYPE := '';
    chrg              sfrfaud.sfrfaud_charge%TYPE := 0;
    aud_chrg          sfrfaud.sfrfaud_charge%TYPE := 0;
    aud_crn           sfrfaud.sfrfaud_crn%TYPE := '';
    aud_rsts_code     sfrfaud.sfrfaud_rsts_code%TYPE := '';
    aud_rsts_date     DATE;
    per_cred_chrg     sfrfaud.sfrfaud_tot_per_cred_charge%TYPE := 0;
    flat_fee_amt      sfrfaud.sfrfaud_charge%TYPE := 0;
    overload_hrs      sfrfaud.sfrfaud_crse_overload_hrs%TYPE := 0;
    overload_chrg     sfrfaud.sfrfaud_crse_overload_charge%TYPE := 0;
    next_seqno        sfrfaud.sfrfaud_seqno%TYPE := 0;
    note              sfrfaud.sfrfaud_note%TYPE := '';
    lv_crn_rfnd_table sfrfaud.sfrfaud_refund_source_table%type := crn_rfnd_table;
    clas_desc         stvclas.stvclas_desc%TYPE := '';
    -- performance enhancement to bulk fetch NULL codess
    bf_ptrm        stvptrm.stvptrm_code%TYPE;
    bf_resd        sgbstdn.sgbstdn_resd_code%TYPE;
    bf_clas        stvclas.stvclas_code%TYPE;
    bf_rate        sgbstdn.sgbstdn_rate_code%TYPE;
    bf_styp        sgbstdn.sgbstdn_styp_code%TYPE;
    bf_start       DATE;
    bf_end         DATE;
    bf_curric_rate sgbstdn.sgbstdn_rate_code%TYPE;
    bf_levl        sgbstdn.sgbstdn_levl_code%TYPE;
    bf_coll        sgbstdn.sgbstdn_coll_code_1%TYPE;
    bf_camp        sgbstdn.sgbstdn_camp_code%TYPE;
    bf_curric_styp sgbstdn.sgbstdn_styp_code%TYPE;
    bf_term        sgbstdn.sgbstdn_term_code_admit%TYPE;
    bf_degc        sgbstdn.sgbstdn_degc_code_1%TYPE;
    bf_prgm        sgbstdn.sgbstdn_program_1%TYPE;

    bf_majr               sgbstdn.sgbstdn_majr_code_1%TYPE;
    bf_dept               sgbstdn.sgbstdn_dept_code%TYPE;
    var_vtyp_code_current gorvisa.gorvisa_vtyp_code%TYPE := null;
    cursor sel_dcat_c(p_detail tbbdetc.tbbdetc_detail_code%type) is
      SELECT tbbdetc_dcat_code
        FROM tbbdetc
       WHERE tbbdetc_detail_code = p_detail;

    RULE_MET     BOOLEAN := TRUE;
    FLAT_FEE_MET BOOLEAN := FALSE;

    cnt                   PLS_INTEGER := 0;
    sfrrgfe_rec           sfrrgfe_record;
    flat_rec              flat_hour_ranges_rec;
    multiple_flat_met_cnt pls_integer := 0;

    -- 8.0.1 separate cursors into 4 parts to minimize io and take advantage of new indexes
    --- STUDENT type cursor
    CURSOR student_rules_cursor(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                p_add_date                date,
                                p_term_in                 stvterm.stvterm_code%type,
                                p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                p_gmod_code_in            stvgmod.stvgmod_code%type,
                                p_schd_code_in            stvschd.stvschd_code%type,
                                p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE --- columns part of index
       sfrrgfe_term_code = p_term_in
       AND sfrrgfe_type = p_rule_type_in
       AND sfrrgfe_entry_type = p_rule_entry_type
       AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
      -- part of the index
       AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
       AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
       AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
       AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
      --- optional
       AND ((sfrrgfe_from_add_date IS NULL AND sfrrgfe_to_add_date IS NULL) OR
       TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
       TRUNC(sfrrgfe_to_add_date))
      -- curriculum
       AND sotfcur_pidm = pidm_in
      --   AND  sotfcur_order = 1  and  nvl(sfrrgfe_prim_sec_cde ,'A')  in ('P','A')
       AND ((sotfcur_order = 1 and nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
       (sotfcur_order = 2 AND nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
       AND (sotfcur_lfst_order = 1 and
       nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
       (sotfcur_lfst_order = 2 AND
       nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
       AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
       AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
       AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
       nvl(sotfcur_dept_code, '@')
       AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
       AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
       AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
       nvl(sotfcur_camp_code, '@')
       AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
       AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
       nvl(sotfcur_program, '@')
       AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
       nvl(sotfcur_term_code_admit, '@')
       AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
       nvl(sotfcur_rate_code, '@')
       AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
       nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR student_rules_cursor_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                     p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                     p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                     p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                     p_add_date                date,
                                     p_term_in                 stvterm.stvterm_code%type,
                                     p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                     p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                     p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                     p_gmod_code_in            stvgmod.stvgmod_code%type,
                                     p_schd_code_in            stvschd.stvschd_code%type,
                                     p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE --- columns part of index
       sfrrgfe_term_code = p_term_in
       AND sfrrgfe_type = p_rule_type_in
       AND sfrrgfe_entry_type = p_rule_entry_type
       AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
      -- part of the index
       AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
       AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
       AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
       AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
      --- optional
       AND (sfrrgfe_gmod_code = p_gmod_code_in OR sfrrgfe_gmod_code IS NULL)
       AND (sfrrgfe_schd_code = p_schd_code_in OR sfrrgfe_schd_code IS NULL)
       AND (sfrrgfe_insm_code = p_insm_code_in OR sfrrgfe_insm_code IS NULL)
       AND ((sfrrgfe_from_add_date IS NULL AND sfrrgfe_to_add_date IS NULL) OR
       TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
       TRUNC(sfrrgfe_to_add_date))
      -- curriculum
       AND sotfcur_pidm = pidm_in
       AND ((sotfcur_order = 1 and nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
       (sotfcur_order = 2 AND nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
       AND (sotfcur_lfst_order = 1 and
       nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
       (sotfcur_lfst_order = 2 AND
       nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
       AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
       AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
       AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
       nvl(sotfcur_dept_code, '@')
       AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
       AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
       AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
       nvl(sotfcur_camp_code, '@')
       AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
       AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
       nvl(sotfcur_program, '@')
       AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
       nvl(sotfcur_term_code_admit, '@')
       AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
       nvl(sotfcur_rate_code, '@')
       AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
       nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR student_rules_cursor_nocc(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                     p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                     p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                     p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                     p_add_date                date,
                                     p_term_in                 stvterm.stvterm_code%type,
                                     p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                     p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                     p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                     p_gmod_code_in            stvgmod.stvgmod_code%type,
                                     p_schd_code_in            stvschd.stvschd_code%type,
                                     p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE --- columns part of index
       sfrrgfe_term_code = p_term_in
       AND sfrrgfe_type = p_rule_type_in
       AND sfrrgfe_entry_type = p_rule_entry_type
       AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
      -- part of the index
       AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
       AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
       AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
       AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
      --- optional
       AND ((sfrrgfe_from_add_date IS NULL AND sfrrgfe_to_add_date IS NULL) OR
       TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
       TRUNC(sfrrgfe_to_add_date));

    CURSOR student_rules_cursor_nocc_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                          p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                          p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                          p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                          p_add_date                date,
                                          p_term_in                 stvterm.stvterm_code%type,
                                          p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                          p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                          p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                          p_gmod_code_in            stvgmod.stvgmod_code%type,
                                          p_schd_code_in            stvschd.stvschd_code%type,
                                          p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE --- columns part of index
       sfrrgfe_term_code = p_term_in
       AND sfrrgfe_type = p_rule_type_in
       AND sfrrgfe_entry_type = p_rule_entry_type
       AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
      -- part of the index
       AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
       AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
       AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
       AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
      --- optional
       AND (sfrrgfe_gmod_code = p_gmod_code_in OR sfrrgfe_gmod_code IS NULL)
       AND (sfrrgfe_schd_code = p_schd_code_in OR sfrrgfe_schd_code IS NULL)
       AND (sfrrgfe_insm_code = p_insm_code_in OR sfrrgfe_insm_code IS NULL)
       AND ((sfrrgfe_from_add_date IS NULL AND sfrrgfe_to_add_date IS NULL) OR
       TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
       TRUNC(sfrrgfe_to_add_date));
    --- campus type cursor
    CURSOR campus_rules_cursor_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                    p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                    p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                    p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                    p_add_date                date,
                                    p_term_in                 stvterm.stvterm_code%type,
                                    p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                    p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                    p_rule_value              sfrrgfe.sfrrgfe_camp_code_crse%type,
                                    p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                    p_gmod_code_in            stvgmod.stvgmod_code%type,
                                    p_schd_code_in            stvschd.stvschd_code%type,
                                    p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_camp_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND (sfrrgfe_gmod_code = p_gmod_code_in OR
             sfrrgfe_gmod_code IS NULL)
         AND (sfrrgfe_schd_code = p_schd_code_in OR
             sfrrgfe_schd_code IS NULL)
         AND (sfrrgfe_insm_code = p_insm_code_in OR
             sfrrgfe_insm_code IS NULL)
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date))
            -- curriculum
         AND sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
         AND (sotfcur_lfst_order = 1 and
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
             (sotfcur_lfst_order = 2 AND
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
         AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
         AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
         AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
         AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
         AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
             nvl(sotfcur_rate_code, '@')
         AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
             nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR campus_rules_cursor(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                               p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                               p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                               p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                               p_add_date                date,
                               p_term_in                 stvterm.stvterm_code%type,
                               p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                               p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                               p_rule_value              sfrrgfe.sfrrgfe_camp_code_crse%type,
                               p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                               p_gmod_code_in            stvgmod.stvgmod_code%type,
                               p_schd_code_in            stvschd.stvschd_code%type,
                               p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_camp_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date))
            -- curriculum
         AND sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
         AND (sotfcur_lfst_order = 1 and
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
             (sotfcur_lfst_order = 2 AND
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
         AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
         AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
         AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
         AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
         AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
             nvl(sotfcur_rate_code, '@')
         AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
             nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR campus_rules_cursor_nocc(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                    p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                    p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                    p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                    p_add_date                date,
                                    p_term_in                 stvterm.stvterm_code%type,
                                    p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                    p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                    p_rule_value              sfrrgfe.sfrrgfe_camp_code_crse%type,
                                    p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                    p_gmod_code_in            stvgmod.stvgmod_code%type,
                                    p_schd_code_in            stvschd.stvschd_code%type,
                                    p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_camp_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date));

    CURSOR campus_rules_cursor_nocc_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                         p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                         p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                         p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                         p_add_date                date,
                                         p_term_in                 stvterm.stvterm_code%type,
                                         p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                         p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                         p_rule_value              sfrrgfe.sfrrgfe_camp_code_crse%type,
                                         p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                         p_gmod_code_in            stvgmod.stvgmod_code%type,
                                         p_schd_code_in            stvschd.stvschd_code%type,
                                         p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_camp_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND (sfrrgfe_gmod_code = p_gmod_code_in OR
             sfrrgfe_gmod_code IS NULL)
         AND (sfrrgfe_schd_code = p_schd_code_in OR
             sfrrgfe_schd_code IS NULL)
         AND (sfrrgfe_insm_code = p_insm_code_in OR
             sfrrgfe_insm_code IS NULL)
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date));
    --- LEVEL type cursor
    CURSOR level_rules_cursor(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                              p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                              p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                              p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                              p_add_date                date,
                              p_term_in                 stvterm.stvterm_code%type,
                              p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                              p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                              p_rule_value              sfrrgfe.sfrrgfe_levl_code_crse%type,
                              p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                              p_gmod_code_in            stvgmod.stvgmod_code%type,
                              p_schd_code_in            stvschd.stvschd_code%type,
                              p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
         AND sfrrgfe_levl_code_crse = p_rule_value
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date))
            -- curriculum
         AND sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
         AND (sotfcur_lfst_order = 1 and
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
             (sotfcur_lfst_order = 2 AND
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
         AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
         AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
         AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
         AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
         AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
             nvl(sotfcur_rate_code, '@')
         AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
             nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR level_rules_cursor_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                   p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                   p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                   p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                   p_add_date                date,
                                   p_term_in                 stvterm.stvterm_code%type,
                                   p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                   p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                   p_rule_value              sfrrgfe.sfrrgfe_levl_code_crse%type,
                                   p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                   p_gmod_code_in            stvgmod.stvgmod_code%type,
                                   p_schd_code_in            stvschd.stvschd_code%type,
                                   p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
         AND sfrrgfe_levl_code_crse = p_rule_value
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND (sfrrgfe_gmod_code = p_gmod_code_in OR
             sfrrgfe_gmod_code IS NULL)
         AND (sfrrgfe_schd_code = p_schd_code_in OR
             sfrrgfe_schd_code IS NULL)
         AND (sfrrgfe_insm_code = p_insm_code_in OR
             sfrrgfe_insm_code IS NULL)
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date))
            -- curriculum
         AND sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
         AND (sotfcur_lfst_order = 1 and
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
             (sotfcur_lfst_order = 2 AND
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
         AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
         AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
         AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
         AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
         AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
             nvl(sotfcur_rate_code, '@')
         AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
             nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;
    CURSOR level_rules_cursor_nocc(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                   p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                   p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                   p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                   p_add_date                date,
                                   p_term_in                 stvterm.stvterm_code%type,
                                   p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                   p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                   p_rule_value              sfrrgfe.sfrrgfe_levl_code_crse%type,
                                   p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                   p_gmod_code_in            stvgmod.stvgmod_code%type,
                                   p_schd_code_in            stvschd.stvschd_code%type,
                                   p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
         AND sfrrgfe_levl_code_crse = p_rule_value
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date));

    CURSOR level_rules_cursor_nocc_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                        p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                        p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                        p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                        p_add_date                date,
                                        p_term_in                 stvterm.stvterm_code%type,
                                        p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                        p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                        p_rule_value              sfrrgfe.sfrrgfe_levl_code_crse%type,
                                        p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                        p_gmod_code_in            stvgmod.stvgmod_code%type,
                                        p_schd_code_in            stvschd.stvschd_code%type,
                                        p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
         AND sfrrgfe_levl_code_crse = p_rule_value
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND (sfrrgfe_gmod_code = p_gmod_code_in OR
             sfrrgfe_gmod_code IS NULL)
         AND (sfrrgfe_schd_code = p_schd_code_in OR
             sfrrgfe_schd_code IS NULL)
         AND (sfrrgfe_insm_code = p_insm_code_in OR
             sfrrgfe_insm_code IS NULL)
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date));
    --- Attr type cursor
    CURSOR attr_rules_cursor(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                             p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                             p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                             p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                             p_add_date                date,
                             p_term_in                 stvterm.stvterm_code%type,
                             p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                             p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                             p_rule_value              sfrrgfe.sfrrgfe_attr_code_crse%type,
                             p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                             p_gmod_code_in            stvgmod.stvgmod_code%type,
                             p_schd_code_in            stvschd.stvschd_code%type,
                             p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_attr_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date))
            -- curriculum
         AND sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
         AND (sotfcur_lfst_order = 1 and
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
             (sotfcur_lfst_order = 2 AND
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
         AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
         AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
         AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
         AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
         AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
             nvl(sotfcur_rate_code, '@')
         AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
             nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR attr_rules_cursor_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                  p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                  p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                  p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                  p_add_date                date,
                                  p_term_in                 stvterm.stvterm_code%type,
                                  p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                  p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                  p_rule_value              sfrrgfe.sfrrgfe_attr_code_crse%type,
                                  p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                  p_gmod_code_in            stvgmod.stvgmod_code%type,
                                  p_schd_code_in            stvschd.stvschd_code%type,
                                  p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sotfcur, sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_attr_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND (sfrrgfe_gmod_code = p_gmod_code_in OR
             sfrrgfe_gmod_code IS NULL)
         AND (sfrrgfe_schd_code = p_schd_code_in OR
             sfrrgfe_schd_code IS NULL)
         AND (sfrrgfe_insm_code = p_insm_code_in OR
             sfrrgfe_insm_code IS NULL)
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date))
            -- curriculum
         AND sotfcur_pidm = pidm_in
         AND ((sotfcur_order = 1 and
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('P', 'A')) OR
             (sotfcur_order = 2 AND
             nvl(sfrrgfe_prim_sec_cde, 'A') in ('S', 'A')))
         AND (sotfcur_lfst_order = 1 and
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('P', 'A') or
             (sotfcur_lfst_order = 2 AND
             nvl(sfrrgfe_lfst_prim_sec_cde, 'A') in ('S', 'A')))
         AND NVL(sfrrgfe_majr_code, sotfcur_majr_code) = sotfcur_majr_code
         AND NVL(sfrrgfe_lfst_code, sotfcur_lfst_code) = sotfcur_lfst_code
         AND NVL(sfrrgfe_dept_code, nvl(sotfcur_dept_code, '@')) =
             nvl(sotfcur_dept_code, '@')
         AND NVL(sfrrgfe_levl_code, sotfcur_levl_code) = sotfcur_levl_code
         AND NVL(sfrrgfe_coll_code, sotfcur_coll_code) = sotfcur_coll_code
         AND NVL(sfrrgfe_camp_code, nvl(sotfcur_camp_code, '@')) =
             nvl(sotfcur_camp_code, '@')
         AND NVL(sfrrgfe_degc_code, sotfcur_degc_code) = sotfcur_degc_code
         AND NVL(sfrrgfe_program, nvl(sotfcur_program, '@')) =
             nvl(sotfcur_program, '@')
         AND NVL(sfrrgfe_term_code_admit, nvl(sotfcur_term_code_admit, '@')) =
             nvl(sotfcur_term_code_admit, '@')
         AND NVL(sfrrgfe_rate_code_curric, nvl(sotfcur_rate_code, '@')) =
             nvl(sotfcur_rate_code, '@')
         AND NVL(sfrrgfe_styp_code_curric, nvl(sotfcur_styp_code, '@')) =
             nvl(sotfcur_styp_code, '@')
       group by SFRRGFE_TERM_CODE,
                SFRRGFE_DETL_CODE,
                SFRRGFE_ACTIVITY_DATE,
                SFRRGFE_PER_CRED_CHARGE,
                SFRRGFE_MIN_CHARGE,
                SFRRGFE_MAX_CHARGE,
                SFRRGFE_PTRM_CODE,
                SFRRGFE_RESD_CODE,
                SFRRGFE_LEVL_CODE,
                SFRRGFE_COLL_CODE,
                SFRRGFE_MAJR_CODE,
                SFRRGFE_CLAS_CODE,
                SFRRGFE_RATE_CODE,
                SFRRGFE_CRSE_WAIV_IND,
                SFRRGFE_FROM_CRED_HRS,
                SFRRGFE_TO_CRED_HRS,
                SFRRGFE_FROM_ADD_DATE,
                SFRRGFE_TO_ADD_DATE,
                SFRRGFE_TYPE,
                SFRRGFE_CAMP_CODE,
                SFRRGFE_LEVL_CODE_CRSE,
                SFRRGFE_CAMP_CODE_CRSE,
                SFRRGFE_ENTRY_TYPE,
                SFRRGFE_FROM_STUD_HRS,
                SFRRGFE_TO_STUD_HRS,
                SFRRGFE_CRED_IND,
                SFRRGFE_FLAT_HRS,
                SFRRGFE_COPY_IND,
                SFRRGFE_SEQNO,
                SFRRGFE_PROGRAM,
                SFRRGFE_DEPT_CODE,
                SFRRGFE_DEGC_CODE,
                SFRRGFE_STYP_CODE,
                SFRRGFE_TERM_CODE_ADMIT,
                SFRRGFE_ATTR_CODE_CRSE,
                SFRRGFE_ATTS_CODE,
                SFRRGFE_GMOD_CODE,
                SFRRGFE_ASSESS_BY_COURSE_IND,
                SFRRGFE_USER_ID,
                SFRRGFE_FROM_FLAT_HRS,
                SFRRGFE_TO_FLAT_HRS,
                SFRRGFE_FLAT_FEE_AMOUNT,
                SFRRGFE_CRSE_OVERLOAD_START_HR,
                SFRRGFE_INSM_CODE,
                SFRRGFE_SCHD_CODE,
                SFRRGFE_LFST_CODE,
                SFRRGFE_PRIM_SEC_CDE,
                SFRRGFE_RATE_CODE_CURRIC,
                SFRRGFE_STYP_CODE_CURRIC,
                SFRRGFE_CHRT_CODE,
                SFRRGFE_VTYP_CODE,
                SFRRGFE_LFST_PRIM_SEC_CDE;

    CURSOR attr_rules_cursor_nocc(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                  p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                  p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                  p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                  p_add_date                date,
                                  p_term_in                 stvterm.stvterm_code%type,
                                  p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                  p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                  p_rule_value              sfrrgfe.sfrrgfe_attr_code_crse%type,
                                  p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                  p_gmod_code_in            stvgmod.stvgmod_code%type,
                                  p_schd_code_in            stvschd.stvschd_code%type,
                                  p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_attr_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date));

    CURSOR attr_rules_cursor_nocc_crse(p_bf_ptrm                 stvptrm.stvptrm_code%TYPE,
                                       p_bf_resd                 sgbstdn.sgbstdn_resd_code%TYPE,
                                       p_bf_rate                 sgbstdn.sgbstdn_rate_code%TYPE,
                                       p_bf_styp                 sgbstdn.sgbstdn_styp_code%TYPE,
                                       p_add_date                date,
                                       p_term_in                 stvterm.stvterm_code%type,
                                       p_rule_type_in            sfrrgfe.sfrrgfe_type%type,
                                       p_rule_entry_type         sfrrgfe.sfrrgfe_entry_type%type,
                                       p_rule_value              sfrrgfe.sfrrgfe_attr_code_crse%type,
                                       p_assess_by_course_ind_in sfrrgfe.sfrrgfe_assess_by_course_ind%type,
                                       p_gmod_code_in            stvgmod.stvgmod_code%type,
                                       p_schd_code_in            stvschd.stvschd_code%type,
                                       p_insm_code_in            gtvinsm.gtvinsm_code%type) IS
      SELECT SFRRGFE_TERM_CODE,
             SFRRGFE_DETL_CODE,
             SFRRGFE_ACTIVITY_DATE,
             SFRRGFE_PER_CRED_CHARGE,
             SFRRGFE_MIN_CHARGE,
             SFRRGFE_MAX_CHARGE,
             SFRRGFE_PTRM_CODE,
             SFRRGFE_RESD_CODE,
             SFRRGFE_LEVL_CODE,
             SFRRGFE_COLL_CODE,
             SFRRGFE_MAJR_CODE,
             SFRRGFE_CLAS_CODE,
             SFRRGFE_RATE_CODE,
             SFRRGFE_CRSE_WAIV_IND,
             SFRRGFE_FROM_CRED_HRS,
             SFRRGFE_TO_CRED_HRS,
             SFRRGFE_FROM_ADD_DATE,
             SFRRGFE_TO_ADD_DATE,
             SFRRGFE_TYPE,
             SFRRGFE_CAMP_CODE,
             SFRRGFE_LEVL_CODE_CRSE,
             SFRRGFE_CAMP_CODE_CRSE,
             SFRRGFE_ENTRY_TYPE,
             SFRRGFE_FROM_STUD_HRS,
             SFRRGFE_TO_STUD_HRS,
             SFRRGFE_CRED_IND,
             SFRRGFE_FLAT_HRS,
             SFRRGFE_COPY_IND,
             SFRRGFE_SEQNO,
             SFRRGFE_PROGRAM,
             SFRRGFE_DEPT_CODE,
             SFRRGFE_DEGC_CODE,
             SFRRGFE_STYP_CODE,
             SFRRGFE_TERM_CODE_ADMIT,
             SFRRGFE_ATTR_CODE_CRSE,
             SFRRGFE_ATTS_CODE,
             SFRRGFE_GMOD_CODE,
             SFRRGFE_ASSESS_BY_COURSE_IND,
             SFRRGFE_USER_ID,
             SFRRGFE_FROM_FLAT_HRS,
             SFRRGFE_TO_FLAT_HRS,
             SFRRGFE_FLAT_FEE_AMOUNT,
             SFRRGFE_CRSE_OVERLOAD_START_HR,
             SFRRGFE_INSM_CODE,
             SFRRGFE_SCHD_CODE,
             SFRRGFE_LFST_CODE,
             SFRRGFE_PRIM_SEC_CDE,
             SFRRGFE_RATE_CODE_CURRIC,
             SFRRGFE_STYP_CODE_CURRIC,
             SFRRGFE_CHRT_CODE,
             SFRRGFE_VTYP_CODE,
             SFRRGFE_LFST_PRIM_SEC_CDE
        FROM sfrrgfe
       WHERE sfrrgfe_term_code = p_term_in
         AND sfrrgfe_type = p_rule_type_in
         AND sfrrgfe_attr_code_crse = p_rule_value
         AND sfrrgfe_entry_type = p_rule_entry_type
         AND sfrrgfe_assess_by_course_ind = p_assess_by_course_ind_in
            -- part of the index
         AND nvl(sfrrgfe_ptrm_code, p_bf_ptrm) = p_bf_ptrm
         AND nvl(sfrrgfe_resd_code, p_bf_resd) = p_bf_resd
         AND nvl(sfrrgfe_rate_code, p_bf_rate) = p_bf_rate
         AND nvl(sfrrgfe_styp_code, p_bf_styp) = p_bf_styp
            --- optional
         AND (sfrrgfe_gmod_code = p_gmod_code_in OR
             sfrrgfe_gmod_code IS NULL)
         AND (sfrrgfe_schd_code = p_schd_code_in OR
             sfrrgfe_schd_code IS NULL)
         AND (sfrrgfe_insm_code = p_insm_code_in OR
             sfrrgfe_insm_code IS NULL)
         AND ((sfrrgfe_from_add_date IS NULL AND
             sfrrgfe_to_add_date IS NULL) OR
             TRUNC(p_add_date) BETWEEN TRUNC(sfrrgfe_from_add_date) AND
             TRUNC(sfrrgfe_to_add_date));

    procedure p_apply_rules_logic is
    begin
      RULE_MET     := TRUE;
      FLAT_FEE_MET := FALSE;

      /* ******************************************************************* */
      /*  Need to parse out rule processing due to limitations now in        */
      /*  Oracle for evaluating AND/OR logic. Evaluate the rule values on    */
      /*  a record by record basis for each row returned from the cursor.    */
      /*  Evaluate the rule values in steps, leaving the evaluation once the */
      /*  rule is considered not met.                                        */
      /* ******************************************************************* */

      --  skip rule if CRN flagged for assessment rule waive (SFRSTCR_WAIV_HR = 0)
      --  AND rule flagged for course fee O/R
      IF (t_reg_waiv_hr = 0 AND sfrrgfe_rec.sfrrgfe_crse_waiv_ind = 'Y') THEN
        RULE_MET := FALSE;
      END IF;

      p_print_dbms('rule met for:' || sfrrgfe_rec.sfrrgfe_seqno || '  ' ||
                   sfrrgfe_rec.sfrrgfe_detl_code);
      IF (RULE_MET) THEN
        IF sfrrgfe_rec.sfrrgfe_ptrm_code IS NOT NULL THEN
          IF t_ptrm_rule <> sfrrgfe_rec.sfrrgfe_ptrm_code THEN
            RULE_MET := FALSE;
          END IF;
        END IF;
      END IF;
      --  8.2 only calculate the class if it's used in a rule
      IF (RULE_MET) THEN
        IF sfrrgfe_rec.sfrrgfe_clas_code IS NOT NULL THEN
          p_print_dbms('Save class code: ' || save_clas_code || ' level: ' ||
                       save_primary_levl_code);
          if save_clas_code is null then
            SOKLIBS.p_class_calc(pidm_in,
                                 save_primary_levl_code,
                                 term_in,
                                 TRANSLATE(sobterm_rec.sobterm_incl_attmpt_hrs_ind,
                                           'Y',
                                           'A'),
                                 save_clas_code,
                                 clas_desc);
            if save_clas_code is null then
              save_clas_code := ' ';
            end if;
          end if;
          p_print_dbms('save clas: ' || save_clas_code || ' rgfe: ' ||
                       sfrrgfe_rec.sfrrgfe_clas_code);
          IF save_clas_code <> sfrrgfe_rec.sfrrgfe_clas_code THEN
            RULE_MET := FALSE;
          END IF;
        END IF;
      END IF;
      -- determine if student has student attribute specified in rule
      IF (RULE_MET) THEN
        IF sfrrgfe_rec.sfrrgfe_atts_code IS NOT NULL THEN
          if sgksels.f_query_sgrsatt(p_pidm          => pidm_in,
                                     P_term_code_eff => term_in,
                                     P_atts_code     => sfrrgfe_rec.sfrrgfe_atts_code,
                                     p_multiple_ind  => 'S') = 'Y' THEN
            RULE_MET := TRUE;
          ELSE
            RULE_MET := FALSE;
          END IF;
        END IF;
      END IF;

      -- determine if student has student cohort specified in rule
      IF (RULE_MET) THEN
        IF sfrrgfe_rec.sfrrgfe_chrt_code IS NOT NULL THEN
          if sgksels.f_query_sgrchrt(p_pidm          => pidm_in,
                                     P_term_code_eff => term_in,
                                     P_chrt_code     => sfrrgfe_rec.sfrrgfe_chrt_code,
                                     p_multiple_ind  => 'S') = 'Y' then
            RULE_MET := TRUE;
          ELSE
            RULE_MET := FALSE;
          END IF;
        end if;
      end if;

      -- determine if student has student vsa ty[e specified in rule
      IF (RULE_MET) THEN
        IF sfrrgfe_rec.sfrrgfe_vtyp_code IS NOT NULL THEN
          var_vtyp_code_current := gokvisa.f_check_visa(par_id         => gb_common.f_get_id(pidm_in),
                                                        par_as_of_date => sysdate);
          if var_vtyp_code_current is not null and
             var_vtyp_code_current = sfrrgfe_rec.sfrrgfe_vtyp_code then
            RULE_MET := TRUE;
          ELSE
            RULE_MET := FALSE;
          END IF;
        end if;
      end if;

      -- determine if part of flat,  or is a per credit that picks up after the flat
      --  the rule is the flat from hours minus the per credit to hours must be
      --  >0 and < 1 to be included with the flat liability
      --  If we are processing the percredit, and there were previous flats, do not process
      --  the rule if it falls into the >0 and <1 range of another flat
      p_print_dbms('in applyrules, before check if matches flat: ' ||
                   flat_ranges.count || ' process flat: ' ||
                   process_flat_from_hrs || ' rgfe flat: ' ||
                   sfrrgfe_rec.sfrrgfe_from_flat_hrs);
      if nvl(process_flat_from_hrs, 0) > 0 and
         nvl(process_flat_to_hrs, 0) > 0 then
        p_print_dbms('is flat: ' || process_flat_from_hrs || ' to ' ||
                     process_flat_to_hrs || ' credit: ' ||
                     sfrrgfe_rec.sfrrgfe_from_cred_hrs || ' to: ' ||
                     sfrrgfe_rec.sfrrgfe_to_cred_hrs || ' rgfe flat: ' ||
                     sfrrgfe_rec.sfrrgfe_from_flat_hrs || ' to: ' ||
                     sfrrgfe_rec.sfrrgfe_to_flat_hrs);

        if sfrrgfe_rec.sfrrgfe_from_flat_hrs = process_flat_from_hrs and
           sfrrgfe_rec.sfrrgfe_to_flat_hrs = process_flat_to_hrs then
          p_print_dbms('OK of rule at 1');
        elsif nvl(sfrrgfe_rec.sfrrgfe_from_flat_hrs, 0) > 0 and
              nvl(sfrrgfe_rec.sfrrgfe_from_flat_hrs, 0) <>
              process_flat_from_hrs and
              nvl(sfrrgfe_rec.sfrrgfe_to_flat_hrs, 0) <>
              process_flat_to_hrs then
          p_print_dbms('failure of rule at 1, flats do not match');
          RULE_MET := FALSE;
        elsif nvl(sfrrgfe_rec.sfrrgfe_from_flat_hrs, 0) = 0 and
              ((process_flat_from_hrs -
               nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0) < 1 and
               process_flat_from_hrs -
               nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0) >= 0)) then
          cnt                   := 1;
          multiple_flat_met_cnt := 0;
          FOR cnt in 1 .. flat_ranges.count loop
            flat_rec := flat_ranges(cnt);

            if (flat_rec.r_flat_from_hrs -
               nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0) < 1 and
               flat_rec.r_flat_from_hrs -
               nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0) >= 0)

             then
              p_print_dbms('failure of rule at 5b, per credit overlaps with flat rule ');
              multiple_flat_met_cnt := multiple_flat_met_cnt + 1;
            end if;
          end loop;
          --  the per credit rule can only be tied to one flat, so if it overlaps with multiple flat rules it will
          --  always be treated as a per credit hour.  If it overlaps with one flat,or is within 1 hour of the flat
          --  range it will carry the flat liability
          if multiple_flat_met_cnt > 1 then
            RULE_MET := FALSE;
            p_print_dbms('failure of rule at 3 - credit overlap with flat');
          end if;

        else
          RULE_MET := FALSE;
          p_print_dbms('failure of rule at 4 - not flat and not overlap');
        end if;
      else
        if flat_ranges.count > 0 then
          if nvl(sfrrgfe_rec.sfrrgfe_from_flat_hrs, 0) > 0 then
            -- we may need to pick up new flat ranges , so eliminate the ones already processed

            cnt := 1;
            FOR cnt in 1 .. flat_ranges.count loop
              flat_rec := flat_ranges(cnt);
              p_print_dbms('in flat loop to check if flat rule is new: ' ||
                           flat_rec.r_flat_from_hrs || ' to ' ||
                           flat_rec.r_flat_to_hrs || ' rgfe flat: ' ||
                           sfrrgfe_rec.sfrrgfe_from_flat_hrs || ' to: ' ||
                           sfrrgfe_rec.sfrrgfe_to_flat_hrs || ' ct: ' || cnt || ' ' ||
                           flat_ranges.count);

              if (flat_rec.r_flat_from_hrs =
                 nvl(sfrrgfe_rec.sfrrgfe_from_flat_hrs, 0) and
                 flat_rec.r_flat_to_hrs =
                 nvl(sfrrgfe_rec.sfrrgfe_to_flat_hrs, 0)) THEN
                p_print_dbms('failure of rule at 5a flat already processed');
                RULE_MET := FALSE;
                exit;
              end if;
            end loop;

          else
            --- per credit
            cnt                   := 1;
            multiple_flat_met_cnt := 0;
            FOR cnt in 1 .. flat_ranges.count loop
              flat_rec := flat_ranges(cnt);
              p_print_dbms('in flat loop to check for 1 diff or overlap: ' ||
                           flat_rec.r_flat_from_hrs || ' to ' ||
                           flat_rec.r_flat_to_hrs || ' credit: ' ||
                           sfrrgfe_rec.sfrrgfe_from_cred_hrs || ' to: ' ||
                           sfrrgfe_rec.sfrrgfe_to_cred_hrs || ' ct: ' || cnt || ' ' ||
                           flat_ranges.count);
              xx := flat_rec.r_flat_from_hrs -
                    nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0);
              xy := flat_rec.r_flat_from_hrs -
                    nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0);
              p_print_dbms('NOT for a match: this is less then 1: ' || xx ||
                           '  and this is be >= 0 ' || xy);

              if ((flat_rec.r_flat_from_hrs -
                 nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0) < 1 and
                 flat_rec.r_flat_from_hrs -
                 nvl(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 0) >= 0)) THEN
                p_print_dbms('failure of rule at 5, per credit is part of flat rule ');
                multiple_flat_met_cnt := multiple_flat_met_cnt + 1;

              end if;
            end loop;
            --  the per credit rule can only be tied to one flat, so if it is within 1 hour for multiple flat rules it will
            --  always be treated as a per credit hour.  If it is within 1 hour of  only one flat
            --  range it will carry that flat liability
            if multiple_flat_met_cnt = 1 then
              RULE_MET := FALSE;
            end if;
          end if; -- per credit or flat
        end if; --- no flat
      end if; --- processing flat or not

      p_print_dbms('Apply rules met sfrrgfe seqno: ' ||
                   sfrrgfe_rec.sfrrgfe_seqno || ' rate ' ||
                   sfrrgfe_rec.sfrrgfe_rate_code || ' detl ' ||
                   sfrrgfe_rec.sfrrgfe_detl_code);

      IF (RULE_MET) THEN
        -- get DCAT code to use in remaining processing, then go on to do the work
        open sel_dcat_c(p_detail => sfrrgfe_rec.sfrrgfe_detl_code);
        fetch sel_dcat_c
          into dcat_code;
        close sel_dcat_c;

        /* ******************************************************************* */
        /* Always determine the credit hours and student hours for the rule    */
        /* qualification.                                                      */
        /*                                                                     */
        /* For credit hour determination, values in t_% variables will be      */
        /* either for a specific course or will be the totals for the grouping.*/
        /* When the hours data held in the t_% variables are for a specific    */
        /* course in a grouping, the rule will be evaluated where the assess   */
        /* by course indicator (SFRRGFE_ASSESS_BY_COURSE_IND) is set to 'Y'.   */
        /*                                                                     */
        /* For student hour determination, the values in t_stud% variables are */
        /* for the student's overall liable hours.                             */
        /*                                                                     */
        /* The SFRRGFE_FROM/SFRRGFE_TO_STUD_HRS are only used by LEVEL, CAMPUS */
        /* and ATTR rules; will always be NULL for STUDENT rules.              */
        /*                                                                     */
        /* If sfrrgfe_crse_waiv_ind = Y, total waived hours for the course(s)  */
        /* are used. If sfrrgfe_crse_waiv_ind = N, total bill hours are used.  */
        /*                                                                     */
        /* Automatically meet rule if no credit or student hours are specified */
        /* in the rule; otherwise go on to compare the determined hour values. */
        /* ******************************************************************* */
        p_print_dbms('in p apply rules, before f calc rule hrs 1:  t_reg_bill_hr ' ||
                     t_reg_bill_hr || ' t_liab_bill_hr_tuit: ' ||
                     t_liab_bill_hr_tuit || 'waiv:' ||
                     sfrrgfe_rec.sfrrgfe_crse_waiv_ind);
        rule_cred_hrs := ME_SFKFEES.f_calc_rule_hrs(dcat_code,
                                                    sfrrgfe_rec.sfrrgfe_crse_waiv_ind,
                                                    t_reg_bill_hr,
                                                    t_liab_bill_hr_tuit,
                                                    t_liab_bill_hr_fees,
                                                    t_reg_waiv_hr,
                                                    t_liab_waiv_hr_tuit,
                                                    t_liab_waiv_hr_fees);
        p_print_dbms('return with rule cred hrs ' || rule_cred_hrs);
        p_print_dbms('in p apply rules, before f calc rule hrs 2:  t_reg_bill_hr ' ||
                     t_reg_bill_hr || ' t_liab_bill_hr_tuit: ' ||
                     t_liab_bill_hr_tuit || ' t_stud_liab_bill_hr_tuit: ' ||
                     t_stud_liab_bill_hr_tuit);
        rule_stud_hrs := ME_SFKFEES.f_calc_rule_hrs(dcat_code,
                                                    sfrrgfe_rec.sfrrgfe_crse_waiv_ind,
                                                    t_reg_bill_hr,
                                                    t_stud_liab_bill_hr_tuit,
                                                    t_stud_liab_bill_hr_fees,
                                                    t_reg_waiv_hr,
                                                    t_stud_liab_waiv_hr_tuit,
                                                    t_stud_liab_waiv_hr_fees);
        p_print_dbms('return with rule stud hrs ' || rule_stud_hrs);
        p_print_dbms('after f calc rule hrs cred hrs ' || rule_cred_hrs ||
                     ' rule std hrs: ' || rule_stud_hrs);
        IF (sfrrgfe_rec.sfrrgfe_from_cred_hrs IS NULL AND
           sfrrgfe_rec.sfrrgfe_to_cred_hrs IS NULL) THEN
          RULE_MET := TRUE;
        ELSE
          IF (rule_cred_hrs NOT BETWEEN
             NVL(sfrrgfe_rec.sfrrgfe_from_cred_hrs, 0) AND
             NVL(sfrrgfe_rec.sfrrgfe_to_cred_hrs, 999999.999)) THEN
            RULE_MET := FALSE;
          END IF;
        END IF;

        IF (RULE_MET) THEN
          IF (sfrrgfe_rec.sfrrgfe_from_stud_hrs IS NULL AND
             sfrrgfe_rec.sfrrgfe_to_stud_hrs IS NULL) THEN
            RULE_MET := TRUE;
          ELSIF (rule_stud_hrs NOT BETWEEN
                NVL(sfrrgfe_rec.sfrrgfe_from_stud_hrs, 0) AND
                NVL(sfrrgfe_rec.sfrrgfe_to_stud_hrs, 999999.999)) THEN
            RULE_MET := FALSE;
          END IF;
        END IF;

        -- Qualify the flat hour range and save the flat fee amount.
        IF (RULE_MET) THEN
          IF (sfrrgfe_rec.sfrrgfe_from_flat_hrs IS NULL AND
             sfrrgfe_rec.sfrrgfe_to_flat_hrs IS NULL) THEN
            IF sfrrgfe_rec.sfrrgfe_flat_fee_amount IS NOT NULL THEN
              flat_fee_amt := sfrrgfe_rec.sfrrgfe_flat_fee_amount;
              flat_fee_met := TRUE;
            ELSE
              flat_fee_amt := 0;
            END IF;
          ELSIF (rule_cred_hrs BETWEEN sfrrgfe_rec.sfrrgfe_from_flat_hrs AND
                sfrrgfe_rec.sfrrgfe_to_flat_hrs) THEN
            flat_fee_amt := NVL(sfrrgfe_rec.sfrrgfe_flat_fee_amount, 0);
            flat_fee_met := TRUE;
          ELSE
            RULE_MET := FALSE;
          END IF;
        END IF;

        -- Calculate the per credit charge or the overload/'plus per credit' charge.
        IF (RULE_MET) THEN
          IF (FLAT_FEE_MET) THEN
            -- determine if overload charging is applicable
            IF rule_cred_hrs >= sfrrgfe_rec.sfrrgfe_crse_overload_start_hr THEN
              overload_hrs  := (rule_cred_hrs -
                               sfrrgfe_rec.sfrrgfe_crse_overload_start_hr);
              overload_chrg := (overload_hrs *
                               sfrrgfe_rec.sfrrgfe_per_cred_charge);
            END IF;
            aud_chrg := (flat_fee_amt + overload_chrg);
          ELSE
            -- not flat charge, so calc conventional per credit charge
            per_cred_chrg := f_calc_per_cred_charge(sfrrgfe_rec.sfrrgfe_max_charge,
                                                    sfrrgfe_rec.sfrrgfe_min_charge,
                                                    dcat_code,
                                                    sfrrgfe_rec.sfrrgfe_crse_waiv_ind,
                                                    t_reg_bill_hr, -- for non-TUI/FEE DCAT codes
                                                    t_liab_bill_hr_tuit,
                                                    t_liab_bill_hr_fees,
                                                    t_reg_waiv_hr, -- for non-TUI/FEE DCAT codes
                                                    t_liab_waiv_hr_tuit,
                                                    t_liab_waiv_hr_fees,
                                                    sfrrgfe_rec.sfrrgfe_per_cred_charge);
            aud_chrg      := per_cred_chrg;
          END IF;
        END IF;
      END IF;

      IF (RULE_MET) THEN
        --- create the faud record for the N hrs for the type and value
        --- 8.0.1  create the N type faud record with non drop for type and  rule value
        if rule_type_in <> STUDENT_TYPE then
          if NOT (N_FAUD_CREATED) then
            p_create_nondrop_type(pidm_in       => pidm_in,
                                  term_in       => term_in,
                                  source_pgm_in => source_pgm_in,
                                  type_in       => rule_type_in,
                                  rule_value_in => rule_value_in);
          end if;
        end if;
      END IF;
      -- If the rule is met, there should be a charge generated. Make sure
      -- the charge is within the min/max for the rule before recording it.
      IF (RULE_MET) THEN
        IF aud_chrg > sfrrgfe_rec.sfrrgfe_max_charge THEN
          note     := g$_nls.get('SFKFEE1-0017',
                                 'SQL',
                                 '%01% %02% >rule max %03%, reset to max',
                                 note,
                                 aud_chrg,
                                 sfrrgfe_rec.sfrrgfe_max_charge);
          aud_chrg := sfrrgfe_rec.sfrrgfe_max_charge;
        ELSIF aud_chrg < sfrrgfe_rec.sfrrgfe_min_charge THEN
          note     := g$_nls.get('SFKFEE1-0018',
                                 'SQL',
                                 '%01% %02% <rule min %03%, reset to min',
                                 note,
                                 aud_chrg,
                                 sfrrgfe_rec.sfrrgfe_min_charge);
          aud_chrg := sfrrgfe_rec.sfrrgfe_min_charge;
        END IF;

        /* ************************************************************ */
        /* Allow the determined liability for $0.00 to be recorded in   */
        /* SFRFAUD so as to allow p_totalfees to enforce SFRFMAX min    */
        /* and max values for the detail code for the term.             */
        /* ************************************************************ */
        -- determine the next seqno for the audit record and insert it
        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        -- if assessing by course, record the CRN in SFRFAUD
        IF sfrrgfe_rec.sfrrgfe_assess_by_course_ind = 'Y' THEN
          aud_crn       := t_crn;
          aud_rsts_code := t_rsts_code;
          aud_rsts_date := t_rsts_date;
        ELSE
          aud_crn       := NULL;
          aud_rsts_code := NULL;
          aud_rsts_date := NULL;
        END IF;

        IF NVL(swap_assess_rfnd_cde, 'A') = 'S' THEN
          -- swap liability
          note := g$_nls.get('SFKFEE1-0019', 'SQL', 'SWAP %01%', note);
        END IF;
        --- 8.1.1 note that the liability is now 0 if the charge is 0
        if nvl(aud_chrg, 0) = 0 and note is null then
          note := g$_nls.get('SFKFEE1-0020',
                             'SQL',
                             'No longer liable for charges to %01%.',
                             sfrrgfe_rec.sfrrgfe_detl_code);
          p_print_dbms('p_insert_sfrfaud 7 note: ' || note);
        end if;

        p_print_dbms('p_insert_sfrfaud 7 :' || rule_stud_hrs ||
                     ' does it match the flat: ' ||
                     sfrrgfe_rec.sfrrgfe_flat_fee_amount ||
                     ' swap ass rnd cde: ' || swap_assess_rfnd_cde ||
                     ' flat: ' || sfrrgfe_rec.sfrrgfe_from_flat_hrs ||
                     ' to: ' || sfrrgfe_rec.sfrrgfe_to_flat_hrs);
        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         NVL(swap_assess_rfnd_cde, 'A'),
                         dcat_code,
                         sfrrgfe_rec.sfrrgfe_detl_code,
                         --  8.1.1 add nvl to prevent process from aborting if amt is 0, which may
                         --  happen if rule from previous charge was deleted
                         nvl(aud_chrg, 0), -- per credit OR flat fee + plus per credit
                         sfrrgfe_rec.sfrrgfe_assess_by_course_ind,
                         source_pgm_in,
                         sfrrgfe_rec.sfrrgfe_seqno,
                         sfrrgfe_rec.sfrrgfe_type,
                         aud_crn,
                         aud_rsts_code,
                         aud_rsts_date,
                         sfbetrm_rec.sfbetrm_ests_code,
                         sfbetrm_rec.sfbetrm_ests_date,
                         lv_crn_rfnd_table,
                         t_reg_bill_hr, -- SFRSTCR_BILL_HR
                         t_reg_waiv_hr, -- SFRSTCR_WAIV_HR
                         rule_cred_hrs,
                         rule_stud_hrs,
                         per_cred_chrg, -- total per credit charge
                         flat_fee_amt, -- flat fee amount
                         overload_hrs, -- overload hrs for crse reg
                         overload_chrg, -- crse overload charge
                         NULL, -- populate TBRACCD tran when charge is inserted
                         note,
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         sfrrgfe_rec.sfrrgfe_per_cred_charge,
                         sfrrgfe_rec.sfrrgfe_flat_fee_amount,
                         sfrrgfe_rec.sfrrgfe_from_flat_hrs,
                         sfrrgfe_rec.sfrrgfe_to_flat_hrs,
                         sfrrgfe_rec.sfrrgfe_crse_overload_start_hr,
                         rule_value_in);
      END IF;

    end p_apply_rules_logic;

  BEGIN
    -- performance enhancement to bulk fetch NULL codes
    -- change assignment of 'z^' to ' ' to prevent value errors
    bf_ptrm := NVL(t_ptrm_rule, ' ');
    bf_resd := NVL(sgbstdn_rec.sgbstdn_resd_code, ' ');
    bf_clas := NVL(clas_code, ' ');
    bf_rate := NVL(sgbstdn_rec.sgbstdn_rate_code, ' ');
    bf_styp := NVL(sgbstdn_rec.sgbstdn_styp_code, ' ');

    --- use the   cursor

    CASE rule_type_in
      WHEN STUDENT_TYPE then
        if gv_rgfe_student_cc_exists = 'N' then
          if assess_by_course_ind_in = 'N' then
            OPEN student_rules_cursor_nocc(p_bf_ptrm                 => bf_ptrm,
                                           p_bf_resd                 => bf_resd,
                                           p_bf_rate                 => bf_rate,
                                           p_bf_styp                 => bf_styp,
                                           p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                           p_term_in                 => term_in,
                                           p_rule_type_in            => rule_type_in,
                                           p_rule_entry_type         => rule_entry_type,
                                           p_assess_by_course_ind_in => assess_by_course_ind_in,
                                           p_gmod_code_in            => gmod_code_in,
                                           p_schd_code_in            => schd_code_in,
                                           p_insm_code_in            => insm_code_in);
          else
            OPEN student_rules_cursor_nocc_crse(p_bf_ptrm                 => bf_ptrm,
                                                p_bf_resd                 => bf_resd,
                                                p_bf_rate                 => bf_rate,
                                                p_bf_styp                 => bf_styp,
                                                p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                                p_term_in                 => term_in,
                                                p_rule_type_in            => rule_type_in,
                                                p_rule_entry_type         => rule_entry_type,
                                                p_assess_by_course_ind_in => assess_by_course_ind_in,
                                                p_gmod_code_in            => gmod_code_in,
                                                p_schd_code_in            => schd_code_in,
                                                p_insm_code_in            => insm_code_in);
          end if;
        else
          if assess_by_course_ind_in = 'N' then
            OPEN student_rules_cursor(p_bf_ptrm                 => bf_ptrm,
                                      p_bf_resd                 => bf_resd,
                                      p_bf_rate                 => bf_rate,
                                      p_bf_styp                 => bf_styp,
                                      p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                      p_term_in                 => term_in,
                                      p_rule_type_in            => rule_type_in,
                                      p_rule_entry_type         => rule_entry_type,
                                      p_assess_by_course_ind_in => assess_by_course_ind_in,
                                      p_gmod_code_in            => gmod_code_in,
                                      p_schd_code_in            => schd_code_in,
                                      p_insm_code_in            => insm_code_in);
          else
            OPEN student_rules_cursor_crse(p_bf_ptrm                 => bf_ptrm,
                                           p_bf_resd                 => bf_resd,
                                           p_bf_rate                 => bf_rate,
                                           p_bf_styp                 => bf_styp,
                                           p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                           p_term_in                 => term_in,
                                           p_rule_type_in            => rule_type_in,
                                           p_rule_entry_type         => rule_entry_type,
                                           p_assess_by_course_ind_in => assess_by_course_ind_in,
                                           p_gmod_code_in            => gmod_code_in,
                                           p_schd_code_in            => schd_code_in,
                                           p_insm_code_in            => insm_code_in);
          end if;
        end if;
      WHEN LEVEL_TYPE then
        if gv_rgfe_level_cc_exists = 'N' then
          if assess_by_course_ind_in = 'N' then
            OPEN level_rules_cursor_nocc(p_bf_ptrm                 => bf_ptrm,
                                         p_bf_resd                 => bf_resd,
                                         p_bf_rate                 => bf_rate,
                                         p_bf_styp                 => bf_styp,
                                         p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                         p_term_in                 => term_in,
                                         p_rule_type_in            => rule_type_in,
                                         p_rule_entry_type         => rule_entry_type,
                                         p_rule_value              => rule_value_in,
                                         p_assess_by_course_ind_in => assess_by_course_ind_in,
                                         p_gmod_code_in            => gmod_code_in,
                                         p_schd_code_in            => schd_code_in,
                                         p_insm_code_in            => insm_code_in);
          else
            OPEN level_rules_cursor_nocc_crse(p_bf_ptrm                 => bf_ptrm,
                                              p_bf_resd                 => bf_resd,
                                              p_bf_rate                 => bf_rate,
                                              p_bf_styp                 => bf_styp,
                                              p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                              p_term_in                 => term_in,
                                              p_rule_type_in            => rule_type_in,
                                              p_rule_entry_type         => rule_entry_type,
                                              p_rule_value              => rule_value_in,
                                              p_assess_by_course_ind_in => assess_by_course_ind_in,
                                              p_gmod_code_in            => gmod_code_in,
                                              p_schd_code_in            => schd_code_in,
                                              p_insm_code_in            => insm_code_in);
          end if;
        else
          if assess_by_course_ind_in = 'N' then
            OPEN level_rules_cursor(p_bf_ptrm                 => bf_ptrm,
                                    p_bf_resd                 => bf_resd,
                                    p_bf_rate                 => bf_rate,
                                    p_bf_styp                 => bf_styp,
                                    p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                    p_term_in                 => term_in,
                                    p_rule_type_in            => rule_type_in,
                                    p_rule_entry_type         => rule_entry_type,
                                    p_rule_value              => rule_value_in,
                                    p_assess_by_course_ind_in => assess_by_course_ind_in,
                                    p_gmod_code_in            => gmod_code_in,
                                    p_schd_code_in            => schd_code_in,
                                    p_insm_code_in            => insm_code_in);
          else
            OPEN level_rules_cursor_crse(p_bf_ptrm                 => bf_ptrm,
                                         p_bf_resd                 => bf_resd,
                                         p_bf_rate                 => bf_rate,
                                         p_bf_styp                 => bf_styp,
                                         p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                         p_term_in                 => term_in,
                                         p_rule_type_in            => rule_type_in,
                                         p_rule_entry_type         => rule_entry_type,
                                         p_rule_value              => rule_value_in,
                                         p_assess_by_course_ind_in => assess_by_course_ind_in,
                                         p_gmod_code_in            => gmod_code_in,
                                         p_schd_code_in            => schd_code_in,
                                         p_insm_code_in            => insm_code_in);
          end if;
        end if;
      WHEN ATTR_TYPE then
        if gv_rgfe_attr_cc_exists = 'N' then
          if assess_by_course_ind_in = 'N' then
            OPEN attr_rules_cursor_nocc(p_bf_ptrm                 => bf_ptrm,
                                        p_bf_resd                 => bf_resd,
                                        p_bf_rate                 => bf_rate,
                                        p_bf_styp                 => bf_styp,
                                        p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                        p_term_in                 => term_in,
                                        p_rule_type_in            => rule_type_in,
                                        p_rule_entry_type         => rule_entry_type,
                                        p_rule_value              => rule_value_in,
                                        p_assess_by_course_ind_in => assess_by_course_ind_in,
                                        p_gmod_code_in            => gmod_code_in,
                                        p_schd_code_in            => schd_code_in,
                                        p_insm_code_in            => insm_code_in);
          else
            OPEN attr_rules_cursor_nocc_crse(p_bf_ptrm                 => bf_ptrm,
                                             p_bf_resd                 => bf_resd,
                                             p_bf_rate                 => bf_rate,
                                             p_bf_styp                 => bf_styp,
                                             p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                             p_term_in                 => term_in,
                                             p_rule_type_in            => rule_type_in,
                                             p_rule_entry_type         => rule_entry_type,
                                             p_rule_value              => rule_value_in,
                                             p_assess_by_course_ind_in => assess_by_course_ind_in,
                                             p_gmod_code_in            => gmod_code_in,
                                             p_schd_code_in            => schd_code_in,
                                             p_insm_code_in            => insm_code_in);
          end if;
        else
          if assess_by_course_ind_in = 'N' then
            OPEN attr_rules_cursor(p_bf_ptrm                 => bf_ptrm,
                                   p_bf_resd                 => bf_resd,
                                   p_bf_rate                 => bf_rate,
                                   p_bf_styp                 => bf_styp,
                                   p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                   p_term_in                 => term_in,
                                   p_rule_type_in            => rule_type_in,
                                   p_rule_entry_type         => rule_entry_type,
                                   p_rule_value              => rule_value_in,
                                   p_assess_by_course_ind_in => assess_by_course_ind_in,
                                   p_gmod_code_in            => gmod_code_in,
                                   p_schd_code_in            => schd_code_in,
                                   p_insm_code_in            => insm_code_in);
          else
            OPEN attr_rules_cursor_crse(p_bf_ptrm                 => bf_ptrm,
                                        p_bf_resd                 => bf_resd,
                                        p_bf_rate                 => bf_rate,
                                        p_bf_styp                 => bf_styp,
                                        p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                        p_term_in                 => term_in,
                                        p_rule_type_in            => rule_type_in,
                                        p_rule_entry_type         => rule_entry_type,
                                        p_rule_value              => rule_value_in,
                                        p_assess_by_course_ind_in => assess_by_course_ind_in,
                                        p_gmod_code_in            => gmod_code_in,
                                        p_schd_code_in            => schd_code_in,
                                        p_insm_code_in            => insm_code_in);
          end if;
        end if;
      WHEN CAMPUS_TYPE then
        if gv_rgfe_campus_cc_exists = 'N' then
          if assess_by_course_ind_in = 'N' then
            OPEN campus_rules_cursor_nocc(p_bf_ptrm                 => bf_ptrm,
                                          p_bf_resd                 => bf_resd,
                                          p_bf_rate                 => bf_rate,
                                          p_bf_styp                 => bf_styp,
                                          p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                          p_term_in                 => term_in,
                                          p_rule_type_in            => rule_type_in,
                                          p_rule_entry_type         => rule_entry_type,
                                          p_rule_value              => rule_value_in,
                                          p_assess_by_course_ind_in => assess_by_course_ind_in,
                                          p_gmod_code_in            => gmod_code_in,
                                          p_schd_code_in            => schd_code_in,
                                          p_insm_code_in            => insm_code_in);
          else
            OPEN campus_rules_cursor_nocc_crse(p_bf_ptrm                 => bf_ptrm,
                                               p_bf_resd                 => bf_resd,
                                               p_bf_rate                 => bf_rate,
                                               p_bf_styp                 => bf_styp,
                                               p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                               p_term_in                 => term_in,
                                               p_rule_type_in            => rule_type_in,
                                               p_rule_entry_type         => rule_entry_type,
                                               p_rule_value              => rule_value_in,
                                               p_assess_by_course_ind_in => assess_by_course_ind_in,
                                               p_gmod_code_in            => gmod_code_in,
                                               p_schd_code_in            => schd_code_in,
                                               p_insm_code_in            => insm_code_in);
          end if;
        else
          if assess_by_course_ind_in = 'N' then
            OPEN campus_rules_cursor(p_bf_ptrm                 => bf_ptrm,
                                     p_bf_resd                 => bf_resd,
                                     p_bf_rate                 => bf_rate,
                                     p_bf_styp                 => bf_styp,
                                     p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                     p_term_in                 => term_in,
                                     p_rule_type_in            => rule_type_in,
                                     p_rule_entry_type         => rule_entry_type,
                                     p_rule_value              => rule_value_in,
                                     p_assess_by_course_ind_in => assess_by_course_ind_in,
                                     p_gmod_code_in            => gmod_code_in,
                                     p_schd_code_in            => schd_code_in,
                                     p_insm_code_in            => insm_code_in);
          else
            OPEN campus_rules_cursor_crse(p_bf_ptrm                 => bf_ptrm,
                                          p_bf_resd                 => bf_resd,
                                          p_bf_rate                 => bf_rate,
                                          p_bf_styp                 => bf_styp,
                                          p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                          p_term_in                 => term_in,
                                          p_rule_type_in            => rule_type_in,
                                          p_rule_entry_type         => rule_entry_type,
                                          p_rule_value              => rule_value_in,
                                          p_assess_by_course_ind_in => assess_by_course_ind_in,
                                          p_gmod_code_in            => gmod_code_in,
                                          p_schd_code_in            => schd_code_in,
                                          p_insm_code_in            => insm_code_in);
          end if;
        end if;
      ELSE
        if gv_rgfe_student_cc_exists = 'N' then
          if assess_by_course_ind_in = 'N' then
            OPEN student_rules_cursor_nocc(p_bf_ptrm                 => bf_ptrm,
                                           p_bf_resd                 => bf_resd,
                                           p_bf_rate                 => bf_rate,
                                           p_bf_styp                 => bf_styp,
                                           p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                           p_term_in                 => term_in,
                                           p_rule_type_in            => rule_type_in,
                                           p_rule_entry_type         => rule_entry_type,
                                           p_assess_by_course_ind_in => assess_by_course_ind_in,
                                           p_gmod_code_in            => gmod_code_in,
                                           p_schd_code_in            => schd_code_in,
                                           p_insm_code_in            => insm_code_in);
          else
            OPEN student_rules_cursor_nocc_crse(p_bf_ptrm                 => bf_ptrm,
                                                p_bf_resd                 => bf_resd,
                                                p_bf_rate                 => bf_rate,
                                                p_bf_styp                 => bf_styp,
                                                p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                                p_term_in                 => term_in,
                                                p_rule_type_in            => rule_type_in,
                                                p_rule_entry_type         => rule_entry_type,
                                                p_assess_by_course_ind_in => assess_by_course_ind_in,
                                                p_gmod_code_in            => gmod_code_in,
                                                p_schd_code_in            => schd_code_in,
                                                p_insm_code_in            => insm_code_in);
          end if;
        else
          if assess_by_course_ind_in = 'N' then
            OPEN student_rules_cursor(p_bf_ptrm                 => bf_ptrm,
                                      p_bf_resd                 => bf_resd,
                                      p_bf_rate                 => bf_rate,
                                      p_bf_styp                 => bf_styp,
                                      p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                      p_term_in                 => term_in,
                                      p_rule_type_in            => rule_type_in,
                                      p_rule_entry_type         => rule_entry_type,
                                      p_assess_by_course_ind_in => assess_by_course_ind_in,
                                      p_gmod_code_in            => gmod_code_in,
                                      p_schd_code_in            => schd_code_in,
                                      p_insm_code_in            => insm_code_in);
          else
            OPEN student_rules_cursor_crse(p_bf_ptrm                 => bf_ptrm,
                                           p_bf_resd                 => bf_resd,
                                           p_bf_rate                 => bf_rate,
                                           p_bf_styp                 => bf_styp,
                                           p_add_date                => sfbetrm_rec.sfbetrm_add_date,
                                           p_term_in                 => term_in,
                                           p_rule_type_in            => rule_type_in,
                                           p_rule_entry_type         => rule_entry_type,
                                           p_assess_by_course_ind_in => assess_by_course_ind_in,
                                           p_gmod_code_in            => gmod_code_in,
                                           p_schd_code_in            => schd_code_in,
                                           p_insm_code_in            => insm_code_in);
          end if;
        end if;
    end case;

    loop
      rule_cred_hrs := 0;
      rule_stud_hrs := 0;
      dcat_code     := '';
      chrg          := 0;
      aud_chrg      := 0;
      per_cred_chrg := 0;
      flat_fee_amt  := 0;
      overload_hrs  := 0;
      overload_chrg := 0;
      note          := '';
      CASE rule_type_in
        WHEN STUDENT_TYPE then
          if assess_by_course_ind_in = 'N' then
            if gv_rgfe_student_cc_exists = 'N' then
              FETCH student_rules_cursor_nocc
                into Sfrrgfe_rec;
              exit when student_rules_cursor_nocc%notfound;
            else
              FETCH student_rules_cursor
                into Sfrrgfe_rec;
              exit when student_rules_cursor%notfound;
              p_print_dbms('select student records: ' ||
                           sfrrgfe_rec.sfrrgfe_seqno);
            end if;
          else
            if gv_rgfe_student_cc_exists = 'N' then
              FETCH student_rules_cursor_nocc_crse
                into Sfrrgfe_rec;
              exit when student_rules_cursor_nocc_crse%notfound;
            else
              FETCH student_rules_cursor_crse
                into Sfrrgfe_rec;
              exit when student_rules_cursor_crse%notfound;
            end if;
          end if;
        WHEN LEVEL_TYPE then
          if assess_by_course_ind_in = 'N' then
            if gv_rgfe_level_cc_exists = 'N' then
              FETCH level_rules_cursor_nocc
                into Sfrrgfe_rec;
              exit when level_rules_cursor_nocc%notfound;
            else
              FETCH level_rules_cursor
                into Sfrrgfe_rec;
              exit when level_rules_cursor%notfound;
            end if;
          else
            if gv_rgfe_level_cc_exists = 'N' then
              FETCH level_rules_cursor_nocc_crse
                into Sfrrgfe_rec;
              exit when level_rules_cursor_nocc_crse%notfound;
            else
              FETCH level_rules_cursor_crse
                into Sfrrgfe_rec;
              exit when level_rules_cursor_crse%notfound;
            end if;
          end if;
        WHEN ATTR_TYPE then
          if assess_by_course_ind_in = 'N' then
            if gv_rgfe_attr_cc_exists = 'N' then
              FETCH attr_rules_cursor_nocc
                into Sfrrgfe_rec;
              exit when attr_rules_cursor_nocc%notfound;
            else
              FETCH attr_rules_cursor
                into Sfrrgfe_rec;
              exit when attr_rules_cursor%notfound;
            end if;
          else
            if gv_rgfe_attr_cc_exists = 'N' then
              FETCH attr_rules_cursor_nocc_crse
                into Sfrrgfe_rec;
              exit when attr_rules_cursor_nocc_crse%notfound;
            else
              FETCH attr_rules_cursor_crse
                into Sfrrgfe_rec;
              exit when attr_rules_cursor_crse%notfound;
            end if;
          end if;
        WHEN CAMPUS_TYPE then
          if assess_by_course_ind_in = 'N' then
            if gv_rgfe_campus_cc_exists = 'N' then
              FETCH campus_rules_cursor_nocc
                into Sfrrgfe_rec;
              exit when campus_rules_cursor_nocc%notfound;
            else
              FETCH campus_rules_cursor
                into Sfrrgfe_rec;
              exit when campus_rules_cursor %notfound;
            end if;
          else
            if gv_rgfe_campus_cc_exists = 'N' then
              FETCH campus_rules_cursor_nocc_crse
                into Sfrrgfe_rec;
              exit when campus_rules_cursor_nocc_crse%notfound;
            else
              FETCH campus_rules_cursor_crse
                into Sfrrgfe_rec;
              exit when campus_rules_cursor_crse%notfound;
            end if;
          end if;
        ELSE
          if assess_by_course_ind_in = 'N' then
            if gv_rgfe_student_cc_exists = 'N' then
              FETCH student_rules_cursor_nocc
                into Sfrrgfe_rec;
              exit when student_rules_cursor_nocc%notfound;
            else
              FETCH student_rules_cursor
                into Sfrrgfe_rec;
              exit when student_rules_cursor%notfound;
            end if;
          else
            if gv_rgfe_student_cc_exists = 'N' then
              FETCH student_rules_cursor_nocc_crse
                into Sfrrgfe_rec;
              exit when student_rules_cursor_nocc_crse%notfound;
            else
              FETCH student_rules_cursor_crse
                into Sfrrgfe_rec;
              exit when student_rules_cursor_crse%notfound;
            end if;
          end if;
      end case;
      --  8.1.1  if flat hrs is > 99 change to 999 to make them all consistent
      if sfrrgfe_rec.sfrrgfe_to_flat_hrs >= max_flat_to_hours then
        p_print_dbms('change the flat to hrs to 9999 from: ' ||
                     sfrrgfe_rec.sfrrgfe_to_flat_hrs);
        sfrrgfe_rec.sfrrgfe_to_flat_hrs := max_flat_to_hours;
      end if;

      p_print_dbms('rules record: ' || sfrrgfe_rec.sfrrgfe_seqno ||
                   ' type: ' || sfrrgfe_rec.sfrrgfe_type);
      p_apply_rules_logic;

    END LOOP;
    CASE rule_type_in
      WHEN STUDENT_TYPE then
        if assess_by_course_ind_in = 'N' then
          if gv_rgfe_student_cc_exists = 'N' then
            CLOSE student_rules_cursor_nocc;
          else
            CLOSE student_rules_cursor;
          end if;
        else
          if gv_rgfe_student_cc_exists = 'N' then
            CLOSE student_rules_cursor_nocc_crse;
          else
            CLOSE student_rules_cursor_crse;
          end if;
        end if;
      WHEN LEVEL_TYPE then
        if assess_by_course_ind_in = 'N' then
          if gv_rgfe_level_cc_exists = 'N' then
            CLOSE level_rules_cursor_nocc;
          else
            CLOSE level_rules_cursor;
          end if;
        else
          if gv_rgfe_level_cc_exists = 'N' then
            CLOSE level_rules_cursor_nocc_crse;
          else
            CLOSE level_rules_cursor_crse;
          end if;
        end if;
      WHEN ATTR_TYPE then
        if assess_by_course_ind_in = 'N' then
          if gv_rgfe_attr_cc_exists = 'N' then
            CLOSE attr_rules_cursor_nocc;
          else
            CLOSE attr_rules_cursor;
          end if;
        else
          if gv_rgfe_attr_cc_exists = 'N' then
            CLOSE attr_rules_cursor_nocc_crse;
          else
            CLOSE attr_rules_cursor_crse;
          end if;
        end if;
      WHEN CAMPUS_TYPE then
        if assess_by_course_ind_in = 'N' then
          if gv_rgfe_campus_cc_exists = 'N' then
            CLOSE campus_rules_cursor_nocc;
          else
            CLOSE campus_rules_cursor;
          end if;
        else
          if gv_rgfe_campus_cc_exists = 'N' then
            CLOSE campus_rules_cursor_nocc_crse;
          else
            CLOSE campus_rules_cursor_crse;
          end if;
        end if;
      ELSE
        if assess_by_course_ind_in = 'N' then
          if gv_rgfe_student_cc_exists = 'N' then
            CLOSE student_rules_cursor_nocc;
          else
            CLOSE student_rules_cursor;
          end if;
        else
          if gv_rgfe_student_cc_exists = 'N' then
            CLOSE student_rules_cursor_nocc_crse;
          else
            CLOSE student_rules_cursor_crse;
          end if;
        end if;
    end case;

  END p_applyrules;

  /* ******************************************************************* */
  /*  Sum the liability amounts by detail code in SFRFAUD and insert the */
  /*  difference (charge or credit), if any, into TBRACCD.               */
  /* ******************************************************************* */
  /*  At this point, the liable hours and charges for each course have   */
  /*  been determined and saved in SFRFAUD. The records in SFRFAUD will  */
  /*  be summed and the summed amount will be compared to what is        */
  /*  currently recorded in the student's accounting information. A      */
  /*  difference will be indicative of a refund or an additional charge. */
  /* ******************************************************************* */
  PROCEDURE p_totalfees(pidm_in               IN spriden.spriden_pidm%TYPE,
                        term_in               IN stvterm.stvterm_code%TYPE,
                        source_pgm_in         IN VARCHAR2,
                        ignore_sfrfmax_ind_in IN VARCHAR2) IS
    max_reg_charge sfrfmax.sfrfmax_max_charge%TYPE;
    min_reg_charge sfrfmax.sfrfmax_min_charge%TYPE;

    aud_liable_amount   sfrfaud.sfrfaud_charge%TYPE := 0;
    aud_crn             sfrfaud.sfrfaud_crn%TYPE := '';
    aud_detl_code       tbbdetc.tbbdetc_detail_code%TYPE := '';
    aud_dcat_code       ttvdcat.ttvdcat_code%TYPE := '';
    aud_adj             tbraccd.tbraccd_amount%TYPE := 0;
    aud_assess_rfnd_cde sfrfaud.sfrfaud_assess_rfnd_penlty_cde%TYPE;
    note                sfrfaud.sfrfaud_note%TYPE;
    next_seqno          sfrfaud.sfrfaud_seqno%TYPE;

    curr_accd_balance   tbraccd.tbraccd_amount%TYPE := 0;
    new_detl_code_total tbraccd.tbraccd_amount%TYPE := 0;
    aud_accd_diff       tbraccd.tbraccd_amount%TYPE := 0;
    accd_tran_number    tbraccd.tbraccd_tran_number%TYPE := 0;

    SKIP_RBT_REFUND BOOLEAN := FALSE;

    /* ******************************************************************* */
    /* Driving cursor. Group and sum audit records based on term settings. */
    /* ******************************************************************* */
    CURSOR sfrfaud_c IS
      SELECT sfrfaud_dcat_code,
             sfrfaud_detl_code,
             NVL(SUM(sfrfaud_charge), 0),
             DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', sfrfaud_crn, NULL)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_accd_tran_number IS NULL -- only want new/unposted
       GROUP BY sfrfaud_dcat_code,
                sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       sfrfaud_crn,
                       NULL)
       ORDER BY sfrfaud_dcat_code,
                sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       sfrfaud_crn,
                       NULL);

    /* ************************************************************************ */
    /* Select SFRFAUD rows by rowid for updating of SFRFAUD_ACCD_TRAN_NUMBER    */
    /* for current assessment. Happens after TBRACCD records have been created. */
    /* ************************************************************************ */
    CURSOR sfrfaud_rowid_c IS
      SELECT ROWID
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = aud_detl_code
         AND NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                        'Y',
                        sfrfaud_crn,
                        NULL),
                 'x') =
             NVL(DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', aud_crn, NULL),
                 'x')
         AND sfrfaud_accd_tran_number IS NULL; -- only want new/unposted

    /* ********************************************************************** */
    /* Sum reg charge for a given detail code currently on student's account. */
    /* ********************************************************************** */
    --- 8.0.1 split the summation up to remove decode, have separate cursors if track by crn is on or off
    CURSOR tbraccd_c IS
      SELECT NVL(SUM(tbraccd_amount), 0)
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = 'R'
         AND tbraccd_detail_code = aud_detl_code;
    CURSOR tbraccd_bycrn_c IS
      SELECT NVL(SUM(tbraccd_amount), 0)
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = 'R'
         AND tbraccd_detail_code = aud_detl_code
         AND nvl(tbraccd_crn, 'x') = nvl(aud_crn, 'x');

    --- do any charges or refunds exists for the min/max detail code in this assessment
    min_detl_cnt pls_integer := 0;
    faud_chrg    sfrfaud.sfrfaud_charge%type;
    CURSOR sfrfaud_minmax_c IS
      SELECT nvl(count(*), 0), NVL(SUM(sfrfaud_charge), 0)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code = aud_detl_code
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid;
  BEGIN

    OPEN sfrfaud_c;
    LOOP
      FETCH sfrfaud_c
        INTO aud_dcat_code, aud_detl_code, aud_liable_amount, aud_crn;
      EXIT WHEN sfrfaud_c%NOTFOUND;

      -- re-init
      aud_adj         := 0;
      aud_accd_diff   := 0;
      note            := '';
      SKIP_RBT_REFUND := FALSE;

      -- determine if refund or charge is needed
      -- 8.0.1 split the cursor in 2 for peformance
      if nvl(sobterm_rec.sobterm_bycrn_ind, 'N') = 'N' then
        OPEN tbraccd_c;
        FETCH tbraccd_c
          INTO curr_accd_balance;
        CLOSE tbraccd_c;
      else
        OPEN tbraccd_bycrn_c;
        FETCH tbraccd_bycrn_c
          INTO curr_accd_balance;
        CLOSE tbraccd_bycrn_c;
      end if;

      /* ********************************************************* */
      /* Remove check for curr_accd_balance <> aud_liable_amount   */
      /* so as to allow the enforcing of SFRFMAX rules for all     */
      /* determined liable amounts as compared to accounting.      */
      /* ********************************************************* */
      -- 91541 IF curr_accd_balance <> aud_liable_amount THEN

      aud_accd_diff := (aud_liable_amount - curr_accd_balance);
      --- 8.0.1 1-1C8L1E  audit diff may be 0 for complete refund
      IF aud_accd_diff <= 0 THEN
        aud_assess_rfnd_cde := 'R'; -- refund
      ELSE
        aud_assess_rfnd_cde := 'A'; -- assessment
      END IF;
      p_print_dbms('in p_totalfees, aud liabl: ' || aud_liable_amount ||
                   ' curr balance: ' || curr_accd_balance || ' rfnd: ' ||
                   aud_assess_rfnd_cde || ' aud accd diff: ' ||
                   aud_accd_diff);

      /* ********************************************************* */
      /* Do not handle RBT refunds for TUI and FEE here. They are  */
      /* are specifically handled in p_calc_rbt_refunds.           */
      /* ********************************************************* */
      -- 8.3 change drops to any_drops
      IF (aud_accd_diff < 0 AND (ANY_DROPS) AND
         NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
         NOT (RFST_RULE_DEFINED) AND aud_dcat_code IN ('TUI', 'FEE')) THEN
        SKIP_RBT_REFUND := TRUE;
      ELSE
        /* ********************************************************* */
        /* Check if new detail code total within SFRFMAX range.      */
        /* ********************************************************* */
        /* Make adjustments to the total amount for the detail code, */
        /* if needed, since only one TBRACCD record will post for    */
        /* the transaction.                                          */
        /*                                                           */
        /* Form SFAREGF supports ignoring the SFRFMAX min/max when   */
        /* doing a mock assessment inside of the form. All other     */
        /* places performing assessment will always consult SFRFMAX. */
        /* ********************************************************* */
        IF ignore_sfrfmax_ind_in = 'N' THEN
          IF (f_sfrfmax_defined(term_in,
                                aud_detl_code,
                                min_reg_charge,
                                max_reg_charge)) THEN
            new_detl_code_total := curr_accd_balance + aud_accd_diff;
            p_print_dbms('adjusted after minmax: ' || new_detl_code_total ||
                         ' curr accd + aud accd diff: ' ||
                         curr_accd_balance || ' + ' || aud_accd_diff ||
                         ' detl: ' || aud_detl_code);
            --- 8.1  find out if all rules behind the charges were removed, i.e. there are no
            --- sfrfaud records for the detail code
            min_detl_cnt := 0;
            open sfrfaud_minmax_c;
            fetch sfrfaud_minmax_c
              into min_detl_cnt, faud_chrg;
            close sfrfaud_minmax_c;
            p_print_dbms('cnt of audit for this assessment: ' ||
                         min_detl_cnt || ' ' || faud_chrg);
            if (DROPS) then
              p_print_dbms('drops occurred, new liab: ' || faud_chrg);
            else
              p_print_dbms('drops did not occurred, new liab: ' ||
                           faud_chrg);
            end if;

            if (NOT (min_detl_cnt = 1 and faud_chrg = 0)) and
               (NOT ((DROPS) and faud_chrg = 0)) then

              IF new_detl_code_total < min_reg_charge THEN
                /* ************************************************* */
                /* Determine diff between new total for the detail   */
                /* code and the SFRFMAX_MIN. Save adjustment amount  */
                /* for back-posting to the audit trail, SFRFAUD.     */
                /*                                                   */
                /* Add adjustment amount to difference between audit */
                /* and accounting balance so as to meet SFRFMAX_MIN. */
                /* The accounting detail will sum up to the minimum. */
                /* ************************************************* */
                aud_adj       := (min_reg_charge - new_detl_code_total);
                note          := 'Adjustment: ' || new_detl_code_total ||
                                 ' < SFRFMAX_MIN of ' || min_reg_charge;
                aud_accd_diff := aud_accd_diff + aud_adj;
              ELSIF new_detl_code_total > max_reg_charge THEN

                /* ************************************************* */
                /* Determine diff between new total for the detail   */
                /* code and the SFRFMAX_MAX. Save adjustment amount  */
                /* for back-posting to the audit trail, SFRFAUD.     */
                /*                                                   */
                /* Add adjustment amount to difference between audit */
                /* and accounting balance so as to meet SFRFMAX_MIN. */
                /* The accounting detail will sum up to the maximum. */
                /* ************************************************* */
                aud_adj       := (max_reg_charge - new_detl_code_total);
                note          := 'Adjustment: ' || new_detl_code_total ||
                                 ' > SFRFMAX_MAX of ' || max_reg_charge;
                aud_accd_diff := aud_accd_diff + aud_adj;
              END IF;
            end if; ---  rules were not dropped and person assessed for them

          END IF;
        END IF;
      END IF;

      /* ********************************************************* */
      /* Do not handle RBT refunds for TUI and FEE here. They are  */
      /* are specifically handled in p_calc_rbt_refunds.           */
      /* ********************************************************* */
      IF NOT (SKIP_RBT_REFUND) THEN
        -- post the assessment difference to acounting
        accd_tran_number := 0;
        p_print_dbms('insert tbraccd1: ' || aud_detl_code || ':' ||
                     aud_accd_diff);
        p_insert_tbraccd_rec(pidm_in,
                             accd_tran_number,
                             term_in,
                             aud_detl_code,
                             aud_accd_diff,
                             aud_accd_diff,
                             assess_eff_date,
                             'R',
                             aud_crn,
                             assess_eff_date,
                             orig_chg_ind);
        p_print_dbms('after insert tbraccd1: ' || accd_tran_number);

        -- back post accd_tran_number to qualified SFRFAUD records by rowid
        IF (ASSESSMENT_POSTED) AND accd_tran_number <> 0 THEN
          FOR sfrfaud_rowid_rec IN sfrfaud_rowid_c LOOP
            UPDATE sfrfaud
               SET sfrfaud_accd_tran_number = accd_tran_number
             WHERE rowid = sfrfaud_rowid_rec.rowid;
          END LOOP;
        END IF;

        IF aud_adj <> 0 THEN
          -- determine next seqno for audit record and insert it
          next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                       term_in,
                                                       save_sessionid);

          --Defect 105101, for some reason TRANSLATE() is failing
          IF (accd_tran_number = 0) THEN
            accd_tran_number := NULL;
          END IF;
          p_print_dbms('p_insert_sfrfaud 8');

          p_insert_sfrfaud(pidm_in,
                           term_in,
                           next_seqno,
                           aud_assess_rfnd_cde, -- 'A'ssessment or 'R'efund
                           aud_dcat_code,
                           aud_detl_code,
                           aud_adj,
                           'N', -- assess by course
                           source_pgm_in,
                           NULL, -- no rule seqno
                           NULL, -- no rule type
                           aud_crn, -- save the CRN if tracking by CRN
                           NULL, -- no rsts_code
                           NULL, -- no rsts_datee
                           NULL, -- no ests_code
                           NULL, -- no ests_date
                           NULL, -- no refund table
                           NULL, -- no reg hours
                           NULL, -- no waiv hours
                           NULL, -- no rule cred hrs
                           NULL, -- no rule stud hrs
                           NULL, -- no per cred charge
                           NULL, -- no flat fee amt
                           NULL, -- no overload hrs
                           NULL, -- no crse overload charge
                           --TRANSLATE(accd_tran_number,0,NULL), Defect 105101
                           accd_tran_number, --Defect 105101
                           note,
                           NULL, -- no liable credit hours
                           -- RPE 35999: following added to store rule calc values
                           NULL, -- rule per credit charge
                           NULL, -- rule flat fee amount
                           NULL, -- rule from flat hrs
                           NULL, -- rule to flat hrs
                           NULL); -- rule course OL start hr

        END IF;
      END IF;
      --END IF;   91541

    END LOOP;
    CLOSE sfrfaud_c;

  END p_totalfees;

  /* ******************************************************************* */
  /* Defect 88291.                                          07/22/03 BAG */
  /* Separate refund by total refund/penalty processing from p_totalfees.*/
  /* ******************************************************************* */
  /* Process the refund by total refunds and penalties for the TUI and   */
  /* FEE DCAT codes when refund by total enabled for the term.           */
  /* ******************************************************************* */
  PROCEDURE p_calc_rbt_refunds(pidm_in               IN spriden.spriden_pidm%TYPE,
                               term_in               IN stvterm.stvterm_code%TYPE,
                               source_pgm_in         IN VARCHAR2,
                               ignore_sfrfmax_ind_in IN VARCHAR2) IS
    max_reg_charge       sfrfmax.sfrfmax_max_charge%TYPE;
    min_reg_charge       sfrfmax.sfrfmax_min_charge%TYPE;
    assessment_detl_code tbbdetc.tbbdetc_detail_code%TYPE;

    aud_crn         sfrfaud.sfrfaud_crn%TYPE;
    aud_detl_code   tbbdetc.tbbdetc_detail_code%TYPE;
    aud_dcat_code   ttvdcat.ttvdcat_code%TYPE;
    aud_detl_type   tbbdetc.tbbdetc_type_ind%TYPE;
    aud_adj         tbraccd.tbraccd_amount%TYPE;
    curr_aud_amount sfrfaud.sfrfaud_charge%TYPE := 0;
    prev_aud_amount sfrfaud.sfrfaud_charge%TYPE := 0;
    note            sfrfaud.sfrfaud_note%TYPE;
    next_seqno      sfrfaud.sfrfaud_seqno%TYPE;

    curr_accd_balance       tbraccd.tbraccd_amount%TYPE;
    prev_assess_amount      sfrfaud.sfrfaud_charge%TYPE;
    new_detl_code_total     tbraccd.tbraccd_amount%TYPE := 0;
    assess_diff             tbraccd.tbraccd_amount%TYPE := 0;
    refund_amount           tbraccd.tbraccd_amount%TYPE := 0;
    charge_amount           tbraccd.tbraccd_amount%TYPE := 0;
    tuit_penalty_amount     tbraccd.tbraccd_amount%TYPE := 0;
    fees_penalty_amount     tbraccd.tbraccd_amount%TYPE := 0;
    tot_tuit_penalty_amount tbraccd.tbraccd_amount%TYPE := 0;
    tot_fees_penalty_amount tbraccd.tbraccd_amount%TYPE := 0;
    curr_refund_amount      tbraccd.tbraccd_amount%TYPE := 0;

    accd_tran_number tbraccd.tbraccd_tran_number%TYPE;
    penalty_percent  NUMBER(3); -- for note, whole number for %
    prorate_percent  NUMBER(3); -- for note, whole number for %

    curr_faud_tuit_total tbraccd.tbraccd_amount%TYPE := 0; -- current liability in SFRFAUD for TUI
    curr_faud_fees_total tbraccd.tbraccd_amount%TYPE := 0; -- current liability in SFRFAUD for FEE
    prev_faud_tuit_total tbraccd.tbraccd_amount%TYPE := 0; -- prev liability in SFRFAUD for TUI
    prev_faud_fees_total tbraccd.tbraccd_amount%TYPE := 0; -- prev liability in SFRFAUD for FEE
    tot_fees_diff        tbraccd.tbraccd_amount%TYPE := 0; -- overall diff for DCAT FEE
    tot_tuit_diff        tbraccd.tbraccd_amount%TYPE := 0; -- overall diff for DCAT TUI
    tot_fees_refund      tbraccd.tbraccd_amount%TYPE := 0; -- total refund for DCAT FEE
    tot_tuit_refund      tbraccd.tbraccd_amount%TYPE := 0; -- total refund for DCAT TUI
    tuit_prorate_percent NUMBER(10, 7) := 0.0;
    fees_prorate_percent NUMBER(10, 7) := 0.0;
    lv_dcat_code         ttvdcat.ttvdcat_code%TYPE;

    /* Sum liability for TUI and FEE detail codes in current assessment. */
    CURSOR curr_sfrfaud_c IS
      SELECT tbbdetc_dcat_code,
             sfrfaud_detl_code,
             NVL(SUM(sfrfaud_charge), 0),
             DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', sfrfaud_crn, NULL),
             tbbdetc_type_ind
        FROM tbbdetc, sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_accd_tran_number IS NULL -- only want new/unposted
         AND tbbdetc_detail_code = sfrfaud_detl_code
         AND tbbdetc_dcat_code IN ('TUI', 'FEE')
       GROUP BY tbbdetc_dcat_code,
                sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       sfrfaud_crn,
                       NULL),
                tbbdetc_refundable_ind,
                tbbdetc_type_ind
       ORDER BY tbbdetc_dcat_code,
                sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       sfrfaud_crn,
                       NULL),
                tbbdetc_type_ind;

    /* Sum liability for specific detail code in previous assessment. */
    /* Used to determine refund and penalty, if appropriate.          */
    CURSOR prev_sfrfaud_c IS
      SELECT NVL(SUM(sfrfaud_charge), 0)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties,swap
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = aud_detl_code
         AND NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                        'Y',
                        sfrfaud_crn,
                        NULL),
                 'x') =
             NVL(DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', aud_crn, NULL),
                 'x');

    CURSOR curr_sfrfaud_dcat_c IS
      SELECT NVL(SUM(sfrfaud_charge), 0)
        FROM tbbdetc, sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = tbbdetc_detail_code
         AND tbbdetc_dcat_code = lv_dcat_code;

    CURSOR prev_sfrfaud_dcat_c IS
      SELECT NVL(SUM(sfrfaud_charge), 0)
        FROM tbbdetc, sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = tbbdetc_detail_code
         AND tbbdetc_dcat_code = lv_dcat_code;

    /* Sum total charge for a given detail code currently on student's account */
    CURSOR tbraccd_detl_c IS
      SELECT SUM(NVL(tbraccd_amount, 0))
        FROM tbbdetc, tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = 'R'
         AND tbraccd_detail_code = aud_detl_code
         AND tbraccd_detail_code = tbbdetc_detail_code -- Defect 105101/105848
         AND tbbdetc_dcat_code = aud_dcat_code -- Defect 105101/105848
         AND NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                        'Y',
                        tbraccd_crn,
                        NULL),
                 'x') =
             NVL(DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', aud_crn, NULL),
                 'x');

    /* Select SFRFAUD rows by rowid for updating of SFRFAUD_ACCD_TRAN_NUMBER    */
    /* for current assessment. Happens after TBRACCD records have been created. */
    CURSOR sfrfaud_rowid_c IS
      SELECT ROWID
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = aud_detl_code
         AND NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                        'Y',
                        sfrfaud_crn,
                        NULL),
                 'x') =
             NVL(DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', aud_crn, NULL),
                 'x')
         AND sfrfaud_accd_tran_number IS NULL; -- only want new/unposted
  BEGIN

    p_print_dbms('Rbt tuit detail code: ' || rbt_clr_tuit_detl_code ||
                 ' save act date: ' ||
                 to_char(save_activity_date, 'DD-MON-YYYY HH24:MI:SS') ||
                 ' sessions; ' || save_sessionid);

    if (ANY_DROPS) then
      p_print_dbms('In p_calc_rbt_refunds, any drops occurred');
    else
      p_print_dbms('In p_calc_rbt_refunds, any drops did not occurr');
    end if;
    if (DROPS) then
      p_print_dbms('In p_calc_rbt_refunds, drops occurred');
    else
      p_print_dbms('In p_calc_rbt_refunds, drops did not occurr');
    end if;
    IF rbt_clr_tuit_detl_code IS NULL THEN
      lv_dcat_code := 'TUI';

      -- get the total for TUI in the current assessment
      OPEN curr_sfrfaud_dcat_c;
      FETCH curr_sfrfaud_dcat_c
        INTO curr_faud_tuit_total;
      CLOSE curr_sfrfaud_dcat_c;

      -- get the total for TUI in the current assessment
      OPEN prev_sfrfaud_dcat_c;
      FETCH prev_sfrfaud_dcat_c
        INTO prev_faud_tuit_total;
      CLOSE prev_sfrfaud_dcat_c;

      -- determine the net refund for TUI
      tot_tuit_diff := curr_faud_tuit_total - prev_faud_tuit_total;

      -- determine the net penalty for TUI
      IF tot_tuit_diff < 0 THEN
        tot_tuit_penalty_amount := (ROUND(((tot_tuit_diff * -1) *
                                          rbt_tuit_penalty_percent),
                                          2));
      END IF;
      p_print_dbms('Tot tuit diff: ' || tot_tuit_diff || ' penality: ' ||
                   tot_tuit_penalty_amount);
    END IF;

    IF rbt_clr_fees_detl_code IS NULL THEN
      lv_dcat_code := 'FEE';

      -- get the total for FEE in the current assessment
      OPEN curr_sfrfaud_dcat_c;
      FETCH curr_sfrfaud_dcat_c
        INTO curr_faud_fees_total;
      CLOSE curr_sfrfaud_dcat_c;

      -- get the total for FEE in the current assessment
      OPEN prev_sfrfaud_dcat_c;
      FETCH prev_sfrfaud_dcat_c
        INTO prev_faud_fees_total;
      CLOSE prev_sfrfaud_dcat_c;

      -- determine the net refund for FEE
      tot_fees_diff := curr_faud_fees_total - prev_faud_fees_total;

      -- determine the net penalty for FEE
      IF tot_fees_diff < 0 THEN
        tot_fees_penalty_amount := (ROUND(((tot_fees_diff * -1) *
                                          rbt_fees_penalty_percent),
                                          2));
      END IF;
    END IF;

    /* ***************************************************************** */
    /* For refund by total, the refund is the difference in charges to   */
    /* a detail code between this assessment and the previous assessment.*/
    /* Refunds always post to the detail code of the original charge.    */
    /*                                                                   */
    /* Refund rules defined in screen SFARFND actually define penalties. */
    /* There are no refund percentages for refund by total. When a drop  */
    /* occurs, the student is liable for 100% of zero hours for the CRN. */
    /* Use the SFRRFND data saved in global variables when determining   */
    /* penalty amounts. Multiply the penalty amount by -1 so it posts as */
    /* as charge on the account.                                         */
    /*                                                                   */
    /* If clearing accounts are NOT in effect for penalties, apply the   */
    /* penalty on the refund amount for the detail code being processed. */
    /*                                                                   */
    /* If clearing accounts are in effect for penalties, determine the   */
    /* net amount for the DCAT code after posting refunds.               */
    /* ***************************************************************** */
    p_print_dbms('aud detail code: ' || aud_detl_code);
    OPEN curr_sfrfaud_c;
    LOOP
      p_print_dbms('inside curr sfrfaud 1 ');
      FETCH curr_sfrfaud_c
        INTO aud_dcat_code,
             aud_detl_code,
             curr_aud_amount,
             aud_crn,
             aud_detl_type;
      EXIT WHEN curr_sfrfaud_c%NOTFOUND;
      p_print_dbms('inside curr sfrfaud ');
      -- re-init
      aud_adj       := 0;
      charge_amount := 0;
      refund_amount := 0;
      note          := '';

      OPEN tbraccd_detl_c;
      FETCH tbraccd_detl_c
        INTO curr_accd_balance;
      CLOSE tbraccd_detl_c;

      OPEN prev_sfrfaud_c;
      FETCH prev_sfrfaud_c
        INTO prev_aud_amount;
      CLOSE prev_sfrfaud_c;

      assess_diff := (curr_aud_amount - prev_aud_amount);
      p_print_dbms('Assess diff: ' || assess_diff);
      IF assess_diff < 0 THEN
        -- refund due
        IF (RBT_RFND_DATE_SPECIFIED) THEN
          -- the RBT refund is the difference between assessments
          refund_amount := assess_diff;

          /* ********************************************************* */
          /* Go on to check if new total for detail code is within     */
          /* SFRMAX min/max range.                                     */
          /*                                                           */
          /* Make adjustments to the refund amount, if needed, since   */
          /* only one TBRACCD record will post for the refund.         */
          /*                                                           */
          /* Form SFAREGF supports ignoring the SFRFMAX min/max when   */
          /* doing a mock assessment inside of the form. All other     */
          /* places performing assessment will always consult SFRFMAX. */
          /* ********************************************************* */
          IF ignore_sfrfmax_ind_in = 'N' THEN
            IF (f_sfrfmax_defined(term_in,
                                  aud_detl_code,
                                  min_reg_charge,
                                  max_reg_charge)) THEN

              new_detl_code_total := curr_accd_balance + refund_amount;

              IF new_detl_code_total < min_reg_charge THEN
                /* ************************************************* */
                /* Determine diff between new total for the detail   */
                /* code and the SFRFMAX_MIN. Save adjustment amount  */
                /* for back-posting to the audit trail, SFRFAUD.     */
                /*                                                   */
                /* Add adjustment amount to refund amount so as to   */
                /* lessen the refund so it meets the SFRFMAX_MIN.    */
                /* The accounting detail will sum up to the minimum. */
                /* ************************************************* */
                aud_adj       := (min_reg_charge - new_detl_code_total);
                note          := g$_nls.get('SFKFEE1-0021',
                                            'SQL',
                                            'Refund adj: orig refund %01%  caused total to fall below SFRFMAX_MIN',
                                            refund_amount);
                refund_amount := refund_amount + aud_adj;
              ELSIF new_detl_code_total > max_reg_charge THEN
                /* ************************************************* */
                /* Determine diff between new total for the detail   */
                /* code and the SFRFMAX_MAX. Save adjustment amount  */
                /* for back-posting to the audit trail, SFRFAUD.     */
                /*                                                   */
                /* Add adjustment amount to refund amount so as to   */
                /* greaten the refund so it meets the SFRFMAX_MAX.   */
                /* The accounting detail will sum up to the maximum. */
                /* ************************************************* */
                aud_adj       := (max_reg_charge - new_detl_code_total);
                note          := g$_nls.get('SFKFEE1-0022',
                                            'SQL',
                                            'Refund adj: orig refund %01% caused total to be above SFRFMAX_MAX',
                                            refund_amount);
                refund_amount := refund_amount + aud_adj;
              END IF;
            END IF;
          END IF;

          /* Keep running total for penalty step later. */
          IF aud_dcat_code = 'TUI' THEN
            tot_tuit_refund := tot_tuit_refund + (refund_amount * -1);
          ELSE
            tot_fees_refund := tot_fees_refund + (refund_amount * -1);
          END IF;

          /* **************************************************** */
          /* For RBT, use the refund by total refund date for the */
          /* effective date of the refund in TBRACCD.             */
          /* **************************************************** */

          accd_tran_number := 0;
          p_print_dbms('insert tbraccd2: ' || aud_detl_code || ':' ||
                       refund_amount || ' orig chrg: ' || orig_chg_ind);

          p_insert_tbraccd_rec(pidm_in,
                               accd_tran_number,
                               term_in,
                               aud_detl_code,
                               refund_amount,
                               refund_amount,
                               rbt_rfnd_eff_date,
                               'R',
                               aud_crn,
                               assess_eff_date,
                               orig_chg_ind);

          /* ******************************************************* */
          /* Post audit adjustment. The audit must equal accounting. */
          /* ******************************************************* */
          IF aud_adj <> 0 THEN
            next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                         term_in,
                                                         save_sessionid);

            --Defect 105101, for some reason TRANSLATE() is failing
            IF (accd_tran_number = 0) THEN
              accd_tran_number := NULL;
            END IF;

            p_print_dbms('p_insert_sfrfaud 9');
            p_insert_sfrfaud(pidm_in,
                             term_in,
                             next_seqno,
                             'R', -- refund adjustment
                             aud_dcat_code,
                             aud_detl_code,
                             aud_adj,
                             'N', -- ABC: default to 'N'
                             source_pgm_in,
                             NULL, -- no rule seqno for adjustment
                             NULL, -- no rule type for adjustment
                             aud_crn, -- save CRN if tracking by CRN
                             NULL, -- no rsts_code for adjustment
                             NULL, -- no rsts_date for adjustment
                             NULL, -- no ests_code for adjustment
                             NULL, -- no ests_date for adjustment
                             NULL, -- no refund table
                             NULL, -- no hours for adjustment
                             NULL, -- no waiv hours for adjustment
                             NULL, -- no rule cred hrs
                             NULL, -- no rule stud hrs
                             NULL, -- no per cred charge
                             NULL, -- no flat fee amt
                             NULL, -- no overload hrs
                             NULL, -- no crse overload charge
                             --TRANSLATE(accd_tran_number,0,NULL), Defect 105101
                             accd_tran_number,
                             note,
                             NULL, -- no liable credit hours
                             -- RPE 35999: following added to store rule calc values
                             NULL, -- rule per credit charge
                             NULL, -- rule flat fee amount
                             NULL, -- rule from flat hrs
                             NULL, -- rule to flat hrs
                             NULL); -- rule course OL start hr

          END IF; -- 88907 RBT_RULE_DEFINED
        END IF; -- RBT refund date specified
      END IF; -- refund due
    END LOOP;
    CLOSE curr_sfrfaud_c;

    -- end of refund processing

    -- determine penalties; need to reprocess detail codes
    p_print_dbms('second round of curr sfrfaud c');
    OPEN curr_sfrfaud_c;
    LOOP
      FETCH curr_sfrfaud_c
        INTO aud_dcat_code,
             aud_detl_code,
             curr_aud_amount,
             aud_crn,
             aud_detl_type;
      p_print_dbms('inside second round of curr sfrfaud c before exit');
      EXIT WHEN curr_sfrfaud_c%NOTFOUND;
      p_print_dbms('inside second round of curr sfrfaud c');
      OPEN prev_sfrfaud_c;
      FETCH prev_sfrfaud_c
        INTO prev_aud_amount;
      CLOSE prev_sfrfaud_c;

      curr_refund_amount := (curr_aud_amount - prev_aud_amount);

      IF curr_refund_amount < 0 THEN
        /* ************************************************************** */
        /* Apply the penalty to the refund amount, if penalty rule found. */
        /*                                                                */
        /* Perform penalty processing for non-clearing accounts only if   */
        /* hours swapping not enabled for term. Need to handle separately */
        /* if swapping is enabled.                                        */
        /* ************************************************************** */
        IF (RBT_RULE_DEFINED) AND NOT (SWAP) THEN
          IF aud_dcat_code = 'TUI' THEN
            IF (rbt_clr_tuit_detl_code IS NULL AND
               tot_tuit_penalty_amount > 0) THEN
              -- prorate detail code penalty based on refund amount
              -- calc is curr refund amount/total refund for DCAT = % of refund to penalize
              tuit_prorate_percent := (curr_refund_amount * -1) /
                                      tot_tuit_refund;
              tuit_penalty_amount  := tot_tuit_penalty_amount *
                                      tuit_prorate_percent;

              note := substr(g$_nls.get('SFKFEE1-0023',
                                        'SQL',
                                        '%01% overall %02% difference,  %03% %  penalty =  %04%, prorated at %05%%',
                                        tot_tuit_diff,
                                        aud_dcat_code,
                                        (rbt_tuit_penalty_percent * 100),
                                        tot_tuit_penalty_amount,
                                        (tuit_prorate_percent * 100)),
                             1,
                             80);
              -- don't accrue total penalty when clearing acct in use;
              -- only post penalty if going to orig detl code
              IF tuit_penalty_amount > 0 THEN
                accd_tran_number := 0;
                --- 8.0.1  CMS-DFCT105899 orig charge not being put on penalties
                p_print_dbms('insert tbraccd3: ' || aud_detl_code || ':' ||
                             tuit_penalty_amount || ' org charg: ' ||
                             orig_chg_ind);
                p_insert_tbraccd_rec(pidm_in,
                                     accd_tran_number,
                                     term_in,
                                     aud_detl_code,
                                     tuit_penalty_amount,
                                     tuit_penalty_amount,
                                     rbt_rfnd_eff_date,
                                     rbt_srce_code,
                                     aud_crn,
                                     assess_eff_date,
                                     orig_chg_ind);

                -- record the penalty in the audit trail
                next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                             term_in,
                                                             save_sessionid);
                --Defect 105101, for some reason TRANSLATE() is failing
                IF (accd_tran_number = 0) THEN
                  accd_tran_number := NULL;
                END IF;
                p_print_dbms('p_insert_sfrfaud 10');

                p_insert_sfrfaud(pidm_in,
                                 term_in,
                                 next_seqno,
                                 'P', -- penalty
                                 aud_dcat_code,
                                 aud_detl_code,
                                 tuit_penalty_amount,
                                 'N', -- ABC: default to 'N'
                                 source_pgm_in,
                                 NULL, -- N/A: no rule seqno
                                 NULL, -- N/A: no rule type
                                 aud_crn,
                                 NULL, -- N/A: no rsts_code
                                 NULL, -- N/A: no rsts_date
                                 NULL, -- N/A: no ests_code
                                 NULL, -- N/A: no ests_date
                                 crn_rfnd_table,
                                 NULL, -- N/A: no hours
                                 NULL, -- N/A: no waiv hours
                                 NULL, -- N/A: no rule cred hrs
                                 NULL, -- N/A: no rule stud hrs
                                 NULL, -- N/A: no per cred charge
                                 NULL, -- N/A: no flat fee amt
                                 NULL, -- N/A: no overload hrs
                                 NULL, -- N/A: no crse overload charge
                                 --TRANSLATE(accd_tran_number,0,NULL), Defect 105101
                                 accd_tran_number,
                                 note,
                                 NULL, -- no liable credit hours
                                 -- RPE 35999: following added to store rule calc values
                                 NULL, -- rule per credit charge
                                 NULL, -- rule flat fee amount
                                 NULL, -- rule from flat hrs
                                 NULL, -- rule to flat hrs
                                 NULL); -- rule course OL start hr
                tuit_penalty_amount := 0; -- re-init
              END IF;
            END IF;
          ELSIF aud_dcat_code = 'FEE' THEN
            IF (rbt_clr_fees_detl_code IS NULL AND
               tot_fees_penalty_amount > 0) THEN
              -- prorate detail code penalty based on refund amount
              -- calc is curr refund amount/total refund for DCAT = % of refund to penalize
              fees_prorate_percent := (curr_refund_amount * -1) /
                                      tot_fees_refund;
              fees_penalty_amount  := tot_fees_penalty_amount *
                                      fees_prorate_percent;

              note := g$_nls.get('SFKFEE1-0024',
                                 'SQL',
                                 '%01%  overall %02% difference,  %03% %  penalty = %04%, prorated at %05% %',
                                 tot_fees_diff,
                                 aud_dcat_code,
                                 (rbt_fees_penalty_percent * 100),
                                 tot_fees_penalty_amount,
                                 (fees_prorate_percent * 100));
              --   note := tot_fees_diff||' overall '||aud_dcat_code||' difference, '||
              --           (rbt_fees_penalty_percent * 100)||'%  penalty = '||tot_fees_penalty_amount||
              --           ', prorated at '||(fees_prorate_percent * 100)||'%';

              -- don't accrue total penalty when clearing acct in use;
              -- only post penalty if going to orig detl code
              IF fees_penalty_amount > 0 THEN
                accd_tran_number := 0;
                --- 8.0.1  CMS-DFCT105899 orig charge not being put on penalties
                p_print_dbms('insert tbraccd4: ' || aud_detl_code || ':' ||
                             fees_penalty_amount);
                p_insert_tbraccd_rec(pidm_in,
                                     accd_tran_number,
                                     term_in,
                                     aud_detl_code,
                                     fees_penalty_amount,
                                     fees_penalty_amount,
                                     rbt_rfnd_eff_date,
                                     rbt_srce_code,
                                     aud_crn,
                                     assess_eff_date,
                                     orig_chg_ind);

                -- record the penalty in the audit trail
                -- record the penalty in the audit trail
                next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                             term_in,
                                                             save_sessionid);
                --Defect 105101, for some reason TRANSLATE() is failing
                IF (accd_tran_number = 0) THEN
                  accd_tran_number := NULL;
                END IF;
                p_print_dbms('p_insert_sfrfaud 11');

                p_insert_sfrfaud(pidm_in,
                                 term_in,
                                 next_seqno,
                                 'P', -- penalty
                                 aud_dcat_code,
                                 aud_detl_code,
                                 fees_penalty_amount,
                                 'N', -- ABC: default to 'N'
                                 source_pgm_in,
                                 NULL, -- N/A: no rule seqno
                                 NULL, -- N/A: no rule type
                                 aud_crn,
                                 NULL, -- N/A: no rsts_code
                                 NULL, -- N/A: no rsts_date
                                 NULL, -- N/A: no ests_code
                                 NULL, -- N/A: no ests_date
                                 crn_rfnd_table,
                                 NULL, -- N/A: no hours
                                 NULL, -- N/A: no waiv hours
                                 NULL, -- N/A: no rule cred hrs
                                 NULL, -- N/A: no rule stud hrs
                                 NULL, -- N/A: no per cred charge
                                 NULL, -- N/A: no flat fee amt
                                 NULL, -- N/A: no overload hrs
                                 NULL, -- N/A: no crse overload charge
                                 --TRANSLATE(accd_tran_number,0,NULL),  -- TBRACCD tran number
                                 accd_tran_number,
                                 note,
                                 NULL, -- no liable credit hours
                                 -- RPE 35999: following added to store rule calc values
                                 NULL, -- rule per credit charge
                                 NULL, -- rule flat fee amount
                                 NULL, -- rule from flat hrs
                                 NULL, -- rule to flat hrs
                                 NULL); -- rule course OL start hr
                fees_penalty_amount := 0; -- re-init
              END IF;
            END IF;
          END IF; -- TUI or FEE DCAT
        END IF; -- 88907 RBT_RULE_DEFINED
      END IF; -- if refund due
    END LOOP;
    CLOSE curr_sfrfaud_c;
  END p_calc_rbt_refunds;

  /* ******************************************************************* */
  /*  Reverse charges from previous assessment that are no longer        */
  /*  applicable for the student by recording $0.00 liability in SFRFAUD.*/
  /* ******************************************************************* */
  PROCEDURE p_reverse_na_charges(pidm_in       IN spriden.spriden_pidm%TYPE,
                                 term_in       IN stvterm.stvterm_code%TYPE,
                                 source_pgm_in IN VARCHAR2) IS
    reversal_dcat_code ttvdcat.ttvdcat_code%TYPE;
    reversal_detl_code tbbdetc.tbbdetc_detail_code%TYPE;
    prev_assess_amount sfrfaud.sfrfaud_charge%TYPE := 0;
    reversal_amount    tbraccd.tbraccd_amount%TYPE := 0;
    reversal_crn       sfrfaud.sfrfaud_crn%TYPE;
    note               sfrfaud.sfrfaud_note%TYPE;
    next_seqno         sfrfaud.sfrfaud_seqno%TYPE;
    --- 7.4.0.2 add date for either prev_assssment_date or sfbetrm_last_assessment
    assmt_date date;

    temp_detl varchar2(4);
    cursor temp_c is
      SELECT x.sfrfaud_detl_code
        FROM sfrfaud x
       WHERE x.sfrfaud_pidm = pidm_in
         AND x.sfrfaud_term_code = term_in
         AND x.sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND x.sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
            --     AND TO_date(x.sfrfaud_activity_date,G$_DATE.GET_NLS_DATE_FORMAT||' HH24:MI:SS') =
            --         TO_date(save_activity_date,G$_DATE.GET_NLS_DATE_FORMAT||' HH24:MI:SS')
         AND x.sfrfaud_activity_date = save_activity_date
         AND x.sfrfaud_sessionid = save_sessionid;

    /* ******************************************************************** */
    /* Select last assessment information similarly to audit data selection */
    /* for use in determining and reversing rules/detail code charges from  */
    /* the prev assessment that are no longer qualified for in the current. */
    /* ******************************************************************** */
    --- 8.0.1 remove detail code, tbbdetc from select as its unnecessary
    CURSOR prev_assess_to_reverse_c IS
      SELECT a.sfrfaud_dcat_code,
             a.sfrfaud_detl_code,
             SUM(a.sfrfaud_charge),
             DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', a.sfrfaud_crn, NULL)
        FROM sfrfaud a
       WHERE a.sfrfaud_pidm = pidm_in
         AND a.sfrfaud_term_code = term_in
         AND a.sfrfaud_detl_code IS NOT NULL
         AND a.sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
            --  7.4.0.2 compare to correct assessment date.  itf this is assessment after
            --  intermediate we want the assessment before the intermediate
            --   AND a.sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND a.sfrfaud_activity_date = assmt_date
         AND NVL(a.sfrfaud_charge, 0) <> 0
         AND NOT EXISTS
       (SELECT x.sfrfaud_detl_code
                FROM sfrfaud x
               WHERE x.sfrfaud_pidm = pidm_in
                 AND x.sfrfaud_term_code = term_in
                 AND x.sfrfaud_detl_code IS NOT NULL -- filter out info records
                 AND x.sfrfaud_detl_code = a.sfrfaud_detl_code
                 AND x.sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
                 AND NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                                'Y',
                                x.sfrfaud_crn,
                                NULL),
                         'x') = NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                                           'Y',
                                           a.sfrfaud_crn,
                                           NULL),
                                    'x')
                    --     AND TO_date(x.sfrfaud_activity_date,G$_DATE.GET_NLS_DATE_FORMAT||' HH24:MI:SS') =
                    --         TO_date(save_activity_date,G$_DATE.GET_NLS_DATE_FORMAT||' HH24:MI:SS')
                 AND x.sfrfaud_activity_date = save_activity_date
                 AND x.sfrfaud_sessionid = save_sessionid)
       GROUP BY a.sfrfaud_dcat_code,
                a.sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       a.sfrfaud_crn,
                       NULL)
       ORDER BY a.sfrfaud_dcat_code,
                a.sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       a.sfrfaud_crn,
                       NULL);
  BEGIN
    --- 7.4.0.2 105831  need to look at the assessment before the intermediate

    if prev_assessment_date is not null and (LAST_ASSESSMENT_HANDLED_DD) then
      assmt_date := prev_assessment_date;
    else
      assmt_date := sfbetrm_rec.sfbetrm_assessment_date;
    end if;

    -- open temp_c;
    -- loop
    --    fetch temp_c into temp_detl;
    --    exit when temp_c%notfound;
    --    p_print_dbms('temp det: ' || temp_detl);
    -- end loop;
    -- close temp_c;

    /* *********************************************************** */
    /* If RBT is in effect, only allow reversals if refund/penalty */
    /* date specified.                                             */
    /* Always allow reversals if RBT not in effect.                */
    /* *********************************************************** */
    p_print_dbms('refund ind: ' || sobterm_rec.sobterm_refund_ind ||
                 ' rev nrf ind: ' ||
                 sobterm_rec.sobterm_assess_rev_nrf_ind ||
                 ' last assmt date: ' ||
                 to_char(sfbetrm_rec.sfbetrm_assessment_date,
                         'DD-MON-YYYY HH24:MI:SS') || ' bycrn: ' ||
                 sobterm_rec.sobterm_bycrn_ind || ' save act date: ' ||
                 to_char(save_activity_date, 'DD-MON-YYYY HH24:MI:SS'));
    p_print_dbms('prv assmnt: ' ||
                 to_char(prev_assessment_date, 'DD-MON-YYYY HH24:MI:SS') ||
                 ' save session: ' || save_sessionid || ' assmt date: ' ||
                 to_char(assmt_date, 'DD-MON-YYYY HH24:MI:SS'));
    if RBT_RFND_DATE_SPECIFIED then
      p_print_dbms('rfnd date: is specified');
    else
      p_print_dbms('rfnd date is not specified');
    end if;

    IF (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'N' OR
       (NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'Y' AND
       RBT_RFND_DATE_SPECIFIED)) THEN
      OPEN prev_assess_to_reverse_c;
      LOOP
        FETCH prev_assess_to_reverse_c
          INTO reversal_dcat_code,
               reversal_detl_code,
               prev_assess_amount,
               reversal_crn;
        p_print_dbms('Record found for prev assess to reverse c: ' ||
                     reversal_crn);
        EXIT WHEN prev_assess_to_reverse_c%NOTFOUND;

        /* ********************************************************** */
        /* Allow option to reverse non-refundable fees (non-TUI/FEE). */
        /* If option to reverse non-TUI/FEE is disabled, continue     */
        /* processing only if DCAT code is TUI or FEE.                */
        /* ********************************************************** */
        IF ((sobterm_rec.sobterm_assess_rev_nrf_ind = 'N' AND
           reversal_dcat_code IN ('TUI', 'FEE')) OR
           sobterm_rec.sobterm_assess_rev_nrf_ind = 'Y') THEN
          -- record 0.00 liability to give 100% credit via refunding
          note := g$_nls.get('SFKFEE1-0025',
                             'SQL',
                             'No longer liable for charges to %01%  as of this assessment.',
                             reversal_detl_code);
          p_print_dbms('In reversal, note: ' || note);
          next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                       term_in,
                                                       save_sessionid);

          p_print_dbms('p_insert_sfrfaud 12');
          p_insert_sfrfaud(pidm_in,
                           term_in,
                           next_seqno,
                           'A', -- assessment
                           reversal_dcat_code,
                           reversal_detl_code,
                           0, -- no longer have liability
                           'N', -- ABC: default to 'N'
                           source_pgm_in,
                           NULL, -- N/A: no rule seqno
                           NULL, -- N/A: no rule type
                           reversal_crn,
                           NULL, -- N/A: no rsts_code
                           NULL, -- N/A: no rsts_date
                           NULL, -- N/A: no ests_code
                           NULL, -- N/A: no ests_date
                           NULL, -- crn_rfnd_table,
                           NULL, -- N/A: no hours
                           NULL, -- N/A: no waiv hours
                           NULL, -- N/A: no rule cred hrs
                           NULL, -- N/A: no rule stud hrs
                           NULL, -- N/A: no per cred chrg
                           NULL, -- N/A: no flat fee amt
                           NULL, -- N/A: no overload hrs
                           NULL, -- N/A: no crse OL chrg
                           NULL, -- TBRACCD tran number
                           note,
                           NULL, -- no liable credit hrs
                           -- RPE 35999: following added to store rule calc values
                           NULL, -- rule per credit charge
                           NULL, -- rule flat fee amount
                           NULL, -- rule from flat hrs
                           NULL, -- rule to flat hrs
                           NULL); -- rule course OL start hr
        END IF;
      END LOOP;
      CLOSE prev_assess_to_reverse_c;
    END IF;

  END p_reverse_na_charges;

  /* ******************************************************************* */
  /* Drop the student's enrollment from the institution.                 */
  /* ******************************************************************* */
  /* Processing occurs when the SFBETRM record is deleted via SFAREGS.   */
  /* This logic comes from the trigger code from SFAREGS.                */
  /* Reverse any existing accounting for the student.                    */
  /* ******************************************************************* */
  PROCEDURE p_process_etrm_drop(pidm_in      IN spriden.spriden_pidm%TYPE,
                                term_in      IN stvterm.stvterm_code%TYPE,
                                drop_date_in IN sfrstcr.sfrstcr_rsts_date%TYPE) IS
    CURSOR get_tbraccd_c IS
      SELECT tbraccd_detail_code,
             tbraccd_srce_code,
             SUM(tbraccd_amount),
             tbraccd_crn
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code IN
             ('R', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
       GROUP BY tbraccd_detail_code, tbraccd_srce_code, tbraccd_crn
       ORDER BY tbraccd_detail_code, tbraccd_srce_code;

    detl_code        tbbdetc.tbbdetc_detail_code%TYPE;
    detl_amount      tbraccd.tbraccd_amount%TYPE;
    accd_srce_code   tbraccd.tbraccd_srce_code%TYPE;
    accd_tran_number tbraccd.tbraccd_tran_number%TYPE;
    accd_crn         tbraccd.tbraccd_crn%TYPE;
    eff_date         DATE;
  BEGIN
    /* ******************************************* */
    /* 07/01/04 BAG: Defect 93995 and 95476.       */
    /* Set create_accd_ind = Y to allow TBRACCD    */
    /* inserts to occur.                           */
    /* ******************************************* */
    create_accd_ind := 'Y';

    /* ****************************************************** */
    /* 103863                                    10/17/05 JC  */
    /* IF sobterm_eff_date < drop_date_in <= SYSDATE          */
    /*   use drop_date_in                                     */
    /* ELSE                                                   */
    /*   use greatest sobterm_eff_date, drop_date_in, SYSDATE */
    /* ****************************************************** */
    IF TRUNC(drop_date_in) <= TRUNC(SYSDATE) AND
       TRUNC(drop_date_in) >
       TRUNC(NVL(sobterm_rec.sobterm_fee_assessmnt_eff_date,
                 drop_date_in - 1)) THEN
      eff_date := NVL(drop_date_in, SYSDATE);
    ELSE
      eff_date := GREATEST(NVL(sobterm_rec.sobterm_fee_assessmnt_eff_date,
                               SYSDATE),
                           NVL(drop_date_in, SYSDATE),
                           SYSDATE);
    END IF;

    save_activity_date := SYSDATE;
    OPEN get_tbraccd_c;
    LOOP
      FETCH get_tbraccd_c
        INTO detl_code, accd_srce_code, detl_amount, accd_crn; -- 94239
      EXIT WHEN get_tbraccd_c%NOTFOUND;

      IF detl_amount <> 0 THEN
        accd_tran_number := 0;
        gb_common.p_set_context('TB_RECEIVABLE',
                                'SYSTEM_TRAN',
                                'TRUE',
                                'N');
        p_print_dbms('insert tbraccd5: ' || detl_code || ':' ||
                     (detl_amount * -1));
        p_insert_tbraccd_rec(pidm_in,
                             accd_tran_number,
                             term_in,
                             detl_code,
                             (detl_amount * -1),
                             (detl_amount * -1),
                             eff_date,
                             accd_srce_code,
                             accd_crn,
                             eff_date,
                             'Y');
      END IF;
    END LOOP;
    CLOSE get_tbraccd_c;

    DELETE FROM sfrefee
     WHERE sfrefee_pidm = pidm_in
       AND sfrefee_term_code = term_in;

    DELETE FROM sfrbtch
     WHERE sfrbtch_pidm = pidm_in
       AND sfrbtch_term_code = term_in;
  END p_process_etrm_drop;

  /* ******************************************************************* */
  /* RPE 35999                                               03/2005 BAG */
  /* ******************************************************************* */
  /* Need:   Show rule values used in charge calcuation in form SFAFAUD. */
  /* Change: Store rule values utilized in charge calc in SFRFAUD table. */
  /* Added new columns: SFRFAUD_RGFE_PER_CRED_CHARGE  NUMBER(12,2)       */
  /*                    SFRFAUD_RGFE_FLAT_FEE_AMOUNT  NUMBER(12,2)       */
  /*                    SFRFAUD_RGFE_FROM_FLAT_HRS    NUMBER(9,3)        */
  /*                    SFRFAUD_RGFE_TO_FLAT_HRS      NUMBER(9,3)        */
  /*                    SFRFAUD_RGFE_CRSE_OL_START_HR NUMBER(9,3)        */
  /* Add new input parms to procedure:                                   */
  /*   rule_per_cred_charge_in    rule_flat_fee_amount_in                */
  /*   rule_from_flat_hrs_in      rule_to_flat_hrs_in                    */
  /*   rule_crse_ol_start_hr_in                                          */
  /* ******************************************************************* */
  /* Insert the SFRFAUD audit record.                                    */
  /* ******************************************************************* */
  PROCEDURE p_insert_sfrfaud(pidm_in                   IN spriden.spriden_pidm%TYPE,
                             term_in                   IN stvterm.stvterm_code%TYPE,
                             sfrfaud_seqno_in          IN sfrfaud.sfrfaud_seqno%TYPE,
                             assess_rfnd_penlty_cde_in IN sfrfaud.sfrfaud_assess_rfnd_penlty_cde%TYPE,
                             dcat_code_in              IN ttvdcat.ttvdcat_code%TYPE,
                             detl_code_in              IN sfrfaud.sfrfaud_detl_code%TYPE,
                             charge_in                 IN sfrfaud.sfrfaud_charge%TYPE,
                             assess_by_crse_ind_in     IN sfrfaud.sfrfaud_assess_by_course_ind%TYPE,
                             source_pgm_in             IN sfrfaud.sfrfaud_assessment_source%TYPE,
                             sfrrgfe_seqno_in          IN sfrfaud.sfrfaud_rgfe_seqno%TYPE,
                             sfrrgfe_type_in           IN sfrfaud.sfrfaud_rgfe_type%TYPE,
                             crn_in                    IN sfrfaud.sfrfaud_crn%TYPE,
                             rsts_code_in              IN stvrsts.stvrsts_code%TYPE,
                             rsts_date_in              IN DATE,
                             ests_code_in              IN stvests.stvests_code%TYPE,
                             ests_date_in              IN DATE,
                             rfnd_table_in             IN sfrfaud.sfrfaud_refund_source_table%TYPE,
                             reg_bill_hr_in            IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                             reg_waiv_hr_in            IN sfrfaud.sfrfaud_reg_bill_hr%TYPE,
                             rule_cred_hr_in           IN sfrfaud.sfrfaud_rule_liable_bill_hrs%TYPE,
                             rule_stud_hr_in           IN sfrfaud.sfrfaud_rule_liable_stud_hrs%TYPE,
                             per_cred_charge_in        IN sfrfaud.sfrfaud_tot_per_cred_charge%TYPE,
                             flat_fee_amt_in           IN sfrfaud.sfrfaud_flat_fee_amount%TYPE,
                             overload_hrs_in           IN sfrfaud.sfrfaud_crse_overload_hrs%TYPE,
                             overload_charge_in        IN sfrfaud.sfrfaud_crse_overload_charge%TYPE,
                             tran_number_in            IN tbraccd.tbraccd_tran_number%TYPE,
                             note_in                   IN sfrfaud.sfrfaud_note%TYPE,
                             liable_cred_hr_in         IN sfrfaud.sfrfaud_liable_cred_hrs%TYPE,
                             rule_per_cred_charge_in   IN sfrfaud.sfrfaud_rgfe_per_cred_charge%TYPE,
                             rule_flat_fee_amount_in   IN sfrfaud.sfrfaud_rgfe_flat_fee_amount%TYPE,
                             rule_from_flat_hrs_in     IN sfrfaud.sfrfaud_rgfe_from_flat_hrs%TYPE,
                             rule_to_flat_hrs_in       IN sfrfaud.sfrfaud_rgfe_to_flat_hrs%TYPE,
                             rule_crse_ol_start_hr_in  IN sfrfaud.sfrfaud_rgfe_crse_ol_start_hr%TYPE,
                             RGFE_RULE_VALUE           IN sfrfaud.SFRFAUD_RGFE_RULE_VALUE%type default null) IS
  BEGIN
    -- ** get next seq no here **

    p_print_dbms('Start Insert Sfrfaud: ' || reg_bill_hr_in || ' : ' ||
                 rule_stud_hr_in || ' note ' || note_in);
    INSERT INTO SFRFAUD
      (SFRFAUD_PIDM,
       SFRFAUD_TERM_CODE,
       SFRFAUD_SESSIONID,
       SFRFAUD_SEQNO,
       SFRFAUD_ASSESS_RFND_PENLTY_CDE,
       SFRFAUD_DCAT_CODE,
       SFRFAUD_DETL_CODE,
       SFRFAUD_CHARGE,
       SFRFAUD_ASSESS_BY_COURSE_IND,
       SFRFAUD_ASSESSMENT_SOURCE,
       SFRFAUD_USER_ID,
       SFRFAUD_ACTIVITY_DATE,
       SFRFAUD_RGFE_SEQNO,
       SFRFAUD_RGFE_TYPE,
       SFRFAUD_CRN,
       SFRFAUD_RSTS_CODE,
       SFRFAUD_RSTS_DATE,
       SFRFAUD_ESTS_CODE,
       SFRFAUD_ESTS_DATE,
       SFRFAUD_REFUND_SOURCE_TABLE,
       SFRFAUD_REG_BILL_HR,
       SFRFAUD_REG_WAIV_HR,
       SFRFAUD_RULE_LIABLE_BILL_HRS,
       SFRFAUD_RULE_LIABLE_STUD_HRS,
       SFRFAUD_TOT_PER_CRED_CHARGE,
       SFRFAUD_FLAT_FEE_AMOUNT,
       SFRFAUD_CRSE_OVERLOAD_HRS,
       SFRFAUD_CRSE_OVERLOAD_CHARGE,
       SFRFAUD_ACCD_TRAN_NUMBER,
       SFRFAUD_NOTE,
       SFRFAUD_LIABLE_CRED_HRS,
       SFRFAUD_RGFE_PER_CRED_CHARGE,
       SFRFAUD_RGFE_FLAT_FEE_AMOUNT,
       SFRFAUD_RGFE_FROM_FLAT_HRS,
       SFRFAUD_RGFE_TO_FLAT_HRS,
       SFRFAUD_RGFE_CRSE_OL_START_HR,
       SFRFAUD_RGFE_RULE_VALUE)
    VALUES
      (pidm_in,
       term_in,
       save_sessionid,
       sfrfaud_seqno_in,
       assess_rfnd_penlty_cde_in,
       dcat_code_in,
       detl_code_in,
       charge_in,
       assess_by_crse_ind_in,
       source_pgm_in,
       USER,
       NVL(intermediate_date, save_activity_date),
       sfrrgfe_seqno_in,
       sfrrgfe_type_in,
       crn_in,
       rsts_code_in,
       rsts_date_in,
       ests_code_in,
       ests_date_in,
       rfnd_table_in,
       reg_bill_hr_in,
       reg_waiv_hr_in,
       rule_cred_hr_in,
       rule_stud_hr_in,
       per_cred_charge_in,
       flat_fee_amt_in,
       overload_hrs_in,
       overload_charge_in,
       tran_number_in,
       note_in,
       liable_cred_hr_in,
       rule_per_cred_charge_in,
       rule_flat_fee_amount_in,
       rule_from_flat_hrs_in,
       rule_to_flat_hrs_in,
       rule_crse_ol_start_hr_in,
       RGFE_RULE_VALUE);

    p_print_dbms('End Insert Sfrfaud ');
  END p_insert_sfrfaud;

  /* ******************************************************************* */
  /* Reinitialize the reusable working variables.                        */
  /* ******************************************************************* */
  PROCEDURE p_reinit_temp_vars IS
  BEGIN
    t_crn                  := '';
    t_levl_code_crse       := '';
    t_camp_code_crse       := '';
    t_ptrm_code            := '';
    t_add_date             := '';
    t_rsts_code            := '';
    t_rsts_date            := '';
    t_orig_chg_ind         := '';
    t_gmod_code            := '';
    t_crse_start_date      := '';
    t_crse_end_date        := '';
    t_reg_bill_hr          := 0;
    t_bill_hr_tuit         := 0;
    t_bill_hr_fees         := 0;
    t_liab_bill_hr_tuit    := 0;
    t_liab_bill_hr_fees    := 0;
    t_reg_waiv_hr          := 0;
    t_waiv_hr_tuit         := 0;
    t_waiv_hr_fees         := 0;
    t_liab_waiv_hr_tuit    := 0;
    t_liab_waiv_hr_fees    := 0;
    t_tuit_liab_percentage := 0;
    t_fees_liab_percentage := 0;

  END p_reinit_temp_vars;

  /* ******************************************************************* */
  /* Initialize the global working variables.                            */
  /* ******************************************************************* */
  PROCEDURE p_init_global_vars IS
  BEGIN
    /* ********************************************************* */
    /* Defect 87360: Reset BOOLEAN indicating that an assessment */
    /* posted. Batch processing was not reinitializing the var   */
    /* when invoking the package since the SGA was in use.       */
    /* ********************************************************* */
    ASSESSMENT_POSTED := FALSE;
    orig_chg_ind      := '';

    /* **************************************************** */
    /* Reset all other global variables/BOOLEANS.           */
    /* **************************************************** */
    rfst_tuit_liab_percentage := 0.00;
    rfst_fees_liab_percentage := 0.00;
    rbt_tuit_penalty_percent  := 0.00;
    rbt_fees_penalty_percent  := 0.00;
    --104070
    rev_penalty_hrs_tui    := 0;
    rev_penalty_hrs_fee    := 0;
    SWAP_RBC_MORE_DROPPED  := FALSE;
    SWAP_RBC_EQUAL         := FALSE;
    SWAP_RBC_MORE_ADDS     := FALSE;
    SWAP                   := FALSE;
    tui_below_flat_pcent   := 0.00;
    tui_below_flat_pcent   := 0.00;
    fee_below_flat_pcent   := 0.00;
    rev_penalty_hrs_tui    := 0.00;
    rev_penalty_hrs_fee    := 0.00;
    rev_dropped_hrs        := 0.00;
    recalc_penalty_hrs_tui := 0.00;
    recalc_penalty_hrs_fee := 0.00;
    rev_tuit               := 0;
    rev_fees               := 0;
    rev_lv_dhrs            := 0;

    PENALTY_REV_DONE       := FALSE;
    N_FAUD_CREATED         := FALSE;
    save_clas_code         := null;
    save_primary_levl_code := null;
    --- new to save nondrop for 7.4.0.1
    new_nondrop_hrs := 0;
    new_nonwaiv_hrs := 0;

    --1-MRADA, to initialize this ptrm variable so that the previous ptrm will
    --not be mistakenly re-used on the next record.
    t_ptrm_rule := '';

    RFST_RULE_DEFINED       := FALSE;
    RFST_RFND_APPLIED       := FALSE;
    RBT_RULE_DEFINED        := FALSE;
    RBT_RFND_DATE_SPECIFIED := FALSE;
    DROPS                   := FALSE;
    ANY_DROPS               := FALSE;
    PREV_FLAT_HR_RULE_MET   := FALSE;
    ESTS_WD_PROCESSED       := FALSE;
    intermediate_date       := NULL;
    last_assessment_date    := NULL;
    SWAP                    := FALSE;
    swap_date               := null;
    start_swap_hours        := 0;
    before_swap_hours       := 0;
    --  new for 8.2.1 to track first swap date
    first_swap_date := null;
    --- 7.4.0.4
    rbt_dd_exists := FALSE;
    rbt_wd_exists := FALSE;
    --- 7.4.0.4
    PENALTY_REV_DONE := FALSE;

    t_stud_liab_bill_hr_tuit := 0;
    t_stud_liab_bill_hr_fees := 0;
    t_stud_liab_waiv_hr_tuit := 0;
    t_stud_liab_waiv_hr_fees := 0;

    save_clas_code         := null;
    save_primary_levl_code := null;
    --  only reset these if the process calling is not batch fee or intermediate
    if upper(source_pgm) not in ('SFRFASC', 'SFKFEES') then
      p_print_dbms('clear globals for checking rules ' || source_pgm);
      save_term                     := null;
      gv_rgfe_student_read          := null;
      gv_rgfe_level_read            := null;
      gv_rgfe_attr_read             := null;
      gv_rgfe_campus_read           := null;
      gv_rgfe_student_exists        := null;
      gv_rgfe_level_exists          := null;
      gv_rgfe_attr_exists           := null;
      gv_rgfe_campus_exists         := null;
      gv_rgfe_student_cc_exists     := null;
      gv_rgfe_level_cc_exists       := null;
      gv_rgfe_attr_cc_exists        := null;
      gv_rgfe_campus_cc_exists      := null;
      gv_rgfe_student_course_exists := null;
      gv_rgfe_level_course_exists   := null;
      gv_rgfe_attr_course_exists    := null;
      gv_rgfe_campus_course_exists  := null;
    else
      p_print_dbms('do not clear globals for checking rules ' ||
                   source_pgm);
    end if;
  END p_init_global_vars;

  /* ******************************************************************* */
  /*  Procedure to insert the accounting record for the assessment and   */
  /*  set a global BOOLEAN indicating that an assessment occurred.       */
  /* ******************************************************************* */
  PROCEDURE p_insert_tbraccd_rec(pidm_in              IN spriden.spriden_pidm%TYPE,
                                 accd_tran_num_in_out IN OUT tbraccd.tbraccd_tran_number%TYPE,
                                 term_in              IN stvterm.stvterm_code%TYPE,
                                 aud_detl_code_in     IN tbraccd.tbraccd_detail_code%TYPE,
                                 amount_in            IN tbraccd.tbraccd_amount%TYPE,
                                 balance_in           IN tbraccd.tbraccd_balance%TYPE,
                                 eff_date_in          IN tbraccd.tbraccd_effective_date%TYPE,
                                 srce_code_in         IN tbraccd.tbraccd_srce_code%TYPE,
                                 crn_in               IN tbraccd.tbraccd_crn%TYPE,
                                 trans_date_in        IN tbraccd.tbraccd_trans_date%TYPE,
                                 orig_chg_ind_in      IN tbraccd.tbraccd_orig_chg_ind%TYPE) IS
    lv_rowid_out VARCHAR2(18); /* 06/23/04 BAG: TBRACCD API. */
  BEGIN
    p_print_dbms('Start Insert TBRACCD');
    IF create_accd_ind = 'Y' THEN
      p_print_dbms('TBRACCD: amount_in: ' || amount_in);
      IF amount_in <> 0 THEN
        /* 05/20/03 BAG: Defect 87325. */
        /* ********************************** */
        /* 6/2004 BAG: Implement TBRACCD API. */
        /* ********************************** */
        BEGIN
          p_print_dbms('TBRACCD :aud_detl_code_in: ' || aud_detl_code_in);
          p_print_dbms('TBRACCD :amount_in: ' || amount_in);
          p_print_dbms('TBRACCD :crn_in: ' || crn_in);
          p_print_dbms('TBRACCD :eff_date_in: ' || eff_date_in);
          p_print_dbms('TBRACCD :accd_tran_num_in_out: ' ||
                       accd_tran_num_in_out);
          tb_receivable.p_create(p_pidm            => pidm_in,
                                 p_term_code       => term_in,
                                 p_detail_code     => aud_detl_code_in,
                                 p_user            => gb_common.f_sct_user,
                                 p_entry_date      => SYSDATE,
                                 p_amount          => amount_in,
                                 p_effective_date  => eff_date_in,
                                 p_srce_code       => srce_code_in,
                                 p_acct_feed_ind   => 'Y',
                                 p_session_number  => 0,
                                 p_crn             => crn_in,
                                 p_trans_date      => trans_date_in,
                                 p_orig_chg_ind    => orig_chg_ind_in,
                                 p_data_origin     => gb_common.data_origin,
                                 p_tran_number_out => accd_tran_num_in_out,
                                 p_rowid_out       => lv_rowid_out);
          p_print_dbms('TBRACCD: after accd tran out: ' ||
                       accd_tran_num_in_out);
          ASSESSMENT_POSTED := TRUE; /* Set that assessment actually occured. */
        EXCEPTION
          WHEN OTHERS THEN
            RAISE;
        END;
      END IF;
    END IF;
    p_print_dbms('END Insert TBRACCD');
  END p_insert_tbraccd_rec;

  /* ******************************************************************* */
  /* Delete SFRFAUD rows created for mock assessment done via SFAREGF.   */
  /* Pass in activity date string as ''||G$_DATE.GET_NLS_DATE_FORMAT||' HH24:MI:SS'.           */
  /* ******************************************************************* */
  PROCEDURE p_delete_mock_assessment(pidm_in         IN spriden.spriden_pidm%TYPE,
                                     term_in         IN stvterm.stvterm_code%TYPE,
                                     sessionid_in    IN VARCHAR2,
                                     act_date_str_in IN VARCHAR2) IS
    indate date := TO_date(act_date_str_in,
                           G$_DATE.GET_NLS_DATE_FORMAT || ' HH24:MI:SS');

  BEGIN

    DELETE FROM sfrfaud
     WHERE sfrfaud_pidm = pidm_in
       AND sfrfaud_term_code = term_in
       AND sfrfaud_sessionid = sessionid_in
       AND sfrfaud_seqno > 0
       AND sfrfaud_activity_date = indate;

    COMMIT;
  END p_delete_mock_assessment;

  /* ******************************************************************* */
  /* Delete SFRBTCH collector record if it exists.                       */
  /* ******************************************************************* */
  PROCEDURE p_delete_sfrbtch(pidm_in IN spriden.spriden_pidm%TYPE,
                             term_in IN stvterm.stvterm_code%TYPE) IS
  BEGIN
    DELETE FROM sfrbtch
     WHERE sfrbtch_pidm = pidm_in
       AND sfrbtch_term_code = term_in;
  END p_delete_sfrbtch;

  /* ******************************************************************* */
  /* Post accrued total refund by total penalties for TUI and FEE to     */
  /* clearing account detail codes, if appropriate.                      */
  /* ******************************************************************* */
  PROCEDURE p_post_rbt_penalties(pidm_in       IN spriden.spriden_pidm%TYPE,
                                 term_in       IN stvterm.stvterm_code%TYPE,
                                 source_pgm_in IN VARCHAR2) IS
    penalty_detl_code tbbdetc.tbbdetc_detail_code%TYPE;
    accd_tran_number  tbraccd.tbraccd_tran_number%TYPE;
    note              sfrfaud.sfrfaud_note%TYPE;
    next_seqno        sfrfaud.sfrfaud_seqno%TYPE;
    penalty_percent   NUMBER(3); -- for note

    prev_aud_tuit       sfrfaud.sfrfaud_charge%TYPE := 0;
    prev_aud_fees       sfrfaud.sfrfaud_charge%TYPE := 0;
    curr_aud_tuit       sfrfaud.sfrfaud_charge%TYPE := 0;
    curr_aud_fees       sfrfaud.sfrfaud_charge%TYPE := 0;
    penalty_dcat_code   ttvdcat.ttvdcat_code%TYPE;
    tuit_total          tbraccd.tbraccd_amount%TYPE := 0;
    fees_total          tbraccd.tbraccd_amount%TYPE := 0;
    tuit_penalty_amount tbraccd.tbraccd_amount%TYPE := 0;
    fees_penalty_amount tbraccd.tbraccd_amount%TYPE := 0;

    /* Sum liability for specific DCAT code in previous assessment. */
    /* Used to determine penalty when posting to clearing account.  */
    CURSOR prev_sfrfaud_dcat_c IS
      SELECT NVL(SUM(sfrfaud_charge), 0)
        FROM tbbdetc, sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = sfbetrm_rec.sfbetrm_assessment_date
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND tbbdetc_detail_code = sfrfaud_detl_code
         AND tbbdetc_dcat_code = penalty_dcat_code;

    /* Sum liability for specific DCAT code in current assessment. */
    /* Used to determine penalty when posting to clearing account.  */
    CURSOR curr_sfrfaud_dcat_c IS
      SELECT NVL(SUM(sfrfaud_charge), 0)
        FROM tbbdetc, sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND tbbdetc_detail_code = sfrfaud_detl_code
         AND tbbdetc_dcat_code = penalty_dcat_code;

    /* ************************************************************************ */
    /* Select SFRFAUD rows by rowid for updating of SFRFAUD_ACCD_TRAN_NUMBER    */
    /* for current assessment. Happens after TBRACCD records have been created. */
    /* ************************************************************************ */
    CURSOR sfrfaud_rowid_c IS
      SELECT ROWID
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- exclude penalties 104667
         AND sfrfaud_detl_code = penalty_detl_code
         AND sfrfaud_accd_tran_number IS NULL; -- only want new/unposted
  BEGIN
    -- After refunds have posted to the detail code, if clearing accounts
    -- are in effect, determine the penalty for the total by DCAT.
    if (ANY_DROPS) then
      p_print_dbms('In p_post_rbt_penalty, any drops occurred');
    else
      p_print_dbms('In p_post_rbt_penalty, any drops did not occurr');
    end if;
    if (DROPS) then
      p_print_dbms('In p_post_rbt_penalty, drops occurred');
    else
      p_print_dbms('In p_post_rbt_penalty, drops did not occurr');
    end if;
    --- 8.3.0.3 include check for drops
    IF rbt_clr_tuit_detl_code IS NOT NULL AND (ANY_DROPS) and (DROPS) THEN
      penalty_dcat_code := 'TUI';

      OPEN curr_sfrfaud_dcat_c;
      FETCH curr_sfrfaud_dcat_c
        INTO curr_aud_tuit;
      CLOSE curr_sfrfaud_dcat_c;

      OPEN prev_sfrfaud_dcat_c;
      FETCH prev_sfrfaud_dcat_c
        INTO prev_aud_tuit;
      CLOSE prev_sfrfaud_dcat_c;
      tuit_total := curr_aud_tuit - prev_aud_tuit;
      p_print_dbms('in p_post_rbt_penalty: ' || tuit_total || ' pen: ' ||
                   (ROUND((tuit_total * rbt_tuit_penalty_percent), 2)) * -1);
      IF tuit_total < 0 THEN
        tuit_penalty_amount := (ROUND((tuit_total *
                                      rbt_tuit_penalty_percent),
                                      2)) * -1;
      END IF;

      IF tuit_penalty_amount > 0 THEN
        penalty_percent := rbt_tuit_penalty_percent * 100; -- for note
        -- record penalty in audit trail
        note       := g$_nls.get('SFKFEE1-0026',
                                 'SQL',
                                 ' %01% % penalty on total TUI difference %02% post penalty to clearing account',
                                 penalty_percent,
                                 tuit_total);
        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        --Defect 105101, for some reason TRANSLATE() is failing
        IF (accd_tran_number = 0) THEN
          accd_tran_number := NULL;
        END IF;

        p_print_dbms('p_insert_sfrfaud 13');
        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'P', -- penalty
                         'TUI',
                         rbt_clr_tuit_detl_code,
                         tuit_penalty_amount, -- 88907 tot_tuit_penalty,
                         'N', -- ABC: default to 'N'
                         source_pgm_in,
                         NULL, -- N/A: no rule seqno
                         NULL, -- N/A: no rule type
                         NULL, -- N/A: no CRN for clearing account
                         NULL, -- N/A: no rsts_code
                         NULL, -- N/A: no rsts_date
                         NULL, -- N/A: no ests_code
                         NULL, -- N/A: no ests_date
                         crn_rfnd_table,
                         NULL, -- N/A: no hours
                         NULL, -- N/A: no waiv hours
                         NULL, -- N/A: no rule cred hrs
                         NULL, -- N/A: no rule stud hrs
                         NULL, -- N/A: no per cred charge
                         NULL, -- N/A: no flat fee amt
                         NULL, -- N/A: no overload hrs
                         NULL, -- N/A: no crse overload charge
                         --TRANSLATE(accd_tran_number,0,NULL), Defect 105101
                         accd_tran_number,
                         note,
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr

        accd_tran_number := 0;
        p_print_dbms('insert tbraccd6: ' || rbt_clr_tuit_detl_code || ':' ||
                     tuit_penalty_amount);
        p_insert_tbraccd_rec(pidm_in,
                             accd_tran_number,
                             term_in,
                             rbt_clr_tuit_detl_code,
                             tuit_penalty_amount, -- 88907 tot_tuit_penalty,
                             tuit_penalty_amount, -- 88907 tot_tuit_penalty,
                             rbt_rfnd_eff_date, -- rbt_rfnd_date 89995
                             rbt_srce_code,
                             NULL, -- no CRN for clearing acct
                             assess_eff_date,
                             'Y');

        -- change during work for 89995
        -- back post accd_tran_number to qualified SFRFAUD records by rowid
        penalty_detl_code := rbt_clr_tuit_detl_code;

        IF (ASSESSMENT_POSTED) AND accd_tran_number <> 0 THEN
          FOR sfrfaud_rowid_rec IN sfrfaud_rowid_c LOOP
            UPDATE sfrfaud
               SET sfrfaud_accd_tran_number = accd_tran_number
             WHERE rowid = sfrfaud_rowid_rec.rowid;
          END LOOP;
        END IF;
      END IF;
    END IF;

    IF rbt_clr_fees_detl_code IS NOT NULL AND (DROPS) THEN
      penalty_dcat_code := 'FEE';

      OPEN curr_sfrfaud_dcat_c;
      FETCH curr_sfrfaud_dcat_c
        INTO curr_aud_fees;
      CLOSE curr_sfrfaud_dcat_c;

      OPEN prev_sfrfaud_dcat_c;
      FETCH prev_sfrfaud_dcat_c
        INTO prev_aud_fees;
      CLOSE prev_sfrfaud_dcat_c;
      fees_total := curr_aud_fees - prev_aud_fees;

      IF fees_total < 0 THEN
        fees_penalty_amount := (ROUND((fees_total *
                                      rbt_fees_penalty_percent),
                                      2)) * -1;
      END IF;

      IF fees_penalty_amount > 0 THEN
        penalty_percent := rbt_fees_penalty_percent * 100; -- for note
        -- record penalty in audit trail
        note := g$_nls.get('SFKFEE1-0027',
                           'SQL',
                           '%01% % penalty on total FEE difference %02%; post penalty to clearing account',
                           penalty_percent,
                           fees_total);

        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);
        --Defect 105101, for some reason TRANSLATE() is failing
        IF (accd_tran_number = 0) THEN
          accd_tran_number := NULL;
        END IF;

        p_print_dbms('p_insert_sfrfaud 14');
        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'P', -- penalty
                         'FEE',
                         rbt_clr_fees_detl_code,
                         fees_penalty_amount,
                         'N', -- ABC: default to 'N'
                         source_pgm_in,
                         NULL, -- N/A: no rule seqno
                         NULL, -- N/A: no rule type
                         NULL, -- N/A: no CRN for clearing account
                         NULL, -- N/A: no rsts_code
                         NULL, -- N/A: no rsts_date
                         NULL, -- N/A: no ests_code
                         NULL, -- N/A: no ests_date
                         crn_rfnd_table,
                         NULL, -- N/A: no hours
                         NULL, -- N/A: no waiv hours
                         NULL, -- N/A: no rule cred hrs
                         NULL, -- N/A: no rule stud hrs
                         NULL, -- N/A: no per cred charge
                         NULL, -- N/A: no flat fee amt
                         NULL, -- N/A: no overload hrs
                         NULL, -- N/A: no crse overload charge
                         --TRANSLATE(accd_tran_number,0,NULL),
                         accd_tran_number,
                         note,
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr

        accd_tran_number := 0;

        p_print_dbms('insert tbraccd7: ' || rbt_clr_fees_detl_code || ':' ||
                     fees_penalty_amount);
        p_insert_tbraccd_rec(pidm_in,
                             accd_tran_number,
                             term_in,
                             rbt_clr_fees_detl_code,
                             fees_penalty_amount,
                             fees_penalty_amount,
                             rbt_rfnd_eff_date,
                             rbt_srce_code,
                             NULL, -- no CRN for clearing acct
                             assess_eff_date,
                             'Y');

        -- back post accd_tran_number to qualified SFRFAUD records by rowid
        penalty_detl_code := rbt_clr_fees_detl_code;

        IF (ASSESSMENT_POSTED) AND accd_tran_number <> 0 THEN
          FOR sfrfaud_rowid_rec IN sfrfaud_rowid_c LOOP
            UPDATE sfrfaud
               SET sfrfaud_accd_tran_number = accd_tran_number
             WHERE rowid = sfrfaud_rowid_rec.rowid;
          END LOOP;
        END IF;

      END IF;
    END IF;
  END p_post_rbt_penalties;

  /* ******************************************************************* */
  /* Swapping.                                                3/2005 BAG */
  /* ******************************************************************* */
  /* Procedure to determine the amount of dropped hours that qualify for */
  /* swapping. Update SFTFEES to capture the amount of swapped hours:    */
  /* RBC: Set both sftfees_tuit_liab_percentage = 0 and                  */
  /*      sftfees_fees_liab_percentage = 0 for dropped hours that swap.  */
  /* ******************************************************************* */
  PROCEDURE p_process_hours_swap(pidm_in    IN spriden.spriden_pidm%TYPE,
                                 term_in    IN stvterm.stvterm_code%TYPE,
                                 phrs_out   OUT sfrstcr.sfrstcr_bill_hr%TYPE,
                                 pstart_out OUT DATE,
                                 pend_out   OUT DATE) IS
    lv_ddate       VARCHAR2(11);
    lv_date_date   date;
    lv_dhrs        sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    lv_ahrs        sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    lv_nethrs      sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    lv_tothrs      sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    lv_pstart      DATE;
    lv_pend        DATE;
    lv_psrce       VARCHAR2(1);
    lv_pamount     tbraccd.tbraccd_amount%TYPE := 0;
    lv_pdetl       tbraccd.tbraccd_detail_code%TYPE := 0;
    lv_tran_number tbraccd.tbraccd_tran_number%TYPE := 0;
    lv_count       NUMBER := 0;
    --1-1BCLLA
    reinit_count            NUMBER := 0;
    lv_tuit                 sftfees.sftfees_tuit_liab_percentage%TYPE := 0;
    lv_fees                 sftfees.sftfees_fees_liab_percentage%TYPE := 0;
    lv_max_rsts             date := null;
    lv_last_drop_assessment date := null;

    -- calc the hours for the assessment before the swap - takes two steps
    -- first first activity date on first swap
    lv_first_swap_date date;
    CURSOR first_assmt_for_swap_c is
      select min(sfrstcr_assess_activity_date)
        from sfrstcr
       where sfrstcr_term_code = term_in
         and sfrstcr_pidm = pidm_in
         and trunc(sfrstcr_rsts_date) = trunc(lv_date_date);

    --- find faud record for assessment at start of the swaps
    lv_min_act_date date := null;
    CURSOR min_act_date_c is
      select min(x.sfrfaud_activity_date)
        from sfrfaud x
       where x.sfrfaud_term_code = term_in
         and x.sfrfaud_pidm = pidm_in
         and x.sfrfaud_assess_rfnd_penlty_cde = 'N'
         and x.sfrfaud_detl_code is null
         and x.sfrfaud_activity_date >= lv_first_swap_date;
    --- find faud record for assessment before the swaps
    lv_max_date_before_swap date := null;

    CURSOR before_swap_hrs_c IS
      SELECT NVL(sfrfaud_reg_bill_hr, 0), sfrfaud_activity_date
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NULL
         AND sfrfaud_activity_date < lv_min_act_date
         AND sfrfaud_assess_rfnd_penlty_cde = 'N'
       order by sfrfaud_activity_date desc;

    --- find faud record for assessment at start of the swaps

    lv_max_date_at_swap date := null;
    CURSOR start_swap_hrs_c IS
      SELECT NVL(sfrfaud_reg_bill_hr, 0), sfrfaud_activity_date
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NULL
         AND sfrfaud_activity_date = lv_min_act_date
         AND sfrfaud_assess_rfnd_penlty_cde = 'N'
       order by sfrfaud_activity_date;

    CURSOR max_rsts_c is
      select max(sfrstcr_rsts_date)
        from sfrstcr
       where sfrstcr_pidm = pidm_in
         and sfrstcr_term_code = term_in;

    CURSOR penalty_period_c IS
      SELECT TRUNC(sfrrfnd_beg_date),
             TRUNC(sfrrfnd_end_date),
             sfrrfnd_srce_code
        FROM sfrrfnd
       WHERE sfrrfnd_term_code = term_in
         AND rbt_rfnd_date BETWEEN TRUNC(sfrrfnd_beg_date) AND
             TRUNC(sfrrfnd_end_date)
       ORDER BY TRUNC(sfrrfnd_beg_date),
                TRUNC(sfrrfnd_end_date),
                sfrrfnd_srce_code;

    CURSOR penalty_amount_c IS
      SELECT NVL(SUM(tbraccd_amount), 0), tbraccd_detail_code
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = lv_psrce
       GROUP BY tbraccd_detail_code;

    CURSOR stca_drop_dates_c IS
      SELECT trunc(sfrstca_rsts_date)
        FROM sfrstca
       WHERE sfrstca_term_code = term_in
         AND sfrstca_pidm = pidm_in
         AND sfrstca_source_cde = 'BASE'
         AND sfrstca_rsts_code IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_incl_assess = 'Y'
                 AND stvrsts_withdraw_ind = 'Y')
       GROUP BY trunc(sfrstca_rsts_date)
       ORDER BY trunc(sfrstca_rsts_date);

    CURSOR drop_last_assessment_c is
      SELECT max(sfrstcr_assess_activity_date)
        FROM stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         and sfrstcr_term_code = term_in
         and stvrsts_code = sfrstcr_rsts_code
            --  and stvrsts_withdraw_ind = 'Y'
            --  and stvrsts_incl_assess = 'Y'
         and trunc(sfrstcr_rsts_date) = lv_date_date;

    lv_any_notassessed varchar2(1);
    CURSOR any_notassessment_last_c is
      SELECT 'Y'
        FROM stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         and sfrstcr_term_code = term_in
         and stvrsts_code = sfrstcr_rsts_code
         and stvrsts_incl_assess = 'Y'
         and sfrstcr_assess_activity_date > last_assessment_date
         and trunc(sfrstcr_rsts_date) = lv_date_date;

    CURSOR any_rbt_notassessment_last_c is
      SELECT 'Y'
        FROM stvrsts, sfrstcr
       WHERE sfrstcr_pidm = pidm_in
         and sfrstcr_term_code = term_in
         and stvrsts_code = sfrstcr_rsts_code
         and stvrsts_incl_assess = 'Y'
         and sfrstcr_assess_activity_date > last_assessment_date;

    CURSOR hrs_dropped_c IS
      SELECT NVL(SUM(d.sfrstca_bill_hr), 0)
        FROM sfrstca d
       WHERE d.sfrstca_pidm = pidm_in
         AND d.sfrstca_term_code = term_in
         AND ((lv_ddate IS NOT NULL AND
             TO_CHAR(d.sfrstca_rsts_date, G$_DATE.GET_NLS_DATE_FORMAT) =
             lv_ddate) OR
             (lv_pstart IS NOT NULL AND lv_pend IS NOT NULL AND
             TO_CHAR(d.sfrstca_rsts_date, G$_DATE.GET_NLS_DATE_FORMAT) BETWEEN
             lv_pstart AND lv_pend))
         AND d.sfrstca_source_cde = 'BASE'
         AND d.sfrstca_rsts_code IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_incl_assess = 'Y'
                 AND stvrsts_withdraw_ind = 'Y')
            --- 8.1.1  do not look only at lastest audit record, you will miss others added on same day
         AND d.sfrstca_seq_number =
             (SELECT MAX(z.sfrstca_seq_number)
                FROM sfrstca z
               WHERE z.sfrstca_term_code = term_in
                 AND z.sfrstca_pidm = pidm_in
                 AND z.sfrstca_source_cde = 'BASE'
                 AND z.sfrstca_crn = d.sfrstca_crn
                 AND z.sfrstca_rsts_code IN
                     (SELECT stvrsts_code
                        FROM stvrsts
                       WHERE stvrsts_incl_assess = 'Y'
                         AND stvrsts_withdraw_ind = 'Y'))
            --  8.2.1 only select crns to add in drop if the last trans was for a drop
            --    AND    exists
            --      (SELECT 1
            --       FROM   sfrstca z
            --       WHERE  z.sfrstca_term_code = term_in
            --       AND    z.sfrstca_pidm = pidm_in
            --       AND    z.sfrstca_source_cde = 'BASE'
            --       AND    z.sfrstca_crn = d.sfrstca_crn
            --      AND    z.sfrstca_rsts_code IN
            --      (SELECT stvrsts_code FROM stvrsts
            --       WHERE  stvrsts_incl_assess  = 'Y'
            --       AND    stvrsts_withdraw_ind = 'Y')
            -- is the first occurrence
            --      and   z.sfrstca_seq_number =
            --       (SELECT MIN(zz.sfrstca_seq_number)
            --        FROM   sfrstca zz
            --        WHERE  zz.sfrstca_term_code = term_in
            --        AND    zz.sfrstca_pidm = pidm_in
            --        AND    zz.sfrstca_source_cde = 'BASE'
            --        AND    zz.sfrstca_crn = d.sfrstca_crn ))
            -- last audit cant be a dd
         AND d.sfrstca_crn NOT IN
             (SELECT x.sfrstca_crn
                FROM stvrsts y, sfrstca x
               WHERE x.sfrstca_term_code = term_in
                 AND x.sfrstca_pidm = pidm_in
                 AND x.sfrstca_source_cde = 'BASE'
                 AND x.sfrstca_rsts_code = y.stvrsts_code
                 AND y.stvrsts_incl_assess = 'N'
                 AND x.sfrstca_seq_number =
                     (SELECT MAX(m.sfrstca_seq_number)
                        FROM sfrstca m
                       WHERE m.sfrstca_term_code = term_in
                         AND m.sfrstca_pidm = pidm_in
                         AND m.sfrstca_source_cde = 'BASE'
                         AND m.sfrstca_crn = x.sfrstca_crn));

    CURSOR hrs_added_c IS
      SELECT NVL(SUM(d.sfrstca_bill_hr), 0)
        FROM sfrstca d
       WHERE d.sfrstca_pidm = pidm_in
         AND d.sfrstca_term_code = term_in
         AND ((lv_ddate IS NOT NULL AND
             TO_CHAR(d.sfrstca_rsts_date, G$_DATE.GET_NLS_DATE_FORMAT) =
             lv_ddate) OR
             (lv_pstart IS NOT NULL AND lv_pend IS NOT NULL AND
             TO_CHAR(d.sfrstca_rsts_date, G$_DATE.GET_NLS_DATE_FORMAT) BETWEEN
             lv_pstart AND lv_pend))
         AND d.sfrstca_source_cde = 'BASE'
         AND d.sfrstca_rsts_code IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_incl_assess = 'Y'
                 AND stvrsts_withdraw_ind = 'N')
            -- 8.1.1 do not look only at lastest audit record, you will miss others added on same day
            -- 8.2.1 uncomment out
         AND d.sfrstca_seq_number =
             (SELECT MAX(z.sfrstca_seq_number)
                FROM sfrstca z
               WHERE z.sfrstca_term_code = term_in
                 AND z.sfrstca_pidm = pidm_in
                 AND z.sfrstca_source_cde = 'BASE'
                 AND z.sfrstca_crn = d.sfrstca_crn
                 AND z.sfrstca_rsts_code IN
                     (SELECT stvrsts_code
                        FROM stvrsts
                       WHERE stvrsts_incl_assess = 'Y'
                         AND stvrsts_withdraw_ind = 'N'))
            --  8.2.1 verify that the last rsts for this scta is different
            --   AND    exists
            --     (SELECT 1
            --      FROM   sfrstca z
            --     WHERE  z.sfrstca_term_code = term_in
            --     AND    z.sfrstca_pidm = pidm_in
            --     AND    z.sfrstca_source_cde = 'BASE'
            --     AND    z.sfrstca_crn = d.sfrstca_crn
            --     AND    z.sfrstca_rsts_code IN
            --     (SELECT stvrsts_code FROM stvrsts
            --     WHERE  stvrsts_incl_assess  = 'Y'
            --     AND    stvrsts_withdraw_ind = 'N')
            --    and   z.sfrstca_seq_number =
            --     (SELECT MAX(zz.sfrstca_seq_number)
            --      FROM   sfrstca zz
            --      WHERE  zz.sfrstca_term_code = term_in
            --      AND    zz.sfrstca_pidm = pidm_in
            --      AND    zz.sfrstca_source_cde = 'BASE'
            --      AND    zz.sfrstca_crn = d.sfrstca_crn
            --        AND    zz.sfrstca_rsts_code IN
            --      and  (SELECT rr.stvrsts_code FROM stvrsts rr
            --           WHERE  rr.stvrsts_incl_assess  = 'Y'
            --           AND    rr.stvrsts_withdraw_ind = 'N')
            --    and TO_CHAR(d.sfrstca_rsts_date,G$_DATE.GET_NLS_DATE_FORMAT) = lv_ddate))
            -- the latest trans cannot be a dd
         AND d.sfrstca_crn NOT IN
             (SELECT x.sfrstca_crn
                FROM stvrsts y, sfrstca x
               WHERE x.sfrstca_term_code = term_in
                 AND x.sfrstca_pidm = pidm_in
                 AND x.sfrstca_source_cde = 'BASE'
                 AND x.sfrstca_rsts_code = y.stvrsts_code
                 AND y.stvrsts_incl_assess = 'N'
                 AND x.sfrstca_seq_number =
                     (SELECT MAX(m.sfrstca_seq_number)
                        FROM sfrstca m
                       WHERE m.sfrstca_term_code = term_in
                         AND m.sfrstca_pidm = pidm_in
                         AND m.sfrstca_source_cde = 'BASE'
                         AND m.sfrstca_crn = x.sfrstca_crn));

    hrs_bill  sfrstca.sfrstca_bill_hr%type;
    hrs_rsts  stvrsts.stvrsts_code%type;
    hrs_date  date;
    hrs_with  stvrsts.stvrsts_withdraw_ind%type;
    hrs_seq   sfrstca.sfrstca_seq_number%type;
    hrs_crn   sfrstca.sfrstca_crn%type;
    save_crn  sfrstca.sfrstca_crn%type;
    add_ok    varchar2(1) := '';
    save_with stvrsts.stvrsts_withdraw_ind%type;
    CURSOR hrs_swapped_c IS
      SELECT d.sfrstca_bill_hr,
             d.sfrstca_rsts_code,
             d.sfrstca_activity_date,
             a.stvrsts_withdraw_ind,
             d.sfrstca_seq_number,
             d.sfrstca_crn
        FROM stvrsts a, sfrstca d
       WHERE d.sfrstca_pidm = pidm_in
         AND d.sfrstca_term_code = term_in
         AND ((lv_ddate IS NOT NULL AND
             TO_CHAR(d.sfrstca_rsts_date, G$_DATE.GET_NLS_DATE_FORMAT) =
             lv_ddate) OR
             (lv_pstart IS NOT NULL AND lv_pend IS NOT NULL AND
             TO_CHAR(d.sfrstca_rsts_date, G$_DATE.GET_NLS_DATE_FORMAT) BETWEEN
             lv_pstart AND lv_pend))
         AND d.sfrstca_source_cde = 'BASE'
         AND d.sfrstca_rsts_code = a.stvrsts_code
            --  must be include in assessment to be included in swap
         and a.stvrsts_incl_assess <> 'N'
            -- last rsts for the day cannot be a dd
         AND d.sfrstca_crn NOT IN
             (SELECT x.sfrstca_crn
                FROM stvrsts y, sfrstca x
               WHERE x.sfrstca_term_code = term_in
                 AND x.sfrstca_pidm = pidm_in
                 AND x.sfrstca_source_cde = 'BASE'
                 AND x.sfrstca_rsts_code = y.stvrsts_code
                 AND y.stvrsts_incl_assess = 'N'
                    --  DD cannot be last stca for the day
                 AND TO_CHAR(d.sfrstca_rsts_date,
                             G$_DATE.GET_NLS_DATE_FORMAT) = lv_ddate
                 AND x.sfrstca_seq_number =
                     (SELECT MAX(m.sfrstca_seq_number)
                        FROM sfrstca m
                       WHERE m.sfrstca_term_code = term_in
                         AND m.sfrstca_pidm = pidm_in
                         AND m.sfrstca_source_cde = 'BASE'
                         AND m.sfrstca_crn = x.sfrstca_crn))
       order by d.sfrstca_crn, d.sfrstca_activity_date;

    --- get the penatly for the first assessment of the swap
    --- need to get the penalty for the first DC of the day

    CURSOR sftfees_drops_c IS
      SELECT sftfees.ROWID,
             sftfees_reg_bill_hr,
             sftfees_fees_liab_percentage,
             sftfees_tuit_liab_percentage,
             sftfees_crn,
             sftfees_rsts_date,
             SFTFEES_WAIV_HR_TUIT,
             SFTFEES_WAIV_HR_FEES,
             sfrstca_activity_date
        FROM sfrstca, stvrsts, sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND stvrsts_code = sftfees_rsts_cde
         AND sfrstca_term_code = term_in
         AND sfrstca_crn = sftfees_crn
         AND sfrstca_pidm = pidm_in
         AND stvrsts_incl_assess = 'Y'
         AND stvrsts_withdraw_ind = 'Y'
         AND ((lv_date_date IS NOT NULL AND
             trunc(sftfees_rsts_date) = lv_date_date))
         AND sfrstca_seq_number =
             (SELECT MAX(m.sfrstca_seq_number)
                FROM sfrstca m
               WHERE m.sfrstca_term_code = term_in
                 AND m.sfrstca_pidm = pidm_in
                 AND m.sfrstca_source_cde = 'BASE'
                 and trunc(m.sfrstca_rsts_date) = lv_date_date
                 AND m.sfrstca_crn = sftfees_crn)
       order by sfrstca_seq_number;

    sftfees_rec sftfees_drops_c%ROWTYPE;

  BEGIN
    p_print_dbms('Start p_process_hours_swap');
    /* *************************************************************** */
    /* For RBC, determine the registration dates where the student     */
    /* has drop activity and go on to determine the number of bill     */
    /* hours dropped and added on the given day.                       */
    /*                                                                 */
    /* For RBT, loop on the defined penalty periods for the term. Go   */
    /* on to determine the number of bill hours dropped and added in   */
    /* the date range for the penalty period.                          */
    /* *************************************************************** */
    --104070
    SWAP_RBC_MORE_DROPPED := FALSE;
    SWAP_RBC_EQUAL        := FALSE;
    SWAP_RBC_MORE_ADDS    := FALSE;
    SWAP                  := FALSE;
    rev_lv_dhrs           := 0;

    IF NVL(sobterm_rec.sobterm_refund_ind, 'N') = 'N' THEN
      -- Get the dates when the student dropped courses.
      OPEN stca_drop_dates_c;
      LOOP
        FETCH stca_drop_dates_c
          INTO lv_date_date;
        IF stca_drop_dates_c%FOUND THEN
          lv_ddate := to_char(lv_date_date, G$_DATE.GET_NLS_DATE_FORMAT);
          --1-1BCLLA
          SWAP_RBC_EQUAL        := FALSE;
          SWAP_RBC_MORE_DROPPED := FALSE;

          -- Get the total hours dropped for the date.
          OPEN hrs_dropped_c;
          FETCH hrs_dropped_c
            INTO lv_dhrs;
          CLOSE hrs_dropped_c;

          p_print_dbms('==p_process_hours_swap1 lv_ddate lv_pstart lv_pend: ' ||
                       lv_ddate || ':' || lv_pstart || ':' || lv_pend ||
                       ' dhrs: ' || lv_dhrs);

          -- Get the total hours added for the date.
          OPEN hrs_added_c;
          FETCH hrs_added_c
            INTO lv_ahrs;
          CLOSE hrs_added_c;

          p_print_dbms('==p_process_hours_swap2 lv_ddate lv_pstart lv_pend: ' ||
                       lv_ddate || ':' || lv_pstart || ':' || lv_pend ||
                       ' add hrs: ' || lv_ahrs);

          -- new with 8.2.1 to find hrs swapped considering all options of drops and adds on the same day
          hrs_bill  := 0;
          hrs_rsts  := '';
          hrs_date  := '';
          hrs_with  := '';
          hrs_seq   := 0;
          hrs_crn   := '';
          save_crn  := '';
          add_ok    := '';
          save_with := '';
          lv_ahrs   := 0;
          lv_dhrs   := 0;
          open hrs_swapped_c;
          loop
            fetch hrs_swapped_c
              into hrs_bill, hrs_rsts, hrs_date, hrs_with, hrs_seq, hrs_crn;
            exit when hrs_swapped_c%notfound;
            -- make sure the last seq on that day for the crn does not have the same rsts type
            if save_crn is null or save_crn <> hrs_crn then
              add_ok    := 'Y';
              save_with := hrs_with;
            else
              if save_with is null or save_with <> hrs_with then
                add_ok    := 'Y';
                save_with := hrs_with;
              else
                add_ok := 'N';
              end if;
            end if;
            save_crn := hrs_crn;
            p_print_dbms('swap crn hrs: ' || hrs_crn || ' with: ' ||
                         hrs_with || ' hrs: ' || hrs_bill || ' ok: ' ||
                         add_ok || ' save crn: ' || save_crn ||
                         '  save with: ' || save_with);
            --  add in hrs
            if add_ok = 'Y' then
              if hrs_with = 'Y' then
                lv_dhrs := lv_dhrs + hrs_bill;
              else
                lv_ahrs := lv_ahrs + hrs_bill;
              end if;
            end if;
          end loop;
          close hrs_swapped_c;
          p_print_dbms('==NEW drop hrs: ' || lv_dhrs || ' add hrs: ' ||
                       lv_ahrs);

          --104708, To make sure that there is a real SWAP going on.
          --Drop/Add hours need to be more than 0 to be processed.
          p_print_dbms('lv_dhrs > 0 AND lv_ahrs > 0: ' || lv_dhrs || ':' ||
                       lv_ahrs);
          -- 7.4.0.2  Reset the rev penalty hours.  This is in case there are multiple refund periods and a swap
          --  was in an earlier one,  we don't want the penalty to carry into subsequent refund periods
          rev_penalty_hrs_tui    := 0;
          rev_penalty_hrs_fee    := 0;
          recalc_penalty_hrs_fee := 0;
          recalc_penalty_hrs_tui := 0;
          rev_lv_dhrs            := 0;
          swap_date              := null;
          start_swap_hours       := 0;
          before_swap_hours      := 0;
          IF lv_dhrs > 0 AND lv_ahrs > 0 THEN
            /* ******************************************************************* */
            /* Adjust SFTFEES row(s) to have no liability for the 'swapped' drops. */
            /* ******************************************************************* */
            /* If more hours are added than dropped, all the hours dropped are     */
            /* considered to be swapped.                                           */
            /*                                                                     */
            /* If more hours are dropped than added, determine the difference in   */
            /* hours and update SFTFEES to have liability on the difference in one */
            /* of the dropped courses.                                             */
            /* ******************************************************************* */
            lv_nethrs := lv_dhrs - lv_ahrs; -- determine the net difference
            p_print_dbms('lv_nethrs: ' || lv_nethrs);
            swap_date := lv_date_date;

            --- 7.4.0.3 get the hours before the swap
            lv_first_swap_date := null;
            open first_assmt_for_swap_c;
            fetch first_assmt_for_swap_c
              into lv_first_swap_date;
            if first_assmt_for_swap_c%found then
              first_swap_date := lv_first_swap_date;
              p_print_dbms('First swap date: ' ||
                           to_char(first_swap_date,
                                   'DD-MON-YYYY HH24:MI:SS'));
              lv_min_act_date := null;
              open min_act_date_c;
              fetch min_act_date_c
                into lv_min_act_date;
              if min_act_date_c%found then
                open before_swap_hrs_c;
                fetch before_swap_hrs_c
                  into before_swap_hours, lv_max_date_before_swap;
                close before_swap_hrs_c;

                open start_swap_hrs_c;
                fetch start_swap_hrs_c
                  into start_swap_hours, lv_max_date_at_swap;
                close start_swap_hrs_c;

                p_print_dbms('hrs before swap: ' || before_swap_hours ||
                             ' hrs before first swap: ' ||
                             start_swap_hours || ' date: ' ||
                             to_char(lv_max_date_before_swap,
                                     'DD-MON-YYYY HH24:MI:SS'));
              end if;
              close min_act_date_c;
            end if;
            close first_assmt_for_swap_c;

            --IF lv_nethrs > 0 THEN
            IF lv_nethrs > 0 THEN
              /*104171, needs to be changed to > 0 */
              -- more hours were dropped than added
              -- set all drops to 0 liablity, then save penalty hours
              -- in one dropped course
              p_print_dbms('More drops than add: ' || lv_nethrs);

              SWAP_RBC_MORE_DROPPED := TRUE; --104070
              rev_dropped_hrs       := lv_dhrs;

              FOR sftfees_rec IN sftfees_drops_c LOOP
                lv_tuit := sftfees_rec.sftfees_tuit_liab_percentage;
                lv_fees := sftfees_rec.sftfees_fees_liab_percentage;

                -- update all tuit liab percents to 0 on all dropped courses
                p_print_dbms('update all tui and fee liab on sfkfees to 0');
                p_print_dbms('crn: ' || sftfees_rec.sftfees_crn || ' ' ||
                             ' tui liab: ' ||
                             sftfees_rec.sftfees_tuit_liab_percentage ||
                             ' rsts date: ' ||
                             sftfees_rec.sftfees_rsts_date ||
                             ' act date: ' ||
                             to_char(sftfees_rec.sfrstca_activity_date,
                                     'DD-MON-YYYY HH24:MI:SS'));

                UPDATE sftfees
                   SET sftfees_tuit_liab_percentage = 0,
                       sftfees_fees_liab_percentage = 0
                 WHERE sftfees.ROWID = sftfees_rec.ROWID;
              END LOOP;
              -- 104070, these rev_penalty_hrs_tui is needed in case
              -- re-evaluation is needed.  i.e., an assessment was previously done
              -- using the same date as what is currently being used for assessment.
              p_print_dbms('lv tuit: ' || lv_tuit || ' lv drop hours: ' ||
                           lv_dhrs || ' lv add hours ' || lv_ahrs);
              rev_penalty_hrs_tui := lv_dhrs * lv_tuit;
              rev_penalty_hrs_fee := lv_dhrs * lv_fees;
              --- 1-2JTK2Y  reinitialize the counter that is used to check for the last drop
              lv_count := 0;
              p_print_dbms('in swapping more drops than adds, rev penalty: ' ||
                           rev_penalty_hrs_tui);
              FOR sftfees_rec IN sftfees_drops_c LOOP
                lv_count := lv_count + 1;
                p_print_dbms('Update hrs crn: ' || sftfees_rec.sftfees_crn || ' ' ||
                             ' new tui liab: ' || lv_nethrs ||
                             ' act date: ' ||
                             to_char(sftfees_rec.sfrstca_activity_date,
                                     'DD-MON-YYYY HH24:MI:SS'));
                ---  do not update the waiv hrs if they are 0   1-86QG5P  8.3
                if sftfees_rec.sftfees_waiv_hr_tuit > 0 and
                   sftfees_rec.sftfees_waiv_hr_fees > 0 then
                  UPDATE sftfees
                     SET sftfees_bill_hr_tuit         = lv_nethrs,
                         sftfees_bill_hr_fees         = lv_nethrs,
                         sftfees_waiv_hr_tuit         = lv_nethrs,
                         sftfees_waiv_hr_fees         = lv_nethrs,
                         sftfees_tuit_liab_percentage = lv_tuit,
                         sftfees_fees_liab_percentage = lv_fees
                   WHERE sftfees.ROWID = sftfees_rec.ROWID;
                elsif sftfees_rec.sftfees_waiv_hr_tuit = 0 and
                      sftfees_rec.sftfees_waiv_hr_fees > 0 then
                  UPDATE sftfees
                     SET sftfees_bill_hr_tuit         = lv_nethrs,
                         sftfees_bill_hr_fees         = lv_nethrs,
                         sftfees_waiv_hr_fees         = lv_nethrs,
                         sftfees_tuit_liab_percentage = lv_tuit,
                         sftfees_fees_liab_percentage = lv_fees
                   WHERE sftfees.ROWID = sftfees_rec.ROWID;
                elsif sftfees_rec.sftfees_waiv_hr_tuit > 0 and
                      sftfees_rec.sftfees_waiv_hr_fees = 0 then
                  UPDATE sftfees
                     SET sftfees_bill_hr_tuit         = lv_nethrs,
                         sftfees_bill_hr_fees         = lv_nethrs,
                         sftfees_waiv_hr_tuit         = lv_nethrs,
                         sftfees_tuit_liab_percentage = lv_tuit,
                         sftfees_fees_liab_percentage = lv_fees
                   WHERE sftfees.ROWID = sftfees_rec.ROWID;
                else
                  UPDATE sftfees
                     SET sftfees_bill_hr_tuit         = lv_nethrs,
                         sftfees_bill_hr_fees         = lv_nethrs,
                         sftfees_tuit_liab_percentage = lv_tuit,
                         sftfees_fees_liab_percentage = lv_fees
                   WHERE sftfees.ROWID = sftfees_rec.ROWID;
                end if;
                /* 104070 - Assign the tui/fee liab percentage
                so that later on the code, during
                computation, fee assessment will use
                these liable percentages and not the
                percentages used in sftfees.   */
                -- 1-2jtk2y do this only if there was a flat and how do we know that?

                tui_below_flat_pcent := lv_tuit;
                fee_below_flat_pcent := lv_fees;

                p_print_dbms('tui_below_flat_pcent: ' ||
                             tui_below_flat_pcent || ' lv cnt: ' ||
                             lv_count);
                --1-1BCLLA, comment the three lines below.
              --IF lv_count = 1 THEN
              --   EXIT;
              --END IF;
              END LOOP;

              -- 1-1BCLLA, to re-initialize the liable percentages on all
              -- sftfees records except for the last one.
              --- 1-2JTK2Y  reinitialize the counter that is used to check for the last drop
              reinit_count := 0;
              IF lv_count > 1 THEN
                p_print_dbms('counter of drops: ' || lv_count);
                FOR sftfees_rec IN sftfees_drops_c LOOP
                  reinit_count := reinit_count + 1;
                  p_print_dbms('counter of drops: ' || lv_count ||
                               ' reinit count: ' || reinit_count ||
                               ' rsts date: ' ||
                               sftfees_rec.sftfees_rsts_date);
                  IF reinit_count = lv_count THEN
                    EXIT;
                  END IF;
                  p_print_dbms('update the per liability for counter: ' ||
                               reinit_count);
                  UPDATE sftfees
                     SET sftfees_tuit_liab_percentage = 0,
                         sftfees_fees_liab_percentage = 0
                   WHERE sftfees.ROWID = sftfees_rec.ROWID;
                END LOOP;
              END IF;

              -- if this student has been BELOW FLAT ever since.  do the following.

              recalc_penalty_hrs_tui := lv_nethrs * lv_tuit;
              recalc_penalty_hrs_fee := lv_nethrs * lv_fees;
              p_print_dbms('recalc_penalty_hrs_tui: ' ||
                           recalc_penalty_hrs_tui || ' swap date: ' ||
                           swap_date || ' start hours: ' ||
                           start_swap_hours);
            ELSE
              -- more hours were added than dropped; all are swapped
              --104171
              p_print_dbms('More hours were added than dropped or equal amts: ' ||
                           lv_nethrs);

              FOR sftfees_rec IN sftfees_drops_c LOOP
                rev_tuit := sftfees_rec.sftfees_tuit_liab_percentage;
                rev_fees := sftfees_rec.sftfees_fees_liab_percentage;
                IF (rev_tuit <> 0) or (rev_fees <> 0) THEN
                  EXIT;
                END IF;
              END LOOP;

              --1-PX0E9
              SWAP_RBC_EQUAL := FALSE;
              IF lv_nethrs = 0 THEN
                SWAP_RBC_EQUAL := TRUE; --104070

                --1-PX0E9
                FOR sftfees_rec IN sftfees_drops_c LOOP
                  lv_tuit := sftfees_rec.sftfees_tuit_liab_percentage;
                  lv_fees := sftfees_rec.sftfees_fees_liab_percentage;

                  --
                  UPDATE sftfees
                     SET sftfees_tuit_liab_percentage = 0,
                         sftfees_fees_liab_percentage = 0
                   WHERE sftfees.ROWID = sftfees_rec.ROWID;
                END LOOP;

                tui_below_flat_pcent := 0;
                fee_below_flat_pcent := 0;

                FOR sftfees_rec IN sftfees_drops_c LOOP
                  lv_count := lv_count + 1;
                  --- 1-2JTK2Y  do not replace hours with 0
                  -- UPDATE sftfees
                  --  SET    sftfees_bill_hr_tuit = lv_nethrs,
                  --         sftfees_bill_hr_fees = lv_nethrs,
                  --         sftfees_waiv_hr_tuit = lv_nethrs,
                  --         sftfees_waiv_hr_fees = lv_nethrs
                  --  WHERE  sftfees.ROWID = sftfees_rec.ROWID;

                  /* 104070 - Assign the tui/fee liab percentage
                  so that later on the code, during
                  computation, fee assessment will use
                  these liable percentages and not the
                  percentages used in sftfees.   */

                  tui_below_flat_pcent := rev_tuit;
                  fee_below_flat_pcent := rev_fees;
                  --   EXIT;
                END LOOP;

              ELSE
                SWAP_RBC_MORE_ADDS := TRUE;
              END IF;

              -- 104070, these rev_penalty_hrs_tui is needed in case
              -- re-evaluation is needed.  i.e., an assessment was previously done
              -- using the same date as what is currently being used for assessment.
              rev_penalty_hrs_tui := lv_dhrs * rev_tuit;
              rev_penalty_hrs_fee := lv_dhrs * rev_fees;
              rev_lv_dhrs         := lv_dhrs;
              lv_tothrs           := 0;
              --
              IF NOT (SWAP_RBC_EQUAL) THEN
                --1-PX0E9
                FOR sftfees_rec IN sftfees_drops_c LOOP
                  lv_tothrs := lv_tothrs + sftfees_rec.sftfees_reg_bill_hr;
                  IF lv_tothrs <= lv_dhrs THEN
                    UPDATE sftfees
                       SET sftfees_tuit_liab_percentage = 0,
                           sftfees_fees_liab_percentage = 0
                     WHERE sftfees.ROWID = sftfees_rec.ROWID;
                  ELSE
                    EXIT;
                  END IF;
                END LOOP;
              END IF;
            END IF;
          END IF; --104708, lv_dhrs AND lv_ahrs > 0
          p_print_dbms('######### lv_dhrs lv_ahrs: ' || lv_dhrs || ':' ||
                       lv_ahrs || ' rev pen: ' || rev_penalty_hrs_tui ||
                       ' recalc: ' || recalc_penalty_hrs_tui);
        ELSE
          EXIT;
        END IF;
      END LOOP;
      CLOSE stca_drop_dates_c;

      lv_any_notassessed := null;
      open any_notassessment_last_c;
      fetch any_notassessment_last_c
        into lv_any_notassessed;
      close any_notassessment_last_c;

      lv_max_rsts := null;
      open max_rsts_c;
      fetch max_rsts_c
        into lv_max_rsts;
      close max_rsts_c;

      lv_last_drop_assessment := null;
      open drop_last_assessment_c;
      fetch drop_last_assessment_c
        into lv_last_drop_assessment;
      close drop_last_assessment_c;

      p_print_dbms('last drop date: ' ||
                   to_char(lv_date_date, 'DD-MON-YYYY HH24:MI:SS') ||
                   ' curr assmnt: ' ||
                   to_char(lv_max_rsts, 'DD-MON-YYYY HH24:MI:SS') ||
                   '  last drop ass: ' ||
                   to_char(lv_last_drop_assessment,
                           'DD-MON-YYYY HH24:MI:SS'));
      p_print_dbms(' any ass left: ' || lv_any_notassessed ||
                   ' prev ass: ' ||
                   to_char(prev_assessment_date, 'DD-MON-YYYY HH24:MI:SS') ||
                   ' last assess date: ' ||
                   to_char(last_assessment_date, 'DD-MON-YYYY HH24:MI:SS'));
      --- reset the swapping variables if the last swapping was for a previous assessmnet
      --  and not the current rsts date
      IF nvl(lv_any_notassessed, 'N') = 'N' then
        p_print_dbms('reset the swapping globals');
        SWAP_RBC_MORE_DROPPED  := FALSE;
        SWAP_RBC_EQUAL         := FALSE;
        SWAP_RBC_MORE_ADDS     := FALSE;
        rev_penalty_hrs_tui    := 0;
        rev_penalty_hrs_fee    := 0;
        recalc_penalty_hrs_fee := 0;
        recalc_penalty_hrs_tui := 0;
        swap_date              := null;
        rev_lv_dhrs            := 0;
        start_swap_hours       := 0;
        before_swap_hours      := 0;
      end if;
    ELSE
      -- RBT in effect
      -- get the start and end dates for the penalty period
      p_print_dbms('rbt date in swapping: ' || rbt_rfnd_date);
      OPEN penalty_period_c;
      LOOP
        FETCH penalty_period_c
          INTO lv_pstart, lv_pend, lv_psrce;

        pstart_out := lv_pstart;
        pend_out   := lv_pend;

        IF penalty_period_c%NOTFOUND THEN
          EXIT;
        ELSE
          -- get the total hours dropped for the penalty period date range
          OPEN hrs_dropped_c;
          FETCH hrs_dropped_c
            INTO lv_dhrs;
          CLOSE hrs_dropped_c;

          -- get the total hours added for the penalty period date range
          OPEN hrs_added_c;
          FETCH hrs_added_c
            INTO lv_ahrs;
          CLOSE hrs_added_c;
        END IF;
      END LOOP;
      CLOSE penalty_period_c;
      --- 8.1 we need to make sure the swapping wsnt already assessed
      -- skip if all courses in the swap were assessed before
      lv_any_notassessed := null;
      open any_rbt_notassessment_last_c;
      fetch any_rbt_notassessment_last_c
        into lv_any_notassessed;
      close any_rbt_notassessment_last_c;
      --- reset the swapping variables if the last swapping was for a previous assessmnet
      --  and not the current rsts date
      IF nvl(lv_any_notassessed, 'N') = 'Y' then
        p_print_dbms('swapping done within last assessment');
        -- If the amount of hours added is greater than or equal to the
        -- amount of hours dropped, all dropped hours are considered swapped.
        IF (lv_ahrs > 0 AND (lv_ahrs >= lv_dhrs)) THEN
          p_print_dbms('1. SWAP is true');
          SWAP := TRUE;
        END IF;

        -- If more hours dropped than added, student pays penalty on net difference in hours.
        -- Set the penalty hours here to return to the caller. Go on to determine penalty.
        IF (lv_dhrs > lv_ahrs) THEN
          lv_nethrs := lv_dhrs - lv_ahrs;
          IF lv_ahrs > 0 THEN
            p_print_dbms('2. SWAP is true');
            SWAP := TRUE;
          END IF;
          phrs_out := lv_nethrs;
        END IF;

        PENALTY_REV_DONE := FALSE;
        IF (SWAP) THEN
          -- check for penalty for the source, reverse if found
          OPEN penalty_amount_c;
          LOOP
            FETCH penalty_amount_c
              INTO lv_pamount, lv_pdetl;
            IF penalty_amount_c%NOTFOUND THEN
              EXIT;
            ELSIF lv_pamount > 0 THEN
              lv_pamount := lv_pamount * -1; -- generate a credit

              PENALTY_REV_DONE := TRUE;

              lv_tran_number := 0;
              p_print_dbms('insert tbraccd8: ' || lv_pdetl || ':' ||
                           lv_pamount);

              p_insert_tbraccd_rec(pidm_in,
                                   lv_tran_number,
                                   term_in,
                                   lv_pdetl,
                                   lv_pamount,
                                   lv_pamount,
                                   rbt_rfnd_eff_date,
                                   lv_psrce,
                                   NULL, -- no CRN for penalty
                                   assess_eff_date,
                                   'Y');
            END IF;
          END LOOP;
          CLOSE penalty_amount_c;
        END IF;
      END IF; --- swapping not assessed yet
    END IF; -- RBT or RBC
    p_print_dbms('End p_process_hours_swap');

  END p_process_hours_swap;

  /* ******************************************************************* */
  /* Procedure to determine the swap liability and store in SFRFAUD.     */
  /* ******************************************************************* */
  PROCEDURE p_calc_swap_liability(pidm_in       IN spriden.spriden_pidm%TYPE,
                                  term_in       IN stvterm.stvterm_code%TYPE,
                                  phrs_in       IN sfrstcr.sfrstcr_bill_hr%TYPE,
                                  pstart_in     IN DATE,
                                  pend_in       IN DATE,
                                  source_pgm_in IN VARCHAR2) IS
    tothrs             sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    balhrs             sfrstcr.sfrstcr_bill_hr%TYPE := 0;
    accd_tran_number   tbraccd.tbraccd_tran_number%TYPE := 0;
    swap_dcat_code     ttvdcat.ttvdcat_code%TYPE;
    swap_detl_code     tbbdetc.tbbdetc_detail_code%TYPE;
    swap_assess_amount sfrfaud.sfrfaud_charge%TYPE := 0;
    curr_assess_amount sfrfaud.sfrfaud_charge%TYPE := 0;
    amount_to_penalize sfrfaud.sfrfaud_charge%TYPE := 0;
    penalty_amount     tbraccd.tbraccd_amount%TYPE := 0;
    penalty_detl_code  tbbdetc.tbbdetc_detail_code%TYPE := '';
    swap_crn           sfrfaud.sfrfaud_crn%TYPE;
    next_seqno         sfrfaud.sfrfaud_seqno%TYPE;
    penalty_percent    NUMBER(3); -- for audit note text
    note               sfrfaud.sfrfaud_note%TYPE := '';

    CURSOR sftfees_drops_c IS
      SELECT ROWID,
             sftfees_reg_bill_hr,
             sftfees_fees_liab_percentage,
             sftfees_tuit_liab_percentage
        FROM sftfees
       WHERE sftfees_pidm = pidm_in
         AND sftfees_term_cde = term_in
         AND sftfees_rsts_cde IN
             (SELECT stvrsts_code
                FROM stvrsts
               WHERE stvrsts_incl_assess = 'Y'
                 AND stvrsts_withdraw_ind = 'Y')
         AND ((pstart_in IS NOT NULL AND
             TRUNC(sftfees_rsts_date) BETWEEN pstart_in AND pend_in));

    CURSOR swap_sfrfaud_c IS
      SELECT tbbdetc_dcat_code,
             sfrfaud_detl_code,
             NVL(SUM(sfrfaud_charge), 0),
             DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', sfrfaud_crn, NULL)
        FROM tbbdetc, sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_accd_tran_number IS NULL -- only want new/unposted
         AND sfrfaud_assess_rfnd_penlty_cde = 'S' -- swap liability
         AND tbbdetc_detail_code = sfrfaud_detl_code
         AND tbbdetc_dcat_code IN ('TUI', 'FEE')
       GROUP BY tbbdetc_dcat_code,
                sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       sfrfaud_crn,
                       NULL),
                tbbdetc_refundable_ind,
                tbbdetc_type_ind
       ORDER BY tbbdetc_dcat_code,
                sfrfaud_detl_code,
                DECODE(sobterm_rec.sobterm_bycrn_ind,
                       'Y',
                       sfrfaud_crn,
                       NULL),
                tbbdetc_type_ind;

    CURSOR curr_sfrfaud_c IS
      SELECT NVL(SUM(sfrfaud_charge), 0)
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_assess_rfnd_penlty_cde NOT IN ('P', 'S') -- penalties, swap liability
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = swap_detl_code
         AND NVL(DECODE(sobterm_rec.sobterm_bycrn_ind,
                        'Y',
                        sfrfaud_crn,
                        NULL),
                 'x') =
             NVL(DECODE(sobterm_rec.sobterm_bycrn_ind, 'Y', swap_crn, NULL),
                 'x');

    CURSOR sfrfaud_rowid_c IS
      SELECT ROWID
        FROM sfrfaud
       WHERE sfrfaud_pidm = pidm_in
         AND sfrfaud_term_code = term_in
         AND sfrfaud_activity_date = save_activity_date
         AND sfrfaud_sessionid = save_sessionid
         AND sfrfaud_detl_code IS NOT NULL -- filter out info records
         AND sfrfaud_detl_code = NVL(penalty_detl_code, swap_detl_code)
         AND sfrfaud_charge = penalty_amount
         AND sfrfaud_accd_tran_number IS NULL; -- only want new/unposted

    sftfees_rec sftfees_drops_c%ROWTYPE;
  BEGIN
    p_print_dbms('Start p_calc_swap_liability, phrs in=' || phrs_in);
    swap_assess_rfnd_cde := 'S'; -- signify swapping audit
    balhrs               := phrs_in;

    -- set drop(s) to have 100% liability for penalty hours
    FOR sftfees_rec IN sftfees_drops_c LOOP
      tothrs := tothrs + sftfees_rec.sftfees_reg_bill_hr;

      IF tothrs <= phrs_in THEN
        UPDATE sftfees
           SET sftfees_tuit_liab_percentage = 1,
               sftfees_fees_liab_percentage = 1
         WHERE sftfees.ROWID = sftfees_rec.ROWID;
        balhrs := balhrs - sftfees_rec.sftfees_reg_bill_hr;
      ELSE
        -- store balance of penalty hours
        UPDATE sftfees
           SET sftfees_tuit_liab_percentage = 1,
               sftfees_fees_liab_percentage = 1,
               sftfees_bill_hr_tuit         = balhrs,
               sftfees_bill_hr_fees         = balhrs,
               sftfees_waiv_hr_tuit         = balhrs,
               sftfees_waiv_hr_fees         = balhrs
         WHERE sftfees.ROWID = sftfees_rec.ROWID;
        EXIT;
      END IF;
    END LOOP;

    --Calculate section fees for the course and insert SFRFAUD record.
    /*  p_sectionfees( pidm_in, term_in, source_pgm_in ); */
    p_print_dbms('In swapping, before pstudent fees');
    if term_in <> nvl(save_term, '@') OR gv_rgfe_student_read is null then
      IF (f_rule_type_defined(term_in, STUDENT_TYPE)) THEN
        gv_rgfe_student_exists := 'Y';
      else
        gv_rgfe_student_exists := 'N';
      end if;
      gv_rgfe_student_read := 'Y';
    end if;
    p_calcliability(pidm_in, term_in, source_pgm_in, STUDENT_TYPE);

    /* ********************************************************* */
    /* To improve performance, determine if course-related rule  */
    /* types are defined in SFRRGFE for the term and type before */
    /* invoking the course processing using the temporary table. */
    /* ********************************************************* */
    p_print_dbms('In swapping, before p level fees');
    if term_in <> nvl(save_term, '@') OR gv_rgfe_level_read is null then
      IF (f_rule_type_defined(term_in, LEVEL_TYPE)) THEN
        gv_rgfe_level_exists := 'Y';
      else
        gv_rgfe_level_exists := 'N';
      end if;
      gv_rgfe_level_read := 'Y';
    end if;
    if gv_rgfe_level_exists = 'Y' then
      p_print_dbms('Begin to assess for level rules');
      p_calcliability(pidm_in, term_in, source_pgm_in, LEVEL_TYPE);
    END IF;
    p_print_dbms('In swapping, before campus fees');
    if term_in <> nvl(save_term, '@') OR gv_rgfe_campus_read is null then
      IF (f_rule_type_defined(term_in, CAMPUS_TYPE)) THEN
        gv_rgfe_campus_exists := 'Y';
      else
        gv_rgfe_campus_exists := 'N';
      end if;
      gv_rgfe_campus_read := 'Y';
    end if;
    if gv_rgfe_campus_exists = 'Y' then
      p_print_dbms('Begin to assess for campus rules');
      p_calcliability(pidm_in, term_in, source_pgm_in, CAMPUS_TYPE);
    END IF;
    p_print_dbms('In swapping, before attr fees');
    if term_in <> nvl(save_term, '@') OR gv_rgfe_attr_read is null then
      IF (f_rule_type_defined(term_in, ATTR_TYPE)) THEN
        gv_rgfe_attr_exists := 'Y';
      else
        gv_rgfe_attr_exists := 'N';
      end if;
      gv_rgfe_attr_read := 'Y';
    end if;
    if gv_rgfe_attr_exists = 'Y' then
      p_print_dbms('Begin to assess for attr rules');
      p_calcliability(pidm_in, term_in, source_pgm_in, ATTR_TYPE);
    END IF;

    p_print_dbms('In swapping, before additional fees');
    p_additionalfees(pidm_in, term_in, source_pgm_in);
    save_term            := term_in;
    swap_assess_rfnd_cde := ''; -- reset

    -- Post a penalty based on the penalty hours.
    -- Deduct the original liability from the swap liability
    -- to get the amount to penalize.
    OPEN swap_sfrfaud_c;
    LOOP
      FETCH swap_sfrfaud_c
        INTO swap_dcat_code, swap_detl_code, swap_assess_amount, swap_crn;
      p_print_dbms('inside swap sfrfaud c cursor : ' || swap_crn);
      EXIT WHEN swap_sfrfaud_c%NOTFOUND;

      OPEN curr_sfrfaud_c;
      FETCH curr_sfrfaud_c
        INTO curr_assess_amount;
      CLOSE curr_sfrfaud_c;

      amount_to_penalize := swap_assess_amount - curr_assess_amount;
      p_print_dbms('amt to penalize : ' || amount_to_penalize);
      p_print_dbms('swap_assess_amount - curr_assess_amount: ' ||
                   swap_assess_amount || ':' || curr_assess_amount);

      IF amount_to_penalize > 0 THEN
        IF swap_dcat_code = 'TUI' THEN
          penalty_amount  := amount_to_penalize * rbt_tuit_penalty_percent;
          penalty_percent := rbt_tuit_penalty_percent * 100;

          p_print_dbms('amount_to_penalize * rbt_tuit_penalty_percent: ' || ':' ||
                       amount_to_penalize || ':' ||
                       rbt_tuit_penalty_percent);

          -- start generation of audit note
          note := g$_nls.get('SFKFEE1-0028',
                             'SQL',
                             'SWAP:  %01%% %02% penalty on %03% difference;',
                             penalty_percent,
                             swap_dcat_code,
                             amount_to_penalize);

          IF rbt_clr_tuit_detl_code IS NOT NULL THEN
            penalty_detl_code := rbt_clr_tuit_detl_code;
            note              := g$_nls.get('SFKFEE1-0029',
                                            'SQL',
                                            '%01% post to clearning account.',
                                            note);
          ELSE
            note := g$_nls.get('SFKFEE1-0030',
                               'SQL',
                               '%01% post to orig detail code.',
                               note);
          END IF;
        ELSE
          -- FEE
          penalty_amount  := amount_to_penalize * rbt_fees_penalty_percent;
          penalty_percent := rbt_fees_penalty_percent * 100;

          -- start generation of audit note
          note := g$_nls.get('SFKFEE1-0031',
                             'SQL',
                             'SWAP:  %01%% %02% penalty on %03% difference;',
                             penalty_percent,
                             swap_dcat_code,
                             amount_to_penalize);

          IF rbt_clr_fees_detl_code IS NOT NULL THEN
            penalty_detl_code := rbt_clr_fees_detl_code;
            note              := g$_nls.get('SFKFEE1-0032',
                                            'SQL',
                                            '%01% post to clearning account.',
                                            note);
          ELSE
            note := g$_nls.get('SFKFEE1-0033',
                               'SQL',
                               '%01% post to orig detail code.',
                               note);
          END IF;
        END IF;
        p_print_dbms('note from building swap: ' || note);
        IF NOT (f_tbraccd_exists(pidm_in,
                                 term_in,
                                 swap_detl_code,
                                 penalty_amount)) THEN
          accd_tran_number := 0;
          p_print_dbms('insert tbraccd9: ' || penalty_detl_code || 'swap' ||
                       swap_detl_code || ':' || penalty_amount);
          p_insert_tbraccd_rec(pidm_in,
                               accd_tran_number,
                               term_in,
                               NVL(penalty_detl_code, swap_detl_code),
                               penalty_amount,
                               penalty_amount,
                               rbt_rfnd_eff_date,
                               rbt_srce_code,
                               NULL, -- no CRN for penalty
                               assess_eff_date,
                               'Y');
        END IF;

        -- record the penalty in the audit trail
        next_seqno := ME_SFKFEES.f_get_sfrfaud_seqno(pidm_in,
                                                     term_in,
                                                     save_sessionid);

        --Defect 105101, for some reason TRANSLATE() is failing
        IF (accd_tran_number = 0) THEN
          accd_tran_number := NULL;
        END IF;

        p_print_dbms('p_insert_sfrfaud 15: ' || next_seqno);

        p_insert_sfrfaud(pidm_in,
                         term_in,
                         next_seqno,
                         'P', -- penalty
                         swap_dcat_code,
                         NVL(penalty_detl_code, swap_detl_code),
                         penalty_amount,
                         'N', -- ABC: default to 'N'
                         source_pgm_in,
                         NULL, -- N/A: no rule seqno
                         NULL, -- N/A: no rule type
                         swap_crn,
                         NULL, -- N/A: no rsts_code
                         NULL, -- N/A: no rsts_date
                         NULL, -- N/A: no ests_code
                         NULL, -- N/A: no ests_date
                         crn_rfnd_table,
                         NULL, -- N/A: no hours
                         NULL, -- N/A: no waiv hours
                         NULL, -- N/A: no rule cred hrs
                         NULL, -- N/A: no rule stud hrs
                         NULL, -- N/A: no per cred charge
                         NULL, -- N/A: no flat fee amt
                         NULL, -- N/A: no overload hrs
                         NULL, -- N/A: no crse overload charge
                         --TRANSLATE(accd_tran_number,0,NULL),  -- TBRACCD tran number;
                         accd_tran_number,
                         note,
                         NULL, -- no liable credit hours
                         -- RPE 35999: following added to store rule calc values
                         NULL, -- rule per credit charge
                         NULL, -- rule flat fee amount
                         NULL, -- rule from flat hrs
                         NULL, -- rule to flat hrs
                         NULL); -- rule course OL start hr
        -- back post accd_tran_number to qualified SFRFAUD records by rowid

        IF (ASSESSMENT_POSTED) AND accd_tran_number <> 0 THEN
          FOR sfrfaud_rowid_rec IN sfrfaud_rowid_c LOOP
            UPDATE sfrfaud
               SET sfrfaud_accd_tran_number = accd_tran_number
             WHERE rowid = sfrfaud_rowid_rec.rowid;
          END LOOP;
        END IF;

        penalty_amount := 0; -- re-init
      END IF;
    END LOOP;
    CLOSE swap_sfrfaud_c;
    p_print_dbms('End p_calc_swap_liability');
  END p_calc_swap_liability;

  /* ******************************************************************* */
  /* 104667, new procedure                                               */
  /* ******************************************************************* */
  /* Non Swapping.                                                       */
  /* ******************************************************************* */
  /* Procedure to determine the amount for penalty reversal in a RBT and */
  /* Non-SWAPPING.                                                       */
  /* ******************************************************************* */
  PROCEDURE p_rev_nonswap_RBT(pidm_in IN spriden.spriden_pidm%TYPE,
                              term_in IN stvterm.stvterm_code%TYPE) IS
    lv_pstart      DATE;
    lv_pend        DATE;
    lv_psrce       VARCHAR2(1);
    lv_pamount     tbraccd.tbraccd_amount%TYPE := 0;
    lv_pdetl       tbraccd.tbraccd_detail_code%TYPE := 0;
    lv_tran_number tbraccd.tbraccd_tran_number%TYPE := 0;

    CURSOR penalty_amount_c IS
      SELECT NVL(SUM(tbraccd_amount), 0),
             tbraccd_detail_code,
             tbraccd_srce_code
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code in
             ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
       GROUP BY tbraccd_detail_code, tbraccd_srce_code
       ORDER by tbraccd_detail_code, tbraccd_srce_code;
    --- 7.4.0.4 new cursor to find amt to rev for dd
    CURSOR penalty_amount_dd_c IS
      SELECT NVL(SUM(tbraccd_amount), 0),
             tbraccd_detail_code,
             tbraccd_srce_code
        FROM tbraccd
       WHERE tbraccd_pidm = pidm_in
         AND tbraccd_term_code = term_in
         AND tbraccd_srce_code = rbt_srce_code
       GROUP BY tbraccd_detail_code, tbraccd_srce_code
       ORDER by tbraccd_detail_code, tbraccd_srce_code;

  BEGIN
    p_print_dbms('Start p_rev_nonswap_RBT');
    -- check for penalty for the source, reverse if found
    if (rbt_wd_exists) then
      OPEN penalty_amount_c;
      LOOP
        FETCH penalty_amount_c
          INTO lv_pamount, lv_pdetl, lv_psrce;
        IF penalty_amount_c%NOTFOUND THEN
          EXIT;
        ELSIF lv_pamount > 0 THEN
          lv_pamount := lv_pamount * -1; -- generate a credit
          p_print_dbms('How much is being reversed for WD: ' || lv_pamount ||
                       ' source: ' || lv_psrce);
          lv_tran_number := 0;
          p_print_dbms('insert tbraccd10 for WD: ' || lv_pdetl || ':' ||
                       lv_pamount);

          p_insert_tbraccd_rec(pidm_in,
                               lv_tran_number,
                               term_in,
                               lv_pdetl,
                               lv_pamount,
                               lv_pamount,
                               rbt_rfnd_eff_date,
                               lv_psrce,
                               NULL, -- no CRN for penalty
                               assess_eff_date,
                               'Y');
        END IF;
      END LOOP;
      CLOSE penalty_amount_c;
    else
      if (rbt_dd_exists) then
        p_print_dbms('rbt dd exists, in p_rev_nonswap rbt, ' ||
                     rbt_srce_code);
        OPEN penalty_amount_dd_c;
        LOOP
          FETCH penalty_amount_dd_c
            INTO lv_pamount, lv_pdetl, lv_psrce;
          IF penalty_amount_dd_c%NOTFOUND THEN
            p_print_dbms('penalty amt for dd not found');
            EXIT;
          ELSIF lv_pamount > 0 THEN
            lv_pamount := lv_pamount * -1; -- generate a credit
            p_print_dbms('How much is being reversed for DD: ' ||
                         lv_pamount || ' source: ' || lv_psrce);
            lv_tran_number := 0;
            p_print_dbms('insert tbraccd10 for dd: ' || lv_pdetl || ':' ||
                         lv_pamount);

            p_insert_tbraccd_rec(pidm_in,
                                 lv_tran_number,
                                 term_in,
                                 lv_pdetl,
                                 lv_pamount,
                                 lv_pamount,
                                 rbt_rfnd_eff_date,
                                 lv_psrce,
                                 NULL, -- no CRN for penalty
                                 assess_eff_date,
                                 'Y');
          END IF;
        END LOOP;
        CLOSE penalty_amount_dd_c;

      end if;
    end if;
    p_print_dbms('End p_rev_nonswap_RBT');

  END p_rev_nonswap_RBT;

  /* ******************************************************************* */
  /* Procedure to print debug messages                                   */
  /* ******************************************************************* */
  PROCEDURE p_print_dbms(dbms_msg_in IN VARCHAR2) IS
  BEGIN
    -- execute with this statement gb_common.p_set_context('SFKFEES','PRINT', 'Y,'N');
    --  proceeding the sfkfees.p_processfeeassessment to get all dbms messages to display

    gv_print := 'Y'; --defaulting for Mobedu

    IF gv_print = 'N' THEN
      RETURN;
    END IF;

    IF gv_print = 'x' THEN
      gv_print := NVL(gb_common.f_get_context('SFKFEES', 'PRINT'), 'N');
    END IF;

    IF gv_print = 'Y' THEN
      dbms_output.put_line(dbms_msg_in);
    END IF;

  END p_print_dbms;

END ME_SFKFEES;
/

