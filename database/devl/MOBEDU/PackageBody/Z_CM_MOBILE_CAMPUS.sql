PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.z_cm_mobile_campus
CREATE OR REPLACE package body mobedu.z_cm_mobile_campus as

  function FZ_GET_SPRADDR_ROWID(PIDM     number,
                                ADDRESS1 varchar2 default 'BU',
                                ADDRESS2 varchar2 default 'BI',
                                ADDRESS3 varchar2 default 'MA')
    return varchar2 as
    ADDRESS_ROW1 varchar2(1000);
    ADDRESS_ROW2 varchar2(1000);
    ADDRESS_ROW3 varchar2(1000);

    cursor GET_ADDR_TYPE(ADDR varchar2) is
      select A.ROWID
        from SATURN.SPRADDR A
       where A.SPRADDR_ATYP_CODE = ADDR
         and A.SPRADDR_PIDM = PIDM
         and A.SPRADDR_STATUS_IND is null
         and (A.SPRADDR_TO_DATE is null or A.SPRADDR_TO_DATE >= sysdate)
         and (A.SPRADDR_SEQNO) =
             (select max(B.SPRADDR_SEQNO)
                from SATURN.SPRADDR B
               where A.SPRADDR_PIDM = B.SPRADDR_PIDM
                 and NVL(B.SPRADDR_ATYP_CODE, '0') =
                     NVL(A.SPRADDR_ATYP_CODE, '0')
                 and B.SPRADDR_STATUS_IND is null
                 and (B.SPRADDR_TO_DATE is null or
                     B.SPRADDR_TO_DATE >= sysdate));
  begin

    for ADR_TYP in GET_ADDR_TYPE(ADDRESS1) loop
      select ADR_TYP.ROWID into ADDRESS_ROW1 from DUAL;
    END LOOP;
    for ADR_TYP in GET_ADDR_TYPE(ADDRESS2) loop
      select ADR_TYP.ROWID into ADDRESS_ROW2 from DUAL;
    END LOOP;
    for ADR_TYP in GET_ADDR_TYPE(ADDRESS3) loop
      select ADR_TYP.ROWID into ADDRESS_ROW3 from DUAL;
    END LOOP;

    if ADDRESS_ROW1 is not null then
      return ADDRESS_ROW1;
    ELSIF ADDRESS_ROW2 is not null then
      return ADDRESS_ROW2;
    ELSIF ADDRESS_ROW3 is not null then
      return ADDRESS_ROW3;
    ELSE
      RETURN NULL;

    END IF;
  end FZ_GET_SPRADDR_ROWID;

  function FZ_GET_SPRTELE_ROWID(PIDM     number,
                                ADDRESS1 varchar2 default 'BU',
                                ADDRESS2 varchar2 default 'BI',
                                ADDRESS3 varchar2 default 'MA')
    return varchar2 as
    TELE_ROW1 varchar2(1000);
    TELE_ROW2 varchar2(1000);
    TELE_ROW3 varchar2(1000);

    cursor GET_PHONE_TYPE(ADDR varchar2) is
      SELECT A.ROWID
        FROM SATURN.SPRTELE A
       WHERE A.SPRTELE_TELE_CODE = ADDR
         AND A.SPRTELE_PIDM = PIDM
         AND A.SPRTELE_STATUS_IND IS NULL
         AND A.SPRTELE_SEQNO =
             (SELECT MAX(B.SPRTELE_SEQNO)
                FROM SATURN.SPRTELE B
               WHERE A.SPRTELE_PIDM = B.SPRTELE_PIDM
                 AND B.SPRTELE_TELE_CODE = A.SPRTELE_TELE_CODE
                 AND NVL(B.SPRTELE_ATYP_CODE, '0') =
                     NVL(A.SPRTELE_ATYP_CODE, '0'));
  begin
    for ADR_TYP in GET_PHONE_TYPE(ADDRESS1) loop
      select ADR_TYP.ROWID into TELE_ROW1 from DUAL;
    END LOOP;
    for ADR_TYP in GET_PHONE_TYPE(ADDRESS2) loop
      select ADR_TYP.ROWID into TELE_ROW2 from DUAL;
    END LOOP;
    for ADR_TYP in GET_PHONE_TYPE(ADDRESS3) loop
      select ADR_TYP.ROWID into TELE_ROW3 from DUAL;
    END LOOP;

    if TELE_ROW1 is not null then
      return TELE_ROW1;
    ELSIF TELE_ROW2 is not null then
      return TELE_ROW2;
    ELSIF TELE_ROW3 is not null then
      return TELE_ROW3;
    ELSE
      RETURN NULL;

    END IF;
  end FZ_GET_SPRTELE_ROWID;

  function FZ_GET_GOREMAL_ROWID(PIDM     number,
                                ADDRESS1 varchar2 default 'BU',
                                ADDRESS2 varchar2 default 'BI',
                                ADDRESS3 varchar2 default 'MA')
    return varchar2 as
    EMAIL_ROW1 varchar2(1000);
    EMAIL_ROW2 varchar2(1000);
    EMAIL_ROW3 varchar2(1000);

    cursor GET_EMAIL_TYPE(ADDR varchar2) is
      select M.ROWID
        from goremal m
        LEFT JOIN GOREMAL M1
          ON M1.GOREMAL_PIDM = m.goremal_pidm
         and m.goremal_emal_code = m1.goremal_emal_code
         and m1.goremal_preferred_ind = 'Y'
       where m.goremal_pidm = pidm
         AND M.GOREMAL_EMAL_CODE = ADDR
         and nvl(m.goremal_status_ind, 'A') = 'A'
         AND ((m.goremal_preferred_ind = 'Y') OR
             (ROWNUM < 2 AND NVL(M1.GOREMAL_PREFERRED_IND, 'N') <> 'Y'))
       order by m.goremal_activity_date desc;

  begin
    for ADR_TYP in GET_EMAIL_TYPE(ADDRESS1) loop
      select ADR_TYP.ROWID into EMAIL_ROW1 from DUAL;
    END LOOP;
    for ADR_TYP in GET_EMAIL_TYPE(ADDRESS2) loop
      select ADR_TYP.ROWID into EMAIL_ROW2 from DUAL;
    END LOOP;
    for ADR_TYP in GET_EMAIL_TYPE(ADDRESS3) loop
      select ADR_TYP.ROWID into EMAIL_ROW3 from DUAL;
    END LOOP;

    if EMAIL_ROW1 is not null then
      return EMAIL_ROW1;
    ELSIF EMAIL_ROW2 is not null then
      return EMAIL_ROW2;
    ELSIF EMAIL_ROW3 is not null then
      return EMAIL_ROW3;
    ELSE
      RETURN NULL;

    END IF;
  end FZ_GET_GOREMAL_ROWID;

  FUNCTION FZ_CATEGORY(P_PIDM NUMBER)RETURN VARCHAR2 IS
    V_FAC NUMBER:=0;
    V_EMP NUMBER:=0;
    V_STU NUMBER:=0;
  BEGIN
    SELECT COUNT(*) INTO V_FAC FROM SIBINST A WHERE A.SIBINST_PIDM=P_PIDM AND EXISTS (SELECT 1 FROM STVFCST F
    WHERE F.STVFCST_CODE=A.SIBINST_FCST_CODE AND F.STVFCST_ACTIVE_IND<>'I');
    IF V_FAC <> 0 THEN
      RETURN 'FACULTY';
    END IF;
    SELECT COUNT(*) INTO V_EMP FROM PEBEMPL P WHERE P.PEBEMPL_PIDM=P_PIDM;
    IF V_EMP <> 0 THEN
      RETURN 'EMPLOYEE';
    END IF;
    SELECT COUNT(*) INTO V_STU FROM SGBSTDN S WHERE S.SGBSTDN_PIDM=P_PIDM;
    IF V_STU <> 0 THEN
      RETURN 'STUDENT';
    END IF;
    RETURN 'OTHER';
   EXCEPTION WHEN OTHERS THEN
     RETURN NULL;
  END FZ_CATEGORY;
  FUNCTION FZ_GET_STATE(P_STATE VARCHAR2) RETURN VARCHAR2 IS
    V_STATE VARCHAR2(200);
  BEGIN
    SELECT A.STVSTAT_DESC INTO V_STATE FROM STVSTAT A WHERE A.STVSTAT_CODE=P_STATE;
    RETURN V_STATE;
    EXCEPTION WHEN OTHERS THEN
      RETURN NULL;
  END FZ_GET_STATE;
end Z_CM_MOBILE_CAMPUS;
/

