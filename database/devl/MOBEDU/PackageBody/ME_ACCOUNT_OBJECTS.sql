PROMPT CREATE OR REPLACE PACKAGE BODY mobedu.me_account_objects
CREATE OR REPLACE PACKAGE BODY me_account_objects is

  tbbterm_rec  tbbterm%ROWTYPE;
  sobterm_rec  sobterm%ROWTYPE;
  tbbctrl_rec  tbbctrl%ROWTYPE;
  sessionid_in tbrclct.tbrclct_sessionid%TYPE;

  function fz_future_term_charges(p_pidm spriden.spriden_pidm%type)
    return number is
    result number;
  begin
    begin
      select SUM(decode(C.tbbdetc_type_ind, 'P', -1, 'C', 1) *
                 A.TBRACCD_AMOUNT) OVER(PARTITION BY A.TBRACCD_PIDM)
        into result
        from tbraccd a
        join tbbdetc c
          on c.tbbdetc_detail_code = a.tbraccd_detail_code
         and a.tbraccd_pidm = p_pidm
        join stvterm b
          on b.stvterm_code = a.tbraccd_term_code
         and b.stvterm_start_date > sysdate;
    exception
      when others then
        result := 0;
    end;
    return result;
  end fz_future_term_charges;

  function fz_get_acc_balance(stu_pidm in number) return number is
    Acc_Bal number;
  begin
    SELECT nvl(SUM(DECODE(TBBDETC_TYPE_IND,
                          'C',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)) -
               SUM(DECODE(TBBDETC_TYPE_IND,
                          'P',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)),
               0)
      INTO Acc_Bal
      FROM TBBDETC, TBRACCD
     WHERE TBBDETC_DETAIL_CODE = TBRACCD_DETAIL_CODE
       AND TBRACCD_PIDM = stu_pidm
     GROUP BY TBRACCD_PIDM;
    return(Acc_Bal);
  end fz_get_acc_balance;

  function fz_get_tot_due_bal(stu_pidm in number) return number is
    DueBal number;
  begin
    select nvl(SUM(A.TBRACCD_BALANCE), 0)
      INTO DueBal
      from tbraccd a
     where a.tbraccd_pidm = stu_pidm
       and trunc(a.tbraccd_effective_date) <= trunc(sysdate)
          --         and a.tbraccd_due_date < trunc(sysdate)
       and a.tbraccd_balance > 0;
    return(DueBal);
  end FZ_GET_TOT_DUE_BAL;

  function fz_get_currtrm_acc_bal(stu_pidm in number) return number is
    Acc_Bal number;
  begin
    SELECT nvl(SUM(DECODE(TBBDETC_TYPE_IND,
                          'C',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)) -
               SUM(DECODE(TBBDETC_TYPE_IND,
                          'P',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)),
               0)
      INTO Acc_Bal
      FROM TBBDETC a
      join TBRACCD b
        on b.tbraccd_detail_code = a.tbbdetc_detail_code
       AND TBRACCD_PIDM = stu_pidm
       and b.tbraccd_term_code IN
           (select t.stvterm_code
              from stvterm t
             where sysdate between t.stvterm_start_date and
                   t.stvterm_end_date
               and t.stvterm_code not like '0%')
    
     GROUP BY TBRACCD_PIDM;
    return(Acc_Bal);
  end fz_get_currtrm_acc_bal;

  function fz_get_prevtrm_acc_bal(stu_pidm in number) return number is
    Acc_Bal number;
  begin
    SELECT nvl(SUM(DECODE(TBBDETC_TYPE_IND,
                          'C',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)) -
               SUM(DECODE(TBBDETC_TYPE_IND,
                          'P',
                          TBRACCD_AMOUNT,
                          TBRACCD_AMOUNT)),
               0)
      INTO Acc_Bal
      FROM TBBDETC a
      join TBRACCD b
        on b.tbraccd_detail_code = a.tbbdetc_detail_code
       AND TBRACCD_PIDM = stu_pidm
       and b.tbraccd_term_code <
           (select t.stvterm_code
              from stvterm t
             where sysdate between t.stvterm_start_date and
                   t.stvterm_end_date
               and t.stvterm_code not like '0%'
               AND T.STVTERM_CODE NOT LIKE '9%')
    
     GROUP BY TBRACCD_PIDM;
    return(Acc_Bal);
  end fz_get_prevtrm_acc_bal;

  FUNCTION fz_get_hold_flag(p_id        IN spriden.spriden_id%TYPE,
                            p_hold_type VARCHAR2) RETURN VARCHAR2 IS
  
    RESULT VARCHAR2(300) := 'N';
    CURSOR c_hold_info IS
    
      SELECT X.id,
             x.pidm,
             x.hold_code,
             x.hold_description,
             x.hold_from_date,
             x.hold_to_date,
             SUM(X.REGISTRATION_HOLDS) over(partition by id) REGISTRATION_HOLDS,
             SUM(X.transcript_holds) over(partition by id) transcript_holds,
             SUM(X.Graduation_holds) over(partition by id) Graduation_holds,
             SUM(X.Financial_holds) over(partition by id) Financial_holds,
             SUM(X.Enrollment_Verification_holds) over(partition by id) Enrollment_Verification_holds,
             SUM(X.Grade_Release_holds) over(partition by id) Grade_Release_holds
        FROM (SELECT DISTINCT b.spriden_id id,
                              sprhold_pidm pidm,
                              a.sprhold_hldd_code hold_code,
                              ME_REG_UTILS.fz_get_desc(a.sprhold_hldd_code,
                                                       'STVHLDD',
                                                       '') Hold_description,
                              a.sprhold_from_date Hold_From_Date,
                              a.sprhold_to_date Hold_To_Date,
                              count(d.stvhldd_reg_hold_ind) over(PARTITION BY sprhold_pidm, d.stvhldd_reg_hold_ind) registration_holds,
                              COUNT(e.stvhldd_trans_hold_ind) over(PARTITION BY sprhold_pidm, e.stvhldd_trans_hold_ind) transcript_holds,
                              COUNT(f.stvhldd_grad_hold_ind) over(PARTITION BY sprhold_pidm, f.stvhldd_grad_hold_ind) Graduation_holds,
                              COUNT(g.stvhldd_grade_hold_ind) over(PARTITION BY sprhold_pidm, g.stvhldd_grade_hold_ind) Grade_Release_holds,
                              COUNT(decode(i.stvhldd_code,
                                           'F0',
                                           NULL,
                                           i.stvhldd_code)) over(PARTITION BY sprhold_pidm, i.stvhldd_code) Financial_holds,
                              COUNT(h.stvhldd_env_hold_ind) over(PARTITION BY sprhold_pidm, h.stvhldd_env_hold_ind) Enrollment_verification_holds
                FROM sprhold a
                JOIN spriden b
                  ON b.spriden_pidm = a.sprhold_pidm
                 AND b.spriden_change_ind IS NULL
                LEFT OUTER JOIN stvhldd d
                  ON d.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(a.sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND d.stvhldd_reg_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd e
                  ON e.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(a.sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND e.stvhldd_trans_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd f
                  ON f.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(a.sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND f.stvhldd_grad_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd g
                  ON g.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND g.stvhldd_grade_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd h
                  ON h.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND h.stvhldd_env_hold_ind = 'Y'
                LEFT OUTER JOIN stvhldd i
                  ON i.stvhldd_code = a.sprhold_hldd_code
                 AND nvl(sprhold_to_date, SYSDATE + 1) > SYSDATE
                 AND i.stvhldd_code LIKE 'F%'
                 AND i.stvhldd_code <> 'F0'
               WHERE spriden_id = p_id
                 AND b.spriden_change_ind IS NULL) X;
  
  BEGIN
    FOR R_HOLD_INFO IN c_hold_info LOOP
      --     dbms_output.put_line('Steve: ' || r_hold_info.registration_holds);
      IF r_hold_info.registration_holds >= 1 AND
         p_hold_type = 'REGISTRATION_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.transcript_holds >= 1 AND
            p_hold_type = 'TRANSCRIPT_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.graduation_holds >= 1 AND
            p_hold_type = 'GRADUATION_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.grade_release_holds >= 1 AND
            p_hold_type = 'GRADE_RELEASE_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.financial_holds >= 1 AND
            p_hold_type = 'FINANCIAL_HOLDS' THEN
        RESULT := 'Y';
      ELSIF r_hold_info.enrollment_verification_holds >= 1 AND
            p_hold_type = 'ENROLLMENT_VERIFICATION' THEN
        RESULT := 'Y';
      ELSE
        RESULT := 'N';
        --        dbms_output.put_line('Steve: returning N');
      END IF;
    
    END LOOP;
  
    RETURN RESULT;
  
  END fz_get_hold_flag;

  function fz_unbilled_charges(p_pidm spriden.spriden_pidm%type)
    return number is
    result number;
  begin
    begin
      select SUM(decode(C.tbbdetc_type_ind, 'P', -1, 'C', 1) *
                 A.TBRACCD_AMOUNT) OVER(PARTITION BY A.TBRACCD_PIDM)
        into result
        from tbraccd a
        join tbbdetc c
          on c.tbbdetc_detail_code = a.tbraccd_detail_code
         and a.tbraccd_pidm = p_pidm
         and a.tbraccd_bill_date is null;
    
    exception
      when others then
        result := 0;
    end;
    return result;
  end fz_unbilled_charges;

  /**
  * Function to get net past due based on pidm
  * @return                    Past Due Amount
  * @param p_pidm              Internal identification number p_pidm
  */
  function fz_net_past_due(p_pidm spriden.spriden_pidm%type) return number is
    result number;
  begin
    begin
      select nvl(SUM(A.TBRACCD_BALANCE), 0)
        into result
        from tbraccd a
       where a.tbraccd_pidm = p_pidm
         and trunc(a.tbraccd_effective_date) <= trunc(sysdate)
         and a.tbraccd_due_date < trunc(sysdate)
         and a.tbraccd_balance > 0;
    
    exception
      when others then
        result := 0;
    end;
    return result;
  end fz_net_past_due;

  FUNCTION fz_check_tiv_status(par_pidm IN VARCHAR2) RETURN VARCHAR2 IS
    /*
    check the student's current status for given pidm
    if has authorize record --A
    if has restrict record -- R
    if no record found --N
    return combination studus of TIV and PY records
    */
    v_status_tiv  VARCHAR2(1);
    v_status_py   VARCHAR2(1);
    v_return_val  VARCHAR2(100);
    v_rec_cnt_tiv integer;
    v_rec_cnt_py  integer;
  BEGIN
  
    SELECT count(*)
      into v_rec_cnt_tiv
      FROM tvrauth a
     WHERE a.tvrauth_pidm = par_pidm
       AND tvrauth_type_code = 'TIV'
       AND sysdate between TVRAUTH_START_DATE and TVRAUTH_END_DATE;
  
    SELECT count(*)
      into v_rec_cnt_py
      FROM tvrauth a
     WHERE a.tvrauth_pidm = par_pidm
       AND tvrauth_type_code = 'PY'
       AND sysdate between TVRAUTH_START_DATE and TVRAUTH_END_DATE;
  
    -- select the most resent effective record for the user with type 'TIV'
    if v_rec_cnt_tiv > 0 then
      SELECT TVRAUTH_STATUS
        into v_status_tiv
        FROM tvrauth
       WHERE tvrauth_pidm = par_pidm
         AND tvrauth_type_code = 'TIV'
         AND sysdate between TVRAUTH_START_DATE and TVRAUTH_END_DATE
         AND tvrauth_start_date =
             (SELECT MAX(a.tvrauth_start_date)
                FROM tvrauth a
               WHERE a.tvrauth_pidm = par_pidm
                 AND a.tvrauth_type_code = 'TIV');
    end if;
    -- select the most resent effective record for the user with type 'PY
    if v_rec_cnt_py > 0 then
      SELECT TVRAUTH_STATUS
        into v_status_py
        FROM tvrauth
       WHERE tvrauth_pidm = par_pidm
         AND tvrauth_type_code = 'PY'
         AND sysdate between TVRAUTH_START_DATE and TVRAUTH_END_DATE
         AND tvrauth_start_date =
             (SELECT MAX(a.tvrauth_start_date)
                FROM tvrauth a
               WHERE a.tvrauth_pidm = par_pidm
                 AND a.tvrauth_type_code = 'PY');
    end if;
    v_status_tiv := nvl(v_status_tiv, 'N');
    v_status_py  := nvl(v_status_py, 'N');
    v_return_val := v_status_tiv || v_status_py;
  
    if (v_return_val in ('RR', 'NN', 'RN', 'NR')) then
      v_return_val := 'Restricted';
    else
      if (v_return_val = 'AA') then
      
        v_return_val := 'Authorized';
      
      else
        if (v_return_val in ('AR', 'AN')) then
          v_return_val := 'AuthorizedCurrent';
        Else
          v_return_val := 'ERROR';
        end if;
      
      end if;
    end if;
  
    RETURN v_return_val;
  END;

  FUNCTION fz_get_detail_desc(Input_det_code IN VARCHAR2) RETURN VARCHAR2 IS
    v_description VARCHAR(999);
  
  BEGIN
    SELECT CODE_DESC
      into v_description
      FROM ME_DETAIL_CODE_DESC_VW
     WHERE DETAIL_CODE = Input_det_code;
  
    RETURN v_description;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 'No information about this code is available';
    
  END;

  function fz_get_detail_link(P_DETAIL_CODE IN varchar2) RETURN VARCHAR2 IS
  
    v_link varchar2(3000);
  
    CURSOR get_link is
      select * from mobile_invoice_links where detail_code = P_DETAIL_CODE;
  
  BEGIN
  
    FOR r_data in get_link loop
    
      v_link := v_link || '<a href=' || r_data.url_link || '>' ||
                r_data.display_value || '</a>';
    
    END LOOP;
  
    return v_link;
  
  END;

  PROCEDURE pz_fee_assesment(term_in IN tbraccd.tbraccd_term_code%TYPE,
                             pidm_in IN stvterm.stvterm_desc%TYPE) IS
    sfbetrm_rec          sfbetrm%ROWTYPE;
    sfrbtch_rec          sfrbtch%ROWTYPE;
    fee_eff_date         sobterm.sobterm_fee_assessmnt_eff_date%TYPE;
    fee_eff_date_out     VARCHAR2(50);
    return_status_in_out NUMBER := 0;
  BEGIN
    -- sfksels.f_get_sfrbtch_rec
    sfrbtch_rec := sfksels.f_get_sfrbtch_rec(term_in, pidm_in);
    IF sfrbtch_rec.sfrbtch_term_code IS NOT NULL THEN
      sfbetrm_rec := sfksels.f_get_sfbetrm_rec(term_in, pidm_in);
      IF sfbetrm_rec.sfbetrm_ar_ind = 'N' THEN
        fee_eff_date := NVL(GREATEST(sobterm_rec.sobterm_fee_assessmnt_eff_date,
                                     SYSDATE),
                            SYSDATE);
      ELSE
        fee_eff_date := SYSDATE;
      END IF;
      sfkfees.p_processfeeassessment(term_in               => term_in,
                                     pidm_in               => pidm_in,
                                     fa_eff_date_in        => fee_eff_date,
                                     rbt_rfnd_date_in      => TRUNC(SYSDATE),
                                     rule_entry_type_in    => 'R',
                                     create_accd_ind_in    => 'Y',
                                     source_pgm_in         => 'WEB',
                                     commit_ind_in         => 'Y',
                                     save_act_date_out     => fee_eff_date_out,
                                     ignore_sfrfmax_ind_in => 'N',
                                     return_status_in_out  => return_status_in_out);
    END IF; -- sfrbtch_rec.sfrbtch_term_code IS NOT NULL assessment needed
  END pz_fee_assesment;

  PROCEDURE pz_loc_assessment(term_in IN tbraccd.tbraccd_term_code%TYPE,
                              pidm_in IN stvterm.stvterm_desc%TYPE) IS
    /*
    If assessment is enabled (tuition/fee via SOATERM or Room/Board/Phone via SLATERM)
    and assessment is needed (sfrbtch entry or slr...._assess_needed) then trigger
    appropriate assessment routine and commit.
    */
    slbterm_rec             slbterm%ROWTYPE;
    rasg_ar_ind             slrrasg.slrrasg_ar_ind%TYPE;
    masg_ar_ind             slrmasg.slrmasg_ar_ind%TYPE;
    pasg_ar_ind             slrpasg.slrpasg_ar_ind%TYPE;
    slrcolr_rec             slrcolr%ROWTYPE;
    rasg_assess_needed      VARCHAR2(1);
    masg_assess_needed      VARCHAR2(1);
    pasg_assess_needed      VARCHAR2(1);
    loc_fee_assessment_date sobterm.sobterm_fee_assessmnt_eff_date%TYPE;
    orig_chg_ind            VARCHAR2(1);
  
  BEGIN
    -- retrieve the housing control information
    slbterm_rec := slksels.f_get_slbterm_row(term_in);
    IF NVL(slbterm_rec.slbterm_fee_assessment, 'N') = 'Y' THEN
      -- get assess indicators to determine if room, meal, and phone fees
      -- need to be assessed
      slksels.p_get_slrrasg_assess_data(rasg_ar_ind,
                                        rasg_assess_needed,
                                        term_in,
                                        pidm_in);
      slksels.p_get_slrmasg_assess_data(masg_ar_ind,
                                        masg_assess_needed,
                                        term_in,
                                        pidm_in);
      slksels.p_get_slrpasg_assess_data(pasg_ar_ind,
                                        pasg_assess_needed,
                                        term_in,
                                        pidm_in);
    
      -- if room, meal, and phone fees need to be assessed
      IF rasg_assess_needed = 'Y' OR masg_assess_needed = 'Y' OR
         pasg_assess_needed = 'Y' THEN
        loc_fee_assessment_date := NVL(GREATEST(slbterm_rec.slbterm_fee_assessmnt_eff_date,
                                                SYSDATE),
                                       SYSDATE);
        -- insert into collector before processing
        slrcolr_rec                       := slksels.f_get_slrcolr_row(sessionid_in);
        slrcolr_rec.slrcolr_activity_date := SYSDATE;
        slrcolr_rec.slrcolr_pidm          := pidm_in;
        slrcolr_rec.slrcolr_term_code     := term_in;
        IF slrcolr_rec.slrcolr_sessionid IS NOT NULL THEN
          slkmods.p_update_slrcolr(slrcolr_rec);
        ELSE
          slrcolr_rec.slrcolr_sessionid := sessionid_in;
          slkmods.p_insert_slrcolr(slrcolr_rec);
        END IF;
      
        -- assess housing
        IF rasg_assess_needed = 'Y' THEN
          IF TRUNC(SYSDATE) <= slbterm_rec.slbterm_cutoff_date OR
             sfkfees.F_EstablishOriginalCharge(pidm_in        => pidm_in,
                                               term_code_in   => term_in,
                                               source_code_in => 'B') = 'Y' THEN
            orig_chg_ind := 'Y';
          ELSE
            orig_chg_ind := NULL;
          END IF;
          sfkmods.p_assess_room(sessionid_in           => sessionid_in,
                                rasg_ar_ind_in         => rasg_ar_ind,
                                fee_assessment_date_in => loc_fee_assessment_date,
                                term_in                => term_in,
                                pidm_in                => pidm_in,
                                orig_chg_ind_in        => orig_chg_ind);
          slkmods.p_update_slrrasg_assess(term_in, pidm_in);
        END IF; -- IF rasg_assess_needed = 'Y'
      
        -- assess meal
        IF masg_assess_needed = 'Y' THEN
          IF TRUNC(SYSDATE) <= slbterm_rec.slbterm_cutoff_date OR
             sfkfees.F_EstablishOriginalCharge(pidm_in        => pidm_in,
                                               term_code_in   => term_in,
                                               source_code_in => 'V') = 'Y' THEN
            orig_chg_ind := 'Y';
          ELSE
            orig_chg_ind := NULL;
          END IF;
          sfkmods.p_assess_meal(sessionid_in           => sessionid_in,
                                masg_ar_ind_in         => masg_ar_ind,
                                fee_assessment_date_in => loc_fee_assessment_date,
                                term_in                => term_in,
                                pidm_in                => pidm_in,
                                orig_chg_ind_in        => orig_chg_ind);
          slkmods.p_update_slrmasg_assess(term_in, pidm_in);
        END IF; -- IF masg_assess_needed = 'Y'
      
        -- assess phone
        IF pasg_assess_needed = 'Y' THEN
          IF TRUNC(SYSDATE) <= slbterm_rec.slbterm_cutoff_date OR
             sfkfees.F_EstablishOriginalCharge(pidm_in        => pidm_in,
                                               term_code_in   => term_in,
                                               source_code_in => 'U') = 'Y' THEN
            orig_chg_ind := 'Y';
          ELSE
            orig_chg_ind := NULL;
          END IF;
          sfkmods.p_assess_phone(sessionid_in           => sessionid_in,
                                 pasg_ar_ind_in         => pasg_ar_ind,
                                 fee_assessment_date_in => loc_fee_assessment_date,
                                 term_in                => term_in,
                                 pidm_in                => pidm_in,
                                 orig_chg_ind_in        => orig_chg_ind);
          slkmods.p_update_slrpasg_assess(term_in, pidm_in);
        END IF; -- IF pasg_assess_needed = 'Y'
        gb_common.p_commit();
      END IF; --  asses needed
    END IF; -- IF NVL(slbterm_rec.slbterm_fee_assessment, 'N') =  'Y'
  END pz_loc_assessment;

  FUNCTION fz_get_detail_titeliv_status(Input_det_code IN VARCHAR2)
    RETURN VARCHAR2 IS
  
    v_status   VARCHAR(1);
    v_red_font VARCHAR2(20) := '<font color=red>';
  
  BEGIN
  
    SELECT title_iv
      into v_status
      FROM ME_DETAIL_CODE_DESC_VW
     WHERE DETAIL_CODE = Input_det_code;
  
    if v_status is null then
      v_status := 'N';
    end if;
  
    if v_status = 'Y' then
      return v_red_font;
    ELSE
      return '';
    END IF;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN '';
    
  END;

  procedure PZ_INVOICE(P_STUDENT_ID  VARCHAR2,
                       P_TERM_CODE   VARCHAR2,
                       P_XML_INVOICE OUT CLOB) is
  
    invoice_xml_data XMLTYPE;
    l_refcursor      SYS_REFCURSOR;
  
    v_seq                  number := 1;
    v_term_charges         number(15, 3) := 0;
    v_term_payments        number(15, 3) := 0;
    v_term_balance         number(15, 3) := 0;
    v_total_balance        number(15, 3) := 0;
    v_other_term_balance   number(15, 3) := 0;
    v_term_auth_total      number(15, 3) := 0;
    v_term_chrg_memo       number(15, 3) := 0;
    v_term_pay_memo        number(15, 3) := 0;
    v_term_memo_total      number(15, 3) := 0;
    v_grand_total          number(15, 3) := 0;
    v_ipp_future_total     number(15, 3) := 0;
    v_amount_owed_cur_term number(15, 3) := 0;
  
    v_pidm         spriden.spriden_pidm%type;
    row_count      number := 0;
    V_TERM_DESC    stvterm.stvterm_desc%type;
    v_eff_date     tbbterm.tbbterm_web_eff_date%type;
    v_student_name varchar2(100);
    v_code_details varchar2(999);
  
    v_has_depo VARCHAR2(1);
    v_has_cont VARCHAR2(1);
    v_has_expt VARCHAR2(1);
    v_red_font VARCHAR2(20) := '<font color=red>';
  
    CURSOR tbbterm_cur(term VARCHAR2) RETURN tbbterm%ROWTYPE IS
      SELECT *
        FROM TBBTERM
       WHERE tbbterm_term_code = term
         AND tbbterm_detail_web_disp_IND = 'Y';
  
    cursor get_details_c is
      select detail_code,
             detail_code_description,
             charge_payment_ind,
             case
               when charge_payment_ind = 'C' then
                NVL(amount, 0)
               else
                NULL
             end charge,
             case
               when charge_payment_ind = 'P' then
                NVL(amount, 0)
               else
                NULL
             end payment,
             '' balance
        from (SELECT tbraccd_detail_code detail_code,
                     tbbdetc_desc detail_code_description,
                     tbbdetc_type_ind charge_payment_ind,
                     sum(tbraccd_amount) amount,
                     sum(tbraccd_balance) balance
                FROM stvterm, tbbdetc, tbraccd, ttvdcat, spriden c
               WHERE tbbdetc_detail_code = tbraccd_detail_code
                 AND tbbdetc_dcat_code = ttvdcat_code
                 AND stvterm_code(+) = tbraccd_term_code
                 and tbraccd_pidm = spriden_pidm
                 and spriden_change_ind is null
                 and spriden_id = P_STUDENT_ID
                 and tbraccd_term_code = P_TERM_CODE
                 and tbbdetc_type_ind = 'C'
               group by tbraccd_detail_code, tbbdetc_desc, tbbdetc_type_ind
              UNION ALL
              SELECT tbraccd_detail_code detail_code,
                     tbbdetc_desc        detail_code_description,
                     tbbdetc_type_ind    charge_payment_ind,
                     tbraccd_amount      amount,
                     tbraccd_balance     balance
                FROM stvterm, tbbdetc, tbraccd, ttvdcat, spriden c
               WHERE tbbdetc_detail_code = tbraccd_detail_code
                 AND tbbdetc_dcat_code = ttvdcat_code
                 AND stvterm_code(+) = tbraccd_term_code
                 and tbraccd_pidm = spriden_pidm
                 and spriden_change_ind is null
                 and spriden_id = P_STUDENT_ID
                 and tbraccd_term_code = P_TERM_CODE
                 and tbbdetc_type_ind = 'P') student_fee
       ORDER BY charge_payment_ind, detail_code, detail_code_description;
  
    CURSOR get_memo_viewc(pidm_in     spriden.spriden_pidm%TYPE,
                          term_in     stvterm.stvterm_code%TYPE,
                          lv_eff_date DATE DEFAULT SYSDATE) IS
      SELECT tbrmemo_term_code,
             tbrmemo_detail_code,
             tbbdetc_desc,
             tbbdetc_type_ind,
             tbrmemo_amount      amount,
             0                   balance
        FROM tbbdetc, tbrmemo
       WHERE tbrmemo_pidm = pidm_in
         AND tbrmemo_term_code || '' = term_in
         AND tbrmemo_srce_code = 'F'
         AND tbbdetc_detail_code = tbrmemo_detail_code
         AND NVL(TRUNC(TBRMEMO_RELEASE_DATE), TRUNC(lv_eff_date)) <=
             TRUNC(lv_eff_date)
         AND NVL(TBRMEMO_EXPIRATION_DATE, TRUNC(lv_eff_date) + 1) >
             TRUNC(lv_eff_date)
         AND TBRMEMO_BILLING_IND = 'Y'
         AND EXISTS (SELECT 'X'
                FROM TBBTERM
               WHERE TBBTERM_AID_MEMO_DISP_IND = 'Y'
                 AND TBBTERM_TERM_CODE = term_in)
      UNION ALL
      SELECT tbrmemo_term_code,
             tbrmemo_detail_code,
             tbbdetc_desc,
             tbbdetc_type_ind,
             tbrmemo_amount      amount,
             0                   balance
        FROM tbbdetc, tbrmemo
       WHERE tbrmemo_pidm = pidm_in
         AND tbrmemo_term_code || '' = term_in
         AND tbrmemo_srce_code != 'F'
         AND tbbdetc_detail_code = tbrmemo_detail_code
         AND NVL(TRUNC(TBRMEMO_RELEASE_DATE), TRUNC(lv_eff_date)) <=
             TRUNC(lv_eff_date)
         AND NVL(TBRMEMO_EXPIRATION_DATE, TRUNC(lv_eff_date) + 1) >
             TRUNC(lv_eff_date)
         AND TBRMEMO_BILLING_IND = 'Y'
         AND EXISTS (SELECT 'X'
                FROM TBBTERM
               WHERE TBBTERM_OTH_MEMO_DISP_IND = 'Y'
                 AND TBBTERM_TERM_CODE = term_in)
       ORDER BY 5, 3;
  
    CURSOR get_finaid_viewc(pidm_in spriden.spriden_pidm%TYPE,
                            term_in stvterm.stvterm_code%TYPE) IS
      SELECT tbtdtlc_detail_code,
             tbbdetc_desc,
             tbbdetc_type_ind,
             tbtdtlc_effective_date,
             tbtdtlc_amount         amount,
             0                      balance
        FROM tbbdetc, tbtdtlc
       WHERE tbtdtlc_pidm = pidm_in
         AND tbbdetc_detail_code = tbtdtlc_detail_code
       ORDER BY 3;
  
    CURSOR get_ipp_details(pidm    spriden.spriden_pidm%TYPE,
                           term_in VARCHAR2 DEFAULT NULL) is
      select tbraccd_detail_code,
             tbraccd_desc,
             tbraccd_effective_date,
             sum(tbraccd_amount) amount,
             sum(tbraccd_balance) balance
        from taismgr.tbraccd
       where tbraccd_term_code = term_in
         and tbraccd_pidm = pidm
         and tbraccd_detail_code in ('M1P', 'M1P1', 'MPP1')
       group by tbraccd_detail_code, tbraccd_desc, tbraccd_effective_date
       order by tbraccd_effective_date;
  
    v_tiv_status varchar2(100);
  
  begin
  
    v_pidm := GB_COMMON.f_get_pidm(P_STUDENT_ID);
  
    dbms_output.put_line('Pidm:' || v_pidm);
  
    v_tiv_status   := fz_check_tiv_status(v_pidm);
    v_student_name := F_Format_Name(v_pidm, 'FMIL');
  
    sessionid_in := USERENV('SESSIONID');
  
    begin
    
      SELECT STVTERM_DESC
        INTO V_TERM_DESC
        FROM STVTERM
       WHERE STVTERM_CODE = P_TERM_CODE;
    
    exception
      when others then
        V_TERM_DESC := NULL;
    end;
  
    OPEN tbbterm_cur(P_TERM_CODE);
    FETCH tbbterm_cur
      INTO tbbterm_rec;
    IF tbbterm_cur%NOTFOUND THEN
      row_count := 0;
    ELSE
      row_count := 1;
    END IF;
    CLOSE tbbterm_cur;
  
    v_eff_date := NVL(GREATEST(tbbterm_rec.tbbterm_web_eff_date, SYSDATE),
                      SYSDATE);
  
    OPEN l_refcursor FOR
      SELECT P_STUDENT_ID AS STUDENT,
             v_student_name AS STUDENT_NAME,
             P_TERM_CODE AS TERM,
             V_TERM_DESC AS TERM_DESC,
             v_tiv_status || ' ' || fz_get_detail_link('TIV') AS TITLE_IV_STATUS,
             v_eff_date AS AS_ON_DATE
        FROM DUAL;
  
    invoice_xml_data := XMLTYPE(l_refcursor);
  
    close l_refcursor;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select v_student_name || '<br>' || 'ID: ' || P_STUDENT_ID ||
                   '<br>' || 'Title IV Status: ' || v_tiv_status ||' '||
                   fz_get_detail_link('TIV') DETAIL_CODE_DESCRIPTION,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   '' BALANCE,
                   'E-Statement for ' || V_TERM_DESC || ' TERM' GROUP_NAME,
                   'N' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    /*SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'ID: ' || P_STUDENT_ID DETAIL_CODE_DESCRIPTION,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   '' BALANCE,
                   'E-Statement for ' || V_TERM_DESC || ' TERM' GROUP_NAME,
                   'N' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
    
    v_seq := v_seq + 1;
    
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Title IV Status: ' || v_tiv_status || ' ' ||
                   fz_get_detail_link('TIV') DETAIL_CODE_DESCRIPTION,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   '' BALANCE,
                   'E-Statement for ' || V_TERM_DESC || ' TERM' GROUP_NAME,
                   'N' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
    
    v_seq := v_seq + 1;*/
  
    if row_count = 0 then
    
      SELECT INSERTCHILDXML(invoice_xml_data,
                            '/ROWSET',
                            'ERROR',
                            XMLElement("ERROR",
                                       XMLElement("ERROR_MSG", ERROR_MSG)))
        into invoice_xml_data
        FROM (select 'Cannot show this term or invalid term' ERROR_MSG
                FROM DUAL) XD;
    
      P_XML_INVOICE := invoice_xml_data.getClobVal;
      return;
    end if;
  
    -- Include current assessments -
  
    -- retrieve the control information from sobterm
    sobterm_rec := soksels.f_get_sobterm_row(P_TERM_CODE);
    IF NVL(sobterm_rec.sobterm_fee_assess_vr, '*') = 'Y' THEN
      pz_fee_assesment(P_TERM_CODE, v_pidm);
      pz_loc_assessment(P_TERM_CODE, v_pidm);
    END IF;
  
    -- populate for installments
    -- assigns auto plan depending on set up
    tbbctrl_rec := tsksels.f_get_tbbctrl_rec;
    IF tbbctrl_rec.TBBCTRL_AUTO_INSTALL_PLAN_IND = 'Y' THEN
      TSKPIST.P_Populate_TBVINSP(P_TERM_CODE, v_pidm);
    END IF;
  
    -- financial aid
    IF tbbterm_rec.tbbterm_aid_auth_disp_ind = 'Y' THEN
      IF genutil.product_installed('R') = 'Y' THEN
        tskmods.P_Populate_from_rprauth(v_pidm, P_TERM_CODE);
      END IF;
    END IF;
  
    ---------------------------------------------------------------------------------------------------
  
    /*
    Include current credits -
    If "Other" memos are enabled and student has contract or exemption assigned for
    the term, call tskfunc.p_process_cont_expt in INVOICING mode; if student
    has Auto Release deposit for the term, call tb_deposit.p_release in INVOICING mode
    
          Deposits      - All deposits with the TBRDEPO_AUTO_RELEASE_IND
                          equal 'Y' and TBRDEPO_RELEASE_DATE <= today for
                          the current person and term will be processed.
    
          Exemptions    - The exemption for the person and term will be
                          processed. The registration total will be used
                          to calculate the exemption.
    
          Contracts     - All contracts for the person and term will be
                          processed. The registration total will be used
                          to calculate the contracts.
    */
  
    IF tbbterm_rec.TBBTERM_OTH_MEMO_DISP_IND = 'Y' THEN
      v_has_cont := tsksels.f_tbbcstu_exists(P_TERM_CODE, v_pidm);
      v_has_expt := tsksels.f_tbbestu_exists(P_TERM_CODE, v_pidm);
      v_has_depo := tsksels.f_tbrdepo_auto_rel_exists(P_TERM_CODE, v_pidm);
    
      IF v_has_depo = 'Y' THEN
        tskmods.p_delete_tbrmemo_by_srce(pidm_in => v_pidm,
                                         term_in => P_TERM_CODE,
                                         srce_in => 'D');
      
        tb_deposit.p_release(p_pidm           => v_pidm,
                             p_term_code      => P_TERM_CODE,
                             p_effective_date => v_eff_date,
                             p_user           => USER,
                             p_session_number => 0,
                             p_release_mode   => 'A',
                             p_run_mode       => 'INVOICING');
      END IF;
    
      IF v_has_cont = 'Y' OR v_has_expt = 'Y' THEN
      
        tskfunc.p_process_cont_expt(pidm_in      => v_pidm,
                                    term_in      => P_TERM_CODE,
                                    sessionid_in => sessionid_in,
                                    has_cont_in  => v_has_cont,
                                    has_expt_in  => v_has_expt,
                                    run_mode_in  => 'INVOICING');
      END IF;
    END IF;
  
    --Current term
  
    /* select INSERTCHILDXML(invoice_xml_data,
                        '/ROWSET',
                        'CURR_TERM',
                        XMLType('<CURR_TERM> </CURR_TERM>'))
    into invoice_xml_data
    from dual;*/
  
    row_count := 0;
  
    FOR r_get_data in get_details_c Loop
      row_count      := row_count + 1;
      v_code_details := '';
      v_red_font     := '';
      v_code_details := fz_get_detail_desc(r_get_data.detail_code);
      v_red_font     := fz_get_detail_titeliv_status(r_get_data.detail_code);
    
      dbms_output.put_line(v_code_details);
    
      SELECT INSERTCHILDXML(invoice_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("DETAIL_CODE_DESCRIPTION",
                                                  DETAIL_CODE_DESCRIPTION),
                                       XMLElement("CODE_DETAILS",
                                                  CODE_DETAILS),
                                       XMLElement("CHARGE", CHARGE),
                                       XMLElement("PAYMENT", PAYMENT),
                                       XMLElement("BALANCE", BALANCE),
                                       XMLElement("GROUP", GROUP_NAME),
                                       XMLElement("SUMMARY", SUMMARY),
                                       XMLElement("SEQ_NO", SEQ_NO)))
        into invoice_xml_data
        FROM (SELECT v_red_font || r_get_data.detail_code_description || ' ' ||
                     fz_get_detail_link(r_get_data.detail_code) detail_code_description,
                     v_code_details code_details,
                     r_get_data.CHARGE,
                     r_get_data.PAYMENT,
                     r_get_data.BALANCE,
                     'Current Term Details' GROUP_NAME,
                     'N' SUMMARY,
                     v_seq SEQ_NO
                FROM DUAL) XD;
    
      v_seq           := v_seq + 1;
      v_term_payments := v_term_payments + nvl(r_get_data.PAYMENT, 0);
      v_term_charges  := v_term_charges + nvl(r_get_data.CHARGE, 0);
    
    end loop;
  
    v_term_balance := v_term_charges - v_term_payments;
  
    --insert current term summary
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Total Current Term Charges' detail_code_description,
                   '' code_details,
                   NVL(v_term_charges, 0) CHARGE,
                   '' PAYMENT,
                   '' BALANCE,
                   'Current Term Details' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Total Current Term Payments' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   NVL(v_term_payments, 0) PAYMENT,
                   '' BALANCE,
                   'Current Term Details' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Total Current Term Balance' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   NVL(v_term_balance, 0) BALANCE,
                   'Current Term Details' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    --Authorised FA
  
    /*select INSERTCHILDXML(invoice_xml_data,
                        '/ROWSET',
                        'AUTH_FA',
                        XMLType('<AUTH_FA> </AUTH_FA>'))
    into invoice_xml_data
    from dual;*/
  
    v_term_auth_total := 0;
    row_count         := 0;
  
    IF tbbterm_rec.tbbterm_aid_auth_disp_ind = 'Y' THEN
    
      FOR r_get_fa IN get_finaid_viewc(v_pidm, p_term_code) LOOP
      
        row_count := row_count + 1;
      
        v_code_details := '';
      
        v_code_details := fz_get_detail_desc(r_get_fa.tbtdtlc_detail_code);
      
        v_term_auth_total := v_term_auth_total + r_get_fa.amount * (-1);
      
        if r_get_fa.amount <> 0 then
        
          SELECT INSERTCHILDXML(invoice_xml_data,
                                '/ROWSET',
                                'RECORD',
                                XMLElement("RECORD",
                                           XMLElement("DETAIL_CODE_DESCRIPTION",
                                                      DETAIL_CODE_DESCRIPTION),
                                           XMLElement("CODE_DETAILS",
                                                      CODE_DETAILS),
                                           XMLElement("CHARGE", CHARGE),
                                           XMLElement("PAYMENT", PAYMENT),
                                           XMLElement("BALANCE", BALANCE),
                                           XMLElement("GROUP", GROUP_NAME),
                                           XMLElement("SUMMARY", SUMMARY),
                                           XMLElement("SEQ_NO", SEQ_NO)))
            into invoice_xml_data
            FROM (select r_get_fa.tbbdetc_desc detail_code_description,
                         v_code_details code_details,
                         '' CHARGE,
                         r_get_fa.amount PAYMENT,
                         r_get_fa.balance BALANCE,
                         'Authorized Financial Aid' GROUP_NAME,
                         'N' SUMMARY,
                         v_seq SEQ_NO
                    FROM DUAL) XD;
        
          v_seq := v_seq + 1;
        
        end if;
      
      END LOOP;
    
    END IF;
  
    --insert FA  summary
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Authorized Financial Aid Balance:' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_term_auth_total BALANCE,
                   'Authorized Financial Aid' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    --Memo FA
  
    row_count := 0;
  
    IF tbbterm_rec.TBBTERM_AID_MEMO_DISP_IND = 'Y' OR
       tbbterm_rec.TBBTERM_OTH_MEMO_DISP_IND = 'Y' THEN
    
      FOR r_get_memo IN get_memo_viewc(v_pidm, p_term_code, v_eff_date) LOOP
      
        IF r_get_memo.amount <> 0 THEN
          row_count := row_count + 1;
          IF r_get_memo.tbbdetc_type_ind = 'C' THEN
          
            v_term_chrg_memo := v_term_chrg_memo + r_get_memo.amount;
          
            SELECT INSERTCHILDXML(invoice_xml_data,
                                  '/ROWSET',
                                  'RECORD',
                                  XMLElement("RECORD",
                                             XMLElement("DETAIL_CODE_DESCRIPTION",
                                                        DETAIL_CODE_DESCRIPTION),
                                             XMLElement("CODE_DETAILS",
                                                        CODE_DETAILS),
                                             XMLElement("CHARGE", CHARGE),
                                             XMLElement("PAYMENT", PAYMENT),
                                             XMLElement("BALANCE", BALANCE),
                                             XMLElement("GROUP", GROUP_NAME),
                                             XMLElement("SUMMARY", SUMMARY),
                                             XMLElement("SEQ_NO", SEQ_NO)))
              into invoice_xml_data
              FROM (select r_get_memo.tbbdetc_desc detail_code_description,
                           '' code_details,
                           r_get_memo.amount CHARGE,
                           '' PAYMENT,
                           '' BALANCE,
                           'Memo Financial Aid' GROUP_NAME,
                           'N' SUMMARY,
                           v_seq SEQ_NO
                      FROM DUAL) XD;
          
            v_seq := v_seq + 1;
          
          ELSE
            v_term_pay_memo := v_term_pay_memo + r_get_memo.amount;
          
            SELECT INSERTCHILDXML(invoice_xml_data,
                                  '/ROWSET',
                                  'RECORD',
                                  XMLElement("RECORD",
                                             XMLElement("DETAIL_CODE_DESCRIPTION",
                                                        DETAIL_CODE_DESCRIPTION),
                                             XMLElement("CODE_DETAILS",
                                                        CODE_DETAILS),
                                             XMLElement("CHARGE", CHARGE),
                                             XMLElement("PAYMENT", PAYMENT),
                                             XMLElement("BALANCE", BALANCE),
                                             XMLElement("GROUP", GROUP_NAME),
                                             XMLElement("SUMMARY", SUMMARY),
                                             XMLElement("SEQ_NO", SEQ_NO)))
              into invoice_xml_data
              FROM (select r_get_memo.tbbdetc_desc detail_code_description,
                           '' code_details,
                           '' CHARGE,
                           r_get_memo.amount PAYMENT,
                           '' BALANCE,
                           'Memo Financial Aid' GROUP_NAME,
                           'N' SUMMARY,
                           v_seq SEQ_NO
                      FROM DUAL) XD;
          
            v_seq := v_seq + 1;
          
          END IF;
        
        END IF;
      
      END LOOP;
    
    END IF;
  
    --insert Memo  summary
  
    v_term_memo_total := v_term_chrg_memo - v_term_pay_memo;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Memo Financial Aid Balance:' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_term_memo_total BALANCE,
                   'Memo Financial Aid' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_Seq + 1;
  
    /*select INSERTCHILDXML(invoice_xml_data,
                        '/ROWSET',
                        'FA',
                        XMLType('<FA><' || v_auth_fa || '>' || '20' || '</' ||
                                v_auth_fa || '></FA>'))
    into invoice_xml_data
    from dual;*/
  
    --Other term balance
  
    v_total_balance := NVL(tb_receivable.f_sum_balance(p_pidm => v_pidm), 0);
  
    v_other_term_balance := v_total_balance - v_term_balance;
  
    dbms_output.put_line('Payment: ' || v_term_payments || ';Charges:' ||
                         v_term_charges || ';Balance:' || v_term_balance ||
                         ';Other term balance:' || v_other_term_balance);
  
    --insert grand summary
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Total Current Term Balance: ' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_term_balance BALANCE,
                   'Total Summary' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Less Authorized Financial Aid: ' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_term_auth_total BALANCE,
                   'Total Summary' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Less Memo Financial Aid: ' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_term_memo_total BALANCE,
                   'Total Summary' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'Total of Other Term(s): ' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_other_term_balance BALANCE,
                   'Total Summary' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    v_grand_total := nvl(v_term_balance, 0) + nvl(v_term_auth_total, 0) +
                     nvl(v_term_memo_total, 0) +
                     nvl(v_other_term_balance, 0);
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select 'TOTAL ACCOUNT BALANCE DUE: ' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   v_grand_total BALANCE,
                   'Total Summary' GROUP_NAME,
                   'Y' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    v_seq := v_seq + 1;
  
    --insert ipp  and installment details
  
    row_count := 0;
  
    --Get IPP installment details
    FOR r_get_data in get_ipp_details(v_pidm, P_TERM_CODE) LOOP
    
      if v_eff_date < r_get_data.tbraccd_effective_date then
      
        v_ipp_future_total := v_ipp_future_total + r_get_data.balance;
      
      end if;
    
      row_count := row_count + 1;
    
      SELECT INSERTCHILDXML(invoice_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("DETAIL_CODE_DESCRIPTION",
                                                  DETAIL_CODE_DESCRIPTION),
                                       XMLElement("CODE_DETAILS",
                                                  CODE_DETAILS),
                                       XMLElement("CHARGE", CHARGE),
                                       XMLElement("PAYMENT", PAYMENT),
                                       XMLElement("BALANCE", BALANCE),
                                       XMLElement("GROUP", GROUP_NAME),
                                       XMLElement("SUMMARY", SUMMARY),
                                       XMLElement("SEQ_NO", SEQ_NO)))
        into invoice_xml_data
        FROM (select r_get_data.tbraccd_desc detail_code_description,
                     '' code_details,
                     r_get_data.tbraccd_effective_date CHARGE,
                     r_get_data.amount PAYMENT,
                     r_get_data.balance BALANCE,
                     'IPP Details' GROUP_NAME,
                     'N' SUMMARY,
                     v_seq + 100000 SEQ_NO
                FROM DUAL) XD;
    
      v_seq := v_seq + 1;
    
    END LOOP;
  
    v_amount_owed_cur_term := v_term_balance + v_term_auth_total +
                              v_term_memo_total - v_ipp_future_total;
  
    -- insert IPP summary
    IF row_count > 0 then
    
      SELECT INSERTCHILDXML(invoice_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("DETAIL_CODE_DESCRIPTION",
                                                  DETAIL_CODE_DESCRIPTION),
                                       XMLElement("CODE_DETAILS",
                                                  CODE_DETAILS),
                                       XMLElement("CHARGE", CHARGE),
                                       XMLElement("PAYMENT", PAYMENT),
                                       XMLElement("BALANCE", BALANCE),
                                       XMLElement("GROUP", GROUP_NAME),
                                       XMLElement("SUMMARY", SUMMARY),
                                       XMLElement("SEQ_NO", SEQ_NO)))
        into invoice_xml_data
        FROM (select 'Amount Owed in current term:' detail_code_description,
                     '' code_details,
                     '' CHARGE,
                     '' PAYMENT,
                     v_amount_owed_cur_term BALANCE,
                     'Amount Owed' GROUP_NAME,
                     'Y' SUMMARY,
                     v_seq SEQ_NO
                FROM DUAL) XD;
    
      v_seq := v_seq + 1;
    
      SELECT INSERTCHILDXML(invoice_xml_data,
                            '/ROWSET',
                            'RECORD',
                            XMLElement("RECORD",
                                       XMLElement("DETAIL_CODE_DESCRIPTION",
                                                  DETAIL_CODE_DESCRIPTION),
                                       XMLElement("CODE_DETAILS",
                                                  CODE_DETAILS),
                                       XMLElement("CHARGE", CHARGE),
                                       XMLElement("PAYMENT", PAYMENT),
                                       XMLElement("BALANCE", BALANCE),
                                       XMLElement("GROUP", GROUP_NAME),
                                       XMLElement("SUMMARY", SUMMARY),
                                       XMLElement("SEQ_NO", SEQ_NO)))
        into invoice_xml_data
        FROM (select 'Future IPP Payments:' || ' ' ||
                     fz_get_detail_link('IPP') detail_code_description,
                     '' code_details,
                     '' CHARGE,
                     '' PAYMENT,
                     v_ipp_future_total BALANCE,
                     'Amount Owed' GROUP_NAME,
                     'Y' SUMMARY,
                     v_seq SEQ_NO
                FROM DUAL) XD;
    
      v_seq := v_seq + 1;
    
    END IF;
  
    SELECT INSERTCHILDXML(invoice_xml_data,
                          '/ROWSET',
                          'RECORD',
                          XMLElement("RECORD",
                                     XMLElement("INFO", INFO),
                                     XMLElement("DETAIL_CODE_DESCRIPTION",
                                                DETAIL_CODE_DESCRIPTION),
                                     XMLElement("CODE_DETAILS", CODE_DETAILS),
                                     XMLElement("CHARGE", CHARGE),
                                     XMLElement("PAYMENT", PAYMENT),
                                     XMLElement("BALANCE", BALANCE),
                                     XMLElement("GROUP", GROUP_NAME),
                                     XMLElement("SUMMARY", SUMMARY),
                                     XMLElement("SEQ_NO", SEQ_NO)))
      into invoice_xml_data
      FROM (select (select TWGRINFO_TEXT
                      from WTAILOR.TWGRINFO
                     where TWGRINFO_NAME = 'web_invoice.p_invoice'
                       and TWGRINFO_LABEL = 'MOBTEXT'
                       and rownum = 1) INFO,
                   '' detail_code_description,
                   '' code_details,
                   '' CHARGE,
                   '' PAYMENT,
                   '' BALANCE,
                   'Information' GROUP_NAME,
                   'N' SUMMARY,
                   v_seq SEQ_NO
              FROM DUAL) XD;
  
    if invoice_xml_data is not null then
      P_XML_INVOICE := invoice_xml_data.getClobVal;
    end if;
  
    GB_COMMON.p_rollback;
  
  exception
    when others then
    
      SELECT INSERTCHILDXML(invoice_xml_data,
                            '/ROWSET',
                            'ERROR',
                            XMLElement("ERROR",
                                       XMLElement("ERROR_MSG", ERROR_MSG)))
        into invoice_xml_data
        FROM (select 'System Error! Could not get data.' ERROR_MSG FROM DUAL) XD;
    
      P_XML_INVOICE := invoice_xml_data.getClobVal;
    
      GB_COMMON.p_rollback;
    
  end PZ_INVOICE;

end me_account_objects;

/

