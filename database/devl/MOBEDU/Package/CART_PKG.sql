PROMPT CREATE OR REPLACE PACKAGE mobedu.cart_pkg
CREATE OR REPLACE package mobedu.cart_pkg as
  function fz_check_cart(p_student_id in varchar2,
                         p_crn        in varchar2,
                         p_term       in varchar2) return varchar2;

  PROCEDURE pz_add_to_cart(p_student_id in varchar2,
                           p_crn        in varchar2,
                           p_term       in varchar2,
                           p_error_msg  out varchar2);

  procedure pz_remove_from_cart(p_student_id in varchar2,
                                p_crn        in varchar2,
                                p_term       in varchar2,
                                p_error_msg  out varchar2);

  procedure pz_clear_cart(p_student_id in varchar2,
                          p_term       in varchar2,
                          p_error_msg  out varchar2);
end cart_pkg;


/

