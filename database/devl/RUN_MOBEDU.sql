spool R_MOBEDU.log
PROMPT ====================================================
PROMPT Installing MOBEDU schema and populating data
PROMPT ====================================================
PROMPT
PROMPT ====================================================
PROMPT Creating MOBEDU User
PROMPT ====================================================
@@MOBEDU\Schema\CREATE_MOBEDU.sql
@@MOBEDU\Schema\grant.sql
@@MOBEDU\Type\T_ARRAY_TYPE.sql
@@MOBEDU\Table\ADMIN_PRIV.sql
@@MOBEDU\Table\ADMIN_ROLE.sql
@@MOBEDU\Table\ADMIN_USER.sql
@@MOBEDU\Table\CAMPUS.sql
@@MOBEDU\Table\CUST_MAT.sql
@@MOBEDU\Table\DELETE_THIS.sql
@@MOBEDU\Table\EMERGENCY_CONTACTS.sql
@@MOBEDU\Table\ERROR_TABLE_TEMP.sql
@@MOBEDU\Table\FEEDS.sql
@@MOBEDU\Table\FEEDS_TEMP.sql
@@MOBEDU\Table\MAPS.sql
@@MOBEDU\Table\MOBEDU_CART.sql
@@MOBEDU\Table\MODULE.sql
@@MOBEDU\Table\MODULES.sql
@@MOBEDU\Table\NOTICE.sql
@@MOBEDU\Table\NOTICELOG.sql
@@MOBEDU\Table\NOTICEPOPULATION.sql
@@MOBEDU\Table\NOTICETYPE.sql
@@MOBEDU\Table\POPULATIONTYPE.sql
@@MOBEDU\Table\PRIVILEGE.sql
@@MOBEDU\Table\ROLE.sql
@@MOBEDU\Table\STU_HOUSING_INFO.sql
@@MOBEDU\Table\ADMIN_PRIV~INX.sql
@@MOBEDU\Table\ADMIN_ROLE~INX.sql
@@MOBEDU\Table\ADMIN_USER~INX.sql
@@MOBEDU\Table\NOTICE~INX.sql
@@MOBEDU\Table\NOTICELOG~INX.sql
@@MOBEDU\Table\NOTICEPOPULATION~INX.sql
@@MOBEDU\Table\NOTICETYPE~INX.sql
@@MOBEDU\Table\POPULATIONTYPE~INX.sql
@@MOBEDU\Table\ADMIN_ROLE~UNQ.sql
@@MOBEDU\Table\ADMIN_USER~UNQ.sql
@@MOBEDU\Table\NOTICE~UNQ.sql
@@MOBEDU\Table\NOTICELOG~UNQ.sql
@@MOBEDU\Table\NOTICEPOPULATION~UNQ.sql
@@MOBEDU\Table\NOTICETYPE~UNQ.sql
@@MOBEDU\Table\POPULATIONTYPE~UNQ.sql
@@MOBEDU\Table\NOTICE~FK.sql
@@MOBEDU\Table\NOTICELOG~FK.sql
@@MOBEDU\Table\NOTICEPOPULATION~FK.sql
@@MOBEDU\Table\feeds_audit.sql
@@MOBEDU\Table\maps_audit.sql
@@MOBEDU\Table\privilege_audit.sql
@@MOBEDU\Table\emergency_contacts_audit.sql
@@MOBEDU\Table\feedback.sql
@@MOBEDU\Table\help.sql
@@MOBEDU\Table\help_audit.sql
@@MOBEDU\Table\SERVICE_AREA.sql
@@MOBEDU\Table\FEEDBACK_DATA.sql
@@MOBEDU\Sequence\NOTICELOG_SEQ.sql
@@MOBEDU\Sequence\NOTICETYPE_SEQ.sql
@@MOBEDU\Sequence\NOTICE_SEQ.sql
@@MOBEDU\Procedure\PZ_DROP_COURSE.sql
@@MOBEDU\Procedure\PZ_ME_AUTHENTICATION.sql
@@MOBEDU\Procedure\P_TEST.sql
@@MOBEDU\Procedure\SEND_EMAIL.sql
@@MOBEDU\Package\ADD_COURSE_PKG_CORQ.sql
@@MOBEDU\Package\CART_PKG.sql
@@MOBEDU\Package\ME_ACCOUNT_OBJECTS.sql
@@MOBEDU\Package\ME_ALT_PIN_PKG.sql
@@MOBEDU\Package\ME_BWCKCOMS.sql
@@MOBEDU\Package\ME_BWCKGENS.sql
@@MOBEDU\Package\ME_BWCKREGS.sql
@@MOBEDU\Package\ME_REG_UTILS.sql
@@MOBEDU\Package\ME_SFKFEES.sql
@@MOBEDU\Package\ME_VALID.sql
@@MOBEDU\Package\Z_CM_MOBILE_CAMPUS.sql
@@MOBEDU\View\ACCOUNT_SUMMARY_VIEW.sql
@@MOBEDU\View\ACCT_SUMM_VW.sql
@@MOBEDU\View\CART_DETAILS_VW.sql
@@MOBEDU\View\CHARGE_PAY_DETAIL_VW.sql
@@MOBEDU\View\CONTACT_VW.sql
@@MOBEDU\View\TERMS_TO_REGISTER_VW.sql
@@MOBEDU\View\COURSE_SEARCH_VW.sql
@@MOBEDU\View\CRSE_MEET_VW.sql
@@MOBEDU\View\GENERAL_PERSON_VW.sql
@@MOBEDU\View\ME_STUDENT_CRSE_REGSTRN_VW.sql
@@MOBEDU\View\PERSON_VW.sql
@@MOBEDU\View\STUDENT_ACCOUNT_DETAIL.sql
@@MOBEDU\View\STU_CURCULAM_INFO.sql
@@MOBEDU\View\STU_HOLD_INFO.sql
@@MOBEDU\View\ACCOUNT_SUMMARY_BY_TERM_VW.sql
@@MOBEDU\View\SUMMARY_BY_TERM_VW.sql
@@MOBEDU\View\TERMS_TO_BURSAR_VW.sql
@@MOBEDU\View\TERM_COURSE_DETAILS_VW.sql
@@MOBEDU\View\VW_TERM_CHARGES.sql
@@MOBEDU\View\VW_TERM_PAYMENTS.sql
@@MOBEDU\View\mobedu_holiday_vw.sql
@@MOBEDU\PackageBody\ADD_COURSE_PKG_CORQ.sql
@@MOBEDU\PackageBody\CART_PKG.sql
@@MOBEDU\PackageBody\ME_ACCOUNT_OBJECTS.sql
@@MOBEDU\PackageBody\ME_ALT_PIN_PKG.sql
@@MOBEDU\PackageBody\ME_BWCKCOMS.sql
@@MOBEDU\PackageBody\ME_BWCKGENS.sql
@@MOBEDU\PackageBody\ME_BWCKREGS.sql
@@MOBEDU\PackageBody\ME_REG_UTILS.sql
@@MOBEDU\PackageBody\ME_SFKFEES.sql
@@MOBEDU\PackageBody\ME_VALID.sql
@@MOBEDU\PackageBody\Z_CM_MOBILE_CAMPUS.sql
@@MOBEDU\Data\maps.sql
@@MOBEDU\Data\emergency_contacts.sql
@@MOBEDU\Data\modules.sql
@@MOBEDU\Data\module.sql
@@MOBEDU\Data\privilege.sql
@@MOBEDU\Data\role.sql
@@MOBEDU\Data\feeds.sql
@@MOBEDU\Data\admin_role.sql
@@MOBEDU\Data\admin_priv.sql
@@MOBEDU\Data\admin_user.sql
commit;
EXEC Dbms_Utility.compile_schema(schema => 'MOBEDU');

show errors;

spool off

